﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
Imports System.Drawing
Imports AjaxControlToolkit
Imports System.IO
Imports System.Web.Services

#End Region

Partial Class wPgPerformanceEvaluation
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private mstrModuleName As String = "frmPerformanceEvaluation"
    Private objEAnalysisMst As New clsevaluation_analysis_master
    Private objGoalsTran As New clsgoal_analysis_tran
    Private objCAssessTran As New clscompetency_analysis_tran
    Private objCCustomTran As New clscompeteny_customitem_tran

    Private menAction As enAction = enAction.ADD_ONE
    Private mintAssessAnalysisUnkid As Integer = -1
    'Private mintEmplId As Integer = 0
    'Private mintPeriodId As Integer = 0
    Private mintYearUnkid As Integer = 0
    'Private mintAssessorId As Integer = 0
    Private menAssess As enAssessmentMode = enAssessmentMode.SELF_ASSESSMENT
    Private mdtBSC_Evaluation As DataTable
    Private mdtGE_Evaluation As DataTable
    Private mdtCustomEvaluation As DataTable
    Private dtBSC_TabularGrid As New DataTable
    Private dtGE_TabularGrid As New DataTable
    Private dtCustomTabularGrid As New DataTable
    Private iWeightTotal As Decimal = 0
    Private dsHeaders As New DataSet
    Private iHeaderId As Integer = 0
    Private iExOrdr As Integer = 0
    Private iLinkedFieldId As Integer
    Private iMappingUnkid As Integer
    Private xVal As Integer = 1
    Private xTotAssignedWeight As Decimal = 0

    Private dtCItems As New DataTable
    Private mblnItemAddEdit As Boolean = False
    Private mstriEditingGUID As String = String.Empty
    'S.SANDEEP [21 JAN 2015] -- START
    Private mdecItemWeight As Decimal = 0
    Private mdecMaxScale As Decimal = 0
    'S.SANDEEP [21 JAN 2015] -- END

    'S.SANDEEP [12 OCT 2016] -- START
    Private mblnIsMatchCompetencyStructure As Boolean = False
    'S.SANDEEP [12 OCT 2016] -- END

    'S.SANDEEP |05-APR-2019| -- START
    Dim disBSCColumn As Dictionary(Of String, Integer) = Nothing
    'S.SANDEEP |05-APR-2019| -- END

#End Region

#Region " Page Event(s) "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'S.SANDEEP |05-APR-2019| -- START
            disBSCColumn = dgvBSC.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvBSC.Columns.IndexOf(x))
            'S.SANDEEP |05-APR-2019| -- END
            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmPerformanceEvaluation"
            StrModuleName2 = "mnuAssessment"
            StrModuleName3 = "mnuPerformaceEvaluation"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")

            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            If IsPostBack = False Then
                If Session("Action") IsNot Nothing AndAlso Session("Unkid") IsNot Nothing Then
                    Call ClearForm_Values()
                    mintAssessAnalysisUnkid = Session("Unkid")
                    ''Session("Action") 1:ADD_CONTINUE 2: ADD_ONE 3 :EDIT_ONE
                    If CInt(Session("Action")) = 0 Then
                        menAction = enAction.ADD_ONE
                    ElseIf CInt(Session("Action")) = 1 Then
                        menAction = enAction.EDIT_ONE
                    ElseIf CInt(Session("Action")) = 2 Then
                        menAction = enAction.ADD_CONTINUE
                    End If


                    txtInstruction.Text = Session("Assessment_Instructions")
                    txtInstruction.Height = Unit.Pixel(430)

                    objlblCaption.Text = Language.getMessage(mstrModuleName, 1, "Instructions")
                    objpnlInstruction.Visible = True

                    objbtnNext.Enabled = False : objbtnBack.Enabled = False
                    objlblValue1.Visible = False : objlblValue2.Visible = False
                    objlblValue3.Visible = False : objlblValue4.Visible = False

                    Call FillCombo()
                    If menAction = enAction.EDIT_ONE Then
                        objEAnalysisMst._Analysisunkid = mintAssessAnalysisUnkid
                        cboEmployee.Enabled = False
                        cboPeriod.Enabled = False
                        BtnSearch.Enabled = False
                        BtnReset.Enabled = False
                    End If
                    Call GetValue()
                    objGoalsTran._AnalysisUnkid = mintAssessAnalysisUnkid
                    mdtBSC_Evaluation = objGoalsTran._DataTable

                    objCAssessTran._ConsiderWeightAsNumber = CBool(Session("ConsiderItemWeightAsNumber"))
                    'S.SANDEEP [09 OCT 2015] -- START
                    objCAssessTran._SelfAssignCompetencies = CBool(Session("Self_Assign_Competencies"))
                    'S.SANDEEP [09 OCT 2015] -- END
                    objCAssessTran._AnalysisUnkid = mintAssessAnalysisUnkid
                    mdtGE_Evaluation = objCAssessTran._DataTable

                    objCCustomTran._AnalysisUnkid = mintAssessAnalysisUnkid
                    objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)
                    'S.SANDEEP [29 DEC 2015] -- START

                    'Shani (26-Sep-2016) -- Start
                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                    'objCCustomTran._AllowCustomItemInPlanning = CBool(Session("IncludeCustomItemInPlanning"))
                    objCCustomTran._IsCompanyNeedReviewer = CBool(Session("IsCompanyNeedReviewer"))
                    'Shani (26-Sep-2016) -- End


                    objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
                    'S.SANDEEP [29 DEC 2015] -- END
                    mdtCustomEvaluation = objCCustomTran._DataTable
                    If menAction = enAction.EDIT_ONE Then
                        Call Fill_BSC_Evaluation()
                        Call Fill_GE_Evaluation()
                        Call Fill_Custom_Grid()
                        Call BtnSearch_Click(sender, e)
                    End If
                Else
                    Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_SelfEvaluationList.aspx", False)
                End If
            End If

            'S.SANDEEP |14-MAR-2019| -- START
            If menAction <> enAction.EDIT_ONE Then
                If CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                    Dim intCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
                    cboPeriod.SelectedValue = intCurrentPeriodId
                    cboPeriod_SelectedIndexChanged(New Object(), New EventArgs())
                End If
            End If
            'S.SANDEEP |14-MAR-2019| -- END

            If Me.ViewState("Action") IsNot Nothing Then
                menAction = Me.ViewState("Action")
            End If

            If Me.ViewState("AssessAnalysisUnkid") IsNot Nothing Then
                mintAssessAnalysisUnkid = Me.ViewState("AssessAnalysisUnkid")
            End If

            menAssess = enAssessmentMode.SELF_ASSESSMENT

            'If Me.ViewState("BSC_Evaluation") IsNot Nothing Then
            '    mdtBSC_Evaluation = Me.ViewState("BSC_Evaluation")
            'End If

            If Me.Session("BSC_Evaluation") IsNot Nothing Then
                mdtBSC_Evaluation = Me.Session("BSC_Evaluation")
            End If

            'If Me.ViewState("GE_Evaluation") IsNot Nothing Then
            '    mdtGE_Evaluation = Me.ViewState("GE_Evaluation")
            'End If
            If Me.Session("GE_Evaluation") IsNot Nothing Then
                mdtGE_Evaluation = Me.Session("GE_Evaluation")
            End If

            'If Me.ViewState("CustomEvaluation") IsNot Nothing Then
            '    mdtCustomEvaluation = Me.ViewState("CustomEvaluation")
            'End If
            If Me.Session("CustomEvaluation") IsNot Nothing Then
                mdtCustomEvaluation = Me.Session("CustomEvaluation")
            End If

            'If Me.ViewState("BSC_TabularGrid") IsNot Nothing Then
            '    dtBSC_TabularGrid = Me.ViewState("BSC_TabularGrid")
            'End If
            If Me.Session("BSC_TabularGrid") IsNot Nothing Then
                dtBSC_TabularGrid = Me.Session("BSC_TabularGrid")
            End If

            'If Me.ViewState("GE_TabularGrid") IsNot Nothing Then
            '    dtGE_TabularGrid = Me.ViewState("GE_TabularGrid")
            'End If
            If Me.Session("GE_TabularGrid") IsNot Nothing Then
                dtGE_TabularGrid = Me.Session("GE_TabularGrid")
            End If

            'If Me.ViewState("CustomTabularGrid") IsNot Nothing Then
            '    dtCustomTabularGrid = Me.ViewState("CustomTabularGrid")
            'End If
            If Me.Session("CustomTabularGrid") IsNot Nothing Then
                dtCustomTabularGrid = Me.Session("CustomTabularGrid")
            End If

            If Me.ViewState("iWeightTotal") IsNot Nothing Then
                iWeightTotal = Me.ViewState("iWeightTotal")
            End If

            If Me.ViewState("Headers") IsNot Nothing Then
                dsHeaders = Me.ViewState("Headers")
            End If

            If Me.ViewState("iHeaderId") IsNot Nothing Then
                iHeaderId = Me.ViewState("iHeaderId")
            End If

            If Me.ViewState("iExOrdr") IsNot Nothing Then
                iExOrdr = Me.ViewState("iExOrdr")
            End If

            If Me.ViewState("iLinkedFieldId") IsNot Nothing Then
                iLinkedFieldId = Me.ViewState("iLinkedFieldId")
            End If

            If Me.ViewState("YearUnkid") IsNot Nothing Then
                mintYearUnkid = Me.ViewState("YearUnkid")
            End If

            If Me.ViewState("iMappingUnkid") IsNot Nothing Then
                iMappingUnkid = Me.ViewState("iMappingUnkid")
            End If

            If Me.ViewState("xTotAssignedWeight") IsNot Nothing Then
                xTotAssignedWeight = Me.ViewState("xTotAssignedWeight")
            End If

            If Me.ViewState("ColIndex") IsNot Nothing Then
                xVal = Me.ViewState("ColIndex")
            End If

            If Me.Session("dtCItems") IsNot Nothing Then
                dtCItems = Me.Session("dtCItems")
            End If

            If Me.ViewState("ItemAddEdit") IsNot Nothing Then
                mblnItemAddEdit = Me.ViewState("ItemAddEdit")
                If mblnItemAddEdit Then
                    popup_CItemAddEdit.Show()
                End If
            End If

            'S.SANDEEP [12 OCT 2016] -- START
            mblnIsMatchCompetencyStructure = Me.ViewState("mblnIsMatchCompetencyStructure")
            'S.SANDEEP [12 OCT 2016] -- END

            If objpnlCItems.Visible = True Then
                dgvItems.DataSource = dtCustomTabularGrid
                dgvItems.DataBind()
            End If
            dtCItems = Session("dtCItems")
            If pnl_CItemAddEdit.Visible Then
                If dtCItems IsNot Nothing Then
                    If dtCItems.Rows.Count > 0 Then
                        dgv_Citems.DataSource = dtCItems
                        dgv_Citems.DataBind()
                    End If
                End If
            End If

            If Me.ViewState("iEditingGUID") IsNot Nothing Then
                mstriEditingGUID = Me.ViewState("iEditingGUID")
            End If

            'S.SANDEEP [21 JAN 2015] -- START
            Me.ViewState("mdecItemWeight") = 0
            Me.ViewState("mdecMaxScale") = 0
            'S.SANDEEP [21 JAN 2015] -- END


            'SHANI [21 Mar 2015]-START
            'Issue : Fixing Issues Sent By Aandrew in PA Testing.
            'If Request.QueryString("Method") = "TxtValRemarkGE_TextChanged" Then
            '    Response.Cache.SetCacheability(HttpCacheability.NoCache)
            '    TxtValRemarkGE_TextChanged(Request.QueryString("Sender"), Request.QueryString("rowindex"))
            'End If
            'SHANI [21 Mar 2015]--END 

            'S.SANDEEP |25-MAR-2019| -- START
            SetLanguage()
            'S.SANDEEP |25-MAR-2019| -- END

            'S.SANDEEP |20-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#0004155}
            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
                objlblValue1.Visible = False
            End If
            'S.SANDEEP |20-SEP-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Me.ViewState("Action") Is Nothing Then
                Me.ViewState.Add("Action", menAction)
            Else
                Me.ViewState("Action") = menAction
            End If

            If Me.ViewState("AssessAnalysisUnkid") Is Nothing Then
                Me.ViewState.Add("AssessAnalysisUnkid", mintAssessAnalysisUnkid)
            Else
                Me.ViewState("AssessAnalysisUnkid") = mintAssessAnalysisUnkid
            End If

            'If Me.ViewState("EmplId") Is Nothing Then
            '    Me.ViewState.Add("EmplId", mintEmplId)
            'Else
            '    Me.ViewState("EmplId") = mintEmplId
            'End If

            'If Me.ViewState("PeriodId") Is Nothing Then
            '    Me.ViewState.Add("PeriodId", mintPeriodId)
            'Else
            '    Me.ViewState("PeriodId") = mintPeriodId
            'End If

            If Me.ViewState("YearUnkid") Is Nothing Then
                Me.ViewState.Add("YearUnkid", mintYearUnkid)
            Else
                Me.ViewState("YearUnkid") = mintYearUnkid
            End If

            'If Me.ViewState("AssessorId") Is Nothing Then
            '    Me.ViewState.Add("AssessorId", mintAssessorId)
            'Else
            '    Me.ViewState("AssessorId") = mintAssessorId
            'End If

            If Me.ViewState("Assess") Is Nothing Then
                Me.ViewState.Add("Assess", menAssess)
            Else
                Me.ViewState("Assess") = menAssess
            End If

            'If Me.ViewState("BSC_Evaluation") Is Nothing Then
            '    Me.ViewState.Add("BSC_Evaluation", mdtBSC_Evaluation)
            'Else
            '    Me.ViewState("BSC_Evaluation") = mdtBSC_Evaluation
            'End If
            If Me.Session("BSC_Evaluation") Is Nothing Then
                Me.Session.Add("BSC_Evaluation", mdtBSC_Evaluation)
            Else
                Me.Session("BSC_Evaluation") = mdtBSC_Evaluation
            End If

            'If Me.ViewState("GE_Evaluation") Is Nothing Then
            '    Me.ViewState.Add("GE_Evaluation", mdtGE_Evaluation)
            'Else
            '    Me.ViewState("GE_Evaluation") = mdtGE_Evaluation
            'End If
            If Me.Session("GE_Evaluation") Is Nothing Then
                Me.Session.Add("GE_Evaluation", mdtGE_Evaluation)
            Else
                Me.Session("GE_Evaluation") = mdtGE_Evaluation
            End If

            'If Me.ViewState("CustomEvaluation") Is Nothing Then
            '    Me.ViewState.Add("CustomEvaluation", mdtCustomEvaluation)
            'Else
            '    Me.ViewState("CustomEvaluation") = mdtCustomEvaluation
            'End If
            If Me.Session("CustomEvaluation") Is Nothing Then
                Me.Session.Add("CustomEvaluation", mdtCustomEvaluation)
            Else
                Me.Session("CustomEvaluation") = mdtCustomEvaluation
            End If

            'If Me.ViewState("BSC_TabularGrid") Is Nothing Then
            '    Me.ViewState.Add("BSC_TabularGrid", dtBSC_TabularGrid)
            'Else
            '    Me.ViewState("BSC_TabularGrid") = dtBSC_TabularGrid
            'End If
            If Me.Session("BSC_TabularGrid") Is Nothing Then
                Me.Session.Add("BSC_TabularGrid", dtBSC_TabularGrid)
            Else
                Me.Session("BSC_TabularGrid") = dtBSC_TabularGrid
            End If

            'If Me.ViewState("GE_TabularGrid") Is Nothing Then
            '    Me.ViewState.Add("GE_TabularGrid", dtGE_TabularGrid)
            'Else
            '    Me.ViewState("GE_TabularGrid") = dtGE_TabularGrid
            'End If
            If Me.Session("GE_TabularGrid") Is Nothing Then
                Me.Session.Add("GE_TabularGrid", dtGE_TabularGrid)
            Else
                Me.Session("GE_TabularGrid") = dtGE_TabularGrid
            End If

            'If Me.ViewState("CustomTabularGrid") Is Nothing Then
            '    Me.ViewState.Add("CustomTabularGrid", dtCustomTabularGrid)
            'Else
            '    Me.ViewState("CustomTabularGrid") = dtCustomTabularGrid
            'End If
            If Me.Session("CustomTabularGrid") Is Nothing Then
                Me.Session.Add("CustomTabularGrid", dtCustomTabularGrid)
            Else
                Me.Session("CustomTabularGrid") = dtCustomTabularGrid
            End If

            If Me.ViewState("iWeightTotal") Is Nothing Then
                Me.ViewState.Add("iWeightTotal", iWeightTotal)
            Else
                Me.ViewState("iWeightTotal") = iWeightTotal
            End If

            If Me.ViewState("Headers") Is Nothing Then
                Me.ViewState.Add("Headers", dsHeaders)
            Else
                Me.ViewState("Headers") = dsHeaders
            End If

            If Me.ViewState("iHeaderId") Is Nothing Then
                Me.ViewState.Add("iHeaderId", iHeaderId)
            Else
                Me.ViewState("iHeaderId") = iHeaderId
            End If

            If Me.ViewState("iExOrdr") Is Nothing Then
                Me.ViewState.Add("iExOrdr", iExOrdr)
            Else
                Me.ViewState("iExOrdr") = iExOrdr
            End If

            If Me.ViewState("iLinkedFieldId") Is Nothing Then
                Me.ViewState.Add("iLinkedFieldId", iLinkedFieldId)
            Else
                Me.ViewState("iLinkedFieldId") = iLinkedFieldId
            End If

            If Me.ViewState("iMappingUnkid") Is Nothing Then
                Me.ViewState.Add("iMappingUnkid", iMappingUnkid)
            Else
                Me.ViewState("iMappingUnkid") = iMappingUnkid
            End If

            If Me.ViewState("xTotAssignedWeight") Is Nothing Then
                Me.ViewState.Add("xTotAssignedWeight", xTotAssignedWeight)
            Else
                Me.ViewState("xTotAssignedWeight") = xTotAssignedWeight
            End If

            If Me.ViewState("ColIndex") Is Nothing Then
                Me.ViewState.Add("ColIndex", xVal)
            Else
                Me.ViewState("ColIndex") = xVal
            End If

            If Me.ViewState("ItemAddEdit") Is Nothing Then
                Me.ViewState.Add("ItemAddEdit", mblnItemAddEdit)
            Else
                Me.ViewState("ItemAddEdit") = mblnItemAddEdit
            End If

            If Me.Session("dtCItems") Is Nothing Then
                Me.Session.Add("dtCItems", dtCItems)
            End If

            If Me.ViewState("iEditingGUID") Is Nothing Then
                Me.ViewState.Add("iEditingGUID", mstriEditingGUID)
            Else
                Me.ViewState("iEditingGUID") = mstriEditingGUID
            End If

            'S.SANDEEP [12 OCT 2016] -- START
            Me.ViewState("mblnIsMatchCompetencyStructure") = mblnIsMatchCompetencyStructure
            'S.SANDEEP [12 OCT 2016] -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Session("Unkid") IsNot Nothing Then
                Session.Remove("Unkid")
            End If
            If Session("Action") IsNot Nothing Then
                Session.Remove("Action")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Try
            If (Session("LoginBy") = Global.User.en_loginby.User) Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
                'Else
                '    dsCombos = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                'End If

                'Shani(14-APR-2016) -- Start
                'dsCombos = objEmp.GetEmployeeList(Session("Database_Name"), _
                '                           Session("UserId"), _
                '                           Session("Fin_year"), _
                '                           Session("CompanyUnkId"), _
                '                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                           Session("UserAccessModeSetting"), True, _
                '                           Session("IsIncludeInactiveEmp"), "Emp", True)

                Dim strFilterQry As String = String.Empty

                Dim csvIds As String = String.Empty
                Dim dsMapEmp As New DataSet
                Dim objEval As New clsevaluation_analysis_master
                dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
                                                        Session("UserId"), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        True, Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)
                'Shani (12-Jan-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]
                strFilterQry = " hremployee_master.employeeunkid NOT IN "
                If dsMapEmp.Tables("List").Rows.Count > 0 Then
                    csvIds = String.Join(",", dsMapEmp.Tables("List").AsEnumerable().Select(Function(x) x.Field(Of Integer)("EmpId").ToString()).ToArray())
                End If
                If csvIds.Trim.Length > 0 Then
                    strFilterQry &= "(" & csvIds & ")"
                Else
                    strFilterQry = ""
                End If

                If dsMapEmp.Tables("List").Rows.Count > 0 Then
                    dsCombos = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                              Session("UserId"), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                              Session("IsIncludeInactiveEmp"), _
                                                              CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)

                    If strFilterQry.Trim.Length > 0 Then
                        strFilterQry &= " AND hremployee_master.employeeunkid IN "
                    Else
                        strFilterQry = " hremployee_master.employeeunkid IN "
                    End If
                    csvIds = String.Join(",", dsCombos.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
                    If csvIds.Trim.Length > 0 Then
                        strFilterQry &= "(" & csvIds & ")"
                    Else
                        strFilterQry &= "(0)"
                    End If
                Else
                    strFilterQry = " hremployee_master.employeeunkid IN (0) "
                End If
                objEval = Nothing

                dsCombos = objEmp.GetEmployeeList(Session("Database_Name"), _
                                           Session("UserId"), _
                                           Session("Fin_year"), _
                                           Session("CompanyUnkId"), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           Session("UserAccessModeSetting"), True, _
                                           Session("IsIncludeInactiveEmp"), "Emp", True, , , , , , , , , , , , , , , strFilterQry)

                'Shani(14-APR-2016) -- End

                'Shani(24-Aug-2015) -- End
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombos.Tables(0)
                    .DataBind()
                    .SelectedValue = 0
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee.Copy
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                End With
            End If


            'S.SANDEEP [17 NOV 2015] -- START
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "APeriod", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "APeriod", True, 1, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "APeriod", True, 1)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "APeriod", True, 1)
            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [17 NOV 2015] -- END

            'Shani (09-May-2016) -- Start
            Dim intCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End


            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("APeriod")
                .DataBind()
                .SelectedValue = intCurrentPeriodId
            End With
            'Shani (09-May-2016) -- Chagnes[0 -- > intCurrentPeriodId]
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objEAnalysisMst._Analysisunkid = mintAssessAnalysisUnkid
            objEAnalysisMst._Periodunkid = CInt(cboPeriod.SelectedValue)
            objEAnalysisMst._Selfemployeeunkid = CInt(cboEmployee.SelectedValue)
            objEAnalysisMst._Assessedemployeeunkid = -1
            objEAnalysisMst._Assessormasterunkid = -1
            objEAnalysisMst._Assessoremployeeunkid = -1
            objEAnalysisMst._Reviewerunkid = -1
            objEAnalysisMst._Assessmodeid = enAssessmentMode.SELF_ASSESSMENT
            objEAnalysisMst._Assessmentdate = dtpAssessdate.GetDate
            objEAnalysisMst._Userunkid = Session("UserId")
            If objEAnalysisMst._Committeddatetime <> Nothing Then
                objEAnalysisMst._Committeddatetime = objEAnalysisMst._Committeddatetime
            Else
                objEAnalysisMst._Committeddatetime = Nothing
            End If
            objEAnalysisMst._Isvoid = False
            objEAnalysisMst._Voiduserunkid = -1
            objEAnalysisMst._Voiddatetime = Nothing
            objEAnalysisMst._Voidreason = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            If objEAnalysisMst._Selfemployeeunkid > 0 Then
                cboEmployee.SelectedValue = objEAnalysisMst._Selfemployeeunkid
            End If
            'End If
            'S.SANDEEP [04 JUN 2015] -- END
            cboPeriod.SelectedValue = objEAnalysisMst._Periodunkid
            If menAction = enAction.EDIT_ONE Then
                Call cboPeriod_SelectedIndexChanged(New Object, New EventArgs)
            End If
            If objEAnalysisMst._Assessmentdate <> Nothing Then
                Dim dtdate As Date = objEAnalysisMst._Assessmentdate.ToShortDateString()
                dtpAssessdate.SetDate = CDate(dtdate).Date
                'Shani(11-NOV-2015) -- Start
                'ENHANCEMENT : 
            Else
                dtpAssessdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                'Shani(11-NOV-2015) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    'Private Sub SetTotals(ByVal iDG As DataGrid)
    '    Try
    '        Select Case iDG.ID.ToUpper
    '            Case dgvBSC.ID.ToUpper
    '                objlblValue1.Text = Language.getMessage(mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
    '                objlblValue1.Visible = True : objlblValue2.Visible = True
    '                'S.SANDEEP [21 JAN 2015] -- START
    '                'If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
    '                '    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
    '                '        objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D'")).ToString
    '                '    End If
    '                '    objlblValue3.Visible = False : objlblValue4.Visible = False

    '                'ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
    '                '    objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(escore)", "")).ToString
    '                '    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
    '                '        objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D'")).ToString
    '                '    End If
    '                '    objlblValue3.Visible = True : objlblValue4.Visible = False
    '                'ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
    '                '    objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(escore)", "")).ToString
    '                '    objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(ascore)", "")).ToString

    '                '    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
    '                '        objlblValue4.Text = Language.getMessage(mstrModuleName, 8, "Reviewer Score :") & " " & CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D'")).ToString
    '                '    End If
    '                '    objlblValue3.Visible = True : objlblValue4.Visible = True
    '                'End If
    '                If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
    '                    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
    '                        'Shani(20-Nov-2015) -- Start
    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                        'objEAnalysisMst.Compute_Score(menAssess, _
    '                        '                              True, _
    '                        '                              Session("ScoringOptionId"), _
    '                        '                              enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
    '                        '                              cboEmployee.SelectedValue, _
    '                        '                              cboPeriod.SelectedValue, , mdtBSC_Evaluation)
    '                        objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
    '                        objEAnalysisMst.Compute_Score(menAssess, _
    '                                                      True, _
    '                                                      Session("ScoringOptionId"), _
    '                                                      enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                      cboEmployee.SelectedValue, _
    '                                                      cboPeriod.SelectedValue, , mdtBSC_Evaluation, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
    '                        'Shani(20-Nov-2015) -- End
    '                    End If
    '                    objlblValue3.Visible = False : objlblValue4.Visible = False
    '                ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
    '                    'Shani(20-Nov-2015) -- Start
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                    'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
    '                    '                              True, _
    '                    '                              Session("ScoringOptionId"), _
    '                    '                              enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
    '                    '                              cboEmployee.SelectedValue, _
    '                    '                              cboPeriod.SelectedValue, , dtBSC_TabularGrid)
    '                    If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
    '                    objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
    '                                                  True, _
    '                                                  Session("ScoringOptionId"), _
    '                                                  enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                  cboEmployee.SelectedValue, _
    '                                                  cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
    '                    'Shani(20-Nov-2015) -- End

    '                    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
    '                        'Shani(20-Nov-2015) -- Start
    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                        'objEAnalysisMst.Compute_Score(menAssess, _
    '                        '                              True, _
    '                        '                              Session("ScoringOptionId"), _
    '                        '                              enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
    '                        '                              cboEmployee.SelectedValue, _
    '                        '                              cboPeriod.SelectedValue, , mdtBSC_Evaluation)
    '                        objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & _
    '                        objEAnalysisMst.Compute_Score(menAssess, _
    '                                                      True, _
    '                                                      Session("ScoringOptionId"), _
    '                                                      enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                      cboEmployee.SelectedValue, _
    '                                                      cboPeriod.SelectedValue, , mdtBSC_Evaluation, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

    '                        'Shani(20-Nov-2015) -- End
    '                    End If
    '                    objlblValue3.Visible = True : objlblValue4.Visible = False
    '                ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
    '                    'Shani(20-Nov-2015) -- Start
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    '                    'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
    '                    '                                                      True, _
    '                    '                                                      Session("ScoringOptionId"), _
    '                    '                                                      enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
    '                    '                                                      cboEmployee.SelectedValue, _
    '                    '                                                      cboPeriod.SelectedValue, , dtBSC_TabularGrid)
    '                    If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
    '                    objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
    '                                                  True, _
    '                                                  Session("ScoringOptionId"), _
    '                                                  enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                  cboEmployee.SelectedValue, _
    '                                                  cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
    '                    'Shani(20-Nov-2015) -- End

    '                    'Shani(20-Nov-2015) -- Start
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                    'objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
    '                    '                                                      True, _
    '                    '                                                      Session("ScoringOptionId"), _
    '                    '                                                      enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
    '                    '                                                      cboEmployee.SelectedValue, _
    '                    '                                                      cboPeriod.SelectedValue, , dtBSC_TabularGrid)
    '                    If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & _
    '                    objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
    '                                                  True, _
    '                                                  Session("ScoringOptionId"), _
    '                                                  enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                  cboEmployee.SelectedValue, _
    '                                                  cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

    '                    'Shani(20-Nov-2015) -- End

    '                    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
    '                        'Shani(20-Nov-2015) -- Start
    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                        'objEAnalysisMst.Compute_Score(menAssess, _
    '                        '                              True, _
    '                        '                              Session("ScoringOptionId"), _
    '                        '                              enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
    '                        '                              cboEmployee.SelectedValue, _
    '                        '                              cboPeriod.SelectedValue, , mdtBSC_Evaluation)
    '                        objlblValue4.Text = Language.getMessage(mstrModuleName, 8, "Reviewer Score :") & " " & _
    '                        objEAnalysisMst.Compute_Score(menAssess, _
    '                                                      True, _
    '                                                      Session("ScoringOptionId"), _
    '                                                      enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                      cboEmployee.SelectedValue, _
    '                                                      cboPeriod.SelectedValue, , mdtBSC_Evaluation, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
    '                        'Shani(20-Nov-2015) -- End
    '                    End If
    '                    objlblValue3.Visible = True : objlblValue4.Visible = True
    '                End If
    '                'S.SANDEEP [21 JAN 2015] -- END

    '            Case dgvGE.ID.ToUpper

    '                objlblValue1.Text = Language.getMessage(mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
    '                objlblValue1.Visible = True : objlblValue2.Visible = True
    '                'S.SANDEEP [21 JAN 2015] -- START
    '                'If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
    '                '    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
    '                '        objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & CDbl(mdtGE_Evaluation.Compute("SUM(dresult)", "AUD <> 'D'")).ToString
    '                '    End If
    '                '    objlblValue3.Visible = False : objlblValue4.Visible = False
    '                'ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
    '                '    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(escore)", "")).ToString
    '                '    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
    '                '        objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & CDbl(mdtGE_Evaluation.Compute("SUM(dresult)", "AUD <> 'D'")).ToString
    '                '    End If
    '                '    objlblValue3.Visible = True : objlblValue4.Visible = False
    '                'ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
    '                '    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(escore)", "")).ToString
    '                '    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(ascore)", "")).ToString

    '                '    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
    '                '        objlblValue4.Text = Language.getMessage(mstrModuleName, 8, "Reviewer Score :") & " " & CDbl(mdtGE_Evaluation.Compute("SUM(dresult)", "AUD <> 'D'")).ToString
    '                '    End If
    '                '    objlblValue3.Visible = True : objlblValue4.Visible = True
    '                'End If
    '                If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
    '                    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
    '                        'Shani(20-Nov-2015) -- Start
    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                        'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
    '                        '                              False, _
    '                        '                              Session("ScoringOptionId"), _
    '                        '                              enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
    '                        '                              cboEmployee.SelectedValue, _
    '                        '                              cboPeriod.SelectedValue, , mdtGE_Evaluation)
    '                        objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
    '                        objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
    '                                                      False, _
    '                                                      Session("ScoringOptionId"), _
    '                                                      enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                      cboEmployee.SelectedValue, _
    '                                                      cboPeriod.SelectedValue, , mdtGE_Evaluation, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
    '                        'Shani(20-Nov-2015) -- End
    '                    End If
    '                    objlblValue3.Visible = False : objlblValue4.Visible = False
    '                ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
    '                    'Shani(20-Nov-2015) -- Start
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                    'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
    '                    '                                                      False, _
    '                    '                                                      Session("ScoringOptionId"), _
    '                    '                                                      enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
    '                    '                                                      cboEmployee.SelectedValue, _
    '                    '                                                      cboPeriod.SelectedValue, , dtGE_TabularGrid)
    '                    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
    '                    objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
    '                                                  False, _
    '                                                  Session("ScoringOptionId"), _
    '                                                  enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                  cboEmployee.SelectedValue, _
    '                                                  cboPeriod.SelectedValue, , dtGE_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
    '                    'Shani(20-Nov-2015) -- End

    '                    objlblValue3.Text = ""
    '                    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
    '                        'Shani(20-Nov-2015) -- Start
    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                        'objEAnalysisMst.Compute_Score(menAssess, _
    '                        '                              False, _
    '                        '                              Session("ScoringOptionId"), _
    '                        '                              enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
    '                        '                              cboEmployee.SelectedValue, _
    '                        '                              cboPeriod.SelectedValue, , mdtGE_Evaluation)
    '                        objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & _
    '                        objEAnalysisMst.Compute_Score(menAssess, _
    '                                                      False, _
    '                                                      Session("ScoringOptionId"), _
    '                                                      enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                      cboEmployee.SelectedValue, _
    '                                                      cboPeriod.SelectedValue, , mdtGE_Evaluation, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
    '                        'Shani(20-Nov-2015) -- End
    '                    End If
    '                    objlblValue3.Visible = True : objlblValue4.Visible = False
    '                ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
    '                    'Shani(20-Nov-2015) -- Start
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                    'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
    '                    '                                                      False, _
    '                    '                                                      Session("ScoringOptionId"), _
    '                    '                                                      enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
    '                    '                                                      cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtGE_TabularGrid)
    '                    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
    '                    objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
    '                                                  False, _
    '                                                  Session("ScoringOptionId"), _
    '                                                  enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                  cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtGE_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

    '                    'Shani(20-Nov-2015) -- End

    '                    'Shani(20-Nov-2015) -- Start
    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                    'objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
    '                    '                                                      False, _
    '                    '                                                      Session("ScoringOptionId"), _
    '                    '                                                      enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
    '                    '                                                      cboEmployee.SelectedValue, cboPeriod.SelectedValue, , dtGE_TabularGrid)
    '                    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Language.getMessage(mstrModuleName, 7, "Assessor Score :") & " " & _
    '                    objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
    '                                                  False, _
    '                                                  Session("ScoringOptionId"), _
    '                                                  enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                  cboEmployee.SelectedValue, cboPeriod.SelectedValue, , _
    '                                                  dtGE_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

    '                    'Shani(20-Nov-2015) -- End
    '                    objlblValue4.Text = ""
    '                    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
    '                        'Shani(20-Nov-2015) -- Start
    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                        'objEAnalysisMst.Compute_Score(menAssess, _
    '                        '                              False, _
    '                        '                              Session("ScoringOptionId"), _
    '                        '                              enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
    '                        '                              cboEmployee.SelectedValue, _
    '                        '                              cboPeriod.SelectedValue, , mdtGE_Evaluation)
    '                        objlblValue4.Text = Language.getMessage(mstrModuleName, 8, "Reviewer Score :") & " " & _
    '                        objEAnalysisMst.Compute_Score(menAssess, _
    '                                                      False, _
    '                                                      Session("ScoringOptionId"), _
    '                                                      enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
    '                                                      cboEmployee.SelectedValue, _
    '                                                      cboPeriod.SelectedValue, , mdtGE_Evaluation, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
    '                        'Shani(20-Nov-2015) -- End
    '                    End If
    '                    objlblValue3.Visible = True : objlblValue4.Visible = True
    '                End If
    '                'S.SANDEEP [21 JAN 2015] -- END
    '        End Select
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    Finally
    '    End Try
    'End Sub
    Private Sub SetTotals(ByVal iDG As DataGrid)
        Try
            Select Case iDG.ID.ToUpper
                Case dgvBSC.ID.ToUpper
                    objlblValue1.Text = Language.getMessage(mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
                    objlblValue1.Visible = True : objlblValue2.Visible = True
                    If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                        If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            'objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
                            'objEAnalysisMst.Compute_Score(menAssess, _
                            '                              True, _
                            '                              Session("ScoringOptionId"), _
                            '                              enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                            '                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                            '                              cboEmployee.SelectedValue, _
                            '                              cboPeriod.SelectedValue, , mdtBSC_Evaluation, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
                            objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
                            objEAnalysisMst.Compute_Score(xAssessMode:=menAssess, _
                                                          IsBalanceScoreCard:=True, _
                                                          xScoreOptId:=Session("ScoringOptionId"), _
                                                          xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                                                          xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                          xEmployeeId:=cboEmployee.SelectedValue, _
                                                          xPeriodId:=cboPeriod.SelectedValue, _
                                                          xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                                                          xDataTable:=mdtBSC_Evaluation, _
                                                          xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
                            'Shani (23-Nov123-2016-2016) -- End
                        End If
                        objlblValue3.Visible = False : objlblValue4.Visible = False
                    End If
                Case dgvGE.ID.ToUpper

                    objlblValue1.Text = Language.getMessage(mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
                    objlblValue1.Visible = True : objlblValue2.Visible = True
                    If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                        If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                            'Shani (23-Nov-2016) -- Start
                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                            'objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
                            'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                            '                              False, _
                            '                              Session("ScoringOptionId"), _
                            '                              enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                            '                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                            '                              cboEmployee.SelectedValue, _
                            '                              cboPeriod.SelectedValue, , mdtGE_Evaluation, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
                            objlblValue2.Text = Language.getMessage(mstrModuleName, 6, "Self Score :") & " " & _
                            objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                                                          IsBalanceScoreCard:=False, _
                                                          xScoreOptId:=Session("ScoringOptionId"), _
                                                          xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                                                          xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                          xEmployeeId:=cboEmployee.SelectedValue, _
                                                          xPeriodId:=cboPeriod.SelectedValue, _
                                                          xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                                                          xDataTable:=mdtGE_Evaluation, _
                                                          xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
                            'Shani (23-Nov123-2016-2016) -- End
                        End If
                        objlblValue3.Visible = False : objlblValue4.Visible = False
                    End If
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Shani (26-Sep-2016) -- End 

    Private Sub PanelVisibility()
        Try
            Dim xVal As Integer = -1
            If iHeaderId >= 0 Then
                objlblCaption.Text = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString
                xVal = dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")
            Else
                objlblCaption.Text = Language.getMessage(mstrModuleName, 1, "Instructions")
                xVal = -1
            End If
            Select Case xVal
                Case -1 'Instruction
                    objpnlBSC.Visible = False
                    objpnlGE.Visible = False
                    objpnlCItems.Visible = False

                    objpnlInstruction.Visible = True
                    objpnlInstruction.Height = Unit.Pixel(500)
                    objpnlInstruction.Width = Unit.Percentage(100)

                    iWeightTotal = 0
                Case -3  'Balance Score Card
                    objpnlInstruction.Visible = False
                    objpnlGE.Visible = False
                    objpnlCItems.Visible = False

                    iWeightTotal = 0
                    'If mdtBSC_Evaluation.Rows.Count <= 0 Or menAction = enAction.EDIT_ONE Then
                    Call Fill_BSC_Evaluation()
                    'End If

                    objpnlBSC.Visible = True
                    'objpnlBSC.Height = Unit.Percentage(100)
                    'objpnlBSC.Width = Unit.Percentage(100)
                Case -2  'Competencies
                    objpnlInstruction.Visible = False
                    objpnlBSC.Visible = False
                    objpnlCItems.Visible = False

                    iWeightTotal = 0
                    'If mdtGE_Evaluation.Rows.Count <= 0 Or menAction = enAction.EDIT_ONE Then
                    Call Fill_GE_Evaluation()
                    'End If
                    objpnlGE.Visible = True
                    'objpnlGE.Height = Unit.Percentage(100)
                    'objpnlGE.Width = Unit.Percentage(100)

                Case Else  'Dynamic Custom Headers
                    iWeightTotal = 0
                    objpnlInstruction.Visible = False
                    objpnlBSC.Visible = False
                    objpnlGE.Visible = False
                    Call Fill_Custom_Grid()
                    Call Fill_Custom_Evaluation_Data()
                    objpnlCItems.Visible = True
                    'objpnlCItems.Height = Unit.Percentage(100)
                    'objpnlCItems.Width = Unit.Percentage(100)
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Emplolyee is compulsory information.Please Select Emplolyee."), Me)
                cboEmployee.Focus()
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Return False
            End If

            'SHANI [09 APR 2015] -- START
            'Dim dsYr As New DataSet : Dim oCompany As New clsCompany_Master
            'dsYr = oCompany.GetFinancialYearList(Session("CompanyUnkId"), Session("UserId"), "List", Session("Fin_year"))
            'If dsYr.Tables("List").Rows.Count > 0 Then
            '    If dtpAssessdate.GetDate.Date > eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date Or _
            '       dtpAssessdate.GetDate.Date < eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date Then
            '        Dim strMsg As String = Language.getMessage(mstrModuleName, 19, "Assessment date should be in between ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date & _
            '                               Language.getMessage(mstrModuleName, 20, " And ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date
            '        DisplayMessage.DisplayMessage(strMsg, Me)
            '        dtpAssessdate.Focus()
            '        Return False
            '    End If
            'Else
            '    If dtpAssessdate.GetDate.Date > Session("fin_enddate") Or _
            '       dtpAssessdate.GetDate.Date < Session("fin_startdate") Then
            '        Dim strMsg As String = Language.getMessage(mstrModuleName, 19, "Assessment date should be in between ") & CDate(Session("fin_startdate")) & _
            '                               Language.getMessage(mstrModuleName, 20, " And ") & CDate(Session("fin_enddate"))
            '        DisplayMessage.DisplayMessage(strMsg, Me)
            '        dtpAssessdate.Focus()
            '        Return False
            '    End If
            'End If
            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            If dtpAssessdate.GetDate <= objPrd._Start_Date.Date Then
                Dim strMsg As String = Language.getMessage(mstrModuleName, 36, "Sorry, assessment date should be greater than") & " ( " & objPrd._Start_Date.Date.ToShortDateString & " )."
                DisplayMessage.DisplayMessage(strMsg, Me)
                dtpAssessdate.Focus()
                objPrd = Nothing
                Return False
            End If
            objPrd = Nothing
            'SHANI [09 APR 2015] -- END

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub ClearForm_Values()
        Try
            Me.ViewState("Action") = Nothing
            Me.ViewState("AssessAnalysisUnkid") = Nothing
            Me.ViewState("YearUnkid") = Nothing
            Me.ViewState("Assess") = Nothing
            Me.Session.Remove("BSC_Evaluation")
            Me.Session.Remove("GE_Evaluation")
            Me.Session.Remove("CustomEvaluation")
            Me.Session.Remove("BSC_TabularGrid")
            Me.Session.Remove("GE_TabularGrid")
            Me.Session.Remove("CustomTabularGrid")
            Me.ViewState("iWeightTotal") = Nothing
            Me.ViewState("Headers") = Nothing
            Me.ViewState("iHeaderId") = Nothing
            Me.ViewState("iExOrdr") = Nothing
            Me.ViewState("iLinkedFieldId") = Nothing
            Me.ViewState("iMappingUnkid") = Nothing
            Me.ViewState("xTotAssignedWeight") = Nothing
            Me.ViewState("ColIndex") = Nothing
            Me.ViewState("ItemAddEdit") = Nothing
            Me.Session.Remove("dtCItems")
            Me.ViewState("iEditingGUID") = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Is_Already_Assessed() As Boolean
        Try
            'S.SANDEEP [13-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : STOP DUPLICATE ASSESSMENT
            'If objEAnalysisMst.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , Me.ViewState("AssessAnalysisUnkid")) = True Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), Me, "wPg_SelfEvaluationList.aspx") 'Shani [21 MAR 2015]--wPg_SelfEvaluationList.aspx
            '    Return False
            'End If

            'S.SANDEEP |30-JAN-2019| -- START
            'ISSUE/ENHANCEMENT : {#0003446|ARUTI-545}
            'If objEAnalysisMst.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , Me.ViewState("AssessAnalysisUnkid")), , , False) = True Then
            If objEAnalysisMst.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , mintAssessAnalysisUnkid, , , False) = True Then
                'S.SANDEEP |30-JAN-2019| -- END
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), Me, "wPg_SelfEvaluationList.aspx") 'Shani [21 MAR 2015]--wPg_SelfEvaluationList.aspx
                Return False
            End If
            'S.SANDEEP [13-NOV-2018] -- END

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    ''''''''''''''''''''''''''BSC EVOLUTION
    Private Sub SetBSC_GridCols_Tags()
        Try
            'S.SANDEEP |05-APR-2019| -- START
            Dim iEval() As String = Nothing
            If CStr(Session("ViewTitles_InEvaluation")).Trim.Length > 0 Then
                iEval = CStr(Session("ViewTitles_InEvaluation")).Split("|")
            End If
            'Dim objFMst As New clsAssess_Field_Master
            'Dim dFld As New DataSet : dFld.Tables.Add(objFMst.GetFieldsForViewSetting())
            'If dFld.Tables(0).Rows.Count > 0 Then
            '    Dim xCol As DataGridColumn = Nothing
            '    Dim iExOrder As Integer = -1
            '    For Each xRow As DataRow In dFld.Tables(0).Rows
            '        iExOrder = -1
            '        iExOrder = objFMst.Get_Field_ExOrder(xRow.Item("Id"))
            '        xCol = Nothing
            '        Select Case iExOrder
            '            Case 1
            '                dgvBSC.Columns(0).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(0)
            '            Case 2
            '                dgvBSC.Columns(1).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(1)
            '            Case 3
            '                dgvBSC.Columns(2).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(2)
            '            Case 4
            '                dgvBSC.Columns(3).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(3)
            '            Case 5
            '                dgvBSC.Columns(4).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(4)
            '            Case 6
            '                dgvBSC.Columns(5).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(5)
            '            Case 7
            '                dgvBSC.Columns(6).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(6)
            '            Case 8
            '                dgvBSC.Columns(7).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(7)
            '        End Select
            '        Select Case xRow.Item("Id")
            '            Case clsAssess_Field_Master.enOtherInfoField.ST_DATE
            '                dgvBSC.Columns(8).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(8)
            '            Case clsAssess_Field_Master.enOtherInfoField.ED_DATE
            '                dgvBSC.Columns(9).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(9)
            '                'S.SANDEEP [11-OCT-2018] -- START
            '            Case clsAssess_Field_Master.enOtherInfoField.GOAL_TYPE
            '                dgvBSC.Columns(10).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(10)
            '            Case clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE
            '                dgvBSC.Columns(11).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(11)

            '            Case clsAssess_Field_Master.enOtherInfoField.PCT_COMPLETE
            '                dgvBSC.Columns(12).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(12)
            '            Case clsAssess_Field_Master.enOtherInfoField.STATUS
            '                dgvBSC.Columns(13).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(13)
            '                'S.SANDEEP [04 MAR 2015] -- START
            '            Case clsAssess_Field_Master.enOtherInfoField.WEIGHT
            '                If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
            '                    xCol = dgvBSC.Columns(14)
            '                Else
            '                    dgvBSC.Columns(15).FooterText = xRow.Item("Id")
            '                    xCol = dgvBSC.Columns(15)
            '                End If
            '            Case clsAssess_Field_Master.enOtherInfoField.SCORE
            '                dgvBSC.Columns(16).FooterText = xRow.Item("Id")
            '                xCol = dgvBSC.Columns(16)
            '                'S.SANDEEP [04 MAR 2015] -- END
            '                'S.SANDEEP [11-OCT-2018] -- END
            '        End Select

            For Each oFooter As String In disBSCColumn.Keys
                dgvBSC.Columns(disBSCColumn(oFooter)).Visible = False
            Next


            Dim objFMst As New clsAssess_Field_Master
            Dim dFld As New DataSet : dFld.Tables.Add(objFMst.GetFieldsForViewSetting())
            If dFld.Tables(0).Rows.Count > 0 Then
                Dim xCol As DataGridColumn = Nothing
                Dim iExOrder As Integer = -1
                For Each xRow As DataRow In dFld.Tables(0).Rows
                    iExOrder = -1
                    iExOrder = objFMst.Get_Field_ExOrder(xRow.Item("Id"))
                    xCol = Nothing
                    Select Case iExOrder
                        Case 1
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField1"))
                        Case 2
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField2"))
                        Case 3
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField3"))
                        Case 4
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField4"))
                        Case 5
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField5"))
                        Case 6
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField6"))
                        Case 7
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField7"))
                        Case 8
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField8"))
                    End Select
                    Select Case xRow.Item("Id")
                        Case clsAssess_Field_Master.enOtherInfoField.ST_DATE
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolhSDate"))
                        Case clsAssess_Field_Master.enOtherInfoField.ED_DATE
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolhEDate"))
                        Case clsAssess_Field_Master.enOtherInfoField.GOAL_TYPE
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolhGoalType"))
                        Case clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE
                            xCol = dgvBSC.Columns(disBSCColumn("dgoalvalue"))

                        Case clsAssess_Field_Master.enOtherInfoField.PCT_COMPLETE
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolhCompleted"))
                        Case clsAssess_Field_Master.enOtherInfoField.STATUS
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolhStatus"))
                        Case clsAssess_Field_Master.enOtherInfoField.WEIGHT
                            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
                                xCol = dgvBSC.Columns(disBSCColumn("dgcolhBSCScore"))
                            Else
                                xCol = dgvBSC.Columns(disBSCColumn("dgcolhBSCWeight"))
                            End If
                        Case clsAssess_Field_Master.enOtherInfoField.SCORE
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolheselfBSC"))
                        Case clsAssess_Field_Master.enOtherInfoField.EMP_REMARK
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolheremarkBSC"))
                    End Select
                    'S.SANDEEP |05-APR-2019| -- END


                    If xCol IsNot Nothing Then
                        'S.SANDEEP |14-MAR-2019| -- START
                        'xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
                        'xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
                        xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                        xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                        'S.SANDEEP |14-MAR-2019| -- END
                    End If

                    'S.SANDEEP |05-APR-2019| -- START
                    If xCol IsNot Nothing Then
                        If Array.IndexOf(iEval, xRow.Item("Id").ToString()) >= 0 Then
                            xCol.Visible = True
                        End If
                    End If
                    'S.SANDEEP |05-APR-2019| -- END

                Next
            End If
            objFMst = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_BSC_Evaluation()
        Try

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, 0, -1, )

            'S.SANDEEP [04-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : MAKE COMMON METHOD TO GET EMPLOYEE ASSESSOR/REVIEWER
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, 0, -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")))

            'S.SANDEEP |18-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, 0, -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")), Session("Database_Name").ToString())
            'S.SANDEEP |17-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES            
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, 0, -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")), Session("Database_Name").ToString(), , Session("fmtCurrency"))
            dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, 0, -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")), Session("Database_Name").ToString(), Session("DontAllowRatingBeyond100"), , Session("fmtCurrency"))
            'S.SANDEEP |17-MAY-2021| -- END

            'S.SANDEEP |18-FEB-2019| -- END

            'S.SANDEEP [04-AUG-2017] -- END
            'Shani (26-Sep-2016) -- End


            Call SetBSC_GridCols_Tags()

            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
                dgvBSC.Columns(15).Visible = False
            Else
                dgvBSC.Columns(14).Visible = False
                dgvBSC.Columns(15).Visible = True
            End If

            'If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
            '    CType(dgvBSC.FindControl("dgcolheselfBSC"), TextBox).ReadOnly = True
            '    CType(dgvBSC.FindControl("dgcolheremarkBSC"), TextBox).ReadOnly = True
            'Else
            '    dgvBSC.Columns(16).Visible = False
            '    dgvBSC.Columns(17).Visible = False
            'End If

            'If menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
            '    CType(dgvBSC.FindControl("dgcolheselfBSC"), TextBox).ReadOnly = True
            '    CType(dgvBSC.FindControl("dgcolheremarkBSC"), TextBox).ReadOnly = True
            '    CType(dgvBSC.FindControl("dgcolhaselfBSC"), TextBox).ReadOnly = True
            '    CType(dgvBSC.FindControl("dgcolharemarkBSC"), TextBox).ReadOnly = True
            '    dgvBSC.Columns(16).Visible = True
            '    dgvBSC.Columns(17).Visible = True
            'Else
            '    dgvBSC.Columns(18).Visible = False
            '    dgvBSC.Columns(19).Visible = False
            'End If

            'S.SANDEEP |05-APR-2019| -- START
            'If dtBSC_TabularGrid.Columns.Contains("Field1") Then
            '    dgvBSC.Columns(0).HeaderText = dtBSC_TabularGrid.Columns("Field1").Caption
            '    dgvBSC.Columns(0).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field1", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(0).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field2") Then
            '    dgvBSC.Columns(1).HeaderText = dtBSC_TabularGrid.Columns("Field2").Caption
            '    dgvBSC.Columns(1).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field2", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(1).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field3") Then
            '    dgvBSC.Columns(2).HeaderText = dtBSC_TabularGrid.Columns("Field3").Caption
            '    dgvBSC.Columns(2).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field3", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(2).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field4") Then
            '    dgvBSC.Columns(3).HeaderText = dtBSC_TabularGrid.Columns("Field4").Caption
            '    dgvBSC.Columns(3).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field4", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(3).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field5") Then
            '    dgvBSC.Columns(4).HeaderText = dtBSC_TabularGrid.Columns("Field5").Caption
            '    dgvBSC.Columns(4).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field5", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(4).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field6") Then
            '    dgvBSC.Columns(5).HeaderText = dtBSC_TabularGrid.Columns("Field6").Caption
            '    dgvBSC.Columns(5).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field6", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(5).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field7") Then
            '    dgvBSC.Columns(6).HeaderText = dtBSC_TabularGrid.Columns("Field7").Caption
            '    dgvBSC.Columns(6).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field7", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(6).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field8") Then
            '    dgvBSC.Columns(7).HeaderText = dtBSC_TabularGrid.Columns("Field8").Caption
            '    dgvBSC.Columns(7).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field8", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(7).Visible = False
            'End If

            'If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField1")).Visible Then
            If dtBSC_TabularGrid.Columns.Contains("Field1") Then
                dgvBSC.Columns(0).HeaderText = dtBSC_TabularGrid.Columns("Field1").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField1")).Visible Then dgvBSC.Columns(0).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field1", System.Type.GetType("System.String"))
                dgvBSC.Columns(0).Visible = False
            End If
            'Else
            'dgvBSC.Columns(0).Visible = False
            'End If

            'If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField2")).Visible Then
            If dtBSC_TabularGrid.Columns.Contains("Field2") Then
                dgvBSC.Columns(1).HeaderText = dtBSC_TabularGrid.Columns("Field2").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField2")).Visible Then dgvBSC.Columns(1).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field2", System.Type.GetType("System.String"))
                dgvBSC.Columns(1).Visible = False
            End If
            'Else
            'dgvBSC.Columns(1).Visible = False
            'End If

            'If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField3")).Visible Then
            If dtBSC_TabularGrid.Columns.Contains("Field3") Then
                dgvBSC.Columns(2).HeaderText = dtBSC_TabularGrid.Columns("Field3").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField3")).Visible Then dgvBSC.Columns(2).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field3", System.Type.GetType("System.String"))
                dgvBSC.Columns(2).Visible = False
            End If
            'Else
            'dgvBSC.Columns(2).Visible = False
            'End If

            'If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField4")).Visible Then
            If dtBSC_TabularGrid.Columns.Contains("Field4") Then
                dgvBSC.Columns(3).HeaderText = dtBSC_TabularGrid.Columns("Field4").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField4")).Visible Then dgvBSC.Columns(3).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field4", System.Type.GetType("System.String"))
                dgvBSC.Columns(3).Visible = False
            End If
            'Else
            '    dgvBSC.Columns(3).Visible = False
            'End If

            'If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField5")).Visible Then
            If dtBSC_TabularGrid.Columns.Contains("Field5") Then
                dgvBSC.Columns(4).HeaderText = dtBSC_TabularGrid.Columns("Field5").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField5")).Visible Then dgvBSC.Columns(4).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field5", System.Type.GetType("System.String"))
                dgvBSC.Columns(4).Visible = False
            End If
            'Else
            'dgvBSC.Columns(4).Visible = False
            'End If

            'If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField6")).Visible Then
            If dtBSC_TabularGrid.Columns.Contains("Field6") Then
                dgvBSC.Columns(5).HeaderText = dtBSC_TabularGrid.Columns("Field6").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField6")).Visible Then dgvBSC.Columns(5).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field6", System.Type.GetType("System.String"))
                dgvBSC.Columns(5).Visible = False
            End If
            'Else
            'dgvBSC.Columns(5).Visible = False
            'End If

            'If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField7")).Visible Then
            If dtBSC_TabularGrid.Columns.Contains("Field7") Then
                dgvBSC.Columns(6).HeaderText = dtBSC_TabularGrid.Columns("Field7").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField7")).Visible Then dgvBSC.Columns(6).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field7", System.Type.GetType("System.String"))
                dgvBSC.Columns(6).Visible = False
            End If
            'Else
            'dgvBSC.Columns(6).Visible = False
            'End If

            'If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField8")).Visible Then
            If dtBSC_TabularGrid.Columns.Contains("Field8") Then
                dgvBSC.Columns(7).HeaderText = dtBSC_TabularGrid.Columns("Field8").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField8")).Visible Then dgvBSC.Columns(7).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field8", System.Type.GetType("System.String"))
                dgvBSC.Columns(7).Visible = False
            End If
            'Else
            'dgvBSC.Columns(7).Visible = False
            'End If
            'S.SANDEEP |05-APR-2019| -- END


            dgvBSC.Columns(16).HeaderText = dtBSC_TabularGrid.Columns("eself").Caption
            dgvBSC.Columns(17).HeaderText = dtBSC_TabularGrid.Columns("eremark").Caption

            'S.SANDEEP [06 Jan 2016] -- START
            If dtBSC_TabularGrid.Columns.Contains("Score") Then
                If dgvBSC.Columns(14).Visible Then
                    dgvBSC.Columns(14).HeaderText = dtBSC_TabularGrid.Columns("Score").Caption
                End If
            End If
            'S.SANDEEP [06 Jan 2016] -- END

            'S.SANDEEP |05-APR-2019| -- START
            'If CStr(Session("ViewTitles_InEvaluation")).Trim.Length > 0 Then
            '    Dim iEval() As String = CStr(Session("ViewTitles_InEvaluation")).Split("|")
            '    If iEval IsNot Nothing Then
            '        For Each xCol As DataGridColumn In dgvBSC.Columns
            '            If xCol.FooterText IsNot Nothing Then
            '                If IsNumeric(xCol.FooterText) Then
            '                    If Array.IndexOf(iEval, xCol.FooterText.ToString) < 0 Then
            '                        xCol.Visible = False
            '                    End If
            '                End If
            '            End If
            '        Next
            '    End If
            'End If
            'S.SANDEEP |05-APR-2019| -- END

            If dgvBSC.Columns(0).Visible = True Then
                xVal = 1
            ElseIf dgvBSC.Columns(1).Visible = True Then
                xVal = 2
            ElseIf dgvBSC.Columns(2).Visible = True Then
                xVal = 3
            ElseIf dgvBSC.Columns(3).Visible = True Then
                xVal = 4
            ElseIf dgvBSC.Columns(4).Visible = True Then
                xVal = 5
            ElseIf dgvBSC.Columns(5).Visible = True Then
                xVal = 6
            ElseIf dgvBSC.Columns(6).Visible = True Then
                xVal = 7
            ElseIf dgvBSC.Columns(7).Visible = True Then
                xVal = 8
            End If

            'S.SANDEEP [07 FEB 2015] -- START
            Dim xWidth As Integer = 0
            Dim objFMst As New clsAssess_Field_Master

            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK, Session("Companyunkid"))

            'S.SANDEEP [04 MAR 2015] -- START
            'If xWidth > 0 Then dgvBSC.Columns(15).ItemStyle.Width = xWidth
            If xWidth > 0 Then
                dgvBSC.Columns(17).HeaderStyle.Width = Unit.Pixel(xWidth)
                dgvBSC.Columns(17).ItemStyle.Width = Unit.Pixel(xWidth)
            End If
            'S.SANDEEP [04 MAR 2015] -- END

            objFMst = Nothing
            'S.SANDEEP [07 FEB 2015] -- END

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            Dim intTotalWidth As Integer = 0
            For Each xCol As DataGridColumn In dgvBSC.Columns
                If xCol.Visible Then
                    intTotalWidth += xCol.HeaderStyle.Width.Value
                End If
            Next
            'S.SANDEEP |12-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}
            'For Each xCol As DataGridColumn In dgvBSC.Columns
            '    If xCol.Visible Then
            '        Dim decColumnWidth As Decimal = xCol.HeaderStyle.Width.Value * 100 / intTotalWidth
            '        xCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
            '        xCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
            '    End If
            'Next
            'S.SANDEEP |12-MAR-2019| -- END

            'SHANI [09 Mar 2015]--END 

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            If menAction <> enAction.EDIT_ONE AndAlso CBool(Session("EnableBSCAutomaticRating")) Then
                If mdtBSC_Evaluation.Select("AUD = 'A' AND iEmployeeId = '" & cboEmployee.SelectedValue & "'").Count <= 0 Then
                    Dim dRow As DataRow
                    For Each dtRow As DataRow In dtBSC_TabularGrid.Select("IsGrp = 'False'")
                        dRow = mdtBSC_Evaluation.NewRow
                        dRow.Item("analysistranunkid") = -1
                        dRow.Item("analysisunkid") = -1
                        dRow.Item("empfield1unkid") = CInt(dtRow.Item("empfield1unkid"))
                        dRow.Item("result") = dtRow.Item("eself")
                        dRow.Item("remark") = ""
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        dRow.Item("isvoid") = False
                        dRow.Item("voiduserunkid") = -1
                        dRow.Item("voiddatetime") = DBNull.Value
                        dRow.Item("voidreason") = ""
                        dRow.Item("empfield2unkid") = CInt(dtRow.Item("empfield2unkid"))
                        dRow.Item("empfield3unkid") = CInt(dtRow.Item("empfield3unkid"))
                        dRow.Item("empfield4unkid") = CInt(dtRow.Item("empfield4unkid"))
                        dRow.Item("empfield5unkid") = CInt(dtRow.Item("empfield5unkid"))
                        dRow.Item("perspectiveunkid") = CInt(dtRow.Item("perspectiveunkid"))
                        dRow.Item("item_weight") = dtRow.Item("eitem_weight")
                        dRow.Item("max_scale") = dtRow.Item("emax_scale")
                        dRow.Item("iPeriodId") = cboPeriod.SelectedValue

                        Select Case iExOrdr
                            Case enWeight_Types.WEIGHT_FIELD1
                                dRow.Item("iItemUnkid") = dRow.Item("empfield1unkid")
                            Case enWeight_Types.WEIGHT_FIELD2
                                dRow.Item("iItemUnkid") = dRow.Item("empfield2unkid")
                            Case enWeight_Types.WEIGHT_FIELD3
                                dRow.Item("iItemUnkid") = dRow.Item("empfield3unkid")
                            Case enWeight_Types.WEIGHT_FIELD4
                                dRow.Item("iItemUnkid") = dRow.Item("empfield4unkid")
                            Case enWeight_Types.WEIGHT_FIELD5
                                dRow.Item("iItemUnkid") = dRow.Item("empfield5unkid")
                        End Select
                        dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
                        dRow.Item("iScore") = dtRow.Item("eself")

                        mdtBSC_Evaluation.Rows.Add(dRow)
                        mdecMaxScale = 0 : mdecMaxScale = 0
                    Next
                End If
            End If
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP |12-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}
            'dgvBSC.Width = Unit.Percentage(99.5)
            dgvBSC.Width = Unit.Pixel(intTotalWidth)
            'S.SANDEEP |12-MAR-2019| -- END

            dgvBSC.DataSource = dtBSC_TabularGrid
            dgvBSC.DataBind()
            Call SetTotals(dgvBSC)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    '''''''''''''''''''''''''' COMPETENCIES/GENERAL EVALUATION METHODS
    Private Sub Fill_GE_Evaluation()
        Try
            'S.SANDEEP [29 JAN 2015] -- START
            'dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, 0, Session("ConsiderItemWeightAsNumber"), -1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, 0, Session("ConsiderItemWeightAsNumber"), -1, Session("Self_Assign_Competencies"))

            'S.SANDEEP [04-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : MAKE COMMON METHOD TO GET EMPLOYEE ASSESSOR/REVIEWER
            'dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(menAssess, _
            '                                                          CInt(cboEmployee.SelectedValue), _
            '                                                          CInt(cboPeriod.SelectedValue), 0, 0, _
            '                                                          Session("ConsiderItemWeightAsNumber"), -1, _
            '                                                          Session("Self_Assign_Competencies"), _
            '                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(menAssess, _
                                                                      CInt(cboEmployee.SelectedValue), _
                                                                      CInt(cboPeriod.SelectedValue), 0, 0, _
                                                                      Session("ConsiderItemWeightAsNumber"), -1, _
                                                                      Session("Self_Assign_Competencies"), _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                      Session("Database_Name").ToString())
            'S.SANDEEP [04-AUG-2017] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [29 JAN 2015] -- END
            dgvGE.AutoGenerateColumns = False
            If Session("ScoringOptionId") = enScoringOption.SC_SCALE_BASED Then
                dgvGE.Columns(1).Visible = False    'WEIGHT
                dgvGE.Columns(2).Visible = True     'SCORE GUIDE
            Else
                dgvGE.Columns(1).Visible = True     'WEIGHT
                dgvGE.Columns(2).Visible = False    'SCORE GUIDE
            End If
            dgvGE.Columns(4).Visible = CBool(Session("ConsiderItemWeightAsNumber"))

            dgvGE.Columns(3).HeaderText = dtGE_TabularGrid.Columns("eself").Caption
            dgvGE.Columns(5).HeaderText = dtGE_TabularGrid.Columns("eremark").Caption


            'S.SANDEEP [06 Jan 2016] -- START
            If dtGE_TabularGrid.Columns.Contains("Score") Then
                If dgvGE.Columns(2).Visible Then
                    dgvGE.Columns(2).HeaderText = dtGE_TabularGrid.Columns("Score").Caption
                End If
            End If
            'S.SANDEEP [06 Jan 2016] -- END


            'S.SANDEEP [07 FEB 2015] -- START
            Dim xWidth As Integer = 0
            Dim objFMst As New clsAssess_Field_Master

            'S.SANDEEP |15-MAR-2019| -- START
            'xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK)
            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK, Session("CompanyUnkid"))
            'S.SANDEEP |15-MAR-2019| -- END

            If xWidth > 0 Then dgvGE.Columns(5).ItemStyle.Width = xWidth

            objFMst = Nothing
            'S.SANDEEP [07 FEB 2015] -- END

            dgvGE.DataSource = dtGE_TabularGrid
            dgvGE.DataBind()
            Call SetTotals(dgvGE)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    '''''''''''''''''''''''''' CUSTOM ITEMS EVALUATION METHODS
    Private Sub Fill_Custom_Grid(Optional ByVal iFromAddEdit As Boolean = False)
        Try
            If iHeaderId >= 0 Then

                'S.SANDEEP [12 OCT 2016] -- START
                Dim objCHdr As New clsassess_custom_header
                objCHdr._Customheaderunkid = dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")
                mblnIsMatchCompetencyStructure = objCHdr._IsMatch_With_Competency
                objCHdr = Nothing
                'S.SANDEEP [12 OCT 2016] -- END

                If iFromAddEdit = False Then
                    'S.SANDEEP |14-MAR-2019| -- START
                    'dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), CInt(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")), mintAssessAnalysisUnkid, menAssess, CInt(cboEmployee.SelectedValue), menAction)
                    If Session("CompanyGroupName") = "NMB PLC" Then
                        dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), CInt(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")), mintAssessAnalysisUnkid, menAssess, CInt(cboEmployee.SelectedValue), menAction, , True)
                    Else
                        dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), CInt(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")), mintAssessAnalysisUnkid, menAssess, CInt(cboEmployee.SelectedValue), menAction)
                    End If
                    'S.SANDEEP |14-MAR-2019| -- END
                Else
                    Dim iRow() As DataRow = dtCustomTabularGrid.Select("periodunkid <= 0")
                    For i As Integer = 0 To iRow.Length - 1
                        dtCustomTabularGrid.Rows.Remove(iRow(i))
                    Next
                End If
                If dtCustomTabularGrid.Rows.Count > 0 Then
                    dgvItems.Columns.Clear()
                End If
                Call Add_GridColumns()
                dgvItems.DataSource = Nothing
                dgvItems.AutoGenerateColumns = False
                Dim iColName As String = String.Empty
                For Each dCol As DataColumn In dtCustomTabularGrid.Columns
                    iColName = "" : iColName = "obj" & dCol.ColumnName
                    Dim dgvCol As New BoundField()
                    dgvCol.FooterText = iColName
                    dgvCol.ReadOnly = True
                    dgvCol.DataField = dCol.ColumnName
                    dgvCol.HeaderText = dCol.Caption
                    If dgvItems.Columns.Contains(dgvCol) = True Then Continue For
                    If dCol.Caption.Length <= 0 Then
                        dgvCol.Visible = False
                    End If
                    dgvItems.Columns.Add(dgvCol)
                    If dgvCol.FooterText = "objHeader_Name" Then dgvCol.Visible = False
                Next

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Call SetDateFormat()
                'Pinkal (16-Apr-2016) -- End
                dgvItems.DataSource = dtCustomTabularGrid
                dgvItems.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Custom_Evaluation_Data()
        Try
            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
                ''S.SANDEEP [10 DEC 2015] -- START
                ''dtEval = New DataView(mdtCustomEvaluation, "custom_header = '" & dsHeaders.Tables(0).Rows(iHeaderId).Item("Name") & "' AND AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable
                ''Shani (26-Sep-2016) -- Start
                ''Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                ''Dim dtEval As DataTable
                ''dtEval = New DataView(mdtCustomEvaluation, "custom_header = '" & dsHeaders.Tables(0).Rows(iHeaderId).Item("Name") & "' AND AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable
                'Dim dtEval As DataView
                'dtEval = New DataView(mdtCustomEvaluation, "custom_header = '" & dsHeaders.Tables(0).Rows(iHeaderId).Item("Name") & "' AND AUD <> 'D'", "", DataViewRowState.CurrentRows)
                ''Shani (26-Sep-2016) -- End
                ''S.SANDEEP [10 DEC 2015] -- END

                'dtCustomTabularGrid.Rows.Clear()
                'Dim dFRow As DataRow = Nothing
                'Dim iCustomId, iNewCustomId As Integer : iCustomId = 0 : iNewCustomId = 0
                'For Each dRow As DataRowView In dtEval
                '    If dRow.Item("AUD") = "D" Then Continue For
                '    If iCustomId <= 0 Then iCustomId = dRow.Item("customitemunkid")
                '    iNewCustomId = dRow.Item("customitemunkid")
                '    If iCustomId = iNewCustomId Then
                '        dFRow = dtCustomTabularGrid.NewRow
                '        dtCustomTabularGrid.Rows.Add(dFRow)

                '        'Shani (26-Sep-2016) -- Start
                '        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                '        If menAction <> enAction.EDIT_ONE Then
                '            'dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
                '            dFRow.Item("GUID") = System.Guid.NewGuid().ToString
                '        End If
                '        'Shani (26-Sep-2016) -- End

                '    End If

                '    Select Case CInt(dRow.Item("itemtypeid"))
                '        Case clsassess_custom_items.enCustomType.FREE_TEXT
                '            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
                '        Case clsassess_custom_items.enCustomType.SELECTION
                '            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                '                Select Case CInt(dRow.Item("selectionmodeid"))
                '                    Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                '                        Dim objCMaster As New clsCommon_Master
                '                        objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                '                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                '                        objCMaster = Nothing
                '                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                '                        Dim objCMaster As New clsassess_competencies_master
                '                        objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
                '                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                '                        objCMaster = Nothing
                '                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                '                        Dim objEmpField1 As New clsassess_empfield1_master
                '                        objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
                '                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
                '                        objEmpField1 = Nothing
                '                End Select
                '            End If
                '        Case clsassess_custom_items.enCustomType.DATE_SELECTION
                '            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                '                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                '            End If
                '        Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                '            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                '                If IsNumeric(dRow.Item("custom_value")) Then
                '                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
                '                End If
                '            End If
                '    End Select
                '    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
                '    dFRow.Item("periodunkid") = dRow.Item("periodunkid")
                '    dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
                '    dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
                '    'Shani (26-Sep-2016) -- Start
                '    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                '    'dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
                '    If menAction <> enAction.EDIT_ONE Then
                '        dRow.Item("customanalysistranguid") = dFRow.Item("GUID")
                '    Else
                '        dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
                '    End If
                '    'Shani (26-Sep-2016) -- End
                '    dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
                'Next
                'mdtCustomEvaluation.AcceptChanges()
                'If dtCustomTabularGrid.Rows.Count <= 0 Then
                '    dtCustomTabularGrid.Rows.Add(dtCustomTabularGrid.NewRow)
                '    dtCustomTabularGrid.Rows(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                'End If
                'Call Fill_Custom_Grid(True)

                dtCustomTabularGrid.Rows.Clear()
                Dim strGUIDArray() As String = Nothing

                Dim dR = mdtCustomEvaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("custom_header") = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString And x.Field(Of String)("AUD") <> "D")
                If dR.Count > 0 Then
                    strGUIDArray = dR.Cast(Of DataRow).Select(Function(x) x.Field(Of String)("customanalysistranguid")).Distinct().ToArray()
                End If

                If strGUIDArray IsNot Nothing AndAlso strGUIDArray.Length > 0 Then
                    Dim dFRow As DataRow = Nothing
                    For Each Str As String In strGUIDArray
                        dFRow = dtCustomTabularGrid.NewRow
                        dtCustomTabularGrid.Rows.Add(dFRow)
                        For Each dRow As DataRow In mdtCustomEvaluation.Select("customanalysistranguid = '" & Str & "' AND AUD <> 'D' ")
                            Select Case CInt(dRow.Item("itemtypeid"))
                                Case clsassess_custom_items.enCustomType.FREE_TEXT
                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
                                Case clsassess_custom_items.enCustomType.SELECTION
                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                        Select Case CInt(dRow.Item("selectionmodeid"))
                                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                                Dim objCMaster As New clsCommon_Master
                                                objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                                objCMaster = Nothing
                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                                Dim objCMaster As New clsassess_competencies_master
                                                objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                                objCMaster = Nothing
                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                                Dim objEmpField1 As New clsassess_empfield1_master
                                                objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
                                                objEmpField1 = Nothing

                                                'S.SANDEEP |16-AUG-2019| -- START
                                                'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                            Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                                Dim objCMaster As New clsCommon_Master
                                                objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                                objCMaster = Nothing
                                                'S.SANDEEP |16-AUG-2019| -- END
                                        End Select
                                    End If
                                Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                                    End If
                                Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                        If IsNumeric(dRow.Item("custom_value")) Then
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
                                        End If
                                    End If
                            End Select
                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
                            dFRow.Item("periodunkid") = dRow.Item("periodunkid")
                            dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
                            dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
                            dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
                            dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
                            dFRow.Item("ismanual") = dRow.Item("ismanual")
                        Next
                    Next
                End If
                mdtCustomEvaluation.AcceptChanges()
                If dtCustomTabularGrid.Rows.Count <= 0 Then
                    dtCustomTabularGrid.Rows.Add(dtCustomTabularGrid.NewRow)
                    dtCustomTabularGrid.Rows(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                End If
                Call Fill_Custom_Grid(True)
                'Shani (02-Nov-2016) -- Start
                objlblValue1.Text = "&nbsp"
                objlblValue2.Text = "&nbsp"
                objlblValue3.Text = "&nbsp"
                objlblValue4.Text = "&nbsp"
                'Shani (02-Nov-2016) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Add_GridColumns()
        Try
            '************* ADD
            Dim iTempField As New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "ObjAdd"
            iTempField.HeaderText = "Add"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            dgvItems.Columns.Add(iTempField)

            '************* EDIT
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "objEdit"
            iTempField.HeaderText = "Edit"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            dgvItems.Columns.Add(iTempField)

            '************* DELETE
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "objDelete"
            iTempField.HeaderText = "Delete"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            dgvItems.Columns.Add(iTempField)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Generate_Popup_Data(ByVal xHeaderUnkid As Integer)
        Try
            Dim dtCItems As DataTable
            'S.SANDEEP |14-MAR-2019| -- START
            'dtCItems = objEAnalysisMst.Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid, menAssess)
            'S.SANDEEP |25-MAR-2019| -- START
            If Session("CompanyGroupName") = "NMB PLC" Then
                'If Session("CompCode").ToString.ToUpper = "NMB" Then
                'S.SANDEEP |25-MAR-2019| -- END
                dtCItems = objEAnalysisMst.Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid, menAssess)
            Else
                dtCItems = objEAnalysisMst.Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid, 0)
            End If
            'S.SANDEEP |14-MAR-2019| -- END

            'S.SANDEEP |09-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : PA CHANGES
            If dtCItems IsNot Nothing AndAlso dtCItems.Rows.Count <= 0 Then Exit Sub
            'S.SANDEEP |09-JUL-2019| -- END

            'S.SANDEEP [12 OCT 2016] -- START
            dtCItems = dtCItems.Select("", "isdefaultentry DESC").CopyToDataTable
            'S.SANDEEP [12 OCT 2016] -- END

            If mstriEditingGUID <> "" Then 'mdtCustomEvaluation
                'If CInt(Me.ViewState("RowIndex")) > -1 AndAlso mstriEditingGUID <> "" Then 'mdtCustomEvaluation
                Dim strFilter As String = ""
                If CInt(Me.ViewState("RowIndex")) > -1 Then
                    strFilter = "customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D'"
                Else
                    strFilter = "customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D' AND isdefaultentry = True"
                End If


                Dim dtmp() As DataRow = mdtCustomEvaluation.Select(strFilter)
                If dtmp.Length > 0 Then
                    For iEdit As Integer = 0 To dtmp.Length - 1
                        Dim xRow() As DataRow = dtCItems.Select("customitemunkid = '" & dtmp(iEdit).Item("customitemunkid") & "'")
                        If xRow.Length > 0 Then
                            Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
                                Case clsassess_custom_items.enCustomType.SELECTION
                                    Select Case CInt(dtmp(iEdit).Item("selectionmodeid"))
                                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")

                                            'S.SANDEEP |16-AUG-2019| -- START
                                            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                        Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
                                            'S.SANDEEP |16-AUG-2019| -- END
                                    End Select
                                Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                    If dtmp(iEdit).Item("custom_value").ToString.Trim.Length > 0 Then
                                        xRow(0).Item("ddate") = eZeeDate.convertDate(dtmp(iEdit).Item("custom_value"))
                                    End If
                            End Select
                            xRow(0).Item("custom_value") = dtmp(iEdit).Item("dispaly_value")
                        End If
                    Next
                End If
            End If

            Me.Session("dtCItems") = dtCItems
            dgv_Citems.AutoGenerateColumns = False
            Dim objCHeader As New clsassess_custom_header
            objCHeader._Customheaderunkid = xHeaderUnkid
            If Me.ViewState("Header") IsNot Nothing Then
                Me.ViewState("Header") = objCHeader._Name
            Else
                Me.ViewState.Add("Header", objCHeader._Name)
            End If
            objCHeader = Nothing
            dgv_Citems.DataSource = dtCItems
            dgv_Citems.DataBind()
            mblnItemAddEdit = True
            'S.SANDEEP |25-MAR-2019| -- START
            lblpopupHeader.Text = Me.ViewState("Header")
            'S.SANDEEP |25-MAR-2019| -- END
            popup_CItemAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    ''''''''''''''''''''''''''''''''''' ADD EDIT DELETE ROWS IN TABLES
    Private Sub Evaluated_Data_BSC(ByVal iResult As Decimal, ByVal iRemark As String, ByVal iRowIdx As Integer, Optional ByVal isRemarkChanged As Boolean = False) 'S.SANDEEP [21 JAN 2015] -- START {isRemarkChanged} -- END
        'Private Sub Evaluated_Data_BSC(ByVal iResult As Decimal, ByVal iRemark As String, ByVal iRowIdx As Integer)
        Try
            'If Session("BSC_Evaluation") IsNot Nothing Then
            '    mdtBSC_Evaluation = Session("BSC_Evaluation")
            'End If
            'If Session("BSC_TabularGrid") IsNot Nothing Then
            '    dtBSC_TabularGrid = Session("BSC_TabularGrid")
            'End If

            Dim dRow As DataRow = Nothing : Dim dTemp() As DataRow = Nothing

            'Select Case CInt(Me.ViewState("iExOrdr"))
            '    Case enWeight_Types.WEIGHT_FIELD1
            '        dTemp = mdtBSC_Evaluation.Select("empfield1unkid = '" & dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield1unkid").ToString & "' AND AUD <> 'D' ")
            '    Case enWeight_Types.WEIGHT_FIELD2
            '        dTemp = mdtBSC_Evaluation.Select("empfield2unkid = '" & dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield2unkid").ToString & "' AND AUD <> 'D' ")
            '    Case enWeight_Types.WEIGHT_FIELD3
            '        dTemp = mdtBSC_Evaluation.Select("empfield3unkid = '" & dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield3unkid").ToString & "' AND AUD <> 'D' ")
            '    Case enWeight_Types.WEIGHT_FIELD4
            '        dTemp = mdtBSC_Evaluation.Select("empfield4unkid = '" & dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield4unkid").ToString & "' AND AUD <> 'D' ")
            '    Case enWeight_Types.WEIGHT_FIELD5
            '        dTemp = mdtBSC_Evaluation.Select("empfield5unkid = '" & dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield5unkid").ToString & "' AND AUD <> 'D' ")
            'End Select

            dTemp = GetOldValue_BSC(iRowIdx)

            'S.SANDEEP [ 17 DEC 2014 ] -- START
            'If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
            '    If CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D' ")) > xTotAssignedWeight Then
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry you cannot go beyond the total weight assigned."), Me)
            '        Exit Sub
            '    End If
            'End If
            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_WEIGHTED_BASED Then
                If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                    If CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D' ")) > xTotAssignedWeight Then
                        'S.SANDEEP |08-JAN-2019| -- START
                        'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry you cannot go beyond the total weight assigned."), Me)
                        'Exit Sub
                        If CBool(Session("EnableBSCAutomaticRating")) = False Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry you cannot go beyond the total weight assigned."), Me)
                            Exit Sub
                        End If
                        'S.SANDEEP |08-JAN-2019| -- END
                    End If
                End If
            End If
            'S.SANDEEP [ 17 DEC 2014 ] -- END

            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            If mdtBSC_Evaluation Is Nothing Then Exit Sub
            'S.SANDEEP |18-JAN-2020| -- END

            If dTemp IsNot Nothing AndAlso dTemp.Length <= 0 Then
                dRow = mdtBSC_Evaluation.NewRow
                dRow.Item("analysistranunkid") = -1
                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                dRow.Item("empfield1unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield1unkid"))
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dRow.Item("result") = iResult
                dRow.Item("result") = Format(CDec(iResult), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END
                dRow.Item("remark") = iRemark
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                dRow.Item("empfield2unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield2unkid"))
                dRow.Item("empfield3unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield3unkid"))
                dRow.Item("empfield4unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield4unkid"))
                dRow.Item("empfield5unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield5unkid"))
                dRow.Item("perspectiveunkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("perspectiveunkid"))
                'S.SANDEEP [21 JAN 2015] -- START
                Select Case iExOrdr
                    Case enWeight_Types.WEIGHT_FIELD1
                        If dRow.Item("empfield1unkid") <= 0 Then Exit Sub
                    Case enWeight_Types.WEIGHT_FIELD2
                        If dRow.Item("empfield2unkid") <= 0 Then Exit Sub
                    Case enWeight_Types.WEIGHT_FIELD3
                        If dRow.Item("empfield3unkid") <= 0 Then Exit Sub
                    Case enWeight_Types.WEIGHT_FIELD4
                        If dRow.Item("empfield4unkid") <= 0 Then Exit Sub
                    Case enWeight_Types.WEIGHT_FIELD5
                        If dRow.Item("empfield5unkid") <= 0 Then Exit Sub
                End Select
                If isRemarkChanged = False Then
                    dRow.Item("item_weight") = Me.ViewState("mdecItemWeight")
                    dRow.Item("max_scale") = Me.ViewState("mdecMaxScale")
                End If
                dRow.Item("iPeriodId") = cboPeriod.SelectedValue
                Select Case iExOrdr
                    Case enWeight_Types.WEIGHT_FIELD1
                        dRow.Item("iItemUnkid") = dRow.Item("empfield1unkid")
                    Case enWeight_Types.WEIGHT_FIELD2
                        dRow.Item("iItemUnkid") = dRow.Item("empfield2unkid")
                    Case enWeight_Types.WEIGHT_FIELD3
                        dRow.Item("iItemUnkid") = dRow.Item("empfield3unkid")
                    Case enWeight_Types.WEIGHT_FIELD4
                        dRow.Item("iItemUnkid") = dRow.Item("empfield4unkid")
                    Case enWeight_Types.WEIGHT_FIELD5
                        dRow.Item("iItemUnkid") = dRow.Item("empfield5unkid")
                End Select
                dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dRow.Item("iScore") = iResult
                dRow.Item("iScore") = Format(CDec(iResult), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END

                'S.SANDEEP [21 JAN 2015] -- END
                mdtBSC_Evaluation.Rows.Add(dRow)
                'S.SANDEEP [21 JAN 2015] -- START
                Me.ViewState("mdecItemWeight") = 0 : Me.ViewState("mdecMaxScale") = 0
                'S.SANDEEP [21 JAN 2015] -- END
            Else
                If menAction <> enAction.EDIT_ONE Then
                    dTemp(0).Item("analysistranunkid") = -1
                Else
                    dTemp(0).Item("analysistranunkid") = dTemp(0).Item("analysistranunkid")
                End If
                dTemp(0).Item("analysisunkid") = mintAssessAnalysisUnkid
                dTemp(0).Item("empfield1unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield1unkid"))
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dTemp(0).Item("result") = iResult
                dTemp(0).Item("result") = Format(CDec(iResult), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END
                dTemp(0).Item("remark") = iRemark
                If IsDBNull(dTemp(0).Item("AUD")) Or CStr(dTemp(0).Item("AUD")).ToString.Trim = "" Then
                    dTemp(0).Item("AUD") = "U"
                End If
                dTemp(0).Item("GUID") = Guid.NewGuid.ToString
                dTemp(0).Item("isvoid") = False
                dTemp(0).Item("voiduserunkid") = -1
                dTemp(0).Item("voiddatetime") = DBNull.Value
                dTemp(0).Item("voidreason") = ""
                dTemp(0).Item("empfield2unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield2unkid"))
                dTemp(0).Item("empfield3unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield3unkid"))
                dTemp(0).Item("empfield4unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield4unkid"))
                dTemp(0).Item("empfield5unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield5unkid"))
                dTemp(0).Item("perspectiveunkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("perspectiveunkid"))
                'S.SANDEEP [21 JAN 2015] -- START
                If isRemarkChanged = False Then
                    dTemp(0).Item("item_weight") = Me.ViewState("mdecItemWeight")
                    dTemp(0).Item("max_scale") = Me.ViewState("mdecMaxScale")
                End If
                dTemp(0).Item("iPeriodId") = cboPeriod.SelectedValue
                Select Case iExOrdr
                    Case enWeight_Types.WEIGHT_FIELD1
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield1unkid")
                    Case enWeight_Types.WEIGHT_FIELD2
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield2unkid")
                    Case enWeight_Types.WEIGHT_FIELD3
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield3unkid")
                    Case enWeight_Types.WEIGHT_FIELD4
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield4unkid")
                    Case enWeight_Types.WEIGHT_FIELD5
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield5unkid")
                End Select
                dTemp(0).Item("iEmployeeId") = cboEmployee.SelectedValue
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dTemp(0).Item("iScore") = iResult
                dTemp(0).Item("iScore") = Format(CDec(iResult), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END

                'S.SANDEEP [21 JAN 2015] -- END
                mdtBSC_Evaluation.AcceptChanges()
                'S.SANDEEP [21 JAN 2015] -- START
                Me.ViewState("mdecItemWeight") = 0 : Me.ViewState("mdecMaxScale") = 0
                'S.SANDEEP [21 JAN 2015] -- END
            End If
            Call SetTotals(dgvBSC)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Evaluated_Data_GE(ByVal iResult As Decimal, ByVal iRemark As String, ByVal iRowIdx As Integer, ByVal xGrdItem As DataGridItem, Optional ByVal isRemarkChanged As Boolean = False) 'S.SANDEEP [21 JAN 2015] -- START {isRemarkChanged} -- END
        'Private Sub Evaluated_Data_GE(ByVal iResult As Decimal, ByVal iRemark As String, ByVal iRowIdx As Integer, ByVal xGrdItem As DataGridItem)
        Dim dRow As DataRow = Nothing
        Dim dTemp() As DataRow = Nothing
        Try
            'If Session("GE_Evaluation") IsNot Nothing Then
            '    mdtGE_Evaluation = Session("GE_Evaluation")
            'End If

            'If Session("GE_TabularGrid") IsNot Nothing Then
            '    dtGE_TabularGrid = Session("GE_TabularGrid")
            'End If

            Dim dtTemp() As DataRow = Nothing
            Dim iDecWeight As Decimal = 0
            Decimal.TryParse(CStr(xGrdItem.Cells(1).Text), iDecWeight)  'WEIGHT

            If iDecWeight <= 0 Then iDecWeight = 1
            If Session("ConsiderItemWeightAsNumber") = False Then iDecWeight = 1

            'dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdItem.Cells(8).Text) & "' AND competenciesunkid = '" & _
            '                                                  CInt(xGrdItem.Cells(7).Text) & "' AND AUD <> 'D' ") '7-> competenciesunkid, 8-> assessgroupunkid

            dtTemp = GetOldValue_GE(xGrdItem)

            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            If mdtGE_Evaluation Is Nothing Then Exit Sub
            'S.SANDEEP |18-JAN-2020| -- END

            If dtTemp IsNot Nothing AndAlso dtTemp.Length <= 0 Then
                dRow = mdtGE_Evaluation.NewRow
                dRow.Item("analysistranunkid") = -1
                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                dRow.Item("assessgroupunkid") = CInt(xGrdItem.Cells(8).Text)    'assessgroupunkid
                dRow.Item("competenciesunkid") = CInt(xGrdItem.Cells(7).Text)   'competenciesunkid
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dRow.Item("result") = iResult
                dRow.Item("result") = Format(CDec(iResult), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END
                dRow.Item("remark") = iRemark
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                dRow.Item("dresult") = iResult * iDecWeight
                'S.SANDEEP [21 JAN 2015] -- START
                If isRemarkChanged = False Then
                    dRow.Item("item_weight") = Me.ViewState("mdecItemWeight")
                    'S.SANDEEP |16-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                    'dRow.Item("max_scale") = Me.ViewState("mdecMaxScale")
                    If CInt(Me.ViewState("mdecMaxScale")) <= 0 Then
                        Dim dsScore_Guide As New DataSet
                        Dim objScaleMaster As New clsAssessment_Scale
                        dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(xGrdItem.Cells(6).Text))
                        objScaleMaster = Nothing
                        If dsScore_Guide.Tables(0).Rows.Count > 0 Then
                            dRow.Item("max_scale") = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
                        End If
                    Else
                        dRow.Item("max_scale") = Me.ViewState("mdecMaxScale")
                    End If
                    'S.SANDEEP |16-JUL-2019| -- END
                End If
                dRow.Item("iPeriodId") = cboPeriod.SelectedValue
                dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dRow.Item("iScore") = iResult
                dRow.Item("iScore") = Format(CDec(iResult), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END
                dRow.Item("iItemUnkid") = dRow.Item("competenciesunkid")
                'S.SANDEEP [21 JAN 2015] -- END
                mdtGE_Evaluation.Rows.Add(dRow)
                'S.SANDEEP [21 JAN 2015] -- START
                Me.ViewState("mdecItemWeight") = 0 : Me.ViewState("mdecMaxScale") = 0
                'S.SANDEEP [21 JAN 2015] -- END
            Else
                dtTemp(0).Item("analysistranunkid") = dtTemp(0).Item("analysistranunkid")
                dtTemp(0).Item("analysisunkid") = mintAssessAnalysisUnkid
                dtTemp(0).Item("assessgroupunkid") = CInt(xGrdItem.Cells(8).Text)    'assessgroupunkid
                dtTemp(0).Item("competenciesunkid") = CInt(xGrdItem.Cells(7).Text)   'competenciesunkid
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dtTemp(0).Item("result") = iResult
                dtTemp(0).Item("result") = Format(CDec(iResult), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END
                dtTemp(0).Item("remark") = iRemark
                If IsDBNull(dtTemp(0).Item("AUD")) Or CStr(dtTemp(0).Item("AUD")).ToString.Trim = "" Then
                    dtTemp(0).Item("AUD") = "U"
                End If
                dtTemp(0).Item("GUID") = Guid.NewGuid.ToString
                dtTemp(0).Item("isvoid") = False
                dtTemp(0).Item("voiduserunkid") = -1
                dtTemp(0).Item("voiddatetime") = DBNull.Value
                dtTemp(0).Item("voidreason") = ""
                dtTemp(0).Item("dresult") = iResult * iDecWeight

                'S.SANDEEP [21 JAN 2015] -- START
                If isRemarkChanged = False Then
                    dtTemp(0).Item("item_weight") = Me.ViewState("mdecItemWeight")

                    'S.SANDEEP |16-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                    'dtTemp(0).Item("max_scale") = Me.ViewState("mdecMaxScale")
                    If CInt(Me.ViewState("mdecMaxScale")) <= 0 Then
                        Dim dsScore_Guide As New DataSet
                        Dim objScaleMaster As New clsAssessment_Scale
                        dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(xGrdItem.Cells(6).Text))
                        objScaleMaster = Nothing
                        If dsScore_Guide.Tables(0).Rows.Count > 0 Then
                            dtTemp(0).Item("max_scale") = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
                        End If
                    Else
                        dtTemp(0).Item("max_scale") = Me.ViewState("mdecMaxScale")
                    End If
                    'S.SANDEEP |16-JUL-2019| -- END


                End If
                dtTemp(0).Item("iPeriodId") = cboPeriod.SelectedValue
                dtTemp(0).Item("iEmployeeId") = cboEmployee.SelectedValue
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dtTemp(0).Item("iScore") = iResult
                dtTemp(0).Item("iScore") = Format(CDec(iResult), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END
                dtTemp(0).Item("iItemUnkid") = dtTemp(0).Item("competenciesunkid")
                'S.SANDEEP [21 JAN 2015] -- END

                dtTemp(0).AcceptChanges()
                'S.SANDEEP [21 JAN 2015] -- START
                Me.ViewState("mdecItemWeight") = 0 : Me.ViewState("mdecMaxScale") = 0
                'S.SANDEEP [21 JAN 2015] -- END
            End If

            'S.SANDEEP [ 17 DEC 2014 ] -- START
            'If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
            '    Dim objAGroup As New clsassess_group_master
            '    objAGroup._Assessgroupunkid = CInt(xGrdItem.Cells(8).Text)
            '    If CBool(Session("ConsiderItemWeightAsNumber")) = True Then
            '        If CDbl(mdtGE_Evaluation.Compute("SUM(dresult)", "assessgroupunkid = '" & CInt(xGrdItem.Cells(8).Text) & "' AND AUD <> 'D' ")) > objAGroup._Weight Then
            '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Sorry you cannot go beyond the total weight assigned for the particular assessment group weight."), Me)

            '            dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdItem.Cells(8).Text) & "' AND competenciesunkid = '" & _
            '                                                      CInt(xGrdItem.Cells(7).Text) & "' AND AUD <> 'D' ") '6-> competenciesunkid, 7-> assessgroupunkid

            '            If dtTemp.Length > 0 Then
            '                dtTemp(0).Item("result") = 0 : CType(xGrdItem.Cells(3).Controls(1), TextBox).Text = 0
            '                dTemp(0).Item("dresult") = 0
            '            End If
            '            Exit Sub
            '        End If
            '    Else
            '        If CDbl(mdtGE_Evaluation.Compute("SUM(result)", "assessgroupunkid = '" & CInt(xGrdItem.Cells(8).Text) & "' AND AUD <> 'D' ")) > objAGroup._Weight Then
            '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Sorry you cannot go beyond the total weight assigned for the particular assessment group weight."), Me)

            '            dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdItem.Cells(8).Text) & "' AND competenciesunkid = '" & _
            '                                                      CInt(xGrdItem.Cells(7).Text) & "' AND AUD <> 'D' ") '6-> competenciesunkid, 7-> assessgroupunkid

            '            If dtTemp.Length > 0 Then
            '                dtTemp(0).Item("result") = 0 : CType(xGrdItem.Cells(3).Controls(1), TextBox).Text = 0
            '                dTemp(0).Item("dresult") = 0
            '            End If
            '            Exit Sub
            '        End If
            '    End If
            '    objAGroup = Nothing
            'End If
            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_WEIGHTED_BASED Then
                If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                    Dim objAGroup As New clsassess_group_master
                    objAGroup._Assessgroupunkid = CInt(xGrdItem.Cells(8).Text)
                    If CBool(Session("ConsiderItemWeightAsNumber")) = True Then
                        If CDbl(mdtGE_Evaluation.Compute("SUM(dresult)", "assessgroupunkid = '" & CInt(xGrdItem.Cells(8).Text) & "' AND AUD <> 'D' ")) > objAGroup._Weight Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Sorry you cannot go beyond the total weight assigned for the particular assessment group weight."), Me)

                            dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdItem.Cells(8).Text) & "' AND competenciesunkid = '" & _
                                                                      CInt(xGrdItem.Cells(7).Text) & "' AND AUD <> 'D' ") '6-> competenciesunkid, 7-> assessgroupunkid

                            If dtTemp.Length > 0 Then
                                dtTemp(0).Item("result") = 0 : CType(xGrdItem.Cells(3).Controls(1), TextBox).Text = 0
                                dTemp(0).Item("dresult") = 0
                            End If
                            Exit Sub
                        End If
                    Else
                        If CDbl(mdtGE_Evaluation.Compute("SUM(result)", "assessgroupunkid = '" & CInt(xGrdItem.Cells(8).Text) & "' AND AUD <> 'D' ")) > objAGroup._Weight Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Sorry you cannot go beyond the total weight assigned for the particular assessment group weight."), Me)

                            dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdItem.Cells(8).Text) & "' AND competenciesunkid = '" & _
                                                                      CInt(xGrdItem.Cells(7).Text) & "' AND AUD <> 'D' ") '6-> competenciesunkid, 7-> assessgroupunkid

                            If dtTemp.Length > 0 Then
                                dtTemp(0).Item("result") = 0 : CType(xGrdItem.Cells(3).Controls(1), TextBox).Text = 0
                                dTemp(0).Item("dresult") = 0
                            End If
                            Exit Sub
                        End If
                    End If
                    objAGroup = Nothing
                End If
            End If
            'S.SANDEEP [ 17 DEC 2014 ] -- END

            If CBool(Session("ConsiderItemWeightAsNumber")) = True Then
                If iDecWeight > 0 Then
                    xGrdItem.Cells(4).Text = iDecWeight * iResult
                End If
            End If
            Call SetTotals(dgvGE)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function isAll_Assessed_BSC(Optional ByVal blnFlag As Boolean = True) As Boolean
        Try
            If mdtBSC_Evaluation.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), Me)
                Return False
            End If
            Dim dtmp1() As DataRow = Nothing
            Dim dtmp2() As DataRow = Nothing

            Dim dView As DataView = dtBSC_TabularGrid.DefaultView

            Select Case iExOrdr
                Case enWeight_Types.WEIGHT_FIELD1
                    'S.SANDEEP [ 17 DEC 2014 ] -- START
                    'dtmp1 = dView.ToTable(True, "empfield1unkid").Select("empfield1unkid > 0")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield1unkid > 0")

                    'S.SANDEEP |09-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'dtmp1 = dView.ToTable(True, "empfield1unkid", "Weight").Select("empfield1unkid > 0 AND Weight <> '' ")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield1unkid > 0")
                    dtmp1 = dView.ToTable(True, "empfield1unkid", "oWeight").Select("empfield1unkid > 0 AND oWeight > 0 ")
                    dtmp2 = mdtBSC_Evaluation.DefaultView.ToTable(True, "empfield1unkid").Select("empfield1unkid > 0")
                    'S.SANDEEP |09-JUL-2019| -- END


                    'S.SANDEEP [ 17 DEC 2014 ] -- END

                Case enWeight_Types.WEIGHT_FIELD2
                    'S.SANDEEP [ 17 DEC 2014 ] -- START
                    'dtmp1 = dView.ToTable(True, "empfield2unkid").Select("empfield2unkid > 0")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield2unkid > 0")

                    'S.SANDEEP |09-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'dtmp1 = dView.ToTable(True, "empfield2unkid", "Weight").Select("empfield2unkid > 0 AND Weight <> '' ")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield2unkid > 0")
                    dtmp1 = dView.ToTable(True, "empfield2unkid", "oWeight").Select("empfield2unkid > 0 AND oWeight > 0 ")
                    dtmp2 = mdtBSC_Evaluation.DefaultView.ToTable(True, "empfield2unkid").Select("empfield2unkid > 0")
                    'S.SANDEEP |09-JUL-2019| -- END


                    'S.SANDEEP [ 17 DEC 2014 ] -- END

                Case enWeight_Types.WEIGHT_FIELD3
                    'S.SANDEEP [ 17 DEC 2014 ] -- START
                    'dtmp1 = dView.ToTable(True, "empfield3unkid").Select("empfield3unkid > 0")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield3unkid > 0")

                    'S.SANDEEP |09-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'dtmp1 = dView.ToTable(True, "empfield3unkid", "Weight").Select("empfield3unkid > 0 AND Weight <> '' ")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield3unkid > 0")
                    dtmp1 = dView.ToTable(True, "empfield3unkid", "oWeight").Select("empfield3unkid > 0 AND oWeight > 0 ")
                    dtmp2 = mdtBSC_Evaluation.DefaultView.ToTable(True, "empfield3unkid").Select("empfield3unkid > 0")
                    'S.SANDEEP |09-JUL-2019| -- END


                    'S.SANDEEP [ 17 DEC 2014 ] -- END

                Case enWeight_Types.WEIGHT_FIELD4
                    'S.SANDEEP [ 17 DEC 2014 ] -- START
                    'dtmp1 = dView.ToTable(True, "empfield4unkid").Select("empfield4unkid > 0")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield4unkid > 0")

                    'S.SANDEEP |09-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'dtmp1 = dView.ToTable(True, "empfield4unkid", "Weight").Select("empfield4unkid > 0 AND Weight <> '' ")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield4unkid > 0")
                    dtmp1 = dView.ToTable(True, "empfield4unkid", "oWeight").Select("empfield4unkid > 0 AND oWeight > 0 ")
                    dtmp2 = mdtBSC_Evaluation.DefaultView.ToTable(True, "empfield4unkid").Select("empfield4unkid > 0")
                    'S.SANDEEP |09-JUL-2019| -- END


                    'S.SANDEEP [ 17 DEC 2014 ] -- END

                Case enWeight_Types.WEIGHT_FIELD5
                    'S.SANDEEP [ 17 DEC 2014 ] -- START
                    'dtmp1 = dView.ToTable(True, "empfield5unkid").Select("empfield5unkid > 0")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield5unkid > 0")

                    'S.SANDEEP |09-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'dtmp1 = dView.ToTable(True, "empfield5unkid", "Weight").Select("empfield5unkid > 0 AND Weight <> '' ")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield5unkid > 0")
                    dtmp1 = dView.ToTable(True, "empfield5unkid", "oWeight").Select("empfield5unkid > 0 AND oWeight > 0 ")
                    dtmp2 = mdtBSC_Evaluation.DefaultView.ToTable(True, "empfield5unkid").Select("empfield5unkid > 0")
                    'S.SANDEEP |09-JUL-2019| -- END

                    'S.SANDEEP [ 17 DEC 2014 ] -- END

            End Select

            If dtmp1.Length <> dtmp2.Length Then
                If blnFlag = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), Me)
                    Return False
                    'Shani(30-MAR-2016) -- Start
                Else
                    Return False
                    'Shani(30-MAR-2016) -- End
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Function isAll_Assessed_GE(Optional ByVal blnFlag As Boolean = True) As Boolean
        Try
            If mdtGE_Evaluation.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some assessment group."), Me)
                Return False
            End If
            Dim dtmp1() As DataRow = Nothing
            Dim dtmp2() As DataRow = Nothing
            Dim dView As DataView = dtGE_TabularGrid.DefaultView
            Dim dMView As DataView = mdtGE_Evaluation.DefaultView
            dtmp1 = dView.ToTable(True, "assessgroupunkid").Select("assessgroupunkid > 0")
            dtmp2 = dMView.ToTable(True, "assessgroupunkid").Select("assessgroupunkid > 0")
            If dtmp1.Length <> dtmp2.Length Then
                If blnFlag = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot do Save Commit Operation as you have not assessed all assessment group."), Me)
                    Return False
                End If
            End If
            dtmp1 = dView.ToTable.Select("assessgroupunkid > 0 AND IsGrp = False AND IsPGrp = False")
            'dtmp2 = dMView.ToTable.Select("assessgroupunkid > 0 AND result <> 0 ")
            dtmp2 = dMView.ToTable.Select("assessgroupunkid > 0")
            If dtmp1.Length <> dtmp2.Length Then
                If blnFlag = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, you cannot do Save Commit Operation as you have not assessed all item in assessment group."), Me)
                    Return False
                    'Shani(30-MAR-2016) -- Start
                Else
                    Return False
                    'Shani(30-MAR-2016) -- End
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    'Shani(23-FEB-2017) -- Start
    'Enhancement - Add new custom item saving setting requested by (aga khan)
    Private Function isAll_Assessed_CustomItem(Optional ByVal blnFlag As Boolean = True) As Boolean
        Dim dsCustomItem As DataSet
        Try
            dsCustomItem = (New clsassess_custom_items).GetCutomItemList(CInt(cboPeriod.SelectedValue), menAssess)
            If dsCustomItem IsNot Nothing AndAlso dsCustomItem.Tables(0).Rows.Count > 0 Then
                If mdtCustomEvaluation.Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 42, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), Me)
                    Return False
                End If

                Dim ArrCustomItem = mdtCustomEvaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("dispaly_value") <> "" AndAlso x.Field(Of Boolean)("isvoid") = False).Select(Function(x) x.Field(Of Integer)("customitemunkid")).Distinct.ToArray

                Dim ar = dsCustomItem.Tables(0).AsEnumerable().Where(Function(x) Not ArrCustomItem.Contains(x.Field(Of Integer)("customitemunkid")))

                If ar.Count > 0 Then
                    If blnFlag = True Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 43, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), Me)
                        Return False
                    Else
                        Return False
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function
    'Shani(23-FEB-2017) -- End

    Private Function GetOldValue_BSC(ByVal xRowIndex As Integer) As DataRow()
        Dim xTemp() As DataRow = Nothing
        Try
            If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                Select Case CInt(Me.ViewState("iExOrdr"))
                    Case enWeight_Types.WEIGHT_FIELD1
                        xTemp = mdtBSC_Evaluation.Select("empfield1unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield1unkid").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD2
                        xTemp = mdtBSC_Evaluation.Select("empfield2unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield2unkid").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD3
                        xTemp = mdtBSC_Evaluation.Select("empfield3unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield3unkid").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD4
                        xTemp = mdtBSC_Evaluation.Select("empfield4unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield4unkid").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD5
                        xTemp = mdtBSC_Evaluation.Select("empfield5unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield5unkid").ToString & "' AND AUD <> 'D' ")
                End Select
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return xTemp
    End Function

    Private Function GetOldValue_GE(ByVal xGrdRow As DataGridItem) As DataRow()
        Dim xTemp() As DataRow = Nothing
        Try
            If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                xTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdRow.Cells(8).Text) & "' AND competenciesunkid = '" & _
                                                              CInt(xGrdRow.Cells(7).Text) & "' AND AUD <> 'D' ")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return xTemp
    End Function

    'S.SANDEEP |08-JAN-2019| -- START
    Private Function LockEmployee() As Boolean
        Dim objLockEmp As New clsassess_plan_eval_lockunlock
        Try
            Dim mintLockId As Integer = 0
            Dim mblnisLock As Boolean = False
            Dim dtNextLockDate As Date = Nothing
            If objLockEmp.isExist(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), clsassess_plan_eval_lockunlock.enAssementLockType.ASSESSMENT, "", mblnisLock, mintLockId, dtNextLockDate) Then
                If mintLockId > 0 And mblnisLock = True Then Return True
            End If

            If objLockEmp.isValidDate(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), clsassess_plan_eval_lockunlock.enAssementLockType.ASSESSMENT, ConfigParameter._Object._CurrentDateAndTime) = True Then
                Return False
            End If

            objLockEmp._AssessorMasterunkid = 0
            objLockEmp._Assessmodeid = enAssessmentMode.SELF_ASSESSMENT
            objLockEmp._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
            objLockEmp._Audittype = enAuditType.ADD
            objLockEmp._Audituserunkid = Session("UserId")
            objLockEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objLockEmp._Form_Name = mstrModuleName
            objLockEmp._Hostname = Session("HOST_NAME")
            objLockEmp._Ip = Session("IP_ADD")
            objLockEmp._Islock = True
            objLockEmp._Isunlocktenure = False
            objLockEmp._Isweb = True
            objLockEmp._Locktypeid = clsassess_plan_eval_lockunlock.enAssementLockType.ASSESSMENT
            objLockEmp._Lockunlockdatetime = ConfigParameter._Object._CurrentDateAndTime
            objLockEmp._Lockuserunkid = Session("UserId")
            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                objLockEmp._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If
            objLockEmp._Nextlockdatetime = Nothing
            objLockEmp._Periodunkid = CInt(cboPeriod.SelectedValue)
            objLockEmp._Unlockdays = 0
            objLockEmp._Unlockreason = ""
            objLockEmp._Unlockuserunkid = 0
            If objLockEmp.Insert() = False Then
                If objLockEmp._Message.Trim.Length > 0 Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'eZeeMsgBox.Show(objLockEmp._Message, enMsgBoxStyle.Information)
                    DisplayMessage.DisplayMessage(objLockEmp._Message, Me)
                    'Sohail (23 Mar 2019) -- End
                End If
            End If
            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function
    'S.SANDEEP |08-JAN-2019| -- END

#End Region

#Region " Button Event(s) "

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or _
              CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Please set following information [Employee,Period] to view assessment."), Me)
                Exit Sub
            End If

            'S.SANDEEP |16-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
            If CBool(Session("EnableBSCAutomaticRating")) Then
                Dim sMsg As String = String.Empty
                sMsg = objEAnalysisMst.IsValidToStartAssessment(CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)), _
                                                                CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)))
                If sMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(sMsg, Me)
                    BtnSearch.Visible = False : BtnReset.Visible = False
                    btnSave.Visible = False : btnSaveCommit.Visible = False
                    objbtnNext.Visible = False : objbtnBack.Visible = False
                    Exit Sub
                    'S.SANDEEP |27-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : If Period Has Defined Progress, Button's Are Not Made Visible
                Else
                    If BtnSearch.Visible = False Then BtnSearch.Visible = True
                    If BtnReset.Visible = False Then BtnReset.Visible = True
                    If btnSave.Visible = False Then btnSave.Visible = True
                    If btnSaveCommit.Visible = False Then btnSaveCommit.Visible = True
                    If objbtnNext.Visible = False Then objbtnNext.Visible = True
                    If objbtnBack.Visible = False Then objbtnBack.Visible = True
                    'S.SANDEEP |27-NOV-2019| -- END
                End If
            End If
            'S.SANDEEP |16-AUG-2019| -- END

            'S.SANDEEP |08-JAN-2019| -- START
            If menAction <> enAction.EDIT_ONE Then
                Dim objPeriod As New clscommom_period_Tran
                Dim intDaysBefore As Integer = 0 : Dim iEndDate As DateTime = Nothing
                objPeriod._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
                intDaysBefore = objPeriod._LockDaysBefore
                If intDaysBefore > 0 Then
                    iEndDate = objPeriod._End_Date.AddDays(-(intDaysBefore))
                End If
                objPeriod = Nothing
                If iEndDate <> Nothing Then
                    If ConfigParameter._Object._CurrentDateAndTime.Date > iEndDate Then
                        If LockEmployee() Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 49, "Sorry, you cannot do assessment as last date for assessment has been passed.") & Language.getMessage(mstrModuleName, 50, "Please contact your administrator/manager to do futher operation on it."), Me)
                            Exit Sub
                        End If
                    End If
                End If
            End If
            'S.SANDEEP |08-JAN-2019| -- END

            If Is_Already_Assessed() = False Then
                Exit Sub
            End If

            If dsHeaders.Tables(0).Rows.Count > 0 Then
                'S.SANDEEP |18-JAN-2019| -- START
                'objbtnNext.Enabled = True : iHeaderId = -1 : objbtnNext_Click(sender, e)
                objbtnNext.Enabled = True : iHeaderId = -1
                'S.SANDEEP |18-JAN-2019| -- END
                objbtnBack.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            txtInstruction.Text = Session("Assessment_Instructions")
            txtInstruction.Height = Unit.Pixel(430)
            objlblCaption.Text = Language.getMessage(mstrModuleName, 1, "Intructions")
            objpnlInstruction.Visible = True

            objbtnNext.Enabled = False : objbtnBack.Enabled = False
            objlblValue1.Visible = False : objlblValue2.Visible = False
            objlblValue3.Visible = False : objlblValue4.Visible = False

            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objEAnalysisMst._Analysisunkid = mintAssessAnalysisUnkid
                cboEmployee.Enabled = False
                cboPeriod.Enabled = False
                BtnSearch.Enabled = False
                BtnReset.Enabled = False
            End If
            Call GetValue()
            objGoalsTran._AnalysisUnkid = mintAssessAnalysisUnkid
            mdtBSC_Evaluation = objGoalsTran._DataTable

            objCAssessTran._ConsiderWeightAsNumber = CBool(Session("ConsiderItemWeightAsNumber"))
            objCAssessTran._AnalysisUnkid = mintAssessAnalysisUnkid
            mdtGE_Evaluation = objCAssessTran._DataTable

            objCCustomTran._AnalysisUnkid = mintAssessAnalysisUnkid
            objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)
            'S.SANDEEP [29 DEC 2015] -- START

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'objCCustomTran._AllowCustomItemInPlanning = CBool(Session("IncludeCustomItemInPlanning"))
            objCCustomTran._IsCompanyNeedReviewer = CBool(Session("IsCompanyNeedReviewer"))
            'Shani (26-Sep-2016) -- End


            objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
            'S.SANDEEP [29 DEC 2015] -- END
            mdtCustomEvaluation = objCCustomTran._DataTable
            If menAction = enAction.EDIT_ONE Then
                Call Fill_BSC_Evaluation()
                Call Fill_GE_Evaluation()
                Call Fill_Custom_Grid()
                Call BtnSearch_Click(sender, e)
            Else
                objpnlBSC.Visible = False
                objpnlGE.Visible = False
                objpnlCItems.Visible = False
            End If
            dtpAssessdate.SetDate = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub objbtnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnNext.Click
        Try
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                If iHeaderId < dsHeaders.Tables(0).Rows.Count - 1 Then
                    iHeaderId += 1
                    objbtnBack.Enabled = True
                End If
                If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
                    objbtnNext.Enabled = False
                End If
                Call PanelVisibility()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub objbtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnBack.Click
        Try
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                iHeaderId = iHeaderId - 1
                If iHeaderId <= -1 Then
                    objbtnBack.Enabled = False
                    objbtnNext.Enabled = True
                End If
                If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
                    objbtnNext.Enabled = False
                Else
                    objbtnNext.Enabled = True
                End If
                Call PanelVisibility()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnIClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIClose.Click
        Try
            mblnItemAddEdit = False
            popup_CItemAddEdit.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnIAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIAdd.Click
        Try
            If Session("dtCItems") IsNot Nothing Then
                dtCItems = Session("dtCItems")
            End If
            Dim dgv As DataGridItem = Nothing
            For Each xRow As DataRow In dtCItems.Rows
                For i As Integer = 0 To dgv_Citems.Items.Count - 1
                    dgv = dgv_Citems.Items(i)
                    Select Case xRow("itemtypeid")
                        Case clsassess_custom_items.enCustomType.FREE_TEXT
                            If dgv.FindControl("txt" & xRow("customitemunkid")) IsNot Nothing Then
                                Dim xtxt As TextBox = CType(dgv.FindControl("txt" & xRow("customitemunkid")), TextBox)
                                xRow.Item("custom_value") = xtxt.Text
                                Exit For
                            End If
                        Case clsassess_custom_items.enCustomType.SELECTION
                            If dgv.FindControl("cbo" & xRow("customitemunkid")) IsNot Nothing Then
                                Dim xCbo As DropDownList = CType(dgv.FindControl("cbo" & xRow("customitemunkid")), DropDownList)
                                If xCbo.SelectedValue > 0 Then
                                    xRow.Item("custom_value") = xCbo.SelectedValue
                                    xRow.Item("selectedid") = xCbo.SelectedValue
                                    Exit For
                                End If
                            End If
                        Case clsassess_custom_items.enCustomType.DATE_SELECTION
                            If dgv.FindControl("dtp" & xRow("customitemunkid")) IsNot Nothing Then
                                If CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).IsNull = False Then
                                    xRow.Item("custom_value") = CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate
                                    xRow.Item("ddate") = CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate.Date
                                    Exit For 'ddate
                                End If
                            End If
                        Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                            If dgv.FindControl("txtnum" & xRow("customitemunkid")) IsNot Nothing Then
                                Dim xtxt As TextBox = CType(dgv.FindControl("txtnum" & xRow("customitemunkid").ToString()), TextBox)
                                xRow.Item("custom_value") = xtxt.Text
                                Exit For
                            End If
                    End Select
                Next
            Next
            Dim dtmp() As DataRow = Nothing
            If dtCItems IsNot Nothing Then
                dtCItems.AcceptChanges()

                'Shani(24-Feb-2016) -- Start
                'dtmp = dtCItems.Select("custom_value <> ''")
                'If dtmp.Length <= 0 Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please enter value for atleast one custom item in order to add."), Me)
                '    Exit Sub
                'End If

                dtmp = dtCItems.Select("custom_value = '' AND rOnly = FALSE ")
                Dim strMsg As String = ""

                If dtmp.Length > 0 Then
                    For Each dRow As DataRow In dtmp
                        strMsg &= " \n * " & dRow.Item("custom_item").ToString
                    Next
                End If

                If strMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage("frmAddCustomValue", 3, "Sorry, Please enter all mandatory information mentioned below in order to save.") & strMsg, Me)
                    Exit Sub
                End If

                'Shani(24-Feb-2016) -- End
                dtmp = dtCItems.Select("")
            End If
            If dtmp.Length > 0 Then
                If CInt(Me.ViewState("RowIndex")) <= -1 Then
                    Dim iGUID As String = ""
                    'If mstriEditingGUID.Trim.Length > 0 Then
                    '    iGUID = mstriEditingGUID
                    'Else
                    iGUID = Guid.NewGuid.ToString
                    'End If
                    For iR As Integer = 0 To dtmp.Length - 1
                        Dim dRow As DataRow = mdtCustomEvaluation.NewRow
                        dRow.Item("customanalysistranguid") = iGUID
                        dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                        dRow.Item("customitemunkid") = dtmp(iR).Item("customitemunkid")
                        dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                        Select Case CInt(dtmp(iR).Item("itemtypeid"))
                            Case clsassess_custom_items.enCustomType.FREE_TEXT
                                dRow.Item("custom_value") = dtmp(iR).Item("custom_value")
                            Case clsassess_custom_items.enCustomType.SELECTION
                                dRow.Item("custom_value") = dtmp(iR).Item("selectedid")
                            Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                If dtmp(iR).Item("ddate").ToString.Trim.Length > 0 Then
                                    dRow.Item("custom_value") = eZeeDate.convertDate(dtmp(iR).Item("ddate"))
                                Else
                                    dRow.Item("custom_value") = ""
                                End If
                            Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                dRow.Item("custom_value") = dtmp(iR).Item("custom_value")
                        End Select
                        dRow.Item("custom_header") = Me.ViewState("Header")
                        dRow.Item("custom_item") = dtmp(iR).Item("custom_item")
                        dRow.Item("dispaly_value") = dtmp(iR).Item("custom_value")
                        dRow.Item("AUD") = "A"
                        dRow.Item("itemtypeid") = dtmp(iR).Item("itemtypeid")
                        dRow.Item("selectionmodeid") = dtmp(iR).Item("selectionmodeid")
                        'S.SANDEEP |25-MAR-2019| -- START
                        dRow.Item("Header_Id") = dtmp(iR).Item("customheaderunkid")
                        'S.SANDEEP |25-MAR-2019| -- END
                        mdtCustomEvaluation.Rows.Add(dRow)
                    Next
                Else
                    If dtmp.Length > 0 Then
                        For iEdit As Integer = 0 To dtmp.Length - 1
                            Dim xRow() As DataRow = mdtCustomEvaluation.Select("customitemunkid = '" & dtmp(iEdit).Item("customitemunkid") & "' AND customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D'")
                            If xRow.Length > 0 Then
                                xRow(0).Item("customanalysistranguid") = xRow(0).Item("customanalysistranguid")
                                xRow(0).Item("analysisunkid") = xRow(0).Item("analysisunkid")
                                xRow(0).Item("customitemunkid") = dtmp(iEdit).Item("customitemunkid")
                                xRow(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
                                    Case clsassess_custom_items.enCustomType.FREE_TEXT
                                        xRow(0).Item("custom_value") = dtmp(iEdit).Item("custom_value")
                                    Case clsassess_custom_items.enCustomType.SELECTION
                                        xRow(0).Item("custom_value") = dtmp(iEdit).Item("selectedid")
                                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                        If dtmp(iEdit).Item("ddate").ToString.Trim.Length > 0 Then
                                            xRow(0).Item("custom_value") = eZeeDate.convertDate(dtmp(iEdit).Item("ddate"))
                                        Else
                                            xRow(0).Item("custom_value") = ""
                                        End If
                                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                        xRow(0).Item("custom_value") = dtmp(iEdit).Item("custom_value")
                                End Select
                                xRow(0).Item("custom_header") = Me.ViewState("Header")
                                xRow(0).Item("custom_item") = dtmp(iEdit).Item("custom_item")
                                xRow(0).Item("dispaly_value") = dtmp(iEdit).Item("custom_value")
                                'S.SANDEEP [31 DEC 2015] -- START
                                'xRow(0).Item("AUD") = "U"
                                If xRow(0).Item("analysisunkid") <= 0 Then
                                    xRow(0).Item("AUD") = "A"
                                Else
                                    xRow(0).Item("AUD") = "U"
                                End If
                                'S.SANDEEP [31 DEC 2015] -- END
                                xRow(0).Item("itemtypeid") = dtmp(iEdit).Item("itemtypeid")
                                xRow(0).Item("selectionmodeid") = dtmp(iEdit).Item("selectionmodeid")
                                'S.SANDEEP |25-MAR-2019| -- START
                                xRow(0).Item("Header_Id") = dtmp(iEdit).Item("customheaderunkid")
                                'S.SANDEEP |25-MAR-2019| -- END
                            Else
                                Dim dRow As DataRow = mdtCustomEvaluation.NewRow
                                dRow.Item("customanalysistranguid") = mstriEditingGUID
                                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                                dRow.Item("customitemunkid") = dtmp(iEdit).Item("customitemunkid")
                                dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
                                    Case clsassess_custom_items.enCustomType.FREE_TEXT
                                        dRow.Item("custom_value") = dtmp(iEdit).Item("custom_value")
                                    Case clsassess_custom_items.enCustomType.SELECTION
                                        dRow.Item("custom_value") = dtmp(iEdit).Item("selectedid")
                                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                        If dtmp(iEdit).Item("ddate").ToString.Trim.Length > 0 Then
                                            dRow.Item("custom_value") = eZeeDate.convertDate(dtmp(iEdit).Item("ddate"))
                                        Else
                                            dRow.Item("custom_value") = ""
                                        End If
                                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                        dRow.Item("custom_value") = dtmp(iEdit).Item("custom_value")
                                End Select
                                dRow.Item("custom_header") = Me.ViewState("Header")
                                dRow.Item("custom_item") = dtmp(iEdit).Item("custom_item")
                                dRow.Item("dispaly_value") = dtmp(iEdit).Item("custom_value")
                                dRow.Item("AUD") = "A"
                                dRow.Item("itemtypeid") = dtmp(iEdit).Item("itemtypeid")
                                dRow.Item("selectionmodeid") = dtmp(iEdit).Item("selectionmodeid")
                                'S.SANDEEP |25-MAR-2019| -- START
                                dRow.Item("Header_Id") = dtmp(iEdit).Item("customheaderunkid")
                                'S.SANDEEP |25-MAR-2019| -- END
                                Dim iRow() = mdtCustomEvaluation.Select("customanalysistranguid = '" & mstriEditingGUID & "' ")
                                mdtCustomEvaluation.Rows.InsertAt(dRow, mdtCustomEvaluation.Rows.IndexOf(iRow(iRow.Length - 1)) + 1)
                            End If
                        Next
                    End If
                End If
            End If
            mblnItemAddEdit = False
            popup_CItemAddEdit.Hide()
            Call Fill_Custom_Evaluation_Data()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Try
            If txtMessage.Text.Trim.Length > 0 Then
                If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
                    Dim xRow() As DataRow = Nothing

                    'S.SANDEEP [12 OCT 2016] -- START
                    'ENHANCEMENT : ACB REPORT CHANGES
                    'xRow = mdtCustomEvaluation.Select("customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D'")
                    xRow = mdtCustomEvaluation.Select("customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D' AND isdefaultentry = False")
                    'S.SANDEEP [12 OCT 2016] -- END

                    If xRow.Length > 0 Then
                        For x As Integer = 0 To xRow.Length - 1
                            xRow(x).Item("isvoid") = True
                            xRow(x).Item("voiduserunkid") = Session("UserId")
                            xRow(x).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            xRow(x).Item("voidreason") = txtMessage.Text
                            xRow(x).Item("AUD") = "D"
                        Next
                        Call Fill_Custom_Evaluation_Data()
                    End If
                End If
            Else
                popup_CItemReason.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If dsHeaders.Tables.Count <= 0 Then Exit Sub
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                If Validation() = False Then Exit Sub

                'S.SANDEEP |14-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : {#0003481|ARUTI-567}
                If CBool(Session("MakeEmpAssessCommentsMandatory")) Then
                    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                        If mdtBSC_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 51, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for Objective/Targets. Please provide remark to continue. "), Me)
                            Exit Sub
                        End If
                    End If
                    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                        If mdtGE_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue. "), enMsgBoxStyle.Information)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue. "), Me)
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                    End If
                End If
                'S.SANDEEP |14-MAR-2019| -- END

                Dim dtmp() As DataRow = Nothing
                If mdtCustomEvaluation IsNot Nothing Then
                    If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables(0).Rows.Count > 0 Then
                        dtmp = dsHeaders.Tables(0).Select("Id > 0")
                        'S.SANDEEP [ 18 DEC 2014 ] -- START
                        'Dim dCEval As DataView = mdtCustomEvaluation.DefaultView
                        Dim dCEval As DataView = New DataView(mdtCustomEvaluation, "periodunkid = '" & CInt(cboPeriod.SelectedValue) & "'", "", DataViewRowState.CurrentRows)
                        'S.SANDEEP [ 18 DEC 2014 ] -- END
                        If dtmp.Length <> dCEval.ToTable(True, "custom_header").Rows.Count Then
                            'SHANI (09 APR 2015)-START
                            'Enhancement - 
                            'lblConfirmMessage.Text = Language.getMessage(mstrModuleName, 31, "You have some custom items along with evaluation items, which you have not assessed. Would you like to assess then before saving this evaluation?")

                            'S.SANDEEP [09 OCT 2015] -- START
                            'lblConfirmMessage.Text = Language.getMessage(mstrModuleName, 31, "You have some custom items along with evaluation items, which you have not assessed. Would you like to assess them before saving this evaluation?")

                            'Shani (26-Sep-2016) -- Start
                            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                            'lblConfirmMessage.Text = Language.getMessage(mstrModuleName, 31, "You have some custom items along with evaluation items, which you have not assessed. Do you want to save without assessing them?")
                            lblConfirmMessage.Text = Language.getMessage(mstrModuleName, 38, "You still have some assessment items not evaluated. Do you want to save now and complete them later?")
                            'Shani (26-Sep-2016) -- End



                            'S.SANDEEP [09 OCT 2015] -- END

                            'SHANI (09 APR 2015)--END 

                            'S.SANDEEP [09 OCT 2015] -- START
                            'btnCnfNo.CommandName = ""
                            'popup_CnfYesNo.Show()
                            'Exit Sub

                            btnCnfYes.CommandName = ""
                            popup_CnfYesNo.Show()
                            Exit Sub
                            'S.SANDEEP [09 OCT 2015] -- START

                        End If
                    End If
                End If
                'S.SANDEEP [09 OCT 2015] -- START
                'btnCnfNo.CommandName = ""
                'Call btnCnfNo_Click(sender, e)

                btnCnfYes.CommandName = ""
                Call btnCnfYes_Click(sender, e)
                'S.SANDEEP [09 OCT 2015] -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnSaveCommit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCommit.Click
        Try
            'S.SANDEEP |20-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : ASSESSMENT DATE SET TO FUTURE 
            If dtpAssessdate.GetDate.Date > Now.Date Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 58, "Sorry, you cannot commit assessment for future date. Please set the assessment date as today date."), Me)
                Exit Sub
            End If
            'S.SANDEEP |20-NOV-2019| -- END

            If dsHeaders.Tables.Count <= 0 Then Exit Sub
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                If Validation() = False Then Exit Sub

                'S.SANDEEP |14-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : {#0003481|ARUTI-567}
                If CBool(Session("MakeEmpAssessCommentsMandatory")) Then
                    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                        If mdtBSC_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 51, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for Objective/Targets. Please provide remark to continue. "), Me)
                            Exit Sub
                        End If
                    End If
                    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                        If mdtGE_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue. "), enMsgBoxStyle.Information)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue. "), Me)
                            'Sohail (23 Mar 2019) -- End

                            Exit Sub
                        End If
                    End If
                End If
                'S.SANDEEP |14-MAR-2019| -- END

                Dim dtmp() As DataRow = Nothing

                'Shani(23-FEB-2017) -- Start
                'Enhancement - Add new custom item saving setting requested by (aga khan)
                'If mdtCustomEvaluation IsNot Nothing Then
                '    If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables(0).Rows.Count > 0 Then
                '        dtmp = dsHeaders.Tables(0).Select("Id > 0")
                '        'S.SANDEEP [ 18 DEC 2014 ] -- START
                '        'Dim dCEval As DataView = mdtCustomEvaluation.DefaultView
                '        Dim dCEval As DataView = New DataView(mdtCustomEvaluation, "periodunkid = '" & CInt(cboPeriod.SelectedValue) & "'", "", DataViewRowState.CurrentRows)
                '        'S.SANDEEP [ 18 DEC 2014 ] -- END
                '        If dtmp.Length <> dCEval.ToTable(True, "custom_header").Rows.Count Then
                '            'SHANI (18 Mar 2015)-START
                '            'Enhancement - Allow more than one employee to be replaced in staff requisition.
                '            'lblConfirmMessage.Text = Language.getMessage(mstrModuleName, 31, "You have some custom items along with evaluation items, which you have not assessed. Would you like to assess then before saving this evaluation?")

                '            'Shani (26-Sep-2016) -- Start
                '            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                '            'lblConfirmMessage.Text = Language.getMessage(mstrModuleName, 31, "You have some custom items along with evaluation items, which you have not assessed. Would you like to assess them before saving this evaluation?")
                '            lblConfirmMessage.Text = Language.getMessage(mstrModuleName, 38, "You still have some assessment items not evaluated. Do you want to save now and complete them later?")
                '            'Shani (26-Sep-2016) -- End
                '            'SHANI (18 Mar 2015)--END 
                '            'S.SANDEEP [09 OCT 2015] -- START
                '            'btnCnfNo.CommandName = btnSaveCommit.ID
                '            btnCnfYes.CommandName = btnSaveCommit.ID
                '            'S.SANDEEP [09 OCT 2015] -- END

                '            popup_CnfYesNo.Show()
                '            Exit Sub
                '        End If
                '    End If
                'End If
                'Shani(23-FEB-2017) -- End
                btnSaveCommit.CommandName = btnSaveCommit.ID

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Call btnCnfNo_Click(sender, e)
                Call btnCnfYes_Click(sender, e)
                'Shani(20-Nov-2015) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'S.SANDEEP [09 OCT 2015] -- START
    'Protected Sub btnCnfYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCnfYes.Click
    '    Try
    '        objbtnNext_Click(sender, e)
    '        Exit Sub
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub

    'Protected Sub btnCnfNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCnfNo.Click
    '    Dim blnFlag As Boolean = False
    '    Dim blnIsBSC_Set As Boolean = False
    '    Dim blnIsGE_Set As Boolean = False
    '    Try
    '        If dsHeaders.Tables.Count <= 0 Then Exit Sub
    '        If dsHeaders.Tables(0).Rows.Count > 0 Then
    '            Dim dtmp() As DataRow = Nothing

    '            'SHANI [21 Mar 2015]-START
    '            'Issue : Fixing Issues Sent By Aandrew in PA Testing.
    '            'If mdtBSC_Evaluation IsNot Nothing Then
    '            '    dtmp = dsHeaders.Tables(0).Select("Id=-3")
    '            '    If dtmp.Length > 0 Then
    '            '        blnIsBSC_Set = True
    '            '        dtmp = mdtBSC_Evaluation.Select("AUD <> 'D'")
    '            '        If dtmp.Length <= 0 Then
    '            '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 30, "Sorry, Please assess atleast one item from Balance Score Card Evaluation in order to save."), Me)
    '            '            Exit Sub
    '            '        End If
    '            '    End If
    '            'End If
    '            'If mdtGE_Evaluation IsNot Nothing Then
    '            '    dtmp = dsHeaders.Tables(0).Select("Id=-2")
    '            '    If dtmp.Length > 0 Then
    '            '        blnIsGE_Set = True
    '            '        dtmp = mdtGE_Evaluation.Select("AUD <> 'D'")
    '            '        If dtmp.Length <= 0 Then
    '            '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 34, "Sorry, Please assess atleast one item from Competencies Evaluation in order to save."), Me)
    '            '            Exit Sub
    '            '        End If
    '            '    End If
    '            'End If

    '            Dim blnIsOneAssessed As Boolean = False
    '            If mdtBSC_Evaluation IsNot Nothing Then
    '                dtmp = dsHeaders.Tables(0).Select("Id=-3")
    '                If dtmp.Length > 0 Then
    '                    blnIsBSC_Set = True
    '                    dtmp = mdtBSC_Evaluation.Select("AUD <> 'D'")
    '                    If dtmp.Length > 0 Then
    '                        blnIsOneAssessed = True
    '                    End If
    '                End If
    '            End If

    '            If mdtGE_Evaluation IsNot Nothing Then
    '                dtmp = dsHeaders.Tables(0).Select("Id=-2")
    '                If dtmp.Length > 0 Then
    '                    'S.SANDEEP [04 JUN 2015] -- START
    '                    'blnIsBSC_Set = True
    '                    blnIsGE_Set = True
    '                    'S.SANDEEP [04 JUN 2015] -- END
    '                    dtmp = mdtGE_Evaluation.Select("AUD <> 'D'")
    '                    If dtmp.Length > 0 Then
    '                        blnIsOneAssessed = True
    '                    End If
    '                End If
    '            End If

    '            If blnIsOneAssessed = False Then
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 35, "Sorry, Please assess atleast one item from Balance Score Card Or Competencies Evaluation in order to save."), Me)
    '                Exit Sub
    '            End If
    '            'SHANI [21 Mar 2015]--END 

    '            Call SetValue()
    '            Select Case CType(sender, Button).CommandName.ToUpper
    '                Case btnSaveCommit.ID.ToUpper
    '                    If Session("IsAllowFinalSave") = False Then
    '                        If blnIsBSC_Set = True Then If isAll_Assessed_BSC() = False Then Exit Sub
    '                        If blnIsGE_Set = True Then If isAll_Assessed_GE() = False Then Exit Sub
    '                        Call CnfSaveCommit_buttonYes_Click(New Object, New EventArgs)
    '                        Exit Sub
    '                    ElseIf Session("IsAllowFinalSave") = True Then
    '                        'S.SANDEEP [01 OCT 2015] -- START
    '                        'If isAll_Assessed_BSC(False) = False Then
    '                        '    CnfSaveCommit.Message = Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
    '                        '    CnfSaveCommit.Show()
    '                        '    Exit Sub
    '                        'End If
    '                        'If isAll_Assessed_GE(False) = False Then
    '                        '    CnfSaveCommit.Message = Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
    '                        '    CnfSaveCommit.Show()
    '                        '    Exit Sub
    '                        'End If
    '                        If isAll_Assessed_BSC(False) = True Then
    '                            CnfSaveCommit.Message = Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
    '                            CnfSaveCommit.Show()
    '                            Exit Sub
    '                        End If
    '                        If isAll_Assessed_GE(False) = False Then
    '                            CnfSaveCommit.Message = Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
    '                            CnfSaveCommit.Show()
    '                            Exit Sub
    '                        End If
    '                        'S.SANDEEP [01 OCT 2015] -- END
    '                    End If
    '            End Select

    '            If menAction = enAction.EDIT_ONE Then
    '                blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
    '            Else
    '                blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
    '            End If

    '            If blnFlag = False And objEAnalysisMst._Message <> "" Then
    '                DisplayMessage.DisplayMessage(objEAnalysisMst._Message, Me)
    '                Exit Sub
    '            End If

    '            If blnFlag = True Then
    '                Call ClearForm_Values()
    '                Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_SelfEvaluationList.aspx", False)
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    Finally
    '    End Try
    'End Sub

    Protected Sub btnCnfNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCnfNo.Click
        Try
            objbtnNext_Click(sender, e)
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCnfYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCnfYes.Click
        Dim blnFlag As Boolean = False
        Dim blnIsBSC_Set As Boolean = False
        Dim blnIsGE_Set As Boolean = False
        Try
            If dsHeaders.Tables.Count <= 0 Then Exit Sub
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                Dim dtmp() As DataRow = Nothing

                'SHANI [21 Mar 2015]-START
                'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                'If mdtBSC_Evaluation IsNot Nothing Then
                '    dtmp = dsHeaders.Tables(0).Select("Id=-3")
                '    If dtmp.Length > 0 Then
                '        blnIsBSC_Set = True
                '        dtmp = mdtBSC_Evaluation.Select("AUD <> 'D'")
                '        If dtmp.Length <= 0 Then
                '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 30, "Sorry, Please assess atleast one item from Balance Score Card Evaluation in order to save."), Me)
                '            Exit Sub
                '        End If
                '    End If
                'End If
                'If mdtGE_Evaluation IsNot Nothing Then
                '    dtmp = dsHeaders.Tables(0).Select("Id=-2")
                '    If dtmp.Length > 0 Then
                '        blnIsGE_Set = True
                '        dtmp = mdtGE_Evaluation.Select("AUD <> 'D'")
                '        If dtmp.Length <= 0 Then
                '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 34, "Sorry, Please assess atleast one item from Competencies Evaluation in order to save."), Me)
                '            Exit Sub
                '        End If
                '    End If
                'End If

                Dim blnIsOneAssessed As Boolean = False
                If mdtBSC_Evaluation IsNot Nothing Then
                    dtmp = dsHeaders.Tables(0).Select("Id=-3")
                    If dtmp.Length > 0 Then
                        blnIsBSC_Set = True
                        dtmp = mdtBSC_Evaluation.Select("AUD <> 'D'")
                        If dtmp.Length > 0 Then
                            blnIsOneAssessed = True
                        End If
                    End If
                End If

                If mdtGE_Evaluation IsNot Nothing Then
                    dtmp = dsHeaders.Tables(0).Select("Id=-2")
                    If dtmp.Length > 0 Then
                        'S.SANDEEP [04 JUN 2015] -- START
                        'blnIsBSC_Set = True
                        blnIsGE_Set = True
                        'S.SANDEEP [04 JUN 2015] -- END
                        dtmp = mdtGE_Evaluation.Select("AUD <> 'D'")
                        If dtmp.Length > 0 Then
                            blnIsOneAssessed = True
                        End If
                    End If
                End If

                If blnIsOneAssessed = False Then
                    'S.SANDEEP [01-OCT-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
                    'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 35, "Sorry, Please assess atleast one item from Balance Score Card Or Competencies Evaluation in order to save."), Me)
                    If blnIsGE_Set = True AndAlso blnIsBSC_Set = True Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 35, "Sorry, Please assess atleast one item from Balance Score Card Or Competencies Evaluation in order to save."), Me)
                    ElseIf blnIsBSC_Set = True Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 46, "Sorry, Please assess atleast one item from Balance Score Card Evaluation in order to save."), Me)
                    ElseIf blnIsGE_Set = True Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 47, "Sorry, Please assess atleast one item from Competencies Evaluation in order to save."), Me)
                    End If
                    'S.SANDEEP [01-OCT-2018] -- END
                    Exit Sub
                End If
                'SHANI [21 Mar 2015]--END 

                Call SetValue()
                Select Case CType(sender, Button).CommandName.ToUpper
                    Case btnSaveCommit.ID.ToUpper
                        If Session("IsAllowFinalSave") = False Then
                            If blnIsBSC_Set = True Then If isAll_Assessed_BSC() = False Then Exit Sub
                            If blnIsGE_Set = True Then If isAll_Assessed_GE() = False Then Exit Sub
                            'Call CnfSaveCommit_buttonYes_Click(New Object, New EventArgs)
                            'Exit Sub
                        ElseIf Session("IsAllowFinalSave") = True Then
                            'S.SANDEEP [01 OCT 2015] -- START
                            'If isAll_Assessed_BSC(False) = False Then
                            '    CnfSaveCommit.Message = Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                            '    CnfSaveCommit.Show()
                            '    Exit Sub
                            'End If
                            'If isAll_Assessed_GE(False) = False Then
                            '    CnfSaveCommit.Message = Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                            '    CnfSaveCommit.Show()
                            '    Exit Sub
                            'End If

                            'blnIsBSC_Set
                            'S.SANDEEP [11-OCT-2018] -- START
                            'If isAll_Assessed_BSC(False) = False Then 'Shani(30-MAR-2016) 
                            '    CnfSaveCommit.Message = Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                            '    CnfSaveCommit.Show()
                            '    Exit Sub
                            'End If
                            'If isAll_Assessed_GE(False) = False Then
                            '    CnfSaveCommit.Message = Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                            '    CnfSaveCommit.Show()
                            '    Exit Sub
                            'End If
                            If blnIsBSC_Set Then
                                If isAll_Assessed_BSC(False) = False Then 'Shani(30-MAR-2016) 
                                    CnfSaveCommit.Message = Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                                    CnfSaveCommit.Show()
                                    Exit Sub
                                End If
                            End If

                            If blnIsGE_Set Then
                                If isAll_Assessed_GE(False) = False Then
                                    CnfSaveCommit.Message = Language.getMessage(mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                                    CnfSaveCommit.Show()
                                    Exit Sub
                                End If
                            End If
                            'S.SANDEEP [11-OCT-2018] -- END

                            '


                            'Shani(30-MAR-2016) -- Start
                            'Call CnfSaveCommit_buttonYes_Click(New Object, New EventArgs)
                            'Exit Sub
                            'Shani(30-MAR-2016) -- End 

                            'S.SANDEEP [01 OCT 2015] -- END
                        End If

                        'Shani(23-FEB-2017) -- Start
                        'Enhancement - Add new custom item saving setting requested by (aga khan)
                        If Session("IsAllowCustomItemFinalSave") = False Then
                            If isAll_Assessed_CustomItem() = False Then Exit Sub
                            Call CnfSaveCommit_buttonYes_Click(New Object, New EventArgs)
                            Exit Sub
                        ElseIf Session("IsAllowFinalSave") = True Then
                            If isAll_Assessed_CustomItem(False) = False Then
                                CnfSaveCommit.Message = Language.getMessage(mstrModuleName, 38, "You still have some assessment items not evaluated. Do you want to save now and complete them later?")
                                CnfSaveCommit.Show()
                                Exit Sub
                            End If
                            Call CnfSaveCommit_buttonYes_Click(New Object, New EventArgs)
                            Exit Sub
                        End If
                        Call CnfSaveCommit_buttonYes_Click(New Object, New EventArgs)
                        Exit Sub
                        'Shani(23-FEB-2017) -- End
                End Select

                'S.SANDEEP [27-APR-2017] -- START
                'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                'If menAction = enAction.EDIT_ONE Then
                '    blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
                'Else
                '    blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
                'End If

                If menAction = enAction.EDIT_ONE Then
                    blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation, Session("ScoringOptionId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("IsUseAgreedScore"), Session("Self_Assign_Competencies"))
                Else
                    'S.SANDEEP |18-JAN-2020| -- START
                    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                    If Is_Already_Assessed() = False Then
                        Exit Sub
                    End If
                    'S.SANDEEP |18-JAN-2020| -- END
                    blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation, Session("ScoringOptionId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("IsUseAgreedScore"), Session("Self_Assign_Competencies"))
                End If
                'S.SANDEEP [27-APR-2017] -- END

                If blnFlag = False And objEAnalysisMst._Message <> "" Then
                    DisplayMessage.DisplayMessage(objEAnalysisMst._Message, Me)
                    Exit Sub
                End If

                If blnFlag = True Then
                    Call ClearForm_Values()
                    'S.SANDEEP |25-MAR-2019| -- START
                    'Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_SelfEvaluationList.aspx", False)

                    'S.SANDEEP |30-MAR-2019| -- START
                    'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 53, "Your assessment has been submitted successfully."), Me, "wPg_SelfEvaluationList.aspx")
                    Select Case CType(sender, Button).CommandName.ToUpper
                        Case btnSaveCommit.ID.ToUpper
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 53, "Your assessment has been submitted successfully."), Me, "wPg_SelfEvaluationList.aspx")
                        Case Else
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 55, "Your assessment has been saved successfully."), Me, "wPg_SelfEvaluationList.aspx")
                    End Select
                    'S.SANDEEP |30-MAR-2019| -- END


                    'S.SANDEEP |25-MAR-2019| -- END
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP [09 OCT 2015] -- END

    Protected Sub CnfSaveCommit_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CnfSaveCommit.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Call SetValue()

            objEAnalysisMst._Iscommitted = True
            objEAnalysisMst._Committeddatetime = ConfigParameter._Object._CurrentDateAndTime

            'S.SANDEEP [27-APR-2017] -- START
            'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
            'If menAction = enAction.EDIT_ONE Then
            '    blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
            'Else
            '    blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
            'End If

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation, Session("ScoringOptionId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("IsUseAgreedScore"), Session("Self_Assign_Competencies"))
            Else
                'S.SANDEEP |18-JAN-2020| -- START
                'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                If Is_Already_Assessed() = False Then
                    Exit Sub
                End If
                'S.SANDEEP |18-JAN-2020| -- END
                blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation, Session("ScoringOptionId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("IsUseAgreedScore"), Session("Self_Assign_Competencies"))
            End If
            'S.SANDEEP [27-APR-2017] -- END

            If blnFlag = False And objEAnalysisMst._Message <> "" Then
                DisplayMessage.DisplayMessage(objEAnalysisMst._Message, Me)
                Exit Sub
            End If

            If blnFlag = True Then
                Call ClearForm_Values()

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEAnalysisMst.Email_Notification(enAssessmentMode.SELF_ASSESSMENT, _
                '                                   CInt(cboEmployee.SelectedValue), _
                '                                   CInt(cboPeriod.SelectedValue), _
                '                                   Session("IsCompanyNeedReviewer"), , , , _
                '                                   enLogin_Mode.DESKTOP, 0)
                objEAnalysisMst.Email_Notification(enAssessmentMode.SELF_ASSESSMENT, _
                                                   CInt(cboEmployee.SelectedValue), _
                                                   CInt(cboPeriod.SelectedValue), _
                                                   Session("IsCompanyNeedReviewer"), Session("Database_Name"), _
                                                   Session("CompanyUnkId"), Session("ArutiSelfServiceURL").ToString(), cboEmployee.SelectedItem.Text, enLogin_Mode.DESKTOP, 0)
                'Shani(20-Nov-2015) -- End


                'S.SANDEEP |25-MAR-2019| -- START
                'Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_SelfEvaluationList.aspx", False)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 53, "Your assessment has been submitted successfully."), Me, "wPg_SelfEvaluationList.aspx")
                'S.SANDEEP |25-MAR-2019| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Call ClearForm_Values()
            Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_SelfEvaluationList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Call ClearForm_Values()
    '    Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_SelfEvaluationList.aspx", False)
    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region

#Region " GridView Event(s) "

    Protected Sub dgvGE_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvGE.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim intCount As Integer = 1
                If CBool(DataBinder.Eval(e.Item.DataItem, "IsPGrp")) = True Then
                    For i = 1 To dgvGE.Columns.Count - 1
                        If dgvGE.Columns(i).Visible Then
                            e.Item.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Item.Cells(0).ColumnSpan = intCount
                    e.Item.Cells(0).CssClass = "MainGroupHeaderStyle"
                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then
                    For i = 1 To dgvGE.Columns.Count - 2
                        If dgvGE.Columns(i).Visible Then
                            e.Item.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Item.Cells(0).ColumnSpan = intCount
                    e.Item.Cells(0).CssClass = "GroupHeaderStylecomp"
                    e.Item.Cells(12).CssClass = "GroupHeaderStylecomp"
                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then
                    e.Item.Cells(0).Text = "&nbsp;" & e.Item.Cells(0).Text
                End If
                If CInt(e.Item.Cells(7).Text) > 0 Then
                    'S.SANDEEP |18-JAN-2020| -- START
                    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                    'If mdtGE_Evaluation.Rows.Count > 0 Then
                    '    Dim dtmp() As DataRow = Nothing
                    '    dtmp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(e.Item.Cells(8).Text) & "' AND competenciesunkid = '" & CInt(e.Item.Cells(7).Text) & "' AND AUD <> 'D' ")
                    '    If dtmp.Length > 0 Then
                    '        CType(e.Item.Cells(3).Controls(1), TextBox).Text = CInt(dtmp(0).Item("result"))
                    '        CType(e.Item.Cells(5).Controls(1), TextBox).Text = dtmp(0).Item("remark")
                    '        If CBool(Session("ConsiderItemWeightAsNumber")) = True Then
                    '            Dim iWgt As Decimal = 0 : Decimal.TryParse(e.Item.Cells(1).Text, iWgt)
                    '            If iWgt <= 0 Then iWgt = 1
                    '            e.Item.Cells(4).Text = iWgt * CInt(dtmp(0).Item("result"))
                    '        Else
                    '            e.Item.Cells(4).Text = 1 * CInt(dtmp(0).Item("result"))
                    '            dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                    '        End If
                    '    End If
                    'End If
                    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                        Dim dtmp() As DataRow = Nothing
                        dtmp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(e.Item.Cells(8).Text) & "' AND competenciesunkid = '" & CInt(e.Item.Cells(7).Text) & "' AND AUD <> 'D' ")
                        If dtmp IsNot Nothing AndAlso dtmp.Length > 0 Then
                            CType(e.Item.Cells(3).Controls(1), TextBox).Text = CInt(dtmp(0).Item("result"))
                            CType(e.Item.Cells(5).Controls(1), TextBox).Text = dtmp(0).Item("remark")
                            If CBool(Session("ConsiderItemWeightAsNumber")) = True Then
                                Dim iWgt As Decimal = 0 : Decimal.TryParse(e.Item.Cells(1).Text, iWgt)
                                If iWgt <= 0 Then iWgt = 1
                                e.Item.Cells(4).Text = iWgt * CInt(dtmp(0).Item("result"))
                            Else
                                e.Item.Cells(4).Text = 1 * CInt(dtmp(0).Item("result"))
                                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                            End If
                        End If
                    End If
                    'S.SANDEEP |18-JAN-2020| -- END
                End If
                If IsNumeric(e.Item.Cells(1).Text) Then
                    iWeightTotal = iWeightTotal + CDec(e.Item.Cells(1).Text)
                End If

                'S.SANDEEP |20-SEP-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#0004155}
                Dim hf As HiddenField = CType(e.Item.Cells(2).FindControl("dgcolhGEScoreId"), HiddenField)
                If hf.Value > 0 Then
                    Dim lk As LinkButton = CType(e.Item.Cells(2).FindControl("dgcolhGEScore"), LinkButton)
                    Dim dsScore_Guide As New DataSet
                    Dim objScaleMaster As New clsAssessment_Scale
                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), hf.Value)
                    objScaleMaster = Nothing
                    Dim strValue As String = String.Join(vbCrLf, dsScore_Guide.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Decimal)("scale").ToString() & " - " & x.Field(Of String)("description")).ToArray())
                    lk.ToolTip = strValue
                End If
                'S.SANDEEP |20-SEP-2019| -- END

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgvBSC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvBSC.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Call SetDateFormat()
                'Pinkal (16-Apr-2016) -- End
                If CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then
                    For i = xVal To dgvBSC.Columns.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next
                    e.Item.Cells(xVal - 1).ColumnSpan = dgvBSC.Columns.Count - 1
                    e.Item.Cells(xVal - 1).CssClass = "GroupHeaderStyleBorderLeft"
                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then
                    e.Item.Cells(xVal - 1).Text = "&nbsp;" & e.Item.Cells(0).Text

                    'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    'CType(e.Item.Cells(14).Controls(1), TextBox).Attributes.Add("onKeypress", "return onlyNumbers(this,event);")
                    ' using New jquery .decimal class in master page
                    If e.Item.Cells(8).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(8).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(8).Text = CDate(e.Item.Cells(8).Text).Date.ToShortDateString
                    End If

                    If e.Item.Cells(9).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(9).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(9).Text = CDate(e.Item.Cells(9).Text).Date.ToShortDateString
                    End If
                    'Pinkal (16-Apr-2016) -- End
                End If
                If CStr(DataBinder.Eval(e.Item.DataItem, "Weight")) = "" Then
                    'S.SANDEEP [ 17 DEC 2014 ] -- START
                    'e.Item.Cells(14).Controls(0).Visible = False : e.Item.Cells(15).Controls(0).Visible = False
                    e.Item.Cells(16).Controls(1).Visible = False : e.Item.Cells(17).Controls(1).Visible = False
                    'S.SANDEEP [ 17 DEC 2014 ] -- END
                ElseIf CStr(DataBinder.Eval(e.Item.DataItem, "Weight")) <> "" Then
                    'S.SANDEEP |18-JAN-2020| -- START
                    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                    'If mdtBSC_Evaluation.Rows.Count > 0 Then
                    '    Dim dtmp() As DataRow = Nothing
                    '    Select Case iExOrdr
                    '        Case enWeight_Types.WEIGHT_FIELD1
                    '            dtmp = mdtBSC_Evaluation.Select("empfield1unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield1unkid").ToString & "' AND AUD <> 'D' ")
                    '        Case enWeight_Types.WEIGHT_FIELD2
                    '            dtmp = mdtBSC_Evaluation.Select("empfield2unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield2unkid").ToString & "' AND AUD <> 'D' ")
                    '        Case enWeight_Types.WEIGHT_FIELD3
                    '            dtmp = mdtBSC_Evaluation.Select("empfield3unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield3unkid").ToString & "' AND AUD <> 'D' ")
                    '        Case enWeight_Types.WEIGHT_FIELD4
                    '            dtmp = mdtBSC_Evaluation.Select("empfield4unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield4unkid").ToString & "' AND AUD <> 'D' ")
                    '        Case enWeight_Types.WEIGHT_FIELD5
                    '            dtmp = mdtBSC_Evaluation.Select("empfield5unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield5unkid").ToString & "' AND AUD <> 'D' ")
                    '    End Select

                    '    If dtmp.Length > 0 Then
                    '        CType(e.Item.Cells(16).Controls(1), TextBox).Text = CDec(dtmp(0).Item("result"))
                    '        CType(e.Item.Cells(17).Controls(1), TextBox).Text = dtmp(0).Item("remark")
                    '    End If
                    '    ' ''
                    '    'If menAction <> enAction.ADD_CONTINUE Then
                    '    '    If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                    '    '        e.Item.Cells(14).Controls(0).Visible = True : e.Item.Cells(15).Controls(0).Visible = True
                    '    '    End If
                    '    'End If
                    'End If
                    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                        Dim dtmp() As DataRow = Nothing
                        Select Case iExOrdr
                            Case enWeight_Types.WEIGHT_FIELD1
                                dtmp = mdtBSC_Evaluation.Select("empfield1unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield1unkid").ToString & "' AND AUD <> 'D' ")
                            Case enWeight_Types.WEIGHT_FIELD2
                                dtmp = mdtBSC_Evaluation.Select("empfield2unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield2unkid").ToString & "' AND AUD <> 'D' ")
                            Case enWeight_Types.WEIGHT_FIELD3
                                dtmp = mdtBSC_Evaluation.Select("empfield3unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield3unkid").ToString & "' AND AUD <> 'D' ")
                            Case enWeight_Types.WEIGHT_FIELD4
                                dtmp = mdtBSC_Evaluation.Select("empfield4unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield4unkid").ToString & "' AND AUD <> 'D' ")
                            Case enWeight_Types.WEIGHT_FIELD5
                                dtmp = mdtBSC_Evaluation.Select("empfield5unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield5unkid").ToString & "' AND AUD <> 'D' ")
                        End Select

                        If dtmp IsNot Nothing AndAlso dtmp.Length > 0 Then
                            CType(e.Item.Cells(16).Controls(1), TextBox).Text = CDec(dtmp(0).Item("result"))
                            CType(e.Item.Cells(17).Controls(1), TextBox).Text = dtmp(0).Item("remark")
                        End If
                    End If
                    'S.SANDEEP |18-JAN-2020| -- END
                End If
                If IsNumeric(e.Item.Cells(15).Text) Then
                    iWeightTotal = iWeightTotal + CDec(e.Item.Cells(15).Text)
                End If
                If CBool(Session("DontAllowToEditScoreGenbySys")) Then
                    CType(e.Item.Cells(16).Controls(1), TextBox).ReadOnly = True
                End If
                'S.SANDEEP |20-SEP-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#0004155}
                Dim hf As HiddenField = CType(e.Item.Cells(14).FindControl("dgcolhBSCScoreId"), HiddenField)
                If hf.Value > 0 Then
                    Dim lk As LinkButton = CType(e.Item.Cells(14).FindControl("dgcolhBSCScore"), LinkButton)
                    Dim dsScore_Guide As New DataSet
                    Dim objScaleMaster As New clsAssessment_Scale
                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), hf.Value)
                    objScaleMaster = Nothing
                    Dim strValue As String = String.Join(vbCrLf, dsScore_Guide.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Decimal)("scale").ToString() & " - " & x.Field(Of String)("description")).ToArray())
                    lk.ToolTip = strValue
                End If
                'S.SANDEEP |20-SEP-2019| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvGE_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvGE.ItemCommand
        Try
            If menAction = enAction.EDIT_ONE Then
                Dim dtmp() As DataRow = Nothing
                dtmp = mdtGE_Evaluation.Select("assessgroupunkid = '" & dtGE_TabularGrid.Rows(e.Item.ItemIndex).Item("assessgroupunkid").ToString & "' AND competenciesunkid = '" & dtGE_TabularGrid.Rows(e.Item.ItemIndex).Item("competenciesunkid").ToString & "' AND AUD <> 'D' ")
                If dtmp.Length > 0 Then
                    If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                        If Session("ConsiderItemWeightAsNumber") Then
                            e.Item.Cells(5).Text = CDbl(e.Item.Cells(2).Text) * CInt(dtmp(0).Item("result"))
                        Else
                            e.Item.Cells(5).Text = 1 * CInt(dtmp(0).Item("result"))
                            dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                        End If
                    ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                        If Session("ConsiderItemWeightAsNumber") Then
                            e.Item.Cells(5).Text = CDbl(e.Item.Cells(2).Text) * CInt(e.Item.Cells(4).Text)
                        Else
                            e.Item.Cells(5).Text = 1 * CInt(e.Item.Cells(4).Text)
                            dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                        End If

                        If Session("ConsiderItemWeightAsNumber") Then
                            e.Item.Cells(8).Text = CDbl(e.Item.Cells(2).Text) * CInt(dtmp(0).Item("result"))
                        Else
                            e.Item.Cells(8).Text = 1 * CInt(dtmp(0).Item("result"))
                            dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                        End If
                    ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                        If Session("ConsiderItemWeightAsNumber") Then
                            e.Item.Cells(5).Text = CDbl(e.Item.Cells(2).Text) * CInt(e.Item.Cells(4).Text)
                        Else
                            e.Item.Cells(5).Text = 1 * CInt(e.Item.Cells(4).Text)
                            dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                        End If

                        If Session("ConsiderItemWeightAsNumber") Then
                            e.Item.Cells(8).Text = CDbl(e.Item.Cells(2).Text) * CInt(e.Item.Cells(7).Text)
                        Else
                            e.Item.Cells(8).Text = 1 * CInt(e.Item.Cells(7).Text)
                            dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                        End If

                        If Session("ConsiderItemWeightAsNumber") Then
                            e.Item.Cells(11).Text = CDbl(e.Item.Cells(2).Text) * CInt(dtmp(0).Item("result"))
                        Else
                            e.Item.Cells(11).Text = 1 * CInt(dtmp(0).Item("result"))
                            dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                        End If
                    End If
                End If
            ElseIf menAction <> enAction.EDIT_ONE Then
                If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
                    e.Item.Cells(4).Text = 0 : e.Item.Cells(6).Text = ""
                ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                    If Session("ConsiderItemWeightAsNumber") Then
                        e.Item.Cells(5).Text = CDbl(IIf(e.Item.Cells(2).Text = "", 1, e.Item.Cells(2).Text)) * CInt(IIf(e.Item.Cells(4).Text = "", 1, e.Item.Cells(4).Text))
                    Else
                        e.Item.Cells(5).Text = 1 * CInt(IIf(e.Item.Cells(4).Text = "", 1, e.Item.Cells(4).Text))
                    End If
                    e.Item.Cells(7).Text = 0 : e.Item.Cells(9).Text = ""
                ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                    If Session("ConsiderItemWeightAsNumber") Then
                        e.Item.Cells(5).Text = CDbl(IIf(e.Item.Cells(2).Text = "", 1, e.Item.Cells(2).Text)) * CInt(IIf(e.Item.Cells(4).Text = "", 1, e.Item.Cells(4).Text))
                    Else
                        e.Item.Cells(5).Text = 1 * CInt(IIf(e.Item.Cells(4).Text = "", 1, e.Item.Cells(4).Text))
                    End If

                    If Session("ConsiderItemWeightAsNumber") Then
                        e.Item.Cells(8).Text = CDbl(IIf(e.Item.Cells(2).Text = "", 1, e.Item.Cells(2).Text)) * CInt(IIf(e.Item.Cells(7).Text = "", 1, e.Item.Cells(7).Text))
                    Else
                        e.Item.Cells(8).Text = 1 * CInt(IIf(e.Item.Cells(7).Text = "", 1, e.Item.Cells(7).Text))
                    End If

                    e.Item.Cells(10).Text = 0 : e.Item.Cells(12).Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvItems.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim imgAdd As New ImageButton()
                imgAdd.ID = "imgAdd"
                imgAdd.Attributes.Add("Class", "objAddBtn")
                imgAdd.CommandName = "objAdd"
                imgAdd.ImageUrl = "~/images/add_16.png"
                imgAdd.ToolTip = "New"
                AddHandler imgAdd.Click, AddressOf imgAdd_Click
                e.Row.Cells(0).Controls.Add(imgAdd)

                Dim imgEdit As New ImageButton()
                imgEdit.ID = "imgEdit"
                imgEdit.Attributes.Add("Class", "objAddBtn")
                imgEdit.CommandName = "objEdit"
                imgEdit.ImageUrl = "~/images/Edit.png"
                imgEdit.ToolTip = "Edit"
                AddHandler imgEdit.Click, AddressOf imgEdit_Click
                e.Row.Cells(1).Controls.Add(imgEdit)

                Dim imgDelete As New ImageButton()
                imgDelete.ID = "imgDelete"
                imgDelete.Attributes.Add("Class", "objAddBtn")
                imgDelete.CommandName = "objDelete"
                imgDelete.ImageUrl = "~/images/remove.png"
                imgDelete.ToolTip = "Delete"
                AddHandler imgDelete.Click, AddressOf imgDelete_Click
                e.Row.Cells(2).Controls.Add(imgDelete)
                'S.SANDEEP |18-JAN-2020| -- START
                'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                'If dtCustomTabularGrid.Rows(e.Row.RowIndex)("analysisunkid") <= 0 AndAlso mintAssessAnalysisUnkid <= 0 Then
                'ElseIf dtCustomTabularGrid.Rows(e.Row.RowIndex)("analysisunkid") <> mintAssessAnalysisUnkid Then
                '    e.Row.Cells(2).Controls.Remove(imgDelete)
                '    e.Row.Cells(1).Controls.Remove(imgEdit)
                'End If
                If dtCustomTabularGrid IsNot Nothing AndAlso dtCustomTabularGrid.Rows(e.Row.RowIndex)("analysisunkid") <= 0 AndAlso mintAssessAnalysisUnkid <= 0 Then

                ElseIf dtCustomTabularGrid IsNot Nothing AndAlso dtCustomTabularGrid.Rows(e.Row.RowIndex)("analysisunkid") <> mintAssessAnalysisUnkid Then
                    e.Row.Cells(2).Controls.Remove(imgDelete)
                    e.Row.Cells(1).Controls.Remove(imgEdit)
                End If
                'S.SANDEEP |18-JAN-2020| -- END


                'S.SANDEEP |25-MAR-2019| -- START
                If Session("CompanyGroupName") = "NMB PLC" Then
                    If dtCustomTabularGrid IsNot Nothing AndAlso dtCustomTabularGrid.Rows(e.Row.RowIndex)("viewmodeid") <> 0 Then
                        e.Row.Cells(0).Controls.Remove(imgAdd)
                        e.Row.Cells(1).Controls.Remove(imgEdit)
                        e.Row.Cells(2).Controls.Remove(imgDelete)
                    End If
                End If
                'S.SANDEEP |25-MAR-2019| -- END
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Shani (31-Aug-2016) -- Start
    'Enhancement - Change Competencies List Design Given by Andrew
    Protected Sub link_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            txtData.Text = ""
            Dim lnkWhatIsThis As LinkButton = CType(sender, LinkButton)
            Dim xRow As DataGridItem = CType(lnkWhatIsThis.NamingContainer, DataGridItem)
            If CBool(xRow.Cells(9).Text) = True Then
                Dim objCOMaster As New clsCommon_Master
                objCOMaster._Masterunkid = CInt(xRow.Cells(11).Text)
                If objCOMaster._Description <> "" Then
                    txtData.Text = objCOMaster._Description
                    popup_ComInfo.Show()
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
                End If
                objCOMaster = Nothing
            ElseIf CBool(xRow.Cells(9).Text) = False Then
                Dim objCPMsater As New clsassess_competencies_master
                objCPMsater._Competenciesunkid = CInt(xRow.Cells(7).Text)
                If objCPMsater._Description <> "" Then
                    txtData.Text = objCPMsater._Description
                    popup_ComInfo.Show()
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
                End If
                objCPMsater = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Shani (31-Aug-2016) -- End

#End Region

#Region " Control Event(s) "

    Protected Sub dgv_Citems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgv_Citems.ItemDataBound
        Try
            dtCItems = Session("dtCItems")
            If e.Item.ItemIndex > -1 Then
                If CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.FREE_TEXT Then
                    Dim txt As New TextBox
                    txt.ID = "txt" & e.Item.Cells(4).Text
                    txt.TextMode = TextBoxMode.MultiLine
                    'S.SANDEEP [25-JAN-2017] -- START
                    'ISSUE/ENHANCEMENT : ISSUE RELATED TO HEIGHT OF TEXTBOX {AKF Self-services In PA Custom Items}
                    txt.Rows = 7
                    'S.SANDEEP [25-JAN-2017] -- END
                    txt.Style.Add("resize", "none")
                    'SHANI [21 Mar 2015]-START
                    'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                    'txt.Width = Unit.Percentage(97)
                    txt.Width = Unit.Percentage(100)
                    txt.CssClass = "removeTextcss"
                    'SHANI [21 Mar 2015]--END 
                    If CBool(e.Item.Cells(3).Text) Then
                        txt.ReadOnly = True
                    End If
                    'S.SANDEEP [12 OCT 2016] -- START
                    'If CInt(Me.ViewState("RowIndex")) > -1 Then
                    '    txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
                    'End If
                    If mblnIsMatchCompetencyStructure Then
                        txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
                    ElseIf CInt(Me.ViewState("RowIndex")) > -1 Then
                        txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
                    End If
                    If CBool(e.Item.Cells(6).Text) Then
                        txt.ReadOnly = True
                    End If
                    'S.SANDEEP [12 OCT 2016] -- END


                    e.Item.Cells(1).Controls.Add(txt)
                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                    Dim dtp As Control
                    dtp = LoadControl("~/Controls/DateCtrl.ascx")
                    dtp.ID = "dtp" & e.Item.Cells(4).Text
                    'SHANI [21 Mar 2015]-START
                    'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                    'CType(dtp, Controls_DateCtrl).AutoPostBack = False
                    CType(dtp, Controls_DateCtrl).AutoPostBack = True
                    'SHANI [21 Mar 2015]--END 
                    If CBool(e.Item.Cells(3).Text) Then
                        CType(dtp, Controls_DateCtrl).Enabled = False
                    End If

                    If CInt(Me.ViewState("RowIndex")) > -1 Then
                        If dtCItems.Rows(e.Item.ItemIndex)("custom_value").ToString().Trim.Length > 0 Then
                            CType(dtp, Controls_DateCtrl).SetDate = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
                        End If
                    End If

                    'SHANI [21 Mar 2015]-START
                    'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                    AddHandler CType(dtp, Controls_DateCtrl).TextChanged, AddressOf dtpCustomItem_TextChanged
                    'SHANI [21 Mar 2015]--END 
                    e.Item.Cells(1).Controls.Add(dtp)
                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.SELECTION Then
                    Dim cbo As New DropDownList
                    'S.SANDEEP |16-AUG-2019| -- START
                    RemoveHandler cbo.DataBound, AddressOf cbo_DataBound
                    AddHandler cbo.DataBound, AddressOf cbo_DataBound
                    'S.SANDEEP |16-AUG-2019| -- END
                    cbo.ID = "cbo" & e.Item.Cells(4).Text
                    'SHANI [01 FEB 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    cbo.Width = Unit.Pixel(250)
                    'SHANI [01 FEB 2015]--END 
                    cbo.Height = Unit.Pixel(20)
                    If CBool(e.Item.Cells(3).Text) Then
                        cbo.Enabled = False
                    End If
                    Select Case CInt(e.Item.Cells(5).Text)
                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                            'S.SANDEEP |08-JAN-2019| -- START
                            'Dim objCMaster As New clsCommon_Master
                            'Dim dsList As New DataSet
                            'dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                            'With cbo
                            '    .DataValueField = "masterunkid"
                            '    .DataTextField = "name"
                            '    .DataSource = dsList.Tables(0)
                            '    .ToolTip = "name"
                            '    .SelectedValue = 0
                            '    .DataBind()
                            'End With
                            'objCMaster = Nothing

                            Dim dsList As New DataSet
                            If CBool(IIf(e.Item.Cells(7).Text = "&nbsp;", 0, e.Item.Cells(7).Text)) = False Then
                                'If CBool(e.Item.Cells(7).Text) = False Then
                                Dim objCMaster As New clsCommon_Master
                                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                objCMaster = Nothing
                            Else
                                Dim objEvalCItem As New clsevaluation_analysis_master
                                dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(CInt(cboEmployee.SelectedValue))
                                objEvalCItem = Nothing
                            End If

                            With cbo
                                .DataValueField = "masterunkid"
                                .DataTextField = "name"
                                .DataSource = dsList.Tables(0)
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With
                            'S.SANDEEP |08-JAN-2019| -- END



                            'S.SANDEEP [06-NOV-2017] -- START
                        Case clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES

                            'S.SANDEEP |08-JAN-2019| -- START
                            'Dim dtab As DataTable = Nothing
                            'Dim objCMaster As New clsCommon_Master
                            'Dim dsList As New DataSet
                            'dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")

                            'If CInt(e.Item.Cells(5).Text) = clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES Then
                            '    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,1)", "", DataViewRowState.CurrentRows).ToTable
                            'ElseIf CInt(e.Item.Cells(5).Text) = clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                            '    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,2)", "", DataViewRowState.CurrentRows).ToTable
                            'End If
                            'With cbo
                            '    .DataValueField = "masterunkid"
                            '    .DataTextField = "name"
                            '    .DataSource = dtab
                            '    .ToolTip = "name"
                            '    .SelectedValue = 0
                            '    .DataBind()
                            'End With
                            'objCMaster = Nothing

                            Dim dtab As DataTable = Nothing
                            Dim dsList As New DataSet
                            If CBool(IIf(e.Item.Cells(7).Text = "&nbsp;", 0, e.Item.Cells(7).Text)) = False Then
                                'If CBool(e.Item.Cells(7).Text) = False Then
                                Dim objCMaster As New clsCommon_Master
                                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                objCMaster = Nothing
                            Else
                                Dim objEvalCItem As New clsevaluation_analysis_master
                                dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(CInt(cboEmployee.SelectedValue))
                                objEvalCItem = Nothing
                            End If

                            If CInt(e.Item.Cells(5).Text) = clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES Then
                                dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,1)", "", DataViewRowState.CurrentRows).ToTable
                            ElseIf CInt(e.Item.Cells(5).Text) = clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                                dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,2)", "", DataViewRowState.CurrentRows).ToTable
                            End If
                            With cbo
                                .DataValueField = "masterunkid"
                                .DataTextField = "name"
                                .DataSource = dtab
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With
                            'S.SANDEEP |08-JAN-2019| -- END


                            'S.SANDEEP [06-NOV-2017] -- END

                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                            Dim objCompetency As New clsassess_competencies_master
                            Dim dsList As New DataSet

                            'Shani(20-Nov-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'dsList = objCompetency.getAssigned_Competencies_List(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), True)
                            dsList = objCompetency.getAssigned_Competencies_List(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), _
                                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), True)
                            'Shani(20-Nov-2015) -- End

                            With cbo
                                .DataValueField = "Id"
                                .DataTextField = "Name"
                                .DataSource = dsList.Tables(0)
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With
                            objCompetency = Nothing
                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                            Dim objEmpField1 As New clsassess_empfield1_master
                            Dim dsList As New DataSet
                            dsList = objEmpField1.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", True, True)
                            With cbo
                                .DataValueField = "Id"
                                .DataTextField = "Name"
                                .DataSource = dsList.Tables(0)
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With
                            objEmpField1 = Nothing



                            'S.SANDEEP |16-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                        Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM

                            Dim dsList As New DataSet
                            If CBool(IIf(e.Item.Cells(7).Text = "&nbsp;", 0, e.Item.Cells(7).Text)) = False Then
                                Dim objCMaster As New clsCommon_Master
                                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.PERFORMANCE_CUSTOM_ITEM, True, "List")
                                objCMaster = Nothing
                            Else
                                Dim objEvalCItem As New clsevaluation_analysis_master
                                dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(CInt(cboEmployee.SelectedValue))
                                objEvalCItem = Nothing
                            End If

                            With cbo
                                .DataValueField = "masterunkid"
                                .DataTextField = "name"
                                .DataSource = dsList.Tables(0)
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With


                            'S.SANDEEP |16-AUG-2019| -- END




                    End Select
                    If CInt(Me.ViewState("RowIndex")) > -1 Then
                        cbo.SelectedValue = IIf(dtCItems.Rows(e.Item.ItemIndex)("custom_value") = "", 0, dtCItems.Rows(e.Item.ItemIndex)("custom_value"))
                    End If
                    e.Item.Cells(1).Controls.Add(cbo)
                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.NUMERIC_DATA Then
                    Dim txt As New TextBox
                    txt.ID = "txtnum" & e.Item.Cells(4).Text
                    If CBool(e.Item.Cells(3).Text) Then
                        txt.ReadOnly = True
                    End If
                    txt.Style.Add("text-align", "right")
                    txt.Attributes.Add("onKeypress", "return onlyNumbers(this, event);")
                    'SHANI [21 Mar 2015]-START
                    'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                    'txt.Width = Unit.Pixel(245)
                    txt.Width = Unit.Percentage(100)
                    txt.CssClass = "removeTextcss"
                    'SHANI [21 Mar 2015]--END 
                    If CInt(Me.ViewState("RowIndex")) > -1 Then
                        txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
                    End If
                    e.Item.Cells(1).Controls.Add(txt)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub TxtValRemarkGE_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        'SHANI [21 Mar 2015]-START
        'Issue : Fixing Issues Sent By Aandrew in PA Testing.
        Dim bln As Boolean = False
        'SHANI [21 Mar 2015]-START
        'Issue : Fixing Issues Sent By Aandrew in PA Testing.
        'Response.Clear()
        'SHANI [21 Mar 2015]--END
        'SHANI [21 Mar 2015]--END
        Try
            Dim xRow As DataGridItem = CType(CType(sender, TextBox).NamingContainer, DataGridItem)
            'S.SANDEEP [21 JAN 2015] -- START
            Dim iDecWgt As Decimal = 0
            Decimal.TryParse(xRow.Cells(1).Text, iDecWgt)
            mdecItemWeight = iDecWgt
            Me.ViewState("mdecItemWeight") = mdecItemWeight
            'S.SANDEEP [21 JAN 2015] -- END

            Dim txt As TextBox = CType(sender, TextBox)
            If CType(sender, TextBox).ID = "dgcolheremarkGE" Then
                Dim xResult As Decimal
                Decimal.TryParse(CType(xRow.Cells(3).Controls(1), TextBox).Text, xResult)
                'S.SANDEEP [21 JAN 2015] -- START
                'Call Evaluated_Data_GE(xResult, txt.Text, xRow.ItemIndex, xRow)
                Call Evaluated_Data_GE(xResult, txt.Text, xRow.ItemIndex, xRow)
                'S.SANDEEP [21 JAN 2015] -- END
            ElseIf CType(sender, TextBox).ID = "dgcolheselfGE" Then
                Dim iDecVal As Decimal
                Decimal.TryParse(txt.Text, iDecVal)
                If IsNumeric(iDecVal) Then
                    Select Case CInt(Session("ScoringOptionId"))
                        Case enScoringOption.SC_WEIGHTED_BASED
                            'S.SANDEEP [21 JAN 2015] -- START
                            'Dim iDecWgt As Decimal = 0
                            'Decimal.TryParse(xRow.Cells(1).Text, iDecWgt)
                            'S.SANDEEP [21 JAN 2015] -- END
                            If CDec(iDecVal) > iDecWgt Then
                                'S.SANDEEP |08-JAN-2019| -- START
                                'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), Me)
                                'bln = True
                                'txt.Text = "" : Exit Sub
                                If CBool(Session("EnableBSCAutomaticRating")) = False Then
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), Me)
                                    bln = True
                                    txt.Text = "" : Exit Sub
                                End If
                                'S.SANDEEP |08-JAN-2019| -- END
                            Else
                                GoTo iValid
                            End If
                        Case enScoringOption.SC_SCALE_BASED
                            Decimal.TryParse(txt.Text, iDecVal)
                            If IsNumeric(iDecVal) Then
                                If CInt(xRow.Cells(6).Text) > 0 Then    'SCALE MASTER ID
                                    Dim dsScore_Guide As New DataSet
                                    Dim objScaleMaster As New clsAssessment_Scale
                                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(xRow.Cells(6).Text))
                                    objScaleMaster = Nothing

                                    'S.SANDEEP [21 JAN 2015] -- START
                                    mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
                                    Me.ViewState("mdecMaxScale") = mdecMaxScale
                                    'S.SANDEEP [21 JAN 2015] -- END

                                    Dim dtmp() As DataRow = dsScore_Guide.Tables(0).Select("scale = " & CDec(iDecVal))

                                    If dtmp.Length <= 0 Then
                                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Sorry, particular scale is not defined. Please refer score guide for the list of score(s) defined."), Me)
                                        Dim dTemp() As DataRow = Nothing
                                        dTemp = GetOldValue_GE(xRow)
                                        If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
                                            txt.Text = dTemp(0).Item("result")
                                        Else
                                            txt.Text = "" : txt.Focus()
                                        End If
                                        'SHANI [21 Mar 2015]-START
                                        'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                                        bln = True
                                        'SHANI [21 Mar 2015]--END 
                                        Exit Sub
                                    Else
                                        GoTo iValid
                                    End If
                                Else
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 29, "Sorry, no scale is not defined. Please define scale."), Me)
                                    Dim dTemp() As DataRow = Nothing
                                    dTemp = GetOldValue_GE(xRow)
                                    If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
                                        txt.Text = dTemp(0).Item("result")
                                    Else
                                        txt.Text = "" : txt.Focus()
                                    End If
                                    'SHANI [21 Mar 2015]-START
                                    'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                                    bln = True
                                    'SHANI [21 Mar 2015]--END 
                                    Exit Sub
                                End If
                            Else
                                'SHANI [21 Mar 2015]-START
                                'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                                bln = True
                                'SHANI [21 Mar 2015]--END 
                                Exit Sub
                            End If
                    End Select
iValid:             If Validation() = False Then bln = True : Exit Sub 'SHANI [21 Mar 2015]- bln = True
                    If Is_Already_Assessed() = False Then
                        'SHANI [21 Mar 2015]-START
                        'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                        bln = True
                        'SHANI [21 Mar 2015]--END 
                        Exit Sub
                    End If
                    Call Evaluated_Data_GE(iDecVal, CType(xRow.Cells(5).Controls(1), TextBox).Text, xRow.ItemIndex, xRow)
                Else
                    txt.Text = ""
                End If
            End If
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & objpnlGE.ClientID & ");", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Throw New Exception(ex.Message)
        Finally

            'SHANI [21 Mar 2015]-START
            'Issue : Fixing Issues Sent By Aandrew in PA Testing.


            'SHANI [21 Mar 2015]-START
            'Issue : Fixing Issues Sent By Aandrew in PA Testing.
            'If bln Then
            '    CType(sender, TextBox).Focus()
            'Else
            '    Dim dtRow As DataGridItem = CType(CType(sender, TextBox).NamingContainer, DataGridItem)
            '    If CType(sender, TextBox).ID = "dgcolheselfGE" Then
            '        CType(dtRow.FindControl("dgcolheremarkGE"), TextBox).Focus()
            '    ElseIf CType(sender, TextBox).ID = "dgcolheremarkGE" Then
            '        If dgvGE.Items.Count > (dtRow.ItemIndex + 1) Then
            '            For i As Integer = dtRow.ItemIndex + 1 To dgvGE.Items.Count - 1
            '                If CBool(dgvGE.Items(i).Cells(9).Text) = False AndAlso CBool(dgvGE.Items(i).Cells(10).Text) = False Then
            '                    CType(dgvGE.Items(i).FindControl("dgcolheselfGE"), TextBox).Focus()
            '                    Exit For
            '                End If
            '            Next
            '        Else
            '            CType(sender, TextBox).Focus()
            '        End If
            '    End If
            'End If
            'Response.End()
            'SHANI [21 Mar 2015]--END 

            'SHANI [21 Mar 2015]--END 
        End Try
    End Sub

    Public Sub TxtValRemarkBSC_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        'SHANI [21 Mar 2015]-START
        'Issue : Fixing Issues Sent By Aandrew in PA Testing.
        Dim bln As Boolean = False
        'SHANI [21 Mar 2015]--END
        Try
            Dim xRow As DataGridItem = CType(CType(sender, TextBox).NamingContainer, DataGridItem)
            'S.SANDEEP [21 JAN 2015] -- START
            Dim iDecWgt As Decimal = 0
            Decimal.TryParse(xRow.Cells(15).Text, iDecWgt)
            mdecItemWeight = iDecWgt
            Me.ViewState("mdecItemWeight") = mdecItemWeight
            'S.SANDEEP [21 JAN 2015] -- END

            Dim txt As TextBox = CType(sender, TextBox)
            If CType(sender, TextBox).ID = "dgcolheremarkBSC" Then
                Dim xResult As Decimal
                Decimal.TryParse(CType(xRow.Cells(16).Controls(1), TextBox).Text, xResult)
                'S.SANDEEP [21 JAN 2015] -- START
                'Call Evaluated_Data_BSC(xResult, txt.Text, xRow.ItemIndex)
                Call Evaluated_Data_BSC(xResult, txt.Text, xRow.ItemIndex, True)
                'S.SANDEEP [21 JAN 2015] -- END
            ElseIf CType(sender, TextBox).ID = "dgcolheselfBSC" Then
                Dim iDecVal As Decimal = 0
                Decimal.TryParse(txt.Text, iDecVal)
                If IsNumeric(iDecVal) Then
                    Select Case CInt(Session("ScoringOptionId"))
                        Case enScoringOption.SC_WEIGHTED_BASED
                            'S.SANDEEP [21 JAN 2015] -- START
                            'Dim iDecWgt As Decimal = 0
                            'Decimal.TryParse(xRow.Cells(13).Text, iDecWgt)
                            'S.SANDEEP [21 JAN 2015] -- END
                            If CDec(iDecVal) > iDecWgt Then
                                'S.SANDEEP |08-JAN-2019| -- START
                                'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), Me)
                                ''SHANI [21 Mar 2015]-START
                                ''Issue : Fixing Issues Sent By Aandrew in PA Testing.
                                'bln = True
                                ''SHANI [21 Mar 2015]--END 
                                'txt.Text = "" : Exit Sub
                                If CBool(Session("EnableBSCAutomaticRating")) = False Then
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), Me)
                                    bln = True
                                    txt.Text = "" : Exit Sub
                                End If
                                'S.SANDEEP |08-JAN-2019| -- END
                            Else
                                GoTo iValid
                            End If
                        Case enScoringOption.SC_SCALE_BASED
                            Decimal.TryParse(txt.Text, iDecVal)
                            If IsNumeric(iDecVal) Then
                                If CInt(xRow.Cells(20).Text) > 0 Then
                                    Dim dsScore_Guide As New DataSet
                                    Dim objScaleMaster As New clsAssessment_Scale
                                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(xRow.Cells(20).Text))
                                    objScaleMaster = Nothing

                                    'S.SANDEEP [21 JAN 2015] -- START
                                    mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
                                    Me.ViewState("mdecMaxScale") = mdecMaxScale
                                    'S.SANDEEP [21 JAN 2015] -- END

                                    Dim dtmp() As DataRow = dsScore_Guide.Tables(0).Select("scale = " & CDec(iDecVal))
                                    If dtmp.Length <= 0 Then
                                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Sorry, particular scale is not defined. Please refer score guide for the list of score(s) defined."), Me)
                                        Dim dTemp() As DataRow = Nothing
                                        dTemp = GetOldValue_BSC(xRow.ItemIndex)
                                        If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
                                            txt.Text = dTemp(0).Item("result")
                                        Else
                                            txt.Text = "" : txt.Focus()
                                        End If
                                        'SHANI [21 Mar 2015]-START
                                        'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                                        bln = True
                                        'SHANI [21 Mar 2015]--END 
                                        Exit Sub
                                    Else
                                        GoTo iValid
                                    End If
                                Else
                                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 29, "Sorry, no scale is not defined. Please define scale."), Me)
                                    Dim dTemp() As DataRow = Nothing
                                    dTemp = GetOldValue_BSC(xRow.ItemIndex)
                                    If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
                                        txt.Text = dTemp(0).Item("result")
                                    Else
                                        txt.Text = "" : txt.Focus()
                                    End If
                                    'SHANI [21 Mar 2015]-START
                                    'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                                    bln = True
                                    'SHANI [21 Mar 2015]--END 
                                    Exit Sub
                                End If
                            Else
                                Exit Sub
                            End If
                    End Select
iValid:             If Validation() = False Then Exit Sub
                    If Is_Already_Assessed() = False Then
                        'SHANI [21 Mar 2015]-START
                        'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                        bln = True
                        'SHANI [21 Mar 2015]--END 
                        txt.Text = "" : Exit Sub
                    End If
                    Call Evaluated_Data_BSC(iDecVal, CType(xRow.Cells(17).Controls(1), TextBox).Text, xRow.ItemIndex)
                Else
                    txt.Text = ""
                End If
            End If
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & objpnlBSC.ClientID & ");", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'SHANI [21 Mar 2015]-START
            'Issue : Fixing Issues Sent By Aandrew in PA Testing.

            'SHANI [21 Mar 2015]-START
            'Issue : Fixing Issues Sent By Aandrew in PA Testing.
            'If bln Then
            '    CType(sender, TextBox).Focus()
            'Else
            '    Dim dtRow As DataGridItem = CType(CType(sender, TextBox).NamingContainer, DataGridItem)
            '    If CType(sender, TextBox).ID = "dgcolheselfBSC" Then
            '        CType(dtRow.FindControl("dgcolheremarkBSC"), TextBox).Focus()
            '    ElseIf CType(sender, TextBox).ID = "dgcolheremarkBSC" Then
            '        If dgvBSC.Items.Count > (dtRow.ItemIndex + 1) Then
            '            If CBool(dgvBSC.Items(dtRow.ItemIndex + 1).Cells(16).Text) Then
            '                'For i As Integer = dtRow.ItemIndex + 1 To dgvBSC.Items.Count - 1
            '                '    If CBool(dgvBSC.Items(i).Cells(16).Text) = False Then
            '                '        CType(dgvBSC.Items(i).FindControl("dgcolheselfBSC"), TextBox).Focus()
            '                '        Exit For
            '                '    End If
            '                'Next
            '                CType(dgvBSC.Items(dtRow.ItemIndex + 2).FindControl("dgcolheselfBSC"), TextBox).Focus()
            '            Else
            '                CType(dgvBSC.Items(dtRow.ItemIndex + 1).FindControl("dgcolheselfBSC"), TextBox).Focus()
            '    End If

            '        Else
            '            CType(sender, TextBox).Focus()
            '        End If
            '    End If
            'End If
            'SHANI [21 Mar 2015]--END 

            'SHANI [21 Mar 2015]--END 
        End Try
    End Sub

    Protected Sub dgcolhGEScore_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If CInt(CType(sender, LinkButton).CommandArgument) > 0 Then 'SCALE MASTER ID
                Dim dsScore_Guide As New DataSet
                Dim objScaleMaster As New clsAssessment_Scale
                dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(CType(sender, LinkButton).CommandArgument))
                objScaleMaster = Nothing
                dgvGEScoreGuide.DataSource = dsScore_Guide.Tables(0)
                dgvGEScoreGuide.DataBind()
                popup_ViewGuideGE.Show()
                'S.SANDEEP [ 15 DEC 2014 ] -- START
                iWeightTotal = 0 'S.SANDEEP [ 17 DEC 2014 ] -- START -- END
                dgvGE.DataSource = dtGE_TabularGrid
                dgvGE.DataBind()
                'S.SANDEEP [ 15 DEC 2014 ] -- END
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Sorry no score gude defined for the selected item."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgcolhBSCScore_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If CInt(CType(sender, LinkButton).CommandArgument) > 0 Then 'SCALE MASTER ID
                Dim dsScore_Guide As New DataSet
                Dim objScaleMaster As New clsAssessment_Scale
                dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(CType(sender, LinkButton).CommandArgument))
                objScaleMaster = Nothing
                dgvBSCScoreGuide.DataSource = dsScore_Guide.Tables(0)
                dgvBSCScoreGuide.DataBind()
                popup_ViewGuideBSC.Show()
                'S.SANDEEP [ 15 DEC 2014 ] -- START
                iWeightTotal = 0 'S.SANDEEP [ 17 DEC 2014 ] -- START -- END
                dgvBSC.DataSource = dtBSC_TabularGrid
                dgvBSC.DataBind()
                'S.SANDEEP [ 15 DEC 2014 ] -- END
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Sorry no score gude defined for the selected item."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim xRow As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)

            'S.SANDEEP [12 OCT 2016] -- START
            'Me.ViewState("RowIndex") = -1
            'mstriEditingGUID = ""
            If mblnIsMatchCompetencyStructure Then
                Me.ViewState("RowIndex") = -1
                mstriEditingGUID = dtCustomTabularGrid.Rows(xRow.RowIndex).Item("GUID")
            Else
                Me.ViewState("RowIndex") = -1
                mstriEditingGUID = ""
            End If
            'S.SANDEEP [12 OCT 2016] -- END

            'S.SANDEEP [25-JAN-2017] -- START
            'ISSUE/ENHANCEMENT : ISSUE RELATED TO HEIGHT OF TEXTBOX {AKF Self-services In PA Custom Items}
            btnIAdd.Text = "Add"
            'S.SANDEEP [25-JAN-2017] -- END

            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
                'S.SANDEEP |25-MAR-2019| -- START
                'Dim xtmp() As DataRow = mdtCustomEvaluation.Select("custom_header = '" & dtCustomTabularGrid.Rows(xRow.RowIndex).Item("Header_Name") & "' AND AUD <> 'D'")
                Dim xtmp() As DataRow = mdtCustomEvaluation.Select("Header_Id = '" & dtCustomTabularGrid.Rows(xRow.RowIndex).Item("Header_Id") & "' AND AUD <> 'D'")
                'S.SANDEEP |25-MAR-2019| -- END
                If xtmp.Length > 0 AndAlso CBool(dtCustomTabularGrid.Rows(xRow.RowIndex).Item("Is_Allow_Multiple")) = False Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 118, "Sorry, this particular custom header is not set for allow multiple entries when defined."), Me)
                    Exit Sub
                End If
            End If
            Call Generate_Popup_Data(dtCustomTabularGrid.Rows(xRow.RowIndex).Item("Header_Id"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub imgEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
            Me.ViewState("RowIndex") = row.RowIndex

            'S.SANDEEP [25-JAN-2017] -- START
            'ISSUE/ENHANCEMENT : ISSUE RELATED TO HEIGHT OF TEXTBOX {AKF Self-services In PA Custom Items}
            btnIAdd.Text = "Save"
            'S.SANDEEP [25-JAN-2017] -- END

            mstriEditingGUID = dtCustomTabularGrid.Rows(row.RowIndex).Item("GUID")

            'S.SANDEEP |30-MAR-2019| -- START
            If mstriEditingGUID.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 57, "Sorry, No comment(s) has been added in order to edit."), Me)
                Exit Sub
            End If
            'S.SANDEEP |30-MAR-2019| -- END

            Call Generate_Popup_Data(dtCustomTabularGrid.Rows(row.RowIndex).Item("Header_Id"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
            Me.ViewState("RowIndex") = row.RowIndex
            mstriEditingGUID = dtCustomTabularGrid.Rows(row.RowIndex).Item("GUID")
            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
                Dim xRow() As DataRow = Nothing
                xRow = mdtCustomEvaluation.Select("customanalysistranguid = '" & dtCustomTabularGrid.Rows(row.RowIndex).Item("GUID") & "' AND AUD <> 'D'")
                If xRow.Length > 0 Then
                    lblTitle.Text = "Aruti"
                    lblMessage.Text = "Please enter vaild reason to void following entry."
                    txtMessage.Text = ""
                    popup_CItemReason.Show()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [21 Mar 2015]-START
    'Issue : Fixing Issues Sent By Aandrew in PA Testing.
    Protected Sub dtpCustomItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If CDate(dtpAssessdate.GetDate) >= CDate(CType(CType(sender, TextBox).NamingContainer, Controls_DateCtrl).GetDate) Then
                CType(CType(sender, TextBox).NamingContainer, Controls_DateCtrl).SetDate = Nothing
                DisplayMessage.DisplayMessage(Language.getMessage("frmAddCustomValue", 2, "Sorry, Selected date should be greter than the assessment date selected."), Me)
                CType(sender, Controls_DateCtrl).Focus()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'SHANI [21 Mar 2015]--END 

#End Region

#Region " ComboBox Event(s) "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            mintYearUnkid = objPrd._Yearunkid
            objPrd = Nothing

            Dim objMapping As New clsAssess_Field_Mapping
            iLinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            iMappingUnkid = objMapping.Get_MappingUnkId(CInt(cboPeriod.SelectedValue))
            objMapping._Mappingunkid = iMappingUnkid
            xTotAssignedWeight = objMapping._Weight
            objMapping = Nothing

            Dim objFMaster As New clsAssess_Field_Master
            iExOrdr = objFMaster.Get_Field_ExOrder(iLinkedFieldId, True)
            objFMaster = Nothing

            objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)
            'S.SANDEEP [29 DEC 2015] -- START

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'objCCustomTran._AllowCustomItemInPlanning = CBool(Session("IncludeCustomItemInPlanning"))
            objCCustomTran._IsCompanyNeedReviewer = CBool(Session("IsCompanyNeedReviewer"))
            'Shani (26-Sep-2016) -- End


            objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
            'S.SANDEEP [29 DEC 2015] -- END
            mdtCustomEvaluation = objCCustomTran._DataTable
            'If menAction <> enAction.EDIT_ONE Then
            '    mdtCustomEvaluation.Rows.Clear()
            'End If

            If CInt(cboPeriod.SelectedValue) > 0 Then
                If CStr(Session("Perf_EvaluationOrder")).Trim.Length > 0 Then
                    Dim objCHeader As New clsassess_custom_header
                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                    dsHeaders.Tables(0).Rows.Clear()
                    Dim xRow As DataRow = Nothing
                    Dim iOrdr() As String = CStr(Session("Perf_EvaluationOrder")).Split("|")
                    If iOrdr.Length > 0 Then
                        Select Case CInt(iOrdr(0))
                            Case enEvaluationOrder.PE_BSC_SECTION
                                xRow = dsHeaders.Tables(0).NewRow
                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                'xRow.Item("Id") = -3 : xRow.Item("Name") = "Goals Evaluation" : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                xRow.Item("Id") = -3 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Shani (26-Sep-2016) -- End
                                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                                xRow = dsHeaders.Tables(0).NewRow
                                xRow.Item("Id") = -2 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                            Case enEvaluationOrder.PE_CUSTOM_SECTION
                                dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                        End Select
                        Select Case CInt(iOrdr(1))
                            Case enEvaluationOrder.PE_BSC_SECTION
                                xRow = dsHeaders.Tables(0).NewRow

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                'xRow.Item("Id") = -3 : xRow.Item("Name") = "Goals Evaluation" : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                xRow.Item("Id") = -3 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Shani (26-Sep-2016) -- End


                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                                xRow = dsHeaders.Tables(0).NewRow
                                xRow.Item("Id") = -2 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_CUSTOM_SECTION
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    Dim dsList As New DataSet
                                    dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                    dsHeaders.Tables(0).Merge(dsList.Tables(0), True)
                                Else
                                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                End If
                        End Select
                        Select Case CInt(iOrdr(2))
                            Case enEvaluationOrder.PE_BSC_SECTION
                                xRow = dsHeaders.Tables(0).NewRow

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                'xRow.Item("Id") = -3 : xRow.Item("Name") = "Goals Evaluation" : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                xRow.Item("Id") = -3 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Shani (26-Sep-2016) -- End


                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                                xRow = dsHeaders.Tables(0).NewRow
                                xRow.Item("Id") = -2 : xRow.Item("Name") = Language.getMessage(mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_CUSTOM_SECTION
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    Dim dsList As New DataSet
                                    dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                    dsHeaders.Tables(0).Merge(dsList.Tables(0), True)
                                Else
                                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                End If
                        End Select
                    End If
                    objCHeader = Nothing
                End If
            Else
                If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables.Count > 0 Then
                    dsHeaders.Tables(0).Rows.Clear()
                End If
            End If
            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            BtnSearch_Click(New Object(), New EventArgs())
            'S.SANDEEP |12-FEB-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'S.SANDEEP |16-AUG-2019| -- START
    Protected Sub cbo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim drp As DropDownList = CType(sender, DropDownList)

            Dim dt As DataTable = CType(drp.DataSource, DataTable)




            For Each item As ListItem In drp.Items
                Dim irow As DataRow() = Nothing
                irow = dt.Select("name = '" & item.Text & "' ")

                If IsNothing(irow) = False AndAlso irow.Length > 0 Then
                    item.Attributes.Add("title", irow(0)("description").ToString())
                End If

            Next


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-AUG-2019| -- END

#End Region

    'S.SANDEEP |25-MAR-2019| -- START
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblAssessDate.Text = Language._Object.getCaption(lblAssessDate.ID, Me.lblAssessDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(lblPeriod.ID, Me.lblPeriod.Text)
            Me.btnSaveCommit.Text = Language._Object.getCaption(btnSaveCommit.ID, Me.btnSaveCommit.Text).Replace("&", "")
            Me.btnSave.Text = Language._Object.getCaption(btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(btnClose.ID, Me.btnClose.Text).Replace("&", "")

            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText = Language._Object.getCaption("dgcolhSDate", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhEDate", False, True)).HeaderText = Language._Object.getCaption("dgcolhEDate", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhCompleted", False, True)).HeaderText = Language._Object.getCaption("dgcolhCompleted", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhGoalType", False, True)).HeaderText = Language._Object.getCaption("dgcolhGoalType", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgoalvalue", False, True)).HeaderText = Language._Object.getCaption("dgoalvalue", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhStatus", False, True)).HeaderText = Language._Object.getCaption("dgcolhStatus", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhBSCScore", False, True)).HeaderText = Language._Object.getCaption("dgcolhBSCScore", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhBSCWeight", False, True)).HeaderText = Language._Object.getCaption("dgcolhBSCWeight", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolheselfBSC", False, True)).HeaderText = Language._Object.getCaption("dgcolheselfBSC", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolheremarkBSC", False, True)).HeaderText = Language._Object.getCaption("dgcolheremarkBSC", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "", False, True)).HeaderText)

            Language.setLanguage("frmAddCustomValue")
            dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhItems", False, True)).HeaderText = Language._Object.getCaption("dgcolhItems", dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhItems", False, True)).HeaderText)
            dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhValue", False, True)).HeaderText = Language._Object.getCaption("dgcolhValue", dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhValue", False, True)).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
    'S.SANDEEP |25-MAR-2019| -- END


End Class
'Dim xRow As DataGridItem = CType(CType(sender, (Control)).NamingContainer, DataGridItem)   'RETURNS FULL ROW FOR PARTICULAR ITEM

'Language.getMessage(mstrModuleName, 3, "Balance Score Card Evaluation") -- Later Change it Back