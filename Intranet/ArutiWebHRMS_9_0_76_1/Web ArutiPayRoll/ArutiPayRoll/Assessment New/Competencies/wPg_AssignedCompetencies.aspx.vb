﻿#Region " Import "

Imports System.Data
Imports Aruti.Data
Imports System.Drawing
Imports AjaxControlToolkit
Imports System.IO
Imports System.Web.Services
Imports System

#End Region

Partial Class wPg_AssignedCompetencies
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private mstrModuleName As String = "frmAssignCompetencies"
    Private menAction As enAction = enAction.ADD_ONE
    Private mdtTran As DataTable
    Private objCompetence As New clsassess_competencies_master
    Private objAssignCompetenceMst As New clsassess_competence_assign_master
    Private objAssignCompetenceTrn As New clsassess_competence_assign_tran
    Private mdtCompetenceTran As DataTable
    Private mintComptenceAssignMasterId As Integer = 0
    Private mDecAssignedWeight As Decimal = 0
    Private iVoidReason As String = String.Empty

#End Region

#Region " Page Event(s) "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmAssignCompetencies"
            StrModuleName2 = "mnuAssessment"
            StrModuleName3 = "mnuSetups"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")
            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If
            delReason.Title = "Unassign Competencies"
            If IsPostBack = False Then
                If Session("Action") IsNot Nothing AndAlso Session("Unkid") IsNot Nothing Then
                    mintComptenceAssignMasterId = Session("Unkid")
                    If CInt(Session("Action")) = 0 Then
                        menAction = enAction.ADD_ONE
                    ElseIf CInt(Session("Action")) = 1 Then
                        menAction = enAction.EDIT_ONE
                    ElseIf CInt(Session("Action")) = 2 Then
                        menAction = enAction.ADD_CONTINUE
                    End If
                    Call FillCombo()
                    cboEmployee.Enabled = CBool(Session("Self_Assign_Competencies"))
                    If menAction = enAction.EDIT_ONE Then
                        objAssignCompetenceMst._Assigncompetenceunkid = mintComptenceAssignMasterId
                        cboAssessGroup.Enabled = False : cboPeriod.Enabled = False
                        cboEmployee.Enabled = False : btnSearch.Enabled = False : btnReset.Enabled = False
                        'Shani (01-Aug-2016) -- Start
                        'Enhancement -
                        Dim dtJobComp As DataSet = (New clsJobCompetencies_Tran).GetCompetencies_tran()

                        'S.SANDEEP [25-JAN-2017] -- START
                        'ISSUE/ENHANCEMENT :
                        'If dtJobComp IsNot Nothing AndAlso dtJobComp.Tables(0).Rows.Count > 0 Then
                        '    chkJobCompetencies.Checked = True
                        'Else
                        '    chkJobCompetencies.Checked = False
                        'End If
                        'S.SANDEEP [25-JAN-2017] -- END

                        'Shani (01-Aug-2016) -- End
                    End If
                    Call GetValue()
                    If menAction = enAction.EDIT_ONE Then
                        Call FillGrid()
                    End If
                    objAssignCompetenceTrn._Assigncompetenceunkid = mintComptenceAssignMasterId
                    mdtCompetenceTran = objAssignCompetenceTrn._DataTable
                    'S.SANDEEP [02 Jan 2016] -- START
                    'Me.Session("mdtCompetenceTran") = mdtCompetenceTran
                    'S.SANDEEP [02 Jan 2016] -- END
                    If mdtCompetenceTran.Rows.Count > 0 Then mDecAssignedWeight = CDec(mdtCompetenceTran.Compute("SUM(weight)", "AUD <>'D'"))

                    'SHANI [09 Mar 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    If Session("Competencies_Filter") IsNot Nothing Then
                        If CBool(Session("Self_Assign_Competencies")) = True Then
                            cboEmployee.SelectedValue = CStr(Session("Competencies_Filter")).Split("-")(0).Trim
                            cboPeriod.SelectedValue = CStr(Session("Competencies_Filter")).Split("-")(1).Trim
                            'S.SANDEEP [29 DEC 2015] -- START
                            Call cboEmployee_SelectedIndexChanged(New Object, New EventArgs)
                            'S.SANDEEP [29 DEC 2015] -- END
                        Else
                            cboAssessGroup.SelectedValue = CStr(Session("Competencies_Filter")).Split("-")(0).Trim
                            cboPeriod.SelectedValue = CStr(Session("Competencies_Filter")).Split("-")(1).Trim
                        End If
                    End If
                    'SHANI [09 Mar 2015]--END
                Else
                    Response.Redirect(Session("servername") & "~/Assessment New/Competencies/wpg_AssignedCompetenciesList.aspx", False)
                End If
            End If

            If Me.ViewState("Action") IsNot Nothing Then
                menAction = Me.ViewState("Action")
            End If

            If Me.ViewState("mintComptenceAssignMasterId") IsNot Nothing Then
                mintComptenceAssignMasterId = Me.ViewState("mintComptenceAssignMasterId")
            End If

            If Me.ViewState("iVoidReason") IsNot Nothing Then
                iVoidReason = Me.ViewState("iVoidReason")
            End If

            'S.SANDEEP [02 JAN 2016] -- START
            'If Me.Session("mdtCompetenceTran") IsNot Nothing Then
            '    mdtCompetenceTran = Me.Session("mdtCompetenceTran")
            'End If

            'If Me.Session("mdtTran") IsNot Nothing Then
            '    mdtTran = Me.Session("mdtTran")
            'End If

            If Me.ViewState("mdtCompetenceTran") IsNot Nothing Then
                mdtCompetenceTran = Me.ViewState("mdtCompetenceTran")
            End If

            If Me.ViewState("mdtTran") IsNot Nothing Then
                mdtTran = Me.ViewState("mdtTran")
            End If
            'S.SANDEEP [02 JAN 2016] -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Me.ViewState("Action") Is Nothing Then
                Me.ViewState.Add("Action", menAction)
            Else
                Me.ViewState("Action") = menAction
            End If

            If Me.ViewState("mintComptenceAssignMasterId") Is Nothing Then
                Me.ViewState.Add("mintComptenceAssignMasterId", mintComptenceAssignMasterId)
            Else
                Me.ViewState("mintComptenceAssignMasterId") = mintComptenceAssignMasterId
            End If

            If Me.ViewState("iVoidReason") Is Nothing Then
                Me.ViewState.Add("iVoidReason", iVoidReason)
            Else
                Me.ViewState("iVoidReason") = iVoidReason
            End If

            'S.SANDEEP [02 JAN 2016] -- START
            'If Me.Session("mdtCompetenceTran") Is Nothing Then
            '    Me.Session.Add("mdtCompetenceTran", mdtCompetenceTran)
            'Else
            '    Me.Session("mdtCompetenceTran") = mdtCompetenceTran
            'End If
            'If Me.Session("mdtTran") Is Nothing Then
            '    Me.Session.Add("mdtTran", mdtTran)
            'Else
            '    Me.Session("mdtTran") = mdtTran
            'End If
            If Me.ViewState("mdtCompetenceTran") Is Nothing Then
                Me.ViewState.Add("mdtCompetenceTran", mdtCompetenceTran)
            Else

                'Shani(24-Feb-2016) -- Start
                ' Me.ViewState("Headers") = mdtCompetenceTran
                Me.ViewState("mdtCompetenceTran") = mdtCompetenceTran
                'Shani(24-Feb-2016) -- End
            End If

            If Me.ViewState("mdtTran") Is Nothing Then
                Me.ViewState.Add("mdtTran", mdtTran)
            Else
                Me.ViewState("mdtTran") = mdtTran
            End If
            'S.SANDEEP [02 JAN 2016] -- END


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Session("Unkid") IsNot Nothing Then
                Session.Remove("Unkid")
            End If
            If Session("Action") IsNot Nothing Then
                Session.Remove("Action")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Method(s) "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objAGroup As New clsassess_group_master
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmployee As New clsEmployee_Master
        Try
            Dim dt As DataTable = Nothing
            dsCombo = objAGroup.getListForCombo("List", True)
            If CBool(Session("Self_Assign_Competencies")) = True Then
                dt = New DataView(dsCombo.Tables(0), "assessgroupunkid = 0", "", DataViewRowState.CurrentRows).ToTable
            Else
                dt = dsCombo.Tables(0)
            End If
            With cboAssessGroup
                .DataValueField = "assessgroupunkid"
                .DataTextField = "name"
                .DataSource = dt
                .SelectedValue = 0
                .DataBind()
            End With

            If (Session("LoginBy") = Global.User.en_loginby.User) Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If Session("IsIncludeInactiveEmp") = False Then
                '    dsCombo = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
                'Else
                '    dsCombo = objEmployee.GetEmployeeList("List", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                'End If

                'S.SANDEEP [05 DEC 2015] -- START
                Dim strFilterQry As String = String.Empty

                'Shani(14-APR-2016) -- Start
                'If Session("SkipApprovalFlowInPlanning") = True Then
                'Shani(14-APR-2016) -- End
                Dim csvIds As String = String.Empty
                Dim dsMapEmp As New DataSet
                Dim objEval As New clsevaluation_analysis_master
                dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
                                                        Session("UserId"), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    True, Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)
                'Shani (12-Jan-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]
                strFilterQry = " hremployee_master.employeeunkid NOT IN "
                If dsMapEmp.Tables("List").Rows.Count > 0 Then
                    csvIds = String.Join(",", dsMapEmp.Tables("List").AsEnumerable().Select(Function(x) x.Field(Of Integer)("EmpId").ToString()).ToArray())
                End If
                If csvIds.Trim.Length > 0 Then
                    strFilterQry &= "(" & csvIds & ")"
                Else
                    strFilterQry = ""
                End If

                'S.SANDEEP [04 Jan 2016] -- START
                If dsMapEmp.Tables("List").Rows.Count > 0 Then

                    dsCombo = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                              Session("UserId"), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                              Session("IsIncludeInactiveEmp"), _
                                                              CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)

                    If strFilterQry.Trim.Length > 0 Then
                        strFilterQry &= " AND hremployee_master.employeeunkid IN "
                    Else
                        strFilterQry = " hremployee_master.employeeunkid IN "
                    End If
                    csvIds = String.Join(",", dsCombo.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
                    If csvIds.Trim.Length > 0 Then
                        strFilterQry &= "(" & csvIds & ")"
                    Else
                        strFilterQry &= "(0)"
                    End If
                    'Shani(14-APR-2016) -- Start
                Else
                    strFilterQry = " hremployee_master.employeeunkid IN (0) "
                    'Shani(14-APR-2016) -- End 
                End If
                'S.SANDEEP [04 Jan 2016] -- END


                objEval = Nothing

                'Shani(14-APR-2016) -- Start

                'End If
                'Shani(14-APR-2016) -- End

                'S.SANDEEP [05 DEC 2015] -- END

                dsCombo = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "List", True, , , , , , , , , , , , , , , strFilterQry) 'S.SANDEEP [05 DEC 2015] {strFilterQry} 
                'Shani(24-Aug-2015) -- End
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombo.Tables("List")
                    .SelectedValue = 0
                    .DataBind()
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee.Copy
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                End With
            End If

            'S.SANDEEP [14 APR 2015] -- START
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), "List", True, 1)


            'S.SANDEEP [17 NOV 2015] -- START
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [17 NOV 2015] -- END


            'S.SANDEEP [31 DEC 2015] -- START

            'Shani (09-May-2016) -- Start
            'Dim mintCurrentPeriodId As Integer = 0
            'If dsCombo.Tables(0).Rows.Count > 1 Then
            '    mintCurrentPeriodId = dsCombo.Tables(0).Rows(dsCombo.Tables(0).Rows.Count - 1).Item("periodunkid")
            'End If
            Dim mintCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End

            'S.SANDEEP [31 DEC 2015] -- END


            'S.SANDEEP [14 APR 2015] -- END
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                'S.SANDEEP [31 DEC 2015] -- START
                '.SelectedValue = 0
                '.DataBind()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
                'S.SANDEEP [31 DEC 2015] -- END
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If CBool(Session("Self_Assign_Competencies")) = True Then
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    cboEmployee.SelectedValue = objAssignCompetenceMst._Employeeunkid
                End If
                cboEmployee_SelectedIndexChanged(New Object(), New EventArgs())
            End If
            cboAssessGroup.SelectedValue = objAssignCompetenceMst._Assessgroupunkid
            cboAssessGroup_SelectedIndexChanged(New Object(), New EventArgs())
            cboPeriod.SelectedValue = objAssignCompetenceMst._Periodunkid
            txtWeight.Text = objAssignCompetenceMst._Weight

            'S.SANDEEP [23 FEB 2016] -- START
            chkZeroCompetenceWeight.Checked = objAssignCompetenceMst._IsZeroWeightAllowed
            'S.SANDEEP [23 FEB 2016] -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objAssignCompetenceMst._Assigncompetenceunkid = mintComptenceAssignMasterId
            objAssignCompetenceMst._Assessgroupunkid = CInt(cboAssessGroup.SelectedValue)
            objAssignCompetenceMst._Periodunkid = CInt(cboPeriod.SelectedValue)
            objAssignCompetenceMst._Weight = txtWeight.Text
            objAssignCompetenceMst._Isvoid = False
            objAssignCompetenceMst._Userunkid = Session("UserId")
            objAssignCompetenceMst._Voiddatetime = Nothing
            objAssignCompetenceMst._Voidreason = ""
            objAssignCompetenceMst._Voiduserunkid = -1
            If CBool(Session("Self_Assign_Competencies")) = True Then
                objAssignCompetenceMst._Employeeunkid = CInt(cboEmployee.SelectedValue)
            End If

            'S.SANDEEP [23 FEB 2016] -- START
            objAssignCompetenceMst._IsZeroWeightAllowed = chkZeroCompetenceWeight.Checked
            'S.SANDEEP [23 FEB 2016] -- END


            'S.SANDEEP [23 DEC 2015] -- START

            ''S.SANDEEP [05 DEC 2015] -- START
            'If Session("SkipApprovalFlowInPlanning") = True Then
            '    objAssignCompetenceMst._IsFinal = True
            'End If
            ''S.SANDEEP [05 DEC 2015] -- END

            'S.SANDEEP [23 DEC 2015] -- END


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillGrid()
        Try

            'S.SANDEEP [23 DEC 2015] -- START
            'If menAction = enAction.EDIT_ONE Then
            '    mdtTran = objCompetence.Grouped_Competencies(CInt(cboAssessGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 2, , CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)))
            'Else
            '    mdtTran = objCompetence.Grouped_Competencies(CInt(cboAssessGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 1, , CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)))
            'End If

            'S.SANDEEP [25-JAN-2017] -- START
            'ISSUE/ENHANCEMENT :
            'If menAction = enAction.EDIT_ONE Then
            '    mdtTran = objCompetence.Grouped_Competencies(CInt(cboAssessGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 2, , CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)), chkJobCompetencies.Checked, eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            'Else
            '    mdtTran = objCompetence.Grouped_Competencies(CInt(cboAssessGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 1, , CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)), chkJobCompetencies.Checked, eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            'End If
            If menAction = enAction.EDIT_ONE Then
                mdtTran = objCompetence.Grouped_Competencies(CInt(cboAssessGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 2, , CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)), Session("AssignByJobCompetencies"), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            Else
                mdtTran = objCompetence.Grouped_Competencies(CInt(cboAssessGroup.SelectedValue), CInt(cboPeriod.SelectedValue), 1, , CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)), Session("AssignByJobCompetencies"), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            End If
            'S.SANDEEP [25-JAN-2017] -- END

            'S.SANDEEP [23 DEC 2015] -- END


            'S.SANDEEP [28 DEC 2015] -- START
            If mdtTran Is Nothing AndAlso objCompetence._Message <> "" Then
                DisplayMessage.DisplayMessage(objCompetence._Message, Me)
                btnSave.Enabled = False
                Exit Sub
            Else
                btnSave.Enabled = True
            End If
            'S.SANDEEP [28 DEC 2015] -- END


            lvData.DataSource = mdtTran
            lvData.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CBool(Session("Self_Assign_Competencies")) = True Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, Employee is mandatory information. Please select employee to continue."), Me)
                    cboEmployee.Focus()
                    Return False
                End If
            End If
            Dim mDecWgt As Decimal = 0 : Decimal.TryParse(txtWeight.Text, mDecWgt)
            If mDecWgt < mDecAssignedWeight Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, Weight assigned to competencies exceeds the total weight set for the selected group. Please provide the correct weight."), Me)
                ''S.SANDEEP [09 FEB 2015] -- START
                'If Me.Session("chkAll") IsNot Nothing Then
                '    Dim chkAll As CheckBox = Me.Session("chkAll")
                '    Dim xRow As DataGridItem = CType(chkAll.NamingContainer, DataGridItem)
                '    Dim itxt As TextBox = CType(lvData.Items(xRow.ItemIndex).Cells(3).FindControl("txtItemWeight"), TextBox)
                '    itxt.Text = ""
                'End If
                ''S.SANDEEP [09 FEB 2015] -- END
                txtWeight.Focus()
                Return False
            End If

            'Shani(24-Feb-2016) -- Start
            '
            If mdtTran.Select("ischeck=True AND IsGrp=False ").Length <= 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, Competencies are mandatory information. Please check atleast one competence item in order to save."), Me)
                'Sohail (23 Mar 2019) -- End
                Return False
            End If
            'Shani(24-Feb-2016) -- End

            'S.SANDEEP [23 DEC 2015] -- START
            If Session("OnlyOneItemPerCompetencyCategory") = True Then
                Dim result = From res In mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsGrp") = True) Select res
                If result IsNot Nothing AndAlso result.Count > 0 Then
                    For Each row In result
                        Dim intGrpId, intCount As Integer
                        intGrpId = -1 : intCount = 0
                        intGrpId = row("GrpId")
                        intCount = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True And x.Field(Of Integer)("GrpId") = intGrpId _
                                                                And x.Field(Of Boolean)("IsGrp") = False).Count()
                        If intCount > 1 Then
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'DisplayMessage.DisplayError(ex, Me)
                            DisplayMessage.DisplayMessage("Sorry, you are not allowed to assign more than one custom items per each group.", Me)
                            'Sohail (23 Mar 2019) -- End
                            Return False
                        End If
                    Next
                End If
            End If
            'S.SANDEEP [23 DEC 2015] -- END



            'S.SANDEEP [23 FEB 2016] -- START
            'Dim dtmp() As DataRow = mdtTran.Select("ischeck=True AND IsGrp=False AND weight = 0")
            'If dtmp.Length > 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, Some of the assigned competencies does not have weight assigned. Please assigned the weight to those checked competencies."), Me)
            '    ''S.SANDEEP [09 FEB 2015] -- START
            '    'If Me.Session("chkAll") IsNot Nothing Then
            '    '    Dim chkAll As CheckBox = Me.Session("chkAll")
            '    '    Dim xRow As DataGridItem = CType(chkAll.NamingContainer, DataGridItem)
            '    '    Dim itxt As TextBox = CType(lvData.Items(xRow.ItemIndex).Cells(3).FindControl("txtItemWeight"), TextBox)
            '    '    itxt.Text = ""
            '    'End If
            '    ''S.SANDEEP [09 FEB 2015] -- END
            '    Return False
            'End If

            'Shani (01-Aug-2016) -- Start
            'Enhancement - 
            'If chkZeroCompetenceWeight.Checked = False Then
            '    Dim dtmp() As DataRow = mdtTran.Select("ischeck=True AND IsGrp=False AND weight = 0")
            '    If dtmp.Length > 0 Then
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, Some of the assigned competencies does not have weight assigned. Please assigned the weight to those checked competencies."), Me)
            '        Return False
            '    End If
            'End If
            If chkZeroCompetenceWeight.Checked = False Then
                Dim d As Decimal = 0
                Dim dtmp = (From p In lvData.Items.Cast(Of DataGridItem)().Where(Function(x) CType(x.Cells(0).FindControl("chkItemSelect"), CheckBox).Checked = True And CBool(x.Cells(5).Text) = False And (Decimal.TryParse(CType(x.Cells(3).FindControl("txtItemWeight"), TextBox).Text, d) = False Or d <= 0)))
                If dtmp.Count > 0 Then
                    For Each gvItem As DataGridItem In dtmp
                        CType(gvItem.Cells(3).FindControl("txtItemWeight"), TextBox).Style.Add("border-color", "red")
                    Next
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, Some of the assigned competencies does not have weight assigned. Please assigned the weight to those checked competencies."), Me)
                    Return False
                End If
            End If
            'Shani (01-Aug-2016) -- End

            'S.SANDEEP [23 FEB 2016] -- END

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub Enable_Disable(ByVal iOpr As Boolean)
        Try
            cboAssessGroup.Enabled = iOpr
            cboPeriod.Enabled = iOpr
            If CBool(Session("Self_Assign_Competencies")) = True Then
                cboEmployee.Enabled = iOpr
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Evaluated_Data(ByVal iWeight As Decimal, ByVal iRowIdx As Integer, ByVal xGrdItem As DataGridItem, Optional ByVal iDelete As Boolean = False)
        Try
            Dim dRow As DataRow = Nothing
            Dim dtmp() As DataRow = Nothing
            If menAction <> enAction.EDIT_ONE Then

                'Shani(24-Feb-2016) -- Start
                Dim chk As CheckBox = CType(xGrdItem.Cells(0).FindControl("chkItemSelect"), CheckBox)
                'Shani(24-Feb-2016) -- End
                If CInt(xGrdItem.Cells(7).Text) > 0 Then
                    dtmp = mdtCompetenceTran.Select("assigncompetencetranunkid = '" & CInt(xGrdItem.Cells(7).Text) & "' AND AUD <> 'D' AND competenciesunkid = '" & CInt(mdtTran.Rows(xGrdItem.ItemIndex).Item("competenciesunkid")) & "'")
                Else
                    dtmp = mdtCompetenceTran.Select("GUID = '" & xGrdItem.Cells(8).Text & "' AND AUD <> 'D' AND competenciesunkid = '" & CInt(mdtTran.Rows(xGrdItem.ItemIndex).Item("competenciesunkid")) & "'")
                End If

                'Shani(24-Feb-2016) -- Start
                If chk.Checked = False AndAlso chkZeroCompetenceWeight.Checked = True Then
                    dtmp = Nothing
                End If
                'Shani(24-Feb-2016) -- End
            Else
                dtmp = mdtCompetenceTran.Select("AUD <> 'D' AND competenciesunkid = '" & CInt(mdtTran.Rows(xGrdItem.ItemIndex).Item("competenciesunkid")) & "'")
            End If

            'Shani(24-Feb-2016) -- Start
            'If dtmp.Length <= 0 Then
            If dtmp IsNot Nothing AndAlso dtmp.Length <= 0 Then
                'Shani(24-Feb-2016) -- End
                dRow = mdtCompetenceTran.NewRow
                dRow.Item("assigncompetencetranunkid") = -1
                dRow.Item("assigncompetenceunkid") = mintComptenceAssignMasterId
                dRow.Item("competenciesunkid") = mdtTran.Rows(xGrdItem.ItemIndex).Item("competenciesunkid")
                dRow.Item("weight") = iWeight
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voidreason") = ""
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("GrpId") = xGrdItem.Cells(4).Text
                xGrdItem.Cells(8).Text = dRow.Item("GUID")
                mdtCompetenceTran.Rows.Add(dRow)

                'Shani(24-Feb-2016) -- Start
                'Else
            ElseIf dtmp IsNot Nothing Then
                'Shani(24-Feb-2016) -- End
                dtmp(0).Item("assigncompetencetranunkid") = dtmp(0).Item("assigncompetencetranunkid")
                dtmp(0).Item("assigncompetenceunkid") = mintComptenceAssignMasterId
                dtmp(0).Item("competenciesunkid") = mdtTran.Rows(xGrdItem.ItemIndex).Item("competenciesunkid")
                dtmp(0).Item("weight") = iWeight
                If iDelete = True Then
                    dtmp(0).Item("isvoid") = True
                    dtmp(0).Item("voidreason") = iVoidReason
                    dtmp(0).Item("voiduserunkid") = Session("UserId")
                    dtmp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    dtmp(0).Item("AUD") = "D"
                Else
                    dtmp(0).Item("isvoid") = False
                    dtmp(0).Item("voiduserunkid") = -1
                    dtmp(0).Item("voidreason") = ""
                    dtmp(0).Item("voiddatetime") = DBNull.Value
                    If IsDBNull(dtmp(0).Item("AUD")) Or CStr(dtmp(0).Item("AUD")).ToString.Trim = "" Then
                        dtmp(0).Item("AUD") = "U"
                    End If
                End If
                dtmp(0).Item("GUID") = Guid.NewGuid.ToString
                xGrdItem.Cells(8).Text = dtmp(0).Item("GUID")
                dtmp(0).AcceptChanges()
            End If

            If mdtTran IsNot Nothing Then
                Dim mIdx As Integer = -1 : Dim dttmp() As DataRow
                dttmp = mdtTran.Select("GrpId = '" & xGrdItem.Cells(4).Text & "' AND IsGrp=True")
                If dttmp.Length > 0 Then
                    mIdx = mdtTran.Rows.IndexOf(dttmp(0))
                    Dim xValue As Decimal = 0
                    If IsDBNull(mdtCompetenceTran.Compute("SUM(weight)", "GrpId = '" & xGrdItem.Cells(4).Text & "' AND AUD <> 'D'")) = False Then
                        xValue = mdtCompetenceTran.Compute("SUM(weight)", "GrpId = '" & xGrdItem.Cells(4).Text & "' AND AUD <> 'D'")
                    End If
                    If xValue > 0 Then
                        lvData.Items(mIdx).Cells(3).Text = xValue
                    Else
                        lvData.Items(mIdx).Cells(3).Text = ""
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearForm_Values()
        Try
            Me.ViewState("Action") = Nothing
            Me.ViewState("mintComptenceAssignMasterId") = Nothing
            Me.Session.Remove("mdtCompetenceTran")
            Me.Session.Remove("mdtTran")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Grid_Operation(ByVal xGrid As DataGrid, ByVal xCallEvalMethod As Boolean, Optional ByVal xPopUpNo As Boolean = False)
        Try
            If Me.Session("chkAll") Is Nothing Then Exit Sub
            Dim chkAll As CheckBox = Me.Session("chkAll")

            If xPopUpNo = True Then
                chkAll.Checked = True
            End If

            Dim xRow As DataGridItem = CType(chkAll.NamingContainer, DataGridItem)

            If CBool(xRow.Cells(5).Text) = True Then
                For Each iRow As DataGridItem In xGrid.Items
                    If iRow.Cells(4).Text = xRow.Cells(4).Text AndAlso CBool(iRow.Cells(5).Text) = False Then
                        Dim chk As CheckBox = CType(xGrid.Items(iRow.ItemIndex).Cells(0).FindControl("chkItemSelect"), CheckBox)
                        chk.Checked = chkAll.Checked
                        mdtTran.Rows(iRow.ItemIndex)("ischeck") = chkAll.Checked
                        'S.SANDEEP [02 Jan 2016] -- START
                        If chkAll.Checked = False Then
                            mdtTran.Rows(xRow.ItemIndex)("weight") = 0
                        End If
                        'S.SANDEEP [02 Jan 2016] -- END
                        Dim itxt As TextBox = CType(xGrid.Items(iRow.ItemIndex).Cells(3).FindControl("txtItemWeight"), TextBox)
                        Select Case chkAll.Checked
                            Case True
                                If xPopUpNo = True Then
                                    itxt.Text = mdtTran.Rows(iRow.ItemIndex)("weight")
                                    itxt.Enabled = True
                                    'S.SANDEEP [29 DEC 2015] -- START
                                    If iRow IsNot Nothing Then
                                        iRow.BackColor = Color.SkyBlue
                                    End If
                                    'S.SANDEEP [29 DEC 2015] -- END

                                Else
                                    itxt.Text = ""
                                    itxt.Enabled = True
                                    'S.SANDEEP [29 DEC 2015] -- START
                                    If iRow IsNot Nothing Then
                                        iRow.BackColor = Color.SkyBlue
                                    End If
                                    'S.SANDEEP [29 DEC 2015] -- END
                                End If
                            Case False
                                itxt.Text = "" : itxt.Enabled = False
                                'S.SANDEEP [29 DEC 2015] -- START
                                If iRow IsNot Nothing Then
                                    iRow.BackColor = Color.White
                                End If
                                'S.SANDEEP [29 DEC 2015] -- END
                                If xCallEvalMethod = True Then Call Evaluated_Data(CDec(mdtTran.Rows(xRow.ItemIndex).Item("weight")), xRow.ItemIndex, xRow, True)
                        End Select
                    End If
                Next
            Else

                'Shani(06-Feb-2016) -- Start
                'PA Changes Given By CCBRT
                'Dim chk As CheckBox = CType(xGrid.Items(xRow.ItemIndex).Cells(0).FindControl("chkItemSelect"), CheckBox)
                'Shani(06-Feb-2016) -- End

                'S.SANDEEP [04 Jan 2016] -- START
                If Session("OnlyOneItemPerCompetencyCategory") Then
                    If mdtTran.AsEnumerable.Where(Function(x) x.Field(Of Boolean)("ischeck") = True And x.Field(Of Integer)("GrpId") = mdtTran.Rows(xRow.ItemIndex)("GrpId") And x.Field(Of Integer)("competenciesunkid") <> mdtTran.Rows(xRow.ItemIndex)("competenciesunkid")).Count = 1 Then
                        DisplayMessage.DisplayMessage("Sorry, you are not allowed to have more than one competence item within a Competency Category", Me)

                        'Shani(06-Feb-2016) -- Start
                        'PA Changes Given By CCBRT
                        'chk.Checked = False
                        chkAll.Checked = False
                        'Shani(06-Feb-2016) -- End
                    End If
                End If
                'S.SANDEEP [04 Jan 2016] -- END

                'Shani(06-Feb-2016) -- Start
                'PA Changes Given By CCBRT
                'chk.Checked = chkAll.Checked
                'Shani(06-Feb-2016) -- End

                mdtTran.Rows(xRow.ItemIndex)("ischeck") = chkAll.Checked
                'S.SANDEEP [02 Jan 2016] -- START
                If chkAll.Checked = False Then
                    mdtTran.Rows(xRow.ItemIndex)("weight") = 0
                End If
                'S.SANDEEP [02 Jan 2016] -- END

                Dim itxt As TextBox = CType(xGrid.Items(xRow.ItemIndex).Cells(3).FindControl("txtItemWeight"), TextBox)
                Select Case chkAll.Checked
                    Case True
                        If xPopUpNo = True Then
                            itxt.Text = mdtTran.Rows(xRow.ItemIndex)("weight")
                            itxt.Enabled = True
                            'S.SANDEEP [29 DEC 2015] -- START
                            If xRow IsNot Nothing Then
                                'Shani (01-Aug-2016) -- Start
                                'Enhancement -
                                'xRow.BackColor = Color.SkyBlue
                                lvData.Items(xRow.ItemIndex).BackColor = Color.SkyBlue
                                'Shani (01-Aug-2016) -- End
                            End If
                            'Shani (01-Aug-2016) -- Start
                            'Enhancement - 
                            CType(lvData.Items(xRow.ItemIndex).Cells(0).FindControl("chkItemSelect"), CheckBox).Checked = True
                            'Shani (01-Aug-2016) -- End
                            'S.SANDEEP [29 DEC 2015] -- END
                        Else
                            itxt.Text = ""
                            itxt.Enabled = True
                            'S.SANDEEP [29 DEC 2015] -- START
                            If xRow IsNot Nothing Then
                                xRow.BackColor = Color.SkyBlue
                            End If
                            'S.SANDEEP [29 DEC 2015] -- END
                        End If
                    Case False
                        itxt.Text = "" : itxt.Enabled = False
                        'S.SANDEEP [29 DEC 2015] -- START
                        If xRow IsNot Nothing Then
                            xRow.BackColor = Color.White
                        End If
                        'S.SANDEEP [29 DEC 2015] -- END
                        If xCallEvalMethod = True Then Call Evaluated_Data(CDec(mdtTran.Rows(xRow.ItemIndex).Item("weight")), xRow.ItemIndex, xRow, True)
                End Select
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'S.SANDEEP [23 FEB 2016] -- START
    Public Sub Generate_Competence_Table(ByVal dgvGrid As DataGrid)
        Try
            If chkZeroCompetenceWeight.Checked = True Then
                If mdtCompetenceTran IsNot Nothing AndAlso mdtCompetenceTran.Rows.Count <= 0 Then
                    For Each xGridItem As DataGridItem In lvData.Items
                        Dim chkbox As CheckBox = CType(xGridItem.Cells(0).FindControl("chkItemSelect"), CheckBox)
                        'Shani(24-Feb-2016) -- Start
                        'If chkbox IsNot Nothing AndAlso chkbox.Checked = True Then
                        If chkbox IsNot Nothing AndAlso chkbox.Checked = True AndAlso CBool(xGridItem.Cells(5).Text) = False Then
                            'Shani(24-Feb-2016) -- End
                            Call Evaluated_Data(0, xGridItem.ItemIndex, xGridItem)
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP [23 FEB 2016] -- END


    'Shani (01-Aug-2016) -- Start
    'Enhancement - 

    Protected Function CheckIsValidData() As Boolean
        Dim d As Decimal = 0
        Try
            Dim arrItemWeight = (From dRow In lvData.Items.Cast(Of DataGridItem)() _
                                       .Where(Function(X) CBool(X.Cells(5).Text) = False And Decimal.TryParse(CType(X.Cells(3).FindControl("txtItemWeight"), TextBox).Text, d) = True And CType(X.Cells(0).FindControl("chkItemSelect"), CheckBox).Checked = True) _
                                      .Select(Function(x) CDec(CType(x.Cells(3).FindControl("txtItemWeight"), TextBox).Text)))
            If arrItemWeight.Count > 0 Then
                mDecAssignedWeight = arrItemWeight.Sum
            Else
                mDecAssignedWeight = 0
            End If

            For Each dgItem As DataGridItem In lvData.Items
                If CBool(dgItem.Cells(5).Text) OrElse CBool(dgItem.Cells(10).Text) Then Continue For
                Dim iTxt As TextBox = CType(dgItem.Cells(3).FindControl("txtItemWeight"), TextBox)
                Dim xWgt As Decimal = 0 : Decimal.TryParse(iTxt.Text, xWgt)
                If xWgt > 0 Then
                    Dim xRow As DataGridItem = CType(iTxt.NamingContainer, DataGridItem)
                    If mDecAssignedWeight > txtWeight.Text Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry you cannot assign more items. Reason the selected group exceeds the total weight of ") & txtWeight.Text, Me)
                        Return False
                    Else
                        mdtTran.Rows(xRow.ItemIndex).Item("weight") = CDec(iTxt.Text)
                        Call Evaluated_Data(CDec(iTxt.Text), xRow.ItemIndex, xRow)
                    End If
                Else
                    iTxt.Text = "0.00"
                End If
            Next
            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function
    'Shani (01-Aug-2016) -- End


#End Region

#Region " Button's Event(s) "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            'S.SANDEEP [09 FEB 2015] -- START
            'If CInt(cboAssessGroup.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Assessment Group and Period are mandatory information. Please selecte Assessment Group and Period to continue."), Me)
            '    Exit Sub
            'End If

            If cboEmployee.Enabled = True Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage("Sorry, employee is mandatory information. Please select employee to continue.", Me)
                    Exit Sub
                End If
            End If

            'S.SANDEEP [29 DEC 2015] -- START
            If CInt(IIf(cboAssessGroup.SelectedValue = "", 0, cboAssessGroup.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage("Sorry, Assessment Group is mandatory information. Please selecte Assessment Group to continue.", Me)
                Exit Sub
            End If
            'S.SANDEEP [29 DEC 2015] -- END

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Sorry, period is mandatory information. Please select period to continue.", Me)
                Exit Sub
            End If


            Dim xEmpId As Integer = 0
            If CBool(Session("Self_Assign_Competencies")) = True Then
                xEmpId = CInt(cboEmployee.SelectedValue)
            End If
            If objAssignCompetenceMst.isExist(CInt(cboAssessGroup.SelectedValue), CInt(cboPeriod.SelectedValue), , xEmpId) Then
                'S.SANDEEP [29 DEC 2015] -- START
                'DisplayMessage.DisplayMessage(Language.getMessage("clsassess_competence_assign_master", 1, "Sorry this group is already defined for the selected period. Please define new group."), Me)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, Competencies are already defined for this employee for the competence group and period selected, use Edit option if you wish to Add/Edit/Delete the competencies."), Me)
                'S.SANDEEP [29 DEC 2015] -- END
                Exit Sub
            End If
            'S.SANDEEP [09 FEB 2015] -- END

            Call FillGrid()
            If menAction <> enAction.EDIT_ONE Then
                'S.SANDEEP [13-FEB-2017] -- START
                'ISSUE/ENHANCEMENT : ISSUE FOR DATATABLE IS NOTHING
                'mdtCompetenceTran.Rows.Clear()
                If mdtCompetenceTran IsNot Nothing Then mdtCompetenceTran.Rows.Clear()
                'S.SANDEEP [13-FEB-2017] -- END
            End If
            Call Enable_Disable(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboAssessGroup.SelectedValue = 0 : cboPeriod.SelectedValue = 0 : txtWeight.Text = ""
            lvData.DataSource = Nothing : lvData.DataBind() : mDecAssignedWeight = 0 : iVoidReason = ""
            Call Enable_Disable(True) : If mdtTran IsNot Nothing Then mdtTran.Rows.Clear()
            If CBool(Session("Self_Assign_Competencies")) = True Then
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    cboEmployee.SelectedValue = 0
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then Exit Sub

            'Shani (01-Aug-2016) -- Start
            'Enhancement -
            If CheckIsValidData() = False Then Exit Sub
            'Shani (01-Aug-2016) -- End

            Call SetValue()

            'S.SANDEEP [23 FEB 2016] -- START
            Call Generate_Competence_Table(lvData)
            'S.SANDEEP [23 FEB 2016] -- END

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objAssignCompetenceMst.Update(mdtCompetenceTran)
            Else
                blnFlag = objAssignCompetenceMst.Insert(mdtCompetenceTran)
            End If
            If blnFlag Then
                If menAction = enAction.ADD_CONTINUE Then
                    objAssignCompetenceMst = New clsassess_competence_assign_master
                    objAssignCompetenceTrn = New clsassess_competence_assign_tran
                    mdtCompetenceTran.Rows.Clear() : mintComptenceAssignMasterId = 0
                    mDecAssignedWeight = 0 : iVoidReason = ""
                    Call btnReset_Click(sender, e)
                Else
                    mintComptenceAssignMasterId = objAssignCompetenceMst._Assigncompetenceunkid
                    Call btnClose_Click(sender, e)
                End If
                'S.SANDEEP [09 FEB 2015] -- START
            Else
                If objAssignCompetenceMst._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssignCompetenceMst._Message, Me)
                End If
                'S.SANDEEP [09 FEB 2015] -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'S.SANDEEP [31 DEC 2015] -- START
            Session("SelectedEmpId") = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue))
            'S.SANDEEP [31 DEC 2015] -- END
            Call ClearForm_Values()
            Response.Redirect(Session("servername") & "~/Assessment New/Competencies/wpg_AssignedCompetenciesList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Call ClearForm_Values()
    '        Response.Redirect(Session("servername") & "~/Assessment New/Competencies/wpg_AssignedCompetenciesList.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    Protected Sub delReason_buttonDelReasonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonNo_Click
        Try
            Call Grid_Operation(lvData, False, True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Try
            If delReason.Reason.ToString.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage("Please enter valid reason.", Me)
                delReason.Show()
                Exit Sub
            Else
                iVoidReason = delReason.Reason
                Call Grid_Operation(lvData, True)
                delReason.Hide()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Controls Event(s) "

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try

            'S.SANDEEP [23 FEB 2016] -- START
            'If ConfigParameter._Object._Self_Assign_Competencies = True Then
            If CBool(Session("Self_Assign_Competencies")) Then
                'S.SANDEEP [23 FEB 2016] -- END
                Dim objAGroup As New clsassess_group_master
                Dim dt As DataTable = Nothing : Dim dsCombo As New DataSet
                Dim xGrpIds As String = String.Empty

                dsCombo = objAGroup.getListForCombo("List", True)
                If CInt(cboEmployee.SelectedValue) > 0 Then

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'xGrpIds = objAGroup.GetCSV_AssessGroupIds(CInt(cboEmployee.SelectedValue))
                    'Shani (23-Nov-2016) -- Start
                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                    'xGrpIds = objAGroup.GetCSV_AssessGroupIds(CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
                    xGrpIds = objAGroup.GetCSV_AssessGroupIds(CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")), CInt(cboPeriod.SelectedValue))
                    'Shani (23-Nov123-2016-2016) -- End
                    'Shani(20-Nov-2015) -- End

                    If xGrpIds.Trim.Length > 0 Then
                        xGrpIds &= ",0"
                    Else
                        xGrpIds = "0"
                    End If
                Else
                    xGrpIds = "0"
                End If
                dt = New DataView(dsCombo.Tables(0), "assessgroupunkid IN(" & xGrpIds & ")", "", DataViewRowState.CurrentRows).ToTable
                With cboAssessGroup
                    .DataValueField = "assessgroupunkid"
                    .DataTextField = "name"
                    .DataSource = dt
                    .DataBind()
                    'S.SANDEEP [23 DEC 2015] -- START
                    '.SelectedValue = 0
                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        'S.SANDEEP [23 FEB 2016] -- START
                        '.SelectedIndex = 1
                        If menAction <> enAction.EDIT_ONE Then
                            'Shani (01-Aug-2016) -- Start
                            'Enhancement -
                            '.SelectedValue = 1
                            If xGrpIds.Trim.Length > 0 Then
                                .SelectedValue = xGrpIds.Split(",")(0)
                            End If
                            'Shani (01-Aug-2016) -- End
                        End If
                        'S.SANDEEP [23 FEB 2016] -- END
                        Call cboAssessGroup_SelectedIndexChanged(sender, e)
                    Else
                        .SelectedIndex = 0
                    End If
                    'S.SANDEEP [23 DEC 2015] -- END
                End With
                objAGroup = Nothing
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub cboAssessGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboAssessGroup.SelectedIndexChanged, cboPeriod.SelectedIndexChanged 'S.SANDEEP |12-FEB-2019| -- START {cboPeriod} -- END
        Try
            If CInt(cboAssessGroup.SelectedValue) > 0 Then
                Dim objAGroup As New clsassess_group_master
                objAGroup._Assessgroupunkid = CInt(cboAssessGroup.SelectedValue)
                txtWeight.Text = objAGroup._Weight
                objAGroup = Nothing

                'S.SANDEEP [23 FEB 2016] -- START

                'S.SANDEEP [04-JAN-2018] -- START
                'Dim decWgt As Decimal = 0
                'Decimal.TryParse(txtWeight.Text, decWgt)
                'If decWgt > 0 Then
                '    chkZeroCompetenceWeight.Checked = False : chkZeroCompetenceWeight.Visible = False
                'Else
                '    chkZeroCompetenceWeight.Visible = True
                'End If
                'S.SANDEEP [04-JAN-2018] -- END



                'S.SANDEEP [23 FEB 2016] -- END


                'S.SANDEEP [31 DEC 2015] -- START
                If CInt(IIf(cboAssessGroup.SelectedValue = "", 0, cboAssessGroup.SelectedValue)) > 0 AndAlso _
                   CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) > 0 Then
                    'S.SANDEEP [02 Jan 2016] -- START
                    If menAction <> enAction.EDIT_ONE Then
                        Call btnSearch_Click(New Object, New EventArgs)
                    End If
                    'Call FillGrid()
                    'S.SANDEEP [02 Jan 2016] -- END
                End If
                'S.SANDEEP [31 DEC 2015] -- END
            Else
                txtWeight.Text = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub chkItemSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkAll As CheckBox = CType(sender, CheckBox)
            If Me.Session("chkAll") IsNot Nothing Then
                Me.Session("chkAll") = chkAll
            Else
                Me.Session.Add("chkAll", chkAll)
            End If
            Dim xRow As DataGridItem = CType(chkAll.NamingContainer, DataGridItem)

            'S.SANDEEP [29 DEC 2015] -- START
            If xRow IsNot Nothing Then
                xRow.BackColor = Color.White
            End If
            'S.SANDEEP [29 DEC 2015] -- END

            If menAction = enAction.EDIT_ONE Then
                If chkAll.Checked = False Then
                    If iVoidReason.Trim.Length <= 0 Then
                        If iVoidReason.Trim.Length <= 0 Then
                            '
                            'Dim itxt As TextBox = CType(xRow.FindControl("txtItemWeight"), TextBox)
                            'If itxt IsNot Nothing Then
                            '    Select Case chkAll.Checked
                            '        Case True
                            '            itxt.Enabled = True
                            '        Case False
                            '            itxt.Text = ""
                            '            itxt.Enabled = False
                            '    End Select
                            'End If

                            'Shani (01-Aug-2016) -- Start
                            'Enhancement -
                            'delReason.Show()
                            'Exit Sub
                            If CInt(xRow.Cells(7).Text) > 0 Then
                                delReason.Show()
                                Exit Sub
                            End If
                            'Shani (01-Aug-2016) -- End
                        End If
                    End If
                End If
            End If

            'If CBool(xRow.Cells(5).Text) = True Then
            '    For Each iRow As DataGridItem In lvData.Items
            '        If iRow.Cells(4).Text = xRow.Cells(4).Text AndAlso CBool(iRow.Cells(5).Text) = False Then
            '            Dim chk As CheckBox = CType(iRow.FindControl("chkItemSelect"), CheckBox)
            '            chk.Checked = chkAll.Checked
            '            mdtTran.Rows(iRow.ItemIndex)("ischeck") = chkAll.Checked
            '            Dim itxt As TextBox = CType(iRow.FindControl("txtItemWeight"), TextBox)
            '            Select Case chkAll.Checked
            '                Case True
            '                    itxt.Enabled = True
            '                Case False
            '                    itxt.Text = ""
            '                    itxt.Enabled = False
            '            End Select
            '        End If
            '    Next
            'Else
            '    Dim chk As CheckBox = CType(xRow.FindControl("chkItemSelect"), CheckBox)
            '    chk.Checked = chkAll.Checked
            '    mdtTran.Rows(xRow.ItemIndex)("ischeck") = chkAll.Checked
            '    Dim itxt As TextBox = CType(xRow.FindControl("txtItemWeight"), TextBox)
            '    Select Case chkAll.Checked
            '        Case True
            '            itxt.Enabled = True
            '        Case False
            '            itxt.Text = ""
            '            itxt.Enabled = False
            '    End Select
            'End If

            Call Grid_Operation(lvData, True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP [05 DEC 2015] -- START
    Protected Sub link_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            txtData.Text = ""
            Dim lnkWhatIsThis As LinkButton = CType(sender, LinkButton)
            Dim xRow As DataGridItem = CType(lnkWhatIsThis.NamingContainer, DataGridItem)
            If CBool(xRow.Cells(5).Text) = True Then
                Dim objCOMaster As New clsCommon_Master
                objCOMaster._Masterunkid = CInt(xRow.Cells(4).Text)
                If objCOMaster._Description <> "" Then
                    txtData.Text = objCOMaster._Description
                    ModalPopupExtender1.Show()
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No description set for the selected information."), Me)
                End If
                objCOMaster = Nothing
            ElseIf CBool(xRow.Cells(5).Text) = False Then
                Dim objCPMsater As New clsassess_competencies_master
                objCPMsater._Competenciesunkid = CInt(xRow.Cells(9).Text)
                If objCPMsater._Description <> "" Then
                    txtData.Text = objCPMsater._Description
                    ModalPopupExtender1.Show()
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, No description set for the selected information."), Me)
                End If
                objCPMsater = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP [05 DEC 2015] -- END

    Protected Sub txtItemWeight_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim iTxt As TextBox = CType(sender, TextBox)
            Dim xWgt As Decimal = 0 : Decimal.TryParse(iTxt.Text, xWgt)
            If xWgt > 0 Then
                Dim xRow As DataGridItem = CType(iTxt.NamingContainer, DataGridItem)
                If CBool(xRow.Cells(5).Text) = False Then
                    'S.SANDEEP [10 FEB 2015] -- START
                    'mDecAssignedWeight = CDec(mdtTran.Compute("SUM(weight)", "IsGrp = False"))

                    'S.SANDEEP [02 JAN 2016] -- START
                    'mDecAssignedWeight = CDec(mdtTran.Compute("SUM(weight)", "IsGrp = False AND competenciesunkid <> '" & xRow.Cells(9).Text & "'"))
                    If IsDBNull(mdtTran.Compute("SUM(weight)", "IsGrp = False AND competenciesunkid <> '" & xRow.Cells(9).Text & "'")) = False Then
                        mDecAssignedWeight = CDec(mdtTran.Compute("SUM(weight)", "IsGrp = False AND competenciesunkid <> '" & xRow.Cells(9).Text & "'"))
                    Else
                        mDecAssignedWeight = 0
                    End If
                    'S.SANDEEP [02 JAN 2016] -- END

                    'S.SANDEEP [10 FEB 2015] -- END
                    If (mDecAssignedWeight + CDec(iTxt.Text)) > txtWeight.Text Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry you cannot assign more items. Reason the selected group exceeds the total weight of ") & txtWeight.Text, Me)
                        mdtTran.Rows(xRow.ItemIndex).Item("weight") = 0
                        'S.SANDEEP [09 FEB 2015] -- START
                        iTxt.Text = 0
                        'S.SANDEEP [09 FEB 2015] -- END
                        Exit Sub
                    Else
                        mdtTran.Rows(xRow.ItemIndex).Item("weight") = CDec(iTxt.Text)
                        Call Evaluated_Data(CDec(mdtTran.Rows(xRow.ItemIndex).Item("weight")), xRow.ItemIndex, xRow)
                    End If
                End If
            Else
                iTxt.Text = ""
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub txtWeight_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtWeight.TextChanged
        Try
            Dim mDecWgt As Decimal = 0 : Decimal.TryParse(txtWeight.Text, mDecWgt)
            If mDecWgt > 0 Then
                lvData.Items(0).Enabled = True
            Else
                lvData.Items(0).Enabled = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Data Grid Event(s) "

    Protected Sub lvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles lvData.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                'S.SANDEEP [05 DEC 2015] -- START
                'If CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then
                '    For i = 2 To lvData.Columns.Count - 1
                '        e.Item.Cells(i).Visible = False
                '    Next
                '    e.Item.Cells(1).ColumnSpan = lvData.Columns.Count - 1
                '    e.Item.Cells(0).CssClass = "GroupHeaderStyle"
                '    e.Item.Cells(1).CssClass = "GroupHeaderStyle"
                'ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then
                '    e.Item.Cells(1).Text = "&nbsp;" & e.Item.Cells(1).Text
                '    Dim itxt As TextBox = CType(e.Item.Cells(3).FindControl("txtItemWeight"), TextBox)
                '    If itxt.Text <> "" Then itxt.Enabled = True
                'End If

                If CBool(DataBinder.Eval(e.Item.DataItem, "IsPGrp")) = True Then
                    Dim chkbox As CheckBox = CType(e.Item.Cells(0).FindControl("chkItemSelect"), CheckBox)
                    If chkbox IsNot Nothing Then
                        chkbox.Visible = False
                    End If
                    For i = 2 To lvData.Columns.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next
                    e.Item.Cells(1).ColumnSpan = lvData.Columns.Count - 1
                    e.Item.Cells(0).CssClass = "MainGroupHeaderStyle"
                    e.Item.Cells(1).CssClass = "MainGroupHeaderStyle"

                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then

                    'S.SANDEEP [23 DEC 2015] -- START
                    If Session("OnlyOneItemPerCompetencyCategory") Then
                        Dim chkbox As CheckBox = CType(e.Item.Cells(0).FindControl("chkItemSelect"), CheckBox)
                        If chkbox IsNot Nothing Then
                            chkbox.Visible = False
                        End If
                    End If
                    'S.SANDEEP [23 DEC 2015] -- END

                    Dim CellCout As Integer = 0

                    For i = 2 To lvData.Columns.Count - 1
                        If lvData.Columns(i).Visible Then
                            e.Item.Cells(i).Visible = False
                            CellCout += 1
                        End If
                    Next

                    e.Item.Cells(1).ColumnSpan = CellCout
                    e.Item.Cells(0).CssClass = "GroupHeaderStyle"
                    e.Item.Cells(1).CssClass = "GroupHeaderStyle"

                    e.Item.Cells(lvData.Columns.Count - 1).CssClass = "GroupHeaderStyle"
                    e.Item.Cells(lvData.Columns.Count - 1).Visible = True

                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then

                    'S.SANDEEP [31 DEC 2015] -- START
                    Dim chkbox As CheckBox = CType(e.Item.Cells(0).FindControl("chkItemSelect"), CheckBox)
                    If chkbox.Checked = True Then
                        e.Item.BackColor = Color.SkyBlue
                    End If
                    'S.SANDEEP [31 DEC 2015] -- END

                    'S.SANDEEP [23 FEB 2016] -- START
                    If (Session("LoginBy") = Global.User.en_loginby.User) Then
                        If Session("AllowtoChangePreassignedCompetencies") = False Then
                            If chkbox IsNot Nothing Then
                                chkbox.Enabled = False
                            End If
                        End If
                    End If
                    'S.SANDEEP [23 FEB 2016] -- END


                    e.Item.Cells(1).Text = "&nbsp;" & e.Item.Cells(1).Text
                    Dim itxt As TextBox = CType(e.Item.Cells(3).FindControl("txtItemWeight"), TextBox)
                    If itxt.Text <> "" Then itxt.Enabled = True
                End If
                'S.SANDEEP [05 DEC 2015] -- END

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

    '#Region "Page Method(S)"
    '    'ByVal mintRow As Integer, ByVal mintCol As Integer, 
    '    <WebMethod()> Public Shared Function UpdateTable(ByVal mdecValue As String) As String

    '        Return " Score : " & mdecValue
    '    End Function

    '#End Region

End Class
