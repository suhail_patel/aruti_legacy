﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing
Imports System.Web.UI.DataVisualization.Charting

#End Region

Partial Class wPg_ApprovalList
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmApproveRejectCalibrationList"
    Private ReadOnly mstrModuleName1 As String = "frmApproveRejectCalibrationAddEdit"
    Private blnShowApplyCalibrationpopup As Boolean = False
    Private objScoreCalibrateApproval As New clsScoreCalibrationApproval
    Private mdtApplyNew As DataTable = Nothing
    Private mdtFullDataTable As DataTable = Nothing
    Private mstrAdvanceFilter As String = String.Empty
    Private mView As DataView = Nothing
    Private mlbnIsRejected As Boolean = False
    Private mdtListDataTable As DataTable = Nothing
    Private mintCalibrationUnkid As Integer = 0
    Private mintEmployeeUnkid As Integer = 0
    Private mintPeriodUnkid As Integer = 0
    Private mblnIsGrp As Boolean = False
    Private mintPriority As Integer = 0
    Private mintMappingUnkid As Integer = 0
    Private mintgrpId As Int64 = 0
    Private mblnIsExpand As Boolean = False
    Private mdtRatings As DataTable = Nothing
    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Private mblnShowHPOCurve As Boolean = False
    'S.SANDEEP |27-JUL-2019| -- END

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Private mstrPreviousUser As String = String.Empty
    'S.SANDEEP |26-AUG-2019| -- END

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Private mblnShowMyReport As Boolean = False
    'S.SANDEEP |26-AUG-2019| -- END
    Private mstrChartAdvanceFilter As String = String.Empty

    'S.SANDEEP |25-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Private mstrReportAdvanceFilter As String = String.Empty
    'S.SANDEEP |25-OCT-2019| -- END

#End Region

#Region " Common Part "

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Dim objRating As New clsAppraisal_Rating
        Try
            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                           Session("UserId"), _
                                           Session("Fin_year"), _
                                           Session("CompanyUnkId"), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           Session("UserAccessModeSetting"), True, _
                                           Session("IsIncludeInactiveEmp"), "List", True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                .DataBind()
            End With
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            With cboRptEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables("List").Copy()
                .SelectedValue = 0
                .DataBind()
            End With
            'S.SANDEEP |26-AUG-2019| -- END

            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            Dim mintCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With
            With cboSPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            With cboChPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With

            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            With cboRptPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With
            Call cboRptPeriod_SelectedIndexChanged(cboRptPeriod, New EventArgs())
            'S.SANDEEP |26-AUG-2019| -- END

            Dim objCScoreMaster As New clsComputeScore_master
            dsList = objCScoreMaster.GetDisplayScoreOption("List", False, Nothing)
            With cbochDisplay
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
                .DataBind()
            End With
            objCScoreMaster = Nothing
            'S.SANDEEP |27-JUL-2019| -- END

            dsList = objScoreCalibrateApproval.GetStatusList("List", True, Nothing, True)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objRating.getComboList("List", True)
            With cbocRating
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
            With cbopRatingFrm
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = 0
            End With
            With cbopRatingTo
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = 0
            End With
            With cbocRatingFrm
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = 0
            End With
            With cbocRatingTo
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = 0
            End With
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            'With cboRatingFrom
            '    .DataValueField = "id"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables(0).Copy()
            '    .DataBind()
            '    .SelectedValue = 0
            'End With
            'With cboRatingTo
            '    .DataValueField = "id"
            '    .DataTextField = "name"
            '    .DataSource = dsList.Tables(0).Copy()
            '    .DataBind()
            '    .SelectedValue = 0
            'End With
            'mdtRatings = dsList.Tables(0).Copy()
            If dsList.Tables(0).Select("Id > 0").Length > 0 Then
                mdtRatings = dsList.Tables(0).Select("Id > 0").CopyToDataTable().Copy
            Else
                mdtRatings = dsList.Tables(0).Copy()
            End If
            Call Fill_FRating()
            dsList = (New clscalibrate_approver_master).GetList("List", True, "", 0, Nothing, True)
            With cboRptApprover
                .DataValueField = "mapuserunkid"
                .DataTextField = "dapprover"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
            'S.SANDEEP |26-AUG-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetApproverInfo()
        Dim dsAppr As New DataSet
        Try
            dsAppr = objScoreCalibrateApproval.GetApproverInfo(Session("UserId"))
            If dsAppr IsNot Nothing AndAlso dsAppr.Tables(0).Rows.Count > 0 Then
                txtApprover.Text = dsAppr.Tables(0).Rows(0).Item("username").ToString()
                txtLevel.Text = dsAppr.Tables(0).Rows(0).Item("levelname").ToString()
                mintPriority = CInt(dsAppr.Tables(0).Rows(0).Item("priority"))
                mintMappingUnkid = CInt(dsAppr.Tables(0).Rows(0).Item("mappingunkid"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Private Sub Fill_FRating()
        Try
            Dim objRating As New clsAppraisal_Rating
            Dim dsList As New DataSet
            dsList = objRating.getComboList("List", False)
            gvFilterRating.AutoGenerateColumns = False
            gvFilterRating.DataSource = dsList
            gvFilterRating.DataBind()
            objRating = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |26-AUG-2019| -- END  

#End Region

#Region " Form's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                'S.SANDEEP |27-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()
                'S.SANDEEP |27-JUL-2019| -- END
                Call FillCombo() : SetApproverInfo()
            Else
                mintPriority = Me.ViewState("mintPriority")
                blnShowApplyCalibrationpopup = Me.ViewState("blnShowApplyCalibrationpopup")
                mdtApplyNew = Me.ViewState("mdtApplyNew")
                mstrAdvanceFilter = Me.ViewState("mstrAdvanceFilter")
                mdtFullDataTable = Me.ViewState("mdtFullDataTable")
                mView = Me.ViewState("mView")
                mlbnIsRejected = Me.ViewState("mlbnIsRejected")
                mdtListDataTable = Me.ViewState("mdtListDataTable")
                mintCalibrationUnkid = Me.ViewState("mintCalibrationUnkid")
                mintEmployeeUnkid = Me.ViewState("mintEmployeeUnkid")
                mintPeriodUnkid = Me.ViewState("mintPeriodUnkid")
                mblnIsGrp = Me.ViewState("mblnIsGrp")
                mintMappingUnkid = Me.ViewState("mintMappingUnkid")
                mintgrpId = Me.ViewState("mintgrpId")
                mblnIsExpand = Me.ViewState("mlbnIsExpand")
                mdtRatings = Me.ViewState("mdtRatings")
                'S.SANDEEP |27-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                mblnShowHPOCurve = Me.ViewState("mblnShowHPOCurve")
                'S.SANDEEP |27-JUL-2019| -- END

                'S.SANDEEP |26-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                mstrPreviousUser = Me.ViewState("mstrPreviousUser")
                'S.SANDEEP |26-AUG-2019| -- END

                'S.SANDEEP |26-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                mblnShowMyReport = Me.ViewState("mblnShowMyReport")
                'S.SANDEEP |26-AUG-2019| -- END
                mstrChartAdvanceFilter = Me.ViewState("mstrChartAdvanceFilter")

                'S.SANDEEP |25-OCT-2019| -- START
                'ISSUE/ENHANCEMENT : Calibration Issues
                mstrReportAdvanceFilter = Me.ViewState("mstrReportAdvanceFilter")
                'S.SANDEEP |25-OCT-2019| -- END

            End If
            If blnShowApplyCalibrationpopup Then
                popupApproverUseraccess.Show()
            End If

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            If mblnShowHPOCurve Then
                popupHPOChart.Show()
            End If
            'S.SANDEEP |27-JUL-2019| -- END


            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            If mblnShowMyReport Then
                popupMyReport.Show()
            End If
            'S.SANDEEP |26-AUG-2019| -- END

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            cbocRating.Enabled = Session("AllowToEditCalibratedScore")
            txtIRemark.Enabled = Session("AllowToEditCalibratedScore")
            btnIApply.Enabled = Session("AllowToEditCalibratedScore")
            'S.SANDEEP |27-JUL-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintPriority") = mintPriority

            Me.ViewState("blnShowApplyCalibrationpopup") = blnShowApplyCalibrationpopup
            Me.ViewState("mdtApplyNew") = mdtApplyNew
            Me.ViewState("mstrAdvanceFilter") = mstrAdvanceFilter
            Me.ViewState("mdtFullDataTable") = mdtFullDataTable
            Me.ViewState("mView") = mView
            Me.ViewState("mlbnIsRejected") = mlbnIsRejected
            Me.ViewState("mdtListDataTable") = mdtListDataTable

            Me.ViewState("mintCalibrationUnkid") = mintCalibrationUnkid
            Me.ViewState("mintEmployeeUnkid") = mintEmployeeUnkid
            Me.ViewState("mintPeriodUnkid") = mintPeriodUnkid
            Me.ViewState("mblnIsGrp") = mblnIsGrp
            Me.ViewState("mintMappingUnkid") = mintMappingUnkid
            Me.ViewState("mintgrpId") = mintgrpId
            Me.ViewState("mlbnIsExpand") = mblnIsExpand
            Me.ViewState("mdtRatings") = mdtRatings
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            Me.ViewState("mblnShowHPOCurve") = mblnShowHPOCurve
            'S.SANDEEP |27-JUL-2019| -- END

            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            Me.ViewState("mstrPreviousUser") = mstrPreviousUser
            'S.SANDEEP |26-AUG-2019| -- END

            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            Me.ViewState("mblnShowMyReport") = mblnShowMyReport
            'S.SANDEEP |26-AUG-2019| -- END

            Me.ViewState("mstrChartAdvanceFilter") = mstrChartAdvanceFilter

            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            Me.ViewState("mstrReportAdvanceFilter") = mstrReportAdvanceFilter
            'S.SANDEEP |25-OCT-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " List Methods "

#Region " Button's Event(s) "

    Protected Sub btnShowAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAll.Click
        Try
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            'mdtFullDataTable = objScoreCalibrateApproval.GetEmployeeApprovalData(CInt(cboPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), False, Nothing, mintPriority, True, "", "")
            'gvMyReport.AutoGenerateColumns = False
            'gvMyReport.DataSource = mdtFullDataTable
            'gvMyReport.DataBind()

            If txtApprover.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 100, "Sorry, No approver defined with the selected login."), Me)
                Exit Sub
            End If

            Call btnRptReset_Click(btnRptReset, New EventArgs())
            mblnShowMyReport = True
            'S.SANDEEP |26-AUG-2019| -- END
            popupMyReport.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Protected Sub btnCloseMyReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseMyReport.Click
        Try
            mblnShowMyReport = False
            popupMyReport.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |26-AUG-2019| -- END

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Sorry, Period is mandatory information, Please select period to continue"), Me)
                Exit Sub
            End If

            If txtApprover.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 100, "Sorry, No approver defined with the selected login."), Me)
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath") & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Protected Sub btnDisplayCurve_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisplayCurve.Click
        Try
            mstrChartAdvanceFilter = ""
            cboChPeriod.SelectedIndex = 0
            cbochDisplay.SelectedIndex = 0
            dgvCalibData.DataSource = Nothing
            dgvCalibData.DataBind()
            mblnShowHPOCurve = True
            popupHPOChart.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnChGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChGenerate.Click
        Try
            If cboChPeriod.SelectedValue <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage("frmPerformanceEvaluationReport", 1, "Period is mandatory information. Please select Period to continue."), Me)
                cboChPeriod.Focus()
                Exit Sub
            End If

            Dim dsDataSet As New DataSet
            Dim objPerformance As New ArutiReports.clsPerformanceEvaluationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            'S.SANDEEP |22-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'dsDataSet = objPerformance.GenerateChartData(CInt(cboChPeriod.SelectedValue), _
            '                                                Session("Database_Name").ToString, _
            '                                                CInt(Session("UserId")), _
            '                                                CInt(Session("Fin_year")), _
            '                                                CInt(Session("CompanyUnkId")), _
            '                                                CStr(Session("UserAccessModeSetting")), True, False, "List", , , , , IIf(cbochDisplay.SelectedValue = "", clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE, cbochDisplay.SelectedValue), mstrChartAdvanceFilter)

            dsDataSet = objPerformance.GenerateChartData(CInt(cboChPeriod.SelectedValue), _
                                                            Session("Database_Name").ToString, _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            CStr(Session("UserAccessModeSetting")), True, _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                            False, "List", , , , , IIf(cbochDisplay.SelectedValue = "", clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE, cbochDisplay.SelectedValue), mstrChartAdvanceFilter)
            'S.SANDEEP |22-OCT-2019| -- END

            If dsDataSet.Tables.Count > 0 Then
                dgvCalibData.DataSource = dsDataSet.Tables(0)
                dgvCalibData.DataBind()

                'S.SANDEEP |26-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                If dgvCalibData.Items.Count > 0 Then
                    Dim gvr As DataGridItem = Nothing
                    'S.SANDEEP |21-SEP-2019| -- START
                    'ISSUE/ENHANCEMENT : {HPO Chart Issue}
                    'gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(5).Text = "N").FirstOrDefault()
                    'If gvr IsNot Nothing Then
                    '    gvr.ForeColor = Color.Blue
                    'End If
                    'gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(5).Text = "A").FirstOrDefault()
                    'If gvr IsNot Nothing Then
                    '    gvr.ForeColor = Color.Tomato
                    'End If
                    'Dim xItems = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Select(Function(x) x.Cells(5))
                    'If xItems IsNot Nothing AndAlso xItems.Count > 0 Then
                    '    For Each item In xItems
                    '        item.Visible = False
                    '    Next
                    'End If
                    gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(6).Text = "N").FirstOrDefault()
                    If gvr IsNot Nothing Then
                        gvr.ForeColor = Color.Blue
                    End If
                    gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(6).Text = "A").FirstOrDefault()
                    If gvr IsNot Nothing Then
                        gvr.ForeColor = Color.Tomato
                    End If
                    Dim xItems = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Select(Function(x) x.Cells(6))
                    If xItems IsNot Nothing AndAlso xItems.Count > 0 Then
                        For Each item In xItems
                            item.Visible = False
                        Next
                    End If
                    'S.SANDEEP |21-SEP-2019| -- END

                End If
                'S.SANDEEP |26-AUG-2019| -- END

                chHpoCurve.ChartAreas(0).AxisX.Title = Language.getMessage(mstrModuleName, 100, "Ratings")
                chHpoCurve.ChartAreas(0).AxisY.Title = Language.getMessage(mstrModuleName, 101, "%")
                chHpoCurve.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chHpoCurve.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.Format = "{0.##}%"

                'S.SANDEEP |26-SEP-2019| -- START
                'ISSUE/ENHANCEMENT : {Calibration Issue}
                'S.SANDEEP |21-SEP-2019| -- START
                'ISSUE/ENHANCEMENT : {HPO Chart Issue}
                'chHpoCurve.ChartAreas(0).AxisY.ScaleBreakStyle.Enabled = True
                'S.SANDEEP |21-SEP-2019| -- END

                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.Interval = 10
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.IntervalOffset = 10
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.IntervalOffsetType = DateTimeIntervalType.Number
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.IntervalType = DateTimeIntervalType.Number
                chHpoCurve.ChartAreas(0).AxisY.MajorGrid.Enabled = False
                chHpoCurve.ChartAreas(0).AxisY.Maximum = 105
                chHpoCurve.ChartAreas(0).AxisY.Minimum = 0
                chHpoCurve.ChartAreas(0).AxisY.ScaleBreakStyle.MaxNumberOfBreaks = 1
                chHpoCurve.ChartAreas(0).AxisY.ScaleBreakStyle.Spacing = 0
                'S.SANDEEP |26-SEP-2019| -- END

                chHpoCurve.Series(0).Points.DataBindXY(dsDataSet.Tables("Chart").DefaultView, "erate", dsDataSet.Tables("Chart").DefaultView, "np")
                chHpoCurve.Series(1).Points.DataBindXY(dsDataSet.Tables("Chart").DefaultView, "erate", dsDataSet.Tables("Chart").DefaultView, "ap")

                chHpoCurve.Titles(0).Text = Language.getMessage(mstrModuleName, 200, "Performance HPO Curve") & vbCrLf & _
                                      cboChPeriod.SelectedItem.Text

                chHpoCurve.Titles(0).Font = New Font(chHpoCurve.Font.Name, 11, FontStyle.Bold)

                chHpoCurve.Series(0).LabelFormat = "{0.##}%"
                chHpoCurve.Series(1).LabelFormat = "{0.##}%"

                chHpoCurve.Series(0).IsVisibleInLegend = False
                chHpoCurve.Series(1).IsVisibleInLegend = False

                chHpoCurve.Series(0).Color = Color.Blue
                chHpoCurve.Series(1).Color = Color.Tomato

                Dim legendItem1 As New LegendItem()
                legendItem1.Name = Language.getMessage(mstrModuleName, 102, "Normal Distribution")
                legendItem1.ImageStyle = LegendImageStyle.Rectangle
                legendItem1.ShadowOffset = 2
                legendItem1.Color = chHpoCurve.Series(0).Color
                chHpoCurve.Legends(0).CustomItems.Add(legendItem1)

                Dim legendItem2 As New LegendItem()
                If dsDataSet.Tables("Chart").Rows.Count > 0 Then
                    legendItem2.Name = Language.getMessage(mstrModuleName, 103, "Actual Performance") & dsDataSet.Tables("Chart").Rows(0)("litem").ToString()
                Else
                    legendItem2.Name = Language.getMessage(mstrModuleName, 103, "Actual Performance")
                End If
                legendItem2.ImageStyle = LegendImageStyle.Rectangle
                legendItem2.ShadowOffset = 2
                legendItem2.Color = chHpoCurve.Series(1).Color
                chHpoCurve.Legends(0).CustomItems.Add(legendItem2)

                chHpoCurve.Series(0).ToolTip = chHpoCurve.Legends(0).CustomItems(0).Name & " : is #VAL "
                chHpoCurve.Series(1).ToolTip = chHpoCurve.Legends(0).CustomItems(1).Name & " : is #VAL "

                For i As Integer = 0 To chHpoCurve.Series.Count - 1
                    For Each dp As DataPoint In chHpoCurve.Series(i).Points
                        If dp.YValues(0) = 0 Then
                            dp.Label = " "
                        End If
                    Next
                Next
            Else
                DisplayMessage.DisplayMessage(Language.getMessage("frmPerformanceEvaluationReport", 4, "Sorry, No performace evaluation data present for the selected period."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |27-JUL-2019| -- END

    'S.SANDEEP |14-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : BOOLEAN VARIABLE WAS NOT SET TO FALSE, RESULT IN OPENING OF POP-UP AGAIN
    Protected Sub btnChClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChClose.Click
        Try
            mblnShowHPOCurve = False
            popupHPOChart.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |14-AUG-2019| -- END

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Protected Sub btnRptShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRptShow.Click
        Try
            Dim strFilter As String = String.Empty
            If CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) > 0 Then
                strFilter &= "AND iData.calibratnounkid = '" & CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) & "'"
            End If
            If CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) > 0 Then
                strFilter &= "AND iData.employeeunkid = '" & CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) & "'"
            End If
            If CInt(IIf(cboRptApprover.SelectedValue = "", 0, cboRptApprover.SelectedValue)) > 0 Then
                strFilter &= "AND Fn.userunkid = '" & CInt(IIf(cboRptApprover.SelectedValue = "", 0, cboRptApprover.SelectedValue)) & "'"
            End If
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'mdtFullDataTable = objScoreCalibrateApproval.GetEmployeeApprovalData(CInt(cboRptPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), False, Nothing, mintPriority, True, strFilter, "", True)
            mdtFullDataTable = objScoreCalibrateApproval.GetEmployeeApprovalData(CInt(cboRptPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), False, Nothing, mintPriority, True, strFilter, "", True, False, mstrReportAdvanceFilter)
            'S.SANDEEP |25-OCT-2019| -- END
            gvMyReport.AutoGenerateColumns = False
            gvMyReport.DataSource = mdtFullDataTable
            gvMyReport.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnRptReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRptReset.Click
        Try
            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            mstrReportAdvanceFilter = ""
            'S.SANDEEP |25-OCT-2019| -- END
            cboRptCalibNo.SelectedValue = 0
            cboRptEmployee.SelectedValue = 0
            cboRptApprover.SelectedValue = 0
            gvMyReport.AutoGenerateColumns = False
            gvMyReport.DataSource = Nothing
            gvMyReport.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExportMyReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportMyReport.Click
        Try
            If gvMyReport.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, there is no data in order to export."), Me)
                Exit Sub
            End If

            Dim strFilterCaption As String = ""
            If CInt(IIf(cboRptPeriod.SelectedValue = "", 0, cboRptPeriod.SelectedValue)) > 0 Then
                strFilterCaption &= "," & Language.getMessage(mstrModuleName, 20, "Period :") & " " & cboRptPeriod.SelectedItem.Text & " "
            End If
            If CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) > 0 Then
                strFilterCaption &= "," & Language.getMessage(mstrModuleName, 21, "Calibration No :") & " " & cboRptCalibNo.SelectedItem.Text & " "
            End If
            If CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) > 0 Then
                strFilterCaption &= "," & Language.getMessage(mstrModuleName, 22, "Employee :") & " " & cboRptEmployee.SelectedItem.Text & " "
            End If
            If CInt(IIf(cboRptApprover.SelectedValue = "", 0, cboRptApprover.SelectedValue)) > 0 Then
                strFilterCaption &= "," & Language.getMessage(mstrModuleName, 23, "Approver :") & " " & cboRptApprover.SelectedItem.Text & " "
            End If
            If strFilterCaption.Trim.Length > 0 Then strFilterCaption = Mid(strFilterCaption, 2)
            Dim objRpt As New ArutiReports.clsPerformanceEvaluationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            objRpt.Export_Calibration_Report(Session("UserId"), _
                                             Session("CompanyUnkId"), _
                                             mdtFullDataTable, strFilterCaption, _
                                             Language.getMessage(mstrModuleName, 24, "Approver Wise Status"), _
                                             IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False)

            If objRpt._FileNameAfterExported.Trim <> "" Then
                Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
                response.ClearContent()
                response.Clear()
                response.ContentType = "text/plain"
                response.AddHeader("Content-Disposition", "attachment; filename=" + objRpt._FileNameAfterExported + ";")
                response.TransmitFile(IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objRpt._FileNameAfterExported)
                response.Flush()
                response.Buffer = False
                response.End()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |26-AUG-2019| -- END

    'S.SANDEEP |26-SEP-2019| -- START
    'ISSUE/ENHANCEMENT : {Calibration Issue}
    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
    End Sub
    'S.SANDEEP |26-SEP-2019| -- END

    Protected Sub AdvanceFilter1_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AdvanceFilter1.buttonApply_Click
        Try
            mstrChartAdvanceFilter = AdvanceFilter1._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |25-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Protected Sub AdvRptFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AdvRptFilter.buttonApply_Click
        Try
            mstrReportAdvanceFilter = AdvRptFilter._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |25-OCT-2019| -- END


    Protected Sub btnChReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChReset.Click
        Try
            mstrChartAdvanceFilter = ""
            cboChPeriod.SelectedIndex = 0
            cbochDisplay.SelectedIndex = 0
            dgvCalibData.DataSource = Nothing
            dgvCalibData.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillGrid()
        Dim strFilter As String = String.Empty
        Try
            If txtCalibrationNo.Text.Trim.Length > 0 Then
                strFilter &= "AND iData.calibration_no LIKE '%" & txtCalibrationNo.Text & "%'"
            End If

            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) > 0 Then
                strFilter &= "AND iData.employeeunkid = '" & CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) & "' "
            End If

            If CInt(IIf(cboStatus.SelectedValue = "", 0, cboStatus.SelectedValue)) > 0 Then
                strFilter &= "AND CASE WHEN iData.isgrp = 0 THEN ISNULL(B.[iStatusId], iData.statusunkid) ELSE 1 END = '" & CInt(IIf(cboStatus.SelectedValue = "", 0, cboStatus.SelectedValue)) & "' "
            End If

            Dim mDecFromScore, mDecToScore As Decimal : mDecFromScore = 0 : mDecToScore = 0
            If cbopRatingFrm.SelectedIndex > 0 AndAlso cbopRatingTo.SelectedIndex > 0 Then
                If cbopRatingTo.SelectedIndex < cbopRatingFrm.SelectedIndex Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 11, "Sorry, To rating cannot be less than From rating. Please select higher rating to continue."), Me)
                    Exit Sub
                Else
                    Dim dr() As DataRow = mdtRatings.Select("id = '" & cbopRatingFrm.SelectedValue & "'")
                    If dr.Length > 0 Then mDecFromScore = dr(0)("scrf")
                    dr = mdtRatings.Select("id = '" & cbopRatingTo.SelectedValue & "'")
                    If dr.Length > 0 Then mDecToScore = dr(0)("scrt")
                    If strFilter.Trim.Length > 0 Then
                        strFilter &= "AND (iData.povisionscore >= " & mDecFromScore & " AND iData.povisionscore <= " & mDecToScore & ") "
                    Else
                        strFilter &= "AND (iData.povisionscore >= " & mDecFromScore & " AND iData.povisionscore <= " & mDecToScore & ") "
                    End If
                End If
            ElseIf cbopRatingFrm.SelectedIndex > 0 Then
                Dim dr() As DataRow = mdtRatings.Select("id = '" & cbopRatingFrm.SelectedValue & "'")
                If dr.Length > 0 Then mDecFromScore = dr(0)("scrf")
                If strFilter.Trim.Length > 0 Then
                    strFilter &= "AND (iData.povisionscore >= " & mDecFromScore & ") "
                Else
                    strFilter &= "AND (iData.povisionscore >= " & mDecFromScore & ") "
                End If
            ElseIf cbopRatingTo.SelectedIndex > 0 Then
                Dim dr() As DataRow = mdtRatings.Select("id = '" & cbopRatingTo.SelectedValue & "'")
                If dr.Length > 0 Then mDecToScore = dr(0)("scrt")
                If strFilter.Trim.Length > 0 Then
                    strFilter &= "AND (iData.povisionscore <= " & mDecToScore & ") "
                Else
                    strFilter &= "AND (iData.povisionscore <= " & mDecToScore & ") "
                End If
            End If

            If cbocRatingFrm.SelectedIndex > 0 AndAlso cbocRatingTo.SelectedIndex > 0 Then
                Dim dr() As DataRow = mdtRatings.Select("id = '" & cbocRatingFrm.SelectedValue & "'")
                If dr.Length > 0 Then mDecFromScore = dr(0)("scrf")
                dr = mdtRatings.Select("id = '" & cbocRatingTo.SelectedValue & "'")
                If dr.Length > 0 Then mDecToScore = dr(0)("scrt")
                If strFilter.Trim.Length > 0 Then
                    strFilter &= "AND (ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) >= " & mDecFromScore & " AND ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) <= " & mDecToScore & ") "
                Else
                    strFilter &= "AND (ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) >= " & mDecFromScore & " AND ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) <= " & mDecToScore & ") "
                End If
            ElseIf cbocRatingFrm.SelectedIndex > 0 Then
                Dim dr() As DataRow = mdtRatings.Select("id = '" & cbocRatingFrm.SelectedValue & "'")
                If dr.Length > 0 Then mDecFromScore = dr(0)("scrf")
                If strFilter.Trim.Length > 0 Then
                    strFilter &= "AND (ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) >= " & mDecFromScore & ") "
                Else
                    strFilter &= "AND (ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) >= " & mDecFromScore & ") "
                End If
            ElseIf cbocRatingTo.SelectedIndex > 0 Then
                Dim dr() As DataRow = mdtRatings.Select("id = '" & cbocRatingTo.SelectedValue & "'")
                If dr.Length > 0 Then mDecToScore = dr(0)("scrt")
                If strFilter.Trim.Length > 0 Then
                    strFilter &= "AND (ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) <= " & mDecToScore & ") "
                Else
                    strFilter &= "AND (ISNULL(NV.cscore,ISNULL([B].bscore,iData.calibratescore)) <= " & mDecToScore & ") "
                End If
            End If

            If txtRemark.Text.Trim.Length > 0 Then
                strFilter &= "AND CASE WHEN ISNULL(iData.calibration_remark,'') = '' THEN ISNULL(iData.c_remark,'') ELSE ISNULL(iData.calibration_remark,'') END LIKE '%" & txtRemark.Text & "%' "
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strFilter &= "AND " & mstrAdvanceFilter
            End If

            If strFilter.Trim.Length > 0 Then
                strFilter = strFilter.Substring(3)
            End If
            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'mdtListDataTable = objScoreCalibrateApproval.GetEmployeeApprovalData(CInt(cboPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), chkMyApproval.Checked, Nothing, mintPriority, True, strFilter, "")
            mdtListDataTable = objScoreCalibrateApproval.GetEmployeeApprovalData(CInt(cboPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), chkMyApproval.Checked, Nothing, mintPriority, True, strFilter, "", , True)
            'S.SANDEEP |25-OCT-2019| -- END
            gvCalibrateList.AutoGenerateColumns = False
            If mdtListDataTable.Rows.Count > 0 Then
                gvCalibrateList.DataSource = mdtListDataTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isgrp") = True Or x.Field(Of Boolean)("ispgrp") = True).CopyToDataTable()
                'gvCalibrateList.DataSource = mdtListDataTable
                gvCalibrateList.DataBind()
            Else
                gvCalibrateList.DataSource = mdtListDataTable
                gvCalibrateList.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Links Event(s) "

    Protected Sub lnkPlus_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mintgrpId = gvCalibrateList.DataKeys(row.RowIndex)("grpid")
            mblnIsExpand = True
            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'gvCalibrateList.DataSource = mdtListDataTable.AsEnumerable().Where(Function(x) x.Field(Of Int64)("grpid") = gvCalibrateList.DataKeys(row.RowIndex)("grpid") Or x.Field(Of Boolean)("isgrp") = True).CopyToDataTable()
            gvCalibrateList.DataSource = mdtListDataTable.AsEnumerable().Where(Function(x) x.Field(Of Int64)("grpid") = gvCalibrateList.DataKeys(row.RowIndex)("grpid") Or x.Field(Of Boolean)("isgrp") = True Or x.Field(Of Boolean)("ispgrp") = True).CopyToDataTable()
            'S.SANDEEP |25-OCT-2019| -- END

            'gvCalibrateList.DataSource = mdtListDataTable
            gvCalibrateList.DataBind()
            mintgrpId = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkMins_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mintgrpId = gvCalibrateList.DataKeys(row.RowIndex)("grpid")
            mblnIsExpand = False
            'S.SANDEEP |25-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : Calibration Issues
            'gvCalibrateList.DataSource = mdtListDataTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isgrp") = True).CopyToDataTable()
            gvCalibrateList.DataSource = mdtListDataTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isgrp") = True Or x.Field(Of Boolean)("ispgrp") = True).CopyToDataTable()
            'S.SANDEEP |25-OCT-2019| -- END
            gvCalibrateList.DataBind()
            mintgrpId = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            If CBool(gvCalibrateList.DataKeys(row.RowIndex)("isgrp").ToString) Then
                Dim blnFlag As Boolean = False
                blnFlag = objScoreCalibrateApproval.IsValidApprover(CInt(cboPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), mintPriority, gvCalibrateList.DataKeys(row.RowIndex)("grpid"), Nothing, True, "", "")
                If blnFlag = False Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, you cannot do approve/reject operation. Reason selected calibration batch is still pending for lower level approver."), Me)
                    Exit Sub
                End If
                blnFlag = False
                blnFlag = objScoreCalibrateApproval.IsValidOperation(CInt(cboPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), mintPriority, gvCalibrateList.DataKeys(row.RowIndex)("grpid"), Nothing, True, "", "")
                If blnFlag = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, you cannot do approve/reject operation. Reason the selected calibration batch is already approved or rejected."), Me)
                    Exit Sub
                End If
                cbocRating.SelectedValue = 0
                cboSPeriod.SelectedValue = gvCalibrateList.DataKeys(row.RowIndex)("periodunkid")
                'S.SANDEEP |26-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                'txtICalibrationNo.Text = gvCalibrateList.DataKeys(row.RowIndex)("calibration_no") 'row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text
                'S.SANDEEP |26-AUG-2019| -- END
                mintCalibrationUnkid = gvCalibrateList.DataKeys(row.RowIndex)("grpid")
                'S.SANDEEP |12-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : CLEARING CONTROLS AFTER SETTING VALUE
                txtIRemark.Text = ""
                'S.SANDEEP |12-AUG-2019| -- END
                cboSPeriod.Enabled = False
                blnShowApplyCalibrationpopup = True
                popupApproverUseraccess.Show()
                FillList("grpid = '" & gvCalibrateList.DataKeys(row.RowIndex)("grpid").ToString & "' AND isgrp = 0 AND userunkid = '" & Session("UserId") & "' AND iStatusId = '" & CInt(clsScoreCalibrationApproval.enCalibrationStatus.Submitted) & "'")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |26-SEP-2019| -- START
    'ISSUE/ENHANCEMENT : {Calibration Issue}
    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "AEM"
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |26-SEP-2019| -- END

    Protected Sub lnkChAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkChAllocation.Click
        Try
            AdvanceFilter1._Hr_EmployeeTable_Alias = "hremployee_master"
            AdvanceFilter1.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |25-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    Protected Sub lnkRptAdvFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRptAdvFilter.Click
        Try
            mstrReportAdvanceFilter = ""
            AdvRptFilter._Hr_EmployeeTable_Alias = "AEM"
            AdvRptFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |25-OCT-2019| -- END

#End Region

#Region " Grid Event(s) "

    'S.SANDEEP |25-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues

    'S.SANDEEP |10-OCT-2019| -- START
    'ISSUE/ENHANCEMENT : Calibration Issues
    'Protected Sub gvCalibrateList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCalibrateList.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            If CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("isgrp").ToString) = True Then
    '                e.Row.BackColor = Color.Silver
    '                e.Row.ForeColor = Color.Black
    '                e.Row.Font.Bold = True
    '                Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = True
    '                'If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpovision", False, True)).Text <> "&nbsp;" Then
    '                '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpovision", False, True)).Text = ""
    '                'End If
    '                'If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgcalibratescore", False, True)).Text <> "&nbsp;" Then
    '                '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgcalibratescore", False, True)).Text = ""
    '                'End If
    '                If mintgrpId = Convert.ToInt64(gvCalibrateList.DataKeys(e.Row.RowIndex)("grpid")) Then
    '                    Dim lnkMins As LinkButton = TryCast(e.Row.FindControl("lnkMins"), LinkButton)
    '                    Dim lnkPlus As LinkButton = TryCast(e.Row.FindControl("lnkPlus"), LinkButton)
    '                    If mblnIsExpand Then
    '                        lnkMins.Visible = True
    '                        lnkPlus.Visible = False
    '                    Else
    '                        lnkMins.Visible = False
    '                        lnkPlus.Visible = True
    '                    End If
    '                End If

    '                'S.SANDEEP |26-AUG-2019| -- START
    '                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    '                Dim strCaption As String = ""
    '                Dim strUser As String = gvCalibrateList.DataKeys(e.Row.RowIndex)("calibuser").ToString()
    '                strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text
    '                If gvCalibrateList.DataKeys(e.Row.RowIndex)("allocation").ToString().Trim <> "" Then
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 19, "By") & " " & strUser & " (" & gvCalibrateList.DataKeys(e.Row.RowIndex)("allocation").ToString() & ") "
    '                Else
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 19, "By") & " " & strUser
    '                End If
    '                'S.SANDEEP |26-AUG-2019| -- END

    '                If gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString().Trim <> "" Then
    '                    strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text
    '                    Dim oDate As Date = eZeeDate.convertDate(gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString()).Date
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 12, "Date") & " [" & oDate & "] "
    '                End If

    '                e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text & " - (" & Language.getMessage(mstrModuleName1, 13, "Total") & " : " & _
    '                mdtListDataTable.Select("grpid = '" & gvCalibrateList.DataKeys(e.Row.RowIndex)("grpid") & "' AND isgrp = 0").Length & ") "
    '            Else
    '                Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = False
    '                Dim lnkPlus As LinkButton = TryCast(e.Row.FindControl("lnkPlus"), LinkButton) : lnkPlus.Visible = False
    '                Dim lnkMins As LinkButton = TryCast(e.Row.FindControl("lnkMins"), LinkButton) : lnkMins.Visible = False
    '                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text <> "&nbsp;" Then
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text.ToString.Replace("/r/n", "&nbsp;")
    '                End If
    '            End If
    '            If CInt(gvCalibrateList.DataKeys(e.Row.RowIndex)("iStatusId").ToString) <> clsScoreCalibrationApproval.enCalibrationStatus.Submitted Then
    '                Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = False
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Protected Sub gvCalibrateList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCalibrateList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim intCount As Integer = 3 : Dim strCaption As String = String.Empty
                If CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("ispgrp").ToString) = True Then
                    For i = 3 To gvCalibrateList.Columns.Count - 1
                        If gvCalibrateList.Columns(i).Visible Then
                            e.Row.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Row.Cells(2).ColumnSpan = intCount
                    e.Row.Cells(0).CssClass = "MainGroupHeaderStyleLeft"
                    e.Row.Cells(1).CssClass = "MainGroupHeaderStyleLeft"
                    e.Row.Cells(2).CssClass = "MainGroupHeaderStyleLeft"
                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = False
                    Dim lnkMins As LinkButton = TryCast(e.Row.FindControl("lnkMins"), LinkButton) : lnkMins.Visible = False
                    Dim lnkPlus As LinkButton = TryCast(e.Row.FindControl("lnkPlus"), LinkButton) : lnkPlus.Visible = False

                ElseIf CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("isgrp").ToString) = True Then
                    For i = 3 To gvCalibrateList.Columns.Count - 1
                        If gvCalibrateList.Columns(i).Visible Then
                            e.Row.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Row.Cells(2).ColumnSpan = intCount
                    e.Row.Cells(0).CssClass = "GroupHeaderStylecompLeft"
                    e.Row.Cells(1).CssClass = "GroupHeaderStylecompLeft"
                    e.Row.Cells(2).CssClass = "GroupHeaderStylecompLeft"

                    If mintgrpId = Convert.ToInt64(gvCalibrateList.DataKeys(e.Row.RowIndex)("grpid")) Then
                        Dim lnkMins As LinkButton = TryCast(e.Row.FindControl("lnkMins"), LinkButton)
                        Dim lnkPlus As LinkButton = TryCast(e.Row.FindControl("lnkPlus"), LinkButton)
                        If mblnIsExpand Then
                            lnkMins.Visible = True
                            lnkPlus.Visible = False
                        Else
                            lnkMins.Visible = False
                            lnkPlus.Visible = True
                        End If
                    End If

                    If gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString().Trim <> "" Then
                        strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text
                        Dim oDate As Date = eZeeDate.convertDate(gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString()).Date
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 12, "Date") & " [" & oDate & "] "
                    End If
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text & " - (" & Language.getMessage(mstrModuleName1, 13, "Total") & " : " & _
                    mdtListDataTable.Select("grpid = '" & gvCalibrateList.DataKeys(e.Row.RowIndex)("grpid") & "' AND isgrp = 0").Length & ") "

                ElseIf CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("isgrp").ToString) = False Then
                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = False
                    Dim lnkPlus As LinkButton = TryCast(e.Row.FindControl("lnkPlus"), LinkButton) : lnkPlus.Visible = False
                    Dim lnkMins As LinkButton = TryCast(e.Row.FindControl("lnkMins"), LinkButton) : lnkMins.Visible = False
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text.ToString.Replace("/r/n", "&nbsp;")
                    End If
                End If
                If CInt(gvCalibrateList.DataKeys(e.Row.RowIndex)("iStatusId").ToString) <> clsScoreCalibrationApproval.enCalibrationStatus.Submitted Then
                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |25-OCT-2019| -- END


    'Protected Sub gvCalibrateList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCalibrateList.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            If CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("isgrp").ToString) = True Then
    '                e.Row.BackColor = Color.Silver
    '                e.Row.ForeColor = Color.Black
    '                e.Row.Font.Bold = True

    '                Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = False
    '                Dim lnkPlus As LinkButton = TryCast(e.Row.FindControl("lnkPlus"), LinkButton) : lnkPlus.Visible = False
    '                Dim lnkMins As LinkButton = TryCast(e.Row.FindControl("lnkMins"), LinkButton) : lnkMins.Visible = False
    '                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text <> "&nbsp;" Then
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text.ToString.Replace("/r/n", "&nbsp;")
    '                End If

    '                Dim strCaption As String = ""
    '                Dim strUser As String = gvCalibrateList.DataKeys(e.Row.RowIndex)("calibuser").ToString()
    '                strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text
    '                If gvCalibrateList.DataKeys(e.Row.RowIndex)("allocation").ToString().Trim <> "" Then
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 19, "By") & " " & strUser & " (" & gvCalibrateList.DataKeys(e.Row.RowIndex)("allocation").ToString() & ") "
    '                Else
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 19, "By") & " " & strUser
    '                End If

    '                If gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString().Trim <> "" Then
    '                    strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text
    '                    Dim oDate As Date = eZeeDate.convertDate(gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString()).Date
    '                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 12, "Date") & " [" & oDate & "] "
    '                End If

    '                e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text & " - (" & Language.getMessage(mstrModuleName1, 13, "Total") & " : " & _
    '                mdtListDataTable.Select("grpid = '" & gvCalibrateList.DataKeys(e.Row.RowIndex)("grpid") & "' AND isgrp = 0").Length & ") "
    '            Else
    '                Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = True
    '                If mintgrpId = Convert.ToInt64(gvCalibrateList.DataKeys(e.Row.RowIndex)("grpid")) Then
    '                    Dim lnkMins As LinkButton = TryCast(e.Row.FindControl("lnkMins"), LinkButton)
    '                    Dim lnkPlus As LinkButton = TryCast(e.Row.FindControl("lnkPlus"), LinkButton)
    '                    If mblnIsExpand Then
    '                        lnkMins.Visible = True
    '                        lnkPlus.Visible = False
    '                    Else
    '                        lnkMins.Visible = False
    '                        lnkPlus.Visible = True
    '                    End If
    '                End If
    '            End If
    '            If CInt(gvCalibrateList.DataKeys(e.Row.RowIndex)("iStatusId").ToString) <> clsScoreCalibrationApproval.enCalibrationStatus.Submitted Then
    '                Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = False
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'S.SANDEEP |10-OCT-2019| -- END

    Protected Sub gvMyReport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMyReport.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CBool(gvMyReport.DataKeys(e.Row.RowIndex)("isgrp").ToString) = True Then
                    e.Row.BackColor = Color.Silver
                    e.Row.ForeColor = Color.Black
                    e.Row.Font.Bold = True

                    'S.SANDEEP |04-OCT-2019| -- START
                    'e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text & " - (" & Language.getMessage(mstrModuleName1, 13, "Total") & " : " & _
                    'mdtFullDataTable.Select("grpid = '" & gvMyReport.DataKeys(e.Row.RowIndex)("grpid") & "' AND isgrp = 0").Length & ") "

                    Dim strCaption As String = ""
                    Dim strUser As String = gvMyReport.DataKeys(e.Row.RowIndex)("calibuser").ToString()
                    Dim strallocation As String = gvMyReport.DataKeys(e.Row.RowIndex)("allocation").ToString()

                    If strUser.Trim.Length <= 0 Then
                        strUser = mdtFullDataTable.AsEnumerable().Where(Function(x) x.Field(Of Int64)("grpid") = CInt(gvMyReport.DataKeys(e.Row.RowIndex)("grpid")) And x.Field(Of Boolean)("isgrp") = False).Select(Function(x) x.Field(Of String)("calibuser")).FirstOrDefault()
                    End If
                    If strallocation.Trim.Length <= 0 Then
                        strallocation = mdtFullDataTable.AsEnumerable().Where(Function(x) x.Field(Of Int64)("grpid") = CInt(gvMyReport.DataKeys(e.Row.RowIndex)("grpid")) And x.Field(Of Boolean)("isgrp") = False).Select(Function(x) x.Field(Of String)("allocation")).FirstOrDefault()
                    End If

                    strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text & " - (" & Language.getMessage(mstrModuleName1, 13, "Total") & " : " & _
                    mdtFullDataTable.Select("grpid = '" & gvMyReport.DataKeys(e.Row.RowIndex)("grpid") & "' AND isgrp = 0").Length & ") "

                    If strallocation.Trim <> "" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 19, "By") & " " & strUser & " (" & strallocation & ") "
                    Else
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = strCaption & " - " & Language.getMessage(mstrModuleName1, 19, "By") & " " & strUser
                    End If
                    'Dim intCount As Integer = 0
                    'For i = 1 To gvMyReport.Columns.Count
                    '    If gvMyReport.Columns(i).Visible Then
                    '        e.Row.Cells(i).Visible = False
                    '        intCount += 1
                    '    End If
                    'Next
                    'e.Row.Cells(0).ColumnSpan = intCount
                    'S.SANDEEP |04-OCT-2019| -- END
                Else
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text.ToString.Replace("/r/n", "&nbsp;")
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Protected Sub chkMyApproval_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Call btnSearch_Click(btnSearch, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
#Region " Combobox Event(s) "

    Protected Sub cboRptPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRptPeriod.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objScoreCalibrateApproval.GetComboListCalibrationNo("List", CInt(cboRptPeriod.SelectedValue), True)
            With cboRptCalibNo
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'S.SANDEEP |26-AUG-2019| -- END

#End Region

#Region " Add Edit Methods "

#Region " Button's Event(s) "

    Protected Sub btnIClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIClose.Click
        Try
            blnShowApplyCalibrationpopup = False
            popupApproverUseraccess.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnIApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIApply.Click
        Dim mDecScore As Decimal = 0
        Try
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            'If radApplyToChecked.Checked = False AndAlso radApplyToAll.Checked = False Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "Sorry, Please select atleast one of the operation in order to apply changes in the list."), Me)
            '    Exit Sub
            'End If

            'If radApplyToChecked.Checked = True Then
            '    If mdtApplyNew.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iCheck") = True).Count <= 0 Then
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Sorry, Please check atleast one item from the list in order to assign calibrated score."), Me)
            '        Exit Sub
            '    End If
            'End If

            'If CInt(IIf(cbocRating.SelectedValue = "", 0, cbocRating.SelectedValue)) <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 4, "Sorry, Rating is mandatory information. Please select rating to continue."), Me)
            '    Exit Sub
            'End If

            'Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            'If radApplyToChecked.Checked = True Then
            '    gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)
            'Else
            '    gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable()
            'End If

            If mdtApplyNew.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("iCheck") = True).Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Sorry, Please check atleast one item from the list in order to assign calibrated score."), Me)
                Exit Sub
            End If

            If CInt(IIf(cbocRating.SelectedValue = "", 0, cbocRating.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 4, "Sorry, Rating is mandatory information. Please select rating to continue."), Me)
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)
            'S.SANDEEP |27-JUL-2019| -- END

            Dim intFirstValue As Integer = 0
            For Each iRow In gRow
                Dim mRow As DataRow() = Nothing
                mRow = mdtApplyNew.Select("Code = '" & gvApplyCalibration.DataKeys(iRow.RowIndex)("code") & "'")
                If mRow.Length > 0 Then
                    Dim dr() As DataRow = mdtRatings.Select("id = '" & cbocRating.SelectedValue & "'")
                    Dim rnd As New Random : Dim random As Integer = 0
                    If intFirstValue <= 0 Then
                        random = rnd.Next(dr(0)("scrf"), dr(0)("scrt"))
                    Else
                        random = rnd.Next(intFirstValue, dr(0)("scrt"))
                        If intFirstValue = random Then
                            random = rnd.Next(intFirstValue, dr(0)("scrt"))
                        End If
                    End If
                    intFirstValue = random
                    mRow(0)("calibratescore") = random
                    mRow(0)("calibration_remark") = txtIRemark.Text
                    mRow(0)("lstcRating") = cbocRating.SelectedItem.Text
                    mRow(0)("acrating") = cbocRating.SelectedItem.Text
                    mRow(0)("acremark") = txtIRemark.Text
                End If
            Next

            mdtApplyNew.AcceptChanges()
            gvApplyCalibration.AutoGenerateColumns = False
            gvApplyCalibration.DataSource = mdtApplyNew
            gvApplyCalibration.DataBind()
            txtListSearch.Focus()
            'S.SANDEEP |12-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CLEARING CONTROLS AFTER SETTING VALUE
            cbocRating.SelectedValue = 0
            txtIRemark.Text = ""
            'S.SANDEEP |12-AUG-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnIApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIApprove.Click
        Try
            If IsValidData() = False Then Exit Sub
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable()
            If gRow.AsEnumerable().Where(Function(x) x.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhcalibRating", False, True)).Text = "").Count > 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 18, "Sorry, calibration rating is mandatory information. Please set calibration rating to continue."), Me)
                Exit Sub
            End If
            mlbnIsRejected = False
            cnfConfirm.Message = Language.getMessage(mstrModuleName1, 14, "Are you sure you want to approve selected score calibration number?")
            cnfConfirm.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnIReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIReject.Click
        Try
            If IsValidData() = False Then Exit Sub
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable()
            If gRow.AsEnumerable().Where(Function(x) x.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhcalibRating", False, True)).Text = "&nbsp;").Count > 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 18, "Sorry, calibration rating is mandatory information. Please set calibration rating to continue."), Me)
                Exit Sub
            End If
            If txtApprRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 7, "Sorry, remark is mandatory information. Please enter remark to continue."), Me)
                Exit Sub
            End If
            mlbnIsRejected = True
            cnfConfirm.Message = Language.getMessage(mstrModuleName1, 15, "Are you sure you want to reject selected score calibration number?")
            cnfConfirm.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Try
            Call SaveData(mlbnIsRejected)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Method(s) "

    Private Sub FillList(Optional ByVal strExtraFilter As String = "")
        Dim strFilter As String = String.Empty
        Try
            'If mstrAdvanceFilter.Trim.Length > 0 Then
            '    strFilter &= "AND " & mstrAdvanceFilter
            'End If
            If strExtraFilter.Trim.Length > 0 Then
                strFilter &= "AND " & strExtraFilter
            End If
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)

            mdtApplyNew = New DataView(mdtListDataTable, strExtraFilter, "", DataViewRowState.CurrentRows).ToTable


            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            If mdtApplyNew IsNot Nothing AndAlso mdtApplyNew.Rows.Count > 0 Then
                mstrPreviousUser = mdtApplyNew.Rows(0).Item("lusername").ToString()
            End If
            If mstrPreviousUser.Trim.Length <= 0 Then
                gvApplyCalibration.Columns(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhlastlvlrating", False, True)).Visible = False
                gvApplyCalibration.Columns(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhlastlvlremark", False, True)).Visible = False
            Else
                gvApplyCalibration.Columns(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhlastlvlrating", False, True)).HeaderText = mstrPreviousUser & " - [" & gvApplyCalibration.Columns(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhcalibRating", False, True)).HeaderText & "] "
                gvApplyCalibration.Columns(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhlastlvlremark", False, True)).HeaderText = mstrPreviousUser & " - [" & gvApplyCalibration.Columns(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhcalibrationremark", False, True)).HeaderText & "]"
            End If
            'S.SANDEEP |26-AUG-2019| -- END

            'mdtFullDataTable = objScoreCalibrateApproval.GetListForSubmit(CInt(cboSPeriod.SelectedValue), Session("Database_Name"), Session("UserId"), Session("Fin_year"), Session("CompanyUnkId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting"), True, False, "List", strFilter)
            'If mdtFullDataTable.Rows.Count > 0 Then
            '    txtICalibrationNo.Text = mdtFullDataTable.Rows(0)("calibration_no")
            '    txtIRemark.Text = mdtFullDataTable.Rows(0)("calibration_remark")
            'End If

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            If CBool(Session("AllowToEditCalibratedScore")) = True Then
                'mdtApplyNew.Columns.Remove("lstcRating") : mdtApplyNew.Columns.Remove("calibration_remark")
                'Dim dCol As DataColumn = Nothing
                'dCol = New DataColumn
                'With dCol
                '    .ColumnName = "lstcRating"
                '    .DataType = GetType(System.String)
                '    .DefaultValue = ""
                'End With
                'mdtApplyNew.Columns.Add(dCol)

                'dCol = New DataColumn
                'With dCol
                '    .ColumnName = "calibration_remark"
                '    .DataType = GetType(System.String)
                '    .DefaultValue = ""
                'End With
                'mdtApplyNew.Columns.Add(dCol)
            End If
            'S.SANDEEP |27-JUL-2019| -- END

            gvApplyCalibration.DataSource = mdtApplyNew
            gvApplyCalibration.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SaveData(ByVal blnIsRejected As Boolean)
        Try
            mdtApplyNew.AcceptChanges()
            Dim eStatus As clsScoreCalibrationApproval.enCalibrationStatus
            If blnIsRejected Then
                eStatus = clsScoreCalibrationApproval.enCalibrationStatus.Rejected
            Else
                eStatus = clsScoreCalibrationApproval.enCalibrationStatus.Approved
            End If

            Dim strCheckedEmpIds As String = String.Empty : Dim drtemp() As DataRow
            strCheckedEmpIds = String.Join(",", mdtApplyNew.AsEnumerable().Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())
            Dim oData As DataTable = objScoreCalibrateApproval.GetEmployeeApprovalData(CInt(cboPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), False, Nothing, mintPriority, True, "", "")
            'S.SANDEEP |12-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : PA CHANGES
            'drtemp = oData.Select("employeeunkid IN (" & strCheckedEmpIds & ") AND grpId = '" & mintCalibrationUnkid & "' AND userunkid <> '" & Session("UserId") & "' AND iStatusId = 1")
            drtemp = oData.Select("employeeunkid IN (" & strCheckedEmpIds & ") AND grpId = '" & mintCalibrationUnkid & "' AND userunkid <> '" & Session("UserId") & "' AND iStatusId = 1 AND priority > " & mintPriority)
            'S.SANDEEP |12-JUL-2019| -- END

            If drtemp.Length <= 0 Then
                drtemp = mdtApplyNew.Select("")
                For index As Integer = 0 To drtemp.Length - 1
                    drtemp(index)("isfinal") = True
                Next
            End If
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            'If objScoreCalibrateApproval.InsertByDataTable(mdtApplyNew, Session("Database_Name"), Session("UserId"), Session("Fin_year"), Session("CompanyUnkId"), ConfigParameter._Object._CurrentDateAndTime, mintMappingUnkid, txtIRemark.Text, eStatus, ConfigParameter._Object._CurrentDateAndTime, enAuditType.ADD, Session("IP_ADD"), Session("HOST_NAME"), mstrModuleName1, True, Nothing) = True Then
            If objScoreCalibrateApproval.InsertByDataTable(mdtApplyNew, Session("Database_Name"), Session("UserId"), Session("Fin_year"), Session("CompanyUnkId"), ConfigParameter._Object._CurrentDateAndTime, mintMappingUnkid, txtApprRemark.Text, eStatus, ConfigParameter._Object._CurrentDateAndTime, enAuditType.ADD, Session("IP_ADD"), Session("HOST_NAME"), mstrModuleName1, True, Nothing) = True Then
                'S.SANDEEP |27-JUL-2019| -- END

                'S.SANDEEP |16-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
                Dim strCalibNo As String = String.Empty
                If mdtApplyNew IsNot Nothing AndAlso mdtApplyNew.Rows.Count > 0 Then
                    strCalibNo = mdtApplyNew.Rows(0).Item("calibration_no")
                End If
                'S.SANDEEP |16-AUG-2019| -- END

                'S.SANDEEP |05-SEP-2019| -- START
                'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
                Dim intGeneratedCalibUnkid As Integer = 0
                If mdtApplyNew IsNot Nothing AndAlso mdtApplyNew.Rows.Count > 0 Then
                    strCalibNo = mdtApplyNew.Rows(0).Item("calibration_no")
                End If
                'S.SANDEEP |05-SEP-2019| -- END


                If blnIsRejected Then
                    'S.SANDEEP |16-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
                    Dim mstrEmpids As String = String.Join(",", mdtApplyNew.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())
                    'S.SANDEEP |05-SEP-2019| -- START
                    'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
                    'objScoreCalibrateApproval.SendNotification(3, Session("Database_Name").ToString(), Session("UserAccessModeSetting").ToString(), Session("CompanyUnkId"), Session("Fin_year"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("UserId"), CInt(cboSPeriod.SelectedValue), cboSPeriod.SelectedItem.Text, strCalibNo, Session("EmployeeAsOnDate"), mstrEmpids, mstrModuleName1, enLogin_Mode.MGR_SELF_SERVICE, Session("UserName").ToString(), Session("ArutiSelfServiceURL").ToString(), Session("UserName"), mintPriority, txtApprRemark.Text, mstrEmpids, Nothing)
                    objScoreCalibrateApproval.SendNotification(3, Session("Database_Name").ToString(), Session("UserAccessModeSetting").ToString(), Session("CompanyUnkId"), Session("Fin_year"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("UserId"), CInt(cboSPeriod.SelectedValue), cboSPeriod.SelectedItem.Text, strCalibNo, Session("EmployeeAsOnDate"), mstrEmpids, mstrModuleName1, enLogin_Mode.MGR_SELF_SERVICE, Session("UserName").ToString(), Session("ArutiSelfServiceURL").ToString(), Session("UserName"), mintPriority, txtApprRemark.Text, mstrEmpids, Nothing, intGeneratedCalibUnkid)
                    'S.SANDEEP |05-SEP-2019| -- END

                    'S.SANDEEP |16-AUG-2019| -- END
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 8, "Information rejected successfully."), Me)
                Else
                    'S.SANDEEP |16-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
                    Dim mstrEmpids As String = String.Join(",", mdtApplyNew.AsEnumerable().Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString).ToArray())
                    'S.SANDEEP |05-SEP-2019| -- START
                    'ISSUE/ENHANCEMENT : ERROR ON SENDING NOTIFICATION
                    'objScoreCalibrateApproval.SendNotification(2, Session("Database_Name").ToString(), Session("UserAccessModeSetting").ToString(), Session("CompanyUnkId"), Session("Fin_year"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("UserId"), CInt(cboSPeriod.SelectedValue), cboSPeriod.SelectedItem.Text, strCalibNo, Session("EmployeeAsOnDate"), mstrEmpids, mstrModuleName1, enLogin_Mode.MGR_SELF_SERVICE, Session("UserName").ToString(), Session("ArutiSelfServiceURL").ToString(), Session("UserName"), mintPriority, txtApprRemark.Text, mstrEmpids, Nothing)
                    objScoreCalibrateApproval.SendNotification(2, Session("Database_Name").ToString(), Session("UserAccessModeSetting").ToString(), Session("CompanyUnkId"), Session("Fin_year"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("UserId"), CInt(cboSPeriod.SelectedValue), cboSPeriod.SelectedItem.Text, strCalibNo, Session("EmployeeAsOnDate"), mstrEmpids, mstrModuleName1, enLogin_Mode.MGR_SELF_SERVICE, Session("UserName").ToString(), Session("ArutiSelfServiceURL").ToString(), Session("UserName"), mintPriority, txtApprRemark.Text, mstrEmpids, Nothing, intGeneratedCalibUnkid)
                    'S.SANDEEP |05-SEP-2019| -- END

                    'S.SANDEEP |16-AUG-2019| -- END

                    'S.SANDEEP |15-NOV-2019| -- START
                    'ISSUE/ENHANCEMENT : Calibration Issues

                    'S.SANDEEP |03-DEC-2019| -- START
                    'ISSUE/ENHANCEMENT : NMB SAYS THAT THE SYSTEM IS GETTING SLOW, SO COMMENTED AGAIN THIS EMAIL NOTIFICATION SECTION
                    'If mdtApplyNew IsNot Nothing AndAlso mdtApplyNew.Rows.Count > 0 Then
                    '    If mdtApplyNew.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isfinal") = True).Count > 0 Then
                    '        objScoreCalibrateApproval.SendNotificationToEmployee(mdtApplyNew, Session("CompanyUnkId"), Session("CompanyUnkId"), cboSPeriod.SelectedItem.Text, "", mstrModuleName1, enLogin_Mode.MGR_SELF_SERVICE, Session("UserName").ToString(), Session("ArutiSelfServiceURL").ToString())
                    '    End If
                    'End If
                    'S.SANDEEP |03-DEC-2019| -- END

                    'S.SANDEEP |15-NOV-2019| -- END

                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 9, "Information approved successfully."), Me)
                End If
                blnShowApplyCalibrationpopup = False
                popupApproverUseraccess.Hide()
                Call FillGrid()
            Else
                If blnIsRejected Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 10, "Problem in rejection selected calibration process"), Me)
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 16, "Problem in approving selected calibration process"), Me)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Private Sub FilterData()
        Try
            Dim dt As New DataTable
            Dim dtfinal As New DataTable
            Dim oView As DataView = mdtApplyNew.DefaultView
            Dim StrSearch As String = String.Empty
            If txtListSearch.Text.Trim.Length > 0 Then
                StrSearch = "Code LIKE '%" & txtListSearch.Text & "%' OR " & _
                            "employee_name LIKE '%" & txtListSearch.Text & "%' OR " & _
                            "JobTitle LIKE '%" & txtListSearch.Text & "%' "
            End If
            oView.RowFilter = StrSearch
            dt = oView.ToTable() : dtfinal = dt.Clone()
            If dt IsNot Nothing Then
                If gvFilterRating.Rows.Count > 0 Then
                    Dim gvr = gvFilterRating.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("chkFRating"), CheckBox).Checked = True)
                    If gvr IsNot Nothing AndAlso gvr.Count > 0 Then
                        For Each rItem In gvr
                            Dim mDecFromScore, mDecToScore As Decimal : mDecFromScore = 0 : mDecToScore = 0
                            mDecFromScore = CDec(gvFilterRating.DataKeys(rItem.RowIndex)("scrf").ToString())
                            mDecToScore = CDec(gvFilterRating.DataKeys(rItem.RowIndex)("scrt").ToString())
                            StrSearch = "(calibratescore >= " & mDecFromScore & " AND calibratescore <= " & mDecToScore & ") "
                            If dt.Rows.Count > 0 Then
                                If dt.Select(StrSearch).Length > 0 Then
                                    dtfinal.Merge(dt.Select(StrSearch).CopyToDataTable().Copy, True)
                                End If
                            Else
                                If dt.Select(StrSearch).Length > 0 Then
                                    dtfinal = dt.Select(StrSearch).CopyToDataTable()
                                End If
                            End If
                        Next
                    ElseIf gvr IsNot Nothing AndAlso gvr.Count <= 0 Then
                        dtfinal = dt.Copy
                    End If
                End If
            End If

            gvApplyCalibration.AutoGenerateColumns = False
            gvApplyCalibration.DataSource = dtfinal
            gvApplyCalibration.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |26-AUG-2019| -- END

#End Region

#Region " Link's Event(s) "

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    'Protected Sub lnkFilterData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFilterData.Click
    '    Try
    '        Dim oView As DataView = mdtApplyNew.DefaultView
    '        Dim StrSearch As String = String.Empty
    '        If txtSearch.Text.Trim.Length > 0 Then
    '            StrSearch = "Code LIKE '%" & txtSearch.Text & "%' OR " & _
    '                        "EmployeeName LIKE '%" & txtSearch.Text & "%' OR " & _
    '                        "JobTitle LIKE '%" & txtSearch.Text & "%' "
    '        End If

    '        Dim mDecFromScore, mDecToScore As Decimal : mDecFromScore = 0 : mDecToScore = 0

    '        'If txtScoreFrom.Text.Trim.Length > 0 Then Decimal.TryParse(txtScoreFrom.Text, mDecFromScore)
    '        'If txtScoreTo.Text.Trim.Length > 0 Then Decimal.TryParse(txtScoreTo.Text, mDecToScore)

    '        'If txtScoreFrom.Text.Trim.Length > 0 AndAlso txtScoreTo.Text.Trim.Length > 0 Then
    '        '    If StrSearch.Trim.Length > 0 Then
    '        '        StrSearch &= " AND (Provision_Score >= " & mDecFromScore & " AND Provision_Score <= " & mDecToScore & ") "
    '        '    Else
    '        '        StrSearch &= " (Provision_Score >= " & mDecFromScore & " AND Provision_Score <= " & mDecToScore & ") "
    '        '    End If
    '        'ElseIf txtScoreFrom.Text.Trim.Length > 0 Then
    '        '    If StrSearch.Trim.Length > 0 Then
    '        '        StrSearch &= " AND (Provision_Score >= " & mDecFromScore & ") "
    '        '    Else
    '        '        StrSearch &= "(Provision_Score >= " & mDecFromScore & ") "
    '        '    End If
    '        'ElseIf txtScoreTo.Text.Trim.Length > 0 Then
    '        '    If StrSearch.Trim.Length > 0 Then
    '        '        StrSearch &= " AND (Provision_Score <= " & mDecToScore & ") "
    '        '    Else
    '        '        StrSearch &= " (Provision_Score <= " & mDecToScore & ") "
    '        '    End If
    '        'End If

    '        oView.RowFilter = StrSearch
    '        gvApplyCalibration.AutoGenerateColumns = False
    '        gvApplyCalibration.DataSource = oView
    '        gvApplyCalibration.DataBind()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'S.SANDEEP |26-AUG-2019| -- END

    Protected Sub lnkInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkInfo.Click, lnklstInfo.Click
        Try
            Dim objRating As New clsAppraisal_Rating
            Dim dsList As New DataSet
            dsList = objRating.getComboList("List", False)
            gvRating.AutoGenerateColumns = False
            gvRating.DataSource = dsList
            gvRating.DataBind()
            objRating = Nothing
            popupinfo.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If gvApplyCalibration.Rows.Count <= 0 Then Exit Sub
            Dim dvEmployee As DataView = mdtApplyNew.DefaultView
            For i As Integer = 0 To gvApplyCalibration.Rows.Count - 1
                Dim gvRow As GridViewRow = gvApplyCalibration.Rows(i)
                CType(gvRow.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
                'S.SANDEEP |27-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                'Dim dRow() As DataRow = CType(Me.ViewState("mdtApplyNew"), DataTable).Select("Code = '" & gvApplyCalibration.DataKeys(gvRow.RowIndex)("Code") & "'")
                Dim dRow() As DataRow = CType(Me.ViewState("mdtApplyNew"), DataTable).Select("code = '" & gvApplyCalibration.DataKeys(gvRow.RowIndex)("code") & "'")
                'S.SANDEEP |27-JUL-2019| -- END
                If dRow.Length > 0 Then
                    dRow(0).Item("iCheck") = cb.Checked
                End If
                dvEmployee.Table.AcceptChanges()
            Next
            Me.ViewState("mdtApplyNew") = dvEmployee.ToTable
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            If gvr.Cells.Count > 0 Then
                'S.SANDEEP |27-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                'Dim dRow() As DataRow = mdtApplyNew.Select("Code = '" & gvApplyCalibration.DataKeys(gvr.RowIndex)("Code") & "'")
                Dim dRow() As DataRow = mdtApplyNew.Select("code = '" & gvApplyCalibration.DataKeys(gvr.RowIndex)("code") & "'")
                'S.SANDEEP |27-JUL-2019| -- END
                If dRow.Length > 0 Then
                    dRow(0).Item("iCheck") = cb.Checked
                End If
                mdtApplyNew.AcceptChanges()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    Protected Sub chkfrating_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            FilterData()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |26-AUG-2019| -- END

#End Region

#Region " Private Function(s) "

    Public Function IsValidData() As Boolean
        Try
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            'If txtICalibrationNo.Enabled = True AndAlso txtICalibrationNo.Text.Trim.Length <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 5, "Sorry, Calibration number is mandatory information. Please provide calibration number to continue."), Me)
            '    Return False
            'End If

            'If mintCalibrationUnkid <= 0 Then
            '    If txtICalibrationNo.Enabled = True AndAlso txtICalibrationNo.Text.Trim.Length > 0 Then
            '        Dim objComputeScore As New clsComputeScore_master
            '        If objComputeScore.IsCalibrationNoExist(txtICalibrationNo.Text.Trim) Then
            '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 6, "Sorry, Calibration number is mandatory information. Please provide calibration number to continue."), Me)
            '            Return False
            '        End If
            '    End If
            'End If
            'S.SANDEEP |26-AUG-2019| -- END

            'If txtIRemark.Text.Trim.Length <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 7, "Sorry, remark is mandatory information. Please enter remark to continue."), Me)
            '    Return False
            'End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Grid Event(s) "

    Protected Sub gvApplyCalibration_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApplyCalibration.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSEmployee", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSEmployee", False, True)).Text.ToString.Replace("/r/n", "")
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
#Region " Textbox Event(s) "

    Protected Sub txtListSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtListSearch.TextChanged
        Try
            FilterData()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'S.SANDEEP |26-AUG-2019| -- END

#End Region

    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Language._Object.setCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Language._Object.setCaption(Me.lnklstInfo.ID, Me.lnklstInfo.Text)
            Language._Object.setCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Language._Object.setCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Language._Object.setCaption(Me.lblLevel.ID, Me.lblLevel.Text)
            Language._Object.setCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Language._Object.setCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Language._Object.setCaption(Me.lblRemark.ID, Me.lblRemark.Text)
            Language._Object.setCaption(Me.lblPScoreFrom.ID, Me.lblPScoreFrom.Text)
            Language._Object.setCaption(Me.lblPScTo.ID, Me.lblPScTo.Text)
            Language._Object.setCaption(Me.lblCScoreForm.ID, Me.lblCScoreForm.Text)
            Language._Object.setCaption(Me.lblCScTo.ID, Me.lblCScTo.Text)
            Language._Object.setCaption(Me.lblCalibrationNo.ID, Me.lblCalibrationNo.Text)
            Language._Object.setCaption(Me.btnShowAll.ID, Me.btnShowAll.Text)
            Language._Object.setCaption(Me.btnSearch.ID, Me.btnSearch.Text)
            Language._Object.setCaption(Me.btnReset.ID, Me.btnReset.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)
            Language._Object.setCaption(Me.lblRatingInfo.ID, Me.lblRatingInfo.Text)
            Language._Object.setCaption(Me.btnCloseRating.ID, Me.btnCloseRating.Text)
            Language._Object.setCaption(Me.lblMyReport.ID, Me.lblMyReport.Text)

            Language._Object.setCaption(Me.lblRptPeriod.ID, Me.lblRptPeriod.Text)
            Language._Object.setCaption(Me.lblRptCalibNo.ID, Me.lblRptCalibNo.Text)
            Language._Object.setCaption(Me.btnRptShow.ID, Me.btnRptShow.Text)
            Language._Object.setCaption(Me.lblRptApprover.ID, Me.lblRptApprover.Text)
            Language._Object.setCaption(Me.lblRptEmployee.ID, Me.lblRptEmployee.Text)
            Language._Object.setCaption(Me.btnRptReset.ID, Me.btnRptReset.Text)
            Language._Object.setCaption(Me.btnExportMyReport.ID, Me.btnExportMyReport.Text)

            Language._Object.setCaption(gvCalibrateList.Columns(2).FooterText, gvCalibrateList.Columns(2).HeaderText)
            Language._Object.setCaption(gvCalibrateList.Columns(3).FooterText, gvCalibrateList.Columns(3).HeaderText)
            Language._Object.setCaption(gvCalibrateList.Columns(4).FooterText, gvCalibrateList.Columns(4).HeaderText)
            Language._Object.setCaption(gvCalibrateList.Columns(5).FooterText, gvCalibrateList.Columns(5).HeaderText)
            Language._Object.setCaption(gvCalibrateList.Columns(6).FooterText, gvCalibrateList.Columns(6).HeaderText)
            Language._Object.setCaption(gvCalibrateList.Columns(7).FooterText, gvCalibrateList.Columns(7).HeaderText)
            Language._Object.setCaption(gvCalibrateList.Columns(8).FooterText, gvCalibrateList.Columns(8).HeaderText)

            Language._Object.setCaption(gvRating.Columns(0).FooterText, gvRating.Columns(0).HeaderText)
            Language._Object.setCaption(gvRating.Columns(1).FooterText, gvRating.Columns(1).HeaderText)
            Language._Object.setCaption(gvRating.Columns(2).FooterText, gvRating.Columns(2).HeaderText)

            Language._Object.setCaption(gvMyReport.Columns(0).FooterText, gvMyReport.Columns(0).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(1).FooterText, gvMyReport.Columns(1).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(2).FooterText, gvMyReport.Columns(2).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(3).FooterText, gvMyReport.Columns(3).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(4).FooterText, gvMyReport.Columns(4).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(5).FooterText, gvMyReport.Columns(5).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(6).FooterText, gvMyReport.Columns(6).HeaderText)
            Language._Object.setCaption(gvMyReport.Columns(7).FooterText, gvMyReport.Columns(7).HeaderText)

            Language.setLanguage(mstrModuleName1)

            Language._Object.setCaption(Me.lblCalibrateHeading.ID, Me.lblCalibrateHeading.Text)
            Language._Object.setCaption(Me.lblFilter.ID, Me.lblFilter.Text)
            Language._Object.setCaption(Me.lblSPeriod.ID, Me.lblSPeriod.Text)

            Language._Object.setCaption(Me.lblFilterData.ID, Me.lblFilterData.Text)
            Language._Object.setCaption(gvFilterRating.Columns(1).FooterText, gvApplyCalibration.Columns(1).HeaderText)

            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            'Language._Object.setCaption(Me.lblICalibrationNo.ID, Me.lblICalibrationNo.Text)
            'Language._Object.setCaption(Me.lblpRatingFrm.ID, Me.lblpRatingFrm.Text)
            'Language._Object.setCaption(Me.lblpRatingTo.ID, Me.lblpRatingTo.Text)
            'Language._Object.setCaption(Me.lnkFilterData.ID, Me.lnkFilterData.Text)
            'S.SANDEEP |26-AUG-2019| -- END
            Language._Object.setCaption(Me.lblCRating.ID, Me.lblCRating.Text)
            Language._Object.setCaption(Me.btnIApply.ID, Me.btnIApply.Text)
            Language._Object.setCaption(Me.lblApprRemark.ID, Me.lblApprRemark.Text)
            Language._Object.setCaption(Me.btnIApprove.ID, Me.btnIApprove.Text)
            Language._Object.setCaption(Me.btnIReject.ID, Me.btnIReject.Text)
            Language._Object.setCaption(Me.btnIClose.ID, Me.btnIClose.Text)
            Language._Object.setCaption(Me.btnShowAll.ID, Me.btnShowAll.Text)

            Language._Object.setCaption(gvApplyCalibration.Columns(1).FooterText, gvApplyCalibration.Columns(1).HeaderText)
            Language._Object.setCaption(gvApplyCalibration.Columns(2).FooterText, gvApplyCalibration.Columns(2).HeaderText)
            Language._Object.setCaption(gvApplyCalibration.Columns(3).FooterText, gvApplyCalibration.Columns(3).HeaderText)
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            'Language._Object.setCaption(gvApplyCalibration.Columns(4).FooterText, gvApplyCalibration.Columns(4).HeaderText)
            'Language._Object.setCaption(gvApplyCalibration.Columns(5).FooterText, gvApplyCalibration.Columns(5).HeaderText)
            Language._Object.setCaption(gvApplyCalibration.Columns(6).FooterText, gvApplyCalibration.Columns(6).HeaderText)
            Language._Object.setCaption(gvApplyCalibration.Columns(7).FooterText, gvApplyCalibration.Columns(7).HeaderText)
            'S.SANDEEP |26-AUG-2019| -- END


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Me.lnklstInfo.Text = Language._Object.getCaption(Me.lnklstInfo.ID, Me.lnklstInfo.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.ID, Me.lblLevel.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.ID, Me.lblRemark.Text)
            Me.lblPScoreFrom.Text = Language._Object.getCaption(Me.lblPScoreFrom.ID, Me.lblPScoreFrom.Text)
            Me.lblPScTo.Text = Language._Object.getCaption(Me.lblPScTo.ID, Me.lblPScTo.Text)
            Me.lblCScoreForm.Text = Language._Object.getCaption(Me.lblCScoreForm.ID, Me.lblCScoreForm.Text)
            Me.lblCScTo.Text = Language._Object.getCaption(Me.lblCScTo.ID, Me.lblCScTo.Text)
            Me.lblCalibrationNo.Text = Language._Object.getCaption(Me.lblCalibrationNo.ID, Me.lblCalibrationNo.Text)
            Me.btnShowAll.Text = Language._Object.getCaption(Me.btnShowAll.ID, Me.btnShowAll.Text)
            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text)
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text)
            Me.lblRatingInfo.Text = Language._Object.getCaption(Me.lblRatingInfo.ID, Me.lblRatingInfo.Text)
            Me.btnCloseRating.Text = Language._Object.getCaption(Me.btnCloseRating.ID, Me.btnCloseRating.Text)
            Me.lblMyReport.Text = Language._Object.getCaption(Me.lblMyReport.ID, Me.lblMyReport.Text)

            gvCalibrateList.Columns(2).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(2).FooterText, gvCalibrateList.Columns(2).HeaderText)
            gvCalibrateList.Columns(3).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(3).FooterText, gvCalibrateList.Columns(3).HeaderText)
            gvCalibrateList.Columns(4).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(4).FooterText, gvCalibrateList.Columns(4).HeaderText)
            gvCalibrateList.Columns(5).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(5).FooterText, gvCalibrateList.Columns(5).HeaderText)
            gvCalibrateList.Columns(6).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(6).FooterText, gvCalibrateList.Columns(6).HeaderText)
            gvCalibrateList.Columns(7).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(7).FooterText, gvCalibrateList.Columns(7).HeaderText)
            gvCalibrateList.Columns(8).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(8).FooterText, gvCalibrateList.Columns(8).HeaderText)

            gvRating.Columns(0).HeaderText = Language._Object.getCaption(gvRating.Columns(0).FooterText, gvRating.Columns(0).HeaderText)
            gvRating.Columns(1).HeaderText = Language._Object.getCaption(gvRating.Columns(1).FooterText, gvRating.Columns(1).HeaderText)
            gvRating.Columns(2).HeaderText = Language._Object.getCaption(gvRating.Columns(2).FooterText, gvRating.Columns(2).HeaderText)

            gvMyReport.Columns(0).HeaderText = Language._Object.getCaption(gvMyReport.Columns(0).FooterText, gvMyReport.Columns(0).HeaderText)
            gvMyReport.Columns(1).HeaderText = Language._Object.getCaption(gvMyReport.Columns(1).FooterText, gvMyReport.Columns(1).HeaderText)
            gvMyReport.Columns(2).HeaderText = Language._Object.getCaption(gvMyReport.Columns(2).FooterText, gvMyReport.Columns(2).HeaderText)
            gvMyReport.Columns(3).HeaderText = Language._Object.getCaption(gvMyReport.Columns(3).FooterText, gvMyReport.Columns(3).HeaderText)
            gvMyReport.Columns(4).HeaderText = Language._Object.getCaption(gvMyReport.Columns(4).FooterText, gvMyReport.Columns(4).HeaderText)
            gvMyReport.Columns(5).HeaderText = Language._Object.getCaption(gvMyReport.Columns(5).FooterText, gvMyReport.Columns(5).HeaderText)
            gvMyReport.Columns(6).HeaderText = Language._Object.getCaption(gvMyReport.Columns(6).FooterText, gvMyReport.Columns(6).HeaderText)
            gvMyReport.Columns(7).HeaderText = Language._Object.getCaption(gvMyReport.Columns(7).FooterText, gvMyReport.Columns(7).HeaderText)

            Language.setLanguage(mstrModuleName1)

            Me.lblCalibrateHeading.Text = Language._Object.getCaption(Me.lblCalibrateHeading.ID, Me.lblCalibrateHeading.Text)
            Me.lblFilter.Text = Language._Object.getCaption(Me.lblFilter.ID, Me.lblFilter.Text)
            Me.lblSPeriod.Text = Language._Object.getCaption(Me.lblSPeriod.ID, Me.lblSPeriod.Text)
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            'Me.lblICalibrationNo.Text = Language._Object.getCaption(Me.lblICalibrationNo.ID, Me.lblICalibrationNo.Text)
            'Me.lblpRatingFrm.Text = Language._Object.getCaption(Me.lblpRatingFrm.ID, Me.lblpRatingFrm.Text)
            'Me.lblpRatingTo.Text = Language._Object.getCaption(Me.lblpRatingTo.ID, Me.lblpRatingTo.Text)
            'Me.lnkFilterData.Text = Language._Object.getCaption(Me.lnkFilterData.ID, Me.lnkFilterData.Text)
            'S.SANDEEP |26-AUG-2019| -- END
            Me.lblCRating.Text = Language._Object.getCaption(Me.lblCRating.ID, Me.lblCRating.Text)
            Me.btnIApply.Text = Language._Object.getCaption(Me.btnIApply.ID, Me.btnIApply.Text)
            Me.lblApprRemark.Text = Language._Object.getCaption(Me.lblApprRemark.ID, Me.lblApprRemark.Text)
            Me.btnIApprove.Text = Language._Object.getCaption(Me.btnIApprove.ID, Me.btnIApprove.Text)
            Me.btnIReject.Text = Language._Object.getCaption(Me.btnIReject.ID, Me.btnIReject.Text)
            Me.btnIClose.Text = Language._Object.getCaption(Me.btnIClose.ID, Me.btnIClose.Text)
            Me.btnShowAll.Text = Language._Object.getCaption(Me.btnShowAll.ID, Me.btnShowAll.Text)

            gvApplyCalibration.Columns(1).HeaderText = Language._Object.getCaption(gvApplyCalibration.Columns(1).FooterText, gvApplyCalibration.Columns(1).HeaderText)
            gvApplyCalibration.Columns(2).HeaderText = Language._Object.getCaption(gvApplyCalibration.Columns(2).FooterText, gvApplyCalibration.Columns(2).HeaderText)
            gvApplyCalibration.Columns(3).HeaderText = Language._Object.getCaption(gvApplyCalibration.Columns(3).FooterText, gvApplyCalibration.Columns(3).HeaderText)
            gvApplyCalibration.Columns(4).HeaderText = Language._Object.getCaption(gvApplyCalibration.Columns(4).FooterText, gvApplyCalibration.Columns(4).HeaderText)
            gvApplyCalibration.Columns(5).HeaderText = Language._Object.getCaption(gvApplyCalibration.Columns(5).FooterText, gvApplyCalibration.Columns(5).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |27-JUL-2019| -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage("frmPerformanceEvaluationReport", 1, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage("frmPerformanceEvaluationReport", 4, "Sorry, No performace evaluation data present for the selected period.")
            Language.setMessage(mstrModuleName, 1, "Sorry, you cannot do approve/reject operation. Reason selected calibration batch is still pending for lower level approver.")
            Language.setMessage(mstrModuleName, 2, "Sorry, you cannot do approve/reject operation. Reason the selected calibration batch is already approved or rejected.")
            Language.setMessage(mstrModuleName, 20, "Period :")
            Language.setMessage(mstrModuleName, 21, "Calibration No :")
            Language.setMessage(mstrModuleName, 22, "Employee :")
            Language.setMessage(mstrModuleName, 23, "Approver :")
            Language.setMessage(mstrModuleName, 24, "Approver Wise Status")
            Language.setMessage(mstrModuleName, 25, "Sorry, there is no data in order to export.")
            Language.setMessage(mstrModuleName, 100, "Ratings")
            Language.setMessage(mstrModuleName, 101, "%")
            Language.setMessage(mstrModuleName, 102, "Normal Distribution")
            Language.setMessage(mstrModuleName, 103, "Actual Performance")
            Language.setMessage(mstrModuleName, 200, "Performance HPO Curve")
            Language.setMessage(mstrModuleName1, 1, "Sorry, Period is mandatory information, Please select period to continue")
            Language.setMessage(mstrModuleName1, 3, "Sorry, Please check atleast one item from the list in order to assign calibrated score.")
            Language.setMessage(mstrModuleName1, 4, "Sorry, Rating is mandatory information. Please select rating to continue.")
            Language.setMessage(mstrModuleName1, 7, "Sorry, remark is mandatory information. Please enter remark to continue.")
            Language.setMessage(mstrModuleName1, 8, "Information rejected successfully.")
            Language.setMessage(mstrModuleName1, 9, "Information approved successfully.")
            Language.setMessage(mstrModuleName1, 10, "Problem in rejection selected calibration process")
            Language.setMessage(mstrModuleName1, 11, "Sorry, To rating cannot be less than From rating. Please select higher rating to continue.")
            Language.setMessage(mstrModuleName1, 12, "Date")
            Language.setMessage(mstrModuleName1, 13, "Total")
            Language.setMessage(mstrModuleName1, 14, "Are you sure you want to approve selected score calibration number?")
            Language.setMessage(mstrModuleName1, 15, "Are you sure you want to reject selected score calibration number?")
            Language.setMessage(mstrModuleName1, 16, "Problem in approving selected calibration process")
            Language.setMessage(mstrModuleName1, 18, "Sorry, calibration rating is mandatory information. Please set calibration rating to continue.")
            Language.setMessage(mstrModuleName1, 19, "By")
            Language.setMessage(mstrModuleName1, 100, "Sorry, No approver defined with the selected login.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class