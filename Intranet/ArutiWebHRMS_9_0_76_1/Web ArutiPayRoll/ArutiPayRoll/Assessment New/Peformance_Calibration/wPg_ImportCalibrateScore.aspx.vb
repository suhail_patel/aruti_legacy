﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing
Imports System.IO

#End Region

Partial Class Assessment_New_Peformance_Calibration_wPg_ImportCalibrateScore
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmImportCalibrateScore"
    Private objScoreCalibrateApproval As New clsScoreCalibrationApproval
    Private mdtFullDataTable As DataTable = Nothing
    Private objCalibrateApproverTran As New clscalibrate_approver_tran

#End Region

#Region " Private Method "

    Private Sub ClearCombo()
        Try
            Dim cbo As New DropDownList
            Dim xCtrl = GetAll(Me, cbo.GetType())
            If xCtrl IsNot Nothing AndAlso xCtrl.Count > 0 Then
                For Each icbo In xCtrl
                    CType(icbo, DropDownList).Items.Clear()
                    CType(icbo, DropDownList).Items.Add("")
                Next
            End If
            cbo = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub IsValidMapping(ByVal cbo As DropDownList, ByVal strText As String, ByRef xMsg As String)
        Try
            Dim xCtrl = GetAll(Me, cbo.GetType())
            If xCtrl IsNot Nothing Then
                '---> CHECKING IF SOME DATA MANDATORY IS BLANK
                If gvData.Rows.Count > 0 Then
                    Dim iCellIdx As Integer = -1
                    iCellIdx = getColumnID_Griview(gvData, cbo.SelectedItem.Text, False, False)
                    If gvData.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) x.Cells(iCellIdx).Text.Trim = "&nbsp;").Count > 0 Then
                        xMsg = Language.getMessage(mstrModuleName, 111, "Sorry, Data in particular column") & " [" & strText & "] " & Language.getMessage(mstrModuleName, 112, "is blank. Please check file again and import.")
                        Exit Try
                    End If
                End If

                '---> CHECKING SAME MAPPING FOR DIFFERENT COMBO SELECTION
                Dim cboToCheck = xCtrl.AsEnumerable().Where(Function(x) x.ID <> cbo.ID And CType(x, DropDownList).Enabled = True)
                If cboToCheck IsNot Nothing AndAlso cboToCheck.Count > 0 Then
                    Dim iText As String = String.Empty
                    For Each icbo In cboToCheck
                        Select Case icbo.ID
                            Case cboCalibrationRating.ID
                                iText = lblCalibratorRating.Text
                            Case cboCalibrationRemark.ID
                                iText = lblCalibrationRemark.Text
                            Case cboCalibratorName.ID
                                iText = lblCalibratorName.Text
                            Case cboEmployeeCode.ID
                                iText = lblEmployeeCode.Text
                            Case cboEmployeeName.ID
                                iText = lblEmployeeName.Text
                            Case cboPeriodName.ID
                                iText = lblPeriodName.Text
                            Case cboCalibrationNo.ID
                                iText = lblCalibrationNo.Text
                        End Select
                        If CType(icbo, DropDownList).SelectedIndex = cbo.SelectedIndex Then
                            xMsg = Language.getMessage(mstrModuleName, 107, "Sorry, Same field is mapped for") & " " & _
                                   strText & " " & Language.getMessage(mstrModuleName, 108, "and") & " " & _
                                   iText & " " & Language.getMessage(mstrModuleName, 109, ". Please map proper field.")
                            Exit Try
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function GetAll(ByVal control As Control, ByVal type As Type) As IEnumerable(Of Control)
        Try
        Dim controls = control.Controls.Cast(Of Control)()
        Return controls.SelectMany(Function(ctrl) GetAll(ctrl, type)).Concat(controls).Where(Function(c) c.GetType() Is type)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub ImportData(ByVal iList As List(Of KeyValuePair(Of String, String)))
        Try
            Dim iCalibUserId, iEmpId, iPeriodId, iCalibNoId As Integer
            Dim mScrF, mScrT, sScrM As Decimal
            Dim objUsr As New clsUserAddEdit
            Dim objEmp As New clsEmployee_Master
            Dim objPrd As New clscommom_period_Tran
            Dim objRat As New clsAppraisal_Rating
            Dim objCbn As New clsComputeScore_master
            Dim dtRating As New DataTable
            dtRating = objRat._DataTable
            Dim strCalibNo As String = String.Empty
            Dim intErColIdx As Integer = -1
            intErColIdx = getColumnID_Griview(gvData, "Message", False, False)
            Dim mDicNumEmp As New Dictionary(Of String, String)
            Dim mDicNumPrd As New Dictionary(Of String, String)
            Dim xRowCollection As IEnumerable(Of GridViewRow) = Nothing
            If intErColIdx > -1 Then
                For Each item In iList
                    Dim iKey As String = item.Key
                    Dim iValue As String = item.Value
                    If CInt(Session("ScoreCalibrationFormNotype")) = 1 Then
                        xRowCollection = gvData.Rows.Cast(Of GridViewRow)().Where(Function(x) x.Cells(getColumnID_Griview(gvData, cboCalibratorName.SelectedItem.Text, False, False)).Text = iKey)
                    Else
                        xRowCollection = gvData.Rows.Cast(Of GridViewRow)().Where(Function(x) x.Cells(getColumnID_Griview(gvData, cboCalibrationNo.SelectedItem.Text, False, False)).Text = iKey And _
                                                                                      x.Cells(getColumnID_Griview(gvData, cboCalibratorName.SelectedItem.Text, False, False)).Text = iValue)
                    End If
                    Dim intNewCalibUserId As Integer = 0
                    Dim strPeriodNameId As String = String.Empty
                    For Each xgvr As GridViewRow In xRowCollection
                        Dim iColIdx As Integer = -1
                        iCalibUserId = 0 : iEmpId = 0 : iPeriodId = 0 : iCalibNoId = 0 : mScrF = 0 : mScrT = 0 : sScrM = 0
                        strPeriodNameId = String.Empty
                        'If cboCalibrationNo.Enabled = True AndAlso cboCalibrationNo.SelectedIndex > 0 Then
                        '    iColIdx = getColumnID_Griview(gvData, cboCalibrationNo.SelectedItem.Text, False, False)
                        '    If iColIdx > -1 Then
                        '        strCalibNo = xgvr.Cells(iColIdx).Text.Trim
                        '        iCalibNoId = objCbn.GetCalibrationNoUnkid(xgvr.Cells(iColIdx).Text.Trim, Nothing)
                        '        If iCalibNoId <= 0 Then
                        '            xgvr.Cells(intErColIdx).Text = "Invalid Calibration No"
                        '            xgvr.ForeColor = Color.Red
                        '            Continue For
                        '        End If
                        '    End If
                        'End If

                        If cboCalibratorName.SelectedIndex > 0 Then
                            iColIdx = getColumnID_Griview(gvData, cboCalibratorName.SelectedItem.Text, False, False)
                            If iColIdx > -1 Then
                                iCalibUserId = objUsr.Return_UserId(xgvr.Cells(iColIdx).Text.Trim, "hrmsConfiguration", enLoginMode.USER)
                                If iCalibUserId <= 0 Then
                                    xgvr.Cells(intErColIdx).Text = Language.getMessage(mstrModuleName, 118, "Invalid Username")
                                    xgvr.ForeColor = Color.Red
                                    Continue For
                                Else
                                    Dim ds As New DataSet
                                    ds = objUsr.getNewComboList("List", iCalibUserId, False, Session("CompanyUnkId"), CInt(enUserPriviledge.AllowtoCalibrateProvisionalScore).ToString() & "," & CInt(enUserPriviledge.AllowToApproveRejectCalibratedScore).ToString(), Session("Fin_year"), False)
                                    If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count <= 0 Then
                                        xgvr.Cells(intErColIdx).Text = Language.getMessage(mstrModuleName, 119, "User does not contains calibration privileges")
                                        xgvr.ForeColor = Color.Red
                                        Continue For
                                    End If
                                End If
                            End If
                        End If

                        If objCalibrateApproverTran.IsValidUserType(iCalibUserId, True) = False Then
                            xgvr.Cells(intErColIdx).Text = Language.getMessage(mstrModuleName, 200, "User is not created as calibrator")
                            xgvr.ForeColor = Color.Red
                            Continue For
                        End If

                        If cboCalibrationNo.Enabled = False Then
                            If intNewCalibUserId <> iCalibUserId Then
                                strCalibNo = ""
                                intNewCalibUserId = iCalibUserId
                            End If
                        Else
                            iColIdx = getColumnID_Griview(gvData, cboCalibrationNo.SelectedItem.Text, False, False)
                            strCalibNo = xgvr.Cells(iColIdx).Text.Trim
                        End If

                        If cboEmployeeCode.SelectedIndex > 0 Then
                            iColIdx = getColumnID_Griview(gvData, cboEmployeeCode.SelectedItem.Text, False, False)
                            If iColIdx > 0 Then
                                iEmpId = objEmp.GetEmployeeUnkidFromEmpCode(xgvr.Cells(iColIdx).Text.Trim)
                                If iEmpId <= 0 Then
                                    xgvr.Cells(intErColIdx).Text = Language.getMessage(mstrModuleName, 120, "Invalid Employee")
                                    xgvr.ForeColor = Color.Red
                                    Continue For
                                End If
                            End If
                        End If

                        If objCalibrateApproverTran.IsValidMapping(iCalibUserId, iEmpId, True) = False Then
                            xgvr.Cells(intErColIdx).Text = Language.getMessage(mstrModuleName, 201, "Employee is not mapped with this calibrator.")
                            xgvr.ForeColor = Color.Red
                            Continue For
                        End If

                        If cboPeriodName.SelectedIndex > 0 Then
                            iColIdx = getColumnID_Griview(gvData, cboPeriodName.SelectedItem.Text, False, False)
                            If iColIdx > 0 Then
                                iPeriodId = objPrd.GetPeriodByName(xgvr.Cells(iColIdx).Text.Trim, enModuleReference.Assessment)
                                If iPeriodId <= 0 Then
                                    xgvr.Cells(intErColIdx).Text = Language.getMessage(mstrModuleName, 121, "Invalid Period")
                                    xgvr.ForeColor = Color.Red
                                    Continue For
                                End If
                                strPeriodNameId = iPeriodId.ToString() & "|" & xgvr.Cells(iColIdx).Text.Trim
                            End If
                        End If

                        If cboCalibrationRating.SelectedIndex > 0 Then
                            iColIdx = getColumnID_Griview(gvData, cboCalibrationRating.SelectedItem.Text, False, False)
                            If iColIdx > 0 Then
                                If dtRating IsNot Nothing AndAlso dtRating.Rows.Count > 0 Then
                                    Dim iRow() As DataRow = dtRating.Select("grade_award = '" & xgvr.Cells(iColIdx).Text.Trim & "'")
                                    If iRow.Length <= 0 Then
                                        xgvr.Cells(intErColIdx).Text = Language.getMessage(mstrModuleName, 122, "Invalid Rating")
                                        xgvr.ForeColor = Color.Red
                                        Continue For
                                    Else
                                        mScrF = iRow(0)("score_from")
                                        mScrT = iRow(0)("score_to")
                                        Dim rnd As New Random
                                        sScrM = rnd.Next(mScrF, mScrT)
                                        sScrM = IIf(sScrM <= 0, mScrT, sScrM)
                                    End If
                                Else
                                    xgvr.Cells(intErColIdx).Text = Language.getMessage(mstrModuleName, 123, "Ratings Not Defined")
                                    xgvr.ForeColor = Color.Red
                                    Continue For
                                End If
                            End If
                        End If

                        Dim strRemark As String = String.Empty
                        If cboCalibrationRemark.SelectedIndex > 0 Then
                            iColIdx = getColumnID_Griview(gvData, cboCalibrationRemark.SelectedItem.Text, False, False)
                            strRemark = xgvr.Cells(iColIdx).Text.Trim
                        End If

                        Dim oMsg As String = String.Empty
                        Dim oStatusId As Integer = 0
                        objCbn.ImportCalibrationData(Session("Database_Name"), _
                                                     Session("UserId"), _
                                                     Session("Fin_year"), _
                                                     Session("CompanyUnkId"), _
                                                     iCalibNoId, _
                                                     strCalibNo, _
                                                     iCalibUserId, _
                                                     iEmpId, _
                                                     iPeriodId, _
                                                     sScrM, strRemark, chkSubmitStatus.Checked, _
                                                     Session("IP_ADD"), Session("HOST_NAME"), mstrModuleName, _
                                                     True, Now, oMsg, oStatusId, strCalibNo)

                        Select Case oStatusId
                            Case 1
                                xgvr.ForeColor = Color.Green
                                If chkSubmitStatus.Checked Then
                                    If mDicNumEmp.ContainsKey(strCalibNo) = False Then
                                        mDicNumEmp.Add(strCalibNo, iEmpId.ToString())
                                    Else
                                        mDicNumEmp(strCalibNo) = mDicNumEmp(strCalibNo) & "," & iEmpId.ToString()
                                    End If
                                    If mDicNumPrd.ContainsKey(strCalibNo) = False Then
                                        mDicNumPrd.Add(strCalibNo, strPeriodNameId)
                                    End If
                                End If
                            Case 2
                                xgvr.ForeColor = Color.DarkGray
                        End Select
                        xgvr.Cells(intErColIdx).Text = oMsg
                        xgvr.Cells(intErColIdx).Attributes.Add(intErColIdx, oStatusId)
                    Next
                Next

                If mDicNumEmp IsNot Nothing AndAlso mDicNumEmp.Count > 0 Then
                    Dim objScoreCalibrateApproval As New clsScoreCalibrationApproval
                    For Each iCNum In mDicNumEmp.Keys
                        If mDicNumPrd.ContainsKey(iCNum) Then
                            iCalibNoId = objCbn.GetCalibrationNoUnkid(iCNum.ToString(), Nothing)
                            Dim strval() As String = mDicNumPrd(iCNum).ToString().Split("|")
                            If strval.Length > 0 Then
                                objScoreCalibrateApproval.SendNotification(1, Session("Database_Name").ToString(), _
                                                                           Session("UserAccessModeSetting").ToString(), _
                                                                           Session("CompanyUnkId"), Session("Fin_year"), _
                                                                           enUserPriviledge.AllowToApproveRejectCalibratedScore, iCalibUserId, _
                                                                           CInt(strval(0)), strval(1).ToString(), iCNum, _
                                                                           Session("EmployeeAsOnDate"), mDicNumEmp(iCNum), _
                                                                           mstrModuleName, enLogin_Mode.MGR_SELF_SERVICE, _
                                                                           Session("UserName").ToString(), Session("ArutiSelfServiceURL").ToString(), _
                                                                           Session("UserName"), -1, "", mDicNumEmp(iCNum), Nothing, iCalibNoId)
                            End If
                        End If
                    Next
                    objScoreCalibrateApproval = Nothing
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ExportImportedData(ByVal iExpStatusId As Integer, ByVal strFileName As String)
        Try
            If gvData.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 117, "Sorry, No data present to export."), Me)
                Exit Sub
            End If

            Dim intErColIdx As Integer = -1 : Dim xRowCollection As IEnumerable(Of GridViewRow)
            intErColIdx = getColumnID_Griview(gvData, "Message", False, False)
            Select Case iExpStatusId
                Case 1, 2     '1 = SUCCESS | '2 = FAILURE
                    xRowCollection = gvData.Rows.Cast(Of GridViewRow)().Where(Function(x) x.Cells(intErColIdx).HasAttributes = True And x.Cells(intErColIdx).Attributes(intErColIdx) = iExpStatusId)
                Case Else   'ALL DATA
                    xRowCollection = gvData.Rows.Cast(Of GridViewRow)()
            End Select

            Dim dtTable = New DataTable("List")
            For Each cell As TableCell In gvData.HeaderRow.Cells
                dtTable.Columns.Add(cell.Text)
            Next
            For Each row As GridViewRow In xRowCollection
                dtTable.Rows.Add()
                For i As Integer = 0 To row.Cells.Count - 1
                    dtTable.Rows(dtTable.Rows.Count - 1)(i) = row.Cells(i).Text
                Next
            Next
            Dim ds As New DataSet : ds.Tables.Add(dtTable.Copy())
            Dim xPath As String = My.Computer.FileSystem.SpecialDirectories.Temp + "\" & strFileName & ".xlsx"
            Aruti.Data.OpenXML_Export(xPath, ds)
            Dim fl As IO.FileInfo = New FileInfo(xPath)
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & fl.Name & """")
            Response.Clear()
            Response.TransmitFile(xPath)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call SetControlCaptions()
            Call SetMessages()
            Call Language._Object.SaveValue()
            Call SetLanguage()
            If CInt(Session("ScoreCalibrationFormNotype")) = 1 Then
                cboCalibrationNo.Enabled = False
            Else
                cboCalibrationNo.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button Event(s) "

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If FileUpload1.HasFile Then
                Dim FileName As String = Path.GetFileName(FileUpload1.PostedFile.FileName)
                Dim Extension As String = Path.GetExtension(FileUpload1.PostedFile.FileName)
                If Extension.ToLower() = ".xlsx" Then
                    Dim FolderPath As String = "~/UploadImage/"
                    Dim FilePath As String = Server.MapPath(FolderPath + FileName)
                    Dim DircPath As String = IO.Directory.GetParent(FilePath).FullName
                    If IO.Directory.Exists(DircPath) = True Then
                        FileUpload1.SaveAs(FilePath)
                        Dim dsImport As New DataSet
                        dsImport = Aruti.Data.OpenXML_Import(FilePath)
                        Dim strColArry As String() = dsImport.Tables(0).Columns.Cast(Of DataColumn).AsEnumerable().Select(Function(x) x.ColumnName).ToArray()
                        If strColArry IsNot Nothing AndAlso strColArry.Length > 0 Then
                            Call ClearCombo()
                            Dim cbo As New DropDownList
                            Dim xCtrl = GetAll(Me, cbo.GetType())
                            For Each icbo In xCtrl
                                For index As Integer = 0 To strColArry.Length - 1
                                    CType(icbo, DropDownList).Items.Add(strColArry(index))
                                Next
                            Next
                        End If
                        Dim xCol As New DataColumn
                        With xCol
                            .DataType = GetType(System.String)
                            .DefaultValue = ""
                            .ColumnName = "Message"
                        End With

                        If gvData.Columns.Count > 0 Then
                            gvData.Columns.Clear()
                        End If

                        dsImport.Tables(0).Columns.Add(xCol)
                        For Each iCol As DataColumn In dsImport.Tables(0).Columns
                            Dim dgvCol As New BoundField()
                            dgvCol.FooterText = iCol.ColumnName
                            dgvCol.ReadOnly = True
                            dgvCol.DataField = iCol.ColumnName
                            dgvCol.HeaderText = iCol.ColumnName
                            gvData.Columns.Add(dgvCol)
                        Next

                        gvData.DataSource = dsImport.Tables(0)
                        gvData.DataBind()
                    Else
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 115, "Sorry, directory path not found on server. Please check the path and try to import again."), Me)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnGetFileFormat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetFileFormat.Click
        Try
            Dim dtTable As New DataTable
            dtTable.Columns.Add("CalibratorUsername", GetType(System.String))
            dtTable.Columns.Add("EmployeeCode", GetType(System.String))
            dtTable.Columns.Add("EmployeeName", GetType(System.String))
            dtTable.Columns.Add("PeriodName", GetType(System.String))
            dtTable.Columns.Add("CalibrationRating", GetType(System.String))
            dtTable.Columns.Add("CalibrationRemark", GetType(System.String))
            If CInt(Session("ScoreCalibrationFormNotype")) <> 1 Then
                dtTable.Columns.Add("CalibrationNo", GetType(System.String))
            End If
            Dim ds As New DataSet : ds.Tables.Add(dtTable.Copy())
            Dim xPath As String = My.Computer.FileSystem.SpecialDirectories.Temp + "\CalibrationFormat.xlsx"
            Aruti.Data.OpenXML_Export(xPath, ds)
            Dim fl As IO.FileInfo = New FileInfo(xPath)
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & fl.Name & """")
            Response.Clear()
            Response.TransmitFile(xPath)
            HttpContext.Current.ApplicationInstance.CompleteRequest()
        Catch ex As Threading.ThreadAbortException

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Try
            Response.Redirect(Session("rootpath") & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event "

    Protected Sub lnkImportData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkImportData.Click
        Try
            If cboCalibratorName.SelectedIndex <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, Calibrator name is mandatory information. Please map field for calibrator name."), Me)
                Exit Sub
            End If
            If cboEmployeeCode.SelectedIndex <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 102, "Sorry, Employee code is mandatory information. Please map field for employee code."), Me)
                Exit Sub
            End If
            If cboEmployeeName.SelectedIndex <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 103, "Sorry, Employee name is mandatory information. Please map field for employee name."), Me)
                Exit Sub
            End If
            If cboPeriodName.SelectedIndex <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 104, "Sorry, Period name is mandatory information. Please map field for period name."), Me)
                Exit Sub
            End If
            If cboCalibrationRating.SelectedIndex <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 105, "Sorry, Calibration rating is mandatory information. Please map field for calibration rating."), Me)
                Exit Sub
            End If
            If cboCalibrationRemark.SelectedIndex <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 106, "Sorry, Calibration remark is mandatory information. Please map field for calibration remark."), Me)
                Exit Sub
            End If
            If cboCalibrationNo.Enabled = True AndAlso cboCalibrationNo.SelectedIndex <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 110, "Sorry, Calibration number is mandatory information. Please map field for calibration number."), Me)
                Exit Sub
            End If
            Dim xMsg As String = String.Empty
            If cboCalibratorName.SelectedIndex > 0 Then
                IsValidMapping(cboCalibratorName, lblCalibratorName.Text, xMsg)
                If xMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(xMsg, Me)
                    Exit Sub
                End If
            End If
            If cboEmployeeCode.SelectedIndex > 0 Then
                IsValidMapping(cboEmployeeCode, lblEmployeeCode.Text, xMsg)
                If xMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(xMsg, Me)
                    Exit Sub
                End If
            End If
            If cboEmployeeName.SelectedIndex > 0 Then
                IsValidMapping(cboEmployeeName, lblEmployeeName.Text, xMsg)
                If xMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(xMsg, Me)
                    Exit Sub
                End If
            End If
            If cboPeriodName.SelectedIndex > 0 Then
                IsValidMapping(cboPeriodName, lblPeriodName.Text, xMsg)
                If xMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(xMsg, Me)
                    Exit Sub
                End If
            End If
            If cboCalibrationRating.SelectedIndex > 0 Then
                IsValidMapping(cboCalibrationRating, lblCalibratorRating.Text, xMsg)
                If xMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(xMsg, Me)
                    Exit Sub
                End If
            End If
            If cboCalibrationRemark.SelectedIndex > 0 Then
                IsValidMapping(cboCalibrationRemark, lblCalibrationRemark.Text, xMsg)
                If xMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(xMsg, Me)
                    Exit Sub
                End If
            End If
            If cboCalibrationNo.Enabled = True AndAlso cboCalibrationNo.SelectedIndex > 0 Then
                IsValidMapping(cboCalibrationNo, lblCalibrationNo.Text, xMsg)
                If xMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(xMsg, Me)
                    Exit Sub
                End If
            End If

            If cboCalibrationNo.Enabled = True AndAlso cboCalibrationNo.SelectedIndex > 0 Then
                Dim iCalibNoIdx = getColumnID_Griview(gvData, cboCalibrationNo.SelectedItem.Text, False, False)
                Dim iCalibUrIdx = getColumnID_Griview(gvData, cboCalibratorName.SelectedItem.Text, False, False)

                Dim iValues = (From iRow In gvData.Rows.Cast(Of GridViewRow)() _
                              Where iRow.Cells(iCalibUrIdx).Text <> "&nbsp;" _
                              Select New With {Key .Number = iRow.Cells(iCalibNoIdx).Text, Key .Name = iRow.Cells(iCalibUrIdx).Text})

                Dim iFValue = iValues.Distinct()
                Dim iValid As New Dictionary(Of String, String)
                Dim iList As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))
                If iFValue IsNot Nothing AndAlso iFValue.Count > 0 Then
                    For Each xval In iFValue
                        If iValid.ContainsKey(xval.Number) = True Then
                            If iValid(xval.Number) <> xval.Name Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 114, "Sorry, Duplicate calibration number is present for different calibrator(s), Calibration No : ") & " " & xval.Number & ". " & _
                                                              Language.getMessage(mstrModuleName, 113, "Please check the file and import again."), Me)
                                Exit Sub
                            End If
                        Else
                            iValid.Add(xval.Number, xval.Name)
                        End If
                        iList.Add(New KeyValuePair(Of String, String)(xval.Number, xval.Name))
                    Next

                    Call ImportData(iList)
                End If
            Else
                Dim iCalibUrIdx = getColumnID_Griview(gvData, cboCalibratorName.SelectedItem.Text, False, False)
                Dim iValues = From iRow In gvData.Rows.Cast(Of GridViewRow)() _
                              Where iRow.Cells(iCalibUrIdx).Text <> "&nbsp;" _
                              Select New With {Key .Number = iRow.Cells(iCalibUrIdx).Text, Key .Name = iRow.Cells(iCalibUrIdx).Text}

                Dim iFValue = iValues.Distinct()
                Dim iList As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))
                If iFValue IsNot Nothing AndAlso iFValue.Count > 0 Then
                    For Each xval In iFValue
                        iList.Add(New KeyValuePair(Of String, String)(xval.Number, xval.Name))
                    Next
                    Call ImportData(iList)
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub mnuExportEE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExportEE.Click
        Try
            Call ExportImportedData(2, "ImportError")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub mnuExportSF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExportSF.Click
        Try
            Call ExportImportedData(1, "SuccessfulImport")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub mnuExportEA_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExportEA.Click
        Try
            Call ExportImportedData(0, "AllData")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Language "

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Language._Object.setCaption(Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Language._Object.setCaption(Me.lblSelectFile.ID, Me.lblSelectFile.Text)
            Language._Object.setCaption(Me.btnUpload.ID, Me.btnUpload.Text)
            Language._Object.setCaption(Me.btnGetFileFormat.ID, Me.btnGetFileFormat.Text)
            Language._Object.setCaption(Me.lblCalibratorName.ID, Me.lblCalibratorName.Text)
            Language._Object.setCaption(Me.lblEmployeeCode.ID, Me.lblEmployeeCode.Text)
            Language._Object.setCaption(Me.lblEmployeeName.ID, Me.lblEmployeeName.Text)
            Language._Object.setCaption(Me.lblPeriodName.ID, Me.lblPeriodName.Text)
            Language._Object.setCaption(Me.lblCalibratorRating.ID, Me.lblCalibratorRating.Text)
            Language._Object.setCaption(Me.lblCalibrationRemark.ID, Me.lblCalibrationRemark.Text)
            Language._Object.setCaption(Me.lblCalibrationNo.ID, Me.lblCalibrationNo.Text)
            Language._Object.setCaption(Me.chkSubmitStatus.ID, Me.chkSubmitStatus.Text)
            Language._Object.setCaption(Me.lnkImportData.ID, Me.lnkImportData.Text)
            Language._Object.setCaption(Me.btnExport.ID, Me.btnExport.Text)
            Language._Object.setCaption(Me.mnuExportEE.ID, Me.mnuExportEE.Text)
            Language._Object.setCaption(Me.mnuExportSF.ID, Me.mnuExportSF.Text)
            Language._Object.setCaption(Me.mnuExportEA.ID, Me.mnuExportEA.Text)
            Language._Object.setCaption(Me.btnclose.ID, Me.btnclose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption(Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.lblSelectFile.Text = Language._Object.getCaption(Me.lblSelectFile.ID, Me.lblSelectFile.Text)
            Me.btnUpload.Text = Language._Object.getCaption(Me.btnUpload.ID, Me.btnUpload.Text)
            Me.btnGetFileFormat.Text = Language._Object.getCaption(Me.btnGetFileFormat.ID, Me.btnGetFileFormat.Text)
            Me.lblCalibratorName.Text = Language._Object.getCaption(Me.lblCalibratorName.ID, Me.lblCalibratorName.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.ID, Me.lblEmployeeCode.Text)
            Me.lblEmployeeName.Text = Language._Object.getCaption(Me.lblEmployeeName.ID, Me.lblEmployeeName.Text)
            Me.lblPeriodName.Text = Language._Object.getCaption(Me.lblPeriodName.ID, Me.lblPeriodName.Text)
            Me.lblCalibratorRating.Text = Language._Object.getCaption(Me.lblCalibratorRating.ID, Me.lblCalibratorRating.Text)
            Me.lblCalibrationRemark.Text = Language._Object.getCaption(Me.lblCalibrationRemark.ID, Me.lblCalibrationRemark.Text)
            Me.lblCalibrationNo.Text = Language._Object.getCaption(Me.lblCalibrationNo.ID, Me.lblCalibrationNo.Text)
            Me.chkSubmitStatus.Text = Language._Object.getCaption(Me.chkSubmitStatus.ID, Me.chkSubmitStatus.Text)
            Me.lnkImportData.Text = Language._Object.getCaption(Me.lnkImportData.ID, Me.lnkImportData.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.ID, Me.btnExport.Text)
            Me.mnuExportEE.Text = Language._Object.getCaption(Me.mnuExportEE.ID, Me.mnuExportEE.Text)
            Me.mnuExportSF.Text = Language._Object.getCaption(Me.mnuExportSF.ID, Me.mnuExportSF.Text)
            Me.mnuExportEA.Text = Language._Object.getCaption(Me.mnuExportEA.ID, Me.mnuExportEA.Text)
            Me.btnclose.Text = Language._Object.getCaption(Me.btnclose.ID, Me.btnclose.Text)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 101, "Sorry, Calibrator name is mandatory information. Please map field for calibrator name.")
            Language.setMessage(mstrModuleName, 102, "Sorry, Employee code is mandatory information. Please map field for employee code.")
            Language.setMessage(mstrModuleName, 103, "Sorry, Employee name is mandatory information. Please map field for employee name.")
            Language.setMessage(mstrModuleName, 104, "Sorry, Period name is mandatory information. Please map field for period name.")
            Language.setMessage(mstrModuleName, 105, "Sorry, Calibration rating is mandatory information. Please map field for calibration rating.")
            Language.setMessage(mstrModuleName, 106, "Sorry, Calibration remark is mandatory information. Please map field for calibration remark.")
            Language.setMessage(mstrModuleName, 107, "Sorry, Same field is mapped for")
            Language.setMessage(mstrModuleName, 108, "and")
            Language.setMessage(mstrModuleName, 109, ". Please map proper field.")
            Language.setMessage(mstrModuleName, 110, "Sorry, Calibration number is mandatory information. Please map field for calibration number.")
            Language.setMessage(mstrModuleName, 111, "Sorry, Data in particular column")
            Language.setMessage(mstrModuleName, 112, "is blank. Please check file again and import.")
            Language.setMessage(mstrModuleName, 113, "Please check the file and import again.")
            Language.setMessage(mstrModuleName, 114, "Sorry, Duplicate calibration number is present for different calibrator(s), Calibration No :")
            Language.setMessage(mstrModuleName, 115, "Sorry, directory path not found on server. Please check the path and try to import again.")
            Language.setMessage(mstrModuleName, 117, "Sorry, No data present to export.")
            Language.setMessage(mstrModuleName, 118, "Invalid Username")
            Language.setMessage(mstrModuleName, 119, "User does not contains calibration privileges")
            Language.setMessage(mstrModuleName, 120, "Invalid Employee")
            Language.setMessage(mstrModuleName, 121, "Invalid Period")
            Language.setMessage(mstrModuleName, 122, "Invalid Rating")
            Language.setMessage(mstrModuleName, 123, "Ratings Not Defined")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
