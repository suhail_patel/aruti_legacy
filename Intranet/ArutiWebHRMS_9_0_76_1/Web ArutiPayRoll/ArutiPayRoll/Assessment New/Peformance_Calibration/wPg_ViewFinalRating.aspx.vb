﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing

#End Region

Partial Class wPg_ViewFinalRating
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmViewRating"
    Private objScoreCalibrateApproval As New clsScoreCalibrationApproval
    Private mdtFullDataTable As DataTable = Nothing

#End Region

#Region " Private Method "

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objMData.Get_Database_Year_List("List", True, CInt(Session("CompanyUnkId")))
            Dim oRow As DataRow = dsList.Tables(0).NewRow()
            With oRow
                .Item("yearunkid") = 0
                .Item("financialyear_name") = Language.getMessage(mstrModuleName, 1, "Select")
                .Item("database_name") = ""
                .Item("companyunkid") = CInt(Session("CompanyUnkId"))
                .Item("isclosed") = False
                .Item("start_date") = ""
                .Item("end_date") = ""                
            End With
            dsList.Tables(0).Rows.InsertAt(oRow, 0)
            With cboYear
                .DataValueField = "yearunkid"
                .DataTextField = "financialyear_name"
                .DataSource = dsList.Tables(0)
                .SelectedValue = 0
                .DataBind()
            End With
            Call cboYear_SelectedIndexChanged(cboYear, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetInfo()
        Try
            Dim objEmp As New clsEmployee_Master
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(Session("Employeeunkid"))
            txtEmployeeName.Text = objEmp._Employeecode & " - " & objEmp._Firstname & " " & objEmp._Surname
            objEmp = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Dim strFilter As String = String.Empty
        Try
            mdtFullDataTable = objScoreCalibrateApproval.GetFinalCalibarionRating(CInt(Session("CompanyUnkId")), chkShowPreviousRating.Checked, CInt(Session("Employeeunkid")), CBool(Session("IsCalibrationSettingActive")), "", Nothing)
            If CInt(cboYear.SelectedValue) > 0 Then
                strFilter &= "AND yid = '" & CInt(cboYear.SelectedValue) & "' "
            End If
            If CInt(cboPeriod.SelectedValue) > 0 Then
                strFilter &= "AND pid = '" & CInt(cboPeriod.SelectedValue) & "' "
            End If
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            mdtFullDataTable = New DataView(mdtFullDataTable, strFilter, "yid,sdate", DataViewRowState.CurrentRows).ToTable()
            gvCalibrateList.DataSource = mdtFullDataTable
            gvCalibrateList.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                'S.SANDEEP |27-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()
                'S.SANDEEP |27-JUL-2019| -- END
                Call FillCombo()
                Call SetInfo()
                Call FillGrid()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Combobox Event(s) "

    Protected Sub cboYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(cboYear.SelectedValue), Session("Database_Name"), Session("fin_startdate"), "List", True, 0)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath") & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link's Event(s) "

    Protected Sub lnkInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            Dim objRating As New clsAppraisal_Rating
            Dim dsList As New DataSet
            Dim strDBName As String = gvCalibrateList.DataKeys(row.RowIndex)("dbname")

            dsList = objRating.getComboList("List", False, strDBName)
            gvRating.AutoGenerateColumns = False
            gvRating.DataSource = dsList
            gvRating.DataBind()
            objRating = Nothing
            popupinfo.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'S.SANDEEP |27-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Language._Object.setCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Language._Object.setCaption(Me.chkShowPreviousRating.ID, Me.chkShowPreviousRating.Text)
            Language._Object.setCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Language._Object.setCaption(Me.lblYear.ID, Me.lblYear.Text)
            Language._Object.setCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Language._Object.setCaption(Me.btnSearch.ID, Me.btnSearch.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)
            Language._Object.setCaption(Me.lblRatingInfo.ID, Me.lblRatingInfo.Text)
            Language._Object.setCaption(Me.btnCloseRating.ID, Me.btnCloseRating.Text)

            Language._Object.setCaption(gvCalibrateList.Columns(0).FooterText, gvCalibrateList.Columns(0).HeaderText)
            Language._Object.setCaption(gvCalibrateList.Columns(1).FooterText, gvCalibrateList.Columns(1).HeaderText)
            Language._Object.setCaption(gvCalibrateList.Columns(2).FooterText, gvCalibrateList.Columns(2).HeaderText)

            Language._Object.setCaption(gvRating.Columns(0).FooterText, gvRating.Columns(0).HeaderText)
            Language._Object.setCaption(gvRating.Columns(1).FooterText, gvRating.Columns(1).HeaderText)
            Language._Object.setCaption(gvRating.Columns(2).FooterText, gvRating.Columns(2).HeaderText)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Me.chkShowPreviousRating.Text = Language._Object.getCaption(Me.chkShowPreviousRating.ID, Me.chkShowPreviousRating.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblYear.Text = Language._Object.getCaption(Me.lblYear.ID, Me.lblYear.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text)
            Me.lblRatingInfo.Text = Language._Object.getCaption(Me.lblRatingInfo.ID, Me.lblRatingInfo.Text)
            Me.btnCloseRating.Text = Language._Object.getCaption(Me.btnCloseRating.ID, Me.btnCloseRating.Text)

            gvCalibrateList.Columns(0).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(0).FooterText, gvCalibrateList.Columns(0).HeaderText)
            gvCalibrateList.Columns(1).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(1).FooterText, gvCalibrateList.Columns(1).HeaderText)
            gvCalibrateList.Columns(2).HeaderText = Language._Object.getCaption(gvCalibrateList.Columns(2).FooterText, gvCalibrateList.Columns(2).HeaderText)

            gvRating.Columns(0).HeaderText = Language._Object.getCaption(gvRating.Columns(0).FooterText, gvRating.Columns(0).HeaderText)
            gvRating.Columns(1).HeaderText = Language._Object.getCaption(gvRating.Columns(1).FooterText, gvRating.Columns(1).HeaderText)
            gvRating.Columns(2).HeaderText = Language._Object.getCaption(gvRating.Columns(2).FooterText, gvRating.Columns(2).HeaderText)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |27-JUL-2019| -- END


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
