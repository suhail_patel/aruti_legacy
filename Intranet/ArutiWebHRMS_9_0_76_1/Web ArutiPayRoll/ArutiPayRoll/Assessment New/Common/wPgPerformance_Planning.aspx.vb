﻿#Region " Import "

Imports System.Data
Imports Aruti.Data
Imports System.Drawing
Imports AjaxControlToolkit
Imports System.IO
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Data.SqlClient
Imports ArutiReports

#End Region

Partial Class wPgPerformance_Planning
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmViewPerformancePlanning"
    Private ReadOnly mstrModuleName_AppRej As String = "frmApproveRejectPlanning"
    Private mdtEmpGoal As DataTable = Nothing
    Private mdtEmpCmpt As DataTable = Nothing
    Private objEmpField1 As New clsassess_empfield1_master
    Private objCmpt_Mast As New clsassess_competence_assign_master
    Private DisplayMessage As New CommonCodes
    Private mblnpopupShow_AproverReject As Boolean = False
    Private objCONN As SqlConnection

    'S.SANDEEP [23 DEC 2015] -- START
    Private dsHeaders As New DataSet
    Private iHeaderId As Integer = -1
    Private iMultiviewPageCount As Integer = 3
    'S.SANDEEP [23 DEC 2015] -- END

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Private strPerspectiveColName As String = String.Empty
    Private mintPerspectiveColIndex As Integer = -1
    Private strGoalValueColName As String = String.Empty
    Private mintGoalValueColIndex As Integer = -1
    Private mstrGroupName As String = String.Empty
    Private mblnIsGrpAdded As Boolean = False
    'S.SANDEEP |12-FEB-2019| -- END

    'S.SANDEEP |18-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Private mintUoMColIndex As Integer = -1
    Private mstrUoMColName As String = String.Empty
    'S.SANDEEP |18-FEB-2019| -- END

    'S.SANDEEP |04-DEC-2019| -- START
    'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB
    Private mstrReportComments As String = String.Empty
    'S.SANDEEP |04-DEC-2019| -- END

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Try
            If (Session("LoginBy") = Global.User.en_loginby.User) Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If Session("IsIncludeInactiveEmp") = False Then
                '    dsList = objEmp.GetEmployeeList("List", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
                'Else
                '    dsList = objEmp.GetEmployeeList("List", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                'End If

                'S.SANDEEP [04 Jan 2016] -- START
                'dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                '                                Session("UserId"), _
                '                                Session("Fin_year"), _
                '                                Session("CompanyUnkId"), _
                '                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                Session("UserAccessModeSetting"), True, _
                '                                Session("IsIncludeInactiveEmp"), "List", True)
                Dim strFilterQry As String = String.Empty
                Dim blnApplyFilter As Boolean = False 'Shani(14-APR-2016) --[True]

                'Shani(14-APR-2016) -- Start
                'If Session("SkipApprovalFlowInPlanning") = True Then
                'Shani(14-APR-2016) -- End
                    Dim csvIds As String = String.Empty
                    Dim dsMapEmp As New DataSet
                    Dim objEval As New clsevaluation_analysis_master

                    dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
                                                            Session("UserId"), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        True, Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)
                'Shani (12-Jan-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]
                    If dsMapEmp.Tables("List").Rows.Count > 0 Then

                        dsList = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                                  Session("UserId"), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                                  Session("IsIncludeInactiveEmp"), _
                                                                  CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)


                        strFilterQry = " hremployee_master.employeeunkid IN "
                        csvIds = String.Join(",", dsList.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
                        If csvIds.Trim.Length > 0 Then
                            strFilterQry &= "(" & csvIds & ")"
                        Else
                        strFilterQry &= "(0)"
                    End If

                    'Shani(14-APR-2016) -- Start
                Else
                    strFilterQry = " hremployee_master.employeeunkid IN (0) "
                    'Shani(14-APR-2016) -- End 
                End If

                'Shani(14-APR-2016) -- Start
                'End If
                'Shani(14-APR-2016) -- End 
                dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                                Session("UserId"), _
                                                Session("Fin_year"), _
                                                Session("CompanyUnkId"), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                Session("UserAccessModeSetting"), True, _
                                                Session("IsIncludeInactiveEmp"), "List", True, , , , , , , , , , , , , , , strFilterQry, , blnApplyFilter)

                'S.SANDEEP [04 Jan 2016] -- END
                

                'Shani(24-Aug-2015) -- End
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsList.Tables("List")
                    .SelectedValue = 0
                    .DataBind()
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee.Copy
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                End With
            End If


            'S.SANDEEP [17 NOV 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [17 NOV 2015] -- END


            'S.SANDEEP [04 JUN 2015] -- START

            'Shani (09-May-2016) -- Start
            'Dim mintCurrentPeriodId As Integer = 0
            'If dsList.Tables(0).Rows.Count > 1 Then
            '    mintCurrentPeriodId = dsList.Tables(0).Rows(dsList.Tables(0).Rows.Count - 1).Item("periodunkid")
            'End If
            Dim mintCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End

            'S.SANDEEP [04 JUN 2015] -- END

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                'S.SANDEEP [04 JUN 2015] -- START
                .SelectedValue = mintCurrentPeriodId
                'S.SANDEEP [04 JUN 2015] -- END
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Public Sub MergeRows(ByVal gridView As GridView)
        Try
        If gridView.Rows.Count > 1 AndAlso mintPerspectiveColIndex > 0 Then
            For rowIndex As Integer = gridView.Rows.Count - 2 To 0 Step -1
                Dim row As GridViewRow = gridView.Rows(rowIndex)
                Dim previousRow As GridViewRow = gridView.Rows(rowIndex + 1)
                If row.Cells(mintPerspectiveColIndex).Text = previousRow.Cells(mintPerspectiveColIndex).Text Then
                    row.Cells(mintPerspectiveColIndex).RowSpan = If(previousRow.Cells(mintPerspectiveColIndex).RowSpan < 2, 2, previousRow.Cells(mintPerspectiveColIndex).RowSpan + 1)
                    previousRow.Cells(mintPerspectiveColIndex).Visible = False
                End If
            Next
        End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        
    End Sub
    'S.SANDEEP |12-FEB-2019| -- END

    Private Sub Fill_Goals_Grid()
        Dim objFMaster As New clsAssess_Field_Master(True)
        Try
            If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, No data defined for asseessment caption settings screen."), Me)
                Exit Sub
            End If

            Dim objMap As New clsAssess_Field_Mapping
            If objMap.isExist(CInt(cboPeriod.SelectedValue)) = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry no field is mapped with the selected period."), Me)
                Exit Sub
            End If
            objMap = Nothing

            'S.SANDEEP [23 APR 2015] -- START
            objEmpField1 = New clsassess_empfield1_master
            'S.SANDEEP [23 APR 2015] -- END

            'S.SANDEEP |05-APR-2019| -- START
            'mdtEmpGoal = objEmpField1.GetDisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List")
            mdtEmpGoal = objEmpField1.GetDisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", Session("CascadingTypeId"))
            'S.SANDEEP |05-APR-2019| -- END


            dgvPData.AutoGenerateColumns = False
            Dim iColName As String = String.Empty

            Dim iPlan() As String = Nothing
            If CStr(Session("ViewTitles_InPlanning")).Trim.Length > 0 Then
                iPlan = CStr(Session("ViewTitles_InPlanning")).Split("|")
            End If

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            Dim intTotalWidth As Integer = 0
            If iPlan IsNot Nothing Then
                For index As Integer = 0 To iPlan.Length - 1
                    intTotalWidth += objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, iPlan(index), Session("CompanyUnkId"))
                Next
            End If
            'SHANI [09 Mar 2015]--END 

            If dgvPData.Columns.Count > 0 Then dgvPData.Columns.Clear()

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            mstrGroupName = "" : objlblCurrentStatus.Text = ""
            strPerspectiveColName = mdtEmpGoal.Columns.Cast(Of DataColumn)().Where(Function(x) CInt(x.ExtendedProperties(x.ColumnName)) = CInt(clsAssess_Field_Master.enOtherInfoField.PERSPECTIVE)).Select(Function(x) x.ColumnName).First()
            strGoalValueColName = mdtEmpGoal.Columns.Cast(Of DataColumn)().Where(Function(x) CInt(x.ExtendedProperties(x.ColumnName)) = CInt(clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE)).Select(Function(x) x.ColumnName).First()
            If mdtEmpGoal.Rows.Count > 0 Then
                'S.SANDEEP |07-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#0004244}
                'mstrGroupName = mdtEmpGoal.AsEnumerable().Where(Function(x) x.Field(Of String)("STypeName") <> "").Select(Function(x) x.Field(Of String)("STypeName")).First
                mstrGroupName = mdtEmpGoal.AsEnumerable().Where(Function(x) x.Field(Of String)("STypeName") <> "").Select(Function(x) x.Field(Of String)("STypeName")).DefaultIfEmpty().First
                If mstrGroupName Is Nothing Then mstrGroupName = ""
                'S.SANDEEP |07-NOV-2019| -- END
                mdtEmpGoal.Columns.Remove("STypeName")
            End If
            If mstrGroupName.Trim.Length > 0 Then objlblCurrentStatus.Text = "Current Status : " & mstrGroupName
            'S.SANDEEP |12-FEB-2019| -- END

            'S.SANDEEP |18-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            mstrUoMColName = mdtEmpGoal.Columns.Cast(Of DataColumn)().Where(Function(x) x.ColumnName = "UoMType").Select(Function(x) x.ColumnName).First()
            'S.SANDEEP |18-FEB-2019| -- END

            For Each dCol As DataColumn In mdtEmpGoal.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                Dim dgvCol As New BoundField()
                dgvCol.FooterText = iColName
                dgvCol.ReadOnly = True
                dgvCol.DataField = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                If dgvPData.Columns.Contains(dgvCol) = True Then Continue For
                'If CInt(Session("CascadingTypeId")) = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                If dgvCol.DataField.StartsWith("Owr") Then
                    dgvCol.Visible = False
                End If
                'End If
                If dCol.Caption.Length <= 0 Or dCol.ColumnName = "Emp" Or dCol.ColumnName = "OPeriod" Then
                    dgvCol.Visible = False
                Else
                    If mdtEmpGoal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then

                        'SHANI [09 Mar 2015]-START
                        'Enhancement - REDESIGN SELF SERVICE.
                        'dgvCol.HeaderStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtEmpGoal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)))
                        'dgvCol.ItemStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtEmpGoal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)))

                        'SHANI [21 Mar 2015]-START
                        'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                        'Dim decColumnWidth As Decimal = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtEmpGoal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)) * 100 / intTotalWidth
                        'dgvCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
                        'dgvCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)

                        'S.SANDEEP |12-MAR-2019| -- START
                        'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}
                        'If intTotalWidth > 0 Then
                        '    dgvCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
                        '    dgvCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
                        'Else
                        '    dgvCol.HeaderStyle.Width = Unit.Pixel(decColumnWidth)
                        '    dgvCol.ItemStyle.Width = Unit.Pixel(decColumnWidth)
                        'End If
                        'Dim decColumnWidth As Decimal
                        'If intTotalWidth > 0 Then
                        '    decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtEmpGoal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)) * 100 / intTotalWidth
                        'Else
                        '    decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtEmpGoal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName))
                        'End If
                        Dim decColumnWidth As Decimal
                        decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtEmpGoal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), Session("CompanyUnkId"))
                        dgvCol.HeaderStyle.Width = Unit.Pixel(decColumnWidth)
                        dgvCol.ItemStyle.Width = Unit.Pixel(decColumnWidth)
                        'S.SANDEEP |12-MAR-2019| -- END


                        'SHANI [21 Mar 2015]--END

                        'SHANI [09 Mar 2015]--END
                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, mdtEmpGoal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                            End If
                        End If
                    End If
                End If

                'S.SANDEEP [01 JUL 2015] -- START
                If dCol.ColumnName = "vuRemark" Or dCol.ColumnName = "vuProgress" Then
                    dgvCol.Visible = False
                End If
                'S.SANDEEP [01 JUL 2015] -- END

                dgvPData.Columns.Add(dgvCol)

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                mdtEmpGoal.Columns(dCol.ColumnName).ExtendedProperties.Add("index", dgvPData.Columns.IndexOf(dgvCol))
                'Pinkal (16-Apr-2016) -- End

                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                If dCol.ColumnName = "OPeriod" Then
                    dgvCol.Visible = False
                End If
                If dgvCol.Visible = True Then
                    If strPerspectiveColName = dCol.ColumnName Then
                        mintPerspectiveColIndex = dgvPData.Columns.IndexOf(dgvCol)
                    End If
                End If
                If dgvCol.Visible = True Then
                    If strGoalValueColName = dCol.ColumnName Then
                        mintGoalValueColIndex = dgvPData.Columns.IndexOf(dgvCol)
                    End If
                End If
                'S.SANDEEP |12-FEB-2019| -- END

                'S.SANDEEP |18-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                If mstrUoMColName = dCol.ColumnName Then
                    mintUoMColIndex = dgvPData.Columns.IndexOf(dgvCol)
                End If
                'S.SANDEEP |18-FEB-2019| -- END

            Next
            'Dim idgvWidth As Integer = 0
            'For i As Integer = 0 To dgvPData.Columns.Count - 1
            '    idgvWidth = idgvWidth + CInt(dgvPData.Columns(i).ItemStyle.Width.Value)
            'Next

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'pnl_dgvPData.Height = Unit.Pixel(400)
            'SHANI [09 Mar 2015]--END

            'dgvPData.Width = Unit.Pixel(idgvWidth)

            For Each dgvRow As DataRow In mdtEmpGoal.Rows
                If CBool(dgvRow("isfinal")) = True Then
                    'dgvPData.Style.Add("color", "blue")
                    If Session("BSC_StatusColors").Keys.Count > 0 Then
                        If Session("BSC_StatusColors").ContainsKey(CInt(enObjective_Status.FINAL_SAVE)) Then
                            dgvPData.Style.Add("color", ColorTranslator.ToHtml(Color.FromArgb(CInt(Session("BSC_StatusColors")(CInt(enObjective_Status.FINAL_SAVE))))))
                            objlblCurrentStatus.ForeColor = Color.FromArgb(CInt(Session("BSC_StatusColors")(CInt(enObjective_Status.FINAL_SAVE))))
                        Else
                    dgvPData.Style.Add("color", "blue")
                            objlblCurrentStatus.ForeColor = Color.Blue
                        End If
                    Else
                        dgvPData.Style.Add("color", "blue")
                        objlblCurrentStatus.ForeColor = Color.Blue
                    End If
                    Exit For
                ElseIf CInt(dgvRow("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then
                    'dgvPData.Style.Add("color", "Green")
                    If Session("BSC_StatusColors").Keys.Count > 0 Then
                        If Session("BSC_StatusColors").ContainsKey(CInt(enObjective_Status.SUBMIT_APPROVAL)) Then
                            dgvPData.Style.Add("color", ColorTranslator.ToHtml(Color.FromArgb(CInt(Session("BSC_StatusColors")(CInt(enObjective_Status.SUBMIT_APPROVAL))))))
                            objlblCurrentStatus.ForeColor = Color.FromArgb(CInt(Session("BSC_StatusColors")(CInt(enObjective_Status.SUBMIT_APPROVAL))))
                        Else
                    dgvPData.Style.Add("color", "Green")
                            objlblCurrentStatus.ForeColor = Color.Green
                        End If
                    Else
                    dgvPData.Style.Add("color", "Green")
                        objlblCurrentStatus.ForeColor = Color.Green
                    End If
                    Exit For
                Else
                    'dgvPData.Style.Add("color", "black")
                    Dim strHex As String = ""
                    Select Case CInt(dgvRow("opstatusid"))
                        Case enObjective_Status.OPEN_CHANGES
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.OPEN_CHANGES), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.OPEN_CHANGES))), ColorTranslator.ToHtml(Color.Black))
                        Case enObjective_Status.NOT_SUBMIT
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.NOT_SUBMIT), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.NOT_SUBMIT))), ColorTranslator.ToHtml(Color.Red))
                        Case enObjective_Status.NOT_COMMITTED
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.NOT_COMMITTED), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.NOT_COMMITTED))), ColorTranslator.ToHtml(Color.Purple))
                        Case enObjective_Status.FINAL_COMMITTED
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.FINAL_COMMITTED), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.FINAL_COMMITTED))), ColorTranslator.ToHtml(Color.Blue))
                        Case enObjective_Status.PERIODIC_REVIEW
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.PERIODIC_REVIEW), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.PERIODIC_REVIEW))), ColorTranslator.ToHtml(Color.Brown))
                    End Select
                    dgvPData.Style.Add("color", strHex)
                    objlblCurrentStatus.ForeColor = ColorTranslator.FromHtml(strHex)
                    Exit For
                End If
            Next

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            dgvPData.Width = Unit.Percentage(99.5)
            'SHANI [09 Mar 2015]--END 

            dgvPData.DataSource = mdtEmpGoal
            dgvPData.DataBind()

            Call Set_Totals_Label()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objFMaster = Nothing
        End Try
    End Sub

    Private Sub Fill_Competancy_Grid()
        Dim xDecTotalWgt As Decimal = 0
        Try
            'S.SANDEEP |02-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
            Dim exOrdr As String = Session("Perf_EvaluationOrder").ToString
            If exOrdr Is Nothing Then exOrdr = ""
            If exOrdr.Trim.Length > 0 Then
                Dim str As String() = exOrdr.Split("|")
                If Array.IndexOf(str, CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString) = -1 Then Exit Sub
            End If
            'S.SANDEEP |02-MAR-2020| -- END

            Dim dsList As New DataSet

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objCmpt_Mast.GetList("List", 0, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), Session("Self_Assign_Competencies"))
            dsList = objCmpt_Mast.GetList("List", eZeeDate.convertDate(Session("EmployeeAsOnDate")), 0, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), Session("Self_Assign_Competencies"))
            'Shani(20-Nov-2015) -- End


            'S.SANDEEP [14 APR 2015] -- START
            If dsList Is Nothing Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, there is no list of competencies assigned to this employee or some defined assessment group does not fall in the allocation of the selected employee."), Me)
                Exit Sub
            End If
            'S.SANDEEP [14 APR 2015] -- END

            'Shani (05-Sep-2016) -- Start
            'mdtEmpCmpt = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            Dim dtTable As DataTable = dsList.Tables(0).Clone
            Dim intCatUnkId As Integer = 0
            Dim dtRow As DataRow = Nothing
            dtTable.Columns.Add("isGrp", Type.GetType("System.Boolean"))

            For Each dRow As DataRow In dsList.Tables(0).Rows
                If intCatUnkId <> CInt(dRow.Item("masterunkid")) Then
                    dtRow = dtTable.NewRow
                    dtRow.Item("isGrp") = True
                    dtRow.Item("competencies") = dRow.Item("ccategory")
                    dtRow.Item("assigned_weight") = 0
                    dtRow.Item("isfinal") = dRow.Item("isfinal")
                    dtRow.Item("opstatusid") = dRow.Item("opstatusid")
                    dtRow.Item("masterunkid") = dRow.Item("masterunkid")
                    intCatUnkId = dRow.Item("masterunkid")
                    dtTable.Rows.Add(dtRow)
                End If
                dtRow = dtTable.NewRow
                dtRow.Item("isGrp") = False
                For Each dCol As DataColumn In dsList.Tables(0).Columns
                    dtRow.Item(dCol.ColumnName) = dRow.Item(dCol.ColumnName)
                Next
                dtTable.Rows.Add(dtRow)
            Next
            mdtEmpCmpt = New DataView(dtTable, "", "", DataViewRowState.CurrentRows).ToTable
            'Shani (05-Sep-2016) -- End




            'Dim idgvWidth As Integer = 0
            'For i As Integer = 0 To dgvPData.Columns.Count - 1
            '    idgvWidth = idgvWidth + CInt(dgvPData.Columns(i).ItemStyle.Width.Value)
            'Next
            pnl_dgvCData.Height = Unit.Pixel(400)
            'dgvCData.Width = Unit.Pixel(idgvWidth)

            If mdtEmpCmpt.Rows.Count > 0 Then
                xDecTotalWgt = mdtEmpCmpt.Compute("SUM(assigned_weight)", "")
            End If

            For Each dgvRow As DataRow In mdtEmpCmpt.Rows
                If CBool(dgvRow("isfinal")) = True Or CInt(dgvRow("opstatusid")) = enObjective_Status.FINAL_SAVE Then
                    dgvCData.Style.Add("color", "blue")
                    Exit For
                ElseIf CInt(dgvRow("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then
                    dgvCData.Style.Add("color", "Green")
                    Exit For
                Else
                    dgvCData.Style.Add("color", "black")
                    Exit For
                End If
            Next

            dgvCData.DataSource = mdtEmpCmpt
            dgvCData.DataBind()
            Call Set_Totals_Label(xDecTotalWgt)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Set_Totals_Label(Optional ByVal xCmptWeght As Decimal = 0)
        Try
            Dim xMessage As String = String.Empty
            Dim objFMapping As New clsAssess_Field_Mapping
            Dim xTotalWeight As Double = 0
            xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
            If xTotalWeight > 0 Then
                xMessage = Language.getMessage(mstrModuleName, 3, "Total Goals Weight :") & " " & xTotalWeight.ToString
            Else
                xMessage = ""
            End If
            objFMapping = Nothing


            'SHANI [25 Mar 2015]-START
            'ENHANCEMENT : Claim AND Request CHANGES.
            'If CBool(Session("Self_Assign_Competencies")) = True Then
            '    If xCmptWeght > 0 Then
            '        xMessage &= " | " & Language.getMessage(mstrModuleName, 4, "Total Competency Weight :") & " " & xCmptWeght.ToString
            '    End If
            'End If
            If xCmptWeght > 0 Then
                xMessage &= " | " & Language.getMessage(mstrModuleName, 4, "Total Competency Weight :") & " " & xCmptWeght.ToString
            End If
            'SHANI [25 Mar 2015]--END

            objlblTotalWeight.Text = xMessage
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Clear_Session()
        Try
            Session.Remove("mdtEmpCmpt")
            Session.Remove("mdtEmpGoal")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Get_Assessor(ByRef iAssessorEmpId As Integer, ByRef sAssessorName As String) As Integer
        Dim iAssessor As Integer = 0
        Try
            Dim dLst As New DataSet

            'Shani(01-MAR-2016) -- Start
            'Enhancement
            'Dim objBSC As New clsBSC_Analysis_Master
            'dLst = objBSC.getAssessorComboList("List", False, False, CInt(Session("Userid")))
            Dim objBSC As New clsevaluation_analysis_master
            dLst = objBSC.getAssessorComboList(CStr(Session("Database_Name")), _
                                               CInt(Session("Userid")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               True, True, "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False, CInt(cboEmployee.SelectedValue))

            'Shani(14-FEB-2017) -- Add Visibility[clsAssessor.enARVisibilityTypeId.VISIBLE]
            'Shani(01-MAR-2016) -- End



            If dLst.Tables(0).Rows.Count > 0 Then
                iAssessor = CInt(dLst.Tables(0).Rows(0).Item("Id"))
                iAssessorEmpId = objBSC.GetAssessorEmpId(iAssessor)
                sAssessorName = CStr(dLst.Tables(0).Rows(0).Item("Name"))
            End If
            objBSC = Nothing
            Return iAssessor
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    'S.SANDEEP [23 APR 2015] -- START
    Private Sub InsertStatus(ByVal iAssessorMasterId As Integer, ByVal iAssessorEmpId As Integer, ByVal sAssessorName As String)
        Try
            Dim objStatus As New clsassess_empstatus_tran
            objStatus._Assessoremployeeunkid = iAssessorMasterId
            objStatus._Assessormasterunkid = iAssessorEmpId
            objStatus._Commtents = txtComments.Text
            objStatus._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objStatus._Periodunkid = CInt(cboPeriod.SelectedValue)
            objStatus._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            objStatus._Statustypeid = enObjective_Status.FINAL_SAVE
            objStatus._Isunlock = False
            objStatus._Userunkid = Session("Userid")
            'S.SANDEEP [29 JAN 2015] -- START
            'If objStatus.Insert() = False Then
            If objStatus.Insert(, Session("Self_Assign_Competencies")) = False Then
                'S.SANDEEP [29 JAN 2015] -- END
                DisplayMessage.DisplayMessage(objStatus._Message, Me)
                Exit Sub
            Else
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, "", True, False)

                'S.SANDEEP [01-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001965}
                'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, "", True, False, CInt(Session("CompanyUnkId")), , , Session("Ntf_GoalsUnlockUserIds").ToString())
                objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, "", True, False, CInt(Session("CompanyUnkId")), , , , Session("Ntf_GoalsUnlockUserIds").ToString())
                'S.SANDEEP [01-Feb-2018] -- END

                'Sohail (30 Nov 2017) -- End
                objEmpField1 = Nothing
            End If
            objStatus = Nothing
            Call Fill_Goals_Grid() : Call Fill_Competancy_Grid()
            mblnpopupShow_AproverReject = False
            popup_ApprovrReject.Hide()
            If Request.QueryString.Count > 0 Then
                Response.Redirect("~/Index.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [23 APR 2015] -- END

    'S.SANDEEP [23 DEC 2015] -- START
    Private Sub Fill_Custom_Items_Grid(ByVal intHeaderUnkid As Integer)
        Try
            Dim objPlannedItems As New clsassess_plan_customitem_tran
            Dim mdtTable As DataTable

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'mdtTable = objPlannedItems.GetList(cboEmployee.SelectedValue, cboPeriod.SelectedValue, intHeaderUnkid, "List")
            mdtTable = objPlannedItems.GetList(cboEmployee.SelectedValue, cboPeriod.SelectedValue, intHeaderUnkid, "List", CBool(Session("IsCompanyNeedReviewer")))
            'Shani (26-Sep-2016) -- End


            dgvItems.Columns.Clear()
            dgvItems.DataSource = Nothing
            dgvItems.AutoGenerateColumns = False
            Dim iColName As String = String.Empty
            For Each dCol As DataColumn In mdtTable.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                Dim dgvCol As New BoundField()
                dgvCol.FooterText = iColName
                dgvCol.ReadOnly = True
                dgvCol.DataField = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                If dgvItems.Columns.Contains(dgvCol) = True Then Continue For
                If dCol.Caption.Length <= 0 Then
                    dgvCol.Visible = False
                End If
                dgvItems.Columns.Add(dgvCol)
                If dgvCol.FooterText = "objHeader_Name" Then dgvCol.Visible = False
            Next
            dgvItems.DataSource = mdtTable
            dgvItems.DataBind()
            objPlannedItems = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [23 DEC 2015] -- END

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    Private Sub SetVisibility()

        Try
            Dim objReport As New clsArutiReportClass
            Dim dsData As DataSet = objReport.getReportList(CInt(Session("UserId")), Session("CompanyUnkId"), True)
            Dim drRow() As DataRow = dsData.Tables(0).Select("ReportId=" & enArutiReport.EmployeeAssessmentFormReport)

            'S.SANDEEP |22-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : ISSUE ON PA
            If drRow.Length > 0 Then
                btnViewTemplate.Visible = True
            Else
                btnViewTemplate.Visible = False
            End If
            'S.SANDEEP |22-MAR-2019| -- END



        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub
    'Varsha Rana (17-Oct-2017) -- End

    'S.SANDEEP |09-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : PA CHANGES
    Private Sub UnlockFinalSavedGoals(ByVal blnVoidProgressUpdate As Boolean)
        Try
            Dim iAssessorMasterId, iAssessorEmpId As Integer
            Dim sAssessorName As String = ""
            iAssessorMasterId = 0 : iAssessorEmpId = 0
            iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
            Dim objEmpField1 As New clsassess_empfield1_master
            If txtUnlockReason.Text.Trim.Length > 0 Then
                Dim objEStatusTran As New clsassess_empstatus_tran
                objEStatusTran._Assessoremployeeunkid = 0
                objEStatusTran._Assessormasterunkid = 0
                objEStatusTran._Commtents = txtUnlockReason.Text
                objEStatusTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEStatusTran._Isunlock = True
                objEStatusTran._Loginemployeeunkid = 0
                objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
                objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
                objEStatusTran._Statustypeid = enObjective_Status.OPEN_CHANGES
                objEStatusTran._Userunkid = CInt(Session("UserID"))
                If objEStatusTran.Insert(, Session("Self_Assign_Competencies"), blnVoidProgressUpdate) = False Then
                    DisplayMessage.DisplayMessage(objEStatusTran._Message, Me)
                    Exit Sub
                Else
                    objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, txtUnlockReason.Text, False, True, CInt(Session("CompanyUnkId")), , , , Session("Ntf_GoalsUnlockUserIds").ToString(), blnVoidProgressUpdate)
                    Call Fill_Goals_Grid() : Call Fill_Competancy_Grid()
                End If
                objEStatusTran = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |09-JUL-2019| -- END

#End Region

#Region " Page Event "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                'Sohail (02 Apr 2019) -- Start
                'NMB Issue - 74.1 - Error "On Load Event !! Bad Data" on clicking any page with link after session get expired.
                If Request.QueryString.Count <= 0 Then Exit Sub
                'Sohail (02 Apr 2019) -- End

                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")
                If arr.Length = 5 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    Blank_ModuleName()
                    clsCommonATLog._WebFormName = "frmAssignCompetencies"
                    StrModuleName2 = "mnuAssessment"
                    StrModuleName3 = "mnuSetups"
                    clsCommonATLog._WebClientIP = Session("IP_ADD")
                    clsCommonATLog._WebHostName = Session("HOST_NAME")
                    Me.ViewState.Add("IsDirect", True)

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    HttpContext.Current.Session("UserId") = CInt(arr(1))
                    Me.ViewState.Add("employeeunkid", CInt(arr(2)))
                    Me.ViewState.Add("assessormasterunkid", CInt(arr(3)))
                    Me.ViewState.Add("periodid", CInt(arr(4)))

                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Dim objCommon As New CommonCodes
                    'objCommon.GetCompanyYearInfo(CInt(Session("CompanyUnkId")))
                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If
                    'Sohail (30 Mar 2015) -- End
                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp
                    Session("EmployeeAsOnDate") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)
                    Session("Self_Assign_Competencies") = clsConfig._Self_Assign_Competencies
                    Session("ViewTitles_InPlanning") = clsConfig._ViewTitles_InPlanning

                    'S.SANDEEP [29-NOV-2017] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # (38,41)
                    Session("Ntf_FinalAcknowledgementUserIds") = clsConfig._Ntf_FinalAcknowledgementUserIds
                    Session("Ntf_GoalsUnlockUserIds") = clsConfig._Ntf_GoalsUnlockUserIds
                    'S.SANDEEP [29-NOV-2017] -- END


                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    'Sohail (21 Mar 2015) -- Start
                    'Enhancement - New UI Notification Link Changes.
                    'Dim clsuser As New User(objUser._Username, objUser._Password, Global.User.en_loginby.User, Session("mdbname"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname"))
                    'Sohail (21 Mar 2015) -- End
                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName
                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If
                    'Sohail (30 Mar 2015) -- End

                    Dim objUserPrivilege As New clsUserPrivilege
                    objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))

                    'S.SANDEEP [28 MAY 2015] -- START
                    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                    'Session("Allow_FinalSaveBSCPlanning") = objUserPrivilege._Allow_FinalSaveBSCPlanning
                    'Session("Allow_UnlockFinalSaveBSCPlanning") = objUserPrivilege._Allow_UnlockFinalSaveBSCPlanning

                    Session("AllowtoApproveRejectGoalsPlanning") = objUserPrivilege._AllowtoApproveRejectGoalsPlanning
                    Session("Allow_UnlockFinalSaveBSCPlanning") = objUserPrivilege._AllowtoUnlockFinalSavedGoals
                    'S.SANDEEP [28 MAY 2015] -- END



                    If (Session("LoginBy") = Global.User.en_loginby.User) Then
                        'S.SANDEEP [28 MAY 2015] -- START
                        'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                        'btnApproveSubmitted.Visible = Session("Allow_FinalSaveBSCPlanning")
                        'btnUnlockFinalSave.Visible = Session("Allow_UnlockFinalSaveBSCPlanning")

                        btnApproveSubmitted.Visible = Session("AllowtoApproveRejectGoalsPlanning")
                        btnUnlockFinalSave.Visible = Session("Allow_UnlockFinalSaveBSCPlanning")
                        btnSubmitforApproval.Visible = Session("AllowtoSubmitGoalsforApproval")
                        'S.SANDEEP [28 MAY 2015] -- END

                        'SHANI [09 Mar 2015]-START
                        'Enhancement - REDESIGN SELF SERVICE.
                        'btnSubmitforApproval.Visible = False
                        'SHANI [21 Mar 2015]-START
                        'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                        btnSubmitforApproval.Visible = False
                        'SHANI [21 Mar 2015]--END 
                        'SHANI [09 Mar 2015]--END
                    ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                        btnApproveSubmitted.Visible = False
                        btnUnlockFinalSave.Visible = False
                        btnSubmitforApproval.Visible = True
                    End If

                    'Varsha (17 Oct 2017) -- Start
                    'Enhancement : Give user privileges.
                    Call SetVisibility()
                    'Varsha (17 Oct 2017) -- End

                    Call FillCombo()
                    cboEmployee.SelectedValue = Me.ViewState("employeeunkid") : cboEmployee.Enabled = False
                    cboPeriod.SelectedValue = Me.ViewState("periodid") : cboPeriod.Enabled = False
                    Call btnSearch_Click(New Object(), New EventArgs()) : btnSearch.Enabled = False : btnReset.Enabled = False

                    If CBool(mdtEmpGoal.Rows(0)("isfinal")) = True Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, Performance Planning is already approved for selected employee and period. ."), Me, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")

                    End If

                    If CBool(mdtEmpGoal.Rows(0)("isfinal")) <> True AndAlso mdtEmpGoal.Rows(0)("opstatusid") <> enObjective_Status.SUBMIT_APPROVAL Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, Performance Planning is yet not submitted for selected employee and period."), Me, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                    End If

                    btnUnlockFinalSave.Visible = False

                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                    'Sohail (30 Mar 2015) -- End
                    HttpContext.Current.Session("Login") = True
                    GoTo Link
                End If
            End If

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmAssignCompetencies"
            StrModuleName2 = "mnuAssessment"
            StrModuleName3 = "mnuSetups"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")
            If IsPostBack = False Then
                Call FillCombo()
                'Varsha (17 Oct 2017) -- Start
                'Enhancement : Give user privileges.
                Call SetVisibility()
                'Varsha (17 Oct 2017) -- End
                If (Session("LoginBy") = Global.User.en_loginby.User) Then

                    'S.SANDEEP [28 MAY 2015] -- START
                    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                    'btnApproveSubmitted.Visible = Session("Allow_FinalSaveBSCPlanning")
                    'btnUnlockFinalSave.Visible = Session("Allow_UnlockFinalSaveBSCPlanning")

                    btnApproveSubmitted.Visible = Session("AllowtoApproveRejectGoalsPlanning")
                    btnUnlockFinalSave.Visible = Session("Allow_UnlockFinalSaveBSCPlanning")
                    'S.SANDEEP |25-MAR-2019| -- START
                    'btnSubmitforApproval.Visible = Session("AllowtoSubmitGoalsforApproval")
                    btnSubmitforApproval.Visible = False
                    'S.SANDEEP |25-MAR-2019| -- END

                    'S.SANDEEP [28 MAY 2015] -- END


                    'SHANI [09 Mar 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    'btnSubmitforApproval.Visible = False
                    'SHANI [09 Mar 2015]--END
                ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    btnApproveSubmitted.Visible = False
                    btnUnlockFinalSave.Visible = False
                    btnSubmitforApproval.Visible = True

                    'Shani(24-JAN-2017) -- Start
                    ''S.SANDEEP [04 JUN 2015] -- START
                    'Call btnSearch_Click(btnSearch, Nothing)
                    ''S.SANDEEP [04 JUN 2015] -- END
                    If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                        Call btnSearch_Click(btnSearch, Nothing)
                    End If
                    'Shani(24-JAN-2017) -- End
                End If
                'lblcnfTMessage.Text = Language.getMessage(mstrModuleName, 4, "This approving process will transfer the goals to your subordinate employee(s), Those employee(s) who has already committed their goals will not be affected by this process. Do you wish to continue?")
                lblcnfTMessage.Text = "This approving process will transfer the goals to your subordinate employee(s), Those employee(s) who has already committed their goals will not be affected by this process. Do you wish to continue?"
            End If

Link:

            'Shani (01-Dec-2016) -- Start
            'Issue - CCBRT not showing Custom item 
            If IsPostBack = False Then
                dsHeaders = (New clsassess_custom_header).getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                Dim dttble As DataTable = dsHeaders.Tables(0).Clone
                If dsHeaders.Tables(0).Select("isinclude_planning = 1").Length > 0 Then
                    dttble = dsHeaders.Tables(0).Select("isinclude_planning = 1").CopyToDataTable
                End If
                dsHeaders.Tables.RemoveAt(0)
                dsHeaders.Tables.Add(dttble)
                If dttble.Rows.Count <= 0 Then
                    iMultiviewPageCount = 2
                End If


                'Pinkal (22-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If HttpContext.Current.Session("CompanyGroupName").ToString().ToUpper() = "NMB PLC" Then
                    btnPrevious.Visible = False
                    btnNext.Visible = False
                End If
                'Pinkal (22-Mar-2019) -- End


            End If
            'Shani (01-Dec-2016) -- End


            If Session("mdtEmpGoal") IsNot Nothing Then
                mdtEmpGoal = Me.Session("mdtEmpGoal")
            End If
            If Session("mdtEmpCmpt") IsNot Nothing Then
                mdtEmpCmpt = Me.Session("mdtEmpCmpt")
            End If


            'S.SANDEEP [23 DEC 2015] -- START
            If Me.ViewState("Headers") IsNot Nothing Then
                dsHeaders = Me.ViewState("Headers")
            End If

            If Me.ViewState("iHeaderId") IsNot Nothing Then
                iHeaderId = Me.ViewState("iHeaderId")
            End If

            If Me.ViewState("iMultiviewPageCount") IsNot Nothing Then
                iMultiviewPageCount = Me.ViewState("iMultiviewPageCount")
            End If
            'S.SANDEEP [23 DEC 2015] -- END


            mblnpopupShow_AproverReject = Me.ViewState("mblnpopupShow_AproverReject")
            If mblnpopupShow_AproverReject Then
                popup_ApprovrReject.Show()
            End If

            'S.SANDEEP [12 FEB 2015] -- START
            'If CBool(Session("Self_Assign_Competencies")) = True Then
            '    btnNext.Visible = True
            '    btnPrevious.Visible = True
            'Else
            '    btnNext.Visible = False
            '    btnPrevious.Visible = False
            'End If
            'S.SANDEEP [12 FEB 2015] -- END


            'S.SANDEEP [23 DEC 2015] -- START
            If Session("SkipApprovalFlowInPlanning") = True Then

                'Shani(06-Feb-2016) -- Start
                'PA Changes Given By CCBRT
                'btnCommitGoals.Visible = True
                'btnOpenGoals.Visible = True
                'btnViewTemplate.Visible = True
                btnCommitGoals.Visible = CBool(Session("AllowtoCommitAllocationGoals"))
                btnOpenGoals.Visible = CBool(Session("AllowtoUnlockcommittedAllocationGoals"))
                'S.SANDEEP |22-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : ISSUE ON PA
                'btnViewTemplate.Visible = True
                'S.SANDEEP |22-MAR-2019| -- END

                'Shani(06-Feb-2016) -- End



                btnSubmitforApproval.Visible = False
                btnApproveSubmitted.Visible = False
                btnUnlockFinalSave.Visible = False
            Else

                'Shani(06-Feb-2016) -- Start
                'PA Changes Given By CCBRT
                'btnSubmitforApproval.Visible = True
                'btnApproveSubmitted.Visible = True
                'btnUnlockFinalSave.Visible = True

                'Shani(12-APR-2016) -- Start
                'btnSubmitforApproval.Visible = CBool(Session("AllowtoSubmitGoalsforApproval"))
                'btnApproveSubmitted.Visible = CBool(Session("AllowtoSubmitGoalsforApproval"))
                'btnUnlockFinalSave.Visible = CBool(Session("Allow_UnlockFinalSaveBSCPlanning"))
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    btnApproveSubmitted.Visible = Session("AllowtoApproveRejectGoalsPlanning")
                    btnUnlockFinalSave.Visible = Session("Allow_UnlockFinalSaveBSCPlanning")
                    'S.SANDEEP |25-MAR-2019| -- START
                    'btnSubmitforApproval.Visible = Session("AllowtoSubmitGoalsforApproval")
                    btnSubmitforApproval.Visible = False
                    'S.SANDEEP |25-MAR-2019| -- END
                ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    btnApproveSubmitted.Visible = False
                    btnUnlockFinalSave.Visible = False
                    btnSubmitforApproval.Visible = True
                End If
                'Shani(12-APR-2016) -- End 
                'Shani(06-Feb-2016) -- End

                btnCommitGoals.Visible = False
                btnOpenGoals.Visible = False
                'S.SANDEEP |22-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : ISSUE ON PA
                'btnViewTemplate.Visible = True
                'S.SANDEEP |22-MAR-2019| -- END
            End If
            'S.SANDEEP [23 DEC 2015] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}

            'S.SANDEEP |25-MAR-2019| -- START
            'strPerspectiveColName = Me.ViewState("strPerspectiveColName")
            'mintPerspectiveColIndex = Me.ViewState("mintPerspectiveColIndex")
            'strGoalValueColName = Me.ViewState("strGoalValueColName")
            'mintGoalValueColIndex = Me.ViewState("mintGoalValueColIndex")
            'mstrGroupName = Me.ViewState("mstrGroupName")
            'mblnIsGrpAdded = Me.ViewState("mblnIsGrpAdded")
            'S.SANDEEP |12-FEB-2019| -- END
            ''S.SANDEEP |18-FEB-2019| -- START
            ''ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'mintUoMColIndex = Me.ViewState("mintUoMColIndex")
            'mstrUoMColName = Me.ViewState("mstrUoMColName")
            ''S.SANDEEP |18-FEB-2019| -- END

            If Me.ViewState("strPerspectiveColName") IsNot Nothing Then
                strPerspectiveColName = Me.ViewState("strPerspectiveColName")
            End If
            If Me.ViewState("mintPerspectiveColIndex") IsNot Nothing Then
                mintPerspectiveColIndex = Me.ViewState("mintPerspectiveColIndex")
            End If
            If Me.ViewState("strGoalValueColName") IsNot Nothing Then
                strGoalValueColName = Me.ViewState("strGoalValueColName")
            End If
            If Me.ViewState("mintGoalValueColIndex") IsNot Nothing Then
                mintGoalValueColIndex = Me.ViewState("mintGoalValueColIndex")
            End If
            If Me.ViewState("mstrGroupName") IsNot Nothing Then
                mstrGroupName = Me.ViewState("mstrGroupName")
            End If
            If Me.ViewState("mblnIsGrpAdded") IsNot Nothing Then
                mblnIsGrpAdded = Me.ViewState("mblnIsGrpAdded")
            End If
            If Me.ViewState("mintUoMColIndex") IsNot Nothing Then
                mintUoMColIndex = Me.ViewState("mintUoMColIndex")
            End If
            If Me.ViewState("mstrUoMColName") IsNot Nothing Then
                mstrUoMColName = Me.ViewState("mstrUoMColName")
            End If
            'S.SANDEEP |25-MAR-2019| -- END

            'S.SANDEEP |04-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB
            If Me.ViewState("mstrReportComments") IsNot Nothing Then
                mstrReportComments = Me.ViewState("mstrReportComments")
            End If
            'S.SANDEEP |04-DEC-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
        'S.SANDEEP |11-APR-2019| -- START
        'If Request.QueryString.Count <= 0 Then
        '    Me.IsLoginRequired = True
        'End If
        Me.IsLoginRequired = True
        'S.SANDEEP |11-APR-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Session("mdtEmpGoal") Is Nothing Then
                Me.Session.Add("mdtEmpGoal", mdtEmpGoal)
            Else
                Me.Session("mdtEmpGoal") = mdtEmpGoal
            End If

            If Session("mdtEmpCmpt") Is Nothing Then
                Me.Session.Add("mdtEmpCmpt", mdtEmpCmpt)
            Else
                Me.Session("mdtEmpCmpt") = mdtEmpCmpt
            End If

            If Me.ViewState("mblnpopupShow_AproverReject") Is Nothing Then
                Me.ViewState.Add("mblnpopupShow_AproverReject", mblnpopupShow_AproverReject)
            Else
                Me.ViewState("mblnpopupShow_AproverReject") = mblnpopupShow_AproverReject
            End If

            'S.SANDEEP [23 DEC 2015] -- START
            If Me.ViewState("Headers") Is Nothing Then
                Me.ViewState.Add("Headers", dsHeaders)
            Else
                Me.ViewState("Headers") = dsHeaders
            End If

            If Me.ViewState("iHeaderId") Is Nothing Then
                Me.ViewState.Add("iHeaderId", iHeaderId)
            Else
                Me.ViewState("iHeaderId") = iHeaderId
            End If

            'Shani (01-Dec-2016) -- Start
            'Issue - CCBRT not showing Custom item 
            'If Session("IncludeCustomItemInPlanning") = False Then
            '    iMultiviewPageCount = 2
            'End If

            'If Me.ViewState("iMultiviewPageCount") Is Nothing Then
            '    Me.ViewState.Add("iMultiviewPageCount", iMultiviewPageCount)
            'End If
            Me.ViewState("iMultiviewPageCount") = iMultiviewPageCount
            'Shani (01-Dec-2016) -- End
            'S.SANDEEP [23 DEC 2015] -- END


            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            If Me.ViewState("strPerspectiveColName") IsNot Nothing Then
                If CStr(Me.ViewState("strPerspectiveColName")).Trim.Length <= 0 Then Me.ViewState("strPerspectiveColName") = strPerspectiveColName
            Else
                Me.ViewState("strPerspectiveColName") = strPerspectiveColName
            End If
            If Me.ViewState("mintPerspectiveColIndex") IsNot Nothing Then
                If CInt(Me.ViewState("mintPerspectiveColIndex")) <= 0 Then Me.ViewState("mintPerspectiveColIndex") = mintPerspectiveColIndex
            Else
                Me.ViewState("mintPerspectiveColIndex") = mintPerspectiveColIndex
            End If
            If Me.ViewState("strGoalValueColName") IsNot Nothing Then
                If CStr(Me.ViewState("strGoalValueColName")).Trim.Length <= 0 Then Me.ViewState("strGoalValueColName") = strGoalValueColName
            Else
                Me.ViewState("strGoalValueColName") = strGoalValueColName
            End If
            If Me.ViewState("mintGoalValueColIndex") IsNot Nothing Then
                If CInt(Me.ViewState("mintGoalValueColIndex")) < 0 Then Me.ViewState("mintGoalValueColIndex") = mintGoalValueColIndex
            Else
                Me.ViewState("mintGoalValueColIndex") = mintGoalValueColIndex
            End If
            If Me.ViewState("mstrGroupName") IsNot Nothing Then
                If Me.ViewState("mstrGroupName").ToString().Trim.Length <= 0 Then Me.ViewState("mstrGroupName") = mstrGroupName
            Else
                Me.ViewState("mstrGroupName") = mstrGroupName
            End If
            Me.ViewState("mblnIsGrpAdded") = mblnIsGrpAdded
            'S.SANDEEP |12-FEB-2019| -- END

            'S.SANDEEP |18-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            If Me.ViewState("mstrUoMColName") IsNot Nothing Then
                If Me.ViewState("mstrUoMColName").ToString().Trim.Length <= 0 Then Me.ViewState("mstrUoMColName") = mstrUoMColName
            Else
                Me.ViewState("mstrUoMColName") = mstrUoMColName
            End If
            If Me.ViewState("mintUoMColIndex") IsNot Nothing Then
                If CInt(Me.ViewState("mintUoMColIndex")) < 0 Then Me.ViewState("mintUoMColIndex") = mintUoMColIndex
            Else
                Me.ViewState("mintUoMColIndex") = mintUoMColIndex
            End If
            'S.SANDEEP |18-FEB-2019| -- END

            'S.SANDEEP |04-DEC-2019| -- START
            'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB
            If Me.ViewState("mstrReportComments") IsNot Nothing Then
                If Me.ViewState("mstrReportComments").ToString.Length < 0 Then Me.ViewState("mstrReportComments") = mstrReportComments
            Else
                Me.ViewState("mstrReportComments") = mstrReportComments
            End If
            'S.SANDEEP |04-DEC-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button Event(s) "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Sorry, Please select Period to view Performance Planning."), Me)  '--ADDED NEW LANGUAGE
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, Please select Employee and Period to view Performance Planning."), Me)
                End If
                Exit Sub
            End If
            Call Fill_Goals_Grid()
            Call Fill_Competancy_Grid()
            'S.SANDEEP [23 DEC 2015] -- START
            'Shani (01-Dec-2016) -- Start
            'Issue - CCBRT not showing Custom item 
            'Dim objCHeader As New clsassess_custom_header
            'dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
            'objCHeader = Nothing

            dsHeaders = (New clsassess_custom_header).getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
            Dim dttble As DataTable = dsHeaders.Tables(0).Clone
            If dsHeaders.Tables(0).Select("isinclude_planning = 1").Length > 0 Then
                dttble = dsHeaders.Tables(0).Select("isinclude_planning = 1").CopyToDataTable
            End If
            dsHeaders.Tables.RemoveAt(0)
            dsHeaders.Tables.Add(dttble)
            If dttble.Rows.Count <= 0 Then
                iMultiviewPageCount = 2
            Else
                iMultiviewPageCount = 3
            End If

            If mvPlanning.ActiveViewIndex = 0 Then
                btnPrevious.Enabled = False
                btnNext.Enabled = True
            ElseIf mvPlanning.ActiveViewIndex = iMultiviewPageCount - 1 Then
                btnPrevious.Enabled = True
                btnNext.Enabled = False
            Else
                btnPrevious.Enabled = True
                btnNext.Enabled = True
            End If
            'Shani (01-Dec-2016) -- End
            'S.SANDEEP [23 DEC 2015] -- END

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            mblnIsGrpAdded = False
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                cboEmployee.SelectedValue = 0
            End If
            cboPeriod.SelectedValue = 0
            dgvPData.DataSource = Nothing : dgvCData.DataSource = Nothing
            dgvPData.DataBind() : dgvCData.DataBind()
            objlblTotalWeight.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
        Try
            'S.SANDEEP [23 DEC 2015] -- START
            'mvPlanning.ActiveViewIndex -= 1
            'If mvPlanning.ActiveViewIndex <= mvPlanning.Views.Count - 1 Then
            '    btnNext.Enabled = True
            '    btnPrevious.Enabled = False
            'Else
            '    If mvPlanning.ActiveViewIndex <= 0 Then
            '        btnNext.Enabled = False
            '    End If
            'End If

            mvPlanning.ActiveViewIndex -= 1
            If mvPlanning.ActiveViewIndex <= iMultiviewPageCount - 1 Then
                btnNext.Enabled = True
                If mvPlanning.ActiveViewIndex = 0 Then
                    btnPrevious.Enabled = False
                End If
            Else
                If mvPlanning.ActiveViewIndex <= 0 Then
                    btnPrevious.Enabled = False
                End If
            End If
            'S.SANDEEP [23 DEC 2015] -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            'S.SANDEEP [23 DEC 2015] -- START

            'mvPlanning.ActiveViewIndex += 1
            'If mvPlanning.ActiveViewIndex >= mvPlanning.Views.Count - 1 Then
            '    btnPrevious.Enabled = True
            '    btnNext.Enabled = False
            'Else
            '    If mvPlanning.ActiveViewIndex = mvPlanning.Views.Count - 1 Then
            '        btnPrevious.Enabled = False
            '    End If
            'End If

            mvPlanning.ActiveViewIndex += 1
            If mvPlanning.ActiveViewIndex >= iMultiviewPageCount - 1 Then
                btnPrevious.Enabled = True
                btnNext.Enabled = False

                'Shani (01-Dec-2016) -- Start
                'Issue - CCBRT not showing Custom item 
                'If Session("IncludeCustomItemInPlanning") = True Then
                '    If mvPlanning.ActiveViewIndex = iMultiviewPageCount - 1 Then
                '        Call btnForward_Click(sender, e)
                '    End If
                'End If
                If dsHeaders.Tables(0).Rows.Count > 0 Then
                    If mvPlanning.ActiveViewIndex = iMultiviewPageCount - 1 Then
                        Call btnForward_Click(sender, e)
                    End If
                End If

            Else
                If mvPlanning.ActiveViewIndex = iMultiviewPageCount - 1 Then
                    btnPrevious.Enabled = False
                Else
                    btnPrevious.Enabled = True
                    'Shani (01-Dec-2016) -- End
                End If
            End If
            'S.SANDEEP [23 DEC 2015] -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSubmitforApproval_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitforApproval.Click
        Try
            mdtEmpGoal = Session("mdtEmpGoal")
            If dgvPData.Rows.Count > 0 Then
                Dim dtemp() As DataRow = mdtEmpGoal.Select("Emp <> ''")
                If dtemp.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Performance Planning is not planned for selected employee and period."), Me)
                    Exit Sub
                End If

                'S.SANDEEP [07 FEB 2015] -- START
                If CBool(Session("Self_Assign_Competencies")) = True Then
                    If dgvCData.Items.Count <= 0 Then
                        'S.SANDEEP [10 DEC 2015] -- START
                        'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, Performance Planning is not planned for selected employee and period for competencies."), Me)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Submission for Approval Failed, Reason: Competencies are not set for this employee for the selected period."), Me)
                        'S.SANDEEP [10 DEC 2015] -- END
                        Exit Sub
                    End If
                End If
                'S.SANDEEP [07 FEB 2015] -- END

                If CBool(mdtEmpGoal.Rows(0)("isfinal")) = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, Performance Planning is already approved for selected employee and period."), Me)
                    Exit Sub
                ElseIf mdtEmpGoal.Rows(0)("opstatusid") = enObjective_Status.SUBMIT_APPROVAL Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, Performance Planning is already sent for approval for selected employee and period."), Me)
                    Exit Sub
                End If

                Dim objAssessor As New clsAssessor_tran
                Dim sMsg As String = String.Empty
                sMsg = objAssessor.IsAssessorPresent(CInt(cboEmployee.SelectedValue))
                If sMsg <> "" Then
                    DisplayMessage.DisplayMessage(sMsg, Me)
                    Exit Sub
                End If
                objAssessor = Nothing

                Dim objFMapping As New clsAssess_Field_Mapping
                sMsg = objFMapping.Is_Weight_Set_For_Commiting(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
                If sMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(sMsg, Me)
                    objFMapping = Nothing
                    Exit Sub
                End If

                'S.SANDEEP [10 FEB 2015] -- START
                If CBool(Session("Self_Assign_Competencies")) = True Then
                    mdtEmpCmpt = Session("mdtEmpCmpt")
                    If mdtEmpCmpt IsNot Nothing AndAlso mdtEmpCmpt.Rows.Count > 0 Then
                        Dim xGrpWeight As Decimal = 0 : Dim xGrpId As Integer = -1
                        'Shani (19-Oct-2016) -- Start
                        'For Each xRow As DataRow In mdtEmpCmpt.Rows
                        For Each xRow As DataRow In mdtEmpCmpt.Select("isGrp = False")
                            'Shani (19-Oct-2016) -- End
                            If xGrpId <> xRow.Item("assessgroupunkid") Then
                                Dim objAGroup As New clsassess_group_master
                                objAGroup._Assessgroupunkid = xRow.Item("assessgroupunkid")
                                xGrpWeight = xGrpWeight + objAGroup._Weight
                                objAGroup = Nothing
                                xGrpId = xRow.Item("assessgroupunkid")
                            End If
                        Next

                        'SHANI [21 Mar 2015]-START
                        'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                        'If CInt(mdtEmpCmpt.Compute("SUM(assigned_weight)", "")) <> xGrpWeight Then
                        Dim dblwgt As Double = 0
                        dblwgt = mdtEmpCmpt.Compute("SUM(assigned_weight)", "")
                        dblwgt = Format(dblwgt, "###################0.#0")
                        If CDbl(dblwgt) <> xGrpWeight Then
                            'SHANI [21 Mar 2015]--END 
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot Submit this Performance Planning for Approval, Reason Competencies are not adding upto assessment group weight."), Me)
                            Exit Sub
                        End If
                    End If
                End If
                'S.SANDEEP [10 FEB 2015] -- END

                'S.SANDEEP [07-NOV-2018] -- START
                Dim objCMaster As New clsassess_custom_items
                Dim strMsg As String = String.Empty
                strMsg = objCMaster.CheckPlannedItem(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
                If strMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(strMsg, Me)
                    Exit Sub
                End If
                'S.SANDEEP [07-NOV-2018] -- END

                If objFMapping IsNot Nothing Then objFMapping = Nothing
                lblSubmitTitle.Text = "Aruti"
                lblSubmitMessage.Text = Language.getMessage(mstrModuleName, 9, "You are about to Submit this Performance Planning for Approval, if you follow this process then this employee won't be able to modify or delete this transaction." & vbCrLf & _
                                                                      "Do you wish to continue?")
                popup_SubmitApproval.Show()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSubmitYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmitYes.Click
        Try

            Dim objEStatusTran As New clsassess_empstatus_tran
            objEStatusTran._Assessoremployeeunkid = 0
            objEStatusTran._Assessormasterunkid = 0
            objEStatusTran._Commtents = ""
            objEStatusTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEStatusTran._Isunlock = False
            objEStatusTran._Loginemployeeunkid = 0
            objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
            objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            objEStatusTran._Statustypeid = enObjective_Status.SUBMIT_APPROVAL
            objEStatusTran._Userunkid = Session("UserId")

            If objEStatusTran.Insert() = False Then
                DisplayMessage.DisplayMessage(objEStatusTran._Message, Me)
                Exit Sub
            Else
                If CStr(Session("ArutiSelfServiceURL")).Trim.Length > 0 Then
                    If CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) > 0 Then
                        Dim objperiod As New clscommom_period_Tran

                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objperiod._Periodunkid = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
                        objperiod._Periodunkid(Session("Database_Name")) = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
                        'Shani(20-Nov-2015) -- End


                        'Shani(01-MAR-2016) -- Start
                        'Enhancement
                        'objEmpField1.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), _
                        '                                        CInt(cboPeriod.SelectedValue), _
                        '                                        objperiod._Yearunkid, _
                        '                                        Session("FinancialYear_Name"), Session("CompanyUnkId"), Session("ArutiSelfServiceURL"), _
                        '                                        enLogin_Mode.EMP_SELF_SERVICE, 0, 0)
                        objEmpField1.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), _
                                                                CInt(cboPeriod.SelectedValue), _
                                                                objperiod._Yearunkid, _
                                                                Session("FinancialYear_Name"), _
                                                                CStr(Session("Database_Name")), _
                                                                Session("CompanyUnkId"), _
                                                                Session("ArutiSelfServiceURL"), _
                                                                enLogin_Mode.EMP_SELF_SERVICE, 0, 0)
                        'Shani(01-MAR-2016) -- End

                        objperiod = Nothing
                    End If
                End If
                Call Fill_Goals_Grid() : Call Fill_Competancy_Grid()
            End If
            objEStatusTran = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnApproveSubmitted_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveSubmitted.Click
        Try
            mdtEmpGoal = Session("mdtEmpGoal")
            If dgvPData.Rows.Count > 0 Then
                Dim dtemp() As DataRow = mdtEmpGoal.Select("Emp <> ''")
                If dtemp.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Performance Planning is not planned for selected employee and period."), Me)
                    Exit Sub
                End If

                If CBool(mdtEmpGoal.Rows(0)("isfinal")) <> True AndAlso mdtEmpGoal.Rows(0)("opstatusid") <> enObjective_Status.SUBMIT_APPROVAL Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, Performance Planning is yet not submitted for selected employee and period."), Me)
                    Exit Sub
                End If

                Dim iFinal As Boolean = False
                If CBool(mdtEmpGoal.Rows(0)("isfinal")) = True Then
                    iFinal = True
                    'S.SANDEEP [09 FEB 2015] -- START
                    btnApRejFinalSave.Visible = False
                    'S.SANDEEP [09 FEB 2015] -- END
                ElseIf mdtEmpGoal.Rows(0)("opstatusid") = enObjective_Status.SUBMIT_APPROVAL Then
                    iFinal = False
                    'S.SANDEEP [09 FEB 2015] -- START
                    btnApRejFinalSave.Visible = True
                    'S.SANDEEP [09 FEB 2015] -- END
                End If
                If iFinal = True Then
                    Dim objAnalysis_Master As New clsevaluation_analysis_master
                    'S.SANDEEP |22-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
                    'If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                    If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                        'S.SANDEEP |22-JUL-2019| -- END
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
                        Exit Sub
                    End If

                    'S.SANDEEP |22-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
                    'If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                    If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                        'S.SANDEEP |22-JUL-2019| -- END
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
                        Exit Sub
                    End If

                    'S.SANDEEP |22-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
                    'If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                    If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                        'S.SANDEEP |22-JUL-2019| -- END
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
                        Exit Sub
                    End If
                    objAnalysis_Master = Nothing
                End If
                lblAppRejTitle.Text = "Aruti"
                lblAppRejMessage.Text = Language.getMessage(mstrModuleName, 12, "You are about to Approve/Disapprove this Performance Planning, with this process this employee can't be modified or deleted. It can only be modified or deleted if it is disapproved." & vbCrLf & _
                                                       "Do you wish to continue?")
                popup_ApproverRejYesNo.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAppRejYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAppRejYes.Click
        Try
            If dgvPData.Rows.Count > 0 Then
                mblnpopupShow_AproverReject = True
                txtComments.Text = ""
                popup_ApprovrReject.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnApRejClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApRejClose.Click
        Try
            mblnpopupShow_AproverReject = False
            popup_ApprovrReject.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnApRejFinalSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApRejFinalSave.Click, btnOpen_Changes.Click
        Dim btn As Button = CType(sender, Button)
        Dim iAssessorMasterId, iAssessorEmpId As Integer
        Dim sAssessorName As String = ""
        iAssessorMasterId = 0 : iAssessorEmpId = 0
        Try

            'S.SANDEEP |28-MAR-2019| -- START
            If txtComments.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRej, 5, "Sorry, Comments are mandatory information. Please provide comments in order to continue"), Me)
                Exit Sub
            End If
            'S.SANDEEP |28-MAR-2019| -- END

            iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
            If iAssessorMasterId <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName_AppRej, 1, "Sorry, You cannot perform this operation there is no Assessor mapped with your account."), Me)
                Exit Sub
            End If

            lblFinalTitle.Text = "Aruti"
            If btn.ID.ToUpper = "BTNAPREJFINALSAVE" Then
                lblFinalMessage.Text = Language.getMessage(mstrModuleName_AppRej, 2, "You are about to Approve this Performance Planning for the selected employee, if you follow this process then this employee won't be able to modify or delete this transaction.." & vbCrLf & "Do you wish to continue?")
                btnOpenYes.Visible = False
                btnFinalYes.Visible = True
            ElseIf btn.ID.ToUpper = "BTNOPEN_CHANGES" Then
                lblFinalMessage.Text = Language.getMessage(mstrModuleName_AppRej, 3, "You are about to Disapprove this Performance Planning for the selected employee, Due to this employee will be able to modify or delete this." & vbCrLf & "Do you wish to continue?")
                btnOpenYes.Visible = True
                btnFinalYes.Visible = False
            End If
            popup_ApprovrFinalYesNo.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnFinalYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalYes.Click
        mdtEmpGoal = Session("mdtEmpGoal")
        Dim iAssessorMasterId, iAssessorEmpId As Integer
        Dim sAssessorName As String = ""
        iAssessorMasterId = 0 : iAssessorEmpId = 0
        Try
            iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
            Dim objEmpField1 As New clsassess_empfield1_master
            Dim xRow() As DataRow = Nothing
            If mdtEmpGoal IsNot Nothing Then
                xRow = mdtEmpGoal.Select("OwnerIds <> ''")
            End If
            Dim blnFlag As Boolean = False
            If xRow.Length > 0 Then
                'S.SANDEEP [23 APR 2015] -- START
                'blnFlag = objEmpField1.Transfer_Goals_Subordinate_Employee(xRow)
                popupCnf.TargetControlID = btnFinalYes.ID
                popupCnf.Show()
                'S.SANDEEP [23 APR 2015] -- END
            Else
                'S.SANDEEP [23 APR 2015] -- START
                Call InsertStatus(iAssessorMasterId, iAssessorEmpId, sAssessorName)
                'S.SANDEEP [23 APR 2015] -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP [23 APR 2015] -- START
    Protected Sub btncnfYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncnfYes.Click
        Dim blnFlag As Boolean = False
        Try
            Dim xRow() As DataRow = Nothing
            If mdtEmpGoal IsNot Nothing Then
                xRow = mdtEmpGoal.Select("OwnerIds <> ''")
            End If
            If xRow.Length > 0 Then
                blnFlag = objEmpField1.Transfer_Goals_Subordinate_Employee(xRow, CInt(Session("CompanyUnkId")), enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), False)
            End If
            'S.SANDEEP |08-DEC-2020| -- START
            'ISSUE/ENHANCEMENT : NMB CASCADING CHANGES
            'Dim objGrp As New clsGroup_Master(True)
            'If objGrp._Groupname.ToString.ToUpper() = "NMB PLC" Then
            '    If xRow.Length <= 0 Then
            '        xRow = mdtEmpGoal.DefaultView.ToTable(True, "employeeunkid").Select("employeeunkid >0 ")

            '        'Pinkal (07-Dec-2020) -- Start
            '        'Enhancement NMB - Cascading PM UAT Comments.
            '        'blnFlag = objassess_Empfield1Mst.Transfer_Goals_Subordinate_Employee(xRow)
            '        blnFlag = objEmpField1.Transfer_Goals_Subordinate_Employee(xRow, CInt(Session("CompanyUnkId")), enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), True)
            '        'Pinkal (07-Dec-2020) -- End
            '    End If
            'End If
            'S.SANDEEP |08-DEC-2020| -- END
            Dim iAssessorMasterId, iAssessorEmpId As Integer
            Dim sAssessorName As String = ""
            iAssessorMasterId = 0 : iAssessorEmpId = 0
            iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
            Call InsertStatus(iAssessorMasterId, iAssessorEmpId, sAssessorName)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btncnfNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncnfNo.Click
        Try
            mblnpopupShow_AproverReject = False
            txtComments.Text = ""
            popup_ApproverRejYesNo.Hide()
            popup_ApprovrFinalYesNo.Hide()
            popup_ApprovrReject.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [23 APR 2015] -- END

    Protected Sub btnOpenYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOpenYes.Click
        mdtEmpGoal = Session("mdtEmpGoal")
        Dim iAssessorMasterId, iAssessorEmpId As Integer
        Dim sAssessorName As String = ""
        iAssessorMasterId = 0 : iAssessorEmpId = 0
        Try
            iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
            Dim objStatus As New clsassess_empstatus_tran
            objStatus._Assessoremployeeunkid = iAssessorMasterId
            objStatus._Assessormasterunkid = iAssessorEmpId
            objStatus._Commtents = txtComments.Text
            objStatus._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objStatus._Periodunkid = CInt(cboPeriod.SelectedValue)
            objStatus._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            objStatus._Statustypeid = enObjective_Status.OPEN_CHANGES
            objStatus._Isunlock = False
            objStatus._Userunkid = Session("Userid")

            'S.SANDEEP [29 JAN 2015] -- START
            'If objStatus.Insert() = False Then
            If objStatus.Insert(, Session("Self_Assign_Competencies")) = False Then
                'S.SANDEEP [29 JAN 2015] -- END
                DisplayMessage.DisplayMessage(objStatus._Message, Me)
                Exit Sub
            Else
                Dim objEmpField1 As New clsassess_empfield1_master
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, txtComments.Text, False, False)

                'S.SANDEEP [01-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001965}
                'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, txtComments.Text, False, False, CInt(Session("CompanyUnkId")), , , Session("Ntf_GoalsUnlockUserIds").ToString())
                objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, txtComments.Text, False, False, CInt(Session("CompanyUnkId")), , , , Session("Ntf_GoalsUnlockUserIds").ToString())
                'S.SANDEEP [01-Feb-2018] -- END

                'Sohail (30 Nov 2017) -- End
                objEmpField1 = Nothing
            End If
            objStatus = Nothing
            Call Fill_Goals_Grid() : Call Fill_Competancy_Grid()
            mblnpopupShow_AproverReject = False
            popup_ApprovrReject.Hide()
            If Request.QueryString.Count > 0 Then
                Response.Redirect("~/Index.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUnlockFinalSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnlockFinalSave.Click
        Try
            mdtEmpGoal = Session("mdtEmpGoal")
            If dgvPData.Rows.Count > 0 Then
                Dim dtemp() As DataRow = mdtEmpGoal.Select("Emp <> ''")
                If dtemp.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Performance Planning is not planned for selected employee and period."), Me)
                    Exit Sub
                End If

                If CBool(mdtEmpGoal.Rows(0)("isfinal")) <> True Then
                    'S.SANDEEP |12-MAR-2019| -- START
                    'ISSUE/ENHANCEMENT : {Action List Phase 2 - 61}
                    'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Sorry, Performance Planning is yet not final saved for selected employee and period."), Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Sorry, Performance Planning is yet not final saved for selected employee and period."), Me)
                    'S.SANDEEP |12-MAR-2019| -- END
                    Exit Sub
                End If

                Dim objAnalysis_Master As New clsevaluation_analysis_master
                'S.SANDEEP |22-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
                'If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                    'S.SANDEEP |22-JUL-2019| -- END
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
                    Exit Sub
                End If

                'S.SANDEEP |22-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
                'If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                    'S.SANDEEP |22-JUL-2019| -- END
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
                    Exit Sub
                End If

                'S.SANDEEP |22-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
                'If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                    'S.SANDEEP |22-JUL-2019| -- END
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot unlock Performance Planning for the selected period. Reason : Assessment is already done for the selected period"), Me)
                    Exit Sub
                End If
                objAnalysis_Master = Nothing


                'S.SANDEEP |27-NOV-2020| -- START
                'ISSUE/ENHANCEMENT : VALID GOAL OPERATION
                'Dim xMsg As String = ""
                'xMsg = (New clsassess_empfield1_master).IsGoalsAssignedSubEmployee(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
                'If xMsg.Trim.Length > 0 Then
                '    DisplayMessage.DisplayMessage(xMsg, Me)
                '    Exit Sub
                'End If
                'S.SANDEEP |27-NOV-2020| -- END


                Dim iMsgText As String = ""
                'iMsgText = Language.getMessage(mstrModuleName, 22, "You are about to unlock the employee goals. This will result in opening of all operation on employee goals.") & vbCrLf & _
                '           Language.getMessage(mstrModuleName, 23, "Do you wish to continue?")
                iMsgText = Language.getMessage(mstrModuleName, 15, "You are about to unlock the employee performance planning. This will result in opening of all operation on employee performance planning." & vbCrLf & _
                                                                   "Do you wish to continue?")

                lblUnlockTitel.Text = "Aruti"
                lblUnlockMessage.Text = iMsgText
                txtUnlockReason.Visible = False
                'S.SANDEEP |09-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : PA CHANGES
                txtUnlockReason.Text = ""
                'S.SANDEEP |09-JUL-2019| -- END
                popup_UnclokFinalSave.Show()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUnlockYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnlockYes.Click
        Try
            If txtUnlockReason.Visible = False Then
                txtUnlockReason.Visible = True
                lblUnlockMessage.Text = Language.getMessage(mstrModuleName, 16, "Enter reason to unlock")
                popup_UnclokFinalSave.Show()

            ElseIf txtUnlockReason.Visible = True Then

                'S.SANDEEP |09-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : PA CHANGES

                Dim blnVoidProgress As Boolean = False
                lblVoidUpdateProgress.Text = "Aruti"
                lblVoidUpdateProgressMsg.Text = Language.getMessage(mstrModuleName, 20, "You are about to unlock the employee performance planning. Do you want to void approved progress update(s)?")
                popup_VoidUpdateProgress.Show()
                Exit Sub

                ''S.SANDEEP [15-Feb-2018] -- START
                ''ISSUE/ENHANCEMENT : {#38}
                'Dim iAssessorMasterId, iAssessorEmpId As Integer
                'Dim sAssessorName As String = ""
                'iAssessorMasterId = 0 : iAssessorEmpId = 0
                'iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
                'Dim objEmpField1 As New clsassess_empfield1_master
                ''S.SANDEEP [15-Feb-2018] -- END

                'If txtUnlockReason.Text.Trim.Length > 0 Then
                '    Dim objEStatusTran As New clsassess_empstatus_tran
                '    objEStatusTran._Assessoremployeeunkid = 0
                '    objEStatusTran._Assessormasterunkid = 0
                '    objEStatusTran._Commtents = txtUnlockReason.Text
                '    objEStatusTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
                '    objEStatusTran._Isunlock = True
                '    objEStatusTran._Loginemployeeunkid = 0
                '    objEStatusTran._Periodunkid = CInt(cboPeriod.SelectedValue)
                '    objEStatusTran._Status_Date = ConfigParameter._Object._CurrentDateAndTime
                '    objEStatusTran._Statustypeid = enObjective_Status.OPEN_CHANGES
                '    objEStatusTran._Userunkid = CInt(Session("UserID"))
                '    'S.SANDEEP [10 FEB 2015] -- START
                '    'If objEStatusTran.Insert() = False Then
                '    If objEStatusTran.Insert(, Session("Self_Assign_Competencies")) = False Then
                '        'S.SANDEEP [10 FEB 2015] -- END
                '        DisplayMessage.DisplayMessage(objEStatusTran._Message, Me)
                '        Exit Sub
                '    Else
                '        'S.SANDEEP [15-Feb-2018] -- START
                '        'ISSUE/ENHANCEMENT : {#38}

                '        'S.SANDEEP [09-MAR-2018] -- START
                '        'ISSUE/ENHANCEMENT : {#Internal Issues}
                '        'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, "", True, False, CInt(Session("CompanyUnkId")), , , , Session("Ntf_GoalsUnlockUserIds").ToString())
                '        objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, txtUnlockReason.Text, False, True, CInt(Session("CompanyUnkId")), , , , Session("Ntf_GoalsUnlockUserIds").ToString())
                '        'S.SANDEEP [09-MAR-2018] -- END

                '        'S.SANDEEP [15-Feb-2018] -- END

                '        Call Fill_Goals_Grid() : Call Fill_Competancy_Grid()
                '    End If
                '    objEStatusTran = Nothing
                'End If

                'S.SANDEEP |09-JUL-2019| -- END

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Call Clear_Session()
            If Request.QueryString.Count > 0 Then
                Response.Redirect("~/Index.aspx", False)
            Else
                'Nilay (02-Mar-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
                Response.Redirect(Session("rootpath") & "UserHome.aspx", False)
                'Nilay (02-Mar-2015) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP [23 DEC 2015] -- START
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                iHeaderId = iHeaderId - 1
                If iHeaderId <= 0 Then
                    btnBack.Enabled = False
                    btnForward.Enabled = True
                End If
                If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
                    btnForward.Enabled = False
                Else
                    btnForward.Enabled = True
                End If
            End If
            If iHeaderId >= 0 Then
                lblCustomItemHeader.Text = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString
            End If
            If iHeaderId >= 0 Then
                Call Fill_Custom_Items_Grid(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnForward_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnForward.Click
        Try
            If iHeaderId < dsHeaders.Tables(0).Rows.Count - 1 Then
                iHeaderId += 1
                btnBack.Enabled = True
            End If
            If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
                btnForward.Enabled = False
            End If
            If iHeaderId >= 0 Then
                lblCustomItemHeader.Text = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString
            End If
            Call Fill_Custom_Items_Grid(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCommitGoals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCommitGoals.Click
        Try
            mdtEmpGoal = Session("mdtEmpGoal")
            If dgvPData.Rows.Count > 0 Then
                Dim dtemp() As DataRow = mdtEmpGoal.Select("Emp <> ''")
                If dtemp.Length <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Performance Planning is not planned for selected employee and period."), Me)
                    Exit Sub
                End If

                If CBool(Session("Self_Assign_Competencies")) = True Then
                    If dgvCData.Items.Count <= 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Submission for Approval Failed, Reason: Competencies are not set for this employee for the selected period."), Me)
                        Exit Sub
                    End If
                End If

                If CBool(mdtEmpGoal.Rows(0)("isfinal")) = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, Performance Planning is already approved for selected employee and period."), Me)
                    Exit Sub
                ElseIf mdtEmpGoal.Rows(0)("opstatusid") = enObjective_Status.SUBMIT_APPROVAL Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, Performance Planning is already sent for approval for selected employee and period."), Me)
                    Exit Sub
                End If

                Dim objAssessor As New clsAssessor_tran
                Dim sMsg As String = String.Empty
                sMsg = objAssessor.IsAssessorPresent(CInt(cboEmployee.SelectedValue))
                If sMsg <> "" Then
                    DisplayMessage.DisplayMessage(sMsg, Me)
                    Exit Sub
                End If
                objAssessor = Nothing

                Dim objFMapping As New clsAssess_Field_Mapping
                sMsg = objFMapping.Is_Weight_Set_For_Commiting(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
                If sMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(sMsg, Me)
                    objFMapping = Nothing
                    Exit Sub
                End If

                If CBool(Session("Self_Assign_Competencies")) = True Then
                    mdtEmpCmpt = Session("mdtEmpCmpt")
                    If mdtEmpCmpt IsNot Nothing AndAlso mdtEmpCmpt.Rows.Count > 0 Then
                        Dim xGrpWeight As Decimal = 0 : Dim xGrpId As Integer = -1
                        For Each xRow As DataRow In mdtEmpCmpt.Select("isGrp = false")   'Shani(29-Nov-2016) -- Start

                            If xGrpId <> xRow.Item("assessgroupunkid") Then
                                Dim objAGroup As New clsassess_group_master
                                objAGroup._Assessgroupunkid = xRow.Item("assessgroupunkid")
                                xGrpWeight = xGrpWeight + objAGroup._Weight
                                objAGroup = Nothing
                                xGrpId = xRow.Item("assessgroupunkid")
                            End If
                        Next

                        Dim dblwgt As Double = 0
                        dblwgt = mdtEmpCmpt.Compute("SUM(assigned_weight)", "")
                        dblwgt = Format(dblwgt, "###################0.#0")
                        If CDbl(dblwgt) <> xGrpWeight Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot Submit this Performance Planning for Approval, Reason Competencies are not adding upto assessment group weight."), Me)
                            Exit Sub
                        End If
                    End If
                End If
                If objFMapping IsNot Nothing Then objFMapping = Nothing

                cnfGoalsCommit.Title = "Aruti"
                cnfGoalsCommit.Message = "You are about to commit the goals for (" & cboEmployee.SelectedItem.Text & "), once committed these goals will be transferred to the employee and you won’t be able to Add/Edit/Delete them. Are you sure you want to Commit the Goals?"
                cnfGoalsCommit.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnOpenGoals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOpenGoals.Click
        Try
            mdtEmpGoal = Session("mdtEmpGoal")
            If dgvPData.Rows.Count > 0 Then
                If CBool(mdtEmpGoal.Rows(0)("isfinal")) <> True Then
                    'S.SANDEEP |12-MAR-2019| -- START
                    'ISSUE/ENHANCEMENT : {Action List Phase 2 - 61}
                    'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, Balance Score Card Planning is yet not final saved for selected employee and period."), Me)

                    'Pinkal (15-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, Balance Score Card Planning is yet not final approved for selected employee and period."), Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, Balance Score Card Planning is yet not approved for selected employee and period."), Me)
                    'Pinkal (15-Mar-2019) -- End

                    'S.SANDEEP |12-MAR-2019| -- END
                    Exit Sub
                End If

                Dim objFMapping As New clsAssess_Field_Mapping
                Dim strMsg As String = String.Empty
                strMsg = objFMapping.Is_Update_Progress_Started(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
                objFMapping = Nothing
                If strMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(strMsg, Me)
                    Exit Sub
                End If

                Dim objAnalysis_Master As New clsevaluation_analysis_master
                'S.SANDEEP |22-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
                'If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                If objAnalysis_Master.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                    'S.SANDEEP |22-JUL-2019| -- END
                    DisplayMessage.DisplayMessage("Failed to Open Goals because Performance Evaluation has started for these Goals.", Me)
                    Exit Sub
                End If

                'S.SANDEEP |22-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
                'If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                If objAnalysis_Master.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                    'S.SANDEEP |22-JUL-2019| -- END
                    DisplayMessage.DisplayMessage("Failed to Open Goals because Performance Evaluation has started for these Goals.", Me)
                    Exit Sub
                End If

                'S.SANDEEP |22-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : COMMITTED ASSESSMENT WAS CHECKED
                'If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
                If objAnalysis_Master.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                    'S.SANDEEP |22-JUL-2019| -- END
                    DisplayMessage.DisplayMessage("Failed to Open Goals because Performance Evaluation has started for these Goals.", Me)
                    Exit Sub
                End If
                objAnalysis_Master = Nothing

                cnfGoalsOpen.Title = "Aruti"
                cnfGoalsOpen.Message = " Are you sure you want to Open the goals for (" & cboEmployee.SelectedItem.Text & ")? Goals will be removed from the employee until you commit them."
                cnfGoalsOpen.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfGoalsCommit_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfGoalsCommit.buttonYes_Click
        Dim xDataTable As DataTable = Session("mdtEmpGoal")
        Dim iAssessorMasterId, iAssessorEmpId As Integer
        Dim sAssessorName As String = ""
        iAssessorMasterId = 0 : iAssessorEmpId = 0
        Try

            'S.SANDEEP [28 DEC 2015] -- START
            If CBool(xDataTable.Rows(0)("isfinal")) = True Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, Performance Planning is already approved for selected employee and period."), Me)
                Exit Sub
            ElseIf xDataTable.Rows(0)("opstatusid") = enObjective_Status.SUBMIT_APPROVAL Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, Performance Planning is already sent for approval for selected employee and period."), Me)
                Exit Sub
            End If
            'S.SANDEEP [28 DEC 2015] -- END

            iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
            Dim objEmpField1 As New clsassess_empfield1_master
            Dim xRow() As DataRow = Nothing
            If xDataTable IsNot Nothing Then
                xRow = xDataTable.Select("OwnerIds <> ''")
            End If
            Dim blnFlag As Boolean = False

            'S.SANDEEP [28 DEC 2015] -- START
            'If xRow.Length > 0 Then
            '    blnFlag = objEmpField1.Transfer_Goals_Subordinate_Employee(xRow)
            'End If

            If xRow IsNot Nothing AndAlso xRow.Length > 0 Then
                blnFlag = objEmpField1.Transfer_Goals_Subordinate_Employee(xRow, CInt(Session("CompanyUnkId")), enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), False)
            End If
            'S.SANDEEP [28 DEC 2015] -- END
            'S.SANDEEP |08-DEC-2020| -- START
            'ISSUE/ENHANCEMENT : NMB CASCADING CHANGES
            'Dim objGrp As New clsGroup_Master(True)
            'If objGrp._Groupname.ToString.ToUpper() = "NMB PLC" Then
            '    If xRow.Length <= 0 Then
            '        xRow = mdtEmpGoal.DefaultView.ToTable(True, "employeeunkid").Select("employeeunkid >0 ")

            '        'Pinkal (07-Dec-2020) -- Start
            '        'Enhancement NMB - Cascading PM UAT Comments.
            '        'blnFlag = objassess_Empfield1Mst.Transfer_Goals_Subordinate_Employee(xRow)
            '        blnFlag = objEmpField1.Transfer_Goals_Subordinate_Employee(xRow, CInt(Session("CompanyUnkId")), enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), True)
            '        'Pinkal (07-Dec-2020) -- End
            '    End If
            'End If
            'objGrp = Nothing
            'S.SANDEEP |08-DEC-2020| -- END

            Dim objStatus As New clsassess_empstatus_tran
            objStatus._Assessoremployeeunkid = iAssessorMasterId
            objStatus._Assessormasterunkid = iAssessorEmpId
            objStatus._Commtents = txtComments.Text
            objStatus._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objStatus._Periodunkid = CInt(cboPeriod.SelectedValue)
            objStatus._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            objStatus._Statustypeid = enObjective_Status.FINAL_SAVE
            objStatus._Isunlock = False
            objStatus._Userunkid = Session("Userid")
            If Session("SkipApprovalFlowInPlanning") = True Then
                objStatus._SkipApprovalFlow = True
            End If

            'S.SANDEEP [29 DEC 2015] -- START
            If objStatus.Insert(, Session("Self_Assign_Competencies")) = False Then
                'If objStatus.Insert() = False Then
                'S.SANDEEP [29 DEC 2015] -- END
                DisplayMessage.DisplayMessage(objStatus._Message, Me)
                Exit Sub
            Else
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, "", True, False)

                'S.SANDEEP [01-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001965}
                'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, "", True, False, CInt(Session("CompanyUnkId")), , , Session("Ntf_GoalsUnlockUserIds").ToString())
                objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, "", True, False, CInt(Session("CompanyUnkId")), , , , Session("Ntf_GoalsUnlockUserIds").ToString())
                'S.SANDEEP [01-Feb-2018] -- END

                'Sohail (30 Nov 2017) -- End
                objEmpField1 = Nothing
            End If
            objStatus = Nothing
            Call btnSearch_Click(sender, e)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfGoalsOpen_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfGoalsOpen.buttonYes_Click
        mdtEmpGoal = Session("mdtEmpGoal")
        Dim iAssessorMasterId, iAssessorEmpId As Integer
        Dim sAssessorName As String = ""
        iAssessorMasterId = 0 : iAssessorEmpId = 0
        Try
            iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)
            Dim objStatus As New clsassess_empstatus_tran
            objStatus._Assessoremployeeunkid = iAssessorMasterId
            objStatus._Assessormasterunkid = iAssessorEmpId
            objStatus._Commtents = txtComments.Text
            objStatus._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objStatus._Periodunkid = CInt(cboPeriod.SelectedValue)
            objStatus._Status_Date = ConfigParameter._Object._CurrentDateAndTime
            objStatus._Statustypeid = enObjective_Status.OPEN_CHANGES
            objStatus._Isunlock = False
            objStatus._Userunkid = Session("Userid")
            If Session("SkipApprovalFlowInPlanning") = True Then
                objStatus._SkipApprovalFlow = True
            End If

            If objStatus.Insert(, Session("Self_Assign_Competencies")) = False Then
                DisplayMessage.DisplayMessage(objStatus._Message, Me)
                Exit Sub
            Else
                Dim objEmpField1 As New clsassess_empfield1_master
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, txtComments.Text, False, False)

                'S.SANDEEP [01-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001965}
                'objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, txtComments.Text, False, False, CInt(Session("CompanyUnkId")), , , Session("Ntf_GoalsUnlockUserIds").ToString())
                objEmpField1.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), sAssessorName, txtComments.Text, False, False, CInt(Session("CompanyUnkId")), , , , Session("Ntf_GoalsUnlockUserIds").ToString())
                'S.SANDEEP [01-Feb-2018] -- END

                'Sohail (30 Nov 2017) -- End
                objEmpField1 = Nothing
            End If
            objStatus = Nothing
            Call btnSearch_Click(sender, e)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [23 DEC 2015] -- END

    'S.SANDEEP [29 DEC 2015] -- START
    Protected Sub btnViewTemplate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewTemplate.Click
        Try
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, Please select Employee and Period to view Performance Planning."), Me)
                Exit Sub
            End If

            'S.SANDEEP [31 DEC 2015] -- START
            'Dim ObjAssesss_Form = New ArutiReports.clsAssessment_Form

            'ObjAssesss_Form._PeriodId = cboPeriod.SelectedValue
            'ObjAssesss_Form._PeriodName = cboPeriod.SelectedItem.Text
            'ObjAssesss_Form._IsSelfAssignCompetencies = Session("Self_Assign_Competencies")
            'ObjAssesss_Form._EmployeeId = cboEmployee.SelectedValue
            'ObjAssesss_Form._EmployeeName = cboEmployee.SelectedItem.Text
            'ObjAssesss_Form._IsPreviewDisplay = False
            'ObjAssesss_Form._SelectedTemplateId = Session("AssessmentReportTemplateId")
            'ObjAssesss_Form._ScoringOptionId = Session("ScoringOptionId")
            'ObjAssesss_Form._EmployeeAsOnDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
            'ObjAssesss_Form._IncludeCustomItemInPlanning = Session("IncludeCustomItemInPlanning")

            'ObjAssesss_Form.generateReportNew(Session("Database_Name"), _
            '                                  Session("UserId"), _
            '                                  Session("Fin_year"), _
            '                                  Session("CompanyUnkId"), _
            '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                  Session("UserAccessModeSetting"), True, _
            '                                  Session("ExportReportPath"), _
            '                                  Session("OpenAfterExport"), 0, enPrintAction.None, _
            '                                  enExportAction.None, _
            '                                  Session("Base_CurrencyId"))

            'Session("objRpt") = ObjAssesss_Form._Rpt
            ''Response.Redirect("../../Aruti Report Structure/Report.aspx")

            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab1();", True)

            mdtEmpGoal = Session("mdtEmpGoal")
            If dgvPData.Rows.Count > 0 Then

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Call SetDateFormat()
                'Pinkal (16-Apr-2016) -- End


                'Pinkal (15-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                'If CBool(mdtEmpGoal.Rows(0)("isfinal")) <> True Then
                '    'S.SANDEEP |12-MAR-2019| -- START
                '    'ISSUE/ENHANCEMENT : {Action List Phase 2 - 61}
                '    'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, Balance Score Card Planning is yet not final saved for selected employee and period."), Me)
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, Balance Score Card Planning is yet not final approved for selected employee and period."), Me)
                '    'S.SANDEEP |12-MAR-2019| -- END
                '    Exit Sub
                'End If

                'Pinkal (15-Mar-2019) -- End

                Dim ObjAssesss_Form = New ArutiReports.clsAssessment_Form(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

                ObjAssesss_Form._PeriodId = cboPeriod.SelectedValue
                ObjAssesss_Form._PeriodName = cboPeriod.SelectedItem.Text
                ObjAssesss_Form._IsSelfAssignCompetencies = Session("Self_Assign_Competencies")
                ObjAssesss_Form._EmployeeId = cboEmployee.SelectedValue
                ObjAssesss_Form._EmployeeName = cboEmployee.SelectedItem.Text
                ObjAssesss_Form._IsPreviewDisplay = False
                ObjAssesss_Form._SelectedTemplateId = Session("AssessmentReportTemplateId")
                ObjAssesss_Form._ScoringOptionId = Session("ScoringOptionId")
                ObjAssesss_Form._EmployeeAsOnDate = eZeeDate.convertDate(Session("EmployeeAsOnDate"))
                'ObjAssesss_Form._IncludeCustomItemInPlanning = Session("IncludeCustomItemInPlanning")


                'S.SANDEEP |04-DEC-2019| -- START
                'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB
                'ObjAssesss_Form.generateReportNew(Session("Database_Name"), _
                '                                  Session("UserId"), _
                '                                  Session("Fin_year"), _
                '                                  Session("CompanyUnkId"), _
                '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                  Session("UserAccessModeSetting"), True, _
                '                                  Session("ExportReportPath"), _
                '                                  Session("OpenAfterExport"), 0, enPrintAction.None, _
                '                                  enExportAction.None, _
                '                                  Session("Base_CurrencyId"))
                'Session("objRpt") = ObjAssesss_Form._Rpt
                'If ObjAssesss_Form._Rpt IsNot Nothing Then
                '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab1();", True)
                'End If
                If Session("AssessmentReportTemplateId") = enAssessmentReportTemplate.ART_TEMPLATE_5 Then
                    Dim strBuilder As System.Text.StringBuilder
                    Dim mDicPeriods As New Dictionary(Of Integer, String)
                    mDicPeriods.Add(CInt(cboPeriod.SelectedValue), cboPeriod.SelectedItem.Text)
                    If mDicPeriods.Keys.Count > 0 Then
                        strBuilder = ObjAssesss_Form.Generate_MultiPeriod_DetailReport(cboEmployee.SelectedValue, mDicPeriods, Session("FinancialYear_Name").ToString(), Session("ConsiderItemWeightAsNumber"), Session("ConsiderItemWeightAsNumber"), False, Session("Database_Name").ToString, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
                        divhtml.InnerHtml = strBuilder.ToString
                        popupEmpAssessForm.Show()
                    End If
                Else
                    ObjAssesss_Form.generateReportNew(Session("Database_Name"), _
                                                      Session("UserId"), _
                                                      Session("Fin_year"), _
                                                      Session("CompanyUnkId"), _
                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                      Session("UserAccessModeSetting"), True, _
                                                      Session("ExportReportPath"), _
                                                      Session("OpenAfterExport"), 0, enPrintAction.None, _
                                                      enExportAction.None, _
                                                      Session("Base_CurrencyId"))
                    Session("objRpt") = ObjAssesss_Form._Rpt
                    If ObjAssesss_Form._Rpt IsNot Nothing Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab1();", True)
                    End If
                End If

                'S.SANDEEP |04-DEC-2019| -- END            
            End If
            'S.SANDEEP [31 DEC 2015] -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [29 DEC 2015] -- END

    'S.SANDEEP |09-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : PA CHANGES
    Protected Sub btnVUPYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVUPYes.Click
        Try
            Call UnlockFinalSavedGoals(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnVUPNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnVUPNo.Click
        Try
            Call UnlockFinalSavedGoals(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |09-JUL-2019| -- END

    'S.SANDEEP |04-DEC-2019| -- START
    'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB
    Protected Sub btnViewPlanningReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewPlanningReport.Click
        Try
            If dgvPData.Rows.Count > 0 Then
                Dim strBuilder As New StringBuilder

                Dim objEmp As New clsEmployee_Master
                Dim objPrd As New clscommom_period_Tran
                Dim objDep As New clsDepartment
                Dim objJob As New clsJobs
                Dim objCls As New clsClass

                objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate")), Nothing) = CInt(cboEmployee.SelectedValue)
                objDep._Departmentunkid = objEmp._Departmentunkid
                objCls._Classesunkid = objEmp._Classunkid
                objJob._Jobunkid = objEmp._Jobunkid

                mstrReportComments = mdtEmpGoal.AsEnumerable().Where(Function(x) x.Field(Of String)("xcomments") <> "").Select(Function(x) x.Field(Of String)("xcomments")).DefaultIfEmpty().First
                If mstrReportComments Is Nothing Then mstrReportComments = ""

                Dim objEvalMaster As New clsevaluation_analysis_master
                Dim dsList As DataSet = objEvalMaster.GetEmployee_AssessorReviewerDetails(CInt(cboPeriod.SelectedValue), Session("Database_Name").ToString(), Nothing, CInt(cboEmployee.SelectedValue))
                objEvalMaster = Nothing
                Dim iEAssessorName As String = String.Empty
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim dtmp() As DataRow = Nothing
                    dtmp = dsList.Tables(0).Select("isreviewer = 0 AND isfound = 1")
                    If dtmp.Length > 0 Then
                        iEAssessorName = dtmp(0).Item("arName")
                    Else
                        dtmp = dsList.Tables(0).Select("isreviewer = 0 AND isfound = 0 AND visibletypeid = 1")
                        If dtmp.Length > 0 Then
                            iEAssessorName = dtmp(0).Item("arName")
                        End If
                    End If
                End If

                strBuilder.Append("<HTML>" & vbCrLf)
                strBuilder.Append("<HEAD>" & vbCrLf)
                strBuilder.Append("<STYLE TYPE='text/css'>" & vbCrLf)
                strBuilder.Append("u.dotted" & vbCrLf)
                strBuilder.Append("{" & vbCrLf)
                strBuilder.Append("border-bottom: 1px dotted #000;" & vbCrLf)
                strBuilder.Append("text-decoration: none;" & vbCrLf)
                strBuilder.Append("}" & vbCrLf)
                strBuilder.Append("</STYLE>" & vbCrLf)
                strBuilder.Append("</HEAD>" & vbCrLf)
                strBuilder.Append("<BODY style='width: 100%'>" & vbCrLf)
                strBuilder.Append("<TABLE width = '700px' align='center'>" & vbCrLf)
                strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("    <td style='width: 100%'>" & vbCrLf)
                strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                strBuilder.Append("            <tr style='text-align: Center; width: 100%'>" & vbCrLf)
                strBuilder.Append("                <td style='text-align: Center; width: 100%'>" & vbCrLf)
                Dim objCompany As New clsCompany_Master
                objCompany._Companyunkid = CInt(Session("CompanyUnkId"))
                Dim strPath As String = String.Empty
                Dim base64String As String = ""
                If objCompany._Image IsNot Nothing Then
                    strPath = My.Computer.FileSystem.SpecialDirectories.Temp & "\clogo.png"
                    objCompany._Image.Save(strPath)
                    Using image As Image = image.FromFile(strPath)
                        Using m As New MemoryStream()
                            image.Save(m, image.RawFormat)
                            Dim imageBytes() As Byte = m.ToArray()
                            base64String = Convert.ToBase64String(imageBytes)
                        End Using
                    End Using
                    strBuilder.Append(" <img src = ""data:image/png;base64, " & base64String & """ HEIGHT = '100px' WIDTH = '150px' />" & vbCrLf)
                Else
                    strBuilder.Append(" <img src = '' HEIGHT = '100px' WIDTH = '150px' />" & vbCrLf)
                End If
                strBuilder.Append("                </td>" & vbCrLf)
                strBuilder.Append("            </tr>" & vbCrLf)
                strBuilder.Append("        </table>" & vbCrLf)
                strBuilder.Append("    </td>" & vbCrLf)
                strBuilder.Append("</tr>" & vbCrLf)
                strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("    <td style='text-align: Center; width: 100%'><B><FONT SIZE=3 FONT COLOR = 'Blue'>" & vbCrLf)
                strBuilder.Append("                " & objCompany._Name & vbCrLf)
                strBuilder.Append("    </td>" & vbCrLf)
                strBuilder.Append("</tr>" & vbCrLf)
                strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("    <td style='width: 100%'>" & vbCrLf)
                strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                strBuilder.Append("            <tr style='text-align: Center; width: 100%'>" & vbCrLf)
                strBuilder.Append("                <td style='text-align: Center; width: 100%'><FONT SIZE=2>" & vbCrLf)
                strBuilder.Append("                    <B>" & Language.getMessage(mstrModuleName, 3000, "BALANCED SCORE CARD PLANNING") & "</B></FONT>" & vbCrLf)
                strBuilder.Append("                </td>" & vbCrLf)
                strBuilder.Append("            </tr>" & vbCrLf)
                strBuilder.Append("        </table>" & vbCrLf)
                strBuilder.Append("    </td>" & vbCrLf)
                strBuilder.Append("</tr>" & vbCrLf)

                strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("    <td style='width: 100%'>" & vbCrLf)
                strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage("clsAssessmentListReport", 74, "PERIOD UNDER REVIEW") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage("clsAssessmentListReport", 75, "FROM") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td><u class='dotted'><b>" & objPrd._Start_Date.Date & "</b></u></td>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage("clsAssessmentListReport", 76, "TO") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td><u class='dotted'><b>" & objPrd._End_Date.Date & "</b></u></td>" & vbCrLf)
                strBuilder.Append("            </tr>" & vbCrLf)
                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage("clsAssessmentListReport", 77, "NAME OF STAFF") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td><u class='dotted'><b>" & objEmp._Firstname & " " & objEmp._Surname & "</b></u></td>" & vbCrLf)
                strBuilder.Append("            </tr>" & vbCrLf)
                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage("clsAssessmentListReport", 78, "POSITION TITLE") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td><u class='dotted'><b>" & objJob._Job_Name & "</b></u></td>" & vbCrLf)
                strBuilder.Append("            </tr>" & vbCrLf)
                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage("clsAssessmentListReport", 79, "DEPARTMENT/BRANCH") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td><u class='dotted'><b>" & objDep._Name & "/" & objCls._Name & "</b></u></td>" & vbCrLf)
                strBuilder.Append("            </tr>" & vbCrLf)
                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage("clsAssessmentListReport", 80, "DATE OF APPOINTMENT") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td><u class='dotted'><b>" & objEmp._Appointeddate.Date & "</b></u></td>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage("clsAssessmentListReport", 81, "NAME OF SUPERVISOR") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td><u class='dotted'><b>" & iEAssessorName & "</b></u></td>" & vbCrLf)
                strBuilder.Append("            </tr>" & vbCrLf)
                strBuilder.Append("        </table>" & vbCrLf)
                strBuilder.Append("    </td>" & vbCrLf)
                strBuilder.Append("</tr>" & vbCrLf)

                strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("    <td style='width: 100%'>" & vbCrLf)
                strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                For Each iCol As DataControlField In dgvPData.Columns
                    If iCol.Visible = True Then
                        strBuilder.Append("         <TD bgcolor='#2e6da4'>" & vbCrLf)
                        strBuilder.Append("             <b><Font Color = 'White'>" & iCol.HeaderText & "</Font></b>" & vbCrLf)
                        strBuilder.Append("         </TD>" & vbCrLf)
                    End If
                Next
                strBuilder.Append("            </tr>" & vbCrLf)
                Dim mDicPerspective As New Dictionary(Of String, Integer)
                mDicPerspective = dgvPData.Rows.Cast(Of GridViewRow).AsEnumerable().GroupBy(Function(x) x.Cells(mintPerspectiveColIndex).Text) _
                                 .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Cells(mintPerspectiveColIndex).Text) _
                                 .Count()}).ToDictionary(Function(x) x.barid, Function(y) y.barCount)

                Dim blnIsSamePerspective As Boolean = False
                Dim strOldPerspective As String = String.Empty
                For Each iRow As GridViewRow In dgvPData.Rows
                    strBuilder.Append("<TR style='width: 100%'>" & vbCrLf)
                    If strOldPerspective <> dgvPData.Rows(iRow.RowIndex).Cells(getColumnID_Griview(dgvPData, dgvPData.Columns(mintPerspectiveColIndex).FooterText, False, True)).Text Then
                        strOldPerspective = dgvPData.Rows(iRow.RowIndex).Cells(getColumnID_Griview(dgvPData, dgvPData.Columns(mintPerspectiveColIndex).FooterText, False, True)).Text
                        blnIsSamePerspective = True
                    End If
                    For Each iCol As DataControlField In dgvPData.Columns
                        If iCol.Visible = True Then
                            If blnIsSamePerspective Then
                                strBuilder.Append("<TD rowspan = '" & mDicPerspective(strOldPerspective) & "'>" & vbCrLf)
                                strBuilder.Append("<b>" & dgvPData.Rows(iRow.RowIndex).Cells(getColumnID_Griview(dgvPData, iCol.FooterText, False, True)).Text & "</b>" & vbCrLf)
                                strBuilder.Append("</TD>" & vbCrLf)
                                blnIsSamePerspective = False
                            Else
                                If getColumnID_Griview(dgvPData, iCol.FooterText, False, True) <> mintPerspectiveColIndex Then
                                    strBuilder.Append("<TD>" & vbCrLf)
                                    strBuilder.Append(dgvPData.Rows(iRow.RowIndex).Cells(getColumnID_Griview(dgvPData, iCol.FooterText, False, True)).Text & vbCrLf)
                                    strBuilder.Append("</TD>" & vbCrLf)
                                End If
                            End If
                        End If
                    Next
                    strBuilder.Append("         </TR>" & vbCrLf)
                Next
                strBuilder.Append("        </table>" & vbCrLf)
                strBuilder.Append("    </td>" & vbCrLf)
                strBuilder.Append("</tr>" & vbCrLf)

                strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("</tr>" & vbCrLf)
                strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("    <td style='width: 100%'>" & vbCrLf)
                strBuilder.Append("        <table border='1' style='width: 100%;' bordercolor='#000000'>" & vbCrLf)
                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("                <td>" & vbCrLf)
                strBuilder.Append("                     <b>" & Language.getMessage(mstrModuleName, 3003, "Supervisor remarks:") & "</b><br>" & vbCrLf)
                strBuilder.Append(mstrReportComments & vbCrLf)
                strBuilder.Append("                     " & vbCrLf)
                strBuilder.Append("                </td>" & vbCrLf)
                strBuilder.Append("            </tr>" & vbCrLf)
                strBuilder.Append("        </table>" & vbCrLf)
                strBuilder.Append("    </td>" & vbCrLf)
                strBuilder.Append("</tr>" & vbCrLf)
                strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("    <td style='width: 100%'>" & vbCrLf)
                strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage(mstrModuleName, 3001, "Submitted by") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td></td>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage(mstrModuleName, 3002, "Approved by") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td></td>" & vbCrLf)
                strBuilder.Append("            </tr>" & vbCrLf)
                strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage("clsAssessmentListReport", 93, "Date") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td></td>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage("clsAssessmentListReport", 92, "Signature") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td></td>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage("clsAssessmentListReport", 93, "Date") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td></td>" & vbCrLf)
                strBuilder.Append("                <td><b>" & Language.getMessage("clsAssessmentListReport", 92, "Signature") & "</b></td>" & vbCrLf)
                strBuilder.Append("                <td></td>" & vbCrLf)
                strBuilder.Append("            </tr>" & vbCrLf)
                strBuilder.Append("        </table>" & vbCrLf)
                strBuilder.Append("    </td>" & vbCrLf)
                strBuilder.Append("</tr>" & vbCrLf)
                strBuilder.Append("</TABLE>" & vbCrLf)
                strBuilder.Append("</BODY>" & vbCrLf)
                strBuilder.Append("</HEAD>" & vbCrLf)
                strBuilder.Append("</HTML>")
                divhtml.InnerHtml = strBuilder.ToString
                popupEmpAssessForm.Show()
                objEmp = Nothing : objPrd = Nothing : objDep = Nothing : objCls = Nothing : objJob = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |04-DEC-2019| -- END

#End Region

#Region "Control's Event (S)"

    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Protected Sub dgvPData_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvPData.PreRender
        Try
            MergeRows(dgvPData)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'S.SANDEEP |12-FEB-2019| -- END

    'mdtEmpGoal
    Protected Sub dgvPData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvPData.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Call SetDateFormat()
                Dim intindex As Integer = 0

                'S.SANDEEP |14-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                'intindex = mdtEmpGoal.Columns("St_date").ExtendedProperties("index")
                'If e.Row.Cells(intindex).Text.ToString().Trim <> "" AndAlso e.Row.Cells(intindex).Text.Trim <> "&nbsp;" Then
                '    e.Row.Cells(intindex).Text = CDate(e.Row.Cells(intindex).Text).Date.ToShortDateString
                'End If

                'intindex = mdtEmpGoal.Columns("Ed_Date").ExtendedProperties("index")
                'If e.Row.Cells(intindex).Text.ToString().Trim <> "" AndAlso e.Row.Cells(intindex).Text.Trim <> "&nbsp;" Then
                '    e.Row.Cells(intindex).Text = CDate(e.Row.Cells(intindex).Text).Date.ToShortDateString
                'End If
                'S.SANDEEP |14-MAR-2020| -- END


                'S.SANDEEP |18-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                If mintGoalValueColIndex < 0 Then mintGoalValueColIndex = Me.ViewState("mintGoalValueColIndex")
                If mintGoalValueColIndex <> -1 Then
                    If e.Row.Cells(mintGoalValueColIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(mintGoalValueColIndex).Text.Trim <> "&nbsp;" Then
                        If mintUoMColIndex < 0 Then
                            e.Row.Cells(mintGoalValueColIndex).Text = Format(CDbl(e.Row.Cells(mintGoalValueColIndex).Text), Session("fmtCurrency"))
                        Else
                            Dim iUoM As String = ""
                            If mdtEmpGoal.Rows.Count > 0 Then
                                iUoM = mdtEmpGoal.Rows(e.Row.RowIndex)(mstrUoMColName).ToString()
                            End If
                            e.Row.Cells(mintGoalValueColIndex).Text = Format(CDbl(e.Row.Cells(mintGoalValueColIndex).Text), Session("fmtCurrency")) & " " & iUoM
                        End If
                    End If
                End If
                'S.SANDEEP |18-FEB-2019| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvCData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvCData.ItemDataBound
        Try
            If e.Item.Cells.Count > 0 Then
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    If CBool(e.Item.Cells(7).Text) = True Then
                        Dim CellCout As Integer = 0
                        For i = 0 To dgvCData.Columns.Count - 1
                            If dgvCData.Columns(i).Visible Then
                                e.Item.Cells(i).Visible = False
                                CellCout += 1
                            End If
                        Next
                        e.Item.Cells(1).Visible = True
                        e.Item.Cells(1).ColumnSpan = CellCout - 1
                        e.Item.Cells(1).CssClass = "GroupHeaderStylecomp"
                        e.Item.Cells(8).Visible = True
                        e.Item.Cells(8).CssClass = "GroupHeaderStylecomp"
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub link_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            txtData.Text = ""
            Dim lnkWhatIsThis As LinkButton = CType(sender, LinkButton)
            Dim xRow As DataGridItem = CType(lnkWhatIsThis.NamingContainer, DataGridItem)
            If CBool(xRow.Cells(7).Text) = True Then
                Dim objCOMaster As New clsCommon_Master
                objCOMaster._Masterunkid = CInt(xRow.Cells(5).Text)
                If objCOMaster._Description <> "" Then
                    txtData.Text = objCOMaster._Description
                    popup_ComInfo.Show()
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
                End If
                objCOMaster = Nothing
            ElseIf CBool(xRow.Cells(7).Text) = False Then
                Dim objCPMsater As New clsassess_competencies_master
                objCPMsater._Competenciesunkid = CInt(xRow.Cells(6).Text)
                If objCPMsater._Description <> "" Then
                    txtData.Text = objCPMsater._Description
                    popup_ComInfo.Show()
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
                End If
                objCPMsater = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
#End Region


    'S.SANDEEP |12-FEB-2019| -- START
    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                Call btnSearch_Click(New Object(), New EventArgs())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |12-FEB-2019| -- END

End Class

'Nilay (01-Feb-2015) -- Start
'Enhancement - REDESIGN SELF SERVICE.
'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
'    Call Clear_Session()
'    If Request.QueryString.Count > 0 Then
'        Response.Redirect("~/Index.aspx")
'    Else
'        Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
'    End If
'End Sub
'Nilay (01-Feb-2015) -- End