<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Index.aspx.vb" Inherits="Index"
    Title="Aruti Login Page" Theme="blue" StylesheetTheme="blue" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="YesNobutton" TagPrefix="uc2" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Aruti Login Page</title>
    <link href="App_Themes/blue/blue.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="font-awesome-4.3.0/css/font-awesome.min.css" />
    <link href="Help/alert/sweetalert.css" rel="stylesheet" type="text/css" />
    <style type="text/css"> 
        /* HTML5 display-role reset for older browsers article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section
        {
            display: block;
        }
        body
        {
            font-family: sans-serif;
            background-color: #004474;
        }
        */body
        {
            /*background-image: -webkit-gradient(radial, 0% 0%, 0% 100%, from(rgba(213,246,255,1)), to(rgba(213,246,255,0)));
            background-image: -webkit-radial-gradient(50% 50%, 40% 40%, rgba(213,246,255,1), rgba(213,246,255,0));
            background-image: -moz-radial-gradient(50% 50%, 50% 50%, rgba(213,246,255,1), rgba(213,246,255,0));
            background-image: -ms-radial-gradient(50% 50%, 50% 50%, rgba(213,246,255,1), rgba(213,246,255,0));
            background-image: -o-radial-gradient(50% 50%, 50% 50%, rgba(213,246,255,1), rgba(213,246,255,0));*/
            background-color: #004474; /*background: url( 'images/background1.JPG' ) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;*/
        }
        #main
        {
            width: 450px;
            position: absolute;
            top: 35%;
            left: 35%;
            background: #fff;
            padding-top: 15px;
            padding-left: 15px;
            padding-right: 30px;
            padding-bottom: 15px;
            border-radius: 13px;
        }
        #main label
        {
            display: none;
        }
        .placeholder
        {
            color: #444;
        }
        #avtar
        {
            border: 1px solid rgba(0, 0, 0, .49);
            border-radius: 6px;
        }
        #main input[type="text"], #main input[type="password"]
        {
            width: 95%;
            height: 35px;
            positon: relative;
            margin-top: 7px;
            font-size: 14px;
            color: #444;
            outline: none;
            border: 1px solid rgba(0, 0, 0, .49);
            padding-left: 20px;
            -webkit-background-clip: padding-box;
            -moz-background-clip: padding-box;
            background-clip: padding-box;
            border-radius: 6px;
            box-shadow: inset 0px 2px 0px #d9d9d9;
            padding-top: 5px;
        }
        #main input[type="text"]:focus, #main input[type="password"]:focus
        {
            -webkit-box-shadow: inset 0px 2px 0px #a7a7a7;
            box-shadow: inset 0px 2px 0px #a7a7a7;
        }
        #main input:first-child
        {
            margin-top: 0px;
        }
        #main span
        {
            font-size: 14px;
        }
        /* #main input[type="submit"]
        {
            min-width: 100px;
            height: 50px;
            margin-top: 7px;
            color: #fff;
            font-size: 18px;
            font-weight: bold;
            text-shadow: 0px-1px0px#5b6ddc;
            outline: none;
            border: 1pxsolidrgba(0, 0, 0, .49);
            -webkit-background-clip: padding-box;
            -moz-background-clip: padding-box;
            background-clip: padding-box;
            border-radius: 6px;
            background-color: #5466da;
            background-image: -webkit-linear-gradient(bottom, #5466da 0%, #768ee4 100%);
            background-image: -moz-linear-gradient(bottom, #5466da 0%, #768ee4 100%);
            background-image: -o-linear-gradient(bottom, #5466da 0%, #768ee4 100%);
            background-image: -ms-linear-gradient(bottom, #5466da 0%, #768ee4 100%);
            background-image: linear-gradient(bottom, #5466da 0%, #768ee4 100%);
            -webkit-box-shadow: inset0px1px0px#9ab1ec;
            box-shadow: inset0px1px0px#9ab1ec;
            cursor: pointer;
            -webkit-transition: all.1sease-in-out;
            -moz-transition: all.1sease-in-out;
            -o-transition: all.1sease-in-out;
            -ms-transition: all.1sease-in-out;
            transition: all.1sease-in-out;
        }
        #main input[type="submit"]:hover
        {
            background-color: #5f73e9;
            background-image: -webkit-linear-gradient(bottom, #5f73e9 0%, #859bef 100%);
            background-image: -moz-linear-gradient(bottom, #5f73e9 0%, #859bef 100%);
            background-image: -o-linear-gradient(bottom, #5f73e9 0%, #859bef 100%);
            background-image: -ms-linear-gradient(bottom, #5f73e9 0%, #859bef 100%);
            background-image: linear-gradient(bottom, #5f73e9 0%, #859bef 100%);
            -webkit-box-shadow: inset 0px 1px 0px #aab9f4;
            box-shadow: inset 0px 1px 0px #aab9f4;
            margin-top: 10px;
        }
        #main input[type="submit"]:active
        {
            background-color: #7588e1;
            background-image: -webkit-linear-gradient(bottom, #7588e1 0%, #7184df 100%);
            background-image: -moz-linear-gradient(bottom, #7588e1 0%, #7184df 100%);
            background-image: -o-linear-gradient(bottom, #7588e1 0%, #7184df 100%);
            background-image: -ms-linear-gradient(bottom, #7588e1 0%, #7184df 100%);
            background-image: linear-gradient(bottom, #7588e1 0%, #7184df 100%);
            -webkit-box-shadow: inset 0px 1px 0px #93a9e9;
            box-shadow: inset 0px 1px 0px #93a9e9;
        }*/a
        {
            font-size: 12px;
            text-decoration: none;
            color: #004474;
            font-weight: bold;
        }
        a:hover
        {
            color: #FEBF01;
        }
        .watermarkcss
        {
            color: #c0c0c0;
        }
        .oldPassword
        {
            background-image: url(images/old_pass.png);
            background-repeat: no-repeat;
            background-position: 15px 10px;
            color: #fff;
            text-align: right;
        }
        .NewPassword
        {
            background-image: url(images/New_pass.png);
            background-repeat: no-repeat;
            background-position: 18px 13px;
            color: #fff;
            text-align: right;
        }
        .ConfirmPassword
        {
            background-image: url(images/Confirm_pass.png);
            background-repeat: no-repeat;
            background-position: 18px 10px;
            color: #fff;
            text-align: right;
        }
        .panel-body-default input[type="text"], .panel-body-default input[type="password"]
        {
            width: 96%;
            height: 35px;
            positon: relative;
            margin-top: 7px;
            font-size: 14px;
            color: #444;
            outline: none;
            border: 1px solid rgba(0, 0, 0, .49);
            padding-left: 20px;
            -webkit-background-clip: padding-box;
            -moz-background-clip: padding-box;
            background-clip: padding-box;
            border-radius: 6px;
            box-shadow: inset 0px 2px 0px #d9d9d9;
            padding-top: 5px;
        }
        .showpassword
        {
            background-image: url(images/password.png);
            background-repeat: no-repeat;
            background-position: 20px 15px;
            opacity: 0.8;
        }
        .hidepassword
        {
            background-image: none;
        }
    </style>
</head>
<body class="loginbody">
    <form id="form2" runat="server" defaultbutton="btnlogin" visible="True" autocomplete="off">

    <script type="text/javascript">
        //
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        //Raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(BeginRequestHandler);

        // Raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(EndRequestHandler);

        function BeginRequestHandler(sender, args) {
            //Shows the modal popup - the update progress
            var popup = $find('<%= modalPopup.ClientID %>');
            if (popup != null) {
                popup.show();
            }
        }

        function EndRequestHandler(sender, args) {
            //Hide the modal popup - the update progress
            var popup = $find('<%= modalPopup.ClientID %>');
            if (popup != null) {
                popup.hide();
            }
        }

        function hidecode(bln) {
            if (bln == true) {
                document.getElementById('rowccode').style.display = 'none';
                document.getElementById('rowforgotccode').style.display = 'none';
            }
            else {
                document.getElementById('rowccode').style.display = '';
                document.getElementById('rowforgotccode').style.display = '';
            }
        }
        function Checkvalidation()
        {
            var usename = document.getElementById('<%= txtloginname.ClientID  %>');
            var cmpnycode = document.getElementById('<%= txtCompanyCode.ClientID  %>');
            var psswrd = document.getElementById('<%= txtPwd.ClientID  %>');
            if (usename.value == '' || usename.value == 'User Name') {

                //S.SANDEEP [21-NOV-2018] -- START
                //alert('User Name can not be blank.');
                alert('Please enter a valid Username and Password');
                //S.SANDEEP [21-NOV-2018] -- END                

                    return false;
                }
            if (cmpnycode != null && (cmpnycode.value == '' || cmpnycode.value =='Company Code'))
                {
                    alert('Company Code can not be blank.');
                    return false; 
                }
        }
        
    </script>

    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdateProgress ID="UpdateProgress1" runat="server">
            <ProgressTemplate>
                <%--<asp:Image ID="Image1" ImageUrl="~/images/waiting.gif" AlternateText="Processing" runat="server" />--%>
                <div class="loadingbar" style="left: auto; top: auto; position: absolute; cursor: wait;
                    margin-top: 13%;">
                    <span class="loadspan"></span>
                    <h3>
                        Processing , Please wait...</h3>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
        <cc1:ModalPopupExtender ID="modalPopup" runat="server" TargetControlID="UpdateProgress1"
            PopupControlID="UpdateProgress1" BackgroundCssClass="transbg">
        </cc1:ModalPopupExtender>
        <%--<div class="panel-heading">
            Login</div>--%>
        <div class="panel-body" style="width: 100%;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlMain" runat="server" >
                    <div id="main">
                        <div style="float: left; width: 100%;">
                            <div style="float: left; width: 30%;">
                                <%-- <img src="images/user_icon12.png" id="avtar" style="margin-top: 3px;" />--%>
                                <i class="fa fa-user user" style="margin-top: -10px; font-size: 165px"></i>
                                <asp:Label ID="lblUser" CssClass="label" runat="server" Text="* Login Name:" Visible="false"></asp:Label>
                                <asp:Label ID="lblPss" runat="server" Text="* Password:" Visible="false"></asp:Label>
                                <asp:Label ID="LblCompanyCode" runat="server" CssClass="label" Text="* Company Code:"
                                    Visible="false"></asp:Label>
                            </div>
                            <div style="float: left; width: 70%;">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <asp:TextBox ID="txtloginname" runat="server" ValidationGroup="login" placeholder="User Name"
                                                ></asp:TextBox>
                                            <%--<cc1:TextBoxWatermarkExtender ID="WaterUsername" runat="server" TargetControlID="txtloginname"
                                                WatermarkText="User Name" WatermarkCssClass="watermarkcss" />--%>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                                ControlToValidate="txtloginname" ErrorMessage="User name cannot be blank. "
                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" SetFocusOnError="True"
                                                ValidationGroup="login"></asp:RequiredFieldValidator>
                                            <%--<cc1:ValidatorCalloutExtender runat="Server" ID="PNReqE" TargetControlID="RequiredFieldValidator1"
                                                Width="300px" />--%>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <asp:TextBox ID="txtPwd" runat="server" Text="" TextMode="Password"  
                                                 placeholder="Password"></asp:TextBox>
                                            <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtPwd"
                                                WatermarkText="*" WatermarkCssClass="watermarkcss" />--%>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%" id="rowccode">
                                        <td style="width: 100%">
                                            <asp:TextBox ID="txtCompanyCode" runat="server" ValidationGroup="login"
                                                placeholder="Company Code" Visible="false"></asp:TextBox>
                                            <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtCompanyCode"
                                                WatermarkText="Company Code" WatermarkCssClass="watermarkcss" />--%>
                                            <asp:RequiredFieldValidator ID="rfvCompanyCode" runat="server" Display="Dynamic" ControlToValidate="txtCompanyCode"
                                                ErrorMessage="Company Code cannot be blank. " CssClass="ErrorControl" ForeColor="White"
                                                Style="z-index: 1000" SetFocusOnError="True" ValidationGroup="login"></asp:RequiredFieldValidator>
                                            <%--<cc1:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="rfvCompanyCode"
                                                Width="300px" />--%>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 100%; text-align: right; padding-top: 10px;">
                                            <div style="float: left">
                                                <asp:LinkButton ID="lnkForgotPassword" runat="server" Text="Forgot Password?"></asp:LinkButton>
                                            </div>
                                            <asp:Button ID="btnlogin" runat="server" CssClass="btnDefault" Text="Login" ValidationGroup="login"
                                                 CausesValidation="true" /> <%--  Sohail (17 Dec 2018) -- CausesValidation="true"  --%>
                                            <div style="display: none">
                                                <asp:Label ID="lblLoginMsg" runat="server" CssClass="staticlbl" ForeColor="Red" Visible="false"></asp:Label>
                                                <asp:Label ID="lblusertype" runat="server" CssClass="label" Text="Login As :" Visible="False"></asp:Label>
                                                <asp:RadioButton ID="rtbEmployee" runat="server" AutoPostBack="True" CssClass="label"
                                                    Text="Employee" Visible="False" />
                                                &nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rtbUser" CssClass="label" runat="server" AutoPostBack="True"
                                                    Checked="True" Text="Manager" TabIndex="1" Visible="False" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlError" runat="server" Visible="false">
                        <div id="main">
                            <div style="float: left; width: 100%;">
                                <div style="float: left; width: 30%;">
                                    <i class="fa fa-user user" style="margin-top: -10px; font-size: 165px"></i>
                                </div>
                                <div style="float: left; width: 70%;">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <asp:Label ID="lblErrorMsg" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%; padding-top: 10px;" align="center">
                                                <asp:Button ID="btnRetry" runat="server" CssClass="btnDefault" Text="Retry again..." OnClientClick="location.reload(true);" />
                                            </td>
                                        </tr>
                                    </table>                                    
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--Sohail (28 Mar 2018) - Start--%>
                   
                    <asp:Panel ID="pnlBottom" runat="server" style="bottom:0px;position:absolute;width:100%">
                    
                    <asp:Image ID="imgIBank" runat="server" ImageUrl="~/images/IslamicBank.png" style="border-width:0px;" />
                    <%--<asp:Image ID="imgCompLogo" runat="server" style="border-width:0px;float:right;height:93px;" />--%>
                    <asp:Image ID="imgCompLogo" runat="server" ImageUrl="~/images/amn.png" style="border-width:0px;float:right;height:93px;" />
                    </asp:Panel>
                    
                    
                    <%--Sohail (28 Mar 2018) - End--%>
                    <asp:Button ID="bttnHidden" runat="Server" Style="display: none" />
                    <asp:Button ID="btnHidde1" runat="Server" Style="display: none" />
                    <asp:Button ID="btnChUName" runat="server" Style="display: none" />
                    <uc2:YesNobutton ID="radYesNo" runat="server" Title="Password Expired" Message="Sorry, you cannot login to Aruti Self Service. Reason : Your password has been expired. Do you want to change your password now?" ValidationGroup="ChangePassYesNo" />
                    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="bttnHidden"
                        CancelControlID="btnCancel" PopupControlID="pnlForgotRefNo" DropShadow="true"
                        PopupDragHandleControlID="pnlForgotRefNo" BackgroundCssClass="ModalPopupBG" Drag="True">
                    </cc1:ModalPopupExtender>
                    <%--<asp:Panel ID="" runat="server" Style="cursor: move; display: none; background-color: #FFFFFF;
                        border-style: solid; border-width: thin;" Width="440px">
                        <asp:ImageButton ID="img" runat="server" ImageUrl="~/images/icon_close.gif" AlternateText="Close"
                            CssClass="rightcorner" OnClientClick="$find('ModalPopupExtender1').hide(); return false;" />
                        <div id="divPopup" style="background-color: #FFFFFF; width: 100%;">
                            <div id="inr" style="margin: 40px auto 40px auto; width: 80%;">
                            </div>
                        </div>
                    </asp:Panel>--%>
                    <asp:Panel ID="pnlForgotRefNo" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px" DefaultButton="btnSubmit">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblTitle" runat="server" Text="Forgot Password"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="FilterCriteria" class="panel-default">
                                    <div id="FilterCriteriaTitle" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblEmployeeCode" runat="server" Text="* Employee Code :" Visible="false"></asp:Label>
                                            <asp:Label ID="lblForgetCompanyCode" runat="server" CssClass="label" Text="* Company Code:"
                                                Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="panel-body-default">
                                        <table style="width: 100%;">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtEmpCode" runat="server" MaxLength="50" ValidationGroup="RefNo" placeholder="Employee Code"></asp:TextBox>
                                                            <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtEmpCode"
                                                                WatermarkText="Employee Code" WatermarkCssClass="watermarkcss" />--%>
                                                            <asp:RequiredFieldValidator ID="rfvEmpCode" runat="server" Display="None" ValidationGroup="RefNo"
                                                                ControlToValidate="txtEmpCode" ErrorMessage="Employee Code cannot be Blank "
                                                                CssClass="ErrorControl" ForeColor="White" Style="z-index: 900" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                                            <cc1:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="rfvEmpCode"
                                                                Width="300px">
                                                            </cc1:ValidatorCalloutExtender>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnSubmit" EventName="Click" />
                                                            <asp:AsyncPostBackTrigger ControlID="lnkForgotPassword" EventName="Click" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%" id="rowforgotccode">
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtForgetCompanyCode" runat="server" TabIndex="4" ValidationGroup="RefNo" placeholder="Company Code"></asp:TextBox>
                                                    <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtForgetCompanyCode"
                                                        WatermarkText="Company Code" WatermarkCssClass="watermarkcss" />--%>
                                                    <asp:RequiredFieldValidator ID="rfvForgetCompanyCode" runat="server" Display="None"
                                                        ControlToValidate="txtForgetCompanyCode" ErrorMessage="Company Code cannot be blank. "
                                                        CssClass="ErrorControl" ForeColor="White" Style="z-index: 1000" SetFocusOnError="True"
                                                        ValidationGroup="RefNo"></asp:RequiredFieldValidator>
                                                    <cc1:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="rfvForgetCompanyCode"
                                                        Width="300px">
                                                    </cc1:ValidatorCalloutExtender>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btnDefault" CausesValidation="true"
                                                ValidationGroup="RefNo" />
                                                <%--  Sohail (17 Dec 2018) -- Start  --%>
                                            <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnDefault" CausesValidation="false" />--%>
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btnDefault" CausesValidation="true" ValidationGroup="RefNoCancel" />
                                            <%--  Sohail (17 Dec 2018) -- End  --%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" TargetControlID="btnHidde1"
                        CancelControlID="btnCancel" PopupControlID="pnlChangePasswd" DropShadow="true"
                        PopupDragHandleControlID="pnlChangePasswd" BackgroundCssClass="ModalPopupBG"
                        Drag="True">
                    </cc1:ModalPopupExtender>
                    <%--<asp:Panel ID="" runat="server" Style="cursor: move; display: none;
                        background-color: #FFFFFF; border-style: solid; border-width: thin;" Width="440px"
                        DefaultButton="">
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/icon_close.gif"
                            AlternateText="Close" CssClass="rightcorner" OnClientClick="$find('ModalPopupExtender2').hide(); return false;" />
                        <div id="div1" style="background-color: #FFFFFF; width: 100%;">
                            <div id="Div2" style="margin: 40px auto 40px auto; width: 80%;">
                            </div>
                        </div>
                    </asp:Panel>--%>
                    <asp:Panel ID="pnlChangePasswd" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px" DefaultButton="btnSave">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="Label1" runat="server" Text="Change Password"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div5" class="panel-default">
                                    <div id="Div6" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblUserCP" runat="server" Text="Username :" Visible="false"></asp:Label>
                                            <asp:Label ID="lblOldPwd" runat="server" Text="Old Password :" Visible="false"></asp:Label>
                                            <asp:Label ID="lblNewPwd" runat="server" Text="New Password :" Visible="false"></asp:Label>
                                            <asp:Label ID="lblConfirmPwd" runat="server" Text="Confirm Password :" Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="panel-body-default">
                                        <table style="width: 100%;">
                                            <tr style="width: 100%;">
                                                <td style="width: 100%;">
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtUsername" runat="server" ReadOnly="true" ValidationGroup="ChangePwd"></asp:TextBox>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnlogin" EventName="Click" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%;">
                                                <td style="width: 100%;">
                                                    <asp:TextBox ID="txtOldPasswd" runat="server" TextMode="Password" placeholder="Old Password" ValidationGroup="ChangePwd"></asp:TextBox>
                                                    <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtOldPasswd"
                                                        WatermarkText="*" WatermarkCssClass="oldPassword" />--%>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%;">
                                                <td style="width: 100%;">
                                                    <asp:TextBox ID="txtNewPasswd" runat="server" TextMode="Password" placeholder="New Password" ValidationGroup="ChangePwd"></asp:TextBox>
                                                   <%-- <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtNewPasswd"
                                                        WatermarkText="*" WatermarkCssClass="NewPassword" />--%>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%;">
                                                <td style="width: 100%;">
                                                    <asp:TextBox ID="txtConfirmPasswd" runat="server" TextMode="Password" placeholder="Confirm Password" ValidationGroup="ChangePwd"></asp:TextBox>
                                                   <%-- <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtConfirmPasswd"
                                                        WatermarkText="*" WatermarkCssClass="ConfirmPassword" />--%>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnDefault" OnClientClick="$find('ModalPopupExtender2').hide(); return true;" CausesValidation="true" ValidationGroup="ChangePwd" />
                                            <%--  Sohail (17 Dec 2018) -- Start  --%>
                                            <%--<asp:Button ID="btnCancelCP" runat="server" Text="Cancel" CssClass="btnDefault" CausesValidation="false" />--%>
                                            <asp:Button ID="btnCancelCP" runat="server" Text="Cancel" CssClass="btnDefault" CausesValidation="true" ValidationGroup="NoChangePwd" />
                                            <%--  Sohail (17 Dec 2018) -- End  --%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="ModalPopupExtender3" runat="server" TargetControlID="btnChUName"
                        CancelControlID="btnUCancel" PopupControlID="pnlChangeUName" DropShadow="true"
                        PopupDragHandleControlID="pnlChangeUName" BackgroundCssClass="ModalPopupBG" Drag="True">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlChangeUName" runat="server" Style="cursor: move; display: none;
                        background-color: #FFFFFF; border-style: solid; border-width: thin;" Width="440px"
                        DefaultButton="btnUSubmit">
                        <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/icon_close.gif"
                            AlternateText="Close" CssClass="rightcorner" OnClientClick="$find('ModalPopupExtender3').hide(); return false;" />
                        <div id="div3" style="background-color: #FFFFFF; width: 100%;">
                            <div id="Div4" style="margin: 40px auto 40px auto; width: 80%;">
                                <table id="Table2" style="border-style: solid; width: 100%;">
                                    <tr class="row">
                                        <td class="col1">
                                            <asp:Label ID="lblOldUName" runat="server" Text="Old Username :"></asp:Label>
                                        </td>
                                        <td class="col2">
                                            <asp:TextBox ID="txtOldUName" runat="server" ValidationGroup="ChangeUsr"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="row">
                                        <td class="col1">
                                            <asp:Label ID="lblNewUName" runat="server" Text="New Username :"></asp:Label>
                                        </td>
                                        <td class="col2">
                                            <asp:TextBox ID="txtNewUName" runat="server" ValidationGroup="ChangeUsr"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="row">
                                        <td class="col1">
                                            <asp:Label ID="lblConfUName" runat="server" Text="Confirm Username :"></asp:Label>
                                        </td>
                                        <td class="col2">
                                            <asp:TextBox ID="txtConfUName" runat="server" ValidationGroup="ChangeUsr"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="row">
                                        <td class="col1">
                                            <asp:Label ID="lblUPassword" runat="server" Text="Password :"></asp:Label>
                                        </td>
                                        <td class="col2">
                                            <asp:TextBox ID="txtUPassword" runat="server" TextMode="Password" ValidationGroup="ChangeUsr"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr class="row">
                                        <td colspan="2" align="right" style="padding-top: 10px; padding-bottom: 5px; padding-right: 0px;">
                                            <asp:Button ID="btnUSubmit" runat="server" Text="Submit" CssClass="btnDefault" OnClientClick="$find('ModalPopupExtender3').hide(); return true;" CausesValidation="true" ValidationGroup="ChangeUsr" />
                                            &nbsp;
                                            <%--  Sohail (17 Dec 2018) -- Start  --%>
                                            <%--<asp:Button ID="btnUCancel" runat="server" Text="Cancel" CssClass="btnDefault" CausesValidation="false" />--%>
                                            <asp:Button ID="btnUCancel" runat="server" Text="Cancel" CssClass="btnDefault" CausesValidation="true" ValidationGroup="NoChangeUsr" />
                                            <%--  Sohail (17 Dec 2018) -- End  --%>
                                        </td>
                                        <td class="col2">
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--  'Sohail (17 Dec 2018) -- Start  --%>
                    <%--  NMB Enhancement - Security issues in Self Service  --%>
                    <%-- <cc1:ModalPopupExtender ID="popCompanySelection" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="btnCompanyClose" PopupControlID="pnlCompanySelect" TargetControlID="btnCompanySelect">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlCompanySelect" runat="server" class="panel-primary" Style="display: none;
                        width: 370px;" DefaultButton="btnOpen">
                        <div class="panel-heading">
                            Company Selection</div>
                        <div class="panel-body">
                            <div style="vertical-align: middle; overflow: auto; margin-left: 5px; margin-top: 5px;
                                margin-bottom: 10px;">
                                <asp:UpdatePanel ID="UpdatePanel12" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table style="padding: 10px; padding-right: 0px;">
                                            <tr style="height: 30px;">
                                                <td align="left" style="width: 30%">
                                                    <asp:Label ID="lblcompany" runat="server" Text="Company:"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 68%">
                                                    <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass=" staticlbl"
                                                        TabIndex="2">
                                                    </asp:DropDownList>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr style="height: 30px;" style="width: 30%">
                                                <td align="left">
                                                    <asp:Label ID="lblYear" runat="server" Text="Financial Year:"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 68%">
                                                    <asp:TextBox ID="txtfinyear" runat="server" ReadOnly="True" TabIndex="3"></asp:TextBox>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnlogin" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <div class="btn-default" style="margin-left: 0px; margin-right: 0px; margin-bottom: 0px;">
                                <asp:Button ID="btnOpen" runat="server" Text="Open" Width="70px" CssClass="btnDefault" />
                                <asp:Button ID="btnCompanyClose" runat="server" Text="Close" Width="70px" CssClass="btnDefault" />
                                <asp:Button ID="btnCompanySelect" runat="Server" Style="display: none" />
                            </div>
                        </div>
                    </asp:Panel>--%>
                    <asp:DropDownList ID="ddlCompany" runat="server" AutoPostBack="True" CssClass=" staticlbl" Visible="false"
                                                        TabIndex="2">
                                                    </asp:DropDownList>
                     <%--  Sohail (17 Dec 2018) -- End  --%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    </form>

    <script src="Help/alert/sweetalert.min.js" type="text/javascript"></script>
    <script src="Help/alert/dialogs.js" type="text/javascript"></script>
</body>
</html>
