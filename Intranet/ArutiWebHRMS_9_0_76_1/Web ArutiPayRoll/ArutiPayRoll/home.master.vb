﻿Imports Aruti.Data
Imports System.Data
Imports System.Globalization

Partial Class template2_mstTemplate2
    Inherits System.Web.UI.MasterPage
    Private DisplayMessage As New CommonCodes
    'Hemant (06 Aug 2019) -- Start
             'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                 Private ReadOnly mstrModuleName As String = "frmNewMDI"
    'Hemant (06 Aug 2019) -- End

#Region " Methods & Functions "

    Private Sub GetReportPriviliage()
        Try
            Dim objReport As New clsArutiReportClass
            Dim dsData As DataSet = objReport.getReportList(CInt(Session("UserId")), Session("CompanyUnkId"), True)
            Dim lstPanels As List(Of Panel) = (From p In pnlMenuWrapper.Controls.OfType(Of Panel)() Select (p)).ToList
            Dim strId As String = ""
            For Each pnl In lstPanels
                If pnl.ID = pnlPayrollMSSReport_1.ID OrElse pnl.ID = pnlPayrollMSSReport_2.ID OrElse pnl.ID = pnlPayrollMSSReport_3.ID OrElse pnl.ID = pnlPayrollMSSReport_4.ID OrElse pnl.ID = pnlLeaveMSSReport_1.ID _
                   OrElse pnl.ID = pnlEmployeeReports_1.ID OrElse pnl.ID = pnlEmployeeReports_2.ID OrElse pnl.ID = pnlEmployeeReports_3.ID OrElse pnl.ID = pnlEmployeeReports_4.ID OrElse pnl.ID = pnlTnAMSSReport_1.ID _
                   OrElse pnl.ID = pnlCRMSSReport_1.ID OrElse pnl.ID = pnl_statutory.ID OrElse pnl.ID = pnlRetirementMSSReport_1.ID OrElse pnl.ID = pnlEmployeeReports_5.ID Then

                    'Pinkal (30-Mar-2021)-- 'NMB Enhancement  -  Working on Employee Recategorization history Report. [pnl.ID = pnlEmployeeReports_5.ID]

                    'Pinkal (18-Mar-2021) --  'NMB Enhancmenet : AD Enhancement for NMB.[pnl.ID = pnlRetirementMSSReport_1.ID  ]

                    'Sohail (30 Jun 2017) - [pnl_statutory.ID]
                    Dim lstMSS As List(Of Panel) = (From p In pnl.Controls.OfType(Of Panel)() Select (p)).ToList
                    For Each pnlMSS In lstMSS
                        'SHANI (13 APR 2015)-START
                        If pnlMSS.Visible = False Then Continue For
                        'SHANI (13 APR 2015)-END
                        strId = pnlMSS.ID.ToString().Substring(pnlMSS.ID.ToString().IndexOf("_") + 1, (pnlMSS.ID.ToString().Length - 1) - pnlMSS.ID.ToString().IndexOf("_"))
                        Dim drRow() As DataRow = dsData.Tables(0).Select("ReportId=" & strId)
                        If drRow.Length > 0 Then
                            pnlMSS.Visible = True
                        Else
                            pnlMSS.Visible = False
                        End If
                    Next
                ElseIf pnl.ID.ToUpper.StartsWith("PNLEMPLOYEEREPORTS") Then
                    Dim lstEMPRep As List(Of Panel) = (From p In pnl.Controls.OfType(Of Panel)() Select (p)).ToList
                    For Each pnlEmpRep In lstEMPRep
                        'SHANI (13 APR 2015)-START
                        If pnlEmpRep.Visible = False Then Continue For
                        'SHANI (13 APR 2015)-END
                        strId = pnlEmpRep.ID.ToString().Substring(pnlEmpRep.ID.ToString().IndexOf("_") + 1, (pnlEmpRep.ID.ToString().Length - 1) - pnlEmpRep.ID.ToString().IndexOf("_"))
                        Dim drRow() As DataRow = dsData.Tables(0).Select("ReportId=" & strId)
                        If drRow.Length > 0 Then
                            pnlEmpRep.Visible = True
                        Else
                            pnlEmpRep.Visible = False
                        End If
                    Next
                ElseIf IsNumeric(pnl.ID.Substring(4)) = True Then
                    'SHANI (13 APR 2015)-START
                    If pnl.Visible = False Then Continue For
                    'SHANI (13 APR 2015)--END
                    strId = pnl.ID.Substring(4)

                    Dim drRow() As DataRow = dsData.Tables(0).Select("ReportId=" & strId)

                    If drRow.Length > 0 Then
                        pnl.Visible = True
                    Else
                        pnl.Visible = False
                    End If
                End If
            Next
        Catch ex As Exception
            'MsgBox("GetReportPriviliage:- " & ex.Message)
			DisplayMessage.DisplayError(ex, Me.Page)
        End Try
    End Sub

    Private Sub GetReportPriviliageESS()
        Try
            Dim objReport As New clsArutiReportClass
            Dim dsData As DataSet = objReport.getReportList(CInt(Session("UserId")), Session("CompanyUnkId"), False)
            'Dim Ctrl As Control = Me.FindControl("form1")
            'If Ctrl Is Nothing Then Exit Sub
            Dim lstPanels As List(Of Panel) = (From p In pnlMenuWrapper.Controls.OfType(Of Panel)() Where p.ID.ToUpper.StartsWith("PNLEMPLOYEEREPORTS") Select (p)).ToList
            For Each pnlEmpRep In lstPanels
                Dim lstEMPRep As List(Of Panel) = (From p In pnlEmpRep.Controls.OfType(Of Panel)() Select (p)).ToList
                For Each pnl In lstEMPRep
                    'SHANI (13 APR 2015)-START
                    If pnl.Visible = False Then Continue For
                    'SHANI (13 APR 2015)-END

                    If pnl.ID = pnl_67.ID Then
                        pnl.Visible = True
                    Else
                        pnl.Visible = False
                    End If
                Next
            Next
        Catch ex As Exception
            'MsgBox("GetReportPriviliageESS:- " & ex.Message)
			DisplayMessage.DisplayError(ex, Me.Page)
        End Try
    End Sub

    Protected Sub objlnkCustomHeader_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
        Dim ilnk As LinkButton = CType(sender, LinkButton)
        Session.Add("customheaderunkid", ilnk.CommandArgument)
        Response.Redirect(Session("servername") & "~/Assessment New/Common/wPg_CustomeItemView.aspx", False)
		Catch ex As Exception
                 DisplayMessage.DisplayError(ex, Me.Page)
		End Try
    End Sub

    Protected Sub objlnkVacancyAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Session("ScanAttchLableCaption") = "Select Employee"
            Session("ScanAttchRefModuleID") = enImg_Email_RefId.Applicant_Job_Vacancy
            Session("ScanAttchenAction") = enAction.ADD_ONE
            Session("ScanAttchToEditIDs") = ""
            Response.Redirect(Session("servername") & "~/Others_Forms/wPg_ScanOrAttachmentInfo.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me.Page)
        End Try
    End Sub

    Private Sub Generate_Custom_Header_Menu()
        Try
            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
            'Dim objCHeader As New clsassess_custom_header
            'Dim dsList As New DataSet
            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    dsList = objCHeader.Get_Headers_For_Menu(False, True)
            'ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    dsList = objCHeader.Get_Headers_For_Menu(True, False)
            'End If
            'dlCHeaderMenu.DataSource = dsList
            'dlCHeaderMenu.DataBind()
            'S.SANDEEP [01-OCT-2018] -- END
        Catch ex As Exception
        Finally
        End Try
    End Sub

#End Region

#Region " Page Events "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Session("UserName") IsNot Nothing Then
                'Sohail (08 Jul 2017) -- Start
                'lblUsername.Text = Session("UserName").ToString.Replace(vbCrLf, " <br />")
                lblUsername.Text = Session("UserName").ToString.Replace(vbCrLf, " <br />").Replace("Employee :", "")
                'Sohail (08 Jul 2017) -- End
            Else
                pnlTRALogo.Visible = False
                pnlViewUser.Visible = False
                'Response.Redirect(Session("servername") & "~/Index.aspx", False)
                DisplayMessage.DisplayMessage("Sorry, Session is Expired. Please Re-Login again.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/index.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
             'Hemant (06 Aug 2019) -- Start
             'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.          
                Call SetLanguage()	
		Call SetCaptions()
                Call Language._Object.SaveValue()

                'Pinkal (27-Aug-2020) -- Start
                'Bug NMB:  Working on IIS Freezing and Dump Issue for NMB.
                ' Call SetLanguage()
                'Pinkal (27-Aug-2020) -- End


              'Hemant (06 Aug 2019) -- End			
			   'Gajanan [18-Mar-2019] -- Start
               'Enhancement - Change Message Box For NMB
                'Me.Page.ClientScript.RegisterClientScriptInclude(Me.GetType(), "Global", ResolveClientUrl("~/Help/alert/sweetalert.min.js"))
                'Me.Page.ClientScript.RegisterClientScriptInclude(Me.GetType(), "Global1", ResolveClientUrl("~/Help/alert/dialogs.js"))
                'Gajanan [18-Mar-2019] -- End

                'Sohail (28 Mar 2018) -- Start
                'Amana bank Enhancement : Ref. No. 192 - Company logo Should display on Aruti SS,The Aruti SS theme color should be of Amana Bank in 71.1.
                imgCompLogo.ImageUrl = "data:image/png;base64," + Session("CompanyLogo")
                'Sohail (28 Mar 2018) -- End

                If CBool(Session("PolicyManagementTNA")) = False Then
                    If Not Page.ClientScript.IsStartupScriptRegistered("PolicyManagementTNA") Then
                        Page.ClientScript.RegisterStartupScript(Me.GetType(), "PolicyManagementTNA", "try {if ($('#divTnA') != null) $('#divTnA').remove();} catch (err) { }", True)
                    End If
                End If

                pnlViewUser.Visible = False
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    If CInt(Session("RoleID")) = 1 Then 'Administrator
                        'pnlViewUser.Visible = True 'Commented due to performance issue at NMB
                    End If
                    clsActiveUserEngine.Joined("USER" & Session("UserId").ToString, Session("MemberName").ToString, clsActiveUsers.enUserType.User)
                Else
                    clsActiveUserEngine.Joined("EMP" & Session("Employeeunkid").ToString, Session("MemberName").ToString, clsActiveUsers.enUserType.Employee)
                End If

                If Session("CompCode").ToString.ToUpper <> "TRA" Then
                    pnlTRALogo.Visible = False
                Else
                    pnlTRALogo.Visible = True
                End If

                For Each s As String In clsActiveUserEngine.GetUserName()
                    Dim li As New ListItem
                    li.Text = s
                    li.Value = s
                    If s = Session("UserName").ToString Then
                        li.Selected = True
                        li.Attributes.Add("class", "listitem-selected")
                    Else
                        li.Attributes.Add("class", "listitem")
                    End If
                    lstMembers.Items.Add(li)
                Next
                lblCount.Text = "Online Members ( " + lstMembers.Items.Count.ToString + " )"


                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("ApproveDisapproveStaffRequisition.aspx?") = True Then 'AndAlso (Request.UrlReferrer Is Nothing OrElse Request.UrlReferrer.ToString().Length <= 0)
                    pnlMenuWrapper.Visible = False
                End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPgEmployeeProfile.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPgInterviewAnalysisList.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_ApplicantFilterList.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_FinalApplicant.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPgAssessorEvaluation.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPgReviewerEvaluation.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_ViewPerfEvaluation.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("Paymentapprovedisapprove.aspx?") = True Then 'AndAlso (Request.UrlReferrer Is Nothing OrElse Request.UrlReferrer.ToString().Length <= 0) 
                    pnlMenuWrapper.Visible = False
                End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPgPerformance_Planning?") = True Then
                    pnlMenuWrapper.Visible = False
                End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_AuthorizedPayment.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If

                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_LoanApproval.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_NewLoanAdvance_AddEdit.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_LoanAdvanceOperationList.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If
                'Shani (21-Jul-2016) -- End

                'S.SANDEEP [25 JUL 2016] -- START
                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_DisciplineCharge_Add_Edit.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPgViewEmployeeDisciplineResponse.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If
                'S.SANDEEP [25 JUL 2016] -- END

                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.

                'If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("ProcessPendingLeave.aspx?") = True AndAlso Session("mainfullurl").ToString.Contains("ProcessPendingLeave.aspx?Id=") = False Then
                '    pnlMenuWrapper.Visible = False
                'End If

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_ProcessLeaveAddEdit.aspx?") = True AndAlso Session("mainfullurl").ToString.Contains("wPg_ProcessLeaveAddEdit.aspx?Id=") = False Then
                    pnlMenuWrapper.Visible = False
                End If

                'Pinkal (06-Jan-2016) -- End

                'Pinkal (28-Apr-2015) -- Start
                'Enhancement - CONVERT VIEWSTATE TO SESSION FOR AKFTZ BCOZ PROCESS PENDING PAGE CAN'T BE RESPOND WHEN PAGE IS REJECT OR APPROVED.

                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.
                'If Session("mainfullurl") IsNot Nothing AndAlso Session("dsPedingList") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("ProcessPendingLeave.aspx") = False AndAlso Session("mainfullurl").ToString.Contains("ProcessPendingLeave.aspx?Id=") = False Then
                '    Session.Remove("dsPedingList")
                'End If
                If Session("mainfullurl") IsNot Nothing AndAlso Session("dsPedingList") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_ProcessLeaveAddEdit.aspx") = False AndAlso Session("mainfullurl").ToString.Contains("wPg_ProcessLeaveAddEdit.aspx?Id=") = False Then
                    Session.Remove("dsPedingList")
                End If
                'Pinkal (06-Jan-2016) -- End
                'Pinkal (28-Apr-2015) -- End


                'Pinkal (18-Jun-2015) -- Start
                'Enhancement - WORKING ON DOCUMENT ATTACHMENT FEATURE IN LEAVE FORM IN WEB.

                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("LeaveApplication.aspx") = False AndAlso Session("mainfullurl").ToString.Contains("LeaveApplication.aspx?") = True Then
                    If Session("LVForm_EmployeeID") IsNot Nothing Then
                        Session("LVForm_EmployeeID") = Nothing
                    End If
                    If Session("LVForm_LeaveTypeID") IsNot Nothing Then
                        Session("LVForm_LeaveTypeID") = Nothing
                    End If
                    If Session("LVForm_ApplyDate") IsNot Nothing Then
                        Session("LVForm_ApplyDate") = Nothing
                    End If
                    If Session("LVForm_StartDate") IsNot Nothing Then
                        Session("LVForm_StartDate") = Nothing
                    End If
                    If Session("LVForm_EndDate") IsNot Nothing Then
                        Session("LVForm_EndDate") = Nothing
                    End If
                    If Session("ScanAttachData") IsNot Nothing Then
                        Session("ScanAttachData") = Nothing
                    End If
                End If

                'Pinkal (18-Jun-2015) -- End


                'Pinkal (13-Jul-2015) -- Start
                'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_ExpenseApproval.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If
                'Pinkal (13-Jul-2015) -- End



                'Pinkal (11-Sep-2019) -- Start
                'Enhancement NMB - Working On Claim Retirement for NMB.
                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPgClaimRetirementApproval.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If
                'Pinkal (11-Sep-2019) -- End


                'Pinkal (07-Nov-2019) -- Start
                'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("Ot_RequisitionApproval.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If
                'Pinkal (07-Nov-2019) -- End


                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_ClaimAndRequestList.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If
                'Pinkal (04-Feb-2020) -- End



                'Pinkal (27-Feb-2020) -- Start
                'Enhancement - Changes related To OT NMB Testing.
                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("OTRequisition.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If
                'Pinkal (27-Feb-2020) -- End



                'Nilay (21-Oct-2016) -- Start
                'Enhancement : Kenya P9A statutory report on ESS for CCK
                If CInt(Session("Compcountryid")) = 112 Then 'KENYA
                    pnl_statutory.Visible = True
                    'Sohail (30 Jun 2017) -- Start
                    'Issue - Report Previledge not applied on P9A report.
                    'pnl_P9A.Visible = True
                    pnl_124.Visible = True
                    'Sohail (30 Jun 2017) -- End
                Else
                    pnl_statutory.Visible = False
                    'Sohail (30 Jun 2017) -- Start
                    'Issue - Report Previledge not applied on P9A report.
                    'pnl_P9A.Visible = False
                    pnl_124.Visible = False
                    'Sohail (30 Jun 2017) -- End
                End If
                'Nilay (21-Oct-2016) -- End

                'Sohail (18 Apr 2018) -- Start
                'Malawi Enhancement - Ref # 211 : New statutory report for Malawi - Form P.9 shows annual gross incomes and taxable income for employee in 72.1.
                If CInt(Session("Compcountryid")) = 129 Then 'MALAWI
                    If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                        pnl_statutory.Visible = True
                        pnl_205.Visible = True
                    End If
                End If
                'Sohail (18 Apr 2018) -- End

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPgEmployeeLvlGoalsList.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If
                'Shani (26-Sep-2016) -- End


                'S.SANDEEP |18-OCT-2021| -- START
                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("ChangePassword.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If
                'S.SANDEEP |18-OCT-2021| -- END


                'Sohail (17 Aug 2020) -- Start
                'NMB Issue : Object reference not set to an instance of an object.; at template2_mstTemplate2.Page_Load.                
                ''Nilay (27 Feb 2017) -- Start
                '            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                '            If Session("mainfullurl") IsNot Nothing _
                '                AndAlso Session("mainfullurl").ToString.Contains("wPg_EmployeeTimeSheetApprovalList.aspx?") = True _
                '                OrElse Session("mainfullurl").ToString.Contains("wPg_EmployeeTimeSheetApproval.aspx?") = True Then
                '                pnlMenuWrapper.Visible = False
                '            End If
                ''Nilay (27 Feb 2017) -- End				
                If Session("mainfullurl") IsNot Nothing _
                    AndAlso (Session("mainfullurl").ToString.Contains("wPg_EmployeeTimeSheetApprovalList.aspx?") = True _
                    OrElse Session("mainfullurl").ToString.Contains("wPg_EmployeeTimeSheetApproval.aspx?") = True) Then
                    pnlMenuWrapper.Visible = False
                End If
                'Sohail (17 Aug 2020) -- End

                'Gajanan [13-AUG-2018] -- Start
                'Enhancement - Implementing Grievance Module.
                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_ResolutionStep.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If
                'Gajanan(13-AUG-2018) -- End

                'Gajanan [13-AUG-2018] -- Start
                'Enhancement - Implementing Grievance Module.
                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("AgreeDisagreeResponse.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If
                'Gajanan(13-AUG-2018) -- End


                'S.SANDEEP [09-OCT-2018] -- START
                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPgApplyTrainingRequisition.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If
                'S.SANDEEP [09-OCT-2018] -- END

                'S.SANDEEP |16-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : ZRA {Language & Custom Item} & NMB Email Notification {Calibration}
                If Session("mainfullurl") IsNot Nothing AndAlso Session("mainfullurl").ToString.Contains("wPg_SubmitList.aspx?") = True Then
                    pnlMenuWrapper.Visible = False
                End If
                'S.SANDEEP |16-AUG-2019| -- END

            End If

            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                'pnlTraining.Visible = False '*** One login feature
                pnlSalaryChange.Visible = True
                'pnlAssetDeclaration.Visible = False 'Asset Declaration
                pnlLeaveApprover.Visible = True
                pnlPayrollMSSReport_1.Visible = True
                pnlPayrollMSSReport_2.Visible = True
                pnlPayrollTransactions.Visible = True
                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                pnlPayrollMSSReport_3.Visible = True
                pnlPayrollMSSReport_4.Visible = True
                pnlLeaveMSSReport_1.Visible = True
                pnlEmployeeReports_4.Visible = True
                pnlTnAMSSReport_1.Visible = True
                'SHANI [09 Mar 2015]--END 
                If CBool(Session("SetPayslipPaymentApproval")) = True Then
                    pnlPaymentApproval.Visible = True
                Else
                    pnlPaymentApproval.Visible = False
                End If

                pnlLoan_Advance_List.Visible = True
                pnlEmployeeSavings.Visible = True
                'SHANI (05 May 2015) -- Start
                pnlLeaveApprover.Visible = True
                'SHANI (05 May 2015) -- End 
                If CBool(Session("PolicyManagementTNA")) = False Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "PolicyManagementTNA", "try {if ($('#divTnA') != null) $('#divTnA').remove();} catch (err) { }", True)
                End If


                If CBool(Session("ApplyStaffRequisition")) = True Then
                    pnlStaffRequisition.Visible = True
                Else
                    pnlStaffRequisition.Visible = False
                End If

                '    lnkMyprmcGoals.ToolTip = lnkMyprmcGoals.Text
                pnlSelf.Visible = False
                pnlAssessUser.Visible = True
                pnlReviewEmployeeAssessment.Visible = CBool(Session("IsCompanyNeedReviewer"))


                pnlClaimApproverTran.Visible = True
                pnlClaimPosing.Visible = True


                'Pinkal (13-Jun-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                pnl_214.Visible = True  'Leave Application Report
                'Pinkal (13-Jun-2019) -- End


                'Pinkal (13-Aug-2019) -- Start
                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                pnlExemptEmpBudgetTimesheet.Visible = CBool(Session("AllowEmpAssignedProjectExceedTime"))
                'Pinkal (13-Aug-2019) -- End


                'Pinkal (12-Nov-2020) -- Start
                'Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.
                pnlShortListingFilterList.Visible = Not CBool(Session("SkipApprovalFlowForApplicantShortListing"))
                pnlFinalShortListedApplicant.Visible = Not CBool(Session("SkipApprovalFlowForFinalApplicant"))
                'Pinkal (12-Nov-2020) -- End


                'Pinkal (20-Nov-2020) -- Start
                'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.
                If Session("CompName").ToString().ToUpper() = "KADCO" Then
                    pnl_236.Visible = True
                Else
                    pnl_236.Visible = False
                End If
                'Pinkal (20-Nov-2020) -- End


                GetReportPriviliage()
            Else

                '    lnkMyprmcGoals.ToolTip = lnkMyprmcGoals.Text

                'Shani(24-Dec-2015) -- Start
                'Enhancement - CCBRT WORK
                'pnlSelf.Visible = True
                If CBool(Session("AllowAssessor_Before_Emp")) Then
                    pnlSelf.Visible = False
                Else
                    pnlSelf.Visible = True
                End If
                'Shani(24-Dec-2015) -- End

                pnlAssessUser.Visible = False

                pnlMedical.Visible = False     'Medical

                'pnlTraining.Visible = True '*** One login feature
                pnlSalaryChange.Visible = False

                'Nilay (03-Nov-2016) -- Start
                'Enhancement : Option to Show / hide payslip on ESS
                'pnl_5.Visible = True
                If CBool(Session("ShowPayslipOnESS")) = True Then
                    pnl_5.Visible = True
                Else
                    pnl_5.Visible = False
                End If
                'Nilay (03-Nov-2016) -- End

                If CBool(Session("AllowToViewPersonalSalaryCalculationReport")) = True Then
                    pnl_1.Visible = True
                Else
                    pnl_1.Visible = False
                End If
                If CBool(Session("AllowToViewEDDetailReport")) = True Then
                    pnl_60.Visible = True
                Else
                    pnl_60.Visible = False
                End If

                pnlPayrollMSSReport_1.Visible = False
                pnlPayrollMSSReport_2.Visible = False
                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                pnlPayrollMSSReport_3.Visible = False
                pnlPayrollMSSReport_4.Visible = False
                pnlLeaveMSSReport_1.Visible = False
                pnlEmployeeReports_4.Visible = False

                pnlTnAMSSReport_1.Visible = False
                'SHANI [09 Mar 2015]--END 
                pnlPayrollTransactions.Visible = False


                pnlMedical.Visible = False

                pnlLeaveApprover.Visible = False
                pnlDeclaration.Visible = True

                'pnlLoan_Advance_List.Visible = False 'Varsha Rana (12-Sept-2017
                pnlEmployeeSavings.Visible = False
                'SHANI (05 May 2015) -- Start
                pnlLeaveApprover.Visible = False
                'SHANI (05 May 2015) -- End 
                If CBool(Session("PolicyManagementTNA")) = False Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "PolicyManagementTNA", "try {if ($('#divTnA') != null) $('#divTnA').remove();} catch (err) { }", True)
                End If
                pnlRecruitment.Visible = False

                pnlStaffRequisition.Visible = False

                pnl_105.Visible = False
                pnl_101.Visible = False
                pnl_12.Visible = False

                pnlClaimApproverTran.Visible = False
                pnlClaimPosing.Visible = False

                'Pinkal (13-Jun-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                pnl_214.Visible = False  'Leave Application Report
                'Pinkal (13-Jun-2019) -- End

                GetReportPriviliageESS()

                'S.SANDEEP [25-OCT-2017] -- START
                If CBool(Session("ShowCustomHeaders")) = True Then
                    pnlCustomHeaders.Visible = True
                Else
                    pnlCustomHeaders.Visible = False
                End If
				
                If Session("ShowSelfAssessment") = True Then
                    pnlSelf.Visible = True
                Else
                    pnlSelf.Visible = False
                End If
                If Session("ShowViewPerformanceAssessment") = True Then
                    pnl_ViewPerfAssesments.Visible = True
                Else
                    pnl_ViewPerfAssesments.Visible = False
                End If
                'S.SANDEEP [25-OCT-2017] -- END

                'Pinkal (20-Nov-2020) -- Start
                'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.
                pnl_236.Visible = False
                'Pinkal (20-Nov-2020) -- End

            End If

            pnlLeaveIssueUser.Visible = CBool(Session("AllowToAssignIssueUsertoEmp"))
            pnlMigrateApprovers.Visible = CBool(Session("AllowtoMigrateLeaveApprovers"))

            If Session("Theme") = "blue" Then
                rdbBlue.Checked = True
            ElseIf Session("Theme") = "black" Then
                rdbBlack.Checked = True
                'Shani(15-MAR-2016) -- Start
            ElseIf Session("Theme") = "green" Then
                rdbGreen.Checked = True
            ElseIf Session("Theme") = "yellow" Then
                rdbYellow.Checked = True
                'Shani(15-MAR-2016) -- End
                'Sohail (28 Mar 2018) -- Start
                'Amana bank Enhancement : Ref. No. 192 - Company logo Should display on Aruti SS,The Aruti SS theme color should be of Amana Bank in 71.1.
            ElseIf Session("Theme") = "brown" Then
                rdbBrown.Checked = True
                'Sohail (28 Mar 2018) -- End
            End If


            If CInt(Session("CascadingTypeId")) = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                plnGoals.Visible = False
            End If
            Call Generate_Custom_Header_Menu()


            'SHANI (09 APR 2015)-START
            'Enhancement - Allow more than one employee to be replaced in staff requisition.
            'pnlSelfCompetencies.Visible = Session("Self_Assign_Competencies")
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                pnlSelfCompetencies.Visible = Session("Self_Assign_Competencies")

                'Shani(11-Feb-2016) -- Start
                'If dlCHeaderMenu.Items.Count <= 0 AndAlso CBool(Session("Self_Assign_Competencies")) = False Then
                '    pnlSelfVisible.Visible = False
                'Else
                '    pnlSelfVisible.Visible = True
                'End If
                'Shani(11-Feb-2016) -- End

            Else
                pnlSelfCompetencies.Visible = True
            End If
            'SHANI (APR 2015)--END 


            'Pinkal (13-Jul-2015) -- Start
            'Enhancement - WORKING ON C & R ACCESS PRIVILEGE.
            PnlClaimApproverMigration.Visible = CBool(Session("AllowtoMigrateExpenseApprover"))
            pnlClaimSwapApprover.Visible = CBool(Session("AllowtoSwapExpenseApprover"))
            pnlClaimPosing.Visible = CBool(Session("AllowtoPostClaimExpenseToPayroll"))
            'Pinkal (13-Jul-2015) -- End


            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By CCBRT

            'Shani (17-Aug-2016) -- Start
            'pnl_ViewPerfAssesments.Visible = CBool(Session("AllowtoViewPerformanceEvaluation"))
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                pnl_ViewPerfAssesments.Visible = CBool(Session("AllowtoViewPerformanceEvaluation"))
            End If
            'Shani (17-Aug-2016) -- End



            pnl_CpnyGoalList.Visible = CBool(Session("AllowtoViewCompanyGoalsList"))
            'Shani(06-Feb-2016) -- End

            'Pinkal (21-Dec-2016) -- Start
            'Enhancement - Adding Swap Approver Privilage for Leave Module.
            pnlLeaveSwapApprover.Visible = CBool(Session("AllowToSwapLeaveApprover"))
            'Pinkal (21-Dec-2016) -- End


            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            If CBool(Session("AllowtoProcessClaimExpenseForm")) = True Then
                pnlGlobalExpenseApproval.Visible = True
            Else
                pnlGlobalExpenseApproval.Visible = False
            End If

            If CBool(Session("AllowToTransferLoanApprover")) = True Then
                pnlLoan_Approver_Migration.Visible = True
            Else
                pnlLoan_Approver_Migration.Visible = False
            End If

            If CBool(Session("AllowToSwapLoanApprover")) = True Then
                pnlLoan_Swap_Approver.Visible = True
            Else
                pnlLoan_Swap_Approver.Visible = False
            End If

            'Varsha Rana (17-Oct-2017) -- End


            'Pinkal (03-Dec-2018) -- Start
            'Enhancement - Working on Asset Declaration Notification settings and notification from reflex.
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                pnlUnlockEmpAssetDeclaration.Visible = CBool(Session("AllowtoUnlockEmployeeForAssetDeclaration"))
            End If
            'Pinkal (03-Dec-2018) -- End

			'Sohail (04 Feb 2020) -- Start
			'NMB Enhancement # : New screen "Non-Disclosure Declaration" with lock / unlock options.
			If (Session("LoginBy") = Global.User.en_loginby.User) Then
                pnlUnlockEmpNonDisclosureDeclaration.Visible = CBool(Session("AllowtoUnlockEmployeeForNonDisclosureDeclaration"))
            End If
			'Sohail (04 Feb 2020) -- End

            'Gajanan [3-April-2019] -- Start
            Dim SkipApprovalOnEmpData As String = CInt(enScreenName.frmQualificationsList) & "," & _
                                                   CInt(enScreenName.frmEmployeeRefereeList) & "," & _
                                                   CInt(enScreenName.frmJobHistory_ExperienceList) & "," & _
                                                   CInt(enScreenName.frmEmployee_Skill_List) & "," & _
                                                   CInt(enScreenName.frmIdentityInfoList) & "," & _
                                                   CInt(enScreenName.frmDependantsAndBeneficiariesList) & "," & _
                                                   CInt(enScreenName.frmAddressList) & "," & _
                                                   CInt(enScreenName.frmEmergencyAddressList) & "," & _
                                                   CInt(enScreenName.frmMembershipInfoList)
            'S.SANDEEP |15-APR-2019| -- START

            'S.SANDEEP |15-APR-2019| -- END


            If SkipApprovalOnEmpData.Length <> CStr(Session("SkipApprovalOnEmpData")).Length Or Session("SkipApprovalOnEmpData") = String.Empty Then
                pnlSkipEmployeeApprovalData.Visible = True
            ElseIf SkipApprovalOnEmpData.Length = CStr(Session("SkipApprovalOnEmpData")).Length Then
                pnlSkipEmployeeApprovalData.Visible = False

            End If

            'Gajanan [3-April-2019] -- End


            'Pinkal (15-Nov-2019) -- Start
            'Enhancement  St. Jude[0004149]  - Employee Timesheet Report should be optional for viewing in Employee SelfService.
            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                pnl_38.Visible = CBool(Session("AllowToviewEmpTimesheetReportForESS"))
            End If
            'Pinkal (15-Nov-2019) -- End


            'Pinkal (08-Jan-2020) -- Start
            'Enhancement - NMB - Working on NMB OT Requisition Requirement.
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                pnlcancelOTRequeistion.Visible = CBool(Session("AllowToCancelEmpOTRequisition"))
            End If
            'Pinkal (08-Jan-2020) -- End


            'Pinkal (15-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.
              If pnl_217.Visible Then
            pnl_217.Visible = CBool(Session("SetOTRequisitionMandatory"))
            End If
			 If pnl_219.Visible Then
                pnl_219.Visible = CBool(Session("SetOTRequisitionMandatory"))
            End If
			 If pnl_220.Visible Then
                pnl_220.Visible = CBool(Session("SetOTRequisitionMandatory"))
            End If
            'Pinkal (15-Jan-2020) -- End


            'Pinkal (29-Jan-2020) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                pnlOTPostToPayroll.Visible = CBool(Session("AllowToPostOTRequisitionToPayroll"))
                'Pinkal (03-Mar-2020) -- Start
                'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.
                pnlOTReqApproverMigration.Visible = CBool(Session("AllowToMigrateOTRequisitionApprover"))
                'Pinkal (03-Mar-2020) -- End
            End If
            'Pinkal (29-Jan-2020) -- End


            'Pinkal (03-Mar-2021)-- Start
            'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
            If Session("CompName") IsNot Nothing AndAlso Session("CompName").ToString().ToUpper() = "KADCO" Then                
                pnlExpenseAdjustment.Visible = CBool(Session("AllowToAdjustExpenseBalance"))
            Else
                pnlExpenseAdjustment.Visible = False
            End If
            'Pinkal (03-Mar-2021) -- End


            'Pinkal (10-Mar-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                pnlRetirementPostToPayroll.Visible = CBool(Session("AllowToPostRetiredApplicationtoPayroll"))
            End If
            'Pinkal (10-Mar-2021) -- End


        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
            'DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
        script1.EnableScriptGlobalization = True

        Me.Page.ClientScript.RegisterClientScriptInclude(Me.GetType(), "Global", ResolveClientUrl("~/Help/alert/sweetalert.min.js"))
        Me.Page.ClientScript.RegisterClientScriptInclude(Me.GetType(), "Global1", ResolveClientUrl("~/Help/alert/dialogs.js"))
		Catch ex As Exception
			CommonCodes.LogErrorOnly(ex)
		End Try
    End Sub

#End Region

#Region " Radio Button Events "
    Protected Sub rdbBlue_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbBlue.CheckedChanged
        Try
        If rdbBlue.Checked = True Then
            Session("Theme") = "blue"

            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            Basepage.UpdateThemeID(enSelfServiceTheme.Blue)
            'Sohail (24 Nov 2016) -- End

            'Response.Redirect(Session("rootpath") & "UserHome.aspx", True)
            'Sohail (21 Mar 2015) -- Start
            'Enhancement - New UI Notification Link Changes.
            'Response.Redirect(Request.Url.AbsolutePath)
            Response.Redirect(Request.Url.AbsoluteUri, False)
            'Sohail (21 Mar 2015) -- End

        End If
		Catch ex As Exception
			CommonCodes.LogErrorOnly(ex)
		End Try
    End Sub

    Protected Sub rdbBlack_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbBlack.CheckedChanged
        Try
        If rdbBlack.Checked = True Then
            Session("Theme") = "black"

            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            Basepage.UpdateThemeID(enSelfServiceTheme.Black)
            'Sohail (24 Nov 2016) -- End

            'Response.Redirect(Session("rootpath") & "UserHome.aspx", True)
            'Sohail (21 Mar 2015) -- Start
            'Enhancement - New UI Notification Link Changes.
            'Response.Redirect(Request.Url.AbsolutePath)
            Response.Redirect(Request.Url.AbsoluteUri, False)
            'Sohail (21 Mar 2015) -- End
        End If
		Catch ex As Exception
			CommonCodes.LogErrorOnly(ex)
		End Try
    End Sub


    'Shani(11-Jan-2016) -- Start
    '
    Protected Sub rdbGreen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbGreen.CheckedChanged
        Try
        If rdbGreen.Checked = True Then
            Session("Theme") = "green"

            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            Basepage.UpdateThemeID(enSelfServiceTheme.Green)
            'Sohail (24 Nov 2016) -- End

            Response.Redirect(Request.Url.AbsoluteUri, False)
        End If
	    Catch ex As Exception
			CommonCodes.LogErrorOnly(ex)
		End Try
    End Sub
    Protected Sub rdbYellow_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbYellow.CheckedChanged
        Try
        If rdbYellow.Checked = True Then
            Session("Theme") = "yellow"

            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            Basepage.UpdateThemeID(enSelfServiceTheme.Yellow)
            'Sohail (24 Nov 2016) -- End

            Response.Redirect(Request.Url.AbsoluteUri, False)
        End If
		Catch ex As Exception
		CommonCodes.LogErrorOnly(ex)
		End Try
    End Sub
    'Shani(11-Jan-2016) -- End

    'Sohail (28 Mar 2018) -- Start
    'Amana bank Enhancement : Ref. No. 192 - Company logo Should display on Aruti SS,The Aruti SS theme color should be of Amana Bank in 71.1.
    Protected Sub rdbBrown_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbBrown.CheckedChanged
        Try
        If rdbBrown.Checked = True Then
            Session("Theme") = "brown"

            Basepage.UpdateThemeID(CInt(enSelfServiceTheme.Brown))

            Response.Redirect(Request.Url.AbsoluteUri, False)

        End If
		Catch ex As Exception
			CommonCodes.LogErrorOnly(ex)
		End Try
    End Sub
    'Sohail (28 Mar 2018) -- End

#End Region

    'Sohail (30 Mar 2015) -- Start
    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
#Region " Link Buttons Events "

    Protected Sub lnkMSS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMSS.Click
        Dim base As New Basepage
        Dim strError As String = ""
        Try

            If Basepage.CheckLicence("MANAGER_SELF_SERVICE") = False Then
                DisplayMessage.DisplayMessage(Basepage.m_NoLicenceMsg, Me.Page)
                Exit Sub
            End If

            If base.IsAccessGivenUserEmp(strError, Global.User.en_loginby.User, CInt(Session("U_UserID"))) = False Then
                DisplayMessage.DisplayMessage(strError, Me.Page)
                Exit Try
            End If

            SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.EMP_SELF_SERVICE, HttpContext.Current.Session("Employeeunkid"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")), HttpContext.Current.Session("IP_ADD").ToString(), HttpContext.Current.Session("HOST_NAME").ToString())
            clsActiveUserEngine.Leaved("EMP" & Session("Employeeunkid").ToString)



            Session("LoginBy") = Global.User.en_loginby.User
            Session("LangId") = Session("U_LangId")
            Session("UserId") = Session("U_UserID")
            Session("Employeeunkid") = Session("U_Employeeunkid")
            Session("UserName") = Session("U_UserName")
            Session("Password") = Session("U_Password")
            Session("LeaveBalances") = Session("U_LeaveBalances")
            Session("MemberName") = Session("U_MemberName")
            Session("RoleID") = Session("U_RoleID")
            Session("Firstname") = Session("U_Firstname")
            Session("Surname") = Session("U_Surname")
            Session("DisplayName") = Session("U_DisplayName")
            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            Session("Theme_id") = Session("U_Theme_id")
            Session("Lastview_id") = Session("U_Lastview_id")
            'Sohail (24 Nov 2016) -- End


            strError = ""
            If base.SetUserSessions(strError) = False Then
                DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Exit Try
            End If

            strError = ""
            If base.SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Exit Try
            End If


            Response.Redirect(Session("rootpath") & "UserHome.aspx", False)

        Catch ex As Exception
			CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub

    Protected Sub lnkESS_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkESS.Click
        Dim base As New Basepage
        Dim strError As String = ""
        Try

            If Basepage.CheckLicence("EMPLOYEE_SELF_SERVICE") = False Then
                DisplayMessage.DisplayMessage(Basepage.m_NoLicenceMsg, Me.Page)
                Exit Sub
            End If


            If base.IsAccessGivenUserEmp(strError, Global.User.en_loginby.Employee, CInt(Session("E_Employeeunkid"))) = False Then
                DisplayMessage.DisplayMessage(strError, Me.Page)
                Exit Try
            End If

            SetUserTracingInfo(False, Aruti.Data.enUserMode.Loging, Aruti.Data.enLogin_Mode.MGR_SELF_SERVICE, HttpContext.Current.Session("UserId"), True, HttpContext.Current.Session("mdbname"), HttpContext.Current.Session("CompanyUnkId"), CInt(HttpContext.Current.Session("Fin_year")), HttpContext.Current.Session("IP_ADD").ToString(), HttpContext.Current.Session("HOST_NAME").ToString())
            clsActiveUserEngine.Leaved("USER" & Session("UserId").ToString)


            Session("LoginBy") = Global.User.en_loginby.Employee
            Session("LangId") = 1 'Session("E_LangId")
            Session("UserId") = Session("E_UserID")
            Session("Employeeunkid") = Session("E_Employeeunkid")
            Session("UserName") = Session("E_UserName")
            Session("Password") = Session("E_Password")
            Session("LeaveBalances") = Session("E_LeaveBalances")
            Session("MemberName") = Session("E_MemberName")
            Session("RoleID") = Session("E_RoleID")
            Session("Firstname") = Session("E_Firstname")
            Session("Surname") = Session("E_Surname")
            Session("DisplayName") = Session("E_DisplayName")
            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            Session("Theme_id") = Session("E_Theme_id")
            Session("Lastview_id") = Session("E_Lastview_id")
            'Sohail (24 Nov 2016) -- End

            strError = ""
            If base.SetUserSessions(strError) = False Then
                DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Exit Try
            End If

            strError = ""
            If base.SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                Exit Try
            End If


            Response.Redirect(Session("rootpath") & "UserHome.aspx", False)

        Catch ex As Exception
			CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub

#End Region
    'Sohail (30 Mar 2015) -- End

    'Hemant (06 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
    Private Sub SetCaptions() 'SANDEEP == TRY CATCH BLOCK (2020-03-15)
        Try
            '	    Language.setLanguage(mstrModuleName)
            '	    Language._Object.setCaption(Me.mnuNewStaffRequisition.ID, Me.mnuNewStaffRequisition.Text)
            '	    Language._Object.setCaption(Me.mnuRecruitmentProcesses.ID, Me.mnuRecruitmentProcesses.Text)

            ''S.SANDEEP |26-JUL-2019| -- START
            '        Language._Object.setCaption(Me.mnuPerformanceObjectives.ID, Me.mnuPerformanceObjectives.Text)
            '		Language._Object.setCaption(Me.mnuCompanyGoals.ID, Me.mnuCompanyGoals.Text)
            '		Language._Object.setCaption(Me.mnuAllocationGoals.ID, Me.mnuAllocationGoals.Text)
            '		Language._Object.setCaption(Me.mnuEmployeeGoals.ID, Me.mnuEmployeeGoals.Text)
            '		Language._Object.setCaption(Me.mnuSetScoreCard.ID, Me.mnuSetScoreCard.Text)
            '		Language._Object.setCaption(Me.mnuMyGoals.ID, Me.mnuMyGoals.Text)
            '		Language._Object.setCaption(Me.mnuApproveRejectGoals.ID, Me.mnuApproveRejectGoals.Text)
            '		Language._Object.setCaption(Me.mnuViewPerfTemplate.ID, Me.mnuViewPerfTemplate.Text)
            '		Language._Object.setCaption(Me.mnuSubmitForApproval.ID, Me.mnuSubmitForApproval.Text)
            '		Language._Object.setCaption(Me.mnuGoalsApproval.ID, Me.mnuGoalsApproval.Text)
            '		Language._Object.setCaption(Me.mnuApproveRejectUpdateProgress.ID, Me.mnuApproveRejectUpdateProgress.Text)
            '		Language._Object.setCaption(Me.mnuUpdateProgress.ID, Me.mnuUpdateProgress.Text)
            '		Language._Object.setCaption(Me.mnuCustomHeader.ID, Me.mnuCustomHeader.Text)
            '		Language._Object.setCaption(Me.mnuCustomtems.ID, Me.mnuCustomtems.Text)
            '		Language._Object.setCaption(Me.mnuAddEditCompetencies.ID, Me.mnuAddEditCompetencies.Text)
            '		Language._Object.setCaption(Me.mnuMyCompetencies.ID, Me.mnuMyCompetencies.Text)
            '		Language._Object.setCaption(Me.mnuMyAssessment.ID, Me.mnuMyAssessment.Text)
            '		Language._Object.setCaption(Me.mnuEmployeeAssessment.ID, Me.mnuEmployeeAssessment.Text)
            '		Language._Object.setCaption(Me.mnuViewPerformanceAssessments.ID, Me.mnuViewPerformanceAssessments.Text)
            '		Language._Object.setCaption(Me.mnuCalibrateAssessment.ID, Me.mnuCalibrateAssessment.Text)
            '		Language._Object.setCaption(Me.mnuApproverLevel.ID, Me.mnuApproverLevel.Text)
            '        Language._Object.setCaption(Me.mnuApproverMaster.ID, Me.mnuApproverMaster.Text)
            '		Language._Object.setCaption(Me.mnuCalibration.ID, Me.mnuCalibration.Text)
            '		Language._Object.setCaption(Me.mnuCalibrationApproval.ID, Me.mnuCalibrationApproval.Text)
            '		Language._Object.setCaption(Me.mnuViewFinalRating.ID, Me.mnuViewFinalRating.Text)
            '            Language._Object.setCaption(Me.mnuAssignCalibrator.ID, Me.mnuAssignCalibrator.Text)
            ''S.SANDEEP |26-JUL-2019| -- END


            '            'Pinkal (10-Feb-2021) -- Start
            '            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            '            Language._Object.setCaption(Me.mnuClaimRetirement.ID, Me.mnuClaimRetirement.Text)
            '            Language._Object.setCaption(Me.mnuRetirementApproverList.ID, Me.mnuRetirementApproverList.Text)
            '            Language._Object.setCaption(Me.mnuClaimRetirementList.ID, Me.mnuClaimRetirementList.Text)
            '            Language._Object.setCaption(Me.mnuClaimRetirementApprovalList.ID, Me.mnuClaimRetirementApprovalList.Text)
            '            Language._Object.setCaption(Me.mnuClaimRetirementApprovalStatusList.ID, Me.mnuClaimRetirementApprovalStatusList.Text)
            '            Language._Object.setCaption(Me.mnuRetirementPostToPayroll.ID, Me.mnuRetirementPostToPayroll.Text)
            '            'Pinkal (10-Feb-2021) -- End

            '            'Hemant (08 Jul 2021) -- Start
            '            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            '            Language._Object.setCaption(Me.btnSearchJob.ID, Me.btnSearchJob.Text)
            '            Language._Object.setCaption(Me.btnApplicantAppliedJob.ID, Me.btnApplicantAppliedJob.Text)
            'Hemant (08 Jul 2021) -- End




            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuNewStaffRequisition.ID, Me.mnuNewStaffRequisition.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuRecruitmentProcesses.ID, Me.mnuRecruitmentProcesses.Text)

            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuPerformanceObjectives.ID, Me.mnuPerformanceObjectives.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuCompanyGoals.ID, Me.mnuCompanyGoals.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuAllocationGoals.ID, Me.mnuAllocationGoals.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuEmployeeGoals.ID, Me.mnuEmployeeGoals.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuSetScoreCard.ID, Me.mnuSetScoreCard.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuMyGoals.ID, Me.mnuMyGoals.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuApproveRejectGoals.ID, Me.mnuApproveRejectGoals.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuViewPerfTemplate.ID, Me.mnuViewPerfTemplate.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuSubmitForApproval.ID, Me.mnuSubmitForApproval.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuGoalsApproval.ID, Me.mnuGoalsApproval.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuApproveRejectUpdateProgress.ID, Me.mnuApproveRejectUpdateProgress.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuUpdateProgress.ID, Me.mnuUpdateProgress.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuCustomHeader.ID, Me.mnuCustomHeader.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuCustomtems.ID, Me.mnuCustomtems.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuAddEditCompetencies.ID, Me.mnuAddEditCompetencies.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuMyCompetencies.ID, Me.mnuMyCompetencies.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuMyAssessment.ID, Me.mnuMyAssessment.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuEmployeeAssessment.ID, Me.mnuEmployeeAssessment.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuViewPerformanceAssessments.ID, Me.mnuViewPerformanceAssessments.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuCalibrateAssessment.ID, Me.mnuCalibrateAssessment.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuApproverLevel.ID, Me.mnuApproverLevel.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuApproverMaster.ID, Me.mnuApproverMaster.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuCalibration.ID, Me.mnuCalibration.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuCalibrationApproval.ID, Me.mnuCalibrationApproval.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuViewFinalRating.ID, Me.mnuViewFinalRating.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuAssignCalibrator.ID, Me.mnuAssignCalibrator.Text)
            'S.SANDEEP |26-JUL-2019| -- END


            'Pinkal (10-Feb-2021) -- Start
            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuClaimRetirement.ID, Me.mnuClaimRetirement.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuRetirementApproverList.ID, Me.mnuRetirementApproverList.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuClaimRetirementList.ID, Me.mnuClaimRetirementList.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuClaimRetirementApprovalList.ID, Me.mnuClaimRetirementApprovalList.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuClaimRetirementApprovalStatusList.ID, Me.mnuClaimRetirementApprovalStatusList.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.mnuRetirementPostToPayroll.ID, Me.mnuRetirementPostToPayroll.Text)
            'Pinkal (10-Feb-2021) -- End

            'Hemant (08 Jul 2021) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.btnSearchJob.ID, Me.btnSearchJob.Text)
            Basepage.SetWebCaption(Session("mdbname").ToString(), mstrModuleName, Me.btnApplicantAppliedJob.ID, Me.btnApplicantAppliedJob.Text)
            'Hemant (08 Jul 2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me.Page)
        End Try
    End Sub
		
    Private Sub SetLanguage() 'SANDEEP == TRY CATCH BLOCK (2020-03-15)
        Try
            Language.setLanguage(mstrModuleName)
            '        Me.mnuRecruitment.Text = Language._Object.getCaption(Me.mnuRecruitment.ID, Me.mnuRecruitment.Text).Replace("&", "")
            '		Me.btnStaffRequisition.Text = Language._Object.getCaption(Me.btnStaffRequisition.ID, Me.btnStaffRequisition.Text).Replace("&", "")		
            '		Me.btnInterviewAnalysis.Text = Language._Object.getCaption(Me.btnInterviewAnalysis.ID, Me.btnInterviewAnalysis.Text).Replace("&", "")
            '		Me.btnFinalShortListedApplicant.Text = Language._Object.getCaption(Me.btnFinalShortListedApplicant.ID, Me.btnFinalShortListedApplicant.Text).Replace("&", "")
            '		Me.mnuNewStaffRequisition.Text = Language._Object.getCaption(Me.mnuNewStaffRequisition.ID, Me.mnuNewStaffRequisition.Text).Replace("&", "")
            '		Me.mnuRecruitmentProcesses.Text = Language._Object.getCaption(Me.mnuRecruitmentProcesses.ID, Me.mnuRecruitmentProcesses.Text).Replace("&", "")

            ''S.SANDEEP |26-JUL-2019| -- START

            ' Me.mnuAssessment.Text = Language._Object.getCaption(Me.mnuAssessment.ID, Me.mnuAssessment.Text).Replace("&", "")
            '		Me.mnuSetups.Text = Language._Object.getCaption(Me.mnuSetups.ID, Me.mnuSetups.Text).Replace("&", "")
            '		Me.btnMigration.Text = Language._Object.getCaption(Me.btnMigration.ID, Me.btnMigration.Text).Replace("&", "")
            '		Me.mnuPerformanceObjectives.Text = Language._Object.getCaption(Me.mnuPerformanceObjectives.ID, Me.mnuPerformanceObjectives.Text).Replace("&", "")
            '		Me.mnuCompanyGoals.Text = Language._Object.getCaption(Me.mnuCompanyGoals.ID, Me.mnuCompanyGoals.Text).Replace("&", "")
            '		Me.mnuAllocationGoals.Text = Language._Object.getCaption(Me.mnuAllocationGoals.ID, Me.mnuAllocationGoals.Text).Replace("&", "")
            '		Me.mnuEmployeeGoals.Text = Language._Object.getCaption(Me.mnuEmployeeGoals.ID, Me.mnuEmployeeGoals.Text).Replace("&", "")
            '		Me.mnuSetScoreCard.Text = Language._Object.getCaption(Me.mnuSetScoreCard.ID, Me.mnuSetScoreCard.Text).Replace("&", "")
            '		Me.mnuMyGoals.Text = Language._Object.getCaption(Me.mnuMyGoals.ID, Me.mnuMyGoals.Text).Replace("&", "")
            '		Me.mnuApproveRejectGoals.Text = Language._Object.getCaption(Me.mnuApproveRejectGoals.ID, Me.mnuApproveRejectGoals.Text).Replace("&", "")
            '		Me.btnPerformancePlan.Text = Language._Object.getCaption(Me.btnPerformancePlan.ID, Me.btnPerformancePlan.Text).Replace("&", "")
            '		Me.mnuViewPerfTemplate.Text = Language._Object.getCaption(Me.mnuViewPerfTemplate.ID, Me.mnuViewPerfTemplate.Text).Replace("&", "")
            '		Me.mnuSubmitForApproval.Text = Language._Object.getCaption(Me.mnuSubmitForApproval.ID, Me.mnuSubmitForApproval.Text).Replace("&", "")
            '		Me.mnuGoalsApproval.Text = Language._Object.getCaption(Me.mnuGoalsApproval.ID, Me.mnuGoalsApproval.Text).Replace("&", "")
            '		Me.mnuApproveRejectUpdateProgress.Text = Language._Object.getCaption(Me.mnuApproveRejectUpdateProgress.ID, Me.mnuApproveRejectUpdateProgress.Text).Replace("&", "")
            '		Me.mnuUpdateProgress.Text = Language._Object.getCaption(Me.mnuUpdateProgress.ID, Me.mnuUpdateProgress.Text).Replace("&", "")
            '		Me.mnuCustomHeader.Text = Language._Object.getCaption(Me.mnuCustomHeader.ID, Me.mnuCustomHeader.Text).Replace("&", "")
            '		Me.mnuCustomtems.Text = Language._Object.getCaption(Me.mnuCustomtems.ID, Me.mnuCustomtems.Text).Replace("&", "")
            '		Me.btnCompetencies.Text = Language._Object.getCaption(Me.btnCompetencies.ID, Me.btnCompetencies.Text).Replace("&", "")
            '		Me.mnuAddEditCompetencies.Text = Language._Object.getCaption(Me.mnuAddEditCompetencies.ID, Me.mnuAddEditCompetencies.Text).Replace("&", "")
            '		Me.mnuMyCompetencies.Text = Language._Object.getCaption(Me.mnuMyCompetencies.ID, Me.mnuMyCompetencies.Text).Replace("&", "")
            '		Me.mnuPerformaceEvaluation.Text = Language._Object.getCaption(Me.mnuPerformaceEvaluation.ID, Me.mnuPerformaceEvaluation.Text).Replace("&", "")
            '		Me.mnuMyAssessment.Text = Language._Object.getCaption(Me.mnuMyAssessment.ID, Me.mnuMyAssessment.Text).Replace("&", "")
            '		Me.mnuEmployeeAssessment.Text = Language._Object.getCaption(Me.mnuEmployeeAssessment.ID, Me.mnuEmployeeAssessment.Text).Replace("&", "")
            '		Me.btnAssessorPEvaluation.Text = Language._Object.getCaption(Me.btnAssessorPEvaluation.ID, Me.btnAssessorPEvaluation.Text).Replace("&", "")
            '		Me.btnReviewerPEvaluation.Text = Language._Object.getCaption(Me.btnReviewerPEvaluation.ID, Me.btnReviewerPEvaluation.Text).Replace("&", "")
            '		Me.mnuViewPerformanceAssessments.Text = Language._Object.getCaption(Me.mnuViewPerformanceAssessments.ID, Me.mnuViewPerformanceAssessments.Text).Replace("&", "")
            '		Me.mnuCalibrateAssessment.Text = Language._Object.getCaption(Me.mnuCalibrateAssessment.ID, Me.mnuCalibrateAssessment.Text).Replace("&", "")
            '		Me.mnuApproverLevel.Text = Language._Object.getCaption(Me.mnuApproverLevel.ID, Me.mnuApproverLevel.Text).Replace("&", "")
            '		Me.mnuApproverMaster.Text = Language._Object.getCaption(Me.mnuApproverMaster.ID, Me.mnuApproverMaster.Text).Replace("&", "")
            '		Me.mnuCalibration.Text = Language._Object.getCaption(Me.mnuCalibration.ID, Me.mnuCalibration.Text).Replace("&", "")
            '		Me.mnuCalibrationApproval.Text = Language._Object.getCaption(Me.mnuCalibrationApproval.ID, Me.mnuCalibrationApproval.Text).Replace("&", "")
            '		Me.mnuViewFinalRating.Text = Language._Object.getCaption(Me.mnuViewFinalRating.ID, Me.mnuViewFinalRating.Text).Replace("&", "")

            ''S.SANDEEP |26-JUL-2019| -- END 


            '            'Pinkal (10-Feb-2021) -- Start  
            '            'Enhancement/Bug Retirement -   Working Retirement enhancement/Bug.
            '            Me.mnuClaimRetirement.Text = Language._Object.getCaption(Me.mnuClaimRetirement.ID, Me.mnuClaimRetirement.Text).Replace("&", "")
            '            Me.mnuRetirementApproverList.Text = Language._Object.getCaption(Me.mnuRetirementApproverList.ID, Me.mnuRetirementApproverList.Text).Replace("&", "")
            '            Me.mnuClaimRetirementList.Text = Language._Object.getCaption(Me.mnuClaimRetirementList.ID, Me.mnuClaimRetirementList.Text).Replace("&", "")
            '            Me.mnuClaimRetirementApprovalList.Text = Language._Object.getCaption(Me.mnuClaimRetirementApprovalList.ID, Me.mnuClaimRetirementApprovalList.Text).Replace("&", "")
            '            Me.mnuClaimRetirementApprovalStatusList.Text = Language._Object.getCaption(Me.mnuClaimRetirementApprovalStatusList.ID, Me.mnuClaimRetirementApprovalStatusList.Text).Replace("&", "")
            '            Me.mnuRetirementPostToPayroll.Text = Language._Object.getCaption(Me.mnuRetirementPostToPayroll.ID, Me.mnuRetirementPostToPayroll.Text).Replace("&", "")


            '            'Pinkal (10-Feb-2021) -- End
            '            'Hemant (08 Jul 2021) -- Start
            '            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            '            Me.btnSearchJob.Text = Language._Object.getCaption(Me.btnSearchJob.ID, Me.btnSearchJob.Text).Replace("&", "")
            '            Me.btnApplicantAppliedJob.Text = Language._Object.getCaption(Me.btnApplicantAppliedJob.ID, Me.btnApplicantAppliedJob.Text).Replace("&", "")
            '            'Hemant (08 Jul 2021) -- End

            Me.mnuStaffRequisitionApproval.Text = Language._Object.getCaption(Me.mnuStaffRequisitionApproval.ID, Me.mnuStaffRequisitionApproval.Text).Replace("&", "")

            Me.mnuRefNoApproval.Text = Language._Object.getCaption(Me.mnuRefNoApproval.ID, Me.mnuRefNoApproval.Text).Replace("&", "")


            Me.mnuRecruitment.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuRecruitment.ID, Me.mnuRecruitment.Text).Replace("&", "")
            Me.btnStaffRequisition.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.btnStaffRequisition.ID, Me.btnStaffRequisition.Text).Replace("&", "")
            Me.btnInterviewAnalysis.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.btnInterviewAnalysis.ID, Me.btnInterviewAnalysis.Text).Replace("&", "")
            Me.btnFinalShortListedApplicant.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.btnFinalShortListedApplicant.ID, Me.btnFinalShortListedApplicant.Text).Replace("&", "")
            Me.mnuNewStaffRequisition.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuNewStaffRequisition.ID, Me.mnuNewStaffRequisition.Text).Replace("&", "")
            Me.mnuRecruitmentProcesses.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuRecruitmentProcesses.ID, Me.mnuRecruitmentProcesses.Text).Replace("&", "")

            'S.SANDEEP |26-JUL-2019| -- START
            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString().ToUpper() <> "TRA" Then
                Me.mnuAssessment.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuAssessment.ID, Me.mnuAssessment.Text).Replace("&", "")
                Me.mnuSetups.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuSetups.ID, Me.mnuSetups.Text).Replace("&", "")
                Me.btnMigration.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.btnMigration.ID, Me.btnMigration.Text).Replace("&", "")
                Me.mnuPerformanceObjectives.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuPerformanceObjectives.ID, Me.mnuPerformanceObjectives.Text).Replace("&", "")
                Me.mnuCompanyGoals.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuCompanyGoals.ID, Me.mnuCompanyGoals.Text).Replace("&", "")
                Me.mnuAllocationGoals.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuAllocationGoals.ID, Me.mnuAllocationGoals.Text).Replace("&", "")
                Me.mnuEmployeeGoals.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuEmployeeGoals.ID, Me.mnuEmployeeGoals.Text).Replace("&", "")
                Me.mnuSetScoreCard.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuSetScoreCard.ID, Me.mnuSetScoreCard.Text).Replace("&", "")
                Me.mnuMyGoals.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuMyGoals.ID, Me.mnuMyGoals.Text).Replace("&", "")
                Me.mnuApproveRejectGoals.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuApproveRejectGoals.ID, Me.mnuApproveRejectGoals.Text).Replace("&", "")
                Me.btnPerformancePlan.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.btnPerformancePlan.ID, Me.btnPerformancePlan.Text).Replace("&", "")
                Me.mnuViewPerfTemplate.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuViewPerfTemplate.ID, Me.mnuViewPerfTemplate.Text).Replace("&", "")
                Me.mnuSubmitForApproval.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuSubmitForApproval.ID, Me.mnuSubmitForApproval.Text).Replace("&", "")
                Me.mnuGoalsApproval.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuGoalsApproval.ID, Me.mnuGoalsApproval.Text).Replace("&", "")
                Me.mnuApproveRejectUpdateProgress.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuApproveRejectUpdateProgress.ID, Me.mnuApproveRejectUpdateProgress.Text).Replace("&", "")
                Me.mnuUpdateProgress.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuUpdateProgress.ID, Me.mnuUpdateProgress.Text).Replace("&", "")
                Me.mnuCustomHeader.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuCustomHeader.ID, Me.mnuCustomHeader.Text).Replace("&", "")
                Me.mnuCustomtems.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuCustomtems.ID, Me.mnuCustomtems.Text).Replace("&", "")
                Me.btnCompetencies.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.btnCompetencies.ID, Me.btnCompetencies.Text).Replace("&", "")
                Me.mnuAddEditCompetencies.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuAddEditCompetencies.ID, Me.mnuAddEditCompetencies.Text).Replace("&", "")
                Me.mnuMyCompetencies.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuMyCompetencies.ID, Me.mnuMyCompetencies.Text).Replace("&", "")
                Me.mnuPerformaceEvaluation.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuPerformaceEvaluation.ID, Me.mnuPerformaceEvaluation.Text).Replace("&", "")
                Me.mnuMyAssessment.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuMyAssessment.ID, Me.mnuMyAssessment.Text).Replace("&", "")
                Me.mnuEmployeeAssessment.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuEmployeeAssessment.ID, Me.mnuEmployeeAssessment.Text).Replace("&", "")
                Me.btnAssessorPEvaluation.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.btnAssessorPEvaluation.ID, Me.btnAssessorPEvaluation.Text).Replace("&", "")
                Me.btnReviewerPEvaluation.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.btnReviewerPEvaluation.ID, Me.btnReviewerPEvaluation.Text).Replace("&", "")
                Me.mnuViewPerformanceAssessments.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuViewPerformanceAssessments.ID, Me.mnuViewPerformanceAssessments.Text).Replace("&", "")
                Me.mnuCalibrateAssessment.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuCalibrateAssessment.ID, Me.mnuCalibrateAssessment.Text).Replace("&", "")
                Me.mnuApproverLevel.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuApproverLevel.ID, Me.mnuApproverLevel.Text).Replace("&", "")
                Me.mnuApproverMaster.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuApproverMaster.ID, Me.mnuApproverMaster.Text).Replace("&", "")
                Me.mnuCalibration.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuCalibration.ID, Me.mnuCalibration.Text).Replace("&", "")
                Me.mnuCalibrationApproval.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuCalibrationApproval.ID, Me.mnuCalibrationApproval.Text).Replace("&", "")
                Me.mnuViewFinalRating.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuViewFinalRating.ID, Me.mnuViewFinalRating.Text).Replace("&", "")
            End If            
            'S.SANDEEP |26-JUL-2019| -- END 


            Me.mnuClaimRetirement.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuClaimRetirement.ID, Me.mnuClaimRetirement.Text).Replace("&", "")
            Me.mnuRetirementApproverList.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuRetirementApproverList.ID, Me.mnuRetirementApproverList.Text).Replace("&", "")
            Me.mnuClaimRetirementList.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuClaimRetirementList.ID, Me.mnuClaimRetirementList.Text).Replace("&", "")
            Me.mnuClaimRetirementApprovalList.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuClaimRetirementApprovalList.ID, Me.mnuClaimRetirementApprovalList.Text).Replace("&", "")
            Me.mnuClaimRetirementApprovalStatusList.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuClaimRetirementApprovalStatusList.ID, Me.mnuClaimRetirementApprovalStatusList.Text).Replace("&", "")
            Me.mnuRetirementPostToPayroll.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuRetirementPostToPayroll.ID, Me.mnuRetirementPostToPayroll.Text).Replace("&", "")



            Me.btnSearchJob.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.btnSearchJob.ID, Me.btnSearchJob.Text).Replace("&", "")
            Me.btnApplicantAppliedJob.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.btnApplicantAppliedJob.ID, Me.btnApplicantAppliedJob.Text).Replace("&", "")


            Me.mnuStaffRequisitionApproval.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuStaffRequisitionApproval.ID, Me.mnuStaffRequisitionApproval.Text).Replace("&", "")

            Me.mnuRefNoApproval.Text = Basepage.SetWebLanguage(Session("mdbname").ToString(), mstrModuleName, CInt(Session("LangId")), Me.mnuRefNoApproval.ID, Me.mnuRefNoApproval.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me.Page)
        End Try
    End Sub
    'Hemant (06 Aug 2019) -- End
End Class

