﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgApplyTrainingRequisition.aspx.vb" Inherits="Training_Requisition_wPgApplyTrainingRequisition" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DReason" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"
        type="text/javascript"></script>

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script>
        function FileUploadChangeEvent() {
            var fupld = document.getElementById('<%= flUpload.ClientID %>' + '_image_file');
            if (fupld != null)
                fileUpLoadChange();

            var fupcld = document.getElementById('<%= flcUpload.ClientID %>' + '_image_file');
            if (fupcld != null)
                fileUpLoadChange();
        }
    </script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>
    
    <script>
        function IsValidAttachDoc() {
            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {

            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13 || charCode == 47)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            NumberOnly()
        });
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(NumberOnly);

        function NumberOnly() {
            $(".numberonly").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        }
    </script>

    <script>

        $(document).ready(function() {
            ImageLoad();
        });

        function ImageLoad() {
            debugger;
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#cfileuploader").uploadFile({
                    url: "wPgApplyTrainingRequisition.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
                    dragDropStr: "",
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        debugger;
                        $("#<%= btnSaveCAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });

                $("#fileuploader").uploadFile({
                    url: "wPgApplyTrainingRequisition.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
                    dragDropStr: "",
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        debugger;
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $('input[type=file]').live("click", function() {
            if ($(this).attr("class") != "flupload") {
                debugger;
                return IsValidAttach();
            }
        });
    </script>

    <script>
        function IsValidAttach() {
            return true;
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Training Request"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Training Request Form"></asp:Label>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlPart1" runat="server" Width="100%">
                                    <div id="FilterCriteriaBody" class="panel-body-default">
                                        <div class="row2">
                                            <div id="lblName" style="width: 6%;" class="ib">
                                                <asp:Label ID="lblEmpName" runat="server" Text="Employee"></asp:Label>
                                            </div>
                                            <div style="width: 32%;" class="ib">
                                                <asp:DropDownList ID="drpEmpName" Width="367px" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                            <div id="PAOpt" style="width: 14%;" class="ib">
                                                <asp:Label ID="lblOpt1" runat="server" Text="Is Training Aligned with your current role?"></asp:Label>
                                            </div>
                                            <div style="width: 10%;" class="ib">
                                                <asp:RadioButtonList ID="radYes" runat="server" RepeatDirection="Horizontal" Width="100%"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                    <asp:ListItem Value="0">No</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                            <div id="divPA" style="width: 7%;" class="ib">
                                                <asp:Label ID="lblPAPeriod" runat="server" Text="Performance Period"></asp:Label>
                                            </div>
                                            <div style="width: 23%;" class="ib">
                                                <asp:DropDownList ID="drpPeriod" Width="268px" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div style="width: 6%;" class="ib">
                                                <asp:Label ID="lblJob" runat="server" Text="Title" Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 32%;" class="ib">
                                                <asp:TextBox ID="txtJob" runat="server" Width="100%"></asp:TextBox>
                                            </div>
                                            <div style="width: 13%;" class="ib">
                                                <asp:Label ID="lblDepartment" runat="server" Text="Department" Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 44%;" class="ib">
                                                <asp:TextBox ID="txtDepartment" runat="server" Width="100%"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="Div4" class="panel-default">
                                <div id="TrInfo" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="TrOptions" runat="server" Text="Training Options"></asp:Label>
                                    </div>
                                </div>
                                <asp:Panel ID="pnlPart2" runat="server" Width="100%">
                                    <div id="Div1" class="panel-body-default">
                                        <div class="row2">
                                            <div id="lblTraName" style="width: 8%;" class="ib">
                                                <asp:Label ID="TrName" runat="server" Text="Training Name"></asp:Label>
                                            </div>
                                            <div style="width: 34%;" class="ib">
                                                <asp:DropDownList ID="drpTrainingCourse" Width="397Px" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 7%;" class="ib">
                                                <asp:Label ID="lblIntName" runat="server" Text="Vendor" Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 36%;" class="ib">
                                                <asp:DropDownList ID="DrpVendorName" Width="534Px" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div id="Mode" style="width: 8%;" class="ib">
                                                <asp:Label ID="lblMode" runat="server" Text="Training Mode"></asp:Label>
                                            </div>
                                            <div style="width: 13%;" class="ib">
                                                <asp:DropDownList ID="drpTrainingMode" Width="145Px" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                            <div id="Div10" style="width: 6%;" class="ib">
                                                <asp:Label ID="lblapplydate" runat="server" Text="Apply Date"></asp:Label>
                                            </div>
                                            <div style="width: 13%;" class="ibwm">
                                                <uc3:DateCtrl ID="dtapplydate" runat="server" Enabled="false" />
                                            </div>
                                            <div style="width: 7%;" class="ib">
                                                <asp:Label ID="lblListTrDatefrom" runat="server" Text="Training Date From" Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 12%;" class="ib">
                                                <uc3:DateCtrl ID="dttrainingFromdate" runat="server" AutoPostBack="true" />
                                            </div>
                                            <div style="width: 2%;" class="ib">
                                                <asp:Label ID="lblListTrDateto" runat="server" Text="To " Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 15%;" class="ib">
                                                <uc3:DateCtrl ID="dttrainingTodate" runat="server" />
                                            </div>
                                            <div style="width: 5%;" class="ib">
                                                <asp:Label ID="lblDuration" runat="server" Text="Duration" Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 7%;" class="ib">
                                                <asp:TextBox ID="txtDuration" runat="server" Width="100%" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div style="width: 8%;" class="ib">
                                                <asp:Label ID="lblVenue" runat="server" Text="Venue Name" Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 13%;" class="ib no-padding">
                                                <asp:TextBox ID="txtVenue" runat="server" Width="100%" TextMode="MultiLine" Rows="3"></asp:TextBox> 
                                            </div>
                                            <%--'S.SANDEEP [07-NOV-2018] -- START--%>
                                            <div id="Div17" style="width: 6%;" class="ib">
                                                <asp:Label ID="lblTrainingtype" runat="server" Text="Training type"></asp:Label>
                                            </div>
                                            <div style="width: 13%;" class="ibwm">
                                                <asp:DropDownList ID="drpTrainingType" runat="server" ></asp:DropDownList>
                                            </div>
                                            <%--'S.SANDEEP [07-NOV-2018] -- END--%>
                                            <div style="width: 7%;" class="ib">
                                                <asp:Label ID="LblReqRemark" runat="server" Text="Requisition Remark" Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 46%;" class="ib no-padding">
                                                <asp:TextBox ID="txtReqRemark" runat="server" TextMode="MultiLine" Rows="3" Width="100%"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="Div2" class="panel-default">
                                <div id="Div3" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblOtherInformation" runat="server" Text="Other Training Data"></asp:Label>
                                    </div>
                                </div>
                                <div id="Div7" class="panel-body-default">
                                    <div class="row2">
                                        <cc1:TabContainer ID="tabcOtherInformation" runat="server" ActiveTabIndex="0" Width="99%"
                                            Height="350px">
                                            <cc1:TabPanel runat="server" HeaderText="Other Information" ID="tabpOtherInfo">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlPart3" runat="server" Width="100%" Height="100%">
                                                        <div class="row2">
                                                            <div id="Div5" class="ib" style="width: 100%;">
                                                                <asp:Label ID="LblOthTrainings" runat="server" Text="Have you attended any other training/trainings for the past one (1) year?"></asp:Label>
                                                            </div>
                                                            <div class="ib" style="width: 100%; height: 45px; vertical-align: top">
                                                                <div class="ib" style="width: 9%;">
                                                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" RepeatDirection="Horizontal"
                                                                        Width="100%">
                                                                        <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                        <asp:ListItem Value="0">No</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </div>
                                                                <div id="div6" class="ib" style="width: 9%;">
                                                                    <asp:Label ID="LblTrMode1" runat="server" Text="Training Mode"></asp:Label>
                                                                </div>
                                                                <div class="ib" style="width: 21%;">
                                                                    <asp:DropDownList ID="drpOtherTrainingMode" runat="server" AutoPostBack="True" Width="194px">
                                                                    </asp:DropDownList>
                                                                </div>
                                                                <div class="ib" style="width: 6%;">
                                                                    <asp:Label ID="LblTrainings" runat="server" Text=" Trainings" Width="100%"></asp:Label>
                                                                </div>
                                                                <div class="ib" style="width: 36%;">
                                                                    <asp:TextBox ID="txtTrRemark" runat="server" Rows="2" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                                                </div>
                                                                <div class="ib">
                                                                    <asp:Button ID="btnAddOthTraining" runat="server" CssClass="btndefault" Text="Add"
                                                                        Width="100%" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row2">
                                                            <div id="scrollable-container" style="width: 100%; height: 228px; overflow: auto">
                                                                <asp:GridView ID="gvOtherTrainingList" runat="server" AutoGenerateColumns="False"
                                                                    CssClass="gridview" Width="100%">
                                                                    <Columns>
                                                                        <asp:TemplateField FooterText="colhedit">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkedit" runat="server" CommandArgument='<%#Eval("tranguid")%>'
                                                                                    CssClass="gridedit" ToolTip="Edit">
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <ItemStyle VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField FooterText="colhdelete">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%#Eval("tranguid")%>'
                                                                                    CssClass="griddelete" OnClick="lnkOtherTrainingdelete_Click" ToolTip="Delete">
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                            <ItemStyle VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="trainingmode" FooterText="colhoTrainingMode" HeaderText="Training Mode">
                                                                            <ItemStyle VerticalAlign="Top" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="item_description" FooterText="colhoTrainingName" HeaderText="Other Training">
                                                                            <ItemStyle VerticalAlign="Top" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                    <RowStyle CssClass="griviewitem" />
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel runat="server" HeaderText="Requisition Resources" ID="tabpResourceInfo">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlPart4" runat="server" Width="100%" Height="100%">
                                                        <div class="row2">
                                                            <div id="Div11" style="width: 15%;" class="ib">
                                                                <asp:Label ID="LblReqRes" runat="server" Text="Requisition Resources"></asp:Label>
                                                            </div>
                                                            <div style="width: 26%;" class="ib">
                                                                <asp:DropDownList ID="drpTrainingResources" Width="295px" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div style="width: 11%;" class="ib">
                                                                <asp:Label ID="LblResRemark" runat="server" Text="Resource Remark" Width="100%"></asp:Label>
                                                            </div>
                                                            <div style="width: 30%;" class="ib">
                                                                <asp:TextBox ID="txtResRemark" runat="server" TextMode="MultiLine" Rows="2" Width="100%"></asp:TextBox>
                                                            </div>
                                                            <div class="ib">
                                                                <asp:Button ID="btnAddResources" runat="server" Text="Add" CssClass="btndefault"
                                                                    Width="100%"></asp:Button>
                                                            </div>
                                                        </div>
                                                        <div class="row2">
                                                            <div id="Div13" style="width: 100%; height: 242px; overflow: auto">
                                                                <asp:GridView ID="gvResourcesList" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" ShowFooter="false">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhedit">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkedit" runat="server" CssClass="gridedit" CommandArgument='<%#Eval("tranguid")%>'
                                                                                    ToolTip="Edit" OnClick="lnkResourceEdit_Click">
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhdelete">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                                                    CommandArgument='<%#Eval("tranguid")%>' OnClick="lnkResourceDelete_Click">
                                                                                </asp:LinkButton>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField HeaderText="Training Resource" DataField="resourcetype" ItemStyle-VerticalAlign="Top"
                                                                            FooterText="colhResourcetype" />
                                                                        <asp:BoundField HeaderText="Resource Description" DataField="item_description" ItemStyle-VerticalAlign="Top"
                                                                            FooterText="colhResourceDescription" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel runat="server" HeaderText="Training Costing" ID="tabpCosting">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlPart7" runat="server" Width="100%" Height="100%">
                                                        <div class="row2">
                                                            <div class="ib" style="width: 25%">
                                                                <div class="row2">
                                                                    <div style="width: 33%" class="ib">
                                                                        <asp:Label ID="lblClaimNo" runat="server" Text="Claim No."></asp:Label>
                                                                    </div>
                                                                    <div style="width: 44%" class="ib">
                                                                        <asp:TextBox ID="txtClaimNo" runat="server"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div style="width: 33%" class="ib">
                                                                        <asp:Label ID="lblClaimDate" runat="server" Text="Claim Date"></asp:Label>
                                                                    </div>
                                                                    <div style="width: 58%" class="ib">
                                                                        <uc3:DateCtrl ID="dtpClaimDate" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="ib" style="width: 35%">
                                                                <div style="width: 99%; vertical-align: top">
                                                                    <asp:Label ID="lblClaimRemark" runat="server" Text="Claim Remark"></asp:Label></div>
                                                                <div style="width: 95%">
                                                                    <asp:TextBox ID="txtClaimRemark" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox></div>
                                                            </div>
                                                            <div class="ib" style="width: 35%">
                                                                <div style="width: 99%; vertical-align: top">
                                                                    <asp:Label ID="lblDomicileAddress" runat="server" Text="Domiclie Address"></asp:Label></div>
                                                                <div style="width: 99%">
                                                                    <asp:TextBox ID="txtDomicileAddress" runat="server" ReadOnly="True" TextMode="MultiLine"
                                                                        Rows="3"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row2">
                                                            <div class="ib" style="width: 25%; border-right: 1px solid;">
                                                                <div class="row2">
                                                                    <div style="width: 26%" class="ib">
                                                                        <asp:Label ID="lblExpense" runat="server" Text="Expense"></asp:Label></div>
                                                                    <div style="width: 65%" class="ib">
                                                                        <asp:DropDownList ID="cboExpense" Width="178px" runat="server" AutoPostBack="True">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div style="width: 26%" class="ib">
                                                                        <asp:Label ID="lblUoMType" runat="server" Text="UoM Type"></asp:Label></div>
                                                                    <div style="width: 33%" class="ib">
                                                                        <asp:TextBox ID="txtUoMType" runat="server" ReadOnly="True"></asp:TextBox></div>
                                                                    <div style="width: 7%" class="ib">
                                                                        <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label></div>
                                                                    <div style="width: 12%" class="ib">
                                                                        <asp:TextBox ID="txtQty" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div style="width: 26%" class="ib">
                                                                        <asp:Label ID="lblSectorRoute" runat="server" Text="Sector / Route" Width="100%"></asp:Label>
                                                                    </div>
                                                                    <div style="width: 65%" class="ib">
                                                                        <asp:DropDownList ID="cboSecRoute" Width="178px" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div style="width: 26%" class="ib">
                                                                        <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label></div>
                                                                    <div style="width: 65%" class="ib">
                                                                        <asp:TextBox ID="txtUnitPrice" Width="97%" runat="server" Style="text-align: right"
                                                                            onKeypress="return onlyNumbers(this, event);"></asp:TextBox></div>
                                                                </div>
                                                                <div class="row2">
                                                                    <div style="width: 26%" class="ib">
                                                                        <asp:Label ID="lblExpenseRemark" runat="server" Text="Expense Remark"></asp:Label></div>
                                                                    <div style="width: 65%" class="ib no-padding">
                                                                        <asp:TextBox ID="txtExpenseRemark" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox></div>
                                                                </div>
                                                                <div class="row2">
                                                                    <asp:Button ID="btnAddExpense" runat="server" Text="Add" class="btndefault" />
                                                                </div>
                                                            </div>
                                                            <div class="ib" style="width: 71%; vertical-align: top; height: 265px;">
                                                                <div class="row2">
                                                                    <asp:Panel ID="plnCostingData" runat="server" Width="100%" Height="207px">
                                                                        <asp:GridView ID="gvCosting" runat="server" AutoGenerateColumns="False" Width="100%"
                                                                            CssClass="gridview" DataKeyNames="GUID,crtranunkid,crmasterunkid">
                                                                            <Columns>
                                                                                <asp:TemplateField FooterText="colhedit">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkCEdit" runat="server" OnClick="lnkEditExpense_Click" CssClass="gridedit"
                                                                                            CommandArgument='<%#Eval("crtranunkid")%>' ToolTip="Edit"></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle VerticalAlign="Top" />
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField FooterText="objcolhAttachment" HeaderStyle-HorizontalAlign="Center"
                                                                                    HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="imgView" runat="server" CommandArgument='<%#Eval("crtranunkid")%>'
                                                                                            ToolTip="Attachment" OnClick="lnkClaimAttach_Click">
                                                                                    <i class="fa fa-paperclip" aria-hidden="true" style="font-size:20px;font-weight:bold"></i>
                                                                                        </asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField FooterText="colhdelete">
                                                                                    <ItemTemplate>
                                                                                        <asp:LinkButton ID="lnkCDelete" runat="server" OnClick="lnkDeleteExpense_Click" CssClass="griddelete"
                                                                                            ToolTip="Delete" CommandArgument='<%#Eval("crtranunkid")%>'></asp:LinkButton>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle VerticalAlign="Top" />
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="expense" FooterText="dgcolhExpense" HeaderText="Claim/Expense Desc" />
                                                                                <asp:BoundField DataField="sector" FooterText="dgcolhSectorRoute" HeaderText="Sector / Route" />
                                                                                <asp:BoundField DataField="uom" FooterText="dgcolhUoM" HeaderText="UoM" />
                                                                                <asp:BoundField DataField="quantity" FooterText="dgcolhQty" HeaderText="Quantity">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="unitprice" FooterText="dgcolhUnitPrice" HeaderText="Unit Price">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="amount" FooterText="dgcolhAmount" HeaderText="Amount">
                                                                                    <HeaderStyle HorizontalAlign="Right" />
                                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                                </asp:BoundField>
                                                                            </Columns>
                                                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                                            <RowStyle CssClass="griviewitem" />
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </div>
                                                                <div class="row2">
                                                                    <div class="ib" style="width: 76%">
                                                                        <asp:Label ID="lblTotalCosting" runat="server" Text="Total" Font-Bold="True" Font-Size="Large"></asp:Label>
                                                                    </div>
                                                                    <div class="ib" style="width: 20%">
                                                                        <asp:TextBox ID="txtTotalCosting" ReadOnly="True" runat="server" Style="text-align: right"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel runat="server" HeaderText="Attach Document" ID="tabpDocuments">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlPart6" runat="server" Width="100%" Height="100%">
                                                        <div style="width: 11%;" class="ib">
                                                            <asp:Label ID="lblDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                        </div>
                                                        <div style="width: 23%;" class="ib">
                                                            <asp:DropDownList ID="cboDocumentType" runat="server" Width="250px">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div style="width: 38%; vertical-align: bottom;" class="ib">
                                                            <asp:UpdatePanel ID="UPUpload" runat="server" Visible="False">
                                                                <ContentTemplate>
                                                                    <uc9:FileUpload ID="flUpload" runat="server" />
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            <asp:Panel ID="pnl_ImageAdd" runat="server" Width="100%">
                                                                <div id="fileuploader">
                                                                    <input type="button" id="btnAddFile" runat="server" class="btndefault" onclick="return IsValidAttach()"
                                                                        value="Save Attachment" />
                                                                </div>
                                                            </asp:Panel>
                                                            <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" />
                                                        </div>
                                                    </asp:Panel>
                                                    <div style="width: 100%; max-height: 200px; overflow: auto">
                                                        <asp:GridView ID="gvGrievanceAttachment" runat="server" AutoGenerateColumns="False"
                                                            Width="99%" CssClass="gridview" ItemStyle-CssClass="griviewitem" DataKeyNames="GUID,scanattachtranunkid">
                                                            <Columns>
                                                                <asp:TemplateField FooterText="objcohDelete">
                                                                    <ItemTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="RemoveAttachment"
                                                                                ToolTip="Delete"></asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="25px" />
                                                                    <ItemStyle Width="25px" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField FooterText="objcohdownload" HeaderStyle-Width="25px" ItemStyle-Width="25px"
                                                                    Visible="false">
                                                                    <ItemTemplate>
                                                                        <span class="gridiconbc">
                                                                            <asp:LinkButton ID="DownloadLink" runat="server" ToolTip="Download" CommandArgument='<%#Eval("scanattachtranunkid") %>'
                                                                                OnClick="lnkdownloadAttachment_Click">
                                                                                   <i class="fa fa-download"></i>
                                                                            </asp:LinkButton>
                                                                        </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                <asp:BoundField HeaderText="File Size" DataField="filesize" FooterText="colhSize">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                        </asp:GridView>
                                                    </div>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                        </cc1:TabContainer>
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="pnlPart5" runat="server" Visible="false">
                                <div id="Div15" class="panel-default">
                                    <div class="panel-heading-default">
                                        <asp:Label ID="lblApprovalData" runat="server" Text="Approval Info"></asp:Label>
                                    </div>
                                    <div id="Div12" class="panel-body-default">
                                        <div class="row2">
                                            <div id="Div14" style="width: 6%; vertical-align: top" class="ib">
                                                <asp:Label ID="lblApprover" runat="server" Text="Approver"></asp:Label>
                                            </div>
                                            <div style="width: 32%;" class="ib">
                                                <asp:DropDownList ID="drpApprover" Width="367px" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <div id="Div16" style="width: 6%; vertical-align: top" class="ib">
                                                <asp:Label ID="lblLevel" runat="server" Text="Level"></asp:Label>
                                            </div>
                                            <div style="width: 15%;" class="ib">
                                                <asp:TextBox ID="txtApproverLevel" runat="server" ReadOnly="true" Width="100%"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div style="width: 6%; vertical-align: top" class="ib">
                                                <asp:Label ID="lblApprRejectRemark" runat="server" Text="Remark" Width="99%"></asp:Label>
                                            </div>
                                            <div style="width: 60%;" class="ib">
                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="3" Width="92%"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="panel-footer">
                            <div id="ApproveDisapprove" runat="server" style="float: left">
                                <asp:Button ID="btnApprove" runat="server" Text="Approve" class="btndefault" Visible="false" />
                                <asp:Button ID="btnDisapprove" runat="server" Text="Reject" class="btndefault" Visible="false" />
                            </div>
                            <asp:Button ID="btnSave" runat="server" Text="Save" class="btndefault" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" class="btndefault" />
                        </div>
                    </div>
                    <uc1:DReason ID="popupDelete" runat="server" Title="Enter Reason to delete" />
                    <uc2:Cnf_YesNo ID="cnfApprovDisapprove" runat="server" Title="Aruti" Message="" />
                    <uc2:Cnf_YesNo ID="cnfDeleteImage" runat="server" Title="Aruti" Message="Are you sure you want to remove attached document?" />
                    <uc2:Cnf_YesNo ID="cnfDeleteOtraining" runat="server" Title="Aruti" Message="Are you sure you want to remove added other training?" />
                    <uc2:Cnf_YesNo ID="cnfDeleteOResources" runat="server" Title="Aruti" Message="Are you sure you want to remove added other resources?" />
                    <uc2:Cnf_YesNo ID="cnfUnitPrice" runat="server" Title="Aruti" />
                    <uc2:Cnf_YesNo ID="CnfCosting" runat="server" Title="Aruti" Message="Are you sure you want to remove added other resources?" />
                    <uc2:Cnf_YesNo ID="cnfDeleteCostingFile" runat="server" Title="Aruti" Message="Are you sure you want to delete added file?" />
                    <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" TargetControlID="hdf_ScanAttchment">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="newpopup" Width="600px"
                        Style="display: none;">
                        <div class="panel-primary" style="margin: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div8" class="panel-default">
                                    <div id="Div9" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 30%">
                                                    <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                </td>
                                                <td style="width: 40%">
                                                    <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server"
                                                        Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 30%">
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" Visible="False">
                                                        <ContentTemplate>
                                                            <uc9:FileUpload ID="flcUpload" runat="server" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                    <asp:Panel ID="pnl_cImageAdd" runat="server" Width="100%">
                                                        <div id="cfileuploader">
                                                            <input type="button" id="btnCAddFile" runat="server" class="btndefault" onclick="return IsValidAttachDoc()"
                                                                value="Save Attachment" />
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Button ID="btnSaveCAttachment" runat="server" Style="display: none" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td colspan="3" style="width: 100%">
                                                    <asp:GridView ID="dgv_Attchment" runat="server" AutoGenerateColumns="False" Width="99%"
                                                        CssClass="gridview" ItemStyle-CssClass="griviewitem" DataKeyNames="scanattachtranunkid,GUID">
                                                        <Columns>
                                                            <asp:TemplateField FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="rDelete"
                                                                            ToolTip="Delete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField FooterText="objcohdownload" HeaderStyle-Width="25px" ItemStyle-Width="25px"
                                                                Visible="false">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="colhDownloadImage" runat="server" ToolTip="Download" CommandArgument='<%#Eval("scanattachtranunkid") %>'
                                                                            OnClick="lnkdownloadClaimAttachmentAttachment_Click">
                                                                                   <i class="fa fa-download"></i>
                                                                        </asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                            <asp:BoundField DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                            <asp:BoundField DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                        </Columns>
                                                        <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnScanSave" runat="server" Text="Save" CssClass="btnDefault" Visible="false" />
                                            <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btnDefault" />
                                            <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
