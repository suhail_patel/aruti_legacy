﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.Globalization

#End Region

Partial Class Training_Requisition_wPgTrainingRequisitionApproverLevel
    Inherits Basepage

#Region "Private Variable"

    Private DisplayMessage As New CommonCodes
    Private ReadOnly mstrModuleName As String = "frmTrainingRequisitionApproverLevelList"
    Private ReadOnly mstrModuleName1 As String = "frmTrainingRequisitionApproverAddEdit"
    Private mintLevelmstid As Integer
    Private blnpopupApproverLevel As Boolean = False
    Private objTrainingApprLvlMaster As New clstraining_approverlevel_master

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                If CBool(Session("AllowToViewTrainingApproverLevel")) Then
                    FillList(False)
                Else
                    FillList(True)

                End If
                SetVisibility()
            Else
                If ViewState("mintLevelmstid") IsNot Nothing Then
                    mintLevelmstid = Convert.ToInt32(ViewState("mintLevelmstid").ToString())
                End If

                If ViewState("blnpopupApproverLevel") IsNot Nothing Then
                    blnpopupApproverLevel = Convert.ToBoolean(ViewState("blnpopupApproverLevel").ToString())
                End If

                If blnpopupApproverLevel Then
                    popupApproverLevel.Show()
                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mintLevelmstid") = mintLevelmstid
            ViewState("blnpopupGreApprover") = blnpopupApproverLevel
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsList As New DataSet
        Dim strfilter As String = ""
        Try
            If isblank Then
                strfilter = " 1 = 2 "
            End If

            dsList = objTrainingApprLvlMaster.GetList("List", True, False, strfilter)
            If dsList.Tables(0).Rows.Count <= 0 Then
                dsList = objTrainingApprLvlMaster.GetList("List", True, True, "1 = 2")
                isblank = True
            End If
            GvApprLevelList.DataSource = dsList.Tables("List")
            GvApprLevelList.DataBind()
            If isblank Then
                GvApprLevelList.Rows(0).Visible = False
                isblank = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnnew.Visible = CBool(Session("AllowToAddTrainingApproverLevel"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            If mintLevelmstid > 0 Then
                objTrainingApprLvlMaster._Levelunkid = mintLevelmstid
            End If

            objTrainingApprLvlMaster._Levelcode = txtlevelcode.Text.Trim
            objTrainingApprLvlMaster._Levelname = txtlevelname.Text.Trim
            objTrainingApprLvlMaster._Priority = Convert.ToInt32(txtlevelpriority.Text)
            Call SetAtValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetAtValue()
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objTrainingApprLvlMaster._AuditUserId = CInt(Session("UserId"))
            End If
            objTrainingApprLvlMaster._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objTrainingApprLvlMaster._ClientIP = CStr(Session("IP_ADD"))
            objTrainingApprLvlMaster._HostName = CStr(Session("HOST_NAME"))
            objTrainingApprLvlMaster._FormName = mstrModuleName1
            objTrainingApprLvlMaster._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub GetValue()
        Try
            If mintLevelmstid > 0 Then
                objTrainingApprLvlMaster._Levelunkid = mintLevelmstid
            End If
            txtlevelcode.Text = objTrainingApprLvlMaster._Levelcode
            txtlevelname.Text = objTrainingApprLvlMaster._Levelname
            txtlevelpriority.Text = objTrainingApprLvlMaster._Priority.ToString()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearData()
        Try
            txtlevelcode.Text = ""
            txtlevelname.Text = ""
            txtlevelpriority.Text = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Buttons Methods "

    Protected Sub btnnew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            mintLevelmstid = 0
            popupApproverLevel.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnnew_Click :" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSaveGreApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveApprover.Click
        Dim blnFlag As Boolean = False
        Try
            If txtlevelcode.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Approver Level Code cannot be blank. Approver Level Code is required information "), Me)
            ElseIf txtlevelname.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "Approver Level Name cannot be blank. Approver Level Name is required information."), Me)
            ElseIf txtlevelpriority.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Approver Level Name cannot be blank. Approver Level Name is required information."), Me)
            End If
            Call SetValue()
            If mintLevelmstid > 0 Then
                blnFlag = objTrainingApprLvlMaster.Update()
            ElseIf mintLevelmstid = 0 Then
                blnFlag = objTrainingApprLvlMaster.Insert()
            End If

            If blnFlag = False And objTrainingApprLvlMaster._Message <> "" Then
                DisplayMessage.DisplayMessage(objTrainingApprLvlMaster._Message, Me)
                blnpopupApproverLevel = True
                popupApproverLevel.Show()
            Else
                ClearData()
                FillList(False)
                mintLevelmstid = 0
                blnpopupApproverLevel = False
                popupApproverLevel.Hide()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())
            GetValue()
            blnpopupApproverLevel = True
            popupApproverLevel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            mintLevelmstid = Convert.ToInt32(lnk.CommandArgument.ToString())

            If objTrainingApprLvlMaster.isUsed(mintLevelmstid) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 4, "Sorry, You cannot delete this Approver Level. Reason: This Approver Level is in use."), Me)
                Exit Sub
            End If
            popup_YesNo.Show()
            popup_YesNo.Title = "Confirmation"
            popup_YesNo.Message = "Are You Sure You Want To Delete This Approver Level ?"

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False

            SetAtValue()
            blnFlag = objTrainingApprLvlMaster.Delete(mintLevelmstid)

            If blnFlag = False And objTrainingApprLvlMaster._Message <> "" Then
                DisplayMessage.DisplayMessage(objTrainingApprLvlMaster._Message, Me)
            Else
                FillList(False)
                mintLevelmstid = 0
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCloseGreApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClosApprover.Click
        Try
            ClearData()
            blnpopupApproverLevel = False
            popupApproverLevel.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            ClearData()
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Gridview Events "

    Protected Sub GvApprLevelList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvApprLevelList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table
                If CInt(GvApprLevelList.DataKeys(e.Row.RowIndex)("levelunkid").ToString) > 0 Then
                    If dt.Rows.Count > 0 AndAlso dt.Rows(e.Row.RowIndex)(0).ToString <> "" Then
                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                        lnkedit.Visible = CBool(Session("AllowToEditTrainingApproverLevel"))
                        lnkdelete.Visible = CBool(Session("AllowToDeleteTrainingApproverLevel"))
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class
