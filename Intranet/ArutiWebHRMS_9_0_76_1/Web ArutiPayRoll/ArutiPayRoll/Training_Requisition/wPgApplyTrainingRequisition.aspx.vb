﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.Globalization
Imports System.IO
Imports System.Data.SqlClient
Imports System.Net.Dns

#End Region

Partial Class Training_Requisition_wPgApplyTrainingRequisition
    Inherits Basepage

#Region " Private Variable "

    Private DisplayMessage As New CommonCodes
    Private objRequisitionMaster As New clstraining_requisition_master
    Private objRequisitionTran As New clstraining_requisition_tran
    Private objRequisitionApproval As New clstraining_requisition_approval_master
    Private objRequisitionApprovalTran As New clstraining_requisition_approval_tran
    Private ReadOnly mstrModuleName As String = "frmTrainingRequisitionAddEdit"
    Private ReadOnly mstrModuleNameClaimExp As String = "frmClaims_RequestAddEdit"
    Private mintRequisitionMasterId As Integer = 0
    Private mstrRequisitionMasterguid As String = ""
    Private mdtRequisitionTran As DataTable
    Private mblnIsFinal As String = ""
    Private mblnIsProcessed As String = ""
    Private mblnIsAddMode As Boolean
    Private mblnFromApproval As Boolean
    Private mstrTranguid As String = String.Empty
    Private objCONN As SqlConnection
    Private mintMappingUnkid As Integer = 0
    Private mdtTrainingAttachment As DataTable = Nothing
    Private objDocument As New clsScan_Attach_Documents
    Private mintLinkedMasterId As Integer = 0
    Private mdtCosting As DataTable
    Private objClaimMaster As New clsclaim_request_master
    Private objClaimTran As New clsclaim_request_tran
    Private mintClaimRequestMasterId As Integer = 0
    Private mdtClaimAttachment As DataTable
    Private mdtFullClaimAttachment As DataTable
    Private blnShowAttchmentPopup As Boolean = False
    Private mintClaimRequestTranId As Integer = 0
    Private mstrClaimRequestTranguid As String = String.Empty

#End Region

#Region " Page Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim blnFlag As Boolean = False
            If Request.QueryString.Count > 0 Then
                blnFlag = Request.QueryString.HasKeys()
            End If
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False AndAlso blnFlag = False Then
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                If arr.Length = 7 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If
                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    Me.ViewState.Add("IsDirect", True)

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    HttpContext.Current.Session("UserId") = CInt(arr(1))
                    Me.ViewState.Add("RequisitionMasterguid", CStr(arr(2)))
                    Me.ViewState.Add("priority", CInt(arr(3)))
                    Me.ViewState.Add("mintMappingUnkid", CInt(arr(4)))
                    Me.ViewState.Add("mintLinkedMasterId", CInt(arr(5)))
                    Me.ViewState.Add("mintClaimRequestMasterId", CInt(arr(6)))
                    mblnIsAddMode = True

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Session("DateFormat") = clsConfig._CompanyDateFormat
                    Session("DateSeparator") = clsConfig._CompanyDateSeparator
                    SetDateFormat()

                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, Convert.ToString(Session("mdbname")))
                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName
                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    Call FillCombo()

                    mintMappingUnkid = CInt(Me.ViewState("mintMappingUnkid"))
                    mintLinkedMasterId = CInt(Me.ViewState("mintLinkedMasterId"))
                    mintClaimRequestMasterId = CInt(Me.ViewState("mintClaimRequestMasterId"))
                    Dim objApprover As New clstraining_approver_master
                    objApprover._Mappingunkid = mintMappingUnkid
                    mstrRequisitionMasterguid = Me.ViewState("RequisitionMasterguid").ToString
                    objRequisitionApproval._Masterguid = mstrRequisitionMasterguid
                    objRequisitionApprovalTran._RequisitionMasterguid = mstrRequisitionMasterguid
                    objClaimMaster._Crmasterunkid = mintClaimRequestMasterId
                    objClaimTran._ClaimRequestMasterId = mintClaimRequestMasterId

                    If objRequisitionApproval._Coursemasterunkid <= 0 Then
                        objApprover = Nothing
                        DisplayMessage.DisplayMessage("Sorry, could not find details with the link. May be employee had removed the training requisition.", Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                    End If

                    If objRequisitionApproval.IsResponseGiven(objRequisitionApproval._Employeeunkid, objRequisitionApproval._Coursemasterunkid, objRequisitionApproval._Training_Startdate, objRequisitionApproval._Training_Enddate, objRequisitionApproval._Instituteunkid, mintMappingUnkid, objApprover._Levelunkid, CInt(Session("UserId"))) = True Then
                        objApprover = Nothing
                        DisplayMessage.DisplayMessage("Sorry, You have already added response with the link.", Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If
                    Dim strMsg As String = String.Empty
                    strMsg = objRequisitionApproval.IsResponseGivenOnSamePriority(objRequisitionApproval._Employeeunkid, objRequisitionApproval._Coursemasterunkid, objRequisitionApproval._Training_Startdate, objRequisitionApproval._Training_Enddate, objRequisitionApproval._Instituteunkid, CInt(Me.ViewState("priority")))

                    If strMsg.Trim.Length > 0 Then
                        objApprover = Nothing
                        DisplayMessage.DisplayMessage(strMsg, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    objApprover = Nothing

                    mdtRequisitionTran = objRequisitionApprovalTran._DataTable
                    mdtCosting = objClaimTran._DataTable

                    mdtTrainingAttachment = objDocument.GetQulificationAttachment(objRequisitionApproval._Employeeunkid, enScanAttactRefId.TRAINING_REQUISITION, mintLinkedMasterId, CStr(Session("Document_Path")))
                    mdtFullClaimAttachment = objDocument.GetQulificationAttachment(objRequisitionApproval._Employeeunkid, enScanAttactRefId.TRAINING_REQUISITION, 0, CStr(Session("Document_Path")))
                    If mdtFullClaimAttachment Is Nothing Then
                        'Call objDocument.GetList(CStr(Session("Document_Path")), "List", "", CInt(drpEmpName.SelectedValue))
                        Dim strTranIds As String = String.Join(",", mdtCosting.AsEnumerable().Select(Function(x) x.Field(Of Integer)("crtranunkid").ToString).ToArray)
                        If strTranIds.Trim.Length <= 0 Then strTranIds = "0"
                        mdtFullClaimAttachment = New DataView(objDocument._Datatable, "scanattachrefid = " & enScanAttactRefId.TRAINING_REQUISITION & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                        objDocument = Nothing
                    End If
                    mdtClaimAttachment = mdtFullClaimAttachment.Copy

                    FillTranGrids(False, gvOtherTrainingList)
                    FillTranGrids(False, gvResourcesList)
                    FillAttachment()

                    gvOtherTrainingList.Columns(0).Visible = False  'Edit
                    gvOtherTrainingList.Columns(1).Visible = False  'Delete

                    gvGrievanceAttachment.Columns(0).Visible = False 'Delete
                    gvGrievanceAttachment.Columns(0).Visible = True 'Download

                    gvResourcesList.Columns(0).Visible = False  'Edit
                    gvResourcesList.Columns(1).Visible = False  'Delete

                    gvCosting.Columns(0).Visible = False    'Edit
                    gvCosting.Columns(2).Visible = False    'Delete

                    dgv_Attchment.Columns(0).Visible = False 'Delete
                    dgv_Attchment.Columns(0).Visible = True 'Download

                    btnAddOthTraining.Visible = False
                    btnSaveAttachment.Visible = False
                    btnAddResources.Visible = False
                    btnSave.Visible = False
                    btnApprove.Visible = True
                    btnDisapprove.Visible = True
                    GetValue()
                    pnlPart1.Enabled = False    'Basic Data
                    pnlPart2.Enabled = False    'Venue & Other Data
                    pnlPart3.Enabled = False    'Other Training Data
                    pnlPart4.Enabled = False    'Other Resources
                    pnlPart6.Visible = False    'Attachment Part
                    pnlPart5.Visible = True     'Approver Part
                    btnAddExpense.Visible = False

                    drpApprover.SelectedValue = CStr(mintMappingUnkid)
                    drpApprover_SelectedIndexChanged(New Object, New EventArgs)
                    drpApprover.Enabled = False
                    txtApproverLevel.Enabled = False

                    HttpContext.Current.Session("Login") = True
                    GoTo Link

                End If
            End If

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            If IsPostBack = False Then
                Call FillCombo()

                If Session("mblnIsAddMode") IsNot Nothing Then
                    ViewState("mblnIsAddMode") = CBool(Session("mblnIsAddMode"))
                    Session.Remove("mblnIsAddMode")
                End If

                If Session("mintLinkedMasterId") IsNot Nothing Then
                    ViewState("mintLinkedMasterId") = CInt(Session("mintLinkedMasterId"))
                    Session.Remove("mintLinkedMasterId")
                End If

                If Session("mblnFromApproval") IsNot Nothing Then
                    ViewState("mblnFromApproval") = CBool(Session("mblnFromApproval"))
                    Session.Remove("mblnFromApproval")
                End If

                If Session("mintClaimRequestMasterId") IsNot Nothing Then
                    ViewState("mintClaimRequestMasterId") = CInt(Session("mintClaimRequestMasterId"))
                    Session.Remove("mintClaimRequestMasterId")
                End If

                If Not Session("RequisitionMasterId") Is Nothing Or Not Session("RequisitionMasterguid") Is Nothing Then
                    ViewState("RequisitionMasterId") = CInt(Session("RequisitionMasterId"))
                    ViewState("RequisitionMasterguid") = CStr(Session("RequisitionMasterguid"))
                    Session.Remove("RequisitionMasterId")
                    Session.Remove("RequisitionMasterguid")
                Else
                    ViewState("RequisitionMasterguid") = Guid.NewGuid.ToString()
                End If

                mblnFromApproval = CBool(ViewState("mblnFromApproval"))
                mblnIsAddMode = CBool(ViewState("mblnIsAddMode"))
                mintRequisitionMasterId = CInt(ViewState("RequisitionMasterId"))
                mstrRequisitionMasterguid = CStr(ViewState("RequisitionMasterguid"))
                mintLinkedMasterId = CInt(Me.ViewState("mintLinkedMasterId"))
                mintClaimRequestMasterId = CInt(Me.ViewState("mintClaimRequestMasterId"))

                objRequisitionApproval._Masterguid = mstrRequisitionMasterguid
                objRequisitionApprovalTran._RequisitionMasterguid = mstrRequisitionMasterguid
                objClaimMaster._Crmasterunkid = mintClaimRequestMasterId
                objClaimTran._ClaimRequestMasterId = mintClaimRequestMasterId

                mdtRequisitionTran = objRequisitionApprovalTran._DataTable
                mdtCosting = objClaimTran._DataTable

                Dim iEmpId As Integer = 0
                If CInt(drpEmpName.SelectedValue) > 0 Then
                    iEmpId = CInt(drpEmpName.SelectedValue)
                Else
                    iEmpId = objRequisitionApproval._Employeeunkid
                End If

                mdtTrainingAttachment = objDocument.GetQulificationAttachment(iEmpId, enScanAttactRefId.TRAINING_REQUISITION, mintLinkedMasterId, CStr(Session("Document_Path")))
                mdtFullClaimAttachment = objDocument.GetQulificationAttachment(iEmpId, enScanAttactRefId.TRAINING_REQUISITION, 0, CStr(Session("Document_Path")))
                If mdtFullClaimAttachment Is Nothing Then
                    'Call objDocument.GetList(CStr(Session("Document_Path")), "List", "", iEmpId)
                    Dim strTranIds As String = String.Join(",", mdtCosting.AsEnumerable().Select(Function(x) x.Field(Of Integer)("crtranunkid").ToString).ToArray)
                    If strTranIds.Trim.Length <= 0 Then strTranIds = "0"
                    mdtFullClaimAttachment = New DataView(objDocument._Datatable, "scanattachrefid = " & enScanAttactRefId.TRAINING_REQUISITION & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                    objDocument = Nothing
                End If
                mdtClaimAttachment = mdtFullClaimAttachment.Copy

                If mblnIsAddMode = False Then
                    FillTranGrids(False, gvOtherTrainingList)
                    FillTranGrids(False, gvResourcesList)
                    FillCostings(False)
                    FillAttachment()
                Else
                    If mblnFromApproval = True Then
                        If Session("mintMappingUnkid") IsNot Nothing Then
                            ViewState("mintMappingUnkid") = Session("mintMappingUnkid")
                            Session.Remove("mintMappingUnkid")
                            mintMappingUnkid = CInt(ViewState("mintMappingUnkid"))
                        End If

                        FillTranGrids(False, gvOtherTrainingList)
                        FillTranGrids(False, gvResourcesList)
                        FillCostings(False)
                        FillAttachment()

                        gvOtherTrainingList.Columns(0).Visible = False
                        gvOtherTrainingList.Columns(1).Visible = False

                        gvResourcesList.Columns(0).Visible = False
                        gvResourcesList.Columns(1).Visible = False

                        gvCosting.Columns(0).Visible = False
                        gvCosting.Columns(2).Visible = False

                        gvGrievanceAttachment.Columns(0).Visible = False 'Delete
                        gvGrievanceAttachment.Columns(1).Visible = False 'Download

                        dgv_Attchment.Columns(0).Visible = False
                        dgv_Attchment.Columns(1).Visible = False

                        btnAddOthTraining.Visible = False
                        btnAddResources.Visible = False
                        btnSave.Visible = False
                        btnApprove.Visible = True
                        btnDisapprove.Visible = True
                        btnAddExpense.Visible = False

                        pnlPart1.Enabled = False
                        pnlPart2.Enabled = False
                        pnlPart5.Visible = True

                        drpApprover.SelectedValue = CStr(mintMappingUnkid)
                        drpApprover_SelectedIndexChanged(New Object, New EventArgs)
                        drpApprover.Enabled = False
                        txtApproverLevel.Enabled = False

                    Else
                        FillTranGrids(True, gvOtherTrainingList)
                        FillTranGrids(True, gvResourcesList)
                        FillCostings(True)
                    End If
                End If
                Call GetValue()
            Else
                mblnFromApproval = CBool(ViewState("mblnFromApproval"))
                mblnIsAddMode = CBool(ViewState("mblnIsAddMode"))
                mintRequisitionMasterId = CInt(Me.ViewState("mintRequisitionMasterId"))
                mstrRequisitionMasterguid = CStr(Me.ViewState("mstrRequisitionMasterguid"))
                mdtRequisitionTran = CType(Me.ViewState("mdtRequisitionTran"), DataTable)
                mstrTranguid = CStr(Me.ViewState("mstrTranguid"))
                mintLinkedMasterId = CInt(Me.ViewState("mintLinkedMasterId"))
                mdtTrainingAttachment = CType(Me.ViewState("mdtTrainingAttachment"), DataTable)
                mdtCosting = CType(Me.ViewState("mdtCosting"), DataTable)
                mdtClaimAttachment = CType(Me.ViewState("mdtClaimAttachment"), DataTable)
                mdtFullClaimAttachment = CType(Me.ViewState("mdtFullClaimAttachment"), DataTable)
                mintClaimRequestMasterId = CInt(Me.ViewState("mintClaimRequestMasterId"))
                blnShowAttchmentPopup = CBool(Me.ViewState("blnShowAttchmentPopup"))
                mintClaimRequestTranId = CInt(Me.ViewState("mintClaimRequestTranId"))
                mstrClaimRequestTranguid = CStr(Me.ViewState("mstrClaimRequestTranguid"))
            End If
Link:
            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If

            If CInt(Session("ClaimRequestVocNoType")) = 1 Then
                txtClaimNo.Enabled = False
            ElseIf CInt(Session("ClaimRequestVocNoType")) = 0 Then
                txtClaimNo.Enabled = True
            End If

            If blnShowAttchmentPopup Then
                popup_ScanAttchment.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintRequisitionMasterId") = mintRequisitionMasterId
            Me.ViewState("mstrRequisitionMasterguid") = mstrRequisitionMasterguid
            Me.ViewState("mdtRequisitionTran") = mdtRequisitionTran
            Me.ViewState("mblnFromApproval") = mblnFromApproval
            Me.ViewState("mblnIsAddMode") = mblnIsAddMode
            Me.ViewState("mstrTranguid") = mstrTranguid
            Me.ViewState("mintLinkedMasterId") = mintLinkedMasterId
            Me.ViewState("mdtTrainingAttachment") = mdtTrainingAttachment
            Me.ViewState("mdtFullClaimAttachment") = mdtFullClaimAttachment
            Me.ViewState("mdtCosting") = mdtCosting
            Me.ViewState("mdtClaimAttachment") = mdtClaimAttachment
            Me.ViewState("mintClaimRequestMasterId") = mintClaimRequestMasterId
            Me.ViewState("blnShowAttchmentPopup") = blnShowAttchmentPopup
            Me.ViewState("mintClaimRequestTranId") = mintClaimRequestTranId
            Me.ViewState("mstrClaimRequestTranguid") = mstrClaimRequestTranguid
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim objMst As New clsCommon_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim objInstitute As New clsinstitute_master
        Dim objApprover As New clstraining_approver_master
        Dim objExpense As New clsExpense_Master
        'S.SANDEEP [07-NOV-2018] -- START
        Dim objMaster As New clsMasterData
        'S.SANDEEP [07-NOV-2018] -- END
        Dim dsCombo As New DataSet
        Try
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            Dim blnApplyFilter As Boolean = True

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                blnSelect = True
            ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If
            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                             Session("UserAccessModeSetting").ToString(), True, _
                                             False, "List", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)
            With drpEmpName
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = CStr(intEmpId)
            End With
            drpEmpName_SelectedIndexChanged(New Object(), New EventArgs())

            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, CInt(Session("Fin_year")), Session("Database_Name").ToString(), CDate(Session("fin_startdate")), "List", True, enStatusType.OPEN, False)
            With drpPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombo = objMst.getComboList(clsCommon_Master.enCommonMaster.TRAINING_RESOURCES, True, "List", , , , , , True)
            With drpTrainingResources
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombo = objMst.getComboList(clsCommon_Master.enCommonMaster.TRAINING_MODE, True, "List")
            With drpOtherTrainingMode
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0).Copy()
                .DataBind()
                .SelectedValue = CStr(0)
            End With
            With drpTrainingMode
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0).Copy()
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombo = objInstitute.getListForCombo(False, "List", True)
            With DrpVendorName
                .DataValueField = "instituteunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombo = objApprover.GetList("List", True, "", 0, Nothing, True)
            With drpApprover
                .DataValueField = "mappingunkid"
                .DataTextField = "approver"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombo = objMst.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List").Copy
                .DataBind()
            End With
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List").Copy
                .DataBind()
            End With

            dsCombo = objExpense.getComboList(enExpenseType.EXP_TRAINING, True, "List", CInt(drpEmpName.SelectedValue))
            With cboExpense
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
            End With

            dsCombo = objMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
            With cboSecRoute
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
            End With

            'S.SANDEEP [07-NOV-2018] -- START
            dsCombo = objMaster.GetTrainingtypeList("List", True)
            With drpTrainingType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
            End With
            'S.SANDEEP [07-NOV-2018] -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmp = Nothing
            objMst = Nothing
            objPeriod = Nothing
            objInstitute = Nothing
            objApprover = Nothing
            objExpense = Nothing
            'S.SANDEEP [07-NOV-2018] -- START
            objMaster = Nothing
            'S.SANDEEP [07-NOV-2018] -- END
            dsCombo.Dispose()
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objRequisitionApproval._Masterguid = mstrRequisitionMasterguid
            objRequisitionApproval._Additonal_Comments = txtReqRemark.Text
            objRequisitionApproval._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
            If mblnIsAddMode = False Then
                objRequisitionApproval._Audittype = enAuditType.EDIT
            Else
                objRequisitionApproval._Audittype = enAuditType.ADD
            End If
            objRequisitionApproval._Audituserunkid = CInt(Session("UserId"))
            objRequisitionApproval._Coursemasterunkid = CInt(drpTrainingCourse.SelectedValue)
            objRequisitionApproval._Employeeunkid = CInt(drpEmpName.SelectedValue)
            objRequisitionApproval._Form_Name = mstrModuleName
            objRequisitionApproval._Hostname = CStr(Session("HOST_NAME"))
            objRequisitionApproval._Instituteunkid = CInt(DrpVendorName.SelectedValue)
            objRequisitionApproval._Ip = CStr(Session("IP_ADD"))
            objRequisitionApproval._Isvoid = False
            objRequisitionApproval._Isweb = True
            objRequisitionApproval._Loginemoployeeunkid = CInt(Session("Employeeunkid"))
            objRequisitionApproval._Periodunkid = CInt(drpPeriod.SelectedValue)
            objRequisitionApproval._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval
            objRequisitionApproval._Training_Enddate = dttrainingTodate.GetDate
            objRequisitionApproval._Training_Startdate = dttrainingFromdate.GetDate
            objRequisitionApproval._Training_Venue = txtVenue.Text
            objRequisitionApproval._Trainingmodeunkid = CInt(drpTrainingMode.SelectedValue)
            If mblnIsAddMode = False Then
                objRequisitionApproval._Transactiondate = objRequisitionApproval._Auditdatetime
            Else
                objRequisitionApproval._Transactiondate = ConfigParameter._Object._CurrentDateAndTime
            End If
            objRequisitionApproval._Voiddatetime = Nothing
            objRequisitionApproval._Voidreason = ""
            objRequisitionApproval._Voiduserunkid = -1
            objRequisitionApproval._IsCancel = False
            objRequisitionApproval._CancelDateTime = Nothing
            objRequisitionApproval._CancelFromModuleId = 0
            objRequisitionApproval._CancelRemark = ""
            objRequisitionApproval._CancelUserId = 0
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                If mblnIsAddMode = True Then
                    objRequisitionApproval._LinkedMasterId = 0
                End If
            Else
                objRequisitionApproval._LinkedMasterId = mintLinkedMasterId
            End If
            objRequisitionApproval._ApplyDate = dtapplydate.GetDate.Date
            'S.SANDEEP [07-NOV-2018] -- START
            objRequisitionApproval._TrainingTypeid = CInt(IIf(drpTrainingType.SelectedValue = "", 0, drpTrainingType.SelectedValue))
            'S.SANDEEP [07-NOV-2018] -- END
            ''''''''''''''''''''''''' CLAIM REQUEST PART --------- START
            objRequisitionApproval._ClaimRequestMasterId = mintClaimRequestMasterId
            objRequisitionApproval._ObjClaimRequestMaster._Crmasterunkid = mintClaimRequestMasterId
            objRequisitionApproval._ObjClaimRequestMaster._Claimrequestno = txtClaimNo.Text
            objRequisitionApproval._ObjClaimRequestMaster._Expensetypeid = enExpenseType.EXP_TRAINING
            objRequisitionApproval._ObjClaimRequestMaster._Employeeunkid = CInt(drpEmpName.SelectedValue)
            objRequisitionApproval._ObjClaimRequestMaster._Transactiondate = dtpClaimDate.GetDate
            objRequisitionApproval._ObjClaimRequestMaster._Claim_Remark = txtClaimRemark.Text
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                objRequisitionApproval._ObjClaimRequestMaster._Userunkid = 0
            Else
                objRequisitionApproval._ObjClaimRequestMaster._Userunkid = CInt(Session("UserId"))
            End If
            objRequisitionApproval._ObjClaimRequestMaster._Isvoid = False
            objRequisitionApproval._ObjClaimRequestMaster._Voiduserunkid = -1
            objRequisitionApproval._ObjClaimRequestMaster._Voiddatetime = Nothing
            objRequisitionApproval._ObjClaimRequestMaster._Voidreason = ""
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                objRequisitionApproval._ObjClaimRequestMaster._Loginemployeeunkid = CInt(drpEmpName.SelectedValue)
            Else
                objRequisitionApproval._ObjClaimRequestMaster._Loginemployeeunkid = 0
            End If
            objRequisitionApproval._ObjClaimRequestMaster._Modulerefunkid = enModuleReference.Training
            objRequisitionApproval._ObjClaimRequestMaster._Referenceunkid = 0
            objRequisitionApproval._ObjClaimRequestMaster._FromModuleId = enExpFromModuleID.FROM_TRAINING
            objRequisitionApproval._ObjClaimRequestMaster._TrainingRequisitionMasterId = mintLinkedMasterId
            objRequisitionApproval._ObjClaimRequestMaster._Statusunkid = 2 'PENDING
            ''''''''''''''''''''''''' CLAIM REQUEST PART --------- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            objRequisitionApproval._Masterguid = mstrRequisitionMasterguid
            drpEmpName.SelectedValue = CStr(objRequisitionApproval._Employeeunkid)
            If CBool(Me.ViewState("IsDirect")) = True Or mblnFromApproval = True Then
                Call drpEmpName_SelectedIndexChanged(New Object, New EventArgs)
            End If
            txtReqRemark.Text = objRequisitionApproval._Additonal_Comments
            drpPeriod.SelectedValue = CStr(objRequisitionApproval._Periodunkid)
            If objRequisitionApproval._Periodunkid > 0 Then
                drpPeriod_SelectedIndexChanged(New Object(), New EventArgs())
                radYes.Items.FindByValue("1").Selected = True
            Else
                radYes.Items.FindByValue("0").Selected = True
                Call radYes_SelectedIndexChanged(New Object, New EventArgs)
            End If
            drpTrainingCourse.SelectedValue = CStr(objRequisitionApproval._Coursemasterunkid)
            DrpVendorName.SelectedValue = CStr(objRequisitionApproval._Instituteunkid)
            dttrainingFromdate.SetDate = objRequisitionApproval._Training_Startdate
            dttrainingTodate.SetDate = objRequisitionApproval._Training_Enddate
            dttrainingTodate_TextChanged(New Object(), New EventArgs())
            txtVenue.Text = objRequisitionApproval._Training_Venue
            drpTrainingMode.SelectedValue = CStr(objRequisitionApproval._Trainingmodeunkid)
            If mdtRequisitionTran IsNot Nothing Then
                Dim dtRow() As DataRow = Nothing
                dtRow = mdtRequisitionTran.Select("trainingmodeunkid > 0")
                If dtRow.Length > 0 Then
                    RadioButtonList1.Items.FindByValue("1").Selected = True
                Else
                    RadioButtonList1.Items.FindByValue("0").Selected = False
                End If
            End If
            If objRequisitionApproval._ApplyDate <> Nothing Then
                dtapplydate.SetDate = objRequisitionApproval._ApplyDate
            Else
                dtapplydate.SetDate = Now.Date
            End If
            'S.SANDEEP [07-NOV-2018] -- START
            drpTrainingType.SelectedValue = CStr(objRequisitionApproval._TrainingTypeid)
            'S.SANDEEP [07-NOV-2018] -- END
            GetValueClaimRequest()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetValueClaimRequest()
        Try
            objClaimMaster._Crmasterunkid = mintClaimRequestMasterId
            txtClaimNo.Text = objClaimMaster._Claimrequestno
            txtClaimRemark.Text = objClaimMaster._Claim_Remark
            If objClaimMaster._Transactiondate.Date <> Nothing Then
                dtpClaimDate.SetDate = objClaimMaster._Transactiondate.Date
            Else
                dtpClaimDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidInfo() As Boolean
        Try
            If CInt(IIf(drpEmpName.SelectedValue = "", 0, drpEmpName.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Emplyoee is mandatory information. Please select employee to continue."), Me)
                Return False
            End If
            If radYes.Items.FindByValue("1").Selected = True AndAlso CInt(IIf(drpPeriod.SelectedValue = "", 0, drpPeriod.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Period is mandatory information. Please select period to continue."), Me)
                Return False
            End If
            If CInt(IIf(drpTrainingCourse.SelectedValue = "", 0, drpTrainingCourse.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, Training course is mandatory information. Please select Training course to continue."), Me)
                Return False
            End If
            If CInt(IIf(DrpVendorName.SelectedValue = "", 0, DrpVendorName.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, Vendor is mandatory information. Please select Vendor to continue."), Me)
                Return False
            End If
            If txtVenue.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, venue is mandatory information. Please provide venue to continue."), Me)
                Return False
            End If
            If dttrainingFromdate.IsNull Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, training from date is mandatory information. Please provide training from date to continue."), Me)
                Return True
            End If
            If dttrainingTodate.IsNull Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, training to date is mandatory information. Please provide training to date to continue."), Me)
                Return True
            End If
            If dttrainingFromdate.IsNull = False AndAlso dttrainingTodate.IsNull = False Then
                If dttrainingTodate.GetDate.Date < dttrainingFromdate.GetDate.Date Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, training to date cannot be less than from date. Please provide correct training dates to continue."), Me)
                    Return False
                End If
            End If
            If mdtCosting IsNot Nothing AndAlso mdtCosting.Rows.Count > 0 Then
                If mdtCosting.AsEnumerable().Where(Function(x) x.Field(Of Integer)("crtranunkid") > 0).Count() > 0 Then
                    If CInt(Session("ClaimRequestVocNoType")) = 0 Then
                        If txtClaimNo.Text.Trim.Length <= 0 Then
                            Language.setLanguage(mstrModuleNameClaimExp)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleNameClaimExp, 1, "Claim No. is mandatory information. Please select Claim No. to continue."), Me)
                            txtClaimNo.Focus()
                            Return False
                        End If
                    End If

                    If dtpClaimDate.IsNull Then
                        DisplayMessage.DisplayMessage("Sorry, Claim date is mandatory information. Please set correct claim date.", Me)
                        Return False
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub FillCostings(ByVal blnIsBlank As Boolean)
        Dim dvCosting As DataView = Nothing
        Try
            If blnIsBlank Then
                If mdtCosting IsNot Nothing Then
                    Dim dtRow() As DataRow = Nothing
                    dtRow = mdtCosting.Select("crtranunkid > 0")
                    If dtRow.Length <= 0 Then
                        Dim drow As DataRow = mdtCosting.NewRow()
                        drow.Item("crtranunkid") = -999
                        drow.Item("crmasterunkid") = -999
                        drow.Item("quantity") = 0
                        drow.Item("unitprice") = -999
                        drow.Item("amount") = 0
                        mdtCosting.Rows.Add(drow)
                        dvCosting = mdtCosting.DefaultView
                    End If
                    gvCosting.DataSource = dvCosting
                    gvCosting.DataBind()
                    gvCosting.Rows(0).Visible = False
                    txtTotalCosting.Text = Format(CDec(0), Session("fmtCurrency").ToString())
                End If
            Else
                dvCosting = New DataView(mdtCosting, "AUD <> 'D' AND quantity > 0 ", "", DataViewRowState.CurrentRows)
                gvCosting.DataSource = dvCosting
                gvCosting.DataBind()
                If mdtCosting.Rows.Count > 0 Then
                    Try
                        Dim mDecTotal As Decimal = mdtCosting.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(y) y.Field(Of Decimal)("amount")).Sum()
                        txtTotalCosting.Text = Format(CDec(mDecTotal), Session("fmtCurrency").ToString())
                    Catch ex As Exception
                        txtTotalCosting.Text = Format(CDec(0), Session("fmtCurrency").ToString())
                    End Try
                Else
                    txtTotalCosting.Text = Format(CDec(0), Session("fmtCurrency").ToString())
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillTranGrids(ByVal blnIsBlank As Boolean, ByVal gvGrid As GridView)
        Try
            Dim dvOtherTraining As DataView
            Dim dvResources As DataView
            If blnIsBlank Then
                If mdtRequisitionTran IsNot Nothing Then
                    Dim dtRow() As DataRow = Nothing
                    Select Case gvGrid.ID.ToString().ToUpper()
                        Case gvOtherTrainingList.ID.ToUpper()
                            dtRow = mdtRequisitionTran.Select("trainingmodeunkid > 0")
                            If dtRow.Length <= 0 Then
                                Dim drow As DataRow = mdtRequisitionTran.NewRow()
                                drow.Item("trainingmodeunkid") = -999
                                mdtRequisitionTran.Rows.Add(drow)
                            End If
                            dvOtherTraining = New DataView(mdtRequisitionTran, "trainingmodeunkid = -999 AND AUD <> 'D'", "", DataViewRowState.CurrentRows)
                            gvGrid.DataSource = dvOtherTraining

                        Case gvResourcesList.ID.ToUpper()
                            dtRow = mdtRequisitionTran.Select("resourcemasterunkid > 0")
                            If dtRow.Length <= 0 Then
                                Dim drow As DataRow = mdtRequisitionTran.NewRow()
                                drow.Item("resourcemasterunkid") = -999
                                mdtRequisitionTran.Rows.Add(drow)
                            End If
                            dvResources = New DataView(mdtRequisitionTran, "resourcemasterunkid = -999 AND AUD <> 'D'", "", DataViewRowState.CurrentRows)
                            gvGrid.DataSource = dvResources
                    End Select
                    gvGrid.DataBind()
                    gvGrid.Rows(0).Visible = False
                End If
            Else
                Select Case gvGrid.ID.ToString().ToUpper()
                    Case gvOtherTrainingList.ID.ToUpper()
                        dvOtherTraining = New DataView(mdtRequisitionTran, "trainingmodeunkid > 0 AND AUD <> 'D'", "", DataViewRowState.CurrentRows)
                        gvGrid.DataSource = dvOtherTraining
                    Case gvResourcesList.ID.ToUpper()
                        dvResources = New DataView(mdtRequisitionTran, "resourcemasterunkid > 0 AND AUD <> 'D'", "", DataViewRowState.CurrentRows)
                        gvGrid.DataSource = dvResources
                End Select
                gvGrid.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetDuration(ByVal dtFrom As DateTime, ByVal dtTo As DateTime)
        Dim strMsg As String = ""
        Try
            txtDuration.Text = CStr(DateDiff(DateInterval.Day, dtFrom, dtTo) + 1)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetNewGuidForApprovers(ByVal strNewMstGuid As String)
        Try
            Dim dRows As DataRow() = mdtRequisitionTran.Select("")
            dRows.ToList.ForEach(Function(x) UpdateRow(x, strNewMstGuid, "A"))
            mdtRequisitionTran.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function UpdateRow(ByVal dr As DataRow, ByVal strMasterGuid As String, ByVal strAUDValue As String) As Boolean
        Try
            dr("masterguid") = strMasterGuid
            dr("tranguid") = Guid.NewGuid.ToString
            dr("AUD") = strAUDValue
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
        Return True
    End Function

    Private Sub ClearForm()
        Try
            Me.ViewState.Remove("mintRequisitionMasterId")
            Me.ViewState.Remove("mstrRequisitionMasterguid")
            Me.ViewState.Remove("mdtRequisitionTran")
            Me.ViewState.Remove("mblnFromApproval")
            Me.ViewState.Remove("mblnIsAddMode")
            Me.ViewState.Remove("mstrTranguid")
            Me.ViewState.Remove("mintLinkedMasterId")
            Me.ViewState.Remove("mdtTrainingAttachment")
            Me.ViewState.Remove("mdtClaimAttachment")
            Me.ViewState.Remove("mdtFullClaimAttachment")
            Me.ViewState.Remove("mdtCosting")
            Me.ViewState.Remove("mintClaimRequestMasterId")
            mintRequisitionMasterId = 0
            mstrRequisitionMasterguid = ""
            mintLinkedMasterId = 0
            mdtRequisitionTran = Nothing
            mdtTrainingAttachment = Nothing
            mdtCosting = Nothing
            mdtClaimAttachment = Nothing
            mdtFullClaimAttachment = Nothing
            mblnFromApproval = False
            mblnIsAddMode = False
            mstrTranguid = ""
            mintClaimRequestMasterId = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String, ByVal dtTable As DataTable, ByVal iModulerefid As Integer, ByVal iScanattachrefid As Integer, ByVal iTransactionunkid As Integer, Optional ByVal strGUID As String = "")
        Dim dRow As DataRow
        Try
            'Dim dtRow() As DataRow = mdtTrainingAttachment.Select("filename = '" & f.Name & "' AND AUD <> 'D' AND employeeunkid <> '" & CInt(IIf(drpEmpName.SelectedValue = "", 0, drpEmpName.SelectedValue)) & "'")
            Dim dtRow() As DataRow = dtTable.Select("filename = '" & f.Name & "' AND AUD <> 'D' AND employeeunkid <> '" & CInt(IIf(drpEmpName.SelectedValue = "", 0, drpEmpName.SelectedValue)) & "'")
            If dtRow.Length <= 0 Then
                dRow = dtTable.NewRow 'mdtTrainingAttachment.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(Session("Employeeunkid"))
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = iModulerefid 'enImg_Email_RefId.Training_Module
                dRow("scanattachrefid") = iScanattachrefid 'enScanAttactRefId.TRAINING_REQUISITION
                dRow("transactionunkid") = iTransactionunkid 'mintLinkedMasterId
                dRow("filepath") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = Date.Today()
                dRow("localpath") = strfullpath
                dRow("GUID") = IIf(strGUID.Trim.Length <= 0, Guid.NewGuid(), strGUID)
                dRow("AUD") = "A"
                dRow("form_name") = mstrModuleName
                dRow("userunkid") = CInt(Session("userid"))
                dRow("filesize_kb") = f.Length / 1024
                'S.SANDEEP |25-JAN-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
                'S.SANDEEP |25-JAN-2019| -- END
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            dtTable.Rows.Add(dRow)
            Call FillAttachment()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            If mdtTrainingAttachment Is Nothing Then Exit Sub
            dtView = New DataView(mdtTrainingAttachment, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            gvGrievanceAttachment.AutoGenerateColumns = False
            gvGrievanceAttachment.DataSource = dtView
            gvGrievanceAttachment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillClaimAttachment()
        Dim dtView As DataView
        Try
            If mdtClaimAttachment Is Nothing Then Exit Sub
            dtView = New DataView(mdtClaimAttachment, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgv_Attchment.AutoGenerateColumns = False
            dgv_Attchment.DataSource = dtView
            dgv_Attchment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SaveImageOnServer(ByVal dtDocuments As DataTable)
        Try
            Dim mdsDoc As DataSet
            Dim mstrFolderName As String = ""
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim strFileName As String = ""
            For Each dRow As DataRow In dtDocuments.Rows
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRow("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault
                If dRow("AUD").ToString = "A" AndAlso dRow("localpath").ToString <> "" Then
                    strFileName = Session("CompCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("localpath")))
                    If File.Exists(CStr(dRow("localpath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(CStr(dRow("localpath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dRow("fileuniquename") = strFileName
                        dRow("filepath") = strPath
                        dRow.AcceptChanges()
                    End If
                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dRow("filepath").ToString
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If File.Exists(Server.MapPath(strFilepath)) Then
                            File.Delete(Server.MapPath(strFilepath))
                        Else
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'DisplayMessage.DisplayError(ex, Me)
                            DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub AddExpenseRow()
        Try
            Dim dtemp() As DataRow = Nothing
            If mintClaimRequestTranId > 0 Then
                dtemp = mdtCosting.Select("crtranunkid = '" & mintClaimRequestTranId & "'")
            ElseIf mstrClaimRequestTranguid.ToString() <> "" Then
                dtemp = mdtCosting.Select("GUID = '" & mstrClaimRequestTranguid & "'")
            End If
            If dtemp IsNot Nothing AndAlso dtemp.Length > 0 Then
                With dtemp(0)
                    .Item("crtranunkid") = dtemp(0).Item("crtranunkid")
                    .Item("crmasterunkid") = mintClaimRequestMasterId
                    .Item("expenseunkid") = CInt(cboExpense.SelectedValue)
                    .Item("secrouteunkid") = CInt(cboSecRoute.SelectedValue)
                    If txtUnitPrice.Attributes("iCostingId") IsNot Nothing Then
                        .Item("costingunkid") = CInt(txtUnitPrice.Attributes("iCostingId"))
                    Else
                        .Item("costingunkid") = 0
                    End If
                    .Item("unitprice") = Format(CDec(txtUnitPrice.Text), Session("fmtCurrency").ToString())
                    .Item("quantity") = CDbl(IIf(txtQty.Text.Trim.Length <= 0, 0, txtQty.Text))
                    .Item("amount") = Format(CDec(CDbl(txtUnitPrice.Text) * CDbl(IIf(txtQty.Text.Trim.Length <= 0, 0, txtQty.Text))), Session("fmtCurrency").ToString())
                    .Item("expense_remark") = txtExpenseRemark.Text
                    .Item("isvoid") = False
                    .Item("voiduserunkid") = -1
                    .Item("voiddatetime") = DBNull.Value
                    .Item("voidreason") = ""
                    .Item("loginemployeeunkid") = -1
                    If .Item("AUD").ToString() = "" Then .Item("AUD") = "U"
                    .Item("GUID") = Guid.NewGuid.ToString
                    .Item("expense") = cboExpense.SelectedItem.Text
                    .Item("uom") = txtUoMType.Text
                    .Item("voidloginemployeeunkid") = -1
                    .Item("sector") = IIf(CInt(cboSecRoute.SelectedValue) <= 0, "", cboSecRoute.SelectedItem.Text)
                End With
                mintClaimRequestTranId = 0 : mstrClaimRequestTranguid = ""
                mdtCosting.AcceptChanges()
            Else
                Dim dRow As DataRow = mdtCosting.NewRow
                dRow.Item("crtranunkid") = -1
                dRow.Item("crmasterunkid") = mintClaimRequestMasterId
                dRow.Item("expenseunkid") = CInt(cboExpense.SelectedValue)
                dRow.Item("secrouteunkid") = CInt(cboSecRoute.SelectedValue)
                If txtUnitPrice.Attributes("iCostingId") IsNot Nothing Then
                    dRow.Item("costingunkid") = CInt(txtUnitPrice.Attributes("iCostingId"))
                Else
                    dRow.Item("costingunkid") = 0
                End If
                dRow.Item("unitprice") = Format(CDec(txtUnitPrice.Text), Session("fmtCurrency").ToString())
                dRow.Item("quantity") = txtQty.Text
                dRow.Item("amount") = Format(CDec(CDbl(txtUnitPrice.Text) * CDbl(txtQty.Text)), Session("fmtCurrency").ToString())
                dRow.Item("expense_remark") = txtExpenseRemark.Text
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                dRow.Item("loginemployeeunkid") = -1
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("expense") = cboExpense.SelectedItem.Text
                dRow.Item("uom") = txtUoMType.Text
                dRow.Item("voidloginemployeeunkid") = -1
                dRow.Item("sector") = IIf(CInt(cboSecRoute.SelectedValue) <= 0, "", cboSecRoute.SelectedItem.Text)
                mdtCosting.Rows.Add(dRow)
            End If
            FillCostings(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetDefaultCosting(ByVal intSectorId As Integer, ByVal dtEffectiveDate As Date)
        Try
            Dim iCostingId As Integer = -1 : Dim iAmount As Decimal = 0
            Dim objCosting As New clsExpenseCosting
            objCosting.GetDefaultCosting(intSectorId, iCostingId, iAmount, dtEffectiveDate)
            txtUnitPrice.Text = Format(CDec(iAmount), Session("fmtCurrency").ToString())
            txtUnitPrice.Attributes.Remove("iCostingId")
            txtUnitPrice.Attributes.Add("iCostingId", CStr(iCostingId))
            txtQty.Text = "1"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub DownloadAttachment(ByVal lnk As LinkButton, ByVal gv As GridView, ByVal dtTable As DataTable)
        Try
            Dim row As GridViewRow = TryCast((lnk).NamingContainer, GridViewRow)
            Dim xrow() As DataRow
            If CInt(dgv_Attchment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) > 0 Then
                xrow = dtTable.Select("scanattachtranunkid = " & CInt(gv.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) & "")
            Else
                xrow = dtTable.Select("GUID = '" & CInt(gv.DataKeys(row.RowIndex)("GUID").ToString) & "'")
            End If
            Dim xPath As String = ""
            If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                xPath = xrow(0).Item("filepath").ToString
                xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                If Strings.Left(xPath, 1) <> "/" Then
                    xPath = "~/" & xPath
                Else
                    xPath = "~" & xPath
                End If
                xPath = Server.MapPath(xPath)

            ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                xPath = xrow(0).Item("localpath").ToString
            End If
            If xPath.Trim <> "" Then
                Dim fileInfo As New IO.FileInfo(xPath)
                If fileInfo.Exists = False Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage("File does not Exist...", Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
                fileInfo = Nothing
                Dim strFile As String = xPath

                Response.ContentType = "application/zip"
                Response.AddHeader("Content-Disposition", "attachment;filename=" + row.Cells(GetColumnIndex.getColumnID_Griview(dgv_Attchment, "colhName", False, True)).Text)
                Response.Clear()
                Response.TransmitFile(strFile)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                Response.End()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearCosting()
        Try
            cboExpense.SelectedValue = CStr(0)
            cboSecRoute.SelectedValue = CStr(0)
            cboScanDcoumentType.SelectedValue = CStr(0)
            txtQty.Text = ""
            txtUnitPrice.Text = ""
            txtUoMType.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Dates Event(s) "

    Protected Sub dtpClaimDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpClaimDate.TextChanged
        Try
            Call SetDefaultCosting(CInt(cboSecRoute.SelectedValue), dtpClaimDate.GetDate.Date)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dttrainingFromdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dttrainingFromdate.TextChanged
        Try
            dttrainingTodate.SetDate() = dttrainingFromdate.GetDate
            GetDuration(dttrainingFromdate.GetDate(), dttrainingTodate.GetDate())
            Dim trainingdatedayscount As String = CStr(DateDiff(DateInterval.Day, dtapplydate.GetDate(), dttrainingFromdate.GetDate()) + 1)
            If CInt(Session("DonotAllowToRequestTrainingAfterDays")) > 0 Then
                If CInt(Session("DonotAllowToRequestTrainingAfterDays")) >= CInt(trainingdatedayscount) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, you are not allowed to apply training requisition. Reason difference of start date and apply date should be " & Session("DonotAllowToRequestTrainingAfterDays").ToString & " set in configuration."), Me)
                    dttrainingTodate.SetDate() = Nothing
                    dttrainingFromdate.SetDate() = Nothing
                End If
            Else
                If dtapplydate.GetDate.Date > dttrainingFromdate.GetDate.Date Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Sorry, training start date cannot be less than training apply date."), Me)
                    dttrainingTodate.SetDate() = Nothing
                    dttrainingFromdate.SetDate() = Nothing
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dttrainingTodate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dttrainingTodate.TextChanged
        Try
            If dttrainingTodate.GetDate().Date < dttrainingFromdate.GetDate().Date Then
                dttrainingTodate.SetDate() = dttrainingFromdate.GetDate
            End If
            GetDuration(dttrainingFromdate.GetDate(), dttrainingTodate.GetDate())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            Dim intPriority As Integer = -1
            If IsValidInfo() = False Then Exit Sub

            Call SetValue()

            SaveImageOnServer(mdtTrainingAttachment)
            SaveImageOnServer(mdtFullClaimAttachment)

            If mblnIsAddMode = False Then
                blnFlag = objRequisitionApproval.Update(Session("Database_Name").ToString(), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, False, Now.Date, mdtRequisitionTran, False, mdtTrainingAttachment, mdtCosting, mdtFullClaimAttachment)
            Else
                blnFlag = objRequisitionApproval.Insert(Session("Database_Name").ToString(), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, False, Now.Date, mdtRequisitionTran, False, mdtTrainingAttachment, mdtCosting, mdtFullClaimAttachment)
            End If
            If blnFlag Then
                If mblnIsAddMode Then
                    Dim eMode As enLogin_Mode
                    Dim blnAddAccessCondition As Boolean = True
                    Dim intUserId As Integer = CInt(Session("UserId"))
                    If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        eMode = enLogin_Mode.EMP_SELF_SERVICE
                        blnAddAccessCondition = False
                        intUserId = -1
                    ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                        eMode = enLogin_Mode.MGR_SELF_SERVICE
                    End If
                    objRequisitionApproval.SendNotification(1, Session("Database_Name").ToString(), Session("UserAccessModeSetting").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), 1224, Session("EmployeeAsOnDate").ToString(), intUserId, mstrModuleName, eMode, Session("UserName").ToString(), Session("ArutiSelfServiceURL").ToString(), intPriority, IIf(drpEmpName.SelectedValue = "", 0, drpEmpName.SelectedValue).ToString(), "", blnAddAccessCondition, mstrRequisitionMasterguid)
                End If
                ClearForm()
                DisplayMessage.DisplayMessage("Information saved successfully.", Me, "../Training_Requisition/wPgTrainingRequisitionList.aspx")
            Else
                DisplayMessage.DisplayMessage(objRequisitionApproval._Message, Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Request.QueryString.Count <= 0 Then
                If mblnIsAddMode = False Then
                    Response.Redirect("~\Training_Requisition\wPgTrainingRequisitionList.aspx")
                Else
                    Response.Redirect("~\Training_Requisition\wPg_TrainingRequisitionApprovalList.aspx")
                End If
            Else
                Response.Redirect("~\Index.aspx")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupDelete_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelete.buttonDelReasonYes_Click
        Try
            If popupDelete.Reason.Trim.Length > 0 Then
                If Me.ViewState("gvName").ToString.ToUpper() <> gvCosting.ID.ToUpper() Then
                    If mdtRequisitionTran IsNot Nothing Then
                        Dim dtemp() As DataRow = mdtRequisitionTran.Select("tranguid='" & mstrTranguid.ToString() & "'")
                        If dtemp.Length > 0 Then
                            With dtemp(0)
                                .Item("isvoid") = True
                                .Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                                    .Item("voiduserunkid") = CInt(Session("UserId"))
                                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                                    .Item("voiduserunkid") = -1
                                End If
                                .Item("voidreason") = popupDelete.Reason
                                .Item("AUD") = "D"
                            End With
                        End If
                        mstrTranguid = ""
                    End If
                    Dim gv As GridView = Nothing
                    Select Case Me.ViewState("gvName").ToString().ToUpper()
                        Case gvOtherTrainingList.ID.ToUpper()
                            gv = gvOtherTrainingList
                        Case gvResourcesList.ID.ToUpper()
                            gv = gvResourcesList
                    End Select
                    Me.ViewState("gvName") = ""
                    FillTranGrids(False, gv)
                Else
                    If mdtCosting IsNot Nothing Then
                        Dim dtemp() As DataRow = Nothing
                        If mintClaimRequestTranId > 0 Then
                            dtemp = mdtCosting.Select("crtranunkid = '" & mintClaimRequestTranId & "'")
                        ElseIf mstrClaimRequestTranguid.ToString() <> "" Then
                            dtemp = mdtCosting.Select("GUID = '" & mstrClaimRequestTranguid & "'")
                        End If

                        If dtemp.Length > 0 Then
                            With dtemp(0)
                                .Item("isvoid") = True
                                .Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                                If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                                    .Item("voiduserunkid") = CInt(Session("UserId"))
                                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                                    .Item("voiduserunkid") = -1
                                    .Item("loginemployeeunkid") = CInt(Session("Employeeunkid"))
                                    .Item("voidloginemployeeunkid") = CInt(Session("Employeeunkid"))
                                End If
                                .Item("voidreason") = popupDelete.Reason
                                .Item("AUD") = "D"
                            End With
                        End If
                        mintClaimRequestTranId = 0
                        mstrClaimRequestTranguid = ""
                    End If
                    Me.ViewState("gvName") = ""
                    FillCostings(False)

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddOthTraining_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddOthTraining.Click
        Try
            mdtRequisitionTran.AcceptChanges()
            If mdtRequisitionTran IsNot Nothing Then
                Dim dtemp() As DataRow = Nothing
                dtemp = mdtRequisitionTran.Select("trainingmodeunkid = '" & CInt(IIf(drpOtherTrainingMode.SelectedValue = "", 0, drpOtherTrainingMode.SelectedValue)) & "' AND item_description = '" & txtTrRemark.Text.Replace("'", "''") & "'")
                If dtemp.Length > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, Training is already added into the list. Please add new training to continue."), Me)
                    Exit Sub
                End If

                If mstrTranguid.ToString() <> "" Then
                    dtemp = mdtRequisitionTran.Select("tranguid='" & mstrTranguid.ToString() & "'")
                    If dtemp.Length > 0 Then
                        With dtemp(0)
                            .Item("tranguid") = .Item("tranguid")
                            .Item("masterguid") = .Item("masterguid")
                            .Item("requisitiontranunkid") = 0
                            .Item("requisitionmasterunkid") = 0
                            .Item("trainingmodeunkid") = CInt(IIf(drpOtherTrainingMode.SelectedValue = "", 0, drpOtherTrainingMode.SelectedValue))
                            .Item("resourcemasterunkid") = 0
                            .Item("item_description") = txtTrRemark.Text
                            .Item("isvoid") = False
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voiduserunkid") = -1
                            .Item("voidreason") = ""
                            If .Item("AUD").ToString() = "" Then .Item("AUD") = "U"
                            .Item("trainingmode") = drpOtherTrainingMode.SelectedItem.Text
                            .Item("resourcetype") = ""
                        End With
                    End If
                    mstrTranguid = ""
                Else
                    Dim dRow As DataRow = mdtRequisitionTran.NewRow()
                    With dRow
                        .Item("tranguid") = Guid.NewGuid.ToString()
                        .Item("masterguid") = mstrRequisitionMasterguid
                        .Item("requisitiontranunkid") = 0
                        .Item("requisitionmasterunkid") = 0
                        .Item("trainingmodeunkid") = CInt(IIf(drpOtherTrainingMode.SelectedValue = "", 0, drpOtherTrainingMode.SelectedValue))
                        .Item("resourcemasterunkid") = 0
                        .Item("item_description") = txtTrRemark.Text
                        .Item("isvoid") = False
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voiduserunkid") = -1
                        .Item("voidreason") = ""
                        .Item("AUD") = "A"
                        .Item("trainingmode") = drpOtherTrainingMode.SelectedItem.Text
                        .Item("resourcetype") = ""
                    End With
                    mdtRequisitionTran.Rows.Add(dRow)
                End If
            End If
            mdtRequisitionTran.AcceptChanges()
            FillTranGrids(False, gvOtherTrainingList)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddResources_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddResources.Click
        Try
            mdtRequisitionTran.AcceptChanges()
            If mdtRequisitionTran IsNot Nothing Then
                Dim dtemp() As DataRow = Nothing
                dtemp = mdtRequisitionTran.Select("resourcemasterunkid = '" & CInt(IIf(drpTrainingResources.SelectedValue = "", 0, drpTrainingResources.SelectedValue)) & "' AND item_description = '" & txtResRemark.Text.Replace("'", "''") & "'")
                If dtemp.Length > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, Resource is already added into the list. Please add new resource to continue."), Me)
                    Exit Sub
                End If
                If mstrTranguid.ToString() <> "" Then
                    dtemp = mdtRequisitionTran.Select("tranguid='" & mstrTranguid.ToString() & "'")
                    If dtemp.Length > 0 Then
                        With dtemp(0)
                            .Item("tranguid") = .Item("tranguid")
                            .Item("masterguid") = .Item("masterguid")
                            .Item("requisitiontranunkid") = 0
                            .Item("requisitionmasterunkid") = 0
                            .Item("trainingmodeunkid") = 0
                            .Item("resourcemasterunkid") = CInt(IIf(drpTrainingResources.SelectedValue = "", 0, drpTrainingResources.SelectedValue))
                            .Item("item_description") = txtResRemark.Text
                            .Item("isvoid") = False
                            .Item("voiddatetime") = DBNull.Value
                            .Item("voiduserunkid") = -1
                            .Item("voidreason") = ""
                            .Item("AUD") = .Item("AUD")
                            .Item("trainingmode") = ""
                            .Item("resourcetype") = drpTrainingResources.SelectedItem.Text
                        End With
                    End If
                    mstrTranguid = ""
                Else
                    Dim dRow As DataRow = mdtRequisitionTran.NewRow()
                    With dRow
                        .Item("tranguid") = Guid.NewGuid.ToString()
                        .Item("masterguid") = mstrRequisitionMasterguid
                        .Item("requisitiontranunkid") = 0
                        .Item("requisitionmasterunkid") = 0
                        .Item("trainingmodeunkid") = 0
                        .Item("resourcemasterunkid") = CInt(IIf(drpTrainingResources.SelectedValue = "", 0, drpTrainingResources.SelectedValue))
                        .Item("item_description") = txtResRemark.Text
                        .Item("isvoid") = False
                        .Item("voiddatetime") = DBNull.Value
                        .Item("voiduserunkid") = -1
                        .Item("voidreason") = ""
                        .Item("AUD") = "A"
                        .Item("trainingmode") = ""
                        .Item("resourcetype") = drpTrainingResources.SelectedItem.Text
                    End With
                    mdtRequisitionTran.Rows.Add(dRow)
                End If
            End If
            FillTranGrids(False, gvResourcesList)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddExpense_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddExpense.Click
        Try
            If CInt(cboExpense.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleNameClaimExp, 5, "Expense is mandatory information. Please select Expense to continue."), Me)
                Exit Sub
            End If

            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpense._IsSecRouteMandatory AndAlso CInt(IIf(cboSecRoute.SelectedValue = "", 0, cboSecRoute.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleNameClaimExp, 20, "Sector/Route is mandatory information. Please enter Sector/Route to continue."), Me)
                objExpense = Nothing
                Exit Sub
            End If
            If objExpense IsNot Nothing Then objExpense = Nothing

            If CDbl(IIf(txtQty.Text.Trim.Length <= 0, 0, txtQty.Text)) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleNameClaimExp, 6, "Quantity is mandatory information. Please enter Quantity to continue."), Me)
                Exit Sub
            End If

            Dim objExapprover As New clsExpenseApprover_Master
            Dim dsList As DataSet = objExapprover.GetEmployeeApprovers(CInt(enExpenseType.EXP_TRAINING), CInt(drpEmpName.SelectedValue), "List", Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleNameClaimExp, 17, "Please Assign Expense Approver to this employee and also map Expense Approver to system user."), Me)
                objExapprover = Nothing
                Exit Sub
            End If
            If objExapprover IsNot Nothing Then objExapprover = Nothing

            Dim sMsg As String = String.Empty

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'sMsg = objClaimMaster.IsValid_Expense(CInt(drpEmpName.SelectedValue), CInt(cboExpense.SelectedValue))


            'Pinkal (22-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.

            'sMsg = objClaimMaster.IsValid_Expense(CInt(drpEmpName.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
            '                                                                           , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpClaimDate.GetDate.Date _
            '                                                                           , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting))

            sMsg = objClaimMaster.IsValid_Expense(CInt(drpEmpName.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
                                                                                       , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpClaimDate.GetDate.Date _
                                                                                     , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimRequestTranId, True)

            'Pinkal (22-Jan-2020) -- End

            'Pinkal (26-Feb-2019) -- End
            If sMsg <> "" Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Exit Sub
            End If
            sMsg = ""

            If mintClaimRequestTranId <= 0 AndAlso mstrClaimRequestTranguid.Trim.Length <= 0 Then
                Dim dtmp() As DataRow = Nothing
                If mdtCosting.Rows.Count > 0 Then
                    dtmp = mdtCosting.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND AUD <> 'D'")
                    If dtmp.Length > 0 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleNameClaimExp, 10, "Sorry, you cannot add same expense again in the below list."), Me)
                        Exit Sub
                    End If
                End If
            End If

            If CDbl(txtUnitPrice.Text) <= 0 Then
                cnfUnitPrice.Message = Language.getMessage(mstrModuleNameClaimExp, 25, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                       Language.getMessage(mstrModuleNameClaimExp, 26, "Do you wish to continue?")
                cnfUnitPrice.Show()
                Exit Sub
            End If

            Call AddExpenseRow()

            Call ClearCosting()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnDisapprove.Click
        Try
            Select Case CType(sender, Button).ID.ToUpper
                Case btnDisapprove.ID.ToUpper
                    If txtRemark.Text.Trim.Length <= 0 Then
                        DisplayMessage.DisplayMessage("Sorry, Remark is mandatory information. Please add remark to continue.", Me)
                        Exit Sub
                    End If
                    cnfApprovDisapprove.Message = "Are you sure you want to reject this training requisition?"
                Case btnApprove.ID.ToUpper
                    cnfApprovDisapprove.Message = "Are you sure you want to approve this training requisition?"
            End Select
            Me.ViewState("Sender") = CType(sender, Button).ID.ToUpper
            cnfApprovDisapprove.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfApprovDisapprove_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfApprovDisapprove.buttonYes_Click
        Dim blnFlag As Boolean = False
        Me.ViewState("OldRequisitionMasterguid") = mstrRequisitionMasterguid
        Try
            If IsValidInfo() = False Then Exit Sub
            mstrRequisitionMasterguid = Guid.NewGuid.ToString()
            Call SetValue()
            SetNewGuidForApprovers(mstrRequisitionMasterguid)
            Select Case Me.ViewState("Sender").ToString().ToUpper()
                Case btnApprove.ID.ToUpper
                    Dim dtAppr As DataTable : Dim drtemp() As DataRow = Nothing : Dim strCheckedEmpIds As String = ""
                    dtAppr = objRequisitionApproval.GetNextEmployeeApprovers(Session("Database_Name").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString(), 1224, Session("EmployeeAsOnDate").ToString(), CInt(Session("UserId")), Nothing, CInt(txtApproverLevel.Attributes("priority")), True, "", True)
                    If dtAppr.Rows.Count > 0 Then
                        strCheckedEmpIds = String.Join(",", dtAppr.AsEnumerable().Select(Function(y) y.Field(Of Integer)("employeeunkid").ToString).Distinct().ToArray())
                    Else
                        strCheckedEmpIds = ""
                    End If
                    drtemp = dtAppr.Select("employeeunkid in (" & strCheckedEmpIds & ") AND priority > " & CInt(txtApproverLevel.Attributes("priority")), "priority ASC")
                    If drtemp.Length <= 0 Then
                        drtemp = dtAppr.Select("")
                        For index As Integer = 0 To drtemp.Length - 1
                            objRequisitionApproval._Isfinal = True
                        Next
                    End If
                    objRequisitionApproval._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.Approved
                Case btnDisapprove.ID.ToUpper
                    objRequisitionApproval._Statusunkid = clstraining_requisition_approval_master.enApprovalStatus.Rejected
            End Select

            objRequisitionApproval._Remark = txtRemark.Text
            objRequisitionApproval._Mappingunkid = CInt(drpApprover.Attributes("mappingunkid"))
            objRequisitionApproval._Mapuserunkid = CInt(drpApprover.Attributes("mapuserunkid"))
            objRequisitionApproval._Levelunkid = CInt(txtApproverLevel.Attributes("levelunkid"))

            If mblnIsAddMode = False Then
                blnFlag = objRequisitionApproval.Update(Session("Database_Name").ToString(), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, False, Now.Date, mdtRequisitionTran, True, mdtTrainingAttachment, mdtCosting, mdtFullClaimAttachment)
            Else
                blnFlag = objRequisitionApproval.Insert(Session("Database_Name").ToString(), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, False, Now.Date, mdtRequisitionTran, True, mdtTrainingAttachment, mdtCosting, mdtFullClaimAttachment)
            End If
            If blnFlag Then
                If objRequisitionApproval._Isfinal = False AndAlso objRequisitionApproval._Statusunkid <> clstraining_requisition_approval_master.enApprovalStatus.Rejected Then
                    If mblnIsAddMode Then
                        Dim eMode As enLogin_Mode
                        Dim blnAddAccessCondition As Boolean = True
                        Dim intUserId As Integer = CInt(Session("UserId"))
                        Dim intPriority As Integer = CInt(txtApproverLevel.Attributes("priority"))
                        If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                            eMode = enLogin_Mode.EMP_SELF_SERVICE
                            blnAddAccessCondition = False
                            intUserId = -1
                            intPriority = -1
                        ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                            eMode = enLogin_Mode.MGR_SELF_SERVICE
                        End If
                        objRequisitionApproval.SendNotification(2, Session("Database_Name").ToString(), Session("UserAccessModeSetting").ToString(), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), 1224, Session("EmployeeAsOnDate").ToString(), intUserId, mstrModuleName, eMode, Session("UserName").ToString(), Session("ArutiSelfServiceURL").ToString(), intPriority, IIf(drpEmpName.SelectedValue = "", 0, drpEmpName.SelectedValue).ToString(), "", blnAddAccessCondition, Me.ViewState("OldRequisitionMasterguid").ToString())
                    End If
                End If
                If mblnFromApproval = False Then
                    ClearForm()
                    DisplayMessage.DisplayMessage("Information saved successfully.", Me, Session("rootpath").ToString() & "Index.aspx")
                Else
                    ClearForm()
                    DisplayMessage.DisplayMessage("Information saved successfully.", Me, "../Training_Requisition/wPg_TrainingRequisitionApprovalList.aspx")
                End If
            Else
                DisplayMessage.DisplayMessage(objRequisitionApproval._Message, Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            Me.ViewState.Remove("OldRequisitionMasterguid")
        End Try
    End Sub

    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAttachment.Click
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    DisplayMessage.DisplayMessage(Language.getMessage("frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                AddDocumentAttachment(f, f.FullName, mdtTrainingAttachment, CInt(enImg_Email_RefId.Training_Module), CInt(enScanAttactRefId.TRAINING_REQUISITION), mintLinkedMasterId)
                Call FillAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveCAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCAttachment.Click
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    DisplayMessage.DisplayMessage(Language.getMessage("frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                AddDocumentAttachment(f, f.FullName, mdtFullClaimAttachment, CInt(enImg_Email_RefId.Training_Module), CInt(enScanAttactRefId.TRAINING_REQUISITION), mintClaimRequestTranId, mstrClaimRequestTranguid)
                Dim xrow As DataRow()
                If CInt(mintClaimRequestTranId) > 0 Then
                    xrow = mdtFullClaimAttachment.Select("transactionunkid = " & CInt(mintClaimRequestTranId) & "")
                Else
                    xrow = mdtFullClaimAttachment.Select("GUID = '" & mstrClaimRequestTranguid & "'")
                End If
                If xrow.Length > 0 Then
                    mdtClaimAttachment = xrow.CopyToDataTable
                Else
                    mdtClaimAttachment = mdtFullClaimAttachment.Clone()
                End If
                Call FillClaimAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfDeleteImage_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfDeleteImage.buttonYes_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                mdtTrainingAttachment.Rows(CInt(Me.ViewState("DeleteAttRowIndex")))("AUD") = "D"
                mdtTrainingAttachment.AcceptChanges()
                Call FillAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfDeleteOResources_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfDeleteOResources.buttonYes_Click
        Try
            popupDelete.Reason = ""
            popupDelete.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cnfDeleteOtraining_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfDeleteOtraining.buttonYes_Click
        Try
            popupDelete.Reason = ""
            popupDelete.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cnfUnitPrice_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfUnitPrice.buttonYes_Click
        Try
            AddExpenseRow()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnScanClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanClose.Click
        Try
            mintClaimRequestTranId = 0 : mstrClaimRequestTranguid = ""
            blnShowAttchmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfDeleteCostingFile_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfDeleteCostingFile.buttonYes_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                mdtClaimAttachment.Rows(CInt(Me.ViewState("DeleteAttRowIndex")))("AUD") = "D"
                mdtClaimAttachment.AcceptChanges()
                FillClaimAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Protected Sub cboExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpense.SelectedIndexChanged
        Try
            Dim objExpMaster As New clsExpense_Master
            If CInt(cboExpense.SelectedValue) > 0 Then
                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)
                cboSecRoute.Enabled = objExpMaster._IsSecRouteMandatory
                cboSecRoute.SelectedValue = "0"
                txtUnitPrice.Enabled = True : txtUnitPrice.Text = "0.00"
                If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                    txtUoMType.Text = Language.getMessage("clsExpCommonMethods", 6, "Quantity")
                    txtQty.Text = "1"
                ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                    txtUoMType.Text = Language.getMessage("clsExpCommonMethods", 7, "Amount")
                    txtQty.Text = "1"
                Else
                    txtUoMType.Text = ""
                    txtQty.Text = "1"
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub drpApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpApprover.SelectedIndexChanged
        Try
            If CInt(IIf(drpApprover.SelectedValue = "", 0, drpApprover.SelectedValue)) > 0 Then
                Dim objlevel As New clstraining_approverlevel_master
                Dim objApprover As New clstraining_approver_master
                objApprover._Mappingunkid = CInt(IIf(drpApprover.SelectedValue = "", 0, drpApprover.SelectedValue))
                objlevel._Levelunkid = objApprover._Levelunkid
                txtApproverLevel.Text = objlevel._Levelname
                txtApproverLevel.Attributes.Add("levelunkid", objlevel._Levelunkid.ToString())
                txtApproverLevel.Attributes.Add("priority", objlevel._Priority.ToString())
                drpApprover.Attributes.Add("mappingunkid", objApprover._Mappingunkid.ToString())
                drpApprover.Attributes.Add("mapuserunkid", objApprover._Mapuserunkid.ToString())
                objlevel = Nothing : objApprover = Nothing
            Else
                txtApproverLevel.Attributes.Remove("levelunkid")
                txtApproverLevel.Attributes.Remove("priority")
                drpApprover.Attributes.Remove("mappingunkid")
                drpApprover.Attributes.Remove("mapuserunkid")
                txtApproverLevel.Text = ""
                txtRemark.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub drpEmpName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpEmpName.SelectedIndexChanged
        Dim objEmp As New clsEmployee_Master
        Dim objDepartment As New clsDepartment
        Dim objJob As New clsJobs
        Try
            If CInt(IIf(drpEmpName.SelectedValue = "", 0, drpEmpName.SelectedValue)) > 0 Then
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = CInt(IIf(drpEmpName.SelectedValue = "", 0, drpEmpName.SelectedValue))
                objDepartment._Departmentunkid = objEmp._Departmentunkid
                objJob._Jobunkid = objEmp._Jobunkid
                txtDepartment.Text = objDepartment._Name
                txtJob.Text = objJob._Job_Name

                Dim mstrCountry As String = ""
                Dim objCountry As New clsMasterData
                Dim objState As New clsstate_master
                Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmp._Domicile_Countryunkid)
                If dsCountry.Tables("List").Rows.Count > 0 Then
                    mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
                End If
                dsCountry.Clear()
                dsCountry = Nothing
                Dim strAddress As String = IIf(objEmp._Domicile_Address1 <> "", objEmp._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                           IIf(objEmp._Domicile_Address2 <> "", objEmp._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                           IIf(objEmp._Domicile_Road <> "", objEmp._Domicile_Road & Environment.NewLine, "").ToString & _
                                           IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                           mstrCountry

                txtDomicileAddress.Text = strAddress
                objState = Nothing
                objCountry = Nothing
                objEmp = Nothing
            Else
                txtDepartment.Text = ""
                txtJob.Text = ""
                txtDomicileAddress.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmp = Nothing : objDepartment = Nothing : objJob = Nothing
        End Try
    End Sub

    Protected Sub drpPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpPeriod.SelectedIndexChanged
        Try
            If CInt(IIf(drpPeriod.SelectedValue = "", 0, drpPeriod.SelectedValue)) > 0 Then
                radYes.Items.FindByValue("1").Selected = True
                Dim strIds As String = objRequisitionApproval.GetAssessmentTrainingIds(CInt(drpEmpName.SelectedValue), CInt(drpPeriod.SelectedValue))
                Dim dsCombo As DataSet = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                Dim dtInfo As DataTable = Nothing
                If dsCombo IsNot Nothing Then
                    dtInfo = New DataView(dsCombo.Tables(0), "masterunkid IN (" & strIds & ")", "", DataViewRowState.CurrentRows).ToTable()
                End If
                With drpTrainingCourse
                    .DataValueField = "masterunkid"
                    .DataTextField = "name"
                    .DataSource = dtInfo
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            Else
                radYes.Items.FindByValue("1").Selected = False
                radYes.Items.FindByValue("0").Selected = False
                drpTrainingCourse.Items.Clear()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub drpOtherTrainingMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpOtherTrainingMode.SelectedIndexChanged
        Try
            If CInt(IIf(drpOtherTrainingMode.SelectedValue = "", 0, drpOtherTrainingMode.SelectedValue)) > 0 Then
                RadioButtonList1.Items.FindByValue("1").Selected = True
            Else
                RadioButtonList1.Items.FindByValue("1").Selected = False
                RadioButtonList1.Items.FindByValue("0").Selected = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboSecRoute_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSecRoute.SelectedIndexChanged
        Try
            Call SetDefaultCosting(CInt(cboSecRoute.SelectedValue), dtpClaimDate.GetDate.Date)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Radiobutton List Events "

    Protected Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadioButtonList1.SelectedIndexChanged
        Try
            If CDbl(RadioButtonList1.SelectedValue) = 0 Then
                drpOtherTrainingMode.SelectedValue = CStr(0)
                drpOtherTrainingMode.Enabled = False
            Else
                drpOtherTrainingMode.SelectedValue = CStr(0)
                drpOtherTrainingMode.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub radYes_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radYes.SelectedIndexChanged
        Try
            If CDbl(radYes.SelectedValue) = 0 Then
                drpPeriod.SelectedValue = CStr(0)
                drpPeriod.Enabled = False
                Dim dsCombo As DataSet = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                With drpTrainingCourse
                    .DataValueField = "masterunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            Else
                drpPeriod.SelectedValue = CStr(0)
                drpPeriod.Enabled = True
                drpTrainingCourse.Items.Clear()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkOtherTrainingEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mstrTranguid = Convert.ToString(lnk.CommandArgument.ToString())
            Me.ViewState("gvName") = TryCast(lnk.NamingContainer, GridViewRow).NamingContainer.ID
            Dim dtemp() As DataRow = mdtRequisitionTran.Select("tranguid='" & mstrTranguid.ToString() & "'")
            If dtemp.Length > 0 Then
                drpOtherTrainingMode.SelectedValue = CStr(dtemp(0)("trainingmodeunkid"))
                txtTrRemark.Text = CStr(dtemp(0)("item_description"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkOtherTrainingdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mstrTranguid = Convert.ToString(lnk.CommandArgument.ToString())
            Me.ViewState("gvName") = TryCast(lnk.NamingContainer, GridViewRow).NamingContainer.ID
            Dim dtemp() As DataRow = mdtRequisitionTran.Select("tranguid = '" & mstrTranguid.ToString & "'")
            If dtemp.Length > 0 Then
                Me.ViewState("mstrTranguid") = mstrTranguid
                cnfDeleteOtraining.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkResourceEdit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mstrTranguid = Convert.ToString(lnk.CommandArgument.ToString())
            Me.ViewState("gvName") = TryCast(lnk.NamingContainer, GridViewRow).NamingContainer.ID
            Dim dtemp() As DataRow = mdtRequisitionTran.Select("tranguid='" & mstrTranguid.ToString & "'")
            If dtemp.Length > 0 Then
                drpTrainingResources.SelectedValue = CStr(dtemp(0)("resourcemasterunkid"))
                txtResRemark.Text = CStr(dtemp(0)("item_description"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkResourceDelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mstrTranguid = Convert.ToString(lnk.CommandArgument.ToString())
            Me.ViewState("gvName") = TryCast(lnk.NamingContainer, GridViewRow).NamingContainer.ID
            Dim dtemp() As DataRow = mdtRequisitionTran.Select("tranguid = '" & mstrTranguid.ToString & "'")
            If dtemp.Length > 0 Then
                Me.ViewState("mstrTranguid") = mstrTranguid
                cnfDeleteOResources.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdownloadClaimAttachmentAttachment_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)

            DownloadAttachment(lnkedit, dgv_Attchment, mdtFullClaimAttachment)

            'Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            'Dim xrow() As DataRow
            'If CInt(dgv_Attchment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) > 0 Then
            '    xrow = mdtFullClaimAttachment.Select("scanattachtranunkid = " & CInt(dgv_Attchment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) & "")
            'Else
            '    xrow = mdtFullClaimAttachment.Select("GUID = '" & CInt(dgv_Attchment.DataKeys(row.RowIndex)("GUID").ToString) & "'")
            'End If

            'Dim xPath As String = ""

            'If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
            '    xPath = xrow(0).Item("filepath").ToString
            '    xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
            '    If Strings.Left(xPath, 1) <> "/" Then
            '        xPath = "~/" & xPath
            '    Else
            '        xPath = "~" & xPath
            '    End If
            '    xPath = Server.MapPath(xPath)

            'ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
            '    xPath = xrow(0).Item("localpath").ToString
            'End If
            'If xPath.Trim <> "" Then
            '    Dim fileInfo As New IO.FileInfo(xPath)
            '    If fileInfo.Exists = False Then
            '        DisplayMessage.DisplayError(ex, Me)
            '        Exit Sub
            '    End If
            '    fileInfo = Nothing
            '    Dim strFile As String = xPath

            '    Response.ContentType = "application/zip"
            '    Response.AddHeader("Content-Disposition", "attachment;filename=" + row.Cells(GetColumnIndex.getColumnID_Griview(dgv_Attchment, "colhName", False, True)).Text)
            '    Response.Clear()
            '    Response.TransmitFile(strFile)
            '    HttpContext.Current.ApplicationInstance.CompleteRequest()
            '    Response.End()
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkdownloadAttachment_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)

            DownloadAttachment(lnkedit, gvGrievanceAttachment, mdtTrainingAttachment)

            'Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            'Dim xrow() As DataRow
            'If CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) > 0 Then
            '    xrow = mdtTrainingAttachment.Select("scanattachtranunkid = " & CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) & "")
            'Else
            '    xrow = mdtTrainingAttachment.Select("GUID = '" & CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("GUID").ToString) & "'")
            'End If

            'Dim xPath As String = ""

            'If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
            '    xPath = xrow(0).Item("filepath").ToString
            '    xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
            '    If Strings.Left(xPath, 1) <> "/" Then
            '        xPath = "~/" & xPath
            '    Else
            '        xPath = "~" & xPath
            '    End If
            '    xPath = Server.MapPath(xPath)

            'ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
            '    xPath = xrow(0).Item("localpath").ToString
            'End If
            'If xPath.Trim <> "" Then
            '    Dim fileInfo As New IO.FileInfo(xPath)
            '    If fileInfo.Exists = False Then
            '        DisplayMessage.DisplayError(ex, Me)
            '        Exit Sub
            '    End If
            '    fileInfo = Nothing
            '    Dim strFile As String = xPath

            '    Response.ContentType = "application/zip"
            '    Response.AddHeader("Content-Disposition", "attachment;filename=" + row.Cells(GetColumnIndex.getColumnID_Griview(gvGrievanceAttachment, "colhName", False, True)).Text)
            '    Response.Clear()
            '    Response.TransmitFile(strFile)
            '    HttpContext.Current.ApplicationInstance.CompleteRequest()
            '    Response.End()
            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkEditExpense_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mintClaimRequestTranId = Convert.ToInt32(lnk.CommandArgument.ToString())
            Dim dtemp() As DataRow = Nothing
            If mintClaimRequestTranId <= 0 Then
                mstrClaimRequestTranguid = gvCosting.DataKeys(row.RowIndex)("GUID").ToString
                dtemp = mdtCosting.Select("GUID = '" & mstrClaimRequestTranguid & "'")
            Else
                dtemp = mdtCosting.Select("crtranunkid = '" & mintClaimRequestTranId & "'")
            End If
            If dtemp.Length > 0 Then
                cboExpense.SelectedValue = CStr(dtemp(0).Item("expenseunkid"))
                cboSecRoute.SelectedValue = CStr(dtemp(0).Item("secrouteunkid"))
                txtUnitPrice.Text = CStr(dtemp(0).Item("unitprice"))
                txtExpenseRemark.Text = CStr(dtemp(0).Item("expense_remark"))
                cboSecRoute.SelectedValue = CStr(dtemp(0).Item("secrouteunkid"))
                txtQty.Text = CStr(dtemp(0).Item("quantity"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDeleteExpense_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            mintClaimRequestTranId = Convert.ToInt32(lnk.CommandArgument.ToString())
            If mintClaimRequestTranId <= 0 Then
                mstrClaimRequestTranguid = gvCosting.DataKeys(row.RowIndex)("GUID").ToString
            End If
            ViewState("gvName") = gvCosting.ID
            popupDelete.Reason = ""
            popupDelete.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDeleteClaimAttachedDoc(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            Dim xrow() As DataRow
            If CInt(dgv_Attchment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) > 0 Then
                xrow = mdtFullClaimAttachment.Select("scanattachtranunkid = " & CInt(dgv_Attchment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) & "")
            Else
                xrow = mdtFullClaimAttachment.Select("GUID = '" & dgv_Attchment.DataKeys(row.RowIndex)("GUID").ToString & "'")
            End If
            mintClaimRequestTranId = Convert.ToInt32(lnk.CommandArgument.ToString())
            If mintClaimRequestTranId <= 0 Then
                mstrClaimRequestTranguid = dgv_Attchment.DataKeys(row.RowIndex)("GUID").ToString
            End If
            cnfDeleteCostingFile.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkClaimAttach_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            If gvCosting.Rows.Count > 0 Then
                Dim lnk As LinkButton = TryCast(sender, LinkButton)
                Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
                Dim xrow() As DataRow
                If CInt(gvCosting.DataKeys(row.RowIndex)("crtranunkid").ToString) > 0 Then
                    xrow = mdtFullClaimAttachment.Select("transactionunkid = " & CInt(gvCosting.DataKeys(row.RowIndex)("crtranunkid").ToString) & "")
                Else
                    xrow = mdtFullClaimAttachment.Select("GUID = '" & gvCosting.DataKeys(row.RowIndex)("GUID").ToString & "'")
                End If
                mintClaimRequestTranId = Convert.ToInt32(lnk.CommandArgument.ToString())
                If mintClaimRequestTranId <= 0 Then
                    mstrClaimRequestTranguid = gvCosting.DataKeys(row.RowIndex)("GUID").ToString
                End If
                If xrow.Length > 0 Then
                    mdtClaimAttachment = xrow.CopyToDataTable
                Else
                    mdtClaimAttachment = mdtFullClaimAttachment.Clone()
                End If
            End If
            Call FillClaimAttachment()
            blnShowAttchmentPopup = True
            popup_ScanAttchment.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " GridView Event(s) "

    Protected Sub gvCosting_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCosting.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim iValue As Decimal = 0
                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCosting, "dgcolhUnitPrice", False, True)).Text <> "&nbsp;" Then
                    iValue = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCosting, "dgcolhUnitPrice", False, True)).Text)
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCosting, "dgcolhUnitPrice", False, True)).Text = Format(iValue, Session("fmtCurrency").ToString())
                End If
                iValue = 0
                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCosting, "dgcolhAmount", False, True)).Text <> "&nbsp;" Then
                    iValue = CDec(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCosting, "dgcolhAmount", False, True)).Text)
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCosting, "dgcolhAmount", False, True)).Text = Format(iValue, Session("fmtCurrency").ToString())
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub gvGrievanceAttachment_RowCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvGrievanceAttachment.RowCommand
        Try
            Dim Row As GridViewRow = CType(((CType(e.CommandSource, Control)).NamingContainer), GridViewRow)

            If Row.RowIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(gvGrievanceAttachment.DataKeys(Row.RowIndex)("scanattachtranunkid").ToString) > 0 Then
                    xrow = mdtTrainingAttachment.Select("scanattachtranunkid = " & CInt(gvGrievanceAttachment.DataKeys(Row.RowIndex)("scanattachtranunkid").ToString) & "")
                Else
                    xrow = mdtTrainingAttachment.Select("GUID = '" & gvGrievanceAttachment.DataKeys(Row.RowIndex)("GUID").ToString.Trim & "'")
                End If
                If e.CommandName.ToUpper = "REMOVEATTACHMENT" Then
                    If xrow.Length > 0 Then
                        cnfDeleteImage.Message = "Are you sure you want to delete this attachment?"
                        Me.ViewState("DeleteAttRowIndex") = mdtTrainingAttachment.Rows.IndexOf(xrow(0))
                        cnfDeleteImage.Show()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgv_Attchment_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgv_Attchment.RowCommand
        Try
            Dim Row As GridViewRow = CType(((CType(e.CommandSource, Control)).NamingContainer), GridViewRow)
            If Row.RowIndex >= 0 Then
                If e.CommandName.ToUpper = "RDELETE" Then
                    cnfDeleteCostingFile.Message = "Are you sure you want to delete this attachment?"
                    Me.ViewState("DeleteAttRowIndex") = Row.RowIndex
                    'cnfDeleteCostingFile.Show()

                    If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                        mdtClaimAttachment.Rows(CInt(Me.ViewState("DeleteAttRowIndex")))("AUD") = "D"
                        mdtClaimAttachment.AcceptChanges()
                        FillClaimAttachment()
                    End If

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class