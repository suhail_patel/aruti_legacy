﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data

Partial Class Loan_Savings_wPg_Add_Edit_Employee_Saving
    Inherits Basepage

#Region " Private Variables "

    Private msg As New CommonCodes

    Private objEmployeeSaving As clsSaving_Tran
    Private objInterestRate As clsInterestRate_tran
    Private objSavingScheme As clsSavingScheme
    Private objPeriodData As clscommom_period_Tran
    Private objEmployeeData As clsEmployee_Master

    Private mintSavingtranunkid As Integer = -1
    Private mintMinLoan_Tenure As Integer = -1
    Private mdecMinContribution As Decimal = 0
    Private mdecMaturityAmount As Decimal = 0
    Private mblnFormload As Boolean = False
    
    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmEmployeeSavings_AddEdit"
    'Anjan [04 June 2014] -- End

    'SHANI [12 JAN 2015]-START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    Private objSavingContribution As New clsSaving_contribution_tran
    Private objSavingInterestRate As New clsSaving_interest_rate_tran
    'SHANI [12 JAN 2015]--END 

    'SHANI [09 MAR 2015]-START
    'Enhancement - Add Currency Field.
    Dim objExRate As New clsExchangeRate
    Private mstrBaseCurrSign As String = ""
    Private mintBaseCurrId As Integer = 0
    Private mintBaseCountryId As Integer = 0
    Private mdecBaseExRate As Decimal = 0.0
    Private mdecPaidExRate As Decimal = 0.0
    Private mintPaidCurrId As Integer = 0
    'SHANI [09 MAR 2015]-END

#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (07-Feb-2016) -- CBool(Session("IsArutiDemo"))

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            Call SetLanguage()
            'Anjan [04 June 2014] -- End


            Dim clsuser As New User
            objSavingScheme = New clsSavingScheme
            objEmployeeSaving = New clsSaving_Tran
            If Not IsPostBack Then

                'SHANI [12 JAN 2015]-START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                Me.ViewState.Add("SavingtranunkId", -1)
                If Session("SavingtranunkId") IsNot Nothing AndAlso CInt(Session("SavingtranunkId")) > 0 Then

                    'Shani(18-Dec-2015) -- Start
                    'Enhancement - 
                    'Me.ViewState("SavingtranunkId") = Session("SavingtranunkId")
                    mintSavingtranunkid = CInt(Session("SavingtranunkId"))
                    'Shani(18-Dec-2015) -- End

                End If
                'SHANI [12 JAN 2015]--END 

                SetVisibility()
                Call FillCombo()
                If Session("SavingtranunkId") IsNot Nothing AndAlso CInt(Session("SavingtranunkId")) > 0 Then
                    objEmployeeSaving._Savingtranunkid = CInt(Session("SavingtranunkId"))

                    'Shani(18-Dec-2015) -- Start
                    'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
                    txtOpeningContribution.Enabled = False
                    txtOpeningBalance.Enabled = False
                Else
                    txtOpeningContribution.Enabled = True
                    txtOpeningBalance.Enabled = True
                    'Shani(18-Dec-2015) -- End

                End If
                GetValue()

                'SHANI [12 JAN 2015]-START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.


                Dim mdtContribution As New DataTable
                Dim mdtInterestRate As New DataTable
                objSavingContribution._Savingtranunkid = CInt(Session("SavingtranunkId"))
                mdtContribution = objSavingContribution._DataTable

                objSavingInterestRate._Savingtranunkid = CInt(Session("SavingtranunkId"))
                mdtInterestRate = objSavingInterestRate._DataTable

                Me.ViewState.Add("mdtContribution", mdtContribution)
                Me.ViewState.Add("mdtInterestRate", mdtInterestRate)

                Call Fill_Contribution_Data()
                Call Fill_Interest_Rate()

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("SavingtranunkId") Is Nothing Then
                    If Session("Saving_EmpUnkID") IsNot Nothing Then
                        cboEmpName.SelectedValue = CStr(Session("Saving_EmpUnkID"))
                    End If
                End If
                'SHANI [09 Mar 2015]--END

                Session("SavingtranunkId") = Nothing
                'SHANI [12 JAN 2015]--END 

                'SHANI [09 MAR 2015]-START
                'Enhancement - Add Currency Field.
            Else
                mstrBaseCurrSign = CStr(Me.ViewState("mstrBaseCurrSign"))
                mintBaseCurrId = CInt(Me.ViewState("mintBaseCurrId"))
                mintBaseCountryId = CInt(Me.ViewState("mintBaseCountryId"))
                mdecBaseExRate = CDec((Me.ViewState("mdecBaseExRate")))
                mdecPaidExRate = CDec(Me.ViewState("mdecPaidExRate"))
                mintPaidCurrId = CInt((Me.ViewState("mintPaidCurrId")))
                'SHANI [09 MAR 2015]-END

                'Shani(18-Dec-2015) -- Start
                'Enhancement - 
                mintSavingtranunkid = CInt(Me.ViewState("SavingtranunkId"))
                'Shani(18-Dec-2015) -- End

            End If

            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Form)
            'SHANI [12 JAN 2015]--END
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [12 JAN 2015]--END
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)
        'SHANI [12 JAN 2015]-START
        'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
        'ToolbarEntry1.VisibleSaveButton(False)
        'ToolbarEntry1.VisibleDeleteButton(False)
        'ToolbarEntry1.VisibleCancelButton(False)
        'ToolbarEntry1.VisibleExitButton(False)
        'SHANI [12 JAN 2015]--END

        'SHANI [09 MAR 2015]-START
        'Enhancement - Add Currency Field.
        Me.ViewState("mstrBaseCurrSign") = mstrBaseCurrSign
        Me.ViewState("mintBaseCurrId") = mintBaseCurrId
        Me.ViewState("mintBaseCountryId") = mintBaseCountryId
        Me.ViewState("mdecBaseExRate") = mdecBaseExRate
        Me.ViewState("mdecPaidExRate") = mdecPaidExRate
        Me.ViewState("mintPaidCurrId") = mintPaidCurrId
        'Shani(18-Dec-2015) -- Start
        'Enhancement - 
        Me.ViewState("SavingtranunkId") = mintSavingtranunkid
        'Shani(18-Dec-2015) -- End
        'SHANI [09 MAR 2015]-END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Private Methods "

    Private Sub GetValue()

        Dim objExchangeRate As New clsExchangeRate
        Try

            objExchangeRate._ExchangeRateunkid = 1

            txtVoucherNo.Text = objEmployeeSaving._Voucherno

            'Shani(18-Dec-2015) -- Start
            'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
            cboPayPeriod.SelectedValue = CStr(objEmployeeSaving._Payperiodunkid)
            cboPayPeriod_SelectedIndexChanged(New Object, New EventArgs())
            'Shani(18-Dec-2015) -- End

            If objEmployeeSaving._Effectivedate = Nothing Then
                dtpDate.SetDate = Now.Date
            Else
                dtpDate.SetDate = objEmployeeSaving._Effectivedate.Date
            End If

            'Shani(18-Dec-2015) -- Start
            'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
            'cboPayPeriod.SelectedValue = objEmployeeSaving._Payperiodunkid
            'cboPayPeriod_SelectedIndexChanged(New Object, New EventArgs())
            'Shani(18-Dec-2015) -- End

            cboEmpName.SelectedValue = CStr(objEmployeeSaving._Employeeunkid)

            cboSavingScheme.SelectedValue = CStr(objEmployeeSaving._Savingschemeunkid)
            cboSavingScheme_SelectedIndexChanged(New Object, New EventArgs())

            txtMonthlyContribution.Text = CStr(Format(objEmployeeSaving._Contribution, objExchangeRate._fmtCurrency))
            objEmployeeSaving._Savingstatus = objEmployeeSaving._Savingstatus
            dtpStopDate.SetDate = objEmployeeSaving._Stopdate

            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            txtRate.Text = "0"
            'SHANI [12 JAN 2015]--END 

            'SHANI (09 Mar 2015) -- Start
            'Enhancement - Provide Multi Currency on Saving scheme contribution.

            'Shani(18-Dec-2015) -- Start
            'Enhancement - CCBRT 
            'If Me.ViewState("SavingtranunkId") IsNot Nothing AndAlso CInt(Me.ViewState("SavingtranunkId")) > 0 Then
            If mintSavingtranunkid > 0 Then
                'Shani(18-Dec-2015) -- End

                cboCurrency.SelectedValue = CStr(objEmployeeSaving._Countryunkid)
            Else
                cboCurrency.SelectedValue = CStr(mintBaseCountryId)
            End If
            Call cboCurrency_SelectedIndexChanged(cboCurrency, Nothing)
            'SHANI (09 Mar 2015) -- End

            'Shani(18-Dec-2015) -- Start
            'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
            txtOpeningContribution.Text = Format(objEmployeeSaving._Bf_Amount, Session("fmtCurrency").ToString)
            txtOpeningBalance.Text = Format(objEmployeeSaving._Bf_Balance, Session("fmtCurrency").ToString)
            'Shani(18-Dec-2015) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objExchangeRate = Nothing
        End Try
    End Sub

    Private Sub SetValue()
        Try

            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'If Session("SavingtranunkId") IsNot Nothing AndAlso CInt(Session("SavingtranunkId")) > 0 Then
            'objEmployeeSaving._Savingtranunkid = CInt(Session("SavingtranunkId"))

            'Shani(18-Dec-2015) -- Start
            'Enhancement - 
            'If Me.ViewState("SavingtranunkId") IsNot Nothing AndAlso CInt(Me.ViewState("SavingtranunkId")) > 0 Then
            'objEmployeeSaving._Savingtranunkid = CInt(Me.ViewState("SavingtranunkId"))
            If mintSavingtranunkid > 0 Then
                'Shani(18-Dec-2015) -- End
                objEmployeeSaving._Savingtranunkid = mintSavingtranunkid
                'SHANI [12 JAN 2015]--END 
            End If
            objEmployeeSaving._Voucherno = txtVoucherNo.Text

            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'objEmployeeSaving._Effectivedate = dtpDate.GetDate.Date
            objPeriodData = New clscommom_period_Tran
            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            'objPeriodData._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objPeriodData._Periodunkid(Session("Database_Name").ToString) = CInt(cboPayPeriod.SelectedValue)
            'Shani(02-Dec-2015) -- End

            objEmployeeSaving._Effectivedate = objPeriodData._End_Date
            'SHANI [12 JAN 2015]--END 

            objEmployeeSaving._Payperiodunkid = CInt(cboPayPeriod.SelectedValue)
            objEmployeeSaving._Employeeunkid = CInt(cboEmpName.SelectedValue)
            objEmployeeSaving._Savingschemeunkid = CInt(cboSavingScheme.SelectedValue)
            objEmployeeSaving._Contribution = CDec(txtMonthlyContribution.Text)
            If mintSavingtranunkid = -1 Then
                objEmployeeSaving._Savingstatus = 1 ' In progress
            Else
                objEmployeeSaving._Savingstatus = objEmployeeSaving._Savingstatus
            End If


            If dtpStopDate.IsNull = False Then
                objEmployeeSaving._Stopdate = dtpStopDate.GetDate.Date
            Else
                objEmployeeSaving._Stopdate = Nothing
            End If
            objEmployeeSaving._Userunkid = CInt(Session("UserId"))

            'SHANI [09 MAR 2015]-START
            'Enhancement - Add Currency Field.
            objEmployeeSaving._Countryunkid = CInt(cboCurrency.SelectedValue)
            'SHANI [09 MAR 2015]--END   


            'Shani(18-Dec-2015) -- Start
            'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
            If mintSavingtranunkid <= 0 Then
                Dim decOpeningContribution As Decimal = 0
                Dim decOpeningBalance As Decimal = 0

                Decimal.TryParse(txtOpeningContribution.Text, decOpeningContribution)
                Decimal.TryParse(txtOpeningContribution.Text, decOpeningBalance)

                objEmployeeSaving._Total_Contribution = decOpeningContribution
                If decOpeningBalance < decOpeningContribution Then
                    decOpeningBalance = decOpeningContribution
                End If
                objEmployeeSaving._Balance_Amount = decOpeningBalance
                objEmployeeSaving._Bf_Amount = decOpeningContribution
                objEmployeeSaving._Bf_Balance = decOpeningBalance
            End If
            'Shani(18-Dec-2015) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsList As DataSet
        objPeriodData = New clscommom_period_Tran
        objSavingScheme = New clsSavingScheme
        Try

            If Session("SavingtranunkId") IsNot Nothing AndAlso CInt(Session("SavingtranunkId")) > 0 Then
                'Shani(02-Dec-2015) -- Start
                'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
                'dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, 0, "List", True)
                dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, 0, CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True)
                'Shani(02-Dec-2015) -- End

            Else
                'Shani(02-Dec-2015) -- Start
                'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
                'dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), ConfigParameter._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, 1)
                dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True, 1)
                'Shani(02-Dec-2015) -- End
            End If

            cboPayPeriod.DataValueField = "periodunkid"
            cboPayPeriod.DataTextField = "name"
            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'cboPayPeriod.DataSource = dsList.Tables("List")
            cboPayPeriod.DataSource = dsList.Tables("List").Copy
            'SHANI [12 JAN 2015]--END 
            cboPayPeriod.DataBind()
            cboPayPeriod.SelectedValue = "0"

            'Sohail (06 Dec 2019) -- Start
            'NMB UAT Enhancement # : Do not Show closed period on employee saving add edit screen.
            dsList = objPeriodData.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True, 1)
            'Sohail (06 Dec 2019) -- End

            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            cboContPeriod.DataValueField = "periodunkid"
            cboContPeriod.DataTextField = "name"
            cboContPeriod.DataSource = dsList.Tables("List").Copy
            cboContPeriod.DataBind()
            cboContPeriod.SelectedValue = "0"

            cboRatePeriod.DataValueField = "periodunkid"
            cboRatePeriod.DataTextField = "name"
            cboRatePeriod.DataSource = dsList.Tables("List").Copy
            cboRatePeriod.DataBind()
            cboRatePeriod.SelectedValue = "0"
            'SHANI [12 JAN 2015]--END 

            dsList = objSavingScheme.getComboList(True, "List")
            cboSavingScheme.DataValueField = "savingschemeunkid"
            cboSavingScheme.DataTextField = "name"
            cboSavingScheme.DataSource = dsList.Tables("List")
            cboSavingScheme.DataBind()
            cboSavingScheme.SelectedValue = "0"

            cboPayPeriod_SelectedIndexChanged(New Object(), New EventArgs())


            'SHANI [09 MAR 2015]-START
            'Enhancement - Add Currency Field.
            dsList = objExRate.getComboList("ExRate", True)
            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    mintBaseCountryId = CInt(dtTable.Rows(0).Item("countryunkid")) 'Sohail (09 Mar 2015)
                End If

                With cboCurrency
                    .DataValueField = "countryunkid"
                    .DataTextField = "currency_sign"
                    .DataSource = dsList.Tables("ExRate")
                    .DataBind()
                    .SelectedValue = "0"
                End With
            End If
            'SHANI [09 MAR 2015]--END 
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try

            'Nilay (07-Feb-2016) -- Start
            'If ConfigParameter._Object._SavingsVocNoType = 0 Then
            If CInt(Session("SavingsVocNoType")) = 0 Then
                'Nilay (07-Feb-2016) -- End

                If Trim(txtVoucherNo.Text) = "" Then

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Voucher No. cannot be blank. Voucher No. is required information."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Anjan [04 June 2014] -- End

                    txtVoucherNo.Focus()
                    Return False
                End If
            End If

            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(ex, Me)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Pay Period cannot be blank. Pay Period is required information."), Me)
                'Sohail (23 Mar 2019) -- End
                'Anjan [04 June 2014] -- End
                cboPayPeriod.Focus()
                Return False
            End If

            If CInt(cboEmpName.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(ex, Me)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Employee Name cannot be blank. Employee Name is required information."), Me)
                'Sohail (23 Mar 2019) -- End
                cboEmpName.Focus()
                Return False
            End If

            If CInt(cboSavingScheme.SelectedValue) <= 0 Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(ex, Me)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Saving Scheme cannot be blank. Saving Scheme is required information."), Me)
                'Sohail (23 Mar 2019) -- End
                'Anjan [04 June 2014] -- End
                cboSavingScheme.Focus()
                Return False
            End If


            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'If Trim(txtMonthlyContribution.Text) = "" Then
            '    'Anjan [04 June 2014] -- Start
            '    'ENHANCEMENT : Implementing Language,requested by Andrew
            '    Language.setLanguage(mstrModuleName)
            '    msg.DisplayError(ex, Me)
            '    'Anjan [04 June 2014] -- End
            '    txtMonthlyContribution.Focus()
            '    Return False
            'End If


            'If CDec(txtMonthlyContribution.Text) < mdecMinContribution Then
            '    'Anjan [04 June 2014] -- Start
            '    'ENHANCEMENT : Implementing Language,requested by Andrew
            '    Language.setLanguage(mstrModuleName)
            '    msg.DisplayError(ex, Me)
            '    'Anjan [04 June 2014] -- End
            '    txtMonthlyContribution.Focus()
            '    Return False
            'End If


            'If dtpDate.GetDate.Date > CDate(Me.ViewState("PeriodEndDate")).Date Or dtpDate.GetDate.Date < CDate(Me.ViewState("PeriodStartDate")).Date Then
            '    'Anjan [04 June 2014] -- Start
            '    'ENHANCEMENT : Implementing Language,requested by Andrew
            '    Language.setLanguage(mstrModuleName)
            '    msg.DisplayError(ex, Me)
            '    'Anjan [04 June 2014] -- End
            '    dtpDate.Focus()
            '    Return False
            'End If

            'Dim objScheme As New clsSavingScheme
            'objScheme._Savingschemeunkid = CInt(cboSavingScheme.SelectedValue)

            'If txtMonthlyContribution.Text < objScheme._Mincontribution Then
            '    'Anjan [04 June 2014] -- Start
            '    'ENHANCEMENT : Implementing Language,requested by Andrew
            '    Language.setLanguage(mstrModuleName)
            '    msg.DisplayError(ex, Me)
            '    'Anjan [04 June 2014] -- End
            '    txtMonthlyContribution.Focus()
            '    Return False
            'End If
            'SHANI [12 JAN 2015]--END

            If dtpStopDate.IsNull = False AndAlso dtpStopDate.GetDate.Date < CDate(Session("fin_startdate")).Date Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(ex, Me)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Sorry, you cannot set this Stop date. Reason this date is less than current financial year."), Me)
                'Sohail (23 Mar 2019) -- End
                Return False
            End If

            Dim objMasterData As New clsMasterData
            Dim intPeriodId As Integer
            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            'intPeriodId = objMasterData.getCurrentPeriodID(enModuleReference.Payroll, dtpStopDate.GetDate.Date)
            intPeriodId = objMasterData.getCurrentPeriodID(enModuleReference.Payroll, dtpStopDate.GetDate.Date, CInt(Session("Fin_year")), )
            'Shani(02-Dec-2015) -- End

            If intPeriodId > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                'Shani(02-Dec-2015) -- Start
                'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
                'objPeriod._Periodunkid = intPeriodId
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = intPeriodId
                'Shani(02-Dec-2015) -- End

                If objPeriod._Statusid = 2 Then

                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Sorry, you cannot set this Stop date. Reason the period is already closed. Please set new Stop date."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Anjan [04 June 2014] -- End
                    Return False
                End If
                objPeriod = Nothing
            End If
            objMasterData = Nothing
            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'objScheme = Nothing
            'SHANI [12 JAN 2015]--END 

            'SHANI [09 MAR 2015]-START
            'Enhancement - Add Currency Field.
            If CInt(cboCurrency.SelectedValue) <= 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(ex, Me)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 30, "Currency cannot be blank. Currency is required information."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Return False
            End If
            'SHANI [09 MAR 2015]--END 

            Return True

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SetVisibility()
        Try
            If Session("SavingsVocNoType").ToString = "1" Then
                txtVoucherNo.Enabled = False
            Else
                txtVoucherNo.Enabled = True
            End If

            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'If Session("SavingtranunkId") IsNot Nothing AndAlso CInt(Session("SavingtranunkId")) > 0 Then

            'Shani(18-Dec-2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            'If Me.ViewState("SavingtranunkId") IsNot Nothing AndAlso CInt(Me.ViewState("SavingtranunkId")) > 0 Then
            If mintSavingtranunkid > 0 Then
                'Shani(18-Dec-2015) -- End

                'SHANI [12 JAN 2015]--END 
                cboSavingScheme.Enabled = False
                dtpDate.Enabled = False
                cboPayPeriod.Enabled = False
                cboEmpName.Enabled = False
                'SHANI [09 Mar 2015]-START
                'Enhancement - Add Currency Field.
                cboCurrency.Enabled = False
                'SHANI [09 Mar 2015]--END 
            Else
                cboSavingScheme.Enabled = True
                'SHANI [12 JAN 2015]-START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                'dtpDate.Enabled = True
                'SHANI [12 JAN 2015]--END 
                cboPayPeriod.Enabled = True
                cboEmpName.Enabled = True
                'SHANI [09 Mar 2015]-START
                'Enhancement - Add Currency Field.
                cboCurrency.Enabled = True
                'SHANI [09 Mar 2015]--END 
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillEmployeeCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim dsCombo As DataSet
        Try
            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            'dsCombo = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , CDate(Me.ViewState("PeriodStartDate")).Date, CDate(Me.ViewState("PeriodEndDate")).Date)
            dsCombo = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  CDate(Me.ViewState("PeriodStartDate")).Date, _
                                                  CDate(Me.ViewState("PeriodEndDate")).Date, _
                                                  CStr(Session("UserAccessModeSetting")), True, _
                                                  CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
            'Shani(02-Dec-2015) -- End

            With cboEmpName
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombo.Tables("Employee")
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub Fill_Contribution_Data()
        Try
            Dim mdtContribution As DataTable = CType(Me.ViewState("mdtContribution"), DataTable)
            Dim gvContribution As DataView = mdtContribution.DefaultView
            gvContribution.RowFilter = "AUD <> 'D' "
            gvContribution.Sort = "contributiondate DESC"
            lvContributionData.DataSource = gvContribution
            lvContributionData.DataBind()
            If mdtContribution.Select("AUD <> 'D'").Length > 0 Then
                cboContPeriod.Enabled = True
                cboContPeriod.SelectedValue = "0"
            Else
                cboContPeriod.Enabled = False
                cboContPeriod.SelectedValue = cboPayPeriod.SelectedValue
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_Interest_Rate()
        Try
            Dim mdtInterestRate As DataTable = CType(Me.ViewState("mdtInterestRate"), DataTable)
            Dim dvInterestRate As DataView = mdtInterestRate.DefaultView
            dvInterestRate.RowFilter = "AUD <> 'D' "
            dvInterestRate.Sort = "effectivedate DESC"
            lvInterestRateData.DataSource = dvInterestRate
            lvInterestRateData.DataBind()
            If mdtInterestRate.Select("AUD <> 'D'").Length > 0 Then
                cboRatePeriod.Enabled = True
                cboRatePeriod.SelectedValue = "0"
            Else
                cboRatePeriod.Enabled = False
                cboRatePeriod.SelectedValue = cboPayPeriod.SelectedValue
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Control_Enabled_Disbled(ByVal blnEnable As Boolean)
        Try
            txtVoucherNo.Enabled = blnEnable
            cboPayPeriod.Enabled = blnEnable
            dtpDate.Enabled = blnEnable
            cboEmpName.Enabled = blnEnable
            dtpStopDate.Enabled = blnEnable
            cboSavingScheme.Enabled = blnEnable

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            cboCurrency.Enabled = blnEnable
            'SHANI [09 Mar 2015]--END 

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "Control_Enabled_Disbled", mstrModuleName)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region "Combobox Event"

    Protected Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Try
            Dim objPeriod As New clscommom_period_Tran
            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPayPeriod.SelectedValue)
            'Shani(02-Dec-2015) -- End

            Me.ViewState.Add("PeriodStartDate", objPeriod._Start_Date)
            Me.ViewState.Add("PeriodEndDate", objPeriod._End_Date)

            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.

            'Shani(18-Dec-2015) -- Start
            'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
            'dtpDate.SetDate = objPeriod._End_Date
            dtpDate.SetDate = objPeriod._Start_Date
            'Shani(18-Dec-2015) -- End
            cboRatePeriod.SelectedValue = cboPayPeriod.SelectedValue
            cboContPeriod.SelectedValue = cboPayPeriod.SelectedValue
            dtpRateDate.SetDate = objPeriod._End_Date
            'SHANI [12 JAN 2015]--END 
            Call FillEmployeeCombo()
            'SHANI [09 MAR 2015]-START
            'Enhancement - Add Currency Field.
            Call cboCurrency_SelectedIndexChanged(cboCurrency, Nothing)
            'SHANI [09 MAR 2015]-END
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboSavingScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSavingScheme.SelectedIndexChanged
        Try
            objSavingScheme._Savingschemeunkid = CInt(cboSavingScheme.SelectedValue)
            Me.ViewState.Add("Mincontribution", objSavingScheme._Mincontribution)
            Me.ViewState.Add("DefualtInterest", objSavingScheme._DefualtIntRate)
            txtRate.Text = Format(CDec(objSavingScheme._DefualtIntRate), Session("fmtCurrency").ToString)
            txtMonthlyContribution.Text = Format(CDec(objSavingScheme._DefualtContribution), Session("fmtCurrency").ToString)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Shani [ 12 JAN 2015 ] -- START
    '
    Private Sub cboRatePeriod_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRatePeriod.SelectedIndexChanged
        Try
            Dim objperiod As New clscommom_period_Tran
            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            'objperiod._Periodunkid = CInt(cboRatePeriod.SelectedValue)
            objperiod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboRatePeriod.SelectedValue)
            'Shani(02-Dec-2015) -- End
            dtpRateDate.SetDate = objperiod._End_Date
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Shani [ 12 JAN 2015 ] -- END

    'SHANI [09 MAR 2015]-START
    'Enhancement - Add Currency Field.
    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExRate.Text = ""

            If cboCurrency.SelectedItem Is Nothing OrElse CInt(cboCurrency.SelectedValue) <= 0 Then Exit Sub
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpDate.GetDate.Date, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'SHANI [09 MAR 2015]--END 
#End Region

#Region "Button's Event"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim mdtContribution As DataTable = CType(Me.ViewState("mdtContribution"), DataTable)
        Dim mdtInterestRate As DataTable = CType(Me.ViewState("mdtInterestRate"), DataTable)
        Try

            If IsValidData() = False Then Exit Sub
            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            If mdtContribution.Select("AUD <> 'D'").Length <= 0 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Please add atleast one Contribution."), Me)
                txtMonthlyContribution.Focus()
                Exit Sub
            ElseIf mdtContribution.Select("periodunkid = '" & CInt(cboPayPeriod.SelectedValue) & "' AND AUD <> 'D'").Length <= 0 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Plase add atleast one contribution for effective period."), Me)
                cboContPeriod.SelectedValue = cboPayPeriod.SelectedValue
                txtMonthlyContribution.Focus()
                Exit Sub
                'Sohail (12 Jan 2015) -- Start
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            Else

                'Shani(18-Dec-2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                'If Me.ViewState("SavingtranunkId") Is Nothing OrElse CInt(Me.ViewState("SavingtranunkId")) <= 0 Then
                If mintSavingtranunkid <= 0 Then
                    'Shani(18-Dec-2015) -- End

                    Dim objTnALeaveTran As New clsTnALeaveTran
                    Dim objPeriod As New clscommom_period_Tran
                    'Shani(02-Dec-2015) -- Start
                    'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
                    'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboPayPeriod.SelectedValue)
                    'Shani(02-Dec-2015) -- End

                    If objTnALeaveTran.IsPayrollProcessDone(CInt(cboPayPeriod.SelectedValue), cboEmpName.SelectedValue.ToString, objPeriod._End_Date.Date, enModuleReference.Payroll) = True Then
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry, You can not Save this Saving Scheme. Reason : Process Payroll is already done for last date of selected Period."), Me)
                        Exit Try
                    End If
                End If
                'Sohail (12 Jan 2015) -- End
            End If

            If mdtInterestRate.Select("AUD <> 'D'").Length <= 0 Then
                cboRatePeriod.SelectedValue = cboPayPeriod.SelectedValue : Call cboRatePeriod_SelectionChangeCommitted(cboRatePeriod, Nothing)
                txtRate.Text = CStr(Me.ViewState("DefualtInterest")) : Call btnRateAdd_Click(btnRateAdd, Nothing)
            ElseIf mdtInterestRate.Select("periodunkid = '" & CInt(cboPayPeriod.SelectedValue) & "' AND AUD <> 'D'").Length <= 0 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Plase add atleast one Interest Rate for effective period."), Me)
                cboRatePeriod.SelectedValue = cboPayPeriod.SelectedValue
                txtRate.Focus()
                Exit Sub
            End If
            'SHANI [12 JAN 2015]--END 
            Call SetValue()

            Blank_ModuleName()
            objEmployeeSaving._WebFormName = "frmEmployeeSavings_AddEdit"
            StrModuleName2 = "mnuLoan_Advance_Savings"
            objEmployeeSaving._WebIP = CStr(Session("IP_ADD"))
            objEmployeeSaving._WebHost = CStr(Session("HOST_NAME"))

            'Shani(18-Dec-2015) -- Start
            'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
            If mdtContribution.Rows.Count > 0 Then
                objEmployeeSaving._Contribution = CDec(mdtContribution.Rows(0).Item("contribution"))
            End If
            If mdtInterestRate.Rows.Count > 0 Then
                objEmployeeSaving._Interest_Rate = CDec(mdtInterestRate.Rows(0).Item("interest_rate"))
            End If
            'Shani(18-Dec-2015) -- End

            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            'If Session("SavingtranunkId") IsNot Nothing AndAlso CInt(Session("SavingtranunkId")) > 0 Then

            'Shani(18-Dec-2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            'If Me.ViewState("SavingtranunkId") IsNot Nothing AndAlso CInt(Me.ViewState("SavingtranunkId")) > 0 Then
            If mintSavingtranunkid > 0 Then
                'Shani(18-Dec-2015) -- End

                'SHANI [12 JAN 2015]--END 

                'Shani(02-Dec-2015) -- Start
                'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
                'blnFlag = objEmployeeSaving.Update(, mdtContribution, mdtInterestRate)
                blnFlag = objEmployeeSaving.Update(DateAndTime.Now.Date, Nothing, mdtContribution, mdtInterestRate)
                'Shani(02-Dec-2015) -- End

                If objEmployeeSaving._Message.Length > 0 Then
                    msg.DisplayMessage("Entry Not Updated. Reason : " & objEmployeeSaving._Message, Me)
                Else
                    msg.DisplayMessage("Entry Updated Successfully !!!!!", Me)
                    'Nilay (02-Mar-2015) -- Start
                    'Enhancement - REDESIGN SELF SERVICE.
                    'Response.Redirect(Session("servername") & "~/Loan_Savings/wPg_Employee_Saving_List.aspx", False)
                    Response.Redirect(Session("rootpath").ToString & "Loan_Savings/wPg_Employee_Saving_List.aspx", False)
                    'Nilay (02-Mar-2015) -- End
                End If
                objEmployeeSaving._Savingtranunkid = -1
                'SHANI [12 JAN 2015]-START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                'Session("SavingtranunkId") = Nothing
                'SHANI [12 JAN 2015]-START
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            Else
                'Shani(02-Dec-2015) -- Start
                'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
                'blnFlag = objEmployeeSaving.Insert(Session("CompanyUnkId"), , mdtContribution, mdtInterestRate)
                blnFlag = objEmployeeSaving.Insert(DateAndTime.Now.Date, CInt(Session("CompanyUnkId")), , mdtContribution, mdtInterestRate)
                'Shani(02-Dec-2015) -- End

                If blnFlag = False AndAlso objEmployeeSaving._Message.Length > 0 Then
                    msg.DisplayMessage("Entry Not Saved. Reason : " & objEmployeeSaving._Message, Me)
                Else
                    msg.DisplayMessage("Entry Saved Successfully !!!!!", Me)
                    objEmployeeSaving = New clsSaving_Tran
                    'SHANI [12 JAN 2015]-START
                    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                    objSavingContribution._Savingtranunkid = -1
                    mdtContribution = objSavingContribution._DataTable
                    Me.ViewState("mdtContribution") = mdtContribution
                    Call Fill_Contribution_Data()

                    objSavingInterestRate._Savingtranunkid = -1
                    mdtInterestRate = objSavingInterestRate._DataTable
                    Me.ViewState("mdtInterestRate") = mdtInterestRate
                    Call Fill_Interest_Rate()
                    Call SetVisibility()
                    'SHANI [12 JAN 2015]--END 
                    GetValue()
                End If
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Nilay (01-Feb-2015) -- End
        'Shani [ 24 DEC 2014 ] -- END
        Try
            Session("SavingtranunkId") = Nothing
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/wPg_Employee_Saving_List.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [12 JAN 2015]-START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    Protected Sub btnContributionAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContributionAdd.Click
        Dim mdtContribution As DataTable = CType(Me.ViewState("mdtContribution"), DataTable)
        Try
            If IsValidData() = False Then Exit Sub
            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Call Control_Enabled_Disbled(False)
            'SHANI [09 Mar 2015]--END

            If CInt(cboContPeriod.SelectedValue) <= 0 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Period cannot be blank. Period is required information."), Me)
                cboContPeriod.Focus()
                Exit Sub
            ElseIf txtMonthlyContribution.Text.Trim.Length <= 0 OrElse CDec(txtMonthlyContribution.Text) <= 0 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, Contribution should be greater than zero."), Me)
                txtMonthlyContribution.Focus()
                Exit Sub
            ElseIf CDec(txtMonthlyContribution.Text) < CDec(Me.ViewState("Mincontribution")) Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Minimum Contribution of employee cannot be less than Minimum Contribution of this scheme."), Me)
                txtMonthlyContribution.Focus()
                Exit Sub
            End If

            Dim objPeriod As New clscommom_period_Tran
            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            'objPeriod._Periodunkid = CInt(cboContPeriod.SelectedValue)
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboContPeriod.SelectedValue)
            'Shani(02-Dec-2015) -- End

            If objPeriod._Statusid = 2 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period."), Me)
                Exit Sub
            ElseIf mdtContribution.Select("periodunkid=" & CInt(cboContPeriod.SelectedValue) & " AND AUD <> 'D'").Length > 0 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Sorry, Contribution for the selected period is already given."), Me)
                cboContPeriod.Focus()
                Exit Sub
            ElseIf CDate(Me.ViewState("PeriodEndDate")) > CDate(objPeriod._End_Date) Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, Contribution period should not be less than effective period."), Me) '?1
                cboRatePeriod.Focus()
                Exit Sub
            End If

            'Sohail (12 Jan 2015) -- Start
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            Dim objTnALeaveTran As New clsTnALeaveTran
            If objTnALeaveTran.IsPayrollProcessDone(CInt(cboContPeriod.SelectedValue), cboEmpName.SelectedValue.ToString, objPeriod._End_Date.Date, enModuleReference.Payroll) = True Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry, You can not Add / Edit / Delete this Contribution. Reason : Process Payroll is already done for last date of selected period."), Me)
                Exit Try
            End If
            'Sohail (12 Jan 2015) -- End

            'SHANI [09 Mar 2015]-START
            'Enhancement -  Add Currency Field..
            Call Control_Enabled_Disbled(False)
            'SHANI [09 Mar 2015]--END

            Dim dRow As DataRow = mdtContribution.NewRow
            dRow.Item("savingcontributiontranunkid") = -1

            'Shani(18-Dec-2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            'dRow.Item("savingtranunkid") = Me.ViewState("SavingtranunkId")
            dRow.Item("savingtranunkid") = mintSavingtranunkid
            'Shani(18-Dec-2015) -- End

            dRow.Item("period_name") = cboContPeriod.SelectedItem.Text
            dRow.Item("periodunkid") = CInt(cboContPeriod.SelectedValue)
            dRow.Item("contribution") = txtMonthlyContribution.Text

            'Shani(18-Dec-2015) -- Start
            'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
            'dRow.Item("contributiondate") = CStr(CDate(objPeriod._End_Date))
            dRow.Item("contributiondate") = objPeriod._End_Date
            'Shani(18-Dec-2015) -- End

            dRow.Item("userunkid") = Session("UserId")
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString

            mdtContribution.Rows.Add(dRow) : Call Fill_Contribution_Data()
            cboContPeriod.SelectedValue = "0" : txtMonthlyContribution.Text = "0"
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnContributionEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContributionEdit.Click
        Dim mdtContribution As DataTable = CType(Me.ViewState("mdtContribution"), DataTable)
        Dim iRow As DataRow() = Nothing
        Try
            If txtMonthlyContribution.Text.Trim.Length <= 0 OrElse CDec(txtMonthlyContribution.Text) <= 0 Then '<TODO check Blank string validation>
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Contribution cannot be blank. Contribution is required information."), Me)
                txtMonthlyContribution.Focus()
                Exit Sub
            ElseIf CDec(txtMonthlyContribution.Text) < CDec(Me.ViewState("Mincontribution")) Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Sorry, Contribution should not be less than Minimum Contribution of selected scheme."), Me)
                txtMonthlyContribution.Focus()
                Exit Sub
            End If
            If CInt(Me.ViewState("EditContributionUnkid")) > 0 Then
                iRow = mdtContribution.Select("savingcontributiontranunkid = '" & CInt(Me.ViewState("EditContributionUnkid")) & "' AND AUD <> 'D'")
            Else
                iRow = mdtContribution.Select("GUID = '" & CStr(Me.ViewState("EditContributionGuid")) & "' AND AUD <> 'D'")
            End If

            If iRow IsNot Nothing Then
                Dim objPeriod As New clscommom_period_Tran
                'Shani(02-Dec-2015) -- Start
                'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
                'objPeriod._Periodunkid = CInt(cboRatePeriod.SelectedValue)
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboContPeriod.SelectedValue)
                'Shani(02-Dec-2015) -- End

                If objPeriod._Statusid = 2 Then
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, Period is already closed. Please select open Period."), Me)
                    Exit Sub
                End If
                Dim objTnaLeaveTran As New clsTnALeaveTran
                If objTnaLeaveTran.IsPayrollProcessDone(CInt(iRow(0).Item("periodunkid")), CStr(cboEmpName.SelectedValue), objPeriod._End_Date, enModuleReference.Payroll) Then
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Sorry, You can not Add / Edit / Delete this Contribution. Reason : Process Payroll is already done for last date of selected period."), Me)
                    Exit Sub
                End If
                iRow(0).Item("savingcontributiontranunkid") = iRow(0).Item("savingcontributiontranunkid")

                'Shani(18-Dec-2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                'iRow(0).Item("savingtranunkid") = Me.ViewState("SavingtranunkId")
                iRow(0).Item("savingtranunkid") = mintSavingtranunkid
                'Shani(18-Dec-2015) -- End

                iRow(0).Item("period_name") = cboContPeriod.SelectedItem.Text
                iRow(0).Item("periodunkid") = CInt(cboContPeriod.SelectedValue)
                iRow(0).Item("contribution") = txtMonthlyContribution.Text
                iRow(0).Item("contributiondate") = CStr(CDate(objPeriod._End_Date))
                iRow(0).Item("userunkid") = Session("UserId")
                iRow(0).Item("isvoid") = False
                iRow(0).Item("voiduserunkid") = -1
                iRow(0).Item("voiddatetime") = DBNull.Value
                iRow(0).Item("voidreason") = ""
                If iRow(0).Item("AUD").ToString.Trim = "" Then
                    iRow(0).Item("AUD") = "U"
                End If
                iRow(0).Item("GUID") = Guid.NewGuid.ToString
                iRow(0).AcceptChanges()
                Me.ViewState("mdtContribution") = mdtContribution
                cboContPeriod.SelectedValue = "0" : txtMonthlyContribution.Text = "0.0"
                Call Fill_Contribution_Data()
                btnContributionEdit.Visible = False : btnContributionAdd.Visible = True
                iRow = Nothing : Me.ViewState("EditContributionUnkid") = -1 : Me.ViewState("EditContributionGuid") = ""
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnRateAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRateAdd.Click
        Dim mdtInterestRate As DataTable = CType(Me.ViewState("mdtInterestRate"), DataTable)
        Try

            If IsValidData() = False Then Exit Sub
            'SHANI [09 Mar 2015]-START
            'Enhancement -  Add Currency Field..
            'Call Control_Enabled_Disbled(False)
            'SHANI [09 Mar 2015]--END
            If CInt(cboRatePeriod.SelectedValue) <= 0 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Period cannot be blank. Period is required information."), Me) '?1
                cboRatePeriod.Focus()
                Exit Sub
            ElseIf txtRate.Text.Trim.Length <= 0 OrElse CDec(txtRate.Text) < 0 OrElse CDec(txtRate.Text) > 100 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Interest Rate should be in between 0 and 100."), Me) '?1
                txtRate.Focus()
                Exit Sub
            End If

            Dim objPeriod As New clscommom_period_Tran
            'Shani(02-Dec-2015) -- Start
            'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
            'objPeriod._Periodunkid = CInt(cboRatePeriod.SelectedValue)
            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboRatePeriod.SelectedValue)
            'Shani(02-Dec-2015) -- End

            If objPeriod._Statusid = 2 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period."), Me)
                Exit Sub
            ElseIf mdtInterestRate.Select("periodunkid ='" & cboRatePeriod.SelectedValue & "' AND AUD <> 'D'").Length > 0 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Sorry, you cannot add same Date Interest Rate again in the below list."), Me) '?1
                cboRatePeriod.Focus()
                Exit Sub
            ElseIf CDate(Me.ViewState("PeriodEndDate")) > CDate(objPeriod._End_Date) Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Sorry, Interest Rate period should not be less than effective period."), Me) '?1
                cboRatePeriod.Focus()
                Exit Sub
            End If

            'SHANI [09 Mar 2015]-START
            'Enhancement -  Add Currency Field..
            Call Control_Enabled_Disbled(False)
            'SHANI [09 Mar 2015]--END

            Dim dRow As DataRow = mdtInterestRate.NewRow
            dRow.Item("savinginterestratetranunkid") = -1

            'Shani(18-Dec-2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            'dRow.Item("savingtranunkid") = Me.ViewState("SavingtranunkId")
            dRow.Item("savingtranunkid") = mintSavingtranunkid
            'Shani(18-Dec-2015) -- End

            dRow.Item("period_name") = cboRatePeriod.SelectedItem.Text
            dRow.Item("periodunkid") = CInt(cboRatePeriod.SelectedValue)
            dRow.Item("effectivedate") = CDate(objPeriod._End_Date)
            dRow.Item("interest_rate") = txtRate.Text
            dRow.Item("userunkid") = Session("UserId")
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            mdtInterestRate.Rows.Add(dRow) : Call Fill_Interest_Rate()
            cboRatePeriod.SelectedValue = "0" : txtRate.Text = "0"
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnRateEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRateEdit.Click

        Dim mdtInterestRate As DataTable = CType(Me.ViewState("mdtInterestRate"), DataTable)
        Dim iRow As DataRow() = Nothing
        Try
            If txtRate.Text.Trim.Trim.Length <= 0 OrElse CDec(txtRate.Text) < 0 OrElse CDec(txtRate.Text) > 100 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Interest Rate should be in between 0 and 100."), Me) '?1
                txtRate.Focus()
                Exit Sub
            End If

            If CInt(Me.ViewState("EditInterestRateUnkid")) > 0 Then
                iRow = mdtInterestRate.Select("savinginterestratetranunkid = '" & CInt(Me.ViewState("EditInterestRateUnkid")) & "' AND AUD <> 'D'")
            Else
                iRow = mdtInterestRate.Select("GUID = '" & CStr(Me.ViewState("EditInterestRateGuid")) & "' AND AUD <> 'D'")
            End If

            'Dim objTnaLeaveTran As New clsTnALeaveTran
            'If objTnaLeaveTran.IsPayrollProcessDone(CInt(dRowIntRate(0).Item("periodunkid")), CStr(cboEmpName.SelectedValue), objPeriod._End_Date) Then
            '    eZeeMsgBox.Show("Sorry,PayRoll Process is DONE. can not Edit/Delete Record ", enMsgBoxStyle.Information)
            '    dRowIntRate = Nothing
            '    Exit Sub
            'End If

            If iRow IsNot Nothing Then
                Dim objPeriod As New clscommom_period_Tran
                'Shani(02-Dec-2015) -- Start
                'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
                'objPeriod._Periodunkid = CInt(cboRatePeriod.SelectedValue)
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboRatePeriod.SelectedValue)
                'Shani(02-Dec-2015) -- End

                If objPeriod._Statusid = 2 Then
                    msg.DisplayMessage("Sorry,period is already closed. Please set new Period.", Me)
                    Exit Sub
                End If

                'Shani(18-Dec-2015) -- Start
                'Enhancement - Provide Deposit feaure in Employee Saving.
                'iRow(0).Item("savingtranunkid") = Me.ViewState("SavingtranunkId")
                iRow(0).Item("savingtranunkid") = mintSavingtranunkid
                'Shani(18-Dec-2015) -- End

                iRow(0).Item("period_name") = cboRatePeriod.SelectedItem.Text
                iRow(0).Item("periodunkid") = CInt(cboRatePeriod.SelectedValue)
                'iRow(0).Item("effectivedate") = CDate(dtpRateDate.GetDate)
                iRow(0).Item("interest_rate") = txtRate.Text
                iRow(0).Item("userunkid") = Session("UserId")
                iRow(0).Item("isvoid") = False
                iRow(0).Item("voiduserunkid") = -1
                iRow(0).Item("voiddatetime") = DBNull.Value
                If iRow(0).Item("AUD").ToString.Trim = "" Then
                    iRow(0).Item("AUD") = "U"
                End If
                iRow(0).Item("GUID") = Guid.NewGuid.ToString
                iRow(0).AcceptChanges()
                Me.ViewState("mdtInterestRate") = mdtInterestRate
                cboRatePeriod.SelectedValue = "0" : txtRate.Text = "0.0"
                Call Fill_Interest_Rate()
                btnRateEdit.Visible = False : btnRateAdd.Visible = True
                iRow = Nothing : Me.ViewState("EditInterestRateUnkid") = -1 : Me.ViewState("EditInterestRateGuid") = ""
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnContributionDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_Contribution.buttonDelReasonYes_Click
        Dim iRow() As DataRow = Nothing
        Dim mdtContribution As DataTable = CType(Me.ViewState("mdtContribution"), DataTable)
        Try
            If CInt(Me.ViewState("EditContributionUnkid")) > 0 Then
                iRow = mdtContribution.Select("savingcontributiontranunkid = '" & CInt(Me.ViewState("EditContributionUnkid")) & "' AND AUD <> 'D'")
            End If
            If iRow IsNot Nothing Then
                'Sohail (12 Jan 2015) -- Start
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                Dim objPeriod As New clscommom_period_Tran
                'Shani(02-Dec-2015) -- Start
                'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboContPeriod.SelectedValue)
                'Shani(02-Dec-2015) -- End

                If objPeriod._Statusid = 2 Then
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period."), Me)
                    iRow = Nothing
                    Exit Sub
                End If
                Dim objTnaLeaveTran As New clsTnALeaveTran
                If objTnaLeaveTran.IsPayrollProcessDone(CInt(cboContPeriod.SelectedValue), CStr(cboEmpName.SelectedValue), objPeriod._End_Date, enModuleReference.Payroll) Then
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Sorry, You can not Add / Edit / Delete this Contribution. Reason : Process Payroll is already done for last date of selected period."), Me)
                    Exit Sub
                End If
                'Sohail (12 Jan 2015) -- End

                iRow(0).Item("isvoid") = True
                iRow(0).Item("voiduserunkid") = Session("UserId")
                iRow(0).Item("voiddatetime") = DateAndTime.Now.Date
                iRow(0).Item("voidreason") = popup_Contribution.Reason
                iRow(0).Item("AUD") = "D"
                iRow(0).AcceptChanges()
                Me.ViewState("mdtContribution") = mdtContribution
                cboContPeriod.SelectedValue = "0" : txtMonthlyContribution.Text = "0.0"
                Call Fill_Contribution_Data()
                btnContributionEdit.Visible = False : btnContributionAdd.Visible = True
                iRow = Nothing
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnInterestDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_InterestRate.buttonDelReasonYes_Click
        Dim mdtInterestRate As DataTable = CType(Me.ViewState("mdtInterestRate"), DataTable)
        Dim iRow As DataRow() = Nothing
        Try
            If CInt(Me.ViewState("EditInterestRateUnkid")) > 0 Then
                iRow = mdtInterestRate.Select("savinginterestratetranunkid = '" & CInt(Me.ViewState("EditInterestRateUnkid")) & "' AND AUD <> 'D'")
            End If
            If iRow IsNot Nothing Then
                'Sohail (12 Jan 2015) -- Start
                'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                Dim objPeriod As New clscommom_period_Tran
                'Shani(02-Dec-2015) -- Start
                'ENHANCEMENT : Add New Employee Status Report In HR Report-kania
                'Shani(02-Dec-2015) -- End
                objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(cboRatePeriod.SelectedValue)
                If objPeriod._Statusid = 2 Then
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, Period is already closed. Please select open Period."), Me)
                    Exit Sub
                End If
                Dim objTnaLeaveTran As New clsTnALeaveTran
                If objTnaLeaveTran.IsPayrollProcessDone(CInt(cboRatePeriod.SelectedValue), CStr(cboEmpName.SelectedValue), objPeriod._End_Date) Then
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Sorry, You can not Add / Edit / Delete this Rate. Reason : Process Payroll is already done for last date of selected period."), Me)
                    Exit Sub
                End If
                'Sohail (12 Jan 2015) -- End

                iRow(0).Item("isvoid") = True
                iRow(0).Item("voiduserunkid") = Session("UserId")
                iRow(0).Item("voiddatetime") = DateAndTime.Now.Date
                iRow(0).Item("voidreason") = popup_InterestRate.Reason
                iRow(0).Item("AUD") = "D"
                iRow(0).AcceptChanges()
                Me.ViewState("mdtInterestRate") = mdtInterestRate
                cboRatePeriod.SelectedValue = "0" : txtRate.Text = "0.0"
                Call Fill_Interest_Rate()
                btnRateEdit.Visible = False : btnRateAdd.Visible = True
                iRow = Nothing
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'SHANI [12 JAN 2015]--END 

#End Region

#Region "ImageButton Events"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Session("SavingtranunkId") = Nothing
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

#Region "Toolbar Event"

    'SHANI [12 JAN 2015]-START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        Response.Redirect("~\Loan_Savings\wPg_Employee_Saving_List.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [12 JAN 2015]--END
#End Region

    'Shani(18-Dec-2015) -- Start
    'ENHANCEMENT : Provide Deposit feaure in Employee Saving.
#Region "Control(S) Event's"
    Protected Sub dtpDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDate.TextChanged
        Try
            dtpRateDate.SetDate = dtpDate.GetDate
        Catch ex As Exception
            msg.DisplayError(ex, Me)   'Hemant (13 Aug 2020)
        End Try
    End Sub
#End Region
    'Shani(18-Dec-2015) -- End

    'SHANI [12 JAN 2015]-START
    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
#Region "GridView Events"

    Protected Sub lvContributionData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvContributionData.ItemCommand
        Try
            Dim mdtContribution As DataTable = CType(Me.ViewState("mdtContribution"), DataTable)
            Dim iRow As DataRow() = Nothing
            Select Case e.CommandName
                Case "Delete"
                    If CInt(e.Item.Cells(4).Text) > 0 Then
                        iRow = mdtContribution.Select("savingcontributiontranunkid = '" & CInt(e.Item.Cells(4).Text) & "' AND AUD <> 'D'")
                        If iRow.Length > 0 Then
                            'Sohail (06 Dec 2019) -- Start
                            'NMB UAT Enhancement # : Do not Show closed period on employee saving add edit screen.
                            Dim objPeriod As New clscommom_period_Tran
                            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(iRow(0).Item("periodunkid"))
                            If objPeriod._Statusid = 2 Then
                                msg.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period."), Me)
                                Exit Sub
                            End If
                            'Sohail (06 Dec 2019) -- End
                            Me.ViewState("EditContributionUnkid") = iRow(0).Item("savingcontributiontranunkid")
                            popup_Contribution.Show()
                        End If
                    ElseIf CStr(e.Item.Cells(5).Text) <> "" Then
                        iRow = mdtContribution.Select("GUID = '" & CStr(e.Item.Cells(5).Text) & "' AND AUD <> 'D'")
                        If iRow.Length > 0 Then
                            'Sohail (06 Dec 2019) -- Start
                            'NMB UAT Enhancement # : Do not Show closed period on employee saving add edit screen.
                            Dim objPeriod As New clscommom_period_Tran
                            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(iRow(0).Item("periodunkid"))
                            If objPeriod._Statusid = 2 Then
                                msg.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period."), Me)
                                Exit Sub
                            End If
                            'Sohail (06 Dec 2019) -- End
                            iRow(0).Item("AUD") = "D"
                            iRow(0).AcceptChanges()
                            Me.ViewState("mdtContribution") = mdtContribution
                            cboContPeriod.SelectedValue = "0" : txtMonthlyContribution.Text = "0.00"
                            btnContributionAdd.Visible = True : btnContributionEdit.Visible = False
                            Call Fill_Contribution_Data()
                        End If
                    End If

                Case "Edit"
                    If CInt(e.Item.Cells(4).Text) > 0 Then
                        iRow = mdtContribution.Select("savingcontributiontranunkid = '" & CInt(e.Item.Cells(4).Text) & "' AND AUD <> 'D'")
                    ElseIf e.Item.Cells(5).Text.ToString <> "" Then
                        iRow = mdtContribution.Select("GUID = '" & e.Item.Cells(5).Text.ToString & "' AND AUD <> 'D'")
                    End If

                    If iRow.Length > 0 Then
                        'Sohail (06 Dec 2019) -- Start
                        'NMB UAT Enhancement # : Do not Show closed period on employee saving add edit screen.
                        Dim objPeriod As New clscommom_period_Tran
                        objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(iRow(0).Item("periodunkid"))
                        If objPeriod._Statusid = 2 Then
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period."), Me)
                            Exit Sub
                        End If
                        'Sohail (06 Dec 2019) -- End
                        btnContributionAdd.Visible = False : btnContributionEdit.Visible = True : cboContPeriod.Enabled = False
                        cboContPeriod.SelectedValue = CStr(iRow(0).Item("periodunkid"))
                        txtMonthlyContribution.Text = Format(CDec(iRow(0).Item("contribution")), Session("fmtCurrency").ToString)
                        If CInt(e.Item.Cells(4).Text) > 0 Then
                            Me.ViewState("EditContributionUnkid") = CStr(iRow(0).Item("savingcontributiontranunkid"))
                        ElseIf e.Item.Cells(5).Text.ToString <> "" Then
                            Me.ViewState("EditContributionGuid") = CStr(iRow(0).Item("GUID"))
                        End If
                    End If
            End Select
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lvContributionData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles lvContributionData.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Cells(3).Text = Format(CDec(e.Item.Cells(3).Text), Session("fmtCurrency").ToString)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lvInterestRateData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvInterestRateData.ItemCommand
        Dim mdtInterestRate As DataTable = CType(Me.ViewState("mdtInterestRate"), DataTable)
        Dim iRow As DataRow() = Nothing
        Try
            Select Case e.CommandName
                Case "Delete"
                    If CInt(e.Item.Cells(5).Text) > 0 Then
                        iRow = mdtInterestRate.Select("savinginterestratetranunkid = '" & CInt(e.Item.Cells(5).Text) & "' AND AUD <> 'D'")
                        If iRow.Length > 0 Then
                            'Sohail (06 Dec 2019) -- Start
                            'NMB UAT Enhancement # : Do not Show closed period on employee saving add edit screen.
                            Dim objPeriod As New clscommom_period_Tran
                            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(iRow(0).Item("periodunkid"))
                            If objPeriod._Statusid = 2 Then
                                msg.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period."), Me)
                                Exit Sub
                            End If
                            'Sohail (06 Dec 2019) -- End
                            Me.ViewState("EditInterestRateUnkid") = iRow(0).Item("savinginterestratetranunkid")
                            popup_InterestRate.Show()
                        End If
                    ElseIf CStr(e.Item.Cells(6).Text) <> "" Then
                        iRow = mdtInterestRate.Select("GUID = '" & CStr(e.Item.Cells(6).Text) & "' AND AUD <> 'D'")
                        If iRow.Length > 0 Then
                            'Sohail (06 Dec 2019) -- Start
                            'NMB UAT Enhancement # : Do not Show closed period on employee saving add edit screen.
                            Dim objPeriod As New clscommom_period_Tran
                            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(iRow(0).Item("periodunkid"))
                            If objPeriod._Statusid = 2 Then
                                msg.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period."), Me)
                                Exit Sub
                            End If
                            'Sohail (06 Dec 2019) -- End
                            iRow(0).Item("AUD") = "D"
                            iRow(0).AcceptChanges()
                            Me.ViewState("mdtInterestRate") = mdtInterestRate
                            cboRatePeriod.SelectedValue = "0" : txtRate.Text = "0.00"
                            Call Fill_Interest_Rate()
                            btnRateEdit.Visible = False : btnRateAdd.Visible = True
                        End If
                    End If

                Case "Edit"
                    If CInt(e.Item.Cells(5).Text) > 0 Then
                        iRow = mdtInterestRate.Select("savinginterestratetranunkid = '" & CInt(e.Item.Cells(5).Text) & "' AND AUD <> 'D'")
                    ElseIf e.Item.Cells(6).Text.ToString <> "" Then
                        iRow = mdtInterestRate.Select("GUID = '" & e.Item.Cells(6).Text.ToString & "' AND AUD <> 'D'")
                    End If

                    If iRow.Length > 0 Then
                        'Sohail (06 Dec 2019) -- Start
                        'NMB UAT Enhancement # : Do not Show closed period on employee saving add edit screen.
                        Dim objPeriod As New clscommom_period_Tran
                        objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(iRow(0).Item("periodunkid"))
                        If objPeriod._Statusid = 2 Then
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, Period is already closed. Please select open Period."), Me)
                            Exit Sub
                        End If
                        'Sohail (06 Dec 2019) -- End
                        btnRateAdd.Visible = False : btnRateEdit.Visible = True : cboRatePeriod.Enabled = False
                        cboRatePeriod.SelectedValue = CStr(iRow(0).Item("periodunkid"))
                        Call cboRatePeriod_SelectionChangeCommitted(cboRatePeriod, Nothing)
                        txtRate.Text = Format((iRow(0).Item("interest_rate")), Session("fmtCurrency").ToString)
                        If CInt(e.Item.Cells(5).Text) > 0 Then
                            Me.ViewState("EditInterestRateUnkid") = CStr(iRow(0).Item("savinginterestratetranunkid"))
                        ElseIf e.Item.Cells(6).Text.ToString <> "" Then
                            Me.ViewState("EditInterestRateGuid") = CStr(iRow(0).Item("GUID"))
                        End If

                    End If
            End Select
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lvInterestRateData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles lvInterestRateData.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Cells(3).Text = CDate(e.Item.Cells(3).Text).ToShortDateString
                e.Item.Cells(4).Text = Format(CDec(e.Item.Cells(4).Text), Session("fmtCurrency").ToString)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'SHANI [12 JAN 2015]--END 


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblContribution.Text = Language._Object.getCaption(Me.lblContribution.ID, Me.lblContribution.Text)
            Me.lblSavingScheme.Text = Language._Object.getCaption(Me.lblSavingScheme.ID, Me.lblSavingScheme.Text)
            Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.lblDate.Text = Language._Object.getCaption(Me.lblDate.ID, Me.lblDate.Text)
            Me.lblVoucherNo.Text = Language._Object.getCaption(Me.lblVoucherNo.ID, Me.lblVoucherNo.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.ID, Me.lblPayPeriod.Text)
            Me.lblStopDate.Text = Language._Object.getCaption(Me.lblStopDate.ID, Me.lblStopDate.Text)
            Me.lblContPeriod.Text = Language._Object.getCaption(Me.lblContPeriod.ID, Me.lblContPeriod.Text)
            Me.lblRatePeriod.Text = Language._Object.getCaption(Me.lblRatePeriod.ID, Me.lblRatePeriod.Text)
            Me.lblRate.Text = Language._Object.getCaption(Me.lblRate.ID, Me.lblRate.Text)
            Me.btnContributionAdd.Text = Language._Object.getCaption(Me.btnContributionAdd.ID, Me.btnContributionAdd.Text)
            Me.btnRateAdd.Text = Language._Object.getCaption(Me.btnRateAdd.ID, Me.btnRateAdd.Text)
            Me.lblRateDate.Text = Language._Object.getCaption(Me.lblRateDate.ID, Me.lblRateDate.Text)
            Me.btnContributionEdit.Text = Language._Object.getCaption(Me.btnContributionEdit.ID, Me.btnContributionEdit.Text)
            Me.btnRateEdit.Text = Language._Object.getCaption(Me.btnRateEdit.ID, Me.btnRateEdit.Text)
            'SHANI [12 JAN 2015]-START
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            Me.lvContributionData.Columns(2).HeaderText = Language._Object.getCaption(Me.lvContributionData.Columns(2).FooterText, Me.lvContributionData.Columns(2).HeaderText)
            Me.lvContributionData.Columns(3).HeaderText = Language._Object.getCaption(Me.lvContributionData.Columns(3).FooterText, Me.lvContributionData.Columns(3).HeaderText)
            Me.lvInterestRateData.Columns(2).HeaderText = Language._Object.getCaption(Me.lvInterestRateData.Columns(2).FooterText, Me.lvInterestRateData.Columns(2).HeaderText)
            Me.lvInterestRateData.Columns(3).HeaderText = Language._Object.getCaption(Me.lvInterestRateData.Columns(3).FooterText, Me.lvInterestRateData.Columns(3).HeaderText)
            Me.lvInterestRateData.Columns(4).HeaderText = Language._Object.getCaption(Me.lvInterestRateData.Columns(4).FooterText, Me.lvInterestRateData.Columns(4).HeaderText)
            'SHANI [12 JAN 2015]-START
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.ID, Me.lblCurrency.Text)
            'SHANI [12 JAN 2015]-END
        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End

  
End Class
