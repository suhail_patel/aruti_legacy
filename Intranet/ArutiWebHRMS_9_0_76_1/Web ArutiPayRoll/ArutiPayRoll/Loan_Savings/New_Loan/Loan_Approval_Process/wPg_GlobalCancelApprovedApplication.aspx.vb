﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO

#End Region

Partial Class Loan_Savings_New_Loan_Loan_Approval_Process_wPg_GlobalCancelApprovedApplication
    Inherits Basepage

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "frmGlobalCancelApprovedApplication"
    Dim DisplayMessage As New CommonCodes
    Private objPendingLoan As New clsProcess_pending_loan
    Private mdtTable As New DataTable
    Private mstrAdvanceFilter As String = String.Empty

    'Nilay (10-Dec-2016) -- Start
    'Issue #26: Setting to be included on configuration for Loan flow Approval process
    Private mblnIsSendEmailNotification As Boolean = False
    'Nilay (10-Dec-2016) -- End

#End Region

#Region " Page's Events "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromDesktopMSS"))
                ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromESS"))
                End If
                'Nilay (10-Dec-2016) -- End

                Call SetLanguage()
                Call FillCombo()

                dgvLoanApproved.DataSource = New List(Of String)
                dgvLoanApproved.DataBind()
            Else
                mdtTable = CType(Me.ViewState("DataTable"), DataTable)
                mstrAdvanceFilter = Me.ViewState("AdvanceFilter").ToString
                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                mblnIsSendEmailNotification = CBool(Me.ViewState("SendEmailNotification"))
                'Nilay (10-Dec-2016) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("DataTable") = mdtTable
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            'Nilay (10-Dec-2016) -- Start
            'Issue #26: Setting to be included on configuration for Loan flow Approval process
            Me.ViewState("SendEmailNotification") = mblnIsSendEmailNotification
            'Nilay (10-Dec-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objMaster As New clsMasterData
        Try
            dsList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 CStr(Session("UserAccessModeSetting")), _
                                                 True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), _
                                                 "Employee", _
                                                 True)
            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsList.Tables("Employee")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objLoanScheme.getComboList(True, "LoanScheme")
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End
            With cboLoanScheme
                .DataValueField = "loanschemeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objPendingLoan.GetLoan_Status("Status", False, False, True)
            Dim xRow = From p In dsList.Tables("Status") Where CInt(p.Item("Id")) = enLoanApplicationStatus.CANCELLED Select p
            Dim dtTable As DataTable = xRow.CopyToDataTable
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = "5"
            End With

            cboLoanAdvance.Items.Clear()
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 1, "Select"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 2, "Loan"))
            cboLoanAdvance.Items.Add(Language.getMessage(mstrModuleName, 3, "Advance"))
            cboLoanAdvance.SelectedIndex = 0

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            'dsList = objMaster.GetCondition(False, True)
            dsList = objMaster.GetCondition(False, True, True, False, False)
            'Nilay (10-Nov-2016) -- End
            Dim dtCondition As DataTable = New DataView(dsList.Tables(0), "", "id desc", DataViewRowState.CurrentRows).ToTable

            With cboAmountCondition
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dtCondition
                .DataBind()
                .SelectedIndex = 0
            End With


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        objPendingLoan = New clsProcess_pending_loan
        Dim dsList As New DataSet
        Dim strSearch As String = String.Empty
        Try
            If CBool(Session("AllowToViewProcessLoanAdvanceList")) = True Then

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    strSearch &= "AND lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboLoanScheme.SelectedValue) > 0 Then
                    strSearch &= "AND lnloan_process_pending_loan.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue) & " "
                End If

                If txtApplicationNo.Text.Trim <> "" Then
                    strSearch &= "AND lnloan_process_pending_loan.Application_No like '" & txtApplicationNo.Text.Trim & "%' "
                End If

                If CInt(cboLoanAdvance.SelectedIndex) > 0 Then
                    strSearch &= "AND CASE WHEN lnloan_process_pending_loan.isloan = 1 THEN 1 ELSE 2 END = " & CInt(cboLoanAdvance.SelectedIndex) & " "
                End If

                If txtApprovedAmount.Text.Trim <> "" AndAlso CDec(txtApprovedAmount.Text) > 0 Then
                    strSearch &= "AND lnloan_process_pending_loan.approved_amount  " & cboAmountCondition.SelectedItem.Text & " " & CDec(txtApprovedAmount.Text) & " "
                End If

                If dtpFromDate.IsNull = False Then
                    strSearch &= "AND CONVERT(CHAR(8),lnloan_process_pending_loan.application_date,112) >='" & eZeeDate.convertDate(dtpFromDate.GetDate.Date) & "' "
                End If

                If dtpToDate.IsNull = False Then
                    strSearch &= "AND lnloan_process_pending_loan.application_date <='" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
                End If

                If mstrAdvanceFilter.Trim.Length > 0 Then
                    strSearch &= "AND " & mstrAdvanceFilter
                End If

                If strSearch.Trim.Length > 0 Then
                    strSearch = strSearch.Substring(3)
                End If

                dsList = objPendingLoan.GetList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                CStr(Session("UserAccessModeSetting")), _
                                                True, _
                                                CBool(Session("IsIncludeInactiveEmp")), _
                                                "List", enLoanApplicationStatus.APPROVED, strSearch)
                'Nilay (04-Nov-2016) -- Start
                'Enhancements: Global Change Status feature requested by {Rutta}
                'If dsList.Tables("List").Columns.Contains("IsChecked") = False Then
                '    dsList.Tables("List").Columns.Add("IsChecked", Type.GetType("System.Boolean")).DefaultValue = False
                'End If
                ' mdtTable = New DataView(dsList.Tables("List"), "", "processpendingloanunkid desc", DataViewRowState.CurrentRows).ToTable
                'mdtTable = dsList.Tables("List")
                mdtTable = New DataView(dsList.Tables("List"), "", "processpendingloanunkid desc", DataViewRowState.CurrentRows).ToTable
                If mdtTable.Columns.Contains("IsChecked") = False Then
                    mdtTable.Columns.Add("IsChecked", Type.GetType("System.Boolean")).DefaultValue = False
                    mdtTable.AcceptChanges()
                End If
                'Nilay (04-Nov-2016) -- End

                dgvLoanApproved.AutoGenerateColumns = False
                dgvLoanApproved.DataSource = mdtTable
                dgvLoanApproved.DataBind()

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CInt(cboStatus.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Status is mandatory information. Please select atleast one status from the list."), Me)
                cboStatus.Focus()
                Return False
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                If txtRemarks.Text.Trim = "" Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Remarks cannot be blank. Please specify remarks."), Me)
                    txtRemarks.Focus()
                    Return False
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

#End Region

#Region " Button's Events "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim enMailType As New clsProcess_pending_loan.enApproverEmailType
        Try
            If dgvLoanApproved.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "No Records found."), Me)
                Exit Sub
            End If

            If mdtTable Is Nothing Then Exit Sub

            Dim dRow As DataRow() = mdtTable.Select("IsChecked=1")

            If dRow.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Please check atleast one record for further process."), Me)
                dgvLoanApproved.Focus()
                Exit Sub
            End If

            If IsValidate() = True Then

                For i As Integer = 0 To dRow.Length - 1

                    objPendingLoan._Processpendingloanunkid = CInt(dRow(i).Item("processpendingloanunkid"))
                    objPendingLoan._Loan_Statusunkid = CInt(cboStatus.SelectedValue)
                    objPendingLoan._Remark = txtRemarks.Text.Trim.ToString
                    objPendingLoan._Userunkid = CInt(Session("UserId"))

                    If objPendingLoan.Update(False) = False Then
                        DisplayMessage.DisplayMessage(objPendingLoan._Message, Me)
                        Exit Sub
                    End If

                    'Nilay (10-Dec-2016) -- Start
                    'Issue #26: Setting to be included on configuration for Loan flow Approval process
                    If mblnIsSendEmailNotification = True Then
                        If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.CANCELLED Then

                            If CBool(dRow(i).Item("isloan")) = True Then
                                enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Approver
                            ElseIf CBool(dRow(i).Item("isloan")) = False Then
                                enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Advance
                            End If

                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objPendingLoan.Send_Notification_Employee(CInt(dRow(i).Item("employeeunkid")), _
                            '                                      CInt(dRow(i).Item("processpendingloanunkid")), _
                            '                                      clsProcess_pending_loan.enNoticationLoanStatus.CANCELLED, _
                            '                                      enMailType, _
                            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                            '                                      txtRemarks.Text.Trim, _
                            '                                      enLogin_Mode.MGR_SELF_SERVICE, 0, _
                            '                                      CInt(Session("UserId")))
                            objPendingLoan.Send_Notification_Employee(CInt(dRow(i).Item("employeeunkid")), _
                                                                 CInt(dRow(i).Item("processpendingloanunkid")), _
                                                                 clsProcess_pending_loan.enNoticationLoanStatus.CANCELLED, _
                                                                 enMailType, _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("CompanyUnkId")), _
                                                                 txtRemarks.Text.Trim, _
                                                                 enLogin_Mode.MGR_SELF_SERVICE, 0, _
                                                                 CInt(Session("UserId")))
                            'Sohail (30 Nov 2017) -- End
                        End If
                    End If
                    'Nilay (10-Dec-2016) -- End
                Next
                Call FillList()
                txtRemarks.Text = ""
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedValue = CStr(0)
            cboLoanScheme.SelectedValue = CStr(0)
            cboLoanAdvance.SelectedIndex = 0
            cboAmountCondition.SelectedIndex = 0
            txtApplicationNo.Text = ""
            txtApprovedAmount.Text = "0"
            dtpFromDate.SetDate = Nothing
            dtpToDate.SetDate = Nothing
            txtRemarks.Text = ""
            mstrAdvanceFilter = ""

            Call FillList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Application/wPg_LoanApplicationList.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAdvanceFilter_buttonApply_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " LinkButton's Events "

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "emp."
            popupAdvanceFilter.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAllocation_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " DataGrid's Events "

    Protected Sub dgvLoanApproved_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvLoanApproved.ItemDataBound
        Try
            Call SetDateFormat()

            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then

                If e.Item.Cells(2).Text.ToString().Trim <> "" AndAlso e.Item.Cells(2).Text.Trim <> "&nbsp;" Then
                    e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).Date.ToShortDateString
                End If

                e.Item.Cells(6).Text = Format(CDec(e.Item.Cells(6).Text), Session("fmtCurrency").ToString)
                e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), Session("fmtCurrency").ToString)

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvLoanApproved_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkAll As CheckBox = CType(sender, CheckBox)

            If dgvLoanApproved.Items.Count <= 0 Then Exit Sub

            For Each item As DataGridItem In dgvLoanApproved.Items
                CType(item.Cells(0).FindControl("chkSelect"), CheckBox).Checked = chkAll.Checked
                'Nilay (04-Nov-2016) -- Start
                'Enhancements: Global Change Status feature requested by {Rutta}
                'Dim dRow As DataRow() = mdtTable.Select("employeeunkid=" & item.Cells(11).Text)
                Dim dRow As DataRow() = mdtTable.Select("processpendingloanunkid=" & item.Cells(13).Text)
                'Nilay (04-Nov-2016) -- End

                If dRow.Length > 0 Then
                    dRow(0).Item("IsChecked") = chkAll.Checked
                End If
            Next
            mdtTable.AcceptChanges()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelectAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkSelect As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkSelect.NamingContainer, DataGridItem)

            'Nilay (04-Nov-2016) -- Start
            'Enhancements: Global Change Status feature requested by {Rutta}
            'Dim dRow As DataRow() = mdtTable.Select("employeeunkid=" & CInt(item.Cells(11).Text))
            Dim dRow As DataRow() = mdtTable.Select("processpendingloanunkid=" & CInt(item.Cells(13).Text))
            'Nilay (04-Nov-2016) -- End
            If dRow.Length > 0 Then
                dRow(0).Item("IsChecked") = chkSelect.Checked
            End If

            mdtTable.AcceptChanges()

            Dim xCount As DataRow() = mdtTable.Select("IsChecked=1")
            If xCount.Length = dgvLoanApproved.Items.Count Then
                CType(dgvLoanApproved.Controls(0).Controls(0).FindControl("chkSelectAll"), CheckBox).Checked = True
            Else
                CType(dgvLoanApproved.Controls(0).Controls(0).FindControl("chkSelectAll"), CheckBox).Checked = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkSelect_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub SetLanguage()
        Try
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbFilterCriteria", Me.lblDetialHeader.Text)



            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")

            Me.gbInfo.Text = Language._Object.getCaption(Me.gbInfo.ID, Me.gbInfo.Text)

            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.ID, Me.lblRemarks.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.lblApprovedAmount.Text = Language._Object.getCaption(Me.lblApprovedAmount.ID, Me.lblApprovedAmount.Text)
            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.ID, Me.lblLoanAdvance.Text)
            Me.lblApplicationNo.Text = Language._Object.getCaption(Me.lblApplicationNo.ID, Me.lblApplicationNo.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.ID, Me.lblTo.Text)
            Me.lblApplicationDate.Text = Language._Object.getCaption(Me.lblApplicationDate.ID, Me.lblApplicationDate.Text)

            Me.dgvLoanApproved.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvLoanApproved.Columns(1).FooterText, Me.dgvLoanApproved.Columns(1).HeaderText)
            Me.dgvLoanApproved.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvLoanApproved.Columns(2).FooterText, Me.dgvLoanApproved.Columns(2).HeaderText)
            Me.dgvLoanApproved.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvLoanApproved.Columns(3).FooterText, Me.dgvLoanApproved.Columns(3).HeaderText)
            Me.dgvLoanApproved.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvLoanApproved.Columns(4).FooterText, Me.dgvLoanApproved.Columns(4).HeaderText)
            Me.dgvLoanApproved.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvLoanApproved.Columns(5).FooterText, Me.dgvLoanApproved.Columns(5).HeaderText)
            Me.dgvLoanApproved.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvLoanApproved.Columns(6).FooterText, Me.dgvLoanApproved.Columns(6).HeaderText)
            Me.dgvLoanApproved.Columns(7).HeaderText = Language._Object.getCaption(Me.dgvLoanApproved.Columns(7).FooterText, Me.dgvLoanApproved.Columns(7).HeaderText)
            Me.dgvLoanApproved.Columns(8).HeaderText = Language._Object.getCaption(Me.dgvLoanApproved.Columns(8).FooterText, Me.dgvLoanApproved.Columns(8).HeaderText)


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

End Class
