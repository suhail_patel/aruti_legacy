﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_LoanApplicationList.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Application_wPg_LoanApplicationList" MaintainScrollPositionOnPostback="true"
    Title="Loan Application List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/OperationButton.ascx" TagName="OperationButton" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>

    <script type="text/javascript">
    function onlyNumbers(txtBox, e) {
        
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
            if (charCode == 46)
            if (cval.indexOf(".") > -1)
            return false;

        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;

        return true;
    }
  
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Loan Application List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="float: right; font-weight: bold; text-align: right; font-size: 12px">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 9%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblLoanAdvance" Style="margin-left: 10px" runat="server" Text="Loan/Advance">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:DropDownList ID="cboLoanAdvance" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblAppNo" runat="server" Style="margin-left: 10px" Text="App. No.">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:TextBox ID="txtApplicationNo" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblAmount" runat="server" Style="margin-left: 10px" Text="Amount">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:TextBox ID="txtAmount" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0"></asp:TextBox>
                                            </td>
                                            <td style="width: 4%">
                                                <asp:DropDownList ID="cboAmountCondition" Style="margin-left: 5px" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 9%">
                                                <asp:Label ID="lblLoanScheme" runat="server" Text="Loan Scheme">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboLoanScheme" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblStatus" Style="margin-left: 10px" runat="server" Text="Status">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 13%">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblAppDate" runat="server" Style="margin-left: 10px" Text="App. Date">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 12%">
                                                <uc1:DateCtrl ID="dtpAppDate" runat="server" Width="90" AutoPostBack="false" />
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblApprovedAmount" runat="server" Style="margin-left: 10px" Text="App. Amount">
                                                </asp:Label>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:TextBox ID="txtApprovedAmount" runat="server" Style="text-align: right" onKeypress="return onlyNumbers(this, event);"
                                                    Text="0"></asp:TextBox>
                                            </td>
                                            <td style="width: 4%">
                                                <asp:DropDownList ID="cboAppAmountCondition" Style="margin-left: 5px" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <div style="float: left">
                                            <uc3:OperationButton ID="btnOperation" runat="server" Text="Operation" TragateControlId="data" />
                                            <div id="data" style="display: none">
                                                <asp:LinkButton ID="lnkGlobalApprove" runat="server" Text="Global Approve"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkGlobalCancelApproved" runat="server" Text="Global Cancel Approved"></asp:LinkButton>
                                                <asp:LinkButton ID="lnkGlobalAssign" runat="server" Text="Global Assign"></asp:LinkButton>
                                            </div>
                                        </div>
                                        <%--<div style="float: left">
                                            <asp:Button ID="btnGlobalApprove" runat="server" Text="Global Approve" CssClass="btndefault" />
                                            <asp:Button ID="btnGlobalAssign" runat="server" Text="Global Assign" CssClass="btndefault" />
                                        </div>--%>
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <div id="scrollable-container" class="gridscroll" style="vertical-align: top; overflow: auto;
                                                    max-height: 400px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                    <asp:DataGrid ID="dgvLoanApplicationList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" Width="125%">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="brnEdit">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="btnDelete">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                            CommandName="Delete" CssClass="griddelete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="60px" ItemStyle-Width="60px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="btnDelete">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="lnkAssign" Style="text-align: center; font-weight: bold" runat="server"
                                                                            ToolTip="Assign" CommandName="Assign" Font-Underline="false" Text="Assign"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="Application_No" HeaderText="Application No" ReadOnly="true"
                                                                FooterText="dgcolhApplicationNo" />
                                                            <asp:BoundColumn DataField="applicationdate" HeaderText="App Date" FooterText="dgcolhAppDate" />
                                                            <asp:BoundColumn DataField="EmpName" HeaderText="Employee" ReadOnly="true" FooterText="dgcolhEmployee" />
                                                            <asp:BoundColumn DataField="LoanScheme" HeaderText="Loan Scheme" ReadOnly="true"
                                                                FooterText="dgcolhLoanScheme" />
                                                            <asp:BoundColumn DataField="Loan_Advance" HeaderText="Loan/Advance" ReadOnly="true"
                                                                FooterText="dgcolhLoan_Advance" />
                                                            <asp:BoundColumn DataField="Amount" HeaderText="Loan Amount" HeaderStyle-HorizontalAlign="Right"
                                                                ItemStyle-HorizontalAlign="Right" ReadOnly="true" FooterText="dgcolhAmount" />
                                                            <asp:BoundColumn DataField="Approved_Amount" HeaderText="Approved Amount" HeaderStyle-HorizontalAlign="Right"
                                                                ItemStyle-HorizontalAlign="Right" ReadOnly="true" FooterText="dgcolhApp_Amount" />
                                                            <asp:BoundColumn DataField="LoanStatus" HeaderText="Status" ReadOnly="true" FooterText="dgcolhStatus" />
                                                            <asp:BoundColumn DataField="isloan" HeaderText="IsLoan" ReadOnly="true" Visible="false"
                                                                FooterText="objdgcolhIsLoan" />
                                                            <asp:BoundColumn DataField="loan_statusunkid" HeaderText="Statusunkid" ReadOnly="true"
                                                                Visible="false" FooterText="objdgcolhStatusunkid" />
                                                            <asp:BoundColumn DataField="employeeunkid" HeaderText="EmpID" ReadOnly="true" Visible="false"
                                                                FooterText="objdgcolhEmpId" />
                                                            <asp:BoundColumn DataField="loanschemeunkid" HeaderText="SchemeID" ReadOnly="true"
                                                                Visible="false" FooterText="objdgcolhSchemeID" />
                                                            <asp:BoundColumn DataField="approverunkid" HeaderText="Approver" ReadOnly="true"
                                                                Visible="false" FooterText="objdgcolhApprover" />
                                                            <asp:BoundColumn DataField="isexternal_entity" HeaderText="IsExternal" ReadOnly="true"
                                                                Visible="false" FooterText="objdgcolhIsExternal" />
                                                            <asp:BoundColumn DataField="processpendingloanunkid" HeaderText="" ReadOnly="true"
                                                                Visible="false" FooterText="objdgcolhprocesspendingloanunkid" />
                                                            <asp:BoundColumn DataField="remark" HeaderText="Cancel Remarks" ReadOnly="true" FooterText="dgcolhCancelRemark" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc2:DeleteReason ID="popDeleteReason" runat="server" />
                    <uc4:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
