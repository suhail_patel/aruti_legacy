﻿Option Strict On
#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Net.Dns
#End Region

Partial Class Loan_Savings_New_Loan_Loan_Assignment_wPg_NewLoanAdvance_AddEdit
    Inherits Basepage

#Region "Private Variables"
    Public Shared ReadOnly mstrModuleName As String = "frmNewLoanAdvance_AddEdit"

    Dim DisplayMessage As New CommonCodes

    Private objLoan_Advance As New clsLoan_Advance
    Private objlnInterest As New clslnloan_interest_tran
    Private objlnEMI As New clslnloan_emitenure_tran
    Private objlnTopUp As New clslnloan_topup_tran

    Private mintPendingApprovalTranID As Integer = -1
    Private mintEmployeeID As Integer = -1
    Private mintApprovertranunkid As Integer = -1
    Private mintLoanAdvanceUnkid As Integer = -1
    Private mintProcessPendingLoanUnkid As Integer = -1
    Private mintCurrOpenPeriodId As Integer = -1
    Private mintLastCalcTypeId As Integer = 0
    Private mintItemIndex As Integer = -1
    Private mintBaseCurrId As Integer = -1
    Private mintPaidCurrId As Integer = -1
    Private mintNoOfInstallment As Integer = 0

    Private mdecBaseExRate As Decimal
    Private mdecPaidExRate As Decimal
    Private mdecInstallmentAmnt As Decimal = 0
    Private mdecNetAmount As Decimal = 0
    'Sohail (28 Jun 2018) -- Start
    'Support Issue Id # 0002352 - Loan deducts only interest and not the whole installment amount, omits principal amount yet it shows as assigned in 72.1.
    Private mdecInterestAmt As Decimal = 0
    'Sohail (28 Jun 2018) -- End

    Private mdtInterest As New DataTable
    Private mdtEMI As New DataTable
    Private mdtTopup As New DataTable

    Private mblnIsFormLoan As Boolean = False
    Private mblnIsValidDeductionPeriod As Boolean = False
    Private mblnIsExternalEntity As Boolean = False
    Private mblnAllowChangePeriod As Boolean = False

    Private mstrCommandName As String = String.Empty
    Private mstrBaseCurrSign As String = ""
    'Nilay (25-Mar-2016) -- Start
    Private mdtStartDate As Date
    Private mdtEndDate As Date
    'Nilay (25-Mar-2016) -- End
    'Shani (21-Jul-2016) -- Start
    'Enhancement - Create New Loan Notification 
    Private objCONN As SqlConnection
    'Shani (21-Jul-2016) -- End
    'Nilay (21-Jul-2016) -- Start
    'Enhancement - Create New Loan Notification 
    Private mblnIsFromEdit As Boolean = False
    'Nilay (21-Jul-2016) -- End

    'Nilay (10-Dec-2016) -- Start
    'Issue #26: Setting to be included on configuration for Loan flow Approval process
    Private mblnIsSendEmailNotification As Boolean = False
    'Nilay (10-Dec-2016) -- End

    'Hemant (29 Jan 2022) -- Start            
    Private mdecExchangerate As Decimal
    'Hemant (29 Jan 2022) -- End

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                'Sohail (02 Apr 2019) -- Start
                'NMB Issue - 74.1 - Error "On Load Event !! Bad Data" on clicking any page with link after session get expired.
                If Request.QueryString.Count <= 0 Then Exit Sub
                'Sohail (02 Apr 2019) -- End
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                mintPendingApprovalTranID = CInt(arr(0))
                mintEmployeeID = CInt(arr(1))
                If arr.Length > 0 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try
                    Blank_ModuleName()
                    clsCommonATLog._WebFormName = "frmPerformanceEvaluation"
                    StrModuleName2 = "mnuAssessment"
                    StrModuleName3 = "mnuPerformaceEvaluation"
                    clsCommonATLog._WebClientIP = Session("IP_ADD").ToString
                    clsCommonATLog._WebHostName = Session("HOST_NAME").ToString
                    Me.ViewState.Add("IsDirect", True)

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(2))
                    HttpContext.Current.Session("UserId") = CInt(arr(3))

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ''ArtLic._Object = New ArutiLic(False)
                    ''If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                    ''    Dim objGroupMaster As New clsGroup_Master
                    ''    objGroupMaster._Groupunkid = 1
                    ''    ArtLic._Object.HotelName = objGroupMaster._Groupname
                    ''End If

                    ''If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                    ''    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                    ''    Exit Sub
                    ''End If

                    ''If ConfigParameter._Object._IsArutiDemo Then
                    ''    If ConfigParameter._Object._IsExpire Then
                    ''        DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                    ''        Exit Try
                    ''    Else
                    ''        If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                    ''            DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                    ''            Exit Try
                    ''        End If
                    ''    End If
                    ''End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If
                    Session("LoanApprover_ForLoanScheme") = clsConfig._IsLoanApprover_ForLoanScheme
                    Session("_LoanVocNoType") = clsConfig._LoanVocNoType

                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    Session("DateFormat") = clsConfig._CompanyDateFormat
                    Session("DateSeparator") = clsConfig._CompanyDateSeparator
                    Call SetDateFormat()
                    'Pinkal (16-Apr-2016) -- End

                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))

                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName

                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If
                    'Dim objUserPrivilege As New clsUserPrivilege
                    'objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                    'Session("AllowtoAddReviewerEvaluation") = objUserPrivilege._AllowtoAddReviewerEvaluation
                    Dim dsList As DataSet = Nothing
                    dsList = (New clsloanapproval_process_Tran).GetApprovalTranList(Session("Database_Name").ToString, _
                                                                                    CInt(Session("UserId")), _
                                                                                    CInt(Session("Fin_year")), _
                                                                                    CInt(Session("CompanyUnkId")), _
                                                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                                    CStr(Session("UserAccessModeSetting")), True, _
                                                                                    CBool(Session("IsIncludeInactiveEmp")), "List", _
                                                                                    mintEmployeeID)
                    'employeeunkid CInt
                    'ProcessPendingunkid
                    Dim dRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("pendingloantranunkid") = mintPendingApprovalTranID)
                    If CInt(dRow(0).Item("loan_statusunkid")) = 1 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "You can't Edit this Loan detail. Reason: This Loan can not be approve."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    ElseIf CInt(dRow(0).Item("loan_statusunkid")) = 3 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "You can't Edit this Loan detail. Reason: This Loan is already rejected."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    ElseIf CInt(dRow(0).Item("loan_statusunkid")) = 4 Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "You can't Edit this Loan detail. Reason: This Loan is already assign."), Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    GoTo Link
                End If

            End If
            'Shani (21-Jul-2016) -- End

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Loan_and_Savings_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
Link:
            If IsPostBack = False Then

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromDesktopMSS"))
                ElseIf CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee Then
                    mblnIsSendEmailNotification = CBool(Session("SendLoanEmailFromESS"))
                End If
                'Nilay (10-Dec-2016) -- End

                Call SetLanguage()

                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                'If Request.QueryString.Count > 0 Then
                '    Dim array() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                '    If array.Length = 2 Then
                '        mintPendingApprovalTranID = CInt(array(0)) 'For Assign(ADD) Mode
                '        mintEmployeeID = CInt(array(1))
                '    ElseIf array.Length = 1 Then
                '        mintLoanAdvanceUnkid = CInt(array(0)) 'For Edit Mode
                '    End If
                'End If
                If Session("PendingApprovalTranID") IsNot Nothing Then
                    mintPendingApprovalTranID = CInt(Session("PendingApprovalTranID"))
                    Session("PendingApprovalTranID") = Nothing
                    End If
                If Session("EmployeeID") IsNot Nothing Then
                    mintEmployeeID = CInt(Session("EmployeeID"))
                    Session("EmployeeID") = Nothing
                End If
                If Session("LoanAdvanceunkid") IsNot Nothing Then
                    mintLoanAdvanceUnkid = CInt(Session("LoanAdvanceunkid"))
                    Session("LoanAdvanceunkid") = Nothing
                End If
                'Nilay (06-Aug-2016) -- END

                'Nilay (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                If Session("IsFromEdit") IsNot Nothing Then
                    mblnIsFromEdit = CBool(Session("IsFromEdit"))
                End If
                'Nilay (21-Jul-2016) -- End

                If mintLoanAdvanceUnkid < 0 Then
                    pnl_LoanTopupInfo.Visible = False
                    cboCurrency.Enabled = False
                End If

                Call SetProcessVisibility()
                Call FillCombo()

                'Call FillComboLoanPara()
                'cboPeriod.Enabled = True

                If mintLoanAdvanceUnkid > 0 Then
                    objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceUnkid
                    fpnlType.Enabled = False
                    cboDeductionPeriod.Enabled = False
                    cboPayPeriod.Enabled = False
                    cboCurrency.Enabled = False
                    cboLoanCalcType.Enabled = False
                    cboInterestCalcType.Enabled = False
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    cboMappedHead.Enabled = False
                    'Sohail (29 Apr 2019) -- End

                    Call ShowHideProjectedLoan()

                    If objLoan_Advance._Isloan = False Then
                        pnl_LoanInterestInfo.Visible = False
                        pnl_LoanEMIInfo.Visible = False
                        pnl_LoanTopupInfo.Visible = False
                    End If
                End If

                'objlnInterest._Loanadvancetranunkid = mintLoanAdvanceUnkid
                'mdtInterest = objlnInterest._DataTable.Copy

                'objlnEMI._Loanadvancetranunkid = mintLoanAdvanceUnkid
                'mdtEMI = objlnEMI._DataTable.Copy

                'objlnTopUp._Loanadvancetranunkid = mintLoanAdvanceUnkid
                'mdtTopup = objlnTopUp._DataTable.Copy

                'Call FillComboLoanPara()
                Call GetValue()
                'If dgvHistory.Items.Count <= 0 Then
                '    dgvHistory.DataSource = New List(Of String)
                '    dgvHistory.DataBind()
                'End If
                'Call FillHistoryList()
                Call Fill_Transaction_Grids()

                Call cboDeductionPeriod_SelectedIndexChanged(Nothing, Nothing)
                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 50
                If mintLoanAdvanceUnkid <= 0 Then
                    Call cboLoanCalcType_SelectedIndexChanged(Nothing, Nothing)
                End If
                'S.SANDEEP [20-SEP-2017] -- END
                txtVoucherNo.Focus()
                mblnIsFormLoan = True

            Else
                mintPendingApprovalTranID = CInt(Me.ViewState("PendingApprovalTranID"))
                mintEmployeeID = CInt(Me.ViewState("EmployeeID"))
                mintApprovertranunkid = CInt(Me.ViewState("Approvertranunkid"))
                mintLoanAdvanceUnkid = CInt(Me.ViewState("LoanAdvanceUnkid"))
                mintProcessPendingLoanUnkid = CInt(Me.ViewState("ProcessPendingLoanUnkid"))
                mintCurrOpenPeriodId = CInt(Me.ViewState("CurrOpenPeriodId"))
                mintLastCalcTypeId = CInt(Me.ViewState("LastCalcTypeId"))
                mintItemIndex = CInt(Me.ViewState("ItemIndex"))
                mintBaseCurrId = CInt(Me.ViewState("BaseCurrId"))
                mintNoOfInstallment = CInt(Me.ViewState("NoOfInstallment"))
                mdecInstallmentAmnt = CDec(Me.ViewState("InstallmentAmount"))
                mdecNetAmount = CDec(Me.ViewState("NetAmount"))
                'Sohail (28 Jun 2018) -- Start
                'Support Issue Id # 0002352 - Loan deducts only interest and not the whole installment amount, omits principal amount yet it shows as assigned in 72.1.
                mdecInterestAmt = CDec(Me.ViewState("mdecInterestAmt"))
                'Sohail (28 Jun 2018) -- End
                mdecPaidExRate = CDec(Me.ViewState("PaidExRate"))
                mblnIsValidDeductionPeriod = CBool(Me.ViewState("IsValidDeductionPeriod"))
                mblnIsExternalEntity = CBool(Me.ViewState("IsExternalEntity"))
                mblnIsFormLoan = CBool(Me.ViewState("IsFromLoan"))
                'mblnAllowChangePeriod = Me.ViewState("AllowChangePeriod")
                mdtInterest = CType(Me.Session("InterestTable"), DataTable)
                mdtEMI = CType(Me.Session("EMITable"), DataTable)
                mdtTopup = CType(Me.Session("TopupTable"), DataTable)
                mstrCommandName = Me.ViewState("CommandName").ToString
                mstrBaseCurrSign = Me.ViewState("BaseCurrSign").ToString
                'Nilay (25-Mar-2016) -- Start
                mdtStartDate = CDate(Me.ViewState("PeriodStartDate"))
                mdtEndDate = CDate(Me.ViewState("PeriodEndDate"))
                'Nilay (25-Mar-2016) -- End

                'Nilay (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                mblnIsFromEdit = CBool(Me.ViewState("IsFromEdit"))
                'Nilay (21-Jul-2016) -- End

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                mblnIsSendEmailNotification = CBool(Me.ViewState("SendEmailNotification"))
                'Nilay (10-Dec-2016) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load1:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("PendingApprovalTranID") = mintPendingApprovalTranID
            Me.ViewState("EmployeeID") = mintEmployeeID
            Me.ViewState("Approvertranunkid") = mintApprovertranunkid
            Me.ViewState("LoanAdvanceUnkid") = mintLoanAdvanceUnkid
            Me.ViewState("ProcessPendingLoanUnkid") = mintProcessPendingLoanUnkid
            Me.ViewState("CurrOpenPeriodId") = mintCurrOpenPeriodId
            Me.ViewState("LastCalcTypeId") = mintLastCalcTypeId
            Me.ViewState("ItemIndex") = mintItemIndex
            Me.ViewState("BaseCurrId") = mintBaseCurrId
            Me.ViewState("NoOfInstallment") = mintNoOfInstallment
            Me.ViewState("InstallmentAmount") = mdecInstallmentAmnt
            Me.ViewState("NetAmount") = mdecNetAmount
            'Sohail (28 Jun 2018) -- Start
            'Support Issue Id # 0002352 - Loan deducts only interest and not the whole installment amount, omits principal amount yet it shows as assigned in 72.1.
            Me.ViewState("mdecInterestAmt") = mdecInterestAmt
            'Sohail (28 Jun 2018) -- End
            Me.ViewState("PaidExRate") = mdecPaidExRate
            Me.ViewState("IsValidDeductionPeriod") = mblnIsValidDeductionPeriod
            Me.ViewState("IsExternalEntity") = mblnIsExternalEntity
            Me.ViewState("IsFromLoan") = mblnIsFormLoan
            'Me.ViewState.Add("AllowChangePeriod", mblnAllowChangePeriod)
            Me.Session("InterestTable") = mdtInterest
            Me.Session("EMITable") = mdtEMI
            Me.Session("TopupTable") = mdtTopup
            Me.ViewState("CommandName") = mstrCommandName
            Me.ViewState("BaseCurrSign") = mstrBaseCurrSign
            'Nilay (25-Mar-2016) -- Start
            Me.ViewState("PeriodStartDate") = mdtStartDate
            Me.ViewState("PeriodEndDate") = mdtEndDate
            'Nilay (25-Mar-2016) -- End

            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Me.ViewState("IsFromEdit") = mblnIsFromEdit
            'Nilay (21-Jul-2016) -- End

            'Nilay (10-Dec-2016) -- Start
            'Issue #26: Setting to be included on configuration for Loan flow Approval process
            Me.ViewState("SendEmailNotification") = mblnIsSendEmailNotification
            'Nilay (10-Dec-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

#End Region

#Region "Private Methods"

    Private Sub SetProcessVisibility()
        Try
            If CInt(Session("_LoanVocNoType")) = 1 Then
                txtVoucherNo.Enabled = False
            Else
                txtVoucherNo.Enabled = True
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetProcessVisibility:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub ShowHideProjectedLoan()
        Try
            If mintLoanAdvanceUnkid > 0 Then
                lnProjectedAmount.Visible = False
                pnlProjectedAmt.Visible = False
                pnlInformation.Visible = False
                elProjectedINSTLAmt.Visible = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ShowHideProjectedLoan:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objHead As New clsTransactionHead 'Sohail (29 Apr 2019)
        Try
            Dim dsList As DataSet = Nothing
            Dim objPeriod As New clscommom_period_Tran
            Dim objExRate As New clsExchangeRate

            Dim objEmployee As New clsEmployee_Master

            'Nilay (06-Aug-2016) -- Start
            'CHANGES : Replace Query String with Session and ViewState
            'dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
            '                                     CInt(Session("CompanyUnkId")), _
            '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                     Session("UserAccessModeSetting").ToString, True, False, _
            '                                     "Employee", False, mintEmployeeID)
            dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                 Session("UserAccessModeSetting").ToString, True, False, _
                                                 "Employee", True, mintEmployeeID)
            'Nilay (06-Aug-2016) -- END

            With cboEmpName
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataValueField = "employeeunkid"
                .DataSource = dsList.Tables("Employee")
                'Nilay (06-Aug-2016) -- Start
                'CHANGES : Replace Query String with Session and ViewState
                If mintEmployeeID > 0 Then
                    .SelectedValue = mintEmployeeID.ToString
                Else
                    .SelectedValue = "0"
                End If
                'Nilay (06-Aug-2016) -- END
                .DataBind()
            End With
            objEmployee = Nothing

            Dim objLoanScheme As New clsLoan_Scheme
            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objLoanScheme.getComboList(True, "LoanScheme")
            dsList = objLoanScheme.getComboList(True, "LoanScheme", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            With cboLoanScheme
                .DataTextField = "name"
                .DataValueField = "loanschemeunkid"
                .DataSource = dsList.Tables("LoanScheme")
                .DataBind()
                .SelectedValue = "0"
            End With
            objLoanScheme = Nothing

            If mintLoanAdvanceUnkid < 0 Then
                dsList = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                   CInt(Session("Fin_year")), _
                                                   Session("Database_Name").ToString, _
                                                   CDate(Session("fin_startdate")), _
                                                   "PeriodList", True, 1)
            Else
                dsList = objPeriod.getListForCombo(enModuleReference.Payroll, 0, _
                                                   Session("Database_Name").ToString, _
                                                   CDate(Session("fin_startdate")), _
                                                   "PeriodList", True)
            End If
            With cboPayPeriod
                .DataTextField = "name"
                .DataValueField = "periodunkid"
                .DataSource = dsList.Tables("PeriodList")
                .DataBind()
                .SelectedValue = "0"
            End With

            With cboDeductionPeriod
                .DataTextField = "name"
                .DataValueField = "periodunkid"
                .DataSource = dsList.Tables("PeriodList")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objExRate.getComboList("ExRate", False)
            With cboCurrency
                .DataTextField = "currency_sign"
                .DataValueField = "countryunkid"
                .DataSource = dsList.Tables("ExRate")
                .DataBind()
                .SelectedValue = "0"
            End With

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsList.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                End If
            End If

            dsList = objLoan_Advance.GetLoanCalculationTypeList("List", True)
            With cboLoanCalcType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsList = objLoan_Advance.GetLoan_Interest_Calculation_Type("List", True)
            With cboInterestCalcType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, 0, 0, -1, False, False, "calctype_id <> " & CInt(enCalcType.NET_PAY) & " ")
            With cboMappedHead
                .DataValueField = "tranheadunkid"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Sohail (29 Apr 2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
        Finally
            objHead = Nothing
            'Sohail (29 Apr 2019) -- End
        End Try
    End Sub

    Private Sub Fill_InterestRate_Grid()
        Try
            mdtInterest = objlnInterest.GetList("List", mintLoanAdvanceUnkid).Tables(0)

            dgvInterestHistory.AutoGenerateColumns = False

            If mdtInterest.Rows.Count <= 0 Then
                dgvInterestHistory.DataSource = New List(Of String)
                dgvInterestHistory.DataBind()
            Else
                dgvInterestHistory.DataSource = mdtInterest
                dgvInterestHistory.DataBind()
            End If

            'Dim dtRow() As DataRow = mdtInterest.Select("AUD <> 'D' AND periodunkid > 0 ")
            'If dtRow.Length > 0 Then
            '    mdvInterestView.RowFilter = "AUD <> 'D' AND periodunkid > 0 "
            'Else
            '    mdvInterestView.RowFilter = "AUD <> 'D' AND periodunkid <= 0 "
            'End If

            'If mintLoanAdvanceUnkid < 0 Then
            '    Call Calculate_Projected_Loan_Amount()
            'End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_InterestRate_Grid:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Fill_EMI_Transaction_Grid()
        Try
            mdtEMI = objlnEMI.GetList("List", mintLoanAdvanceUnkid).Tables(0)

            dgvEMIHistory.AutoGenerateColumns = False

            If mdtEMI.Rows.Count <= 0 Then
                dgvEMIHistory.DataSource = New List(Of String)
                dgvEMIHistory.DataBind()
            Else
                dgvEMIHistory.DataSource = mdtEMI
                dgvEMIHistory.DataBind()
            End If

            'Dim dtRow() As DataRow = mdtEMI.Select("AUD <> 'D' AND periodunkid > 0 ")
            'If dtRow.Length > 0 Then
            '    mdvEMIView.RowFilter = "AUD <> 'D' AND periodunkid > 0 "
            'Else
            '    mdvEMIView.RowFilter = "AUD <> 'D' AND periodunkid <= 0 "
            'End If

            'If mintLoanAdvanceUnkid < 0 Then
            '    Call Calculate_Projected_Loan_Amount()
            'End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_EMI_Transaction_Grid:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Fill_Topup_Transaction_Grid()
        Try
            mdtTopup = objlnTopUp.GetList("List", mintLoanAdvanceUnkid).Tables(0)

            dgvTopupHistory.AutoGenerateColumns = False

            If mdtTopup.Rows.Count <= 0 Then
                dgvTopupHistory.DataSource = New List(Of String)
                dgvTopupHistory.DataBind()
            Else
                dgvTopupHistory.DataSource = mdtTopup
                dgvTopupHistory.DataBind()
            End If

            'Dim dtRow() As DataRow = mdtTopup.Select("AUD <> 'D' AND periodunkid > 0 ")
            'If dtRow.Length > 0 Then
            '    mdvTopupView.RowFilter = "AUD <> 'D' AND periodunkid > 0 "
            'Else
            '    mdvTopupView.RowFilter = "AUD <> 'D' AND periodunkid <= 0 "
            'End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Topup_Transaction_Grid:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillHistoryList()
        Try
            'Dim dsHistory As New DataSet
            'dsHistory = objLoan_Advance.GetLoanAdvance_History_List(CInt(IIf(cboEmpName.SelectedValue = "", 0, cboEmpName.SelectedValue)), ConfigParameter._Object._CurrentDateAndTime.Date)
            ''Dim dsHistory As DataSet = objLoan_Advance.GetLoanAdvance_History_List(mintLoanAdvanceUnkid)

            'dgvHistory.AutoGenerateColumns = False
            'If dsHistory Is Nothing Then
            '    dgvHistory.DataSource = New List(Of String)
            '    dgvHistory.DataBind()
            'Else
            '    dgvHistory.DataSource = dsHistory.Tables(0)
            '    dgvHistory.DataBind()

            'End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillHistoryList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Fill_Transaction_Grids()
        Try
            Call Fill_InterestRate_Grid()
            Call Fill_EMI_Transaction_Grid()
            Call Fill_Topup_Transaction_Grid()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Transaction_Grids:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Private Sub Calculate_Projected_Loan_Amount()
    '    Try
    '        Dim mdecInterestAmount, mdecNetAmount, mdecRate As Decimal
    '        Dim mintDuration As Integer

    '        mdecInterestAmount = 0
    '        mdecNetAmount = 0
    '        mdecRate = 0
    '        mintDuration = 0

    '        'Dim dRow() As DataRow = Nothing

    '        'If mdtInterest IsNot Nothing Then
    '        '    dRow = mdtInterest.Select("periodunkid = '" & CInt(cboDeductionPeriod.SelectedValue) & "' AND AUD <> 'D'")
    '        '    If dRow.Length > 0 Then
    '        '        mdecRate = CDec(dRow(0).Item("interest_rate"))
    '        '    End If
    '        'End If

    '        'If mdtEMI IsNot Nothing Then
    '        '    dRow = mdtEMI.Select("periodunkid = '" & CInt(cboDeductionPeriod.SelectedValue) & "' AND AUD <> 'D'")
    '        '    If dRow.Length > 0 Then
    '        '        mintDuration = CDec(dRow(0).Item("loan_duration"))
    '        '    End If
    '        'End If

    '        mdecRate = CDec(txtLoanRate.Text)
    '        mintDuration = CInt(txtDuration.Text)

    '        If radSimpleInterest.Checked Then
    '            objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Simple_Interest, CDec(txtLoanAmt.Text), mintDuration, mdecRate, mdecNetAmount, mdecInterestAmount)
    '            txtInterestAmt.Text = CDec(Format(CDec(mdecInterestAmount), Session("fmtCurrency")))
    '            txtNetAmount.Text = Format(CDec(mdecNetAmount), Session("fmtCurrency"))
    '        ElseIf radReduceBalInterest.Checked Then
    '            objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Reducing_Amount, CDec(txtLoanAmt.Text), mintDuration, mdecRate, mdecNetAmount, mdecInterestAmount)
    '            txtInterestAmt.Text = Format(CDec(mdecInterestAmount), Session("fmtCurrency"))
    '            txtNetAmount.Text = Format(CDec(mdecNetAmount), Session("fmtCurrency"))
    '        ElseIf radNoInterest.Checked Then
    '            txtInterestAmt.Text = 0
    '            txtNetAmount.Text = 0
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("Calculate_Projected_Loan_Amount:- " & ex.Message, Me)
    '    End Try
    'End Sub

    Private Sub Calculate_Projected_Loan_Balance(ByVal eLnCalcType As enLoanCalcId, _
                                                 ByVal eIntRateCalcTypeID As enLoanInterestCalcType, _
                                                 Optional ByVal blnIsDurationChange As Boolean = False)
        Try
            'If eLnCalcType <> enLoanCalcId.No_Interest Then
            Dim decInstallmentAmount As Decimal = 0
            Dim decInstrAmount As Decimal = 0
            Dim dtEndDate As Date = Nothing
            Dim decTotInstallmentAmount As Decimal = 0
            Dim decTotIntrstAmount As Decimal = 0

            dtEndDate = DateAdd(DateInterval.Month, CDec(txtDuration.Text), dtpDate.GetDate)

            Dim objApprovalTran As New clsloanapproval_process_Tran
            Dim objLoanApplication As New clsProcess_pending_loan
            objApprovalTran._Pendingloantranunkid = mintPendingApprovalTranID
            objLoanApplication._Processpendingloanunkid = objApprovalTran._Processpendingloanunkid

            Dim dtFPeriodStart As Date = dtEndDate
            Dim dtFPeriodEnd As Date = dtEndDate

            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPayPeriod.SelectedValue)
                dtFPeriodStart = objPeriod._Start_Date
                dtFPeriodEnd = objPeriod._End_Date
            End If

            objLoan_Advance.Calulate_Projected_Loan_Balance(CDec(txtLoanAmt.Text) _
                                                          , CInt(DateDiff(DateInterval.Day, dtpDate.GetDate.Date, dtEndDate.Date)) _
                                                          , CDec(txtLoanRate.Text) _
                                                          , eLnCalcType _
                                                          , eIntRateCalcTypeID _
                                                          , CInt(txtDuration.Text) _
                                                          , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
                                                          , Convert.ToDecimal(IIf(eLnCalcType = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, txtPrincipalAmt.Text, txtInstallmentAmt.Text)) _
                                                          , decInstrAmount _
                                                          , decInstallmentAmount _
                                                          , decTotIntrstAmount _
                                                          , decTotInstallmentAmount _
                                                          )
            'Hemant (12 Nov 2021) -- [objApprovalTran._Installmentamt --> Convert.ToDecimal(IIf(eLnCalcType = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, txtPrincipalAmt.Text, txtInstallmentAmt.Text))]
            'Sohail (28 Jun 2018) -- Start
            'Support Issue Id # 0002352 - Loan deducts only interest and not the whole installment amount, omits principal amount yet it shows as assigned in 72.1.
            'mdecInstallmentAmnt = decInstrAmount
            'txtInterestAmt.Text = Format(decInstrAmount, Session("fmtCurrency").ToString)
            mdecInterestAmt = decTotIntrstAmount
            txtInterestAmt.Text = Format(decTotIntrstAmount, Session("fmtCurrency").ToString)
            'Sohail (28 Jun 2018) -- End

            'RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            mdecInstallmentAmnt = decInstallmentAmount
            txtInstallmentAmt.Text = Format(decInstallmentAmount, Session("fmtCurrency").ToString)
            'AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            'Call txtInstallmentAmt_TextChanged(Nothing, Nothing)

            txtPrincipalAmt.Text = Format(decInstallmentAmount - decInstrAmount, Session("fmtCurrency").ToString)
            txtIntAmt.Text = Format(decInstrAmount, Session("fmtCurrency").ToString)

            mdecNetAmount = CDec(decTotInstallmentAmount + decTotIntrstAmount)
            'Sohail (28 Jun 2018) -- Start
            'Support Issue Id # 0002352 - Loan deducts only interest and not the whole installment amount, omits principal amount yet it shows as assigned in 72.1.
            'txtNetAmount.Text = Format(CDec(CDec(txtLoanAmt.Text) + decInstrAmount), Session("fmtCurrency").ToString)
            txtNetAmount.Text = Format(CDec(decTotInstallmentAmount + decTotIntrstAmount), Session("fmtCurrency").ToString)
            'Sohail (28 Jun 2018) -- End

            'Else
            '    Dim intNoOfEMI As Integer = 1
            '    If blnIsDurationChange = False Then
            '        If CDec(txtInstallmentAmt.Text) > 0 Then
            '            intNoOfEMI = CInt(IIf(CDec(txtInstallmentAmt.Text) = 0, 0, CDec(txtLoanAmt.Text) / CDec(txtInstallmentAmt.Text)))
            '        Else
            '            intNoOfEMI = 1
            '        End If

            '        If CInt(CDec(IIf(CDec(txtInstallmentAmt.Text) = 0, 0, CDec(txtLoanAmt.Text) / CDec(txtInstallmentAmt.Text)))) > intNoOfEMI Then
            '            intNoOfEMI += 1
            '        End If

            '        'RemoveHandler nudduration.valuechanged, AddressOf nudduration_valuechanged
            '        txtDuration.Text = intNoOfEMI
            '        'AddHandler nudduration.valuechanged, AddressOf nudduration_valuechanged
            '        Call txtDuration_TextChanged(Nothing, Nothing)

            '    Else
            '        Dim decInstallmentAmt As Decimal
            '        If intNoOfEMI > 0 Then
            '            decInstallmentAmt = CDec(IIf(intNoOfEMI = 0, 0, CDec(txtLoanAmt.Text) / txtDuration.Text))
            '        Else
            '            decInstallmentAmt = 0
            '        End If

            '        'RemoveHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
            '        mdecInstallmentAmnt = decInstallmentAmt
            '        txtInstallmentAmt.Text = CDec(Format(decInstallmentAmt, Session("fmtCurrency")))
            '        'AddHandler txtInstallmentAmt.TextAlignChanged, AddressOf txtInstallmentAmt_TextChanged
            '        Call txtInstallmentAmt_TextChanged(Nothing, Nothing)

            '    End If
            'End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "Calculate_Projected_Loan_Balance", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try

            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            'Dim objApprove As New clsEmployee_Master
            Dim objEmployee As New clsEmployee_Master
            'Nilay (21-Jul-2016) -- End

            If mintPendingApprovalTranID > 0 Then

                dtpDate.SetDate = CDate(Date.Now.ToShortDateString)

                Dim objApprovalTran As New clsloanapproval_process_Tran
                Dim objLoanApplication As New clsProcess_pending_loan
                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 50
                Dim objLScheme As New clsLoan_Scheme
                'S.SANDEEP [20-SEP-2017] -- END
                objApprovalTran._Pendingloantranunkid = mintPendingApprovalTranID
                objLoanApplication._Processpendingloanunkid = objApprovalTran._Processpendingloanunkid
                mintProcessPendingLoanUnkid = objLoanApplication._Processpendingloanunkid
                mblnIsExternalEntity = objLoanApplication._Isexternal_Entity
                txtPurpose.Text = objLoanApplication._Emp_Remark

                If objLoanApplication._Isloan Then
                    radLoan.Checked = True
                    radAdvance.Checked = False
                    cboLoanCalcType.Enabled = True
                    cboInterestCalcType.Enabled = True
                    lblloanamt.Visible = True
                    lblAdvanceAmt.Visible = False
                    txtLoanAmt.Visible = True
                    txtAdvanceAmt.Visible = False
                    pnlInformation.Enabled = True
                    pnlProjectedAmt.Enabled = True

                    'S.SANDEEP [20-SEP-2017] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 50
                    objLScheme._Loanschemeunkid = objLoanApplication._Loanschemeunkid
                    txtLoanRate.Text = CStr(objLScheme._InterestRate)
                    cboLoanCalcType.SelectedValue = CStr(objLScheme._LoanCalcTypeId)
                    cboInterestCalcType.SelectedValue = CStr(objLScheme._InterestCalctypeId)
                    'S.SANDEEP [20-SEP-2017] -- END

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    cboMappedHead.Enabled = True
                    cboMappedHead.SelectedValue = objLScheme._Mapped_TranheadUnkid.ToString
                    'Sohail (29 Apr 2019) -- End

                Else
                    radLoan.Checked = False
                    radAdvance.Checked = True
                    cboLoanCalcType.Enabled = False
                    cboInterestCalcType.Enabled = False
                    lblloanamt.Visible = False
                    lblAdvanceAmt.Visible = True
                    txtLoanAmt.Visible = False
                    txtAdvanceAmt.Visible = True

                    pnlInformation.Enabled = False
                    pnlInformation.Visible = False
                    pnlProjectedAmt.Enabled = False
                    pnlProjectedAmt.Visible = False

                    pnl_LoanInterestInfo.Visible = False
                    pnl_LoanEMIInfo.Visible = False
                    pnl_LoanTopupInfo.Visible = False
                    'lnProjectedAmount.Visible = False
                    'elProjectedINSTLAmt.Visible = False

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    cboMappedHead.Enabled = False
                    'Sohail (29 Apr 2019) -- End
                End If

                fpnlType.Enabled = False
                cboLoanScheme.SelectedValue = CStr(objLoanApplication._Loanschemeunkid)

                'Nilay (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objApprovalTran._Approverempunkid
                'txtApprovedBy.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
                'mintApprovertranunkid = objApprovalTran._Approverempunkid
                Dim objLoanApprover As New clsLoanApprover_master
                Dim objLoanLevel As New clslnapproverlevel_master
                objLoanApprover._lnApproverunkid = objLoanApplication._Approverunkid
                objLoanLevel._lnLevelunkid = objLoanApprover._lnLevelunkid

                If objLoanApprover._IsExternalApprover = True Then
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = objLoanApprover._ApproverEmpunkid
                    Dim strAppName As String = objUser._Username & " - " & objLoanLevel._lnLevelname
                    If strAppName.Trim.Length > 0 Then
                        txtApprovedBy.Text = strAppName
                    Else
                        txtApprovedBy.Text = objUser._Username
                    End If
                    If objUser._Userunkid <= 0 Then
                        txtApprovedBy.Text = ""
                    End If
                    objUser = Nothing
                Else
                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLoanApprover._ApproverEmpunkid
                    txtApprovedBy.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & " - " & objLoanLevel._lnLevelname
                    If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) <= 0 Then
                        txtApprovedBy.Text = ""
                    End If
                End If

                'Varsha Rana (12-Sept-2017) -- Start
                'Enhancement - Loan Topup in ESS
                'mintApprovertranunkid = objApprovalTran._Approverempunkid
                mintApprovertranunkid = objApprovalTran._Approvertranunkid
                ' Varsha Rana (12-Sept-2017) -- End


                'objEmployee = Nothing
                'Nilay (21-Jul-2016) -- End

                cboCurrency.SelectedValue = CStr(objApprovalTran._Countryunkid)

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 50
                'Dim objMaster As New clsMasterData
                'Dim objPeriod As New clscommom_period_Tran
                'Dim intFirstOpenPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, _
                '                                                                 CInt(Session("Fin_year")), _
                '                                                                 StatusType.Open, _
                '                                                                 False, True)

                'objPeriod._Periodunkid(Session("Database_Name").ToString) = objApprovalTran._Deductionperiodunkid
                'If objPeriod._Statusid = StatusType.Close Then
                '    cboPayPeriod.SelectedValue = intFirstOpenPeriodId.ToString
                '    cboDeductionPeriod.SelectedValue = intFirstOpenPeriodId.ToString
                'Else
                '    cboPayPeriod.SelectedValue = CStr(objApprovalTran._Deductionperiodunkid)
                '    cboDeductionPeriod.SelectedValue = CStr(objApprovalTran._Deductionperiodunkid)
                'End If

                'If radLoan.Checked Then
                '    txtLoanAmt.Text = Format(objApprovalTran._Loan_Amount, Session("fmtCurrency").ToString)
                'Else
                '    txtAdvanceAmt.Text = Format(objApprovalTran._Loan_Amount, Session("fmtCurrency").ToString)
                'End If

                'If objLoanApplication._Isloan Then
                '    If CInt(objApprovalTran._Noofinstallment) <= 0 Then
                '        txtDuration.Text = "1"
                '    Else
                '        txtDuration.Text = CStr(objApprovalTran._Noofinstallment)
                '    End If

                '    mintNoOfInstallment = CInt(objApprovalTran._Noofinstallment)
                '    mdecInstallmentAmnt = objApprovalTran._Installmentamt
                '    txtInstallmentAmt.Text = Format(CDec(objApprovalTran._Installmentamt), Session("fmtCurrency").ToString)
                'End If

                If radLoan.Checked Then
                    txtLoanAmt.Text = Format(objApprovalTran._Loan_Amount, Session("fmtCurrency").ToString)
                Else
                    txtAdvanceAmt.Text = Format(objApprovalTran._Loan_Amount, Session("fmtCurrency").ToString)
                End If

                If objLoanApplication._Isloan Then
                    If CInt(objApprovalTran._Noofinstallment) <= 0 Then
                        txtDuration.Text = "1"
                    Else
                        txtDuration.Text = CStr(objApprovalTran._Noofinstallment)
                    End If

                    mintNoOfInstallment = CInt(objApprovalTran._Noofinstallment)
                    mdecInstallmentAmnt = objApprovalTran._Installmentamt
                    txtInstallmentAmt.Text = Format(CDec(objApprovalTran._Installmentamt), Session("fmtCurrency").ToString)
                    'Hemant (12 Nov 2021) -- Start
                    'ISSUE(REA) : on changing No of installment of emi , emi amount should get changed too but it remained unchanged.
                    If CInt(cboLoanCalcType.SelectedValue) = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI) Then
                        txtPrincipalAmt.Text = Format(CDec(objApprovalTran._Installmentamt), Session("fmtCurrency").ToString)
                    End If
                    'Hemant (12 Nov 2021) -- End
                End If

                Dim objMaster As New clsMasterData
                Dim objPeriod As New clscommom_period_Tran
                Dim intFirstOpenPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, _
                                                                                 CInt(Session("Fin_year")), _
                                                                                 StatusType.Open, _
                                                                                 False, True)

                objPeriod._Periodunkid(Session("Database_Name").ToString) = objApprovalTran._Deductionperiodunkid
                If objPeriod._Statusid = StatusType.Close Then
                    cboPayPeriod.SelectedValue = intFirstOpenPeriodId.ToString
                    cboDeductionPeriod.SelectedValue = intFirstOpenPeriodId.ToString
                Else
                    cboPayPeriod.SelectedValue = CStr(objApprovalTran._Deductionperiodunkid)
                    cboDeductionPeriod.SelectedValue = CStr(objApprovalTran._Deductionperiodunkid)
                End If
                'S.SANDEEP [20-SEP-2017] -- END
                'Sohail (28 Jun 2018) -- Start
                'Support Issue Id # 0002352 - Loan deducts only interest and not the whole installment amount, omits principal amount yet it shows as assigned in 72.1.
                Call cboLoanCalcType_SelectedIndexChanged(cboPayPeriod, New System.EventArgs)
                'Sohail (28 Jun 2018) -- End

                objLoanApplication = Nothing
                objApprovalTran = Nothing

            Else
                'Nilay (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objLoan_Advance._Approverunkid
                'txtApprovedBy.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
                'mintApprovertranunkid = objLoan_Advance._Approverunkid
                'objEmployee = Nothing
                Dim objLoanApprover As New clsLoanApprover_master
                Dim objLoanLevel As New clslnapproverlevel_master
                objLoanApprover._lnApproverunkid = objLoan_Advance._Approverunkid
                objLoanLevel._lnLevelunkid = objLoanApprover._lnLevelunkid

                If objLoanApprover._IsExternalApprover = True Then
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = objLoanApprover._ApproverEmpunkid
                    Dim strAppName As String = objUser._Username & " - " & objLoanLevel._lnLevelname
                    If strAppName.Trim.Length > 0 Then
                        txtApprovedBy.Text = strAppName
                    Else
                        txtApprovedBy.Text = objUser._Username
                    End If
                    If objUser._Userunkid <= 0 Then
                        txtApprovedBy.Text = ""
                    End If
                    objUser = Nothing
                Else
                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLoanApprover._ApproverEmpunkid
                    txtApprovedBy.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & " - " & objLoanLevel._lnLevelname
                    If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) <= 0 Then
                        txtApprovedBy.Text = ""
                    End If
                End If
                mintApprovertranunkid = objLoan_Advance._Approverunkid
                'Nilay (21-Jul-2016) -- End

                cboLoanCalcType.SelectedValue = CStr(objLoan_Advance._Calctype_Id)
                cboInterestCalcType.SelectedValue = CStr(objLoan_Advance._Interest_Calctype_Id)
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                cboMappedHead.SelectedValue = objLoan_Advance._Mapped_TranheadUnkid.ToString
                'Sohail (29 Apr 2019) -- End

                If objLoan_Advance._Isloan = True Then
                    radLoan.Checked = True
                Else
                    radAdvance.Checked = True
                End If

                If Not (objLoan_Advance._Effective_Date = Nothing) Then
                    dtpDate.SetDate = objLoan_Advance._Effective_Date
                End If

                cboEmpName.SelectedValue = CStr(objLoan_Advance._Employeeunkid)
                txtInterestAmt.Text = Format(objLoan_Advance._Interest_Amount, Session("fmtCurrency").ToString)
                radLoan.Checked = objLoan_Advance._Isloan
                txtLoanAmt.Text = Format(objLoan_Advance._Loan_Amount, Session("fmtCurrency").ToString)
                txtAdvanceAmt.Text = Format(objLoan_Advance._Advance_Amount, Session("fmtCurrency").ToString)
                txtPurpose.Text = objLoan_Advance._Loan_Purpose
                cboLoanScheme.SelectedValue = CStr(objLoan_Advance._Loanschemeunkid)
                txtVoucherNo.Text = objLoan_Advance._Loanvoucher_No
                txtNetAmount.Text = Format(objLoan_Advance._Net_Amount, Session("fmtCurrency").ToString)
                cboPayPeriod.SelectedValue = CStr(objLoan_Advance._Periodunkid)
                'Sohail (28 Jun 2018) -- Start
                'Support Issue Id # 0002352 - Loan deducts only interest and not the whole installment amount, omits principal amount yet it shows as assigned in 72.1.
                Call cboLoanCalcType_SelectedIndexChanged(cboPayPeriod, New System.EventArgs)
                'Sohail (28 Jun 2018) -- End
                objLoan_Advance._Userunkid = objLoan_Advance._Userunkid
                objLoan_Advance._Voiduserunkid = -1
                objLoan_Advance._Voiddatetime = Nothing
                objLoan_Advance._LoanStatus = objLoan_Advance._LoanStatus
                cboCurrency.SelectedValue = CStr(objLoan_Advance._CountryUnkid)
                cboDeductionPeriod.SelectedValue = CStr(objLoan_Advance._Deductionperiodunkid)
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetValue()
        Try
            'Nilay (25-Mar-2016) -- Start
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPayPeriod.SelectedValue)
            mdtStartDate = objPeriod._Start_Date
            mdtEndDate = objPeriod._End_Date
            'Nilay (25-Mar-2016) -- End
            objLoan_Advance._Effective_Date = dtpDate.GetDate.Date
            objLoan_Advance._Employeeunkid = CInt(cboEmpName.SelectedValue)
            objLoan_Advance._Isbrought_Forward = False
            objLoan_Advance._Loan_Amount = CDec(txtLoanAmt.Text)
            objLoan_Advance._Loanvoucher_No = txtVoucherNo.Text
            objLoan_Advance._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            If mintPendingApprovalTranID > 0 Then
                objLoan_Advance._LoanStatus = 1
            Else
                objLoan_Advance._LoanStatus = objLoan_Advance._LoanStatus
            End If
            objLoan_Advance._Deductionperiodunkid = CInt(cboDeductionPeriod.SelectedValue)
            If mintLoanAdvanceUnkid <= 0 Then
                objLoan_Advance._Processpendingloanunkid = mintProcessPendingLoanUnkid
            End If
            objLoan_Advance._Isloan = radLoan.Checked
            objLoan_Advance._Advance_Amount = CDec(txtAdvanceAmt.Text)
            objLoan_Advance._Approverunkid = mintApprovertranunkid
            objLoan_Advance._CountryUnkid = CInt(cboCurrency.SelectedValue)

            If radLoan.Checked Then

                objLoan_Advance._Calctype_Id = CInt(cboLoanCalcType.SelectedValue)
                objLoan_Advance._Interest_Calctype_Id = CInt(cboInterestCalcType.SelectedValue)
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                objLoan_Advance._Mapped_TranheadUnkid = CInt(cboMappedHead.SelectedValue)
                'Sohail (29 Apr 2019) -- End

                objLoan_Advance._Exchange_rate = mdecPaidExRate
                objLoan_Advance._Basecurrency_amount = CDec(txtLoanAmt.Text) / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                objLoan_Advance._Opening_balance = CDec(txtLoanAmt.Text) / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                objLoan_Advance._Opening_balancePaidCurrency = CDec(txtLoanAmt.Text)
            Else
                objLoan_Advance._Calctype_Id = 0
                objLoan_Advance._Interest_Calctype_Id = 0
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                objLoan_Advance._Mapped_TranheadUnkid = 0
                'Sohail (29 Apr 2019) -- End

                objLoan_Advance._Exchange_rate = CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                objLoan_Advance._Basecurrency_amount = CDec(txtAdvanceAmt.Text) / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                objLoan_Advance._Opening_balance = CDec(txtAdvanceAmt.Text) / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                objLoan_Advance._Opening_balancePaidCurrency = CDec(txtAdvanceAmt.Text)
            End If

            If radLoan.Checked = True Then
                If mintPendingApprovalTranID > 0 Then
                    If mblnIsExternalEntity = True Then
                        objLoan_Advance._Balance_Amount = CDec(txtLoanAmt.Text) / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                        objLoan_Advance._Balance_AmountPaidCurrency = CDec(txtLoanAmt.Text)
                    End If
                End If
                objLoan_Advance._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                'objLoan_Advance._Net_Amount = CDec(txtNetAmount.Text)
                If mintPendingApprovalTranID > 0 Then
                    objLoan_Advance._Interest_Rate = CDec(txtLoanRate.Text)
                    objLoan_Advance._Loan_Duration = CInt(txtDuration.Text)
                    'Sohail (28 Jun 2018) -- Start
                    'Support Issue Id # 0002352 - Loan deducts only interest and not the whole installment amount, omits principal amount yet it shows as assigned in 72.1.
                    'objLoan_Advance._Interest_Amount = mdecInstallmentAmnt / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                    objLoan_Advance._Interest_Amount = mdecInterestAmt / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                    'Sohail (28 Jun 2018) -- End
                    objLoan_Advance._Net_Amount = mdecNetAmount / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                    objLoan_Advance._Emi_Tenure = CInt(txtDuration.Text)
                    objLoan_Advance._Emi_Amount = mdecInstallmentAmnt / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                End If
            Else
                If mintPendingApprovalTranID > 0 Then
                    If mblnIsExternalEntity = True Then
                        objLoan_Advance._Balance_Amount = CDec(txtAdvanceAmt.Text) / CDec(IIf(mdecPaidExRate = 0, 1, mdecPaidExRate))
                        objLoan_Advance._Balance_AmountPaidCurrency = CDec(txtAdvanceAmt.Text)
                    End If
                End If
                objLoan_Advance._Loanschemeunkid = 0
                objLoan_Advance._Net_Amount = 0
            End If

            If mintPendingApprovalTranID > 0 Then
                objLoan_Advance._Userunkid = CInt(Session("UserId"))
                objLoan_Advance._Voiduserunkid = -1
                objLoan_Advance._Voiddatetime = Nothing
                objLoan_Advance._Isvoid = False
            Else
                objLoan_Advance._Userunkid = objLoan_Advance._Userunkid
                objLoan_Advance._Voiduserunkid = objLoan_Advance._Voiduserunkid
                objLoan_Advance._Voiddatetime = objLoan_Advance._Voiddatetime
                objLoan_Advance._Isvoid = objLoan_Advance._Isvoid
            End If

            objLoan_Advance._RateOfInterest = CDec(txtLoanRate.Text)
            objLoan_Advance._NoOfInstallments = CInt(txtDuration.Text)
            objLoan_Advance._InstallmentAmount = CDec(txtInstallmentAmt.Text)

            'Nilay (05-May-2016) -- Start
            Blank_ModuleName()
            StrModuleName2 = "mnuLoan_Advance_Savings"
            objLoan_Advance._WebClientIP = Session("IP_ADD").ToString
            objLoan_Advance._WebFormName = "frmNewLoanAdvance_AddEdit"
            objLoan_Advance._WebHostName = Session("HOST_NAME").ToString
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objLoan_Advance._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If
            'Nilay (05-May-2016) -- End

            'Sohail (28 Jun 2018) -- Start
            'Support Issue Id # 0002352 - Loan deducts only interest and not the whole installment amount, omits principal amount yet it shows as assigned in 72.1.
            objLoan_Advance._EMI_Principal_amount = CDec(txtPrincipalAmt.Text)
            objLoan_Advance._EMI_Interest_amount = CDec(txtIntAmt.Text)
            'Sohail (28 Jun 2018) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CInt(Session("_LoanVocNoType")) = 0 Then
                If txtVoucherNo.Text.Trim = "" Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 22, "Voucher No. cannot be blank. Voucher No. is compulsory information."), Me)
                    txtVoucherNo.Focus()
                    Return False
                End If
            End If

            If CInt(cboEmpName.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 23, "Employee is compulsory information. Please select Employee to continue."), Me)
                cboEmpName.Focus()
                Return False
            End If

            If radLoan.Checked = True Then
                If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), Me)
                    cboLoanScheme.Focus()
                    Return False
                End If

                If CDec(txtLoanAmt.Text) = 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Loan Amount cannot be blank. Loan Amount is compulsory information."), Me)
                    txtLoanAmt.Focus()
                    Return False
                End If

                'Sohail (21 Mar 2018) -- Start
                'TRA Issue - Support Issue Id # 0002126 : Duplicate loan transactions are coming at time of salary in 70.2.
                'Sohail (14 Jul 2018) -- Start
                'Issue : Audit Trail Issues in Loan module in 72.1.
                'If objLoan_Advance.GetExistLoan(CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), True) = True Then
                If mintLoanAdvanceUnkid <= 0 AndAlso objLoan_Advance.GetExistLoan(CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), True) = True Then
                    'Sohail (14 Jul 2018) -- End
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 29, "Sorry, you can not assign loan for the selected scheme: Reason, The selected loan scheme is already assigned to you and having (In-progress or On hold) status. Instead of applying new loan, You can add top-up amount from Loan Advance List-> Operation -> Other Loan Operations."), Me)
                    Return False
                End If
                'Sohail (21 Mar 2018) -- End

            ElseIf radAdvance.Checked = True Then
                If CDec(txtAdvanceAmt.Text) = 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Advance Amount cannot be blank. Advance Amount is compulsory information."), Me)
                    txtAdvanceAmt.Focus()
                    Return False
                End If

                'Sohail (16 May 2018) -- Start
                'CCK Enhancement - Ref # 180 : On Advances Application, provide configuration where we can be able to limit maximum amount one can apply as advance in 72.1.
                'Sohail (14 Jul 2018) -- Start
                'Issue : Audit Trail Issues in Loan module in 72.1.
                'If CDec(Session("Advance_NetPayPercentage")) > 0 AndAlso objLoan_Advance.GetExistLoan(0, CInt(cboEmpName.SelectedValue), False) = True Then
                If mintLoanAdvanceUnkid <= 0 AndAlso CDec(Session("Advance_NetPayPercentage")) > 0 AndAlso objLoan_Advance.GetExistLoan(0, CInt(cboEmpName.SelectedValue), False) = True Then
                    'Sohail (14 Jul 2018) -- End
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 30, "Sorry, you can not assign Advance: Reason, The Advance is already assigned to you and having (In-progress or On hold) status."), Me)
                    Return False
                End If
                'Sohail (16 May 2018) -- End
            End If

            If Common_Validation() = False Then Return False

            'If CDec(txtLoanRate.Text) <= 0 Then
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Sorry, Interest rate is mandatory information. Please add interest rate on the list."), Me)
            '    txtLoanRate.Focus()
            '    Return False
            'End If

            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest OrElse CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount OrElse CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                If CDec(txtLoanRate.Text) <= 0 Then
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    Language.setLanguage(mstrModuleName)
                    'Varsha (25 Nov 2017) -- Start
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, Interest rate is mandatory information. Please enter interest rate."), Me)
                    txtLoanRate.Focus()
                    Return False
                End If
            End If

            'Dim dtemp() As DataRow = Nothing
            'dtemp = mdtEMI.Select("AUD <> 'D' AND periodunkid > 0")
            'If dtemp.Length <= 0 Then
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 34, "Sorry, EMI is mandatory information. Please add emi on the list."), Me)
            '    Return False
            'End If

            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            Dim objLoanScheme As New clsLoan_Scheme
            objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(txtDuration.Text) > objLoanScheme._MaxNoOfInstallment Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Installment months cannot be greater than ") & objLoanScheme._MaxNoOfInstallment & Language.getMessage(mstrModuleName, 27, " for ") & objLoanScheme._Name & Language.getMessage(mstrModuleName, 28, " Scheme."), Me)
                txtDuration.Focus()
                Return False
            End If
            'Varsha (25 Nov 2017) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidate:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Private Function Common_Validation() As Boolean
        Try
            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "Pay Period is compulsory information. Please select Pay Period."), Me)
                cboPayPeriod.Focus()
                Return False
            End If

            If CInt(cboDeductionPeriod.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Deduction period is compulsory information. Please select Deduction period to continue."), Me)
                cboDeductionPeriod.Focus()
                Return False
            End If

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPayPeriod.SelectedValue)

            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then

                Dim objDeducPeriod As New clscommom_period_Tran
                objDeducPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboDeductionPeriod.SelectedValue)

                If dtpDate.IsNull = False Then
                    If dtpDate.GetDate.Date < objDeducPeriod._Start_Date Or dtpDate.GetDate.Date > objDeducPeriod._End_Date Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Effective date should be in between ") & _
                                                        objDeducPeriod._Start_Date & Language.getMessage(mstrModuleName, 18, " And ") & _
                                                           objDeducPeriod._End_Date, Me)
                        dtpDate.Focus()
                        Return False
                    End If
                Else
                    DisplayMessage.DisplayMessage("Please set the Effective Date first.", Me)
                    Return False
                End If

                If objDeducPeriod._Start_Date < objPeriod._Start_Date Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Deduction period cannot be less then the Loan Period."), Me)
                    cboDeductionPeriod.Focus()
                    Return False
                End If

                Dim objPayment As New clsPayment_tran
                Dim ds As DataSet = objPayment.GetListByPeriod(Session("Database_Name").ToString, _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                               Session("UserAccessModeSetting").ToString, True, _
                                                               CBool(Session("IsIncludeInactiveEmp")), "List", _
                                                               clsPayment_tran.enPaymentRefId.PAYSLIP, _
                                                               CInt(cboDeductionPeriod.SelectedValue), _
                                                               clsPayment_tran.enPayTypeId.PAYMENT, _
                                                               cboEmpName.SelectedValue.ToString, False)
                If ds.Tables("List").Rows.Count > 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Sorry, you cannot assign Loan/Advance. Reason: Payslip Payment is already done for selected period."), Me)
                    Return False
                End If

                Dim objTnA As New clsTnALeaveTran
                If objTnA.IsPayrollProcessDone(CInt(cboDeductionPeriod.SelectedValue), cboEmpName.SelectedValue.ToString, objDeducPeriod._End_Date, enModuleReference.Payroll) = True Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot assign Loan/Advance. Reason: Payroll process is already done for the last date of selected deduction period."), Me)
                    Return False
                End If

                objDeducPeriod = Nothing
            End If
            objPeriod = Nothing

            If radAdvance.Checked = False Then
                If CInt(cboLoanCalcType.SelectedValue) <= 0 Then
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    Language.setLanguage(mstrModuleName)
                    'Varsha (25 Nov 2017) -- Start
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, Loan calculation type is mandatory information. Please select loan calculation type to continue."), Me)
                    Return False
                End If
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                'If CInt(cboLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest AndAlso CInt(cboInterestCalcType.SelectedValue) <= 0 Then
                If Not (CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest OrElse CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head) AndAlso CInt(cboInterestCalcType.SelectedValue) <= 0 Then
                    'Sohail (29 Apr 2019) -- End
                    'Varsha (25 Nov 2017) -- Start
                    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                    Language.setLanguage(mstrModuleName)
                    'Varsha (25 Nov 2017) -- Start
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, Interest calculation type is mandatory information. Please select interest calculation type to continue."), Me)
                    Return False

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head AndAlso CInt(cboMappedHead.SelectedValue) <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Please select Mapped Head to continue."), Me)
                    cboMappedHead.Focus()
                    Return False
                    'Sohail (29 Apr 2019) -- End

                End If

                'Sohail (17 Dec 2019) -- Start
                'Mukuba University # 0004115: Option for EOC to affect number of installments on loans.
                Dim intNoOfInstallment As Integer
                Integer.TryParse(txtDuration.Text, intNoOfInstallment)
                If intNoOfInstallment > 1 Then
                    Dim dtLoanEnd As Date = dtpDate.GetDate.Date.AddMonths(intNoOfInstallment).AddDays(-1)
                    Dim objEmpDates As New clsemployee_dates_tran
                    Dim ds As DataSet = objEmpDates.GetEmployeeEndDates(dtLoanEnd, cboEmpName.SelectedValue.ToString)
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim intEmpTenure As Integer = CInt(DateDiff(DateInterval.Month, dtpDate.GetDate.Date, eZeeDate.convertDate(ds.Tables(0).Rows(0).Item("finalenddate").ToString).AddDays(1)))
                        If intNoOfInstallment > intEmpTenure Then
                            'Hemant (24 Nov 2021) -- Start
                            'ISSUE/ENHANCEMENT : OLD-495 - Add an option that will allow users to import/add loan without considering the employee tenure(EOC).
                            If CBool(Session("SkipEOCValidationOnLoanTenure")) = False Then
                                'Hemant (24 Nov 2021) -- End
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Sorry, Loan tenure should not be greater than employee tenure month") & " [" & intEmpTenure & "].", Me)
                            If txtDuration.Enabled = True Then txtDuration.Focus()
                            Return False
                            End If 'Hemant (24 Nov 2021)
                        End If
                    End If
                End If
                'Sohail (17 Dec 2019) -- End

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Common_Validation:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    'Private Function OtherLoanValidation() As Boolean
    '    Try
    '        Dim xRow() As DataRow = Nothing
    '        mblnIsValidDeductionPeriod = False
    '        If radNoInterest.Checked = False Then
    '            If mdtInterest IsNot Nothing Then
    '                xRow = mdtInterest.Select("periodunkid = '" & CInt(cboDeductionPeriod.SelectedValue) & "' AND AUD <> 'D' AND periodunkid > 0 ")
    '                If xRow.Length <= 0 Then
    '                    Language.setLanguage(mstrModuleName)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, Deduction period you set is not present in loan interest list. Please set the correct effective period to continue."), Me)
    '                    mblnIsValidDeductionPeriod = True
    '                    Return False
    '                End If
    '            End If
    '        End If

    '        If mdtEMI IsNot Nothing Then
    '            xRow = mdtEMI.Select("periodunkid = '" & CInt(cboDeductionPeriod.SelectedValue) & "' AND AUD <> 'D' AND periodunkid > 0 ")
    '            If xRow.Length <= 0 Then
    '                Language.setLanguage(mstrModuleName)
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, Deduction period you set is not present in loan EMI list. Please set the correct effective period to continue."), Me)
    '                mblnIsValidDeductionPeriod = True
    '                Return False
    '            End If
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("OtherLoanValidation:- " & ex.Message, Me)
    '    End Try
    '    Return True
    'End Function

    'Private Sub ADD_LoanParameter()
    '    Try
    '        If mstrCommandName = "ADDINTEREST" Then

    '            Dim dRTemp() As DataRow = Nothing
    '            dRTemp = mdtInterest.Select("periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' AND AUD <> 'D'")
    '            If dRTemp.Length > 0 Then
    '                Language.setLanguage(mstrModuleName)
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Interest Rate is already present for the selected period."), Me)
    '                Exit Sub
    '            End If

    '            If CInt(cboPeriod.SelectedValue) > 0 Then
    '                Dim objTnaLeaveTran As New clsTnALeaveTran
    '                Dim objPeriod As New clscommom_period_Tran
    '                objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)

    '                If objTnaLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                    Language.setLanguage(mstrModuleName)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot add this Interest Rate Information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), Me)
    '                    objPeriod = Nothing
    '                    objTnaLeaveTran = Nothing
    '                    Exit Sub
    '                End If
    '                objTnaLeaveTran = Nothing
    '                objPeriod = Nothing
    '            End If

    '            Dim dRow As DataRow = mdtInterest.NewRow()
    '            dRow.Item("lninteresttranunkid") = -1
    '            dRow.Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
    '            dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
    '            dRow.Item("effectivedate") = dtpEffectiveDate.GetDate.Date
    '            'dRow.Item("interest_rate") = CInt(txtLoanInterest.Text)
    '            dRow.Item("interest_rate") = CDec(txtLoanInterest.Text)
    '            dRow.Item("userunkid") = Session("UserId")
    '            dRow.Item("isvoid") = False
    '            dRow.Item("voiduserunkid") = -1
    '            dRow.Item("voiddatetime") = DBNull.Value
    '            dRow.Item("voidreason") = ""
    '            dRow.Item("AUD") = "A"
    '            dRow.Item("GUID") = Guid.NewGuid.ToString()
    '            dRow.Item("dperiod") = cboPeriod.SelectedItem.Text
    '            dRow.Item("ddate") = dtpEffectiveDate.GetDate.Date.ToShortDateString
    '            dRow.Item("pstatusid") = 1
    '            mdtInterest.Rows.Add(dRow)

    '            Call Fill_InterestRate_Grid()

    '        ElseIf mstrCommandName = "ADDEMI" Then

    '            If mintLoanAdvanceUnkid < 0 Then
    '                If CInt(cboPeriod.SelectedValue) <> CInt(cboDeductionPeriod.SelectedValue) Then
    '                    Language.setLanguage(mstrModuleName)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 36, "Sorry, Selected Period does not match with the deduction Period. Please select correct deduction period."), Me)
    '                    Exit Sub
    '                End If
    '            End If

    '            Dim xtmp() As DataRow = mdtEMI.Select("periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' AND AUD <> 'D'")
    '            If xtmp.Length > 0 Then
    '                Language.setLanguage(mstrModuleName)
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Sorry, EMI is already present for the selected period."), Me)
    '                Exit Sub
    '            End If

    '            If CInt(cboPeriod.SelectedValue) > 0 Then
    '                Dim objTnaLeaveTran As New clsTnALeaveTran
    '                Dim objPeriod As New clscommom_period_Tran
    '                objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)

    '                If objTnaLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 28, "Sorry, you cannot add this EMI information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), Me)
    '                    objPeriod = Nothing
    '                    objTnaLeaveTran = Nothing
    '                    Exit Sub
    '                End If
    '                objTnaLeaveTran = Nothing
    '                objPeriod = Nothing

    '            End If

    '            Dim dRow As DataRow = mdtEMI.NewRow()
    '            dRow.Item("lnemitranunkid") = -1
    '            dRow.Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
    '            dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
    '            dRow.Item("effectivedate") = dtpEffectiveDate.GetDate.Date
    '            dRow.Item("emi_tenure") = CInt(txtEMIInstallments.Text)
    '            dRow.Item("emi_amount") = CDec(txtInstallmentAmt.Text)
    '            dRow.Item("loan_duration") = CInt(txtDuration.Text)
    '            dRow.Item("userunkid") = Session("UserId")
    '            dRow.Item("isvoid") = False
    '            dRow.Item("voiduserunkid") = -1
    '            dRow.Item("voiddatetime") = DBNull.Value
    '            dRow.Item("voidreason") = ""
    '            dRow.Item("AUD") = "A"
    '            dRow.Item("GUID") = Guid.NewGuid.ToString()
    '            dRow.Item("dperiod") = cboPeriod.SelectedItem.ToString
    '            dRow.Item("ddate") = dtpEffectiveDate.GetDate.Date.ToShortDateString
    '            dRow.Item("pstatusid") = 1
    '            mdtEMI.Rows.Add(dRow)

    '            Call Fill_EMI_Transaction_Grid()

    '        ElseIf mstrCommandName = "ADDTOPUP" Then

    '            Dim oDeductionPeriod As New clscommom_period_Tran
    '            oDeductionPeriod._Periodunkid = CInt(cboDeductionPeriod.SelectedValue)

    '            Dim xtmp() As DataRow = mdtTopup.Select("periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' AND AUD <> 'D'")
    '            If xtmp.Length > 0 Then
    '                Language.setLanguage(mstrModuleName)
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 29, "Sorry, Topup is already present for the selected period."), Me)
    '                Exit Sub
    '            End If
    '            If CInt(cboPeriod.SelectedValue) > 0 Then
    '                Dim objTnaLeaveTran As New clsTnALeaveTran
    '                Dim objPeriod As New clscommom_period_Tran
    '                objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)

    '                If objPeriod._End_Date.Date < oDeductionPeriod._End_Date.Date Then
    '                    Language.setLanguage(mstrModuleName)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 37, "Sorry, you can give topup based on deduction period set for this loan."), Me)
    '                    Exit Sub
    '                End If

    '                If objTnaLeaveTran.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 30, "Sorry, you cannot add Topup information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), Me)
    '                    objPeriod = Nothing
    '                    objTnaLeaveTran = Nothing
    '                    Exit Sub
    '                End If
    '                objTnaLeaveTran = Nothing
    '                objPeriod = Nothing

    '            End If

    '            Dim dRow As DataRow = mdtTopup.NewRow()
    '            dRow.Item("lntopuptranunkid") = -1
    '            dRow.Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
    '            dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
    '            dRow.Item("effectivedate") = dtpEffectiveDate.GetDate.Date
    '            dRow.Item("topup_amount") = CDec(txtTopupAmt.Text)
    '            If radSimpleInterest.Checked Then
    '                dRow.Item("interest_amount") = 0
    '            Else
    '                dRow.Item("interest_amount") = 0
    '            End If
    '            dRow.Item("userunkid") = Session("UserId")
    '            dRow.Item("isvoid") = False
    '            dRow.Item("voiduserunkid") = -1
    '            dRow.Item("voiddatetime") = DBNull.Value
    '            dRow.Item("voidreason") = ""
    '            dRow.Item("AUD") = "A"
    '            dRow.Item("GUID") = Guid.NewGuid.ToString()
    '            dRow.Item("dperiod") = cboPeriod.SelectedItem.ToString
    '            dRow.Item("ddate") = dtpEffectiveDate.GetDate.Date.ToShortDateString
    '            dRow.Item("pstatusid") = 1
    '            mdtTopup.Rows.Add(dRow)

    '            Call Fill_Topup_Transaction_Grid()

    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("ADD_LoanParameter:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Private Sub EDIT_LoanParameter()
    '    Try
    '        If mstrCommandName = "EDITINTEREST" Then
    '            If mintItemIndex > -1 Then
    '                Dim xtmp() As DataRow = Nothing
    '                If CInt(dgvInterestHistory.Items(mintItemIndex).Cells(6).Text) > 0 Then
    '                    xtmp = mdtInterest.Select("lninteresttranunkid = '" & dgvInterestHistory.Items(mintItemIndex).Cells(6).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                Else
    '                    xtmp = mdtInterest.Select("GUID = '" & dgvInterestHistory.Items(mintItemIndex).Cells(7).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                End If

    '                If xtmp.Length > 0 Then

    '                    If mintLoanAdvanceUnkid < 0 Then
    '                        If CInt(cboPeriod.SelectedValue) <> CInt(cboDeductionPeriod.SelectedValue) Then
    '                            Language.setLanguage(mstrModuleName)
    '                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 36, "Sorry, Selected Period does not match with the deduction Period. Please select correct deduction period."), Me)
    '                            Exit Sub
    '                        End If
    '                    End If

    '                    Dim xFRow() As DataRow = mdtInterest.Select("periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' AND AUD <> 'D' AND periodunkid <> '" & CInt(xtmp(0).Item("periodunkid")) & "'")

    '                    If xFRow.Length > 0 Then
    '                        Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Interest Rate is already present for the selected period."), Me)
    '                        Exit Sub
    '                    End If

    '                    xtmp(0).Item("lninteresttranunkid") = xtmp(0).Item("lninteresttranunkid")
    '                    xtmp(0).Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
    '                    'xtmp(0).Item("lntopuptranunkid") = xtmp(0).Item("lntopuptranunkid")
    '                    xtmp(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
    '                    xtmp(0).Item("effectivedate") = dtpEffectiveDate.GetDate.Date
    '                    xtmp(0).Item("interest_rate") = CDec(txtLoanInterest.Text)
    '                    xtmp(0).Item("userunkid") = Session("UserId")
    '                    xtmp(0).Item("isvoid") = False
    '                    xtmp(0).Item("voiduserunkid") = -1
    '                    xtmp(0).Item("voiddatetime") = DBNull.Value
    '                    xtmp(0).Item("voidreason") = ""
    '                    If xtmp(0).Item("AUD").ToString = "" Then
    '                        xtmp(0).Item("AUD") = "U"
    '                    End If
    '                    xtmp(0).Item("GUID") = xtmp(0).Item("GUID")
    '                    xtmp(0).Item("dperiod") = cboPeriod.SelectedItem.Text
    '                    xtmp(0).Item("ddate") = dtpEffectiveDate.GetDate.Date.ToShortDateString
    '                    xtmp(0).Item("pstatusid") = xtmp(0).Item("pstatusid")
    '                    xtmp(0).AcceptChanges()

    '                    mdtInterest.AcceptChanges()

    '                    Call Fill_InterestRate_Grid()

    '                End If
    '            End If
    '        End If
    '        If mstrCommandName = "EDITEMI" Then
    '            If mintItemIndex > -1 Then
    '                Dim xtmp() As DataRow = Nothing
    '                If CInt(dgvEMIHistory.Items(mintItemIndex).Cells(8).Text) > 0 Then
    '                    xtmp = mdtEMI.Select("lnemitranunkid = '" & dgvEMIHistory.Items(mintItemIndex).Cells(8).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                Else
    '                    xtmp = mdtEMI.Select("GUID = '" & dgvEMIHistory.Items(mintItemIndex).Cells(9).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                End If

    '                If xtmp.Length > 0 Then

    '                    If mintLoanAdvanceUnkid < 0 Then
    '                        If CInt(cboPeriod.SelectedValue) <> CInt(cboDeductionPeriod.SelectedValue) Then
    '                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 36, "Sorry, Selected Period does not match with the deduction Period. Please select correct deduction period."), Me)
    '                            Exit Sub
    '                        End If
    '                    End If

    '                    Dim xFRow() As DataRow = mdtEMI.Select("periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' AND AUD <> 'D' AND periodunkid <> '" & CInt(xtmp(0).Item("periodunkid")) & "'")

    '                    If xFRow.Length > 0 Then
    '                        Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 27, "Sorry, EMI is already present for the selected period."), Me)
    '                        Exit Sub
    '                    End If

    '                    xtmp(0).Item("lnemitranunkid") = xtmp(0).Item("lnemitranunkid")
    '                    xtmp(0).Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
    '                    xtmp(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
    '                    xtmp(0).Item("effectivedate") = dtpEffectiveDate.GetDate.Date
    '                    xtmp(0).Item("emi_tenure") = CInt(txtEMIInstallments.Text)
    '                    xtmp(0).Item("emi_amount") = CDec(txtInstallmentAmt.Text)
    '                    xtmp(0).Item("loan_duration") = CInt(txtDuration.Text)
    '                    xtmp(0).Item("userunkid") = Session("UserId")
    '                    xtmp(0).Item("isvoid") = False
    '                    xtmp(0).Item("voiduserunkid") = -1
    '                    xtmp(0).Item("voiddatetime") = DBNull.Value
    '                    xtmp(0).Item("voidreason") = ""
    '                    If xtmp(0).Item("AUD").ToString = "" Then
    '                        xtmp(0).Item("AUD") = "U"
    '                    End If
    '                    xtmp(0).Item("GUID") = xtmp(0).Item("GUID")
    '                    xtmp(0).Item("dperiod") = cboPeriod.SelectedItem.ToString
    '                    xtmp(0).Item("ddate") = dtpEffectiveDate.GetDate.Date.ToShortDateString
    '                    xtmp(0).Item("pstatusid") = 1
    '                    xtmp(0).AcceptChanges()
    '                    mdtEMI.AcceptChanges()
    '                    Call Fill_EMI_Transaction_Grid()

    '                End If

    '            End If
    '        End If

    '        If mstrCommandName = "EDITTOPUP" Then
    '            If mintItemIndex > -1 Then
    '                Dim xtmp() As DataRow = Nothing
    '                If CInt(dgvTopupHistory.Items(mintItemIndex).Cells(7).Text) > 0 Then
    '                    xtmp = mdtTopup.Select("lntopuptranunkid = '" & dgvTopupHistory.Items(mintItemIndex).Cells(7).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                Else
    '                    xtmp = mdtTopup.Select("GUID = '" & dgvTopupHistory.Items(mintItemIndex).Cells(8).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                End If

    '                If xtmp.Length > 0 Then

    '                    Dim xFRow() As DataRow = mdtTopup.Select("periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' AND AUD <> 'D' AND periodunkid <> '" & CInt(xtmp(0).Item("periodunkid")) & "'")
    '                    If xFRow.Length > 0 Then
    '                        Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 29, "Sorry, Topup is already present for the selected period."), Me)
    '                        Exit Sub
    '                    End If

    '                    xtmp(0).Item("lntopuptranunkid") = xtmp(0).Item("lntopuptranunkid")
    '                    xtmp(0).Item("loanadvancetranunkid") = mintLoanAdvanceUnkid
    '                    xtmp(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
    '                    xtmp(0).Item("effectivedate") = dtpEffectiveDate.GetDate.Date
    '                    xtmp(0).Item("topup_amount") = CDec(txtTopupAmt.Text)
    '                    If radSimpleInterest.Checked = True Then
    '                        xtmp(0).Item("interest_amount") = 0
    '                    Else
    '                        xtmp(0).Item("interest_amount") = 0
    '                    End If
    '                    xtmp(0).Item("userunkid") = Session("UserId")
    '                    xtmp(0).Item("isvoid") = False
    '                    xtmp(0).Item("voiduserunkid") = -1
    '                    xtmp(0).Item("voiddatetime") = DBNull.Value
    '                    xtmp(0).Item("voidreason") = ""
    '                    If xtmp(0).Item("AUD").ToString = "" Then
    '                        xtmp(0).Item("AUD") = "U"
    '                    End If
    '                    xtmp(0).Item("GUID") = xtmp(0).Item("GUID")
    '                    xtmp(0).Item("dperiod") = cboPeriod.SelectedItem.Text
    '                    xtmp(0).Item("ddate") = dtpEffectiveDate.GetDate.Date.ToShortDateString
    '                    xtmp(0).Item("pstatusid") = 1
    '                    xtmp(0).AcceptChanges()

    '                    mdtTopup.AcceptChanges()
    '                    Fill_Topup_Transaction_Grid()

    '                End If
    '            End If
    '        End If

    '    Catch ex As Exception

    '    End Try
    'End Sub

    'Private Sub DELETE_LoanParameter()
    '    Try
    '        If mstrModuleName = "DELETEINTEREST" Then
    '            If mintItemIndex > -1 Then

    '                Dim xtmp() As DataRow = Nothing
    '                If CInt(dgvInterestHistory.Items(mintItemIndex).Cells(6).Text) > 0 Then
    '                    xtmp = mdtInterest.Select("lninteresttranunkid = '" & dgvInterestHistory.Items(mintItemIndex).Cells(6).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                Else
    '                    xtmp = mdtInterest.Select("GUID = '" & dgvInterestHistory.Items(mintItemIndex).Cells(7).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                End If
    '                If xtmp.Length > 0 Then
    '                    If popup_DeleteReason.Reason.Trim.Length <= 0 Then Exit Sub

    '                    xtmp(0).Item("isvoid") = True
    '                    xtmp(0).Item("voiduserunkid") = Session("UserId")
    '                    xtmp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
    '                    xtmp(0).Item("voidreason") = popup_DeleteReason.Reason
    '                    xtmp(0).Item("AUD") = "D"

    '                    xtmp(0).AcceptChanges()
    '                    mdtInterest.AcceptChanges()

    '                    Call Fill_InterestRate_Grid()
    '                End If

    '            End If
    '        End If

    '        If mstrModuleName = "DELETEEMI" Then
    '            If mintItemIndex > -1 Then

    '                Dim xtmp() As DataRow = Nothing
    '                If CInt(dgvEMIHistory.Items(mintItemIndex).Cells(8).Text) > 0 Then
    '                    xtmp = mdtEMI.Select("lnemitranunkid = '" & dgvEMIHistory.Items(mintItemIndex).Cells(8).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                Else
    '                    xtmp = mdtEMI.Select("GUID = '" & dgvEMIHistory.Items(mintItemIndex).Cells(9).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                End If

    '                If xtmp.Length > 0 Then
    '                    If popup_DeleteReason.Reason.Trim.Length <= 0 Then Exit Sub

    '                    xtmp(0).Item("isvoid") = True
    '                    xtmp(0).Item("voiduserunkid") = Session("UserId")
    '                    xtmp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
    '                    xtmp(0).Item("voidreason") = popup_DeleteReason.Reason
    '                    xtmp(0).Item("AUD") = "D"

    '                    xtmp(0).AcceptChanges()
    '                    mdtEMI.AcceptChanges()
    '                    Call Fill_EMI_Transaction_Grid()

    '                End If
    '            End If
    '        End If

    '        If mstrModuleName = "DELETETOPUP" Then
    '            If mintItemIndex > -1 Then

    '                Dim xtmp() As DataRow = Nothing
    '                If CInt(dgvTopupHistory.Items(mintItemIndex).Cells(7).Text) > 0 Then
    '                    xtmp = mdtTopup.Select("lntopuptranunkid = '" & dgvTopupHistory.Items(mintItemIndex).Cells(7).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                Else
    '                    xtmp = mdtTopup.Select("GUID = '" & dgvTopupHistory.Items(mintItemIndex).Cells(8).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '                End If

    '                If xtmp.Length > 0 Then
    '                    If popup_DeleteReason.Reason.Trim.Length <= 0 Then Exit Sub

    '                    xtmp(0).Item("isvoid") = True
    '                    xtmp(0).Item("voiduserunkid") = Session("UserId")
    '                    xtmp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
    '                    xtmp(0).Item("voidreason") = popup_DeleteReason.Reason
    '                    xtmp(0).Item("AUD") = "D"

    '                    xtmp(0).AcceptChanges()
    '                    mdtTopup.AcceptChanges()
    '                    Call Fill_Topup_Transaction_Grid()

    '                End If

    '            End If
    '        End If

    '    Catch ex As Exception

    '    End Try
    'End Sub

    Private Sub ClearLoanAdvanceValues()
        Try
            Me.ViewState("PendingApprovalTranID") = Nothing
            Me.ViewState("EmployeeID") = Nothing
            Me.ViewState("Approvertranunkid") = Nothing
            Me.ViewState("LoanAdvanceUnkid") = Nothing
            Me.ViewState("ProcessPendingLoanUnkid") = Nothing
            Me.ViewState("CurrOpenPeriodId") = Nothing
            Me.ViewState("IsValidDeductionPeriod") = Nothing
            Me.ViewState("IsExternalEntity") = Nothing

            Me.Session("InterestTable") = Nothing
            Me.Session("EMITable") = Nothing
            Me.Session("TopupTable") = Nothing
            Me.ViewState("LastCalcTypeId") = Nothing

            Me.ViewState("CommandName") = Nothing
            Me.ViewState("ItemIndex") = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "DataGrid Events"

    'Protected Sub dgvInterestHistory_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvInterestHistory.ItemCommand
    '    Try
    '        If e.Item.ItemIndex <= -1 Then Exit Sub

    '        If e.CommandName.ToUpper = "ADDINTEREST" Then

    '            mstrCommandName = e.CommandName.ToUpper

    '            If Common_Validation() = False Then
    '                Exit Sub
    '            End If

    '            If mintLoanAdvanceUnkid < 0 Then
    '                If mblnIsValidDeductionPeriod = True Then
    '                    Language.setLanguage(mstrModuleName)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 35, "Sorry, Please use edit operion from list and set correct deduction period to continue."), Me)
    '                    Exit Sub
    '                End If
    '            End If

    '            If radNoInterest.Checked = True Then
    '                Language.setLanguage(mstrModuleName)
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 19, "Sorry, You are not allowed to add interest rate for No Interest Loans."), Me)
    '                Exit Sub
    '            End If

    '            If mintLoanAdvanceUnkid > 0 Then
    '                If radSimpleInterest.Checked = True Then
    '                    Language.setLanguage(mstrModuleName)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 25, "Sorry, You are not allowed to add new interest rate for Simple Interest Loans."), Me)
    '                    Exit Sub
    '                End If
    '            End If

    '            Dim drTmp() As DataRow = Nothing
    '            If radSimpleInterest.Checked = True Then
    '                If mdtInterest IsNot Nothing AndAlso mdtInterest.Rows.Count > 0 Then
    '                    drTmp = mdtInterest.Select("periodunkid > 0 AND AUD <> 'D'")
    '                    If drTmp.Length > 0 Then
    '                        Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, Interest Rate is already present for simple interest. You are not allowed to add new interest rate for Simple Interest Loans."), Me)
    '                        Exit Sub
    '                    End If
    '                End If
    '            End If

    '            If mintLoanAdvanceUnkid > 0 Then
    '                cboPeriod.SelectedValue = mintCurrOpenPeriodId
    '            Else
    '                cboPeriod.SelectedValue = CInt(cboDeductionPeriod.SelectedValue)
    '            End If

    '            Call SetLoanParaControls(enParameterMode.LN_RATE)
    '            Call cboPeriod_SelectedIndexChanged(Nothing, Nothing)
    '            Call popupLoanParameter.Show()
    '            mblnPopupShowHide = True

    '        End If

    '        If e.CommandName.ToUpper = "EDITINTEREST" Then

    '            mstrCommandName = e.CommandName.ToUpper
    '            mintItemIndex = e.Item.ItemIndex

    '            Dim xtmp() As DataRow = Nothing

    '            If CInt(e.Item.Cells(6).Text) > 0 Then
    '                xtmp = mdtInterest.Select("lninteresttranunkid = '" & e.Item.Cells(6).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '            Else
    '                xtmp = mdtInterest.Select("GUID = '" & e.Item.Cells(7).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '            End If

    '            If xtmp.Length > 0 Then
    '                If CInt(xtmp(0).Item("lninteresttranunkid").ToString) > 0 Then
    '                    Dim objTnaLeaveTran As New clsTnALeaveTran
    '                    Dim objPeriod As New clscommom_period_Tran
    '                    objPeriod._Periodunkid = CInt(xtmp(0).Item("periodunkid"))

    '                    If objTnaLeaveTran.IsPayrollProcessDone(CInt(xtmp(0).Item("periodunkid")), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                        Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot edit assiged Interest Information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), Me)
    '                        objPeriod = Nothing
    '                        objTnaLeaveTran = Nothing
    '                        Exit Sub
    '                    End If
    '                    objTnaLeaveTran = Nothing
    '                    objPeriod = Nothing
    '                End If

    '                cboPeriod.SelectedValue = CInt(xtmp(0).Item("periodunkid"))
    '                dtpEffectiveDate.SetDate = CDate(xtmp(0).Item("effectivedate"))
    '                txtLoanInterest.Text = CDec(xtmp(0).Item("interest_rate"))
    '                mblnAllowChangePeriod = CBool(IIf(CInt(cboDeductionPeriod.SelectedValue) = CInt(xtmp(0).Item("periodunkid")), False, True))
    '                cboPeriod.Enabled = mblnAllowChangePeriod

    '                Call SetLoanParaControls(enParameterMode.LN_RATE)

    '                Call popupLoanParameter.Show()
    '                mblnPopupShowHide = True

    '            End If

    '        End If

    '        If e.CommandName.ToUpper = "DELETEINTEREST" Then

    '            mstrCommandName = e.CommandName.ToUpper
    '            mintItemIndex = e.Item.ItemIndex

    '            If mintLoanAdvanceUnkid < 0 Then
    '                If mblnIsValidDeductionPeriod = True Then
    '                    Language.setLanguage(mstrModuleName)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 35, "Sorry, Please use edit operion from list and set correct deduction period to continue."), Me)
    '                    Exit Sub
    '                End If
    '            End If

    '            Dim xtmp() As DataRow = Nothing

    '            If CInt(e.Item.Cells(6).Text) > 0 Then
    '                xtmp = mdtInterest.Select("lninteresttranunkid = '" & e.Item.Cells(6).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '            Else
    '                xtmp = mdtInterest.Select("GUID = '" & e.Item.Cells(7).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '            End If

    '            If xtmp.Length > 0 Then

    '                If CInt(cboDeductionPeriod.SelectedValue) = CInt(xtmp(0).Item("periodunkid")) Then
    '                    Language.setLanguage(mstrModuleName)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 26, "Sorry, you cannot delete this entry. Reason : It is linked with deduction period."), Me)
    '                    Exit Sub
    '                End If

    '                If CInt(xtmp(0).Item("lninteresttranunkid").ToString) > 0 Then

    '                    Dim objTnaLeaveTran As New clsTnALeaveTran
    '                    Dim objPeriod As New clscommom_period_Tran
    '                    objPeriod._Periodunkid = CInt(xtmp(0).Item("periodunkid"))

    '                    If objTnaLeaveTran.IsPayrollProcessDone(CInt(xtmp(0).Item("periodunkid")), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                        Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot remove assiged Interest information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), Me)
    '                        objPeriod = Nothing
    '                        objTnaLeaveTran = Nothing
    '                        Exit Sub
    '                    End If
    '                    objTnaLeaveTran = Nothing
    '                    objPeriod = Nothing

    '                    Dim xUsedMessage As String = String.Empty
    '                    xUsedMessage = objLoan_Advance.Check_Transaction_Linking("lninteresttranunkid", CInt(xtmp(0).Item("lninteresttranunkid")), mintLoanAdvanceUnkid)

    '                    If xUsedMessage.Trim.Length > 0 Then
    '                        DisplayMessage.DisplayMessage(xUsedMessage, Me)
    '                        Exit Sub
    '                    End If

    '                    popup_DeleteReason.Show()
    '                Else
    '                    xtmp(0).Item("AUD") = "D"

    '                    xtmp(0).AcceptChanges()
    '                    mdtInterest.AcceptChanges()
    '                    Call Fill_InterestRate_Grid()
    '                End If

    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("dgvInterestHistory_ItemCommand:- " & ex.Message, Me)
    '    End Try
    'End Sub

    Protected Sub dgvInterestHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvInterestHistory.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                'e.Item.Cells(5).Text = e.Item.Cells(5).Text
                e.Item.Cells(4).Text = CDate(e.Item.Cells(4).Text).Date.ToString(Session("DateFormat").ToString, New System.Globalization.CultureInfo(""))
                e.Item.Cells(5).Text = Format(CDbl(e.Item.Cells(5).Text), Session("fmtCurrency").ToString)
                'Nilay (01-Apr-2016) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvInterestHistory_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Protected Sub dgvEMIHistory_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvEMIHistory.ItemCommand
    '    Try
    '        If e.Item.ItemIndex <= -1 Then Exit Sub

    '        If e.CommandName.ToUpper = "ADDEMI" Then

    '            mstrCommandName = e.CommandName.ToUpper

    '            If Common_Validation() = False Then
    '                Exit Sub
    '            End If

    '            If mintLoanAdvanceUnkid < 0 Then
    '                If mblnIsValidDeductionPeriod = True Then
    '                    Language.setLanguage(mstrModuleName)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 35, "Sorry, Please use edit operion from list and set correct deduction period to continue."), Me)
    '                    Exit Sub
    '                End If
    '            End If

    '            If mintLoanAdvanceUnkid > 0 Then
    '                cboPeriod.SelectedValue = mintCurrOpenPeriodId
    '            Else
    '                cboPeriod.SelectedValue = CInt(cboDeductionPeriod.SelectedValue)
    '            End If

    '            Call SetLoanParaControls(enParameterMode.LN_EMI)
    '            Call cboPeriod_SelectedIndexChanged(Nothing, Nothing)
    '            Call popupLoanParameter.Show()
    '            mblnPopupShowHide = True

    '        End If

    '        If e.CommandName.ToUpper = "EDITEMI" Then

    '            mstrCommandName = e.CommandName.ToUpper
    '            mintItemIndex = e.Item.ItemIndex

    '            Dim xtmp() As DataRow = Nothing
    '            If CInt(dgvEMIHistory.Items(mintItemIndex).Cells(8).Text) > 0 Then
    '                xtmp = mdtEMI.Select("lnemitranunkid = '" & dgvEMIHistory.Items(mintItemIndex).Cells(8).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '            Else
    '                xtmp = mdtEMI.Select("GUID = '" & dgvEMIHistory.Items(mintItemIndex).Cells(9).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '            End If

    '            If xtmp.Length > 0 Then
    '                If CInt(xtmp(0).Item("lnemitranunkid").ToString) > 0 Then
    '                    Dim objTnaLeaveTran As New clsTnALeaveTran
    '                    Dim objPeriod As New clscommom_period_Tran
    '                    objPeriod._Periodunkid = CInt(xtmp(0).Item("periodunkid"))

    '                    If objTnaLeaveTran.IsPayrollProcessDone(CInt(xtmp(0).Item("periodunkid")), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                        Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot edit assiged EMI Information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), Me)
    '                        objPeriod = Nothing
    '                        objTnaLeaveTran = Nothing
    '                        Exit Sub
    '                    End If
    '                    objTnaLeaveTran = Nothing
    '                    objPeriod = Nothing
    '                End If

    '                cboPeriod.SelectedValue = CInt(xtmp(0).Item("periodunkid"))
    '                dtpEffectiveDate.SetDate = CDate(xtmp(0).Item("effectivedate"))
    '                txtEMIInstallments.Text = CInt(xtmp(0).Item("emi_tenure"))
    '                txtInstallmentAmt.Text = CDec(xtmp(0).Item("emi_amount"))
    '                txtDuration.Text = CInt(xtmp(0).Item("loan_duration"))
    '                mblnAllowChangePeriod = CBool(IIf(CInt(cboDeductionPeriod.SelectedValue) = CInt(xtmp(0).Item("periodunkid")), False, True))
    '                cboPeriod.Enabled = mblnAllowChangePeriod

    '                Call SetLoanParaControls(enParameterMode.LN_EMI)

    '                Call popupLoanParameter.Show()
    '                mblnPopupShowHide = True

    '            End If

    '        End If

    '        If e.CommandName.ToUpper = "DELETEEMI" Then

    '            mstrCommandName = e.CommandName.ToUpper
    '            mintItemIndex = e.Item.ItemIndex

    '            If mintLoanAdvanceUnkid < 0 Then
    '                If mblnIsValidDeductionPeriod = True Then
    '                    Language.setLanguage(mstrModuleName)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 35, "Sorry, Please use edit operion from list and set correct deduction period to continue."), Me)
    '                    Exit Sub
    '                End If
    '            End If

    '            Dim xtmp() As DataRow = Nothing
    '            If CInt(dgvEMIHistory.Items(mintItemIndex).Cells(8).Text) > 0 Then
    '                xtmp = mdtEMI.Select("lnemitranunkid = '" & dgvEMIHistory.Items(mintItemIndex).Cells(8).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '            Else
    '                xtmp = mdtEMI.Select("GUID = '" & dgvEMIHistory.Items(mintItemIndex).Cells(9).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '            End If

    '            If xtmp.Length > 0 Then

    '                If CInt(cboDeductionPeriod.SelectedValue) = CInt(xtmp(0).Item("periodunkid")) Then
    '                    Language.setLanguage(mstrModuleName)
    '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 31, "Sorry, you cannot delete this entry. Reason : It is linked with deduction period."), Me)
    '                    Exit Sub
    '                End If

    '                If CInt(xtmp(0).Item("lnemitranunkid").ToString) > 0 Then

    '                    Dim objTnaLeaveTran As New clsTnALeaveTran
    '                    Dim objPeriod As New clscommom_period_Tran
    '                    objPeriod._Periodunkid = CInt(xtmp(0).Item("periodunkid"))

    '                    If objTnaLeaveTran.IsPayrollProcessDone(CInt(xtmp(0).Item("periodunkid")), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                        Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, you cannot remove assiged EMI Information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), Me)
    '                        objPeriod = Nothing
    '                        objTnaLeaveTran = Nothing
    '                        Exit Sub
    '                    End If
    '                    objTnaLeaveTran = Nothing
    '                    objPeriod = Nothing

    '                    Dim xUsedMessage As String = String.Empty
    '                    xUsedMessage = objLoan_Advance.Check_Transaction_Linking("lnemitranunkid", CInt(xtmp(0).Item("lnemitranunkid")), mintLoanAdvanceUnkid)

    '                    If xUsedMessage.Trim.Length > 0 Then
    '                        DisplayMessage.DisplayMessage(xUsedMessage, Me)
    '                        Exit Sub
    '                    End If

    '                    popup_DeleteReason.Show()
    '                Else
    '                    xtmp(0).Item("AUD") = "D"

    '                    xtmp(0).AcceptChanges()
    '                    mdtEMI.AcceptChanges()
    '                    Call Fill_EMI_Transaction_Grid()
    '                End If

    '            End If

    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("dgvEMIHistory_ItemCommand:- " & ex.Message, Me)
    '    End Try
    'End Sub

    Protected Sub dgvEMIHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvEMIHistory.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                e.Item.Cells(6).Text = Format(CDec(e.Item.Cells(6).Text), Session("fmtCurrency").ToString)
                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                e.Item.Cells(4).Text = CDate(e.Item.Cells(4).Text).Date.ToString(Session("DateFormat").ToString, New System.Globalization.CultureInfo(""))
                'Nilay (01-Apr-2016) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvEMIHistory_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    'Protected Sub dgvTopupHistory_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvTopupHistory.ItemCommand
    '    Try
    '        If e.Item.ItemIndex <= -1 Then Exit Sub

    '        If radLoan.Checked = False Then
    '            objLoan_Advance._Calctype_Id = 0
    '        End If

    '        If e.CommandName.ToUpper = "ADDTOPUP" Then

    '            mstrCommandName = e.CommandName.ToUpper

    '            If Common_Validation() = False Then
    '                Exit Sub
    '            End If

    '            Call SetLoanParaControls(enParameterMode.LN_TOPUP)
    '            Call cboPeriod_SelectedIndexChanged(Nothing, Nothing)
    '            Call popupLoanParameter.Show()
    '            mblnPopupShowHide = True

    '        End If

    '        If e.CommandName.ToUpper = "EDITTOPUP" Then

    '            mstrCommandName = e.CommandName.ToUpper
    '            mintItemIndex = e.Item.ItemIndex

    '            Dim xtmp() As DataRow = Nothing
    '            If CInt(dgvTopupHistory.Items(mintItemIndex).Cells(7).Text) > 0 Then
    '                xtmp = mdtTopup.Select("lntopuptranunkid = '" & dgvTopupHistory.Items(mintItemIndex).Cells(7).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '            Else
    '                xtmp = mdtTopup.Select("GUID = '" & dgvTopupHistory.Items(mintItemIndex).Cells(8).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '            End If

    '            If xtmp.Length > 0 Then

    '                If CInt(xtmp(0).Item("lntopuptranunkid").ToString) > 0 Then
    '                    Dim objTnaLeaveTran As New clsTnALeaveTran
    '                    Dim objPeriod As New clscommom_period_Tran
    '                    objPeriod._Periodunkid = CInt(xtmp(0).Item("periodunkid"))
    '                    If objTnaLeaveTran.IsPayrollProcessDone(CInt(xtmp(0).Item("periodunkid")), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                        Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 20, "Sorry, you cannot edit assiged Topup Information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), Me)
    '                        objPeriod = Nothing
    '                        objTnaLeaveTran = Nothing
    '                        Exit Sub
    '                    End If
    '                    objTnaLeaveTran = Nothing
    '                    objPeriod = Nothing
    '                End If

    '                cboPeriod.SelectedValue = CInt(xtmp(0).Item("periodunkid"))
    '                dtpEffectiveDate.SetDate = CDate(xtmp(0).Item("effectivedate"))
    '                txtTopupAmt.Text = CInt(xtmp(0).Item("topup_amount"))
    '                cboPeriod.Enabled = False

    '                Call SetLoanParaControls(enParameterMode.LN_TOPUP)
    '                Call popupLoanParameter.Show()
    '                mblnPopupShowHide = True

    '            End If

    '        End If

    '        If e.CommandName.ToUpper = "DELETETOPUP" Then

    '            mstrCommandName = e.CommandName.ToUpper
    '            mintItemIndex = e.Item.ItemIndex

    '            Dim xtmp() As DataRow = Nothing
    '            If CInt(dgvTopupHistory.Items(mintItemIndex).Cells(7).Text) > 0 Then
    '                xtmp = mdtTopup.Select("lntopuptranunkid = '" & dgvTopupHistory.Items(mintItemIndex).Cells(7).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '            Else
    '                xtmp = mdtTopup.Select("GUID = '" & dgvTopupHistory.Items(mintItemIndex).Cells(8).Text & "' AND periodunkid > 0 AND AUD <> 'D' ")
    '            End If

    '            If xtmp.Length > 0 Then
    '                If CInt(xtmp(0).Item("lntopuptranunkid").ToString) > 0 Then

    '                    Dim objTnaLeaveTran As New clsTnALeaveTran
    '                    Dim objPeriod As New clscommom_period_Tran
    '                    objPeriod._Periodunkid = CInt(xtmp(0).Item("periodunkid"))
    '                    If objTnaLeaveTran.IsPayrollProcessDone(CInt(xtmp(0).Item("periodunkid")), cboEmpName.SelectedValue.ToString, objPeriod._End_Date) Then
    '                        Language.setLanguage(mstrModuleName)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 21, "Sorry, you cannot delete assiged Topup Information from this employee.Reason:Payroll Process already done for this employee for last date of current open period."), Me)
    '                        objPeriod = Nothing
    '                        objTnaLeaveTran = Nothing
    '                        Exit Sub
    '                    End If
    '                    objTnaLeaveTran = Nothing
    '                    objPeriod = Nothing

    '                    Dim xUsedMessage As String = String.Empty
    '                    xUsedMessage = objLoan_Advance.Check_Transaction_Linking("lntopuptranunkid", CInt(xtmp(0).Item("lntopuptranunkid")), mintLoanAdvanceUnkid)

    '                    If xUsedMessage.Trim.Length > 0 Then
    '                        DisplayMessage.DisplayMessage(xUsedMessage, Me)
    '                        Exit Sub
    '                    End If

    '                    popup_DeleteReason.Show()
    '                Else
    '                    xtmp(0).Item("AUD") = "D"

    '                    xtmp(0).AcceptChanges()
    '                    mdtTopup.AcceptChanges()
    '                    Call Fill_Topup_Transaction_Grid()
    '                End If
    '            End If

    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("dgvTopupHistory_ItemCommand:- " & ex.Message, Me)
    '    End Try
    'End Sub

    Protected Sub dgvTopupHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvTopupHistory.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                e.Item.Cells(5).Text = Format(CDec(e.Item.Cells(5).Text), Session("fmtCurrency").ToString)
                'Nilay (01-Apr-2016) -- Start
                'ENHANCEMENT - Approval Process in Loan Other Operations
                e.Item.Cells(4).Text = CDate(e.Item.Cells(4).Text).Date.ToString(Session("DateFormat").ToString, New System.Globalization.CultureInfo(""))
                'Nilay (01-Apr-2016) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvTopupHistory_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Protected Sub dgvHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvHistory.ItemDataBound
    '    Try
    '        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
    '            e.Item.Cells(3).Text = Format(CDec(e.Item.Cells(5).Text), Session("fmtCurrency"))
    '            e.Item.Cells(4).Text = Format(CDec(e.Item.Cells(5).Text), Session("fmtCurrency"))
    '            e.Item.Cells(5).Text = Format(CDec(e.Item.Cells(5).Text), Session("fmtCurrency"))
    '            e.Item.Cells(6).Text = Format(CDec(e.Item.Cells(5).Text), Session("fmtCurrency"))
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("" & ex.Message, Me)
    '    End Try
    'End Sub

#End Region

#Region "Button's Events"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim blnFlag As Boolean = False

            If IsValidate() = False Then Exit Sub

            Call SetValue()

            If mintLoanAdvanceUnkid <= 0 Then 'ADD Mode
                'Nilay (25-Mar-2016) -- Start
                'blnFlag = objLoan_Advance.Insert(CInt(Session("UserId")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("Database_Name").ToString)
                blnFlag = objLoan_Advance.Insert(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                 mdtStartDate, mdtEndDate, CStr(Session("UserAccessModeSetting")), True)
                'Nilay (25-Mar-2016) -- End

            Else 'EDIT Mode
                objLoan_Advance._Loanadvancetranunkid = mintLoanAdvanceUnkid
                'Nilay (05-May-2016) -- Start
                'blnFlag = objLoan_Advance.Update(CInt(Session("UserId")))
                blnFlag = objLoan_Advance.Update()
                'Nilay (05-May-2016) -- End
            End If

            If blnFlag = False And objLoan_Advance._Message <> "" Then
                DisplayMessage.DisplayMessage(objLoan_Advance._Message, Me)
            Else
                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Call SetDateFormat()
                'Pinkal (16-Apr-2016) -- End

                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                Dim objProcessLoan As New clsProcess_pending_loan

                GUI.fmtCurrency = Session("fmtCurrency").ToString

                'Nilay (10-Sept-2016) -- Start
                'Enhancement - Create New Loan Notification 
                'objProcessLoan.Send_Notification_After_Assign(Session("Database_Name").ToString, _
                '                                              CInt(Session("UserId")), _
                '                                              CInt(Session("Fin_year")), _
                '                                              CInt(Session("CompanyUnkId")), _
                '                                              CBool(Session("IsIncludeInactiveEmp")), _
                '                                              mdtStartDate, mdtEndDate, _
                '                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                                              CStr(Session("UserAccessModeSetting")), _
                '                                              CStr(Session("ArutiSelfServiceURL")), _
                '                                              objLoan_Advance._Loanadvancetranunkid, enLogin_Mode.MGR_SELF_SERVICE)
                'objProcessLoan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), _
                '                                          mintProcessPendingLoanUnkid, _
                '                                          clsProcess_pending_loan.enNoticationLoanStatus.ASSIGN, _
                '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , _
                '                                          enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))

                'Nilay (10-Dec-2016) -- Start
                'Issue #26: Setting to be included on configuration for Loan flow Approval process
                If mblnIsSendEmailNotification = True Then

                    Dim enMailType As New clsProcess_pending_loan.enApproverEmailType

                    If radLoan.Checked = True Then 'LOAN
                        enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Approver
                    ElseIf radAdvance.Checked = True Then 'ADVANCE
                        enMailType = clsProcess_pending_loan.enApproverEmailType.Loan_Advance
                    End If

                    objProcessLoan.Send_Notification_After_Assign(Session("Database_Name").ToString, _
                                                                  CInt(Session("UserId")), _
                                                                  CInt(Session("Fin_year")), _
                                                                  CInt(Session("CompanyUnkId")), _
                                                                  CBool(Session("IsIncludeInactiveEmp")), _
                                                                  mdtStartDate, mdtEndDate, _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                  CStr(Session("UserAccessModeSetting")), _
                                                                  CStr(Session("ArutiSelfServiceURL")), _
                                                                  objLoan_Advance._Loanadvancetranunkid, _
                                                                  enMailType, _
                                                                      enLogin_Mode.MGR_SELF_SERVICE, , , _
                                                                      Session("Notify_LoanAdvance_Users").ToString _
                                                                      )
                    'Hemant (30 Aug 2019) -- [Session("Notify_LoanAdvance_Users").ToString]
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objProcessLoan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), _
                    '                                      mintProcessPendingLoanUnkid, _
                    '                                      clsProcess_pending_loan.enNoticationLoanStatus.ASSIGN, _
                    '                                      enMailType, _
                    '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , _
                    '                                      enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                    objProcessLoan.Send_Notification_Employee(CInt(cboEmpName.SelectedValue), _
                                                          mintProcessPendingLoanUnkid, _
                                                          clsProcess_pending_loan.enNoticationLoanStatus.ASSIGN, _
                                                          enMailType, _
                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(Session("CompanyUnkId")), , _
                                                          enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                    'Sohail (30 Nov 2017) -- End
                End If
                'Nilay (10-Dec-2016) -- End

                'Nilay (10-Sept-2016) -- End

                'Shani (21-Jul-2016) -- End
                Call ClearLoanAdvanceValues()
                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
                If Request.QueryString.Count > 0 Then
                    Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
                    Exit Sub
                End If
                'Shani (21-Jul-2016) -- End


                If mintLoanAdvanceUnkid > 0 Then
                    'Nilay (21-Jul-2016) -- Start
                    'Enhancement - Create New Loan Notification 
                    Session("IsFromEdit") = Nothing
                    'Nilay (21-Jul-2016) -- End
                    Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvanceList.aspx", False)
                Else
                    Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Application/wPg_LoanApplicationList.aspx", False)
                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Call ClearLoanAdvanceValues()
            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            If Request.QueryString.Count > 0 Then
                Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
                Exit Sub
            End If
            'Shani (21-Jul-2016) -- End

            'Nilay (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            'If mintLoanAdvanceUnkid > 0 Then
            If mblnIsFromEdit = True Then
                Session("IsFromEdit") = Nothing
                'Nilay (21-Jul-2016) -- End
                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Assignment/wPg_NewLoanAdvanceList.aspx", False)
            Else
                Response.Redirect(Session("rootpath").ToString & "Loan_Savings/New_Loan/Loan_Application/wPg_LoanApplicationList.aspx", False)
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
    '    Try
    '        If mintItemIndex > -1 Then
    '            Call DELETE_LoanParameter()
    '        End If
    '    Catch ex As Exception

    '    End Try
    'End Sub

#End Region

#Region "Radio Button Event"

    Protected Sub radLoan_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radLoan.CheckedChanged
        Try
            If CType(sender, RadioButton).ID.ToUpper = "RADLOAN" Then
                lblloanamt.Visible = True
                txtLoanAmt.Visible = True
                lblAdvanceAmt.Visible = False
                txtAdvanceAmt.Visible = False
            ElseIf CType(sender, RadioButton).ID.ToUpper = "RADADVANCE" Then
                lblloanamt.Visible = False
                txtLoanAmt.Visible = False
                lblAdvanceAmt.Visible = True
                txtAdvanceAmt.Visible = True
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("radLoan_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Protected Sub radSimpleInterest_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radSimpleInterest.CheckedChanged
    '    Try
    '        If radSimpleInterest.Checked = True Then
    '            mintLastCalcTypeId = enLoanCalcId.Simple_Interest
    '            If mintLoanAdvanceUnkid < 0 Then
    '                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest)
    '                Call ShowHideProjectedLoan()
    '                If txtLoanRate.Enabled = False Then
    '                    txtLoanRate.Enabled = True
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("radSimpleInterest_CheckedChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub radReduceBalInterest_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radReduceBalInterest.CheckedChanged
    '    Try
    '        If radReduceBalInterest.Checked = True Then
    '            mintLastCalcTypeId = enLoanCalcId.Reducing_Amount
    '            If mintLoanAdvanceUnkid < 0 Then
    '                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount)
    '                Call ShowHideProjectedLoan()
    '                If txtLoanRate.Enabled = False Then
    '                    txtLoanRate.Enabled = True
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("radReduceBalInterest_CheckedChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

    'Protected Sub radNoInterest_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radNoInterest.CheckedChanged
    '    Try
    '        If radNoInterest.Checked = True Then
    '            If CDec(txtLoanRate.Text) > 0 Then
    '                Language.setLanguage(mstrModuleName)
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 32, "Sorry, you cannot change the loan calculation type. Reason : Interest rate is already define for this loan."), Me)
    '                If mintLastCalcTypeId <> 0 Then
    '                    Select Case mintLastCalcTypeId
    '                        Case enLoanCalcId.Simple_Interest
    '                            radNoInterest.Checked = False
    '                            radSimpleInterest.Checked = True
    '                        Case enLoanCalcId.Reducing_Amount
    '                            radNoInterest.Checked = False
    '                            radReduceBalInterest.Checked = True
    '                    End Select
    '                End If
    '                Exit Sub
    '            End If
    '            If mintLoanAdvanceUnkid = -1 Then
    '                Call ShowHideProjectedLoan()
    '                txtInstallmentAmt.ReadOnly = False
    '                txtLoanRate.Text = 0
    '                txtLoanRate.Enabled = False
    '            End If
    '            mintLastCalcTypeId = enLoanCalcId.No_Interest
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("radNoInterest_CheckedChanged:- " & ex.Message, Me)
    '    End Try
    'End Sub

#End Region

#Region "ComboBox Event"

    Protected Sub cboDeductionPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDeductionPeriod.SelectedIndexChanged

        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet

        Try
            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                Dim objPrd As New clscommom_period_Tran
                objPrd._Periodunkid(Session("Database_Name").ToString) = CInt(cboDeductionPeriod.SelectedValue)
                dtpDate.SetDate = objPrd._Start_Date
                dtpDate.Enabled = False
                objPrd = Nothing
            Else
                dtpDate.Enabled = True
                dtpDate.SetDate = DateAndTime.Now.Date
            End If

            objlblExRate.Text = ""
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpDate.GetDate.Date, True)
            If dsList.Tables("ExRate").Rows.Count > 0 Then
                mdecBaseExRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1"))
                'Hemant (30 Jan 2022) -- Start            
                'mdecPaidExRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                mdecExchangerate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2"))
                mdecPaidExRate = CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate"))
                'Hemant (30 Jan 2022) -- End
                mintPaidCurrId = CInt(dsList.Tables("ExRate").Rows(0).Item("exchangerateunkid"))
                'Hemant (30 Jan 2022) -- Start            
                'objlblExRate.Text = Format(mdecBaseExRate, Session("fmtCurrency").ToString) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dsList.Tables("ExRate").Rows(0).Item("currency_sign").ToString & " "
                objlblExRate.Text = Format(mdecBaseExRate, Session("fmtCurrency").ToString) & " " & mstrBaseCurrSign & " = " & mdecExchangerate.ToString & " " & dsList.Tables("ExRate").Rows(0).Item("currency_sign").ToString & " "
                'Hemant (30 Jan 2022) -- End
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboDeductionPeriod_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboLoanCalcType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanCalcType.SelectedIndexChanged, cboInterestCalcType.SelectedIndexChanged, cboPayPeriod.SelectedIndexChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType

            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If

            If mintProcessPendingLoanUnkid > 0 Then 'Add Mode(Assign)
                cboInterestCalcType.Enabled = True
            End If

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If CInt(cboLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                cboMappedHead.SelectedValue = "0"
                cboMappedHead.Enabled = False
            End If
            'Sohail (29 Apr 2019) -- End

            If CInt(cboLoanCalcType.SelectedValue) > 0 Then

                Select Case CInt(cboLoanCalcType.SelectedValue)

                    Case enLoanCalcId.Simple_Interest

                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        If (mblnIsFromEdit = False) Then
                            cboInterestCalcType.Enabled = True
                        End If
                        'Nilay (04-Nov-2016) -- End

                        txtDuration.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Simple_Interest

                        If mintProcessPendingLoanUnkid > 0 Then
                            Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)
                            Call ShowHideProjectedLoan()
                            txtInstallmentAmt.ReadOnly = True
                            If txtLoanRate.Enabled = False Then
                                txtLoanRate.Enabled = True
                            End If
                        End If

                    Case enLoanCalcId.Reducing_Amount

                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        If (mblnIsFromEdit = False) Then
                            cboInterestCalcType.Enabled = True
                        End If
                        'Nilay (04-Nov-2016) -- End

                        txtDuration.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Reducing_Amount

                        If mintProcessPendingLoanUnkid > 0 Then
                            Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)
                            'Call ShowHideProjectedLoan()
                            txtInstallmentAmt.ReadOnly = True
                            If txtLoanRate.Enabled = False Then
                                txtLoanRate.Enabled = True
                            End If
                        End If

                    Case enLoanCalcId.No_Interest

                        cboInterestCalcType.Enabled = False
                        RemoveHandler cboInterestCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
                        cboInterestCalcType.SelectedValue = "0"

                        If CDec(txtLoanRate.Text) > 0 Then

                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot change the loan calculation type. Reason : Interest rate is already define for this loan."), Me)

                            If mintLastCalcTypeId <> 0 Then
                                Select Case mintLastCalcTypeId
                                    Case enLoanCalcId.Simple_Interest
                                        cboLoanCalcType.SelectedValue = CStr(enLoanCalcId.Simple_Interest)

                                    Case enLoanCalcId.Reducing_Amount
                                        cboLoanCalcType.SelectedValue = CStr(enLoanCalcId.Reducing_Amount)
                                End Select
                            End If
                            Exit Sub

                        End If

                        If mintProcessPendingLoanUnkid > 0 Then
                            'Call ShowHideProjectedLoan()
                            'Hemant (17 Oct 2020) -- Start
			    'Issue : AH1108-HYATT TZ-The Holded loan not deducting after changing status to in progress
                            Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest, enIntCalcType)
                            'Hemant (17 Oct 2020) -- End
                            txtDuration.Enabled = True
                            txtInstallmentAmt.Text = Format(mdecInstallmentAmnt, Session("fmtCurrency").ToString)
                            txtInstallmentAmt.ReadOnly = False
                            txtLoanRate.Text = "0"
                            txtLoanRate.Enabled = False
                        End If

                        mintLastCalcTypeId = enLoanCalcId.No_Interest

                    Case enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI

                        'Nilay (04-Nov-2016) -- Start
                        'Enhancements: Global Change Status feature requested by {Rutta}
                        If (mblnIsFromEdit = False) Then
                            cboInterestCalcType.Enabled = True
                        End If
                        'Nilay (04-Nov-2016) -- End

                        txtDuration.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI

                        If mintProcessPendingLoanUnkid > 0 Then
                            Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)
                            'Call ShowHideProjectedLoan()
                            txtInstallmentAmt.ReadOnly = True
                            If txtLoanRate.Enabled = False Then
                                txtLoanRate.Enabled = True
                            End If
                        End If

                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    Case enLoanCalcId.No_Interest_With_Mapped_Head

                        cboInterestCalcType.Enabled = False
                        RemoveHandler cboInterestCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
                        cboInterestCalcType.SelectedValue = "0"

                        If CDec(txtLoanRate.Text) > 0 Then

                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 24, "Sorry, you cannot change the loan calculation type. Reason : Interest rate is already define for this loan."), Me)

                            If mintLastCalcTypeId <> 0 Then
                                Select Case mintLastCalcTypeId
                                    Case enLoanCalcId.Simple_Interest
                                        cboLoanCalcType.SelectedValue = CStr(enLoanCalcId.Simple_Interest)

                                    Case enLoanCalcId.Reducing_Amount
                                        cboLoanCalcType.SelectedValue = CStr(enLoanCalcId.Reducing_Amount)
                                End Select
                            End If
                            Exit Sub

                        End If

                        If mintProcessPendingLoanUnkid > 0 Then
                            txtDuration.Enabled = True
                            txtInstallmentAmt.Text = Format(mdecInstallmentAmnt, Session("fmtCurrency").ToString)
                            txtInstallmentAmt.ReadOnly = False
                            txtLoanRate.Text = "0"
                            txtLoanRate.Enabled = False
                            cboMappedHead.Enabled = True
                        End If

                        mintLastCalcTypeId = enLoanCalcId.No_Interest_With_Mapped_Head
                        'Sohail (29 Apr 2019) -- End

                End Select
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboLoanCalcType_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "TextBox Events"
    Protected Sub txtDuration_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDuration.TextChanged
        Try
            If mblnIsFormLoan = True Then

                Dim enIntCalcType As enLoanInterestCalcType

                If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                    enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
                Else
                    enIntCalcType = enLoanInterestCalcType.MONTHLY
                End If

                If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                    'Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, True)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                    'Hemant (12 Nov 2021) -- Start
                    'ISSUE(REA) : on changing No of installment of emi , emi amount should get changed too but it remained unchanged.                
                    txtPrincipalAmt.Text = Format(CDbl(CDec(txtLoanAmt.Text) / CDec(txtDuration.Text)), Session("fmtCurrency").ToString)
                    'Hemant (12 Nov 2021) -- End
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                    'Sohail (29 Apr 2019) -- End

                End If

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtDuration_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtInstallmentAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
        Try
            If mblnIsFormLoan = True Then

                Dim enIntCalcType As enLoanInterestCalcType

                If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                    enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
                Else
                    enIntCalcType = enLoanInterestCalcType.MONTHLY
                End If

                If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.No_Interest_With_Mapped_Head, enIntCalcType)
                    'Sohail (29 Apr 2019) -- End

                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtInstallmentAmt_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub txtLoanRate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLoanRate.TextChanged
        Try
            If mblnIsFormLoan = True Then
                If txtLoanRate.Text.Trim.Length <= 0 Then
                    txtLoanRate.Text = "0"
                End If

                Dim enIntCalcType As enLoanInterestCalcType

                If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                    enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
                Else
                    enIntCalcType = enLoanInterestCalcType.MONTHLY
                End If

                If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

                ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                    Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

                End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtLoanRate_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

    '#Region "Loan Parameter Popup"

    '#Region "Private Methods"

    '    Private Sub FillComboLoanPara()
    '        Try
    '            Dim dsList As DataSet = Nothing
    '            Dim objPeriodPara As New clscommom_period_Tran

    '            Dim objMstData As New clsMasterData
    '            mintCurrOpenPeriodId = objMstData.getFirstPeriodID(enModuleReference.Payroll, 1, CInt(Session("Fin_year")))

    '            dsList = objPeriodPara.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "List", True, 1)
    '            With cboPeriod
    '                .DataTextField = "name"
    '                .DataValueField = "periodunkid"
    '                .DataSource = dsList.Tables("List")
    '                .DataBind()
    '                .SelectedValue = mintCurrOpenPeriodId
    '            End With
    '            Call cboPeriod_SelectedIndexChanged(Nothing, Nothing)

    '        Catch ex As Exception
    '            DisplayMessage.DisplayMessage("FillComboLoanPara:- " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Sub SetLoanParaControls(ByVal xParameterMode As enParameterMode)
    '        Try
    '            mintParameterId = xParameterMode

    '            Select Case xParameterMode
    '                Case enParameterMode.LN_RATE
    '                    txtEMIInstallments.Enabled = False
    '                    txtInstallmentAmt.Enabled = False
    '                    txtLoanInterest.Enabled = True
    '                    txtDuration.Enabled = False
    '                    nudDuration.Enabled = False

    '                Case enParameterMode.LN_EMI
    '                    dtpEffectiveDate.Enabled = False
    '                    txtLoanInterest.Enabled = False
    '                    txtDuration.Enabled = True
    '                    nudDuration.Enabled = True
    '                    txtEMIInstallments.Enabled = True
    '                    txtInstallmentAmt.Enabled = True
    '                    txtInstallmentAmt.Visible = True
    '                    txtTopupAmt.Visible = False
    '                    lblEMIAmount.Visible = True
    '                    lblTopupAmt.Visible = False

    '                    If radNoInterest.Checked = True Then
    '                        nudDuration.Enabled = False
    '                    End If

    '                Case enParameterMode.LN_TOPUP
    '                    dtpEffectiveDate.Enabled = False
    '                    txtLoanInterest.Enabled = False
    '                    txtDuration.Enabled = False
    '                    nudDuration.Enabled = False
    '                    txtEMIInstallments.Enabled = False
    '                    lblEMIAmount.Visible = False
    '                    txtInstallmentAmt.Visible = False
    '                    lblTopupAmt.Visible = True
    '                    txtTopupAmt.Visible = True

    '            End Select

    '        Catch ex As Exception
    '            DisplayMessage.DisplayMessage("SetLoanParaControls:- " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Private Function ValidateLoanPara() As Boolean
    '        Try
    '            If CInt(cboPeriod.SelectedValue) <= 0 Then
    '                Language.setLanguage(mstrModuleName1)
    '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "Sorry, Period is mandatory information. Please select Period to continue."), Me)
    '                cboPeriod.Focus()
    '                Return False
    '            End If

    '            Select Case mintParameterId
    '                Case enParameterMode.LN_RATE
    '                    If CDec(txtLoanInterest.Text) <= 0 Then
    '                        Language.setLanguage(mstrModuleName1)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "Sorry, Interest Rate is mandatory information. Please set Interest Rate to continue."), Me)
    '                        txtLoanInterest.Focus()
    '                        Return False
    '                    End If
    '                Case enParameterMode.LN_EMI
    '                    If CInt(txtEMIInstallments.Text) <= 0 Or CDec(txtInstallmentAmt.Text) <= 0 Then
    '                        Language.setLanguage(mstrModuleName1)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "Sorry, Please give atleast No Of Installment or Installment Amount to continue."), Me)
    '                        txtEMIInstallments.Focus()
    '                        Return False
    '                    End If
    '                Case enParameterMode.LN_TOPUP
    '                    If CDec(txtTopupAmt.Text) <= 0 Then
    '                        Language.setLanguage(mstrModuleName1)
    '                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 4, "Sorry, Topup amount is mandatory information. Please set Totup amount to continue."), Me)
    '                        txtTopupAmt.Focus()
    '                        Return False
    '                    End If
    '            End Select

    '        Catch ex As Exception
    '            DisplayMessage.DisplayMessage("ValidateLoanPara:- " & ex.Message, Me)
    '        Finally
    '            'popupLoanParameter.Show()
    '        End Try
    '        Return True
    '    End Function

    '    Private Sub ClearLoanParameters()
    '        Try
    '            cboPeriod.Enabled = True
    '            dtpEffectiveDate.SetDate = Nothing
    '            txtLoanInterest.Text = 0
    '            txtDuration.Text = 1
    '            txtEMIInstallments.Text = 0
    '            txtInstallmentAmt.Text = 0
    '            txtTopupAmt.Text = 0
    '            mintItemIndex = -1
    '            mintParameterId = -1

    '            Me.ViewState("ParameterId") = Nothing
    '            Me.ViewState("PopupShowHide") = Nothing

    '        Catch ex As Exception
    '            DisplayMessage.DisplayMessage("ClearParameters:- " & ex.Message, Me)
    '        End Try
    '    End Sub

    '#End Region

    '#Region "ComboBox Events"
    '    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
    '        Try
    '            If CInt(cboPeriod.SelectedValue) > 0 Then
    '                Dim objPeriod As New clscommom_period_Tran
    '                objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
    '                dtpEffectiveDate.SetDate = objPeriod._Start_Date
    '            End If
    '        Catch ex As Exception
    '            DisplayMessage.DisplayMessage("cboPeriod_SelectedIndexChanged:- " & ex.Message, Me)
    '        Finally
    '            'popupLoanParameter.Show()
    '        End Try
    '    End Sub
    '#End Region

    '#Region "Button's Events"

    '    Protected Sub btnSavelnPara_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSavelnPara.Click
    '        Try
    '            If ValidateLoanPara() = False Then Exit Sub

    '            If mintItemIndex > -1 Then
    '                Call EDIT_LoanParameter()
    '            Else
    '                Call ADD_LoanParameter()
    '            End If

    '            Call ClearLoanParameters()
    '            mblnPopupShowHide = False
    '            popupLoanParameter.Hide()

    '        Catch ex As Exception
    '            DisplayMessage.DisplayMessage("btnSavelnPara_Click:- " & ex.Message, Me)
    '        End Try
    '    End Sub

    '    Protected Sub btnCloselnPara_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloselnPara.Click
    '        Try
    '            Call ClearLoanParameters()

    '            mblnPopupShowHide = False
    '            popupLoanParameter.Hide()

    '        Catch ex As Exception
    '            DisplayMessage.DisplayMessage("btnCloselnPara_Click:- " & ex.Message, Me)
    '        End Try
    '    End Sub

    '#End Region

    '#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.

#Region " Language & UI Settings "

    Private Sub SetLanguage()
        Try
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblDetialHeader.Text = Language._Object.getCaption("gbLoanAdvanceInfo", Me.lblDetialHeader.Text)
            Me.lblPurpose.Text = Language._Object.getCaption(Me.lblPurpose.ID, Me.lblPurpose.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblApprovedBy.Text = Language._Object.getCaption(Me.lblApprovedBy.ID, Me.lblApprovedBy.Text)
            Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.ID, Me.lblEmpName.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.ID, Me.lblEffectiveDate.Text)
            Me.lblVoucherNo.Text = Language._Object.getCaption(Me.lblVoucherNo.ID, Me.lblVoucherNo.Text)
            Me.lblloanamt.Text = Language._Object.getCaption(Me.lblloanamt.ID, Me.lblloanamt.Text)
            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.ID, Me.lblLoanScheme.Text)
            Me.lblDeductionPeriod.Text = Language._Object.getCaption(Me.lblDeductionPeriod.ID, Me.lblDeductionPeriod.Text)
            Me.lblAdvanceAmt.Text = Language._Object.getCaption(Me.lblAdvanceAmt.ID, Me.lblAdvanceAmt.Text)
            Me.radAdvance.Text = Language._Object.getCaption(Me.radAdvance.ID, Me.radAdvance.Text)
            Me.radLoan.Text = Language._Object.getCaption(Me.radLoan.ID, Me.radLoan.Text)
            Me.lblInterestAmt.Text = Language._Object.getCaption(Me.lblInterestAmt.ID, Me.lblInterestAmt.Text)
            Me.lblNetAmount.Text = Language._Object.getCaption(Me.lblNetAmount.ID, Me.lblNetAmount.Text)
            Me.lblPrincipalAmt.Text = Language._Object.getCaption(Me.lblPrincipalAmt.ID, Me.lblPrincipalAmt.Text)
            Me.lblInterestAmt.Text = Language._Object.getCaption(Me.lblInterestAmt.ID, Me.lblInterestAmt.Text)
            'Me.radReduceBalInterest.Text = Language._Object.getCaption(Me.radReduceBalInterest.ID, Me.radReduceBalInterest.Text)
            'Me.radSimpleInterest.Text = Language._Object.getCaption(Me.radSimpleInterest.ID, Me.radSimpleInterest.Text)
            'Me.radNoInterest.Text = Language._Object.getCaption(Me.radNoInterest.ID, Me.radNoInterest.Text)

            Me.lnProjectedAmount.Text = Language._Object.getCaption(Me.lnProjectedAmount.ID, Me.lnProjectedAmount.Text)
            Me.lblMappedHead.Text = Language._Object.getCaption(Me.lblMappedHead.ID, Me.lblMappedHead.Text)

            Me.dgvInterestHistory.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvInterestHistory.Columns(3).FooterText, Me.dgvInterestHistory.Columns(3).HeaderText)
            Me.dgvInterestHistory.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvInterestHistory.Columns(4).FooterText, Me.dgvInterestHistory.Columns(4).HeaderText)
            Me.dgvInterestHistory.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvInterestHistory.Columns(5).FooterText, Me.dgvInterestHistory.Columns(5).HeaderText)

            Me.dgvEMIHistory.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvEMIHistory.Columns(3).FooterText, Me.dgvEMIHistory.Columns(3).HeaderText)
            Me.dgvEMIHistory.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvEMIHistory.Columns(4).FooterText, Me.dgvEMIHistory.Columns(4).HeaderText)
            Me.dgvEMIHistory.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvEMIHistory.Columns(5).FooterText, Me.dgvEMIHistory.Columns(5).HeaderText)
            Me.dgvEMIHistory.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvEMIHistory.Columns(6).FooterText, Me.dgvEMIHistory.Columns(6).HeaderText)
            Me.dgvEMIHistory.Columns(7).HeaderText = Language._Object.getCaption(Me.dgvEMIHistory.Columns(7).FooterText, Me.dgvEMIHistory.Columns(7).HeaderText)

            Me.dgvTopupHistory.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvTopupHistory.Columns(3).FooterText, Me.dgvTopupHistory.Columns(3).HeaderText)
            Me.dgvTopupHistory.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvTopupHistory.Columns(4).FooterText, Me.dgvTopupHistory.Columns(4).HeaderText)
            Me.dgvTopupHistory.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvTopupHistory.Columns(5).FooterText, Me.dgvTopupHistory.Columns(5).HeaderText)
            Me.dgvTopupHistory.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvTopupHistory.Columns(6).FooterText, Me.dgvTopupHistory.Columns(6).HeaderText)

            'Me.dgvHistory.Columns(0).HeaderText = Language._Object.getCaption(Me.dgvHistory.Columns(0).FooterText, Me.dgvHistory.Columns(0).HeaderText)
            'Me.dgvHistory.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvHistory.Columns(1).FooterText, Me.dgvHistory.Columns(1).HeaderText)
            'Me.dgvHistory.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvHistory.Columns(2).FooterText, Me.dgvHistory.Columns(2).HeaderText)
            'Me.dgvHistory.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvHistory.Columns(3).FooterText, Me.dgvHistory.Columns(3).HeaderText)
            'Me.dgvHistory.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvHistory.Columns(4).FooterText, Me.dgvHistory.Columns(4).HeaderText)
            'Me.dgvHistory.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvHistory.Columns(5).FooterText, Me.dgvHistory.Columns(5).HeaderText)
            'Me.dgvHistory.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvHistory.Columns(6).FooterText, Me.dgvHistory.Columns(6).HeaderText)
            'Me.dgvHistory.Columns(7).HeaderText = Language._Object.getCaption(Me.dgvHistory.Columns(7).FooterText, Me.dgvHistory.Columns(7).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("" & Ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region 'Language & UI Settings
    '</Language>

End Class
