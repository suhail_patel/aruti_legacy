﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_LoanAdvanceOperationList.aspx.vb"
    Inherits="Loan_Savings_New_Loan_Loan_Assignment_wPg_LoanAdvanceOperationList"
    Title="Loan/Advance Operation List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
    function onlyNumbers(txtBox, e) {
        
        if (window.event)
            var charCode = window.event.keyCode;       // IE
        else
            var charCode = e.which;

        var cval = txtBox.value;

        if (cval.length > 0)
            if (charCode == 46)
            if (cval.indexOf(".") > -1)
            return false;

        if (charCode == 13)
            return false;

        if (charCode > 31 && (charCode < 46 || charCode > 57))
            return false;

        return true;
    }
  
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Loan/Advance Operation List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Loan/Advance Operation List"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%; margin-bottom: 10px;">
                                        <tr style="width: 100%">
                                            <td style="width: 12%">
                                                <asp:Label ID="lblEmpName" runat="server" Text="Employee Name"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:TextBox ID="txtEmployeeName" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblLoanScheme" runat="server" Style="margin-left: 10px" Text="Loan Scheme"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtLoanScheme" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblVocNo" runat="server" Style="margin-left: 10px" Text="Voucher No."></asp:Label>
                                            </td>
                                            <td style="width: 11%">
                                                <asp:TextBox ID="txtVocNo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="border: 1px solid #DDD">
                                    </div>
                                    <table style="width: 100%; margin-top: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <div id="Div6" class="panel-default">
                                                    <div id="Div7" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblPendingOp" runat="server" Text="Pending Operation(s)"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div8" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 10%">
                                                                    <asp:Label ID="lblStatusSearch" runat="server" Text="Status" Style="margin-left: 10px"></asp:Label>
                                                                </td>
                                                                <td style="width: 20%">
                                                                    <asp:DropDownList ID="cboStatusSearch" runat="server" AutoPostBack="true" Style="margin-left: 3px">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td style="width: 70%" align="right">
                                                                    <asp:CheckBox ID="chkMyApproval" runat="server" AutoPostBack="true" Text="My Approvals"
                                                                        Checked="true" Style="margin-right: 10px; font-size: 12px; font-weight: bold" />
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%" colspan="4">
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%" colspan="3">
                                                                    <asp:Panel ID="pnl_PendingOp" runat="server" ScrollBars="Auto" Style="max-height: 300px">
                                                                        <asp:DataGrid ID="dgPendingOp" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                            HeaderStyle-Font-Bold="false" Width="125%">
                                                                            <Columns>
                                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="lnkChangeStatus" Font-Underline="false" Style="font-weight: bold;
                                                                                                font-size: 12px" CommandName="ChangeStatus" Text="Change Status" runat="server"></asp:LinkButton>
                                                                                        </span>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <asp:BoundColumn DataField="PeriodName" HeaderText="Period" Visible="false" FooterText="dgcolhPeriodName">
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="effectivedate" HeaderText="Effective Date" FooterText="dgcolhEffectiveDate">
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="Operation" HeaderText="Operation" FooterText="dgcolhOperation">
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="interest_rate" HeaderText="Rate(%)" FooterText="dgcolhRate"
                                                                                    ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="emi_tenure" HeaderText="Installment No." FooterText="dgcolhInstlNo"
                                                                                    ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="emi_amount" HeaderText="Installment Amount" FooterText="dgcolhInstlAmount"
                                                                                    ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="topup_amount" HeaderText="Topup Amount" FooterText="dgcolhTopupAmount"
                                                                                    ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="Approver" HeaderText="Approver" FooterText="dgcolhApprover">
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="Status" HeaderText="Status" FooterText="dgcolhStatus">
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="remark" HeaderText="Remark" FooterText="dgcolhRemark">
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="isgrp" HeaderText="isgrp" Visible="false" FooterText="objdgcolhisgrp">
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="lnoperationtypeid" HeaderText="lnoperationtypeid" Visible="false"
                                                                                    FooterText="objdgcolhloanOpTypeId"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="MappedUserId" HeaderText="MappedUserId" Visible="false"
                                                                                    FooterText="objdgcolhmappeduserid"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="employeeunkid" HeaderText="employeeunkid" Visible="false"
                                                                                    FooterText="objdgcolhEmployeeId"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="approvertranunkid" HeaderText="approvertranunkid" Visible="false"
                                                                                    FooterText="objdgcolhlnapproverunkid"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="approverempunkid" HeaderText="approverempunkid" Visible="false"
                                                                                    FooterText="objdgcolhApproverEmpId"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="priority" HeaderText="priority" Visible="false" FooterText="objdgcolhPriority">
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="lnotheroptranunkid" HeaderText="lnotheroptranunkid" Visible="false"
                                                                                    FooterText="objdgcolhlnotheroptranunkid"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="identify_guid" HeaderText="identify_guid" Visible="false"
                                                                                    FooterText="objdgcolhIdentifyGuid"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="grpRow" HeaderText="grpRow" Visible="false"></asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="statusunkid" HeaderText="statusunkid" Visible="false">
                                                                                </asp:BoundColumn>
                                                                                <asp:BoundColumn DataField="loanadvancetranunkid" HeaderText="loanadvancetranunkid"
                                                                                    Visible="false"></asp:BoundColumn>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <div id="Div3" class="panel-default">
                                                    <div id="Div4" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblApprovedOp" runat="server" Text="Approved Operation(s)"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div5" class="panel-body-default">
                                                        <asp:Panel ID="pnl_ApprovedOp" runat="server" ScrollBars="Auto" Style="max-height: 300px">
                                                    <asp:DataGrid ID="lvData" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                        FooterText="btnEdit">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="btnDelete">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                            CommandName="Delete" CssClass="griddelete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="EffDate" HeaderText="Effective Date" FooterText="colhEffectiveDate">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="Rate" HeaderText="Rate(%)" FooterText="colhRate" ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="NoOfInstallment" HeaderText="Installment No." FooterText="colhINSTLNo"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="InstallmentAmount" HeaderText="Installment Amount" FooterText="colhINSTLAmt"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="TopupAmount" HeaderText="Topup Amount" FooterText="colhTopup"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="OprType" HeaderText="Operation" FooterText="colhChangeType">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="PeriodName" HeaderText="Period Name" Visible="false"
                                                                FooterText="objcolhPeriodName"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="periodunkid" HeaderText="objcolhPeriod" Visible="false"
                                                                FooterText="objcolhPeriod"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="IdType" HeaderText="objcolhIdType" Visible="false" FooterText="objcolhIdType">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="statusid" HeaderText="objcolhStatusId" Visible="false"
                                                                FooterText="objcolhStatusId"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="oDate" HeaderText="oDate" Visible="false" FooterText="objcolhoDate">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="TabUnkid" HeaderText="TabUnkid" Visible="false" FooterText="objcolhTabUnkid">
                                                            </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="isdefault" HeaderText="isdefault" Visible="false" FooterText="objcolhIsdefault">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="approverunkid" HeaderText="approverunkid" Visible="false"
                                                                        FooterText="objcolhApproverunkid"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="approver" HeaderText="Approver" FooterText="colhApprover">
                                                                    </asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <div style="float: left">
                                            <asp:Label ID="objlblEffDate" runat="server" Text=""></asp:Label>
                                        </div>
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupLoanParameter" runat="server" PopupControlID="pnl_LoanParameter"
                        TargetControlID="hdf_LoanParameter" CancelControlID="hdf_LoanParameter">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_LoanParameter" runat="server" CssClass="newpopup" Style="display: none;"
                        Width="40%">
                        <div class="panel-primary" style="margin-bottom: 0px;">
                            <div class="panel-heading">
                                <asp:Label ID="lblpopupHeader" runat="server" Text="Loan Parameters"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div2" class="panel-default">
                                    <div id="Div1" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblPopupDetailHeader" runat="server" Text="Loan Parameters"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div11" class="panel-body-default">
                                        <asp:Panel ID="pnlradOpr" runat="server" Style="border-bottom: 1px solid #DDD">
                                            <table style="width: 100%; margin-bottom: 10px">
                                                <tr style="width: 100%">
                                                    <td style="width: 100%">
                                                        <asp:Label ID="elOperation" runat="server" Text="Operation Type" Style="font-weight: bold;
                                                            font-size: 12px;"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 100%">
                                                        <asp:RadioButton ID="radInterestRate" Style="margin-left: 10px" runat="server" AutoPostBack="true"
                                                            Text="Loan Rate(%)" GroupName="OperationType" />
                                                        <asp:RadioButton ID="radInstallment" Style="margin-left: 10px" runat="server" AutoPostBack="true"
                                                            Text="Loan Installment Amount/No." GroupName="OperationType" />
                                                        <asp:RadioButton ID="radTopup" Style="margin-left: 10px" runat="server" AutoPostBack="true"
                                                            Text="Loan Topup" GroupName="OperationType" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlControls" runat="server">
                                            <table style="width: 100%; margin-top: 10px">
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblPeriod" runat="server" Text="Effective Period"></asp:Label>
                                                    </td>
                                                    <td style="width: 80%" colspan="2">
                                                        <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" Width="343px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                        <uc1:DateControl ID="dtpEffectiveDate" runat="server" />
                                                    </td>
                                                    <td style="width: 60%" colspan="2">
                                                        <asp:Label ID="objlblExRate" runat="server" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblLoanInterest" runat="server" Text="Interest (%)"></asp:Label>
                                                    </td>
                                                    <td style="width: 80%" colspan="2">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 23%;">
                                                                    <asp:TextBox ID="txtLoanInterest" runat="server" Text="0" AutoPostBack="true" Style="text-align: right;
                                                                        margin-left: -4px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 54%">
                                                                    <asp:Label ID="lblDuration" runat="server" Style="margin-left: 5px;" Text="Installment No. (In Months)"></asp:Label>
                                                                </td>
                                                                <td style="width: 23%">
                                                                    <asp:TextBox ID="txtDuration" runat="server" AutoPostBack="true" Text="1" Style="text-align: right;
                                                                        padding-right: 3px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblPrincipalAmt" runat="server" Text="Principal Amt."></asp:Label>
                                                    </td>
                                                    <td style="width: 80%" colspan="2">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 50%;">
                                                                    <asp:TextBox ID="txtPrincipalAmt" runat="server" Text="0" AutoPostBack="true" Style="text-align: right;
                                                                        margin-left: -4px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 27%">
                                                                    <asp:Label ID="lblInterestAmt" runat="server" Style="margin-left: 5px;" Text="Interest Amt."></asp:Label>
                                                                </td>
                                                                <td style="width: 23%">
                                                                    <asp:TextBox ID="txtInterestAmt" runat="server" ReadOnly="true" Text="1" Style="text-align: right;
                                                                        padding-right: 3px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 20%">
                                                        <asp:Label ID="lblEMIAmount" runat="server" Text="Installment Amount"></asp:Label>
                                                        <asp:Label ID="lblTopupAmt" runat="server" Text="Topup Amount" Visible="false"></asp:Label>
                                                    </td>
                                                    <td style="width: 80%" colspan="2">
                                                        <asp:TextBox ID="txtInstallmentAmt" runat="server" Style="text-align: right" Text="0" 
                                                            AutoPostBack="true" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                        <asp:TextBox ID="txtTopupAmt" runat="server" Style="text-align: right" Text="0" Visible="false"
                                                            onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnl_ChangeStatus" runat="server">
                                            <table style="width: 100%">
                                                <tr style="width: 100%">
                                                    <td style="width: 28%">
                                                        <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                                    </td>
                                                    <td style="width: 72%">
                                                        <asp:DropDownList ID="cboStatus" runat="server" Width="345px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td style="width: 28%" valign="top">
                                                        <asp:Label ID="lblRemark" runat="server" Text="Remark"></asp:Label>
                                                    </td>
                                                    <td style="width: 72%">
                                                        <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <div class="btn-default">
                                            <div style="float: left">
                                                <asp:Label ID="objlblEndDate" runat="server" Style="font-size: 12px" Text=""></asp:Label>
                                            </div>
                                            <asp:HiddenField ID="hdf_LoanParameter" runat="server" />
                                            <asp:Button ID="btnSavelnPara" runat="server" Text="Save" CssClass="btndefault" />
                                            <asp:Button ID="btnCloselnPara" runat="server" Text="Close" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <ucDel:DeleteReason ID="popupDeleteReason" Title="Are you sure you want to Delete?"
                        runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
