﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_Employee_Saving_List.aspx.vb" Inherits="Loan_Savings_wPg_Employee_Saving_List"
    Title="Employee Saving List" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="Delete" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function closePopup() {
        $('#imgclose').click(function(evt) {
            $find('<%= popupChangeStatus.ClientID %>').hide();
        });
    }

    function onlyNumbers(txtBox, evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;
        //var cval = document.getElementById(txtBoxId).value;
        var cval = txtBox.value;

        if (cval.length > 0)
                if (charCode == 46)
            if (cval.indexOf(".") > -1)
                    return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

        return true;
    }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Saving List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="width: 12%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblPayPeriod" runat="server" Text="Pay Period"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 10%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 12%">
                                                <asp:Label ID="lblSvScheme" runat="server" Text="Saving Scheme"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboSavingsScheme" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblContribution" runat="server" Text="Contribution"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtContribution" runat="server" Style="text-align: right" Text="0"
                                                    onKeypress="return onlyNumbers(this);"></asp:TextBox>
                                            </td>
                                            <td colspan="2">
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" CssClass="btndefault" Text="New" />
                                        <asp:Button ID="btnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:HiddenField ID="btnHidden" runat="Server" />
                                    </div>
                                </div>
                                <table style="width: 100%;">
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <asp:Panel ID="pnl_dgView" ScrollBars="Auto" runat="server" style="max-height:400px; margin: 10px">
                                                <asp:DataGrid ID="dgView" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" Width="150%">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="EditImg" runat="server" CommandName="Change" ToolTip="Edit" CssClass="gridedit"></asp:LinkButton>
                                                                </span>
                                                                <%--<asp:ImageButton ID="" runat="server" CommandName="Change" ImageUrl="~/images/edit.png"
                                                                    ToolTip="Edit" />--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Remove" ToolTip="Delete"
                                                                        CssClass="griddelete"></asp:LinkButton>
                                                                </span>
                                                                <%-- <asp:ImageButton ID="DeleteImg" runat="server" CommandName="Remove" ImageUrl="~/images/remove.png"
                                                                    ToolTip="Delete" />--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkChangeStatus" runat="server" Text="Change Status" Font-Underline="false"
                                                                    CommandName="Status" ToolTip="Change Status" /></ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkPayment" runat="server" Text="Repayment/Withdrawal" Font-Underline="false"
                                                                    CommandName="Payment" ToolTip="Repayment/Withdrawal" /></ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkDeposit" runat="server" Text="Deposit" Font-Underline="false"
                                                                    CommandName="Deposit" ToolTip="Deposit" /></ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn HeaderText="Employee" ReadOnly="true" DataField="employeename" FooterText="colhEmpName">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Saving Scheme" ReadOnly="True" DataField="savingscheme"
                                                            FooterText="colhSavingScheme"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Pay Period" ReadOnly="true" DataField="period" FooterText="colhPayPeriod">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Balance Amount" ReadOnly="true" DataField="balance_amount"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" FooterText="colhBalAmount">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Interest Rate" ReadOnly="true" DataField="interest_rate"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" FooterText="colhInterestRate">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Interested Accrued" ReadOnly="true" DataField="interest_amount"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" FooterText="colhInterestAccrued">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Contribution" ReadOnly="True" DataField="contribution"
                                                            ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" FooterText="colhContribution">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Status" ReadOnly="True" DataField="statusname" FooterText="colhStatus">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Stop Date" ReadOnly="True" DataField="stopdate" FooterText="colhStopDate">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="StatusId" ReadOnly="True" DataField="savingstatus" Visible="false">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="EmployeeId" ReadOnly="True" DataField="employeeunkid"
                                                            Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="bf_balance" ReadOnly="True" DataField="bf_balance" Visible="false">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="broughtforward" ReadOnly="True" DataField="broughtforward"
                                                            Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="countryunkid" ReadOnly="True" DataField="countryunkid"
                                                            Visible="false"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="effectivedate" Visible="false" />
                                                    </Columns>
                                                    <%--<PagerStyle Mode="NumericPages" />--%>
                                                </asp:DataGrid>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupChangeStatus" runat="server" TargetControlID="btnHidden"
                        PopupControlID="pnlChangeStatus" CancelControlID="btnClose" BackgroundCssClass="ModalPopupBG">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlChangeStatus" runat="server" CssClass="newpopup" Style="display: none"
                        Width="60%" >
                        <div class="panel-primary" style="margin:0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblStatusform" runat="server" Text="Add/Edit Saving Status"> </asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblTitle" runat="server" Text="Saving Status"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="LblSavingScheme" runat="server" Text="Saving Scheme"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:TextBox ID="txtSavingScheme" runat="server" Text="" ReadOnly="true"></asp:TextBox>
                                                </td>
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblEmpName" runat="server" Text="Employee Name"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:TextBox ID="txtEmployee" runat="server" Text="" ReadOnly="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboPeriod" Width="260px" runat="server" AutoPostBack="false">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 15%; vertical-align: top">
                                                    <asp:Label ID="lblRemarks" runat="server" Text="Remark"></asp:Label>
                                                </td>
                                                <td style="width: 35%" rowspan="2">
                                                    <asp:TextBox ID="txtStatusRemark" runat="server" Text="" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblSvStatus" runat="server" Text="Saving Status"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboStatusChange" Width="260px" runat="server" AutoPostBack="false">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <%--<tr style="width: 100%">
                                                <td style="width: 15%">
                                                </td>
                                                <td style="width: 35%">
                                                </td>
                                            </tr>--%>
                                        </table>
                                        <table style="width: 100%; margin-top: 10px">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:Panel ID="pnl_dgvHistory" ScrollBars="Auto" style="max-height:300px" runat="server">
                                                        <asp:DataGrid ID="dgvHistory" Style="margin: auto" runat="server" AutoGenerateColumns="False"
                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false" Width="100%">
                                                            <Columns>
                                                                <%--<asp:TemplateColumn>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate><asp:ImageButton ID="DeleteImgSavingStatus" runat="server" CommandName="Delete" ImageUrl="~/images/remove.png" ToolTip="Delete"  /></ItemTemplate>
                                                </asp:TemplateColumn>--%>
                                                                <asp:BoundColumn HeaderText="User" ReadOnly="True" DataField="username" FooterText="dgcolhUser">
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn HeaderText="Transaction Date" ReadOnly="True" DataField="status_date"
                                                                    FooterText="dgcolhTransactionDate"></asp:BoundColumn>
                                                                <asp:BoundColumn HeaderText="Status" ReadOnly="True" DataField="status" FooterText="dgcolhStatus">
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn HeaderText="IsGroup" ReadOnly="True"  DataField="grp" Visible="false"
                                                                    FooterText="objdgcolhIsGroup"></asp:BoundColumn>
                                                                <asp:BoundColumn HeaderText="objSavingstatustranunkid" ReadOnly="True" DataField="savingstatustranunkid"
                                                                    Visible="false" FooterText="objSavingstatustranunkid"></asp:BoundColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnDefault" />
                                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you sure you want to Delete this Employee's Saving Scheme?"
                        ValidationGroup="SavingScheme" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
