﻿Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Net.Dns
Imports System.Drawing
Imports System.Web.Services
Imports System.Globalization
Imports System.IO.Compression
Imports System.IO.Packaging

Partial Class Grievance_AgreeDisagreeResponse
    Inherits Basepage
#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "frmAgreeDisagreeResponse"
    Private DisplayMessage As New CommonCodes
    Private objresolution_step_tran As New clsResolution_Step_Tran
    Private objGrievance_master As New clsGrievance_Master
    Private objclsgreinitiater_response_tran As New clsgreinitiater_response_tran
    Private objDocument As New clsScan_Attach_Documents
    Private mdtGrievanceAttachment As DataTable = Nothing


    Private blnissubmited As Boolean = False
    Private mintResolutionStepTranunkid As Integer = 0
    Private mintGrievancemasterunkid As Integer = 0
    Private mintInitiatortranunkid As Integer = 0
    Private mintpriority As Integer = 0

    Private mblnIsFinal As String = ""
    Private mblnIsProcessed As String = ""
    Private objCONN As SqlConnection


    'Gajanan [10-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Changes        
    Private objemp As New clsEmployee_Master
    'Gajanan [10-June-2019] -- End


    'Gajanan [27-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
    Private dtPriviousApproverResolution As DataTable = Nothing
    Private mblnPreviousResponseBtn As Boolean = False
    'Gajanan [27-June-2019] -- End


#End Region

#Region "Public Method"
    Dim objclsMasterData As New clsMasterData

    Public Sub FillCombo()
        Dim dslist As DataSet
        Try
            Dim objAppr As New clsgrievanceapprover_master

            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
            'Dim dsApprovers As New DataSet
            Dim dsApprovers As New DataTable
            'Gajanan [24-OCT-2019] -- End
            Dim eApprType As enGrievanceApproval


            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   


            'If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
            '    eApprType = enGrievanceApproval.ApproverEmpMapping
            'ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
            '    eApprType = enGrievanceApproval.UserAcess
            'End If

            'Gajanan [24-OCT-2019] -- End

            Dim againstemp As Integer = objGrievance_master._Againstemployeeunkid


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
            'dsApprovers = objAppr.GetApproversList(eApprType, CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, CInt(Session("Employeeunkid")).ToString())


            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
            'dsApprovers = objAppr.GetApproversList(eApprType, CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, againstemp)


            If CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) = enGrievanceApproval.ReportingTo Then
                dsApprovers = objAppr.GetApproversList(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing).Tables(0)


                If IsNothing(dsApprovers) = False AndAlso dsApprovers.Rows.Count > 0 Then
                    Dim drrow() As DataRow = dsApprovers.Select("grievanceId=" & mintGrievancemasterunkid)

                    If drrow.Length > 0 Then
                        dsApprovers = drrow.CopyToDataTable()
                    End If

                End If
            Else
                dsApprovers = objAppr.GetApproversList(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, againstemp).Tables(0)
            End If


            'Gajanan [24-OCT-2019] -- End   
            'Gajanan [10-June-2019] -- End

            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   


            'If dsApprovers.Tables(0).Rows.Count <= 0 Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance."), Me)
            '    Exit Sub
            'End If
            'Dim dt() As DataRow = Nothing : Dim intPriority As Integer = -1

            'If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping OrElse _
            '     Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ReportingTo Then

            '    dt = dsApprovers.Tables(0).Select("approverempid = '" & CInt(IIf(againstemp > 0, 0, againstemp)) & "'")
            '    If dt.Length > 0 Then
            '        intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "'")

            '        If intPriority > 0 Then
            '            Dim dr As Integer = dsApprovers.Tables(0).Select("priority = " & intPriority & "approverempid = " & againstemp).Count
            '        End If

            '    Else
            '        dt = Nothing
            '        If intPriority <> -1 Then
            '            dt = dsApprovers.Tables(0).Select("priority = '" & intPriority & "' AND activestatus = 1")
            '            If dt.Length <= 0 Then
            '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)

            '                Exit Sub
            '            End If
            '        End If
            '        intPriority = IIf(IsDBNull(dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "'")), -2, dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "'"))
            '    End If

            'ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
            '    dt = dsApprovers.Tables(0).Select("approverempid = '" & CInt(IIf(againstemp > 0, 0, againstemp)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "' ")

            '    If dt.Length > 0 Then
            '        intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "'")
            '        If intPriority > 0 Then
            '            Dim dr As Integer = dsApprovers.Tables(0).Select("priority = " & intPriority & "approverempid = " & againstemp).Count
            '        End If

            '        'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
            '    Else
            '        dt = Nothing
            '        If intPriority <> -1 Then
            '            dt = dsApprovers.Tables(0).Select("priority = '" & intPriority & "'")
            '            If dt.Length <= 0 Then
            '                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
            '                Exit Sub
            '            End If
            '        End If
            '        intPriority = IIf(IsDBNull(dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "'")), -2, dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "'"))

            '    End If
            'End If

            If dsApprovers.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance."), Me)
                Exit Sub
            End If
            Dim dt() As DataRow = Nothing : Dim intPriority As Integer = -1

            If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping OrElse _
                 Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ReportingTo Then

                dt = dsApprovers.Select("approverempid = '" & CInt(IIf(againstemp > 0, 0, againstemp)) & "'")
                If dt.Length > 0 Then
                    intPriority = dsApprovers.Compute("MIN(priority)", "priority > '" & mintpriority & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "'")

                    If intPriority > 0 Then
                        Dim dr As Integer = dsApprovers.Select("priority = " & intPriority & "approverempid = " & againstemp).Count
                    End If

                Else
                    dt = Nothing
                    If intPriority <> -1 Then
                        dt = dsApprovers.Select("priority = '" & intPriority & "' AND activestatus = 1")
                        If dt.Length <= 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)

                            Exit Sub
                        End If
                    End If
                    intPriority = IIf(IsDBNull(dsApprovers.Compute("MIN(priority)", "priority > '" & mintpriority & "'")), -2, dsApprovers.Compute("MIN(priority)", "priority > '" & mintpriority & "'"))
                End If

            ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
                dt = dsApprovers.Select("approverempid = '" & CInt(IIf(againstemp > 0, 0, againstemp)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "' ")

                If dt.Length > 0 Then
                    intPriority = dsApprovers.Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "'")
                    If intPriority > 0 Then
                        Dim dr As Integer = dsApprovers.Select("priority = " & intPriority & "approverempid = " & againstemp).Count
                    End If

                Else
                    dt = Nothing
                    If intPriority <> -1 Then
                        dt = dsApprovers.Select("priority = '" & intPriority & "'")
                        If dt.Length <= 0 Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
                            Exit Sub
                        End If
                    End If
                    intPriority = IIf(IsDBNull(dsApprovers.Compute("MIN(priority)", "priority > '" & mintpriority & "'")), -2, dsApprovers.Compute("MIN(priority)", "priority > '" & mintpriority & "'"))

                End If
            End If
            'Gajanan [24-OCT-2019] -- End



            If intPriority = -2 Then
                'Gajanan [13-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
                'dslist = objclsMasterData.getComboListGrievanceResponse("List", True)
                dslist = objclsMasterData.getComboListGrievanceResponse("List", True, True)
                'Gajanan [13-July-2019] -- End
            Else
                'Gajanan [13-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Enhancement


                'Gajanan [24-OCT-2019] -- Start   
                'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   


                'Dim ApproverLevel As String = String.Join(",", dsApprovers.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("apprlevelunkid").ToString()).Distinct().ToArray())
                'If ApproverLevel.Length > 0 Then
                '    If objresolution_step_tran.IsResolutionAddedForGrievance(ApproverLevel, mintGrievancemasterunkid) Then
                '        dslist = objclsMasterData.getComboListGrievanceResponse("List", True, False)
                '    Else
                '        dslist = objclsMasterData.getComboListGrievanceResponse("List", False, False)
                '    End If
                'End If

                Select Case CType(Session("GrievanceApprovalSetting"), enGrievanceApproval)
                    Case enGrievanceApproval.ReportingTo
                        Dim ResolutionCount As Integer = 0

                        If CInt(Session("GrievanceReportingToMaxApprovalLevel")) > 0 Then
                            If objresolution_step_tran.IsInitiatorResponseAddedForGrievance(0, mintGrievancemasterunkid, CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), True, ResolutionCount) Then
                                If ResolutionCount = CInt(Session("GrievanceReportingToMaxApprovalLevel")) Then
                                    dslist = objclsMasterData.getComboListGrievanceResponse("List", True, True)
                                Else
                                    dslist = objclsMasterData.getComboListGrievanceResponse("List", True, False)
                                End If
                            Else
                                If ResolutionCount = CInt(Session("GrievanceReportingToMaxApprovalLevel")) Then
                                    dslist = objclsMasterData.getComboListGrievanceResponse("List", True, True)
                                Else
                                    dslist = objclsMasterData.getComboListGrievanceResponse("List", False, False)
                                End If

                            End If
                        Else
                            If objresolution_step_tran.IsInitiatorResponseAddedForGrievance(0, mintGrievancemasterunkid, CType(Session("GrievanceApprovalSetting"), enGrievanceApproval)) Then
                                dslist = objclsMasterData.getComboListGrievanceResponse("List", True, False)
                            Else
                                dslist = objclsMasterData.getComboListGrievanceResponse("List", False, False)
                            End If
                        End If



                    Case Else

                        Dim ApproverLevel As String = String.Join(",", dsApprovers.AsEnumerable().Select(Function(x) x.Field(Of Integer)("apprlevelunkid").ToString()).Distinct().ToArray())
                        If ApproverLevel.Length > 0 Then
                            If objresolution_step_tran.IsInitiatorResponseAddedForGrievance(ApproverLevel, mintGrievancemasterunkid, CType(Session("GrievanceApprovalSetting"), enGrievanceApproval)) Then
                                dslist = objclsMasterData.getComboListGrievanceResponse("List", True, False)
                            Else
                                dslist = objclsMasterData.getComboListGrievanceResponse("List", False, False)
                            End If
                        End If
                End Select

                'Gajanan [24-OCT-2019] -- End




            End If
            'Gajanan [13-July-2019] -- End

            drpresponsetype.DataSource = dslist
            drpresponsetype.DataTextField = "Name"
            drpresponsetype.DataValueField = "Id"
            drpresponsetype.DataBind()


            dslist = objresolution_step_tran.GetResponse_Type("Response Type", True)
            With drpresolutiontype
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dslist.Tables("Response Type")
                .DataBind()
            End With

            dslist = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.GRIEVANCE_TYPE, True, "Grievance Type")
            With drpGreType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dslist.Tables("Grievance Type")
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dslist.Dispose()
        End Try
    End Sub

    Private Sub GetValue()

        Try
            drpGreType.SelectedValue = Convert.ToString(objGrievance_master._Grievancetypeid)
            txtgrerefno.Text = objGrievance_master._Grievancerefno
            txtgredate.Text = objGrievance_master._Grievancedate
            txtGreDesc.Text = objGrievance_master._Grievance_Description
            txtRelifWanted.Text = objGrievance_master._Grievance_Reliefwanted
            drpresolutiontype.SelectedValue = Convert.ToString(objresolution_step_tran._Responsetypeunkid)
            txtresolutionremark.Text = objresolution_step_tran._Responseremark
            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
            txtQualificationRemark.Text = objresolution_step_tran._Qualifyremark

            objemp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate").ToString).Date) = objGrievance_master._Againstemployeeunkid
            txtAgainstEmp.Text = objemp._Firstname + " " + objemp._Surname
            'Gajanan [10-June-2019] -- End

            If mintInitiatortranunkid > 0 Then
                txtresponseremark.Text = objclsgreinitiater_response_tran._Remark
                drpresponsetype.SelectedValue = objclsgreinitiater_response_tran._Statusunkid
            End If

            drpGreType.Enabled = False
            txtgrerefno.Enabled = False
            txtgredate.Enabled = False
            txtGreDesc.Enabled = False
            txtRelifWanted.Enabled = False
            drpresolutiontype.Enabled = False
            txtresolutionremark.Enabled = False


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
            txtQualificationRemark.Enabled = False
            txtAgainstEmp.Enabled = False
            'Gajanan [10-June-2019] -- End


            mdtGrievanceAttachment = objDocument.GetQulificationAttachment(objGrievance_master._Fromemployeeunkid, enScanAttactRefId.GRIEVANCES, mintGrievancemasterunkid, CStr(Session("Document_Path")))
            fillAttachment()



            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            Dim objApprover As New clsgrievanceapprover_master
            objApprover._Approvermasterunkid = CInt(objresolution_step_tran._Approvermasterunkid)

            dtPriviousApproverResolution = objresolution_step_tran.GetEmployeeResolutionStatus(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), _
                                                                                               objApprover._Mapuserunkid, Session("Database_Name").ToString, _
                                                                                               CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                                                               Session("UserAccessModeSetting").ToString, Session("EmployeeAsOnDate").ToString(), _
                                                                                               objGrievance_master._Againstemployeeunkid, objGrievance_master._Grievancerefno, objGrievance_master._Grievancemasterunkid, True, , , CInt(Session("Employeeunkid")))


            If IsNothing(dtPriviousApproverResolution) = False AndAlso dtPriviousApproverResolution.Rows.Count > 0 Then
                mblnPreviousResponseBtn = True
                btnViewPreviousResolution.Visible = True
                ViewState("dtPriviousApproverResolution") = dtPriviousApproverResolution
            Else
                mblnPreviousResponseBtn = False
            End If
            'Gajanan [27-June-2019] -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue()

        Try
            If mintResolutionStepTranunkid > 0 Then
                objresolution_step_tran._ResolutionStepTranunkid = mintResolutionStepTranunkid
            End If

            objGrievance_master._Grievancemasterunkid = mintGrievancemasterunkid
            objclsgreinitiater_response_tran._Resolutionsteptranunkid = mintResolutionStepTranunkid
            objclsgreinitiater_response_tran._Grievancemasterunkid = mintGrievancemasterunkid
            objclsgreinitiater_response_tran._Statusunkid = CInt(drpresponsetype.SelectedValue)
            objclsgreinitiater_response_tran._Remark = txtresponseremark.Text
            objclsgreinitiater_response_tran._Initiatorunkid = objGrievance_master._Fromemployeeunkid
            objclsgreinitiater_response_tran._Isvoid = False
            objclsgreinitiater_response_tran._Voiduserunkid = -1
            objclsgreinitiater_response_tran._Voidreason = String.Empty
            objclsgreinitiater_response_tran._Voiddatetime = Nothing
            objclsgreinitiater_response_tran._Isprocessed = 0



            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   


            'Dim objAppr As New clsgrievanceapprover_master
            'objAppr._Approvermasterunkid = CInt(objresolution_step_tran._Approvermasterunkid)
            'objclsgreinitiater_response_tran._Approvalsettingid = objAppr._Approvalsettingid
            'objclsgreinitiater_response_tran._Apprlevelunkid = objAppr._Apprlevelunkid
            'objclsgreinitiater_response_tran._Approverempid = objAppr._Approverempid


            If CInt(Session("GrievanceApprovalSetting").ToString()) = CInt(enGrievanceApproval.ReportingTo) Then
                Dim objAppr As New clsEmployee_Master
                objAppr._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), Nothing) = CInt(objresolution_step_tran._Approvermasterunkid)
                objclsgreinitiater_response_tran._Approvalsettingid = CInt(enGrievanceApproval.ReportingTo)
                objclsgreinitiater_response_tran._Apprlevelunkid = 0
                objclsgreinitiater_response_tran._Approverempid = CInt(objresolution_step_tran._Approvermasterunkid)
                objAppr = Nothing
            Else
                Dim objAppr As New clsgrievanceapprover_master
                objAppr._Approvermasterunkid = CInt(objresolution_step_tran._Approvermasterunkid)
                objclsgreinitiater_response_tran._Approvalsettingid = objAppr._Approvalsettingid
                objclsgreinitiater_response_tran._Apprlevelunkid = objAppr._Apprlevelunkid
                objclsgreinitiater_response_tran._Approverempid = objAppr._Approverempid
                objAppr = Nothing
            End If
            'Gajanan [24-OCT-2019] -- End


            objclsgreinitiater_response_tran._AuditDate = Now
            objclsgreinitiater_response_tran._ClientIp = Session("IP_ADD").ToString()
            objclsgreinitiater_response_tran._HostName = Session("HOST_NAME").ToString()
            'objclsgreinitiater_response_tran._Loginemployeeunkid = -1
            'objclsgreinitiater_response_tran._AuditUserid = CInt(Session("UserId"))

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objclsgreinitiater_response_tran._AuditUserid = CInt(Session("UserId"))
            Else
                objclsgreinitiater_response_tran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
            End If

            objclsgreinitiater_response_tran._FormName = mstrModuleName
            objclsgreinitiater_response_tran._IsFromWeb = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try

            If CInt(drpresponsetype.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)

                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Responsetype is compulsory information. Please select Responsetype to continue."), Me)
                drpresponsetype.Focus()
                Return False
            End If

            If CInt(txtresponseremark.Text.Trim.Length) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Responseremark is compulsory information. Please select Responseremark to continue."), Me)
                txtresponseremark.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SaveResolutionData(ByVal blnIsSubmmitted As Boolean)
        Dim blnFlag As Boolean = False
        Dim objAppr As New clsgrievanceapprover_master

        'Gajanan [24-OCT-2019] -- Start   
        'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   


        'Dim dsApprovers As New DataSet
        Dim dsApprovers As New DataTable
        'Gajanan [24-OCT-2019] -- End


        'Gajanan [24-OCT-2019] -- Start   
        'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   


        'Dim eApprType As enGrievanceApproval
        'If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
        '    eApprType = enGrievanceApproval.ApproverEmpMapping
        'ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
        '    eApprType = enGrievanceApproval.UserAcess
        'End If


        'Gajanan [24-OCT-2019] -- End

        Try
            SetValue()
            objclsgreinitiater_response_tran._Issubmitted = blnIsSubmmitted

            If mintInitiatortranunkid > 0 Then

                'Gajanan [13-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
                'If blnIsSubmmitted AndAlso (drpresponsetype.SelectedValue = enGrievanceResponse.AgreedAndClose OrElse drpresponsetype.SelectedValue = enGrievanceResponse.Appeal OrElse drpresponsetype.SelectedValue = enGrievanceResponse.DisagreeAndClose) Then
                If blnIsSubmmitted AndAlso (drpresponsetype.SelectedValue = enGrievanceResponse.AgreedAndClose OrElse drpresponsetype.SelectedValue = enGrievanceResponse.Disagree OrElse drpresponsetype.SelectedValue = enGrievanceResponse.DisagreeAndClose) Then
                    'Gajanan [13-July-2019] -- End

                    'objGrievance_master._AuditUserId = CInt(Session("UserId"))
                    'objGrievance_master._ClientIP = Session("IP_ADD").ToString()
                    'objGrievance_master._HostName = Session("HOST_NAME").ToString()
                    'objGrievance_master._LoginEmployeeUnkid = -1
                    'objGrievance_master._FormName = mstrModuleName
                    'objGrievance_master._FromWeb = True

                    blnFlag = objclsgreinitiater_response_tran.Update(mintInitiatortranunkid, True)

                    If blnFlag = False Then
                        DisplayMessage.DisplayMessage(objclsgreinitiater_response_tran._Message, Me)
                        Exit Sub

                    End If

                Else
                    blnFlag = objclsgreinitiater_response_tran.Update(mintInitiatortranunkid)

                    If blnFlag = False Then
                        DisplayMessage.DisplayMessage(objclsgreinitiater_response_tran._Message, Me)
                        Exit Sub

                    End If

                End If
            Else

                'Gajanan [13-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Enhancement

                'If blnIsSubmmitted AndAlso (drpresponsetype.SelectedValue = enGrievanceResponse.AgreedAndClose OrElse drpresponsetype.SelectedValue = enGrievanceResponse.Appeal OrElse drpresponsetype.SelectedValue = enGrievanceResponse.DisagreeAndClose) Then
                If blnIsSubmmitted AndAlso (drpresponsetype.SelectedValue = enGrievanceResponse.AgreedAndClose OrElse drpresponsetype.SelectedValue = enGrievanceResponse.Disagree OrElse drpresponsetype.SelectedValue = enGrievanceResponse.DisagreeAndClose) Then
                    'Gajanan [13-July-2019] -- End

                    'objGrievance_master._AuditUserId = CInt(Session("UserId"))
                    'objGrievance_master._ClientIP = Session("IP_ADD").ToString()
                    'objGrievance_master._HostName = Session("HOST_NAME").ToString()
                    'objGrievance_master._LoginEmployeeUnkid = -1
                    'objGrievance_master._FormName = mstrModuleName
                    'objGrievance_master._FromWeb = True
                    blnFlag = objclsgreinitiater_response_tran.Insert(True)

                    If blnFlag = False Then
                        DisplayMessage.DisplayMessage(objclsgreinitiater_response_tran._Message, Me)
                        Exit Sub

                    End If

                Else
                    blnFlag = objclsgreinitiater_response_tran.Insert()

                    If blnFlag = False Then
                        DisplayMessage.DisplayMessage(objclsgreinitiater_response_tran._Message, Me)
                        Exit Sub
                    End If

                End If
            End If


            If blnissubmited = True AndAlso blnFlag = True Then

                'Gajanan [13-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Enhancement

                'If drpresponsetype.SelectedValue = enGrievanceResponse.DisagreeAndEscalateToNextLevel Then
                If drpresponsetype.SelectedValue = enGrievanceResponse.DisagreeAndEscalateToNextLevel Or drpresponsetype.SelectedValue = enGrievanceResponse.Appeal Then
                    'Gajanan [13-July-2019] -- End
                    Dim againstemp As Integer = objGrievance_master._Againstemployeeunkid


                    'Gajanan [10-June-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                    'dsApprovers = objAppr.GetApproversList(eApprType, CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, CInt(Session("Employeeunkid")).ToString())


                    'Gajanan [24-OCT-2019] -- Start   
                    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
                    'dsApprovers = objAppr.GetApproversList(eApprType, CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, againstemp)


                    If CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) = enGrievanceApproval.ReportingTo Then

                    Else
                    End If


                    If CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) = enGrievanceApproval.ReportingTo Then
                        dsApprovers = objAppr.GetApproversList(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing).Tables(0)
                        If IsNothing(dsApprovers) = False AndAlso dsApprovers.Rows.Count > 0 Then
                            Dim drrow() As DataRow = dsApprovers.Select("grievanceId=" & mintGrievancemasterunkid)

                            If drrow.Length > 0 Then
                                dsApprovers = drrow.CopyToDataTable()
                            End If
                        End If
                    Else
                        dsApprovers = objAppr.GetApproversList(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, againstemp).Tables(0)
                    End If
                    'Gajanan [24-OCT-2019] -- End

                    'Gajanan [10-June-2019] -- End


                    'Gajanan [24-OCT-2019] -- Start   
                    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   


                    'If dsApprovers.Tables(0).Rows.Count <= 0 Then
                    '    'Gajanan [24-June-2019] -- Start      
                    '    'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance."), Me)
                    '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance."), Me)
                    '    'Gajanan [24-June-2019] -- End
                    '    Exit Sub
                    'End If
                    'Dim dt() As DataRow = Nothing : Dim intPriority As Integer = -1


                    'If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping OrElse _
                    '   Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ReportingTo Then

                    '    dt = dsApprovers.Tables(0).Select("approverempid = '" & CInt(IIf(againstemp > 0, 0, againstemp)) & "'")
                    '    If dt.Length > 0 Then

                    '        If IsDBNull(dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "'")) = False Then
                    '            intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "'")
                    '        Else
                    '            'Sohail (23 Mar 2019) -- Start
                    '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    '            'DisplayMessage.DisplayError(ex, Me)
                    '            DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                    '            'Sohail (23 Mar 2019) -- End
                    '            Exit Sub
                    '        End If


                    '        'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "'")
                    '    Else
                    '        If IsDBNull(dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "'")) = False Then
                    '            intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "'")
                    '        Else
                    '            'Sohail (23 Mar 2019) -- Start
                    '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    '            'DisplayMessage.DisplayError(ex, Me)
                    '            DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                    '            'Sohail (23 Mar 2019) -- End
                    '            Exit Sub
                    '        End If
                    '        'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "'")
                    '    End If
                    '    dt = Nothing
                    '    If intPriority <> -1 Then
                    '        dt = dsApprovers.Tables(0).Select("priority = '" & intPriority & "' AND activestatus = 1")
                    '        If dt.Length <= 0 Then

                    '            'Gajanan [24-June-2019] -- Start      



                    '            'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
                    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
                    '            'Gajanan [24-June-2019] -- End


                    '            Exit Sub
                    '        End If
                    '    End If

                    'ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then

                    '    dt = dsApprovers.Tables(0).Select("approverempid = '" & CInt(IIf(againstemp > 0, 0, againstemp)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "' ")

                    '    If dt.Length > 0 Then

                    '        If IsDBNull(dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "'")) = False Then
                    '            intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "'")
                    '        Else
                    '            'Sohail (23 Mar 2019) -- Start
                    '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    '            'DisplayMessage.DisplayError(ex, Me)
                    '            DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                    '            'Sohail (23 Mar 2019) -- End
                    '            Exit Sub
                    '        End If

                    '        'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
                    '    Else
                    '        If IsDBNull(dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "'")) = False Then
                    '            intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "'")
                    '        Else
                    '            'Sohail (23 Mar 2019) -- Start
                    '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    '            'DisplayMessage.DisplayError(ex, Me)
                    '            DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                    '            'Sohail (23 Mar 2019) -- End
                    '            Exit Sub
                    '        End If
                    '        'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "")
                    '    End If
                    '    dt = Nothing
                    '    If intPriority <> -1 Then
                    '        dt = dsApprovers.Tables(0).Select("priority = '" & intPriority & "' AND activestatus = 1")
                    '        If dt.Length <= 0 Then

                    '            'Gajanan [24-June-2019] -- Start      
                    '            'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
                    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
                    '            'Gajanan [24-June-2019] -- End
                    '            Exit Sub
                    '        End If
                    '    End If
                    'End If


                    If dsApprovers.Rows.Count <= 0 Then
                        'Gajanan [24-June-2019] -- Start      
                        'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance."), Me)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance."), Me)
                        'Gajanan [24-June-2019] -- End
                        Exit Sub
                    End If
                    Dim dt() As DataRow = Nothing : Dim intPriority As Integer = -1


                    If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping OrElse _
                       Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ReportingTo Then

                        dt = dsApprovers.Select("approverempid = '" & CInt(IIf(againstemp > 0, 0, againstemp)) & "'")
                        If dt.Length > 0 Then

                            If IsDBNull(dsApprovers.Compute("MIN(priority)", "priority > '" & mintpriority & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "'")) = False Then
                                intPriority = dsApprovers.Compute("MIN(priority)", "priority > '" & mintpriority & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "'")
                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError(ex, Me)
                                DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                                'Sohail (23 Mar 2019) -- End
                                Exit Sub
                            End If


                            'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "'")
                        Else
                            If IsDBNull(dsApprovers.Compute("MIN(priority)", "priority > '" & mintpriority & "'")) = False Then
                                intPriority = dsApprovers.Compute("MIN(priority)", "priority > '" & mintpriority & "'")
                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError(ex, Me)
                                DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                                'Sohail (23 Mar 2019) -- End
                                Exit Sub
                            End If
                            'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority > '" & mintpriority & "'")
                        End If
                        dt = Nothing
                        If intPriority <> -1 Then
                            dt = dsApprovers.Select("priority = '" & intPriority & "' AND activestatus = 1")
                            If dt.Length <= 0 Then

                                'Gajanan [24-June-2019] -- Start      



                                'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
                                'Gajanan [24-June-2019] -- End


                                Exit Sub
                            End If
                        End If

                    ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then

                        dt = dsApprovers.Select("approverempid = '" & CInt(IIf(againstemp > 0, 0, againstemp)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "' ")

                        If dt.Length > 0 Then

                            If IsDBNull(dsApprovers.Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "'")) = False Then
                                intPriority = dsApprovers.Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(againstemp = "", 0, againstemp)) & "' and ucompanyid = '" & CInt(Session("CompanyUnkId")) & "'")
                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError(ex, Me)
                                DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                                'Sohail (23 Mar 2019) -- End
                                Exit Sub
                            End If

                            'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "priority >= '" & CInt(dt(0)("priority")) & "' AND approverempid <> '" & CInt(IIf(drppopupEmployee.SelectedValue = "", 0, drppopupEmployee.SelectedValue)) & "'")
                        Else
                            If IsDBNull(dsApprovers.Compute("MIN(priority)", "priority > '" & mintpriority & "'")) = False Then
                                intPriority = dsApprovers.Compute("MIN(priority)", "priority > '" & mintpriority & "'")
                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError(ex, Me)
                                DisplayMessage.DisplayMessage("Sorry,There Is No Approver Availabel So You Can Not Apply This Grievacne.", Me)
                                'Sohail (23 Mar 2019) -- End
                                Exit Sub
                            End If
                            'intPriority = dsApprovers.Tables(0).Compute("MIN(priority)", "")
                        End If
                        dt = Nothing
                        If intPriority <> -1 Then
                            dt = dsApprovers.Select("priority = '" & intPriority & "' AND activestatus = 1")
                            If dt.Length <= 0 Then

                                'Gajanan [24-June-2019] -- Start      
                                'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Approver is inactive. Please contact system admin in order to post grievance."), Me)
                                'Gajanan [24-June-2019] -- End
                                Exit Sub
                            End If
                        End If
                    End If
                    'Gajanan [24-OCT-2019] -- End





                    Dim objRaiseEmp As New clsEmployee_Master
                    objRaiseEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))

                    Dim objAgainstEmp As New clsEmployee_Master
                    objAgainstEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(objGrievance_master._Againstemployeeunkid)

                    objAppr.SendNotificationToApprover(dt, Session("ArutiSelfServiceURL"), mstrModuleName, enLogin_Mode.EMP_SELF_SERVICE, IIf(objRaiseEmp._Email = "", objRaiseEmp._Firstname & " " & objRaiseEmp._Surname, objRaiseEmp._Email), Convert.ToString(objAgainstEmp._Firstname & " " & objAgainstEmp._Surname), objGrievance_master._Grievancerefno, objGrievance_master._Grievancemasterunkid, objRaiseEmp._Firstname & " " & objRaiseEmp._Surname, CInt(Session("CompanyUnkId")), CInt(Session("Employeeunkid")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString(), lblGreType.Text)


                    'Gajanan [10-June-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                    objAppr.SendNotificationToEmployeeOnRaisedGrievance(dt, mstrModuleName, objemp._Email, enLogin_Mode.EMP_SELF_SERVICE, IIf(objemp._Email = "", objemp._Firstname & " " & objemp._Surname, objemp._Email), Convert.ToString(objAgainstEmp._Firstname & " " & objAgainstEmp._Surname), objGrievance_master._Grievancerefno, objGrievance_master._Grievancemasterunkid, StrConv(objemp._Firstname & " " & objemp._Surname, VbStrConv.ProperCase), CInt(Session("CompanyUnkId")), CInt(Session("Employeeunkid")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString(), lblGreType.Text)
                    'Gajanan [10-June-2019] -- End

                    objRaiseEmp = Nothing
                    objAgainstEmp = Nothing



                    'Gajanan [13-July-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
                ElseIf drpresponsetype.SelectedValue = enGrievanceResponse.Disagree Then

                    Dim objRaiseEmp As New clsEmployee_Master
                    objRaiseEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))

                    Dim objAgainstEmp As New clsEmployee_Master
                    objAgainstEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(objGrievance_master._Againstemployeeunkid)


                    objAppr.SendNotificationToEmployeeAfterDisagreeFinalResolution(mstrModuleName, objRaiseEmp._Email, enLogin_Mode.EMP_SELF_SERVICE, _
                                                                                   IIf(objRaiseEmp._Email = "", objRaiseEmp._Firstname & " " & objRaiseEmp._Surname, objRaiseEmp._Email), _
                                                                                   objAgainstEmp._Firstname & " " & objAgainstEmp._Surname, objGrievance_master._Grievancerefno, _
                                                                                   objGrievance_master._Grievancemasterunkid, objRaiseEmp._Firstname & " " & objRaiseEmp._Surname, _
                                                                                   CInt(Session("CompanyUnkId")), CInt(Session("Employeeunkid")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())


                    If Session("EmployeeDisagreeGrievanceNotificationUserIds").ToString.Length > 0 Then
                        Dim objLetterType As New clsLetterType
                        Dim objLetterFields As New clsLetterFields
                        Dim DtTable As New DataTable

                        objLetterType._LettertypeUnkId = Session("EmployeeDisagreeGrievanceNotificationTemplateId")

                        DtTable = objLetterFields.GetEmployeeData(objGrievance_master._Grievancerefno, enImg_Email_RefId.Grievance, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        CInt(Session("CompanyUnkId")), CStr(Session("Database_Name")), "GrievanceFieldList").Tables("GrievanceFieldList")

                        If IsNothing(DtTable) = False AndAlso DtTable.Rows.Count > 0 Then

                            objclsgreinitiater_response_tran.SendNotificationDisagreeGrievance(DtTable, Session("EmployeeDisagreeGrievanceNotificationUserIds").ToString(), _
                                                                                               mstrModuleName, enLogin_Mode.EMP_SELF_SERVICE, "", Session("EmployeeDisagreeGrievanceNotificationEmailTitle").ToString(), _
                                                                                               objLetterType._Lettercontent, objGrievance_master._Grievancerefno, _
                                                                                               objGrievance_master._Grievancemasterunkid, CInt(Session("CompanyUnkId")), _
                                                                                               CInt(Session("Employeeunkid")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                        End If

                    End If




                    'Gajanan [13-July-2019] -- End


                End If

            End If
            'Gajanan [10-July-2019] -- Start
            'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Save Successfully"), Me)
            'Gajanan [10-July-2019] -- End
            mintInitiatortranunkid = -1
            mintGrievancemasterunkid = -1
            mintpriority = -1
            mintResolutionStepTranunkid = -1


            'Gajanan [10-July-2019] -- Start      

            'If Request.QueryString.ToString.Length > 0 Then
            '    Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
            'Else
            '    Response.Redirect(Session("rootpath").ToString & "Grievance/GrievanceInitiatorResponseList.aspx", False)
            'End If

            If Request.QueryString.ToString.Length > 0 Then
                If blnissubmited = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Submitted Successfully"), Me, Session("rootpath").ToString & "Index.aspx")
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Save Successfully"), Me, Session("rootpath").ToString & "Index.aspx")
                End If
            Else
                If blnissubmited = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "Submitted Successfully"), Me, Session("rootpath").ToString & "Grievance/GrievanceInitiatorResponseList.aspx")
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Save Successfully"), Me, Session("rootpath").ToString & "Grievance/GrievanceInitiatorResponseList.aspx")
                End If
            End If

            'Gajanan [10-July-2019] -- End  


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub fillAttachment()
        Dim dtView As DataView
        Try
            If mdtGrievanceAttachment Is Nothing Then Exit Sub

            dtView = New DataView(mdtGrievanceAttachment, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            gvGrievanceAttachment.AutoGenerateColumns = False
            gvGrievanceAttachment.DataSource = dtView
            gvGrievanceAttachment.DataBind()

            If gvGrievanceAttachment.Rows.Count > 1 Then
                btndownloadall.Visible = True
            Else
                btndownloadall.Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddFileToZip(ByVal zipFilename As String, ByVal fileToAdd As List(Of String), Optional ByVal compression As CompressionOption = CompressionOption.Normal)
        Try   'Hemant (13 Aug 2020)
        Using ms As New MemoryStream()
            Using pkg As Package = Package.Open(ms, FileMode.Create)
                For Each item As String In fileToAdd
                    Dim destFilename As String = "/" & Path.GetFileName(item)
                    Dim part As PackagePart = pkg.CreatePart(New Uri(destFilename, UriKind.Relative), "", CompressionOption.Normal)
                    Dim byt As Byte() = File.ReadAllBytes(item)

                    Using fileStream As FileStream = New FileStream(item, FileMode.Open)
                        CopyStream(CType(fileStream, Stream), part.GetStream())
                    End Using
                Next
                pkg.Close()
            End Using
            Response.Clear()
            Response.ContentType = "application/zip"
            Response.AddHeader("content-disposition", "attachment; filename=" + zipFilename + "")
            Response.OutputStream.Write(ms.GetBuffer(), 0, CInt(ms.Length))
            Response.End()

        End Using

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Private Sub CopyStream(ByVal source As Stream, ByVal target As Stream)
        Try   'Hemant (13 Aug 2020)
        Const bufSize As Integer = (5859 * 1024)
        Dim buf(bufSize - 1) As Byte
        Dim bytesRead As Integer = 0

        bytesRead = source.Read(buf, 0, bufSize)
        Do While bytesRead > 0
            target.Write(buf, 0, bytesRead)
            bytesRead = source.Read(buf, 0, bufSize)
        Loop

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub
#End Region

#Region "Page Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                'Sohail (02 Apr 2019) -- Start
                'NMB Issue - 74.1 - Error "On Load Event !! Bad Data" on clicking any page with link after session get expired.
                If Request.QueryString.Count <= 0 Then Exit Sub
                'Sohail (02 Apr 2019) -- End
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                If arr.Length = 5 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    'Blank_ModuleName()


                    'Gajanan [27-June-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                    'clsCommonATLog._WebFormName = "frmPerformanceEvaluation"
                    clsCommonATLog._WebFormName = mstrModuleName
                    'Gajanan [27-June-2019] -- End


                    'StrModuleName2 = "mnuAssessment"
                    'StrModuleName3 = "mnuPerformaceEvaluation"
                    'clsCommonATLog._WebClientIP = Convert.ToString(Session("IP_ADD"))
                    'clsCommonATLog._WebHostName = Convert.ToString(Session("HOST_NAME"))
                    Me.ViewState.Add("IsDirect", True)

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    HttpContext.Current.Session("Employeeunkid") = CInt(arr(1))
                    Me.ViewState.Add("Grievancemasterunkid", CInt(arr(2)))
                    Me.ViewState.Add("ResolutionStepTranId", CInt(arr(3)))
                    Me.ViewState.Add("Priority", CInt(arr(4)))

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Session("IsAllowFinalSave") = clsConfig._IsAllowFinalSave
                    Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp
                    'Gajanan [19-July-2019] -- Start
                    'Session("EmployeeAsOnDate") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)
                    Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
                    'Gajanan [19-July-2019] -- End
                    Session("IsBSC_ByEmployee") = clsConfig._IsBSC_ByEmployee
                    Session("IsCompanyNeedReviewer") = clsConfig._IsCompanyNeedReviewer
                    Session("Assessment_Instructions") = clsConfig._Assessment_Instructions
                    Session("ViewTitles_InPlanning") = clsConfig._ViewTitles_InPlanning
                    Session("FollowEmployeeHierarchy") = clsConfig._FollowEmployeeHierarchy
                    Session("CascadingTypeId") = clsConfig._CascadingTypeId
                    Session("ScoringOptionId") = clsConfig._ScoringOptionId
                    Session("Perf_EvaluationOrder") = clsConfig._Perf_EvaluationOrder
                    Session("ViewTitles_InEvaluation") = clsConfig._ViewTitles_InEvaluation
                    Session("ConsiderItemWeightAsNumber") = clsConfig._ConsiderItemWeightAsNumber
                    Session("Self_Assign_Competencies") = clsConfig._Self_Assign_Competencies
                    Session("DateFormat") = clsConfig._CompanyDateFormat
                    Session("DateSeparator") = clsConfig._CompanyDateSeparator
                    SetDateFormat()


                    Dim xUName, xPasswd As String : xUName = "" : xPasswd = ""

                    Dim objEmp As New clsEmployee_Master
                    objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(Session("Employeeunkid"))
                    xUName = objEmp._Displayname : xPasswd = objEmp._Password
                    HttpContext.Current.Session("E_Theme_id") = objEmp._Theme_Id
                    HttpContext.Current.Session("E_Lastview_id") = objEmp._LastView_Id
                    HttpContext.Current.Session("Theme_id") = objEmp._Theme_Id
                    HttpContext.Current.Session("Lastview_id") = objEmp._LastView_Id
                    objEmp = Nothing

                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()

                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
                    HttpContext.Current.Session("DisplayName") = xUName

                    HttpContext.Current.Session("UserName") = xUName
                    HttpContext.Current.Session("MemberName") = xUName
                    'Gajanan [27-June-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                    HttpContext.Current.Session("LangId") = 1

                    'Gajanan [27-June-2019] -- End
                    'Dim objUser As New clsUserAddEdit
                    'objUser._Userunkid = -1
                    'Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    'Call GetDatabaseVersion()
                    'Dim clsuser As New User(objUser._Username, objUser._Password, Convert.ToString(Session("mdbname")))
                    'HttpContext.Current.Session("clsuser") = clsuser
                    'HttpContext.Current.Session("UserName") = clsuser.UserName
                    'HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    'HttpContext.Current.Session("Surname") = clsuser.Surname
                    'HttpContext.Current.Session("MemberName") = clsuser.MemberName
                    'HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    'HttpContext.Current.Session("UserId") = clsuser.UserID
                    ''HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    'HttpContext.Current.Session("Password") = clsuser.password
                    'HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    'HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    mintResolutionStepTranunkid = CInt(ViewState("ResolutionStepTranId"))
                    mintGrievancemasterunkid = CInt(ViewState("Grievancemasterunkid"))
                    mintInitiatortranunkid = ViewState("initiatortranunkid")
                    mintpriority = ViewState("Priority")

                    objresolution_step_tran._ResolutionStepTranunkid = mintResolutionStepTranunkid
                    objGrievance_master._Grievancemasterunkid = mintGrievancemasterunkid
                    If mintInitiatortranunkid > 0 Then
                        objclsgreinitiater_response_tran._Initiatortranunkid = mintInitiatortranunkid
                    End If

                    If objclsgreinitiater_response_tran.isExist(mintResolutionStepTranunkid, mintGrievancemasterunkid) Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, you have already given response for selected resolution posted by approver,Please Login into the system order to access."), Me.Page, Session("rootpath").ToString() & "Index.aspx")
                        Exit Sub
                    End If

                    'Gajanan [27-June-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                    Call SetControlCaptions()
                    SetMessages()
                    Call Language._Object.SaveValue()
                    Call SetLanguage()
                    'Gajanan [27-June-2019] -- End  

                    FillCombo()
                    Call GetValue()

                    HttpContext.Current.Session("Login") = True
                    GoTo Link
                End If
            End If

            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                'Sohail (22 Nov 2018) -- Start
                'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
                If IsPostBack = False Then
                    Call SetControlCaptions()
                    SetMessages()
                    Call Language._Object.SaveValue()
                    Call SetLanguage()

                End If
                'Sohail (22 Nov 2018) -- End

                If Not Session("ResolutionStepTranid") Is Nothing And Not Session("grievancemasteid") Is Nothing Then
                    ViewState("ResolutionStepTranid") = CInt(Session("ResolutionStepTranid"))
                    ViewState("grievancemasteid") = CInt(Session("grievancemasteid"))
                    ViewState("initiatortranunkid") = CInt(Session("initiatortranunkid"))
                    ViewState("priority") = CInt(Session("priority"))

                    Session.Remove("ResolutionStepTranid")
                    Session.Remove("grievancemasteid")
                    Session.Remove("initiatortranunkid")
                    Session.Remove("priority")
                End If

                mintResolutionStepTranunkid = CInt(ViewState("ResolutionStepTranid"))
                mintGrievancemasterunkid = CInt(ViewState("grievancemasteid"))
                mintInitiatortranunkid = ViewState("initiatortranunkid")
                mintpriority = ViewState("priority")


                objresolution_step_tran._ResolutionStepTranunkid = mintResolutionStepTranunkid
                objGrievance_master._Grievancemasterunkid = mintGrievancemasterunkid

                If mintInitiatortranunkid > 0 Then
                    objclsgreinitiater_response_tran._Initiatortranunkid = mintInitiatortranunkid
                End If




                FillCombo()
                Call GetValue()
            Else
                mintResolutionStepTranunkid = ViewState("ResolutionStepTranid")
                mintGrievancemasterunkid = ViewState("grievancemasteid")
                mintInitiatortranunkid = ViewState("initiatortranunkid")
                mintpriority = ViewState("priority")
                blnissubmited = ViewState("blnissubmited")
                mdtGrievanceAttachment = CType(ViewState("mdtGrievanceAttachment"), DataTable)

                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                mblnPreviousResponseBtn = CBool(Me.ViewState("mblnPreviousResponse"))
                If IsNothing(ViewState("dtPriviousApproverResolution")) = False Then
                    dtPriviousApproverResolution = CType(ViewState("dtPriviousApproverResolution"), DataTable)
                End If
                'Gajanan [27-June-2019] -- End





            End If
Link:

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("ResolutionStepTranid") = mintResolutionStepTranunkid
            ViewState("grievancemasteid") = mintGrievancemasterunkid
            ViewState("initiatortranunkid") = mintInitiatortranunkid
            ViewState("blnissubmited") = blnissubmited
            ViewState("priority") = mintpriority
            Me.ViewState("mdtGrievanceAttachment") = mdtGrievanceAttachment


            'Gajanan [27-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
            Me.ViewState("mblnPreviousResponseBtn") = mblnPreviousResponseBtn
            ViewState("dtPriviousApproverResolution") = dtPriviousApproverResolution
            'Gajanan [27-June-2019] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button Event"
    Protected Sub btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "Grievance/GrievanceInitiatorResponseList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnsubmit.Click
        Try
            If IsValidate() = False Then Exit Sub

            If CType(sender, Button).ID.ToUpper() = btnsubmit.ID.ToUpper() Then
                blnissubmited = True


                'Gajanan [24-June-2019] -- Start      
                cnfSubmit.Title = Language.getMessage(mstrModuleName, 9, "Confirmation")

                'cnfSubmit.Message = "You are about to submit the selected resolution response , " & _
                '                    "due to this you will not be able to edit or delete the resolution response. Are you sure you want to continue ?"

                cnfSubmit.Message = Language.getMessage(mstrModuleName, 10, "You are about to submit the selected resolution response ,due to this you will not be able to edit or delete the resolution response. Are you sure you want to continue ?")
                'Gajanan [24-June-2019] -- End
                cnfSubmit.Show()
            Else
                blnissubmited = False
                Call SaveResolutionData(False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub cnfSubmit_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfSubmit.buttonYes_Click
        Try
            If blnissubmited Then
                Call SaveResolutionData(True)
            Else
                Call SaveResolutionData(False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub lnkdownloadAttachment_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            Dim xrow() As DataRow
            If CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) > 0 Then
                xrow = mdtGrievanceAttachment.Select("scanattachtranunkid = " & CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) & "")
            Else
                xrow = mdtGrievanceAttachment.Select("GUID = '" & CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("GUID").ToString) & "'")
            End If

            Dim xPath As String = ""

            If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                xPath = xrow(0).Item("filepath").ToString
                xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                If Strings.Left(xPath, 1) <> "/" Then
                    xPath = "~/" & xPath
                Else
                    xPath = "~" & xPath
                End If
                xPath = Server.MapPath(xPath)

            ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                xPath = xrow(0).Item("localpath").ToString
            End If
            If xPath.Trim <> "" Then
                Dim fileInfo As New IO.FileInfo(xPath)
                If fileInfo.Exists = False Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage("File does not Exist...", Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
                fileInfo = Nothing
                Dim strFile As String = xPath

                Response.ContentType = "application/zip"
                Response.AddHeader("Content-Disposition", "attachment;filename=" + row.Cells(GetColumnIndex.getColumnID_Griview(gvGrievanceAttachment, "colhName", False, True)).Text)
                Response.Clear()
                Response.TransmitFile(strFile)
                HttpContext.Current.ApplicationInstance.CompleteRequest()
                Response.End()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btndownloadall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndownloadall.Click
        Try
            Dim filelist As New List(Of String)
            For Each row As GridViewRow In gvGrievanceAttachment.Rows
                Dim xPath As String = ""
                Dim xrow() As DataRow
                objGrievance_master._Grievancemasterunkid = mintGrievancemasterunkid


                If CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) > 0 Then
                    xrow = mdtGrievanceAttachment.Select("scanattachtranunkid = " & CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) & "")
                Else
                    xrow = mdtGrievanceAttachment.Select("GUID = '" & CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("GUID").ToString) & "'")
                End If

                If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                    xPath = xrow(0).Item("filepath").ToString
                    xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                    If Strings.Left(xPath, 1) <> "/" Then
                        xPath = "~/" & xPath
                    Else
                        xPath = "~" & xPath
                    End If
                    xPath = Server.MapPath(xPath)

                ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                    xPath = xrow(0).Item("localpath").ToString
                End If

                If xPath.Trim <> "" Then
                    Dim fileInfo As New IO.FileInfo(xPath)
                    If fileInfo.Exists = False Then
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage("File does not Exist...", Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                    fileInfo = Nothing
                End If
                filelist.Add(xPath)
            Next
            AddFileToZip("Grievance-" & objGrievance_master._Fromemployeeunkid & "-" & objGrievance_master._Grievancerefno.ToString & ".zip", filelist)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Gajanan [27-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
    Protected Sub btnViewPreviousResolution_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewPreviousResolution.Click
        Try
            popupGrePreviousResponse.Show()
            txtPopupReferanceNo.Text = txtgrerefno.Text
            GvPreviousResponse.DataSource = dtPriviousApproverResolution
            GvPreviousResponse.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Gajanan [27-June-2019] -- End
#End Region

    'Gajanan [4-July-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes
    Protected Sub GvPreviousResponse_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvPreviousResponse.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvPreviousResponse, "colhcommittee", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvPreviousResponse, "colhcommittee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvPreviousResponse, "colhcommittee", False, True)).Text.Replace(",", "<br/>")
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
    'Gajanan [4-July-2019] -- End
    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)

            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Language._Object.setCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Language._Object.setCaption(Me.lblGreType.ID, Me.lblGreType.Text)
            Language._Object.setCaption(Me.lblAgainstEmp.ID, Me.lblAgainstEmp.Text)
            Language._Object.setCaption(Me.lblgrerefno.ID, Me.lblgrerefno.Text)
            Language._Object.setCaption(Me.lblgredate.ID, Me.lblgredate.Text)
            Language._Object.setCaption(Me.TabGre.ID, Me.TabGre.HeaderText)


            Language._Object.setCaption(Me.btndownloadall.ID, Me.btndownloadall.Text)
            Language._Object.setCaption(Me.btnSave.ID, Me.btnSave.Text)
            Language._Object.setCaption(Me.btnsubmit.ID, Me.btnsubmit.Text)
            Language._Object.setCaption(Me.btnclose.ID, Me.btnclose.Text)

            Language._Object.setCaption(Me.lblresolutiontype.ID, Me.lblresolutiontype.Text)
            Language._Object.setCaption(Me.lblresolutionremark.ID, Me.lblresolutionremark.Text)
            Language._Object.setCaption(Me.lblQualificationRemark.ID, Me.lblQualificationRemark.Text)
            Language._Object.setCaption(Me.lblresponsetype.ID, Me.lblresponsetype.Text)
            Language._Object.setCaption(Me.lblresponseremark.ID, Me.lblresponseremark.Text)

            Language._Object.setCaption(Me.TabGre.ID, Me.TabGre.HeaderText)
            Language._Object.setCaption(Me.TabRelifWanted.ID, Me.TabRelifWanted.HeaderText)
            Language._Object.setCaption(Me.TabAttachDocument.ID, Me.TabAttachDocument.HeaderText)
            Language._Object.setCaption(Me.TabApplealRemark.ID, Me.TabApplealRemark.HeaderText)

            Language._Object.setCaption(gvGrievanceAttachment.Columns(1).FooterText, gvGrievanceAttachment.Columns(1).HeaderText)
            Language._Object.setCaption(gvGrievanceAttachment.Columns(2).FooterText, gvGrievanceAttachment.Columns(2).HeaderText)

            Language._Object.setCaption(Me.lblPopupReferanceNo.ID, Me.lblPopupReferanceNo.Text)
            Language._Object.setCaption(GvPreviousResponse.Columns(0).FooterText, GvPreviousResponse.Columns(0).HeaderText)
            Language._Object.setCaption(GvPreviousResponse.Columns(1).FooterText, GvPreviousResponse.Columns(1).HeaderText)
            Language._Object.setCaption(GvPreviousResponse.Columns(2).FooterText, GvPreviousResponse.Columns(2).HeaderText)
            Language._Object.setCaption(GvPreviousResponse.Columns(3).FooterText, GvPreviousResponse.Columns(3).HeaderText)
            Language._Object.setCaption(GvPreviousResponse.Columns(4).FooterText, GvPreviousResponse.Columns(4).HeaderText)
            Language._Object.setCaption(GvPreviousResponse.Columns(5).FooterText, GvPreviousResponse.Columns(5).HeaderText)
            Language._Object.setCaption(GvPreviousResponse.Columns(6).FooterText, GvPreviousResponse.Columns(6).HeaderText)
            Language._Object.setCaption(GvPreviousResponse.Columns(7).FooterText, GvPreviousResponse.Columns(7).HeaderText)

            Language._Object.setCaption(Me.btnCloseGrePreviousResponse.ID, Me.btnCloseGrePreviousResponse.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.btndownloadall.Text = Language._Object.getCaption(Me.btndownloadall.ID, Me.btndownloadall.Text).Replace("&", "")
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnsubmit.Text = Language._Object.getCaption(Me.btnsubmit.ID, Me.btnsubmit.Text).Replace("&", "")
            Me.btnclose.Text = Language._Object.getCaption(Me.btnclose.ID, Me.btnclose.Text).Replace("&", "")

            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Me.lblGreType.Text = Language._Object.getCaption(Me.lblGreType.ID, Me.lblGreType.Text)
            Me.lblAgainstEmp.Text = Language._Object.getCaption(Me.lblAgainstEmp.ID, Me.lblAgainstEmp.Text)
            Me.lblgrerefno.Text = Language._Object.getCaption(Me.lblgrerefno.ID, Me.lblgrerefno.Text)
            Me.lblgredate.Text = Language._Object.getCaption(Me.lblgredate.ID, Me.lblgredate.Text)
            Me.lblresolutiontype.Text = Language._Object.getCaption(Me.lblresolutiontype.ID, Me.lblresolutiontype.Text)
            Me.lblresolutionremark.Text = Language._Object.getCaption(Me.lblresolutionremark.ID, Me.lblresolutionremark.Text)
            Me.lblresponsetype.Text = Language._Object.getCaption(Me.lblresponsetype.ID, Me.lblresponsetype.Text)
            Me.lblresponseremark.Text = Language._Object.getCaption(Me.lblresponseremark.ID, Me.lblresponseremark.Text)

            Me.TabGre.HeaderText = Language._Object.getCaption(Me.TabGre.ID, Me.TabGre.HeaderText)
            Me.TabRelifWanted.HeaderText = Language._Object.getCaption(Me.TabRelifWanted.ID, Me.TabRelifWanted.HeaderText)
            Me.TabAttachDocument.HeaderText = Language._Object.getCaption(Me.TabAttachDocument.ID, Me.TabAttachDocument.HeaderText)
            Me.TabApplealRemark.HeaderText = Language._Object.getCaption(Me.TabApplealRemark.ID, Me.TabApplealRemark.HeaderText)

            Me.lblQualificationRemark.Text = Language._Object.getCaption(Me.lblQualificationRemark.ID, Me.lblQualificationRemark.Text)


            gvGrievanceAttachment.Columns(1).HeaderText = Language._Object.getCaption(gvGrievanceAttachment.Columns(1).FooterText, gvGrievanceAttachment.Columns(1).HeaderText)
            gvGrievanceAttachment.Columns(2).HeaderText = Language._Object.getCaption(gvGrievanceAttachment.Columns(2).FooterText, gvGrievanceAttachment.Columns(2).HeaderText)


            Me.lblPopupReferanceNo.Text = Language._Object.getCaption(Me.lblPopupReferanceNo.ID, Me.lblPopupReferanceNo.Text)

            GvPreviousResponse.Columns(0).HeaderText = Language._Object.getCaption(GvPreviousResponse.Columns(0).FooterText, GvPreviousResponse.Columns(0).HeaderText)
            GvPreviousResponse.Columns(1).HeaderText = Language._Object.getCaption(GvPreviousResponse.Columns(1).FooterText, GvPreviousResponse.Columns(1).HeaderText)
            GvPreviousResponse.Columns(2).HeaderText = Language._Object.getCaption(GvPreviousResponse.Columns(2).FooterText, GvPreviousResponse.Columns(2).HeaderText)
            GvPreviousResponse.Columns(3).HeaderText = Language._Object.getCaption(GvPreviousResponse.Columns(3).FooterText, GvPreviousResponse.Columns(3).HeaderText)
            GvPreviousResponse.Columns(4).HeaderText = Language._Object.getCaption(GvPreviousResponse.Columns(4).FooterText, GvPreviousResponse.Columns(4).HeaderText)
            GvPreviousResponse.Columns(5).HeaderText = Language._Object.getCaption(GvPreviousResponse.Columns(5).FooterText, GvPreviousResponse.Columns(5).HeaderText)
            GvPreviousResponse.Columns(6).HeaderText = Language._Object.getCaption(GvPreviousResponse.Columns(6).FooterText, GvPreviousResponse.Columns(6).HeaderText)
            GvPreviousResponse.Columns(7).HeaderText = Language._Object.getCaption(GvPreviousResponse.Columns(7).FooterText, GvPreviousResponse.Columns(7).HeaderText)
            Me.btnCloseGrePreviousResponse.Text = Language._Object.getCaption(Me.btnCloseGrePreviousResponse.ID, Me.btnCloseGrePreviousResponse.Text).Replace("&", "")


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (22 Nov 2018) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, No Approver defined in the system or email address not assigned. Please define approver in order to post grievance.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Approver is inactive. Please contact system admin in order to post grievance.")
            Language.setMessage(mstrModuleName, 3, "Sorry, you have already given response for selected resolution posted by approver,Please Login into the system order to access.")
            Language.setMessage(mstrModuleName, 6, "Save Successfully")
            Language.setMessage(mstrModuleName, 7, "Responsetype is compulsory information. Please select Responsetype to continue.")
            Language.setMessage(mstrModuleName, 8, "Responseremark is compulsory information. Please select Responseremark to continue.")
            Language.setMessage(mstrModuleName, 9, "Confirmation")
            Language.setMessage(mstrModuleName, 10, "You are about to submit the selected resolution response ,due to this you will not be able to edit or delete the resolution response. Are you sure you want to continue ?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
