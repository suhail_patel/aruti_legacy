﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="GrievanceApprover.aspx.vb"
    Inherits="Grievance_GrievanceApprover" Title="Grievance Approver Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Grievance Approver"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Grievance Approver List"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="width: 10%">
                                                <asp:Label ID="lblLevel" runat="server" Text="Approver Level" Width="100%" />
                                            </td>
                                            <td style="width: 23%">
                                                <asp:DropDownList ID="drpLevel" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblApprover" runat="server" Text="Approver" Width="100%" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="drpApprover" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status" />
                                            </td>
                                            <td style="width: 25%">
                                                <asp:DropDownList ID="drpStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="scrollable-container" style="width: 100%; height: 350px; overflow: auto">
                                <asp:GridView ID="gvApproverList" DataKeyNames="approvermasterunkid" runat="server"
                                    AutoGenerateColumns="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                    RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkedit" runat="server" CssClass="gridedit" ToolTip="Edit" OnClick="lnkedit_Click"
                                                    CommandArgument='<%#Eval("approvermasterunkid") %>'>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete" OnClick="lnkdelete_Click"
                                                    CommandArgument='<%#Eval("approvermasterunkid") %>' ToolTip="Delete">
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkactive" runat="server" ToolTip="Active" OnClick="lnkActive_Click"
                                                    CommandArgument='<%#Eval("approvermasterunkid") %>'>
                                                    <i class="fa fa-user-plus" style="font-size:18px;color:Green"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lnkDeactive" runat="server" ToolTip="DeActive" OnClick="lnkDeActive_Click"
                                                    CommandArgument='<%#Eval("approvermasterunkid") %>'>
                                                    <i class="fa fa-user-times" style="font-size:18px;color:red" ></i>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--'S.SANDEEP |30-MAY-2019| -- START--%>
                                        <%--'ISSUE/ENHANCEMENT : [Grievance UAT]--%>
                                        <%--<asp:BoundField HeaderText="Approver Name" DataField="Name" />--%>
                                        <%--<asp:BoundField HeaderText="Department" DataField="departmentname" />--%>
                                        <%--<asp:BoundField HeaderText="Job" DataField="jobname" />--%>
                                        <%--<asp:BoundField HeaderText="External Approver" DataField="exappr" />--%>
                                        <asp:BoundField HeaderText="Approver Name" DataField="Name" FooterText="dgcolhName" />
                                        <asp:BoundField HeaderText="Department" DataField="departmentname" FooterText="dgcolhdepartmentname" />
                                        <asp:BoundField HeaderText="Job" DataField="jobname" FooterText="dgcolhjobname" />
                                        <asp:BoundField HeaderText="External Approver" DataField="exappr" FooterText="dgcolhexappr" />
                                        <%--'S.SANDEEP |30-MAY-2019| -- END--%>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupGreAddEditApproverEmpMapping" BackgroundCssClass="modalBackground"
                        TargetControlID="lblCaption" runat="server" PopupControlID="pnlGreAddEditApproverEmpMapping"
                        CancelControlID="lblPageHeader1" />
                    <asp:Panel ID="pnlGreAddEditApproverEmpMapping" runat="server" CssClass="newpopup"
                        Style="display: none; width: 1000px;">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblPageHeader1" runat="server" Text="Add/ Edit Grievance Approver"></asp:Label>
                            </div>
                            <div class="panel-body" style="max-height: 480px; overflow: auto">
                                <div id="Div7" class="panel-default" style="display: inline-block; width: 48%;">
                                    <div id="Div8" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblApproverInfo" runat="server" Text="Approvers Info" Font-Bold="true" />
                                        </div>
                                    </div>
                                    <div id="Div9" class="panel-body-default">
                                        <div class="row2">
                                            <div style="width: 100%; height: 350px">
                                                <div class="row2">
                                                    <div class="ib" style="width: 15%">
                                                    </div>
                                                    <div class="ib" style="width: 78%">
                                                        <asp:CheckBox ID="chkAddEditExtApprover" runat="server" AutoPostBack="true" Text="Make External Approver" />
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ib" style="width: 15%">
                                                        <asp:Label ID="lblApproverName" runat="server" Text="Name" />
                                                    </div>
                                                    <div class="ib" style="width: 78%">
                                                        <asp:DropDownList ID="drpAddEditApproverName" runat="server" Width="350px" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="row2">
                                                    <div class="ib" style="width: 15%">
                                                        <asp:Label ID="lblApprovalevel" runat="server" Text="Level" />
                                                    </div>
                                                    <div class="ib" style="width: 78%">
                                                        <asp:DropDownList ID="drpAddEditApprovalLevel" runat="server" Width="350px">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <asp:Panel ID="PanelApprovalUser" runat="server">
                                                    <div class="row2">
                                                        <div class="ib" style="width: 15%">
                                                            <asp:Label ID="lblApprovalUser" runat="server" Text="User" />
                                                        </div>
                                                        <div class="ib" style="width: 78%">
                                                            <asp:DropDownList ID="drpAddEditUser" runat="server" Width="350px">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                <%--'Hemant (29 May 2019) -- Start--%>
                                                <div class="row2">
                                                    <div class="ib" style="width: 75%; margin-bottom: 5px"> 
                                                        <asp:TextBox ID="txtSearchEmployee" runat="server" AutoPostBack="true"></asp:TextBox>
                                                    </div>                                                    
                                                    <div class="ib" style="width: 15%; text-align: right; font-weight: bold"> 
                                                        <asp:LinkButton ID="lnkReset" runat="server" Text="Reset" CssClass="lnkhover"></asp:LinkButton>
                                                    </div>
                                                </div>
                                                <%--'Hemant (29 May 2019) -- End--%>                                               
                                                <div id="Div10" style="width: 100%; height: 175px; overflow: auto">
                                                    <asp:GridView ID="gvAddEditEmployee" runat="server" AutoGenerateColumns="false" Width="99%"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" DataKeyNames="employeeunkid,employeecode"
                                                        RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="ChkAllAddEditEmployee" runat="server" AutoPostBack="true" OnCheckedChanged="ChkAllAddEditEmployee_CheckedChanged" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="ChkSelectAddEditEmployee" runat="server" AutoPostBack="true" OnCheckedChanged="ChkSelectAddEditEmployee_CheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--'S.SANDEEP |30-MAY-2019| -- START--%>
                                                            <%--'ISSUE/ENHANCEMENT : [Grievance UAT]--%>
                                                            <%--<asp:BoundField HeaderText="Employee Code" DataField="employeecode" />--%>
                                                            <%--<asp:BoundField HeaderText="Employee" DataField="name" />--%>
                                                            <asp:BoundField HeaderText="Employee Code" DataField="employeecode" FooterText="dgEcode" />
                                                            <asp:BoundField HeaderText="Employee" DataField="name" FooterText="dgcolhEname" />
                                                            <%--'S.SANDEEP |30-MAY-2019| -- END--%>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="btn-default">
                                            <asp:Button ID="btnAddEditAdd" runat="server" Text="Add" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                                <div id="Div12" class="panel-default" style="display: inline-block; width: 47%;">
                                    <div id="Div13" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="LblGreAddEditLeaveApprover" runat="server" Text="Selected Employee"
                                                Font-Bold="true" />
                                        </div>
                                    </div>
                                    <div id="Div14" class="panel-body-default">
                                        <div class="row2">
                                            <div style="width: 100%; height: 350px; overflow: auto">
                                                <asp:GridView ID="GvApproverEmpMapping" runat="server" AutoGenerateColumns="false"
                                                    Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="employeeunkid">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="ChkAllSelectedEmp" runat="server" AutoPostBack="true" OnCheckedChanged="ChkAllSelectedEmp_CheckedChanged" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkSelectedEmp" runat="server" AutoPostBack="true" OnCheckedChanged="chkAddEditSelectEmpChkgvSelect_CheckedChanged" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--'S.SANDEEP |30-MAY-2019| -- START--%>
                                                        <%--'ISSUE/ENHANCEMENT : [Grievance UAT]--%>
                                                        <%--<asp:BoundField HeaderText="Employee" DataField="name" />--%>
                                                        <%--<asp:BoundField HeaderText="Department" DataField="departmentname" />--%>
                                                        <%--<asp:BoundField HeaderText="Job" DataField="jobname" />--%>                                                        
                                                        <asp:BoundField HeaderText="Employee" DataField="name" FooterText = "dgcolhEmpName" />
                                                        <asp:BoundField HeaderText="Department" DataField="departmentname" FooterText = "dgcolhEDept" />
                                                        <asp:BoundField HeaderText="Job" DataField="jobname" FooterText = "dgcolhEJob" />
                                                        <%--'S.SANDEEP |30-MAY-2019| -- END--%>                                                        
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="btn-default">
                                            <asp:Button ID="btnAddeditDelete" runat="server" Text="Delete" CssClass="btndefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <asp:Button ID="btnpopupsave" runat="server" Text="Save" CssClass="btndefault" />
                                <asp:Button ID="btnpopupclose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupApproverUseraccess" BackgroundCssClass="modalBackground"
                        TargetControlID="lblApproverInfo" runat="server" PopupControlID="PanelApproverUseraccess"
                        CancelControlID="lblApproverName" />
                    <asp:Panel ID="PanelApproverUseraccess" runat="server" CssClass="newpopup" Style="display: none;
                        width: 600px;">
                        <div class="panel-primary" style="margin: 0">
                            <div class="panel-heading">
                                <asp:Label ID="lblPageHeader2" runat="server" Text="Add/ Edit Grievance Approver"></asp:Label>
                            </div>
                            <div class="panel-body" style="max-height: 480px; overflow: auto">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblApproverInfo1" runat="server" Text="Approvers Info" Font-Bold="true" />
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default" style="height: 300px; margin: auto; padding: auto">
                                        <div class="row2">
                                            <div style="width: 15%" class="ib">
                                                <asp:Label ID="lblApproverUseraccess_level" runat="server" Text="Level"></asp:Label>
                                            </div>
                                            <div style="width: 80%" class="ib">
                                                <asp:DropDownList ID="drpApproverUseraccess_level" runat="server" Width="450px">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div style="width: 15%" class="ib">
                                                <asp:Label ID="lblApproverUseraccess_user" runat="server" Text="User"></asp:Label>
                                            </div>
                                            <div style="width: 80%" class="ib">
                                                <asp:DropDownList ID="drpApproverUseraccess_user" runat="server" Width="450px" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    
                                        <div id="Div11" style="width: 100%; height: 225px; overflow: auto">
                                            <%--  <asp:TreeView ID="TvApproverUseraccess" ExpandDepth="0" LeafNodeStyle-ChildNodesPadding="0"
                                                ShowLines="true" runat="server">
                                            </asp:TreeView>--%>
                                            <asp:GridView ID="TvApproverUseraccess" runat="server" ShowHeader="false" AutoGenerateColumns="False"
                                                Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:BoundField DataField="UserAccess" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <asp:Button ID="btnApproverUseraccessSave" runat="server" Text="Save" CssClass="btndefault" />
                                <asp:Button ID="btnApproverUseraccessClose" runat="server" Text="Close" CssClass="btndefault" />
                            </div>
                        </div>
                    </asp:Panel>
                    
                    
                    <uc9:ConfirmYesNo ID="confirmapproverdelete" runat="server" />
                    <ucDel:DeleteReason ID="DeleteApprovalReason" runat="server" />
                    <uc9:ConfirmYesNo ID="popupconfirmDeactiveAppr" runat="server" />
                    <uc9:ConfirmYesNo ID="popupconfirmActiveAppr" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
