﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="GreivanceApprLevel.aspx.vb"
    Inherits="Grievance_GreivanceApprLevel" Title="Grievance Approver Level" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="txtNumeric" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 80%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Grievance Approver Level"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblPageHeader2" runat="server" Text="Grievance Approver Level List"></asp:Label>
                                    </div>
                                </div>
                                <div id="Div2" class="panel-body-default">
                                    <asp:Panel ID="pnl_dgView" runat="server" Height="350px" ScrollBars="Auto">
                                        <asp:GridView ID="GvApprLevelList" DataKeyNames="apprlevelunkid,ApprovalSettingid" runat="server" AutoGenerateColumns="False"
                                            Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false"  show>
                                            <Columns>
                                                <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"> 
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkedit" runat="server" CssClass="gridedit" OnClick="lnkedit_Click" CommandArgument='<%#Eval("apprlevelunkid") %>'>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField HeaderStyle-Width="5%" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete" OnClick="lnkdelete_Click" CommandArgument='<%#Eval("apprlevelunkid") %>'> 
                                                  
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="levelcode" HeaderText="Level Code" HeaderStyle-Width="20%" FooterText="colhLevelCode"></asp:BoundField>
                                                <asp:BoundField DataField="levelname" HeaderText="Level Name" HeaderStyle-Width="25%" FooterText="colhLevelName">  </asp:BoundField>
                                                <asp:BoundField DataField="priority" HeaderText="Priority Level" HeaderStyle-Width="15%" FooterText="colhPriority" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundField>
                                                <asp:BoundField DataField="ApprovalSetting" HeaderText="Approval Setting" HeaderStyle-Width="30%" FooterText="colhApprovalSetting"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnnew" runat="server" CssClass="btndefault" Text="New" OnClick="btnnew_Click" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupGreApproverLevel" BackgroundCssClass="modalBackground"
                        TargetControlID="txtlevelcode" runat="server" PopupControlID="pnlGreApprover"
                        DropShadow="false" CancelControlID="btnHiddenLvCancel">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlGreApprover" runat="server" CssClass="newpopup" Style="display: none;
                        width: 500px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblCancelText1" Text="Grievance Approver Level Add/ Edit" runat="server" />
                            </div>
                            <div class="panel-body">
                                <div id="Div20" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lbllevelcode" runat="server" Text="Level Code" />
                                            </td>
                                            <td style="width: 60%">
                                                <asp:TextBox ID="txtlevelcode" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lbllevelname" runat="server" Text="Level Name" />
                                            </td>
                                            <td style="width: 60%">
                                                <asp:TextBox ID="txtlevelname" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lbllevelpriority" runat="server" Text="Priority" />
                                            </td>
                                            <td style="width: 30%">
                                                <asp:TextBox ID="txtlevelpriority" runat="server" Enabled="false" Style="text-align: right;
                                                    background-color: White"></asp:TextBox>
                                                <cc1:NumericUpDownExtender ID="nudYear" runat="server" Width="100" Minimum="0" TargetControlID="txtlevelpriority">
                                                </cc1:NumericUpDownExtender>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSaveGreApprover" runat="server" CssClass="btndefault" Text="Save" />
                                        <asp:Button ID="btnCloseGreApprover" runat="server" CssClass="btndefault" Text="Close" />
                                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                        <uc7:Confirmation ID="popup_YesNo"  runat="server" />
                    
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
