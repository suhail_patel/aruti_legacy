﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="AgreeDisagreeResponse.aspx.vb"
    Inherits="Grievance_AgreeDisagreeResponse" Title="Agree Disagree Response" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

  
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 80%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Grievance Agree/Disagree Response"></asp:Label>
                             <asp:LinkButton ID="btnViewPreviousResolution" runat="server" Visible="false" Style="margin-top: -2px;">
                           <i class="fa fa-comments" style="color:#fff !important;font-size: 20px;"> </i>
                            </asp:LinkButton>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Grievance Master"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <div class="row2 ib" style="width: 100%">
                                        <div style="width: 15%;" class="ib">
                                            <asp:Label ID="lblGreType" runat="server" Text="Grievnce Type" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 32%;" class="ib">
                                            <asp:DropDownList ID="drpGreType" runat="server" Width="100%" >
                                            </asp:DropDownList>
                                        </div>
                                        <div style="width: 15%;" class="ib">
                                            <asp:Label ID="lblAgainstEmp" runat="server" Text="Against Employee" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 32%;" class="ib">
                                            <asp:TextBox ID="txtAgainstEmp" runat="server" Width="100%" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 15%;" class="ib">
                                            <asp:Label ID="lblgrerefno" runat="server" Text="Ref No." Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 32%;" class="ib">
                                            <asp:TextBox ID="txtgrerefno" runat="server" Width="100%" ></asp:TextBox>
                                        </div>
                                        <div style="width: 15%;" class="ib">
                                            <asp:Label ID="lblgredate" runat="server" Text="Date" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 32%;" class="ib">
                                            <asp:TextBox ID="txtgredate" runat="server" Width="100%" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 99%; display: inline-block">
                                            <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0">
                                                <cc1:TabPanel runat="server" HeaderText="Grievance Description" ID="TabGre">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtGreDesc" Width="99%" runat="server" Rows="10" TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                                <cc1:TabPanel runat="server" HeaderText="Relief Wanted" ID="TabRelifWanted">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtRelifWanted" Width="99%" runat="server" Rows="10" TextMode="MultiLine"></asp:TextBox>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                               <cc1:TabPanel ID="TabAttachDocument" runat="server" HeaderText="Attach Document">
                                                <ContentTemplate>
                                                    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gvGrievanceAttachment" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                HeaderStyle-Font-Bold="false" DataKeyNames="GUID,scanattachtranunkid,localpath">
                                                                <Columns>
                                                                    <asp:TemplateField FooterText="objcohdownload" HeaderStyle-Width="25px" ItemStyle-Width="25px">
                                                                        <ItemTemplate>
                                                                            <span class="gridiconbc">
                                                                                <asp:LinkButton ID="DownloadLink" runat="server" ToolTip="Delete"
                                                                                    CommandArgument='<%#Eval("scanattachtranunkid") %>' OnClick="lnkdownloadAttachment_Click">
                                                                                   <i class="fa fa-download"></i>
                                                                                </asp:LinkButton>
                                                                            </span> 
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                    <asp:BoundField HeaderText="File Size" DataField="filesize" FooterText="colhSize"
                                                                        ItemStyle-HorizontalAlign="Right" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <div style="width: 100%;">
                                                                <asp:Button ID="btndownloadall" runat="server" Text="Download All" CssClass="btndefault" />
                                                            </div>
                                                     </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="gvGrievanceAttachment" />
                                                            <asp:PostBackTrigger ControlID="btndownloadall" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                                <cc1:TabPanel runat="server" HeaderText="Appeal Remark" ID="TabApplealRemark" Visible="false">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtpopupApplealRemark" Width="99%" runat="server" Rows="3" TextMode="MultiLine">
                                                        </asp:TextBox>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                            </cc1:TabContainer>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 15%;" class="ib">
                                            <asp:Label ID="lblresolutiontype" runat="server" Text="Resolution Type" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 82%;" class="ib">
                                            <asp:DropDownList ID="drpresolutiontype" runat="server" Width="100%" >
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 15%;" class="ib">
                                            <asp:Label ID="lblresolutionremark" runat="server" Text="Resolution Remark" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 82%;" class="ib">
                                            <asp:TextBox ID="txtresolutionremark" TextMode="MultiLine" Rows="5" runat="server"
                                                Width="100%" ></asp:TextBox>
                                        </div>
                                    </div>
                                    
                                     <div class="row2">
                                        <div style="width: 15%;" class="ib">
                                            <asp:Label ID="lblQualificationRemark" runat="server" Text="Qualification Remark" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 82%;" class="ib">
                                            <asp:TextBox ID="txtQualificationRemark" TextMode="MultiLine" Rows="5" runat="server"
                                                Width="100%" ></asp:TextBox>
                                        </div>
                                    </div>
                                    
                                    <div class="row2">
                                        <div style="width: 15%;" class="ib">
                                            <asp:Label ID="lblresponsetype" runat="server" Text="Response Type" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 82%;" class="ib">
                                            <asp:DropDownList ID="drpresponsetype" runat="server" Width="100%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 15%;" class="ib">
                                            <asp:Label ID="lblresponseremark" runat="server" Text="Response Remark" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 82%;" class="ib">
                                            <asp:TextBox ID="txtresponseremark" TextMode="MultiLine" Rows="5" runat="server"
                                                Width="100%"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnsubmit" runat="server" Text=" Save & Submit" CssClass="btndefault" />
                                        <asp:Button ID="btnclose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupGrePreviousResponse" BackgroundCssClass="modalBackground"
                        TargetControlID="lblPopupheader" runat="server" PopupControlID="pnlGrePreviousResponse"
                        DropShadow="false" CancelControlID="btnHiddenLvCancel">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlGrePreviousResponse" runat="server" CssClass="newpopup" Style="display: none;
                        width: 920px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblPopupheader" Text="Previous Lower Level Approver Resolution" runat="server" />
                            </div>
                            <div class="panel-body">
                                <div id="Div20" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 25%">
                                            <asp:Label ID="lblPopupReferanceNo" runat="server" Text="Referance Number"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 70%">
                                            <asp:Label ID="txtPopupReferanceNo" runat="server" Text="0" Font-Bold="true"></asp:Label>
                                        </div>
                                    </div>
                                    <asp:Panel ID="pnl_dgView" runat="server" Height="400px" ScrollBars="Auto">
                                        <asp:GridView ID="GvPreviousResponse" runat="server" AutoGenerateColumns="False"
                                            Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                            <Columns>
                                                <asp:BoundField DataField="levelname" HeaderText="Approver Level" 
                                                    FooterText="colhlevelname"></asp:BoundField>
                                                <asp:BoundField DataField="name" HeaderText="Approver Name" 
                                                    FooterText="colhApproverName"></asp:BoundField>
                                                <asp:BoundField DataField="priority" HeaderText="Priority" 
                                                    FooterText="colhPriority"></asp:BoundField>
                                                <asp:BoundField DataField="qualifyremark" HeaderText="Qualification Remark" ItemStyle-Width="20%"
                                                    FooterText="colhqualifyremark"></asp:BoundField>
                                                <asp:BoundField DataField="responseremark" HeaderText="Response Remark" ItemStyle-Width="20%"
                                                    FooterText="colhresponseremark"></asp:BoundField>
                                                <asp:BoundField DataField="EmpResponseRemark" HeaderText="Employee Response"  ItemStyle-Width="20%"
                                                    FooterText="colhempresponseremark"></asp:BoundField>
                                                <asp:BoundField DataField="EmployeeStatus" HeaderText="Employee Status" 
                                                    FooterText="colhempstatus"></asp:BoundField>
                                                <asp:BoundField DataField="EmployeeIds" HeaderText="Committee Members" ItemStyle-Width="25%"
                                                    FooterText="colhcommittee"></asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnCloseGrePreviousResponse" runat="server" CssClass="btndefault"
                                            Text="Close" />
                                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                        <uc2:Cnf_YesNo ID="cnfSubmit" runat="server" Title="Aruti" />
                    
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
