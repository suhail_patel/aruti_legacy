﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Net.Dns
Imports System.Drawing
Imports System.Web.Services
Imports System.Globalization
Imports System.IO.Compression
Imports System.IO.Packaging

#End Region

Partial Class Grievance_wPg_ResolutionStep
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmResolutionStep"
    Private DisplayMessage As New CommonCodes
    Private objgrievanceapprover_master As New clsgrievanceapprover_master
    Private objresolution_step_tran As New clsResolution_Step_Tran
    Private objGrievance_master As New clsGrievance_Master
    Private mintResolutionStepTranunkid As Integer = 0
    Private mblnIsFinal As String = ""
    Private mblnIsProcessed As String = ""
    Private dtGrievanceList As New DataTable
    Private mintGrievancemasterunkid As Integer = -1
    Private dtEmployee As DataTable
    Private objCONN As SqlConnection
    Private blnissubmited As Boolean = False
    Private blnisdeleted As Boolean = False
    Private mstrDeletereason As String = ""
    Private objDocument As New clsScan_Attach_Documents
    Private mdtGrievanceAttachment As DataTable = Nothing
    Private mintPriority As Integer = 0
    Private mintApprovalSetting As Integer = 0
    Private mDicVisibleGrievances As Dictionary(Of Integer, Integer)

    'Gajanan [10-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Changes        
    Private dtPriviousApproverResolution As DataTable = Nothing
    Private mblnPreviousResponseBtn As Boolean = False
    Private mblnPreviousResponsePopup As Boolean = False
    'Gajanan [10-June-2019] -- End



    'Gajanan [27-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
    Private mdtGrievanceDate As Date = Nothing

    'Gajanan [27-June-2019] -- End




    Private ms As MemoryStream
#End Region

#Region " Page's Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                'Sohail (02 Apr 2019) -- Start
                'NMB Issue - 74.1 - Error "On Load Event !! Bad Data" on clicking any page with link after session get expired.
                If Request.QueryString.Count <= 0 Then Exit Sub
                'Sohail (02 Apr 2019) -- End
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                If arr.Length = 4 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    'Blank_ModuleName()

                    'clsCommonATLog._WebFormName = "frmPerformanceEvaluation"
                    'StrModuleName2 = "mnuAssessment"
                    'StrModuleName3 = "mnuPerformaceEvaluation"
                    'clsCommonATLog._WebClientIP = Convert.ToString(Session("IP_ADD"))
                    'clsCommonATLog._WebHostName = Convert.ToString(Session("HOST_NAME"))
                    Me.ViewState.Add("IsDirect", True)

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    HttpContext.Current.Session("UserId") = CInt(arr(1))
                    Me.ViewState.Add("Grievancemasterunkid", CInt(arr(2)))
                    Me.ViewState.Add("approvermasterunkid", CInt(arr(3)))

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Session("IsAllowFinalSave") = clsConfig._IsAllowFinalSave
                    Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp
                    'Gajanan [19-July-2019] -- Start
                    'Session("EmployeeAsOnDate") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)
                    Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
                    'Gajanan [19-July-2019] -- End
                    Session("IsBSC_ByEmployee") = clsConfig._IsBSC_ByEmployee
                    Session("IsCompanyNeedReviewer") = clsConfig._IsCompanyNeedReviewer
                    Session("Assessment_Instructions") = clsConfig._Assessment_Instructions
                    Session("ViewTitles_InPlanning") = clsConfig._ViewTitles_InPlanning
                    Session("FollowEmployeeHierarchy") = clsConfig._FollowEmployeeHierarchy
                    Session("CascadingTypeId") = clsConfig._CascadingTypeId
                    Session("ScoringOptionId") = clsConfig._ScoringOptionId
                    Session("Perf_EvaluationOrder") = clsConfig._Perf_EvaluationOrder
                    Session("ViewTitles_InEvaluation") = clsConfig._ViewTitles_InEvaluation
                    Session("ConsiderItemWeightAsNumber") = clsConfig._ConsiderItemWeightAsNumber
                    Session("Self_Assign_Competencies") = clsConfig._Self_Assign_Competencies
                    Session("DateFormat") = clsConfig._CompanyDateFormat
                    Session("DateSeparator") = clsConfig._CompanyDateSeparator
                    SetDateFormat()

                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, Convert.ToString(Session("mdbname")))
                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName
                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Convert.ToString(Session("rootpath")) & "Index.aspx")
                        Exit Sub
                    End If

                    Call FillCombo()

                    mintGrievancemasterunkid = CInt(Me.ViewState("Grievancemasterunkid"))
                    objGrievance_master._Grievancemasterunkid = mintGrievancemasterunkid

                    'Gajanan [27-June-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                    mdtGrievanceDate = objGrievance_master._Grievancedate
                    'Gajanan [27-June-2019] -- End


                    drpApprover.SelectedValue = Convert.ToString(ViewState("approvermasterunkid"))


                    'mDicVisibleGrievances = CType(Me.ViewState("mDicVisibleGrievances"), Global.System.Collections.Generic.Dictionary(Of Integer, Integer))

                    'Call Fill_Employee_List()

                    CommiteeResponseControlsVisible(False)

                    'Call SetGrievanceInfo()


                    EnableDisableResolutionControl(False)

                    Call SetControlCaptions()
                    Call SetMessages()
                    Call Language._Object.SaveValue()
                    Call SetLanguage()

                    GetValue()

                    HttpContext.Current.Session("Login") = True
                    GoTo Link
                End If
            End If



            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                'Sohail (22 Nov 2018) -- Start
                'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
                Call SetControlCaptions()
                'Gajanan [24-June-2019] -- Start      
                Call SetMessages()
                'Gajanan [24-June-2019] -- End
                Call Language._Object.SaveValue()
                'Sohail (22 Nov 2018) -- End

                Call SetLanguage()

                'Gajanan [24-June-2019] -- Start      
                Call SetMessages()
                'Gajanan [24-June-2019] -- End


                'If Session("ResolutionStepTran") IsNot Nothing Then
                '    mintResolutionStepTranunkid = CInt(Session("ResolutionStepTran"))
                '    Session("LoanAdvanceTranunkid") = Nothing
                'End If
                'If Session("IsFinal") IsNot Nothing Then
                '    mblnIsFinal = CStr(Session("IsFinal"))
                '    Session("IsFromLoan") = Nothing
                'End If
                'If Session("IsProcessed") IsNot Nothing Then
                '    mblnIsProcessed = CStr(Session("IsProcessed"))
                '    Session("StatusId") = Nothing
                'End If


                If Not Session("ResolutionStepTranid") Is Nothing And Not Session("grievancemasteid") Is Nothing Then
                    ViewState("ResolutionStepTranid") = CInt(Session("ResolutionStepTranid"))
                    ViewState("grievancemasteid") = CInt(Session("grievancemasteid"))

                    Session.Remove("ResolutionStepTranid")
                    Session.Remove("grievancemasteid")

                End If

                mintResolutionStepTranunkid = CInt(ViewState("ResolutionStepTranid"))
                mintGrievancemasterunkid = CInt(ViewState("grievancemasteid"))

                objresolution_step_tran._ResolutionStepTranunkid = mintResolutionStepTranunkid
                objGrievance_master._Grievancemasterunkid = mintGrievancemasterunkid

                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                mdtGrievanceDate = objGrievance_master._Grievancedate
                'Gajanan [27-June-2019] -- End

                Call FillCombo()

                'Call Fill_Employee_List()

                CommiteeResponseControlsVisible(False)

                Call GetValue()

                If mintResolutionStepTranunkid > 0 Then
                    EnableDisableResolutionControl(False)
                Else
                    EnableDisableResolutionControl(True)
                End If

            Else
                mintGrievancemasterunkid = CInt(Me.ViewState("Grievancemasterunkid"))
                mintResolutionStepTranunkid = CInt(Me.ViewState("ResolutionStepTranid"))
                blnissubmited = CBool(Me.ViewState("blnissubmited"))
                mstrDeletereason = CStr(Me.ViewState("mstrDeletereason"))
                blnisdeleted = CBool(Me.ViewState("blnisdeleted"))
                mdtGrievanceAttachment = CType(ViewState("mdtGrievanceAttachment"), DataTable)
                mintPriority = CInt(Me.ViewState("mintPriority"))
                mintApprovalSetting = CInt(Me.ViewState("mintApprovalSetting"))

                mDicVisibleGrievances = CType(Me.ViewState("mDicVisibleGrievances"), Global.System.Collections.Generic.Dictionary(Of Integer, Integer))


                mblnPreviousResponseBtn = CBool(Me.ViewState("mblnPreviousResponse"))
                mblnPreviousResponsePopup = CBool(Me.ViewState("mblnPreviousResponsePopup"))

                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                mdtGrievanceDate = CDate(Me.ViewState("GrievanceDate"))
                If IsNothing(ViewState("dtPriviousApproverResolution")) = False Then
                    dtPriviousApproverResolution = CType(ViewState("dtPriviousApproverResolution"), DataTable)
                End If
                'Gajanan [27-June-2019] -- End







            End If
Link:

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try   'Hemant (13 Aug 2020)
        Me.ViewState("Grievancemasterunkid") = mintGrievancemasterunkid
        Me.ViewState("ResolutionStepTranid") = mintResolutionStepTranunkid
        Me.ViewState("blnissubmited") = blnissubmited
        Me.ViewState("mstrDeletereason") = mstrDeletereason
        Me.ViewState("blnisdeleted") = blnisdeleted
        Me.ViewState("mdtGrievanceAttachment") = mdtGrievanceAttachment
        Me.ViewState("mintPriority") = mintPriority
        Me.ViewState("mintApprovalSetting") = mintApprovalSetting
        Me.ViewState("mDicVisibleGrievances") = mDicVisibleGrievances


        'Gajanan [27-June-2019] -- Start      
        'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
        Me.ViewState("GrievanceDate") = mdtGrievanceDate
        'Gajanan [27-June-2019] -- End



        'Gajanan [10-June-2019] -- Start      
        'ISSUE/ENHANCEMENT : Grievance UAT Changes        
        Me.ViewState("mblnPreviousResponseBtn") = mblnPreviousResponseBtn
        Me.ViewState("mblnPreviousResponsePopup") = mblnPreviousResponsePopup
        ViewState("dtPriviousApproverResolution") = dtPriviousApproverResolution
        'Gajanan [10-June-2019] -- End

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try   'Hemant (13 Aug 2020)
        'Gajanan [5-July-2019] -- Start      
        'ISSUE/ENHANCEMENT : Grievance NMB Report
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
        'Gajanan [5-July-2019] -- End

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Private Methods "

    Private Sub SetGrievanceInfo()
        Try
            cboEmpName.SelectedValue = CStr(objGrievance_master._Fromemployeeunkid)
            cboEmpName_SelectedIndexChanged(Nothing, Nothing)
            cboRefNo.SelectedValue = objGrievance_master._Grievancerefno.ToString
            'cboRepsonseType_SelectedIndexChanged(Nothing, Nothing)
            cboGrievanceType.SelectedValue = CStr(objGrievance_master._Grievancetypeid)
            dtGrievanceDate.SetDate = Convert.ToDateTime(objGrievance_master._Grievancedate)
            txtGrievanceDesc.Text = Convert.ToString(objGrievance_master._Grievance_Description)
            txtReliefReq.Text = Convert.ToString(objGrievance_master._Grievance_Reliefwanted)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing
            'Dim objEmployee As New clsEmployee_Master
            Dim objApprover As New clsgrievanceapprover_master


            'Gajanan [4-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes


            'dsList = objApprover.GetApproversList(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), _
            '                                      Session("Database_Name").ToString, _
            '                                      CInt(Session("UserId")), _
            '                                      CInt(Session("Fin_year")), _
            '                                      CInt(Session("CompanyUnkId")), 1202, _
            '                                      Session("UserAccessModeSetting").ToString, True, False, _
            '                                      True, Session("EmployeeAsOnDate").ToString(), True, Nothing, "", True, " And greapprover_master.mapuserunkid= " + CInt(Session("UserId")).ToString)

            dsList = objApprover.GetApproversList(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), _
                                                  Session("Database_Name").ToString, _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), 1202, _
                                                  Session("UserAccessModeSetting").ToString, True, False, _
                                      True, Session("EmployeeAsOnDate").ToString(), True, Nothing, "", True, , , CInt(Session("UserId").ToString()))

            'Gajanan [4-July-2019] -- End



            'S.SANDEEP |30-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Grievance UAT]
            'Dim dtTable As DataTable 
            Dim dtTable As DataTable = Nothing
            'S.SANDEEP |30-MAY-2019| -- END
            Dim strCols As String() = New String() {"approvermasterunkid", "name", _
                                                    "email", "priority", "levelname", _
                                                    "approverempid", "ucompanyid", "mapuserunkid", "rno"}
            If CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) = enGrievanceApproval.UserAcess Then
                dtTable = dsList.Tables(0).DefaultView.ToTable(True, strCols).Copy()
            ElseIf CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) = enGrievanceApproval.ApproverEmpMapping Then
                dtTable = dsList.Tables(0).Copy()
                'Gajanan [24-OCT-2019] -- Start   
                'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
            ElseIf CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) = enGrievanceApproval.ReportingTo Then
                dtTable = dsList.Tables(0).DefaultView.ToTable(True, "approvermasterunkid", "apprlevelunkid", "levelname", "name", "mapuserunkid", "userunkid", "isexternal", "ExAppr", "activestatus")
                'Gajanan [24-OCT-2019] -- End
            End If

            With drpApprover
                .DataValueField = "approvermasterunkid"


                'Gajanan [24-OCT-2019] -- Start   
                'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   

                '.DataTextField = "name"
                Select Case CType(Session("GrievanceApprovalSetting"), enGrievanceApproval)
                    Case enGrievanceApproval.ApproverEmpMapping
                        .DataTextField = "ApproverNameWithLevel"
                    Case Else
                        .DataTextField = "name"
                End Select
                'Gajanan [24-OCT-2019] -- End


                .DataSource = dtTable
                .DataBind()
                'S.SANDEEP |30-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [Grievance UAT]
                '.SelectedValue = "0"
                If dtTable.Rows.Count > 1 Then
                    .SelectedIndex = 1
                Else
                    .SelectedIndex = 0
                End If
                'S.SANDEEP |30-MAY-2019| -- END
            End With



            'If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

            '    dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
            '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                         Session("UserAccessModeSetting").ToString, _
            '                                         True, CBool(Session("IsIncludeInactiveEmp")), _
            '                                         "List", True)

            '    With cboEmpName
            '        .DataValueField = "employeeunkid"
            '        .DataTextField = "EmpCodeName"
            '        .DataSource = dsList.Tables(0)
            '        .DataBind()
            '        .SelectedValue = "0"
            '    End With
            'Else
            '    Dim objglobalassess = New GlobalAccess
            '    objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
            '    cboEmpName.DataSource = objglobalassess.ListOfEmployee
            '    cboEmpName.DataTextField = "loginname"
            '    cboEmpName.DataValueField = "employeeunkid"
            '    cboEmpName.DataBind()
            'End If

            dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.GRIEVANCE_TYPE, True, "Grievance Type")
            With cboGrievanceType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Grievance Type")
                .DataBind()
            End With

            dsList = objresolution_step_tran.GetResponse_Type("Response Type", True)

            With cboRepsonseType
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Response Type")
                .DataBind()
                .SelectedValue = "0"
            End With


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
            'cboRepsonseType.SelectedValue = CInt(clsResolution_Step_Tran.enResponseType.Commitee_Response).ToString
            'Call cboRepsonseType_SelectedIndexChanged(cboRepsonseType, New System.EventArgs)
            cboRepsonseType.Enabled = False
            'Gajanan [10-June-2019] -- End

            'Gajanan [26-Dec-2019] -- Start   
            'Enhancement:Worked On Grievance Reporting To Testing Document
            If CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) = enGrievanceApproval.ReportingTo Then
                pnl_Approverlevel.Visible = False
            End If
            'Gajanan [26-Dec-2019] -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If CInt(cboEmpName.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), Me)
                cboEmpName.Focus()
                Return False
            End If

            If CInt(cboRefNo.SelectedIndex) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Ref No is compulsory information. Please select Ref No to continue."), Me)
                cboRefNo.Focus()
                Return False
            End If

            If CInt(cboRepsonseType.SelectedIndex) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Repsonse Type is compulsory information. Please select Repsonse Type to continue."), Me)
                cboRepsonseType.Focus()
                Return False
            End If

            If CInt(txtRespnonseRemark.Text.Trim.Length) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Respnonse Remark is compulsory information. Please Enter Respnonse Remark to continue."), Me)
                txtRespnonseRemark.Focus()
                Return False
            End If

            If CInt(cboRepsonseType.SelectedIndex) = 2 AndAlso dtMeetingDate.GetDate.Date = Nothing Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Meeting Date is compulsory information. Please Select Meeting Date to continue."), Me)
                txtRespnonseRemark.Focus()
                Return False
            End If

            If CInt(cboRepsonseType.SelectedValue) = clsResolution_Step_Tran.enResponseType.Commitee_Response Then
                If txtQualifingRemark.Text.Trim.Length <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Qualification Remark is compulsory information. Please Enter Qualification Remark to continue."), Me)
                    txtQualifingRemark.Focus()
                    Return False
                End If
            End If

            dtEmployee = CType(Me.ViewState("dtEmployee"), DataTable)
            Dim drRow() As DataRow = dtEmployee.Select("ischeck=true")

            If CInt(cboRepsonseType.SelectedIndex) = 2 AndAlso drRow.Count = 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Committee Members are compulsory information. Please Select Committee Members to continue."), Me)
                Return False
            End If





            If dtMeetingDate.GetDate > Date.Today Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Meeting Date Should Not Greater Than Today's Date."), Me)
                Return False
            End If




            If dtMeetingDate.IsNull = False AndAlso mdtGrievanceDate > dtMeetingDate.GetDate Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Meeting Date Should Not Less Than Grievance Date."), Me)
                Return False
            End If


            If mintResolutionStepTranunkid = 0 Then
                If objresolution_step_tran.isExist(CInt(cboRefNo.SelectedValue), CInt(drpApprover.SelectedValue)) Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry,you have already given resolution for this selected grievance."), Me)
                    Exit Function
                End If
            End If

            'Gajanan [27-June-2019] -- End




            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub FillList()
        Try
            Dim dsGrievanceList As New DataSet

            Dim StrSearching As String = String.Empty

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToSubmitResolutionStep")) = False Then Exit Sub
            End If

            If CInt(cboEmpName.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmpName.SelectedValue) & " "
            End If

            If CInt(cboRefNo.SelectedValue) > 0 Then
                StrSearching &= "AND lnloan_process_pending_loan.loanschemeunkid = " & CInt(cboRefNo.SelectedValue) & " "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        


            'dsGrievanceList = objGrievance_master.GetList(Session("Database_Name").ToString, -1, "", True)
            dsGrievanceList = objGrievance_master.GetList(Session("Database_Name").ToString, clsGrievance_Master.EmployeeType.AgainstEmployee, -1, "", True)
            'Gajanan [10-June-2019] -- End


            dtGrievanceList = New DataView(dsGrievanceList.Tables("Grievance"), "", "grievancemasterunkid desc", DataViewRowState.CurrentRows).ToTable


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_Employee_List()
        Dim ObjEmp As New clsEmployee_Master
        Dim dsEmployee As New DataSet
        Try

            dsEmployee = ObjEmp.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     Session("UserAccessModeSetting").ToString, _
                                                     True, CBool(Session("IsIncludeInactiveEmp")), _
                                                     "List", False)


            Dim dCol As DataColumn

            If dsEmployee.Tables(0).Columns.Contains("ischeck") = False Then
                dCol = New DataColumn
                With dCol
                    .DataType = GetType(System.Boolean)
                    .DefaultValue = False
                    .ColumnName = "ischeck"
                End With
                dsEmployee.Tables(0).Columns.Add(dCol)
            End If

            If dsEmployee.Tables(0).Columns.Contains("AUD") = False Then
                dCol = New DataColumn
                With dCol
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                    .ColumnName = "AUD"
                End With
                dsEmployee.Tables(0).Columns.Add(dCol)
            End If




            Dim strFilter As String = ""
            If CInt(IIf(cboEmpName.SelectedValue = "", 0, cboEmpName.SelectedValue)) > 0 Then
                strFilter = "employeeunkid NOT IN (" & CInt(cboEmpName.SelectedValue) & "," & objGrievance_master._Againstemployeeunkid.ToString & ") "
            End If
            dtEmployee = New DataView(dsEmployee.Tables(0), strFilter, "", DataViewRowState.CurrentRows).ToTable(True, "employeeunkid", "employeecode", "employeename", "ischeck", "AUD")

            If Me.ViewState("dtEmployee") Is Nothing Then
                Me.ViewState.Add("dtEmployee", dtEmployee)
            Else
                Me.ViewState("dtEmployee") = dtEmployee
            End If

            If Me.ViewState("mdtTran") Is Nothing Then
                Me.ViewState.Add("mdtTran", dtEmployee)
            Else
                Me.ViewState("mdtTran") = dtEmployee
            End If

            dgvEmp.AutoGenerateColumns = False
            dgvEmp.DataSource = dtEmployee
            dgvEmp.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If dtEmployee.Rows.Count <= 0 Then
                Dim r As DataRow = dtEmployee.NewRow

                r.Item("employeecode") = "" ' To Hide the row and display only Row Header
                r.Item("employeename") = ""

                dtEmployee.Rows.Add(r)
            End If

            dgvEmp.DataSource = dtEmployee
            dgvEmp.DataBind()
            ObjEmp = Nothing : dsEmployee.Dispose()
        End Try
    End Sub

    Private Sub Fill_Scan_Attachment_List(ByVal GrievanceId As Integer)
        Try

            Dim mstrPreviewIds As String = ""
            Dim dtTable As New DataTable

            dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(CInt(cboEmpName.SelectedValue), _
                                                                                 enScanAttactRefId.GRIEVANCES, _
                                                                                 enImg_Email_RefId.Scan_Attached_Doc, _
                                                                                 CInt(mintGrievancemasterunkid))
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mstrPreviewIds = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
            End If


            'Gajanan [5-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance NMB Report


            'popup_ShowAttchment.ScanTranIds = mstrPreviewIds
            ''S.SANDEEP |25-JAN-2019| -- START
            ''ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
            'popup_ShowAttchment._ZipFileName = "Grievance_" + cboEmpName.SelectedItem.Text.Replace(" ", "_") + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
            ''S.SANDEEP |25-JAN-2019| -- END
            'popup_ShowAttchment._Webpage = Me
            'popup_ShowAttchment.Show()

            'Gajanan [5-July-2019] -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Gajanan [5-July-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance NMB Report


    'Protected Sub popup_ShowAttchment_btnPreviewClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ShowAttchment.btnPreviewClose_Click
    '    Try
    '        popup_ShowAttchment.Hide()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Gajanan [5-July-2019] -- End

    Private Sub GetValue()

        Try
            If mintResolutionStepTranunkid > 0 Then
                drpApprover.SelectedValue = Convert.ToString(objresolution_step_tran._Approvermasterunkid)
                txtRespnonseRemark.Text = Convert.ToString(objresolution_step_tran._Responseremark)
                txtQualifingRemark.Text = Convert.ToString(objresolution_step_tran._Qualifyremark)
                cboRepsonseType.SelectedValue = Convert.ToString(objresolution_step_tran._Responsetypeunkid)
                cboRepsonseType_SelectedIndexChanged(Nothing, Nothing)
            End If

            drpApprover_SelectedIndexChanged(Nothing, Nothing)

            'cboEmpName.SelectedValue = Convert.ToString(objGrievance_master._Fromemployeeunkid)

            cboEmpName.SelectedValue = Convert.ToString(objGrievance_master._Againstemployeeunkid)


            cboEmpName_SelectedIndexChanged(Nothing, Nothing)
            cboRefNo.SelectedValue = Convert.ToString(objGrievance_master._Grievancemasterunkid)
            cboRefNo_SelectedIndexChanged(Nothing, Nothing)
            dtMeetingDate.SetDate = CDate(objresolution_step_tran._Meetingdate)


            'Gajanan [5-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance NMB Report
            'mdtGrievanceAttachment = objDocument.GetQulificationAttachment(objGrievance_master._Fromemployeeunkid, enScanAttactRefId.GRIEVANCES, mintGrievancemasterunkid, CStr(Session("Document_Path")))
            If CInt(objGrievance_master._Grievancemasterunkid) > 0 Then
                mdtGrievanceAttachment = objDocument.GetQulificationAttachment(objGrievance_master._Fromemployeeunkid, enScanAttactRefId.GRIEVANCES, mintGrievancemasterunkid, CStr(Session("Document_Path")))
            Else
                If IsNothing(mdtGrievanceAttachment) = False Then
                    mdtGrievanceAttachment.Clear()
                End If
            End If
            'Gajanan [5-July-2019] -- End

            fillAttachment()

            CheckState()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub fillAttachment()
        Dim dtView As DataView
        Try
            If mdtGrievanceAttachment Is Nothing Then Exit Sub

            dtView = New DataView(mdtGrievanceAttachment, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            gvGrievanceAttachment.AutoGenerateColumns = False
            gvGrievanceAttachment.DataSource = dtView
            gvGrievanceAttachment.DataBind()

            If gvGrievanceAttachment.Rows.Count > 1 Then
                btndownloadall.Visible = True
            Else
                btndownloadall.Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub CheckState()
        Try   'Hemant (13 Aug 2020)
        If cboRepsonseType.SelectedIndex = 2 Then


            Dim dsEmployeeList As New DataSet
            dsEmployeeList = objresolution_step_tran.GetDataOfCommitteeData("EmployeeList", mintResolutionStepTranunkid)
            Dim dvEmployee As DataView = CType(Me.ViewState("dtEmployee"), DataTable).DefaultView
            Dim strEmployeeList As String = String.Empty
            For Each dr As DataRow In dsEmployeeList.Tables(0).Rows

                For i As Integer = 0 To dgvEmp.Rows.Count - 1
                    Dim gvRow As GridViewRow = dgvEmp.Rows(i)
                    If dgvEmp.DataKeys(i).Values("employeeunkid").ToString = dr("employeeunkid").ToString Then
                        Dim dRow() As DataRow = CType(Me.ViewState("dtEmployee"), DataTable).Select("employeeunkid = '" & dr("employeeunkid").ToString & "'")
                        If dRow.Length > 0 Then
                            dRow(0).Item("ischeck") = True
                            CType(gvRow.FindControl("chkbox1"), CheckBox).Checked = True
                        End If
                    End If
                Next
            Next
            Me.ViewState("dtEmployee") = dvEmployee.ToTable
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Private Sub SetValue()
        Try   'Hemant (13 Aug 2020)
        If mintResolutionStepTranunkid > 0 Then
            objresolution_step_tran._ResolutionStepTranunkid = mintResolutionStepTranunkid
        End If

        objGrievance_master._Grievancemasterunkid = mintGrievancemasterunkid
        objresolution_step_tran._Grievancemasterunkid = mintGrievancemasterunkid
        objresolution_step_tran._Responsetypeunkid = CInt(cboRepsonseType.SelectedValue)
        objresolution_step_tran._Responseremark = txtRespnonseRemark.Text

        If cboRepsonseType.SelectedValue = Convert.ToString(clsResolution_Step_Tran.enResponseType.Commitee_Response) Then
            objresolution_step_tran._Qualifyremark = txtQualifingRemark.Text
        Else
            objresolution_step_tran._Qualifyremark = ""
        End If


        If cboRepsonseType.SelectedValue = Convert.ToString(clsResolution_Step_Tran.enResponseType.Commitee_Response) Then
            objresolution_step_tran._Meetingdate = dtMeetingDate.GetDateTime()
        Else
            objresolution_step_tran._Meetingdate = Nothing
        End If
        objresolution_step_tran._Isvoid = False
        objresolution_step_tran._Voiduserunkid = -1
        objresolution_step_tran._Voidreason = String.Empty
        objresolution_step_tran._Voiddatetime = Nothing
        objresolution_step_tran._Approvermasterunkid = 0
        objresolution_step_tran._AuditDate = Now
        objresolution_step_tran._AuditUserId = CInt(Session("UserId"))
        objresolution_step_tran._ClientIP = Session("IP_ADD").ToString()
        objresolution_step_tran._HostName = Session("HOST_NAME").ToString()
        objresolution_step_tran._Loginemployeeunkid = -1
        objresolution_step_tran._FormName = mstrModuleName
        objresolution_step_tran._FromWeb = True

        'Gajanan [24-OCT-2019] -- Start   
        'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
        'Dim objAppr As New clsgrievanceapprover_master
        'objAppr._Approvermasterunkid = CInt(drpApprover.SelectedValue)
        'objresolution_step_tran._Apprlevelunkid = objAppr._Apprlevelunkid
        'objresolution_step_tran._Approvalsettingid = objAppr._Approvalsettingid
        'objresolution_step_tran._Approverempid = objAppr._Approverempid
        'objresolution_step_tran._Approvermasterunkid = objAppr._Approvermasterunkid
        'objAppr = Nothing

        If CInt(Session("GrievanceApprovalSetting").ToString()) = CInt(enGrievanceApproval.ReportingTo) Then
            Dim objAppr As New clsEmployee_Master
            objAppr._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), Nothing) = CInt(drpApprover.SelectedValue)
            objresolution_step_tran._Apprlevelunkid = 0
            objresolution_step_tran._Approvalsettingid = CInt(enGrievanceApproval.ReportingTo)
            objresolution_step_tran._Approverempid = CInt(drpApprover.SelectedValue)
            objresolution_step_tran._Approvermasterunkid = CInt(drpApprover.SelectedValue)
            objAppr = Nothing
        Else
            Dim objAppr As New clsgrievanceapprover_master
            objAppr._Approvermasterunkid = CInt(drpApprover.SelectedValue)
            objresolution_step_tran._Apprlevelunkid = objAppr._Apprlevelunkid
            objresolution_step_tran._Approvalsettingid = objAppr._Approvalsettingid
            objresolution_step_tran._Approverempid = objAppr._Approverempid
            objresolution_step_tran._Approvermasterunkid = objAppr._Approvermasterunkid
            objAppr = Nothing
        End If

        'Gajanan [24-OCT-2019] -- End
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Private Sub CommiteeResponseControlsVisible(ByVal Isvisible As Boolean)
        Try
            txtQualifingRemark.Enabled = Isvisible
            dtMeetingDate.Enabled = Isvisible
            txtSearch.Enabled = Isvisible
            dgvEmp.Enabled = Isvisible
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub EnableDisableResolutionControl(ByVal status As Boolean)
        Try
            drpApprover.Enabled = status
            txtLevel.Enabled = status
            dtGrievanceDate.Enabled = status
            cboEmpName.Enabled = status
            cboRefNo.Enabled = status

            cboGrievanceType.Enabled = False
            txtAgainstEmployee.Enabled = False
            txtGrievanceDesc.Enabled = False
            txtReliefReq.Enabled = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SaveResolutionData(ByVal blnIsSubmmitted As Boolean)
        Dim blnFlag As Boolean = False
        Try
            SetValue()

            Dim isfinal As Boolean = False

            If blnisdeleted Then
                objresolution_step_tran._Voidreason = mstrDeletereason
                mstrDeletereason = ""
                blnisdeleted = False
            End If

            objresolution_step_tran._Issubmitted = blnIsSubmmitted
            dtEmployee = CType(Me.ViewState("dtEmployee"), DataTable)

            Dim dsApprovers As New DataSet
            Dim objAppr As New clsgrievanceapprover_master
            Dim eApprType As enGrievanceApproval

            'If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
            '    eApprType = enGrievanceApproval.ApproverEmpMapping
            'ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
            '    eApprType = enGrievanceApproval.UserAcess
            'End If


            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
            'dsApprovers = objAppr.GetApproversList(eApprType, CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, cboEmpName.SelectedValue)
            dsApprovers = objAppr.GetApproversList(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), 1202, CStr(Session("UserAccessModeSetting")), True, True, True, Session("EmployeeAsOnDate").ToString, False, Nothing, cboEmpName.SelectedValue)
            'Gajanan [24-OCT-2019] -- End

            If dsApprovers.Tables(0).Rows.Count <= 0 Then
                isfinal = True
            End If


            If mintResolutionStepTranunkid > 0 Then
                blnFlag = objresolution_step_tran.Update(dtEmployee)
            Else
                blnFlag = objresolution_step_tran.Insert(dtEmployee)
                mintResolutionStepTranunkid = objresolution_step_tran._ResolutionStepTranunkid
            End If


            If blnFlag = False Then
                DisplayMessage.DisplayMessage(objresolution_step_tran._Message, Me)
                Exit Sub
            End If


            If blnIsSubmmitted And blnFlag = True Then


                If isfinal Then
                    blnFlag = objresolution_step_tran.setIsfinal(mintResolutionStepTranunkid)
                    If blnFlag = False Then
                        DisplayMessage.DisplayMessage(objresolution_step_tran._Message, Me)
                    End If
                End If


                Dim objEmp As New clsEmployee_Master


                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes


                Dim strAgainstEmployeeName As String = String.Empty
                Dim strRaisedEmployeeName As String = String.Empty

                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = objGrievance_master._Againstemployeeunkid
                strAgainstEmployeeName = objEmp._Firstname + " " + objEmp._Surname

                'Gajanan [27-June-2019] -- End


                'Gajanan [24-June-2019] -- Start      
                'objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmpName.SelectedValue)
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = objGrievance_master._Fromemployeeunkid
                'objgrievanceapprover_master.SendNotificationToEmployee(CStr(Session("ArutiSelfServiceURL")), CInt(cboEmpName.SelectedValue), objEmp._Email, _
                '                                                       CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mintGrievancemasterunkid, cboRefNo.SelectedItem.ToString, _
                '                                                        mstrModuleName, enLogin_Mode.EMP_SELF_SERVICE, objEmp._Email, cboEmpName.SelectedItem.ToString, CInt(Session("UserId")), _
                '                                                      Session("IP_ADD").ToString(), Session("HOST_NAME").ToString(), mintResolutionStepTranunkid, mintPriority)


                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes


                'objgrievanceapprover_master.SendNotificationToEmployee(CStr(Session("ArutiSelfServiceURL")), objGrievance_master._Fromemployeeunkid, objEmp._Email, _
                '                                                       CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mintGrievancemasterunkid, cboRefNo.SelectedItem.ToString, _
                '                                                        mstrModuleName, enLogin_Mode.EMP_SELF_SERVICE, objEmp._Email, txtAgainstEmployee.Text, CInt(Session("UserId")), _
                '                                                      Session("IP_ADD").ToString(), Session("HOST_NAME").ToString(), mintResolutionStepTranunkid, mintPriority)


                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                strRaisedEmployeeName = objEmp._Firstname + " " + objEmp._Surname






                'Gajanan [27-June-2019] -- End
                'Gajanan [3-July-2019] -- Start
                'objgrievanceapprover_master.SendNotificationToEmployee(CStr(Session("ArutiSelfServiceURL")), objGrievance_master._Fromemployeeunkid, objEmp._Email, _
                '                                                       CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mintGrievancemasterunkid, cboRefNo.SelectedItem.ToString, _
                '                                                        mstrModuleName, enLogin_Mode.EMP_SELF_SERVICE, objEmp._Email, txtAgainstEmployee.Text, CInt(Session("UserId")), _
                '                                      Session("IP_ADD").ToString(), Session("HOST_NAME").ToString(), mintResolutionStepTranunkid, mintPriority, objEmp._Firstname & " " & objEmp._Surname)


                objgrievanceapprover_master.SendNotificationToEmployee(CStr(Session("ArutiSelfServiceURL")), objGrievance_master._Fromemployeeunkid, objEmp._Email, _
                                                                       CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), mintGrievancemasterunkid, cboRefNo.SelectedItem.ToString, _
                                                                   mstrModuleName, enLogin_Mode.EMP_SELF_SERVICE, objEmp._Email, strRaisedEmployeeName, CInt(Session("UserId")), _
                                                 Session("IP_ADD").ToString(), Session("HOST_NAME").ToString(), mintResolutionStepTranunkid, mintPriority, strAgainstEmployeeName)

                'Gajanan [3-July-2019] -- End

                'Gajanan [27-June-2019] -- End

                'Gajanan [24-June-2019] -- End

            End If




            mintResolutionStepTranunkid = 0
            mintPriority = 0
            mintApprovalSetting = 0

            'Gajanan [10-July-2019] -- Start      

            'If Request.QueryString.ToString.Length > 0 Then
            '    Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
            'Else
            '    Response.Redirect(Session("rootpath").ToString & "Grievance/wpg_ResolutionStepList.aspx", False)
            'End If

            If Request.QueryString.ToString.Length > 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Submitted Successfully"), Me, Session("rootpath").ToString & "Index.aspx")
            Else
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 18, "Submitted Successfully"), Me, Session("rootpath").ToString & "Grievance/wpg_ResolutionStepList.aspx")
            End If

            'Gajanan [10-July-2019] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " ComboBox Event(s) "

    Protected Sub cboRefNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRefNo.SelectedIndexChanged
        Try
            If CInt(IIf(cboRefNo.SelectedValue = "", 0, cboRefNo.SelectedValue)) > 0 Then
                Dim dsList As DataSet = Nothing

                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                'dsList = objGrievance_master.GetList("Grievance Details", CInt(cboEmpName.SelectedValue), cboRefNo.SelectedItem.Text, True, False, Convert.ToInt32(Session("GrievanceApprovalSetting").ToString))
                dsList = objGrievance_master.GetList("Grievance Details", clsGrievance_Master.EmployeeType.AgainstEmployee, CInt(cboEmpName.SelectedValue), cboRefNo.SelectedItem.Text, True, False, Convert.ToInt32(Session("GrievanceApprovalSetting").ToString))
                'Gajanan [10-June-2019] -- End
                objGrievance_master._Grievancemasterunkid = CInt(dsList.Tables("Grievance Details").Rows(0).Item("grievancemasterunkid"))
                Me.ViewState("Grievancemasterunkid") = objGrievance_master._Grievancemasterunkid
                mintGrievancemasterunkid = objGrievance_master._Grievancemasterunkid
                txtAgainstEmployee.Text = dsList.Tables("Grievance Details").Rows(0)("Fromemployee").ToString
                'txtAgainstEmployee.Attributes.Add("empid", objGrievance_master._Againstemployeeunkid.ToString())
                'GetValue()

                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                mdtGrievanceDate = objGrievance_master._Grievancedate
                'Gajanan [27-June-2019] -- End





                If mintResolutionStepTranunkid = 0 Then

                    Dim strMsg As String = String.Empty
                    'Gajanan [27-June-2019] -- Start      
                    'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes
                    If objresolution_step_tran.isExist(CInt(dsList.Tables("Grievance Details").Rows(0).Item("grievancemasterunkid")), CInt(drpApprover.SelectedValue)) Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry,you have already given resolution for this selected grievance."), Me)
                        Exit Sub
                    End If
                    'Gajanan [27-June-2019] -- End

                    If enGrievanceApproval.UserAcess = mintApprovalSetting Then

                        strMsg = objresolution_step_tran.IsValidApprover(mintGrievancemasterunkid, mintPriority, mintApprovalSetting, Session("Database_Name").ToString, _
                                                                         CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), Session("UserAccessModeSetting").ToString, _
                                                                         Session("EmployeeAsOnDate").ToString(), CInt(cboEmpName.SelectedValue))

                    ElseIf enGrievanceApproval.ApproverEmpMapping = mintApprovalSetting Then
                        'strMsg = objresolution_step_tran.IsValidApprover(mintGrievancemasterunkid, mintPriority, mintApprovalSetting, Session("Database_Name").ToString, _
                        '                                                 CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), Session("UserAccessModeSetting").ToString, _
                        '                                                 Session("EmployeeAsOnDate").ToString(), CInt(cboEmpName.SelectedValue))
                    End If




                    'objlblError.Text = strMsg

                    If strMsg.Trim.Length > 0 Then

                        If Request.QueryString.ToString.Length > 0 Then
                            DisplayMessage.DisplayMessage(strMsg, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        Else
                            DisplayMessage.DisplayMessage(strMsg, Me)
                        End If

                        txtAgainstEmployee.Text = ""
                        cboGrievanceType.SelectedIndex = 0
                        dtGrievanceDate.SetDate = Nothing
                        txtGrievanceDesc.Text = ""
                        txtReliefReq.Text = ""
                        cboRefNo.SelectedValue = CStr(0)
                        cboGrievanceType.SelectedValue = CStr(0)
                        Exit Sub
                    End If
                    If strMsg.Trim.Length <= 0 Then
                        pnlData.Enabled = True
                        btnSave.Visible = True
                        btnSubmit.Visible = True
                    Else
                        pnlData.Enabled = False
                        btnSave.Visible = False
                        btnSubmit.Visible = False
                    End If




                ElseIf mintResolutionStepTranunkid > 0 Then
                    pnlData.Enabled = True
                    btnSave.Visible = True
                    btnSubmit.Visible = True
                End If


                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                'cboGrievanceType.SelectedValue = CStr(objGrievance_master._Grievancetypeid)
                cboRepsonseType.SelectedValue = CInt(clsResolution_Step_Tran.enResponseType.Commitee_Response).ToString
                Call cboRepsonseType_SelectedIndexChanged(cboRepsonseType, New System.EventArgs)
                'Gajanan [10-June-2019] -- End

                dtGrievanceDate.SetDate = Convert.ToDateTime(objGrievance_master._Grievancedate)
                txtGrievanceDesc.Text = Convert.ToString(objGrievance_master._Grievance_Description)
                txtReliefReq.Text = Convert.ToString(objGrievance_master._Grievance_Reliefwanted)

                cboGrievanceType.SelectedValue = Convert.ToString(objGrievance_master._Grievancetypeid)

                Fill_Employee_List()

                mdtGrievanceAttachment = objDocument.GetQulificationAttachment(objGrievance_master._Fromemployeeunkid, enScanAttactRefId.GRIEVANCES, mintGrievancemasterunkid, CStr(Session("Document_Path")))
                fillAttachment()


                'Gajanan [27-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V3 Changes


                Dim objApprover As New clsgrievanceapprover_master
                objApprover._Approvermasterunkid = CInt(drpApprover.SelectedValue)
                dtPriviousApproverResolution = objresolution_step_tran.GetEmployeeResolutionStatus(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), _
                                                                                                   objApprover._Mapuserunkid, Session("Database_Name").ToString, _
                                                                                                   CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                                                                   Session("UserAccessModeSetting").ToString, Session("EmployeeAsOnDate").ToString(), _
                                                                                                   cboEmpName.SelectedValue.ToString(), cboRefNo.SelectedItem.ToString(), CInt(cboRefNo.SelectedValue), True, , , objGrievance_master._Fromemployeeunkid)


                If IsNothing(dtPriviousApproverResolution) = False AndAlso dtPriviousApproverResolution.Rows.Count > 0 Then
                    mblnPreviousResponseBtn = True
                    btnViewPreviousResolution.Visible = True
                    ViewState("dtPriviousApproverResolution") = dtPriviousApproverResolution
                Else
                    mblnPreviousResponseBtn = False
                End If


                'Gajanan [27-June-2019] -- End

            Else
                'dtGrievanceDate.SetDate = Nothing
                'txtGrievanceDesc.Text = ""
                'txtReliefReq.Text = ""
                'cboRefNo.SelectedValue = CStr(0)
                'cboGrievanceType.SelectedValue = CStr(0)
                'pnlData.Enabled = False
                'btnSave.Visible = True
                'btnSubmit.Visible = True

                'mdtGrievanceAttachment.Clear()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboEmpName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpName.SelectedIndexChanged
        Try
            If CInt(IIf(cboEmpName.SelectedValue = "", 0, cboEmpName.SelectedValue)) > 0 Then
                Dim dsList As DataSet = Nothing
                Dim strFilter As String = ""
                If CBool(Me.ViewState("IsDirect")) = True Then
                    strFilter = "gregrievance_master.grievancemasterunkid = '" & mintGrievancemasterunkid & "'"
                Else
                    Dim strIds As String = ""
                    strIds = String.Join(",", mDicVisibleGrievances.AsEnumerable().Where(Function(x) x.Value = CInt(IIf(cboEmpName.SelectedValue = "", 0, cboEmpName.SelectedValue))).Select(Function(x) x.Key.ToString()).ToArray())
                    strFilter = "gregrievance_master.grievancemasterunkid IN (" & strIds & ") "
                End If


                dsList = objGrievance_master.GetRefno("RefNoList", CType(Session("GrievanceApprovalSetting").ToString, enGrievanceApproval), CInt(cboEmpName.SelectedValue), "", True, strFilter)

                With cboRefNo
                    .DataValueField = "grievancemasterunkid"
                    .DataTextField = "grievancerefno"
                    .DataSource = dsList.Tables("RefNoList")
                    .DataBind()
                    If CBool(Me.ViewState("IsDirect")) = True Then
                        .SelectedValue = mintGrievancemasterunkid.ToString()
                        cboRefNo_SelectedIndexChanged(sender, e)
                    End If
                End With

            Else
                cboRefNo.SelectedValue = CStr(0)
                cboRefNo_SelectedIndexChanged(sender, e)
                cboRefNo.Items.Clear()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboRepsonseType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRepsonseType.SelectedIndexChanged
        Try
            If (cboRepsonseType.SelectedValue = Convert.ToString(clsResolution_Step_Tran.enResponseType.Commitee_Response)) Then
                CommiteeResponseControlsVisible(True)
            Else
                CommiteeResponseControlsVisible(False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub



    'Gajanan [10-June-2019] -- Start      
    'ISSUE/ENHANCEMENT : Grievance UAT Changes        


    'Protected Sub drpApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpApprover.SelectedIndexChanged
    '    Dim dsList As DataSet = Nothing
    '    Try
    '        Dim objApprover As New clsgrievanceapprover_master
    '        Dim objApprLevel As New clsGrievanceApproverLevel
    '        If CInt(drpApprover.SelectedValue) > 0 Then
    '            objApprover._Approvermasterunkid = CInt(drpApprover.SelectedValue)
    '            objApprLevel._ApprLevelunkid = objApprover._Apprlevelunkid

    '            dsList = objApprover.GetEmployeeByApprover(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), "List", objApprover._Mapuserunkid, Session("Database_Name").ToString, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, Session("EmployeeAsOnDate").ToString(), True, True)

    '            'S.SANDEEP |30-MAY-2019| -- START
    '            'ISSUE/ENHANCEMENT : [Grievance UAT]
    '            If CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) = enGrievanceApproval.ApproverEmpMapping Then
    '                Dim strFilterString As String = String.Empty
    '                strFilterString = "approvermasterunkid IN (0," & CInt(drpApprover.SelectedValue).ToString() & ")"
    '                Dim dtTable As DataTable = New DataView(dsList.Tables(0), strFilterString, "", DataViewRowState.CurrentRows).ToTable().Copy()
    '                If dtTable IsNot Nothing And dtTable.Rows.Count > 0 Then
    '                    dsList.Tables.RemoveAt(0)
    '                    dsList.Tables.Add(dtTable.Copy)
    '                End If
    '            End If
    '            'S.SANDEEP |30-MAY-2019| -- END

    '            mDicVisibleGrievances = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("visible") = 1).ToDictionary(Function(x) x.Field(Of Integer)("grievancemasterunkid"), Function(y) y.Field(Of Integer)("employeeunkid"))

    '            With cboEmpName
    '                .DataValueField = "employeeunkid"
    '                .DataTextField = "EmpCodeName"
    '                .DataSource = dsList.Tables(0).DefaultView.ToTable("employeecode", True, "EmpCodeName", "employeeunkid")
    '                .DataBind()
    '                .SelectedValue = "0"
    '            End With
    '            Call cboEmpName_SelectedIndexChanged(sender, e)
    '            txtLevel.Text = objApprLevel._Levelname
    '            'txtLevel.Attributes.Add("lvl", CStr(objApprLevel._Priority))
    '            mintPriority = objApprLevel._Priority
    '            mintApprovalSetting = objApprover._Approvalsettingid
    '            'drpApprover.Attributes.Add("set", objApprover._Approvalsettingid.ToString())
    '        Else
    '            cboEmpName.SelectedValue = CStr(0)
    '            Call cboEmpName_SelectedIndexChanged(sender, e)
    '            txtLevel.Text = ""
    '            cboEmpName.Items.Clear()
    '            cboEmpName.DataSource = Nothing
    '            'txtLevel.Attributes.Remove("lvl")
    '            'drpApprover.Attributes.Remove("set")
    '            mintPriority = 0
    '            mintApprovalSetting = 0
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub



    Protected Sub drpApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpApprover.SelectedIndexChanged
        Dim dsList As DataSet = Nothing
        Try
            Dim objApprover As New clsgrievanceapprover_master
            Dim objApprLevel As New clsGrievanceApproverLevel
            If CInt(drpApprover.SelectedValue) > 0 Then
                objApprover._Approvermasterunkid = CInt(drpApprover.SelectedValue)
                objApprLevel._ApprLevelunkid = objApprover._Apprlevelunkid

                dsList = objApprover.GetEmployeeByApprover(CType(Session("GrievanceApprovalSetting"), enGrievanceApproval), "List", objApprover._Mapuserunkid, Session("Database_Name").ToString, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("UserAccessModeSetting").ToString, Session("EmployeeAsOnDate").ToString(), True, True)

                If CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) = enGrievanceApproval.ApproverEmpMapping OrElse _
                    CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) = enGrievanceApproval.ReportingTo Then
                    'Gajanan [24-OCT-2019] -- Add Reporting To Condition
                    Dim Priority As Integer = 0
                    Dim GrievanceId As Integer = 0
                    Dim OldRow As DataRow = Nothing

                    For Each irow As DataRow In dsList.Tables("List").Rows

                        If CInt(irow.Item("grievancemasterunkid")) > 0 Then


                            If GrievanceId > 0 AndAlso GrievanceId < CInt(irow.Item("grievancemasterunkid")) Then
                                Priority = 0
                                GrievanceId = CInt(irow.Item("grievancemasterunkid"))
                            Else
                                GrievanceId = CInt(irow.Item("grievancemasterunkid"))
                            End If


                            If Priority = 0 Then
                                If CInt(irow.Item("priority")) > 0 Then
                                    Priority = CInt(irow.Item("priority"))
                                    OldRow = Nothing
                                End If
                            End If

                            If CInt(irow.Item("priority")) = Priority Then

                                If CInt(irow.Item("resolutionsteptranunkid")) > 0 Then

                                    If CInt(irow.Item("Resolutionissubmitted")) <= 0 Then
                                        irow.Item("visible") = "1"
                                    Else
                                        irow.Item("visible") = "0"
                                    End If

                                Else
                                    irow.Item("visible") = "1"
                                End If

                            ElseIf CInt(irow.Item("priority")) > Priority Then
                                If IsNothing(OldRow) = False Then

                                    'Gajanan [13-July-2019] -- Start      
                                    'ISSUE/ENHANCEMENT : Grievance UAT Enhancement
                                    'If CInt(OldRow.Item("initiatortranunkid")) > 0 And (CInt(OldRow.Item("statusunkid")) = 2 CInt(OldRow.Item("statusunkid")) = 2) Then
                                    If CInt(OldRow.Item("initiatortranunkid")) > 0 And (CInt(OldRow.Item("statusunkid")) = enGrievanceResponse.DisagreeAndEscalateToNextLevel Or CInt(OldRow.Item("statusunkid")) = enGrievanceResponse.Appeal) Then
                                        'Gajanan [13-July-2019] -- End
                                        If CInt(irow.Item("resolutionsteptranunkid")) > 0 Then

                                            If CInt(irow.Item("Resolutionissubmitted")) <= 0 Then
                                                irow.Item("visible") = "1"
                                            Else
                                                irow.Item("visible") = "0"
                                            End If

                                        Else
                                            irow.Item("visible") = "1"
                                        End If
                                    Else
                                        irow.Item("visible") = "0"
                                    End If
                                End If
                            End If

                            dsList.Tables("List").AcceptChanges()
                            OldRow = irow
                        End If

                    Next

                End If


                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        



                ''S.SANDEEP |30-MAY-2019| -- START
                ''ISSUE/ENHANCEMENT : [Grievance UAT]
                'If CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) = enGrievanceApproval.ApproverEmpMapping Then
                '    Dim strFilterString As String = String.Empty
                '    strFilterString = "approvermasterunkid IN (0," & CInt(drpApprover.SelectedValue).ToString() & ")"
                '    Dim dtTable As DataTable = New DataView(dsList.Tables(0), strFilterString, "", DataViewRowState.CurrentRows).ToTable().Copy()
                '    If dtTable IsNot Nothing And dtTable.Rows.Count > 0 Then
                '        dsList.Tables.RemoveAt(0)
                '        dsList.Tables.Add(dtTable.Copy)
                '    End If
                'End If
                ''S.SANDEEP |30-MAY-2019| -- END

                Dim dtTable As DataTable
                If CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) = enGrievanceApproval.ApproverEmpMapping OrElse _
CType(Session("GrievanceApprovalSetting"), enGrievanceApproval) = enGrievanceApproval.ReportingTo Then
                    'Gajanan [24-OCT-2019] -- Add Reporting To Condition
                    Dim strFilterString As String = String.Empty
                    strFilterString = "approvermasterunkid IN(0, " & CInt(drpApprover.SelectedValue).ToString() & ") and visible = 1"
                    dtTable = New DataView(dsList.Tables(0), strFilterString, "", DataViewRowState.CurrentRows).ToTable().Copy()

                    mDicVisibleGrievances = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("visible") = 1).ToDictionary(Function(x) x.Field(Of Integer)("grievancemasterunkid"), Function(y) y.Field(Of Integer)("employeeunkid"))

                    With cboEmpName
                        .DataValueField = "employeeunkid"
                        .DataTextField = "EmpCodeName"
                        .DataSource = dtTable.DefaultView.ToTable("employeecode", True, "EmpCodeName", "employeeunkid")
                        .DataBind()
                        .SelectedValue = "0"
                    End With

                Else
                    mDicVisibleGrievances = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("visible") = 1).ToDictionary(Function(x) x.Field(Of Integer)("grievancemasterunkid"), Function(y) y.Field(Of Integer)("employeeunkid"))
                    With cboEmpName
                        .DataValueField = "employeeunkid"
                        .DataTextField = "EmpCodeName"
                        .DataSource = dsList.Tables(0).DefaultView.ToTable("employeecode", True, "EmpCodeName", "employeeunkid")
                        .DataBind()
                        .SelectedValue = "0"
                    End With

                End If







                Call cboEmpName_SelectedIndexChanged(sender, e)
                txtLevel.Text = objApprLevel._Levelname
                mintPriority = objApprLevel._Priority
                mintApprovalSetting = objApprover._Approvalsettingid
            Else
                cboEmpName.SelectedValue = CStr(0)
                Call cboEmpName_SelectedIndexChanged(sender, e)
                txtLevel.Text = ""
                cboEmpName.Items.Clear()
                cboEmpName.DataSource = Nothing
                'txtLevel.Attributes.Remove("lvl")
                'drpApprover.Attributes.Remove("set")
                mintPriority = 0
                mintApprovalSetting = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [10-June-2019] -- End

#End Region

#Region " Button's Events "

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSubmit.Click
        Try
            If IsValidate() = False Then Exit Sub
            If cboRepsonseType.SelectedIndex = 2 AndAlso Me.ViewState("dtEmployee") Is Nothing Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "There is no employee(s) for Commitee Members."), Me)
                Exit Sub
            End If
            'Call SetValue()

            If mintResolutionStepTranunkid > 0 AndAlso cboRepsonseType.SelectedValue = Convert.ToString(clsResolution_Step_Tran.enResponseType.My_Response) AndAlso txtQualifingRemark.Text <> "" Then
                If CType(sender, Button).ID.ToUpper() = btnSubmit.ID.ToUpper() Then
                    blnissubmited = True
                Else
                    blnissubmited = False
                End If


                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                Cnf_changeResponse_onedit.Title = Language.getMessage(mstrModuleName, 13, "Confirmation")
                'Gajanan [10-June-2019] -- End

                Cnf_changeResponse_onedit.Message = Language.getMessage(mstrModuleName, 9, "You have changed the response type, due to this previous data will be discarded and new information will be updated. Are you share you want to continue?")
                Cnf_changeResponse_onedit.Show()
                Exit Sub
            End If

            If blnisdeleted Then
                DeleteResolutionEmpReason.Title = Language.getMessage(mstrModuleName, 10, "Enter Reason For Removing Employee")
                DeleteResolutionEmpReason.Show()
                Exit Sub
            End If


            If CType(sender, Button).ID.ToUpper() = btnSubmit.ID.ToUpper() Then
                blnissubmited = True

                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                cnfSubmit.Title = Language.getMessage(mstrModuleName, 13, "Confirmation")
                'Gajanan [10-June-2019] -- End

                cnfSubmit.Message = Language.getMessage(mstrModuleName, 15, "You are about to submit resolution step for the selected grievance,due to this you will not be able to edit or delete the resolution step. Are you sure you want to continue ?")
                cnfSubmit.Show()

            Else
                blnissubmited = False
                Call SaveResolutionData(False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Cnf_changeResponse_onedit_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cnf_changeResponse_onedit.buttonYes_Click
        Try
            blnisdeleted = True
            Dim dvEmployee As DataView = CType(Me.ViewState("dtEmployee"), DataTable).DefaultView
            For i As Integer = 0 To dvEmployee.Count - 1
                Dim gvRow As GridViewRow = dgvEmp.Rows(i)
                Dim dRow() As DataRow = CType(Me.ViewState("dtEmployee"), DataTable).Select("employeecode = '" & gvRow.Cells(1).Text & "'")
                If dRow.Length > 0 Then
                    If mintResolutionStepTranunkid > 0 AndAlso CBool(dRow(0).Item("ischeck")) = True Then
                        dRow(0).Item("AUD") = "D"
                        dRow(0).Item("ischeck") = False

                    End If
                End If
                dvEmployee.Table.AcceptChanges()
            Next
            Me.ViewState("dtEmployee") = dvEmployee.ToTable


            If blnisdeleted Then
                DeleteResolutionEmpReason.Title = Language.getMessage(mstrModuleName, 12, "Enter Reason For Changeing Response Type")
                DeleteResolutionEmpReason.Show()
            End If

            'If blnissubmited Then
            '    cnfSubmit.Show()
            'Else
            '    Call SaveResolutionData(False)
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub DeleteResolutionEmpReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DeleteResolutionEmpReason.buttonDelReasonYes_Click
        Try
            mstrDeletereason = DeleteResolutionEmpReason.Reason
            If blnissubmited Then
                blnissubmited = False

                'Gajanan [10-June-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Changes        
                cnfSubmit.Title = Language.getMessage(mstrModuleName, 13, "Confirmation")
                cnfSubmit.Message = Language.getMessage(mstrModuleName, 15, "You are about to submit resolution step for the selected grievance,due to this you will not be able to edit or delete the resolution step. Are you sure you want to continue ?")
                'Gajanan [10-June-2019] -- End

                cnfSubmit.Show()
            Else
                Call SaveResolutionData(False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub cnfSubmit_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfSubmit.buttonYes_Click
        Try
            Call SaveResolutionData(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Private Sub AddFileToZip(ByVal zipFilename As String, ByVal fileToAdd As String, Optional ByVal compression As CompressionOption = CompressionOption.Normal)
    '    Dim destFilename As String = "/" & Path.GetFileName(fileToAdd)
    '    Using ms As New MemoryStream()
    '        Using pkg As Package = Package.Open(ms, FileMode.Create)
    '            Dim part As PackagePart = pkg.CreatePart(New Uri(destFilename, UriKind.Relative), "", CompressionOption.Normal)
    '            Dim byt As Byte() = File.ReadAllBytes(fileToAdd)

    '            Using fileStream As FileStream = New FileStream(fileToAdd, FileMode.Open)
    '                CopyStream(CType(fileStream, Stream), part.GetStream())
    '            End Using

    '            pkg.Close()
    '        End Using
    '        Response.Clear()
    '        Response.ContentType = "application/zip"
    '        Response.AddHeader("content-disposition", "attachment; filename=test.zip")
    '        Response.OutputStream.Write(ms.GetBuffer(), 0, CInt(ms.Length))
    '        Response.End()

    '    End Using

    'End Sub

    Private Sub AddFileToZip(ByVal zipFilename As String, ByVal fileToAdd As List(Of String), Optional ByVal compression As CompressionOption = CompressionOption.Normal)
        Try   'Hemant (13 Aug 2020)
        Using ms As New MemoryStream()
            Using pkg As Package = Package.Open(ms, FileMode.Create)
                For Each item As String In fileToAdd
                    Dim destFilename As String = "/" & Path.GetFileName(item)
                    Dim part As PackagePart = pkg.CreatePart(New Uri(destFilename, UriKind.Relative), "", CompressionOption.Normal)
                    Dim byt As Byte() = File.ReadAllBytes(item)

                    Using fileStream As FileStream = New FileStream(item, FileMode.Open)
                        CopyStream(CType(fileStream, Stream), part.GetStream())
                    End Using
                Next
                pkg.Close()
            End Using
            Response.Clear()
            Response.ContentType = "application/zip"
            Response.AddHeader("content-disposition", "attachment; filename=" + zipFilename + "")
            Response.OutputStream.Write(ms.GetBuffer(), 0, CInt(ms.Length))
            Response.End()

        End Using
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Private Sub CopyStream(ByVal source As Stream, ByVal target As Stream)
        Try   'Hemant (13 Aug 2020)
        Const bufSize As Integer = (5859 * 1024)
        Dim buf(bufSize - 1) As Byte
        Dim bytesRead As Integer = 0

        bytesRead = source.Read(buf, 0, bufSize)
        Do While bytesRead > 0
            target.Write(buf, 0, bytesRead)
            bytesRead = source.Read(buf, 0, bufSize)
        Loop
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Protected Sub lnkdownloadAttachment_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            Dim xrow() As DataRow
            If CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) > 0 Then
                xrow = mdtGrievanceAttachment.Select("scanattachtranunkid = " & CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) & "")
            Else
                xrow = mdtGrievanceAttachment.Select("GUID = '" & CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("GUID").ToString) & "'")
            End If

            Dim xPath As String = ""

            If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                xPath = xrow(0).Item("filepath").ToString
                xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                If Strings.Left(xPath, 1) <> "/" Then
                    xPath = "~/" & xPath
                Else
                    xPath = "~" & xPath
                End If
                xPath = Server.MapPath(xPath)

            ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                xPath = xrow(0).Item("localpath").ToString
            End If
            If xPath.Trim <> "" Then
                Dim fileInfo As New IO.FileInfo(xPath)
                If fileInfo.Exists = False Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage("File does not Exist...", Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
                fileInfo = Nothing
                Dim strFile As String = xPath

                Response.ContentType = "application/zip"
                Response.AddHeader("Content-Disposition", "attachment;filename=" + row.Cells(GetColumnIndex.getColumnID_Griview(gvGrievanceAttachment, "colhName", False, True)).Text)
                Response.Clear()
                Response.TransmitFile(strFile)
                HttpContext.Current.ApplicationInstance.CompleteRequest()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
    '    Try
    '        Dim btnFlag As Boolean = False

    '        If IsValidate() = False Then Exit Sub

    '        If cboRepsonseType.SelectedIndex = 2 AndAlso Me.ViewState("dtEmployee") Is Nothing Then
    '            Language.setLanguage(mstrModuleName)
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "There is no employee(s) for Commitee Members."), Me)
    '            Exit Sub
    '        End If


    '        Call SetValue()


    '        If mintResolutionStepTranunkid > 0 Then
    '            Dim mstrEmployeeIDs As String
    '            Dim lstEmpoyeeId As List(Of String)
    '            btnFlag = objresolution_step_tran.Update()
    '            If btnFlag = True AndAlso cboRepsonseType.SelectedIndex = 2 Then
    '                dtEmployee = CType(Me.ViewState("dtEmployee"), DataTable)
    '                Dim drRow() As DataRow = dtEmployee.Select("AUD='A'")

    '                lstEmpoyeeId = (From p In drRow Select (p.Item("employeeunkid").ToString)).ToList
    '                If lstEmpoyeeId.Count > 0 Then
    '                    mstrEmployeeIDs = String.Join(",", lstEmpoyeeId.ToArray)
    '                    If Me.ViewState("EmployeeIDs") Is Nothing Then
    '                        Me.ViewState.Add("EmployeeIDs", mstrEmployeeIDs)
    '                    Else
    '                        Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
    '                    End If


    '                    objresolution_step_tran.InsertCommitteeData(mstrEmployeeIDs)
    '                End If

    '                drRow = dtEmployee.Select("AUD='D'")

    '                lstEmpoyeeId = (From p In drRow Select (p.Item("employeeunkid").ToString)).ToList
    '                If lstEmpoyeeId.Count > 0 Then
    '                    mstrEmployeeIDs = String.Join(",", lstEmpoyeeId.ToArray)
    '                    If Me.ViewState("EmployeeIDs") Is Nothing Then
    '                        Me.ViewState.Add("EmployeeIDs", mstrEmployeeIDs)
    '                    Else
    '                        Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
    '                    End If


    '                    objresolution_step_tran.DeleteCommitteeDataByEmployeeId(mstrEmployeeIDs)
    '                End If
    '            Else
    '                dtEmployee = CType(Me.ViewState("dtEmployee"), DataTable)
    '                Dim drRow() As DataRow = dtEmployee.Select("ischeck=true")
    '                lstEmpoyeeId = (From p In drRow Select (p.Item("employeeunkid").ToString)).ToList
    '                If lstEmpoyeeId.Count > 0 Then
    '                    mstrEmployeeIDs = String.Join(",", lstEmpoyeeId.ToArray)
    '                    If Me.ViewState("EmployeeIDs") Is Nothing Then
    '                        Me.ViewState.Add("EmployeeIDs", mstrEmployeeIDs)
    '                    Else
    '                        Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
    '                    End If


    '                    objresolution_step_tran.DeleteCommitteeDataByEmployeeId(mstrEmployeeIDs)
    '                End If
    '            End If

    '        Else
    '            btnFlag = objresolution_step_tran.Insert()
    '            If btnFlag = True AndAlso cboRepsonseType.SelectedIndex = 2 Then
    '                dtEmployee = CType(Me.ViewState("dtEmployee"), DataTable)
    '                Dim drRow() As DataRow = dtEmployee.Select("ischeck=true")

    '                Dim lstEmpoyeeId As List(Of String) = (From p In drRow Select (p.Item("employeeunkid").ToString)).ToList
    '                Dim mstrEmployeeIDs As String = String.Join(",", lstEmpoyeeId.ToArray)
    '                If Me.ViewState("EmployeeIDs") Is Nothing Then
    '                    Me.ViewState.Add("EmployeeIDs", mstrEmployeeIDs)
    '                Else
    '                    Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
    '                End If


    '                objresolution_step_tran.InsertCommitteeData(mstrEmployeeIDs)
    '            End If
    '        End If


    '        mintResolutionStepTranunkid = -1

    '        Response.Redirect(Session("rootpath").ToString & "Grievance/wPg_ResolutionStepList.aspx", False)

    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("btnSubmit_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Request.QueryString.ToString.Length > 0 Then
                Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
            Else
                Response.Redirect(Session("rootpath").ToString & "Grievance/wpg_ResolutionStepList.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btndownloadall_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndownloadall.Click
        Try
            Dim filelist As New List(Of String)
            For Each row As GridViewRow In gvGrievanceAttachment.Rows
                Dim xPath As String = ""
                Dim xrow() As DataRow
                If CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) > 0 Then
                    xrow = mdtGrievanceAttachment.Select("scanattachtranunkid = " & CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("scanattachtranunkid").ToString) & "")
                Else
                    xrow = mdtGrievanceAttachment.Select("GUID = '" & CInt(gvGrievanceAttachment.DataKeys(row.RowIndex)("GUID").ToString) & "'")
                End If

                If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                    xPath = xrow(0).Item("filepath").ToString
                    xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                    If Strings.Left(xPath, 1) <> "/" Then
                        xPath = "~/" & xPath
                    Else
                        xPath = "~" & xPath
                    End If
                    xPath = Server.MapPath(xPath)

                ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                    xPath = xrow(0).Item("localpath").ToString
                End If

                If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                    xPath = xrow(0).Item("filepath").ToString
                    xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                    If Strings.Left(xPath, 1) <> "/" Then
                        xPath = "~/" & xPath
                    Else
                        xPath = "~" & xPath
                    End If
                    xPath = Server.MapPath(xPath)

                ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                    xPath = xrow(0).Item("localpath").ToString
                End If

                If xPath.Trim <> "" Then
                    Dim fileInfo As New IO.FileInfo(xPath)
                    If fileInfo.Exists = False Then
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage("File does not Exist...", Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                    fileInfo = Nothing
                End If
                filelist.Add(xPath)
            Next
            AddFileToZip("Grievance-" + cboEmpName.SelectedValue + "-" + cboRefNo.SelectedItem.Text + ".zip", filelist)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnViewPreviousResolution_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewPreviousResolution.Click
        Try
            Dim objApprover As New clsgrievanceapprover_master

            mblnPreviousResponsePopup = True

            txtPopupReferanceNo.Text = cboRefNo.SelectedItem.ToString()

            popupGrePreviousResponse.Show()

            GvPreviousResponse.DataSource = dtPriviousApproverResolution
            GvPreviousResponse.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnCloseGrePreviousResponse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseGrePreviousResponse.Click
        Try
            mblnPreviousResponsePopup = False
            popupGrePreviousResponse.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " CheckBox Event "

    Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If dgvEmp.Rows.Count <= 0 Then Exit Sub
            Dim dvEmployee As DataView = CType(Me.ViewState("dtEmployee"), DataTable).DefaultView
            For i As Integer = 0 To dgvEmp.Rows.Count - 1
                Dim gvRow As GridViewRow = dgvEmp.Rows(i)
                CType(gvRow.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
                Dim dRow() As DataRow = CType(Me.ViewState("dtEmployee"), DataTable).Select("employeecode = '" & gvRow.Cells(1).Text & "'")
                If dRow.Length > 0 Then
                    dRow(0).Item("ischeck") = cb.Checked
                    If cb.Checked = True Then
                        dRow(0).Item("AUD") = "A"
                    Else
                        If mintResolutionStepTranunkid > 0 Then
                            dRow(0).Item("AUD") = "D"
                            blnisdeleted = True
                        Else
                            dRow(0).Item("AUD") = ""
                        End If

                    End If
                End If
                dvEmployee.Table.AcceptChanges()
            Next
            Me.ViewState("dtEmployee") = dvEmployee.ToTable
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            If gvr.Cells.Count > 0 Then
                Dim dRow() As DataRow = CType(Me.ViewState("dtEmployee"), DataTable).Select("employeecode = '" & gvr.Cells(1).Text & "'")
                If dRow.Length > 0 Then
                    dRow(0).Item("ischeck") = cb.Checked

                    If cb.Checked = True Then
                        dRow(0).Item("AUD") = "A"
                    Else
                        If mintResolutionStepTranunkid > 0 Then
                            dRow(0).Item("AUD") = "D"
                            blnisdeleted = True
                        Else
                            dRow(0).Item("AUD") = ""
                        End If
                    End If
                End If
                CType(Me.ViewState("dtEmployee"), DataTable).AcceptChanges()

                Dim drRow() As DataRow = CType(Me.ViewState("dtEmployee"), DataTable).Select("ischeck = 1")

                If CType(Me.ViewState("dtEmployee"), DataTable).Rows.Count = drRow.Length Then
                    CType(dgvEmp.HeaderRow.FindControl("chkHeder1"), CheckBox).Checked = True
                Else
                    CType(dgvEmp.HeaderRow.FindControl("chkHeder1"), CheckBox).Checked = False
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "TextBox Event"

    Protected Sub TxtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            Dim dvEmployee As DataView = CType(Me.ViewState("dtEmployee"), DataTable).DefaultView



            If dvEmployee IsNot Nothing Then
                If dvEmployee.Table.Rows.Count > 0 Then




                    dvEmployee.RowFilter = "(employeecode like '%" & txtSearch.Text.Trim & "%'  OR employeename like '%" & txtSearch.Text.Trim & "%') and ischeck =false "
                    dgvEmp.DataSource = dvEmployee
                    dgvEmp.DataBind()



                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("TxtSearch_TextChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Gridview Event"

    Protected Sub GvPreviousResponse_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvPreviousResponse.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Gajanan [4-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : Grievance UAT Documnet V4 Changes

                'If e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvPreviousResponse, "colhcommittee", False, True)).Text <> "&nbsp;" Then
                '    e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvPreviousResponse, "colhcommittee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvPreviousResponse, "colhcommittee", False, True)).Text.Replace(",", "<br/>")
                'End If

                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvPreviousResponse, "colhcomiteemember", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvPreviousResponse, "colhcomiteemember", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(GvPreviousResponse, "colhcomiteemember", False, True)).Text.Replace(",", "<br/>")
                End If
                'Gajanan [4-July-2019] -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

#End Region

#Region " Language & UI Settings "

    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)


            Language._Object.setCaption(lblPageHeader.ID, Me.lblPageHeader.Text)
            Language._Object.setCaption(lblDetialHeader.ID, Me.lblDetialHeader.Text)

            Language._Object.setCaption(lblApprover.ID, Me.lblApprover.Text)
            Language._Object.setCaption(lblLevel.ID, Me.lblLevel.Text)
            Language._Object.setCaption(lblGrievanceDate.ID, Me.lblGrievanceDate.Text)
            Language._Object.setCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Language._Object.setCaption(Me.lblRefNo.ID, Me.lblRefNo.Text)
            Language._Object.setCaption(Me.lblGrievanceType.ID, Me.lblGrievanceType.Text)
            Language._Object.setCaption(Me.lblAgainstEmp.ID, Me.lblAgainstEmp.Text)
            Language._Object.setCaption(Me.tbGrievanceDesc.ID, Me.tbGrievanceDesc.HeaderText)
            Language._Object.setCaption(Me.tbReliefReq.ID, Me.tbReliefReq.HeaderText)
            Language._Object.setCaption(Me.TabAttachDocument.ID, Me.TabAttachDocument.HeaderText)
            Language._Object.setCaption(Me.dgvEmp.Columns(1).FooterText, Me.dgvEmp.Columns(1).HeaderText)
            Language._Object.setCaption(Me.dgvEmp.Columns(2).FooterText, Me.dgvEmp.Columns(2).HeaderText)

            Language._Object.setCaption(Me.lblResponseType.ID, Me.lblResponseType.Text)
            Language._Object.setCaption(Me.lblRespnonseRemark.ID, Me.lblRespnonseRemark.Text)
            Language._Object.setCaption(Me.lblQualifingRemark.ID, Me.lblQualifingRemark.Text)
            Language._Object.setCaption(Me.lblMeetingDate.ID, Me.lblMeetingDate.Text)


            Language._Object.setCaption(lblPopupheader.ID, lblPopupheader.Text)
            Language._Object.setCaption(lblPopupReferanceNo.ID, lblPopupReferanceNo.Text)

            Language._Object.setCaption(Me.GvPreviousResponse.Columns(0).FooterText, Me.GvPreviousResponse.Columns(0).HeaderText)
            Language._Object.setCaption(Me.GvPreviousResponse.Columns(1).FooterText, Me.GvPreviousResponse.Columns(1).HeaderText)
            Language._Object.setCaption(Me.GvPreviousResponse.Columns(2).FooterText, Me.GvPreviousResponse.Columns(2).HeaderText)
            Language._Object.setCaption(Me.GvPreviousResponse.Columns(3).FooterText, Me.GvPreviousResponse.Columns(3).HeaderText)
            Language._Object.setCaption(Me.GvPreviousResponse.Columns(4).FooterText, Me.GvPreviousResponse.Columns(4).HeaderText)
            Language._Object.setCaption(Me.GvPreviousResponse.Columns(5).FooterText, Me.GvPreviousResponse.Columns(5).HeaderText)
            Language._Object.setCaption(Me.GvPreviousResponse.Columns(6).FooterText, Me.GvPreviousResponse.Columns(6).HeaderText)
            Language._Object.setCaption(Me.GvPreviousResponse.Columns(7).FooterText, Me.GvPreviousResponse.Columns(7).HeaderText)


            Language._Object.setCaption(Me.btnCloseGrePreviousResponse.ID, Me.btnCloseGrePreviousResponse.Text)

            Language._Object.setCaption(Me.btnSave.ID, Me.btnSave.Text)
            Language._Object.setCaption(Me.btnSubmit.ID, Me.btnSubmit.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)


            'Gajanan [10-July-2019] -- Start      
            Language._Object.setCaption(Me.btnViewPreviousResolution.ID, Me.btnViewPreviousResolution.ToolTip)
            'Gajanan [10-July-2019] -- End

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (22 Nov 2018) -- End

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption(lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.btnSubmit.Text = Language._Object.getCaption(Me.btnSubmit.ID, Me.btnSubmit.Text).Replace("&", "")
            Me.lblApprover.Text = Language._Object.getCaption(lblApprover.ID, Me.lblApprover.Text)
            Me.lblLevel.Text = Language._Object.getCaption(lblLevel.ID, Me.lblLevel.Text)
            Me.lblGrievanceDate.Text = Language._Object.getCaption(lblGrievanceDate.ID, Me.lblGrievanceDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblRefNo.Text = Language._Object.getCaption(lblRefNo.ID, Me.lblRefNo.Text)
            Me.lblGrievanceType.Text = Language._Object.getCaption(lblGrievanceType.ID, Me.lblGrievanceType.Text)
            Me.lblAgainstEmp.Text = Language._Object.getCaption(lblAgainstEmp.ID, Me.lblAgainstEmp.Text)
            Me.tbGrievanceDesc.HeaderText = Language._Object.getCaption(tbGrievanceDesc.ID, Me.tbGrievanceDesc.HeaderText)
            Me.tbReliefReq.HeaderText = Language._Object.getCaption(tbReliefReq.ID, Me.tbReliefReq.HeaderText)
            Me.TabAttachDocument.HeaderText = Language._Object.getCaption(TabAttachDocument.ID, Me.TabAttachDocument.HeaderText)
            Me.dgvEmp.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvEmp.Columns(1).FooterText, Me.dgvEmp.Columns(1).HeaderText)
            Me.dgvEmp.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvEmp.Columns(2).FooterText, Me.dgvEmp.Columns(2).HeaderText)

            Me.lblResponseType.Text = Language._Object.getCaption(lblResponseType.ID, Me.lblResponseType.Text)
            Me.lblRespnonseRemark.Text = Language._Object.getCaption(lblRespnonseRemark.ID, Me.lblRespnonseRemark.Text)
            Me.lblQualifingRemark.Text = Language._Object.getCaption(lblQualifingRemark.ID, Me.lblQualifingRemark.Text)
            Me.lblMeetingDate.Text = Language._Object.getCaption(lblMeetingDate.ID, Me.lblMeetingDate.Text)


            Me.lblPopupheader.Text = Language._Object.getCaption(lblPopupheader.ID, Me.lblPopupheader.Text)
            Me.lblPopupReferanceNo.Text = Language._Object.getCaption(lblPopupReferanceNo.ID, Me.lblPopupReferanceNo.Text)

            Me.GvPreviousResponse.Columns(0).HeaderText = Language._Object.getCaption(Me.GvPreviousResponse.Columns(0).FooterText, Me.GvPreviousResponse.Columns(0).HeaderText)
            Me.GvPreviousResponse.Columns(1).HeaderText = Language._Object.getCaption(Me.GvPreviousResponse.Columns(1).FooterText, Me.GvPreviousResponse.Columns(1).HeaderText)
            Me.GvPreviousResponse.Columns(2).HeaderText = Language._Object.getCaption(Me.GvPreviousResponse.Columns(2).FooterText, Me.GvPreviousResponse.Columns(2).HeaderText)
            Me.GvPreviousResponse.Columns(3).HeaderText = Language._Object.getCaption(Me.GvPreviousResponse.Columns(3).FooterText, Me.GvPreviousResponse.Columns(3).HeaderText)
            Me.GvPreviousResponse.Columns(4).HeaderText = Language._Object.getCaption(Me.GvPreviousResponse.Columns(4).FooterText, Me.GvPreviousResponse.Columns(4).HeaderText)
            Me.GvPreviousResponse.Columns(5).HeaderText = Language._Object.getCaption(Me.GvPreviousResponse.Columns(5).FooterText, Me.GvPreviousResponse.Columns(5).HeaderText)
            Me.GvPreviousResponse.Columns(6).HeaderText = Language._Object.getCaption(Me.GvPreviousResponse.Columns(6).FooterText, Me.GvPreviousResponse.Columns(6).HeaderText)
            Me.GvPreviousResponse.Columns(7).HeaderText = Language._Object.getCaption(Me.GvPreviousResponse.Columns(7).FooterText, Me.GvPreviousResponse.Columns(7).HeaderText)

            Me.btnCloseGrePreviousResponse.Text = Language._Object.getCaption(Me.btnCloseGrePreviousResponse.ID, Me.btnCloseGrePreviousResponse.Text).Replace("&", "")

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnSubmit.Text = Language._Object.getCaption(Me.btnSubmit.ID, Me.btnSubmit.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            'Gajanan [10-July-2019] -- Start      
            Me.btnViewPreviousResolution.ToolTip = Language._Object.getCaption(Me.btnViewPreviousResolution.ID, Me.btnViewPreviousResolution.ToolTip)
            'Gajanan [10-July-2019] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Ref No is compulsory information. Please select Ref No to continue.")
            Language.setMessage(mstrModuleName, 3, "Repsonse Type is compulsory information. Please select Repsonse Type to continue.")
            Language.setMessage(mstrModuleName, 4, "Respnonse Remark is compulsory information. Please Enter Respnonse Remark to continue.")
            Language.setMessage(mstrModuleName, 5, "Meeting Date is compulsory information. Please Select Meeting Date to continue.")
            Language.setMessage(mstrModuleName, 6, "Qualification Remark is compulsory information. Please Enter Qualification Remark to continue.")
            Language.setMessage(mstrModuleName, 7, "Committee Members are compulsory information. Please Select Committee Members to continue.")
            Language.setMessage(mstrModuleName, 8, "Meeting Date Should Not Greater Than Today's Date.")
            Language.setMessage(mstrModuleName, 9, "You have changed the response type, due to this previous data will be discarded and new information will be updated. Are you share you want to continue?")
            Language.setMessage(mstrModuleName, 10, "Enter Reason For Removing Employee")
            Language.setMessage(mstrModuleName, 12, "Enter Reason For Changeing Response Type")
            Language.setMessage(mstrModuleName, 13, "Confirmation")
            Language.setMessage(mstrModuleName, 14, "There is no employee(s) for Commitee Members.")
            Language.setMessage(mstrModuleName, 15, "You are about to submit resolution step for the selected grievance,due to this you will not be able to edit or delete the resolution step. Are you sure you want to continue ?")
            Language.setMessage(mstrModuleName, 16, "Meeting Date Should Not Less Than Grievance Date.")
            Language.setMessage(mstrModuleName, 17, "Sorry,you have already given resolution for this selected grievance.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
