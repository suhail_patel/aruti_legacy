﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="GrievanceInitiatorResponseList.aspx.vb" Inherits="Grievance_GrievanceInitiatorResponseList" title="Initiator Response List" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

   
    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Initialtor Response List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 12%">
                                            <asp:Label ID="lblAgainstEmp" runat="server" Text="Against Employee"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                           <asp:DropDownList ID="drpAgainstEmp" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblRefno" runat="server" Text="Ref No."></asp:Label>
                                            
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="drpRefno" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                      
                                    </div>
                                    
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-body-default" style="position: relative">
                                    <div class="row2">
                                        <div style="width: 100%">
                                            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Height="250px">
                                                <asp:GridView ID="GvIntiatorResponse" runat="server" AutoGenerateColumns="false"
                                                    AllowPaging="false" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                    RowStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false" DataKeyNames="initiatortranunkid,resolutionsteptranunkid,responsetypeunkid,issubmitted,priority">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                            FooterText="btnEdit">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <%--<asp:LinkButton ID="ImgSelect" runat="server" ToolTip="View/ Edit Response" CommandName="View"
                                                                        CommandArgument="<%# Container.DataItemIndex %>">
                                                                        <i class="fa fa-comment" style="color:#5bace4"></i>
                                                                    </asp:LinkButton>--%>
                                                                    
                                                                   <asp:LinkButton ID="ImgSelect" runat="server" ToolTip="View/ Edit Response" CommandArgument="<%# Container.DataItemIndex %>"
                                                                        OnClick="lnkView_Click"> 
                                                                        <i class="fa fa-comment" style="color:#5bace4"></i>
                                                                        </asp:LinkButton>
                                                                    
                                                                </span>
                                                                
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="grievancetype" HeaderText="Grievance Type" ReadOnly="True"
                                                            FooterText="colhGrievanceType" />
                                                        <asp:BoundField DataField="againstEmployee" HeaderText="Against Employee" ReadOnly="True"
                                                            FooterText="colhEmployee" Visible="false" />
                                                        <asp:BoundField DataField="grievancerefno" HeaderText="Ref No" ReadOnly="True" FooterText="colhRefNo"
                                                            Visible="false" />
                                                        <asp:BoundField DataField="grievancedate" HeaderText="Meeting Date" ReadOnly="True"
                                                            FooterText="colhMeetingDate" />
                                                        <asp:BoundField DataField="responseremark" HeaderText="Resolution Remark" ReadOnly="True"
                                                            FooterText="colhResolutionRemark" />
                                                        <asp:BoundField DataField="responsetype" HeaderText="Resolution Type" ReadOnly="True"
                                                            FooterText="colhResolutiontype" />
                                                        <asp:BoundField DataField="remark" HeaderText="Your Response" ReadOnly="True" FooterText="colhEmployeeRemark" />
                                                        <asp:BoundField DataField="levelname" HeaderText="Approverlevel" ReadOnly="True"
                                                            FooterText="colhApproverlevel" />
                                                            <asp:BoundField DataField="approvername" HeaderText="Approver Name" ReadOnly="True"
                                                            FooterText="colhApprovername" />
                                                            
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnClose" runat="server" CssClass="btnDefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc10:DeleteReason ID="popupDelete" runat="server" Title="Are you sure you want to delete this Resolution Step?" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

