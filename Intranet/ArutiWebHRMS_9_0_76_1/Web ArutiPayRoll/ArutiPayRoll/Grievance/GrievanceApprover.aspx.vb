﻿Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing

Partial Class Grievance_GrievanceApprover
    Inherits Basepage

#Region "Private Variable"
    Private DisplayMessage As New CommonCodes
    Private objgrievanceapprover_master As New clsgrievanceapprover_master
    Private objGrievanceApproverTran As New clsgrievanceapprover_Tran
    Private ReadOnly mstrModuleName As String = "frmGrievanceApproverList"
    Private ReadOnly mstrModuleName1 As String = "frmGrievanceApproverAddedit"
    Private mstrEmployeeIDs As String = ""
    'Hemant (29 May 2019) -- Start
    'ISSUE/ENHANCEMENT : UAT Changes
    Dim dtEmployee As DataTable = Nothing
    Private mstrAdvanceFilter As String = String.Empty
    'Hemant (29 May 2019) -- End
    Dim blnpopupApproverEmpMapping As Boolean = False
    Dim blnpopupApproverUseraccess As Boolean = False

    Private mdtTran As DataTable
    Private mintApprovermasterunkid As Integer
    Private mdtApproverList As DataTable
    Dim DtPopupAddEdit As DataTable
    Dim DtPopupAddeditSelectEmpList As DataTable
    Private currentId As String = ""
    Private Index As Integer
    Private mintActiveDeactiveApprId As Integer = 0
    Private mblActiveDeactiveApprStatus As Boolean
#End Region

#Region "Form Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try   'Hemant (13 Aug 2020)
        If Session("clsuser") Is Nothing Then
            Exit Sub
        End If

        If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
            DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            Exit Sub
        End If

        If IsPostBack = False Then
            'Sohail (22 Nov 2018) -- Start
            'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()

              
            End If
            'Sohail (22 Nov 2018) -- End

            ListFillCombo()
            FillList(True)
            SetVisibility()
            mdtTran = objGrievanceApproverTran._DataList
        Else

            mintActiveDeactiveApprId = CInt(ViewState("mintActiveDeactiveApprId"))
            mblActiveDeactiveApprStatus = CBool(ViewState("mblActiveDeactiveApprStatus"))
            blnpopupApproverEmpMapping = CBool(ViewState("blnpopupApproverEmpMapping"))
            blnpopupApproverUseraccess = ViewState("blnpopupApproverUseraccess")
            If ViewState("EmpList") IsNot Nothing Then
                DtPopupAddEdit = CType(ViewState("EmpList"), DataTable)
            End If

            If ViewState("mdtTran") IsNot Nothing Then
                mdtTran = CType(ViewState("mdtTran"), DataTable)
            End If

            If ViewState("mintApprovermasterunkid") IsNot Nothing Then
                mintApprovermasterunkid = CType(ViewState("mintApprovermasterunkid"), Integer)
            End If

            'Hemant (29 May 2019) -- Start
            'ISSUE/ENHANCEMENT : UAT Changes
            dtEmployee = CType(Me.ViewState("dvEmployee"), DataTable)
            mstrAdvanceFilter = CStr(Me.ViewState("AdvanceFilter"))
            'Hemant (29 May 2019) -- End

            If ViewState("mdtApproverList") IsNot Nothing Then
                mdtApproverList = CType(ViewState("mdtApproverList"), DataTable)
                If mdtApproverList.Rows.Count > 0 AndAlso CInt(mdtApproverList.Rows(0)("approvermasterunkid").ToString) > 0 Then
                    gvApproverList.DataSource = mdtApproverList
                    gvApproverList.DataBind()
                End If
            End If
            If blnpopupApproverEmpMapping Then
                popupGreAddEditApproverEmpMapping.Show()
            End If

            If blnpopupApproverUseraccess Then
                popupApproverUseraccess.Show()
            End If

        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("blnpopupApproverEmpMapping") = blnpopupApproverEmpMapping
            ViewState("blnpopupApproverUseraccess") = blnpopupApproverUseraccess
            ViewState("EmpList") = DtPopupAddEdit
            ViewState("mdtTran") = mdtTran
            ViewState("mdtApproverList") = mdtApproverList
            ViewState("mintApprovermasterunkid") = mintApprovermasterunkid

            ViewState("mintActiveDeactiveApprId") = mintActiveDeactiveApprId
            ViewState("mblActiveDeactiveApprStatus") = mblActiveDeactiveApprStatus

            'Hemant (29 May 2019) -- Start
            'ISSUE/ENHANCEMENT : UAT Changes
            Me.ViewState("dvEmployee") = dtEmployee
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
            'Hemant (29 May 2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "For List"


#Region "Private Function"

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsApproverList As New DataSet
        Dim dtApprover As DataTable
        Dim strSearching As String = ""
        Try
            If CInt(drpLevel.SelectedValue) > 0 Then
                strSearching &= "AND greapprover_master.apprlevelunkid = " & CInt(drpLevel.SelectedValue) & " "
            End If

            If CInt(drpApprover.SelectedValue) > 0 Then
                strSearching &= "AND greapprover_master.approverempid = " & CInt(drpApprover.SelectedValue) & " "
            End If

            If isblank Or CBool(Session("AllowToViewGrievanceApprover")) = False Then
                strSearching = "and 1 = 2 "
            End If

            dsApproverList = objgrievanceapprover_master.GetList("List", _
                                                   Session("Database_Name").ToString(), _
                                                   CInt(Session("UserId")), _
                                                   CInt(Session("Fin_year")), _
                                                   CInt(Session("CompanyUnkId")), _
                                                   Session("EmployeeAsOnDate").ToString(), _
                                                   Session("UserAccessModeSetting").ToString(), True, _
                                                   True, _
                                                   CBool(IIf(CInt(drpStatus.SelectedValue) = 1, True, False)), False, -1, Nothing, strSearching, "", 0, False, False, Convert.ToInt32(Session("GrievanceApprovalSetting").ToString))

            If dsApproverList.Tables(0).Rows.Count = 0 Then
                dsApproverList = objgrievanceapprover_master.GetList("List", _
                                                   Session("Database_Name").ToString(), _
                                                   CInt(Session("UserId")), _
                                                   CInt(Session("Fin_year")), _
                                                   CInt(Session("CompanyUnkId")), _
                                                   Session("EmployeeAsOnDate").ToString(), _
                                                   Session("UserAccessModeSetting").ToString(), True, _
                                                   True, _
                                                   CBool(IIf(CInt(drpStatus.SelectedValue) = 1, True, False)), False, -1, Nothing, "and 1=2", "", 0, False, True, Convert.ToInt32(Session("GrievanceApprovalSetting").ToString))
                isblank = True
            End If

            If dsApproverList IsNot Nothing Then
                dtApprover = New DataView(dsApproverList.Tables("List"), "", "levelname", DataViewRowState.CurrentRows).ToTable()

                Dim strLeaveName As String = ""
                Dim dtTable As DataTable = dtApprover.Clone
                dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))
                Dim dtRow As DataRow = Nothing
                For Each drow As DataRow In dtApprover.Rows
                    If CStr(drow("levelname")).Trim <> strLeaveName.Trim Then
                        dtRow = dtTable.NewRow
                        dtRow("IsGrp") = True
                        dtRow("approvermasterunkid") = drow("approvermasterunkid")
                        dtRow("levelname") = drow("levelname")
                        strLeaveName = drow("levelname").ToString()
                        dtTable.Rows.Add(dtRow)
                    End If
                    dtRow = dtTable.NewRow
                    For Each dtcol As DataColumn In dtApprover.Columns
                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next

                dtTable.AcceptChanges()
                gvApproverList.DataSource = dtTable
                gvApproverList.DataBind()
                mdtApproverList = dtTable
                If isblank = True Then
                    gvApproverList.Rows(0).Visible = False
                End If

            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
    Private Sub SetVisibility()
        Try
            btnNew.Visible = CBool(Session("AllowToAddGrievanceApprover"))

            'Session("AllowToAddGrievanceApprover") = objUserPrivilege._AllowToAddGrievanceApprover
            'Session("AllowToDeleteGrievanceApprover") = objUserPrivilege._AllowToDeleteGrievanceApprover
            'Session("AllowToViewGrievanceApprover") = objUserPrivilege._AllowToViewGrievanceApprover
            'Session("AllowToActivateGrievanceApprover") = objUserPrivilege._AllowToActivateGrievanceApprover
            'Session("AllowToInActivateGrievanceApprover") = objUserPrivilege._AllowToInActivateGrievanceApprover

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try


    End Sub
    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            rw.Visible = False
            Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
            row.BackColor = ColorTranslator.FromHtml("#DDD")
            Dim cell As TableCell = New TableCell()
            cell.Text = title
            cell.ColumnSpan = gd.Columns.Count
            row.Cells.Add(cell)
            gd.Controls(0).Controls.Add(row)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
    Private Sub ListFillCombo()
        Try
            Dim objMaster As New clsMasterData
            Dim dsCombo As DataSet = objMaster.getComboListTranHeadActiveInActive("List", False)
            With drpStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "1"
            End With
            'Call cboStatus_SelectedIndexChanged(cboStatus, Nothing)

            Dim objLevel As New clsGrievanceApproverLevel
            Dim dsList As DataSet = objLevel.getListForCombo(Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()), "List", True)
            drpLevel.DataTextField = "name"
            drpLevel.DataValueField = "apprlevelunkid"
            drpLevel.DataSource = dsList.Tables(0)
            drpLevel.DataBind()

            Dim objApprover As New clsgrievanceapprover_master
            Dim dsApprList As DataSet

            'S.SANDEEP |30-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Grievance UAT]
            'dsApprList = objApprover.GetList("List", _
            '                                 Session("Database_Name").ToString(), _
            '                                 CInt(Session("UserId")), _
            '                                 CInt(Session("Fin_year")), _
            '                                 CInt(Session("CompanyUnkId")), _
            '                                 Session("EmployeeAsOnDate").ToString(), _
            '                                 Session("UserAccessModeSetting").ToString(), True, _
            '                                 True, True)
            dsApprList = objApprover.GetList("List", _
                                                 Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 Session("EmployeeAsOnDate").ToString(), _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                             True, True, , , , , , , , , Convert.ToInt32(Session("GrievanceApprovalSetting").ToString))
            'S.SANDEEP |30-MAY-2019| -- END

            If dsApprList Is Nothing Then Exit Sub
            Dim dtTable As DataTable = Nothing
            dtTable = dsApprList.Tables(0).DefaultView.ToTable(True, "approverempid", "name", "isexternal")

            Dim dr As DataRow = dtTable.NewRow
            dr("approverempid") = 0
            dr("name") = "Select"
            dr("isexternal") = False
            dtTable.Rows.InsertAt(dr, 0)

            drpApprover.DataTextField = "name"
            drpApprover.DataValueField = "approverempid"
            drpApprover.DataSource = dtTable
            drpApprover.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub SetValueToPopup()
        Try
            If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
                drpApproverUseraccess_level.SelectedValue = objgrievanceapprover_master._Apprlevelunkid
                drpApproverUseraccess_user.SelectedValue = objgrievanceapprover_master._Mapuserunkid
                drpApproverUseraccess_user_SelectedIndexChanged(Nothing, Nothing)

            ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then

                If objgrievanceapprover_master._Isexternal Then
                    chkAddEditExtApprover.Checked = True
                    chkAddEditExtApprover_CheckedChanged(Nothing, Nothing)
                Else
                    chkAddEditExtApprover.Checked = False
                    chkAddEditExtApprover_CheckedChanged(Nothing, Nothing)
                End If

                drpAddEditApproverName.SelectedValue = objgrievanceapprover_master._Approverempid
                drpAddEditApproverName.Enabled = False
                drpAddEditApprovalLevel.SelectedValue = objgrievanceapprover_master._Apprlevelunkid
                drpAddEditApprovalLevel.Enabled = False
                drpAddEditUser.SelectedValue = objgrievanceapprover_master._Mapuserunkid
                chkAddEditExtApprover.Checked = objgrievanceapprover_master._Isexternal
                chkAddEditExtApprover.Enabled = False

                drpAddEditApproverName_SelectedIndexChanged(Nothing, Nothing)
                mdtTran = objGrievanceApproverTran._DataList

                GvApproverEmpMapping.DataSource = mdtTran
                GvApproverEmpMapping.DataBind()

            End If




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button Event"

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            FillAddEditCombo()
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            objgrievanceapprover_master._Approvermasterunkid = lnkedit.CommandArgument.ToString()
            objGrievanceApproverTran._Approvermasterunkid = lnkedit.CommandArgument.ToString()
            objGrievanceApproverTran.GetApproverTran(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, objgrievanceapprover_master._Approvermasterunkid)



            mintApprovermasterunkid = lnkedit.CommandArgument.ToString()

            SetValueToPopup()

            If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
                popupApproverUseraccess.Show()
                blnpopupApproverUseraccess = True

            ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
                blnpopupApproverEmpMapping = True
                popupGreAddEditApproverEmpMapping.Show()
            End If




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            objgrievanceapprover_master._Approvermasterunkid = CInt(lnkdelete.CommandArgument.ToString())
            mintApprovermasterunkid = CInt(lnkdelete.CommandArgument.ToString())

            If objgrievanceapprover_master.isUsed(CInt(lnkdelete.CommandArgument.ToString())) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, You cannot delete this Approver . Reason: This Approver is in use."), Me)
                Exit Sub
            End If


            confirmapproverdelete.Show()
            confirmapproverdelete.Title = Language.getMessage(mstrModuleName, 6, "Confirmation")
            confirmapproverdelete.Message = Language.getMessage(mstrModuleName, 7, "Are You Sure You Want To Delete This Approver ?")

            'SetAtValue()

            'If objgrievanceapprover_master.Delete(CInt(lnkdelete.CommandArgument.ToString()), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) Then
            '    DisplayMessage.DisplayMessage(objgrievanceapprover_master._Message, Me)
            '    Exit Sub
            'End If





        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkActive_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkActive As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkActive).NamingContainer, GridViewRow)
            Dim blnFlag As Boolean = False

            mintActiveDeactiveApprId = CInt(lnkActive.CommandArgument)
            mblActiveDeactiveApprStatus = True

            objgrievanceapprover_master._Approvermasterunkid = CInt(lnkActive.CommandArgument.ToString())


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
            popupconfirmActiveAppr.Title = Language.getMessage(mstrModuleName, 9, "Confirmation")
            popupconfirmActiveAppr.Message = Language.getMessage(mstrModuleName, 11, "Are You Sure You Want To Active This Approver?")
            'Gajanan [10-June-2019] -- End

            popupconfirmActiveAppr.Show()


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDeActive_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkDeActive As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkDeActive).NamingContainer, GridViewRow)

            mintActiveDeactiveApprId = CInt(lnkDeActive.CommandArgument)
            mblActiveDeactiveApprStatus = False

            objgrievanceapprover_master._Approvermasterunkid = CInt(lnkDeActive.CommandArgument)

            If objgrievanceapprover_master.isApproverInGrievanceUsed(objgrievanceapprover_master._Approvermasterunkid) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver . Reason: This Approver is in use."), Me)
                Exit Sub
            End If

            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
            popupconfirmDeactiveAppr.Title = Language.getMessage(mstrModuleName, 9, "Confirmation")
            popupconfirmDeactiveAppr.Message = Language.getMessage(mstrModuleName, 10, "Are You Sure You Want To Deactive  This Approver?")
            'Gajanan [10-June-2019] -- End
            popupconfirmDeactiveAppr.Show()




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub



    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            FillAddEditCombo()
            mintApprovermasterunkid = 0

            If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
                blnpopupApproverUseraccess = True
                popupApproverUseraccess.Show()

            ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
                blnpopupApproverEmpMapping = True
                popupGreAddEditApproverEmpMapping.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupconfirmActiveAppr_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmActiveAppr.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False
            objgrievanceapprover_master._Approvalsettingid = CInt(Session("GrievanceApprovalSetting").ToString)
            objgrievanceapprover_master._FormName = mstrModuleName

            blnFlag = objgrievanceapprover_master.ActiveDeactiveEmployee(mintActiveDeactiveApprId, True)

            If blnFlag = False And objgrievanceapprover_master._Message <> "" Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayMessage("popupconfirmActiveAppr_buttonYes_Click :- " & objgrievanceapprover_master._Message, Me)
                DisplayMessage.DisplayMessage(objgrievanceapprover_master._Message, Me)
                'Sohail (23 Mar 2019) -- End
            Else
                FillList(False)
                mintApprovermasterunkid = 0
                mintActiveDeactiveApprId = 0
                mblActiveDeactiveApprStatus = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupconfirmDeactiveAppr_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmDeactiveAppr.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False
            objgrievanceapprover_master._Approvalsettingid = CInt(Session("GrievanceApprovalSetting").ToString)
            objgrievanceapprover_master._FormName = mstrModuleName
            blnFlag = objgrievanceapprover_master.ActiveDeactiveEmployee(mintActiveDeactiveApprId, False)

            If blnFlag = False And objgrievanceapprover_master._Message <> "" Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayMessage("popupconfirmDeactiveAppr_buttonYes_Click :- " & objgrievanceapprover_master._Message, Me)
                DisplayMessage.DisplayMessage(objgrievanceapprover_master._Message, Me)
                'Sohail (23 Mar 2019) -- End
            Else
                FillList(False)
                mintApprovermasterunkid = 0
                mintActiveDeactiveApprId = 0
                mblActiveDeactiveApprStatus = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub DeleteApprovalReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DeleteApprovalReason.buttonDelReasonYes_Click
        Try
            SetAtValue()
            objgrievanceapprover_master._Approvalsettingid = CInt(Session("GrievanceApprovalSetting").ToString)
            objgrievanceapprover_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objgrievanceapprover_master._Voidreason = DeleteApprovalReason.Reason
            objgrievanceapprover_master._FormName = mstrModuleName

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objgrievanceapprover_master._Voiduserunkid = CInt(Session("UserId"))
            End If

            If objgrievanceapprover_master.Delete(CInt(mintApprovermasterunkid), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = False Then
                DisplayMessage.DisplayMessage(objgrievanceapprover_master._Message, Me)
                Exit Sub
            End If
            FillList(False)
            mintApprovermasterunkid = 0

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
    'Protected Sub gvApproverList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApproverList.RowCreated
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then

    '            Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table
    '            If dt.Rows.Count > 0 AndAlso dt.Rows(e.Row.RowIndex)(0).ToString() <> "" Then
    '                Dim oldid As String = dt.Rows(e.Row.RowIndex)("LevelName").ToString()
    '                If dt.Rows(e.Row.RowIndex)("IsGrp").ToString() = True AndAlso oldid <> currentId Then
    '                    Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("LevelName").ToString(), gvApproverList)
    '                    currentId = oldid
    '                End If


    '                'If Index = 0 Then
    '                '    Me.AddGroup(dt.Rows(e.Row.RowIndex)("LevelName").ToString(), gvApproverList)
    '                '    Index += 1
    '                '    currentId = oldid
    '                'ElseIf Index > 0 Then

    '                '    If oldid <> currentId Then

    '                '        If e.Row.RowIndex > 0 Then
    '                '            Me.AddGroup(dt.Rows(e.Row.RowIndex)("LevelName").ToString(), gvApproverList)
    '                '            Index += 1
    '                '        End If

    '                '        currentId = oldid
    '                '    End If
    '                'End If
    '            End If
    '        End If


    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub






    'Protected Sub drpLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpLevel.SelectedIndexChanged
    '    Try
    '        Try
    '            Dim objApprover As New clsgrievanceapprover_master

    '            Dim dsList As DataSet = Nothing

    '            If CInt(drpStatus.SelectedValue) = 1 Then
    '                dsList = objApprover.GetList("List", _
    '                                             Session("Database_Name").ToString(), _
    '                                             CInt(Session("UserId")), _
    '                                             CInt(Session("Fin_year")), _
    '                                             CInt(Session("CompanyUnkId")), _
    '                                             Session("EmployeeAsOnDate").ToString(), _
    '                                             Session("UserAccessModeSetting").ToString(), True, _
    '                                             True, True)
    '            ElseIf CInt(drpStatus.SelectedValue) = 2 Then
    '                dsList = objApprover.GetList("List", _
    '                                             Session("Database_Name").ToString(), _
    '                                             CInt(Session("UserId")), _
    '                                             CInt(Session("Fin_year")), _
    '                                             CInt(Session("CompanyUnkId")), _
    '                                             Session("EmployeeAsOnDate").ToString(), _
    '                                             Session("UserAccessModeSetting").ToString(), True, _
    '                                             True, False)
    '            End If

    '            If dsList Is Nothing Then Exit Sub
    '            Dim dtTable As DataTable = Nothing
    '            If CInt(drpLevel.SelectedValue) > 0 Then
    '                dtTable = New DataView(dsList.Tables(0), "apprlevelunkid = " & CInt(drpLevel.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
    '            Else
    '                dtTable = dsList.Tables(0).DefaultView.ToTable(True, "approverempid", "name", "isexternal")
    '            End If

    '            Dim dr As DataRow = dtTable.NewRow
    '            dr("approverempid") = 0
    '            dr("name") = "Select"
    '            dr("isexternal") = False
    '            dtTable.Rows.InsertAt(dr, 0)

    '            drpApprover.DataTextField = "name"
    '            drpApprover.DataValueField = "approverempid"
    '            drpApprover.DataSource = dtTable
    '            drpApprover.DataBind()
    '        Catch ex As Exception
    '            DisplayMessage.DisplayMessage("cboLevel_SelectedIndexChanged :- " & ex.Message, Me)
    '        End Try
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
#End Region

#Region "For AddEdit Approver"

#Region "Private Function"
    Dim objEmployee As New clsEmployee_Master
    Dim objGrievanceApproverLevel As New clsGrievanceApproverLevel
    Dim objUser As New clsUserAddEdit
    Dim dsList As DataSet

    Private Sub FillAddEditCombo()
        Try
            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            Dim objLevel As New clsGrievanceApproverLevel

            If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then

                dsList = objLevel.getListForCombo(Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()), "List", True)
                drpApproverUseraccess_level.DataSource = dsList
                drpApproverUseraccess_level.DataTextField = "name"
                drpApproverUseraccess_level.DataValueField = "apprlevelunkid"
                drpApproverUseraccess_level.DataBind()

                dsList = Nothing
                dsList = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), CStr(1212), CInt(Session("Fin_year")), True)
                drpApproverUseraccess_user.DataSource = dsList.Tables("User")
                drpApproverUseraccess_user.DataTextField = "name"
                drpApproverUseraccess_user.DataValueField = "userunkid"
                drpApproverUseraccess_user.DataBind()

            ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
                If chkAddEditExtApprover.Checked = False Then
                    dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         Session("UserAccessModeSetting").ToString(), True, _
                                                         CBool(Session("IsIncludeInactiveEmp")), "Employee", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)
                    drpAddEditApproverName.DataSource = dsList
                    drpAddEditApproverName.DataTextField = "employeename"
                    drpAddEditApproverName.DataValueField = "employeeunkid"
                    drpAddEditApproverName.DataBind()
                Else
                    Dim objUser As New clsUserAddEdit

                    dsList = objUser.GetExternalApproverList("List", _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, "1212")

                    drpAddEditApproverName.DataSource = dsList.Tables("List")
                    drpAddEditApproverName.DataTextField = "Name"
                    drpAddEditApproverName.DataValueField = "userunkid"
                    drpAddEditApproverName.DataBind()
                End If

                dsList = Nothing
                dsList = objLevel.getListForCombo(Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()), "List", True)
                drpAddEditApprovalLevel.DataSource = dsList
                drpAddEditApprovalLevel.DataTextField = "name"
                drpAddEditApprovalLevel.DataValueField = "apprlevelunkid"
                drpAddEditApprovalLevel.DataBind()

                dsList = Nothing
                dsList = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), CStr(1212), CInt(Session("Fin_year")), True)
                drpAddEditUser.DataSource = dsList.Tables("User")
                drpAddEditUser.DataTextField = "name"
                drpAddEditUser.DataValueField = "userunkid"
                drpAddEditUser.DataBind()
            End If



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillEmployeeList()
        Dim dsEmployee As DataSet = Nothing
        'Hemant (29 May 2019) -- Start
        'ISSUE/ENHANCEMENT : UAT Changes
        'Dim dtEmployee As DataTable = Nothing
        'Hemant (29 May 2019) -- End
        Dim strSearch As String = String.Empty
        Try
            Dim objEmployee As New clsEmployee_Master

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Trim.Substring(3)
            End If

            Dim blnInActiveEmp As Boolean = False

            If chkAddEditExtApprover.Checked = False Then
                If drpAddEditApproverName.SelectedValue > 0 Then
                    strSearch &= "AND employeeunkid <> " & CInt(drpAddEditApproverName.SelectedValue) & " "
                End If
            End If

            If mstrEmployeeIDs.Trim.Length > 0 Then
                strSearch &= "AND employeeunkid not in ( " & mstrEmployeeIDs.Trim & " )"
            End If

            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Trim.Substring(3)
            End If

            'dsEmployee = objEmployee.GetList(Session("Database_Name").ToString(), _
            '                                CInt(Session("UserId")), _
            '                                CInt(Session("Fin_year")), _
            '                                CInt(Session("CompanyUnkId")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                Session("UserAccessModeSetting").ToString(), True, _
            '                                blnInActiveEmp, _
            '                                "Employee", _
            '                               CBool(Session("ShowFirstAppointmentDate")), , , , False, False)

            Dim strfield As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job

            dsEmployee = objEmployee.GetListForDynamicField(strfield, Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                            blnInActiveEmp, _
                                         "Employee", -1, False, "", CBool(Session("ShowFirstAppointmentDate")), False, False, True)

            dsEmployee.Tables(0).Columns(1).ColumnName = "employeecode"
            dsEmployee.Tables(0).Columns(2).ColumnName = "name"

            dsEmployee.Tables(0).Columns(3).ColumnName = "departmentname"
            dsEmployee.Tables(0).Columns(4).ColumnName = "jobname"


            If dsEmployee.Tables(0).Columns.Contains("IsCheck") = False Then
                Dim dccolumn As New DataColumn("IsCheck")
                dccolumn.DataType = Type.GetType("System.Boolean")
                dccolumn.DefaultValue = False

                dsEmployee.Tables(0).Columns.Add(dccolumn)
            End If

            dtEmployee = New DataView(dsEmployee.Tables(0), strSearch, "", DataViewRowState.CurrentRows).ToTable
            DtPopupAddEdit = dtEmployee

            If dtEmployee.Rows.Count <= 0 Then
                dtEmployee.Rows.Add(dtEmployee.NewRow())
                gvAddEditEmployee.DataSource = dtEmployee
                gvAddEditEmployee.DataBind()
                gvAddEditEmployee.Rows(0).Visible = False
            Else
                gvAddEditEmployee.DataSource = dtEmployee
                gvAddEditEmployee.DataBind()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
                If drpApproverUseraccess_level.SelectedValue = 0 Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage("Approver Level cannot be blank. Approver Level is required information.", Me)
                    'Sohail (23 Mar 2019) -- End
                    Return False
                ElseIf drpApproverUseraccess_user.SelectedValue = 0 Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage("Approver User cannot be blank. Approver User is required information.", Me)
                    'Sohail (23 Mar 2019) -- End
                    Return False
                End If

            ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
                If drpAddEditApproverName.SelectedValue = 0 Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage("Approver Name cannot be blank. Approver Name is required information.", Me)
                    'Sohail (23 Mar 2019) -- End
                    Return False
                ElseIf drpAddEditApprovalLevel.SelectedValue = 0 Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage("Approver Level is compulsory information.Please Select Approver Level.", Me)
                    'Sohail (23 Mar 2019) -- End
                    Return False

                ElseIf drpAddEditUser.SelectedValue = 0 And Not chkAddEditExtApprover.Checked Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage("Approver User is compulsory information.Please Select Approver User.", Me)
                    'Sohail (23 Mar 2019) -- End
                    Return False
                End If

            End If



            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    Private Sub SelectedEmplyeeList()
        Dim strSearch As String = String.Empty
        Dim dtEmployee As DataTable = Nothing
        Try
            If mdtTran.Rows.Count = 0 Then Exit Sub
            If Session("ApproverId") IsNot Nothing AndAlso mdtTran.Columns.Contains("IsCheck") = False Then
                mdtTran.Columns.Add("IsCheck", Type.GetType("System.Boolean"))
                mdtTran.Columns("IsCheck").DefaultValue = False
            End If
            strSearch = "AND AUD <> 'D' "

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
                dtEmployee = New DataView(mdtTran, strSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtEmployee = mdtTran
            End If

            If dtEmployee.Rows.Count <= 0 Then
                dtEmployee.Rows.Add(dtEmployee.NewRow())
                GvApproverEmpMapping.DataSource = dtEmployee
                GvApproverEmpMapping.DataBind()
                GvApproverEmpMapping.Rows(0).Visible = False
            Else
                GvApproverEmpMapping.DataSource = dtEmployee
                GvApproverEmpMapping.DataBind()
            End If

            'Me.ViewState("mdtTran") = dtEmployee

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try

    End Sub

    Private Sub SetValue()
        Try
            If mintApprovermasterunkid > 0 Then
                objgrievanceapprover_master._Approvermasterunkid = mintApprovermasterunkid
            End If
            objgrievanceapprover_master._Approvalsettingid = Convert.ToInt32(Session("GrievanceApprovalSetting").ToString())


            If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
                objgrievanceapprover_master._Approverempid = 0
                objgrievanceapprover_master._Apprlevelunkid = Convert.ToInt32(drpApproverUseraccess_level.SelectedValue)
                objgrievanceapprover_master._Mapuserunkid = Convert.ToInt32(drpApproverUseraccess_user.SelectedValue)
                objgrievanceapprover_master._Isexternal = 0



            ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then

                objgrievanceapprover_master._Approverempid = Convert.ToInt32(drpAddEditApproverName.SelectedValue)
                objgrievanceapprover_master._Apprlevelunkid = Convert.ToInt32(drpAddEditApprovalLevel.SelectedValue)
                objgrievanceapprover_master._Mapuserunkid = Convert.ToInt32(drpAddEditUser.SelectedValue)
                objgrievanceapprover_master._Isexternal = Convert.ToBoolean(chkAddEditExtApprover.Checked)
            End If

            objgrievanceapprover_master._Userunkid = CInt(Session("UserId"))
            objgrievanceapprover_master._Isactive = True
            Call SetAtValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub CheckAll(ByVal gv As GridView, ByVal dt As DataTable, ByVal chkid As String, ByVal filter As String)
        Try
            Dim head As GridViewRow = gv.HeaderRow
            Dim chkall As CheckBox = TryCast(head.FindControl(chkid), CheckBox)
            If gv.Rows.Count <> dt.Select(filter + "=True").Length Then
                chkall.Checked = False
            ElseIf gv.Rows.Count = dt.Select(filter + "=True").Length Then
                chkall.Checked = True
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub emptyGrid(ByVal gv As GridView, ByVal dt As DataTable)
        Try
            If dt Is Nothing Then
                dt.Rows.Add(dt.NewRow())
                gv.DataSource = dt
                gv.DataBind()
                gv.Rows(0).Visible = False
            Else
                If dt.Rows.Count <= 0 Then
                    dt.Rows.Add(dt.NewRow())
                    gv.DataSource = dt
                    gv.DataBind()
                    gv.Rows(0).Visible = False
                Else
                    gv.DataSource = dt
                    gv.DataBind()
                    gv.Rows(0).Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetAtValue()
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objgrievanceapprover_master._AuditUserid = CInt(Session("UserId"))
            End If
            objgrievanceapprover_master._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objgrievanceapprover_master._ClientIp = CStr(Session("IP_ADD"))
            objgrievanceapprover_master._loginemployeeunkid = -1
            objgrievanceapprover_master._HostName = CStr(Session("HOST_NAME"))
            objgrievanceapprover_master._FormName = mstrModuleName1
            objgrievanceapprover_master._IsFromWeb = True




        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub ResetAddedit()

        If Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.UserAcess Then
            drpApproverUseraccess_level.SelectedIndex = 0
            drpApproverUseraccess_user.SelectedIndex = 0
            'TvApproverUseraccess.Nodes.Clear()

            popupApproverUseraccess.Hide()
            blnpopupApproverUseraccess = False

        ElseIf Convert.ToInt32(Session("GrievanceApprovalSetting").ToString()) = enGrievanceApproval.ApproverEmpMapping Then
            chkAddEditExtApprover.Enabled = True
            chkAddEditExtApprover.Checked = False
            chkAddEditExtApprover_CheckedChanged(Nothing, Nothing)
            mdtTran.Rows.Clear()
            DtPopupAddEdit = Nothing
            'mdtTran = Nothing
            mdtApproverList = Nothing

            gvAddEditEmployee.DataSource = DtPopupAddEdit
            gvAddEditEmployee.DataBind()

            GvApproverEmpMapping.DataSource = mdtTran
            GvApproverEmpMapping.DataBind()

            popupGreAddEditApproverEmpMapping.Hide()
            blnpopupApproverEmpMapping = False

            drpAddEditApprovalLevel.Enabled = True
            drpAddEditApprovalLevel.SelectedIndex = 0
            drpAddEditApproverName.Enabled = True
            drpAddEditApproverName.SelectedIndex = 0
            drpAddEditUser.Enabled = True
            drpAddEditUser.SelectedIndex = 0
        End If
        FillList(False)
        mintApprovermasterunkid = 0
    End Sub

    Public Sub FillTreeEmpMapping_userAccess(ByVal dt As DataTable)
        Try

            TvApproverUseraccess.DataSource = dt
            TvApproverUseraccess.DataBind()



            'TvApproverUseraccess.Nodes.Clear()
            'If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            '    Dim root As TreeNode
            '    Dim child As TreeNode
            '    For Each row As DataRow In dt.Rows

            '        If Convert.ToBoolean(row("IsGrp")) Then
            '            root = New TreeNode()
            '            root.Text = row("UserAccess").ToString()
            '            root.Value = row("AllocationId").ToString()

            '        Else
            '            child = New TreeNode()
            '            child.Text = row("UserAccess").ToString()
            '            child.Value = row("AllocationId").ToString()

            '        End If

            '        If Convert.ToBoolean(row("IsGrp")) = True Then
            '            TvApproverUseraccess.Nodes.Add(root)
            '        Else
            '            root.ChildNodes.Add(child)
            '        End If
            '    Next

            '    TvApproverUseraccess.ExpandAll()
            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Hemant (29 May 2019) -- Start
    'ISSUE/ENHANCEMENT : UAT Changes
    Private Sub FillEmployeeGrid()
        Try
            Dim strSearch As String = ""
            Dim dvFromEmp As New DataView

            If dtEmployee Is Nothing Then Exit Sub

            dvFromEmp = dtEmployee.DefaultView
            If mstrAdvanceFilter.Trim.Length > 0 Then
                dvFromEmp.RowFilter = mstrAdvanceFilter.Trim.Replace("ADF.", "")
                dvFromEmp.Table.AcceptChanges()
                dtEmployee = dvFromEmp.Table
            End If

            gvAddEditEmployee.AutoGenerateColumns = False
            gvAddEditEmployee.DataSource = dtEmployee
            gvAddEditEmployee.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (29 May 2019) -- End

#End Region

#Region "Dropdown Event"
    Protected Sub drpApproverUseraccess_user_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpApproverUseraccess_user.SelectedIndexChanged
        Dim objUser As New clsUserAddEdit
        Try
            Dim dtTable As DataTable = Nothing
            If CInt(drpApproverUseraccess_user.SelectedValue) > 0 Then
                dtTable = objUser.GetUserAccessList(Convert.ToString(Session("UserAccessModeSetting")), CInt(drpApproverUseraccess_user.SelectedValue), Convert.ToInt32(Session("CompanyUnkId")))
                dtTable = New DataView(dtTable, "isAssign = 1 OR isGrp = 1", "", DataViewRowState.CurrentRows).ToTable()
            End If
            FillTreeEmpMapping_userAccess(dtTable)
            'FillUserAccess(dtTable)
            dtTable = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUser = Nothing
        End Try
    End Sub

    Protected Sub drpAddEditApproverName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpAddEditApproverName.SelectedIndexChanged
        Try
            mstrEmployeeIDs = objgrievanceapprover_master.GetApproverEmployeeId(CInt(drpAddEditApproverName.SelectedValue), chkAddEditExtApprover.Checked)
            FillEmployeeList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Checkbox Event"
    Protected Sub chkAddEditExtApprover_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAddEditExtApprover.CheckedChanged
        Try
            PanelApprovalUser.Visible = Not chkAddEditExtApprover.Checked
            FillAddEditCombo()
            Call drpAddEditApproverName_SelectedIndexChanged(sender, e)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkSelectAddEditEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chk As CheckBox = TryCast((sender), CheckBox)
            Dim gvRow As GridViewRow = TryCast((chk).NamingContainer, GridViewRow)

            Dim drRow() As DataRow = CType(DtPopupAddEdit, DataTable).Select("employeecode = '" & gvAddEditEmployee.DataKeys(gvRow.RowIndex)("employeecode").ToString() & "'")
            If drRow.Length > 0 Then
                drRow(0)("ischeck") = chk.Checked
                drRow(0).AcceptChanges()
            End If
            CheckAll(gvAddEditEmployee, DtPopupAddEdit, "ChkAllAddEditEmployee", "ischeck")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkAllAddEditEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If gvAddEditEmployee.Rows.Count <= 0 Then Exit Sub
            Dim dtEmployee As DataTable = CType(DtPopupAddEdit, DataTable)
            If gvAddEditEmployee.Rows.Count > 0 Then
                For i As Integer = 0 To gvAddEditEmployee.Rows.Count - 1
                    If dtEmployee.Rows.Count - 1 < i Then Exit For
                    Dim drRow As DataRow() = dtEmployee.Select("employeecode = '" & gvAddEditEmployee.Rows(i).Cells(1).Text.Trim & "'")
                    If drRow.Length > 0 Then
                        drRow(0)("IsCheck") = cb.Checked
                        Dim gvRow As GridViewRow = gvAddEditEmployee.Rows(i)
                        CType(gvRow.FindControl("ChkSelectAddEditEmployee"), CheckBox).Checked = cb.Checked
                    End If
                    dtEmployee.AcceptChanges()
                Next
                DtPopupAddEdit = dtEmployee
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkAddEditSelectEmpChkgvSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chk As CheckBox = TryCast((sender), CheckBox)
            Dim gvRow As GridViewRow = TryCast((chk).NamingContainer, GridViewRow)

            Dim drRow() As DataRow = CType(mdtTran, DataTable).Select("employeeunkid = '" & GvApproverEmpMapping.DataKeys(gvRow.RowIndex)("employeeunkid").ToString() & "'")
            If drRow.Length > 0 Then
                drRow(0)("ischeck") = chk.Checked
                drRow(0).AcceptChanges()
            End If
            CheckAll(GvApproverEmpMapping, mdtTran, "ChkAllSelectedEmp", "ischeck")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkAllSelectedEmp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If GvApproverEmpMapping.Rows.Count <= 0 Then Exit Sub
            Dim dtSelectedEmp As DataTable = CType(mdtTran, DataTable)
            If GvApproverEmpMapping.Rows.Count > 0 Then

                For i As Integer = 0 To GvApproverEmpMapping.Rows.Count - 1
                    If dtSelectedEmp.Rows.Count - 1 < i Then Exit For
                    Dim drRow As DataRow() = dtSelectedEmp.Select("employeeunkid = '" & GvApproverEmpMapping.DataKeys(i)("employeeunkid").ToString().Trim & "'")
                    If drRow.Length > 0 Then
                        drRow(0)("IsCheck") = cb.Checked
                        Dim gvRow As GridViewRow = GvApproverEmpMapping.Rows(i)
                        CType(gvRow.FindControl("ChkSelectedEmp"), CheckBox).Checked = cb.Checked
                    End If
                    dtSelectedEmp.AcceptChanges()
                Next
                mdtTran = dtSelectedEmp

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Button Event"

    Protected Sub btnAddEditAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddEditAdd.Click
        Try
            If Validation() = False Then Exit Sub
            Dim result As DataRow() = DtPopupAddEdit.Select("IsCheck=True")
            If (result.Length <= 0) Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
                'Sohail (23 Mar 2019) -- End
                Exit Sub
            End If

            Dim dtRow As DataRow
            Dim count As Integer = 0

            If result.Length > 0 Then
                For i As Integer = 0 To result.Length - 1
                    count = CInt(mdtTran.Compute("count(employeeunkid)", "employeeunkid=" & result(i)("employeeunkid").ToString()))
                    If count > 0 Then
                        Dim dr() As DataRow = Nothing
                        dr = mdtTran.Select("employeeunkid=" & CInt(result(i)("employeeunkid")))
                        If dr.Length > 0 Then
                            dr(0)("approverunkid") = Convert.ToInt32(drpAddEditApproverName.SelectedValue)
                            dr(0)("approvername") = drpAddEditApproverName.Text.Trim
                            dr(0)("AUD") = "A"
                        End If
                    Else
                        dtRow = mdtTran.NewRow
                        dtRow("approvertranunkid") = -1
                        dtRow("approverunkid") = Convert.ToInt32(drpAddEditApproverName.SelectedValue)
                        dtRow("approvername") = drpAddEditApproverName.Text.Trim
                        dtRow("employeeunkid") = CInt(result(i)("employeeunkid"))
                        dtRow("name") = CStr(result(i)("employeecode")) + "-" + CStr(result(i)("name"))
                        'dtRow("departmentunkid") = CInt(result(i)("departmentunkid"))
                        dtRow("departmentname") = CStr(result(i)("departmentname"))
                        'dtRow("jobunkid") = CInt(result(i)("jobunkid"))
                        dtRow("jobname") = CStr(result(i)("jobname"))
                        dtRow("AUD") = "A"
                        dtRow("GUID") = Guid.NewGuid.ToString
                        mdtTran.Rows.Add(dtRow)
                    End If
                Next
            End If

            GvApproverEmpMapping.DataSource = mdtTran
            GvApproverEmpMapping.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddeditDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddeditDelete.Click
        Try

            If mdtTran Is Nothing OrElse mdtTran.Columns.Contains("IsCheck") = False Then Exit Sub

            Dim drRow() As DataRow = mdtTran.Select("IsCheck = true")
            If drRow.Length <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Selected Employee is compulsory information.Please Check atleast One Selected Employee."), Me)
                Exit Sub
            End If
            Dim drTemp As DataRow() = Nothing
            Dim objLeaveForm As New clsleaveform
            For i As Integer = 0 To drRow.Length - 1
                If CInt(drRow(i)("approvertranunkid")) = -1 Then
                    drTemp = mdtTran.Select("GUID = '" & drRow(i)("GUID").ToString() & "'")
                Else
                    drTemp = mdtTran.Select("employeeunkid = " & CInt(drRow(i)("employeeunkid")))
                End If

                If drTemp IsNot Nothing AndAlso drTemp.Length > 0 Then
                    drTemp(0).Item("AUD") = "D"
                    drTemp(0).AcceptChanges()
                End If
            Next
            SelectedEmplyeeList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnpopupsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnpopupsave.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then Exit Sub

            Dim count As Integer = CInt(mdtTran.Compute("Count(employeeunkid)", "AUD <> 'D'"))
            If mdtTran.Rows.Count = 0 Or count = 0 Or GvApproverEmpMapping.Rows.Count <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Employee is compulsory information.Please Check atleast One Employee."), Me)
                Exit Sub
            End If

            SetValue()

            If mintApprovermasterunkid > 0 Then
                blnFlag = objgrievanceapprover_master.Update(mdtTran)
            Else
                blnFlag = objgrievanceapprover_master.Insert(mdtTran)
            End If

            If blnFlag = False And objgrievanceapprover_master._Message <> "" Then
                DisplayMessage.DisplayMessage(objgrievanceapprover_master._Message, Me)
            End If

            If blnFlag Then

                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Approver Saved Successfully"), Me)

                ResetAddedit()
                'FillList()

                'Session("EmpList") = Nothing
                ''Pinkal (16-Feb-2016) -- Start
                ''Enhancement - Testing on External Approver in Leave Module for Self Service in 57.2.

                'Dim dtTable As DataTable = CType(Me.ViewState("SelectedEmployee"), DataTable)
                'Me.ViewState("SelectedEmployee") = dtTable.Clone
                'dtTable = Nothing

                ''Me.ViewState("FirstRecordNo") = Nothing
                ''Me.ViewState("LastRecordNo") = Nothing
                ''Me.ViewState("FirstSelectedEmpRNo") = Nothing
                ''Me.ViewState("LastSelectedEmpRNo") = Nothing
                ''Pinkal (16-Feb-2016) -- End
                'Me.ViewState("LeaveTypeMapping") = Nothing
                'drpApprover.SelectedIndex = 0
                'drpLevel.SelectedIndex = 0
                'txtSearch.Text = ""

                ''S.SANDEEP [30 JAN 2016] -- START
                ''drpDepartment.SelectedIndex = 0
                ''drpJob.SelectedIndex = 0
                ''drpSection.SelectedIndex = 0
                'chkExternalApprover.Checked = False
                'Call chkExternalApprover_CheckedChanged(New Object, New EventArgs)
                ''S.SANDEEP [30 JAN 2016] -- END

                'GvEmployee.DataSource = Nothing
                'GvEmployee.DataBind()
                'GvSelectedEmployee.DataSource = Nothing
                'GvSelectedEmployee.DataBind()
                'If Session("ApproverId") IsNot Nothing Then
                '    Session("ApproverId") = Nothing

                '    'Pinkal (06-Jan-2016) -- Start
                '    'Enhancement - Working on Changes in SS for Leave Module.
                '    'Response.Redirect(Session("servername").ToString() & "~/Leave/wPg_LeaveApproverList.aspx")
                '    Response.Redirect("~/Leave/wPg_LeaveApproverList.aspx")
                '    'Pinkal (06-Jan-2016) -- End


                'End If
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnpopupsave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    Protected Sub btnpopupclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnpopupclose.Click
        Try
            blnpopupApproverEmpMapping = False
            popupGreAddEditApproverEmpMapping.Hide()
            ResetAddedit()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnApproverUseraccessClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproverUseraccessClose.Click

        Try
            blnpopupApproverUseraccess = False
            popupApproverUseraccess.Hide()
            ResetAddedit()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub btnApproverUseraccessSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproverUseraccessSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then Exit Sub

            SetValue()

            If mintApprovermasterunkid > 0 Then
                blnFlag = objgrievanceapprover_master.Update(mdtTran)
            Else
                blnFlag = objgrievanceapprover_master.Insert(mdtTran)
            End If

            If blnFlag = False And objgrievanceapprover_master._Message <> "" Then
                DisplayMessage.DisplayMessage(objgrievanceapprover_master._Message, Me)
            End If

            If blnFlag Then
                popupGreAddEditApproverEmpMapping.Hide()
                blnpopupApproverEmpMapping = False
                mdtTran = Nothing
            End If


            'Gajanan [10-June-2019] -- Start      
            'ISSUE/ENHANCEMENT : Grievance UAT Changes        
            'DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Approver Saved Successfully"), Me)
            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Approver Saved Successfully"), Me)
            'Gajanan [10-June-2019] -- End
            ResetAddedit()


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub confirmapproverdelete_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles confirmapproverdelete.buttonYes_Click
        Try

            DeleteApprovalReason.Title = Language.getMessage(mstrModuleName, 8, "Are You Sure You Want Delete Approval ?")

            DeleteApprovalReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            drpLevel.SelectedIndex = 0
            drpApprover.SelectedIndex = 0
            drpStatus.SelectedIndex = 0
            Call FillList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
#End Region

#Region "Gridview Event"
    Protected Sub gvApproverList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApproverList.RowDataBound
        Dim oldid As String
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table
                If CInt(gvApproverList.DataKeys(e.Row.RowIndex)("approvermasterunkid").ToString) > 0 Then

                    oldid = dt.Rows(e.Row.RowIndex)("LevelName").ToString()
                    If dt.Rows(e.Row.RowIndex)("IsGrp").ToString() = True AndAlso oldid <> currentId Then
                        Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("LevelName").ToString(), gvApproverList)
                        currentId = oldid
                    Else
                        Dim lnkactive As LinkButton = TryCast(e.Row.FindControl("lnkactive"), LinkButton)
                        Dim lnkDeactive As LinkButton = TryCast(e.Row.FindControl("lnkDeactive"), LinkButton)
                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)

                        If CBool(Session("AllowToEditGrievanceApprover")) Then
                            lnkedit.Visible = True
                        Else
                            lnkedit.Visible = False
                        End If

                        If CBool(Session("AllowToDeleteGrievanceApprover")) Then
                            lnkdelete.Visible = True
                        Else
                            lnkdelete.Visible = False
                        End If

                        If CBool(Session("AllowToActivateGrievanceApprover")) Then
                            lnkactive.Visible = True
                        Else
                            lnkactive.Visible = False
                        End If

                        If CBool(Session("AllowToInActivateGrievanceApprover")) Then
                            lnkDeactive.Visible = True
                        Else
                            lnkDeactive.Visible = False
                        End If

                        If dt.Rows(e.Row.RowIndex)("isactive").ToString() <> "" Then
                            If dt.Rows(e.Row.RowIndex)("isactive").ToString() = True Then
                                lnkactive.Visible = False
                                lnkDeactive.Visible = True
                            Else
                                lnkactive.Visible = True
                                lnkDeactive.Visible = False
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            currentId = ""
            oldid = ""
        End Try
    End Sub

    Protected Sub TvApproverUseraccess_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles TvApproverUseraccess.RowDataBound
        Dim oldid As String
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                oldid = dt.Rows(e.Row.RowIndex)("UserAccess").ToString()
                If dt.Rows(e.Row.RowIndex)("AllocationLevel").ToString() = -1 AndAlso oldid <> currentId Then
                    Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("UserAccess").ToString(), TvApproverUseraccess)
                    currentId = oldid
                Else
                    Dim statusCell As TableCell = e.Row.Cells(0)
                    statusCell.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dt.Rows(e.Row.RowIndex)("UserAccess").ToString
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            currentId = ""
            oldid = ""
        End Try
    End Sub


#End Region

    'Hemant (29 May 2019) -- Start
    'ISSUE/ENHANCEMENT : UAT Changes
#Region "TextBox Events"
    Protected Sub txtSearchEmployee_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmployee.TextChanged
        Try
            If dtEmployee IsNot Nothing Then
                Dim dvEmployee As DataView = dtEmployee.DefaultView
                If dvEmployee.Table.Rows.Count > 0 Then
                    If txtSearchEmployee.Text.Trim.Length > 0 Then
                        dvEmployee.RowFilter = "employeecode like '%" & txtSearchEmployee.Text.Trim & "%' OR name like '%" & txtSearchEmployee.Text.Trim & "%' "
                    End If
                    If mstrAdvanceFilter.Trim.Length > 0 Then
                        dvEmployee.RowFilter &= IIf(dvEmployee.RowFilter.Trim.Length > 0, " AND ", "").ToString & mstrAdvanceFilter
                    End If
                    gvAddEditEmployee.DataSource = dvEmployee.ToTable
                    gvAddEditEmployee.DataBind()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "LinkButton Event"

    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
        Try
            mstrAdvanceFilter = ""
            txtSearchEmployee.Text = ""
            If dtEmployee IsNot Nothing Then
                Dim dRow() As DataRow = dtEmployee.Select("isCheck = True")
                If dRow.Length > 0 Then
                    For i As Integer = 0 To dRow.Length - 1
                        dRow(i).Item("isCheck") = False
                        dRow(i).AcceptChanges()
                    Next
                End If

            End If
            Call FillEmployeeGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'Hemant (29 May 2019) -- End


#End Region

    'Sohail (22 Nov 2018) -- Start
    'Internal Enhancement - Applying Controls Language in self service for the module which is not in desktop in 75.1.
    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Language._Object.setCaption(Me.lblLevel.ID, Me.lblLevel.Text)
            Language._Object.setCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Language._Object.setCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Language._Object.setCaption(Me.btnNew.ID, Me.btnNew.Text)
            Language._Object.setCaption(Me.btnSearch.ID, Me.btnSearch.Text)
            Language._Object.setCaption(Me.btnReset.ID, Me.btnReset.Text)
            Language._Object.setCaption(gvApproverList.Columns(3).FooterText, gvApproverList.Columns(3).HeaderText)
            Language._Object.setCaption(gvApproverList.Columns(4).FooterText, gvApproverList.Columns(4).HeaderText)
            Language._Object.setCaption(gvApproverList.Columns(5).FooterText, gvApproverList.Columns(5).HeaderText)
            Language._Object.setCaption(gvApproverList.Columns(6).FooterText, gvApproverList.Columns(6).HeaderText)


            Language.setLanguage(mstrModuleName1)
            Language._Object.setCaption(Me.lblPageHeader1.ID, Me.lblPageHeader1.Text)
            Language._Object.setCaption(Me.lblApproverInfo.ID, Me.lblApproverInfo.Text)
            Language._Object.setCaption(Me.chkAddEditExtApprover.ID, Me.chkAddEditExtApprover.Text)
            Language._Object.setCaption(Me.lblApproverName.ID, Me.lblApproverName.Text)
            Language._Object.setCaption(Me.lblApprovalevel.ID, Me.lblApprovalevel.Text)
            Language._Object.setCaption(Me.lblApprovalUser.ID, Me.lblApprovalUser.Text)
            Language._Object.setCaption(Me.lnkReset.ID, Me.lnkReset.Text)
            Language._Object.setCaption(gvAddEditEmployee.Columns(1).FooterText, gvAddEditEmployee.Columns(1).HeaderText)
            Language._Object.setCaption(gvAddEditEmployee.Columns(2).FooterText, gvAddEditEmployee.Columns(2).HeaderText)
            Language._Object.setCaption(Me.btnAddEditAdd.ID, Me.btnAddEditAdd.Text)

            Language._Object.setCaption(Me.LblGreAddEditLeaveApprover.ID, Me.LblGreAddEditLeaveApprover.Text)
            Language._Object.setCaption(GvApproverEmpMapping.Columns(1).FooterText, GvApproverEmpMapping.Columns(1).HeaderText)
            Language._Object.setCaption(GvApproverEmpMapping.Columns(2).FooterText, GvApproverEmpMapping.Columns(2).HeaderText)
            Language._Object.setCaption(GvApproverEmpMapping.Columns(3).FooterText, GvApproverEmpMapping.Columns(3).HeaderText)

            Language._Object.setCaption(Me.btnAddeditDelete.ID, Me.btnAddeditDelete.Text)
            Language._Object.setCaption(Me.btnpopupsave.ID, Me.btnpopupsave.Text)
            Language._Object.setCaption(Me.btnpopupclose.ID, Me.btnpopupclose.Text)

            Language._Object.setCaption(Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)
            Language._Object.setCaption(Me.lblApproverInfo1.ID, Me.lblApproverInfo1.Text)
            Language._Object.setCaption(Me.lblApproverUseraccess_level.ID, Me.lblApproverUseraccess_level.Text)
            Language._Object.setCaption(Me.lblApproverUseraccess_user.ID, Me.lblApproverUseraccess_user.Text)

            Language._Object.setCaption(Me.btnApproverUseraccessSave.ID, Me.btnApproverUseraccessSave.Text)
            Language._Object.setCaption(Me.btnApproverUseraccessClose.ID, Me.btnApproverUseraccessClose.Text)


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.ID, Me.lblLevel.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            gvApproverList.Columns(3).HeaderText = Language._Object.getCaption(gvApproverList.Columns(3).FooterText, gvApproverList.Columns(3).HeaderText)
            gvApproverList.Columns(4).HeaderText = Language._Object.getCaption(gvApproverList.Columns(4).FooterText, gvApproverList.Columns(4).HeaderText)
            gvApproverList.Columns(5).HeaderText = Language._Object.getCaption(gvApproverList.Columns(5).FooterText, gvApproverList.Columns(5).HeaderText)
            gvApproverList.Columns(6).HeaderText = Language._Object.getCaption(gvApproverList.Columns(6).FooterText, gvApproverList.Columns(6).HeaderText)

            Language.setLanguage(mstrModuleName1)
            Me.lblPageHeader1.Text = Language._Object.getCaption(Me.lblPageHeader1.ID, Me.lblPageHeader1.Text)
            Me.lblApproverInfo.Text = Language._Object.getCaption(Me.lblApproverInfo.ID, Me.lblApproverInfo.Text)
            Me.chkAddEditExtApprover.Text = Language._Object.getCaption(Me.chkAddEditExtApprover.ID, Me.chkAddEditExtApprover.Text)
            Me.lblApproverName.Text = Language._Object.getCaption(Me.lblApproverName.ID, Me.lblApproverName.Text)
            Me.lblApprovalevel.Text = Language._Object.getCaption(Me.lblApprovalevel.ID, Me.lblApprovalevel.Text)
            Me.lblApprovalUser.Text = Language._Object.getCaption(Me.lblApprovalUser.ID, Me.lblApprovalUser.Text)

            gvAddEditEmployee.Columns(1).HeaderText = Language._Object.getCaption(gvAddEditEmployee.Columns(1).FooterText, gvAddEditEmployee.Columns(1).HeaderText)
            gvAddEditEmployee.Columns(2).HeaderText = Language._Object.getCaption(gvAddEditEmployee.Columns(2).FooterText, gvAddEditEmployee.Columns(2).HeaderText)

            Me.btnAddEditAdd.Text = Language._Object.getCaption(Me.btnAddEditAdd.ID, Me.btnAddEditAdd.Text).Replace("&", "")
            Me.LblGreAddEditLeaveApprover.Text = Language._Object.getCaption(Me.LblGreAddEditLeaveApprover.ID, Me.LblGreAddEditLeaveApprover.Text).Replace("&", "")

            GvApproverEmpMapping.Columns(1).HeaderText = Language._Object.getCaption(GvApproverEmpMapping.Columns(1).FooterText, GvApproverEmpMapping.Columns(1).HeaderText)
            GvApproverEmpMapping.Columns(2).HeaderText = Language._Object.getCaption(GvApproverEmpMapping.Columns(2).FooterText, GvApproverEmpMapping.Columns(2).HeaderText)
            GvApproverEmpMapping.Columns(3).HeaderText = Language._Object.getCaption(GvApproverEmpMapping.Columns(3).FooterText, GvApproverEmpMapping.Columns(3).HeaderText)

            Me.btnAddeditDelete.Text = Language._Object.getCaption(Me.btnAddeditDelete.ID, Me.btnAddeditDelete.Text).Replace("&", "")
            Me.btnpopupsave.Text = Language._Object.getCaption(Me.btnpopupsave.ID, Me.btnpopupsave.Text).Replace("&", "")
            Me.btnpopupclose.Text = Language._Object.getCaption(Me.btnpopupclose.ID, Me.btnpopupclose.Text).Replace("&", "")

            Me.lblPageHeader2.Text = Language._Object.getCaption(Me.lblPageHeader2.ID, Me.lblPageHeader2.Text)
            Me.lblApproverInfo1.Text = Language._Object.getCaption(Me.lblApproverInfo1.ID, Me.lblApproverInfo1.Text)
            Me.lblApproverUseraccess_level.Text = Language._Object.getCaption(Me.lblApproverUseraccess_level.ID, Me.lblApproverUseraccess_level.Text)
            Me.lblApproverUseraccess_user.Text = Language._Object.getCaption(Me.lblApproverUseraccess_user.ID, Me.lblApproverUseraccess_user.Text)

            Me.btnApproverUseraccessSave.Text = Language._Object.getCaption(Me.btnApproverUseraccessSave.ID, Me.btnApproverUseraccessSave.Text).Replace("&", "")
            Me.btnApproverUseraccessClose.Text = Language._Object.getCaption(Me.btnApproverUseraccessClose.ID, Me.btnApproverUseraccessClose.Text).Replace("&", "")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (22 Nov 2018) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, You cannot delete this Approver . Reason: This Approver is in use.")
            Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver . Reason: This Approver is in use.")
            Language.setMessage(mstrModuleName, 3, "Selected Employee is compulsory information.Please Check atleast One Selected Employee.")
            Language.setMessage(mstrModuleName, 4, "Employee is compulsory information.Please Check atleast One Employee.")
            Language.setMessage(mstrModuleName, 5, "Approver Saved Successfully")
            Language.setMessage(mstrModuleName, 6, "Confirmation")
            Language.setMessage(mstrModuleName, 7, "Are You Sure You Want To Delete This Approver ?")
            Language.setMessage(mstrModuleName, 8, "Are You Sure You Want Delete Approval ?")
            Language.setMessage(mstrModuleName, 9, "Confirmation")
            Language.setMessage(mstrModuleName, 10, "Are You Sure You Want To Deactive  This Approver?")
            Language.setMessage(mstrModuleName, 11, "Are You Sure You Want To Active This Approver?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
