﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_AddEditApprovalList.aspx.vb" Inherits="Claims_And_Expenses_wPg_AddEditApprovalList"
    Title="Add/Edit Expense Approval" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="ComboList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }


        $("[id*=ChkAll]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    if ($(this).is(":visible")) {
                    $(this).attr("checked", "checked");
                    }
                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        $("[id*=ChkgvSelect]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });

        $("[id*=ChkAsgAll]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    if ($(this).is(":visible")) {
                    $(this).attr("checked", "checked");
                    }
                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        $("[id*=ChkAsgSelect]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });
        
        
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching() {
            if ($('#txtSearchEmp').val().length > 0) {
                $('#<%= dgvAEmployee.ClientID %> tbody tr').hide();
                $('#<%= dgvAEmployee.ClientID %> tbody tr:first').show();
                $('#<%= dgvAEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchEmp').val() + '\')').parent().show();
            }
            else if ($('#txtSearchEmp').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= dgvAEmployee.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtSearchEmp').val('');
            $('#<%= dgvAEmployee.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtSearchEmp').focus();
        }


        function ToSearching() {
            if ($('#txtSearchTo').val().length > 0) {
                $('#<%= dgvAssessor.ClientID %> tbody tr').hide();
                $('#<%= dgvAssessor.ClientID %> tbody tr:first').show();
                $('#<%= dgvAssessor.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearchTo').val() + '\')').parent().show();
            }
            else if ($('#txtSearchTo').val().length == 0) {
                 resetToSearchValue();
            }
            if ($('#<%= dgvAssessor.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetToSearchValue();
            }
        }
        function resetToSearchValue() {
            $('#txtSearchTo').val('');
            $('#<%= dgvAssessor.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtSearchTo').focus();
        }
        
        
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            var scroll1 = {
                Y: '#<%= hfScrollPosition1.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
            $("#scrollable-container1").scrollTop($(scroll1.Y).val());
    }
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Expense Approval"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 50%; vertical-align: top">
                                                <div class="panel-body">
                                                    <div id="Div1" class="panel-default">
                                                        <div id="Div2" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblInfo" runat="server" Text="Approver Information"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div3" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%"></td>
                                                                    <td style="width: 80%">
                                                                        <asp:CheckBox ID="chkExternalApprover" runat="server" Text="Make External Approver" AutoPostBack="true"></asp:CheckBox>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblExpenseCat" runat="server" Text="Expense Cat."></asp:Label>
                                                                    </td>
                                                                    <td style="width: 80%">
                                                                        <asp:DropDownList ID="cboExCategory" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblApproverName" runat="server" Text="Approver"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 80%">
                                                                        <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblApproveLevel" runat="server" Text="Level"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 80%">
                                                                        <asp:DropDownList ID="cboApproveLevel" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblUser" runat="server" Text="User"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 80%">
                                                                        <asp:DropDownList ID="cboUser" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%">
                                                                    </td>
                                                                    <td style="text-align: right; padding-right: 25px; font-weight: bold; width: 80%">
                                                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" CssClass="lnkhover"
                                                                            Style="margin-right: 10px;"></asp:LinkButton>
                                                                        <asp:LinkButton ID="lnkReset" runat="server" Text="Reset" CssClass="lnkhover"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%;" colspan="2">
                                                                    <%--'Pinkal (28-Apr-2020) -- Start
                                                                            'Optimization  - Working on Process Optimization and performance for require module.
                                                                            <asp:TextBox ID="txtSearchEmp" runat="server" AutoPostBack="true"></asp:TextBox> --%>
                                                                            <input type="text" id="txtSearchEmp" name="txtSearchEmp" placeholder = "Type To Search Text"  maxlength="50" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                                                    <%--'Pinkal (28-Apr-2020) -- End --%>        
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="border-bottom: 1px solid #DDD; margin-bottom: 5px; margin-top: 5px" colspan="2">
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%" colspan="2">
                                                                        <div id="scrollable-container" style="overflow: auto; height: 350px; vertical-align: top"
                                                                            onscroll="$(scroll.Y).val(this.scrollTop);">
                                                                            <asp:GridView ID="dgvAEmployee" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                                                HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false" DataKeyNames= "employeeunkid"
                                                                                HeaderStyle-Font-Bold="false" Width="99%">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                        <%-- 'Pinkal (27-Dec-2019) -- Start
                                                                                         'Enhancement - Changes related To OT NMB Testing.--%>
                                                                                       <%-- <HeaderTemplate>
                                                                                            <asp:CheckBox ID="ChkAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll_CheckedChanged" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="ChkgvSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkgvSelect_CheckedChanged" />
                                                                                        </ItemTemplate>--%>
                                                                                         <HeaderTemplate>
                                                                                            <asp:CheckBox ID="ChkAll" runat="server"/>
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="ChkgvSelect" runat="server"/>
                                                                                        </ItemTemplate>
                                                                                          <%-- 'Pinkal (27-Dec-2019) -- --%>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="employeecode" HeaderText="Employee Code" ReadOnly="true"
                                                                                        FooterText="dgcolhEcode" />
                                                                                    <asp:BoundField DataField="name" HeaderText="Employee" ReadOnly="true" FooterText="dgcolhEName" />
                                                                                    <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                                        Visible="false" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btndefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="width: 50%; vertical-align: top">
                                                <div class="panel-body">
                                                    <div id="Div4" class="panel-default">
                                                        <div id="Div5" class="panel-heading-default">
                                                            <div style="float: left;">
                                                                <asp:Label ID="lblAssignedEmployee" runat="server" Text="Assigned Employee"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div id="Div6" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                    <%--'Pinkal (28-Apr-2020) -- Start
                                                                            'Optimization  - Working on Process Optimization and performance for require module.
                                                                        <asp:TextBox ID="txtAsgSearch" runat="server" AutoPostBack="true"></asp:TextBox> --%>
                                                                         <input type="text" id="txtSearchTo" name="txtSearchTo" placeholder = "Type To Search Text"  maxlength="50" style="height: 25px; font: 100" onkeyup="ToSearching();" />
                                                                         <%--'Pinkal (28-Apr-2020) -- End --%>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="border-bottom: 1px solid #DDD; margin-bottom: 5px; margin-top: 5px">
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <div id="scrollable-container1" style="overflow: auto; height: 508px; vertical-align: top"
                                                                            onscroll="$(scroll1.Y).val(this.scrollTop);">
                                                                            <asp:GridView ID="dgvAssessor" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                                                HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"  DataKeyNames= "crapproverunkid,crapprovertranunkid,employeeunkid"
                                                                                HeaderStyle-Font-Bold="false" Width="150%">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                        
                                                                                          <%-- 'Pinkal (27-Dec-2019) -- Start
                                                                                         'Enhancement - Changes related To OT NMB Testing.--%>
                                                                                        <%--<HeaderTemplate>
                                                                                            <asp:CheckBox ID="ChkAsgAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkAsgAll_CheckedChanged" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="ChkAsgSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkAsgSelectedEmp_CheckedChanged" />
                                                                                        </ItemTemplate>--%>
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="ChkAsgAll" runat="server" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:CheckBox ID="ChkAsgSelect" runat="server" />
                                                                                        </ItemTemplate>
                                                                                          <%-- 'Pinkal (27-Dec-2019)--%>
                                                                                    </asp:TemplateField>
                                                                                    <asp:BoundField DataField="ecode" HeaderText="Employee Code" ReadOnly="true" FooterText="dgcolhaCode" />
                                                                                    <asp:BoundField DataField="ename" HeaderText="Employee" ReadOnly="true" FooterText="dgcolhaEmp" />
                                                                                    <asp:BoundField DataField="edept" HeaderText="Department" ReadOnly="true" FooterText="dgcolhaDepartment" />
                                                                                    <asp:BoundField DataField="eJob" HeaderText="Job" ReadOnly="true" FooterText="dgcolhaJob" />
                                                                                    <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                                        Visible="false" />
                                                                                    <asp:BoundField DataField="crapproverunkid" HeaderText="AMasterId" ReadOnly="true"
                                                                                        Visible="false" />
                                                                                    <asp:BoundField DataField="crapprovertranunkid" HeaderText="ATranId" ReadOnly="true"
                                                                                        Visible="false" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="btnDeleteA" runat="server" Text="Delete" CssClass="btndefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                    <uc8:ConfirmYesNo ID="popYesNo" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
