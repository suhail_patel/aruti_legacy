﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_Approval_List.aspx.vb" Inherits="Claims_And_Expenses_Approval_List"
    Title="Expense Approval List" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="ComboList" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="confirmyesno" TagPrefix="cnfpopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="pblMian" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Expense Approval List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 12%">
                                                <asp:Label ID="lblExpenseCat" runat="server" Text="Expense Category" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboExCategory" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 9%; padding-left: 10px">
                                                <asp:Label ID="lblStatus" runat="server" Text="Staus" />
                                            </td>
                                            <td style="width: 21%">
                                                <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 9%; padding-left: 10px">
                                                <asp:Label ID="lblLevel" runat="server" Text="Level" />
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="cboLevel" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 12%">
                                                <asp:Label ID="lblApprover" runat="server" Text="Approver" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboApprover" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 68%; padding-left: 10px" colspan="4">
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                                <table style="width: 100%; margin-top:10px">
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <asp:Panel ID="pnl_GvApprovalList" ScrollBars="Auto" Height="350px" runat="server">
                                                <asp:DataGrid ID="GvApprovalList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                    CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit"></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete"
                                                                        CssClass="griddelete"></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderStyle-Width="60px" ItemStyle-Width="60px" HeaderStyle-HorizontalAlign="Center">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkActive" runat="server" ToolTip="Make Approver Active" Text="Active" CommandName="Active" Font-Underline="false"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderStyle-Width="60px" ItemStyle-Width="60px" HeaderStyle-HorizontalAlign="Center">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkInActive" runat="server" ToolTip="Make Approver Inactive" Text="Inactive"
                                                                    CommandName="Inactive"  Font-Underline="false"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="expensetype" HeaderText="Exp. Type" ReadOnly="true" FooterText="colhExpType" />
                                                        <asp:BoundColumn DataField="ename" HeaderText="Expense Approval" ReadOnly="true"
                                                            FooterText="colhApprover" />
                                                        <asp:BoundColumn DataField="clevel" HeaderText="Level" ReadOnly="true" FooterText="colhLevel" />
                                                        <asp:BoundColumn DataField="department" HeaderText="Department" ReadOnly="true" FooterText="colhDepartment" />
                                                        <asp:BoundColumn DataField="jobname" HeaderText="Job" ReadOnly="true" FooterText="colhJob" />
                                                        <asp:BoundColumn DataField="usermapped" HeaderText="Mapped User" ReadOnly="true" FooterText="colhMappedUser" />
                                                         <asp:BoundColumn DataField="ExAppr" HeaderText="External Approver" ReadOnly="true" FooterText="objcolhExCat"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="crapproverunkid" HeaderText="Expese CateGory" ReadOnly="true"
                                                            Visible="false" />
                                                        <asp:BoundColumn DataField="IsGrp" Visible="false" />
                                                    </Columns>
                                                    <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                        Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                                </asp:DataGrid>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server"  />
                    <cnfpopup:confirmyesno ID="popupActive" runat="server" />
                    <cnfpopup:confirmyesno ID="popupInactive" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>

