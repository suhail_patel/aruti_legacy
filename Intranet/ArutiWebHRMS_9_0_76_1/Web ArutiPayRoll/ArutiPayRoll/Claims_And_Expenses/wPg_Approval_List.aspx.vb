﻿Option Strict On

#Region "Import"
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
#End Region

Partial Class Claims_And_Expenses_Approval_List
    Inherits Basepage

#Region " Private Variables "
    Private DisplayMessage As New CommonCodes

    'Pinkal (05-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objExApprover As New clsExpenseApprover_Master
    'Pinkal (05-Sep-2020) -- End

    Private ReadOnly mstrModuleName As String = "frmExApproverList"

    'Pinkal (24-Dec-2019) -- Start
    'Enhancement - Claim Retirement Enhancements for NMB.
    Private mintExpenseCategoryId As Integer = 0
    'Pinkal (24-Dec-2019) -- End

#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            SetLanguage()

            If IsPostBack = False Then

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (05-Sep-2020) -- End

                'Pinkal (24-Dec-2019) -- Start
                'Enhancement - Claim Retirement Enhancements for NMB.
                If Request.QueryString.Count > 0 Then
                    If Request.QueryString("ID") IsNot Nothing Then
                        mintExpenseCategoryId = CInt(Server.UrlDecode(Basepage.b64decode(Request.QueryString("ID"))))
                    End If
                End If
                'Pinkal (24-Dec-2019) -- End

                FillCombo()
                Call drpExCategory_SelectedIndexChanged(cboExCategory, New EventArgs())
                Fill_List()

                If Session("Claim_ExpenseUnkID") IsNot Nothing Then
                    cboExCategory.SelectedValue = Session("Claim_ExpenseUnkID").ToString()
                    Session.Remove("Claim_ExpenseUnkID")
                    If CInt(cboExCategory.SelectedValue) > 0 Then
                        Call btnSearch_Click(btnSearch, Nothing)
                    End If
                End If

                SetVisibility()
            Else

                'Pinkal (24-Dec-2019) -- Start
                'Enhancement - Claim Retirement Enhancements for NMB.
                mintExpenseCategoryId = CInt(Me.ViewState("ExpenseCategoryId"))
                'Pinkal (24-Dec-2019) -- End

                If Me.ViewState("dtTable") IsNot Nothing Then
                    GvApprovalList.DataSource = CType(Me.ViewState("dtTable"), DataTable)
                    GvApprovalList.DataBind()
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub


    'Pinkal (24-Dec-2019) -- Start
    'Enhancement - Claim Retirement Enhancements for NMB.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("ExpenseCategoryId") = mintExpenseCategoryId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (24-Dec-2019) -- End


#End Region

#Region "Button's Event"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            If CInt(cboExCategory.SelectedValue) > 0 Then
                Session("Claim_ExpenseUnkID") = cboExCategory.SelectedValue
            End If

            'Pinkal (24-Dec-2019) -- Start
            'Enhancement - Claim Retirement Enhancements for NMB.
            If Request.QueryString.ToString.Length > 0 AndAlso mintExpenseCategoryId = enExpenseType.EXP_IMPREST Then
                Response.Redirect("~/Claims_And_Expenses/wPg_AddEditApprovalList.aspx?ID=" & Server.UrlEncode(Basepage.b64encode(mintExpenseCategoryId.ToString())), False)
            Else
                Response.Redirect("~/Claims_And_Expenses/wPg_AddEditApprovalList.aspx", False)
            End If
            'Pinkal (24-Dec-2019) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnNew_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboExCategory.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, Expense Category is mandatory information. Please select Expense Category to continue."), Me)
                Exit Sub
            End If

            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            'GvApprovalList.CurrentPageIndex = 0
            'Shani(17-Aug-2015) -- End
            Call Fill_List()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("objbtnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExApprover As New clsExpenseApprover_Master
        'Pinkal (05-Sep-2020) -- End
        Try
            If (popup_DeleteReason.Reason.Trim = "") Then
                DisplayMessage.DisplayMessage("Please enter delete reason.", Me)
                popup_DeleteReason.Show()
                Exit Sub
            End If

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmExApproverList"
            StrModuleName2 = "mnuUtilitiesMain"
            StrModuleName3 = "mnuClaimsExpenses"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            With objExApprover
                ._Isvoid = True
                ._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popup_DeleteReason.Reason.Trim 'Nilay (01-Feb-2015) -- txtreasondel.Text

                If (CInt(Session("loginBy")) = Global.User.en_loginby.User) Then
                    ._Voiduserunkid = CInt(Session("UserId"))
                End If

                .Delete(CInt(Me.ViewState("crapproverunkid")))

                If (._Message.Length > 0) Then
                    DisplayMessage.DisplayMessage("You Cannot Delete this Entry." & ._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Successfully deleted." & ._Message, Me)
                    Me.ViewState("crapproverunkid") = Nothing
                End If
                popup_DeleteReason.Dispose() 'Nilay (01-Feb-2015) -- popup1.Dispose()
                popup_DeleteReason.Reason = "" 'Nilay (01-Feb-2015) -- txtreasondel.Text = ""
                Fill_List()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            objExApprover = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboExCategory.SelectedValue = "0"
            cboStatus.SelectedValue = "1"
            Me.ViewState("dtTable") = Nothing
            Call drpExCategory_SelectedIndexChanged(cboExCategory, Nothing)
            Fill_List(True)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupActive_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupActive.buttonYes_Click
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExApprover As New clsExpenseApprover_Master
        'Pinkal (05-Sep-2020) -- End
        Try
            If Me.ViewState("crapproverid") IsNot Nothing Then
                If objExApprover.InActiveApprover(CInt(Me.ViewState("crapproverid")), CInt(Session("CompanyUnkId")), True) = False Then
                    DisplayMessage.DisplayMessage(objExApprover._Message, Me)
                    Exit Sub
                Else
                    Call FillApprover()
                    Call Fill_List()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            objExApprover = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub popupInactive_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupInactive.buttonYes_Click
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExApprover As New clsExpenseApprover_Master
        'Pinkal (05-Sep-2020) -- End
        Try
            If Me.ViewState("crapproverid") IsNot Nothing Then
                Dim objClaimMst As New clsclaim_request_master
                If objClaimMst.GetApproverPendingExpenseFormCount(CInt(Me.ViewState("crapproverid")), "") > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "You cannot inactive this approver.Reason :This Approver has Pending Leave Application Form(s)."), Me)
                    'Pinkal (05-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objClaimMst = Nothing
                    'Pinkal (05-Sep-2020) -- End
                    Exit Sub
                End If
                objClaimMst = Nothing

                If objExApprover.InActiveApprover(CInt(Me.ViewState("crapproverid")), CInt(Session("CompanyUnkId")), False) = False Then
                    DisplayMessage.DisplayMessage(objExApprover._Message, Me)
                    Exit Sub
                Else
                    Call FillApprover()
                    Call Fill_List()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            objExApprover = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub popupActive_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupActive.buttonNo_Click, popupInactive.buttonNo_Click
        Try
            If Me.ViewState("crapproverid") IsNot Nothing Then
                Me.ViewState("crapproverid") = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Shani(17-Aug-2015) -- End

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombo As DataSet = Nothing
        Try
            Dim objMaster As New clsMasterData
            dsCombo = objMaster.getComboListTranHeadActiveInActive("List", False)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "1"
            End With


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objMaster = Nothing
            'Pinkal (05-Sep-2020) -- End


            Call cboStatus_SelectedIndexChanged(cboStatus, Nothing)

            Dim dtTable As DataTable = Nothing

            'Pinkal (11-Sep-2019) -- Start
            'Enhancement NMB - Working On Claim Retirement for NMB.

            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)

            If CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
                dtTable = New DataView(dsCombo.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
            Else
                'dsCombo = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True)
                dtTable = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If
            'Pinkal (11-Sep-2019) -- End


            cboExCategory.DataTextField = "Name"
            cboExCategory.DataValueField = "Id"
            cboExCategory.DataSource = dtTable
            cboExCategory.DataBind()


            'Pinkal (24-Dec-2019) -- Start
            'Enhancement - Claim Retirement Enhancements for NMB.
            If Request.QueryString.ToString().Length > 0 Then
                cboExCategory.SelectedValue = mintExpenseCategoryId.ToString()
                cboExCategory.Enabled = False
                'Pinkal (04-Jul-2020) -- Start
                'Enhancement NMB -   Working on Claim Retirement Post to Payroll enhancement.
            Else
                cboExCategory.SelectedValue = "0"
                'Pinkal (04-Jul-2020) -- End
            End If
            'Pinkal (24-Dec-2019) -- End


            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (05-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try

    End Sub

    Private Sub Fill_List(Optional ByVal blnflag As Boolean = False)
        Dim dsList As New DataSet
        Dim dTable As DataTable = Nothing
        Dim StrSearch As String = String.Empty
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExApprover As New clsExpenseApprover_Master
        'Pinkal (05-Sep-2020) -- End
        Try

            If CBool(Session("AllowtoViewExpenseApproverList")) = False Then Exit Sub

            If CInt(cboApprover.SelectedValue) > 0 Then
                StrSearch &= "AND cmexpapprover_master.crapproverunkid = '" & CInt(cboApprover.SelectedValue) & "' "
            End If

            If CInt(cboExCategory.SelectedValue) > 0 Then
                StrSearch &= "AND cmexpapprover_master.expensetypeid = '" & CInt(cboExCategory.SelectedValue) & "' "
            End If

            If CInt(cboLevel.SelectedValue) > 0 Then
                StrSearch &= "AND cmexpapprover_master.crlevelunkid = '" & CInt(cboLevel.SelectedValue) & "' "
            End If

            If StrSearch.Trim.Length > 0 Then
                StrSearch = StrSearch.Substring(3)
            End If


            dsList = objExApprover.GetList("List", _
                                               Session("Database_Name").ToString(), _
                                               CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                               CInt(Session("CompanyUnkId")), _
                                               Session("EmployeeAsOnDate").ToString(), _
                                               Session("UserAccessModeSetting").ToString(), True, _
                                               True, , StrSearch, CBool(IIf(CInt(cboStatus.SelectedValue) = 1, True, False)))


            dTable = New DataView(dsList.Tables("List"), "", "clevel", DataViewRowState.CurrentRows).ToTable


            If IsPostBack = True And blnflag = False Then
                Dim dtTable As DataTable = dTable.Clone
                Dim strLevelName As String = ""
                dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))
                Dim dtRow As DataRow = Nothing
                For Each drow As DataRow In dTable.Rows
                    If CStr(drow("clevel")).Trim <> strLevelName.Trim Then
                        dtRow = dtTable.NewRow
                        strLevelName = drow("clevel").ToString()
                        dtRow("IsGrp") = True
                        dtRow("clevel") = strLevelName
                        dtTable.Rows.Add(dtRow)
                    End If

                    dtRow = dtTable.NewRow
                    For Each dtcol As DataColumn In dTable.Columns
                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next
                GvApprovalList.DataSource = dtTable

                GvApprovalList.DataKeyField = "crapproverunkid"

                Me.ViewState("dtTable") = dtTable
                If CInt(cboStatus.SelectedValue) = 1 Then
                    GvApprovalList.Columns(0).Visible = CBool(Session("AllowtoEditExpenseApprover"))
                    GvApprovalList.Columns(1).Visible = CBool(Session("AllowtoDeleteExpenseApprover"))
                Else
                    GvApprovalList.Columns(0).Visible = False
                    GvApprovalList.Columns(1).Visible = False
                End If
                GvApprovalList.DataBind()

            Else
                Dim dtGvApp As DataTable = dTable.Clone()
                GvApprovalList.DataSource = dtGvApp.Copy
                GvApprovalList.AllowPaging = False
                GvApprovalList.DataBind()

                'Pinkal (05-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtGvApp IsNot Nothing Then dtGvApp.Clear()
                dtGvApp = Nothing
                'Pinkal (05-Sep-2020) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            If dTable IsNot Nothing Then dTable.Clear()
            dTable = Nothing
            objExApprover = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Visible = CBool(Session("AllowtoAddExpenseApprover"))
            GvApprovalList.Columns(0).Visible = CBool(Session("AllowtoEditExpenseApprover"))
            GvApprovalList.Columns(1).Visible = CBool(Session("AllowtoDeleteExpenseApprover"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        End Try
    End Sub
   
    Private Sub FillApprover()
        Dim dsList As DataSet = Nothing
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExApprover As New clsExpenseApprover_Master
        'Pinkal (05-Sep-2020) -- End
        Try
            If CInt(cboStatus.SelectedValue) = 1 Then


                'Gajanan [23-May-2020] -- Start
                'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.

                'dsList = objExApprover.getListForCombo(CInt(cboExCategory.SelectedValue), True, "List", , True)
                dsList = objExApprover.getListForCombo(CInt(cboExCategory.SelectedValue), Session("Database_Name").ToString(), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       Session("EmployeeAsOnDate").ToString(), _
                                                       Session("UserAccessModeSetting").ToString(), _
                                                       True, "List", "", True, True)
                'Gajanan [23-May-2020] -- End

            ElseIf CInt(cboStatus.SelectedValue) = 2 Then


                dsList = objExApprover.getListForCombo(CInt(cboExCategory.SelectedValue), Session("Database_Name").ToString(), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       Session("EmployeeAsOnDate").ToString(), _
                                                       Session("UserAccessModeSetting").ToString(), _
                                                       True, "List", "", False, True)

            End If

            With cboApprover
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objExApprover = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "Control's Events"

    Protected Sub drpExCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExCategory.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            Dim objExLevel As New clsExApprovalLevel
            dsList = objExLevel.getListForCombo(CInt(cboExCategory.SelectedValue), "List", True)
            cboLevel.DataValueField = "Id"
            cboLevel.DataTextField = "Name"
            cboLevel.DataSource = dsList.Tables(0)
            cboLevel.DataBind()
            objExLevel = Nothing

            If CInt(cboExCategory.SelectedValue) <= 0 Then
                Me.ViewState("dtTable") = Nothing
            End If
            GvApprovalList.DataSource = New List(Of String)
            GvApprovalList.DataBind()
            Call FillApprover()

            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'dsList.Dispose()
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (05-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    Private Sub cboStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            If CInt(cboStatus.SelectedValue) = 1 Then
                GvApprovalList.Columns(2).Visible = False
                GvApprovalList.Columns(3).Visible = CBool(Session("AllowSetClaimInactiveApprover"))
            ElseIf CInt(cboStatus.SelectedValue) = 2 Then
                GvApprovalList.Columns(2).Visible = CBool(Session("AllowSetClaimActiveApprover"))
                GvApprovalList.Columns(3).Visible = False
            Else
                GvApprovalList.Columns(2).Visible = False
                GvApprovalList.Columns(3).Visible = False
            End If
            If cboExCategory.SelectedValue.Trim <> "" AndAlso CInt(cboExCategory.SelectedValue) > 0 Then
                Call SetVisibility()
                Call Fill_List()
                Call FillApprover()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " GridView's Event(s) "

    Protected Sub GvApprovalList_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GvApprovalList.DeleteCommand
        'Pinkal (05-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objExApprover As New clsExpenseApprover_Master
        'Pinkal (05-Sep-2020) -- End

        Try
            If objExApprover.isUsed(CInt(GvApprovalList.DataKeys(e.Item.ItemIndex))) = True Then
                DisplayMessage.DisplayMessage(objExApprover._Message, Me)
                Exit Sub
            End If
            Me.ViewState.Add("crapproverunkid", CInt(GvApprovalList.DataKeys(e.Item.ItemIndex)))
            popup_DeleteReason.Title = Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Approver?")
            popup_DeleteReason.Show() 'Nilay (01-Feb-2015) -- popup1.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (05-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            objExApprover = Nothing
            'Pinkal (05-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub GvApprovalList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GvApprovalList.ItemCommand
        Try
            If e.CommandName = "Edit" Then
                Session.Add("CrApproverUnkId", GvApprovalList.DataKeys(e.Item.ItemIndex))
                If CInt(cboExCategory.SelectedValue) > 0 Then
                    Session("Claim_ExpenseUnkID") = cboExCategory.SelectedValue
                End If
                Response.Redirect("~/Claims_And_Expenses/wPg_AddEditApprovalList.aspx", False)

            ElseIf e.CommandName.ToUpper = "ACTIVE" Then
                Me.ViewState("crapproverid") = GvApprovalList.DataKeys(e.Item.ItemIndex)
                popupActive.Title = "Aruti"
                popupActive.Message = "Are you sure you want to set selected approver as Active ?"
                popupActive.Show()

            ElseIf e.CommandName.ToUpper = "INACTIVE" Then
                Me.ViewState("crapproverid") = GvApprovalList.DataKeys(e.Item.ItemIndex)
                popupActive.Title = "Aruti"
                popupInactive.Message = "Are you sure you want to set selected approver as Inactive ?"
                popupInactive.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try

    End Sub

    Protected Sub GvApprovalList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GvApprovalList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Header Then
                e.Item.Cells(5).ColumnSpan = 2
                e.Item.Cells(6).Visible = False
            End If
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then

                If CBool(e.Item.Cells(12).Text) Then
                    CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("lnkActive"), LinkButton).Visible = False
                    CType(e.Item.Cells(3).FindControl("lnkInActive"), LinkButton).Visible = False
                    If CInt(cboStatus.SelectedValue) = 1 Then
                        If CBool(Session("AllowtoEditExpenseApprover")) Then
                            e.Item.Cells(0).Text = e.Item.Cells(6).Text
                            e.Item.Cells(0).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(0).Style.Add("text-align", "left")
                            e.Item.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(1).Visible = False
                            e.Item.Cells(2).Visible = False
                            e.Item.Cells(3).Visible = False
                            e.Item.Cells(4).Visible = False
                        ElseIf CBool(Session("AllowtoDeleteExpenseApprover")) Then
                            e.Item.Cells(1).Text = e.Item.Cells(6).Text
                            e.Item.Cells(1).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(1).Style.Add("text-align", "left")
                            e.Item.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(2).Visible = False
                            e.Item.Cells(3).Visible = False
                            e.Item.Cells(4).Visible = False
                        ElseIf CBool(Session("AllowSetClaimInactiveApprover")) Then
                            e.Item.Cells(3).Text = e.Item.Cells(6).Text
                            e.Item.Cells(3).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(3).Style.Add("text-align", "left")
                            e.Item.Cells(3).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(4).Visible = False
                        Else
                            e.Item.Cells(4).Text = e.Item.Cells(6).Text
                            e.Item.Cells(4).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(4).Style.Add("text-align", "left")
                            e.Item.Cells(4).CssClass = "GroupHeaderStyleBorderLeft"
                        End If
                    ElseIf CInt(cboStatus.SelectedValue) = 2 Then
                        If CBool(Session("AllowSetClaimActiveApprover")) Then
                            e.Item.Cells(2).Text = e.Item.Cells(6).Text
                            e.Item.Cells(2).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(2).Style.Add("text-align", "left")
                            e.Item.Cells(2).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(3).Visible = False
                            e.Item.Cells(4).Visible = False
                        Else
                            e.Item.Cells(4).Text = e.Item.Cells(6).Text
                            e.Item.Cells(4).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(4).Style.Add("text-align", "left")
                            e.Item.Cells(4).CssClass = "GroupHeaderStyleBorderLeft"
                        End If
                    End If

                    For i = 5 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next
                Else
                    e.Item.Cells(5).Attributes.Add("Level", e.Item.Cells(6).Text.Trim)
                    e.Item.Cells(5).ColumnSpan = 2
                    e.Item.Cells(6).Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblDetialHeader.Text)

            Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.ID, Me.lblLevel.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblExpenseCat.Text = Language._Object.getCaption(Me.lblExpenseCat.ID, Me.lblExpenseCat.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")

            GvApprovalList.Columns(2).HeaderText = Language._Object.getCaption(GvApprovalList.Columns(2).FooterText, GvApprovalList.Columns(2).HeaderText)
            GvApprovalList.Columns(3).HeaderText = Language._Object.getCaption(GvApprovalList.Columns(3).FooterText, GvApprovalList.Columns(3).HeaderText)
            GvApprovalList.Columns(4).HeaderText = Language._Object.getCaption(GvApprovalList.Columns(4).FooterText, GvApprovalList.Columns(4).HeaderText)
            GvApprovalList.Columns(5).HeaderText = Language._Object.getCaption(GvApprovalList.Columns(5).FooterText, GvApprovalList.Columns(5).HeaderText)
            GvApprovalList.Columns(6).HeaderText = Language._Object.getCaption(GvApprovalList.Columns(6).FooterText, GvApprovalList.Columns(6).HeaderText)
            GvApprovalList.Columns(7).HeaderText = Language._Object.getCaption(GvApprovalList.Columns(7).FooterText, GvApprovalList.Columns(7).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
  
End Class
