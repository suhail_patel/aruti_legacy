﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_ExpenseApproval.aspx.vb"
    Inherits="Claims_And_Expenses_wPg_ExpenseApproval" Title="Expense Approval Information" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/PreviewAttachment.ascx" TagName="PreviewAttachment"
    TagPrefix="PrviewAttchmet" %>
<%@ Register Src="~/Controls/ViewEmployeeAllocation.ascx" TagName="ViewEmployeeAllocation"
    TagPrefix="ViewEmployeeAllocation" %>
   <%--  'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .--%>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc3" %>
<%--  'Pinkal (22-Oct-2018) -- End--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;
                
            if (charCode == 45)
                return false;
                
            if (charCode == 13)
                return false;
             
            if (charCode == 47) // '/' Not Allow Sign 
                return false; 

            if (charCode > 31  && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Expense Approval Information"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="float: right;">
                                    <asp:LinkButton ID="lnkViewEmployeeAllocation" runat="server" Text="View Employee Allocation" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 35%; border-right: 2px solid #DDD">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblExpCategory" runat="server" Text="Exp.Cat."></asp:Label>
                                                        </td>
                                                        <td style="width: 80%" colspan="3">
                                                            <asp:DropDownList ID="cboExpCategory" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblPeriod" runat="server" Text="Period" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblName" runat="server" Text="Claim No"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true" Visible="false">
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtClaimNo" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 15%">
                                                            <asp:Label ID="lblDate" runat="server" Style="margin-left: 5px" Text="Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 40%">
                                                            <uc2:DateCtrl ID="dtpDate" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                        </td>
                                                        <td style="width: 80%" colspan="3">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" Enabled="False"
                                                                EnableTheming="True">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%">
                                                            <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type"></asp:Label>
                                                        </td>
                                                        <td style="width: 80%" colspan="3">
                                                            <asp:DropDownList ID="cboLeaveType" runat="server" AutoPostBack="true" Enabled="False">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%">
                                                            <asp:Label ID="objlblValue" runat="server" Text="Leave Form"></asp:Label>
                                                        </td>
                                                        <td style="width: 80%" colspan="3">
                                                            <asp:DropDownList ID="cboReference" runat="server" AutoPostBack="true" Enabled="False">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 20%; vertical-align: top">
                                                            <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address"></asp:Label>
                                                        </td>
                                                        <td style="width: 80%;" colspan="3">
                                                            <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                                ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 65%; padding-left: 5px;vertical-align:top">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 12%">
                                                            <asp:Label ID="lblExpense" runat="server" Text="Expense"></asp:Label>
                                                        </td>
                                                        <%--   'Pinkal (04-Feb-2019) -- Start 
                                                              'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                        <td style="width: 88%" colspan="3">
                                                            <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="true" Width="350px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 10%">
                                                            <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%; padding: 0">
                                                            <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                        </td>                                                    
                                                        
                                                            <%--   'Pinkal (04-Feb-2019) -- End --%> 
                                                        
                                                    </tr>
                                                    <%--'Pinkal (20-Nov-2018) -- Start
                                                    'Enhancement - Working on P2P Integration for NMB.--%>
                                                    <%--'Pinkal (30-Apr-2018) - Start
                                                                'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.--%>
                                                    <%--<tr style="width: 100%">
                                                        <td style="width: 13%">
                                                            <asp:Label ID="lblBalanceasondate" runat="server" Text="Balance As on Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 23%">
                                                            <asp:TextBox ID="txtBalanceAsOnDate" runat="server" CssClass="txttextalignright"
                                                                Enabled="false" Style="text-align: right"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 12%">
                                                            <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 12%">
                                                            <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="txtQty" runat="server" Style="text-align: right" Text="0" AutoPostBack="true"
                                                                onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 11%">
                                                            <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="txtCosting" runat="server" Style="text-align: right" Text="0.00"
                                                                Enabled="false" Visible="false"></asp:TextBox>
                                                            <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 12%">
                                                            <asp:Label ID="lblBalance" runat="server" Style="margin-left: 5px" Text="Balance"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="txtBalance" runat="server" Style="text-align: right" Enabled="false"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 12%">
                                                            <asp:Label ID="lblSector" runat="server" Text="Sector/Route"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="width: 11%">
                                                            <asp:Label ID="lblUnitPrice" runat="server" Style="margin-left: 5px" Text="Unit Price"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                            <asp:TextBox ID="txtUnitPrice" runat="server" Style="text-align: right" Text="0"
                                                                onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                                                        </td>
                                                    </tr>--%>
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%">
                                                            <asp:Label ID="lblSector" runat="server" Text="Sector/Route"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%" colspan="3">
                                                            <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true" Width="350px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        
                                                         <%--   'Pinkal (04-Feb-2019) -- Start 
                                                           'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                              
                                                      <%--  <td style="width: 10%">
                                                            <asp:Label ID="lblUoM" runat="server" Text="UoM Type"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%; padding: 0" colspan="1">
                                                            <asp:TextBox ID="txtUoMType" runat="server" Enabled="false"></asp:TextBox>
                                                        </td>--%>
                                                        
                                                        <td style="width: 10%">
                                                            <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%; padding: 0" colspan="1">
                                                            <asp:TextBox ID="txtQty" runat="server" Style="text-align: right" Text="0" AutoPostBack="true"
                                                                onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                                                        </td>
                                                      
                                                      <%--   'Pinkal (04-Feb-2019) -- End --%>
                                                      
                                                    </tr>
                                                    
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%">
                                                            <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center"></asp:Label>
                                                        </td>
                                                        <td style="width: 50%" colspan="3">
                                                            <asp:DropDownList ID="cboCostCenter" runat="server" AutoPostBack="true" Width="350px">
                                                            </asp:DropDownList>
                                                        </td>
                                                        
                                                       <%--   'Pinkal (04-Feb-2019) -- Start 
                                                                   'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                        
                                                     <%--  <td style="width: 10%">
                                                            <asp:Label ID="lblQty" runat="server" Text="Qty"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%; padding: 0" colspan="1">
                                                            <asp:TextBox ID="txtQty" runat="server" Style="text-align: right" Text="0" AutoPostBack="true"
                                                                onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                                                        </td>--%>
                                                        
                                                        <td style="width: 13%">
                                                            <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                            <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%">
                                                                <%--'Pinkal (07-Feb-2020) -- Start
                                                                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB 
                                                                <asp:TextBox ID="txtUnitPrice" runat="server" Style="text-align: right" Text="0" onKeypress="return onlyNumbers(this,event);"/>--%> 
                                                                <asp:TextBox ID="txtUnitPrice" runat="server" Style="text-align: right" Text="1.00" onKeypress="return onlyNumbers(this,event);"/>
                                                                  <%--  'Pinkal (07-Feb-2019) -- End --%>
                                                                <asp:TextBox ID="txtCosting" runat="server" Style="text-align: right" Text="0.00" Enabled="false" Visible="false"/>
                                                                <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                        </td>
                                                        
                                                      <%--   'Pinkal (04-Feb-2019) -- End --%> 
                                                        
                                                    </tr>
                                                    
                                                    <tr style="width: 100%">
                                                    <td style="width: 14%">
                                                        <asp:Label ID="lblBalanceasondate" runat="server" Text="Balance As on Date"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                         <asp:TextBox ID="txtBalanceAsOnDate" runat="server" CssClass="txttextalignright"
                                                          Enabled="false" Style="text-align: right"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 11%">
                                                        <asp:Label ID="lblBalance" runat="server" Text="Balance"></asp:Label>
                                                    </td>
                                                    <td style="width: 15%">
                                                        <asp:TextBox ID="txtBalance" runat="server" Style="text-align: right" Enabled="false"></asp:TextBox>
                                                    </td>
                                                    
                                                    <%--   'Pinkal (04-Feb-2019) -- Start 
                                                                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                    
                                                  <%--  <td style="width: 13%">
                                                    <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price"></asp:Label>
                                                        <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                    <asp:TextBox ID="txtUnitPrice" runat="server" Style="text-align: right" Text="0"
                                                            onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                                                        <asp:TextBox ID="txtCosting" runat="server" Style="text-align: right" Text="0.00"
                                                            Enabled="false" Visible="false"></asp:TextBox>
                                                        <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                    </td>--%>
                                                    
                                                     <td style="width: 13%">
                                                            <asp:Label ID="LblCurrency" runat="server" Text="Currency"></asp:Label>
                                                    </td>
                                                    <td style="width: 20%">
                                                            <asp:DropDownList ID="cboCurrency" runat="server"></asp:DropDownList>
                                                    </td>
                                                    
                                                    
                                                       <%--   'Pinkal (04-Feb-2019) -- End --%> 
                                                    
                                                    </tr>
                                                    
                                                    
                                                    <tr style="width: 100%">
                                                        <td colspan="5">
                                                            <cc1:TabContainer ID="tabRemarks" runat="server" ActiveTabIndex="0" Width="100%"
                                                                >
                                                                <cc1:TabPanel ID="tbExpenseRemark" runat="server" HeaderText="Expense Remark">
                                                                    <ContentTemplate>
                                                                        <asp:TextBox ID="txtExpRemark" runat="server" Width="98%" TextMode="MultiLine"></asp:TextBox></ContentTemplate>
                                                                </cc1:TabPanel>
                                                                <cc1:TabPanel ID="tbClaimRemark" runat="server" HeaderText="Claim Remark">
                                                                    <ContentTemplate>
                                                                        <asp:TextBox ID="txtClaimRemark" runat="server" Width="98%" TextMode="MultiLine"></asp:TextBox></ContentTemplate>
                                                                </cc1:TabPanel>
                                                            </cc1:TabContainer>
                                                        </td>
                                                       <%--  <td colspan="2" style="width: 50%" valign="top">
                                                                <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address" ></asp:Label>
                                                            <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                                Style="height: 53px;" ReadOnly="true"></asp:TextBox>
                                                        </td>--%>
                                                    <td style="width: 15%;vertical-align: bottom;" align="right">
                                                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btnDefault" />
                                                            <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btnDefault" Visible="false" />
                                                        </td>
                                                        <%--'Pinkal (20-Nov-2018) -- End--%>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <asp:Panel ID="pnl_dgvData" ScrollBars="Auto" Height="350px" runat="server">
                                                    <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false" ShowFooter="False" Width="99%">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                                ItemStyle-Width="30px">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit"></asp:LinkButton>
                                                                    </span>
                                                                    <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" ToolTip="Edit"
                                                                        CommandName="Edit" />--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                                ItemStyle-Width="30px">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete"
                                                                            CssClass="griddelete"></asp:LinkButton>
                                                                    </span>
                                                                    <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                                        CommandName="Delete" />--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="25px" ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkPreview" runat="server" ToolTip="Preview" Font-Underline="false"
                                                                        CommandName="Preview">
                                                                            <i class="fa fa-eye" aria-hidden="true" style="font-size:20px;"></i>                                                                                              
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="expense" HeaderText="Claim/Expense Desc" ReadOnly="true"
                                                                FooterText="dgcolhExpense" />
                                                            <asp:BoundColumn DataField="sector" HeaderText="Sector / Route" ReadOnly="true" FooterText="dgcolhSectorRoute" />
                                                            <asp:BoundColumn DataField="uom" HeaderText="UoM" ReadOnly="true" FooterText="dgcolhUoM" />
                                                            <asp:BoundColumn DataField="quantity" HeaderText="Quantity" ReadOnly="true" FooterText="dgcolhQty"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                            <asp:BoundColumn DataField="unitprice" HeaderText="Unit Price" ReadOnly="true" FooterText="dgcolhUnitPrice"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                            <asp:BoundColumn DataField="amount" HeaderText="Amount" ReadOnly="true" FooterText="dgcolhAmount"
                                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                                                            <asp:BoundColumn DataField="expense_remark" HeaderText="Expense Remark" ReadOnly="true"
                                                                Visible="true" FooterText="dgcolhExpenseRemark" />
                                                            <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" ReadOnly="true" Visible="false" />
                                                        </Columns>
                                                        <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <div align="right">
                                                    <asp:Label ID="lblGrandTotal" Style="margin-right: 5px" runat="server" Text="Grand Total"></asp:Label>
                                                    <asp:TextBox ID="txtGrandTotal" runat="server" Style="text-align: right" Enabled="False"
                                                        Width="20%"></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnViewAttchment" runat="server" Visible="false" Text="View Scan/Attchment"
                                            CssClass="btndefault" />
                                        <%--'Pinkal (20-Nov-2020) -- Start
                                        'Enhancement KADCO -   Working on Fuel Consumption Report required by Kadco.     --%>
                                        <asp:LinkButton ID="lnkShowFuelConsumptionReport" runat="server" Visible="false" Text="Show Monthly Fuel Consumption Report" CssClass="lnkhover" style="margin-right:47%" />    
                                         <%--'Pinkal (20-Nov-2020) -- End--%>
                                         
                                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btndefault" />
                                        <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btndefault" />
                                        <asp:Button ID="btnOK" runat="server" Text="Ok" CssClass="btndefault" Visible="false" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                                <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                <PrviewAttchmet:PreviewAttachment ID="popup_ShowAttchment" runat="server" />
                                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                            </div>
                        </div>
                    </div>
                    <asp:Panel ID="pnlReject" runat="server" CssClass="newpopup" Style="display: none;
                        width: 450px" DefaultButton="btnRemarkOk">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblTitle" runat="server" Text="Rejection Remark"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 100%">
                                                    <asp:TextBox ID="txtRejectRemark" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnRemarkOk" runat="server" Text="Ok" CssClass="btnDefault" />
                                            <asp:Button ID="btnRemarkClose" runat="server" Text="Close" CssClass="btnDefault" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <cc1:ModalPopupExtender ID="popupRejectRemark" runat="server" BackgroundCssClass="modalBackground"
                        CancelControlID="btnRemarkClose" DropShadow="true" PopupControlID="pnlReject"
                        TargetControlID="txtRejectRemark">
                    </cc1:ModalPopupExtender>
                    <%--<PrviewAttchmet:PreviewAttachment ID="popup_ShowAttchment" runat="server" />--%>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To delete?:" />
                         <%--  'Pinkal (22-Oct-2018) -- Start
                         'Enhancement - Implementing Claim & Request changes For NMB .--%>
                    <uc3:Confirmation ID="popup_UnitPriceYesNo" runat="server" Title="Confirmation" Message="" />
                        <%--  'Pinkal (22-Oct-2018) -- End --%>
                
                     <%--  'Pinkal (04-Feb-2019) -- Start
                   'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                    <uc3:Confirmation ID="popup_ExpRemarkYesNo" runat="server" Message="" Title="Confirmation" />
                    <%--  'Pinkal (04-Feb-2019) -- End --%>
                    <ViewEmployeeAllocation:ViewEmployeeAllocation ID="ViewEmployeeAllocation" runat="server" />
                </ContentTemplate>
                <%--'S.SANDEEP |25-JAN-2019| -- START--%>
                <%--'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}--%>
                <Triggers>
                    <asp:PostBackTrigger ControlID="popup_ShowAttchment" />
                </Triggers>
                <%--'S.SANDEEP |25-JAN-2019| -- END--%>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
