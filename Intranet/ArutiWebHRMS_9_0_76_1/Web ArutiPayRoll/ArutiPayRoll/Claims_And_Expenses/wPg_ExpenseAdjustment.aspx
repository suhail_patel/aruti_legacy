﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/home.master" CodeFile="wPg_ExpenseAdjustment.aspx.vb"
    Title="Employee Expense Adjustment" Inherits="Claims_And_Expenses_wPg_ExpenseAdjustment" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="Allocation" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }

        $("[id*=chkselectAll]").live("click", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("input[type=checkbox]", grid).each(function() {
                if (chkHeader.is(":checked")) {
                    debugger;
                    if ($(this).is(":visible")) {
                        $(this).attr("checked", "checked");
                    }
                } else {
                    $(this).removeAttr("checked");
                }
            });
        });
        $("[id*=chkselect]").live("click", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeader]", grid);
            var row = $(this).closest("tr")[0];

            debugger;
            if (!$(this).is(":checked")) {
                var row = $(this).closest("tr")[0];
                chkHeader.removeAttr("checked");
            } else {

                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                    chkHeader.attr("checked", "checked");
                }
            }
        });
        
    </script>
    
    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 45)
            if (cval.indexOf("-") > -1)
                   return false;

            if (charCode == 13)
                return false;

            if (charCode == 47) // '/' Not Allow Sign 
                return false;

            if (charCode > 31 && (charCode < 44 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <center>
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="panel-primary" style="width: 95%;">
                    <div class="panel-heading">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Expense Adjustment"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </div>
                                 <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocation" CssClass="lnkhover"></asp:LinkButton>
                                 </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <div class="row2">
                                    <div class="ib" style="width: 50%; vertical-align: top; border-right: 1px solid #ccc;
                                        height: 100%">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 20%">
                                                <asp:Label ID="LblExpenseCategory" runat="server" Text="Category"></asp:Label>
                                            </div>
                                            <div class="ibwm" style="width: 75%">
                                                <asp:DropDownList ID="cboExpenseCategory" runat="server" AutoPostBack="true" />
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="ibwm" style="width: 20%">
                                                <asp:Label ID="LblExpense" runat="server" Text="Expense"></asp:Label>
                                            </div>
                                            <div class="ibwm" style="width: 75%">
                                                <asp:DropDownList ID="cboExpense" runat="server" />
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="ibwm" style="width: 20%">
                                                <asp:Label ID="LblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </div>
                                            <div class="ibwm" style="width: 75%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="ibwm" style="width: 48%;">
                                        <div class="row2">
                                            <div class="ibwm" style="width: 100%">
                                                <asp:RadioButtonList ID="rdOption" runat="server" RepeatDirection="Vertical">
                                                    <asp:ListItem Value="1" Text="Apply to All"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Apply to checked"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="ibwm" style="width: 10%;">
                                                <asp:Label ID="LblDate" runat="server" Text="Date"></asp:Label>
                                            </div>
                                            <div class="ibwm" style="width: 30%">
                                                <uc1:DateCtrl ID="dtpDate" runat="server" Width = "95" />
                                            </div>
                                            <div class="ibwm" style="width: 30%;">
                                                <asp:Label ID="lblAdjustmentAmount" runat="server" Text="Adjustment Amount"></asp:Label>
                                            </div>
                                            <div class="ibwm" style="width: 27%;">
                                                <asp:TextBox ID="txtAdjustmentAmount" runat="server" style="text-align:right" Width="90%" onKeypress="return onlyNumbers(this,event);"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="btn-default">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                    <asp:Button ID="btnSet" runat="server" Text="Set" CssClass="btndefault" />
                                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                        <div id="Div1" class="panel-default">
                            <div id="Div3" class="panel-body-default">
                                <asp:Panel ID="pnlEmpDetails" runat="server" Width="100%" Height="220px" ScrollBars="Auto">
                                    <asp:GridView ID="dgEmployee" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                        DataKeyNames="employeeunkid,crexpbalanceunkid,Isopenelc,Iselc,Isclose_Fy" HeaderStyle-Font-Bold="false"
                                        Width="98%">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="25">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkselectAll" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgColhEmployee" />
                                            <asp:BoundField DataField="appointeddate" HeaderText="Appointment Date" FooterText="dgColhAppointmentDt" />
                                            <asp:BoundField DataField="Job" HeaderText="Job Title" FooterText="dgColhJobtitle" />
                                            <asp:BoundField DataField="Accrue_amount" HeaderText="Accrue Amount" FooterText="dgColhAccrueAmt"
                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="Issue_amount" HeaderText="Issue Amount" FooterText="dgcolhIssueAmt"
                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="Remaining_Bal" HeaderText="Remaining Balance" FooterText="dgcolhRemaining_Bal"
                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                            <asp:BoundField DataField="Adjustment_Amt" HeaderText="Adjustment Amount" FooterText="dgcolhAdjustmentAmt"
                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                                 <div class="row2">
                                     <div class="ibwm" style="width: 15%;">
                                             <asp:Label ID="lblRemark" runat="server" Text="Remark"></asp:Label>
                                     </div>
                                      <div class="ibwm" style="width: 80%;">
                                            <asp:TextBox ID="txtRemark" runat="server" Text= "" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                      </div>
                                 </div>  
                                <div class="btn-default">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <uc2:Allocation ID = "popupAdvanceFilter" runat ="server" Title= "Advance Filter" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</asp:Content>
