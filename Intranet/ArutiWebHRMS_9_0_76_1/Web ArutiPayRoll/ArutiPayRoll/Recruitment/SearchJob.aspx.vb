﻿#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Net.Dns
Imports System.Data
Imports System.IO

Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.tool.xml

#End Region

Partial Class Recruitment_SearchJob
    Inherits Basepage

#Region " Private Variable(s) "
    Dim DisplayMessage As New CommonCodes
    Private blnHideApply As Boolean = False
    Private Shared ReadOnly mstrModuleName As String = "frmSearchJob"

    Dim idIndex As Integer = 0
#End Region

#Region " Method Functions "

    Private Sub GetApplicantIDByEmployeeID()
        Dim objApplicant As New clsApplicant_master
        Try
            If Session("applicantunkid") Is Nothing OrElse CInt(Session("applicantunkid")) <= 0 Then
                Session("applicantunkid") = objApplicant.GetApplicantIDByEmployeeID(CInt(Session("employeeunkid")))
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillVacancyList(ByVal strDatabaseName As String _
                                , ByVal strExtInt As String _
                                )
        Try

            'odsVacancy.SelectParameters.Item("strCompCode").DefaultValue = strCompCode
            'odsVacancy.SelectParameters.Item("intComUnkID").DefaultValue = intCompanyunkid
            odsVacancy.SelectParameters.Item("strDatabaseName").DefaultValue = strDatabaseName
            odsVacancy.SelectParameters.Item("intMasterTypeId").DefaultValue = clsCommon_Master.enCommonMaster.VACANCY_MASTER
            odsVacancy.SelectParameters.Item("intEType").DefaultValue = clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE
            If strExtInt = "9" Then
                odsVacancy.SelectParameters.Item("blnVacancyType").DefaultValue = False
            Else
                odsVacancy.SelectParameters.Item("blnVacancyType").DefaultValue = True
            End If
            odsVacancy.SelectParameters.Item("blnAllVacancy").DefaultValue = False
            odsVacancy.SelectParameters.Item("intDateZoneDifference").DefaultValue = 0
            odsVacancy.SelectParameters.Item("strVacancyUnkIdLIs").DefaultValue = ""
            odsVacancy.SelectParameters.Item("intApplicantUnkId").DefaultValue = CInt(Session("applicantunkid"))
            odsVacancy.SelectParameters.Item("blnOnlyCurrent").DefaultValue = True
            odsVacancy.SelectParameters.Item("blnOnlyExportToWeb").DefaultValue = True 'Sohail (14 Oct 2020)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Dim dsList As DataSet
        Dim strMsg As String = ""
        Dim objEmp As New clsEmployee_Master
        Try
            objEmp._Employeeunkid(DateAndTime.Now.Date, Nothing) = CInt(Session("Employeeunkid"))
            With objEmp
                'dsList = (New clsPersonalInfo).GetPersonalInfo(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
                'If dsList.Tables(0).Rows.Count > 0 Then

                If ._Firstname.ToString.Length <= 0 Then
                    strMsg &= "\n First Name"
                End If
                If ._Othername.ToString.Length <= 0 AndAlso CBool(Session("MiddleNameMandatory")) = True Then
                    strMsg &= "\n Other Name"
                End If
                If ._Surname.ToString.Length <= 0 Then
                    strMsg &= "\n Surname"
                End If
                If CInt(._Gender) <= 0 AndAlso CBool(Session("GenderMandatory")) = True Then
                    strMsg &= "\n Gender"
                End If
                If ((._Birthdate = Nothing OrElse CDate(._Birthdate) = CDate("01/01/1900")) AndAlso CBool(Session("BirthDateMandatory")) = True) Then
                    strMsg &= "\n Birth Date"
                End If
                If ._Present_Mobile.ToString.Length <= 0 Then
                    strMsg &= "\n Mobile No."
                End If
                If CInt(._Nationalityunkid) <= 0 AndAlso CBool(Session("NationalityMandatory")) = True Then
                    strMsg &= "\n Nationality"
                End If
                'Else
                '    strMsg &= "\n Personal Information"
                '    'GoTo Redirect
                'End If

                'dsList = (New clsContactDetails).GetContactDetail(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")))
                'If dsList.Tables(0).Rows.Count > 0 Then
                If ._Present_Address1.ToString.Length <= 0 AndAlso CBool(Session("Address1Mandatory")) = True Then
                    strMsg &= "\n Current Address 1"
                    'GoTo Redirect
                End If
                If ._Present_Address2.ToString.Length <= 0 AndAlso CBool(Session("Address2Mandatory")) = True Then
                    strMsg &= "\n Current Address 2"
                End If
                If ._Present_Provicnce.ToString.Length <= 0 AndAlso CBool(Session("RegionMandatory")) = True Then
                    strMsg &= "\n Current Region"
                End If
                If CInt(._Present_Countryunkid) <= 0 AndAlso CBool(Session("CountryMandatory")) = True Then
                    strMsg &= "\n Current Country"
                End If
                If CInt(._Present_Stateunkid) <= 0 AndAlso CBool(Session("StateMandatory")) = True Then
                    strMsg &= "\n Current State"
                End If
                If CInt(._Present_Post_Townunkid) <= 0 AndAlso CBool(Session("CityMandatory")) = True Then
                    strMsg &= "\n Current Post Town/City"
                End If
                If CInt(._Present_Postcodeunkid) <= 0 AndAlso CBool(Session("PostCodeMandatory")) = True Then
                    strMsg &= "\n Current Post Code"
                End If
                'Else
                '    strMsg &= "\n Current Address 1"
                '    'GoTo Redirect
                'End If


                'dsList = (New clsOtherInfo).GetOtherInfo(Session("CompCode").ToString, CInt(Session("companyunkid")), CInt(Session("applicantunkid")), clsCommon_Master.enCommonMaster.LANGUAGES, clsCommon_Master.enCommonMaster.MARRIED_STATUS)
                'If dsList.Tables(0).Rows.Count > 0 Then
                If CInt(objEmp._Language1unkid) <= 0 AndAlso CBool(Session("Language1Mandatory")) = True Then
                    strMsg &= "\n Language 1"
                    'GoTo Redirect
                End If
                If CInt(objEmp._Maritalstatusunkid) <= 0 AndAlso CBool(Session("MaritalStatusMandatory")) = True Then
                    strMsg &= "\n Marital Status"
                    'GoTo Redirect
                End If

                'Else
                '    strMsg &= "\n Marital Status"
                '    'GoTo Redirect
                'End If
            End With

            dsList = (New clsEmployee_Skill_Tran).GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), DateAndTime.Now.Date, DateAndTime.Now.Date, CStr(Session("UserAccessModeSetting")), True, False, "List", False, " hremp_app_skills_tran.emp_app_unkid = " & CInt(Session("Employeeunkid")) & " ", False, False)
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneSkillMandatory")) = True Then
                strMsg &= "\n Skills. <br/>"
            End If

            dsList = (New clsEmp_Qualification_Tran).GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), DateAndTime.Now.Date, DateAndTime.Now.Date, CStr(Session("UserAccessModeSetting")), True, False, "List", False, " hremp_qualification_tran.employeeunkid = " & CInt(Session("Employeeunkid")) & " ", False, False)
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneQualificationMandatory")) = True Then
                strMsg &= "\n Atleast one Qualification"
                'GoTo Redirect
            End If
            If dsList.Tables(0).Rows.Count > 0 Then
                If CBool(Session("HighestQualificationMandatory")) = True AndAlso dsList.Tables(0).Select("ishighestqualification =  1 ").Length <= 0 Then
                    strMsg &= "\n Highest Qualification"
                    'GoTo Redirect
                End If
            End If

            'Sohail (11 Nov 2021) -- Start
            'NMB Enhancement: Attachments for applicants should be restricted to only CV and Cover letter on portal. Applicants should not see other document types.
            'Dim dtTable As DataTable = (New clsScan_Attach_Documents).GetQulificationAttachment(CInt(Session("Employeeunkid")), enScanAttactRefId.QUALIFICATIONS, 0, CStr(Session("Document_Path")))
            'If dtTable.Rows.Count <= 0 AndAlso CBool(Session("isQualiCertAttachMandatory")) = True Then
            '    strMsg &= "\n Atleast one Qualification Attachement"
            '    'GoTo Redirect
            'End If
            'Sohail (11 Nov 2021) -- End

            dsList = (New clsJobExperience_tran).GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), DateAndTime.Now.Date, DateAndTime.Now.Date, CStr(Session("UserAccessModeSetting")), True, False, "List", False, " hremp_experience_tran.employeeunkid = " & CInt(Session("Employeeunkid")) & " ", False, False)
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneJobExperienceMandatory")) = True Then
                strMsg &= "\n Job Experiences. <br/>"
            End If

            dsList = (New clsEmployee_Refree_tran).GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), DateAndTime.Now.Date, DateAndTime.Now.Date, CStr(Session("UserAccessModeSetting")), True, False, "List", False, False, " hremployee_referee_tran.employeeunkid = " & CInt(Session("Employeeunkid")) & " ", False, False)
            If dsList.Tables(0).Rows.Count <= 0 AndAlso CBool(Session("OneReferenceMandatory")) = True Then
                strMsg &= "\n References. <br/>"
            End If

            'Redirect:
            If strMsg.Trim <> "" Then
                DisplayMessage.DisplayMessage("Sorry, Your profile is incomplete. \n Please fill required details." & strMsg, Me)
                HttpContext.Current.Response.Redirect("~/User/UserHome.aspx", False)
                Return False
            Else
                Return True
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    Private Function GetApplicantCV() As String
        Dim strBuilder As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master
        Dim objMaster As New clsMasterData
        Dim objState As New clsstate_master
        Dim objCity As New clscity_master
        Dim objZipcode As New clszipcode_master
        Dim strTitle As String = ""
        Dim strName As String = ""
        Dim strEmail As String = ""
        Dim strGender As String = ""
        Dim strBirthDate As String = ""
        Dim strMaritalStatus As String = ""
        Dim strMarriedDate As String = ""
        Dim strNationality As String = ""
        Dim strLanguagesKnown As String = ""
        Dim strMotherTongue As String = ""
        Dim strImpaired As String = ""
        Dim strCurrentSalary As String = ""
        Dim strExpectedSalary As String = ""
        Dim strExpectedBenefits As String = ""
        Dim strWillingToRelocate As String = ""
        Dim strWillingToTravel As String = ""
        Dim strNoticePeriodDays As String = ""
        Dim strSkills As String = ""
        Dim strPresentAddress As String = ""
        Dim strPresentAddress2 As String = ""
        Dim strPermenantAddress As String = ""
        Dim strPermenantAddress2 As String = ""
        Dim strMemberships As String = ""
        Dim strAchievements As String = ""
        Dim strJournalResearchPaper As String = ""
        Dim mstrFilePath As String = ""
        Try

            Dim dsTitle As DataSet = objCommon.getComboList(clsCommon_Master.enCommonMaster.TITLE, True, "List")
            Dim dsGender As DataSet = objMaster.getGenderList()
            Dim dsLang As DataSet = objCommon.getComboList(clsCommon_Master.enCommonMaster.LANGUAGES, True, "List")
            Dim dsMarital As DataSet = objCommon.getComboList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, True, "List")

            objEmp._Employeeunkid(DateAndTime.Now.Date) = CInt(Session("Employeeunkid"))

            strName = objEmp._Firstname.ToString & " " & objEmp._Othername.ToString & " " & objEmp._Surname.ToString
            strEmail = Language.getMessage(mstrModuleName, 1, "Email: ") & objEmp._Email.ToString & ", " & Language.getMessage(mstrModuleName, 2, "Mobile: ") & objEmp._Present_Mobile.ToString
            strBirthDate = CDate(objEmp._Birthdate).ToString("dd-MMM-yyyy")

            If CInt(objEmp._Titalunkid) > 0 Then
                If dsTitle.Tables(0).Select("masterunkid = " & CInt(objEmp._Titalunkid) & " ").Length > 0 Then
                    strTitle = dsTitle.Tables(0).Select("masterunkid = " & CInt(objEmp._Titalunkid) & " ")(0).Item("Name").ToString
                End If
            End If

            If CInt(objEmp._Gender) > 0 Then
                strGender = dsGender.Tables(0).Select("Id = " & CInt(objEmp._Gender) & " ")(0).Item("Name").ToString
            End If

            '*** Tel. No
            If objEmp._Present_Tel_No.ToString.Trim.Length > 0 Then
                strEmail &= ", " & Language.getMessage(mstrModuleName, 3, "Tel. No.: ") & objEmp._Present_Tel_No.ToString.Trim
            End If
            '*** Fax
            If objEmp._Present_Fax.ToString.Trim.Length > 0 Then
                strEmail &= ", " & Language.getMessage(mstrModuleName, 4, "Fax: ") & objEmp._Present_Fax.ToString.Trim
            End If

            '*** Current Address
            strPresentAddress = objEmp._Present_Address1.ToString.Trim

            If objEmp._Present_Address2.ToString.Trim.Length > 0 Then
                strPresentAddress &= ", " & objEmp._Present_Address2.ToString.Trim
            End If

            If objEmp._Present_Plotno.ToString.Trim.Length > 0 Then
                strPresentAddress &= ", " & objEmp._Present_Plotno.ToString.Trim
            End If

            If objEmp._Present_Estate.ToString.Trim.Length > 0 Then
                strPresentAddress &= ", " & objEmp._Present_Estate.ToString.Trim
            End If

            If objEmp._Present_Road.ToString.Trim.Length > 0 Then
                strPresentAddress &= ", " & objEmp._Present_Road.ToString.Trim
            End If

            If objEmp._Present_Provicnce.ToString.Trim.Length > 0 Then
                strPresentAddress &= ", " & objEmp._Present_Provicnce.ToString.Trim
            End If

            If CInt(objEmp._Present_Countryunkid) > 0 Then
                Dim dsCountry As DataSet = objMaster.getCountryList("List", False, objEmp._Present_Countryunkid)
                If dsCountry.Tables(0).Rows.Count > 0 Then
                    strPresentAddress2 &= ", " & dsCountry.Tables(0).Rows(0).Item("country_name").ToString.Trim
                End If
            End If

            If CInt(objEmp._Present_Stateunkid) > 0 Then
                objState._Stateunkid = objEmp._Present_Stateunkid
                strPresentAddress2 &= ", " & objState._Name.ToString.Trim
            End If

            If CInt(objEmp._Present_Post_Townunkid) > 0 Then
                objCity._Cityunkid = objEmp._Present_Post_Townunkid
                strPresentAddress2 &= ", " & objCity._Name.ToString.Trim
            End If

            If objEmp._Present_Postcodeunkid.ToString.Trim.Length > 0 Then
                objZipcode._Zipcodeunkid = objEmp._Present_Postcodeunkid
                strPresentAddress2 &= ", " & objZipcode._Zipcode_No.ToString.Trim
            End If

            If strPresentAddress2.Trim.Length > 0 Then
                strPresentAddress2 = strPresentAddress2.Substring(2)
            End If

            '*** Permenant Address
            If objEmp._Domicile_Address1.ToString.Trim.Length > 0 Then
                strPermenantAddress &= ", " & objEmp._Domicile_Address1.ToString.Trim
            End If

            If objEmp._Domicile_Address2.ToString.Trim.Length > 0 Then
                strPermenantAddress &= ", " & objEmp._Domicile_Address2.ToString.Trim
            End If

            If objEmp._Domicile_Plotno.ToString.Trim.Length > 0 Then
                strPermenantAddress &= ", " & objEmp._Domicile_Plotno.ToString.Trim
            End If

            If objEmp._Domicile_Estate.ToString.Trim.Length > 0 Then
                strPermenantAddress &= ", " & objEmp._Domicile_Estate.ToString.Trim
            End If

            If objEmp._Domicile_Road.ToString.Trim.Length > 0 Then
                strPermenantAddress &= ", " & objEmp._Domicile_Road.ToString.Trim
            End If

            If objEmp._Domicile_Provicnce.ToString.Trim.Length > 0 Then
                strPermenantAddress &= ", " & objEmp._Domicile_Provicnce.ToString.Trim
            End If

            If CInt(objEmp._Domicile_Countryunkid) > 0 Then
                Dim dsCountry As DataSet = objMaster.getCountryList("List", False, objEmp._Domicile_Countryunkid)
                If dsCountry.Tables(0).Rows.Count > 0 Then
                    strPermenantAddress2 &= ", " & dsCountry.Tables(0).Rows(0).Item("country_name").ToString.Trim
                End If
            End If

            If CInt(objEmp._Domicile_Stateunkid) > 0 Then
                objState._Stateunkid = objEmp._Domicile_Stateunkid
                strPermenantAddress2 &= ", " & objState._Name.ToString.Trim
            End If

            If CInt(objEmp._Domicile_Post_Townunkid) > 0 Then
                objCity._Cityunkid = objEmp._Domicile_Post_Townunkid
                strPermenantAddress2 &= ", " & objCity._Name.ToString.Trim
            End If

            If objEmp._Domicile_Postcodeunkid.ToString.Trim.Length > 0 Then
                objZipcode._Zipcodeunkid = objEmp._Domicile_Postcodeunkid
                strPermenantAddress2 &= ", " & objZipcode._Zipcode_No.ToString.Trim
            End If



            If objEmp._Domicile_Tel_No.ToString.Trim.Length > 0 Then
                strPermenantAddress2 &= ", " & Language.getMessage(mstrModuleName, 3, "Tel. No.: ") & objEmp._Domicile_Tel_No.ToString.Trim
            End If

            If objEmp._Domicile_Fax.ToString.Trim.Length > 0 Then
                strPermenantAddress2 &= ", " & Language.getMessage(mstrModuleName, 4, "Fax: ") & objEmp._Domicile_Fax.ToString.Trim
            End If


            '*** Other Info

            If CInt(objEmp._Language1unkid) > 0 Then
                strLanguagesKnown &= ", " & dsLang.Tables(0).Select("masterunkid = " & CInt(objEmp._Language1unkid) & " ")(0).Item("Name").ToString
            End If
            If CInt(objEmp._Language2unkid) > 0 Then
                strLanguagesKnown &= ", " & dsLang.Tables(0).Select("masterunkid = " & CInt(objEmp._Language2unkid) & " ")(0).Item("Name").ToString
            End If
            If CInt(objEmp._Language3unkid) > 0 Then
                strLanguagesKnown &= ", " & dsLang.Tables(0).Select("masterunkid = " & CInt(objEmp._Language3unkid) & " ")(0).Item("Name").ToString
            End If
            If CInt(objEmp._Language4unkid) > 0 Then
                strLanguagesKnown &= ", " & dsLang.Tables(0).Select("masterunkid = " & CInt(objEmp._Language4unkid) & " ")(0).Item("Name").ToString
            End If

            If strLanguagesKnown.Trim.Length > 0 Then
                strLanguagesKnown = strLanguagesKnown.Substring(2)
            End If

            If CInt(objEmp._Nationalityunkid) > 0 Then
                Dim dsCountry As DataSet = objMaster.getCountryList("List", False, objEmp._Nationalityunkid)
                If dsCountry.Tables(0).Rows.Count > 0 Then
                    strNationality = dsCountry.Tables(0).Rows(0).Item("country_name").ToString.Trim & "n"
                End If
            End If

            If CInt(objEmp._Maritalstatusunkid) > 0 Then
                strMaritalStatus = dsMarital.Tables(0).Select("masterunkid = " & CInt(objEmp._Maritalstatusunkid) & " ")(0).Item("Name").ToString
            End If

            If objEmp._Anniversary_Date <> Nothing AndAlso CDate(objEmp._Anniversary_Date).Date <> CDate("01/Jan/1900") Then
                strMarriedDate = CDate(objEmp._Anniversary_Date).ToString("dd-MMM-yyyy")
            End If

            'If objEmp,Mother_Tongue.ToString.Trim.Length > 0 Then
            '    strMotherTongue = objEmp,Mother_Tongue.ToString
            'End If

            'If CBool(objEmp._Isimpaired) = True Then
            '    strImpaired = "Yes <BR />" & objEmp._Isimpaired.ToString
            'Else
            '    strImpaired = "No"
            'End If

            Dim dsScale As DataSet = objMaster.Get_Current_Scale("Scale", CInt(Session("employeeunkid")), DateAndTime.Now.Date)
            If dsScale.Tables("Scale").Rows.Count > 0 Then
                strCurrentSalary = Format(CDec(dsScale.Tables("Scale").Rows(0).Item("newscale")), "0.00")
            Else
                If CDec(objEmp._Scale) <> 0 Then
                    strCurrentSalary = Format(CDec(objEmp._Scale), "0.00")
                End If
            End If

            'If CDec(dsOtherInfo.Tables(0).Rows(0).Item("expected_salary")) <> 0 Then
            '    strExpectedSalary = Format(CDec(dsOtherInfo.Tables(0).Rows(0).Item("expected_salary").ToString), "0.00")
            'End If

            'If dsOtherInfo.Tables(0).Rows(0).Item("expected_benefits").ToString.Trim.Length > 0 Then
            '    strExpectedBenefits = dsOtherInfo.Tables(0).Rows(0).Item("expected_benefits").ToString
            'End If

            'If CBool(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_relocate")) = True Then
            '    strWillingToRelocate = "Yes"
            'Else
            '    strWillingToRelocate = "No"
            'End If

            'If CBool(dsOtherInfo.Tables(0).Rows(0).Item("willing_to_travel")) = True Then
            '    strWillingToTravel = "Yes"
            'Else
            '    strWillingToTravel = "No"
            'End If

            'If CInt(dsOtherInfo.Tables(0).Rows(0).Item("notice_period_days")) <> 0 Then
            '    strNoticePeriodDays = dsOtherInfo.Tables(0).Rows(0).Item("notice_period_days").ToString & " Days"
            'End If

            If objEmp._Domicile_Alternateno.ToString.Trim.Length > 0 Then
                strPermenantAddress2 &= ", " & Language.getMessage(mstrModuleName, 5, "Alt. Tel. No.: ") & objEmp._Domicile_Alternateno.ToString
            End If

            'If dsOtherInfo.Tables(0).Rows(0).Item("memberships").ToString.Trim.Length > 0 Then
            '    strMemberships = dsOtherInfo.Tables(0).Rows(0).Item("memberships").ToString
            'End If

            'If dsOtherInfo.Tables(0).Rows(0).Item("achievements").ToString.Trim.Length > 0 Then
            '    strAchievements = dsOtherInfo.Tables(0).Rows(0).Item("achievements").ToString
            'End If

            'If dsOtherInfo.Tables(0).Rows(0).Item("journalsresearchpapers").ToString.Trim.Length > 0 Then
            '    strJournalResearchPaper = dsOtherInfo.Tables(0).Rows(0).Item("journalsresearchpapers").ToString
            'End If


            If strPermenantAddress.Trim.Length > 0 Then
                strPermenantAddress = strPermenantAddress.Substring(2)
            End If

            If strPermenantAddress2.Trim.Length > 0 Then
                strPermenantAddress2 = strPermenantAddress2.Substring(2)
            End If


            '=============Skill Information Start==============
            Dim dsSkill As DataSet = (New clsEmployee_Skill_Tran).GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), DateAndTime.Now.Date, DateAndTime.Now.Date, CStr(Session("UserAccessModeSetting")), True, False, "List", False, " hremp_app_skills_tran.emp_app_unkid = " & CInt(Session("Employeeunkid")) & " ", False, False)

            Dim strSkillCSV As String = ""
            If dsSkill IsNot Nothing AndAlso dsSkill.Tables(0).Rows.Count > 0 Then
                strSkillCSV = String.Join(", ", dsSkill.Tables(0).AsEnumerable().Select(Function(x) " " & IIf(x.Field(Of Integer)("skillid") > 0, x.Field(Of String)("SkillName"), x.Field(Of String)("other_skill")).ToString).ToArray)
            End If

            '=============Skill Information End==============



            '=============Qualification Information Start ==========
            Dim dsQualification As DataSet = (New clsEmp_Qualification_Tran).GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), DateAndTime.Now.Date, DateAndTime.Now.Date, CStr(Session("UserAccessModeSetting")), True, False, "List", False, " hremp_qualification_tran.employeeunkid = " & CInt(Session("Employeeunkid")) & " ", False, False)
            'Dim dsQualification As DataSet = (New clsApplicantQualification).GetApplicantQualifications(strCompCode:=Session("PreviewCompCode").ToString, _
            '                                                                                            intComUnkID:=CInt(Session("Previewcompanyunkid")), _
            '                                                                                            intApplicantUnkid:=CInt(Session("PreviewApplicantUnkid")), _
            '                                                                                            intQualificationGroup_ID:=clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, intSortOrder:=CInt(Session("appqualisortbyid")))

            '=============Qualification Information END==========


            '=============Experience Information Start ==========
            Dim dsExperience As DataSet = (New clsJobExperience_tran).GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), DateAndTime.Now.Date, DateAndTime.Now.Date, CStr(Session("UserAccessModeSetting")), True, False, "List", False, " hremp_experience_tran.employeeunkid = " & CInt(Session("Employeeunkid")) & " ", False, False)
            '=============Experience Information Start ==========


            '=============Refrences Information Start===========
            Dim dsRefrence As DataSet = (New clsEmployee_Refree_tran).GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), DateAndTime.Now.Date, DateAndTime.Now.Date, CStr(Session("UserAccessModeSetting")), True, False, "List", False, False, " hremployee_referee_tran.employeeunkid = " & CInt(Session("Employeeunkid")) & " ", False, False)
            '=============Refrences Information End===========



            If strTitle.Trim.Length > 0 Then
                strTitle = strTitle & " "
            Else
                strTitle = ""
            End If


            strBuilder.Append(" <HTML> " & vbCrLf)
            strBuilder.Append("<HEAD>")
            strBuilder.Append(" <TITLE>" & strName & " CV </TITLE> " & vbCrLf)
            strBuilder.Append(" <style> body { background: rgb(255,255,255); } .sectionheader { font-size: 14px; font-weight: 600; background-color: #e0e0e0; " & _
                                            "text-align: center;} .sectionpanel { padding-left: 20px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; } #page { background: white; display: block;" & _
                                            "margin: 0 auto;margin-bottom: 0.5cm; box-shadow: 0 0 0.5cm rgba(0,0,0,0.5); } #page[size='A4'] {width: 21cm; min-height: 29.7cm; } #page[size='A4'][layout='portrait'] { width: 29.7cm;" & _
                                           " height: 21cm;}#page[size='A3'] {width: 29.7cm;height: 42cm;}#page[size='A3'][layout='portrait'] {width: 42cm;height: 29.7cm; }#page[size='A5'] { width: 14.8cm; height: 21cm; }" & _
                                            " #page[size='A5'][layout='portrait'] {width: 21cm; height: 14.8cm; }@media print { body, #page { margin: 0;box-shadow: 0; }} .gridHeader > td, .gridItem > td {padding: 5px; }.gridHeader {" & _
                                            "background-color: #AFAFAF;font-weight: bold; }</style>")
            strBuilder.Append("</HEAD>")
            strBuilder.Append("<BODY>")
            strBuilder.Append("<FORM ID = 'form1' autocomplete='off' style='font-family: Verdana, Geneva, Tahoma, sans-serif'>")
            strBuilder.Append("<DIV ID='PAGE' SIZE='A4'>")

            strBuilder.Append("<TABLE STYLE='WIDTH:100%'>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD ALIGN='center'>")
            strBuilder.Append("<SPAN STYLE='font-size:Large;font-weight:bold;'>" & strName & "</SPAN>")
            strBuilder.Append("</TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD ALIGN='center'>")
            strBuilder.Append("<SPAN STYLE='font-size:14px'>" & strEmail & "</SPAN>")
            strBuilder.Append("</TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD ALIGN='center'>")
            strBuilder.Append("<SPAN STYLE='font-size:14px'>" & strPresentAddress & "</SPAN>")
            strBuilder.Append("</TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD ALIGN='center'>")
            strBuilder.Append("<SPAN STYLE='font-size:14px'>" & strPresentAddress2 & "</SPAN>")
            strBuilder.Append("</TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD ALIGN='center'>")
            strBuilder.Append("<HR STYLE='margin-top: 0px; margin-bottom: 0px; border-top: 1px solid #000;' />")
            strBuilder.Append("</TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("</TABLE>")

            strBuilder.Append("<DIV ID='divPersonalInfo' Class='sectionpanel'>")
            strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage(mstrModuleName, 6, "Personal Information") & "</DIV>")
            strBuilder.Append("<BR></BR>")
            strBuilder.Append("<TABLE STYLE = 'width: 100%'>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 7, "Name:") & "</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strTitle & strName & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 8, "Gender:") & "</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strGender & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 9, "Birth Date:") & "</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strBirthDate & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 25%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 10, "Marital Status:") & "</TD>")
            strBuilder.Append("<TD STYLE='width: 25%;font-size:14px'> <SPAN STYLE='font-size:14px'> " & strMaritalStatus & "</SPAN></TD>")

            If strMarriedDate.ToString().Trim.Length > 0 Then
                strBuilder.Append("<TD STYLE='width: 25%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 11, "Married Date:") & "</TD>")
                strBuilder.Append("<TD STYLE='width: 25%;font-size:14px'> <SPAN STYLE='font-size:14px'> " & strMarriedDate & "</SPAN></TD>")
            Else
                strBuilder.Append("<TD STYLE='width: 25%;font-weight:bold;font-size:14px'></TD>")
                strBuilder.Append("<TD STYLE='width: 25%;font-size:14px'> <SPAN STYLE='font-size:14px'></SPAN></TD>")
            End If

            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 12, "Nationality:") & "</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strNationality & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 13, "Languages Known:") & "</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strLanguagesKnown & "</SPAN></TD>")
            strBuilder.Append("</TR>")

            If strMotherTongue.Trim.Length > 0 Then
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 14, "Mother Tongue:") & "</TD>")
                strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strMotherTongue & "</SPAN></TD>")
                strBuilder.Append("</TR>")
            End If

            If strImpaired.Trim.Length > 0 Then
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 15, "Impaired:") & "</TD>")
                strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strImpaired & "</SPAN></TD>")
                strBuilder.Append("</TR>")
            End If

            If strCurrentSalary.Trim.Length > 0 Then
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 16, "Current Salary:") & "</TD>")
                strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strCurrentSalary & "</SPAN></TD>")
                strBuilder.Append("</TR>")
            End If

            If strCurrentSalary.Trim.Length > 0 Then
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 17, "Expected Salary:") & "</TD>")
                strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strExpectedSalary & "</SPAN></TD>")
                strBuilder.Append("</TR>")
            End If

            If strExpectedBenefits.Trim.Length > 0 Then
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 18, "Expected Benefits:") & "</TD>")
                strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strExpectedBenefits & "</SPAN></TD>")
                strBuilder.Append("</TR>")
            End If

            'If strWillingToRelocate.Length > 0 Then
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 19, "Willing to relocate:") & "</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strWillingToRelocate & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            'End If

            'If strWillingToTravel.Length > 0 Then
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 20, "Willing to travel:") & "</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strWillingToTravel & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            'End If

            If strNoticePeriodDays.Trim.Length > 0 Then
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 21, "Notice Period (Days):") & "</TD>")
                strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strNoticePeriodDays & "</SPAN></TD>")
                strBuilder.Append("</TR>")
            End If

            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 22, "Permanent Address:") & "</TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strPermenantAddress & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("<TR>")
            strBuilder.Append("<TD STYLE='width: 30%;font-weight:bold;font-size:14px'></TD>")
            strBuilder.Append("<TD COLSPAN='3'> <SPAN STYLE='font-size:14px'> " & strPermenantAddress2 & "</SPAN></TD>")
            strBuilder.Append("</TR>")
            strBuilder.Append("</TABLE>")
            strBuilder.Append("</DIV>")

            If strSkillCSV.Trim.Length > 0 Then
                strBuilder.Append("<DIV ID='divSkill' Class='sectionpanel'>")
                strBuilder.Append("<TABLE STYLE = 'width: 100%'>")
                strBuilder.Append("<TR>")
                strBuilder.Append("<TD STYLE='width: 15%;vertical-align: top;font-weight:bold;font-size:14px'>" & Language.getMessage(mstrModuleName, 23, "Key Skills:") & "</TD>")
                strBuilder.Append("<TD STYLE='width: 85%;font-size:14px'> " & strSkillCSV & "</TD>")
                strBuilder.Append("</TR>")
                strBuilder.Append("</TABLE>")
                strBuilder.Append("</DIV>")
            End If


            If dsQualification IsNot Nothing AndAlso dsQualification.Tables(0).Rows.Count > 0 Then
                strBuilder.Append("<DIV ID='divQualification' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage(mstrModuleName, 24, "Qualification") & "</DIV>")
                strBuilder.Append("<BR></BR>")
                strBuilder.Append("<TABLE border='1' STYLE = 'width: 100%;'border-collapse: collapse;'>")
                strBuilder.Append("<TR class='gridHeader'>")
                strBuilder.Append("<TD STYLE='width: 17%;font-weight:bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 25, "Qualification") & "</TD>")
                strBuilder.Append("<TD STYLE='width: 17%;font-weight:bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 26, "Institution") & "</TD>")
                strBuilder.Append("<TD STYLE='width: 16%;font-weight:bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 27, "Result") & "</TD>")
                strBuilder.Append("<TD STYLE='width: 17%;font-weight:bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 28, "Duration") & "</TD>")
                strBuilder.Append("<TD STYLE='width: 16%;font-weight:bold;font-size:14px;text-align:right;'>" & Language.getMessage(mstrModuleName, 29, "GPA Point") & "</TD>")
                strBuilder.Append("</TR>")



                If dsQualification.Tables(0).Columns.Contains("StDate") Then
                    dsQualification.Tables(0).Columns("StDate").SetOrdinal(dsQualification.Tables(0).Columns.Count - 1)
                End If
                If dsQualification.Tables(0).Columns.Contains("EnDate") Then
                    dsQualification.Tables(0).Columns("EnDate").SetOrdinal(dsQualification.Tables(0).Columns.Count - 1)
                End If
                If dsQualification.Tables(0).Columns.Contains("gpacode") Then
                    dsQualification.Tables(0).Columns("gpacode").SetOrdinal(dsQualification.Tables(0).Columns.Count - 1)
                End If
                If dsQualification.Tables(0).Columns.Contains("other_resultcode") Then
                    dsQualification.Tables(0).Columns("other_resultcode").SetOrdinal(dsQualification.Tables(0).Columns("other_institute").Ordinal)
                End If


                For Each dr As DataRow In dsQualification.Tables(0).Rows
                    strBuilder.Append("<TR class='gridItem'>")
                    For Each dc As DataColumn In dsQualification.Tables(0).Columns
                        'If CInt(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("QGrpId")) > 0 AndAlso CInt(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("QualifyId")) > 0 Then
                        If dc.ColumnName = "Qualify" OrElse dc.ColumnName = "Institute" OrElse dc.ColumnName = "resultcode" Then
                            strBuilder.Append("<TD STYLE='font-size:14px'> " & dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                        End If
                        'ElseIf CInt(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("QGrpId")) <= 0 AndAlso CInt(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("QualifyId")) <= 0 Then
                        'If dc.ColumnName = "other_qualification" OrElse dc.ColumnName = "other_institute" OrElse dc.ColumnName = "other_resultcode" Then
                        '    strBuilder.Append("<TD STYLE='font-size:14px'> " & dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                        'End If
                        'End If

                        If dc.ColumnName = "gpacode" Then
                            strBuilder.Append("<TD STYLE='font-size:14px;text-align:right;'> " & dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                        ElseIf dc.ColumnName = "StDate" Then
                            Dim strDuration As String = ""
                            If IsDBNull(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName)) = False Then
                                strDuration = CDate(eZeeDate.convertDate(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString())).ToString("dd-MMM-yyyy") & " - "
                                If IsDBNull(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("EnDate")) = False AndAlso CDate(eZeeDate.convertDate(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("EnDate").ToString)).Date <> CDate("01/Jan/1900").Date Then
                                    strDuration &= CDate(eZeeDate.convertDate(dsQualification.Tables(0).Rows(dsQualification.Tables(0).Rows.IndexOf(dr))("EnDate").ToString)).ToString("dd-MMM-yyyy")
                                Else
                                    strDuration &= Language.getMessage(mstrModuleName, 30, "Present")
                                End If
                            End If
                            strBuilder.Append("<TD STYLE='font-size:14px'> " & strDuration & "</TD>")
                        End If

                    Next
                    strBuilder.Append("</TR>")
                Next

                strBuilder.Append("</TABLE>")
                strBuilder.Append("</DIV>")
            End If

            If dsExperience IsNot Nothing AndAlso dsExperience.Tables(0).Rows.Count > 0 Then
                strBuilder.Append("<DIV ID='divExperience' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage(mstrModuleName, 31, "Work Experience") & "</DIV>")
                strBuilder.Append("<BR></BR>")
                strBuilder.Append("<TABLE ID = 'dlExperience'  STYLE='border-collapse: collapse;width: 100%'>")

                For Each dr As DataRow In dsExperience.Tables(0).Rows
                    Dim strEmployerName As String = ""
                    Dim strEmployer As String = ""
                    Dim strOPhone As String = ""

                    If dr.Item("contact_person").ToString.Trim.Length > 0 Then
                        strEmployer = dr.Item("contact_person").ToString
                    End If
                    If dr.Item("contact_no").ToString.Trim.Length > 0 Then
                        strOPhone = "<b>Tel. No.:</b> " & dr.Item("contact_no").ToString
                    End If
                    If strEmployer.Trim.Length > 0 Then
                        If strOPhone.Trim.Length > 0 Then
                            strEmployer &= ", " & strOPhone
                        End If
                    Else
                        strEmployer = strOPhone
                    End If
                    strEmployerName = strEmployer

                    strBuilder.Append("<TR STYLE='width:100%;'>")
                    strBuilder.Append("<TD STYLE='float:Left;width:25%'>")
                    strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 32, "Company:") & "</SPAN>")
                    strBuilder.Append("</TD>")
                    strBuilder.Append("<TD WIDTH= '75%'>")
                    strBuilder.Append("<SPAN style='font-size:14px;'>" & dr("Company").ToString() & "</SPAN>")
                    strBuilder.Append("</TD>")
                    strBuilder.Append("</TR>")

                    If strEmployer.Trim.Length > 0 Then
                        strBuilder.Append("<TR STYLE='width:100%;'>")
                        strBuilder.Append("<TD STYLE='float:Left;width:25%'>")
                        strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 33, "Employer:") & "</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("<TD WIDTH ='75%'>")
                        strBuilder.Append("<SPAN style='font-size:14px;'>" & strEmployerName & "</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("</TR>")
                    End If

                    strBuilder.Append("<TR STYLE='width:100%;'>")
                    strBuilder.Append("<TD STYLE='float:Left;width:25%'>")
                    strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 34, "Designation:") & "</SPAN>")
                    strBuilder.Append("</TD>")
                    strBuilder.Append("<TD WIDTH='75%'>")
                    strBuilder.Append("<SPAN style='font-size:14px;'>" & dr("Jobs").ToString() & "</SPAN>")
                    strBuilder.Append("</TD>")
                    strBuilder.Append("</TR>")

                    strBuilder.Append("<TR STYLE='width:100%;'>")
                    strBuilder.Append("<TD STYLE='float:Left;width:25%'>")
                    strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 35, "Duration:") & "</SPAN>")
                    strBuilder.Append("</TD>")
                    strBuilder.Append("<TD WIDTH='75%'>")
                    strBuilder.Append("<SPAN style='font-size:14px;'>" & GetDuration(dr).ToString() & "</SPAN>")
                    strBuilder.Append("</TD>")
                    strBuilder.Append("</TR>")

                    If dr("Remark").ToString().Trim().Length > 0 Then
                        strBuilder.Append("<TR STYLE='width:100%;'>")
                        strBuilder.Append("<TD STYLE='float:Left;width:25%'>")
                        strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 36, "Responsibility:") & "</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("<TD WIDTH='75%'>")
                        strBuilder.Append("<SPAN style='font-size:14px;'>" & dr("Remark").ToString().Trim() & "</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("</TR>")
                    End If

                    If 1 = 2 Then 'dr("achievements").ToString().Trim().Length > 0 
                        strBuilder.Append("<TR STYLE='width:100%;'>")
                        strBuilder.Append("<TD STYLE='float:Left;width:25%'>")
                        strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 37, "Achievement:") & "</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("<TD WIDTH='75%'>")
                        strBuilder.Append("<SPAN style='font-size:14px;'>" & dr("achievements").ToString().Trim() & "</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("</TR>")
                    End If

                    If dr("leave_reason").ToString().Trim().Length > 0 Then
                        strBuilder.Append("<TR STYLE='width:100%;'>")
                        strBuilder.Append("<TD STYLE='float:Left;width:70%'>")
                        strBuilder.Append("<SPAN style='font-weight:bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 38, "Leaving Reason:") & "</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("<TD WIDTH='75%'>")
                        strBuilder.Append("<SPAN style='font-size:14px;'>" & dr("leave_reason").ToString().Trim() & "</SPAN>")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("</TR>")
                    End If

                    If dsExperience.Tables(0).Rows.Count - 1 <> dsExperience.Tables(0).Rows.IndexOf(dr) Then
                        strBuilder.Append("<TR STYLE='width:100%;'>")
                        strBuilder.Append("<TD STYLE='float:Left;width:25%;'>")
                        strBuilder.Append("<HR STYLE='margin-top: 0px; margin-bottom: 0px; border-top: 1px solid #000;' />")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("<TD WIDTH='75%' STYLE = ''>")
                        strBuilder.Append("<HR STYLE='margin-top: 0px; margin-bottom: 0px; border-top: 1px solid #000;' />")
                        strBuilder.Append("</TD>")
                        strBuilder.Append("</TR>")
                    End If

                Next


                strBuilder.Append("</TABLE>")
                strBuilder.Append("</DIV>")
            End If

            If strMemberships.Trim.Length > 0 OrElse strAchievements.Trim.Length > 0 OrElse strJournalResearchPaper.Trim.Length > 0 Then
                strBuilder.Append("<DIV ID = 'pnlOtherInfo' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage(mstrModuleName, 39, "Other Information") & "</DIV>")
                strBuilder.Append("<BR></BR>")

                If strMemberships.Trim.Length > 0 Then
                    strBuilder.Append("<DIV ID='divMembership' style='padding: 0px; width: 100%'>")
                    strBuilder.Append("<TABLE STYLE = 'WIDTH:100%'>")
                    strBuilder.Append("<TR>")
                    strBuilder.Append("<TD STYLE='width: 25%; vertical-align: top; font-weight: bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 40, "Membership:") & "</TD>")
                    strBuilder.Append("<TD STYLE='width: 75%;font-size:14px;'>" & strMemberships.Trim() & "</TD>")
                    strBuilder.Append("</TR>")
                    strBuilder.Append("</TABLE>")
                    strBuilder.Append("</DIV>")
                End If

                If strAchievements.Trim.Length > 0 Then
                    strBuilder.Append("<DIV ID='divAchievements' style='padding: 0px; width: 100%'>")
                    strBuilder.Append("<TABLE STYLE = 'WIDTH:100%'>")
                    strBuilder.Append("<TR>")
                    strBuilder.Append("<TD STYLE='width: 25%; vertical-align: top; font-weight: bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 41, "Achievements:") & "</TD>")
                    strBuilder.Append("<TD STYLE='width: 75%;font-size:14px;'>" & strAchievements.Trim() & "</TD>")
                    strBuilder.Append("</TR>")
                    strBuilder.Append("</TABLE>")
                    strBuilder.Append("</DIV>")
                End If

                If strJournalResearchPaper.Trim.Length > 0 Then
                    strBuilder.Append("<DIV ID='divJournalResearchPaper' style='padding: 0px; width: 100%'>")
                    strBuilder.Append("<TABLE STYLE = 'WIDTH:100%'>")
                    strBuilder.Append("<TR>")
                    strBuilder.Append("<TD STYLE='width: 25%; vertical-align: top; font-weight: bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 42, "Links / Journals / Research Papers:") & "</TD>")
                    strBuilder.Append("<TD STYLE='width: 75%;font-size:14px;'>" & strJournalResearchPaper.Trim() & "</TD>")
                    strBuilder.Append("</TR>")
                    strBuilder.Append("</TABLE>")
                    strBuilder.Append("</DIV>")
                End If

                strBuilder.Append("</DIV>")
            End If


            If dsRefrence IsNot Nothing AndAlso dsRefrence.Tables(0).Rows.Count > 0 Then
                strBuilder.Append("<DIV ID='pnlRefrence' Class='sectionpanel'>")
                strBuilder.Append("<DIV CLASS='sectionheader' style='margin-bottom:10px'>" & Language.getMessage(mstrModuleName, 43, "References") & "</DIV>")
                strBuilder.Append("<BR></BR>")
                strBuilder.Append("<TABLE border='1' STYLE = 'width: 100%;border-collapse:collapse;'>")
                strBuilder.Append("<TR class='gridHeader'>")
                strBuilder.Append("<TD STYLE='width: 40%;font-weight:bold;font-size:14px;'>Name</TD>")
                strBuilder.Append("<TD STYLE='width: 48%;font-weight:bold;font-size:14px;'>" & Language.getMessage(mstrModuleName, 44, "Contact Details") & "</TD>")
                strBuilder.Append("</TR>")

                For Each dr As DataRow In dsRefrence.Tables(0).Rows
                    strBuilder.Append("<TR class='gridItem'>")
                    Dim strContactDetails As String = ""
                    For Each dc As DataColumn In dsRefrence.Tables(0).Columns
                        If dc.ColumnName.ToLower = "address" Then
                            If dr.Item("Address").ToString.Length > 0 Then
                                strContactDetails &= dr.Item("Address").ToString
                            End If
                            If dr.Item("mobile_no").ToString.Trim.Length > 0 Then
                                If strContactDetails.Trim.Length > 0 Then strContactDetails &= "<br/>"
                                strContactDetails &= "<B> " & Language.getMessage(mstrModuleName, 45, "Mobile No. :") & " </b>" & dr.Item("mobile_no").ToString
                            End If
                            If dr.Item("telephone_no").ToString.Trim.Length > 0 Then
                                If dr.Item("mobile_no").ToString.Trim.Length > 0 Then
                                    strContactDetails &= ", "
                                Else
                                    strContactDetails &= "<br/>"
                                End If
                                strContactDetails &= "<b>" & Language.getMessage(mstrModuleName, 46, "Tel. No. :") & " </b> " & dr.Item("telephone_no").ToString
                            End If
                            strBuilder.Append("<TD STYLE='font-size:14px'>" & strContactDetails & "</TD>")

                        ElseIf dc.ColumnName = "RefName" Then
                            strBuilder.Append("<TD STYLE='font-size:14px'>" & dsRefrence.Tables(0).Rows(dsRefrence.Tables(0).Rows.IndexOf(dr))(dc.ColumnName).ToString() & "</TD>")
                        End If
                    Next
                    strBuilder.Append("</TR>")
                Next
                strBuilder.Append("</TABLE>")
                strBuilder.Append("</DIV>")
            End If
            strBuilder.Append("</DIV>")
            strBuilder.Append("</FORM>")
            strBuilder.Append("</BODY>")
            strBuilder.Append("</HTML>")

            mstrFilePath = Server.MapPath("~") & "/UploadImage"
            Dim mstrCVName As String = Session("E_Email").ToString.Replace(".", "_") & "_CV"
            If Directory.Exists(mstrFilePath & "\CV") = False Then
                Directory.CreateDirectory(mstrFilePath & "\CV")
            End If

            mstrFilePath &= "\CV"

            If File.Exists(mstrFilePath & "\" & mstrCVName & ".pdf") Then
                Try
                    File.Delete(mstrFilePath & "\" & mstrCVName & ".pdf")
                Catch

                End Try
            End If

            mstrFilePath &= "\" & mstrCVName & ".pdf"

            Using sw As New StringWriter(strBuilder)
                Using hw As New HtmlTextWriter(sw)
                    Dim sr As New StringReader(sw.ToString())
                    Dim pdfDoc As New Document(PageSize.A4, 10.0F, 10.0F, 10.0F, 10.0F)
                    Dim writer As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(mstrFilePath, FileMode.Create))
                    pdfDoc.Open()
                    XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr)
                    pdfDoc.Close()
                    Response.ContentType = "application/pdf"
                    Response.AddHeader("content-disposition", "attachment;filename=" & mstrCVName)
                    Response.Cache.SetCacheability(HttpCacheability.NoCache)
                End Using
            End Using

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mstrFilePath
    End Function

    Protected Function GetDuration(ByVal dr As DataRow) As String
        Dim strDuration As String = ""
        Try
            If IsDBNull(dr.Item("StartDate")) = False Then
                strDuration = CDate(eZeeDate.convertDate(dr.Item("StartDate").ToString)).ToString("dd-MMM-yyyy") & " - "
            End If
            If IsDBNull(dr.Item("EndDate")) = False AndAlso CDate(eZeeDate.convertDate(dr.Item("EndDate").ToString)).Date <> CDate("01/Jan/1900").Date Then
                strDuration &= CDate(eZeeDate.convertDate(dr.Item("EndDate").ToString)).ToString("dd-MMM-yyyy")
            Else
                strDuration &= "Present"
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return strDuration
    End Function

    Private Function Savefile(ByVal fpath As String, ByVal sb As System.Text.StringBuilder) As Boolean
        Dim fsFile As New System.IO.FileStream(fpath, System.IO.FileMode.Create, System.IO.FileAccess.Write)
        Dim strWriter As New System.IO.StreamWriter(fsFile)
        Try
            With strWriter
                .BaseStream.Seek(0, System.IO.SeekOrigin.End)
                .WriteLine(sb)
                .Close()
            End With
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        Finally
            sb = Nothing
            strWriter = Nothing
            fsFile = Nothing
        End Try
    End Function

    'Pinkal (18-May-2018) -- End

#End Region

#Region " Form Event(S) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            If Session("LoginBy") = Global.User.en_loginby.User Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 61, "Please login from ESS to view this page."), Me.Page, Session("rootpath") & "UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = True Then
                blnHideApply = Me.ViewState("blnHideApply")
            End If
            Dim blnIsAdmin As Boolean = False
            'LanguageOpner.Visible = False
            'If Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "admin") = True Then
            '    Dim arrAdmin() As String = HttpContext.Current.Session("admin_email").ToString.Split(CChar(";"))
            '    If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
            '        If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            '            LanguageOpner.Visible = True
            '            blnIsAdmin = True
            '        End If
            '    Else
            '        arrAdmin = HttpContext.Current.Session("admin_email").ToString.Split(CChar(","))
            '        If arrAdmin.Contains(Session("email").ToString.Trim) = True Then
            '            If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            '                LanguageOpner.Visible = True
            '                blnIsAdmin = True
            '            End If
            '        End If
            '    End If
            'ElseIf Session("email") IsNot Nothing AndAlso Roles.IsUserInRole(Session("email").ToString, "superadmin") = True Then
            '    If Session("CompCode") IsNot Nothing AndAlso Session("CompCode").ToString.Trim.Length > 0 Then
            '        LanguageOpner.Visible = True
            '        blnIsAdmin = True
            '    End If
            'End If
            'Sohail (16 Aug 2019) -- End
            If IsPostBack = False Then
                pnlMessage.Visible = False
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())
                Dim objEmp As New clsEmployee_Master
                Dim strMsg As String = ""
                lblMessage.Text = "" 'Sohail (10 Feb 2021)
                If objEmp.IsValidForApplyVacancy(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), DateAndTime.Now.Date, DateAndTime.Now.Date, CStr(Session("UserAccessModeSetting")), False, False, False, False, CInt(Session("Employeeunkid")), True, strMsg) = False Then
                    If strMsg.Trim <> "" Then
                        If blnIsAdmin = False Then
                            'DisplayMessage.DisplayMessage("Sorry, Your profile is incomplete. \n Please fill required details." & strMsg, Me)
                            'HttpContext.Current.Response.Redirect("~/UserHome.aspx", False)
                            'Sohail (12 Nov 2020) -- Start
                            'NMB Enhancement # : - NMB wants to add sentence to be read as Details in bold are mandatory.
                            'lblMessage.Text = "Sorry, Your profile is incomplete. <BR /> Please fill required details." & strMsg
                            lblMessage.Text = "Sorry, Your profile is incomplete. <BR /> Please fill required details. <BR /> <B>Details in bold are mandatory</B>" & strMsg
                            'Sohail (12 Nov 2020) -- End
                            'Sohail (10 Feb 2021) -- Start
                            'ZRA Enhancement : OLD-297 : Display Available Vacancies in SS even for Incomplete Applicant Profile. Display pending mandatory fields only above the vacancy list.
                            'pnlForm.Visible = False
                            'pnlMessage.Visible = True
                            'Sohail (10 Feb 2021) -- End
                        Else
                            blnHideApply = True
                        End If
                    End If
                    If blnIsAdmin = False Then
                        'Exit Sub 'Sohail (10 Feb 2021)
                    End If
                End If
                'If Validation() = False Then Exit Sub
                Call GetApplicantIDByEmployeeID()
                Call FillVacancyList(Session("Database_Name").ToString, 9)

                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()
            Else
                idIndex = CInt(ViewState("idIndex"))
                'dsVacancyList = CType(Me.ViewState("dsVacancyList"), DataSet)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SearchJob_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        Try
            ViewState("update") = Session("update")
            ViewState("blnHideApply") = Session("blnHideApply")

            ViewState("idIndex") = idIndex

            'Me.ViewState("dsVacancyList") = dsVacancyList
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " DataList Events "

    Private Sub dlVaanciesList_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlVaanciesList.ItemCommand
      
        Try
            If e.CommandName.ToUpper = "APPLY" Then

                idIndex = e.Item.ItemIndex


                If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
                Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

                Dim Idx As Integer = e.Item.ItemIndex
                Dim txtVFF As TextBox = CType(dlVaanciesList.Items(Idx).FindControl("txtVacancyFoundOutFrom"), TextBox)
                Dim txtCom As TextBox = CType(dlVaanciesList.Items(Idx).FindControl("txtComments"), TextBox)
                Dim dtp As Controls_DateCtrl = CType(dlVaanciesList.Items(Idx).FindControl("dtpEarliestPossibleStartDate"), Controls_DateCtrl)
                Dim chk As CheckBox = CType(dlVaanciesList.Items(Idx).FindControl("chkAccept"), CheckBox)
                Dim lblVTitle As Label = CType(dlVaanciesList.Items(Idx).FindControl("objlblVacancyTitle"), Label)
                Dim chkVFF As CheckBox = CType(dlVaanciesList.Items(Idx).FindControl("chkVacancyFoundOutFrom"), CheckBox)
                Dim cboVFF As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlVacancyFoundOutFrom"), DropDownList)

                Dim hdnVacanyTitleID As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hdnfieldVacancyTitleId"), HiddenField)

                'Sohail (12 Nov 2020) -- Start
                'NMB Enhancement # : - Hide those items which are not set as mandatory on search job page in recruitment portal and hide all items in ESS on same page.
                'If CBool(Session("VacancyFoundOutFromMandatory")) = True Then
                '    If chkVFF.Checked = True Then
                '        If txtVFF.Text.Trim = "" Then
                '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 47, "Please enter Vacancy Found Out From."), Me)
                '            txtVFF.Focus()
                '            Exit Try
                '        End If
                '    Else
                '        If CInt(cboVFF.SelectedValue) <= 0 Then
                '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 48, "Please select Vacancy Found Out From."), Me)
                '            cboVFF.Focus()
                '            Exit Try
                'End If
                '    End If
                'ElseIf CBool(Session("EarliestPossibleStartDateMandatory")) = True AndAlso dtp.GetDate = CDate("01/Jan/1900") Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 49, "Please enter Earliest Possible Start Date."), Me)
                '    dtp.Focus()
                '    Exit Try
                'ElseIf CBool(Session("EarliestPossibleStartDateMandatory")) = True AndAlso dtp.GetDate < Today.Date Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 50, "Earliest Possible Start Date should be greater than current date."), Me)
                '    dtp.Focus()
                '    Exit Try
                'ElseIf CBool(Session("CommentsMandatory")) = True AndAlso txtCom.Text.Trim = "" Then
                '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 51, "Please enter Comments."), Me)
                '    txtCom.Focus()
                '    Exit Try
                'End If
                'Sohail (12 Nov 2020) -- End

                If chk.Visible = True AndAlso chk.Checked = False Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 52, "Please accept the Declaration to proceed."), Me)
                    Exit Try
                End If

                'Sohail (10 Feb 2021) -- Start
                'ZRA Enhancement : OLD-297 : Display Available Vacancies in SS even for Incomplete Applicant Profile. Display pending mandatory fields only above the vacancy list.
                If lblMessage.Text.Trim <> "" Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 62, "Sorry, Your profile is not complete."), Me)
                    CType(e.Item.FindControl("lblValidationMessage"), Label).Text = lblMessage.Text
                    Exit Try
                Else
                    CType(e.Item.FindControl("lblValidationMessage"), Label).Text = ""
                End If
                'Sohail (10 Feb 2021) -- End

                cnfConfirm.Message = Language.getMessage(mstrModuleName, 63, "Are you sure you want to apply for this job?")
                cnfConfirm.Show()

                'Dim intApplicantID As Integer = 0
                ''Sohail (04 Nov 2020) -- Start
                ''NMB Issue # : - Error Cannot insert duplicate key in applicant code column on import data in recruitment.
                'objApplicant._WebFormName = mstrModuleName
                ''Sohail (04 Nov 2020) -- End
                'If objApplicant.InsertUpdateApplicantByEmployeeID(CInt(Session("Employeeunkid")), CStr(Session("E_Email")), CInt(Session("LocalizationCountryUnkid")), DateAndTime.Now.Date, CInt(Session("CompanyUnkId")), CInt(Session("ApplicantCodeNotype")), CStr(Session("ApplicantCodePrifix")), intApplicantID) = False Then
                '    'Sohail (04 Nov 2020) - [ApplicantCodeNotype, ApplicantCodePrifix]
                '    If intApplicantID <= 0 Then
                '        Exit Try
                '    End If
                'End If

                'Session("applicantunkid") = intApplicantID

                'Dim intUnkId As Integer = 0
                'If objSearchJob.AppliedVacancy(intApplicantUnkId:=CInt(Session("applicantunkid")) _
                '                               , intVacancyUnkId:=CInt(dlVaanciesList.DataKeys(e.Item.ItemIndex).ToString) _
                '                               , dtEarliest_possible_startdate:=dtp.GetDate _
                '                               , strComments:=txtCom.Text.Trim _
                '                               , strVacancy_found_out_from:=If(chkVFF.Checked = True, txtVFF.Text.Trim, "").ToString _
                '                               , intVacancy_found_out_from_UnkId:=CInt(If(chkVFF.Checked = True, 0, CInt(cboVFF.SelectedValue))) _
                '                               , intLoginEmployeeunkId:=CInt(Session("employeeunkid")) _
                '                               , intRet_UnkID:=intUnkId _
                '                               ) = True Then

                '    dlVaanciesList.DataBind()

                '    If CBool(Session("isvacancyalert")) Then

                '        '    Dim mintVacancyTitleID As Integer = CInt(hdnVacanyTitleID.Value)

                '        '    Dim objAppNotificationSettings As New clsNotificationSettings
                '        '    Dim mintPriority As Integer = 0
                '        '    Dim dsList As DataSet = objAppNotificationSettings.GetApplicantNotificationSettings(xCompanyID:=CInt(Session("companyunkid")), xCompanyCode:=Session("CompCode").ToString() _
                '        '                                                                                                                                      , xApplicantID:=CInt(Session("applicantunkid")), intMasterType:=clsCommon_Master.enCommonMaster.VACANCY_MASTER)


                '        '    Dim drRow() As DataRow = dsList.Tables(0).Select("isactive = 1 AND priority > 0")

                '        '    If drRow.Length < CInt(Session("maxvacancyalert")) Then

                '        '        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                '        '            If Not IsDBNull(dsList.Tables(0).Compute("Max(priority)", "isactive = 1")) Then
                '        '                mintPriority = dsList.Tables(0).Compute("Max(priority)", "isactive = 1")
                '        '            End If
                '        '        End If

                '        '        mintPriority += 1

                '        '        objAppNotificationSettings.InsertUpdate_ApplicantNotificationSettings(xCompanyID:=CInt(Session("companyunkid")), xCompanyCode:=Session("CompCode").ToString(), xApplicantID:=CInt(Session("applicantunkid")) _
                '        '                                                                                                              , xVacancytitleId:=mintVacancyTitleID, xPriority:=mintPriority, xIsActive:=True)


                '        '        dsList.Clear()
                '        '        dsList = Nothing
                '        '        objAppNotificationSettings = Nothing

                '        '    End If

                        '    End If

                '    If Convert.ToBoolean(Session("issendjobconfirmationemail")) Then
                '        'Dim objApplicant As New clsApplicant
                '        'If objApplicant.GetCompanyInfo(intComUnkID:=CInt(Session("companyunkid")) _
                '        '                           , strCompCode:=Session("CompCode").ToString() _
                '        '                           , ByRefblnIsActive:=False _
                '        '                           ) = False Then

                '        '    objApplicant = Nothing
                '        '    Exit Try
                '        'Else
                '        '    objApplicant = Nothing
                '        'End If

                '        'Dim profile As UserProfile = UserProfile.GetUserProfile(Session("email"))
                '        Dim sFName As String = CStr(Session("Firstname"))
                '        Dim sLName As String = CStr(Session("Surname"))

                '        If sFName.Trim = "" Then sFName = Session("E_Email")
                '        Dim strSubject As String = Language.getMessage(mstrModuleName, 53, "Your application is successfully sent to ") + CStr(Session("CompName"))

                '        Dim strValue As String = (New clsConfigOptions).GetKeyValue(CInt(Session("CompanyUnkId")) _
                '                                                  , strKeyName:="JOBCONFIRMATIONTEMPLATEID" _
                '                                                  )

                '        Dim strBody As String = ""
                '        Dim mstrFilePath As String = ""
                '        Dim intID As Integer
                '        Integer.TryParse(strValue, intID)
                '        Dim intTemplateFound As Boolean = False
                '        If intID > 0 AndAlso intUnkId > 0 Then
                '            Dim objLetterType As New clsLetterType
                '            Dim dsLetter As DataSet = objLetterType.getList(clsCommon_Master.enCommonMaster.LETTER_TYPE _
                '                                                                          , "List" _
                '                                                                          , intUnkId:=intID _
                '                                                                          )

                '            If dsLetter IsNot Nothing AndAlso dsLetter.Tables(0).Rows.Count > 0 Then
                '                Dim strLetterContent As String = dsLetter.Tables(0).Rows(0).Item("Lettercontent")
                '                Dim intFIdx As Integer = strLetterContent.IndexOf(CChar("{")) 'to remove xml start and end tags
                '                Dim intLIdx As Integer = strLetterContent.LastIndexOf(CChar("}")) 'to remove xml start and end tags
                '                strLetterContent = strLetterContent.Substring(intFIdx, intLIdx - intFIdx + 1) 'to remove xml start and end tags

                '                If strLetterContent.Trim.Length > 0 Then
                '                    Dim objApplied As New clsApplicant_Vacancy_Mapping
                '                    Dim dsAppVac As DataSet = clsApplicant_Vacancy_Mapping.GetApplicantAppliedJob(intApplicantUnkId:=CInt(Session("applicantunkid")) _
                '                                                     , intAppvacancytranunkid:=intUnkId _
                '                                                     )

                '                    If dsAppVac.Tables(0).Rows.Count > 0 Then
                '                        Dim objLF As New clsLetterFields
                '                        Dim dsField As DataSet = objLF.GetList(dsLetter.Tables(0).Rows(0).Item("Fieldtypeid"))
                '                        intTemplateFound = True
                '                        Dim strDataName As String = ""
                '                        Dim StrCol As String = ""

                '                        strLetterContent = strLetterContent.Replace("#CompanyCode#", Session("CompCode").ToString)
                '                        strLetterContent = strLetterContent.Replace("#CompanyName#", Session("CompName").ToString)

                '                        For Each dsRow As DataRow In dsField.Tables(0).Rows
                '                            For Each col As DataColumn In dsField.Tables(0).Columns
                '                                'StrCol = dsRow.Item("Name").ToString
                '                                StrCol = col.ColumnName

                '                                If strLetterContent.Contains("#" & StrCol & "#") Then

                '                                    If StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                '                                        strDataName = strLetterContent.Replace("#" & StrCol & "#", "#imgcmp#")
                '                                    Else
                '                                        If dsAppVac.Tables(0).Columns.Contains(StrCol) = True Then
                '                                            If StrCol = "Pay_Range_From" Then
                '                                                strDataName = strLetterContent.Replace("#" & StrCol & "#", Math.Round(CDec(dsAppVac.Tables(0).Rows(0)(StrCol)), 2).ToString)
                '                                            ElseIf StrCol = "Pay_Range_To" Then
                '                                                strDataName = strLetterContent.Replace("#" & StrCol & "#", Math.Round(CDec(dsAppVac.Tables(0).Rows(0)(StrCol)), 2).ToString)
                '                                            ElseIf StrCol = "VacancyOpeningDate" Then
                '                                                strDataName = strLetterContent.Replace("#" & StrCol & "#", eZeeDate.convertDate(dsAppVac.Tables(0).Rows(0)(StrCol).ToString).ToString("dd-MMM-yyyy"))
                '                                            ElseIf StrCol = "VacancyClosingDate" Then
                '                                                strDataName = strLetterContent.Replace("#" & StrCol & "#", eZeeDate.convertDate(dsAppVac.Tables(0).Rows(0)(StrCol).ToString).ToString("dd-MMM-yyyy"))
                '                                            Else
                '                                                strDataName = strLetterContent.Replace("#" & StrCol & "#", dsAppVac.Tables(0).Rows(0)(StrCol).ToString)
                '                                            End If
                '                                        Else
                '                                            strDataName = strLetterContent.Replace("#" & StrCol & "#", "")
                '                                        End If
                '                                    End If

                '                                    strLetterContent = strDataName
                '                                End If
                '                            Next
                '                        Next

                '                        Dim htmlOutput = "Template.html"
                '                        Dim contentUriPrefix = Path.GetFileNameWithoutExtension(htmlOutput)
                '                        Dim htmlResult = RtfToHtmlConverter.RtfToHtml(strLetterContent, contentUriPrefix)

                '                        strBody = htmlResult._HTML

                '                        If Session("isattachapplicantcv") Then
                '                            mstrFilePath = GetApplicantCV()
                '                        End If

                '                    End If
                '                End If
                '            End If
                '        End If
                '        'Sohail (06 Nov 2019) -- End

                '        If intTemplateFound = False Then

                '            strBody = "<p style='font-size:14px'>" & Language.getMessage(mstrModuleName, 54, "Thank you for expressing interest in") & " <b>" + lblVTitle.Text + "</b> " & Language.getMessage(mstrModuleName, 55, "role") & " </p>"
                '            'Gajanan (04 July 2020) -- Start
                '            'Enhancment: #0004765
                '            'strBody += "<p style='font-size:14px'>This email is to confirm that we have received your application. You will be contacted once it has been reviewed.</p>"
                '            strBody += "<p style='font-size:14px'>" & Language.getMessage(mstrModuleName, 56, "This email is to confirm that we have received your application. You will only be contacted if shortlisted.") & "</p>"
                '            'Gajanan (04 July 2020) -- End

                '            If Session("isattachapplicantcv") Then
                '                mstrFilePath = GetApplicantCV()
                '                strBody += "<p style='font-size:14px'>" & Language.getMessage(mstrModuleName, 57, "Attached is the CV generated as per the information you provided in the application form.") & "</p>"
                '            End If

                '            strBody += "<p style='font-size:14px'>" & Language.getMessage(mstrModuleName, 58, "Please do not reply to this email as it is auto generated.") & "</p>"
                '            Dim sValue As String = (New clsConfigOptions).GetKeyValue(CInt(Session("companyunkid")), strKeyName:="SHOWARUTISIGNATURE" _
                '                                                    )
                '            Dim blnValue As Integer
                '            Boolean.TryParse(sValue, blnValue)
                '            If blnValue = True Then
                '                strBody += "<p  style='font-size:12px'><b>" & Language.getMessage(mstrModuleName, 59, "Online Job Application Powered by Aruti HR & Payroll Management Software") & "</b></p> "
                '            End If
                '            strBody += "<p style='font-size:12px'><b>#CompanyName# Recruitment</b></p> "
                '            strBody += "#imgcmp#"
                '            strBody += " #imgaruti#"

                '            'Dim objMail As New clsMail
                '            'If objMail.SendEmail(strToEmail:=Session("email") _
                '            '                 , strSubject:=strSubject _
                '            '                 , strBody:=strBody _
                '            '                 , blnAddLogo:=True _
                '            '                 , blnSuperAdmin:=False, mstrAttachedFiles:=mstrFilePath
                '            '                 ) = False Then

                '            '    ' e.Cancel = True
                '            'End If

                '            'Pinkal (18-May-2018) -- End
                '        End If

                '        Dim objEmp As New clsEmployee_Master
                '        Dim dsRepoTo As DataSet = objEmp.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, CInt(Session("Employeeunkid")), DateAndTime.Now.Date, DateAndTime.Now.Date, CStr(Session("Database_Name")))
                '        Dim strReportingEmail As String = String.Join(",", (From p In dsRepoTo.Tables(0) Where (p.Item("email").ToString.Trim <> "") Select (p.Item("email").ToString)).ToArray)
                '        'For Each rprow As DataRow In dsRepoTo.Tables(0).Rows
                '        '    If rprow.Item("email").ToString.Trim = "" Then Continue For

                '        'Next

                '        Dim objSendMail As New clsSendMail
                '        objSendMail._ToEmail = Session("E_Email")
                '        objSendMail._Subject = strSubject
                '        objSendMail._Message = strBody
                '        objSendMail._AttachedFiles = mstrFilePath
                '        objSendMail._Form_Name = mstrModuleName
                '        objSendMail._LogEmployeeUnkid = CInt(Session("E_Employeeunkid"))
                '        objSendMail._OperationModeId = enLogin_Mode.EMP_SELF_SERVICE
                '        objSendMail._UserUnkid = -1
                '        objSendMail._SenderAddress = Session("Senderaddress")
                '        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT
                '        objSendMail.SendMail(CInt(Session("companyunkid")), False)

                '        If strReportingEmail.Trim.Length > 0 Then
                '            objSendMail = New clsSendMail
                '            objSendMail._ToEmail = strReportingEmail
                '            objSendMail._Subject = sFName & " " & sLName & " " & Language.getMessage(mstrModuleName, 60, "application is successfully sent to ") + CStr(Session("CompName"))
                '            objSendMail._Message = strBody
                '            objSendMail._AttachedFiles = mstrFilePath
                '            objSendMail._Form_Name = mstrModuleName
                '            objSendMail._LogEmployeeUnkid = CInt(Session("E_Employeeunkid"))
                '            objSendMail._OperationModeId = enLogin_Mode.EMP_SELF_SERVICE
                '            objSendMail._UserUnkid = -1
                '            objSendMail._SenderAddress = Session("Senderaddress")
                '            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT
                '            objSendMail.SendMail(CInt(Session("companyunkid")), False)
                '        End If

                '    End If

                'End If

                'Call FillVacancyList(Session("Database_Name").ToString, 9)
                'DisplayMessage.DisplayMessage("Job Applied Successfully !", Me)

            ElseIf e.CommandName = "UpdateDetail" Then
                Response.Redirect(Session("rootpath").ToString & "Recruitment/wPgEmployeeUpdate.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        
        End Try
    End Sub

    Private Sub dlVaanciesList_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles dlVaanciesList.ItemDataBound
        Dim objCommon As New clsCommon_Master
        Dim dsCombo As DataSet
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                If blnHideApply = True Then CType(e.Item.FindControl("btnApply"), LinkButton).Visible = False

                Dim lbl As Label = CType(e.Item.FindControl("objlblIsApplyed"), Label)
                If CBool(lbl.Text) = True Then
                    CType(e.Item.FindControl("lblApplyText"), Label).Text = "Applied"
                    CType(e.Item.FindControl("btnApply"), LinkButton).Enabled = False
                    CType(e.Item.FindControl("divApplied"), Control).Visible = True
                    If Session("applicant_declaration").ToString.Trim.Length > 0 Then
                        CType(e.Item.FindControl("pnlDis"), Panel).Visible = False
                    End If
                Else
                    If Session("applicant_declaration").ToString.Trim.Length <= 0 Then
                        CType(e.Item.FindControl("pnlDis"), Panel).Visible = False
                    End If
                End If
                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
                If drv.Item("experience").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divExp"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isexpbold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isexpitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    CType(e.Item.FindControl("objlblExp"), Label).Text = sB & sI & IIf(CInt(drv.Item("experience")) <> 0, Format(CDec(drv.Item("experience")) / 12, "###0.0#") + " Year(s)", "Fresher Can Apply") & eB & eI
                End If

                If drv.Item("noposition").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divNoPosition"), Control).Visible = False
                End If

                If drv.Item("experience_comment").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("objlblExpCmt"), Control).Visible = False
                End If

                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-508 - Display Job Location on published vacancy.
                Dim arrJobLocation As New ArrayList
                If Session("CompanyGroupName").ToString.ToUpper = "NMB PLC" Then
                    If drv.Item("classgroupname").ToString.Trim.Length > 0 Then
                        arrJobLocation.Add(drv.Item("classgroupname").ToString)
                    End If
                    If drv.Item("classname").ToString.Trim.Length > 0 Then
                        arrJobLocation.Add(drv.Item("classname").ToString)
                    End If
                End If
                CType(e.Item.FindControl("objlblJobLocation"), Label).Text = String.Join(", ", TryCast(arrJobLocation.ToArray(GetType(String)), String()))
                If arrJobLocation.Count <= 0 Then
                    CType(e.Item.FindControl("divJobLocation"), Control).Visible = False
                End If
                'Hemant (01 Nov 2021) -- End

                If drv.Item("skill").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divSkill"), Control).Visible = False

                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isskillbold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isskillitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    'Hemant (08 Jul 2021) -- Start
                    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                    'CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & drv.Item("skill").ToString & eB & eI
                    CType(e.Item.FindControl("objlblSkill"), Label).Text = sB & sI & "• " & drv.Item("skill").ToString.Replace(";", "<BR> • ") & eB & eI
                    'Hemant (08 Jul 2021) -- End
                End If

                If drv.Item("Lang").ToString.Trim.Length <= 0 Then
                    CType(e.Item.FindControl("divLang"), Control).Visible = False
                End If

                If CDec(drv.Item("pay_from").ToString) <= 0 AndAlso CDec(drv.Item("pay_to").ToString) <= 0 Then
                    CType(e.Item.FindControl("divScale"), Control).Visible = False
                Else
                    Dim str As String = Format(CDec(drv.Item("pay_from").ToString), "##,##,##,##,##0.00") & " - " & Format(CDec(drv.Item("pay_to").ToString), "##,##,##,##,##0.00")
                    CType(e.Item.FindControl("objlblScale"), Label).Text = str
                End If

                If drv.Item("openingdate").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divOpenningDate"), Control).Visible = False
                End If
                If drv.Item("closingdate").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divClosuingDate"), Control).Visible = False
                End If
                
                If drv.Item("remark").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divRemark"), Control).Visible = False
                Else
                    Dim str As String = drv.Item("remark").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    Dim strResult As String = String.Concat(Regex.Split(str, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                    strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                    CType(e.Item.FindControl("objlblJobDiscription"), Label).Text = strResult
                End If
                If drv.Item("duties").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divResponsblity"), Control).Visible = False
                Else
                    Dim str As String = drv.Item("duties").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;")
                    Dim strResult As String = String.Concat(Regex.Split(str, "\*").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<strong>", p, "</strong>"))).ToArray())
                    strResult = String.Concat(Regex.Split(strResult, "_").[Select](Function(p, i) If(i Mod 2 = 0, p, String.Concat("<I>", p, "</I>"))).ToArray())
                    CType(e.Item.FindControl("objlblResponsblity"), Label).Text = strResult
                End If
                If drv.Item("Qualification").ToString.Length <= 0 Then
                    CType(e.Item.FindControl("divQualification"), Control).Visible = False
                Else
                    Dim sB As String = ""
                    Dim eB As String = ""
                    Dim sI As String = ""
                    Dim eI As String = ""
                    If CBool(drv.Item("isqualibold")) = True Then
                        sB = "<strong>"
                        eB = "</strong>"
                    End If
                    If CBool(drv.Item("isqualiitalic")) = True Then
                        sI = "<I>"
                        eI = "</I>"
                    End If
                    CType(e.Item.FindControl("objlblQualification"), Label).Text = sB & sI & drv.Item("Qualification").ToString & eB & eI
                End If

                Dim lblVacancyFoundOutFrom As Label = CType(e.Item.FindControl("lblVacancyFoundOutFrom"), Label)
                If CBool(Session("VacancyFoundOutFromMandatory")) = True Then
                    lblVacancyFoundOutFrom.CssClass = "required"
                Else
                    lblVacancyFoundOutFrom.CssClass = ""
                End If

                Dim lblEarliestPossibleStartDateMandatory As Label = CType(e.Item.FindControl("lblEarliestPossibleStartDate"), Label)
                If CBool(Session("EarliestPossibleStartDateMandatory")) = True Then
                    lblEarliestPossibleStartDateMandatory.CssClass = "required"
                Else
                    lblEarliestPossibleStartDateMandatory.CssClass = ""
                End If

                Dim lblComments As Label = CType(e.Item.FindControl("lblComments"), Label)
                If CBool(Session("CommentsMandatory")) = True Then
                    lblComments.CssClass = "required"
                Else
                    lblComments.CssClass = ""
                End If
            End If

            Dim cboVacancyFoundOutFrom As DropDownList = CType(e.Item.FindControl("ddlVacancyFoundOutFrom"), DropDownList)
            dsCombo = objCommon.getComboList(clsCommon_Master.enCommonMaster.VACANCY_SOURCE, True, "List")
            With cboVacancyFoundOutFrom
                .DataValueField = "masterunkid"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Private Sub odsVacancy_Selected(ByVal sender As Object, ByVal e As ObjectDataSourceStatusEventArgs) Handles odsVacancy.Selected
        Try
            Dim intC As Integer = 0

            If TypeOf e.ReturnValue Is DataTable Then
                intC = CType(e.ReturnValue, DataTable).Rows.Count
            End If

            objlblCount.Text = "(" & intC.ToString & ")"

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub odsVacancy_Selecting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.ObjectDataSourceSelectingEventArgs) Handles odsVacancy.Selecting
        Try
            If e.InputParameters("strDatabaseName") Is Nothing Then
                e.Cancel = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Checkbox Events "
    Protected Sub chkGridOther_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim chk As CheckBox = CType(sender, CheckBox)
            Dim itm As DataListItem = DirectCast(chk.NamingContainer, DataListItem)

            If CType(itm.FindControl("chkVacancyFoundOutFrom"), CheckBox).Checked = True Then
                CType(itm.FindControl("pnlVacancyFoundOutFrom"), Panel).Visible = False
                CType(itm.FindControl("pnlOtherVacancyFoundOutFrom"), Panel).Visible = True
            Else
                CType(itm.FindControl("pnlVacancyFoundOutFrom"), Panel).Visible = True
                CType(itm.FindControl("pnlOtherVacancyFoundOutFrom"), Panel).Visible = False
            End If

            CType(itm.FindControl("ddlVacancyFoundOutFrom"), DropDownList).SelectedValue = "0"

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Protected Sub cnfConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfConfirm.buttonYes_Click
        Dim objApplicant As New clsApplicant_master
        Dim objSearchJob As New clsApplicant_Vacancy_Mapping
        Try
            If Session("update") Is Nothing OrElse Session("update").ToString() <> ViewState("update").ToString() Then Exit Sub
            Session("update") = Server.UrlEncode(System.DateTime.Now.ToString())

            Dim Idx As Integer = idIndex
            Dim txtVFF As TextBox = CType(dlVaanciesList.Items(Idx).FindControl("txtVacancyFoundOutFrom"), TextBox)
            Dim txtCom As TextBox = CType(dlVaanciesList.Items(Idx).FindControl("txtComments"), TextBox)
            Dim dtp As Controls_DateCtrl = CType(dlVaanciesList.Items(Idx).FindControl("dtpEarliestPossibleStartDate"), Controls_DateCtrl)
            Dim chk As CheckBox = CType(dlVaanciesList.Items(Idx).FindControl("chkAccept"), CheckBox)
            Dim lblVTitle As Label = CType(dlVaanciesList.Items(Idx).FindControl("objlblVacancyTitle"), Label)
            Dim chkVFF As CheckBox = CType(dlVaanciesList.Items(Idx).FindControl("chkVacancyFoundOutFrom"), CheckBox)
            Dim cboVFF As DropDownList = CType(dlVaanciesList.Items(Idx).FindControl("ddlVacancyFoundOutFrom"), DropDownList)

            Dim hdnVacanyTitleID As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hdnfieldVacancyTitleId"), HiddenField)
            Dim hfVTitle As HiddenField = CType(dlVaanciesList.Items(Idx).FindControl("hfVTitle"), HiddenField)

            If chk.Visible = True AndAlso chk.Checked = False Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 52, "Please accept the Declaration to proceed."), Me)
                Exit Try
            End If

            If lblMessage.Text.Trim <> "" Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 62, "Sorry, Your profile is not complete."), Me)
                CType(dlVaanciesList.Items(Idx).FindControl("lblValidationMessage"), Label).Text = lblMessage.Text
                Exit Try
            Else
                CType(dlVaanciesList.Items(Idx).FindControl("lblValidationMessage"), Label).Text = ""
            End If
            'Sohail (10 Feb 2021) -- End

            Dim intApplicantID As Integer = 0
            'Sohail (04 Nov 2020) -- Start
            'NMB Issue # : - Error Cannot insert duplicate key in applicant code column on import data in recruitment.
            objApplicant._WebFormName = mstrModuleName
            'Sohail (04 Nov 2020) -- End
            If objApplicant.InsertUpdateApplicantByEmployeeID(CInt(Session("Employeeunkid")), CStr(Session("E_Email")), CInt(Session("LocalizationCountryUnkid")), DateAndTime.Now.Date, CInt(Session("CompanyUnkId")), CInt(Session("ApplicantCodeNotype")), CStr(Session("ApplicantCodePrifix")), intApplicantID) = False Then
                'Sohail (04 Nov 2020) - [ApplicantCodeNotype, ApplicantCodePrifix]
                If intApplicantID <= 0 Then
                    Exit Try
                End If
            End If

            Session("applicantunkid") = intApplicantID

            Dim intUnkId As Integer = 0
            If objSearchJob.AppliedVacancy(intApplicantUnkId:=CInt(Session("applicantunkid")) _
                                       , intVacancyUnkId:=CInt(dlVaanciesList.DataKeys(Idx).ToString) _
                                           , dtEarliest_possible_startdate:=dtp.GetDate _
                                           , strComments:=txtCom.Text.Trim _
                                           , strVacancy_found_out_from:=If(chkVFF.Checked = True, txtVFF.Text.Trim, "").ToString _
                                           , intVacancy_found_out_from_UnkId:=CInt(If(chkVFF.Checked = True, 0, CInt(cboVFF.SelectedValue))) _
                                           , intLoginEmployeeunkId:=CInt(Session("employeeunkid")) _
                                           , intRet_UnkID:=intUnkId _
                                           ) = True Then

                dlVaanciesList.DataBind()

                If CBool(Session("isvacancyalert")) Then

                End If

                If Convert.ToBoolean(Session("issendjobconfirmationemail")) Then

                    Dim sFName As String = CStr(Session("Firstname"))
                    Dim sLName As String = CStr(Session("Surname"))

                    If sFName.Trim = "" Then sFName = Session("E_Email")
                    Dim strSubject As String = Language.getMessage(mstrModuleName, 53, "Your application is successfully sent to ") + CStr(Session("CompName"))

                    Dim strValue As String = (New clsConfigOptions).GetKeyValue(CInt(Session("CompanyUnkId")) _
                                                              , strKeyName:="JOBCONFIRMATIONTEMPLATEID" _
                                                              )

                    Dim strBody As String = ""
                    Dim mstrFilePath As String = ""
                    Dim intID As Integer
                    Integer.TryParse(strValue, intID)
                    Dim intTemplateFound As Boolean = False
                    If intID > 0 AndAlso intUnkId > 0 Then
                        Dim objLetterType As New clsLetterType
                        Dim dsLetter As DataSet = objLetterType.getList(clsCommon_Master.enCommonMaster.LETTER_TYPE _
                                                                                      , "List" _
                                                                                      , intUnkId:=intID _
                                                                                      )

                        If dsLetter IsNot Nothing AndAlso dsLetter.Tables(0).Rows.Count > 0 Then
                            Dim strLetterContent As String = dsLetter.Tables(0).Rows(0).Item("Lettercontent")
                            Dim intFIdx As Integer = strLetterContent.IndexOf(CChar("{")) 'to remove xml start and end tags
                            Dim intLIdx As Integer = strLetterContent.LastIndexOf(CChar("}")) 'to remove xml start and end tags
                            strLetterContent = strLetterContent.Substring(intFIdx, intLIdx - intFIdx + 1) 'to remove xml start and end tags

                            If strLetterContent.Trim.Length > 0 Then
                                Dim objApplied As New clsApplicant_Vacancy_Mapping
                                Dim dsAppVac As DataSet = clsApplicant_Vacancy_Mapping.GetApplicantAppliedJob(intApplicantUnkId:=CInt(Session("applicantunkid")) _
                                                                 , intAppvacancytranunkid:=intUnkId _
                                                                 )

                                If dsAppVac.Tables(0).Rows.Count > 0 Then
                                    Dim objLF As New clsLetterFields
                                    Dim dsField As DataSet = objLF.GetList(dsLetter.Tables(0).Rows(0).Item("Fieldtypeid"))
                                    intTemplateFound = True
                                    Dim strDataName As String = ""
                                    Dim StrCol As String = ""

                                    strLetterContent = strLetterContent.Replace("#CompanyCode#", Session("CompCode").ToString)
                                    strLetterContent = strLetterContent.Replace("#CompanyName#", Session("CompName").ToString)

                                    For Each dsRow As DataRow In dsField.Tables(0).Rows
                                        For Each col As DataColumn In dsField.Tables(0).Columns
                                            'StrCol = dsRow.Item("Name").ToString
                                            StrCol = col.ColumnName

                                            If strLetterContent.Contains("#" & StrCol & "#") Then

                                                If StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                                                    strDataName = strLetterContent.Replace("#" & StrCol & "#", "#imgcmp#")
                                                Else
                                                    If dsAppVac.Tables(0).Columns.Contains(StrCol) = True Then
                                                        If StrCol = "Pay_Range_From" Then
                                                            strDataName = strLetterContent.Replace("#" & StrCol & "#", Math.Round(CDec(dsAppVac.Tables(0).Rows(0)(StrCol)), 2).ToString)
                                                        ElseIf StrCol = "Pay_Range_To" Then
                                                            strDataName = strLetterContent.Replace("#" & StrCol & "#", Math.Round(CDec(dsAppVac.Tables(0).Rows(0)(StrCol)), 2).ToString)
                                                        ElseIf StrCol = "VacancyOpeningDate" Then
                                                            strDataName = strLetterContent.Replace("#" & StrCol & "#", eZeeDate.convertDate(dsAppVac.Tables(0).Rows(0)(StrCol).ToString).ToString("dd-MMM-yyyy"))
                                                        ElseIf StrCol = "VacancyClosingDate" Then
                                                            strDataName = strLetterContent.Replace("#" & StrCol & "#", eZeeDate.convertDate(dsAppVac.Tables(0).Rows(0)(StrCol).ToString).ToString("dd-MMM-yyyy"))
                                                        Else
                                                            strDataName = strLetterContent.Replace("#" & StrCol & "#", dsAppVac.Tables(0).Rows(0)(StrCol).ToString)
                                                        End If
                                                    Else
                                                        strDataName = strLetterContent.Replace("#" & StrCol & "#", "")
                                                    End If
                                                End If

                                                strLetterContent = strDataName
                                            End If
                                        Next
                                    Next

                                    Dim htmlOutput = "Template.html"
                                    Dim contentUriPrefix = Path.GetFileNameWithoutExtension(htmlOutput)
                                    Dim htmlResult = RtfToHtmlConverter.RtfToHtml(strLetterContent, contentUriPrefix)

                                    strBody = htmlResult._HTML

                                    If Session("isattachapplicantcv") Then
                                        mstrFilePath = GetApplicantCV()
                                    End If

                                End If
                            End If
                        End If
                    End If
                    'Sohail (06 Nov 2019) -- End

                    If intTemplateFound = False Then

                        strBody = "<p style='font-size:14px'>" & Language.getMessage(mstrModuleName, 54, "Thank you for expressing interest in") & " <b>" + hfVTitle.Value + "</b> " & Language.getMessage(mstrModuleName, 55, "role") & " </p>"
                        'Gajanan (04 July 2020) -- Start
                        'Enhancment: #0004765
                        'strBody += "<p style='font-size:14px'>This email is to confirm that we have received your application. You will be contacted once it has been reviewed.</p>"
                        strBody += "<p style='font-size:14px'>" & Language.getMessage(mstrModuleName, 56, "This email is to confirm that we have received your application. You will only be contacted if shortlisted.") & "</p>"
                        'Gajanan (04 July 2020) -- End

                        If Session("isattachapplicantcv") Then
                            mstrFilePath = GetApplicantCV()
                            strBody += "<p style='font-size:14px'>" & Language.getMessage(mstrModuleName, 57, "Attached is the CV generated as per the information you provided in the application form.") & "</p>"
                        End If

                        strBody += "<p style='font-size:14px'>" & Language.getMessage(mstrModuleName, 58, "Please do not reply to this email as it is auto generated.") & "</p>"
                        Dim sValue As String = (New clsConfigOptions).GetKeyValue(CInt(Session("companyunkid")), strKeyName:="SHOWARUTISIGNATURE" _
                                                                )
                        Dim blnValue As Integer
                        Boolean.TryParse(sValue, blnValue)
                        If blnValue = True Then
                            strBody += "<p  style='font-size:12px'><b>" & Language.getMessage(mstrModuleName, 59, "Online Job Application Powered by Aruti HR & Payroll Management Software") & "</b></p> "
                        End If
                        strBody += "<p style='font-size:12px'><b>#CompanyName# Recruitment</b></p> "
                        'Hemant (12 Nov 2021) -- Start
                        'strBody += "#imgcmp#"
                        'strBody += " #imgaruti#"
                        strBody = strBody.Replace("#CompanyName#", Session("CompName").ToString)
                        'Hemant (12 Nov 2021) -- End

                        'Dim objMail As New clsMail
                        'If objMail.SendEmail(strToEmail:=Session("email") _
                        '                 , strSubject:=strSubject _
                        '                 , strBody:=strBody _
                        '                 , blnAddLogo:=True _
                        '                 , blnSuperAdmin:=False, mstrAttachedFiles:=mstrFilePath
                        '                 ) = False Then

                        '    ' e.Cancel = True
                        'End If

                        'Pinkal (18-May-2018) -- End
                    End If

                    Dim objEmp As New clsEmployee_Master
                    Dim dsRepoTo As DataSet = objEmp.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, CInt(Session("Employeeunkid")), DateAndTime.Now.Date, DateAndTime.Now.Date, CStr(Session("Database_Name")))
                    Dim strReportingEmail As String = String.Join(",", (From p In dsRepoTo.Tables(0) Where (p.Item("email").ToString.Trim <> "") Select (p.Item("email").ToString)).ToArray)
                    'For Each rprow As DataRow In dsRepoTo.Tables(0).Rows
                    '    If rprow.Item("email").ToString.Trim = "" Then Continue For

                    'Next

                    Dim objSendMail As New clsSendMail
                    objSendMail._ToEmail = Session("E_Email")
                    objSendMail._Subject = strSubject
                    objSendMail._Message = strBody
                    objSendMail._AttachedFiles = mstrFilePath
                    objSendMail._Form_Name = mstrModuleName
                    objSendMail._LogEmployeeUnkid = CInt(Session("E_Employeeunkid"))
                    objSendMail._OperationModeId = enLogin_Mode.EMP_SELF_SERVICE
                    objSendMail._UserUnkid = -1
                    objSendMail._SenderAddress = Session("Senderaddress")
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT
                    objSendMail.SendMail(CInt(Session("companyunkid")), False)

                    If strReportingEmail.Trim.Length > 0 Then
                        objSendMail = New clsSendMail
                        objSendMail._ToEmail = strReportingEmail
                        objSendMail._Subject = sFName & " " & sLName & " " & Language.getMessage(mstrModuleName, 60, "application is successfully sent to ") + CStr(Session("CompName"))
                        objSendMail._Message = strBody
                        objSendMail._AttachedFiles = mstrFilePath
                        objSendMail._Form_Name = mstrModuleName
                        objSendMail._LogEmployeeUnkid = CInt(Session("E_Employeeunkid"))
                        objSendMail._OperationModeId = enLogin_Mode.EMP_SELF_SERVICE
                        objSendMail._UserUnkid = -1
                        objSendMail._SenderAddress = Session("Senderaddress")
                        objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT
                        objSendMail.SendMail(CInt(Session("companyunkid")), False)
                    End If

                End If

            End If

            Call FillVacancyList(Session("Database_Name").ToString, 9)
            DisplayMessage.DisplayMessage("Job Applied Successfully !", Me)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApplicant = Nothing
            objSearchJob = Nothing
        End Try
    End Sub

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)

            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(lblHeader.ID, Me.lblHeader.Text)
            Language._Object.setCaption(Me.btnSearchVacancies.ID, Me.btnSearchVacancies.Text)
            'Hemant (08 Jul 2021) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            For Each dli As DataListItem In dlVaanciesList.Items
                Dim lblJobDiscription As Label = CType(dli.FindControl("lblJobDiscription"), Label)
                Language._Object.setCaption(lblJobDiscription.ID, lblJobDiscription.Text)
                Dim lblResponsblity As Label = CType(dli.FindControl("lblResponsblity"), Label)
                Language._Object.setCaption(lblResponsblity.ID, lblResponsblity.Text)
                Dim lblSkill As Label = CType(dli.FindControl("lblSkill"), Label)
                Language._Object.setCaption(lblSkill.ID, lblSkill.Text)
                Dim lblQualification As Label = CType(dli.FindControl("lblQualification"), Label)
                Language._Object.setCaption(lblQualification.ID, lblQualification.Text)
                Dim lblExp As Label = CType(dli.FindControl("lblExp"), Label)
                Language._Object.setCaption(lblExp.ID, lblExp.Text)
                Dim lblNoPosition As Label = CType(dli.FindControl("lblNoPosition"), Label)
                Language._Object.setCaption(lblNoPosition.ID, lblNoPosition.Text)
                Dim lblLang As Label = CType(dli.FindControl("lblLang"), Label)
                Language._Object.setCaption(lblLang.ID, lblLang.Text)
                Dim lblScale As Label = CType(dli.FindControl("lblScale"), Label)
                Language._Object.setCaption(lblScale.ID, lblScale.Text)
                Dim lblOpeningDate As Label = CType(dli.FindControl("lblOpeningDate"), Label)
                Language._Object.setCaption(lblOpeningDate.ID, lblOpeningDate.Text)
                Dim lblClosingDate As Label = CType(dli.FindControl("lblClosingDate"), Label)
                Language._Object.setCaption(lblClosingDate.ID, lblClosingDate.Text)
                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-508 - Display Job Location on published vacancy.
                Dim lblJobLocation As Label = CType(dli.FindControl("lblJobLocation"), Label)
                Language._Object.setCaption(lblJobLocation.ID, lblJobLocation.Text)
                'Hemant (01 Nov 2021) -- End
            Next
            'Hemant (08 Jul 2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)

            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblHeader.Text = Language._Object.getCaption(lblHeader.ID, Me.lblHeader.Text)
            Me.btnSearchVacancies.Text = Language._Object.getCaption(btnSearchVacancies.ID, Me.btnSearchVacancies.Text)
            'Hemant (08 Jul 2021) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            For Each dli As DataListItem In dlVaanciesList.Items
                Dim lblJobDiscription As Label = CType(dli.FindControl("lblJobDiscription"), Label)
                lblJobDiscription.Text = Language._Object.getCaption(lblJobDiscription.ID, lblJobDiscription.Text)
                Dim lblResponsblity As Label = CType(dli.FindControl("lblResponsblity"), Label)
                lblResponsblity.Text = Language._Object.getCaption(lblResponsblity.ID, lblResponsblity.Text)
                Dim lblSkill As Label = CType(dli.FindControl("lblSkill"), Label)
                lblSkill.Text = Language._Object.getCaption(lblSkill.ID, lblSkill.Text)
                Dim lblQualification As Label = CType(dli.FindControl("lblQualification"), Label)
                lblQualification.Text = Language._Object.getCaption(lblQualification.ID, lblQualification.Text)
                Dim lblExp As Label = CType(dli.FindControl("lblExp"), Label)
                lblExp.Text = Language._Object.getCaption(lblExp.ID, lblExp.Text)
                Dim lblNoPosition As Label = CType(dli.FindControl("lblNoPosition"), Label)
                lblNoPosition.Text = Language._Object.getCaption(lblNoPosition.ID, lblNoPosition.Text)
                Dim lblLang As Label = CType(dli.FindControl("lblLang"), Label)
                lblLang.Text = Language._Object.getCaption(lblLang.ID, lblLang.Text)
                Dim lblScale As Label = CType(dli.FindControl("lblScale"), Label)
                lblScale.Text = Language._Object.getCaption(lblScale.ID, lblScale.Text)
                Dim lblOpeningDate As Label = CType(dli.FindControl("lblOpeningDate"), Label)
                lblOpeningDate.Text = Language._Object.getCaption(lblOpeningDate.ID, lblOpeningDate.Text)
                Dim lblClosingDate As Label = CType(dli.FindControl("lblClosingDate"), Label)
                lblClosingDate.Text = Language._Object.getCaption(lblClosingDate.ID, lblClosingDate.Text)
                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-508 - Display Job Location on published vacancy.
                Dim lblJobLocation As Label = CType(dli.FindControl("lblJobLocation"), Label)
                lblJobLocation.Text = Language._Object.getCaption(lblJobLocation.ID, lblJobLocation.Text)
                'Hemant (01 Nov 2021) -- End
            Next
            'Hemant (08 Jul 2021) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Email:")
            Language.setMessage(mstrModuleName, 2, "Mobile:")
            Language.setMessage(mstrModuleName, 3, "Tel. No.:")
            Language.setMessage(mstrModuleName, 4, "Fax:")
            Language.setMessage(mstrModuleName, 5, "Alt. Tel. No.:")
            Language.setMessage(mstrModuleName, 6, "Personal Information")
            Language.setMessage(mstrModuleName, 7, "Name:")
            Language.setMessage(mstrModuleName, 8, "Gender:")
            Language.setMessage(mstrModuleName, 9, "Birth Date:")
            Language.setMessage(mstrModuleName, 10, "Marital Status:")
            Language.setMessage(mstrModuleName, 11, "Married Date:")
            Language.setMessage(mstrModuleName, 12, "Nationality:")
            Language.setMessage(mstrModuleName, 13, "Languages Known:")
            Language.setMessage(mstrModuleName, 14, "Mother Tongue:")
            Language.setMessage(mstrModuleName, 15, "Impaired:")
            Language.setMessage(mstrModuleName, 16, "Current Salary:")
            Language.setMessage(mstrModuleName, 17, "Expected Salary:")
            Language.setMessage(mstrModuleName, 18, "Expected Benefits:")
            Language.setMessage(mstrModuleName, 19, "Willing to relocate:")
            Language.setMessage(mstrModuleName, 20, "Willing to travel:")
            Language.setMessage(mstrModuleName, 21, "Notice Period (Days):")
            Language.setMessage(mstrModuleName, 22, "Permanent Address:")
            Language.setMessage(mstrModuleName, 23, "Key Skills:")
            Language.setMessage(mstrModuleName, 24, "Qualification")
            Language.setMessage(mstrModuleName, 25, "Qualification")
            Language.setMessage(mstrModuleName, 26, "Institution")
            Language.setMessage(mstrModuleName, 27, "Result")
            Language.setMessage(mstrModuleName, 28, "Duration")
            Language.setMessage(mstrModuleName, 29, "GPA Point")
            Language.setMessage(mstrModuleName, 30, "Present")
            Language.setMessage(mstrModuleName, 31, "Work Experience")
            Language.setMessage(mstrModuleName, 32, "Company:")
            Language.setMessage(mstrModuleName, 33, "Employer:")
            Language.setMessage(mstrModuleName, 34, "Designation:")
            Language.setMessage(mstrModuleName, 35, "Duration:")
            Language.setMessage(mstrModuleName, 36, "Responsibility:")
            Language.setMessage(mstrModuleName, 37, "Achievement:")
            Language.setMessage(mstrModuleName, 38, "Leaving Reason:")
            Language.setMessage(mstrModuleName, 39, "Other Information")
            Language.setMessage(mstrModuleName, 40, "Membership:")
            Language.setMessage(mstrModuleName, 41, "Achievements:")
            Language.setMessage(mstrModuleName, 42, "Links / Journals / Research Papers:")
            Language.setMessage(mstrModuleName, 43, "References")
            Language.setMessage(mstrModuleName, 44, "Contact Details")
            Language.setMessage(mstrModuleName, 45, "Mobile No. :")
            Language.setMessage(mstrModuleName, 46, "Tel. No. :")
            Language.setMessage(mstrModuleName, 47, "Please enter Vacancy Found Out From.")
            Language.setMessage(mstrModuleName, 48, "Please select Vacancy Found Out From.")
            Language.setMessage(mstrModuleName, 49, "Please enter Earliest Possible Start Date.")
            Language.setMessage(mstrModuleName, 50, "Earliest Possible Start Date should be greater than current date.")
            Language.setMessage(mstrModuleName, 51, "Please enter Comments.")
            Language.setMessage(mstrModuleName, 52, "Please accept the Declaration to proceed.")
            Language.setMessage(mstrModuleName, 53, "Your application is successfully sent to")
            Language.setMessage(mstrModuleName, 54, "Thank you for expressing interest in")
            Language.setMessage(mstrModuleName, 55, "role")
            Language.setMessage(mstrModuleName, 56, "This email is to confirm that we have received your application. You will only be contacted if shortlisted.")
            Language.setMessage(mstrModuleName, 57, "Attached is the CV generated as per the information you provided in the application form.")
            Language.setMessage(mstrModuleName, 58, "Please do not reply to this email as it is auto generated.")
            Language.setMessage(mstrModuleName, 59, "Online Job Application Powered by Aruti HR & Payroll Management Software")
            Language.setMessage(mstrModuleName, 60, "application is successfully sent to")
            Language.setMessage(mstrModuleName, 61, "Please login from ESS to view this page.")
            Language.setMessage(mstrModuleName, 62, "Sorry, Your profile is not complete.")
            Language.setMessage(mstrModuleName, 63, "Are you sure you want to apply for this job?")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>

    
End Class
