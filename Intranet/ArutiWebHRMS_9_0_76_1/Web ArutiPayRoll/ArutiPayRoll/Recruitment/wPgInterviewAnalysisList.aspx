﻿<%@ Page Title="Interview Analysis List" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgInterviewAnalysisList.aspx.vb" Inherits="wPgInterviewAnalysisList" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirm" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Interview Analysis List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 10%;">
                                                <asp:Label ID="lblVacanyType" runat="server" Text="Vacancy Type"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:DropDownList ID="cboVacType" runat="server" AutoPostBack="true" />
                                            </td>
                                            <td style="width: 10%;">
                                                <asp:Label ID="lblVacancy" runat="server" Text="Vacancy"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:DropDownList ID="cboVacancy" runat="server" AutoPostBack="true" />
                                            </td>
                                            <td style="width: 10%;">
                                                <asp:Label ID="lblBatch" runat="server" Text="Batch"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:DropDownList ID="cboBatch" runat="server" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">
                                                <asp:Label ID="lblResult" runat="server" Text="Result"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:DropDownList ID="cboResult" runat="server" />
                                            </td>
                                            <td style="width: 10%;">
                                                <asp:Label ID="lblApplicant" runat="server" Text="Applicant"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:DropDownList ID="cboApplicant" runat="server" />
                                            </td>
                                            <td style="width: 10%;">
                                                <asp:Label ID="lblInterviewType" runat="server" Text="Interview Type"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:DropDownList ID="cboInterviewType" runat="server" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">
                                                <asp:Label ID="lblAnalysisDateFrom" runat="server" Text="Analysis Date"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <uc2:DateCtrl runat="server" ID="dtFromDate" AutoPostBack="true" />
                                            </td>
                                            <td style="width: 10%;">
                                                <asp:Label ID="lbAnalysisDateTo" runat="server" Text="To"></asp:Label>
                                            </td>
                                            <td style="width: 20%;">
                                                <uc2:DateCtrl AutoPostBack="false" runat="server" ID="dtToDate" />
                                            </td>
                                            <td style="width: 10%;">
                                                <asp:CheckBox ID="chkEligible" runat="server" Text="Eligible" />
                                            </td>
                                            <td style="width: 20%;">
                                                <asp:CheckBox ID="chkNotEligible" runat="server" Text="Not Eligible" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnEligible" runat="server" CssClass="btndefault" Text="Eligible Operation" />
                                        <asp:Button ID="btnNEligible" runat="server" CssClass="btndefault" Text="Not Eligible Operation" />
                                        <asp:Button ID="btnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                        <asp:Button ID="btnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                                <asp:Panel ID="pnlGridDate" runat="server" ScrollBars="Auto" Width="100%" Height="300px">
                                    <asp:GridView ID="dgvData" runat="server" AllowPaging="false" Width="99%" CellPadding="3"
                                        ShowFooter="false" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                        HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="25">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" AutoPostBack="true" OnCheckedChanged="chkHeder1_CheckedChanged" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" CommandArgument='<%# Container.DataItemIndex %>'
                                                        AutoPostBack="true" OnCheckedChanged="chkbox1_CheckedChanged" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="interview_type" HeaderText="Interview Type" FooterText="dgcolhInterviewType" />
                                            <asp:BoundField DataField="analysis_date" HeaderText="Analysis Date" FooterText="dgcolhAnalysisDate" />
                                            <asp:BoundField DataField="reviewer" HeaderText="Reviewer" FooterText="dgcolhReviewer" />
                                            <asp:BoundField DataField="score" HeaderText="Score" FooterText="dgcolhScore" />
                                            <asp:BoundField DataField="remark" HeaderText="Remark" FooterText="dgcolhRemark" />
                                            <asp:BoundField DataField="analysisunkid" HeaderText="analysisunkid" Visible="false" />
                                            <asp:BoundField DataField="appbatchscheduletranunkid" HeaderText="appbatchscheduletranunkid"
                                                Visible="false" />
                                            <asp:BoundField DataField="applicantunkid" HeaderText="applicantunkid" Visible="false" />
                                            <asp:BoundField DataField="grp_id" HeaderText="grp_id" Visible="false" />
                                            <asp:BoundField DataField="sort_id" HeaderText="sort_id" Visible="false" />
                                            <asp:BoundField DataField="vacancyunkid" HeaderText="vacancyunkid" Visible="false" />
                                            <asp:BoundField DataField="is_grp" HeaderText="is_grp" Visible="false" />
                                        </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popOperation" runat="server" BackgroundCssClass="ModalPopupBG"
                        CancelControlID="btnpopClose" PopupControlID="pnlOperation" TargetControlID="btnHide"
                        Drag="true" PopupDragHandleControlID="pnlOperation">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlOperation" runat="server" CssClass="newpopup" Style="display: none;
                        width: 800px" DefaultButton="btnpopClose">
                        <div class="panel-primary">
                            <div class="panel-heading">
                                <asp:Label ID="lblCaption" runat="server" Text="Approve/Diapprove Applicant Eligiblilty" />
                            </div>
                            <div class="panel-body">
                                <div id="Div1" class="panel-default">
                                    <div id="Div2" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div3" class="panel-body-default">
                                        <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Width="100%" Height="200px">
                                            <asp:GridView ID="dgvViewData" runat="server" AllowPaging="false" Width="99%" CssClass="gridview"
                                                HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" HeaderStyle-Font-Bold="false"
                                                ShowFooter="false">
                                                <Columns>
                                                    <asp:BoundField DataField="interview_type" HeaderText="Interview Type" FooterText="dgcolhInterviewType_AD" />
                                                    <asp:BoundField DataField="analysis_date" HeaderText="Analysis Date" FooterText="dgcolhAnalysisDate_AD" />
                                                    <asp:BoundField DataField="reviewer" HeaderText="Reviewer" FooterText="dgcolhReviewer_AD" />
                                                    <asp:BoundField DataField="score" HeaderText="Score" FooterText="dgcolhScore_AD" />
                                                    <asp:BoundField DataField="remark" HeaderText="Remark" FooterText="dgcolhRemark_AD" />
                                                    <asp:BoundField DataField="analysisunkid" HeaderText="analysisunkid" Visible="false" />
                                                    <asp:BoundField DataField="appbatchscheduletranunkid" HeaderText="appbatchscheduletranunkid"
                                                        Visible="false" />
                                                    <asp:BoundField DataField="applicantunkid" HeaderText="applicantunkid" Visible="false" />
                                                    <asp:BoundField DataField="grp_id" HeaderText="grp_id" Visible="false" />
                                                    <asp:BoundField DataField="sort_id" HeaderText="sort_id" Visible="false" />
                                                    <asp:BoundField DataField="vacancyunkid" HeaderText="vacancyunkid" Visible="false" />
                                                    <asp:BoundField DataField="is_grp" HeaderText="is_grp" Visible="false" />
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                    <div id="Div4" class="panel-body-default">
                                        <table width="100%">
                                            <tr width="100%">
                                                <td style="width: 70%;">
                                                    <asp:Label ID="lblRemark" runat="server" Text="Remark" />
                                                </td>
                                                <td style="width: 10%;">
                                                    <asp:Label ID="lblDate" runat="server" Text="Date" />
                                                </td>
                                                <td style="width: 20%">
                                                    <uc2:DateCtrl ID="dtApprDate" runat="server" AutoPostBack="true" />
                                                </td>
                                            </tr>
                                            <tr width="100%">
                                                <td colspan="3" width="100%">
                                                    <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" Rows="3" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btnDefault" />
                                            <asp:Button ID="btnDisapprove" runat="server" Text="Disapprove" CssClass="btnDefault" />
                                            <asp:Button ID="btnpopClose" runat="server" Text="Cancel" CssClass="btnDefault" />
                                            <asp:HiddenField ID="btnHide" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <uc4:Confirm ID="popConfirm1" runat="server" Title="" Message="" />
                    <uc4:Confirm ID="popConfirm2" runat="server" Title="" Message="" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
