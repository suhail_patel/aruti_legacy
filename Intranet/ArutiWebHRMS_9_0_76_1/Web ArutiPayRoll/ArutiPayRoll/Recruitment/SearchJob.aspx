﻿<%@ Page Title="Career | Jobs" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="SearchJob.aspx.vb" Inherits="Recruitment_SearchJob" %>

<%@ Register Src="../Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link rel="stylesheet" href="../font-awesome-4.3.0/css/font-awesome.css" />
    <link rel="stylesheet" href="../App_Themes/blue/style.css" />
    <%--<link rel="stylesheet" href="../Help/bootstrap.css" />--%>
    <link rel="stylesheet" href="../App_Themes/blue/blue.css" />
    <style type="text/css">
        .brow
        {
            margin-right: -15px;
            margin-left: -15px;
        }
        .bbtn
        {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        .bbtn-primary
        {
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
        }
        body
        {
            font-family: "Helvetica Neue" , Helvetica, Arial, sans-serif;
            font-size: 14px !important;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }
        .form-grouprow 
        {
            margin-bottom: 15px;
            display: flow-root;
        }
        .h3
        {
            margin-top: 20px;
            font-size: 24px;
        }
        .col-md-1 
        {
            width: 8%;
            float: left;
            min-height: 1px;
        }
        .col-md-2 
        {
            width: 16%;
            float: left;
            min-height: 1px;
        }
         .col-md-3 
        {
            width: 25%;
            float: left;
            min-height: 1px;
        }
         .col-md-4 
        {
            width: 33%;
            float: left;
            min-height: 1px;
        }
        .col-md-5 
        {
            width: 41%;
            float: left;
            min-height: 1px;
        }
        .col-md-6 
        {
            width: 49%;
            float: left;
            min-height: 1px;
        }
         .col-md-7 
        {
            width: 58%;
            float: left;
            min-height: 1px;
        }
        .col-md-8 
        {
            width: 66%;
            float: left;
            min-height: 1px;
        }
        .col-md-9 
        {
            width: 74%;
            float: left;
            min-height: 1px;
        }
         .col-md-10
         {
            width: 83%;
            float: left;
            min-height: 1px;
         }
         .col-md-11
         {
            width: 90%;
            float: left;
            min-height: 1px;
         }
        .col-md-12 
        {
            width: 100%;
            min-height: 1px;
            display: table;
        }
        .bg-success
        {
             background-color: #dff0d8;
        }
        .text-center 
        {
            text-align: center;
        }
        .text-right 
        {
            text-align: right;
        }
    </style>
    <%--<script type="text/javascript" src="../Help/jquery-3.0.0.min.js"></script>--%>
    <%--<script type="text/javascript" src="../Help/bootstrap.min.js"></script>--%>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
                      
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                gethf();                
            });
        };
       
        function toggle(obj, ispostback=false) {
           
            $(obj).closest('.paneldefault').children('#moretext').toggle('slow', function () {
                if (ispostback == false)
                {
                    setScrollPosition($(window).scrollTop());
                }
                $(window).scrollTop($('#<%=hfScrollPosition.ClientID%>').val());
            });            
            
            if ($(obj).children('span').hasClass('fa-plus-circle') == true) {
                $(obj).children('span').removeClass('fa-plus-circle');
                $(obj).children('span').addClass('fa-minus-circle');
                $(obj).children('div').text('Hide Details');
            }
            else {
                $(obj).children('span').removeClass('fa-minus-circle');
                $(obj).children('span').addClass('fa-plus-circle');
                $(obj).children('div').text('Show Details');
            }
        }

        function gethf() {
            if ($('#<% = hf.ClientID %>').val() != '') {
                toggle($('#' + $('#<% = hf.ClientID %>').val()).closest('#moretext').siblings().find('#adetails'), true);                
            }
        }

        function sethf(obj) {
            $('#<% = hf.ClientID %>').val($(obj).attr('id'));
            setScrollPosition($(window).scrollTop());
        }
              
        function setScrollPosition(scrollValue) {
            $('#<%=hfScrollPosition.ClientID%>').val(scrollValue);
        }  
     
    </script>
    
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <div id="MainDivCtrl" style="margin: 3px auto 0px auto; width: 100%; max-width: 1200px;">
            <div class="container-fluid well">
                <asp:Panel ID="pnlForm" runat="server">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <h3 class="h3" style="float: left; font-weight: 500; margin-bottom: 20px;">
                                <asp:Label ID="lblHeader" runat="server">Vacancies</asp:Label>&nbsp;<asp:Label ID="objlblCount"
                                    runat="server" Text=""></asp:Label></h3>
                            <div class="form-grouprow" style="display: none;">
                                <div class="col-md-11">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <div class="fas fa-search">
                                            </div>
                                        </div>
                                        <asp:TextBox ID="txtVacanciesSearch" runat="server" CssClass="form-control" placeholder="Search Vacancy / Company / Skill / Qualification / Responsibility / Job Description"
                                            Style="height: 34px;" />
                                    </div>
                                </div>
                                <div class="col-md-1 ">
                                    <asp:Button ID="btnSearchVacancies" runat="server" Text="Search" CssClass="btn btn-primary" />
                                </div>
                            </div>
                            <input id="hf" runat="server" type="hidden" />
                            <asp:HiddenField ID="hfScrollPosition" Value="0" runat="server" />
                            <asp:DataList ID="dlVaanciesList" runat="server" Width="100%" DataKeyField="vacancyid"
                                DataSourceID="odsVacancy" Style="font-size: 14px;">
                                <ItemTemplate>
                                    <div class="paneldefault">
                                        <div class="panelbody  bg-success" style="padding: 5px;">
                                            <div class="form-grouprow" style="margin-bottom: 5px">
                                                <div class="col-md-12 " style="margin-bottom: 5px">
                                                    <div class="col-md-1 h3" style="padding-left: 15px;">
                                                        <div class="fa fa-hand-o-right">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9 h3">
                                                        <%--<asp:Label ID="lblVacancyTitle" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("vacancytitle")) %>' ></asp:Label>--%>
                                                        <asp:Label ID="objlblVacancyTitle" runat="server" Text='<%# Eval("vacancytitle") %>'></asp:Label>
                                                        <%--  'Pinkal (01-Jan-2018) -- Start
                                              'Enhancement - Job seekers to get alerts if they already signed up account on the recruitment site.--%>
                                                        <%--<asp:HiddenField ID = "hdnfieldVacancyTitleId" runat="server" Value ='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("vacancytitleid"))%>' />--%>
                                                        <asp:HiddenField ID="hdnfieldVacancyTitleId" runat="server" Value='<%# Eval("vacancytitleid") %>' />
                                                        <%--Pinkal (01-Jan-2018) -- Start--%>
                                                        <asp:HiddenField ID="hfVTitle" runat="server" Value='<%# Eval("vacancytitle") %>'></asp:HiddenField>
                                                    </div>
                                                    <div class="col-md-2 text-right" style="margin-top: 25px;">
                                                        <div id="adetails" onclick="toggle(this, false);" style="cursor: pointer;">
                                                            <span class="fa fa-plus-circle" aria-hidden="true" style="padding-right: 5px;"></span>
                                                            <div id="lblShowDetail" style="float: right;">
                                                                Show Details</div>
                                                </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" style="display: none;">
                                                    <div class="col-md-1">
                                                    </div>
                                                    <div class="col-md-9" style="padding-left: 15px;">
                                                        <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Session("CompName").ToString) %>--%>
                                                        <asp:Label ID="objlblCompName" runat="server" Text='<%# Session("CompName").ToString %>'></asp:Label>
                                                    </div>
                                                    
                                        </div>
                                            </div>
                                        </div>
                                        <div id="moretext" style="display: none;">
                                            <div class="form-grouprow" style="padding-left: 15px; padding-top: 15px" id="divJobLocation"
                                            runat="server">
                                            <div class="col-md-2">
                                                    <asp:Label ID="lblJobLocation" runat="server" Text="Job Location :" Font-Bold="true"></asp:Label>
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="objlblJobLocation" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-grouprow" style="padding-left: 15px" id="divRemark" runat="server">
                                                <div class="col-md-2">
                                                <asp:Label ID="lblJobDiscription" runat="server" Text="Job Description :" Font-Bold="true"></asp:Label>
                                            </div>
                                            <div class="col-md-10">
                                                <asp:Label ID="objlblJobDiscription" runat="server" Text='<%# Eval("remark").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") %>'></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-grouprow" style="padding-left: 15px" id="divResponsblity" runat="server">
                                            <div class="col-md-2">
                                                <asp:Label ID="lblResponsblity" runat="server" Text="Responsiblity: " Font-Bold="true"></asp:Label>
                                            </div>
                                            <div class="col-md-10">
                                                <asp:Label ID="objlblResponsblity" runat="server" Text='<%#Eval("duties").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") %> '></asp:Label>
                                            </div>
                                        </div>
                                            <div class="form-grouprow" style="padding-left: 15px" id="divSkill" runat="server">
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblSkill" runat="server" Text="Skill Required :" Font-Bold="true"></asp:Label>
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="objlblSkill" runat="server" Text='<%# Eval("skill") %>'></asp:Label>
                                                </div>
                                            </div>
                                        <div class="form-grouprow" style="padding-left: 15px" id="divQualification" runat="server">
                                            <div class="col-md-2">
                                                <asp:Label ID="lblQualification" runat="server" Text="Qualification Required: " Font-Bold="true"></asp:Label>
                                            </div>
                                            <div class="col-md-10">
                                                <asp:Label ID="objlblQualification" runat="server" Text='<%# Eval("Qualification") %>'></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-grouprow" style="padding-left: 15px;">
                                            <div class="col-md-6" id="divExp" runat="server">
                                                <asp:Label ID="lblExp" runat="server" Text="Experience :" Font-Bold="true"></asp:Label>
                                                <%--<%# IIf(Eval("experience") <> 0, Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Format(CDec(Eval("experience")) / 12, "###0.0#")) + " Year(s)", Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment("Fresher Can Apply")) %>--%>
                                                <asp:Label ID="objlblExp" runat="server" Text='<%# IIf(Eval("experience") <> 0, Format(CDec(Eval("experience")) / 12, "###0.0#") + " Year(s)", "Fresher Can Apply") %>'></asp:Label>
                                                <br />
                                                <asp:Label ID="objlblExpCmt" runat="server" Text='<%# Eval("experience_comment")%>'></asp:Label>
                                            </div>
                                            <div class="col-md-6" id="divNoPosition" runat="server">
                                                <asp:Label ID="lblNoPosition" runat="server" Text="No. of Position :" Font-Bold="true"></asp:Label>
                                                <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("noposition")) %>--%>
                                                <asp:Label ID="objlblNoPosition" runat="server" Text='<%# Eval("noposition") %>'></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-grouprow" style="padding-left: 15px" id="divLang" runat="server">
                                            <div class="col-md-12">
                                                <asp:Label ID="lblLang" runat="server" Text="Preferred Language Skill :" Font-Bold="true"></asp:Label>
                                                <asp:Label ID="objlblLang" runat="server" Text='<%# Eval("Lang") %>'></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-grouprow" id="divScale" runat="server" style="padding-left: 15px">
                                            <div class="col-md-12">
                                                <asp:Label ID="lblScale" runat="server" Text="Scale :" Font-Bold="true"></asp:Label>
                                                <asp:Label ID="objlblScale" runat="server" Text=""></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-grouprow" style="padding-left: 15px">
                                            <div class="col-md-6" id="divOpenningDate" runat="server">
                                                <asp:Label ID="lblOpeningDate" runat="server" Text="Job Opening date :" Font-Bold="true"></asp:Label>
                                                <asp:Label ID="objlblOpeningDate" runat="server" Text='<%# CDate(Eval("openingdate")).ToShortDateString() %>'></asp:Label>
                                            </div>
                                            <div class="col-md-6" id="divClosuingDate" runat="server">
                                                <asp:Label ID="lblClosingDate" runat="server" Text="Job closing date :" Font-Bold="true"></asp:Label>
                                                <asp:Label ID="objlblClosingDate" runat="server" Text='<%# CDate(Eval("closingdate")).ToShortDateString() %>'></asp:Label>
                                            </div>
                                        </div>
                                            <asp:Panel ID="pnlExternal" runat="server" Visible="false">
                                        <div class="form-grouprow" style="padding-left: 15px" runat="server">
                                                    <div class="col-md-4" style="padding-right: 5px;">
                                                <div class="col-md-9 no-padding">
                                                    <asp:Label ID="lblVacancyFoundOutFrom" runat="server" Text="Vacancy Found Out From: "></asp:Label>
                                                </div>
                                                <div class="col-md-3 no-padding" style="text-align: right">
                                                            <asp:CheckBox ID="chkVacancyFoundOutFrom" runat="server" Text="Others" Style="font-weight: bold;"
                                                                OnCheckedChanged="chkGridOther_CheckedChanged" AutoPostBack="true" />
                                                </div>
                                                        <asp:Panel ID="pnlVacancyFoundOutFrom" runat="server" Width="100%" Style="float: left;">
                                                    <asp:DropDownList ID="ddlVacancyFoundOutFrom" runat="server" CssClass="selectpicker form-control" />
                                                </asp:Panel>
                                                <asp:Panel ID="pnlOtherVacancyFoundOutFrom" runat="server" Visible="false">
                                                    <asp:TextBox ID="txtVacancyFoundOutFrom" runat="server" CssClass="form-control"></asp:TextBox>
                                                </asp:Panel>
                                            </div>
                                            <div class="col-md-4">
                                                        <div style="width: 100%; padding-left: 15px;">
                                                            <asp:Label ID="lblEarliestPossibleStartDate" runat="server" Text="Earliest Possible Start Date: "></asp:Label>
                                                </div>
                                                        <div style="width: 100%; padding-left: 15px;">
                                                <uc2:DateCtrl ID="dtpEarliestPossibleStartDate" runat="server" AutoPostBack="false" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label ID="lblComments" runat="server" Text="Comments: "></asp:Label>
                                                <asp:TextBox ID="txtComments" runat="server" Text="" CssClass="form-control" TextMode="MultiLine"
                                                    Rows="3"></asp:TextBox>
                                            </div>
                                        </div>
                                            </asp:Panel>
                                        <% If Session("applicant_declaration").ToString.Trim.Length > 0 Then%>
                                        <asp:Panel ID="pnlDis" runat="server">
                                            <div class="panel-body  bg-warning" style="padding: 5px;">
                                                <div class="form-grouprow text-center" style="margin-bottom: 5px">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="lblValidate" runat="server" Text="Declaration" Font-Size="15px" Font-Bold="true"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-grouprow" style="padding-left: 15px">
                                                <div class="col-md-12">
                                                    <%--<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Session("applicant_declaration").ToString).ToString %>--%>
                                                    <asp:Label ID="objlblDeclare" runat="server" Text='<%# Session("applicant_declaration").ToString %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-grouprow text-center">
                                                <div class="col-md-12">
                                                        <asp:CheckBox ID="chkAccept" runat="server" Text="&nbsp; I Accept this Declaration."
                                                            Font-Bold="true" AutoPostBack="false" />
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <% End If%>
                                            <asp:Label ID="lblValidationMessage" runat="server" Text=""></asp:Label>
                                        <div class="panel-body  bg-success">
                                            <div class="form-grouprow" style="margin-bottom: 5px">
                                                <div class="col-md-12 text-right">
                                                    <div>
                                                            <asp:LinkButton ID="lnkUpdateDetail" runat="server" CommandName="UpdateDetail" CssClass="btnDefault"
                                                                Font-Underline="false">
                                                                <asp:Label ID="lblUpdateDetailText" runat="server" Text="Update Details"></asp:Label>
                                                            </asp:LinkButton>
                                                        <%--<asp:Label ID="lblIsApplyed" runat="server" Text='<%# Microsoft.Security.Application.Sanitizer.GetSafeHtmlFragment(Eval("IsApplied"))  %>' Visible="false"></asp:Label>--%>
                                                        <asp:Label ID="objlblIsApplyed" runat="server" Text='<%# Eval("IsApplied") %>' Visible="false"></asp:Label>
                                                        <asp:LinkButton ID="btnApply" runat="server" CommandName="Apply" Font-Underline="false"
                                                                CssClass="btnDefault" Text="" ValidationGroup="SearchJob" Style="text-align: center;"
                                                                OnClientClick="sethf(this);">
                                                            <div id="divApplied" runat="server" class="glyphicon glyphicon-ok" style="margin-right: 5px"
                                                                visible="false">
                                                            </div>
                                                            <asp:Label ID="lblApplyText" runat="server" Text="Apply Now"></asp:Label>
                                                        </asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                            <asp:ObjectDataSource ID="odsVacancy" runat="server" SelectMethod="GetApplicantVacancies"
                                TypeName="Aruti.Data.clsVacancy" EnablePaging="false">
                                <SelectParameters>
                                    <asp:Parameter Name="strDatabaseName" Type="String" DefaultValue="" />
                                    <%--<asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />--%>
                                    <asp:Parameter Name="intMasterTypeId" Type="Int32" DefaultValue="0" />
                                    <asp:Parameter Name="intEType" Type="Int32" DefaultValue="0" />
                                    <asp:Parameter Name="blnVacancyType" Type="Boolean" DefaultValue="True" />
                                    <asp:Parameter Name="blnAllVacancy" Type="Boolean" DefaultValue="False" />
                                    <asp:Parameter Name="intDateZoneDifference" Type="Int32" DefaultValue="0" />
                                    <asp:Parameter Name="strVacancyUnkIdLIs" Type="String" DefaultValue="" />
                                    <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                                    <asp:Parameter Name="blnOnlyCurrent" Type="Boolean" DefaultValue="True" />
                                    <asp:Parameter Name="blnOnlyExportToWeb" Type="Boolean" DefaultValue="True" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                    <uc7:Confirmation ID="cnfConfirm" runat="server"  />
                            
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSearchVacancies" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </asp:Panel>
                <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                </asp:Panel>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
