﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/home.master" CodeFile="wPg_StaffRequisition_AddEdit.aspx.vb"
    Inherits="Recruitment_Staff_Requisition_wPg_StaffRequisition_AddEdit" Title="Staff Requisition Add/Edit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript" src="../../Help/aruti_help_employee/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
    
    <script src="../../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>
    
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
            var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

function beginRequestHandler(sender, event) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, evemt) {
    $("#endreq").val("1");
}
        function IsValidAttach() {
        debugger;
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>');
           
            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            return true;
        }    

    </script>
    
     <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width","auto");
        }
        
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 99%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Staff Requisition Add/Edit"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Staff Requisition Add/Edit"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <div class="ib" style="width: 10%;">
                                            <asp:Label ID="lblFormNo" runat="server" Text="Form No."></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%; padding-right: 0 !important">
                                            <asp:TextBox ID="txtFormNo" runat="server" Width="223px"></asp:TextBox>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblStatus" runat="server" Text="Requisition Type"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboStatus" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblLeavingReason" runat="server" Text="Leaving Reason"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 22%">
                                            <asp:DropDownList ID="cboLeavingReason" runat="server" AutoPostBack="false" />
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblAllocation" runat="server" Text="Requisition By"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboAllocation" AutoPostBack="true" runat="server" Width="225px">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboName" AutoPostBack="false" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblEmploymentType" runat="server" Text="Employment Type"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 22%">
                                            <asp:DropDownList ID="cboEmploymentType" runat="server" AutoPostBack="True" />
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblClassGroup" runat="server" Text="Class Group"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboClassGroup" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblClass" runat="server" Text="Class"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboClass" AutoPostBack="false" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 22%">
                                            <asp:Label ID="lblContractDuration" runat="server" Text="Contract Duration (months) if Employment type is Contract"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:TextBox ID="nudContractDuration" runat="server" Enabled="false" Style="text-align: right;
                                                background-color: White"></asp:TextBox>
                                            <cc1:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server" Width="75"
                                                Minimum="0" Maximum="99999" TargetControlID="nudContractDuration">
                                            </cc1:NumericUpDownExtender>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblJob" runat="server" Text="Job"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboJob" runat="server" AutoPostBack="true" />
                                        </div>
                                        <div class="ib" style="width: 31%; vertical-align: top;">
                                            <table style="border: 2px solid #DDD; border-collapse: collapse" width="100%">
                                                <tr style="border: 2px solid #DDD">
                                                    <td style="width: 70%; border-right: 2px solid #DDD;">
                                                        <asp:Label ID="lblPlanned" runat="server" Text="Planned Head Counts" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%;text-align:right;">
                                                        <asp:Label ID="objlblPlanned" runat="server" Text="0" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr style="border: 2px solid #DDD">
                                                    <td style="width: 70%; border-right: 2px solid #DDD;">
                                                        <asp:Label ID="lblAvailable" runat="server" Text="Available Head Counts" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%;text-align:right;">
                                                        <asp:Label ID="objlblAvailable" runat="server" Text="0" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr style="border: 2px solid #DDD">
                                                    <td style="width: 70%; border-right: 2px solid #DDD;">
                                                        <asp:Label ID="lblVariation" runat="server" Text="Variations" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td style="width: 30%;text-align:right;">
                                                        <asp:Label ID="objlblVariation" runat="server" Text="0" Font-Bold="true"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblAddStaffReason" runat="server" Text="Additional Staff Reason"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 22%">
                                            <asp:TextBox ID="txtAddStaffReason" runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblJobDesc" runat="server" Text="Job Description &amp; Indicators"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:TextBox ID="txtJobDesc" runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                            <asp:LinkButton ID="lnkViewJobDescription" runat="server" Text="View Job Description"
                                                CssClass="lnkhover" Style="color: Blue; vertical-align: top; float: left;"></asp:LinkButton>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblNoofPosition" runat="server" Text="No. of Position"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:TextBox ID="txtPosition" runat="server" Enabled="false" Style="text-align: right;
                                                background-color: White"></asp:TextBox>
                                            <cc1:NumericUpDownExtender ID="nuePosition" runat="server" Width="75" Minimum="1"
                                                Maximum="99999" TargetControlID="txtPosition">
                                            </cc1:NumericUpDownExtender>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblWorkStartDate" runat="server" Text="Date to Start Work"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 22%">
                                            <uc2:DateControl ID="dtpWorkStartDate" runat="server" />
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 10%">
                                             <asp:Label ID="lblJobReportTo" runat="server" Text="Job Reporting Line"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboJobReportTo" runat="server" AutoPostBack="false" />
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblJobAdvert" runat="server" Text="Job Advert"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboJobAdvert" runat="server" AutoPostBack="false" />
                                        </div> 
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblGrade" runat="server" Text="Grade"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboGrade" AutoPostBack="true" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 10%">
                                            <asp:Label ID="lblGradeLevel" runat="server" Text="Grade Level"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:DropDownList ID="cboGradeLevel" AutoPostBack="false" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                       
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 48%">
                                            <div class="row2">
                                                <div class="ib" style="width: 21%">
                                                    <asp:Label ID="lblSearchEmp" runat="server" Text="Search Employee"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 73%">
                                                    <asp:TextBox ID="txtSearchEmp" runat="server" AutoPostBack="true"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 98%">
                                                    <asp:Panel ID="pnl_dgvEmployeeList" runat="server" Width="100%" Height="350px" ScrollBars="Auto">
                                                        <asp:DataGrid ID="dgvEmployeeList" runat="server" AutoGenerateColumns="false" CellPadding="3"
                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                            Width="99%">
                                                            <Columns>
                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                    FooterText="objcolhCheckAll">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkAllSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkAllSelect" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Eval("ischecked") %>'
                                                                            OnCheckedChanged="chkSelect" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="employeeunkid" Visible="false" FooterText="objcolhEmpID">
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="employeecode" FooterText="colhEmpCode" HeaderText="Code">
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="name" FooterText="colhEmpName" HeaderText="Employee Name">
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="job_name" FooterText="colhJob" HeaderText="Job"></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="grade" FooterText="colhGrade" HeaderText="Grade"></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="gradelevel" FooterText="colhSalaryBand" HeaderText="Salary Band">
                                                                </asp:BoundColumn>
                                                                <asp:BoundColumn DataField="actionreasonunkid" Visible="false" FooterText="objcolhActionReasonId"
                                                                    HeaderText=""></asp:BoundColumn>
                                                                <asp:BoundColumn DataField="AUD" Visible="false" FooterText="objcolhAUD" HeaderText="">
                                                                </asp:BoundColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 21%">
                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee to be replaced"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 74%">
                                                    <asp:CheckBox ID="chkShowInActiveEmployee" runat="server" Text="Show Inactive Employee"
                                                        AutoPostBack="true"></asp:CheckBox>
                                                    <asp:CheckBox ID="chkShowPreviousJobEmp" runat="server" Text="Show Previous Job"
                                                        AutoPostBack="true"></asp:CheckBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ib" style="width: 48%;vertical-align:top">
                                            <div class="row2">
                                                <div class="ib" style="width: 18%">
                                                    <asp:Label ID="lblDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                </div>
                                                <div class="ib" style="width: 30%">
                                                    <asp:DropDownList ID="cboDocumentType" runat="server" Width="160px">
                                                    </asp:DropDownList>
                                                </div>                                                
                                                <div class="ib" style="width: 19%;vertical-align: top;">
                                                <asp:Panel ID="pnl_ImageAdd" runat="server" >
                                                    <div id="fileuploader">
                                                        <input type="button" id="btnAddFile" runat="server" class="btndefault" onclick="return IsValidAttach()"
                                                            value="Browse" />
                                                    </div>
                                                </asp:Panel>
                                                </div>
                                                
                                                <div class="ib" style="width: 22%">
                                                    <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                                </div>
                                                
                                                <div class="ib" style="width: 100%">
                                                    <asp:Button ID="btnAddAttachment" runat="server" Style="display: none" Text="Browse"/>
                                                </div>
                                            </div>
                                            <div class="row2">
                                                <div class="ib" style="width: 98%">
                                                    <asp:DataGrid ID="dgvQualification" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                        Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                        HeaderStyle-Font-Bold="false">
                                                        <Columns>
                                                            <asp:TemplateColumn FooterText="objcohDelete">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                            ToolTip="Delete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                            <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                            <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center"
                                                                ItemStyle-Font-Size="22px">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Delete">
                                                                                                            <i class="fa fa-download"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                Visible="false" />
                                                            <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                Visible="false" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btnDefault" Text="Save" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btnDefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc4:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                </ContentTemplate>
                 <Triggers>
                    <asp:PostBackTrigger ControlID="btnDownloadAll" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <script>
    
			$(document).ready(function()
			{
				ImageLoad();
				$(".ajax-upload-dragdrop").css("width","auto");
			});
			function ImageLoad(){
			debugger;
			    if ($(".ajax-upload-dragdrop").length <= 0){
			    $("#fileuploader").uploadFile({
				    url: "wPg_StaffRequisition_AddEdit.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
				    dragDropStr: "",
				    showStatusAfterSuccess:false,
                    showAbort:false,
                    showDone:false,
				    fileName:"myfile",
				    onSuccess:function(path,data,xhr){
				        $("#<%= btnAddAttachment.ClientID %>").click();
				    },
                    onError:function(files,status,errMsg){
	                        alert(errMsg);
                        }
                });
			}
			}
			$('input[type=file]').live("click",function(){
			    return IsValidAttach();
			});
    </script>

</asp:Content>
