﻿
#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports System.Net.Dns
#End Region

Partial Class wPg_StaffRequisitionList
    Inherits Basepage

#Region " Private Variable(s) "

    Dim DisplayMessage As New CommonCodes
    'Private objStaffRequisition As clsStaffrequisition_Tran

    Private mdtList As DataTable
    Private mdicAllocaions As New Dictionary(Of Integer, String)
    Private mdicApprovalStatus As New Dictionary(Of Integer, String)
    Private mstrModuleName As String = "frmStaffRequisitionList"
#End Region

#Region " Form's Event(s) "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mdicAllocaions", mdicAllocaions)
            Me.ViewState.Add("mdicApprovalStatus", mdicApprovalStatus)
            Me.ViewState.Add("mdtList", mdtList)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Recruitment_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            Call SetLanguage()

            If (Page.IsPostBack = False) Then
                Call SetVisibility()
                Call CreateListTable()
                Call Fill_Combo()

                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
                chkMyApprovals.Checked = True
                'Sohail (12 Oct 2018) -- End
            Else
                mdicAllocaions = ViewState("mdicAllocaions")
                mdicApprovalStatus = ViewState("mdicApprovalStatus")
                mdtList = ViewState("mdtList")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub Fill_Combo()
        Dim objUsr As New clsUserAddEdit
        Dim objLevel As New clsStaffRequisitionApproverlevel_master
        Dim objOption As New clsPassowdOptions
        Dim objMaster As New clsMasterData
        Dim dsCombo As New DataSet
        Try

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
            'dsCombo = objMaster.GetEAllocation_Notification("List")
            dsCombo = objMaster.GetEAllocation_Notification("List", , , True)
            'Sohail (12 Oct 2018) -- End
            With cboAllocation
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                Call cboAllocation_SelectedIndexChanged(cboAllocation, New System.EventArgs)
            End With
            mdicAllocaions = (From p In dsCombo.Tables("List").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            dsCombo = objMaster.getApprovalStatus("ApprovalStatus", True, , True, True, True, True, True)
            mdicApprovalStatus = (From p In dsCombo.Tables("ApprovalStatus").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            dsCombo = objMaster.getApprovalStatus("List", True, True, True, True, True, True, True)
            With cboStatus
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = enApprovalStatus.PENDING
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsCombo.Dispose() : objUsr = Nothing : objLevel = Nothing : objOption = Nothing
        End Try
    End Sub

    Private Sub CreateListTable()
        Try
            mdtList = New DataTable
            With mdtList
                .Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
                .Columns.Add("formno", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("staffrequisitiontranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("staffrequisitionbyid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("staffrequisitionby", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("allocationunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("JobTitle", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("jobunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("jobdescrription", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("workstartdate", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("form_statusunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("form_status", System.Type.GetType("System.String")).DefaultValue = ""
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_List()
        Dim objStaffRequisition As New clsStaffrequisition_Tran
        Dim dsList As New DataSet
        Dim StrSearch As String = String.Empty
        Try
            mdtList.Rows.Clear()
            If CBool(Session("AllowToViewStaffRequisitionList")) = False Then Exit Try

            If CInt(cboAllocation.SelectedValue) > 0 Then
                StrSearch &= "AND rcstaffrequisition_tran.staffrequisitionbyid = " & CInt(cboAllocation.SelectedValue) & " "
            End If

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            'If CInt(cboName.SelectedValue) > 0 Then
            If cboName.SelectedValue.Trim <> "" AndAlso CInt(cboName.SelectedValue) > 0 Then
                'Sohail (12 Oct 2018) -- End
                StrSearch &= "AND rcstaffrequisition_tran.allocationunkid = " & CInt(cboName.SelectedValue) & " "
            End If

            If CInt(cboStatus.SelectedValue) > 0 Then
                StrSearch &= "AND rcstaffrequisition_tran.form_statusunkid = " & CInt(cboStatus.SelectedValue) & " "
            End If

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
            Dim intUserUnkId As Integer = 0
            If chkMyApprovals.Checked = True Then
                intUserUnkId = CInt(Session("UserId"))
            End If
            'Sohail (12 Oct 2018) -- End

            If StrSearch.Trim.Length > 0 Then StrSearch = StrSearch.Substring(3)

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
            'dsList = objStaffRequisition.GetList("List", StrSearch, "rcstaffrequisition_tran.staffrequisitionbyid")
            dsList = objStaffRequisition.GetList("List", StrSearch, "rcstaffrequisition_tran.staffrequisitionbyid ", intUserUnkId)
            Dim rtf As New System.Windows.Forms.RichTextBox
            'Sohail (12 Oct 2018) -- End

            Dim dRow As DataRow
            Dim strPrevAllocation As String = ""


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            For Each dtRow As DataRow In dsList.Tables("List").Rows


                If strPrevAllocation <> dtRow.Item("staffrequisitionbyid").ToString Then
                    dRow = mdtList.NewRow

                    With dRow
                        .Item("IsGrp") = True
                        .Item("staffrequisitionby") = mdicAllocaions.Item(CInt(dtRow.Item("staffrequisitionbyid")))
                    End With

                    mdtList.Rows.Add(dRow)
                End If

                dRow = mdtList.NewRow
                With dRow

                    .Item("staffrequisitiontranunkid") = CInt(dtRow.Item("staffrequisitiontranunkid"))

                    .Item("formno") = dtRow.Item("formno").ToString

                    .Item("staffrequisitionbyid") = CInt(dtRow.Item("staffrequisitionbyid"))
                    .Item("staffrequisitionby") = mdicAllocaions.Item(CInt(dtRow.Item("staffrequisitionbyid")))

                    Select Case CInt(dtRow.Item("staffrequisitionbyid"))

                        Case enAllocation.BRANCH
                            .Item("name") = dtRow.Item("Branch").ToString

                        Case enAllocation.DEPARTMENT_GROUP
                            .Item("name") = dtRow.Item("DepartmentGroup").ToString

                        Case enAllocation.DEPARTMENT
                            .Item("name") = dtRow.Item("Department").ToString

                        Case enAllocation.SECTION_GROUP
                            .Item("name") = dtRow.Item("SectionGroup").ToString

                        Case enAllocation.SECTION
                            .Item("name") = dtRow.Item("Section").ToString

                        Case enAllocation.UNIT_GROUP
                            .Item("name") = dtRow.Item("UnitGroup").ToString

                        Case enAllocation.UNIT
                            .Item("name") = dtRow.Item("Unit").ToString

                        Case enAllocation.TEAM
                            .Item("name") = dtRow.Item("Team").ToString

                        Case enAllocation.JOB_GROUP
                            .Item("name") = dtRow.Item("JobGroup").ToString

                        Case enAllocation.JOBS
                            .Item("name") = dtRow.Item("Job").ToString

                        Case enAllocation.CLASS_GROUP
                            .Item("name") = dtRow.Item("ClassGroup").ToString

                        Case enAllocation.CLASSES
                            .Item("name") = dtRow.Item("Class").ToString

                    End Select

                    .Item("allocationunkid") = CInt(dtRow.Item("allocationunkid"))
                    .Item("JobTitle") = dtRow.Item("JobTitle").ToString
                    .Item("jobunkid") = CInt(dtRow.Item("jobunkid"))
                    'Sohail (12 Oct 2018) -- Start
                    'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
                    '.Item("jobdescrription") = dtRow.Item("jobdescrription").ToString
                    If dtRow.Item("jobdescrription").ToString.StartsWith("{\rtf") Then
                        rtf.Rtf = dtRow.Item("jobdescrription").ToString
                    Else
                        rtf.Text = dtRow.Item("jobdescrription").ToString
                    End If
                    .Item("jobdescrription") = rtf.Text
                    'Sohail (12 Oct 2018) -- End
                    .Item("workstartdate") = eZeeDate.convertDate(dtRow.Item("workstartdate").ToString).ToShortDateString
                    .Item("form_statusunkid") = CInt(dtRow.Item("form_statusunkid"))
                    .Item("form_status") = mdicApprovalStatus.Item(CInt(dtRow.Item("form_statusunkid"))).ToString
                End With

                mdtList.Rows.Add(dRow)

                strPrevAllocation = dtRow.Item("staffrequisitionbyid")
            Next

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - NMB staff requisition template indicating all fields to be captured on the interface(highlighted in red + comment) in 75.1.
            mdtList = New DataView(mdtList, "", "staffrequisitionby, staffrequisitionbyid", DataViewRowState.CurrentRows).ToTable
            'Sohail (12 Oct 2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If mdtList.Rows.Count <= 0 Then
                Dim r As DataRow = mdtList.NewRow

                r.Item(1) = "None" ' To Hide the row and display only Row Header
                r.Item(2) = "0"

                mdtList.Rows.Add(r)
            End If

            dgStaffReqList.DataSource = mdtList
            dgStaffReqList.DataBind()
            objStaffRequisition = Nothing
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Enabled = CBool(Session("AllowToAddStaffRequisition"))

            'Pinkal (12-Feb-2015) -- Start
            'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
            dgStaffReqList.Columns(0).Visible = CBool(Session("AllowToEditStaffRequisition"))
            dgStaffReqList.Columns(1).Visible = CBool(Session("AllowToDeleteStaffRequisition"))
            'Pinkal (12-Feb-2015) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Session("staffrequisitiontranunkid") = Nothing 'Sohail (15 Oct 2021)
            Response.Redirect(Session("servername") & "~/Recruitment/Staff_Requisition/wPg_StaffRequisition_AddEdit.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call Fill_List()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboAllocation.SelectedIndex = 0
            Call cboAllocation_SelectedIndexChanged(cboAllocation, New System.EventArgs)
            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
            'cboStatus.SelectedValue = 0
            cboStatus.SelectedValue = CInt(enApprovalStatus.PENDING)
            'Sohail (12 Oct 2018) -- End
            Call Fill_List()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Me.ViewState("IsLink") IsNot Nothing Then
                Response.Redirect("~\Index.aspx", False)
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        If Me.ViewState("IsLink") IsNot Nothing Then
    '            Response.Redirect("~\Index.aspx")
    '        Else
    '            Response.Redirect("~\UserHome.aspx", False)
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    Protected Sub popupYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupYesNo.buttonYes_Click
        Try
            popupDelReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupDelReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDelReason.buttonDelReasonYes_Click
        Dim objStaffRequisition As New clsStaffrequisition_Tran
        Try

            Blank_ModuleName()
            objStaffRequisition._WebFormName = "frmStaffRequisitionList"
            StrModuleName2 = "mnuHumanResource"
            StrModuleName3 = "mnuRecruitmentItem"
            objStaffRequisition._WebIP = Session("IP_ADD")
            objStaffRequisition._WebHostName = Session("HOST_NAME")

            If objStaffRequisition.Void(CInt(ViewState("UnkId")), CInt(Session("UserId")), ConfigParameter._Object._CurrentDateAndTime, popupDelReason.Reason) = True Then
                Call Fill_List()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStaffRequisition = Nothing
        End Try
    End Sub
#End Region

#Region " GridView's Event(s) "

    Protected Sub dgStaffReqList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgStaffReqList.RowCommand
        Dim objStaffRequisition As New clsStaffrequisition_Tran
        Dim intRowIdx As Integer
        Try
            intRowIdx = CInt(e.CommandArgument)

            Language.setLanguage(mstrModuleName)

            If e.CommandName = "Change" Then

                If CInt(mdtList.Rows(intRowIdx).Item("form_statusunkid")) = enApprovalStatus.APPROVED Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, This Staff Requisition is already Approved."), Me.Page)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                ElseIf CInt(mdtList.Rows(intRowIdx).Item("form_statusunkid")) = enApprovalStatus.REJECTED Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, This Staff Requisition is already Rejected."), Me.Page)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                ElseIf CInt(mdtList.Rows(intRowIdx).Item("form_statusunkid")) = enApprovalStatus.CANCELLED Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, This Staff Requisition is already Cancelled."), Me.Page)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                ElseIf CInt(mdtList.Rows(intRowIdx).Item("form_statusunkid")) = enApprovalStatus.PUBLISHED Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, This Staff Requisition is already Published."), Me.Page)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If

                Session("staffrequisitiontranunkid") = CInt(dgStaffReqList.DataKeys(intRowIdx).Value)
                Response.Redirect(Session("servername") & "~/Recruitment/Staff_Requisition/wPg_StaffRequisition_AddEdit.aspx", False)

            ElseIf e.CommandName = "Remove" Then

                If CInt(mdtList.Rows(intRowIdx).Item("form_statusunkid")) = enApprovalStatus.APPROVED Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, This Staff Requisition is already Approved."), Me.Page)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                ElseIf CInt(mdtList.Rows(intRowIdx).Item("form_statusunkid")) = enApprovalStatus.REJECTED Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, This Staff Requisition is already Rejected."), Me.Page)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                ElseIf CInt(mdtList.Rows(intRowIdx).Item("form_statusunkid")) = enApprovalStatus.CANCELLED Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, This Staff Requisition is already Cancelled."), Me.Page)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                ElseIf CInt(mdtList.Rows(intRowIdx).Item("form_statusunkid")) = enApprovalStatus.PUBLISHED Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, This Staff Requisition is already Published."), Me.Page)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If

                If objStaffRequisition.isUsed(CInt(dgStaffReqList.DataKeys(intRowIdx).Value)) = True Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage("Sorry, You cannot delete this Staff Requisition. Reason: This Staff Requisition is in use.", Me.Page)
                    'Sohail (23 Mar 2019) -- End
                    Exit Try
                End If

                popupYesNo.Show()
                ViewState.Add("UnkId", CInt(dgStaffReqList.DataKeys(intRowIdx).Value))


                'Gajanan [6-NOV-2019] -- Start    
                'Enhancement:WORKED ON STAFF REQUISITION ATTACHMENT ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB   
                '    'Gajanan [02-SEP-2019] -- Start      
                '    'Enhancements: ENHANCEMENTS IN ONLINE RECRUITMENT FOR NMB
                'ElseIf e.CommandName = "Attach" Then
                '    Session("ScanAttchLableCaption") = "Select Employee"
                '    Session("ScanAttchRefModuleID") = enImg_Email_RefId.Staff_Requisition
                '    Session("ScanAttchenAction") = enAction.ADD_ONE
                '    Session("ScanAttchToEditIDs") = ""
                '    Session("TransactionID") = CInt(dgStaffReqList.DataKeys(intRowIdx).Value)
                '    Session("TransactionScreenName") = mstrModuleName
                '    Session("ScanAttactRefID") = enScanAttactRefId.STAFF_REQUISITION
                '    Response.Redirect(Session("servername") & "~/Others_Forms/wPg_ScanOrAttachmentInfo.aspx", False)
                '    'Gajanan [02-SEP-2019] -- End

                'Gajanan [6-NOV-2019] -- End

                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-515 - Provide quick link on staff requisition report on request (Add/Edit and Approval pages of staff requisition) Similar to the way we have provide an icon.
            ElseIf e.CommandName = "ViewStaffRequisitionFormReport" Then
                Session("ReturnURL") = Request.Url.AbsoluteUri
                Session("intstaffrequisitiontranunkid") = CInt(dgStaffReqList.DataKeys(intRowIdx).Value)
                Response.Redirect(Session("rootpath") & "Reports/Recruitment_Reports/Staff_Requisition_Form/Rpt_StaffRequisitionFormReport.aspx", False)
                'Hemant (01 Nov 2021) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStaffRequisition = Nothing
        End Try
    End Sub

    Protected Sub dgStaffReqList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgStaffReqList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(4).Text = "None" AndAlso e.Row.Cells(5).Text = "&nbsp;" Then
                    e.Row.Visible = False
                Else
                    If CBool(DataBinder.Eval(e.Row.DataItem, "IsGrp")) = True Then
                        For i = 4 To dgStaffReqList.Columns.Count - 1
                            e.Row.Cells(i).Visible = False
                        Next
                        e.Row.Cells(3).Text = "Allocation : " & e.Row.Cells(3).Text
                        e.Row.Cells(3).ColumnSpan = dgStaffReqList.Columns.Count - 3

                        'SHANI [01 FEB 2015]-START
                        'Enhancement - REDESIGN SELF SERVICE.
                        'e.Row.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                        'e.Row.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                        'e.Row.Cells(2).CssClass = "GroupHeaderStyleBorderRight"
                        e.Row.Cells(0).CssClass = "GroupHeaderStyle"
                        e.Row.Cells(1).CssClass = "GroupHeaderStyle"
                        e.Row.Cells(2).CssClass = "GroupHeaderStyle"
                        e.Row.Cells(3).CssClass = "GroupHeaderStyle"

                        'SHANI [01 FEB 2015]--END
                        If IsNothing(e.Row.Cells(0).FindControl("btnView")) = False Then
                            e.Row.Cells(0).FindControl("btnView").Visible = False
                        End If

                        If IsNothing(e.Row.Cells(1).FindControl("btnGvRemove")) = False Then
                            e.Row.Cells(1).FindControl("btnGvRemove").Visible = False
                        End If

                        If IsNothing(e.Row.Cells(2).FindControl("lnkViewStaffRequisitionFormReport")) = False Then
                            e.Row.Cells(2).FindControl("lnkViewStaffRequisitionFormReport").Visible = False
                        End If

                        dgStaffReqList.Columns(0).Visible = CBool(Session("AllowToEditStaffRequisition"))
                        dgStaffReqList.Columns(1).Visible = CBool(Session("AllowToDeleteStaffRequisition"))
                    Else
                        'e.Row.Attributes.Add("onmouseover", "this.style.backgroundColor='#ddd'")
                        'e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=''")
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Dim dsCombos As DataSet = Nothing
        Dim strName As String = ""

        Try
            Language.setLanguage(mstrModuleName)

            Select Case CInt(cboAllocation.SelectedValue)

                Case enAllocation.BRANCH
                    dsCombos = objStation.getComboList("Station", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 14, "Branch")
                Case enAllocation.DEPARTMENT_GROUP
                    dsCombos = objDeptGrp.getComboList("DeptGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 15, "Department Group")
                Case enAllocation.DEPARTMENT
                    dsCombos = objDepartment.getComboList("Department", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 16, "Department")
                Case enAllocation.SECTION_GROUP
                    dsCombos = objSectionGrp.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 17, "Section Group")
                Case enAllocation.SECTION
                    dsCombos = objSection.getComboList("Section", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 18, "Section")
                Case enAllocation.UNIT_GROUP
                    dsCombos = objUnitGroup.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 19, "Unit Group")
                Case enAllocation.UNIT
                    dsCombos = objUnit.getComboList("Unit", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 20, "Unit")
                Case enAllocation.TEAM
                    dsCombos = objTeam.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 21, "Team")
                Case enAllocation.JOB_GROUP
                    dsCombos = objJobGrp.getComboList("JobGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 22, "Job Group")
                Case enAllocation.JOBS
                    dsCombos = objJob.getComboList("Job", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 23, "Jobs")
                Case enAllocation.CLASS_GROUP
                    dsCombos = objClassGrp.getComboList("ClassGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 24, "Class Group")
                Case enAllocation.CLASSES
                    dsCombos = objClass.getComboList("Class", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 25, "Classes")

                    'Sohail (12 Oct 2018) -- Start
                    'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
                Case Else
                    dsCombos = Nothing
                    'Sohail (12 Oct 2018) -- End

            End Select

            lblName.Text = strName
            If dsCombos IsNot Nothing Then

                With cboName
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsCombos.Tables(0)
                    .DataBind()
                End With
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - On staff requisition list, there should be a check box for my approvals and by defauld should be checked in 75.1.
            Else
                cboName.DataSource = Nothing
                cboName.DataBind()
                cboName.Items.Clear()
                'Sohail (12 Oct 2018) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objStation = Nothing
            objDeptGrp = Nothing
            objDepartment = Nothing
            objSection = Nothing
            objUnit = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objClassGrp = Nothing
            objClass = Nothing
            objSectionGrp = Nothing
            objUnitGroup = Nothing
            objTeam = Nothing
            dsCombos = Nothing
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        Language.setLanguage(mstrModuleName)

        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'SHANI [01 FEB 2015]--END

        Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
        Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.ID, Me.lblAllocation.Text)
        Me.lblName.Text = Language._Object.getCaption(Me.lblName.ID, Me.lblName.Text)
        Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
        Me.chkMyApprovals.Text = Language._Object.getCaption(Me.chkMyApprovals.ID, Me.chkMyApprovals.Text)

        dgStaffReqList.Columns(0).HeaderText = Language._Object.getCaption(dgStaffReqList.Columns(0).FooterText, dgStaffReqList.Columns(0).HeaderText).Replace("&", "")
        dgStaffReqList.Columns(1).HeaderText = Language._Object.getCaption(dgStaffReqList.Columns(1).FooterText, dgStaffReqList.Columns(1).HeaderText).Replace("&", "")
        dgStaffReqList.Columns(2).HeaderText = Language._Object.getCaption(dgStaffReqList.Columns(2).FooterText, dgStaffReqList.Columns(2).HeaderText)
        dgStaffReqList.Columns(3).HeaderText = Language._Object.getCaption(dgStaffReqList.Columns(3).FooterText, dgStaffReqList.Columns(3).HeaderText)
        dgStaffReqList.Columns(4).HeaderText = Language._Object.getCaption(dgStaffReqList.Columns(4).FooterText, dgStaffReqList.Columns(4).HeaderText)
        dgStaffReqList.Columns(5).HeaderText = Language._Object.getCaption(dgStaffReqList.Columns(5).FooterText, dgStaffReqList.Columns(5).HeaderText)
        dgStaffReqList.Columns(6).HeaderText = Language._Object.getCaption(dgStaffReqList.Columns(6).FooterText, dgStaffReqList.Columns(6).HeaderText)
        dgStaffReqList.Columns(7).HeaderText = Language._Object.getCaption(dgStaffReqList.Columns(7).FooterText, dgStaffReqList.Columns(7).HeaderText)
        dgStaffReqList.Columns(8).HeaderText = Language._Object.getCaption(dgStaffReqList.Columns(8).FooterText, dgStaffReqList.Columns(8).HeaderText)
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 2, "Sorry, This Staff Requisition is already Approved.")
            Language.setMessage(mstrModuleName, 3, "Sorry, This Staff Requisition is already Rejected.")
            Language.setMessage(mstrModuleName, 4, "Sorry, This Staff Requisition is already Cancelled.")
            Language.setMessage(mstrModuleName, 5, "Sorry, This Staff Requisition is already Published.")
            Language.setMessage(mstrModuleName, 14, "Branch")
            Language.setMessage(mstrModuleName, 15, "Department Group")
            Language.setMessage(mstrModuleName, 16, "Department")
            Language.setMessage(mstrModuleName, 17, "Section Group")
            Language.setMessage(mstrModuleName, 18, "Section")
            Language.setMessage(mstrModuleName, 19, "Unit Group")
            Language.setMessage(mstrModuleName, 20, "Unit")
            Language.setMessage(mstrModuleName, 21, "Team")
            Language.setMessage(mstrModuleName, 22, "Job Group")
            Language.setMessage(mstrModuleName, 23, "Jobs")
            Language.setMessage(mstrModuleName, 24, "Class Group")
            Language.setMessage(mstrModuleName, 25, "Classes")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
