﻿
#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports System.Net.Dns
#End Region

Partial Class wPg_ApproveDisapproveStaffRequisitionList
    Inherits Basepage

#Region " Private Variable(s) "

    Dim msg As New CommonCodes
    Private clsuser As New User
    Private objCONN As SqlConnection
    Private ReadOnly mstrModuleName As String = "frmApproveDisapproveStaffRequisitionList"
    Private objStaffReqApproval As clsStaffrequisition_approval_Tran
    Private mdtList As DataTable

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim objMaster As New clsMasterData
        Dim intCurrPeriodId As Integer = 0
        Dim dsCombo As DataSet
        Try
            Dim mdicApprovalStatus As New Dictionary(Of Integer, String)
            Dim mdicAllocaions As New Dictionary(Of Integer, String)

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
            'dsCombo = objMaster.GetEAllocation_Notification("List")
            dsCombo = objMaster.GetEAllocation_Notification("List", , , True)
            'Sohail (12 Oct 2018) -- End

            With cboAllocation
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedIndex = 0
            End With
            mdicAllocaions = (From p In dsCombo.Tables("List").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            dsCombo = objMaster.getApprovalStatus("ApprovalStatus", True, , True, True, True, True, True)
            mdicApprovalStatus = (From p In dsCombo.Tables("ApprovalStatus").AsEnumerable Select New With {Key .ID = CInt(p.Item("Id")), Key .NAME = p.Item("name").ToString}).ToDictionary(Function(x) x.ID, Function(x) x.NAME)

            cboAllocation_SelectedIndexChanged(New Object(), New EventArgs())

            Me.ViewState.Add("ApprovalStatus", mdicApprovalStatus)
            Me.ViewState.Add("Allocation", mdicAllocaions)

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
            With cboStatus
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("ApprovalStatus")
                .DataBind()
                .SelectedValue = CInt(enApprovalStatus.PENDING)
            End With
            'Sohail (12 Oct 2018) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub CreateListTable()
        Try
            mdtList = New DataTable
            With mdtList
                .Columns.Add("IsGroup", System.Type.GetType("System.Boolean")).DefaultValue = False
                .Columns.Add("staffrequisitionapprovaltranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("formno", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("staffrequisitiontranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("staffrequisitionbyid", System.Type.GetType("System.Int32")).DefaultValue = 0
                '.Columns.Add("staffrequisitionby", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("allocationunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("JobTitle", System.Type.GetType("System.String")).DefaultValue = ""
                '.Columns.Add("jobunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("levelname", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("username", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("priority", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("approval_date", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("statusunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("remarks", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("form_statusunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                '.Columns.Add("form_status", System.Type.GetType("System.String")).DefaultValue = ""
            End With

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Dim strSearching As String = ""
        Dim objApproverMap As New clsStaffRequisition_approver_mapping
        Dim objApproverLevel As New clsStaffRequisitionApproverlevel_master
        Dim dsList As DataSet
        Dim dsCombo As DataSet = Nothing
        Dim mintCurrLevelPriority As Integer = -1
        Dim mintLowerLevelPriority As Integer = -99 'Keep -99
        Dim mintMinPriority As Integer = -1
        Dim mintMaxPriority As Integer = -1
        Dim mintLowerPriorityForFirstLevel As Integer = -1
        Dim mintCurrLevelID As Integer = -1
        Try

            mdtList.Rows.Clear()
            If Session("LoginBy") = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewStaffRequisitionApprovals")) = False Then Exit Sub
            End If

            'Sohail (12 Oct 2018) -- Start
            'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
            'If CInt(cboAllocation.SelectedValue) <= 0 OrElse CInt(cboName.SelectedValue) <= 0 Then
            '    Language.setLanguage(mstrModuleName)
            '    msg.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Please select") & " " & lblName.Text & ".", Me)
            '    cboName.Focus()
            '    Exit Try
            'End If

            'dsCombo = objApproverMap.GetList("ApproverLevel", CInt(Session("UserId")), CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue))

            'If dsCombo.Tables("ApproverLevel").Rows.Count > 0 Then
            '    mintCurrLevelPriority = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("priority"))
            '    mintLowerLevelPriority = objApproverLevel.GetLowerLevelPriority(mintCurrLevelPriority, CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue))
            '    mintMinPriority = objApproverLevel.GetMinPriority(CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue))
            '    mintMaxPriority = objApproverLevel.GetMaxPriority(CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue))
            '    mintLowerPriorityForFirstLevel = objApproverLevel.GetLowerLevelPriority(mintMinPriority, CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue))

            '    If mintCurrLevelPriority >= 0 Then
            '        mintCurrLevelID = CInt(dsCombo.Tables("ApproverLevel").Rows(0).Item("levelunkid"))
            '    End If
            'Else
            '    Language.setLanguage(mstrModuleName)
            '    msg.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, You do not have Approval Permission for the selected") & " " & lblName.Text & ".", Me)
            '    cboName.Focus()
            '    Exit Try
            'End If

            ''Shani(20-Nov-2015) -- Start
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


            ''If mintLowerLevelPriority >= 0 Then
            ''    dsList = objStaffReqApproval.GetList("List", , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), CInt(Session("UserId")), , , True, " rcstaffrequisition_approval_tran.priority <= " & mintCurrLevelPriority & " ")
            ''Else
            ''    dsList = objStaffReqApproval.GetList("List", , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), CInt(Session("UserId")), , , True, " rcstaffrequisition_approval_tran.priority <= " & mintCurrLevelPriority & " ")
            ''End If

            'If mintLowerLevelPriority >= 0 Then
            '    dsList = objStaffReqApproval.GetList("List", Session("UserId"), , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), , , True, " rcstaffrequisition_approval_tran.priority <= " & mintCurrLevelPriority & " ")
            'Else
            '    dsList = objStaffReqApproval.GetList("List", Session("UserId"), , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), , , True, " rcstaffrequisition_approval_tran.priority <= " & mintCurrLevelPriority & " ")
            'End If
            ''Shani(20-Nov-2015) -- End
            'Hemant (22 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            'dsList = objStaffReqApproval.GetList("List", Session("UserId"), , , CInt(cboAllocation.SelectedValue), CInt(IIf(cboName.SelectedValue.Trim = "", "0", cboName.SelectedValue)), , , True, " ", chkMyApprovals.Checked, CInt(cboStatus.SelectedValue))
            'Sohail (15 Oct 2021) -- Start
            'NMB Enhancement :  : Show staff requisition only if they are pending at logged in user level if my approval is ticked on staff requisition approval screen.
            'dsList = objStaffReqApproval.GetList("List", Session("UserId"), , , CInt(cboAllocation.SelectedValue), CInt(IIf(cboName.SelectedValue.Trim = "", "0", cboName.SelectedValue)), , , True, " ", False, CInt(cboStatus.SelectedValue), chkMyApprovals.Checked)

            'Pinkal (16-Nov-2021)-- Start
            'NMB Staff Requisition Approval Enhancements. 
            'dsList = objStaffReqApproval.GetList("List", Session("UserId"), , , CInt(cboAllocation.SelectedValue), CInt(IIf(cboName.SelectedValue.Trim = "", "0", cboName.SelectedValue)), , , True, " ", chkMyApprovals.Checked, CInt(cboStatus.SelectedValue), chkMyApprovals.Checked)
            dsList = objStaffReqApproval.GetList("List", Session("UserId"), Session("EmployeeAsOnDate").ToString(), , , CInt(cboAllocation.SelectedValue), CInt(IIf(cboName.SelectedValue.Trim = "", "0", cboName.SelectedValue)), , , True, " ", chkMyApprovals.Checked, CInt(cboStatus.SelectedValue), chkMyApprovals.Checked)
            'Pinkal (16-Nov-2021)-- End

            'Sohail (15 Oct 2021) -- End
            'Hemant (22 Aug 2019) -- End
            'Sohail (12 Oct 2018) -- End

            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "", "formno asc", DataViewRowState.CurrentRows).ToTable


            'If dtTable.Columns.Contains("IsGroup") = False Then
            '    dtTable.Columns.Add("IsGroup", Type.GetType("System.Boolean"))
            '    dtTable.Columns("IsGroup").DefaultValue = False
            'End If
            Dim drRow As DataRow
            Dim mstrFormNo As String = ""
            For i As Integer = 0 To dtTable.Rows.Count - 1
                If mstrFormNo <> dtTable.Rows(i)("formno").ToString() Then
                    drRow = mdtList.NewRow
                    drRow("formno") = dtTable.Rows(i)("formno")
                    drRow("IsGroup") = True
                    drRow("staffrequisitiontranunkid") = -1
                    drRow("staffrequisitionbyid") = -2
                    drRow("allocationunkid") = -2
                    mdtList.Rows.Add(drRow)
                    mstrFormNo = dtTable.Rows(i)("formno").ToString()
                End If

                drRow = mdtList.NewRow
                With drRow
                    drRow("staffrequisitionapprovaltranunkid") = dtTable.Rows(i)("staffrequisitionapprovaltranunkid")
                    drRow("formno") = dtTable.Rows(i)("formno").ToString
                    drRow("staffrequisitiontranunkid") = dtTable.Rows(i)("staffrequisitiontranunkid")
                    drRow("staffrequisitionbyid") = dtTable.Rows(i)("staffrequisitionbyid")

                    Select Case CInt(dtTable.Rows(i)("staffrequisitionbyid"))

                        Case enAllocation.BRANCH
                            .Item("name") = dtTable.Rows(i).Item("Branch").ToString

                        Case enAllocation.DEPARTMENT_GROUP
                            .Item("name") = dtTable.Rows(i).Item("DepartmentGroup").ToString

                        Case enAllocation.DEPARTMENT
                            .Item("name") = dtTable.Rows(i).Item("Department").ToString

                        Case enAllocation.SECTION_GROUP
                            .Item("name") = dtTable.Rows(i).Item("SectionGroup").ToString

                        Case enAllocation.SECTION
                            .Item("name") = dtTable.Rows(i).Item("Section").ToString

                        Case enAllocation.UNIT_GROUP
                            .Item("name") = dtTable.Rows(i).Item("UnitGroup").ToString

                        Case enAllocation.UNIT
                            .Item("name") = dtTable.Rows(i).Item("Unit").ToString

                        Case enAllocation.TEAM
                            .Item("name") = dtTable.Rows(i).Item("Team").ToString

                        Case enAllocation.JOB_GROUP
                            .Item("name") = dtTable.Rows(i).Item("JobGroup").ToString

                        Case enAllocation.JOBS
                            .Item("name") = dtTable.Rows(i).Item("Job").ToString

                        Case enAllocation.CLASS_GROUP
                            .Item("name") = dtTable.Rows(i).Item("ClassGroup").ToString

                        Case enAllocation.CLASSES
                            .Item("name") = dtTable.Rows(i).Item("Class").ToString

                    End Select

                    drRow("allocationunkid") = dtTable.Rows(i)("allocationunkid")
                    drRow("JobTitle") = dtTable.Rows(i)("JobTitle")
                    drRow("levelname") = dtTable.Rows(i)("levelname")
                    drRow("userunkid") = dtTable.Rows(i)("userunkid")
                    drRow("username") = dtTable.Rows(i)("username")
                    drRow("priority") = dtTable.Rows(i)("priority")
                    drRow("approval_date") = dtTable.Rows(i)("approval_date")
                    drRow("statusunkid") = dtTable.Rows(i)("statusunkid")
                    drRow("remarks") = dtTable.Rows(i)("remarks")
                    drRow("form_statusunkid") = dtTable.Rows(i)("form_statusunkid")
                End With

                mdtList.Rows.Add(drRow)

                mstrFormNo = dtTable.Rows(i)("formno").ToString()
            Next

            'For i As Integer = 0 To dtTable.Rows.Count - 1
            '    If IsDBNull(dtTable.Rows(i)("IsGroup")) Then
            '        dtTable.Rows(i)("IsGroup") = False
            '    End If
            'Next

            'dtTable = New DataView(dtTable, "", "formno asc,staffrequisitiontranunkid", DataViewRowState.CurrentRows).ToTable

            'Me.ViewState.Add("List", dtTable)



            'lvStaffReqApprovalList.DataSource = dtTable
            'lvStaffReqApprovalList.DataBind()


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            'If dsCombo Is Nothing OrElse dsCombo.Tables(0).Rows.Count <= 0 Then
            '    GetGridDataClone()
            'End If
            If mdtList.Rows.Count <= 0 Then
                Dim r As DataRow = mdtList.NewRow

                'r.Item(1) = "None" ' To Hide the row and display only Row Header
                r("formno") = ""
                r("staffrequisitionbyid") = -1
                r("allocationunkid") = -1
                r("JobTitle") = ""
                r("levelname") = ""
                r("username") = ""
                r("priority") = -1
                r("approval_date") = ""
                r("statusunkid") = -1
                r("remarks") = ""
                r("form_statusunkid") = -1
                r("form_statusunkid") = -1
                r("IsGroup") = False

                mdtList.Rows.Add(r)
            End If

            lvStaffReqApprovalList.DataSource = mdtList
            lvStaffReqApprovalList.DataBind()
        End Try
    End Sub

    'Private Sub GetGridDataClone()
    '    Dim objApproverMap As New clsStaffRequisition_approver_mapping
    '    Try

    '        'Shani(20-Nov-2015) -- Start
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'Dim dsCombo As DataSet = objStaffReqApproval.GetList("List", , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), CInt(Session("UserId"))).Clone
    '        Dim dsCombo As DataSet = objStaffReqApproval.GetList("List", Session("UserId"), , , CInt(cboAllocation.SelectedValue), CInt(cboName.SelectedValue), , , ).Clone
    '        'Shani(20-Nov-2015) -- End


    '        If dsCombo.Tables(0).Columns.Contains("IsGroup") = False Then
    '            dsCombo.Tables(0).Columns.Add("IsGroup", Type.GetType("System.Boolean"))
    '            dsCombo.Tables(0).Columns("IsGroup").DefaultValue = False
    '        End If

    '        Dim drRow As DataRow = dsCombo.Tables(0).NewRow
    '        drRow("formno") = ""
    '        drRow("staffrequisitionbyid") = -1
    '        drRow("allocationunkid") = -1
    '        drRow("JobTitle") = ""
    '        drRow("levelname") = ""
    '        drRow("username") = ""
    '        drRow("priority") = -1
    '        drRow("approval_date") = ""
    '        drRow("statusunkid") = -1
    '        drRow("remarks") = ""
    '        drRow("form_statusunkid") = -1
    '        drRow("form_statusunkid") = -1
    '        drRow("IsGroup") = False
    '        dsCombo.Tables(0).Rows.Add(drRow)

    '        Me.ViewState.Add("List", dsCombo.Tables(0))

    '        lvStaffReqApprovalList.DataSource = dsCombo.Tables(0)
    '        lvStaffReqApprovalList.DataBind()
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)
        Me.ViewState.Add("mdtList", mdtList)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objStaffReqApproval = New clsStaffrequisition_approval_Tran
            SetLanguage()
            If IsPostBack = False Then
                Call CreateListTable()
                FillCombo()
                'GetGridDataClone()
                'Hemant (06 Aug 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
                chkMyApprovals.Checked = True
                'Hemant (06 Aug 2019) -- End
            Else
                mdtList = ViewState("mdtList")
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        Me.IsLoginRequired = True
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboAllocation.SelectedIndex = 0
            cboAllocation_SelectedIndexChanged(New Object(), New EventArgs())
            'GetGridDataClone()
            cboStatus.SelectedValue = CInt(enApprovalStatus.PENDING) 'Sohail (12 Oct 2018)
            Call FillList()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Me.ViewState("IsLink") IsNot Nothing Then
                Response.Redirect("~\Index.aspx", False)
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ImageButton Events"


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region

#Region "Combobox Events"

    Protected Sub cboAllocation_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAllocation.SelectedIndexChanged
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGrp As New clsClassGroup
        Dim objClass As New clsClass
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Dim dsCombos As DataSet = Nothing
        Dim strName As String = ""

        Try

            Select Case CInt(cboAllocation.SelectedValue)

                Case enAllocation.BRANCH
                    dsCombos = objStation.getComboList("Station", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 14, "Branch")
                Case enAllocation.DEPARTMENT_GROUP
                    dsCombos = objDeptGrp.getComboList("DeptGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 15, "Department Group")
                Case enAllocation.DEPARTMENT
                    dsCombos = objDepartment.getComboList("Department", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 16, "Department")
                Case enAllocation.SECTION_GROUP
                    dsCombos = objSectionGrp.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 17, "Section Group")
                Case enAllocation.SECTION
                    dsCombos = objSection.getComboList("Section", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 18, "Section")
                Case enAllocation.UNIT_GROUP
                    dsCombos = objUnitGroup.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 19, "Unit Group")
                Case enAllocation.UNIT
                    dsCombos = objUnit.getComboList("Unit", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 20, "Unit")
                Case enAllocation.TEAM
                    dsCombos = objTeam.getComboList("List", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 21, "Team")
                Case enAllocation.JOB_GROUP
                    dsCombos = objJobGrp.getComboList("JobGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 22, "Job Group")
                Case enAllocation.JOBS
                    dsCombos = objJob.getComboList("Job", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 23, "Jobs")
                Case enAllocation.CLASS_GROUP
                    dsCombos = objClassGrp.getComboList("ClassGrp", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 24, "Class Group")
                Case enAllocation.CLASSES
                    dsCombos = objClass.getComboList("Class", True)
                    dsCombos.Tables(0).Columns(0).ColumnName = "Id"
                    strName = Language.getMessage(mstrModuleName, 25, "Classes")

                    'Sohail (12 Oct 2018) -- Start
                    'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
                Case Else
                    dsCombos = Nothing
                    'Sohail (12 Oct 2018) -- End

            End Select

            lblName.Text = strName
            If dsCombos IsNot Nothing Then

                With cboName
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsCombos.Tables(0)
                    .DataBind()
                    .SelectedValue = 0
                End With
                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
            Else
                cboName.DataSource = Nothing
                cboName.DataBind()
                cboName.Items.Clear()
                'Sohail (12 Oct 2018) -- End
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("cboAllocation_SelectedIndexChanged :- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objStation = Nothing
            objDeptGrp = Nothing
            objDepartment = Nothing
            objSection = Nothing
            objUnit = Nothing
            objJobGrp = Nothing
            objJob = Nothing
            objClassGrp = Nothing
            objClass = Nothing
            objSectionGrp = Nothing
            objUnitGroup = Nothing
            objTeam = Nothing
            dsCombos = Nothing
        End Try
    End Sub

#End Region

#Region " GridView's Event(s) "

    Protected Sub lvStaffReqApprovalList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles lvStaffReqApprovalList.RowDataBound
        Try
            'If (e.Item.ItemIndex >= 0) Then
            If e.Row.RowType = DataControlRowType.DataRow Then



                lvStaffReqApprovalList.Columns(0).Visible = CBool(Session("AllowToApproveStaffRequisition"))

                If e.Row.Cells(2).Text = "-1" AndAlso e.Row.Cells(3).Text = "-1" AndAlso e.Row.Cells(4).Text = "&nbsp;" AndAlso e.Row.Cells(5).Text = "&nbsp;" Then
                    e.Row.Visible = False
                    Exit Sub
                End If

                If CBool(DataBinder.Eval(e.Row.DataItem, "IsGroup")) = True Then
                    For i As Integer = 3 To lvStaffReqApprovalList.Columns.Count - 1
                        e.Row.Cells(i).Visible = False
                    Next
                    CType(e.Row.FindControl("lnkChangeStatus"), LinkButton).Visible = False
                    CType(e.Row.FindControl("lnkViewStaffRequisitionFormReport"), LinkButton).Visible = False
                    e.Row.Cells(2).Text = "Form No : " & mdtList.Rows(e.Row.RowIndex)("formno").ToString()
                    e.Row.Cells(2).ColumnSpan = lvStaffReqApprovalList.Columns.Count - 2
                    'SHANI [01 FEB 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    'e.Row.BackColor = Drawing.Color.SteelBlue : e.Row.ForeColor = Drawing.Color.White
                    'e.Row.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                    'e.Row.Cells(1).CssClass = "GroupHeaderStyleBorderRight"
                    e.Row.Cells(0).CssClass = "GroupHeaderStyle"
                    e.Row.Cells(1).CssClass = "GroupHeaderStyle"
                    e.Row.Cells(2).CssClass = "GroupHeaderStyle"
                    'SHANI [01 FEB 2015]--END 
                    Exit Sub
                End If

                e.Row.Cells(2).Text = CType(Me.ViewState("Allocation"), Dictionary(Of Integer, String)).Item(CInt(e.Row.Cells(2).Text))

                'Select Case CInt(DataBinder.Eval(e.Row.DataItem, "staffrequisitionbyid"))

                '    Case enAllocation.BRANCH
                '        e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "Branch").ToString()

                '    Case enAllocation.DEPARTMENT_GROUP
                '        e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "DepartmentGroup").ToString()

                '    Case enAllocation.DEPARTMENT
                '        e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "Department").ToString()

                '    Case enAllocation.SECTION_GROUP
                '        e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "SectionGroup").ToString()

                '    Case enAllocation.SECTION
                '        e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "Section").ToString()

                '    Case enAllocation.UNIT_GROUP
                '        e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "UnitGroup").ToString()

                '    Case enAllocation.UNIT
                '        e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "Unit").ToString()

                '    Case enAllocation.TEAM
                '        e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "Team").ToString()

                '    Case enAllocation.JOB_GROUP
                '        e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "JobGroup").ToString()

                '    Case enAllocation.JOBS
                '        e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "Job").ToString()

                '    Case enAllocation.CLASS_GROUP
                '        e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "ClassGroup").ToString()

                '    Case enAllocation.CLASSES
                '        e.Row.Cells(2).Text = DataBinder.Eval(e.Row.DataItem, "Class").ToString()

                'End Select
                e.Row.Cells(3).Text = DataBinder.Eval(e.Row.DataItem, "name").ToString

                If e.Row.Cells(8).Text = "19000101" Then
                    e.Row.Cells(8).Text = ""
                Else
                    e.Row.Cells(8).Text = eZeeDate.convertDate(e.Row.Cells(8).Text).ToShortDateString
                End If

                If CInt(DataBinder.Eval(e.Row.DataItem, "form_statusunkid")) = enApprovalStatus.PUBLISHED Then
                    e.Row.Cells(9).Text = CType(Me.ViewState("ApprovalStatus"), Dictionary(Of Integer, String)).Item(CInt(DataBinder.Eval(e.Row.DataItem, "form_statusunkid")))
                ElseIf CInt(DataBinder.Eval(e.Row.DataItem, "form_statusunkid")) = enApprovalStatus.APPROVED Then
                    e.Row.Cells(9).Text = Language.getMessage(mstrModuleName, 3, "Final") & " " & CType(Me.ViewState("ApprovalStatus"), Dictionary(Of Integer, String)).Item(CInt(e.Row.Cells(9).Text))
                Else
                    e.Row.Cells(9).Text = CType(Me.ViewState("ApprovalStatus"), Dictionary(Of Integer, String)).Item(CInt(e.Row.Cells(9).Text))
                End If

            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lvStaffReqApprovalList_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles lvStaffReqApprovalList.RowCommand
        Dim objApproverMap As clsStaffRequisition_approver_mapping
        Dim objApproverLevel As clsStaffRequisitionApproverlevel_master
        Dim objApproval As clsStaffrequisition_approval_Tran
        Dim mintLowerLevelPriority As Integer = -1
        Dim dsCombo As DataSet = Nothing
        Dim intRowIdx As Integer
        Try
            intRowIdx = CInt(e.CommandArgument)

            If e.CommandName = "Select" Then

                objApproverMap = New clsStaffRequisition_approver_mapping
                objApproverLevel = New clsStaffRequisitionApproverlevel_master
                objApproval = New clsStaffrequisition_approval_Tran

                Dim mintCurrLevelPriority As Integer = -1
                Dim mintMinPriority As Integer = -1
                Dim mintMaxPriority As Integer = -1
                Dim mintLowerPriorityForFirstLevel As Integer = -1
                Dim mintCurrLevelID As Integer = -1

                If CInt(mdtList.Rows(intRowIdx)("form_statusunkid")) = enApprovalStatus.REJECTED OrElse _
                    CInt(mdtList.Rows(intRowIdx)("statusunkid")) = enApprovalStatus.REJECTED Then
                    Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 5, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Rejected."), Me)
                    Exit Sub

                ElseIf CInt(mdtList.Rows(intRowIdx)("form_statusunkid")) = enApprovalStatus.CANCELLED OrElse _
                         CInt(mdtList.Rows(intRowIdx)("statusunkid")) = enApprovalStatus.CANCELLED Then
                    Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 6, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Cancelled."), Me)
                    Exit Sub

                ElseIf CInt(mdtList.Rows(intRowIdx)("form_statusunkid")) = enApprovalStatus.PUBLISHED OrElse _
                        CInt(mdtList.Rows(intRowIdx)("statusunkid")) = enApprovalStatus.PUBLISHED Then
                    Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 7, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Published."), Me)
                    Exit Sub

                ElseIf CInt(mdtList.Rows(intRowIdx)("form_statusunkid")) = enApprovalStatus.APPROVED AndAlso _
                        CBool(Session("AllowToCancelStaffRequisition")) = False Then
                    Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 8, "You can't Cancel this Staff Requisition detail. Reason: This Staff Requisition is already Final Approved and you don't have permission to Cancel Staff Requisition."), Me)
                    Exit Sub

                ElseIf CInt(mdtList.Rows(intRowIdx)("form_statusunkid")) = enApprovalStatus.PENDING AndAlso _
                        CInt(mdtList.Rows(intRowIdx)("statusunkid")) = enApprovalStatus.APPROVED Then
                    Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 9, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition is already Approved."), Me)
                    Exit Sub

                ElseIf CInt(Session("UserId")) <> CInt(mdtList.Rows(intRowIdx)("userunkid")) AndAlso _
                        CInt(mdtList.Rows(intRowIdx)("statusunkid")) = enApprovalStatus.PENDING Then
                    Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 10, "You can't Edit this Staff Requisition detail. Reason: You are logged in into another user login."), Me)
                    Exit Sub

                ElseIf CInt(mdtList.Rows(intRowIdx)("statusunkid")) = enApprovalStatus.PENDING AndAlso _
                        CBool(Session("AllowToApproveStaffRequisition")) = False Then
                    Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 11, "You can't Edit this Staff Requisition detail. Reason: You don't have permission to Approve Staff Requisition."), Me)
                    Exit Sub
                End If

                'Sohail (12 Oct 2018) -- Start
                'NMB Enhancement - Ref. # :  - When approving staff requisition, do not make it mandatory for user to select allocation allocation for requisition by. Rather when approver clicks on search button, all pending requisitions should be listed in 75.1.
                'dsCombo = objApproverMap.GetList("CurrPriority", CInt(Session("UserId")), CInt(mdtList.Rows(intRowIdx)("staffrequisitionbyid").ToString()), _
                '                                 CInt(mdtList.Rows(intRowIdx)("allocationunkid").ToString()))
                'If dsCombo.Tables("CurrPriority").Rows.Count > 0 Then
                '    mintCurrLevelPriority = CInt(dsCombo.Tables("CurrPriority").Rows(0).Item("priority"))
                'End If

                'If mintCurrLevelPriority > CInt(lvStaffReqApprovalList.Rows(intRowIdx).Cells(6).Text) Then
                '    msg.DisplayMessage(Language.getMessage(mstrModuleName, 12, "You can't Edit this Staff Requisition detail. Reason: You are logged in into another user login."), Me)
                '    Exit Sub
                'End If

                'mintLowerLevelPriority = objApproverLevel.GetLowerLevelPriority(mintCurrLevelPriority, CInt(mdtList.Rows(intRowIdx)("staffrequisitionbyid")), CInt(mdtList.Rows(intRowIdx)("allocationunkid")))
                'If mintLowerLevelPriority >= 0 Then

                '    'Shani(20-Nov-2015) -- Start
                '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '    'dsCombo = objApproval.GetList("List", , CInt(mdtList.Rows(intRowIdx)("staffrequisitiontranunkid").ToString()), , , , , mintLowerLevelPriority)
                '    dsCombo = objApproval.GetList("List", Session("UserId"), , CInt(mdtList.Rows(intRowIdx)("staffrequisitiontranunkid").ToString()), , , , mintLowerLevelPriority)
                '    'Shani(20-Nov-2015) -- End


                '    If dsCombo.Tables("List").Rows.Count > 0 Then
                '        If CInt(dsCombo.Tables("List").Rows(0).Item("statusunkid")) = 1 Then
                '            msg.DisplayMessage(Language.getMessage(mstrModuleName, 13, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition approval is still pending for lower level."), Me)
                '            Exit Sub
                '        End If
                '    End If
                'End If

                'Pinkal (16-Nov-2021)-- Start
                'NMB Staff Requisition Approval Enhancements. 
                'dsCombo = objApproval.GetList("List", CInt(Session("UserId")), , CInt(mdtList.Rows(intRowIdx)("staffrequisitiontranunkid").ToString()), , , , mintLowerLevelPriority, True, "", True, CInt(enApprovalStatus.PENDING))
                dsCombo = objApproval.GetList("List", CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString(), , CInt(mdtList.Rows(intRowIdx)("staffrequisitiontranunkid").ToString()), , , , mintLowerLevelPriority, True, "", True, CInt(enApprovalStatus.PENDING))
                'Pinkal (16-Nov-2021)-- End

                If dsCombo.Tables("List").Rows.Count <= 0 Then
                    If Not (CInt(mdtList.Rows(intRowIdx)("form_statusunkid")) = enApprovalStatus.APPROVED AndAlso CBool(Session("AllowToCancelStaffRequisition")) = True) Then
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 13, "You can't Edit this Staff Requisition detail. Reason: This Staff Requisition approval is still pending for lower level."), Me)
                        Exit Sub
                    End If
                End If
                'Sohail (12 Oct 2018) -- End

                Session("staffrequisitionapprovaltranunkid") = CInt(mdtList.Rows(intRowIdx)("staffrequisitionapprovaltranunkid").ToString())
                Session("staffrequisitiontranunkid") = CInt(mdtList.Rows(intRowIdx)("staffrequisitiontranunkid").ToString())
                Session("statusunkid") = CInt(mdtList.Rows(intRowIdx)("statusunkid").ToString())

                Response.Redirect("~/Recruitment/Staff_Requisition/wPg_ApproveDisapproveStaffRequisition.aspx", False)

                'Hemant (01 Nov 2021) -- Start
                'ENHANCEMENT : OLD-515 - Provide quick link on staff requisition report on request (Add/Edit and Approval pages of staff requisition) Similar to the way we have provide an icon.
            ElseIf e.CommandName = "ViewStaffRequisitionFormReport" Then
                Session("ReturnURL") = Request.Url.AbsoluteUri
                Session("intstaffrequisitiontranunkid") = CInt(mdtList.Rows(intRowIdx)("staffrequisitiontranunkid").ToString())
                Response.Redirect(Session("rootpath") & "Reports/Recruitment_Reports/Staff_Requisition_Form/Rpt_StaffRequisitionFormReport.aspx", False)
                'Hemant (01 Nov 2021) -- End

            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lvStaffReqApprovalList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles lvStaffReqApprovalList.PageIndexChanged
        Try
            lvStaffReqApprovalList.PageIndex = e.NewPageIndex
            lvStaffReqApprovalList.DataSource = mdtList
            lvStaffReqApprovalList.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("lvStaffReqApprovalList_PageIndexChanged :- " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region


    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        Language.setLanguage(mstrModuleName)

        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'SHANI [01 FEB 2015]--END

        Me.lblName.Text = Language._Object.getCaption(Me.lblName.ID, Me.lblName.Text)
        Me.lblAllocation.Text = Language._Object.getCaption(Me.lblAllocation.ID, Me.lblAllocation.Text)

        lvStaffReqApprovalList.Columns(1).HeaderText = Language._Object.getCaption(lvStaffReqApprovalList.Columns(1).FooterText, lvStaffReqApprovalList.Columns(1).HeaderText)
        lvStaffReqApprovalList.Columns(2).HeaderText = Language._Object.getCaption(lvStaffReqApprovalList.Columns(2).FooterText, lvStaffReqApprovalList.Columns(2).HeaderText)
        lvStaffReqApprovalList.Columns(3).HeaderText = Language._Object.getCaption(lvStaffReqApprovalList.Columns(3).FooterText, lvStaffReqApprovalList.Columns(3).HeaderText)
        lvStaffReqApprovalList.Columns(4).HeaderText = Language._Object.getCaption(lvStaffReqApprovalList.Columns(4).FooterText, lvStaffReqApprovalList.Columns(4).HeaderText)
        lvStaffReqApprovalList.Columns(5).HeaderText = Language._Object.getCaption(lvStaffReqApprovalList.Columns(5).FooterText, lvStaffReqApprovalList.Columns(5).HeaderText)
        lvStaffReqApprovalList.Columns(6).HeaderText = Language._Object.getCaption(lvStaffReqApprovalList.Columns(6).FooterText, lvStaffReqApprovalList.Columns(6).HeaderText)
        lvStaffReqApprovalList.Columns(7).HeaderText = Language._Object.getCaption(lvStaffReqApprovalList.Columns(7).FooterText, lvStaffReqApprovalList.Columns(7).HeaderText)
        lvStaffReqApprovalList.Columns(8).HeaderText = Language._Object.getCaption(lvStaffReqApprovalList.Columns(8).FooterText, lvStaffReqApprovalList.Columns(8).HeaderText)
        lvStaffReqApprovalList.Columns(9).HeaderText = Language._Object.getCaption(lvStaffReqApprovalList.Columns(9).FooterText, lvStaffReqApprovalList.Columns(9).HeaderText)
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        'Hemant (06 Aug 2019) -- Start
        'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
        Me.chkMyApprovals.Text = Language._Object.getCaption(Me.chkMyApprovals.ID, Me.chkMyApprovals.Text)
        'Hemant (06 Aug 2019) -- End
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub




End Class
