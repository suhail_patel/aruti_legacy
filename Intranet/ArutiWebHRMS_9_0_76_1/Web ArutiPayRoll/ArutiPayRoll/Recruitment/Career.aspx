﻿<%@ Page Title="Career | Jobs" Language="VB" AutoEventWireup="false" CodeFile="Career.aspx.vb"
    Inherits="Recruitment_Career" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html>
<head id="Head1" runat="server">
    <title>Career | Jobs</title>    
     <link rel="stylesheet" href="../font-awesome-4.3.0/css/font-awesome.css" />
      <link rel="stylesheet" href="../Help/bootstrap.css" />
    <style type="text/css">
        .brow
        {
            margin-right: -15px;
            margin-left: -15px;
        }
        .bbtn
        {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        .bbtn-primary
        {
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
        }
        body
        {
            font-family: "Helvetica Neue" , Helvetica, Arial, sans-serif;
            font-size: 14px !important;
            line-height: 1.42857143;
            color: #333;
            background-color: #fff;
        }
    </style>
</head>
<body class="">
    <form id="form2" runat="server" autocomplete="off">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.10.2.js"></script>
  
    <script type="text/javascript" src="../Help/bootstrap.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, event) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, evemt) {
            $("#endreq").val("1");
        }
    </script>

    <center>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <div id="MainDivCtrl" style="margin: 3px auto 0px auto; width: 100%; max-width: 1200px;">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="container-fluid well">
                            <asp:Panel ID="pnlForm" runat="server">
                                <h3 style="float: left;">
                                    <asp:Label ID="lblHeader" runat="server">Vacancies</asp:Label>&nbsp;<asp:Label ID="objlblCount"
                                        runat="server" Text=""></asp:Label></h3>
                                <div class="form-group row" style="display: none;">
                                    <div class="col-md-11">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <div class="fas fa-search">
                                                </div>
                                            </div>
                                            <asp:TextBox ID="txtVacanciesSearch" runat="server" CssClass="form-control" placeholder="Search Vacancy / Company / Skill / Qualification / Responsibility / Job Description"
                                                Style="height: 34px;" />
                                        </div>
                                    </div>
                                    <div class="col-md-1 ">
                                        <asp:Button ID="btnSearchVacancies" runat="server" Text="Search" CssClass="btn btn-primary" />
                                    </div>
                                </div>
                                <asp:DataList ID="dlVaanciesList" runat="server" Width="100%" DataKeyField="vacancyid"
                                    DataSourceID="odsVacancy" Style="font-size: 14px;">
                                    <ItemTemplate>
                                        <div class="panel panel-default">
                                            <div class="panel-body  bg-success" style="padding: 5px;">
                                                <div class="form-group row" style="margin-bottom: 5px">
                                                    <div class="col-md-12 h3" style="margin-bottom: 5px">
                                                        <div class="col-md-1 ">
                                                            <div class="fa fa-hand-o-right">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-11 ">
                                                            <asp:Label ID="objlblVacancyTitle" runat="server" Text='<%# Eval("vacancytitle") %>'></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12" style="display: none;">
                                                        <div class="col-md-1">
                                                        </div>
                                                        <div class="col-md-11">
                                                            <asp:Label ID="objlblCompName" runat="server" Text='<%# Session("CompName") %>'></asp:Label>
                                                            <asp:Label ID="objlblAuthCode" runat="server" Text=""></asp:Label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="padding-left: 15px" id="divJobLocation" runat="server">
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblJobLocation" runat="server" Text="Job Location :" Font-Bold="true"></asp:Label>
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="objlblJobLocation" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="padding-left: 15px" id="divRemark" runat="server">
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblJobDiscription" runat="server" Text="Job Description" Font-Bold="true"></asp:Label>
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="objlblJobDiscription" runat="server" Text='<%# Eval("remark").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="padding-left: 15px" id="divResponsblity" runat="server">
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblResponsblity" runat="server" Text="Responsibility: " Font-Bold="true"></asp:Label>
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="objlblResponsblity" runat="server" Text='<%#  Eval("duties").ToString.Replace(vbCrLf, "<br />").Replace(" ", "*").Replace(vbTab, "&nbsp;&nbsp;&nbsp;&nbsp;") %> '></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="padding-left: 15px" id="divSkill" runat="server">
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblSkill" runat="server" Text="Skill :" Font-Bold="true"></asp:Label>
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="objlblSkill" runat="server" Text='<%# Eval("skill") %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="padding-left: 15px" id="divQualification" runat="server">
                                                <div class="col-md-2">
                                                    <asp:Label ID="lblQualification" runat="server" Text="Qualification Required: " Font-Bold="true"></asp:Label>
                                                </div>
                                                <div class="col-md-10">
                                                    <asp:Label ID="objlblQualification" runat="server" Text='<%# Eval("Qualification") %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="padding-left: 15px; padding-top: 15px">
                                                <div class="col-md-6" id="divExp" runat="server">
                                                    <asp:Label ID="lblExp" runat="server" Text="Experience :" Font-Bold="true"></asp:Label>
                                                    <asp:Label ID="objlblExp" runat="server" Text='<%# IIf(Eval("experience") <> 0, Format(CDec(Eval("experience")) / 12, "###0.0#") + " Year(s)", "Fresher Can Apply") %>'></asp:Label>
                                                    <br />
                                                    <asp:Label ID="objlblExpCmt" runat="server" Text='<%# Eval("experience_comment")%>'></asp:Label>
                                                </div>
                                                <div class="col-md-6" id="divNoPosition" runat="server">
                                                    <asp:Label ID="lblNoPosition" runat="server" Text="No. of Position :" Font-Bold="true"></asp:Label>
                                                    <asp:Label ID="objlblNoPosition" runat="server" Text='<%# Eval("noposition") %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="padding-left: 15px" id="divLang" runat="server">
                                                <div class="col-md-12">
                                                    <asp:Label ID="lblLang" runat="server" Text="Preferred Language Skill :" Font-Bold="true"></asp:Label>
                                                    <asp:Label ID="objlblLang" runat="server" Text='<%# Eval("Lang") %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" id="divScale" runat="server" style="padding-left: 15px">
                                                <div class="col-md-12">
                                                    <asp:Label ID="lblScale" runat="server" Text="Scale :" Font-Bold="true"></asp:Label>
                                                    <asp:Label ID="objlblScale" runat="server" Text=""></asp:Label>
                                                </div>
                                            </div>
                                            <div class="form-group row" style="padding-left: 15px">
                                                <div class="col-md-6" id="divOpenningDate" runat="server">
                                                    <asp:Label ID="lblOpeningDate" runat="server" Text="Job Opening date :" Font-Bold="true"></asp:Label>
                                                    <asp:Label ID="objlblOpeningDate" runat="server" Text='<%# CDate(Eval("openingdate")).ToShortDateString() %>'></asp:Label>
                                                </div>
                                                <div class="col-md-6" id="divClosuingDate" runat="server">
                                                    <asp:Label ID="lblClosingDate" runat="server" Text="Job closing date :" Font-Bold="true"></asp:Label>
                                                    <asp:Label ID="objlblClosingDate" runat="server" Text='<%# CDate(Eval("closingdate")).ToShortDateString() %>'></asp:Label>
                                                </div>
                                            </div>
                                            <div class="panel-body  bg-success" style="padding: 5px; display: none;">
                                                <div class="form-group row" style="margin-bottom: 5px">
                                                    <div class="col-md-12 text-right">
                                                        <div>
                                                            <asp:Label ID="objlblIsApplyed" runat="server" Text='<%# Eval("IsApplied")  %>' Visible="false"></asp:Label>
                                                            <asp:LinkButton ID="btnApply" runat="server" CommandName="Apply" Font-Underline="false"
                                                                CssClass="btn btn-primary" Text="" ValidationGroup="SearchJob">
                                                                <div id="divApplied" runat="server" class="glyphicon glyphicon-ok" style="margin-right: 5px"
                                                                    visible="false">
                                                                </div>
                                                                <asp:Label ID="objlblApplyText" runat="server" Text="Login to Apply"></asp:Label>
                                                            </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                                <asp:ObjectDataSource ID="odsVacancy" runat="server" SelectMethod="GetApplicantVacancies"
                                    TypeName="Aruti.Data.clsVacancy" EnablePaging="false">
                                    <SelectParameters>
                                        <asp:Parameter Name="strDatabaseName" Type="String" DefaultValue="" />
                                        <%--<asp:Parameter Name="intComUnkID" Type="Int32" DefaultValue="0" />--%>
                                        <asp:Parameter Name="intMasterTypeId" Type="Int32" DefaultValue="0" />
                                        <asp:Parameter Name="intEType" Type="Int32" DefaultValue="0" />
                                        <asp:Parameter Name="blnVacancyType" Type="Boolean" DefaultValue="True" />
                                        <asp:Parameter Name="blnAllVacancy" Type="Boolean" DefaultValue="False" />
                                        <asp:Parameter Name="intDateZoneDifference" Type="Int32" DefaultValue="0" />
                                        <asp:Parameter Name="strVacancyUnkIdLIs" Type="String" DefaultValue="" />
                                        <asp:Parameter Name="intApplicantUnkId" Type="Int32" DefaultValue="0" />
                                        <asp:Parameter Name="blnOnlyCurrent" Type="Boolean" DefaultValue="True" />
                                        <asp:Parameter Name="blnOnlyExportToWeb" Type="Boolean" DefaultValue="False" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </asp:Panel>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSearchVacancies" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>
    </center>
    </form>    
</body>
</html>
