﻿Imports Aruti.Data.clsEmployee_Master
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.Services

Partial Class UserHome
    Inherits Basepage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim clsDataOpr As New eZeeCommonLib.clsDataOperation(True)
        Dim clsUserAddEdit As New Aruti.Data.clsUserAddEdit
        Dim clsEmpMst As New Aruti.Data.clsEmployee_Master
        ' Dim MessageBox As New MessageBox

        Try
            'Dim clsUser As New User

            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    pnlTask.Visible = False

            'End If

            'If IsPostBack = False Then
            '    clsUser = CType(Session("clsuser"), User)
            '    lblRfname.Text = clsUser.Firstname
            '    lblrSurname.Text = clsUser.Surname
            '    lblrAppdate.Text = clsUser.appointeddate
            '    lblRgend.Text = clsUser.gender
            '    lblRbdate.Text = clsUser.birthdate
            '    lblRemail.Text = clsUser.Email
            '    lblRpadd.Text = clsUser.present_address1
            '    lblRpadd2.Text = clsUser.present_address2
            '    lblRMobno.Text = clsUser.present_mobile
            '    lblRcurShift.Text = clsUser.ShiftName
            '    lblRcudept.Text = clsUser.Department
            '    GridView1.DataSource = clsUser.LeaveBalances
            '    GridView1.DataBind()
            'Else
            'End If

        Catch ex As Exception
            MsgBox("Error in page_load Event" & ex.Message)
            Throw ex
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Sohail (12 Mar 2015) -- Start
    'ENHANCEMENT
    <WebMethod()> Public Shared Function LoadFavouritesIcon() As String
        Dim clsDataOpr As New eZeeCommonLib.clsDataOperation(True)
        Dim ds As System.Data.DataSet
        Dim strQ As String
        Try

            strQ = "SELECT  ISNULL(usermenuunkid, " & CInt(HttpContext.Current.Session("UserId")) & ") AS usermenuunkid  " & _
                          ", ISNULL(cfuser_menu.userunkid, 0) AS userunkid " & _
                          ", ISNULL(cfuser_menu.employeeunkid, " & CInt(HttpContext.Current.Session("Employeeunkid")) & ") AS employeeunkid " & _
                          ", A.menuunkid " & _
                          ", A.reportunkid " & _
                          ", ISNULL(cfuser_menu.isfavourite, 0) AS isfavourite " & _
                          ", ISNULL(cfuser_menu.isactive, 1) AS isactive " & _
                    "FROM    ( SELECT    ISNULL(cfmenu_master.menuunkid, 0) AS menuunkid  " & _
                                      ", 0 AS reportunkid " & _
                                      ", ISNULL(cfmenu_master.isactive, 1) AS isactive " & _
                              "FROM      hrmsConfiguration..cfmenu_master " & _
                              "WHERE     cfmenu_master.isactive = 1 " & _
                              "UNION ALL " & _
                              "SELECT    0 AS menuunkid  " & _
                                      ", cfreport_master.reportunkid " & _
                                      ", ISNULL(cfreport_master.isactive, 1) AS isactive " & _
                              "FROM      hrmsConfiguration..cfreport_master " & _
                              "WHERE     ISNULL(cfreport_master.isactive, 1) = 1 " & _
                            ") AS A " & _
                            "LEFT JOIN hrmsConfiguration..cfuser_menu ON A.menuunkid = cfuser_menu.menuunkid " & _
                                                                        "AND cfuser_menu.reportunkid = A.reportunkid " & _
                                                                        "AND ISNULL(cfuser_menu.isactive, 1) = 1 "

            If HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User Then
                strQ &= "AND ISNULL(userunkid, " & CInt(HttpContext.Current.Session("UserId")) & ") = " & CInt(HttpContext.Current.Session("UserId")) & " "
            Else
                strQ &= "AND ISNULL(employeeunkid, " & CInt(HttpContext.Current.Session("Employeeunkid")) & ") = " & CInt(HttpContext.Current.Session("Employeeunkid")) & " "
            End If

            ds = clsDataOpr.WExecQuery(strQ, "List")

            If clsDataOpr.ErrorMessage <> "" Then
                Throw New Exception(clsDataOpr.ErrorMessage)
            End If

            Return ds.GetXml()

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Function
    'Sohail (12 Mar 2015) -- End

    'Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound

    '    If e.Row.RowIndex < 0 Then Exit Sub

    '    If e.Row.Cells(1).Text.Trim <> "" Then
    '        e.Row.Cells(1).Text = eZeeDate.convertDate(e.Row.Cells(1).Text).Date
    '    End If
    '    If e.Row.Cells(2).Text.Trim <> "" Then
    '        e.Row.Cells(2).Text = eZeeDate.convertDate(e.Row.Cells(2).Text).Date
    '    End If

    'End Sub
End Class
