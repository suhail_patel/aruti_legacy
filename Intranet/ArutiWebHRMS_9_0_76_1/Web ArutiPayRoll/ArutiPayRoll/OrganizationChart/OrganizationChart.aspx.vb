﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports
Imports System.Web.Script.Serialization
Imports System.IO

#End Region

Partial Class OrgChart_OrganizationChart
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes
    Private mdtEmployee As DataTable
    Private mdtJob As DataTable
    Private Shared ReadOnly mstrModuleName As String = "frmOrganizationChart"
    Private mstrAdvanceFilter As String = ""
    Private mblnIsFilterData As Boolean = False

    Private mstrFilterValues As String = "''"


#End Region

#Region " Private Functions & Methods "

    'Public Sub FillEmpList()
    '    Dim objorg As New clsOrganizationChart
    '    Dim ctr As Integer = 0
    '    Try
    '        mdtEmployee.Rows.Clear()
    '        Dim blnIncludeInactiveEmp As Boolean = False
    '        Dim dsList As DataSet = objorg.GetEmployeeList(Session("Database_Name").ToString, _
    '                                            CInt(Session("UserId")), _
    '                                            CInt(Session("Fin_year")), _
    '                                            CInt(Session("CompanyUnkId")), _
    '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
    '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
    '                                            Session("UserAccessModeSetting").ToString, True, _
    '                                            blnIncludeInactiveEmp, CBool(Session("IsImgInDataBase").ToString()), "Employee", False, , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, mstrAdvanceFilter, False, True, True, True)

    '        Dim dtTable As DataTable = New DataView(dsList.Tables("Employee"), "", "employeename", DataViewRowState.CurrentRows).ToTable
    '        Dim dRow As DataRow
    '        If mstrAdvanceFilter.Length > 0 Then
    '            mblnIsFilterData = True
    '        End If
    '        Dim unassignImage As String = ""
    '        unassignImage = Server.MapPath("../images/ChartUser.png")
    '        Dim xunassignImageData As Byte() = File.ReadAllBytes(unassignImage)
    '        unassignImage = Convert.ToBase64String(xunassignImageData)

    '        For Each dtRow As DataRow In dtTable.Rows
    '            dRow = mdtEmployee.NewRow
    '            ctr += 1
    '            Dim isJobExist As Integer = 0
    '            If CInt(cbocharttype.SelectedValue) = CInt(enOrganizationChartViewType.JOBTOJOB) Then
    '                isJobExist = mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Integer)("parent") = CInt(dtRow.Item("parent").ToString) AndAlso x.Field(Of Integer)("jobunkid") = CInt(dtRow.Item("jobunkid").ToString())).Count
    '            End If


    '            If isJobExist <= 0 Then

    '                dRow.Item("id") = CInt(dtRow.Item("employeeunkid"))
    '                dRow.Item("name") = dtRow.Item("employeename").ToString
    '                dRow.Item("parent") = dtRow.Item("parent").ToString

    '                dRow.Item("Department") = dtRow.Item("dept").ToString
    '                dRow.Item("Job") = dtRow.Item("job").ToString
    '                dRow.Item("Email") = dtRow.Item("empemail").ToString
    '                dRow.Item("Mobile") = dtRow.Item("empmobile").ToString
    '                dRow.Item("jobunkid") = CInt(dtRow.Item("jobunkid").ToString)



    '                If CInt(cbocharttype.SelectedValue) <> CInt(enOrganizationChartViewType.JOBTOJOB) Then

    '                    Dim st As String = ""
    '                    If CBool(Session("IsImgInDataBase")) Then
    '                        If IsDBNull(dtRow.Item("photo")) <> True Then
    '                            Dim bytes As Byte() = CType(dtRow.Item("photo"), Byte())
    '                            If CBool(Session("IsImgInDataBase").ToString()) Then
    '                                st = Convert.ToBase64String(bytes)
    '                            End If
    '                        Else
    '                            st = unassignImage
    '                        End If
    '                    Else
    '                        If IsDBNull(dtRow.Item("photo")) <> True Then
    '                            st = dtRow.Item("photo").ToString()
    '                            Dim xDocumentData As Byte() = File.ReadAllBytes(st)
    '                            st = Convert.ToBase64String(xDocumentData)
    '                        Else
    '                            st = unassignImage

    '                        End If
    '                    End If
    '                    dRow.Item("Image") = st
    '                End If

    '                dRow.Item("isOriginaldata") = CBool(dtRow.Item("isOriginaldata").ToString)


    '                If CInt(dtRow.Item("parent").ToString) > 0 AndAlso CInt(dtRow.Item("parent").ToString) <> CInt(dtRow.Item("employeeunkid")) Then
    '                    mdtEmployee = objorg.RecursionEmp(Session("Database_Name").ToString, _
    '                                                      CInt(Session("UserId")), _
    '                                                      CInt(Session("Fin_year")), _
    '                                                      CInt(Session("CompanyUnkId")), _
    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
    '                                                      Session("UserAccessModeSetting").ToString, True, blnIncludeInactiveEmp, _
    '                                                      CInt(dtRow.Item("parent").ToString), mdtEmployee, CBool(Session("IsImgInDataBase").ToString()), unassignImage, CType(CInt(cbocharttype.SelectedValue), enOrganizationChartViewType))

    '                End If
    '                mdtEmployee.Rows.Add(dRow)
    '            End If

    '        Next



    '        Dim Dv As DataView = New DataView(mdtEmployee, "", "id", DataViewRowState.CurrentRows)

    '        If mdtEmployee.Rows.Count < 0 Then
    '            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "sorry,their is no employee available for generate organization chart."), Me)
    '            Exit Sub
    '        End If
    '        Dim dsDataList As New DataSet
    '        Dim dtData As DataTable = Nothing
    '        Dim dsr = String.Join(",", mdtEmployee.AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("Id")) _
    '                                   .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Field(Of Integer)("Id")) _
    '                                   .Count()}).Where(Function(y) y.barCount = 1).Select(Function(x) x.barid.ToString).ToArray())
    '        If dsr.Length > 0 Then
    '            dtData = mdtEmployee.Select("Id IN(" & dsr & ")").CopyToDataTable()
    '        End If

    '        If dtData IsNot Nothing Then dsDataList.Tables.Add(dtData.Copy)

    '        dsr = String.Join(",", mdtEmployee.AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("Id")) _
    '                                   .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.Select(Function(y) y.Field(Of Boolean)("isOriginaldata") = True) _
    '                                   .Count()}).Where(Function(y) y.barCount > 1).Select(Function(x) x.barid.ToString).ToArray())

    '        If dsr.Length > 0 Then
    '            dtData = mdtEmployee.Select("Id IN(" & dsr & ") AND isOriginaldata = True").CopyToDataTable()
    '        Else
    '            dtData = Nothing
    '        End If


    '        If dtData IsNot Nothing Then dsDataList.Tables(0).Merge(dtData, True)

    '        If dsDataList IsNot Nothing AndAlso dsDataList.Tables.Count > 0 AndAlso dsDataList.Tables(0).Rows.Count > 0 Then
    '            mdtEmployee = dsDataList.Tables(0).Copy
    '        End If


    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objorg = Nothing
    '        If mdtEmployee.Rows.Count <= 0 Then
    '            Dim r As DataRow = mdtEmployee.NewRow
    '            mdtEmployee.Rows.Add(r)
    '        End If
    '    End Try
    'End Sub


    Public Sub FillEmpList()
        Dim objorg As New clsOrganizationChart
        Dim ctr As Integer = 0
        Try
            GC.Collect()
            mdtEmployee.Rows.Clear()
            Dim blnIncludeInactiveEmp As Boolean = False

            Dim unassignImage As String = ""
            unassignImage = Server.MapPath("../images/ChartUser.png")
            Dim xunassignImageData As Byte() = File.ReadAllBytes(unassignImage)
            unassignImage = Convert.ToBase64String(xunassignImageData)

            Dim dsList As DataSet = objorg.GetEmployeeList(Session("Database_Name").ToString, _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                Session("UserAccessModeSetting").ToString, True, _
                                                blnIncludeInactiveEmp, unassignImage, _
                                                CBool(Session("IsImgInDataBase").ToString()), _
                                                CType(CInt(cbocharttype.SelectedValue), enOrganizationChartViewType), "Employee", False, , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, mstrAdvanceFilter, False, True, True, True)

            mdtEmployee = New DataView(dsList.Tables("Employee"), "", "name", DataViewRowState.CurrentRows).ToTable
            Dim dRow As DataRow
            If mstrAdvanceFilter.Length > 0 Then
                mblnIsFilterData = True
            End If


            'For Each dtRow As DataRow In dtTable.Rows
            '    dRow = mdtEmployee.NewRow
            '    ctr += 1
            '    Dim isJobExist As Integer = 0
            '    If CInt(cbocharttype.SelectedValue) = CInt(enOrganizationChartViewType.JOBTOJOB) Then
            '        isJobExist = mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Integer)("parent") = CInt(dtRow.Item("parent").ToString) AndAlso x.Field(Of Integer)("jobunkid") = CInt(dtRow.Item("jobunkid").ToString())).Count
            '    End If


            '    If isJobExist <= 0 Then

            '        If CInt(dtRow.Item("parent").ToString) > 0 AndAlso CInt(dtRow.Item("parent").ToString) <> CInt(dtRow.Item("employeeunkid")) Then
            '            mdtEmployee = objorg.RecursionEmp(Session("Database_Name").ToString, _
            '                                              CInt(Session("UserId")), _
            '                                              CInt(Session("Fin_year")), _
            '                                              CInt(Session("CompanyUnkId")), _
            '                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                              Session("UserAccessModeSetting").ToString, True, blnIncludeInactiveEmp, _
            '                                              CInt(dtRow.Item("parent").ToString), mdtEmployee, CBool(Session("IsImgInDataBase").ToString()), unassignImage, CType(CInt(cbocharttype.SelectedValue), enOrganizationChartViewType))

            '        End If

            '        mdtEmployee.ImportRow(dRow)
            '    End If

            'Next



            Dim Dv As DataView = New DataView(mdtEmployee, "", "id", DataViewRowState.CurrentRows)
            GC.Collect()

            If mdtEmployee.Rows.Count < 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "sorry,their is no employee available for generate organization chart."), Me)
                Exit Sub
            End If
            Dim dsDataList As New DataSet
            Dim dtData As DataTable = Nothing
            Dim dsr = String.Join(",", mdtEmployee.AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("Id")) _
                                       .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Field(Of Integer)("Id")) _
                                       .Count()}).Where(Function(y) y.barCount = 1).Select(Function(x) x.barid.ToString).ToArray())
            GC.Collect()

            If dsr.Length > 0 Then
                dtData = mdtEmployee.Select("Id IN(" & dsr & ")").CopyToDataTable()
            End If
            GC.Collect()

            If dtData IsNot Nothing Then dsDataList.Tables.Add(dtData.Copy)

            dsr = String.Join(",", mdtEmployee.AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("Id")) _
                                       .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.Select(Function(y) y.Field(Of Boolean)("isOriginaldata") = True) _
                                       .Count()}).Where(Function(y) y.barCount > 1).Select(Function(x) x.barid.ToString).ToArray())
            GC.Collect()

            If dsr.Length > 0 Then
                dtData = mdtEmployee.Select("Id IN(" & dsr & ") AND isOriginaldata = True").CopyToDataTable()
            Else
                dtData = Nothing
            End If
            GC.Collect()


            If dtData IsNot Nothing Then dsDataList.Tables(0).Merge(dtData, True)

            If dsDataList IsNot Nothing AndAlso dsDataList.Tables.Count > 0 AndAlso dsDataList.Tables(0).Rows.Count > 0 Then
                mdtEmployee = dsDataList.Tables(0).Copy
            End If
            GC.Collect()


            If mdtEmployee.Rows.Count <= 0 Then
                Dim r As DataRow = mdtEmployee.NewRow
                mdtEmployee.Rows.Add(r)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objorg = Nothing
            GC.Collect()
        End Try
    End Sub



    Public Sub FillJobList()
        Dim objorg As New clsOrganizationChart
        Try

            Dim Jobfilter As String = String.Empty
            Dim Groupfilter As String = ",hrjob_master.jobunkid"
            mdtJob.Rows.Clear()

            Dim blnIncludeInactiveEmp As Boolean = False

            Dim objFilterJson As New List(Of ClsFilterJson)


            If CInt(cbobranch.SelectedValue) > 0 Then
                Jobfilter &= "AND hrjob_master.jobbranchunkid = '" & cbobranch.SelectedValue & "' "
                Groupfilter &= ",hrjob_master.jobbranchunkid "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lblbranch.Text, .FilterValue = cbobranch.SelectedItem.Text})
            End If
            If CInt(cbodeptgroup.SelectedValue) > 0 Then
                Jobfilter &= "AND hrjob_master.jobdepartmentgrpunkid = '" & cbodeptgroup.SelectedValue & "' "
                Groupfilter &= ",hrjob_master.jobdepartmentgrpunkid "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lbldeptgroup.Text, .FilterValue = cbodeptgroup.SelectedItem.Text})

            End If
            If CInt(cbodepartment.SelectedValue) > 0 Then
                Jobfilter &= "AND hrjob_master.jobdepartmentunkid = '" & cbodepartment.SelectedValue & "' "
                Groupfilter &= ",hrjob_master.jobdepartmentunkid "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lbldepartment.Text, .FilterValue = cbodepartment.SelectedItem.Text})
            End If
            If CInt(cbosectiongroup.SelectedValue) > 0 Then
                Jobfilter &= "AND hrjob_master.jobsectiongrpunkid = '" & cbosectiongroup.SelectedValue & "' "
                Groupfilter &= ",hrjob_master.jobsectiongrpunkid "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lblsectiongroup.Text, .FilterValue = cbosectiongroup.SelectedItem.Text})
            End If
            If CInt(cbosection.SelectedValue) > 0 Then
                Jobfilter &= "AND hrjob_master.jobsectionunkid = '" & cbosection.SelectedValue & "' "
                Groupfilter &= ",hrjob_master.jobsectionunkid "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lblsection.Text, .FilterValue = cbosection.SelectedItem.Text})
            End If
            If CInt(cbounitgroup.SelectedValue) > 0 Then
                Jobfilter &= "AND hrjob_master.jobunitgrpunkid = '" & cbounitgroup.SelectedValue & "' "
                Groupfilter &= ",hrjob_master.jobunitgrpunkid "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lblunitgroup.Text, .FilterValue = cbounitgroup.SelectedItem.Text})
            End If
            If CInt(cbounit.SelectedValue) > 0 Then
                Jobfilter &= "AND hrjob_master.jobunitunkid = '" & cbounit.SelectedValue & "' "
                Groupfilter &= ",hrjob_master.jobunitunkid "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lblunit.Text, .FilterValue = cbounit.SelectedItem.Text})
            End If
            If CInt(cboteam.SelectedValue) > 0 Then
                Jobfilter &= "AND hrjob_master.teamunkid = '" & cboteam.SelectedValue & "' "
                Groupfilter &= ",hrjob_master.teamunkid "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lblteam.Text, .FilterValue = cboteam.SelectedItem.Text})
            End If
            If CInt(cboclassgroup.SelectedValue) > 0 Then
                Jobfilter &= "AND hrjob_master.jobclassgroupunkid = '" & cboclassgroup.SelectedValue & "' "
                Groupfilter &= ",hrjob_master.jobclassgroupunkid "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lblclassgroup.Text, .FilterValue = cboclassgroup.SelectedItem.Text})
            End If
            If CInt(cboclass.SelectedValue) > 0 Then
                Jobfilter &= "AND hrjob_master.jobclassunkid = '" & cboclass.SelectedValue & "' "
                Groupfilter &= ",jobclassunkid "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lblclass.Text, .FilterValue = cboclass.SelectedItem.Text})
            End If
            If CInt(cboGrade.SelectedValue) > 0 Then
                Jobfilter &= "AND hrjob_master.jobgradeunkid = '" & cboGrade.SelectedValue & "' "
                Groupfilter &= ",hrjob_master.jobgradeunkid "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lblGrade.Text, .FilterValue = cboGrade.SelectedItem.Text})
            End If
            If CInt(cboGradeLevel.SelectedValue) > 0 Then
                Jobfilter &= "AND hrjob_master.jobgradelevelunkid = '" & cboGradeLevel.SelectedValue & "' "
                Groupfilter &= ",hrjob_master.jobgradelevelunkid "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lblGradeLevel.Text, .FilterValue = cboGradeLevel.SelectedItem.Text})
            End If
            If CInt(cboJobGroup.SelectedValue) > 0 Then
                Jobfilter &= " AND hrjob_master.jobgroupunkid = '" & cboJobGroup.SelectedValue & "' "
                Groupfilter &= ",hrjob_master.jobgroupunkid "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lblJobGroup.Text, .FilterValue = cboJobGroup.SelectedItem.Text})
            End If

            If CInt(cboJob.SelectedValue) > 0 Then
                Jobfilter &= " AND hrjob_master.jobunkid = '" & cboJob.SelectedValue & "' "
                objFilterJson.Add(New ClsFilterJson With {.Filterkey = lblJob.Text, .FilterValue = cboJob.SelectedItem.Text})
            End If

            'Jobfilter &= ") As Job Where 1 = 1 "


            If Jobfilter.Length > 0 Then
                mblnIsFilterData = True
                Dim jsSerializer As New JavaScriptSerializer
                jsSerializer.MaxJsonLength = 999999999
                Dim json As String = jsSerializer.Serialize(objFilterJson)
                mstrFilterValues = json
            End If


            Dim dsList As DataSet = objorg.GetJobWiseList("Job", Jobfilter, Groupfilter, True)

            Dim Dv As DataView = New DataView(dsList.Tables("Job"), "", "parent", DataViewRowState.CurrentRows)

            Dim dtTable As DataTable = Dv.ToTable()

            Dim dRow As DataRow

            For Each dtRow As DataRow In dtTable.Rows
                dRow = mdtJob.NewRow

                dRow.Item("id") = CInt(dtRow.Item("JobId"))
                dRow.Item("name") = dtRow.Item("Job Name").ToString
                dRow.Item("parent") = dtRow.Item("parent").ToString

                dRow.Item("totalpostion") = dtRow.Item("Total Postion").ToString
                dRow.Item("positionheld") = dtRow.Item("Position Held").ToString
                dRow.Item("variance") = dtRow.Item("Variance").ToString
                dRow.Item("isOriginaldata") = CBool(dtRow.Item("isOriginaldata").ToString)

                If Jobfilter.Length > 0 Then
                    If CInt(dtRow.Item("parent").ToString) > 0 AndAlso CInt(dtRow.Item("parent").ToString) <> CInt(dtRow.Item("JobId")) Then
                        mdtJob = objorg.RecursionJob(mdtJob, CInt(dtRow.Item("parent").ToString))
                    End If
                End If

                mdtJob.Rows.Add(dRow)
            Next

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillJobList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objorg = Nothing

            If mdtJob.Rows.Count <= 0 Then
                Dim r As DataRow = mdtJob.NewRow

                mdtJob.Rows.Add(r)
            End If

        End Try
    End Sub

    Private Sub CreateEmpListTable()
        Try
            mdtEmployee = New DataTable
            With mdtEmployee
                .Columns.Add("id", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("parent", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("empBaseImage", System.Type.GetType("System.String")).DefaultValue = ""

                .Columns.Add("Department", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("Job", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("Email", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("Mobile", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("isOriginaldata", System.Type.GetType("System.Boolean")).DefaultValue = False
                .Columns.Add("TotalPostion", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("PositionHeld", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("Variance", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("jobunkid", System.Type.GetType("System.Int32")).DefaultValue = 0

            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub CreateJobListTable()
        Try

            mdtJob = New DataTable
            With mdtJob
                .Columns.Add("id", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("parent", System.Type.GetType("System.Int32")).DefaultValue = 0

                .Columns.Add("totalpostion", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("positionheld", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("variance", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("isOriginaldata", System.Type.GetType("System.Boolean")).DefaultValue = False
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function DataTableToJSONWithJavaScriptSerializer(ByVal table As DataTable) As String
        Try
            Dim jsSerializer As JavaScriptSerializer = New JavaScriptSerializer()
            Dim parentRow As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()
            Dim childRow As Dictionary(Of String, Object)
            'Dim iLength As Int64 = table.AsEnumerable().Select(Function(x) x.Field(Of String)("Image").Length).Max
            For Each row As DataRow In table.Rows
                childRow = New Dictionary(Of String, Object)()
                For Each col As DataColumn In table.Columns
                    childRow.Add(col.ColumnName, row(col))
                Next
                parentRow.Add(childRow)
            Next

            'jsSerializer.MaxJsonLength = 86753090
            jsSerializer.MaxJsonLength = 999999999
            Return jsSerializer.Serialize(parentRow)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Sub FillCombo()
        Dim objclsMasterData As New clsMasterData
        Dim dsCombos As DataSet

        Try
            dsCombos = objclsMasterData.getComboListCharttype("List")
            With cbocharttype
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            Dim objEmployee As New clsEmployee_Master
            Dim dsComboList As New DataSet
            Dim objJobGroup As New clsJobGroup
            Dim objJob As New clsJobs
            Dim objGrade As New clsGrade
            Dim objGradeLevel As New clsGradeLevel
            Dim objCommon As New clsCommon_Master
            Dim objBranch As New clsStation
            Dim objUnitGroup As New clsUnitGroup
            Dim objDeptGroup As New clsDepartmentGroup
            Dim objUnit As New clsUnits
            Dim objDepartment As New clsDepartment
            Dim objTeam As New clsTeams
            Dim objSectionGrp As New clsSectionGroup
            Dim objClassGroup As New clsClassGroup
            Dim objSection As New clsSections
            Dim objClass As New clsClass

            Dim objDeptGrp As New clsDepartmentGroup


            dsComboList = objDeptGrp.getComboList("DeptGrp", True)
            lblOrganizationChartDeptGroup.Text = Language.getMessage("clsMasterData", 429, "Department Group")
            With drpOrganizationChartDeptGroup
                .DataValueField = "deptgroupunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables("DeptGrp")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objJob.getComboList("Job", True)
            With cboJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objGrade.getComboList("Grade", True)
            With cboGrade
                .DataValueField = "gradeunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()

            End With

            dsComboList = objBranch.getComboList("List", True)
            With cbobranch
                .DataValueField = "stationunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objUnitGroup.getComboList("List", True)
            With cbounitgroup
                .DataValueField = "unitgroupunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objDeptGroup.getComboList("List", True)
            With cbodeptgroup
                .DataValueField = "deptgroupunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objUnit.getComboList("List", True)
            With cbounit
                .DataValueField = "unitunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With


            dsComboList = objDepartment.getComboList("List", True)
            With cbodepartment
                .DataValueField = "departmentunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objTeam.getComboList("List", True)
            With cboteam
                .DataValueField = "teamunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objSectionGrp.getComboList("List", True)
            With cbosectiongroup
                .DataValueField = "sectiongroupunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objClassGroup.getComboList("List", True)
            With cboclassgroup
                .DataValueField = "classgroupunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objSection.getComboList("List", True)
            With cbosection
                .DataValueField = "sectionunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objClass.getComboList("List", True)
            With cboclass
                .DataValueField = "classesunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objGradeLevel.getComboList("GradeLevel", True)
            With cboGradeLevel
                .DataValueField = "gradelevelunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If IsNothing(dsCombos) = False Then
                dsCombos.Tables(0).Rows.Clear()
                dsCombos = Nothing
            End If

        End Try
    End Sub

    Public Sub FindChild(ByRef JsonString As String, ByVal dtChilddata As DataRow())
        Try

            JsonString += "'children': ["

            For i As Integer = 0 To dtChilddata.Length - 1
                Dim childrow As DataRow() = mdtJob.Select("[parent]=" + dtChilddata(i)("Id").ToString() + "")

                If childrow.Length > 0 Then
                    JsonString += "{'name': '" & dtChilddata(i)("name").ToString() & "',"
                    FindChild(JsonString, childrow)
                Else
                    JsonString += "{'name': '" & dtChilddata(i)("name").ToString() & "'}"
                    JsonString += "]},"


                End If
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    'Public Sub FillInterAllocationList()
    '    Dim objorg As New clsOrganizationChart
    '    Try

    '        Dim Jobfilter As String = String.Empty
    '        Dim Groupfilter As String = ",hrjob_master.jobunkid"
    '        mdtJob.Rows.Clear()

    '        Dim blnIncludeInactiveEmp As Boolean = False

    '        Dim dsList As DataSet = objorg.GetInterAllocationList("InterAllocation")

    '        Dim Dv As DataView = New DataView(dsList.Tables("Job"), "", "parent", DataViewRowState.CurrentRows)

    '        Dim dtTable As DataTable = Dv.ToTable()

    '        Dim dRow As DataRow

    '        For Each dtRow As DataRow In dtTable.Rows
    '            dRow = mdtJob.NewRow

    '            dRow.Item("id") = CInt(dtRow.Item("JobId"))
    '            dRow.Item("name") = dtRow.Item("Job Name").ToString
    '            dRow.Item("parent") = dtRow.Item("parent").ToString

    '            dRow.Item("totalpostion") = dtRow.Item("Total Postion").ToString
    '            dRow.Item("positionheld") = dtRow.Item("Position Held").ToString
    '            dRow.Item("variance") = dtRow.Item("Variance").ToString
    '            dRow.Item("isOriginaldata") = CBool(dtRow.Item("isOriginaldata").ToString)

    '            'If Jobfilter.Length > 0 Then
    '            '    If CInt(dtRow.Item("parent").ToString) > 0 AndAlso CInt(dtRow.Item("parent").ToString) <> CInt(dtRow.Item("JobId")) Then
    '            '        mdtJob = objorg.RecursionJob(mdtJob, CInt(dtRow.Item("parent").ToString))
    '            '    End If
    '            'End If

    '            mdtJob.Rows.Add(dRow)
    '        Next

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objorg = Nothing
    '        If mdtJob.Rows.Count <= 0 Then
    '            Dim r As DataRow = mdtJob.NewRow
    '            mdtJob.Rows.Add(r)
    '        End If

    '    End Try
    'End Sub
#End Region

#Region " Page's Event "
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState.Add("mdtEmployee", mdtEmployee)
            ViewState.Add("mdtJob", mdtJob)
            ViewState.Add("mstrAdvanceFilter", mstrAdvanceFilter)
            ViewState.Add("mstrFilterValues", mstrFilterValues)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If


            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            If Me.IsPostBack = False Then
                Call CreateEmpListTable()
                Call CreateJobListTable()
                FillCombo()
            Else
                mdtEmployee = CType(ViewState("mdtEmployee"), DataTable)
                mdtJob = CType(ViewState("mdtJob"), DataTable)
                mstrAdvanceFilter = ViewState("mstrAdvanceFilter").ToString()
                mstrFilterValues = ViewState("mstrFilterValues").ToString()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub
#End Region

#Region " Button's Event(s)"
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            If IsNothing(popupAdvanceFilter._GetFilterText) = False AndAlso popupAdvanceFilter._GetFilterText.Count > 0 AndAlso chkViewFilters.Checked Then

                Dim objFilterJson As New List(Of ClsFilterJson)
                For Each item As KeyValuePair(Of String, String) In popupAdvanceFilter._GetFilterText
                    objFilterJson.Add(New ClsFilterJson With {.Filterkey = item.Key.ToString(), .FilterValue = item.Value.ToString()})
                Next

                If objFilterJson.Count > 0 Then
                    Dim jsSerializer As New JavaScriptSerializer
                    jsSerializer.MaxJsonLength = 999999999
                    Dim json As String = jsSerializer.Serialize(objFilterJson)
                    mstrFilterValues = json
                End If


            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " button's Events "
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnViewchart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewchart.Click

        Try
            mblnIsFilterData = False

            If CInt(cbocharttype.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Chart View Type to Continue", Me)
                Exit Sub
            End If

            Session("expandall") = chkExpandAll.Checked.ToString().ToLower()
            If CInt(cbocharttype.SelectedValue) = CInt(enOrganizationChartViewType.EMPLOYEEWISE) Or CInt(cbocharttype.SelectedValue) = CInt(enOrganizationChartViewType.JOBTOJOB) Then

                Call FillEmpList()
                GC.Collect()
                GC.WaitForPendingFinalizers()
                GC.Collect()


                Dim source = New List(Of ClsEmployee)

                'Dim md As DataTable = mdtEmployee.Rows.Cast(Of DataRow).Take(3000).CopyToDataTable()


                If IsNothing(mdtEmployee) = False AndAlso mdtEmployee.Rows.Count > 0 Then


                    If CInt(cbocharttype.SelectedValue) = CInt(enOrganizationChartViewType.EMPLOYEEWISE) Then
                For Each dtrow As DataRow In mdtEmployee.Rows
                    source.Add(New ClsEmployee With {.Employeename = dtrow.Item("name").ToString(), _
                                                  .Parent = CInt(dtrow.Item("parent")), _
                                                  .Employeeid = CInt(dtrow.Item("id")), _
                                                  .Department = dtrow.Item("Department").ToString(), _
                                                  .JobPosition = dtrow.Item("Job").ToString(), _
                                                  .Mobile = dtrow.Item("Mobile").ToString(), _
                                                          .Photo = dtrow.Item("empBaseImage").ToString(), _
                                                  .Email = dtrow.Item("Email").ToString(), _
                                                  .Isoriginaldata = CBool(dtrow.Item("Isoriginaldata"))})
                Next
                    Else
                        For Each dtrow As DataRow In mdtEmployee.Rows
                            source.Add(New ClsEmployee With {.Employeename = dtrow.Item("name").ToString(), _
                                                          .Parent = CInt(dtrow.Item("parent")), _
                                                          .Employeeid = CInt(dtrow.Item("id")), _
                                                          .Department = dtrow.Item("Department").ToString(), _
                                                          .JobPosition = dtrow.Item("Job").ToString(), _
                                                          .Mobile = dtrow.Item("Mobile").ToString(), _
                                                          .Photo = dtrow.Item("empBaseImage").ToString(), _
                                                          .Email = dtrow.Item("Email").ToString(), _
                                                          .Isoriginaldata = CBool(dtrow.Item("Isoriginaldata")), _
                                                          .PlannedPosition = dtrow.Item("total postion").ToString(), _
                                                          .HeadCount = dtrow.Item("position held").ToString(), _
                                                          .PositionVariance = dtrow.Item("variance").ToString()})
                        Next
                    End If
                End If

                'GC.SuppressFinalize(source)

                GC.Collect()
                GC.WaitForPendingFinalizers()
                GC.Collect()

                If mdtEmployee.Rows.Count > 0 AndAlso CInt(mdtEmployee.Rows(0)("Id").ToString()) > 0 Then
                    Dim output = MapToEmployeeModelJsonCollection(source)

                    Dim jsSerializer As New JavaScriptSerializer
                    jsSerializer.MaxJsonLength = 2147483644
                    Dim json As String = String.Empty
                    json &= jsSerializer.Serialize(output)


                    Session("orgdata") = json
                    Session("status") = CInt(cbocharttype.SelectedValue)
                    Session("isFilterdata") = mblnIsFilterData.ToString.ToLower
                    Session("viewJobdetail") = "true"
                    Session("FilterData") = mstrFilterValues
                    Session("ShowFilter") = chkViewFilters.Checked.ToString.ToLower
                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "Preview", "openwin();", True)
                Else
                    DisplayMessage.DisplayMessage("Data Not Found.", Me)
                    Exit Sub
                End If


            ElseIf CInt(cbocharttype.SelectedValue) = CInt(enOrganizationChartViewType.INTERALLOCATION) Then

                If CInt(drpOrganizationChartDeptGroup.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage("" & Language.getMessage("clsMasterData", 429, "Department Group") & " Is Mandatory Information,Please select for continue.", Me)
                    Exit Sub
                End If


                Dim objorg As New clsOrganizationChart
                Dim dsInterAllocation As New DataSet
                dsInterAllocation = objorg.GetInterAllocationList("List", CInt(drpOrganizationChartDeptGroup.SelectedValue))

                Dim source = New List(Of ClsJob)

                For Each dtrow As DataRow In dsInterAllocation.Tables(0).Rows

                    Dim isMainParent As Boolean = False
                    If CInt(dtrow.Item("parent")) = CInt(dtrow.Item("id")) Then
                        isMainParent = True
                    End If


                    source.Add(New ClsJob With {.Jobname = dtrow.Item("name").ToString(), _
                                                  .Parent = CInt(IIf(isMainParent, 0, CInt(dtrow.Item("parent")))), _
                                                  .Jobid = CInt(dtrow.Item("id")), _
                                                  .PlannedPosition = "0", _
                                                  .HeadCount = "0", _
                                                  .PositionVariance = "0", _
                                                  .Isoriginaldata = True})
                Next


                If dsInterAllocation.Tables(0).Rows.Count > 0 AndAlso CInt(dsInterAllocation.Tables(0).Rows(0)("Id").ToString()) > 0 Then
                    Dim output = MapToJobModelJsonCollection(source)
                    Dim jsSerializer As New JavaScriptSerializer
                    jsSerializer.MaxJsonLength = 2147483644
                    Dim json As String = jsSerializer.Serialize(output)

                    Session("orgdata") = json
                    Session("status") = CInt(cbocharttype.SelectedValue)
                    Session("viewJobdetail") = chkViewJobdetail.Checked.ToString.ToLower
                    Session("isFilterdata") = mblnIsFilterData.ToString.ToLower

                    If mblnIsFilterData = False Then
                        Session("FilterData") = "''"
                    Else
                        Session("FilterData") = mstrFilterValues
                    End If


                    Session("ShowFilter") = chkViewFilters.Checked.ToString.ToLower

                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "Preview", "openwin();", True)

                Else
                    DisplayMessage.DisplayMessage("Data Not Found.", Me)
                    Exit Sub
                End If




            ElseIf CInt(cbocharttype.SelectedValue) = 2 Then

                Call FillJobList()
                Dim source = New List(Of ClsJob)

                For Each dtrow As DataRow In mdtJob.Rows
                    source.Add(New ClsJob With {.Jobname = dtrow.Item("name").ToString(), _
                                                  .Parent = CInt(dtrow.Item("parent")), _
                                                  .Jobid = CInt(dtrow.Item("id")), _
                                                  .PlannedPosition = dtrow.Item("totalpostion").ToString(), _
                                                  .HeadCount = dtrow.Item("positionheld").ToString(), _
                                                  .PositionVariance = dtrow.Item("variance").ToString(), _
                                                  .Isoriginaldata = CBool(dtrow.Item("Isoriginaldata"))})
                Next


                If mdtJob.Rows.Count > 0 AndAlso CInt(mdtJob.Rows(0)("Id").ToString()) > 0 Then
                    Dim output = MapToJobModelJsonCollection(source)
                    Dim jsSerializer As New JavaScriptSerializer
                    jsSerializer.MaxJsonLength = 2147483644
                    Dim json As String = jsSerializer.Serialize(output)

                    Session("orgdata") = json
                    Session("status") = CInt(cbocharttype.SelectedValue)
                    Session("viewJobdetail") = chkViewJobdetail.Checked.ToString.ToLower
                    Session("isFilterdata") = mblnIsFilterData.ToString.ToLower

                    If mblnIsFilterData = False Then
                        Session("FilterData") = "''"
                    Else
                        Session("FilterData") = mstrFilterValues
                    End If


                    Session("ShowFilter") = chkViewFilters.Checked.ToString.ToLower

                    ScriptManager.RegisterStartupScript(Me.Page, Me.GetType, "Preview", "openwin();", True)

                Else
                    DisplayMessage.DisplayMessage("Data Not Found.", Me)
                    Exit Sub
                End If

            End If


        Catch ex As Exception
            GC.Collect()
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
#End Region

#Region " Checkbox Events "
    Private Sub cboJobGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJobGroup.SelectedIndexChanged, _
                                                                                                                     cbobranch.SelectedIndexChanged, _
                                                                                                                     cbodeptgroup.SelectedIndexChanged, _
                                                                                                                     cbodepartment.SelectedIndexChanged, _
                                                                                                                     cbosectiongroup.SelectedIndexChanged, _
                                                                                                                     cbosection.SelectedIndexChanged, _
                                                                                                                     cbounitgroup.SelectedIndexChanged, _
                                                                                                                     cbounit.SelectedIndexChanged, _
                                                                                                                     cboteam.SelectedIndexChanged, _
                                                                                                                     cboclassgroup.SelectedIndexChanged, _
                                                                                                                     cboclass.SelectedIndexChanged, _
                                                                                                                     cboGrade.SelectedIndexChanged
        Dim objCategorize As New clsemployee_categorization_Tran
        Dim objJob As New clsJobs
        Dim dsCombos As New DataSet
        Try
            dsCombos = objCategorize.getJobComboList("List", True, _
                                                     CInt(cbobranch.SelectedValue), _
                                                     CInt(cbodeptgroup.SelectedValue), _
                                                     CInt(cbodepartment.SelectedValue), _
                                                     CInt(cbosectiongroup.SelectedValue), _
                                                     CInt(cbosection.SelectedValue), _
                                                     CInt(cbounitgroup.SelectedValue), _
                                                     CInt(cbounit.SelectedValue), _
                                                     CInt(cboteam.SelectedValue), _
                                                     CInt(cboclassgroup.SelectedValue), _
                                                     CInt(cboclass.SelectedValue), _
                                                     CInt(cboGrade.SelectedValue), _
                                                     CInt(cboGradeLevel.SelectedValue), _
                                                     CInt(cboJobGroup.SelectedValue))

            Dim intProvJobUnkId As String = cboJob.SelectedValue

            With cboJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                Try
                    .SelectedValue = intProvJobUnkId
                Catch ex As Exception
                    .SelectedValue = "0"
                End Try
                If .SelectedValue Is Nothing Then
                    .SelectedValue = "0"
                End If
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboEmployee_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objJob = Nothing
            dsCombos = Nothing
        End Try
    End Sub

    Protected Sub cbocharttype_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbocharttype.SelectedIndexChanged
        Try

            If CInt(cbocharttype.SelectedValue) = enOrganizationChartViewType.EMPLOYEEWISE OrElse CInt(cbocharttype.SelectedValue) = enOrganizationChartViewType.JOBTOJOB Then
                pnljobFilter.Visible = False
                chkViewJobdetail.Visible = False
                lnkAdvanceFilter.Visible = True
                pnlOrganizationChartFilter.Visible = False

            ElseIf CInt(cbocharttype.SelectedValue) = enOrganizationChartViewType.JOBWISE Then
                pnljobFilter.Visible = True
                chkViewJobdetail.Visible = True
                lnkAdvanceFilter.Visible = False
                pnlOrganizationChartFilter.Visible = False

            ElseIf CInt(cbocharttype.SelectedValue) = enOrganizationChartViewType.INTERALLOCATION Then
                pnljobFilter.Visible = False
                chkViewJobdetail.Visible = False
                lnkAdvanceFilter.Visible = False
                pnlOrganizationChartFilter.Visible = True
            End If


            If CInt(cbocharttype.SelectedValue) > 0 Then
                chkViewFilters.Visible = True
            Else
                chkViewFilters.Visible = False

            End If


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    Private Function MapToJobModelJsonCollection(ByVal source As ICollection(Of ClsJob)) As ICollection(Of ClsJobviewJson)
        Try         'Hemant (13 Aug 2020)
        Dim allItems = source.[Select](Function(e) New ClsJobviewJson With {.Jobname = e.Jobname, .Parent = e.Parent, .Jobid = e.Jobid, .PlannedPosition = e.PlannedPosition, .PositionVariance = e.PositionVariance, .HeadCount = e.HeadCount, .Isoriginaldata = e.Isoriginaldata}).ToList()

        For Each item In allItems
            Dim children = allItems.Where(Function(e) e.Parent = item.Jobid).ToList()

            If children.Any() Then
                item.children = children
            End If
        Next

        Return allItems.Where(Function(e) e.Parent = 0).ToList()
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Function

    Private Function MapToInterallocationModelJsonCollection(ByVal source As ICollection(Of ClsJob)) As ICollection(Of ClsJobviewJson)
        Try         'Hemant (13 Aug 2020)
            Dim allItems = source.[Select](Function(e) New ClsJobviewJson With {.Jobname = e.Jobname, .Parent = e.Parent, .Jobid = e.Jobid, .PlannedPosition = e.PlannedPosition, .PositionVariance = e.PositionVariance, .HeadCount = e.HeadCount, .Isoriginaldata = e.Isoriginaldata}).ToList()

            For Each item In allItems
                Dim children = allItems.Where(Function(e) e.Parent = item.Jobid).ToList()

                If children.Any() Then
                    item.children = children
                End If
            Next

            Return allItems.Where(Function(e) e.Parent = 0).ToList()
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Function

    Private Function MapToEmployeeModelJsonCollection(ByVal source As ICollection(Of ClsEmployee)) As ICollection(Of ClsEmployeeviewJson)
        Try         'Hemant (13 Aug 2020)
            Dim allItems = source.[Select](Function(e) New ClsEmployeeviewJson With {.Employeename = e.Employeename, .Parent = e.Parent, .Employeeid = e.Employeeid, .Department = e.Department, .Mobile = e.Mobile, .Email = e.Email, .JobPosition = e.JobPosition, .Photo = e.Photo, .Isoriginaldata = e.Isoriginaldata, .PlannedPosition = e.PlannedPosition, .PositionVariance = e.PositionVariance, .HeadCount = e.HeadCount}).ToList()

        For Each item In allItems
            Dim children = allItems.Where(Function(e) e.Parent = item.Employeeid).ToList()

            If children.Any() Then
                item.children = children
            End If
        Next
        Return allItems.Where(Function(e) e.Parent = 0).ToList()
            'Return allItems.ToList()
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Function

End Class

Public Class ClsJob
    Private mstrJobname As String
    Public Property Jobname() As String
        Get
            Return mstrJobname
        End Get
        Set(ByVal value As String)
            mstrJobname = value
        End Set
    End Property

    Private mstrPlannedPosition As String
    Public Property PlannedPosition() As String
        Get
            Return mstrPlannedPosition
        End Get
        Set(ByVal value As String)
            mstrPlannedPosition = value
        End Set
    End Property

    Private mstrHeadCount As String
    Public Property HeadCount() As String
        Get
            Return mstrHeadCount
        End Get
        Set(ByVal value As String)
            mstrHeadCount = value
        End Set
    End Property

    Private mstrPositionVariance As String
    Public Property PositionVariance() As String
        Get
            Return mstrPositionVariance
        End Get
        Set(ByVal value As String)
            mstrPositionVariance = value
        End Set
    End Property

    Private mintParent As Integer
    Public Property Parent() As Integer
        Get
            Return mintParent
        End Get
        Set(ByVal value As Integer)
            mintParent = value
        End Set
    End Property

    Private mintJobid As Integer
    Public Property Jobid() As Integer
        Get
            Return mintJobid
        End Get
        Set(ByVal value As Integer)
            mintJobid = value
        End Set
    End Property

    Private mblnIsoriginaldata As Boolean
    Public Property Isoriginaldata() As Boolean
        Get
            Return mblnIsoriginaldata
        End Get
        Set(ByVal value As Boolean)
            mblnIsoriginaldata = value
        End Set
    End Property
End Class
Public Class ClsJobviewJson
    Private mstrJobname As String
    Public Property Jobname() As String
        Get
            Return mstrJobname
        End Get
        Set(ByVal value As String)
            mstrJobname = value
        End Set
    End Property

    Private mintParent As Integer
    Public Property Parent() As Integer
        Get
            Return mintParent
        End Get
        Set(ByVal value As Integer)
            mintParent = value
        End Set
    End Property
    Private mintJobid As Integer
    Public Property Jobid() As Integer
        Get
            Return mintJobid
        End Get
        Set(ByVal value As Integer)
            mintJobid = value
        End Set
    End Property

    Private mstrPlannedPosition As String
    Public Property PlannedPosition() As String
        Get
            Return mstrPlannedPosition
        End Get
        Set(ByVal value As String)
            mstrPlannedPosition = value
        End Set
    End Property

    Private mstrHeadCount As String
    Public Property HeadCount() As String
        Get
            Return mstrHeadCount
        End Get
        Set(ByVal value As String)
            mstrHeadCount = value
        End Set
    End Property

    Private mstrPositionVariance As String
    Public Property PositionVariance() As String
        Get
            Return mstrPositionVariance
        End Get
        Set(ByVal value As String)
            mstrPositionVariance = value
        End Set
    End Property

    Private mblnIsoriginaldata As Boolean
    Public Property Isoriginaldata() As Boolean
        Get
            Return mblnIsoriginaldata
        End Get
        Set(ByVal value As Boolean)
            mblnIsoriginaldata = value
        End Set
    End Property

    Private Child As ICollection(Of ClsJobviewJson)
    Public Property children() As ICollection(Of ClsJobviewJson)
        Get
            Return Child
        End Get
        Set(ByVal value As ICollection(Of ClsJobviewJson))
            Child = value
        End Set
    End Property
End Class

Public Class ClsEmployee
    Private mstrEmployeename As String
    Public Property Employeename() As String
        Get
            Return mstrEmployeename
        End Get
        Set(ByVal value As String)
            mstrEmployeename = value
        End Set
    End Property

    Private mstrDepartment As String
    Public Property Department() As String
        Get
            Return mstrDepartment
        End Get
        Set(ByVal value As String)
            mstrDepartment = value
        End Set
    End Property

    Private mstrJobPosition As String
    Public Property JobPosition() As String
        Get
            Return mstrJobPosition
        End Get
        Set(ByVal value As String)
            mstrJobPosition = value
        End Set
    End Property

    Private mstrEmail As String
    Public Property Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = value
        End Set
    End Property

    Private mstrMobile As String
    Public Property Mobile() As String
        Get
            Return mstrMobile
        End Get
        Set(ByVal value As String)
            mstrMobile = value
        End Set
    End Property

    Private mintParent As Integer
    Public Property Parent() As Integer
        Get
            Return mintParent
        End Get
        Set(ByVal value As Integer)
            mintParent = value
        End Set
    End Property

    Private mintJobid As Integer
    Public Property Employeeid() As Integer
        Get
            Return mintJobid
        End Get
        Set(ByVal value As Integer)
            mintJobid = value
        End Set
    End Property

    Private mstrPhoto As String
    Public Property Photo() As String
        Get
            Return mstrPhoto
        End Get
        Set(ByVal value As String)
            mstrPhoto = value
        End Set
    End Property

    Private mstrPlannedPosition As String
    Public Property PlannedPosition() As String
        Get
            Return mstrPlannedPosition
        End Get
        Set(ByVal value As String)
            mstrPlannedPosition = value
        End Set
    End Property

    Private mstrHeadCount As String
    Public Property HeadCount() As String
        Get
            Return mstrHeadCount
        End Get
        Set(ByVal value As String)
            mstrHeadCount = value
        End Set
    End Property

    Private mstrPositionVariance As String
    Public Property PositionVariance() As String
        Get
            Return mstrPositionVariance
        End Get
        Set(ByVal value As String)
            mstrPositionVariance = value
        End Set
    End Property


    Private mblnIsoriginaldata As Boolean
    Public Property Isoriginaldata() As Boolean
        Get
            Return mblnIsoriginaldata
        End Get
        Set(ByVal value As Boolean)
            mblnIsoriginaldata = value
        End Set
    End Property
End Class
Public Class ClsEmployeeviewJson
    Private mstrEmployeename As String
    Public Property Employeename() As String
        Get
            Return mstrEmployeename
        End Get
        Set(ByVal value As String)
            mstrEmployeename = value
        End Set
    End Property

    Private mstrDepartment As String
    Public Property Department() As String
        Get
            Return mstrDepartment
        End Get
        Set(ByVal value As String)
            mstrDepartment = value
        End Set
    End Property

    Private mstrJobPosition As String
    Public Property JobPosition() As String
        Get
            Return mstrJobPosition
        End Get
        Set(ByVal value As String)
            mstrJobPosition = value
        End Set
    End Property

    Private mstrEmail As String
    Public Property Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = value
        End Set
    End Property

    Private mstrMobile As String
    Public Property Mobile() As String
        Get
            Return mstrMobile
        End Get
        Set(ByVal value As String)
            mstrMobile = value
        End Set
    End Property

    Private mintParent As Integer
    Public Property Parent() As Integer
        Get
            Return mintParent
        End Get
        Set(ByVal value As Integer)
            mintParent = value
        End Set
    End Property

    Private mintJobid As Integer
    Public Property Employeeid() As Integer
        Get
            Return mintJobid
        End Get
        Set(ByVal value As Integer)
            mintJobid = value
        End Set
    End Property

    Private mstrPhoto As String
    Public Property Photo() As String
        Get
            Return mstrPhoto
        End Get
        Set(ByVal value As String)
            mstrPhoto = value
        End Set
    End Property

    Private mblnIsoriginaldata As Boolean
    Public Property Isoriginaldata() As Boolean
        Get
            Return mblnIsoriginaldata
        End Get
        Set(ByVal value As Boolean)
            mblnIsoriginaldata = value
        End Set
    End Property

    Private mstrPlannedPosition As String
    Public Property PlannedPosition() As String
        Get
            Return mstrPlannedPosition
        End Get
        Set(ByVal value As String)
            mstrPlannedPosition = value
        End Set
    End Property

    Private mstrHeadCount As String
    Public Property HeadCount() As String
        Get
            Return mstrHeadCount
        End Get
        Set(ByVal value As String)
            mstrHeadCount = value
        End Set
    End Property

    Private mstrPositionVariance As String
    Public Property PositionVariance() As String
        Get
            Return mstrPositionVariance
        End Get
        Set(ByVal value As String)
            mstrPositionVariance = value
        End Set
    End Property



    Private Child As ICollection(Of ClsEmployeeviewJson)
    Public Property children() As ICollection(Of ClsEmployeeviewJson)
        Get
            Return Child
        End Get
        Set(ByVal value As ICollection(Of ClsEmployeeviewJson))
            Child = value
        End Set
    End Property

End Class

Public Class ClsFilterJson
    Private mstrFilterKey As String
    Public Property Filterkey() As String
        Get
            Return mstrFilterKey
        End Get
        Set(ByVal value As String)
            mstrFilterKey = value
        End Set
    End Property

    Private mstrFilterValue As String
    Public Property FilterValue() As String
        Get
            Return mstrFilterValue
        End Get
        Set(ByVal value As String)
            mstrFilterValue = value
        End Set
    End Property

End Class
