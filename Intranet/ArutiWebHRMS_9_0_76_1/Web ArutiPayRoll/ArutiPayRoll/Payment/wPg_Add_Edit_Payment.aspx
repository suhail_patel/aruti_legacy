<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_Add_Edit_Payment.aspx.vb"
    Inherits="Payroll_wPg_Add_Edit_Payment" Title="Untitled Page" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/CashDenomination.ascx" TagName="CashDenomination" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="txtNumeric" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/CommonValidationList.ascx" TagName="CommonValidationList" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/AnalysisBy.ascx" TagName="AnalysisBy" TagPrefix="uc5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    &nbsp;&nbsp;&nbsp;

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;
            var cval = document.getElementById('<%=txtAmount.ClientID%>').value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > 0)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;

            return true;
        }    
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Add/Edit Payment"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Payment Information"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="width: 11%">
                                                <asp:Label ID="lblPaidTo" runat="server" Text="Paid To"></asp:Label>
                                                <asp:Label ID="lblReceivedFrom" runat="server" Text="Recieved From" Visible="false"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblScheme" runat="server" Text="Scheme"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtScheme" runat="server" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td style="width: 11%">
                                            </td>
                                            <td style="width: 22%">
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 11%">
                                                <asp:Label ID="lblPaymentOf" runat="server" Text="Payment Of"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtPaymentOf" runat="server" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblVoucher" runat="server" Text="Voucher"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtVoucherNo" runat="server" ReadOnly="True"></asp:TextBox>
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblPayYear" runat="server" Text="Pay Year" Visible="false"></asp:Label>
                                                <asp:Label ID="lblPayPeriod" runat="server" Text="Pay Period"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtPayYear" runat="server" ReadOnly="True" Visible="false"></asp:TextBox>
                                                <asp:TextBox ID="txtPayPeriod" runat="server" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 11%">
                                                <asp:Label ID="lblPayment" runat="server" Text="Payment" Visible="false"></asp:Label>
                                                <asp:Label ID="lblLoanAmount" runat="server" Text="Loan Amount" Visible="false"></asp:Label>
                                                <asp:Label ID="lblBalanceDue" runat="server" Text="Due Balance" Visible="false"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtBalanceDue" runat="server" ReadOnly="True" Style="text-align: right;"></asp:TextBox>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="false">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblPaymentDate" runat="server" Text="Date"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <uc2:DateCtrl ID="dtpPaymentDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td colspan="6" style="width: 100%">
                                                <div style="border-bottom: 2px solid #DDD; margin-top: 5px; margin-bottom: 5px">
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 11%">
                                                <asp:Label ID="lblAgainstVoucher" runat="server" Text="Voucher#"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtAgainstVoucher" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblPaymentMode" runat="server" Text="Payment Mode"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboPaymentMode" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblCurrency" runat="server" Text="Currency"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;">
                                            <td style="width: 11%">
                                                <asp:Label ID="lblBankGroup" runat="server" Text="Bank Group"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboBankGroup" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboBranch" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 11%">
                                            </td>
                                            <td style="width: 22%">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblAccountNo" runat="server" Text="Account No."></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboAccountNo" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblCheque" runat="server" Text="Cheque No."></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtChequeNo" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblPaymentBy" runat="server" Text="Payment By"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboPaymentBy" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 11%" valign="top">
                                                <asp:Label ID="lblRemarks" runat="server" Text="Remarks"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                            <td style="width: 12%">
                                                <asp:Label ID="lblValue" runat="server" Text="Percent%"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <uc9:txtNumeric ID="txtPercent" runat="server" style="text-align: right;" AutoPostBack="true" />
                                            </td>
                                            <td style="width: 11%">
                                                <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label>
                                            </td>
                                            <td style="width: 22%">
                                                <uc9:txtNumeric ID="txtAmount" runat="server" style="text-align: right;" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <div style="float: left;">
                                            <asp:Label ID="objlblExRate" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="objlblMessage" runat="server" Text="" Style="margin-left: 10px"></asp:Label>
                                        </div>
                                        <div style="text-align: right;">
                                            <asp:Button ID="btnSave" runat="server" CssClass="btndefault" Text="Save" />
                                            <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc1:CashDenomination ID="popupCashDenomination" runat="server" />
                    <uc4:CommonValidationList ID="popupValidationList" runat="server" Message="" ShowYesNo="false" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
