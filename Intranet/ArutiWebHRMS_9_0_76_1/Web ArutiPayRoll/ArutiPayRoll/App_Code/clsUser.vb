﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports Aruti.Data

Public Class User
    Private UserUnkID As Integer
    Dim msql As String
    Dim clsDataOperation As New eZeeCommonLib.clsDataOperation(True)
    Dim type As String
    'Public Sub New(ByVal pusername As String, ByVal password As String, ByVal logintype As en_loginby, ByVal selecteddatabase As String)
    '    Try

    '        ' Me.SelectedDatabase = selecteddatabase + ".."
    '        Me.SelectedDatabase = selecteddatabase
    '        If CheckLogin(pusername, password, logintype, selecteddatabase) = False Then
    '            Me.UserID = -1
    '            Me.Employeeunkid = -1
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception("Erroe in clsDataOperation-->New method")
    '    End Try
    'End Sub



    'Pinkal (12-Feb-2015) -- Start
    'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.

    'Public Sub New(ByVal username_EmailId As String, ByVal password As String, ByVal type As en_loginby, ByVal selecteddatabase As String, Optional ByVal intUserUnkid As Integer = -1)
    '    Try
    '        Me.SelectedDatabase = selecteddatabase
    '        If CheckLogin(username_EmailId, password, type, selecteddatabase, intUserUnkid) = False Then
    '            Me.UserID = -1
    '            Me.Employeeunkid = -1
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception("ClsDataOperation-->New method" & ex.Message.ToString)
    '    End Try
    'End Sub

    Public Sub New(ByVal username_EmailId As String, ByVal password As String, ByVal selecteddatabase As String, Optional ByVal intUserUnkid As Integer = -1)
        Try
            Me.SelectedDatabase = selecteddatabase
            If CheckLogin(username_EmailId, password, selecteddatabase, intUserUnkid) = False Then
                Me.UserID = -1
                Me.Employeeunkid = -1
            End If
        Catch ex As Exception
            Throw New Exception("ClsDataOperation-->New method" & ex.Message.ToString)
        End Try
    End Sub

    'Pinkal (12-Feb-2015) -- End

    Public Sub New()

    End Sub

    Private pEmployeeunkid As Integer
    Public Property Employeeunkid() As Integer
        Get
            Return pEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            pEmployeeunkid = value
        End Set
    End Property
    Public Property UserID() As Integer
        Get
            Return UserUnkID
        End Get
        Set(ByVal value As Integer)
            UserUnkID = value
        End Set
    End Property
    Private PUsername As String
    Public Property UserName() As String
        Get
            Return PUsername
        End Get
        Set(ByVal value As String)
            PUsername = value
        End Set
    End Property

    'Sohail (31 May 2018) -- Start
    'TANAPA Enhancement - Ref # 247 : Show Online Recruitment Internal Vacancy link in ESS in 72.1.
    Private PEmployeeCode As String
    Public Property EmployeeCode() As String
        Get
            Return PEmployeeCode
        End Get
        Set(ByVal value As String)
            PEmployeeCode = value
        End Set
    End Property
    'Sohail (31 May 2018) -- End


    Private Ppassword As String
    Public Property password() As String
        Get
            Return Ppassword
        End Get
        Set(ByVal value As String)
            Ppassword = value
        End Set
    End Property
    Private PselectedDatabase As String
    Public Property SelectedDatabase() As String
        Get
            Return PselectedDatabase
        End Get
        Set(ByVal value As String)
            PselectedDatabase = value
        End Set
    End Property

    Public Enum en_loginby
        User
        Employee
    End Enum

    Private Ploginby As String
    Public Property LoginBy() As en_loginby
        Get
            Return Ploginby
        End Get
        Set(ByVal value As en_loginby)
            Ploginby = value
            If Ploginby = en_loginby.User Then
                type = "User"
            ElseIf Ploginby = en_loginby.Employee Then
                type = "Employee"
            End If
        End Set
    End Property
    Private pFirstName As String
    Public Property Firstname() As String
        Get
            Return pFirstName
        End Get
        Set(ByVal value As String)
            value = pFirstName
        End Set
    End Property
    Private pSurName As String
    Public Property Surname() As String
        Get
            Return pSurName
        End Get
        Set(ByVal value As String)
            value = pSurName
        End Set
    End Property

    'Sohail (30 Mar 2015) -- Start
    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    Private pDisplayName As String
    Public Property DisplayName() As String
        Get
            Return pDisplayName
        End Get
        Set(ByVal value As String)
            value = pDisplayName
        End Set
    End Property
    'Sohail (30 Mar 2015) -- End

    Private Pappointeddate As Date
    Public Property appointeddate() As Date
        Get
            Return Pappointeddate
        End Get
        Set(ByVal value As Date)
            value = Pappointeddate
        End Set
    End Property
    Private PGender As String
    Public Property gender() As String
        Get
            Return PGender
        End Get
        Set(ByVal value As String)
            value = PGender
        End Set
    End Property
    Private pbirthdate As Date
    Public Property birthdate() As Date
        Get
            Return pbirthdate
        End Get
        Set(ByVal value As Date)
            value = pbirthdate
        End Set
    End Property
    Private pEmail As String
    Public Property Email() As String
        Get
            Return pEmail
        End Get
        Set(ByVal value As String)
            value = pEmail
        End Set
    End Property
    Private ppresent_address1 As String
    Public Property present_address1() As String
        Get
            Return ppresent_address1
        End Get
        Set(ByVal value As String)
            value = ppresent_address1
        End Set
    End Property
    Private ppresent_address2 As String
    Public Property present_address2() As String
        Get
            Return ppresent_address2
        End Get
        Set(ByVal value As String)
            value = ppresent_address2
        End Set
    End Property
    Private ppresent_mobile As String
    Public Property present_mobile() As String
        Get
            Return ppresent_mobile
        End Get
        Set(ByVal value As String)
            value = ppresent_mobile
        End Set
    End Property
    Private pshiftunkid As Integer
    Public Property shiftunkid() As Integer
        Get
            Return pshiftunkid
        End Get
        Set(ByVal value As Integer)
            value = pshiftunkid
        End Set
    End Property

    Private pdepartmentunkid As Integer
    Public Property departmentunkid() As Integer
        Get
            Return pdepartmentunkid
        End Get
        Set(ByVal value As Integer)
            value = pdepartmentunkid
        End Set
    End Property
    Private pLeaveBalances As DataTable
    Public Property LeaveBalances() As DataTable
        Get
            Return pLeaveBalances
        End Get
        Set(ByVal value As DataTable)
            value = pLeaveBalances
        End Set
    End Property
    Private pStartDate As Date
    Public Property startdate() As Date
        Get
            Return pStartDate
        End Get
        Set(ByVal value As Date)
            value = pStartDate
        End Set
    End Property
    Private pEndDate As Date
    Public Property Enddate() As Date
        Get
            Return pEndDate
        End Get
        Set(ByVal value As Date)
            value = pEndDate
        End Set
    End Property
    Private pShiftName As String
    Public Property ShiftName() As String
        Get
            Return pShiftName
        End Get
        Set(ByVal value As String)
            pShiftName = value
        End Set
    End Property
    Private pDepartmentName As String
    Public Property Department() As String
        Get
            Return pDepartmentName
        End Get
        Set(ByVal value As String)
            pDepartmentName = value
        End Set
    End Property

    'Sohail (04 Jun 2012) -- Start
    'TRA - ENHANCEMENT - View Online Memberes
    Private intRoleUnkID As Integer
    Public Property RoleUnkID() As Integer
        Get
            Return intRoleUnkID
        End Get
        Set(ByVal value As Integer)
            intRoleUnkID = value
        End Set
    End Property

    Private strMemberName As String
    Public Property MemberName() As String
        Get
            Return strMemberName
        End Get
        Set(ByVal value As String)
            strMemberName = value
        End Set
    End Property
    'Sohail (04 Jun 2012) -- End

    'S.SANDEEP [ 22 JULY 2011 ] -- START
    'ENHANCEMENT : WEB PRIVENT LOGING OF INACTIVE & TERMINATED EMPLOYEE
    Private mstrMessage As String = ""
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property
    'S.SANDEEP [ 22 JULY 2011 ] -- END 

    'S.SANDEEP [ 20 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLangId As Integer = 0
    Public ReadOnly Property LanguageUnkid() As Integer
        Get
            Return mintLangId
        End Get
    End Property
    'S.SANDEEP [ 20 NOV 2012 ] -- END

    'Sohail (24 Nov 2016) -- Start
    'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
    Private pTheme_Id As Integer
    Public Property Theme_Id() As Integer
        Get
            Return pTheme_Id
        End Get
        Set(ByVal value As Integer)
            value = pTheme_Id
        End Set
    End Property

    Private pLastView_Id As Integer
    Public Property LastView_Id() As Integer
        Get
            Return pLastView_Id
        End Get
        Set(ByVal value As Integer)
            value = pLastView_Id
        End Set
    End Property
    'Sohail (24 Nov 2016) -- End


    'Pinkal (12-Feb-2015) -- Start
    'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.

    'Private Function CheckLogin(ByVal pusername As String, ByVal password As String, ByVal logintype As en_loginby, ByVal selecteddatabase As String, Optional ByVal intUserId As Integer = -1) As String
    '    'S.SANDEEP [ 30 May 2011 ] -- END
    '    Dim ds As DataSet
    '    'S.SANDEEP [ 30 May 2011 ] -- START
    '    Dim objUser As New Aruti.Data.clsUserAddEdit
    '    Dim objEmp As New Aruti.Data.clsEmployee_Master
    '    'S.SANDEEP [ 30 May 2011 ] -- END
    '    Try
    '        'S.SANDEEP [ 06 DEC 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES

    '        'If logintype = en_loginby.Employee Then
    '        '    'Sohail (23 Apr 2012) -- Start
    '        '    'TRA - ENHANCEMENT
    '        '    'eZeeCommonLib.eZeeDatabase.change_database(Me.SelectedDatabase)
    '        '    HttpContext.Current.Session("mdbname") = Me.SelectedDatabase
    '        '    'Sohail (23 Apr 2012) -- End

    '        '    objEmp._Employeeunkid = intUserId
    '        '    'Sohail (12 Jul 2012) -- Start
    '        '    'TRA - ENHANCEMENT - Error occurs while " ' " (single quote) is given in username or password.
    '        '    'msql = "select    employeeunkid, password from " & Me.SelectedDatabase & "..hremployee_master where Displayname ='" & pusername & "'"
    '        '    msql = "select    employeeunkid, password from " & Me.SelectedDatabase & "..hremployee_master where Displayname ='" & pusername.Replace("'", "''") & "'"
    '        '    'Sohail (12 Jul 2012) -- End
    '        '    ds = clsDataOperation.WExecQuery(msql, "hremployee_master")

    '        '    If ds.Tables(0).Rows.Count > 0 Then
    '        '        objEmp._Employeeunkid = ds.Tables(0).Rows(0)("employeeunkid")


    '        '        'S.SANDEEP [ 22 JULY 2011 ] -- START
    '        '        'ENHANCEMENT : WEB PRIVENT LOGING OF INACTIVE & TERMINATED EMPLOYEE
    '        '        'If password <> objEmp._Password Then
    '        '        '    Return False
    '        '        'End If

    '        '        'If objEmp._Isactive = False Then
    '        '        If objEmp._Isapproved = False Then
    '        '            mstrMessage = "Sorry, You cannot loging to the system. Reason : You are not approved."
    '        '            Return False
    '        '        ElseIf objEmp._Termination_From_Date.Date <> Nothing Then
    '        '            If objEmp._Termination_From_Date.Date <= Aruti.Data.ConfigParameter._Object._CurrentDateAndTime.Date Then
    '        '                mstrMessage = "Sorry, You cannot loging to the system. Reason : Please contact System Administrator."
    '        '                Return False
    '        '            End If
    '        '        End If

    '        '        If password <> objEmp._Password Then
    '        '            'Sohail (12 Jul 2012) -- Start
    '        '            'TRA - ENHANCEMENT - Do not show seperate message for invalid username and password. Show common message.
    '        '            'mstrMessage = "Invalid Password!! Please try again !!!"
    '        '            mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '        '            'Sohail (12 Jul 2012) -- End
    '        '            Return False
    '        '        End If

    '        '        If pusername.ToUpper <> objEmp._Displayname.ToUpper Then
    '        '            'Sohail (12 Jul 2012) -- Start
    '        '            'TRA - ENHANCEMENT - Do not show seperate message for invalid username and password. Show common message.
    '        '            'mstrMessage = "Invalid Username!! Please try again !!!"
    '        '            mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '        '            'Sohail (12 Jul 2012) -- End
    '        '            Return False
    '        '        End If
    '        '        'S.SANDEEP [ 22 JULY 2011 ] -- END   
    '        '    Else
    '        '        'Sohail (12 Jul 2012) -- Start
    '        '        'TRA - ENHANCEMENT - Do not show seperate message for invalid username and password. Show common message.
    '        '        'mstrMessage = "Invalid Username!! Please try again !!!"
    '        '        mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '        '        'Sohail (12 Jul 2012) -- End
    '        '        Return False
    '        '    End If


    '        'Else

    '        '    'S.SANDEEP [ 30 May 2011 ] -- START
    '        '    'msql = "select convert(int,userunkid) as  userunkid,convert(int,-1) as employeeunkid, username as displayname,password from hrmsConfiguration..cfuser_master where username ='" & pusername & "' and password='" & password & "'"
    '        '    'Sohail (12 Jul 2012) -- Start
    '        '    'TRA - ENHANCEMENT - Error occurs while " ' " (single quote) is given in username or password.
    '        '    'msql = "select convert(int,userunkid) as  userunkid,convert(int,-1) as employeeunkid, username as displayname,password from hrmsConfiguration..cfuser_master where username ='" & pusername & "'"
    '        '    msql = "select convert(int,userunkid) as  userunkid,convert(int,-1) as employeeunkid, username as displayname,password from hrmsConfiguration..cfuser_master where username ='" & pusername.Replace("'", "''") & "'"
    '        '    'Sohail (12 Jul 2012) -- End
    '        '    ds = clsDataOperation.WExecQuery(msql, "cfuser_master")
    '        '    If ds.Tables(0).Rows.Count > 0 Then
    '        '        objUser._Userunkid = ds.Tables(0).Rows(0)("Userunkid")
    '        '    Else
    '        '        'Sohail (12 Jul 2012) -- Start
    '        '        'TRA - ENHANCEMENT - Do not show seperate message for invalid username and password. Show common message.
    '        '        'mstrMessage = "Invalid Username!! Please try again !!!"
    '        '        mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '        '        'Sohail (12 Jul 2012) -- End
    '        '        Return False
    '        '    End If

    '        '    ' objUser._Userunkid = intUserId

    '        '    If password <> objUser._Password Then
    '        '        'Sohail (12 Jul 2012) -- Start
    '        '        'TRA - ENHANCEMENT - Do not show seperate message for invalid username and password. Show common message.
    '        '        'mstrMessage = "Invalid Password!! Please try again !!!"
    '        '        mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '        '        'Sohail (12 Jul 2012) -- End
    '        '        Return False
    '        '    End If

    '        '    If pusername.ToUpper <> objUser._Username.ToUpper Then
    '        '        'Sohail (12 Jul 2012) -- Start
    '        '        'TRA - ENHANCEMENT - Do not show seperate message for invalid username and password. Show common message.
    '        '        'mstrMessage = "Invalid Username!! Please try again !!!"
    '        '        mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '        '        'Sohail (12 Jul 2012) -- End
    '        '        Return False
    '        '    End If
    '        '    'S.SANDEEP [ 30 May 2011 ] -- END 

    '        'End If
    '        'If logintype = en_loginby.Employee Then


    '        '    Me.UserID = -1
    '        '    Me.Employeeunkid = objEmp._Employeeunkid
    '        '    Me.password = objEmp._Password
    '        '    Me.UserName = "Code " & " : " & objEmp._Employeecode & vbCrLf & "Employee : " & objEmp._Firstname & " " & objEmp._Surname 'objEmp._Displayname
    '        '    Me.pFirstName = objEmp._Firstname
    '        '    Me.pSurName = objEmp._Surname
    '        '    Me.MemberName = "Emp. : (" & objEmp._Employeecode & ") " & objEmp._Firstname & " " & objEmp._Surname 'Sohail (04 Jun 2012)
    '        '    Me.Pappointeddate = objEmp._Appointeddate
    '        '    Select Case objEmp._Gender
    '        '        Case 1  'Male
    '        '            Me.PGender = "Male"
    '        '        Case 2 'Female
    '        '            Me.PGender = "Female"
    '        '        Case Else
    '        '            Me.PGender = ""
    '        '    End Select
    '        '    Me.pbirthdate = objEmp._Birthdate.Date
    '        '    Me.pEmail = objEmp._Email
    '        '    Me.ppresent_address1 = objEmp._Present_Address1
    '        '    Me.ppresent_address2 = objEmp._Present_Address2
    '        '    Me.ppresent_mobile = objEmp._Present_Mobile

    '        '    Dim objShift As New Aruti.Data.clsshift_master
    '        '    objShift._Shiftunkid = objEmp._Shiftunkid
    '        '    Me.pShiftName = objShift._Shiftname
    '        '    objShift = Nothing

    '        '    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    '        '    'ENHANCEMENT : TRA CHANGES
    '        '    'Call GetLeaveBalance(objEmp._Employeeunkid)
    '        '    'S.SANDEEP [ 05 MARCH 2012 ] -- END

    '        '    Dim objDept As New Aruti.Data.clsDepartment
    '        '    objDept._Departmentunkid = objEmp._Departmentunkid
    '        '    Me.pDepartmentName = objDept._Name
    '        '    objDept = Nothing


    '        '    Return True
    '        'Else


    '        '    Me.UserID = objUser._Userunkid
    '        '    Me.Employeeunkid = -1
    '        '    Me.password = objUser._Password
    '        '    Me.UserName = objUser._Username
    '        '    Me.MemberName = objUser._Username 'Sohail (04 Jun 2012)
    '        '    Me.RoleUnkID = objUser._Roleunkid 'Sohail (04 Jun 2012)
    '        '    'S.SANDEEP [ 20 NOV 2012 ] -- START
    '        '    'ENHANCEMENT : TRA CHANGES
    '        '    Me.mintLangId = objUser._Languageunkid
    '        '    'S.SANDEEP [ 20 NOV 2012 ] -- END


    '        '    'Pinkal (09-Nov-2012) -- Start
    '        '    'Enhancement : TRA Changes
    '        '    Me.pFirstName = objUser._Firstname
    '        '    Me.pSurName = objUser._Lastname
    '        '    'Pinkal (09-Nov-2012) -- End


    '        '    Return True

    '        'End If

    '        If HttpContext.Current.Session("IsEmployeeAsUser") = True Then

    '            msql = "SELECT CONVERT(int,userunkid) as userunkid,CONVERT(int,-1) as employeeunkid, username as displayname,password,employeeunkid AS EId,companyunkid AS CoId FROM hrmsConfiguration..cfuser_master WHERE username ='" & pusername.Replace("'", "''") & "' AND isactive = 1 AND (ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) > 0 OR hrmsConfiguration..cfuser_master.userunkid = 1) "
    '            ds = clsDataOperation.WExecQuery(msql, "List")
    '            If ds.Tables(0).Rows.Count > 0 Then
    '                objUser._Userunkid = ds.Tables(0).Rows(0)("Userunkid")
    '            Else
    '                mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '                Return False
    '            End If

    '            If password <> objUser._Password Then
    '                mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '                Return False
    '            End If

    '            If pusername.ToUpper <> objUser._Username.ToUpper Then
    '                mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '                Return False
    '            End If

    '            If logintype = en_loginby.Employee Then
    '                If HttpContext.Current.Session("CompanyUnkId") <> CInt(ds.Tables(0).Rows(0).Item("CoId")) Then
    '                    mstrMessage = "Sorry, No such employee exists in the selected company."
    '                    Return False
    '                End If
    '                Me.UserID = -1
    '                objEmp._Employeeunkid = ds.Tables(0).Rows(0).Item("EId")

    '                'S.SANDEEP [ 17 APR 2013 ] -- START
    '                'ENHANCEMENT : TRA CHANGES
    '                If objEmp._Isapproved = False Then
    '                    mstrMessage = "Sorry, You cannot loging to the system. Reason : You are not approved."
    '                    Return False
    '                End If

    '                If objEmp._Reinstatementdate = Nothing Then
    '                    If objEmp._Suspende_To_Date <> Nothing AndAlso objEmp._Suspende_From_Date <> Nothing Then
    '                        If ConfigParameter._Object._CurrentDateAndTime.Date <= objEmp._Suspende_To_Date.Date And _
    '                               ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Suspende_From_Date.Date Then
    '                            mstrMessage = "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator."
    '                            Return False
    '                        End If

    '                        'Anjan [28 May 2014] -- Start
    '                        'ENHANCEMENT : Requested by Rutta/Andrew as at TRA and AKFTZ employees were not able to login on probabted period.
    '                        'ElseIf objEmp._Probation_From_Date <> Nothing AndAlso objEmp._Probation_To_Date <> Nothing Then
    '                        '    If ConfigParameter._Object._CurrentDateAndTime.Date <= objEmp._Probation_To_Date.Date And _
    '                        '       ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Probation_From_Date.Date Then
    '                        '        mstrMessage = "Sorry, you cannot login. Reason : You are in probation period. Please contact Administrator."
    '                        '        Return False
    '                        '    End If
    '                        'Anjan [28 May 2014 ] -- End
    '                    ElseIf objEmp._Empl_Enddate <> Nothing Then
    '                        If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Empl_Enddate.Date Then
    '                            mstrMessage = "Sorry, you cannot login. Reason : Your Contract has been expired. Please contact Administrator."
    '                            Return False
    '                        End If
    '                    ElseIf objEmp._Termination_From_Date <> Nothing Then
    '                        If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_From_Date.Date Then
    '                            mstrMessage = "Sorry, you cannot login. Reason : You are terminated. Please contact Administrator."
    '                            Return False
    '                        End If
    '                    ElseIf objEmp._Termination_To_Date <> Nothing Then
    '                        If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_To_Date.Date Then
    '                            mstrMessage = "Sorry, you cannot login. Reason : You are retired. Please contact Administrator."
    '                            Return False
    '                        End If
    '                    End If
    '                End If
    '                'S.SANDEEP [ 17 APR 2013 ] -- END

    '                Me.Employeeunkid = objEmp._Employeeunkid
    '                Me.password = objEmp._Password
    '                Me.UserName = "Code " & " : " & objEmp._Employeecode & vbCrLf & "Employee : " & objEmp._Firstname & " " & objEmp._Surname 'objEmp._Displayname
    '                Me.pFirstName = objEmp._Firstname
    '                Me.pSurName = objEmp._Surname
    '                Me.MemberName = "Emp. : (" & objEmp._Employeecode & ") " & objEmp._Firstname & " " & objEmp._Surname 'Sohail (04 Jun 2012)
    '                Me.Pappointeddate = objEmp._Appointeddate
    '                Select Case objEmp._Gender
    '                    Case 1  'Male
    '                        Me.PGender = "Male"
    '                    Case 2 'Female
    '                        Me.PGender = "Female"
    '                    Case Else
    '                        Me.PGender = ""
    '                End Select
    '                Me.pbirthdate = objEmp._Birthdate.Date
    '                Me.pEmail = objEmp._Email
    '                Me.ppresent_address1 = objEmp._Present_Address1
    '                Me.ppresent_address2 = objEmp._Present_Address2
    '                Me.ppresent_mobile = objEmp._Present_Mobile

    '                Dim objShift As New Aruti.Data.clsNewshift_master
    '                objShift._Shiftunkid = objEmp._Shiftunkid
    '                Me.pShiftName = objShift._Shiftname
    '                objShift = Nothing

    '                Dim objDept As New Aruti.Data.clsDepartment
    '                objDept._Departmentunkid = objEmp._Departmentunkid
    '                Me.pDepartmentName = objDept._Name
    '                objDept = Nothing
    '                Return True
    '            ElseIf logintype = en_loginby.User Then
    '                'Sohail [16 May 2014] -- Start
    '                'ENHANCEMENT : New login flow
    '                'If IsAccessGiven(objUser._Userunkid, HttpContext.Current.Session("Fin_year")) = False Then
    '                '    mstrMessage = "Sorry, You cannot login to system. Reason : Access to selected company is not given to you. Please contact Administrator."
    '                '    Return False
    '                'End If
    '                'Sohail [16 May 2014] -- End
    '                Me.UserID = objUser._Userunkid
    '                Me.Employeeunkid = -1
    '                Me.password = objUser._Password
    '                Me.UserName = objUser._Username
    '                Me.MemberName = objUser._Username
    '                Me.RoleUnkID = objUser._Roleunkid
    '                Me.mintLangId = objUser._Languageunkid
    '                Me.pFirstName = objUser._Firstname
    '                Me.pSurName = objUser._Lastname
    '                Return True
    '            End If
    '        Else
    'If logintype = en_loginby.Employee Then
    '    HttpContext.Current.Session("mdbname") = Me.SelectedDatabase
    '    objEmp._Employeeunkid = intUserId
    '    msql = "select    employeeunkid, password from " & Me.SelectedDatabase & "..hremployee_master where Displayname ='" & pusername.Replace("'", "''") & "'"
    '    ds = clsDataOperation.WExecQuery(msql, "hremployee_master")

    '    If ds.Tables(0).Rows.Count > 0 Then
    '        objEmp._Employeeunkid = ds.Tables(0).Rows(0)("employeeunkid")

    '        If objEmp._Isapproved = False Then
    '            mstrMessage = "Sorry, You cannot loging to the system. Reason : You are not approved."
    '            Return False
    '                    End If

    '                    If objEmp._Reinstatementdate = Nothing Then
    '                        If objEmp._Suspende_To_Date <> Nothing AndAlso objEmp._Suspende_From_Date <> Nothing Then
    '                            If ConfigParameter._Object._CurrentDateAndTime.Date <= objEmp._Suspende_To_Date.Date And _
    '                                   ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Suspende_From_Date.Date Then
    '                                mstrMessage = "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator."
    '                Return False
    '            End If
    '                        ElseIf objEmp._Probation_From_Date <> Nothing AndAlso objEmp._Probation_To_Date <> Nothing Then
    '                            If ConfigParameter._Object._CurrentDateAndTime.Date <= objEmp._Probation_To_Date.Date And _
    '                               ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Probation_From_Date.Date Then
    '                                mstrMessage = "Sorry, you cannot login. Reason : You are in probation period. Please contact Administrator."
    '                                Return False
    '                            End If
    '                        ElseIf objEmp._Empl_Enddate <> Nothing Then
    '                            If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Empl_Enddate.Date Then
    '                                mstrMessage = "Sorry, you cannot login. Reason : Your Contract has been expired. Please contact Administrator."
    '                                Return False
    '                            End If
    '                        ElseIf objEmp._Termination_From_Date <> Nothing Then
    '                            If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_From_Date.Date Then
    '                                mstrMessage = "Sorry, you cannot login. Reason : You are terminated. Please contact Administrator."
    '                                Return False
    '                            End If
    '                        ElseIf objEmp._Termination_To_Date <> Nothing Then
    '                            If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_To_Date.Date Then
    '                                mstrMessage = "Sorry, you cannot login. Reason : You are retired. Please contact Administrator."
    '                                Return False
    '                            End If
    '                        End If
    '        End If

    '        If password <> objEmp._Password Then
    '            mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '            Return False
    '        End If

    '        If pusername.ToUpper <> objEmp._Displayname.ToUpper Then
    '            mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '            Return False
    '        End If
    '    Else
    '        mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '        Return False
    '    End If
    'Else
    '                msql = "select convert(int,userunkid) as  userunkid,convert(int,-1) as employeeunkid, username as displayname,password from hrmsConfiguration..cfuser_master where username ='" & pusername.Replace("'", "''") & "'  AND isactive = 1 "
    '    ds = clsDataOperation.WExecQuery(msql, "cfuser_master")
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        objUser._Userunkid = ds.Tables(0).Rows(0)("Userunkid")
    '    Else
    '        mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '        Return False
    '    End If

    '    If password <> objUser._Password Then
    '        mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '        Return False
    '    End If

    '    If pusername.ToUpper <> objUser._Username.ToUpper Then
    '        mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '        Return False
    '    End If
    'End If
    'If logintype = en_loginby.Employee Then
    '    Me.UserID = -1
    '    Me.Employeeunkid = objEmp._Employeeunkid
    '    Me.password = objEmp._Password
    '    Me.UserName = "Code " & " : " & objEmp._Employeecode & vbCrLf & "Employee : " & objEmp._Firstname & " " & objEmp._Surname 'objEmp._Displayname
    '    Me.pFirstName = objEmp._Firstname
    '    Me.pSurName = objEmp._Surname
    '    Me.MemberName = "Emp. : (" & objEmp._Employeecode & ") " & objEmp._Firstname & " " & objEmp._Surname 'Sohail (04 Jun 2012)
    '    Me.Pappointeddate = objEmp._Appointeddate
    '    Select Case objEmp._Gender
    '        Case 1  'Male
    '            Me.PGender = "Male"
    '        Case 2 'Female
    '            Me.PGender = "Female"
    '        Case Else
    '            Me.PGender = ""
    '    End Select
    '    Me.pbirthdate = objEmp._Birthdate.Date
    '    Me.pEmail = objEmp._Email
    '    Me.ppresent_address1 = objEmp._Present_Address1
    '    Me.ppresent_address2 = objEmp._Present_Address2
    '    Me.ppresent_mobile = objEmp._Present_Mobile

    '                Dim objShift As New Aruti.Data.clsNewshift_master
    '    objShift._Shiftunkid = objEmp._Shiftunkid
    '    Me.pShiftName = objShift._Shiftname
    '    objShift = Nothing

    '    Dim objDept As New Aruti.Data.clsDepartment
    '    objDept._Departmentunkid = objEmp._Departmentunkid
    '    Me.pDepartmentName = objDept._Name
    '    objDept = Nothing
    '    Return True
    'Else
    '    Me.UserID = objUser._Userunkid
    '    Me.Employeeunkid = -1
    '    Me.password = objUser._Password
    '    Me.UserName = objUser._Username
    '                Me.MemberName = objUser._Username
    '                Me.RoleUnkID = objUser._Roleunkid
    '    Me.mintLangId = objUser._Languageunkid
    '    Me.pFirstName = objUser._Firstname
    '    Me.pSurName = objUser._Lastname
    '                Return True
    '            End If
    '        End If
    '        'S.SANDEEP [ 06 DEC 2012 ] -- END
    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception("ClsDataOperation-->CheckLogin function" & ex.Message)
    '    End Try

    'End Function


    'Sohail (30 Mar 2015) -- Start
    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    '    Private Function CheckLogin(ByVal pusername As String, ByVal password As String, ByVal selecteddatabase As String, Optional ByVal intUserId As Integer = -1) As String
    '        Dim ds As DataSet
    '        Dim objUser As New Aruti.Data.clsUserAddEdit
    '        Dim objEmp As New Aruti.Data.clsEmployee_Master
    '        Try
    '            If HttpContext.Current.Session("IsEmployeeAsUser") = True Then

    '                msql = "SELECT CONVERT(int,userunkid) as userunkid,CONVERT(int,-1) as employeeunkid, username as displayname,password,employeeunkid AS EId,companyunkid AS CoId FROM hrmsConfiguration..cfuser_master WHERE username ='" & pusername.Replace("'", "''") & "' AND password = '" & clsSecurity.Encrypt(password, "ezee") & "' AND isactive = 1 AND (ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) > 0 OR hrmsConfiguration..cfuser_master.userunkid = 1) "
    '                ds = clsDataOperation.WExecQuery(msql, "List")

    'UserChecking:

    '                If ds.Tables(0).Rows.Count > 0 Then
    '                    objUser._Userunkid = ds.Tables(0).Rows(0)("Userunkid")

    '                    If password <> objUser._Password Then
    '                        mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '                        Return False
    '                    End If

    '                    If pusername.ToUpper <> objUser._Username.ToUpper Then
    '                        mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '                        Return False
    '                    End If


    '                    If objUser._IsManager = False Then   ' FOR EMPLOYEE LOGIN

    '                        If HttpContext.Current.Session("IsEmployeeAsUser") = True Then
    '                            If HttpContext.Current.Session("CompanyUnkId") <> CInt(ds.Tables(0).Rows(0).Item("CoId")) Then
    '                                mstrMessage = "Sorry, No such employee exists in the selected company."
    '                                Return False
    '                            End If

    '                            Me.UserID = -1
    '                            objEmp._Employeeunkid = ds.Tables(0).Rows(0).Item("EId")

    '                            If objEmp._Isapproved = False Then
    '                                mstrMessage = "Sorry, You cannot loging to the system. Reason : You are not approved."
    '                                Return False
    '                            End If

    '                            If objEmp._Reinstatementdate = Nothing Then
    '                                If objEmp._Suspende_To_Date <> Nothing AndAlso objEmp._Suspende_From_Date <> Nothing Then
    '                                    If ConfigParameter._Object._CurrentDateAndTime.Date <= objEmp._Suspende_To_Date.Date And _
    '                                           ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Suspende_From_Date.Date Then
    '                                        mstrMessage = "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator."
    '                                        Return False
    '                                    End If

    '                                ElseIf objEmp._Empl_Enddate <> Nothing Then
    '                                    If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Empl_Enddate.Date Then
    '                                        mstrMessage = "Sorry, you cannot login. Reason : Your Contract has been expired. Please contact Administrator."
    '                                        Return False
    '                                    End If
    '                                ElseIf objEmp._Termination_From_Date <> Nothing Then
    '                                    If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_From_Date.Date Then
    '                                        mstrMessage = "Sorry, you cannot login. Reason : You are terminated. Please contact Administrator."
    '                                        Return False
    '                                    End If
    '                                ElseIf objEmp._Termination_To_Date <> Nothing Then
    '                                    If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_To_Date.Date Then
    '                                        mstrMessage = "Sorry, you cannot login. Reason : You are retired. Please contact Administrator."
    '                                        Return False
    '                                    End If
    '                                End If
    '                            End If

    '                            Me.LoginBy = en_loginby.Employee
    '                            Me.Employeeunkid = objEmp._Employeeunkid
    '                            Me.password = objEmp._Password

    '                            'SHANI [09 Mar 2015]-START
    '                            'Enhancement - REDESIGN SELF SERVICE.
    '                            'Me.UserName = "Code " & " : " & objEmp._Employeecode & vbCrLf & "Employee : " & objEmp._Firstname & " " & objEmp._Surname 'objEmp._Displayname
    '                            Me.UserName = "ID " & " : " & objEmp._Employeecode & vbCrLf & "Employee : " & objEmp._Firstname & " " & objEmp._Surname 'objEmp._Displayname
    '                            'SHANI [09 Mar 2015]--END 

    '                            Me.pFirstName = objEmp._Firstname
    '                            Me.pSurName = objEmp._Surname
    '                            'Sohail (30 Mar 2015) -- Start
    '                            'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    '                            Me.pDisplayName = objEmp._Displayname
    '                            'Sohail (30 Mar 2015) -- End
    '                            Me.MemberName = "Emp. : (" & objEmp._Employeecode & ") " & objEmp._Firstname & " " & objEmp._Surname 'Sohail (04 Jun 2012)
    '                            Me.Pappointeddate = objEmp._Appointeddate
    '                            Select Case objEmp._Gender
    '                                Case 1  'Male
    '                                    Me.PGender = "Male"
    '                                Case 2 'Female
    '                                    Me.PGender = "Female"
    '                                Case Else
    '                                    Me.PGender = ""
    '                            End Select
    '                            Me.pbirthdate = objEmp._Birthdate.Date
    '                            Me.pEmail = objEmp._Email
    '                            Me.ppresent_address1 = objEmp._Present_Address1
    '                            Me.ppresent_address2 = objEmp._Present_Address2
    '                            Me.ppresent_mobile = objEmp._Present_Mobile

    '                            Dim objShift As New Aruti.Data.clsNewshift_master
    '                            objShift._Shiftunkid = objEmp._Shiftunkid
    '                            Me.pShiftName = objShift._Shiftname
    '                            objShift = Nothing

    '                            Dim objDept As New Aruti.Data.clsDepartment
    '                            objDept._Departmentunkid = objEmp._Departmentunkid
    '                            Me.pDepartmentName = objDept._Name
    '                            objDept = Nothing
    '                            Return True
    '                        Else
    '                            Me.LoginBy = en_loginby.User
    '                            Me.UserID = objUser._Userunkid
    '                            Me.Employeeunkid = -1
    '                            Me.password = objUser._Password
    '                            Me.UserName = objUser._Username
    '                            Me.MemberName = objUser._Username
    '                            Me.RoleUnkID = objUser._Roleunkid
    '                            Me.mintLangId = objUser._Languageunkid
    '                            Me.pFirstName = objUser._Firstname
    '                            Me.pSurName = objUser._Lastname
    '                            Return True
    '                        End If

    '                    ElseIf objUser._IsManager Then    ' FOR USER LOGIN

    '                        Me.LoginBy = en_loginby.User
    '                        Me.UserID = objUser._Userunkid
    '                        Me.Employeeunkid = -1
    '                        Me.password = objUser._Password
    '                        Me.UserName = objUser._Username
    '                        Me.MemberName = objUser._Username
    '                        Me.RoleUnkID = objUser._Roleunkid
    '                        Me.mintLangId = objUser._Languageunkid
    '                        Me.pFirstName = objUser._Firstname
    '                        Me.pSurName = objUser._Lastname
    '                        Return True

    '                    End If

    '                Else

    '                    HttpContext.Current.Session("mdbname") = Me.SelectedDatabase
    '                    objEmp._Employeeunkid = intUserId
    '                    msql = "select    employeeunkid, password from " & Me.SelectedDatabase & "..hremployee_master where Displayname ='" & pusername.Replace("'", "''") & "'"
    '                    ds = clsDataOperation.WExecQuery(msql, "hremployee_master")

    '                    If ds.Tables(0).Rows.Count > 0 Then
    '                        objEmp._Employeeunkid = ds.Tables(0).Rows(0)("employeeunkid")

    '                        If objEmp._Isapproved = False Then
    '                            mstrMessage = "Sorry, You cannot loging to the system. Reason : You are not approved."
    '                            Return False
    '                        End If

    '                        If objEmp._Reinstatementdate = Nothing Then
    '                            If objEmp._Suspende_To_Date <> Nothing AndAlso objEmp._Suspende_From_Date <> Nothing Then
    '                                If ConfigParameter._Object._CurrentDateAndTime.Date <= objEmp._Suspende_To_Date.Date And _
    '                                       ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Suspende_From_Date.Date Then
    '                                    mstrMessage = "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator."
    '                                    Return False
    '                                End If
    '                            ElseIf objEmp._Probation_From_Date <> Nothing AndAlso objEmp._Probation_To_Date <> Nothing Then
    '                                If ConfigParameter._Object._CurrentDateAndTime.Date <= objEmp._Probation_To_Date.Date And _
    '                                   ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Probation_From_Date.Date Then
    '                                    mstrMessage = "Sorry, you cannot login. Reason : You are in probation period. Please contact Administrator."
    '                                    Return False
    '                                End If
    '                            ElseIf objEmp._Empl_Enddate <> Nothing Then
    '                                If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Empl_Enddate.Date Then
    '                                    mstrMessage = "Sorry, you cannot login. Reason : Your Contract has been expired. Please contact Administrator."
    '                                    Return False
    '                                End If
    '                            ElseIf objEmp._Termination_From_Date <> Nothing Then
    '                                If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_From_Date.Date Then
    '                                    mstrMessage = "Sorry, you cannot login. Reason : You are terminated. Please contact Administrator."
    '                                    Return False
    '                                End If
    '                            ElseIf objEmp._Termination_To_Date <> Nothing Then
    '                                If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_To_Date.Date Then
    '                                    mstrMessage = "Sorry, you cannot login. Reason : You are retired. Please contact Administrator."
    '                                    Return False
    '                                End If
    '                            End If
    '                        End If

    '                        If password <> objEmp._Password Then
    '                            mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '                            Return False
    '                        End If

    '                        If pusername.ToUpper <> objEmp._Displayname.ToUpper Then
    '                            mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '                            Return False
    '                        End If

    '                        Me.LoginBy = en_loginby.Employee
    '                        Me.Employeeunkid = objEmp._Employeeunkid
    '                        Me.password = objEmp._Password

    '                        'SHANI [09 Mar 2015]-START
    '                        'Enhancement - REDESIGN SELF SERVICE.
    '                        'Me.UserName = "Code " & " : " & objEmp._Employeecode & vbCrLf & "Employee : " & objEmp._Firstname & " " & objEmp._Surname 'objEmp._Displayname
    '                        Me.UserName = "ID " & " : " & objEmp._Employeecode & vbCrLf & "Employee : " & objEmp._Firstname & " " & objEmp._Surname 'objEmp._Displayname
    '                        'SHANI [09 Mar 2015]--END 


    '                        Me.pFirstName = objEmp._Firstname
    '                        Me.pSurName = objEmp._Surname
    '                        'Sohail (30 Mar 2015) -- Start
    '                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    '                        Me.pDisplayName = objEmp._Displayname
    '                        'Sohail (30 Mar 2015) -- End
    '                        Me.MemberName = "Emp. : (" & objEmp._Employeecode & ") " & objEmp._Firstname & " " & objEmp._Surname 'Sohail (04 Jun 2012)
    '                        Me.Pappointeddate = objEmp._Appointeddate
    '                        Select Case objEmp._Gender
    '                            Case 1  'Male
    '                                Me.PGender = "Male"
    '                            Case 2 'Female
    '                                Me.PGender = "Female"
    '                            Case Else
    '                                Me.PGender = ""
    '                        End Select
    '                        Me.pbirthdate = objEmp._Birthdate.Date
    '                        Me.pEmail = objEmp._Email
    '                        Me.ppresent_address1 = objEmp._Present_Address1
    '                        Me.ppresent_address2 = objEmp._Present_Address2
    '                        Me.ppresent_mobile = objEmp._Present_Mobile

    '                        Dim objShift As New Aruti.Data.clsNewshift_master
    '                        objShift._Shiftunkid = objEmp._Shiftunkid
    '                        Me.pShiftName = objShift._Shiftname
    '                        objShift = Nothing

    '                        Dim objDept As New Aruti.Data.clsDepartment
    '                        objDept._Departmentunkid = objEmp._Departmentunkid
    '                        Me.pDepartmentName = objDept._Name
    '                        objDept = Nothing
    '                        Return True

    '                    Else
    '                        mstrMessage = "Invalid Username or Password!! Please try again !!!"
    '                        Return False
    '                    End If

    '                End If

    '            ElseIf HttpContext.Current.Session("IsEmployeeAsUser") = False Then
    '                msql = "SELECT CONVERT(int,userunkid) as userunkid,CONVERT(int,-1) as employeeunkid, username as displayname,password,employeeunkid AS EId,companyunkid AS CoId FROM hrmsConfiguration..cfuser_master WHERE username ='" & pusername.Replace("'", "''") & "'  AND password = '" & clsSecurity.Encrypt(password, "ezee") & "'  AND isactive = 1  "
    '                ds = clsDataOperation.WExecQuery(msql, "List")
    '                GoTo UserChecking
    '            End If


    '            Return True
    '        Catch ex As Exception
    '            Throw New Exception("ClsDataOperation-->CheckLogin function" & ex.Message)
    '        End Try

    '    End Function
    Private Function CheckLogin(ByVal pusername As String, ByVal password As String, ByVal selecteddatabase As String, Optional ByVal intUserId As Integer = -1) As Boolean
        Dim ds As DataSet = Nothing
        Dim objUser As New Aruti.Data.clsUserAddEdit
        Dim objEmp As New Aruti.Data.clsEmployee_Master
        Try

            'S.SANDEEP [05 DEC 2015] -- START
            If HttpContext.Current.Session("EmployeeAsOnDate") Is Nothing Then
                HttpContext.Current.Session("EmployeeAsOnDate") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)
            End If
            'S.SANDEEP [05 DEC 2015] -- END

            'Sohail (04 Nov 2019) -- Start
            'EFC Zambia Issue # : User was getting Invalid username or password message when both MSS and ESS licence not issued.
            If Basepage.CheckLicence("MANAGER_SELF_SERVICE") = False AndAlso Basepage.CheckLicence("EMPLOYEE_SELF_SERVICE") = False Then
                mstrMessage = Basepage.m_NoLicenceMsg
                Return False
            End If
            'Sohail (04 Nov 2019) -- End

            'S.SANDEEP [10 MAR 2016] -- START
            'If HttpContext.Current.Session("IsEmployeeAsUser") = True Then
            '    msql = "SELECT CONVERT(int,userunkid) as userunkid,CONVERT(int,-1) as employeeunkid, username as displayname,password,employeeunkid AS EId,companyunkid AS CoId FROM hrmsConfiguration..cfuser_master WHERE username ='" & pusername.Replace("'", "''") & "' AND password = '" & clsSecurity.Encrypt(password, "ezee") & "' AND isactive = 1 AND (ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) > 0 OR hrmsConfiguration..cfuser_master.userunkid = 1) "
            '    ds = clsDataOperation.WExecQuery(msql, "List")
            'ElseIf HttpContext.Current.Session("IsEmployeeAsUser") = False Then
            '    msql = "SELECT CONVERT(int,userunkid) as userunkid,CONVERT(int,-1) as employeeunkid, username as displayname,password,employeeunkid AS EId,companyunkid AS CoId FROM hrmsConfiguration..cfuser_master WHERE username ='" & pusername.Replace("'", "''") & "'  AND password = '" & clsSecurity.Encrypt(password, "ezee") & "'  AND isactive = 1  "
            '    ds = clsDataOperation.WExecQuery(msql, "List")
            'End If

            Dim objPwdOpt As New clsPassowdOptions

            msql = "SELECT CONVERT(int,userunkid) as userunkid,CONVERT(int,-1) as employeeunkid, username as displayname " & _
                   "    ,password,employeeunkid AS EId,companyunkid AS CoId, ISNULL(theme_id, 1) AS theme_id, ISNULL(lastview_id, 0) AS lastview_id FROM hrmsConfiguration..cfuser_master " & _
                   "WHERE username = @username AND isactive = 1  "
            'Sohail (13 Dec 2018) - ['" & pusername.Replace("'", "''") & "'] = [@username]
            'Sohail (24 Nov 2016) - [theme_id, lastview_id]

            'Sohail (24 Jul 2019) -- Start
            'Silensec Africa Limited Issue - #  - 76.1 - Employee was not able to login if he is manager and No MSS licence .
            If Basepage.CheckLicence("MANAGER_SELF_SERVICE") = False Then
                msql &= " AND 1 = 2 "
            End If
            'Sohail (24 Jul 2019) -- End


            'Pinkal (03-Apr-2017) -- Start
            'Enhancement - Working On Active directory Changes for PACRA.

            HttpContext.Current.Session("AuthenticationMode") = enAuthenticationMode.BASIC_AUTHENTICATION

            If objPwdOpt._DataTable IsNot Nothing AndAlso objPwdOpt._DataTable.Rows.Count > 0 Then

                Dim drRow = From dr In objPwdOpt._DataTable Where dr("passwdoptionid") = enPasswordOption.USER_LOGIN_MODE Select dr

                If drRow.Count > 0 Then

                    If CInt(drRow(0)("passwdminlen")) = enAuthenticationMode.BASIC_AUTHENTICATION Then
                        msql &= " AND password = @password "
                        'Sohail (13 Dec 2018) - ['" & clsSecurity.Encrypt(password, "ezee") & "'] = [@password]
                    Else
                        HttpContext.Current.Session("AuthenticationMode") = CInt(drRow(0)("passwdminlen"))
                    End If 'CInt(drRow(0)("passwdminlen")) = enAuthenticationMode.BASIC_AUTHENTICATION

                End If 'drRow.Count > 0 

            End If 'objPwdOpt._DataTable IsNot Nothing AndAlso objPwdOpt._DataTable.Rows.Count > 0

            'Pinkal (03-Apr-2017) -- End


            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Solving Error When Direct Open page from link
            If HttpContext.Current.Session("IsEmployeeAsUser") Is Nothing Then HttpContext.Current.Session("IsEmployeeAsUser") = objPwdOpt._IsEmployeeAsUser
            'Pinkal (30-Jun-2016) -- End


            'Pinkal (03-Apr-2017) -- Start
            'Enhancement - Working On Active directory Changes for PACRA.
            If objPwdOpt._EnableAllUser = False Then

                If HttpContext.Current.Session("AuthenticationMode") = enAuthenticationMode.BASIC_AUTHENTICATION Then

                If HttpContext.Current.Session("IsEmployeeAsUser") = True Then
                    msql &= " AND (ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) > 0 OR hrmsConfiguration..cfuser_master.userunkid = 1) "
                Else
                    msql &= " AND (ISNULL(hrmsConfiguration..cfuser_master.employeeunkid,0) <= 0 OR hrmsConfiguration..cfuser_master.userunkid = 1) "
                End If

                    msql &= " And  (ISNULL(hrmsConfiguration..cfuser_master.isaduser,0) = 0 or hrmsConfiguration..cfuser_master.userunkid = 1) "

                ElseIf HttpContext.Current.Session("AuthenticationMode") = enAuthenticationMode.AD_BASIC_AUTHENTICATION OrElse HttpContext.Current.Session("AuthenticationMode") = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
                    msql &= " And  (hrmsConfiguration..cfuser_master.isaduser = 1  or hrmsConfiguration..cfuser_master.userunkid = 1) "
                End If

            End If
            'Pinkal (03-Apr-2017) -- End

            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement : Security issues in Self Service in 75.1            

            clsDataOperation.ClearParameters()

            'Pinkal (13-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[Due to In NMB Special Character (') apostrophe in between User Name].
            'clsDataOperation.AddParameter("@username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, pusername.Replace("'", "''"))
            clsDataOperation.AddParameter("@username", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, pusername.Trim())
            'Pinkal (13-Mar-2019) -- End
            clsDataOperation.AddParameter("@password", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, clsSecurity.Encrypt(password, "ezee"))
            'Sohail (13 Dec 2018) -- End
            ds = clsDataOperation.WExecQuery(msql, "List")
            'S.SANDEEP [10 MAR 2016] -- END


            'Pinkal (27-Nov-2020) -- Start
            'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
            Dim objConfigOption As New clsConfigOptions
            Dim mstrAllowEmpSystemAccessOnActualEOCRetirementDate As String = ""
            Dim mblnAllowEmpSystemAccessOnActualEOCRetirementDate As Boolean = False
            objConfigOption.IsValue_Changed("AllowEmpSystemAccessOnActualEOCRetirementDate", "-999", mstrAllowEmpSystemAccessOnActualEOCRetirementDate)
            mblnAllowEmpSystemAccessOnActualEOCRetirementDate = CBool(IIf(mstrAllowEmpSystemAccessOnActualEOCRetirementDate.Length <= 0, False, mstrAllowEmpSystemAccessOnActualEOCRetirementDate))
            objConfigOption = Nothing
            'Pinkal (27-Nov-2020) -- End


            '*** MSS Default Temp Sessions
            HttpContext.Current.Session("U_UserID") = -1
            HttpContext.Current.Session("U_Employeeunkid") = -1
            HttpContext.Current.Session("U_UserName") = ""
            HttpContext.Current.Session("U_Password") = ""
            HttpContext.Current.Session("U_LeaveBalances") = 0
            HttpContext.Current.Session("U_MemberName") = ""
            HttpContext.Current.Session("U_RoleID") = 0
            HttpContext.Current.Session("U_LangId") = 0
            HttpContext.Current.Session("U_Firstname") = ""
            HttpContext.Current.Session("U_Surname") = ""
            HttpContext.Current.Session("U_DisplayName") = ""
            HttpContext.Current.Session("U_Appointeddate") = ""
            HttpContext.Current.Session("U_Gender") = ""
            HttpContext.Current.Session("U_Birthdate") = ""
            HttpContext.Current.Session("U_Email") = ""
            HttpContext.Current.Session("U_Present_Address1") = ""
            HttpContext.Current.Session("U_Present_Address2") = ""
            HttpContext.Current.Session("U_Present_Mobile") = ""
            HttpContext.Current.Session("U_Shiftname") = ""
            HttpContext.Current.Session("U_DepartmentName") = ""
            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            HttpContext.Current.Session("U_Theme_id") = ""
            HttpContext.Current.Session("U_Lastview_id") = ""
            'Sohail (24 Nov 2016) -- End

            'S.SANDEEP |08-JAN-2019| -- START
            HttpContext.Current.Session("U_Email") = ""
            'S.SANDEEP |08-JAN-2019| -- END

            '*** ESS Default Temp Sessions
            HttpContext.Current.Session("E_UserID") = -1
            HttpContext.Current.Session("E_Employeeunkid") = -1
            HttpContext.Current.Session("E_UserName") = ""
            HttpContext.Current.Session("E_Password") = ""
            HttpContext.Current.Session("E_LeaveBalances") = 0
            HttpContext.Current.Session("E_MemberName") = ""
            HttpContext.Current.Session("E_RoleID") = 0
            HttpContext.Current.Session("E_LangId") = 1 ' by default set custom1 for employee login for all. 
            HttpContext.Current.Session("E_Firstname") = ""
            HttpContext.Current.Session("E_Surname") = ""
            HttpContext.Current.Session("E_DisplayName") = ""
            HttpContext.Current.Session("E_Appointeddate") = ""
            HttpContext.Current.Session("E_Gender") = ""
            HttpContext.Current.Session("E_Birthdate") = ""
            HttpContext.Current.Session("E_Email") = ""
            HttpContext.Current.Session("E_Present_Address1") = ""
            HttpContext.Current.Session("E_Present_Address2") = ""
            HttpContext.Current.Session("E_Present_Mobile") = ""
            HttpContext.Current.Session("E_Shiftname") = ""
            HttpContext.Current.Session("E_DepartmentName") = ""
            'Sohail (24 Nov 2016) -- Start
            'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
            HttpContext.Current.Session("E_Theme_id") = ""
            HttpContext.Current.Session("E_Lastview_id") = ""
            'Sohail (24 Nov 2016) -- End

            'S.SANDEEP |08-JAN-2019| -- START
            HttpContext.Current.Session("E_Email") = ""
            'S.SANDEEP |08-JAN-2019| -- END

            If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                objUser._Userunkid = ds.Tables(0).Rows(0)("Userunkid")


                'Pinkal (03-Apr-2017) -- Start
                'Enhancement - Working On Active directory Changes for PACRA.

                'Pinkal (14-NOV-2018) -- Start
                'BUG - ADMIN LOGGED IN ARUTI WITH WRONG PASSWORD WHEN ACTIVE DIRECTORY SETTING IS ON.
                'If CInt(HttpContext.Current.Session("AuthenticationMode")) = enAuthenticationMode.BASIC_AUTHENTICATION THEN
                If CInt(HttpContext.Current.Session("AuthenticationMode")) = enAuthenticationMode.BASIC_AUTHENTICATION OrElse objUser._Userunkid = 1 Then  'PASSWORD CHECKING FOR ADMIN OR BASIC AUTHENTICATION
                    'Pinkal (14-NOV-2018) -- End
                If password <> objUser._Password Then
                    mstrMessage = "Invalid Username or Password!! Please try again !!!"
                    Return False
                End If
                End If
                'Pinkal (03-Apr-2017) -- End


                'Pinkal (31-Jan-2020) -- Start
                'Enhancements -  problem in self service login when username having pre or post space.
                'If pusername.ToUpper <> objUser._Username.ToUpper Then
                If pusername.Trim().ToUpper <> objUser._Username.Trim().ToUpper Then
                    'Pinkal (31-Jan-2020) -- End
                    mstrMessage = "Invalid Username or Password!! Please try again !!!"
                    Return False
                End If

                If IsAccessGiven(objUser._Userunkid, HttpContext.Current.Session("Fin_year")) = False Then
                    mstrMessage = "Sorry, You cannot login to system. Reason : Access to selected company is not given to you. Please contact Administrator."
                    Return False
                End If

                'S.SANDEEP [10 MAR 2016] -- START
                'Dim objPwdOpt As New clsPassowdOptions
                'S.SANDEEP [10 MAR 2016] -- END

                If objUser._IsManager = True Then    ' FOR USER LOGIN

                    Me.LoginBy = en_loginby.User
                    Me.UserID = objUser._Userunkid
                    Me.Employeeunkid = -1
                    Me.password = objUser._Password
                    'Pinkal (31-Jan-2020) -- Start
                    'Enhancements -  problem in self service login when username having pre or post space.
                    'Me.UserName = objUser._Username
                    'Me.MemberName = objUser._Username
                    Me.UserName = objUser._Username.Trim()
                    Me.MemberName = objUser._Username.Trim()
                    'Pinkal (31-Jan-2020) -- End

                    Me.RoleUnkID = objUser._Roleunkid
                    Me.mintLangId = objUser._Languageunkid
                    Me.pFirstName = objUser._Firstname
                    Me.pSurName = objUser._Lastname
                    'Sohail (24 Nov 2016) -- Start
                    'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
                    Me.pTheme_Id = objUser._Theme_Id
                    Me.pLastView_Id = objUser._LastView_Id
                    'Sohail (24 Nov 2016) -- End


                    HttpContext.Current.Session("U_UserID") = objUser._Userunkid
                    HttpContext.Current.Session("U_Employeeunkid") = -1

                    'Pinkal (31-Jan-2020) -- Start
                    'Enhancements -  problem in self service login when username having pre or post space.
                    'HttpContext.Current.Session("U_UserName") = objUser._Username
                    HttpContext.Current.Session("U_UserName") = objUser._Username.Trim()
                    'Pinkal (31-Jan-2020) -- End

                    HttpContext.Current.Session("U_Password") = objUser._Password
                    HttpContext.Current.Session("U_LeaveBalances") = 0

                    'Pinkal (31-Jan-2020) -- Start
                    'Enhancements -  problem in self service login when username having pre or post space.
                    'HttpContext.Current.Session("U_MemberName") = objUser._Username
                    HttpContext.Current.Session("U_MemberName") = objUser._Username.Trim()
                    'Pinkal (31-Jan-2020) -- End

                    HttpContext.Current.Session("U_RoleID") = objUser._Roleunkid
                    HttpContext.Current.Session("U_LangId") = objUser._Languageunkid
                    HttpContext.Current.Session("U_Firstname") = objUser._Firstname
                    HttpContext.Current.Session("U_Surname") = objUser._Lastname
                    HttpContext.Current.Session("U_DisplayName") = ""


                    'S.SANDEEP [10 DEC 2015] -- START
                    Dim blnFlag As Boolean = False
                    blnFlag = IsAssessorMappedWithLoginUser(Me.UserID, Me.SelectedDatabase)
                    If blnFlag Then
                        HttpContext.Current.Session("U_MappedAssessor") = True
                    Else
                        HttpContext.Current.Session("U_MappedAssessor") = False
                    End If
                    'S.SANDEEP [10 DEC 2015] -- END

                    'Sohail (24 Nov 2016) -- Start
                    'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
                    HttpContext.Current.Session("U_Theme_id") = objUser._Theme_Id
                    HttpContext.Current.Session("U_Lastview_id") = objUser._LastView_Id
                    HttpContext.Current.Session("Theme_id") = objUser._Theme_Id
                    HttpContext.Current.Session("Lastview_id") = objUser._LastView_Id
                    'Sohail (24 Nov 2016) -- End

                    'S.SANDEEP |08-JAN-2019| -- START
                    HttpContext.Current.Session("U_Email") = objUser._Email
                    'S.SANDEEP |08-JAN-2019| -- END

                End If

                'If objUser._IsManager = False Then   ' FOR EMPLOYEE LOGIN

                'Pinkal (03-Apr-2017) -- Start
                'Enhancement - Working On Active directory Changes for PACRA.
                'If HttpContext.Current.Session("IsEmployeeAsUser") = True  Then
                If HttpContext.Current.Session("IsEmployeeAsUser") = True OrElse (HttpContext.Current.Session("AuthenticationMode") = enAuthenticationMode.AD_BASIC_AUTHENTICATION OrElse HttpContext.Current.Session("AuthenticationMode") = enAuthenticationMode.AD_SSO_AUTHENTICATION) Then
                    'Pinkal (03-Apr-2017) -- End

                  

                    If HttpContext.Current.Session("CompanyUnkId") <> CInt(ds.Tables(0).Rows(0).Item("CoId")) Then

                        'Pinkal (01-Dec-2020) -- Start
                        'Enhancement  -  Working on Temparory Solution for NMB another company.
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        If objGroupMaster._Groupname.ToUpper() = "NMB PLC" AndAlso (HttpContext.Current.Session("CompCode").ToString().ToUpper() = "NMB" OrElse HttpContext.Current.Session("CompCode").ToString().ToUpper() = "NMBX") Then
                            If objUser._IsManager = True AndAlso CInt(ds.Tables(0).Rows(0).Item("CoId")) <= 0 Then Return True
                        Else
                        If objUser._IsManager = False Then
                            mstrMessage = "Sorry, No such employee exists in the selected company."
                            Return False
                        Else
                            Return True
                        End If
                    End If
                        objGroupMaster = Nothing
                        'Pinkal (01-Dec-2020) -- End
                    End If



                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objEmp._Employeeunkid = ds.Tables(0).Rows(0).Item("EId")
                    objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate"))) = ds.Tables(0).Rows(0).Item("EId")
                    'Shani(20-Nov-2015) -- End


                    If objEmp._Isapproved = False Then

                        'Anjan [23 November 2016] -- Start
                        'Issue : This condtion was closed due to TRA auditors query that inactive managers were able to login in ESS.Requested by Rutta as per 23rd november 2016 email.
                        'If objUser._IsManager = False Then

                        mstrMessage = "Sorry, You cannot loging to the system. Reason : You are not approved."
                        Return False
                        'End If -- 'Anjan [23 November 2016] -- End
                    End If



                    'Pinkal (25-May-2019) -- Start
                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                    gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Configuration, Me.SelectedDatabase.ToString())
                    gobjLocalization._LangId = objUser._Languageunkid   ' by default set custom1 for employee login for all. 
                    'Pinkal (25-May-2019) -- End


                    'Anjan [23 November 2016] -- Start
                    'Issue : This condition was closed coz if employee rehire was less than termination/EOC date then alos user/employee was able to login and this condition was just checking if rehire date was nothing .
                    'If objEmp._Reinstatementdate = Nothing Then
                    If objEmp._Suspende_To_Date <> Nothing AndAlso objEmp._Suspende_From_Date <> Nothing Then
                        If ConfigParameter._Object._CurrentDateAndTime.Date <= objEmp._Suspende_To_Date.Date And _
                               ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Suspende_From_Date.Date Then
                            'Pinkal (03-May-2019) -- Start
                            'Enhancement - Working on Leave UAT Changes for NMB.[A.D User Manager doesn't login if user is suspended]
                            ' If objUser._IsManager = False Then
                            'Pinkal (25-May-2019) -- Start
                            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                            'mstrMessage = "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator."
                            mstrMessage = Language.getMessage("frmUser_AddEdit", 21, "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator.")
                            'Pinkal (25-May-2019) -- End
                                Return False
                            'End If
                            'Pinkal (03-May-2019) -- End
                        End If

                    ElseIf objEmp._Empl_Enddate <> Nothing Then

                        'Pinkal (27-Nov-2020) -- Start
                        'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                        Dim mblnFlag As Boolean = False
                        If mblnAllowEmpSystemAccessOnActualEOCRetirementDate Then
                            If ConfigParameter._Object._CurrentDateAndTime.Date > objEmp._Empl_Enddate.Date Then mblnFlag = True
                        Else
                            If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Empl_Enddate.Date Then mblnFlag = True
                        End If
                        If mblnFlag = True Then
                            mstrMessage = Language.getMessage("frmUser_AddEdit", 22, "Sorry, you cannot login. Reason : Your Contract has been expired. Please contact Administrator.")
                            Return False
                        End If
                        'Pinkal (27-Nov-2020) -- End


                    ElseIf objEmp._Termination_From_Date <> Nothing Then

                        'Pinkal (27-Nov-2020) -- Start
                        'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                        Dim mblnFlag As Boolean = False
                        If mblnAllowEmpSystemAccessOnActualEOCRetirementDate Then
                            If ConfigParameter._Object._CurrentDateAndTime.Date > objEmp._Termination_From_Date.Date Then mblnFlag = True
                        Else
                            If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_From_Date.Date Then mblnFlag = True
                        End If
                        If mblnFlag Then
                            mstrMessage = Language.getMessage("frmUser_AddEdit", 23, "Sorry, you cannot login. Reason : You are terminated. Please contact Administrator.")
                            Return False
                        End If
                        'Pinkal (27-Nov-2020) -- End

                    ElseIf objEmp._Termination_To_Date <> Nothing Then

                        'Pinkal (27-Nov-2020) -- Start
                        'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                        Dim mblnFlag As Boolean = False
                        If mblnAllowEmpSystemAccessOnActualEOCRetirementDate Then
                            If ConfigParameter._Object._CurrentDateAndTime.Date > objEmp._Termination_To_Date.Date Then mblnFlag = True
                        Else
                            If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_To_Date.Date Then mblnFlag = True
                        End If
                        If mblnFlag Then
                            mstrMessage = Language.getMessage("frmUser_AddEdit", 24, "Sorry, you cannot login. Reason : You are retired. Please contact Administrator.")
                            Return False
                        End If
                        'Pinkal (27-Nov-2020) -- End

                    End If
                    'End If  --'Anjan [23 November 2016] -- End

                    If objUser._IsManager = False Then
                        Me.UserID = -1
                        Me.LoginBy = en_loginby.Employee

                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'Me.Employeeunkid = objEmp._Employeeunkid
                        Me.Employeeunkid = objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate")))
                        'Shani(20-Nov-2015) -- End

                        'Sohail (31 May 2018) -- Start
                        'TANAPA Enhancement - Ref # 247 : Show Online Recruitment Internal Vacancy link in ESS in 72.1.
                        Me.EmployeeCode = objEmp._Employeecode
                        'Sohail (31 May 2018) -- End

                        Me.password = objEmp._Password
                        Me.UserName = "ID " & " : " & objEmp._Employeecode & vbCrLf & "Employee : " & objEmp._Firstname & " " & objEmp._Surname 'objEmp._Displayname
                        Me.pFirstName = objEmp._Firstname
                        Me.pSurName = objEmp._Surname
                        'Sohail (30 Mar 2015) -- Start
                        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                        Me.pDisplayName = objEmp._Displayname
                        'Sohail (30 Mar 2015) -- End
                        Me.MemberName = "Emp. : (" & objEmp._Employeecode & ") " & objEmp._Firstname & " " & objEmp._Surname 'Sohail (04 Jun 2012)
                        Me.Pappointeddate = objEmp._Appointeddate
                        Select Case objEmp._Gender
                            Case 1  'Male
                                Me.PGender = "Male"
                            Case 2 'Female
                                Me.PGender = "Female"
                            Case Else
                                Me.PGender = ""
                        End Select
                        Me.pbirthdate = objEmp._Birthdate.Date
                        Me.pEmail = objEmp._Email
                        Me.ppresent_address1 = objEmp._Present_Address1
                        Me.ppresent_address2 = objEmp._Present_Address2
                        Me.ppresent_mobile = objEmp._Present_Mobile

                        Dim objShift As New Aruti.Data.clsNewshift_master
                        objShift._Shiftunkid = objEmp._Shiftunkid
                        Me.pShiftName = objShift._Shiftname
                        objShift = Nothing

                        Dim objDept As New Aruti.Data.clsDepartment
                        objDept._Departmentunkid = objEmp._Departmentunkid
                        Me.pDepartmentName = objDept._Name
                        objDept = Nothing

                        'Sohail (24 Nov 2016) -- Start
                        'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
                        Me.pTheme_Id = objEmp._Theme_Id
                        Me.pLastView_Id = objEmp._LastView_Id
                        'Sohail (24 Nov 2016) -- End

                        'Return True
                    End If


                    HttpContext.Current.Session("E_UserID") = -1

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'HttpContext.Current.Session("E_Employeeunkid") = objEmp._Employeeunkid
                    HttpContext.Current.Session("E_Employeeunkid") = objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate")))
                    'Shani(20-Nov-2015) -- End  

                    'Sohail (31 May 2018) -- Start
                    'TANAPA Enhancement - Ref # 247 : Show Online Recruitment Internal Vacancy link in ESS in 72.1.
                    HttpContext.Current.Session("E_Employeecode") = objEmp._Employeecode
                    'Sohail (31 May 2018) -- End

                    HttpContext.Current.Session("E_UserName") = "ID " & " : " & objEmp._Employeecode & vbCrLf & "Employee : " & objEmp._Firstname & " " & objEmp._Surname 'objEmp._Displayname
                    HttpContext.Current.Session("E_Password") = objEmp._Password
                    HttpContext.Current.Session("E_LeaveBalances") = 0
                    HttpContext.Current.Session("E_MemberName") = "Emp. : (" & objEmp._Employeecode & ") " & objEmp._Firstname & " " & objEmp._Surname
                    HttpContext.Current.Session("E_RoleID") = 0
                    HttpContext.Current.Session("E_LangId") = 1 ' by default set custom1 for employee login for all. 
                    HttpContext.Current.Session("E_Firstname") = objEmp._Firstname
                    HttpContext.Current.Session("E_Surname") = objEmp._Surname
                    'Sohail (04 Feb 2020) -- Start
                    'NMB Enhancement # : New screen "Non-Disclosure Declaration" with lock / unlock options.
                    HttpContext.Current.Session("E_Othername") = objEmp._Othername
                    'Sohail (04 Feb 2020) -- End
                    HttpContext.Current.Session("E_DisplayName") = objEmp._Displayname
                    HttpContext.Current.Session("E_Appointeddate") = objEmp._Appointeddate

                    HttpContext.Current.Session("E_Birthdate") = objEmp._Birthdate.Date
                    HttpContext.Current.Session("E_Email") = objEmp._Email
                    HttpContext.Current.Session("E_Present_Address1") = objEmp._Present_Address1
                    HttpContext.Current.Session("E_Present_Address2") = objEmp._Present_Address2
                    HttpContext.Current.Session("E_Present_Mobile") = objEmp._Present_Mobile
                    'Sohail (24 Nov 2016) -- Start
                    'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
                    HttpContext.Current.Session("E_Theme_id") = objEmp._Theme_Id
                    HttpContext.Current.Session("E_Lastview_id") = objEmp._LastView_Id
                    HttpContext.Current.Session("Theme_id") = objEmp._Theme_Id
                    HttpContext.Current.Session("Lastview_id") = objEmp._LastView_Id
                    'Sohail (24 Nov 2016) -- End

                    'S.SANDEEP |08-JAN-2019| -- START
                    HttpContext.Current.Session("E_Email") = objEmp._Email
                    'S.SANDEEP |08-JAN-2019| -- END

                    If objUser._IsManager = True Then
                        Select Case objEmp._Gender
                            Case 1  'Male
                                HttpContext.Current.Session("E_Gender") = "Male"
                            Case 2 'Female
                                HttpContext.Current.Session("E_Gender") = "Female"
                            Case Else
                                HttpContext.Current.Session("E_Gender") = ""
                        End Select

                        Dim objShift As New Aruti.Data.clsNewshift_master
                        objShift._Shiftunkid = objEmp._Shiftunkid
                        HttpContext.Current.Session("E_Shiftname") = objShift._Shiftname
                        objShift = Nothing

                        Dim objDept As New Aruti.Data.clsDepartment
                        objDept._Departmentunkid = objEmp._Departmentunkid
                        HttpContext.Current.Session("E_DepartmentName") = objDept._Name
                        objDept = Nothing

                    Else
                        HttpContext.Current.Session("E_Gender") = Me.PGender
                        HttpContext.Current.Session("E_Shiftname") = Me.pShiftName
                        HttpContext.Current.Session("E_DepartmentName") = Me.pDepartmentName
                    End If

                Else 'IsEmployeeAsUser = FALSE

                    If objUser._IsManager = False Then
                        Me.LoginBy = en_loginby.User
                        Me.UserID = objUser._Userunkid
                        Me.Employeeunkid = -1
                        Me.password = objUser._Password
                        'Pinkal (31-Jan-2020) -- Start
                        'Enhancements -  problem in self service login when username having pre or post space.
                        'Me.UserName = objUser._Username
                        'Me.MemberName = objUser._Username
                        Me.UserName = objUser._Username.Trim()
                        Me.MemberName = objUser._Username.Trim()
                        'Pinkal (31-Jan-2020) -- End
                        Me.RoleUnkID = objUser._Roleunkid
                        Me.mintLangId = objUser._Languageunkid
                        Me.pFirstName = objUser._Firstname
                        Me.pSurName = objUser._Lastname
                        'Sohail (24 Nov 2016) -- Start
                        'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
                        Me.pTheme_Id = objUser._Theme_Id
                        Me.pLastView_Id = objUser._LastView_Id
                        'Sohail (24 Nov 2016) -- End

                        HttpContext.Current.Session("U_UserID") = objUser._Userunkid
                        HttpContext.Current.Session("U_Employeeunkid") = -1
                        'Pinkal (31-Jan-2020) -- Start
                        'Enhancements -  problem in self service login when username having pre or post space.
                        'HttpContext.Current.Session("U_UserName") = objUser._Username
                        HttpContext.Current.Session("U_UserName") = objUser._Username.Trim()
                        'Pinkal (31-Jan-2020) -- End

                        HttpContext.Current.Session("U_Password") = objUser._Password
                        HttpContext.Current.Session("U_LeaveBalances") = 0

                        'Pinkal (31-Jan-2020) -- Start
                        'Enhancements -  problem in self service login when username having pre or post space.
                        'HttpContext.Current.Session("U_MemberName") = objUser._Username
                        HttpContext.Current.Session("U_MemberName") = objUser._Username.Trim()
                        'Pinkal (31-Jan-2020) -- End

                        HttpContext.Current.Session("U_RoleID") = objUser._Roleunkid
                        HttpContext.Current.Session("U_LangId") = objUser._Languageunkid
                        HttpContext.Current.Session("U_Firstname") = objUser._Firstname
                        HttpContext.Current.Session("U_Surname") = objUser._Lastname
                        HttpContext.Current.Session("U_DisplayName") = ""
                        'Sohail (24 Nov 2016) -- Start
                        'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
                        HttpContext.Current.Session("U_Theme_id") = objUser._Theme_Id
                        HttpContext.Current.Session("U_Lastview_id") = objUser._LastView_Id
                        HttpContext.Current.Session("Theme_id") = objUser._Theme_Id
                        HttpContext.Current.Session("Lastview_id") = objUser._LastView_Id
                        'Sohail (24 Nov 2016) -- End

                        'S.SANDEEP |08-JAN-2019| -- START
                        HttpContext.Current.Session("U_Email") = objUser._Email
                        'S.SANDEEP |08-JAN-2019| -- END

                    End If

                    'Return True
                End If

                'ElseIf objUser._IsManager Then    ' FOR USER LOGIN

                '    Me.LoginBy = en_loginby.User
                '    Me.UserID = objUser._Userunkid
                '    Me.Employeeunkid = -1
                '    Me.password = objUser._Password
                '    Me.UserName = objUser._Username
                '    Me.MemberName = objUser._Username
                '    Me.RoleUnkID = objUser._Roleunkid
                '    Me.mintLangId = objUser._Languageunkid
                '    Me.pFirstName = objUser._Firstname
                '    Me.pSurName = objUser._Lastname
                '    Return True

                'End If

            Else 'If No record found in User Master

                HttpContext.Current.Session("mdbname") = Me.SelectedDatabase

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmp._Employeeunkid = intUserId
                objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate"))) = intUserId
                'Shani(20-Nov-2015) -- End

                msql = "select    employeeunkid, password from " & Me.SelectedDatabase & "..hremployee_master where Displayname = @Displayname "

                'Sohail (24 Jul 2019) -- Start
                'Silensec Africa Limited Issue - #  - 76.1 - Employee was not able to login if he is manager and No MSS licence .
                If Basepage.CheckLicence("EMPLOYEE_SELF_SERVICE") = False Then
                    msql &= " AND 1 = 2 "
                End If
                'Sohail (24 Jul 2019) -- End

                'Sohail (13 Dec 2018) -- Start
                'NMB Enhancement : Security issues in Self Service in 75.1            
                '['" & pusername.Replace("'", "''") & "'] = [@Displayname]
                clsDataOperation.ClearParameters()

                'Pinkal (31-Jan-2020) -- Start
                'Enhancements -  problem in self service login when username having pre or post space.
                'clsDataOperation.AddParameter("@Displayname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, pusername.Replace("'", "''"))
                clsDataOperation.AddParameter("@Displayname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, pusername.Trim().Replace("'", "''"))
                'Pinkal (31-Jan-2020) -- End

                'Sohail (13 Dec 2018) -- End
                ds = clsDataOperation.WExecQuery(msql, "hremployee_master")

                If ds.Tables(0).Rows.Count > 0 Then
                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS 
                    objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate"))) = ds.Tables(0).Rows(0)("employeeunkid")
                    'Shani(20-Nov-2015) -- End


                    If objEmp._Isapproved = False Then
                        mstrMessage = "Sorry, You cannot loging to the system. Reason : You are not approved."
                        Return False
                    End If


                    'Pinkal (25-May-2019) -- Start
                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                    gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Configuration, HttpContext.Current.Session("mdbname").ToString)
                    gobjLocalization._LangId = 1   ' by default set custom1 for employee login for all. 
                    'Pinkal (25-May-2019) -- End


                    If objEmp._Reinstatementdate = Nothing Then
                        If objEmp._Suspende_To_Date <> Nothing AndAlso objEmp._Suspende_From_Date <> Nothing Then
                            If ConfigParameter._Object._CurrentDateAndTime.Date <= objEmp._Suspende_To_Date.Date And _
                                   ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Suspende_From_Date.Date Then
                                'Pinkal (25-May-2019) -- Start
                                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                                'mstrMessage = "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator."
                                mstrMessage = Language.getMessage("frmUser_AddEdit", 21, "Sorry, you cannot login. Reason : You are in suspension period. Please contact Administrator.")
                                'Pinkal (25-May-2019) -- End
                                Return False
                            End If
                            'Anjan [28 May 2014] -- Start
                            'ENHANCEMENT : Requested by Rutta/Andrew as at TRA and AKFTZ employees were not able to login on probabted period.
                            'ElseIf objEmp._Probation_From_Date <> Nothing AndAlso objEmp._Probation_To_Date <> Nothing Then
                            '    If ConfigParameter._Object._CurrentDateAndTime.Date <= objEmp._Probation_To_Date.Date And _
                            '       ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Probation_From_Date.Date Then
                            '        mstrMessage = "Sorry, you cannot login. Reason : You are in probation period. Please contact Administrator."
                            '        Return False
                            '    End If
                            'Anjan [28 May 2014] -- End
                        ElseIf objEmp._Empl_Enddate <> Nothing Then

                            'Pinkal (27-Nov-2020) -- Start
                            'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                            Dim mblnFlag As Boolean = False
                            If mblnAllowEmpSystemAccessOnActualEOCRetirementDate Then
                                If ConfigParameter._Object._CurrentDateAndTime.Date > objEmp._Empl_Enddate.Date Then mblnFlag = True
                            Else
                                If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Empl_Enddate.Date Then mblnFlag = True
                            End If
                            If mblnFlag Then
                                mstrMessage = Language.getMessage("frmUser_AddEdit", 22, "Sorry, you cannot login. Reason : Your Contract has been expired. Please contact Administrator.")
                                Return False
                            End If
                            'Pinkal (27-Nov-2020) -- End

                        ElseIf objEmp._Termination_From_Date <> Nothing Then
                            'Pinkal (27-Nov-2020) -- Start
                            'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                            Dim mblnFlag As Boolean = False
                            If mblnAllowEmpSystemAccessOnActualEOCRetirementDate Then
                                If ConfigParameter._Object._CurrentDateAndTime.Date > objEmp._Termination_From_Date.Date Then mblnFlag = True
                            Else
                                If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_From_Date.Date Then mblnFlag = True
                            End If
                            If mblnFlag Then
                                mstrMessage = Language.getMessage("frmUser_AddEdit", 23, "Sorry, you cannot login. Reason : You are terminated. Please contact Administrator.")
                                Return False
                            End If
                            'Pinkal (27-Nov-2020) -- End
                        ElseIf objEmp._Termination_To_Date <> Nothing Then
                            'Pinkal (27-Nov-2020) -- Start
                            'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                            Dim mblnFlag As Boolean = False
                            If mblnAllowEmpSystemAccessOnActualEOCRetirementDate Then
                                If ConfigParameter._Object._CurrentDateAndTime.Date > objEmp._Termination_To_Date.Date Then mblnFlag = True
                            Else
                                If ConfigParameter._Object._CurrentDateAndTime.Date >= objEmp._Termination_To_Date.Date Then mblnFlag = True
                            End If
                            If mblnFlag Then
                                mstrMessage = Language.getMessage("frmUser_AddEdit", 24, "Sorry, you cannot login. Reason : You are retired. Please contact Administrator.")
                                Return False
                            End If
                            'Pinkal (27-Nov-2020) -- End
                        End If
                    End If


                    'Pinkal (03-Apr-2017) -- Start
                    'Enhancement - Working On Active directory Changes for PACRA.
                    If CInt(HttpContext.Current.Session("AuthenticationMode")) = enAuthenticationMode.BASIC_AUTHENTICATION Then
                    If password <> objEmp._Password Then
                        mstrMessage = "Invalid Username or Password!! Please try again !!!"
                        Return False
                    End If
                    End If
                    'Pinkal (03-Apr-2017) -- End 

                    'Pinkal (31-Jan-2020) -- Start
                    'Enhancements -  problem in self service login when username having pre or post space.
                    'If pusername.ToUpper <> objEmp._Displayname.ToUpper Then
                    If pusername.Trim().ToUpper <> objEmp._Displayname.Trim().ToUpper Then
                        'Pinkal (31-Jan-2020) -- End
                        mstrMessage = "Invalid Username or Password!! Please try again !!!"
                        Return False
                    End If

                    Me.LoginBy = en_loginby.Employee

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS 
                    'Me.Employeeunkid = objEmp._Employeeunkid
                    Me.Employeeunkid = objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate")))
                    'Shani(20-Nov-2015) -- End

                    'Sohail (31 May 2018) -- Start
                    'TANAPA Enhancement - Ref # 247 : Show Online Recruitment Internal Vacancy link in ESS in 72.1.
                    Me.EmployeeCode = objEmp._Employeecode
                    'Sohail (31 May 2018) -- End

                    Me.password = objEmp._Password
                    Me.UserName = "ID " & " : " & objEmp._Employeecode & vbCrLf & "Employee : " & objEmp._Firstname & " " & objEmp._Surname 'objEmp._Displayname
                    Me.pFirstName = objEmp._Firstname
                    Me.pSurName = objEmp._Surname
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    Me.pDisplayName = objEmp._Displayname
                    'Sohail (30 Mar 2015) -- End
                    Me.MemberName = "Emp. : (" & objEmp._Employeecode & ") " & objEmp._Firstname & " " & objEmp._Surname 'Sohail (04 Jun 2012)
                    Me.Pappointeddate = objEmp._Appointeddate
                    Select Case objEmp._Gender
                        Case 1  'Male
                            Me.PGender = "Male"
                        Case 2 'Female
                            Me.PGender = "Female"
                        Case Else
                            Me.PGender = ""
                    End Select
                    Me.pbirthdate = objEmp._Birthdate.Date
                    Me.pEmail = objEmp._Email
                    Me.ppresent_address1 = objEmp._Present_Address1
                    Me.ppresent_address2 = objEmp._Present_Address2
                    Me.ppresent_mobile = objEmp._Present_Mobile

                    Dim objShift As New Aruti.Data.clsNewshift_master
                    objShift._Shiftunkid = objEmp._Shiftunkid
                    Me.pShiftName = objShift._Shiftname
                    objShift = Nothing

                    Dim objDept As New Aruti.Data.clsDepartment
                    objDept._Departmentunkid = objEmp._Departmentunkid
                    Me.pDepartmentName = objDept._Name
                    objDept = Nothing

                    'Sohail (24 Nov 2016) -- Start
                    'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
                    Me.pTheme_Id = objEmp._Theme_Id
                    Me.pLastView_Id = objEmp._LastView_Id
                    'Sohail (24 Nov 2016) -- End

                    HttpContext.Current.Session("E_UserID") = -1
                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE   CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'HttpContext.Current.Session("E_Employeeunkid") = objEmp._Employeeunkid
                    HttpContext.Current.Session("E_Employeeunkid") = objEmp._Employeeunkid(eZeeDate.convertDate(HttpContext.Current.Session("EmployeeAsOnDate")))
                    'Shani(20-Nov-2015) -- End

                    'Sohail (31 May 2018) -- Start
                    'TANAPA Enhancement - Ref # 247 : Show Online Recruitment Internal Vacancy link in ESS in 72.1.
                    HttpContext.Current.Session("E_Employeecode") = objEmp._Employeecode
                    'Sohail (31 May 2018) -- End

                    HttpContext.Current.Session("E_UserName") = "ID " & " : " & objEmp._Employeecode & vbCrLf & "Employee : " & objEmp._Firstname & " " & objEmp._Surname 'objEmp._Displayname
                    HttpContext.Current.Session("E_Password") = objEmp._Password
                    HttpContext.Current.Session("E_LeaveBalances") = 0
                    HttpContext.Current.Session("E_MemberName") = "Emp. : (" & objEmp._Employeecode & ") " & objEmp._Firstname & " " & objEmp._Surname
                    HttpContext.Current.Session("E_RoleID") = 0
                    HttpContext.Current.Session("E_LangId") = 1 ' by default set custom1 for employee login for all. 
                    HttpContext.Current.Session("E_Firstname") = objEmp._Firstname
                    HttpContext.Current.Session("E_Surname") = objEmp._Surname
                    'Sohail (04 Feb 2020) -- Start
                    'NMB Enhancement # : New screen "Non-Disclosure Declaration" with lock / unlock options.
                    HttpContext.Current.Session("E_Othername") = objEmp._Othername
                    'Sohail (04 Feb 2020) -- End
                    HttpContext.Current.Session("E_DisplayName") = objEmp._Displayname
                    HttpContext.Current.Session("E_Appointeddate") = objEmp._Appointeddate
                    HttpContext.Current.Session("E_Gender") = Me.PGender
                    HttpContext.Current.Session("E_Birthdate") = objEmp._Birthdate.Date
                    HttpContext.Current.Session("E_Email") = objEmp._Email
                    HttpContext.Current.Session("E_Present_Address1") = objEmp._Present_Address1
                    HttpContext.Current.Session("E_Present_Address2") = objEmp._Present_Address2
                    HttpContext.Current.Session("E_Present_Mobile") = objEmp._Present_Mobile
                    HttpContext.Current.Session("E_Shiftname") = Me.pShiftName
                    HttpContext.Current.Session("E_DepartmentName") = Me.pDepartmentName
                    'Sohail (24 Nov 2016) -- Start
                    'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
                    HttpContext.Current.Session("E_Theme_id") = objEmp._Theme_Id
                    HttpContext.Current.Session("E_Lastview_id") = objEmp._LastView_Id
                    HttpContext.Current.Session("Theme_id") = objEmp._Theme_Id
                    HttpContext.Current.Session("Lastview_id") = objEmp._LastView_Id
                    'Sohail (24 Nov 2016) -- End

                    'S.SANDEEP |08-JAN-2019| -- START
                    HttpContext.Current.Session("E_Email") = objEmp._Email
                    'S.SANDEEP |08-JAN-2019| -- END

                    Return True

                Else
                    mstrMessage = "Invalid Username or Password!! Please try again !!!"
                    Return False
                End If

            End If

            Return True

        Catch ex As Exception
            Throw New Exception("ClsDataOperation-->CheckLogin function" & ex.Message)
        End Try

    End Function
    'Sohail (30 Mar 2015) -- End

    'Pinkal (12-Feb-2015) -- End


    'S.SANDEEP [ 06 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Private Function IsAccessGiven(ByVal intUserId As Integer, ByVal intYearId As Integer) As Boolean
    Public Function IsAccessGiven(ByVal intUserId As Integer, ByVal intYearId As Integer) As Boolean 'S.SANDEEP [ 14 DEC 2012 ] -- START -- END

        Dim StrQ As String = String.Empty
        Dim dList As New DataSet
        Try
            StrQ = "SELECT * FROM hrmsConfiguration..cfcompanyaccess_privilege " & _
                   " WHERE userunkid = @userunkid AND yearunkid = @yearunkid "
            'Sohail (13 Dec 2018) - [" & intUserId & "] = [@userunkid], [" & intYearId & "] = [@yearunkid]

            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement : Security issues in Self Service in 75.1                
            clsDataOperation.ClearParameters()
            clsDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            clsDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            'Sohail (13 Dec 2018) -- End
            dList = clsDataOperation.WExecQuery(StrQ, "Lst")

            If clsDataOperation.ErrorMessage <> "" Then
                Throw New Exception("ClsDataOperation-->IsAccessGiven Function" & clsDataOperation.ErrorNumber & " : " & clsDataOperation.ErrorMessage)
            End If

            If dList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Throw New Exception("ClsDataOperation-->IsAccessGiven Function" & ex.Message)
        End Try
    End Function
    'S.SANDEEP [ 06 DEC 2012 ] -- END

    'S.SANDEEP [ 14 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function UpdateEmpData(ByVal iUserId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim dList As New DataSet
        Try
            StrQ = "SELECT " & _
                                 "hrmsConfiguration..cfuser_master.username " & _
                                 ",employeeunkid " & _
                                 ",database_name " & _
                            "FROM hrmsConfiguration..cfuser_master " & _
                                 "JOIN hrmsConfiguration..cfcompany_master ON hrmsConfiguration..cfuser_master.companyunkid = hrmsConfiguration..cfcompany_master.companyunkid " & _
                                 "JOIN hrmsConfiguration..cffinancial_year_tran ON hrmsConfiguration..cfcompany_master.companyunkid = hrmsConfiguration..cffinancial_year_tran.companyunkid " & _
                            "WHERE hrmsConfiguration..cfuser_master.isactive = 1 AND userunkid = @userunkid AND isclosed = 0 "
            'Sohail (13 Dec 2018) - ['" & iUserId & "'] = [@userunkid]

            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement : Security issues in Self Service in 75.1
            clsDataOperation.ClearParameters()
            clsDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iUserId)
            'Sohail (13 Dec 2018) -- End
            dList = clsDataOperation.WExecQuery(StrQ, "Lst")

            If clsDataOperation.ErrorMessage <> "" Then
                Throw New Exception("ClsDataOperation-->IsAccessGiven Function" & clsDataOperation.ErrorNumber & " : " & clsDataOperation.ErrorMessage)
            End If
            If dList.Tables(0).Rows.Count > 0 Then
                StrQ = "UPDATE " & dList.Tables(0).Rows(0).Item("database_name") & "..hremployee_master SET displayname = '" & dList.Tables(0).Rows(0).Item("username") & "' WHERE employeeunkid = " & dList.Tables(0).Rows(0).Item("employeeunkid")
                clsDataOperation.ExecNonQuery(StrQ)
            End If

        Catch ex As Exception
            Throw New Exception("ClsDataOperation-->UpdateEmpData Function" & ex.Message)
        End Try
    End Function
    'S.SANDEEP [ 14 DEC 2012 ] -- END


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

    Public Sub GetLeaveBalance(ByVal intEmpId As Integer, ByVal strDBname As String, ByVal intYearID As Integer)
        Try

        Dim StrQ As String = "SELECT " & _
                                    "  lvleavebalance_tran.leavetypeunkid " & _
                                    ",ISNULL(lvleavetype_master.leavename,'') AS Leave " & _
                                    "  ,ISNULL(CONVERT(CHAR(8), startdate, 112),'') AS StDate " & _
                                    "  ,ISNULL(CONVERT(CHAR(8), enddate, 112),'') AS EdDate " & _
                                    "  ,CONVERT(DECIMAL(10,2),actualamount) AS actualamount " & _
                                    "  ,CONVERT(DECIMAL(10,2),accrue_amount) AS Acc_Amt " & _
                                    "  ,CONVERT(DECIMAL(10,2),issue_amount) AS Iss_Amt " & _
                                    "  ,CONVERT(DECIMAL(10,2),remaining_bal) AS Balance " & _
                                    "  , CONVERT(DECIMAL(10,2),ISNULL(leavebf,0)) AS LeaveAmountBF " & _
                                    "  , ISNULL(" & strDBname & "..lvleavebalance_tran.isshortleave,0)  AS isshortleave " & _
                                    "  , CONVERT(DECIMAL(10,2),ISNULL(adj_remaining_bal,0)) AS AdjustmentLeave " & _
                                    "  , 0.00 as balanceasondate " & _
                                    "FROM " & strDBname & "..lvleavebalance_tran " & _
                                    "  JOIN " & strDBname & "..lvleavetype_master ON " & strDBname & "..lvleavebalance_tran.leavetypeunkid = " & strDBname & "..lvleavetype_master.leavetypeunkid " & _
                                    "WHERE employeeunkid = @employeeunkid AND isvoid = 0 AND lvleavebalance_tran.yearunkid =  @yearunkid "
        'Sohail (13 Dec 2018) - [" & intEmpId & "] = [@employeeunkid], [" & intYearId & "] = [@yearunkid]


        'Pinkal (03-May-2019) --   'Enhancement - Working on Leave UAT Changes for NMB.[ "  ,CONVERT(DECIMAL(10,2),actualamount) AS actualamount " & _]

        If HttpContext.Current.Session("LeaveBalanceSetting") = enLeaveBalanceSetting.Financial_Year Then
            StrQ &= " AND isclose_fy = 0 "
        ElseIf HttpContext.Current.Session("LeaveBalanceSetting") = enLeaveBalanceSetting.ELC Then
            StrQ &= " AND iselc = 1 AND isopenelc = 1 "
        End If

        StrQ &= " ORDER BY ISNULL(lvleavetype_master.leavename,'') "

                
        clsDataOperation.ClearParameters()
        clsDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
        clsDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearID)
        Me.pLeaveBalances = clsDataOperation.WExecQuery(StrQ, "lvleavebalance_tran").Tables(0)

        If Me.pLeaveBalances.Columns.Contains("Yearly Accrue Entitled") = False Then
            Me.pLeaveBalances.Columns.Add("Yearly Accrue Entitled", GetType(System.Decimal)).DefaultValue = 0
        End If
        
        Dim objLeaveBalance As New clsleavebalance_tran


        If Me.pLeaveBalances.Rows.Count > 0 Then

            'Pinkal (07-Jun-2019) -- Start
            'Bug [0003871]-Epiroc - Issued leave balance from SS leave balance screen differs from Leave accrue screen.
            Dim objLvType As New clsleavetype_master
            Dim mstrLeaveType As String = ""
            'Pinkal (07-Jun-2019) -- End

            For i As Integer = 0 To Me.pLeaveBalances.Rows.Count - 1

                'Pinkal (07-Jun-2019) -- Start
                'Bug [0003871]-Epiroc - Issued leave balance from SS leave balance screen differs from Leave accrue screen.
                mstrLeaveType = Me.pLeaveBalances(i)("leavetypeunkid").ToString()
                Dim mstrFromDeductionLeaveTypeId As String = objLvType.GetDeductdToLeaveTypeIDs(CInt(Me.pLeaveBalances(i)("leavetypeunkid")))

                If mstrFromDeductionLeaveTypeId.Length > 0 Then
                    mstrLeaveType &= "," & mstrFromDeductionLeaveTypeId
                End If
                'Pinkal (07-Jun-2019) -- End

                Me.pLeaveBalances.Rows(i)("Yearly Accrue Entitled") = CDec(Me.pLeaveBalances.Rows(i)("Acc_Amt"))

                Dim dsLeaveList As DataSet = Nothing
                If HttpContext.Current.Session("LeaveBalanceSetting") = enLeaveBalanceSetting.Financial_Year Then

                    'Pinkal (07-Jun-2019) -- Start
                    'Bug [0003871]-Epiroc - Issued leave balance from SS leave balance screen differs from Leave accrue screen.
                    'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, Me.pLeaveBalances(i)("leavetypeunkid"), intEmpId _
                    '                                                                             , HttpContext.Current.Session("LeaveAccrueTenureSetting"), HttpContext.Current.Session("LeaveAccrueDaysAfterEachMonth") _
                    '                                                                             , HttpContext.Current.Session("fin_startdate"), HttpContext.Current.Session("fin_enddate"), False, False, HttpContext.Current.Session("LeaveBalanceSetting"), 0, CInt(HttpContext.Current.Session("Fin_year")))

                    dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, mstrLeaveType, intEmpId _
                                                                                                 , HttpContext.Current.Session("LeaveAccrueTenureSetting"), HttpContext.Current.Session("LeaveAccrueDaysAfterEachMonth") _
                                                                                                 , HttpContext.Current.Session("fin_startdate"), HttpContext.Current.Session("fin_enddate"), False, False, HttpContext.Current.Session("LeaveBalanceSetting"), 0, CInt(HttpContext.Current.Session("Fin_year")))

                    'Pinkal (07-Jun-2019) -- End

                ElseIf HttpContext.Current.Session("LeaveBalanceSetting") = enLeaveBalanceSetting.ELC Then

                    Dim mdtAsonDate As Date = Nothing

                    If ConfigParameter._Object._CurrentDateAndTime.Date > eZeeDate.convertDate(Me.pLeaveBalances(i)("EdDate").ToString()).Date Then
                        mdtAsonDate = eZeeDate.convertDate(Me.pLeaveBalances(i)("EdDate").ToString()).Date
                    Else
                        mdtAsonDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    End If

                    objLeaveBalance._DBStartdate = CDate(HttpContext.Current.Session("fin_startdate")).Date
                    objLeaveBalance._DBEnddate = CDate(HttpContext.Current.Session("fin_enddate")).Date


                    'Pinkal (23-Jan-2019) -- Start
                    'Bug [Ref #: 0003286 HJFMRI KENYA] - Leave balance figures displaying on Self service not tallying up hence employee unable to apply for leave.
                    'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtAsonDate.Date, Me.pLeaveBalances(i)("leavetypeunkid"), intEmpId _
                    '                                                                        , HttpContext.Current.Session("LeaveAccrueTenureSetting"), HttpContext.Current.Session("LeaveAccrueDaysAfterEachMonth") _
                    '                                                                        , eZeeDate.convertDate(Me.pLeaveBalances(i)("StDate").ToString()).Date, eZeeDate.convertDate(Me.pLeaveBalances(i)("EdDate").ToString()).Date, True, True, HttpContext.Current.Session("LeaveBalanceSetting"), 0, CInt(HttpContext.Current.Session("Fin_year")))

                    'Pinkal (07-Jun-2019) -- Start
                    'Bug [0003871]-Epiroc - Issued leave balance from SS leave balance screen differs from Leave accrue screen.

                    'dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtAsonDate.Date, Me.pLeaveBalances(i)("leavetypeunkid"), intEmpId _
                    '                                                                        , HttpContext.Current.Session("LeaveAccrueTenureSetting"), HttpContext.Current.Session("LeaveAccrueDaysAfterEachMonth") _
                    '                                                                       , Nothing, Nothing, True, True, HttpContext.Current.Session("LeaveBalanceSetting"), 0, CInt(HttpContext.Current.Session("Fin_year")))

                    dsLeaveList = objLeaveBalance.GetLeaveBalanceInfo(mdtAsonDate.Date, mstrLeaveType, intEmpId _
                                                                                            , HttpContext.Current.Session("LeaveAccrueTenureSetting"), HttpContext.Current.Session("LeaveAccrueDaysAfterEachMonth") _
                                                                                           , Nothing, Nothing, True, True, HttpContext.Current.Session("LeaveBalanceSetting"), 0, CInt(HttpContext.Current.Session("Fin_year")))

                    'Pinkal (07-Jun-2019) -- End


                    'Pinkal (23-Jan-2019) -- End


                End If

                If dsLeaveList.Tables(0).Rows.Count > 0 Then

                    If CBool(Me.pLeaveBalances.Rows(i)("isshortleave")) AndAlso CDec(dsLeaveList.Tables(0).Rows(0).Item("Accrue_amount")) <= 0 Then
                        Me.pLeaveBalances(i)("Acc_Amt") = Math.Round(CDec(Me.pLeaveBalances.Rows(i).Item("Acc_Amt")), 2).ToString("#0.00")
                    Else
                        Me.pLeaveBalances(i)("Acc_Amt") = CDec(Math.Round(dsLeaveList.Tables(0).Rows(0).Item("Accrue_amount"), 2)).ToString("#0.00")
                    End If

                    If CBool(Me.pLeaveBalances.Rows(i)("isshortleave")) = False AndAlso CDec(dsLeaveList.Tables(0).Rows(0).Item("Issue_amount")) > 0 Then
                        Me.pLeaveBalances(i)("Iss_Amt") = Math.Round(dsLeaveList.Tables(0).Rows(0).Item("Issue_amount"), 2)
                    End If

                    'Pinkal (03-May-2019) -- Start
                    'Enhancement - Working on Leave UAT Changes for NMB.
                    If Me.pLeaveBalances(i)("isshortleave") Then
                        Me.pLeaveBalances(i)("balanceasondate") = CDec(CDec(Me.pLeaveBalances(i)("actualamount")) + CDec(dsLeaveList.Tables(0).Rows(0)("LeaveAdjustment")) + CDec(dsLeaveList.Tables(0).Rows(0)("LeaveBF")) - (CDec(dsLeaveList.Tables(0).Rows(0)("Issue_amount")) + CDec(dsLeaveList.Tables(0).Rows(0)("LeaveEncashment")))).ToString("#0.00")
                    Else
                    Me.pLeaveBalances(i)("balanceasondate") = CDec(CDec(dsLeaveList.Tables(0).Rows(0)("Accrue_amount")) + CDec(dsLeaveList.Tables(0).Rows(0)("LeaveAdjustment")) + CDec(dsLeaveList.Tables(0).Rows(0)("LeaveBF")) - (CDec(dsLeaveList.Tables(0).Rows(0)("Issue_amount")) + CDec(dsLeaveList.Tables(0).Rows(0)("LeaveEncashment")))).ToString("#0.00")
                    End If
                    'Pinkal (03-May-2019) -- End


                    Me.pLeaveBalances(i)("Balance") = Math.Round(CDec(dsLeaveList.Tables(0).Rows(0).Item("balance")), 2).ToString("#0.00")
                End If

            Next

            Me.pLeaveBalances.AcceptChanges()

        End If

        Catch ex As Exception
            CommonCodes.LogErrorOnly(ex)
        End Try
    End Sub

    'Pinkal (11-Sep-2020) -- End




    'Private Function CheckLogin(ByVal pusername As String, ByVal password As String, ByVal logintype As en_loginby, ByVal selecteddatabase As String, Optional ByVal intUserId As Integer = -1) As String
    '    Dim ds As New DataSet
    '    Dim objUser As New Aruti.Data.clsUserAddEdit
    '    Dim objEmp As New Aruti.Data.clsEmployee_Master
    '    Dim clsDataOperation As New eZeeCommonLib.clsDataOperation
    '    Try
    '        If logintype = en_loginby.Employee Then

    '            clsDataOperation.eZeeCommonLib.eZeeDatabase.change_database(Me.SelectedDatabase)

    '            'sql = "select convert(int,-1) as userunkid,employeeunkid,displayname as displayname,password,Firstname,surname, appointeddate,gender, birthdate, " & vbCrLf
    '            'sql &= "email, present_address1, present_address2, present_mobile, isnull(s.shiftunkid,0) as shiftunkid, isnull(d.departmentunkid,0) as departmentunkid " & vbCrLf
    '            'sql &= " from  " & Me.SelectedDatabase & "" & "hrEmployee_master emp " & vbCrLf
    '            'sql &= "left outer join " & Me.SelectedDatabase & "hrdepartment_master d on emp.departmentunkid=d.departmentunkid" & vbCrLf
    '            'sql &= "left outer join " & Me.SelectedDatabase & "tnashift_master s on emp.shiftunkid=s.shiftunkid " & vbCrLf
    '            'sql &= "where displayname ='" & pusername & "'  and password='" & password & "'" & vbCrLf
    '            'ds = clsdataopr.WExecQuery(sql, "hrEmployee_master")

    '            objEmp._Employeeunkid = intUserId

    '        Else

    '            'S.SANDEEP [ 30 May 2011 ] -- START
    '            'sql = "select convert(int,userunkid) as  userunkid,convert(int,-1) as employeeunkid, username as displayname,password from hrmsConfiguration..cfuser_master where username ='" & pusername & "' and password='" & password & "'"
    '            'ds = clsdataopr.WExecQuery(sql, "cfuser_master")
    '            objUser._Userunkid = intUserId

    '            If password <> objUser._Password Then
    '                Return False
    '            End If
    '            'S.SANDEEP [ 30 May 2011 ] -- END 

    '        End If
    '        If logintype = en_loginby.Employee Then
    '            'If ds.Tables(0).Rows.Count > 0 Then
    '            'Me.UserID = Val(ds.Tables(0).Rows(0)("userunkid"))
    '            'Me.Employeeunkid = Val(ds.Tables(0).Rows(0)("employeeunkid"))
    '            'Me.password = ds.Tables(0).Rows(0)("Password")
    '            'Me.UserName = ds.Tables(0).Rows(0)("displayname")
    '            'Me.pFirstName = ds.Tables(0).Rows(0)("FirstName")
    '            'Me.pSurName = ds.Tables(0).Rows(0)("SurName")
    '            'Me.Pappointeddate = ds.Tables(0).Rows(0)("appointeddate")
    '            'Me.PGender = ds.Tables(0).Rows(0)("gender")
    '            'Me.pbirthdate = ds.Tables(0).Rows(0)("birthdate")
    '            'Me.pEmail = ds.Tables(0).Rows(0)("email")
    '            'Me.ppresent_address1 = ds.Tables(0).Rows(0)("present_address1")
    '            'Me.ppresent_address2 = ds.Tables(0).Rows(0)("present_address2")
    '            'Me.ppresent_mobile = ds.Tables(0).Rows(0)("present_mobile")
    '            'Me.pshiftunkid = Val(ds.Tables(0).Rows(0)("shiftunkid"))
    '            'Me.pdepartmentunkid = Val(ds.Tables(0).Rows(0)("departmentunkid"))

    '            Me.UserID = -1
    '            Me.Employeeunkid = objEmp._Employeeunkid
    '            Me.password = objEmp._Password
    '            Me.UserName = objEmp._Displayname
    '            Me.pFirstName = objEmp._Firstname
    '            Me.pSurName = objEmp._Surname
    '            Me.Pappointeddate = objEmp._Appointeddate
    '            Select Case objEmp._Gender
    '                Case 0  'Male
    '                    Me.PGender = "Male"
    '                Case 1 'Female
    '                    Me.PGender = "Female"
    '                Case Else
    '                    Me.PGender = ""
    '            End Select
    '            Me.pbirthdate = objEmp._Birthdate.Date
    '            Me.pEmail = objEmp._Email
    '            Me.ppresent_address1 = objEmp._Present_Address1
    '            Me.ppresent_address2 = objEmp._Present_Address2
    '            Me.ppresent_mobile = objEmp._Present_Mobile

    '            Dim objShift As New Aruti.Data.clsshift_master
    '            objShift._Shiftunkid = objEmp._Shiftunkid
    '            Me.pShiftName = objShift._Shiftname
    '            objShift = Nothing


    '            Dim objDept As New Aruti.Data.clsDepartment
    '            objDept._Departmentunkid = objEmp._Departmentunkid
    '            Me.pDepartmentName = objDept._Name
    '            objDept = Nothing

    '            'S.SANDEEP [ 30 May 2011 ] -- START
    '            'ISSUE : FINCA REQ.
    '            'Me.pLeaveBalances = clsdataopr.WExecQuery("select * from " & Me.SelectedDatabase & "..lvleavebalance_tran", "lvleavebalance_tran").Tables(0)
    '            Me.pLeaveBalances = clsdataopr.WExecQuery("SELECT ISNULL(lvleavetype_master.leavename,'') AS leavename,CONVERT(CHAR(10), startdate, 103) AS startdate,CONVERT(CHAR(10), enddate, 103) AS enddate,CONVERT(DECIMAL(10,2),accrue_amount) AS accrue_amount,CONVERT(DECIMAL(10,2),issue_amount) AS issue_amount,CONVERT(DECIMAL(10,2),remaining_bal) AS remaining_bal,lvleavebalance_tran.ispaid,CASE WHEN accruesetting = 1 THEN 'Issue Balance With Paid'	WHEN accruesetting = 2 THEN 'Exceeding Leave Balance with Unpaid' END AS accruesetting FROM lvleavebalance_tran JOIN lvleavetype_master ON lvleavebalance_tran.leavetypeunkid = lvleavetype_master.leavetypeunkid WHERE employeeunkid = '" & objEmp._Employeeunkid & "'", "lvleavebalance_tran").Tables(0)
    '            'S.SANDEEP [ 30 May 2011 ] -- END 
    '            Return True
    '            'Else
    '        Else
    '            'If ds.Tables(0).Rows.Count > 0 Then
    '            'Me.UserID = Val(ds.Tables(0).Rows(0)("userunkid"))
    '            'Me.Employeeunkid = Val(ds.Tables(0).Rows(0)("employeeunkid"))
    '            'Me.password = ds.Tables(0).Rows(0)("Password")
    '            'Me.UserName = ds.Tables(0).Rows(0)("displayname")

    '            Me.UserID = objUser._Userunkid
    '            Me.Employeeunkid = -1
    '            Me.password = objUser._Password
    '            Me.UserName = objUser._Username

    '            Return True
    '            'Else

    '            'Return False
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception("Error in clsDataOperation-->CheckLogin function" & ex.Message)
    '    End Try
    'End Function


    'S.SANDEEP [ 20 NOV 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Get_Languages(ByVal StrDBName As String, ByVal StrFrmName As String) As DataTable
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Try
            StrQ = "SELECT * " & _
                   "FROM " & StrDBName & "..cflanguage " & _
                   "WHERE formname LIKE @formname " & _
                   "AND controlname LIKE 'lbl%' "
            'Sohail (13 Dec 2018) - ['" & StrFrmName & "'] = [@formname]

            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement : Security issues in Self Service in 75.1                
            clsDataOperation.ClearParameters()
            clsDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrFrmName)
            'Sohail (13 Dec 2018) -- End
            dsList = clsDataOperation.WExecQuery(StrQ, "List")

            dtTable = dsList.Tables(0).Copy

            Return dtTable
        Catch ex As Exception
            Throw New Exception("Get_Languages function : " & ex.Message)
        End Try
    End Function
    'S.SANDEEP [ 20 NOV 2012 ] -- END

    'S.SANDEEP [10 DEC 2015] -- START
    Private Function IsAssessorMappedWithLoginUser(ByVal intUserId As Integer, ByVal strDBname As String) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Try
            StrQ = "SELECT " & _
                   "    assessormasterunkid AS AsrId " & _
                   "FROM " & strDBname & "..hrassessor_master " & _
                   "    JOIN " & strDBname & "..hrapprover_usermapping ON hrapprover_usermapping.approverunkid = assessormasterunkid " & _
                   "WHERE isreviewer = 0 AND isvoid = 0 AND usertypeid = 2 AND hrapprover_usermapping.userunkid = @userunkid "
            'Sohail (13 Dec 2018) - [" & intUserId & "'] = [@userunkid]

            'Sohail (13 Dec 2018) -- Start
            'NMB Enhancement : Security issues in Self Service in 75.1                
            clsDataOperation.ClearParameters()
            clsDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            'Sohail (13 Dec 2018) -- End
            dsList = clsDataOperation.WExecQuery(StrQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                blnFlag = True
            End If

        Catch ex As Exception
            Throw New Exception("IsAssessorMappedWithLoginUser function : " & ex.Message)
        End Try
        Return blnFlag
    End Function
    'S.SANDEEP [10 DEC 2015] -- END

End Class
