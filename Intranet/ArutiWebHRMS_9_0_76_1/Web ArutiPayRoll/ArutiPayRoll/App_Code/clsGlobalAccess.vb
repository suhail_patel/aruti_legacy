﻿'Imports System.Data
'Imports System.Data.SqlClient
'Imports Microsoft.VisualBasic
'Imports System.IO
'Imports System.Configuration

Public Class GlobalAccess
    Private yearUnkID As Integer
    Public Shared phrdbName As String = "hrmsConfiguration.."

    Public Property yearID() As Integer
        Get
            Return yearUnkID
        End Get
        Set(ByVal value As Integer)
            yearUnkID = value
        End Set
    End Property
    Private CompanyunkID As Integer
    Public Property companyid() As Integer
        Get
            Return CompanyunkID
        End Get
        Set(ByVal value As Integer)
            CompanyunkID = value
        End Set
    End Property
    Private userunkid As Integer
    Public Property userid() As Integer
        Get
            Return userunkid
        End Get
        Set(ByVal value As Integer)
            userunkid = value
        End Set
    End Property
    Private employeeunkid As Integer
    Public Property employeeid() As Integer
        Get
            Return employeeunkid
        End Get
        Set(ByVal value As Integer)
            employeeunkid = value
        End Set
    End Property
    Private employeecode As String
    Public Property empcode() As String
        Get
            Return employeecode
        End Get
        Set(ByVal value As String)
            employeecode = value
        End Set
    End Property
    Private financialyear_name As String
    Public Property yearname() As String
        Get
            Return financialyear_name
        End Get
        Set(ByVal value As String)
            financialyear_name = value
        End Set
    End Property

    Private pListOfEmployee As System.Data.DataTable
    Public Property ListOfEmployee() As System.Data.DataTable
        Get
            Return pListOfEmployee
        End Get
        Set(ByVal value As System.Data.DataTable)
            pListOfEmployee = value
        End Set
    End Property

    Private pStartDate As Date
    Public Property StartDate() As Date
        Get
            Return pStartDate
        End Get
        Set(ByVal value As Date)
            pStartDate = value
        End Set
    End Property

    Private pEndDate As Date
    Public Property EndDate() As Date
        Get
            Return pEndDate
        End Get
        Set(ByVal value As Date)
            pEndDate = value
        End Set
    End Property

    Public Sub LogError()
        'MsgBox("Error in ", MsgBoxStyle.Critical)
    End Sub

End Class
