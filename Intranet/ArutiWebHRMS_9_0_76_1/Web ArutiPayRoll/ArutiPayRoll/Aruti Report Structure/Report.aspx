﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Report.aspx.vb" Inherits="Reports_Report" Theme="blue" ClientTarget="uplevel" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   
    <script type="text/javascript">
    
      function pageLoad() {
      }
    
    </script>
</head>
<body style="background-image:none;background-color:White;">
    <form id="form1" runat="server">
    <div>
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <CR:CrystalReportViewer ID="crViewer" runat="server" AutoDataBind="true" 
            ReportSourceID="CrystalReportSource1" DisplayGroupTree="False"  />
         <CR:CrystalReportSource ID="CrystalReportSource1" 
                          runat="server">
            <Report FileName="CrystalReport.rpt">
            </Report>
         </CR:CrystalReportSource>
         
        <%-- <asp:Panel ID="Panel1" runat="server" Width="150px" Height="30px"  CssClass="shadow" BackImageUrl="~/images/bg_titlebar.gif" >
              <%--<div style="text-align:center; vertical-align:middle; color:White;cursor:pointer;font-size:10pt;font-weight:bold;padding-top:6px;" onclick="javascript:location.href='../UserHome.aspx'">--%>
              <%--<div style="text-align:center; vertical-align:middle; color:White;cursor:pointer;font-size:10pt;font-weight:bold;padding-top:6px;" onclick="javascript:history.back(); return false;">              
                <%--<asp:HyperLink ID="hlCloseReport" runat="server" Text="Close Report" NavigateUrl="../UserHome.aspx" />--%>
                <%--Close Report
              </div>
         </asp:Panel>  --%>              
         
         <%-- S.SANDEEP [31 DEC 2015] -- START REPORT IN POPUP NO NEED FOR CLOSE BUTTON --%>      
         <%--<div class="btndefault" style="position:fixed;top:200px;right:3px;" onclick="javascript:document.location.href = '<%= ViewState("referrer") %>'; return false;" >Close Report</div>--%>
         <%-- S.SANDEEP [31 DEC 2015] -- END REPORT IN POPUP NO NEED FOR CLOSE BUTTON --%>      
         
         <%--<cc1:AlwaysVisibleControlExtender ID="avc" runat="server" 
            TargetControlID="Panel1" VerticalOffset="10" HorizontalOffset="20" 
            ScrollEffectDuration=".1" HorizontalSide="Right" VerticalSide="Middle" />--%>
    </div>
    </form>
</body>
</html>
