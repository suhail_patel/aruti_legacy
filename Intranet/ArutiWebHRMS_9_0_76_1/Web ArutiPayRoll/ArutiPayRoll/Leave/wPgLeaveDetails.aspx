﻿<%@ Page Title="Leave Details" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgLeaveDetails.aspx.vb" Inherits="wPgLeaveDetails" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Leave Details"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <table style="width: 50%;">
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 80%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblLeaveInfo" runat="server" Text="Leave Info"></asp:Label>
                                    </div>
                                </div>
                                
                                  <%--Pinkal (26-Feb-2019) -- Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                
                                  
                                <%--'Pinkal (03-May-2019) -- Start
                                       'Enhancement - Working on Leave UAT Changes for NMB.--%>
                                         
                                <div id="Div2" class="panel-body-default">
                                    <asp:Panel ID="pnl_dgView" runat="server" Height="350px" ScrollBars="Auto">
                                        <asp:GridView ID="GvLeaveDetails" runat="server" AutoGenerateColumns="False" Width="99%"
                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                            <Columns>
                                            
                                                <asp:BoundField DataField="Leave" HeaderText="Leave Type" FooterText = "dgcolhLvDetailLvType">
                                                    <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Yearly Accrue Entitled" HeaderText="Yearly Accrue Entitled" FooterText = "dgcolhLvDetailLvAccrueEntitled">
                                                    <HeaderStyle HorizontalAlign="Right" Width="120px" />
                                                    <ItemStyle HorizontalAlign="Right" Width="120px"  />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="StDate" HeaderText="Start Date" FooterText = "dgcolhLvDetailLvStartDate">
                                                    <HeaderStyle HorizontalAlign="Left" Width="120px" />
                                                    <ItemStyle HorizontalAlign="Left" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EdDate" HeaderText="End Date"  FooterText = "dgcolhLvDetailLvEndDate">
                                                    <HeaderStyle HorizontalAlign="Left" Width="120px" />
                                                    <ItemStyle HorizontalAlign="Left" Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="LeaveAmountBF" HeaderText="Leave B/F" FooterText = "dgcolhLvDetailLvLeaveBF">
                                                    <HeaderStyle HorizontalAlign="Right" Width="80px" />
                                                    <ItemStyle HorizontalAlign="Right" Width="80px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Acc_Amt" HeaderText="As On Accrued Days" FooterText = "dgcolhLvDetailLvAsonAccrueDays">
                                                    <HeaderStyle HorizontalAlign="Right" Width="150px" />
                                                    <ItemStyle HorizontalAlign="Right" Width="150px"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Iss_Amt" HeaderText="Issued Days" FooterText = "dgcolhLvDetailLvIssueDays">
                                                    <HeaderStyle HorizontalAlign="Right" Width="90px" />
                                                    <ItemStyle HorizontalAlign="Right" Width="90px"  />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AdjustmentLeave" HeaderText="Leave Adjustment" FooterText = "dgcolhLvDetailLvLeaveAdjustment">
                                                    <HeaderStyle HorizontalAlign="Right"  Width="120px" />
                                                    <ItemStyle HorizontalAlign="Right"  Width="120px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="balanceasondate" HeaderText="Balance as on date" FooterText = "dgcolhLvDetailLvBalAsonDate">
                                                    <HeaderStyle HorizontalAlign="Right" Width="140px" />
                                                    <ItemStyle HorizontalAlign="Right" Width="140px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Balance" HeaderText="Total Balance" FooterText = "dgcolhLvDetailLvTotalBalance">
                                                    <HeaderStyle HorizontalAlign="Right" Width="110px"  />
                                                    <ItemStyle HorizontalAlign="Right" Width="110px"  />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                                
                                <%--'Pinkal (03-May-2019) -- End--%>
                                
                                 <%--Pinkal (26-Feb-2019) -- End--%>
                                
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
