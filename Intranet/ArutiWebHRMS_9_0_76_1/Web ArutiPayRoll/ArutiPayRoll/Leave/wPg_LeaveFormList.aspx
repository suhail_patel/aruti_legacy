﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPg_LeaveFormList.aspx.vb"
    Inherits="Leave_wPg_LeaveFormList" Title="Leave Form List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
			var scroll = {
					Y: '#<%= hfScrollPosition.ClientID %>'
				};
			var scroll1 = {
					Y: '#<%= hfScrollPosition1.ClientID %>'
				};
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
            }

            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
	             if (args.get_error() == undefined) {
                        $("#scrollable-container").scrollTop($(scroll.Y).val());
                      
                }
            }
    </script>

    <script>
        function IsValidAttach() {
            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= CancelFormConfirmation.ClientID %>_Panel1").css("z-index", "100002");
            $("#<%= popup_AttachementYesNo.ClientID %>_Panel1").css("z-index", "100005");
        }
    </script>
    
         
    <script>
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPg_LeaveFormList.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $('input[type=file]').live("click", function() {
            return IsValidAttach();
        });
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="panel-primary">
                    <div class="panel-heading">
                        <asp:Label ID="lblPageHeader" runat="server" Text="Leave Form List"></asp:Label>
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 13%">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee:"></asp:Label>
                                        </td>
                                        <td colspan="3" style="width: 46%">
                                            <asp:DropDownList ID="ddlFilEmpList" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 13%">
                                            <asp:Label ID="lblStatus" runat="server" Text="Status:"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:DropDownList ID="status1" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 13%">
                                            <asp:Label ID="lblApprover" Enabled="true" runat="server" Text="Approver:"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:TextBox ID="txtApprover" CssClass="textboxfil" runat="server" Enabled="False"></asp:TextBox>
                                        </td>
                                        <td style="width: 13%">
                                            <asp:Label ID="lblFormNo" runat="server" Text="Application No:"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:TextBox ID="txtdrpformNo" CssClass="textboxfil" runat="server"></asp:TextBox>
                                        </td>
                                        <td style="width: 13%">
                                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date:"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <uc2:DateCtrl ID="dtfilStartDate" runat="server" AutoPostBack="false" />
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 13%">
                                            <asp:Label ID="lblLeaveType" runat="server" Text="Leave:"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <asp:DropDownList ID="ddlfilLeave" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 13%">
                                            <asp:Label ID="lblApplyDate" runat="server" Text="Apply Date:"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <uc2:DateCtrl ID="dtfilApplyDate" runat="server" AutoPostBack="false" />
                                        </td>
                                        <td style="width: 13%">
                                            <asp:Label ID="lblReturnDate" runat="server" Text="End Date:"></asp:Label>
                                        </td>
                                        <td style="width: 20%">
                                            <uc2:DateCtrl ID="dtfilReturnDate" runat="server" AutoPostBack="false" />
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                    <asp:Button ID="BtnNew" CssClass="btndefault" runat="server" Text="New" />
                                    <asp:Button ID="BtnSearch" CssClass="btndefault" runat="server" Text="Search" />
                                    <asp:Button ID="Btnclose" runat="server" CssClass=" btnDefault" Text="Close" />
                                </div>
                            </div>
                        </div>
                        <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                            height: 350px; overflow: auto">
                            <asp:DataGrid ID="dgView" runat="server" Width="150%" AutoGenerateColumns="False"
                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <span class="gridiconbc">
                                                <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" CommandName="Select"
                                                    ToolTip="Edit"></asp:LinkButton>
                                            </span>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--0--%>
                                    <asp:TemplateColumn HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <span class="gridiconbc">
                                                <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" CommandName="Delete"
                                                    ToolTip="Delete"></asp:LinkButton>
                                            </span>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--1--%>
                                    <asp:TemplateColumn HeaderText="Cancel" HeaderStyle-HorizontalAlign="Center" FooterText="mnuCancelLeaveForm">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgCancel" runat="server" ImageUrl="~/images/CancelLeaveForm.png"
                                                ToolTip="Cancel Leave" CommandName="Cancel" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--2--%>
                                    <asp:TemplateColumn HeaderText="Print" HeaderStyle-HorizontalAlign="Center" FooterText="mnuPrintForm">
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="ImgPrint" runat="server" ImageUrl="~/images/PrintSummary_16.png"
                                                ToolTip="Print Form" CommandName="Print" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--3--%>
                                    <asp:BoundColumn DataField="formno" HeaderText="Application No" FooterText="colhFormNo" />
                                    <%--4--%>
                                    <asp:BoundColumn DataField="employeecode" HeaderText="Employee Code" FooterText="colhEmpCode" />
                                    <%--5--%>
                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="colhEmployee" />
                                    <%--6--%>
                                    <asp:BoundColumn DataField="leavename" HeaderText="Leave" FooterText="colLeaveType" />
                                    <%--7--%>
                                    <asp:BoundColumn DataField="startdate" HeaderText="Start Date" FooterText="colhStartDate" />
                                    <%--8--%>
                                    <asp:BoundColumn DataField="returndate" HeaderText="End Date" FooterText="colhEndDate" />
                                    <%--9--%>
                                    <asp:BoundColumn DataField="days" HeaderText="Days" FooterText="colhLeaveDays" />
                                    <%--10--%>
                                    <asp:BoundColumn DataField="Approved_StartDate" HeaderText="Approved Start Date"
                                        FooterText="colhApprovedStartDate" />
                                    <%--11--%>
                                    <asp:BoundColumn DataField="Approved_EndDate" HeaderText="Approved End Date" FooterText="colhApprovedEndDate" />
                                    <%--12--%>
                                    <asp:BoundColumn DataField="Approved_Days" HeaderText="Approved Days" HeaderStyle-HorizontalAlign="Right"
                                        ItemStyle-HorizontalAlign="Right" FooterText="colhApprovedDays" />
                                    <%--13--%>
                                    <asp:BoundColumn DataField="status" HeaderText="Status" FooterText="colhStatus" />
                                    <%--14--%>
                                    <asp:BoundColumn DataField="statusunkid" HeaderText="Statusunkid" Visible="false" />
                                    <%--15--%>
                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="EmployeeunkId" Visible="false" />
                                    <%--16--%>
                                    <asp:BoundColumn DataField="leavetypeunkid" HeaderText="LeaveTypeunkId" Visible="false" />
                                    <%--17--%>
                                    
                                    <%-- 'Pinkal (01-Oct-2018) -- Start    'Enhancement - Leave Enhancement for NMB.--%>
                                    <asp:BoundColumn DataField="skipapproverflow" HeaderText="Skip Approver Flow" Visible="false" />
                                    <%--18--%>
                                    <%-- 'Pinkal (01-Oct-2018) -- End --%>
                                    
                                </Columns>
                            </asp:DataGrid>
                        </div>
                    </div>
                </div>
                <uc9:DeleteReason ID="popup_DeleteForm" runat="server" Title="Are you sure you want to delete selected transaction?" />
                <%-- <cc1:ModalPopupExtender ID="popupCancel" BackgroundCssClass="modalBackground" TargetControlID="txtCancelEmployee"
                    runat="server" PopupControlID="pnlCancelPopup" DropShadow="true" CancelControlID="btnCancelClose">
                </cc1:ModalPopupExtender>--%>
                
                <cc1:ModalPopupExtender ID="popupCancel" BackgroundCssClass="modalBackground" TargetControlID="txtCancelEmployee"
                    runat="server" PopupControlID="pnlCancelPopup" DropShadow="true" CancelControlID="btnHiddenLvCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlCancelPopup" runat="server" CssClass="newpopup" Style="display: none; width: 900px">
                    <div class="panel-primary" style="margin-bottom: 0px">
                        <div class="panel-heading">
                            <asp:Label ID="lblCancelText1" Text="Leave Cancel Form" runat="server" />
                        </div>
                        <div class="panel-body">
                            <div id="Div18" class="panel-default">
                                <div id="Div19" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCancelText" Text="Leave Cancel Form" runat="server" />
                                    </div>
                                </div>
                                <div id="Div20" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 15%">
                                                <asp:Label ID="lblCancelEmployeeCode" runat="server" Text="Employee Code" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtCancelEmployeeCode" runat="server" ReadOnly="true" />
                                            </td>
                                            <td style="width: 1%" />
                                            <td align="left" style="width: 15%" />
                                            <td style="width: 15%; padding-left: 10px;" align="left">
                                                <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="True" Text="Select All" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 15%">
                                                <asp:Label ID="lblCancelEmployee" runat="server" Text="Employee" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtCancelEmployee" runat="server" ReadOnly="true" />
                                            </td>
                                            <td style="width: 1%" />
                                            <td align="left" style="width: 15%" valign="top">
                                                <asp:Label ID="lblLeaveFormDate" runat="server" Text="Leave Form Date" />
                                            </td>
                                            <td style="width: 18%" rowspan="7" valign="top" align="left">
                                                <asp:UpdatePanel ID="upCheck" runat="server">
                                                    <ContentTemplate>
                                                        <cc1:TabContainer ID="TabContainer1" Width="100%" Height="130px" runat="server" ActiveTabIndex="0"
                                                            ScrollBars="Vertical">
                                                            <cc1:TabPanel ID="TabPanel1" runat="server" Width="100%" Height="110px" HeaderText=" Leave Form Date">
                                                                <ContentTemplate>
                                                                    <asp:CheckBoxList ID="chkListLeavedate" runat="server" AppendDataBoundItems="True"
                                                                        AutoPostBack="True" CssClass="CheckBoxList" RepeatLayout="Flow" Height="130px"
                                                                        RepeatColumns="1" Width="90%">
                                                                    </asp:CheckBoxList>
                                                                </ContentTemplate>
                                                            </cc1:TabPanel>
                                                        </cc1:TabContainer>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="chkSelectAll" EventName="CheckedChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td style="width:50%"; valign="top" align="left" rowspan="7">
                                                <table style="width: 100%">
                                                                <div id="Div3" class="panel-heading-default" style="margin-bottom: 5px">
                                                                    <div style="float: left;">
                                                                        <asp:Label ID="lblDescription" runat="server" Text="Description"></asp:Label>
                                                                    </div>
                                                                    <div style="float: right;">
                                                                        <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label>
                                                                    </div>
                                                                </div>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 70%">
                                                                            <asp:Label ID="LblLeaveBF" runat="server" Text="Leave BF"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 30%; text-align: right">
                                                                            <asp:Label ID="objlblLeaveBFvalue" runat="server" Text="0.00"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 70%">
                                                                        <asp:Label ID="lblToDateAccrued" runat="server" Text="Total Accrued To Date"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%; text-align: right">
                                                                        <asp:Label ID="objlblAccruedToDateValue" runat="server" Text="0"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 70%">
                                                                        <asp:Label ID="lblTotalIssuedToDate" runat="server" Text="Total Issued"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%; text-align: right">
                                                                        <asp:Label ID="objlblIssuedToDateValue" runat="server" Text="0"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 70%">
                                                                            <asp:Label ID="LblTotalAdjustment" runat="server" Text="Total Adjustment"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 30%; text-align: right">
                                                                           <asp:Label ID="objlblTotalAdjustment" runat="server" Text="0.00"></asp:Label>
                                                                        </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 70%">
                                                                        <asp:Label ID="LblBalanceAsonDate" runat="server" Text="Balance As on Date"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 30%; text-align: right">
                                                                        <asp:Label ID="objlblBalanceAsonDateValue" runat="server" align="left" Text="0"></asp:Label>
                                                                    </td>
                                        </tr>
                                        <tr style="width: 100%">
                                                                            <td style="width: 70%">
                                                                                <asp:Label ID="lblBalance" runat="server" Text="Total Balance"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 30%; text-align: right">
                                                                                <asp:Label ID="objlblBalanceValue" runat="server" align="left" Text="0"></asp:Label>
                                                                            </td>
                                                                </tr>
                                                    </table>    
                                            </td>
                                            
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 15%">
                                                <asp:Label ID="lblLeaveFormNo" runat="server" Text="Form No" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtLeaveFormNo" runat="server" ReadOnly="true" />
                                            </td>
                                            <td style="width: 1%" />
                                            <td align="left" style="width: 15%" />
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 15%">
                                                <asp:Label ID="lblCancelLeaveType" runat="server" Text="Leave Type" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtCancelLeaveType" runat="server" ReadOnly="true" />
                                            </td>
                                            <td style="width: 1%" />
                                            <td align="left" style="width: 15%" />
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 15%">
                                                <asp:Label ID="lblCancelFormNo" runat="server" Text="Cancel Form No" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtCancelFormNo" runat="server" />
                                            </td>
                                            <td style="width: 1%" />
                                            <td align="left" style="width: 15%" />
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 15%" valign="top">
                                                <asp:Label ID="lblCancelReason" runat="server" Text="Reason" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:TextBox ID="txtCancelReason" runat="server" Rows="3" TextMode="MultiLine" />
                                            </td>
                                            <td style="width: 1%" />
                                            <td align="left" style="width: 15%" />
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="2" style="text-align: right">
                                                <asp:LinkButton ID="lnkCancelExpense" runat="server" Text="Cancel Expense"></asp:LinkButton>
                                            </td>
                                            <td align="right" colspan="3" />
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnCancelScanAttachment" runat="server" Text="Browse" CssClass="btnDefault" Style="margin-right: 39.5%" />
                                        <asp:Button ID="btnCancelSave" runat="server" CssClass="btndefault" Text="Save" />
                                        <asp:Button ID="btnCancelClose" runat="server" CssClass="btndefault" Text="Close" />
                                        <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc7:Confirmation ID="popup_YesNo" Title="Title" runat="server" />
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_CancelExp" runat="server" CancelControlID="btnCancelExpCancel"
                    DropShadow="true" BackgroundCssClass="ModalPopupBG2" PopupControlID="pnl_CancelExp"
                    TargetControlID="btnHiddenExp">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_CancelExp" runat="server" CssClass="newpopup" Style="display: none;
                    width: 800px">
                    <div class="panel-primary" style="margin-bottom: 0px;">
                        <div class="panel-heading">
                            <asp:Label ID="Label9" runat="server" Text="Cancel Expense Form Information"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="Div15" class="panel-default">
                                <div id="Div16" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="Label10" runat="server" Text="Cancel Expense Form Information"></asp:Label>
                                    </div>
                                </div>
                                <div id="Div17" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 8%">
                                                <asp:Label ID="lblCancelExpCategory" runat="server" Text="Exp. Cat." Width="100%"></asp:Label>
                                            </td>
                                            <td style="width: 48%" colspan="3">
                                                <asp:DropDownList ID="cboCancelExpCategory" runat="server" Width="363px" Height="20"
                                                    Enabled="false">
                                                </asp:DropDownList>
                                            </td>
                                            <td rowspan="3" style="vertical-align: top; width: 12%">
                                                <asp:Label ID="lblCancelExpRemark" runat="server" Text="Cancel Remark" Width="100%"></asp:Label>
                                            </td>
                                            <td rowspan="3" style="vertical-align: top; width: 30%">
                                                <asp:TextBox ID="txtCancelExpRemark" runat="server" TextMode="MultiLine" Width="97%"
                                                    Height="100%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 8%">
                                                <asp:Label ID="lblCancelExpPeriod" runat="server" Text="Period" Width="100%" Visible="false"></asp:Label>
                                                <asp:Label ID="lblCancelExpClaimNO" runat="server" Text="Claim No." Width="100%"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboCancelExpPeriod" runat="server" Width="0px" Visible="false"
                                                    Height="20" Enabled="false">
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtCancelExpClaimNo" runat="server" Width="97%" Enabled="false"></asp:TextBox>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblCancelExpDate" runat="server" Text="Date" Width="100%"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <uc2:DateCtrl ID="dtpCancelExpDate" runat="server" AutoPostBack="False" Width="117"
                                                    Enabled="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 8%">
                                                <asp:Label ID="lblCancelExpEmployee" runat="server" Text="Employee" Width="100%"></asp:Label>
                                            </td>
                                            <td colspan="3" style="width: 48%">
                                                <asp:TextBox ID="txtCanelExpEmployee" runat="server" Enabled="false" Width="100%"></asp:TextBox>
                                                <asp:HiddenField ID="txtCancelExpEmployeeid" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="6" align="center">
                                                <asp:Panel ID="pnl_dgvExpData" runat="server" Width="100%" Height="270px" ScrollBars="Auto">
                                                    <asp:GridView ID="dgvCancelExpData" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                                        ShowFooter="False" Width="750px" PageSize="15" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                        RowStyle-CssClass="griviewitem">
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="ChkCancelExpAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkCancelExpAll_CheckedChanged" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="ChkCancelExpSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkCancelExpSelect_CheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Expense" HeaderText="Claim/Expense Desc" ReadOnly="true"
                                                                FooterText="dgcolhExpense" HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="UoM" HeaderText="UoM" ReadOnly="true" FooterText="dgcolhUoM"
                                                                HeaderStyle-HorizontalAlign="Left" />
                                                            <asp:BoundField DataField="Quantity" HeaderText="Quantity" ReadOnly="true" FooterText="dgcolhQty"
                                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="UnitPrice" HeaderText="Unit Price" ReadOnly="true" FooterText="dgcolhUnitPrice"
                                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                            <asp:BoundField DataField="Amount" HeaderText="Amount" ReadOnly="true" FooterText="dgcolhAmount"
                                                                HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                                
                                                              <%--Pinkal (04-Feb-2019) -- Start Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.--%>
                                                             <asp:BoundField DataField="currency_sign" HeaderText="Currency" ReadOnly="true" FooterText="dgcolhCurrency" />
                                                               <%--Pinkal (04-Feb-2019) -- End --%>
                                                                
                                                            <asp:BoundField DataField="expense_remark" HeaderText="dgcolhRemark" ReadOnly="true"
                                                                HeaderStyle-CssClass="GridViewRowVisibleFalse" ItemStyle-CssClass="GridViewRowVisibleFalse" />
                                                            <asp:BoundField DataField="crapprovaltranunkid" HeaderText="objdgcolhApproverTranId"
                                                                ReadOnly="true" HeaderStyle-CssClass="GridViewRowVisibleFalse" ItemStyle-CssClass="GridViewRowVisibleFalse" />
                                                            <asp:BoundField DataField="crmasterunkid" HeaderText="objdgcolhMasterId" ReadOnly="true"
                                                                HeaderStyle-CssClass="GridViewRowVisibleFalse" ItemStyle-CssClass="GridViewRowVisibleFalse" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="6" align="right">
                                                <asp:Label ID="lblCancelExpGrandTotal" runat="server" Text="Grand Total"></asp:Label>
                                                <asp:TextBox ID="txtCancelExpGrandTotal" runat="server" Enabled="false" Style="text-align: right;
                                                    width: 20%"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnCancelExpSave" runat="server" Text="Save" CssClass="btnDefault" />
                                        <asp:Button ID="btnCancelExpCancel" runat="server" Text="Cancel" CssClass="btnDefault" />
                                        <asp:HiddenField ID="btnHiddenExp" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 
                </asp:Panel>
                
                <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="ModalPopupBG"
                    TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" DropShadow="true"
                    CancelControlID="hdf_ScanAttchment">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="newpopup" Width="600px"
                    Style="display: none;">
                    <div class="panel-primary" style="margin: 0px">
                        <div class="panel-heading">
                            <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="Div2" class="panel-default">
                                <div id="Div1" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 30%">
                                                <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type"></asp:Label>
                                            </td>
                                            <td style="width: 40%">
                                                <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server"
                                                    Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                    <div id="fileuploader">
                                                        <input type="button" id="btnAddFile" runat="server" class="btndefault" value="Add" />
                                                    </div>
                                                </asp:Panel>
                                                <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click" Text="Browse" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="3" style="width: 100%">
                                                <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                    HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                    HeaderStyle-Font-Bold="false" Width="99%">
                                                    <Columns>
                                                        <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                        ToolTip="Delete"></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--0--%>
                                                        <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                        <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                        <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--1--%>
                                                        <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                        <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                        <%--2--%>
                                                        <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                        <%--3--%>
                                                        <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                        <%--4--%>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                        <div style="float: left">
                                            <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                        </div>
                                        <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                        <asp:Button ID="btnScanSave" runat="server" Text="Save" CssClass="btnDefault" />
                                        <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btnDefault" />
                                        <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                 
                 <uc7:Confirmation ID="CancelFormConfirmation" runat="server" Message="" Title="Confirmation" />
                 <uc7:Confirmation ID="popup_AttachementYesNo" runat="server"   Message="" Title="Confirmation"  />
                 
            </ContentTemplate>
            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgv_Attchment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
