﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_AddEditLeaveApprover.aspx.vb" Inherits="Leave_wPg_AddEditLeaveApprover"
    Title="Add/Edit Leave Approver" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="ComboList" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
			var scroll = {
					Y: '#<%= hfScrollPosition.ClientID %>'
				};
			var scroll1 = {
					Y: '#<%= hfScrollPosition1.ClientID %>'
				};
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
            }

            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
                if (args.get_error() == undefined) {
                    $("#scrollable-container").scrollTop($(scroll.Y).val());
                    $("#scrollable-container1").scrollTop($(scroll1.Y).val());

                }
            }

            $("[id*=ChkAll]").live("click", function() {
                var chkHeader = $(this);
                var grid = $(this).closest("table");
                $("input[type=checkbox]", grid).each(function() {
                    if (chkHeader.is(":checked")) {
                        debugger;
                        if ($(this).is(":visible")) {
                            $(this).attr("checked", "checked");
                        }

                    } else {
                        $(this).removeAttr("checked");
                    }
                });
            });

            $("[id*=ChkgvSelect]").live("click", function() {
                var grid = $(this).closest("table");
                var chkHeader = $("[id*=chkHeader]", grid);
                var row = $(this).closest("tr")[0];

                debugger;
                if (!$(this).is(":checked")) {
                    var row = $(this).closest("tr")[0];
                    chkHeader.removeAttr("checked");
                } else {

                    if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                        chkHeader.attr("checked", "checked");
                    }
                }
            });


            $("[id*=ChkSelectedEmpAll]").live("click", function() {
                var chkHeader = $(this);
                var grid = $(this).closest("table");
                $("input[type=checkbox]", grid).each(function() {
                    if (chkHeader.is(":checked")) {
                        debugger;
                        if ($(this).is(":visible")) {
                            $(this).attr("checked", "checked");
                        }

                    } else {
                        $(this).removeAttr("checked");
                    }
                });
            });

            $("[id*=ChkgvSelectedEmp]").live("click", function() {
                var grid = $(this).closest("table");
                var chkHeader = $("[id*=chkHeader]", grid);
                var row = $(this).closest("tr")[0];

                debugger;
                if (!$(this).is(":checked")) {
                    var row = $(this).closest("tr")[0];
                    chkHeader.removeAttr("checked");
                } else {

                    if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                        chkHeader.attr("checked", "checked");
                    }
                }
            });




            $.expr[":"].containsNoCase = function(el, i, m) {
                var search = m[3];
                if (!search) return false;
                return eval("/" + search + "/i").test($(el).text());
            };

            function FromSearching() {
                if ($('#txtSearch').val().length > 0) {
                    $('#<%= GvEmployee.ClientID %> tbody tr').hide();
                    $('#<%= GvEmployee.ClientID %> tbody tr:first').show();
                    $('#<%= GvEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearch').val() + '\')').parent().show();
                }
                else if ($('#txtSearch').val().length == 0) {
                    resetFromSearchValue();
                }
                if ($('#<%= GvEmployee.ClientID %> tr:visible').length == 1) {
                    $('.norecords').remove();
                }

                if (event.keyCode == 27) {
                    resetFromSearchValue();
                }
            }
            function resetFromSearchValue() {
                $('#txtSearch').val('');
                $('#<%= GvEmployee.ClientID %> tr').show();
                $('.norecords').remove();
                $('#txtSearch').focus();
            }


            $("[id*=chkSelectAll]").live("click", function() {
                var chkHeader = $(this);
                var grid = $(this).closest("table");
                $("input[type=checkbox]", grid).each(function() {
                    if (chkHeader.is(":checked")) {
                        debugger;
                        if ($(this).is(":visible")) {
                            $(this).attr("checked", "checked");
                        }

                    } else {
                        $(this).removeAttr("checked");
                    }
                });
            });

            $("[id*=ChkgvSelectAll]").live("click", function() {
                var grid = $(this).closest("table");
                var chkHeader = $("[id*=chkHeader]", grid);
                var row = $(this).closest("tr")[0];

                debugger;
                if (!$(this).is(":checked")) {
                    var row = $(this).closest("tr")[0];
                    chkHeader.removeAttr("checked");
                } else {

                    if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
                        chkHeader.attr("checked", "checked");
                    }
                }
            });


            
    </script>

    <asp:Panel ID="MainPan" CssClass="pnlcontentmain" Width="100%" runat="server">
    </asp:Panel>
    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Add / Edit Leave Approve"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Add / Edit Leave Approver"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                           <%-- 'Pinkal (12-Oct-2020) --Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.--%>
                                            <td style="width: 20%">
                                                    <asp:CheckBox ID="chkExternalApprover" runat="server" Text="Make External Approver" AutoPostBack="true" />
                                            </td>
                                            <td style="width: 30%">
                                                    <asp:CheckBox ID="chkIncludeInactiveEmp" runat="server" Text="Include Inactive Employee" AutoPostBack="true" />
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblApproverName" runat="server" Text="Approver Name"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="drpApprover" runat="server" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblApproveLevel" runat="server" Text="Approver Level"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="drpLevel" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                           <%--   'Pinkal (12-Oct-2020) -- End--%>
                                            <td style="width: 15%">
                                                <asp:Label ID="LblUser" runat="server" Text="User" Width="100%" />
                                            </td>
                                            <td style="width: 35%">
                                                <asp:DropDownList ID="drpUser" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-heading-default">
                                    <div style="float: left;">
                                    </div>
                                </div>
                                <div id="Div3" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 50%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 15%">
                                                            <asp:Label ID="LblSearchEmployee" runat="server" Text="Search" Width="100%" />
                                                        </td>
                                                        <td style="width: 85%">
                                                         <%--'Pinkal (28-Apr-2020) -- Start
                                                                 'Optimization  - Working on Process Optimization and performance for require module.
                                                            <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="True" />--%>
                                                            <input type="text" id="txtSearch" name="txtSearch" placeholder = "Type To Search Text"  maxlength="50" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                                            <%--'Pinkal (28-Apr-2020) -- End --%>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 50%">
                                                <asp:LinkButton ID="lnkMapLeaveType" runat="server" Text="Leave Type Mapping" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%; vertical-align: top; padding-right: 5px;">
                                                <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                                                    height: 400px; overflow: auto; border: 1px solid #DDD;">
                                                    <asp:GridView ID="GvEmployee" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                        Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames = "employeeunkid,Code,Employee Name,Department,Section,Job">
                                                        <Columns>
                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                <%--'Pinkal (28-Apr-2020) -- Start
                                                                        'Optimization  - Working on Process Optimization and performance for require module.
                                                                    <asp:CheckBox ID="ChkAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll_CheckedChanged" />--%>
                                                                    <asp:CheckBox ID="ChkAll" runat="server" />
                                                                    <%-- 'Pinkal (28-Apr-2020) -- End--%>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                <%--'Pinkal (28-Apr-2020) -- Start
                                                                        'Optimization  - Working on Process Optimization and performance for require module.
                                                                    <asp:CheckBox ID="ChkgvSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkgvSelect_CheckedChanged" />--%>
                                                                    <asp:CheckBox ID="ChkgvSelect" runat="server" />
                                                                  <%-- 'Pinkal (28-Apr-2020) -- End--%>   
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                             <%--'Pinkal (28-Apr-2020) -- Start
                                                             'Optimization  - Working on Process Optimization and performance for require module.
                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="ColhEmployeecode" />
                                                            <asp:BoundField DataField="name" HeaderText="Employee" ReadOnly="true" FooterText="colhEmp" />--%>
                                                            <asp:BoundField DataField="Code" HeaderText="Code" ReadOnly="true" FooterText="ColhEmployeecode" />
                                                            <asp:BoundField DataField="Employee Name" HeaderText="Employee" ReadOnly="true" FooterText="colhEmp" />
                                                            <%-- 'Pinkal (28-Apr-2020) -- End--%>
                                                            <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                Visible="false" />
                                                        </Columns>
                                                    </asp:GridView>
                                            </td>
                                            <td style="width: 50%; vertical-align: top; padding-left: 5px">
                                                <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollTop);" style="width: 100%;
                                                    height: 400px; overflow: auto; border: 1px solid #DDD;">
                                                    <asp:GridView ID="GvSelectedEmployee" runat="server" AutoGenerateColumns="False"
                                                        ShowFooter="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                        RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="leaveapprovertranunkid,GUID,employeeunkid">
                                                        <Columns>
                                                            <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                <%--'Pinkal (28-Apr-2020) -- Start
                                                                        'Optimization  - Working on Process Optimization and performance for require module.
                                                                    <asp:CheckBox ID="ChkSelectedEmpAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkSelectedEmpAll_CheckedChanged" /> --%>
                                                                    <asp:CheckBox ID="ChkSelectedEmpAll" runat="server" />
                                                                 <%--'Pinkal (28-Apr-2020) -- End --%>                                                                      
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                <%--'Pinkal (28-Apr-2020) -- Start
                                                                        'Optimization  - Working on Process Optimization and performance for require module.
                                                                    <asp:CheckBox ID="ChkgvSelectedEmp" runat="server" AutoPostBack="true" OnCheckedChanged="ChkgvSelectedEmp_CheckedChanged" />--%>
                                                                     <asp:CheckBox ID="ChkgvSelectedEmp" runat="server" />
                                                                 <%--'Pinkal (28-Apr-2020) -- End --%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="ColhEmployeecode" />
                                                            <asp:BoundField DataField="name" HeaderText="Employee" ReadOnly="true" FooterText="colhEmployee" />
                                                            <asp:BoundField DataField="departmentname" HeaderText="Department" ReadOnly="true"
                                                                FooterText="colhDept" />
                                                            <asp:BoundField DataField="sectionname" HeaderText="Section" ReadOnly="true" FooterText="colhSection" />
                                                            <asp:BoundField DataField="jobname" HeaderText="Job" ReadOnly="true" FooterText="colhJob" />
                                                            <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                Visible="false" />
                                                               <%-- 'Pinkal (28-Apr-2020) -- Start
                                                                'Optimization  - Working on Process Optimization and performance for require module.	
                                                            <asp:BoundField DataField="departmentunkid" HeaderText="Departmentunkid" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundField DataField="sectionunkid" HeaderText="Sectionunkid" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundField DataField="jobunkid" HeaderText="Jobunkid" ReadOnly="true" Visible="false" />
                                                             'Pinkal (28-Apr-2020) -- En d--%>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 50px; padding-right: 10px; text-align: right">
                                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btndefault" />
                                            </td>
                                            <td style="width: 50px; padding-right: 10px; text-align: right">
                                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btndefault" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupLeaveMapping" BackgroundCssClass="modalBackground"
                        TargetControlID="LblLeaveMapping" runat="server" PopupControlID="pnlLeaveMappingPopup"
                        DropShadow="true" CancelControlID="btnTempClose" />
                    <%--<asp:Panel ID="" Width="" Height="100%" runat="server" Style="display: none"
                        CssClass="modalPopup">
                        <asp:Panel ID="pnlMapLeave" runat="server" Width="100%" Height="100%" Style="background-color: #DDDDDD;
                            border: solid 1px Gray; color: Black">
                        </asp:Panel>
                    </asp:Panel>--%>
                    <asp:Panel ID="pnlLeaveMappingPopup" runat="server" CssClass="newpopup" Style="display: none;
                        width: 375px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="LblLeaveMapping" runat="server" Text="Leave Type Mapping"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="Div4" class="panel-default">
                                    <div id="Div5" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="Div6" class="panel-body-default">
                                        <table width="100%">
                                            <tr style="width: 100%">
                                                <td style="width: 40%">
                                                    <asp:Label ID="LblApproverLeaveMap" runat="server" Text="Approver" />
                                                </td>
                                                <td style="width: 60%">
                                                    <asp:Label ID="LblApproverLeaveMapVal" runat="server" Text="" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 40%">
                                                    <asp:Label ID="LblLevelLeaveMap" runat="server" Text="Approver Level" />
                                                </td>
                                                <td style="width: 60%">
                                                    <asp:Label ID="LblLevelLeaveMapVal" runat="server" Text="" />
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td colspan="2">
                                                    <asp:Panel ID="pnl_GvLeaveTypeMapping" runat="server" Height="350px" ScrollBars="Auto">
                                                        <asp:GridView ID="GvLeaveTypeMapping" runat="server" AutoGenerateColumns="false"
                                                            Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames = "leavetypeunkid,leavetypecode">
                                                            <Columns>
                                                                <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                                    HeaderStyle-HorizontalAlign="Center">
                                                                    <HeaderTemplate>
                                                                      <%-- 'Pinkal (28-Apr-2020) -- Start
                                                                               'Optimization  - Working on Process Optimization and performance for require module.
                                                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />  --%>
                                                                        <asp:CheckBox ID="chkSelectAll" runat="server" />
                                                                        <%--'Pinkal (28-Apr-2020) -- End --%> 
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                    <%-- 'Pinkal (28-Apr-2020) -- Start
                                                                             'Optimization  - Working on Process Optimization and performance for require module
                                                                        <asp:CheckBox ID="ChkgvSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkgvSelectAll_CheckedChanged" /> --%>
                                                                        <asp:CheckBox ID="ChkgvSelectAll" runat="server" />
                                                                        <%--'Pinkal (28-Apr-2020) -- End --%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="leavetypecode" HeaderText="Leave Code" ReadOnly="True" />
                                                                <asp:BoundField DataField="leavename" HeaderText="Leave Type" ReadOnly="True" />
                                                                <asp:BoundField DataField="ispaid" HeaderText="IsPaid" ReadOnly="True" />
                                                            </Columns>
                                                            <PagerStyle Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                                                                Font-Underline="False" HorizontalAlign="Left" />
                                                        </asp:GridView>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td colspan="2" align="right">
                                                    <asp:Button ID="btnLeaveMapSave" runat="server" Text="Save" CssClass="btndefault" />
                                                    <asp:Button ID="btnLeaveMapClose" runat="server" Text="Close" CssClass="btndefault" />
                                                    <asp:HiddenField ID="btnTempClose" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
