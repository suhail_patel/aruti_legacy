﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="LeavePLannerViewer.aspx.vb" Inherits="LeavePLannerViewer" Title="Leave PLanner Viewer" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/PaidLeaveTYpe.ascx" TagName="Paidleavetype" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/LeaveType.ascx" TagName="LeaveType" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc5" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="XGridView" Namespace="CustomControls" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Leave PLanner Viewer"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 60%; text-align: center">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%;" align="center">
                                                            <asp:Calendar ID="Calendar1" runat="server" CssClass="calendar" SelectedDate="2011-06-15"
                                                                ShowGridLines="True">
                                                                <SelectedDayStyle CssClass="Calselday" />
                                                                <SelectorStyle CssClass="calsele" />
                                                                <TitleStyle CssClass="cal_title" />
                                                            </asp:Calendar>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td align="center" style="width: 100%">
                                                            <asp:RadioButtonList ID="RbtlistView" runat="server" CssClass="staticlbl" CellSpacing="3"
                                                                RepeatDirection="Horizontal" RepeatLayout="Flow">
                                                                <asp:ListItem Value="0">Weekly View</asp:ListItem>
                                                                <asp:ListItem Value="1">Month View</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 40%">
                                                <table style="width: 100%;">
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                            <asp:Label ID="lbldepartment" runat="server" CssClass="staticlbl" Text="Department:"></asp:Label>
                                                        </td>
                                                        <td style="width: 70%">
                                                            <uc7:DropDownList ID="drpdepartment" runat="server" CssClass="dropdown" Width="180px">
                                                            </uc7:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                            <asp:Label ID="lbljob" CssClass="staticlbl" runat="server" Text="Job:"></asp:Label>
                                                        </td>
                                                        <td style="width: 70%">
                                                            <uc7:DropDownList ID="drpjob" runat="server" CssClass="dropdown" Width="180px"></uc7:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 30%">
                                                            <asp:Label ID="lblSection" runat="server" CssClass="staticlbl" Text="Section:"></asp:Label>
                                                        </td>
                                                        <td style="width: 70%">
                                                            <uc7:DropDownList ID="drpsection" runat="server" CssClass="dropdown" Width="180px"></uc7:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" CssClass="btnDefault" Text="Search" />
                                        <asp:TextBox ID="txtleaveplannerunkid" runat="server" Visible="False"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div style="border: 2px solid #DDD; margin: 7px;">
                                <asp:Panel ID="Panel1" Width="100%" runat="server" ScrollBars="Horizontal" Wrap="False">
                                    <cc1:XGridView ID='XGridView' runat="server" EnableCellClick="True" Width="200%"
                                        CssClass="gridview" AllowPaging="false">
                                        <RowStyle CssClass="griviewitem" />
                                        <PagerStyle CssClass="griviewheader" />
                                        <SelectedRowStyle CssClass="griviewheader" />
                                        <HeaderStyle CssClass="griviewheader" Font-Bold="true" />
                                        <EditRowStyle CssClass="griviewitem" />
                                        <AlternatingRowStyle CssClass="griviewitem" />
                                    </cc1:XGridView>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
