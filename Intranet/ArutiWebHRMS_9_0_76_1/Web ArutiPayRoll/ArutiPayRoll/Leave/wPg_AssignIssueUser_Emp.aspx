﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_AssignIssueUser_Emp.aspx.vb" Inherits="Leave_wPg_AssignIssueUser_Emp"
     %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/EmployeeList.ascx" TagName="EmployeeList" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="ComboList" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    
    <script type="text/javascript">
            var prm;
			var scroll = {
					Y: '#<%= hfScrollPosition.ClientID %>'
				};
			var scroll1 = {
					Y: '#<%= hfScrollPosition1.ClientID %>'
				};
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
	         if (args.get_error() == undefined) {
                    $("#scrollable-container").scrollTop($(scroll.Y).val());
                    $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            }
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Age Analysis Report"></asp:Label>
                        </div>
                        <div id="Div4" class="panel-default">
                            <div id="Div6" class="panel-body-default">
                                <table style="width: 100%;">
                                    <tr style="width: 100%">
                                        <td style="width: 50%; vertical-align: top;">
                                            <div class="panel-body">
                                                <div id="Div1" class="panel-default">
                                                    <div id="Div2" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblCaption" runat="server" Text="Assign Issue User to Employee"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="Div3" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 30%">
                                                                    <asp:Label ID="LblIssueUser" runat="server" Text="Issue User"></asp:Label>
                                                                </td>
                                                                <td style="width: 70%">
                                                                    <asp:DropDownList ID="drpIssueUser" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 30%">
                                                                    <asp:Label ID="LblToIssueUser" runat="server" Text="To Issue User"></asp:Label>
                                                                </td>
                                                                <td style="width: 70%">
                                                                    <asp:DropDownList ID="drpToIssueUser" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 30%">
                                                                </td>
                                                                <td style="width: 70%">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width: 50%; vertical-align: top;">
                                            <div class="panel-body">
                                                <div id="FilterCriteria" class="panel-default">
                                                    <div id="FilterCriteriaTitle" class="panel-heading-default">
                                                        <div style="float: left;">
                                                            <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                                        </div>
                                                    </div>
                                                    <div id="FilterCriteriaBody" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 30%">
                                                                    <asp:Label ID="lblFilterDepartment" runat="server" Text="Department"></asp:Label>
                                                                </td>
                                                                <td style="width: 70%">
                                                                    <asp:DropDownList ID="drpDepartment" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 30%">
                                                                    <asp:Label ID="lblFilterjob" runat="server" Text="Job"></asp:Label>
                                                                </td>
                                                                <td style="width: 70%">
                                                                    <asp:DropDownList ID="drpJob" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 30%">
                                                                    <asp:Label ID="lblFilterSection" runat="server" Text="Section"></asp:Label>
                                                                </td>
                                                                <td style="width: 70%">
                                                                    <asp:DropDownList ID="drpSection" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="btn-default">
                                                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                                            <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 50%">
                                            <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true"></asp:TextBox>
                                        </td>
                                        <td style="width: 50%">
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 50%">
                                            <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 100%;
                                                height: 350px; overflow: auto; border: solid 1px #DDD;">
                                                <asp:GridView ID="GvEmployee" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                    Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                    AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="ChkAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkAll_CheckedChanged" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkgvSelect" runat="server" AutoPostBack="true" OnCheckedChanged="ChkgvSelect_CheckedChanged" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="ColhEmployeecode" />
                                                        <asp:BoundField DataField="name" HeaderText="Employee" ReadOnly="true" FooterText="ColhEmp" />
                                                        <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                            Visible="false" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </td>
                                        <td style="width: 50%">
                                            <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollTop);" style="width: 100%;
                                                height: 350px; overflow: auto; border: solid 1px #DDD;">
                                                <asp:GridView ID="GvSelectedEmployee" runat="server" AutoGenerateColumns="False"
                                                    ShowFooter="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                    RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="ChkSelectedEmpAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkSelectedEmpAll_CheckedChanged" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkgvSelectedEmp" runat="server" AutoPostBack="true" OnCheckedChanged="ChkgvSelectedEmp_CheckedChanged" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="issueuser" HeaderText="Issue User" ReadOnly="true" FooterText="colhIssueUser" />
                                                        <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" />
                                                        <asp:BoundField DataField="employee" HeaderText="Employee" ReadOnly="true" FooterText="colhEmployee" />
                                                        <asp:BoundField DataField="department" HeaderText="Department" ReadOnly="true" FooterText="colhDept" />
                                                        <asp:BoundField DataField="section" HeaderText="Section" ReadOnly="true" FooterText="colhSection" />
                                                        <asp:BoundField DataField="job" HeaderText="Job" ReadOnly="true" FooterText="colhJob" />
                                                        <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                            Visible="false" />
                                                        <asp:BoundField DataField="departmentunkid" HeaderText="Departmentunkid" ReadOnly="true"
                                                            Visible="false" />
                                                        <asp:BoundField DataField="sectionunkid" HeaderText="Sectionunkid" ReadOnly="true"
                                                            Visible="false" />
                                                        <asp:BoundField DataField="jobunkid" HeaderText="Jobunkid" ReadOnly="true" Visible="false" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 50%; padding-right: 10px; text-align: right">
                                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btndefault" />
                                        </td>
                                        <td style="width: 50%; padding-right: 10px; text-align: right">
                                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btndefault" />
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btndefault" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
