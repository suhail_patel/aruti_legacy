﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data

#End Region


Partial Class Leave_wPg_AssignIssueUserList
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private objIssueUser As New clsissueUser_Tran


    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private ReadOnly mstrModuleName As String = "frmAssignIssueUserList"
    'Pinkal (06-May-2014) -- End

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End


            If IsPostBack = False Then
                btnNew.Visible = CBool(Session("AddLeaveApprover"))
                FillCombo()
                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.

                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.

                'If Session("AssignIssueUser_Filter") IsNot Nothing Then
                '    If CStr(Session("AssignIssueUser_Filter")).Split("-")(0).Trim <> "" Then
                '        drpIssueUser.SelectedValue = CStr(Session("AssignIssueUser_Filter")).Split("-")(0)
                '    End If
                '    If CStr(Session("AssignIssueUser_Filter")).Split("-")(1).Trim <> "" Then
                '        drpEmployee.SelectedValue = CStr(Session("AssignIssueUser_Filter")).Split("-")(1)
                '    End If
                '    If CInt(drpIssueUser.SelectedValue) > 0 OrElse CInt(drpEmployee.SelectedValue) > 0 Then
                '        Call btnSearch_Click(btnSearch, Nothing)
                '    End If
                'End If

                If Session("AssignIssueUser_Filter") IsNot Nothing AndAlso Session("AssignIssueUser_Filter").ToString().Trim.Length > 0 Then
                    If CStr(Session("AssignIssueUser_Filter")).Split(CChar("-"))(0).Trim <> "" Then
                        drpIssueUser.SelectedValue = CStr(Session("AssignIssueUser_Filter")).Split(CChar("-"))(0)
                    End If
                    If CStr(Session("AssignIssueUser_Filter")).Split(CChar("-"))(1).Trim <> "" Then
                        drpEmployee.SelectedValue = CStr(Session("AssignIssueUser_Filter")).Split(CChar("-"))(1)
                    End If
                    If CInt(drpIssueUser.SelectedValue) > 0 OrElse CInt(drpEmployee.SelectedValue) > 0 Then
                        Call btnSearch_Click(btnSearch, Nothing)
                    End If
                End If

                'Pinkal (06-Jan-2016) -- End

                'SHANI [09 Mar 2015]--END
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()

        Dim dsFill As DataSet = Nothing
        Dim mblnAdUser As Boolean = False
        Try

            Dim objEmployee As New clsEmployee_Master

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) = False Then
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            'Else
            '    dsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime, ConfigParameter._Object._CurrentDateAndTime)
            'End If


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            'dsFill = objEmployee.GetEmployeeList(Session("Database_Name"), _
            '                                    Session("UserId"), _
            '                                    Session("Fin_year"), _
            '                                    Session("CompanyUnkId"), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                    Session("UserAccessModeSetting"), True, _
            '                                    Session("IsIncludeInactiveEmp"), "Employee", True)

            dsFill = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                               CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                               CInt(Session("CompanyUnkId")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                               Session("UserAccessModeSetting").ToString(), True, _
                                               True, "Employee", True)



            'Pinkal (06-Jan-2016) -- End

            'Shani(24-Aug-2015) -- End
            drpEmployee.DataValueField = "employeeunkid"
            'Nilay (09-Aug-2016) -- Start
            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            'drpEmployee.DataTextField = "employeename"
            drpEmployee.DataTextField = "EmpCodeName"
            'Nilay (09-Aug-2016) -- End

            drpEmployee.DataSource = dsFill.Tables(0)
            drpEmployee.DataBind()

            'dsFill = objIssueUser.GetUserWithIssueLeavePrivilage("IssueUser", True)
            'drpIssueUser.DataValueField = "userunkid"
            'drpIssueUser.DataTextField = "username"
            'drpIssueUser.DataSource = dsFill.Tables("IssueUser")
            'drpIssueUser.DataBind()

            Dim objUser As New clsUserAddEdit
            Dim objOption As New clsPassowdOptions
            Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

            If drOption.Length > 0 Then
                If objOption._UserLogingModeId <> enAuthenticationMode.BASIC_AUTHENTICATION Then mblnAdUser = True
            End If



            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            'dsFill = objUser.getComboList("User", True, mblnAdUser, objOption._IsEmployeeAsUser, CInt(Session("CompanyUnkId")), 264, CInt(Session("Fin_year")))
            dsFill = objUser.getNewComboList("User", 0, True, CInt(Session("CompanyUnkId")), CStr(264), CInt(Session("Fin_year")))
            'Pinkal (01-Mar-2016) -- End

            drpIssueUser.DataTextField = "name"
            drpIssueUser.DataValueField = "userunkid"
            drpIssueUser.DataSource = dsFill.Tables("User")
            drpIssueUser.DataBind()

            objOption = Nothing
            objUser = Nothing

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim dsList As DataSet = Nothing
        Dim dtList As DataTable = Nothing
        Dim strSearch As String = String.Empty
        Try
            dsList = objIssueUser.GetList("IssueUser")

            If CInt(drpIssueUser.SelectedValue) > 0 Then
                strSearch &= "AND issueuserunkid =" & CInt(drpIssueUser.SelectedValue) & " "
            End If

            If CInt(drpEmployee.SelectedValue) > 0 Then
                strSearch &= "AND employeeunkid=" & CInt(drpEmployee.SelectedValue) & " "
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
                dtList = New DataView(dsList.Tables("IssueUser"), strSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtList = dsList.Tables("IssueUser")
            End If

            GvIssueUserList.DataSource = dtList
            GvIssueUserList.DataKeyField = "issueusertranunkid"
            GvIssueUserList.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            Dim mstrFilter As String = ""
            If CInt(drpIssueUser.SelectedValue) > 0 Then
                mstrFilter = drpIssueUser.SelectedValue
            End If
            If mstrFilter <> "" Then
                mstrFilter += "-"
            End If
            If CInt(drpEmployee.SelectedValue) > 0 Then
                mstrFilter += drpEmployee.SelectedValue
            End If
            Session("AssignIssueUser_Filter") = mstrFilter
            'SHANI [09 Mar 2015]--END

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            'Response.Redirect(Session("servername") & "~/Leave/wPg_AssignIssueUser_Emp.aspx", False)
            Response.Redirect("~/Leave/wPg_AssignIssueUser_Emp.aspx", False)
            'Pinkal (06-Jan-2016) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnNew_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(drpIssueUser.SelectedValue) <= 0 AndAlso CInt(drpEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Issue User Or Employee to perform further operation on it.", Me)
                Exit Sub
            End If
            FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("objbtnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            drpIssueUser.SelectedIndex = 0
            drpEmployee.SelectedIndex = 0
            GvIssueUserList.DataSource = Nothing
            GvIssueUserList.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDelete.Click
    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup1.buttonDelReasonYes_Click
        'SHANI [01 FEB 2015]--END
        Try
            If (popup1.Reason.Trim = "") Then 'SHANI [01 FEB 2015]--If (txtreasondel.Text = "") Then
                DisplayMessage.DisplayMessage("Please enter delete reason.", Me)
                Exit Sub
            End If


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            popup1.Reason = ""
            'Pinkal (06-Jan-2016) -- End

            objIssueUser._Userunkid = CInt(Session("UserId"))

            With objIssueUser
                ._Isvoid = True
                ._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popup1.Reason 'SHANI [01 FEB 2015]--txtreasondel.Text

                If (CInt(Session("loginBy")) = Global.User.en_loginby.User) Then
                    ._Voiduserunkid = CInt(Session("UserId"))
                End If

                .Delete(CInt(Me.ViewState("issueusertranunkid")))

                If (._Message.Length > 0) Then
                    DisplayMessage.DisplayMessage("You Cannot Delete this Entry." & ._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Successfully deleted." & ._Message, Me)
                    Me.ViewState("issueusertranunkid") = Nothing
                End If
                popup1.Dispose()

                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'txtreasondel.Text = ""
                'SHANI [01 FEB 2015]--END
                FillList()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region " GridView's Event(s) "

    Protected Sub GvIssueUserList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles GvIssueUserList.PageIndexChanged
        Try
            GvIssueUserList.CurrentPageIndex = e.NewPageIndex
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvIssueUserList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GvIssueUserList.ItemDataBound
        Try
            If (e.Item.ItemIndex >= 0) Then

                If e.Item.HasControls Then
                    Dim lnkMigration As LinkButton = DirectCast(e.Item.FindControl("lnkMigration"), LinkButton)
                    lnkMigration.Text = Language._Object.getCaption("btnMigration", lnkMigration.Text).Replace("&", "")
                End If

                GvIssueUserList.Columns(1).Visible = CBool(Session("AllowToMigrateIssueUser"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvIssueUserList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GvIssueUserList.ItemCommand
        Dim ds As DataSet = Nothing
        Try
            If e.CommandName = "Migration" Then

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                Dim mstrFilter As String = ""
                If CInt(drpIssueUser.SelectedValue) > 0 Then
                    mstrFilter = drpIssueUser.SelectedValue
                End If
                If mstrFilter <> "" Then
                    mstrFilter += "-"
                End If
                If CInt(drpEmployee.SelectedValue) > 0 Then
                    mstrFilter += drpEmployee.SelectedValue
                End If
                Session("AssignIssueUser_Filter") = mstrFilter
                'SHANI [09 Mar 2015]--END 
                Session.Add("issueusertranunkid", GvIssueUserList.DataKeys(e.Item.ItemIndex))

                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.
                'Response.Redirect(Session("servername") & "~/Leave/wPg_AssignIssueUser_Emp.aspx", False)
                Response.Redirect("~/Leave/wPg_AssignIssueUser_Emp.aspx", False)
                'Pinkal (06-Jan-2016) -- End


            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub GvIssueUserList_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GvIssueUserList.DeleteCommand
        Try
            Me.ViewState.Add("issueusertranunkid", CInt(GvIssueUserList.DataKeys(e.Item.ItemIndex)))
            popup1.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)
        'SHANI [01 FEB 2015]--END
        Me.lblCaption.Text = Language._Object.getCaption(mstrModuleName, Me.lblCaption.Text)
        Me.LblIssueUser.Text = Language._Object.getCaption(Me.LblIssueUser.ID, Me.LblIssueUser.Text)
        Me.LblEmployee.Text = Language._Object.getCaption(Me.LblEmployee.ID, Me.LblEmployee.Text)
        Me.GvIssueUserList.Columns(0).HeaderText = Language._Object.getCaption(Me.GvIssueUserList.Columns(0).FooterText, Me.GvIssueUserList.Columns(0).HeaderText).Replace("&", "")
        Me.GvIssueUserList.Columns(2).HeaderText = Language._Object.getCaption(Me.GvIssueUserList.Columns(2).FooterText, Me.GvIssueUserList.Columns(2).HeaderText)
        Me.GvIssueUserList.Columns(3).HeaderText = Language._Object.getCaption(Me.GvIssueUserList.Columns(3).FooterText, Me.GvIssueUserList.Columns(3).HeaderText)
        Me.GvIssueUserList.Columns(4).HeaderText = Language._Object.getCaption(Me.GvIssueUserList.Columns(4).FooterText, Me.GvIssueUserList.Columns(4).HeaderText)
        Me.GvIssueUserList.Columns(5).HeaderText = Language._Object.getCaption(Me.GvIssueUserList.Columns(5).FooterText, Me.GvIssueUserList.Columns(5).HeaderText)
        Me.GvIssueUserList.Columns(6).HeaderText = Language._Object.getCaption(Me.GvIssueUserList.Columns(6).FooterText, Me.GvIssueUserList.Columns(6).HeaderText)
        Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

End Class
