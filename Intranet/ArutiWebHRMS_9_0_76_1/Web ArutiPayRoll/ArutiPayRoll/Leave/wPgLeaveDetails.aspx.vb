﻿#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data

#End Region
Partial Class wPgLeaveDetails
    Inherits Basepage

#Region " Private Variable(s) "

    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Private Shared ReadOnly mstrModuleName As String = "frmLeaveDetails"
    'Pinkal (26-Feb-2019) -- End
    Private msg As New CommonCodes

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private clsuser As New User
    'Private objEmployee As New clsEmployee_Master
    'Pinkal (11-Sep-2020) -- End


#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub SetInfo()
        Dim dsCombos As New DataSet
        Try
            If (Session("LoginBy") = Global.User.en_loginby.User) Then


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Dim objEmployee As New clsEmployee_Master
                'Pinkal (11-Sep-2020) -- End


                dsCombos = objEmployee.GetEmployeeList(Session("Database_Name"), _
                                                    Session("UserId"), _
                                                    Session("Fin_year"), _
                                                    Session("CompanyUnkId"), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    Session("UserAccessModeSetting"), True, _
                                                    Session("IsIncludeInactiveEmp"), "Employee", True)


                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsCombos.Tables("Employee")
                    .DataBind()
                    .SelectedValue = 0
                End With


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objEmployee = Nothing
                'Pinkal (11-Sep-2020) -- End


            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
                cboEmployee_SelectedIndexChanged(Nothing, Nothing)

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objglobalassess = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If
            
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombos IsNot Nothing Then dsCombos.Clear()
            dsCombos = Nothing
            'Pinkal (11-Sep-2020) -- End`
        End Try
    End Sub

#End Region

#Region " Controls Event(s) "

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

    'Protected Sub GvLeaveDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvLeaveDetails.PageIndexChanging
    '    Try
    '        GvLeaveDetails.PageIndex = e.NewPageIndex
    '        GvLeaveDetails.DataSource = CType(Me.ViewState("EmpBalance"), DataTable)
    '        GvLeaveDetails.DataBind()
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Pinkal (11-Sep-2020) -- End

    Protected Sub GvLeaveDetails_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvLeaveDetails.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub
            SetDateFormat()

            If e.Row.Cells(2).Text.Trim <> "" AndAlso e.Row.Cells(2).Text <> "&nbsp;" Then
                e.Row.Cells(2).Text = eZeeDate.convertDate(e.Row.Cells(2).Text).ToShortDateString
            End If

            If e.Row.Cells(3).Text.Trim <> "" AndAlso e.Row.Cells(3).Text <> "&nbsp;" Then
                e.Row.Cells(3).Text = eZeeDate.convertDate(e.Row.Cells(3).Text).Date
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

        If Session("clsuser") Is Nothing Then
            Exit Sub
        End If
        If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            Exit Sub
        End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = lblPageHeader.Text
            'StrModuleName2 = "HR"
            'Pinkal (11-Sep-2020) -- End

        If (Page.IsPostBack = False) Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (11-Sep-2020) -- End


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'clsuser = CType(Session("clsuser"), User)
                'If (Session("LoginBy") = Global.User.en_loginby.User) Then
                'Aruti.Data.User._Object._Userunkid = Session("UserId") 'Sohail (23 Apr 2012)
                'End If
                'Pinkal (11-Sep-2020) -- End

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Call SetControlCaptions()
            Call Language._Object.SaveValue()
            SetLanguage()
            'Pinkal (26-Feb-2019) -- End
            Call SetInfo()

        End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue) > 0 Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Dim clsuser As New User
                clsuser.GetLeaveBalance(cboEmployee.SelectedValue, Session("mdbname"), CInt(Session("Fin_year")))
                GvLeaveDetails.DataSource = clsuser.LeaveBalances
                GvLeaveDetails.DataBind()
                'Me.ViewState.Add("EmpBalance", clsuser.LeaveBalances)
                clsuser = Nothing
                'Pinkal (11-Sep-2020) -- End
            Else
                GvLeaveDetails.DataSource = Nothing
                GvLeaveDetails.DataBind()
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

#Region " Language & UI Settings "

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)

            Language._Object.setCaption(mstrModuleName, Me.lblPageHeader.Text)
            Language._Object.setCaption(lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Language._Object.setCaption(lblLeaveInfo.ID, Me.lblLeaveInfo.Text)
            Language._Object.setCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)

            Language._Object.setCaption(Me.GvLeaveDetails.Columns(0).FooterText, Me.GvLeaveDetails.Columns(0).HeaderText)
            Language._Object.setCaption(Me.GvLeaveDetails.Columns(1).FooterText, Me.GvLeaveDetails.Columns(1).HeaderText)
            Language._Object.setCaption(Me.GvLeaveDetails.Columns(2).FooterText, Me.GvLeaveDetails.Columns(2).HeaderText)
            Language._Object.setCaption(Me.GvLeaveDetails.Columns(3).FooterText, Me.GvLeaveDetails.Columns(3).HeaderText)
            Language._Object.setCaption(Me.GvLeaveDetails.Columns(4).FooterText, Me.GvLeaveDetails.Columns(4).HeaderText)
            Language._Object.setCaption(Me.GvLeaveDetails.Columns(5).FooterText, Me.GvLeaveDetails.Columns(5).HeaderText)
            Language._Object.setCaption(Me.GvLeaveDetails.Columns(6).FooterText, Me.GvLeaveDetails.Columns(6).HeaderText)
            Language._Object.setCaption(Me.GvLeaveDetails.Columns(7).FooterText, Me.GvLeaveDetails.Columns(7).HeaderText)
            Language._Object.setCaption(Me.GvLeaveDetails.Columns(8).FooterText, Me.GvLeaveDetails.Columns(8).HeaderText)
            Language._Object.setCaption(Me.GvLeaveDetails.Columns(9).FooterText, Me.GvLeaveDetails.Columns(9).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetLanguage()
        Try

            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Language._Object._LangId = CInt(Session("LangId"))
            'Pinkal (07-Mar-2019) -- End

            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Language._Object.getCaption(lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.lblLeaveInfo.Text = Language._Object.getCaption(lblLeaveInfo.ID, Me.lblLeaveInfo.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)

            Me.GvLeaveDetails.Columns(0).HeaderText = Language._Object.getCaption(Me.GvLeaveDetails.Columns(0).FooterText, Me.GvLeaveDetails.Columns(0).HeaderText)
            Me.GvLeaveDetails.Columns(1).HeaderText = Language._Object.getCaption(Me.GvLeaveDetails.Columns(1).FooterText, Me.GvLeaveDetails.Columns(1).HeaderText)
            Me.GvLeaveDetails.Columns(2).HeaderText = Language._Object.getCaption(Me.GvLeaveDetails.Columns(2).FooterText, Me.GvLeaveDetails.Columns(2).HeaderText)
            Me.GvLeaveDetails.Columns(3).HeaderText = Language._Object.getCaption(Me.GvLeaveDetails.Columns(3).FooterText, Me.GvLeaveDetails.Columns(3).HeaderText)
            Me.GvLeaveDetails.Columns(4).HeaderText = Language._Object.getCaption(Me.GvLeaveDetails.Columns(4).FooterText, Me.GvLeaveDetails.Columns(4).HeaderText)
            Me.GvLeaveDetails.Columns(5).HeaderText = Language._Object.getCaption(Me.GvLeaveDetails.Columns(5).FooterText, Me.GvLeaveDetails.Columns(5).HeaderText)
            Me.GvLeaveDetails.Columns(6).HeaderText = Language._Object.getCaption(Me.GvLeaveDetails.Columns(6).FooterText, Me.GvLeaveDetails.Columns(6).HeaderText)
            Me.GvLeaveDetails.Columns(7).HeaderText = Language._Object.getCaption(Me.GvLeaveDetails.Columns(7).FooterText, Me.GvLeaveDetails.Columns(7).HeaderText)
            Me.GvLeaveDetails.Columns(8).HeaderText = Language._Object.getCaption(Me.GvLeaveDetails.Columns(8).FooterText, Me.GvLeaveDetails.Columns(8).HeaderText)
            Me.GvLeaveDetails.Columns(9).HeaderText = Language._Object.getCaption(Me.GvLeaveDetails.Columns(9).FooterText, Me.GvLeaveDetails.Columns(9).HeaderText)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Pinkal (26-Feb-2019) -- End

    
End Class
