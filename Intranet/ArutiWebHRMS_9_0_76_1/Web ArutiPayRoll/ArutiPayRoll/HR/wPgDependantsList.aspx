﻿<%@ Page Title="Dependants List" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgDependantsList.aspx.vb" Inherits="wPgDependantsList" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}

    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Dependants List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td colspan="6" valign="top" style="width: 100%">
                                                <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Pending Employee" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboGender" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblDBFirstName" runat="server" Text="Firstname"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblDBRelation" runat="server" Text="Relation"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboRelation" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblBirthdate" runat="server" Text="Birtdate"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <uc2:DateCtrl ID="dtpBirthdate" runat="server" />
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblDBLastName" runat="server" Text="LastName"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblDBIdNo" runat="server" Text="ID. No."></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:TextBox ID="txtIdNo" runat="server"></asp:TextBox>
                                            </td>
                                            <td colspan="4" style="width: 56%">
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                     <asp:Button ID="mnuSetActiveInactive" runat="server" style="float:left;margin-right:10px;" CssClass="btndefault" Text="Set Active / Inactive" />
                                        <asp:Button ID="BtnNew" runat="server" CssClass="btndefault" Text="New" />
                                        <asp:Button ID="BtnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                        <asp:Button ID="BtnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="BtnClose" runat="server" CssClass="btndefault" Text="Close" />
                                      
                                        <asp:Panel ID="objtblPanel" runat="server" Style="float: left;">
                                            <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btndefault" Text="View Detail" />
                                            <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" BackColor="PowderBlue"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                            <asp:Label ID="lblParentData" runat="server" Text="Parent Detail" BackColor="LightCoral"
                                                Style="padding: 2px; font-weight: bold;"></asp:Label>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <asp:Panel ID="pnl_dgvDependantList" runat="server" ScrollBars="Auto" Width="100%">
                                    <asp:DataGrid ID="dgvDependantList" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                        HeaderStyle-Font-Bold="false" Width="99%">
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" CommandName="Select"
                                                            ToolTip="Edit"></asp:LinkButton>
                                                    </span>
                                                    <asp:LinkButton ID="imgDetail" runat="server" ToolTip="Highlight Original Details" CommandName="View"
                                                        Visible="false"><i class="fa fa-eye" style="font-size: 18px;"></i> </asp:LinkButton>
                                                    <asp:HiddenField ID="hfoprationtypeid" runat="server" Value='<%#Eval("operationtypeid") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--0--%>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                            CommandName="Delete"></asp:LinkButton>
                                                    </span>
                                                    <%--<asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                        CommandName="Delete" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%-- 1--%>
                                            <asp:BoundColumn DataField="DpndtBefName" HeaderText="Name" ReadOnly="true" FooterText="colhDBName">
                                            </asp:BoundColumn>
                                            <%-- 2--%>
                                            <asp:BoundColumn DataField="Relation" HeaderText="Relation" ReadOnly="true" FooterText="colhRelation">
                                            </asp:BoundColumn>
                                            <%-- 3--%>
                                            <asp:BoundColumn DataField="Gender" HeaderText="Gender" ReadOnly="true" FooterText="colhGender">
                                            </asp:BoundColumn>
                                            <%-- 4--%>
                                            <asp:BoundColumn DataField="birthdate" HeaderText="Birthdate" ReadOnly="true" FooterText="colhbirthdate">
                                            </asp:BoundColumn>
                                            <%-- 5--%>
                                            <asp:BoundColumn DataField="IdNo" HeaderText="Identify No." ReadOnly="true" FooterText="colhIdNo">
                                            </asp:BoundColumn>
                                            <%-- 6--%>
                                            <asp:BoundColumn DataField="EmpId" HeaderText="Employee ID" ReadOnly="true" Visible="false">
                                            </asp:BoundColumn>
                                            <%-- 7--%>
                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                            </asp:BoundColumn>
                                            <%-- 8--%>
                                            <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" FooterText="colhOperationType">
                                            </asp:BoundColumn>
                                            <%-- 9--%>
                                            <asp:BoundColumn DataField="DpndtTranId" HeaderText="DpndtTranId" Visible="false"
                                                FooterText="objdgcolhRefreeTranId"></asp:BoundColumn>
                                            <%-- 10--%>
                                            <asp:BoundColumn DataField="isactive" HeaderText="Is Active" Visible="false"
                                                FooterText="colhIsActive"></asp:BoundColumn>
                                            <%-- 11--%>
                                        </Columns>
                                    </asp:DataGrid>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you Sure You Want To delete?:" />
                    <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
