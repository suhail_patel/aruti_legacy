﻿<%@ Page Title="Employee Address" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgEmployeeAddress.aspx.vb" Inherits="wPgEmployeeAddress" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%--S.SANDEEP |26-APR-2019| -- START--%>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%--S.SANDEEP |26-APR-2019| -- END--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <%--S.SANDEEP |26-APR-2019| -- START--%>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <%--S.SANDEEP |26-APR-2019| -- END--%>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <%--S.SANDEEP |26-APR-2019| -- START    --%>

    <script type="text/javascript">
        function IsValidAttach() {
            debugger;
            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= popup_AttachementYesNo.ClientID %>_Panel1").css("z-index", "100002");
        }
        
    </script>

    <%--S.SANDEEP |26-APR-2019| -- END--%>
    <center>
        <asp:Panel ID="pnlAddress" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_main" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Address"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="Div7" class="panel-default">
                                <div id="Div8" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblpnl_Header" runat="server" Text="Address Detail"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteria" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblEmployee" Style="margin-left: 10px" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 85%">
                                                <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <%--Gajanan [18-Mar-2019] Start--%>
                                    <%--<div id="Div9" class="panel-default">--%>
                                    <asp:Panel ID="GbPresentAddress" runat="server" class="panel-default">
                                        <%--Gajanan [18-Mar-2019] End --%>
                                        <div id="FilterCriteriaTitle" class="panel-heading-default">
                                            <div style="float: left;">
                                                <asp:Label ID="lnPresentAddress" runat="server" Text="Present Address"></asp:Label>
                                            </div>
                                            <%--S.SANDEEP |26-APR-2019| -- START--%>
                                            <asp:LinkButton ID="lnkScanAttachPersonal" runat="server" Text="Attach Document"
                                                Style="padding: 2px; font-weight: bold; margin-top: -3px; float: right;" Font-Underline="false"></asp:LinkButton>
                                            <%--'S.SANDEEP |26-APR-2019| -- END--%>
                                            <asp:Label ID="lblpresentaddressapproval" Visible="false" runat="server" Text="Pending Approval"
                                                BackColor="PowderBlue" Style="padding: 2px; font-weight: bold; margin-top: -3px;
                                                float: right"></asp:Label>
                                        </div>
                                        <div id="FilterCriteriaBody" class="panel-body-default">
                                            <table>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblAddress" runat="server" Text="Address1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPAddress1" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblAddress2" runat="server" Text="Address2"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPAddress2" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblPostCountry" runat="server" Text="Post Country"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboPCountry" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblPresentState" runat="server" Text="State"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboPState" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblPostTown" runat="server" Text="Post Town"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboPCity" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblPostcode" runat="server" Text="Post Code"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboPZipcode" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblProvince" runat="server" Text="Prov/Region"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPRegion" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRoad" runat="server" Text="Road/Street"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPStreet" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblEstate" runat="server" Text="EState"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPEState" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <%--Gajanan [18-Mar-2019] Start--%>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblPersentProvince1" runat="server" Text="Prov/Region1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboPresentProvRegion1" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblPresentRoadStreet1" runat="server" Text="Road/Street1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboPresentRoadStreet1" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblPresentChiefdom" runat="server" Text="Chiefdom"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboPresentChiefdom" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblPresentVillage" runat="server" Text="Village"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboPresentVillage" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblPresentTown1" runat="server" Text="Town1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboPresentTown1" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblMobile" runat="server" Text="Mobile"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPMobile" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <%--Gajanan [18-Mar-2019] End--%>
                                                <%--Gajanan [18-Mar-2019] Start--%>
                                                <%--<tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblPloteNo" runat="server" Text="Plot"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPPlotNo" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblMobile" runat="server" Text="Mobile"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPMobile" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblAlternativeNo" runat="server" Text="Alt. No"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPAltNo" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblTelNo" runat="server" Text="Tel. No"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPTelNo" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblFax" runat="server" Text="Fax"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPFax" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPEmail" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblTelNo" runat="server" Text="Tel. No"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPTelNo" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblPloteNo" runat="server" Text="Plot"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPPlotNo" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblAlternativeNo" runat="server" Text="Alt. No"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPAltNo" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPEmail" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblFax" runat="server" Text="Fax"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtPFax" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <%--Gajanan [18-Mar-2019] Start--%>
                                            </table>
                                        </div>
                                        <%--Gajanan [18-Mar-2019] Start--%>
                                        <%--</div>--%>
                                        <%--S.SANDEEP |26-APR-2019| -- START--%>
                                        
                                        <%--S.SANDEEP |26-APR-2019| -- END--%>
                                                                            </asp:Panel>
                                    <%--Gajanan [18-Mar-2019] End --%>
                                    <%--Gajanan [18-Mar-2019] Start--%>
                                    <%--<div id="Div3" class="panel-default">--%>
                                    <asp:Panel ID="GbDomicileAddress" runat="server" class="panel-default">
                                        <%--Gajanan [18-Mar-2019] End --%>
                                        <div id="Div1" class="panel-heading-default">
                                            <div style="float: left;">
                                                <asp:Label ID="lnDomicileAddress" runat="server" Text="Domicile Address"></asp:Label>
                                            </div>
                                            <%--S.SANDEEP |26-APR-2019| -- START--%>
                                            <asp:LinkButton ID="lnkAttachDomicile" runat="server" Text="Attach Document" Style="padding: 2px;
                                                font-weight: bold; margin-top: -3px; float: right;" Font-Underline="false"></asp:LinkButton>
                                            <%--S.SANDEEP |26-APR-2019| -- END--%>
                                            <asp:Label ID="lbldomicileaddressapproval" runat="server" Visible="false" Text="Pending Approval"
                                                BackColor="PowderBlue" Style="padding: 2px; font-weight: bold; margin-top: -3px;
                                                float: right"></asp:Label>
                                        </div>
                                        <div id="Div2" class="panel-body-default">
                                            <table>
                                                <tr>
                                                    <%--Gajanan [18-Mar-2019] Start--%>
                                                    <%-- <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileAddress1" runat="server" Text="Address1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDAddress1" runat="server" ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileAddress2" runat="server" Text="Address2"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDAddress2" runat="server" ReadOnly="True"></asp:TextBox>
                                                    </td>--%>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileAddress1" runat="server" Text="Address1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDAddress1" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileAddress2" runat="server" Text="Address2"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDAddress2" runat="server"></asp:TextBox>
                                                    </td>
                                                    <%--Gajanan [18-Mar-2019] End--%>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicilePostCountry" runat="server" Text="Post Country"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <%--Gajanan [18-Mar-2019] Start--%>
                                                        <%--  <asp:DropDownList ID="cboDCountry" runat="server" AutoPostBack="True" Enabled="False">
                                                        </asp:DropDownList>--%>
                                                        <asp:DropDownList ID="cboDCountry" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                        <%--Gajanan [18-Mar-2019] End --%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileState" runat="server" Text="State"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <%--Gajanan [18-Mar-2019] Start--%>
                                                        <%-- <asp:DropDownList ID="cboDState" runat="server" AutoPostBack="True" Enabled="False">
                                                        </asp:DropDownList>--%>
                                                        <asp:DropDownList ID="cboDState" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                        <%--Gajanan [18-Mar-2019] End --%>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicilePostTown" runat="server" Text="Post Town"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <%--Gajanan [18-Mar-2019] Start--%>
                                                        <%--<asp:DropDownList ID="cboDCity" runat="server" AutoPostBack="True" Enabled="False">
                                                        </asp:DropDownList>--%>
                                                        <asp:DropDownList ID="cboDCity" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                        <%--Gajanan [18-Mar-2019] End --%>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicilePostCode" runat="server" Text="Post Code"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <%--Gajanan [18-Mar-2019] Start--%>
                                                        <%--<asp:DropDownList ID="cboDZipcode" runat="server" Enabled="False">
                                                        </asp:DropDownList>--%>
                                                        <asp:DropDownList ID="cboDZipcode" runat="server">
                                                        </asp:DropDownList>
                                                        <%--Gajanan [18-Mar-2019] End --%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileProvince" runat="server" Text="Prov/Region"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDRegion" runat="server" ></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileRoad" runat="server" Text="Road/Street"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDStreet" runat="server" ></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileEState" runat="server" Text="EState"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDEState" runat="server" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <%--Gajanan [18-Mar-2019] Start--%>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileProvince1" runat="server" Text="Prov/Region1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboDomicileProvince1" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileRoadStreet1" runat="server" Text="Road/Street1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboDomicileRoadStreet1" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileChiefdom" runat="server" Text="Chiefdom"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboDomicileChiefdom" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicilevillage" runat="server" Text="Village"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboDomicilevillage" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileTown1" runat="server" Text="Town1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboDomicileTown1" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileMobileNo" runat="server" Text="Mobile"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDMobile" runat="server" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <%--Gajanan [18-Mar-2019] End--%>
                                                <%--Gajanan [18-Mar-2019] Start--%>
                                                <%--<tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicilePlotNo" runat="server" Text="Plot"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDPlotNo" runat="server" ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileMobileNo" runat="server" Text="Mobile"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDMobile" runat="server" ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileAltNo" runat="server" Text="Alt. No"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDAltNo" runat="server" ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileTelNo" runat="server" Text="Tel. No"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDTelNo" runat="server" ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileFax" runat="server" Text="Fax"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDFax" runat="server" ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileEmail" runat="server" Text="Email"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDEmail" runat="server" ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                </tr>--%>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileTelNo" runat="server" Text="Tel. No"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDTelNo" runat="server" ></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicilePlotNo" runat="server" Text="Plot"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDPlotNo" runat="server" ></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileAltNo" runat="server" Text="Alt. No"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDAltNo" runat="server" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileEmail" runat="server" Text="Email"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDEmail" runat="server" ></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblDomicileFax" runat="server" Text="Fax"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtDFax" runat="server" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <%--Gajanan [18-Mar-2019] End--%>
                                            </table>
                                        </div>
                                        <%--Gajanan [18-Mar-2019] Start--%>
                                        <%--</div>--%></asp:Panel>
                                    <%--Gajanan [18-Mar-2019] End --%>
                                    <%--Gajanan [18-Mar-2019] Start--%>
                                    <%--<div id="Div4" class="panel-default">--%>
                                    <asp:Panel ID="gbRecruitmentAddress" runat="server" class="panel-default">
                                        <%--Gajanan [18-Mar-2019] End --%>
                                        <div id="Div5" class="panel-heading-default">
                                            <div style="float: left;">
                                                <asp:Label ID="lnRecruitmentAddress" runat="server" Text="Recruitment Address"></asp:Label>
                                            </div>
                                            <%--S.SANDEEP |26-APR-2019| -- START--%>
                                            <asp:LinkButton ID="lnkAttachRecruitment" runat="server" Text="Attach Document" Style="padding: 2px;
                                                font-weight: bold; margin-top: -3px; float: right;" Font-Underline="false"></asp:LinkButton>
                                            <%--S.SANDEEP |26-APR-2019| -- END--%>
                                            <asp:Label ID="lblrecruitmentaddressapproval" runat="server" Visible="false" Text="Pending Approval"
                                                BackColor="PowderBlue" Style="padding: 2px; font-weight: bold; margin-top: -3px;
                                                float: right"></asp:Label>
                                        </div>
                                        <div id="Div6" class="panel-body-default">
                                            <table>
                                                <tr>
                                                    <%--Gajanan [18-Mar-2019] Start --%>
                                                    <%--<td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRAddress1" runat="server" Text="Address1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtRAddress1" runat="server" ReadOnly="True"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRAddress2" runat="server" Text="Address2"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtRAddress2" runat="server" ReadOnly="True"></asp:TextBox>
                                                    </td>--%>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRAddress1" runat="server" Text="Address1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtRAddress1" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRAddress2" runat="server" Text="Address2"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtRAddress2" runat="server"></asp:TextBox>
                                                    </td>
                                                    <%--Gajanan [18-Mar-2019] End --%>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRCountry" runat="server" Text="Post Country"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <%--Gajanan [18-Mar-2019] Start--%>
                                                        <%--<asp:DropDownList ID="cboRCountry" runat="server" AutoPostBack="True" Enabled="False">
                                                        </asp:DropDownList>--%>
                                                        <asp:DropDownList ID="cboRCountry" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                        <%--Gajanan [18-Mar-2019] End --%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRState" runat="server" Text="State"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <%--Gajanan [18-Mar-2019] Start--%>
                                                        <%--<asp:DropDownList ID="cboRState" runat="server" AutoPostBack="True" Enabled="False">
                                                        </asp:DropDownList>--%>
                                                        <asp:DropDownList ID="cboRState" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                        <%--Gajanan [18-Mar-2019] End --%>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRCity" runat="server" Text="Post Town"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <%--Gajanan [18-Mar-2019] Start--%>
                                                        <%-- <asp:DropDownList ID="cboRCity" runat="server" AutoPostBack="True" Enabled="False">
                                                        </asp:DropDownList>--%>
                                                        <asp:DropDownList ID="cboRCity" runat="server" AutoPostBack="True">
                                                        </asp:DropDownList>
                                                        <%--Gajanan [18-Mar-2019] End --%>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRCode" runat="server" Text="Post Code"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <%--Gajanan [18-Mar-2019] Start--%>
                                                        <%--<asp:DropDownList ID="cboRZipcode" runat="server" Enabled="False">
                                                        </asp:DropDownList>--%>
                                                        <asp:DropDownList ID="cboRZipcode" runat="server">
                                                        </asp:DropDownList>
                                                        <%--Gajanan [18-Mar-2019] End --%>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRRegion" runat="server" Text="Prov/Region"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtRRegion" runat="server" ></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRStreet" runat="server" Text="Road/Street"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtRStreet" runat="server" ></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblREState" runat="server" Text="EState"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtREState" runat="server" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <%--Gajanan [18-Mar-2019] Start--%>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRecruitmentProvince1" runat="server" Text="Prov/Region1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboRecruitmentProvince1" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRecruitmentRoadStreet1" runat="server" Text="Road/Street1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboRecruitmentRoadStreet1" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRecruitmentChiefdom" runat="server" Text="Chiefdom"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboRecruitmentChiefdom" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRecruitmentVillage" runat="server" Text="Village"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboRecruitmentVillage" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRecruitmentTown1" runat="server" Text="Town1"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:DropDownList ID="cboRecruitmentTown1" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRPlotNo" runat="server" Text="Plot"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtRPlotNo" runat="server" ></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <%--Gajanan [18-Mar-2019] End--%>
                                                <tr>
                                                    <td align="left" style="width: 12%">
                                                        <asp:Label ID="lblRTelNo" runat="server" Text="Tel. No"></asp:Label>
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                        <asp:TextBox ID="txtRTelNo" runat="server" ></asp:TextBox>
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                    </td>
                                                    <td align="left" style="width: 12%">
                                                    </td>
                                                    <td align="left" style="width: 15%">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <%--Gajanan [18-Mar-2019] Start--%>
                                        <%--</div>--%></asp:Panel>
                                    <%--Gajanan [18-Mar-2019] End --%>
                                    
                                    <div id="ScanAttachment">
                                            <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="ModalPopupBG"
                                                TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" DropShadow="true"
                                                CancelControlID="hdf_ScanAttchment">
                                            </cc1:ModalPopupExtender>
                                            <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="newpopup" Width="600px"
                                                Style="display: none;">
                                                <div class="panel-primary" style="margin: 0px">
                                                    <div class="panel-heading">
                                                        <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                                                        <asp:Label ID="objlblCaption" runat="server" Text="Scan/Attchment" Visible="false"></asp:Label>
                                                    </div>
                                                    <div class="panel-body">
                                                        <div id="Div36" class="panel-default">
                                                            <div id="Div37" class="panel-body-default">
                                                                <table style="width: 100%">
                                                                    <tr style="width: 100%">
                                                                        <td style="width: 30%">
                                                                            <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                                        </td>
                                                                        <td style="width: 40%">
                                                                            <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server"
                                                                                Width="200px">
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td style="width: 30%">
                                                                            <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                                                <div id="fileuploader">
                                                                                    <input type="button" id="btnAddFile" runat="server" class="btndefault" value="Browse"
                                                                                        onclick="return IsValidAttach();" />
                                                                                </div>
                                                                            </asp:Panel>
                                                                            <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                                                                Text="Browse" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="width: 100%">
                                                                        <td colspan="3" style="width: 100%">
                                                                            <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                HeaderStyle-Font-Bold="false" Width="99%">
                                                                                <Columns>
                                                                                    <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                                                    ToolTip="Delete"></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%--0--%>
                                                                                    <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                                                    <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                                                    <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                                                        <ItemTemplate>
                                                                                            <span class="gridiconbc">
                                                                                                <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                                                            </span>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%--1--%>
                                                                                    <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                                                    <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                                    <%--2--%>
                                                                                    <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                                                    <%--3--%>
                                                                                    <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                                                    <%--4--%>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <div class="btn-default">
                                                                    <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                                    <div style="float: left">
                                                                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                                                    </div>
                                                                    <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                                    <asp:Button ID="btnScanSave" runat="server" Text="Add" CssClass="btnDefault" />
                                                                    <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btnDefault" />
                                                                    <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                            <ucCfnYesno:Confirmation ID="popup_AttachementYesNo" runat="server" Message="" Title="Confirmation"
                                                IsFireButtonNoClick="false" />
                                        </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnsave" runat="server" CssClass="btndefault" Text="Update" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                <Triggers>
                    <asp:PostBackTrigger ControlID="dgv_Attchment" />
                    <asp:PostBackTrigger ControlID="btnDownloadAll" />
                </Triggers>
                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    
    <%--S.SANDEEP |26-APR-2019| -- START--%>

    <script type="text/javascript">
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPgEmployeeAddress.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $('input[type=file]').live("click", function() {
            debugger;
            return IsValidAttach();
        });
        
    </script>

    <%--S.SANDEEP |26-APR-2019| -- END--%>
</asp:Content>
