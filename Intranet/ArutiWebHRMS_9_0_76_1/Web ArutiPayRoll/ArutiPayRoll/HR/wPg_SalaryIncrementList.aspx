﻿<%@ Page Title="Salary Change List" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_SalaryIncrementList.aspx.vb" Inherits="HR_wPg_SalaryIncrementList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Salary Change List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 9%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" />
                                            </td>
                                            <td style="width: 22%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 7%">
                                                <asp:Label ID="lblAccess" runat="server" Text="Pay Period" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 9%">
                                                <asp:Label ID="lblSalaryChangeApprovalStatus" runat="server" Text="Approval Status" />
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboSalaryChangeApprovalStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="width: 99%; overflow: auto; height: 275px; margin-left: 6px">
                                        <asp:GridView ID="dgSalaryIncrement" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                            ShowFooter="False" Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                            RowStyle-CssClass="griviewitem" DataKeyNames="IsGrp" >
                                            <Columns>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                    <HeaderTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" OnCheckedChanged="chkSelectAll_CheckedChanged"
                                                                AutoPostBack="true" />
                                                        </span>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:CheckBox ID="chkSelect" runat="server" OnCheckedChanged="chkSelect_CheckedChanged"
                                                                AutoPostBack="true" />
                                                        </span>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImgSelect" runat="server" ImageUrl="~/images/edit.png" ToolTip="Edit"
                                                            CommandName="Change" CommandArgument="<%# Container.DataItemIndex %>" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                            CommandName="Remove" CommandArgument="<%# Container.DataItemIndex %>" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:CommandField ButtonType="Image" ItemStyle-HorizontalAlign="Center" DeleteImageUrl ="~/images/remove.png" ShowDeleteButton="true" HeaderText="Delete"  HeaderStyle-HorizontalAlign="Center" CommandArgument="<%# Container.DataItemIndex %>"  />--%>
                                                <asp:BoundField DataField="period_name" HeaderText="Pay Period" ReadOnly="True" FooterText="colhPayPeriod" />
                                                <asp:BoundField DataField="incrementdate" HeaderText="Increment Date" ReadOnly="True"
                                                    ItemStyle-Width="150px" FooterText="colhIncrDate" />
                                                <asp:BoundField DataField="currentscale" HeaderText="Current Scale" ReadOnly="True"
                                                    ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Right" FooterText="colhScale" />
                                                <asp:BoundField DataField="increment" HeaderText="Change Amount" ReadOnly="true"
                                                    ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Right" FooterText="colhIncrementAmt" />
                                                <asp:BoundField DataField="newscale" HeaderText="New Scale" ReadOnly="True" ItemStyle-Width="100px"
                                                    ItemStyle-HorizontalAlign="Right" FooterText="colhNewScale" />
                                                <asp:BoundField DataField="isapproved" HeaderText="Status" ReadOnly="True" ItemStyle-Width="60px"
                                                    ItemStyle-HorizontalAlign="Center" FooterText="colhApprovePending" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" Visible="false" CssClass="btnDefault" />
                                        <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btnDefault" />
                                        <asp:Button ID="btnDisapprove" runat="server" Text="Disapprove" CssClass="btnDefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnDefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc9:ConfirmYesNo ID="popupApprove" runat="server" Title="Approve Salary Change" />
                    <uc9:ConfirmYesNo ID="popupDisapprove" runat="server" Title="Disapprove Salary Change" />
                    <uc10:DeleteReason ID="popRejectReason" runat="server" Title="Comments" ValidationGroup="popRejectReason" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
