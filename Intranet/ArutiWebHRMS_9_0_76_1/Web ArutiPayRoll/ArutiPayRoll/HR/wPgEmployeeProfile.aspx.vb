﻿Option Strict On 'Shani(19-MAR-2016)
#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
Imports System.Net.Dns 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
#End Region

Partial Class wPgEmployeeProfile
    Inherits Basepage

#Region " Private Variable(s) "

    Private msg As New CommonCodes
    Private clsuser As New User
    Private objEmployee As New clsEmployee_Master

    'S.SANDEEP [ 11 MAR 2014 ] -- START
    'Dim strExpression As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
    'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
    Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
    'S.SANDEEP [ 11 MAR 2014 ] -- END

    'S.SANDEEP [ 31 DEC 2013 ] -- START
    Private objCONN As SqlConnection
    'S.SANDEEP [ 31 DEC 2013 ] -- END

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeMaster"
    Private ReadOnly mstrModuleName1 As String = "frmEmployeeList"
    Private ReadOnly mstrModuleName2 As String = "frmJobs_AddEdit"
    'Pinkal (06-May-2014) -- End
    'Sohail (09 Nov 2020) -- Start
    'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
    Private mstrURLReferer As String = ""
    'Sohail (09 Nov 2020) -- End
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objcommonmaster As New clsCommon_Master
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                'Shani(24-Aug-2015) -- End

                'Sohail (23 Apr 2012) -- End
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombos.Tables("Employee")
                    .DataBind()

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Change in Employee Profile Pages due to wrong assignment of session("employeeunkid").
                    '.SelectedValue = Session("Employeeunkid")
                    .SelectedValue = CStr(Session("EmpUnkid"))
                    'Shani(24-Aug-2015) -- End
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                    .SelectedValue = CStr(Session("Employeeunkid"))
                End With
            End If

            With cboTitle
                .DataSource = objcommonmaster.getComboList(clsCommon_Master.enCommonMaster.TITLE, True, "list")
                .DataTextField = "name"
                .DataValueField = "masterunkid"
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally

        End Try
    End Sub


    'Pinkal (27-Mar-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub DeleteImage()
        If imgEmp.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False AndAlso System.IO.File.Exists(Server.MapPath(imgEmp.ImageUrl)) Then
            Try


                Dim mstrFile As String = Server.MapPath(imgEmp.ImageUrl)
                imgEmp.ImageUrl = ""
                'Shani(01-MAR-2017) -- Start
                'System.IO.File.Delete(mstrFile)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Delete_File", "DeleteFile('" & mstrFile.Replace("\", "\\") & "');", True)
                'Shani(01-MAR-2017) -- End
                Fill_Info(CInt(cboEmployee.SelectedValue))
            Catch ex As IO.IOException
                If ex.Message.ToString.Trim.Contains("used by another process") Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage("File is used by another process or File is already open.", Me)
                    'Sohail (23 Mar 2019) -- End
                End If
            Catch ex As Exception
                msg.DisplayError(ex, Me)
            End Try
        End If
    End Sub



    'Pinkal (27-Mar-2013) -- End

    'Private Sub ImageOperation(ByVal imgFullPath As String)
    '    Try
    '        Dim f As New System.IO.FileInfo(imgFullPath)
    '        If System.IO.File.Exists(Server.MapPath("~\images\" & f.Name)) = False Then
    '            Dim rbyte() As Byte = IO.File.ReadAllBytes(imgFullPath)
    '            Dim mem As New System.IO.MemoryStream(rbyte)
    '            Drawing.Image.FromStream(mem).Save(Server.MapPath("~\images\" & f.Name))
    '            If f.Name <> objEmployee._ImagePath Then
    '                Drawing.Image.FromStream(mem).Save(ConfigParameter._Object._PhotoPath & "\" & f.Name)
    '                Me.ViewState("OldImage") = Me.ViewState("ImageName")
    '            End If
    '        End If
    '        imgEmp.ImageUrl = "~\images\" & f.Name
    '        Me.ViewState("ImageName") = f.Name
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    Private Sub Clear_Controls()
        Try
            txtCodeValue.Text = ""
            txtEmploymentTypeValue.Text = ""
            txtFirstValue.Text = ""
            txtGenderValue.Text = ""
            txtIdValue.Text = ""
            txtOtherNameValue.Text = ""
            txtPayPointValue.Text = ""
            txtPayValue.Text = ""
            txtShiftValue.Text = ""
            txtSurnameValue.Text = ""
            imgEmp.ImageUrl = ""
            txtcoEmail.Text = ""

            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes

            'If CBool(Session("IsImgInDataBase")) Then
            '    imgEmp.ImageUrl = ""
            'End If

            'Pinkal (01-Apr-2013) -- End
            'Gajanan [13-June-2020] -- Start
            'Enhancement NMB Employee Declaration Changes.
            imgSignature.ImageUrl = ""
            'Gajanan [13-June-2020] -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Info(ByVal intEmpId As Integer)
        Dim objCommon As New clsCommon_Master

        'Pinkal (03-Jul-2013) -- Start
        'Enhancement : TRA Changes

        'Dim objShift As New clsshift_master
        Dim objShift As New clsNewshift_master

        'Pinkal (03-Jul-2013) -- End


        Dim objPayPoint As New clspaypoint_master
        Try


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = intEmpId
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = intEmpId
            'Shani(20-Nov-2015) -- End


            txtCodeValue.Text = objEmployee._Employeecode
            txtFirstValue.Text = objEmployee._Firstname
            txtSurnameValue.Text = objEmployee._Surname
            txtOtherNameValue.Text = objEmployee._Othername

            objCommon._Masterunkid = objEmployee._Employmenttypeunkid
            txtEmploymentTypeValue.Text = objCommon._Name

            If objEmployee._Paytypeunkid > 0 Then
                objCommon._Masterunkid = objEmployee._Paytypeunkid
                txtPayValue.Text = objCommon._Name
            Else
                txtPayValue.Text = ""
            End If
            If objEmployee._Email <> "" Then
                txtcoEmail.Text = objEmployee._Email
            Else
                txtcoEmail.Text = ""
            End If


            Select Case objEmployee._Gender
                Case 1  'MALE
                    txtGenderValue.Text = "Male"
                Case 2  'FEMALE
                    txtGenderValue.Text = "Female"
                Case Else
                    txtGenderValue.Text = ""
            End Select

            objPayPoint._Paypointunkid = objEmployee._Paypointunkid
            txtPayPointValue.Text = objPayPoint._Paypointname

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            'objShift._Shiftunkid = objEmployee._Shiftunkid
            'txtShiftValue.Text = objShift._Shiftname
            Dim objShiftTran As New clsEmployee_Shift_Tran
            Dim iShiftUnkid As Integer = objShiftTran.GetEmployee_Current_ShiftId(ConfigParameter._Object._CurrentDateAndTime, intEmpId)
            objShift._Shiftunkid = iShiftUnkid
            txtShiftValue.Text = objShift._Shiftname
            'S.SANDEEP [ 22 OCT 2013 ] -- END





            'Anjan (24 Nov 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            'If objEmployee._Titalunkid > 0 Then
            '    objCommon._Masterunkid = objEmployee._Titalunkid
            '    txtTitelValue.Text = objCommon._Name
            'Else
            '    txtTitelValue.Text = ""
            'End If


            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes

            If objEmployee._Titalunkid < 0 Then
                cboTitle.SelectedValue = CStr(0)
            Else
                cboTitle.SelectedValue = CStr(objEmployee._Titalunkid)
            End If




            'Anjan (24 Nov 2012)-End 


            'If objEmployee._ImagePath <> "" Then
            '    Dim Path As String = ""

            '    If Aruti.Data.ConfigParameter._Object._PhotoPath.LastIndexOf("\") = Aruti.Data.ConfigParameter._Object._PhotoPath.ToString.Trim.Length - 1 Then
            '        Path = System.IO.Path.GetFullPath(Aruti.Data.ConfigParameter._Object._PhotoPath & objEmployee._ImagePath)
            '        Call ImageOperation(Path)
            '    Else
            '        Path = System.IO.Path.GetFullPath(Aruti.Data.ConfigParameter._Object._PhotoPath & "\" & objEmployee._ImagePath)
            '        Call ImageOperation(Path)
            '    End If
            'End If


            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes

            If objEmployee._blnImgInDb Then

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objEmployee._Employeeunkid > 0 Then
                If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
                    'Shani(20-Nov-2015) -- End

                    If objEmployee._Photo IsNot Nothing Then
                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'imgEmp.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid & "&ModeID=1"
                        imgEmp.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
                        'Shani(20-Nov-2015) -- End
                    Else
                        imgEmp.ImageUrl = ""
                    End If
                End If
            End If

            'Pinkal (01-Apr-2013) -- End

            'S.SANDEEP [16-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 118
            If objEmployee._EmpSignature IsNot Nothing Then
                imgSignature.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=3"
            End If
            'S.SANDEEP [16-Jan-2018] -- END

            Dim objEIdTran As New clsIdentity_tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEIdTran._EmployeeUnkid = objEmployee._Employeeunkid
            objEIdTran._EmployeeUnkid = objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date)
            'Shani(20-Nov-2015) -- End

            Dim dtTable = objEIdTran._DataList
            If dtTable.Rows.Count > 0 Then
                Dim dtTemp() As DataRow = dtTable.Select("isdefault = true")
                If dtTemp.Length > 0 Then
                    txtIdValue.Text = CStr(dtTemp(0)("identity_no"))
                End If
            Else
                txtIdValue.Text = ""
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
        'S.SANDEEP [ 31 DEC 2013 ] -- START
        If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

            'Sohail (02 Apr 2019) -- Start
            'NMB Issue - 74.1 - Error "On Load Event !! Bad Data" on clicking any page with link after session get expired.
            If Request.QueryString.Count <= 0 Then Exit Sub
            'Sohail (02 Apr 2019) -- End

            'S.SANDEEP |17-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : PM ERROR
            KillIdleSQLSessions()
            'S.SANDEEP |17-MAR-2020| -- END
            objCONN = Nothing
            If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                objCONN = New SqlConnection
                objCONN.ConnectionString = constr
                objCONN.Open()
                HttpContext.Current.Session("gConn") = objCONN
            End If
            Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
            If arr.Length = 4 Then
                Session("iQueryString") = Request.QueryString.ToString
                Try
                    If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                    Else
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                        HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                    End If

                Catch ex As Exception
                    HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                    HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                End Try

                Blank_ModuleName()
                objEmployee._WebFormName = "frmEmployeeMaster"
                StrModuleName2 = "mnuCoreSetups"
                objEmployee._WebClientIP = CStr(Session("IP_ADD"))
                objEmployee._WebHostName = CStr(Session("HOST_NAME"))
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    objEmployee._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                End If

                HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                HttpContext.Current.Session("UserId") = CInt(arr(1))
                HttpContext.Current.Session("ShowPending") = CInt(arr(3))

                'Sohail (30 Mar 2015) -- Start
                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                'Dim objCommon As New CommonCodes
                'objCommon.GetCompanyYearInfo(CInt(Session("CompanyUnkId")))
                Dim strError As String = ""
                If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                    msg.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                    Exit Sub
                End If
                'Sohail (30 Mar 2015) -- End
                HttpContext.Current.Session("mdbname") = Session("Database_Name")
                gobjConfigOptions = New clsConfigOptions
                gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                Session("IsImgInDataBase") = gobjConfigOptions._IsImgInDataBase
                Session("CompanyDomain") = gobjConfigOptions._CompanyDomain

                'S.SANDEEP [16-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 118
                Session("AllowtoViewSignatureESS") = gobjConfigOptions._AllowtoViewSignatureESS
                'S.SANDEEP [16-Jan-2018] -- END

                ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))

                CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                ArtLic._Object = New ArutiLic(False)
                If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                    Dim objGroupMaster As New clsGroup_Master
                    objGroupMaster._Groupunkid = 1
                    ArtLic._Object.HotelName = objGroupMaster._Groupname
                End If

                If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management)) = False Then
                    msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                    Exit Sub
                End If
                If ConfigParameter._Object._IsArutiDemo Then
                    If ConfigParameter._Object._IsExpire Then
                        msg.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                        Exit Sub
                    Else
                        If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                            msg.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Sub
                        End If
                    End If
                End If
                Dim clsConfig As New clsConfigOptions
                If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                    Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                Else
                    Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                End If

                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = CInt(Session("UserId"))

                HttpContext.Current.Session("Login") = True

                'Sohail (21 Mar 2015) -- Start
                'Enhancement - New UI Notification Link Changes.
                'Dim clsuser As New User(objUser._Username, objUser._Password, Global.User.en_loginby.User, Session("mdbname"))
                Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                Call GetDatabaseVersion()
                Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))
                'Sohail (21 Mar 2015) -- End
                HttpContext.Current.Session("clsuser") = clsuser
                HttpContext.Current.Session("UserName") = clsuser.UserName
                HttpContext.Current.Session("Firstname") = clsuser.Firstname
                HttpContext.Current.Session("Surname") = clsuser.Surname
                HttpContext.Current.Session("MemberName") = clsuser.MemberName
                'Sohail (30 Mar 2015) -- Start
                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                HttpContext.Current.Session("UserId") = clsuser.UserID
                HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                HttpContext.Current.Session("Password") = clsuser.password
                HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                strError = ""
                If SetUserSessions(strError) = False Then
                    msg.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                    Exit Sub
                End If

                strError = ""
                If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                    msg.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                    Exit Sub
                End If
                'Sohail (30 Mar 2015) -- End

                Dim objUserPrivilege As New clsUserPrivilege
                objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                Session("AllowToApproveEmployee") = objUserPrivilege._AllowToApproveEmployee
                Session("EditEmployee") = objUserPrivilege._EditEmployee
                Session("AllowtoChangeCompanyEmail") = objUserPrivilege._AllowtoChangeCompanyEmail
                Session("ViewEmployee") = objUserPrivilege._AllowToViewEmpList
                Session("ViewScale") = objUserPrivilege._AllowTo_View_Scale
                Session("AllowtoChangeBranch") = objUserPrivilege._AllowtoChangeBranch
                Session("AllowtoChangeDepartmentGroup") = objUserPrivilege._AllowtoChangeDepartmentGroup
                Session("AllowtoChangeDepartment") = objUserPrivilege._AllowtoChangeDepartment
                Session("AllowtoChangeSectionGroup") = objUserPrivilege._AllowtoChangeSectionGroup
                Session("AllowtoChangeSection") = objUserPrivilege._AllowtoChangeSection
                Session("AllowtoChangeUnitGroup") = objUserPrivilege._AllowtoChangeUnitGroup
                Session("AllowtoChangeUnit") = objUserPrivilege._AllowtoChangeUnit
                Session("AllowtoChangeTeam") = objUserPrivilege._AllowtoChangeTeam
                Session("AllowtoChangeJobGroup") = objUserPrivilege._AllowtoChangeJobGroup
                Session("AllowtoChangeJob") = objUserPrivilege._AllowtoChangeJob
                Session("AllowtoChangeGradeGroup") = objUserPrivilege._AllowtoChangeGradeGroup
                Session("AllowtoChangeGrade") = objUserPrivilege._AllowtoChangeGrade
                Session("AllowtoChangeGradeLevel") = objUserPrivilege._AllowtoChangeGradeLevel
                Session("AllowtoChangeClassGroup") = objUserPrivilege._AllowtoChangeClassGroup
                Session("AllowtoChangeClass") = objUserPrivilege._AllowtoChangeClass
                Session("AllowtoChangeCostCenter") = objUserPrivilege._AllowtoChangeCostCenter
                Session("SetReinstatementdate") = objUserPrivilege._AllowtoSetEmpReinstatementDate
                Session("ChangeConfirmationDate") = objUserPrivilege._AllowtoChangeConfirmationDate
                Session("ChangeAppointmentDate") = objUserPrivilege._AllowtoChangeAppointmentDate
                Session("SetEmployeeBirthDate") = objUserPrivilege._AllowtoSetEmployeeBirthDate
                Session("SetEmpSuspensionDate") = objUserPrivilege._AllowtoSetEmpSuspensionDate
                Session("SetEmpProbationDate") = objUserPrivilege._AllowtoSetEmpProbationDate
                Session("SetEmploymentEndDate") = objUserPrivilege._AllowtoSetEmploymentEndDate
                Session("SetLeavingDate") = objUserPrivilege._AllowtoSetLeavingDate
                Session("ChangeRetirementDate") = objUserPrivilege._AllowtoChangeRetirementDate

                'S.SANDEEP [16-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 118
                Session("AllowtoAddEmployeeSignature") = objUserPrivilege._AllowtoAddEmployeeSignature
                Session("AllowtoDeleteEmployeeSignature") = objUserPrivilege._AllowtoDeleteEmployeeSignature
                Session("AllowtoSeeEmployeeSignature") = objUserPrivilege._AllowtoSeeEmployeeSignature
                'S.SANDEEP [16-Jan-2018] -- END

                If CBool(Session("AllowToApproveEmployee")) = False Then
                    Session("ShowPending") = Nothing
                End If

                If Session("ShowPending") IsNot Nothing Then
                    chkShowPending.Checked = CBool(Session("ShowPending"))
                    Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                    chkShowPending.Enabled = False
                End If

                flUpload.Visible = CBool(Session("IsImgInDataBase"))
                btnRemoveImage.Visible = CBool(Session("IsImgInDataBase"))
                imgEmp.Visible = CBool(Session("IsImgInDataBase"))

            'Gajanan [13-June-2020] -- Start
            'Enhancement NMB Employee Declaration Changes.
                flUploadsig.Visible = CBool(Session("AllowtoAddEmployeeSignature"))
                btnRemoveSig.Visible = CBool(Session("AllowtoDeleteEmployeeSignature"))
                imgSignature.Visible = CBool(Session("AllowtoSeeEmployeeSignature"))
            'Gajanan [13-June-2020] -- End

                cboEmployee.Enabled = CBool(Session("ViewEmployee"))
                'Sohail (30 Mar 2015) -- Start
                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                'Sohail (30 Mar 2015) -- End
                Exit Sub
            End If
        Else
            If Session("iQueryString") IsNot Nothing AndAlso Session("iQueryString").ToString <> "" Then
                chkShowPending.Enabled = False
            End If
        End If
        'S.SANDEEP [ 31 DEC 2013 ] -- END


        If Session("clsuser") Is Nothing Then
            Exit Sub
        End If

        'S.SANDEEP [ 04 JULY 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES

        Blank_ModuleName()
        objEmployee._WebFormName = "frmEmployeeMaster"
        StrModuleName2 = "mnuCoreSetups"
        objEmployee._WebClientIP = CStr(Session("IP_ADD"))
        objEmployee._WebHostName = CStr(Session("HOST_NAME"))
        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
            objEmployee._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
        End If

        'S.SANDEEP [ 04 JULY 2012 ] -- END



        'Pinkal (27-Mar-2013) -- Start
        'Enhancement : TRA Changes
        objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
        objEmployee._blnImgInDb = CBool(Session("IsImgInDataBase"))
        'Pinkal (27-Mar-2013) -- End


        'Pinkal (06-May-2014) -- Start
        'Enhancement : Language Changes 
        SetLnaguae()
        'Pinkal (06-May-2014) -- End


        If (Page.IsPostBack = False) Then
            clsuser = CType(Session("clsuser"), User)
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Aruti.Data.User._Object._Userunkid = Session("UserId")
                'Sohail (23 Apr 2012) -- End
                pnlCombo.Visible = True
                'Pinkal (22-Nov-2012) -- Start
                'Enhancement : TRA Changes

                ' txtcoEmail.ReadOnly = True
                ' btnsave.Visible = False

                btnsave.Visible = CBool(Session("EditEmployee"))

                'Pinkal (22-Nov-2012) -- End
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Page.Title = "Employee Profile"
                'S.SANDEEP [ 27 APRIL 2012 ] -- END


                'Anjan (03 Dec 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew's Request
                txtcoEmail.ReadOnly = Not CBool(Session("AllowtoChangeCompanyEmail"))
                'Anjan (03 Dec 2012)-End 

                'S.SANDEEP [ 31 DEC 2013 ] -- START
                chkShowPending.Visible = CBool(Session("AllowToApproveEmployee"))
                'S.SANDEEP [ 31 DEC 2013 ] -- END

                'S.SANDEEP [16-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 118
                '*************************** CURRENTLY GIVEN TO VIEW ONLY NOT ADD/DELETE
                'flUploadsig.Visible = False
                'btnRemoveSig.Visible = False
                'flUploadsig.Visible = CBool(Session("AllowtoAddEmployeeSignature"))
                'btnRemoveSig.Visible = CBool(Session("AllowtoDeleteEmployeeSignature"))
                'S.SANDEEP [16-Jan-2018] -- END

                flUploadsig.Visible = CBool(Session("AllowtoAddEmployeeSignature"))
                btnRemoveSig.Visible = CBool(Session("AllowtoDeleteEmployeeSignature"))
                imgSignature.Visible = CBool(Session("AllowtoSeeEmployeeSignature"))
                lblESign.Visible = imgSignature.Visible

            Else
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'Aruti.Data.User._Object._Userunkid = -1

                'Sohail (23 Apr 2012) -- End
                pnlCombo.Visible = False
                txtcoEmail.ReadOnly = False
                btnsave.Visible = True
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                Page.Title = "MY Profile"
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                'S.SANDEEP [ 06 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                txtcoEmail.ReadOnly = Not CBool(Session("AllowChangeCompanyEmail"))
                'S.SANDEEP [ 06 DEC 2012 ] -- END

                'S.SANDEEP [ 31 DEC 2013 ] -- START
                chkShowPending.Visible = False
                'S.SANDEEP [ 31 DEC 2013 ] -- END

                'S.SANDEEP [16-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 118
                imgSignature.Visible = CBool(Session("AllowtoViewSignatureESS"))

	            'Gajanan [13-June-2020] -- Start
	            'Enhancement NMB Employee Declaration Changes.
                UPUploadSig.Visible = CBool(Session("AllowEmployeeToAddEditSignatureESS"))
                btnRemoveSig.Visible = CBool(Session("AllowEmployeeToAddEditSignatureESS"))

                imgEmp.Visible = CBool(Session("AllowToAddEditImageForESS"))
                flUpload.Visible = CBool(Session("AllowToAddEditImageForESS"))
	            'Gajanan [13-June-2020] -- End

                'flUploadsig.Visible = False
                'btnRemoveSig.Visible = False
                'lblESign.Visible = imgSignature.Visible
                'S.SANDEEP [16-Jan-2018] -- END

            End If

            'S.SANDEEP [ 31 DEC 2013 ] -- START
            If CBool(Session("AllowToApproveEmployee")) = False Then
                Session("ShowPending") = Nothing
            End If

            If Session("ShowPending") Is Nothing Then
                Call FillCombo()
            Else
                chkShowPending.Checked = CBool(Session("ShowPending"))
                Call chkShowPending_CheckedChanged(New Object, New EventArgs)
            End If
            'S.SANDEEP [ 31 DEC 2013 ] -- END

            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes
            cboEmployee.Enabled = CBool(Session("ViewEmployee"))
            'Pinkal (22-Nov-2012) -- End


            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes

            flUpload.Visible = CBool(Session("IsImgInDataBase"))
            btnRemoveImage.Visible = CBool(Session("IsImgInDataBase"))
            imgEmp.Visible = CBool(Session("IsImgInDataBase"))
            lblEPhoto.Visible = imgEmp.Visible
            'Pinkal (01-Apr-2013) -- End

            Call Fill_Info(CInt(Session("Employeeunkid")))
            Call cboEmployee_SelectedIndexChanged(sender, e)


            'Pinkal (03-Sep-2013) -- Start
            'Enhancement : TRA Changes

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                lnkViewLeaveApprover.Visible = False
            End If

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                'Anjan [20 February 2016] -- End
                lnkViewAssessor.Visible = False
            End If
            'Pinkal (03-Sep-2013) -- End


            'Pinkal (05-May-2020) -- Start
            'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .
            If (CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False) OrElse CBool(Session("SetOTRequisitionMandatory")) = False Then
                lnkViewOTApprover.Visible = False
            End If
            'Pinkal (05-May-2020) -- End

                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                If Request.UrlReferrer IsNot Nothing Then
                    mstrURLReferer = Request.UrlReferrer.AbsoluteUri
                Else
                    mstrURLReferer = "~\UserHome.aspx"
                End If
            Else
                mstrURLReferer = ViewState("mstrURLReferer").ToString
                'Sohail (09 Nov 2020) -- End
        End If

        'S.SANDEEP [19-JAN-2017] -- START
        'ISSUE/ENHANCEMENT :
        If chkShowPending.Checked = True Then
            'S.SANDEEP [22-NOV-2018] -- START
            'btnsave.Text = "Approve"
            'btnClose.Text = "Don't"
            If CBool(Session("SkipEmployeeApprovalFlow")) = True Then
                btnsave.Text = "Approve"
                btnClose.Text = "Don't"
            End If
            'S.SANDEEP [22-NOV-2018] -- END
        Else
            btnsave.Text = "Update"
            btnClose.Text = "Close"
        End If
        'S.SANDEEP [19-JAN-2017] -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End


    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        'If IsPostBack = True And Me.ViewState("OldImage") <> "" Then
        '    Call DeleteImage(Server.MapPath("~\images\" & Me.ViewState("OldImage")))
        'End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
        'S.SANDEEP [ 31 DEC 2013 ] -- START
        'Me.IsLoginRequired = True
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
        'S.SANDEEP [ 31 DEC 2013 ] -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    'Sohail (09 Nov 2020) -- Start
    'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mstrURLReferer") = mstrURLReferer
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (09 Nov 2020) -- End

#End Region

#Region " Controls Events "

    Protected Sub cboEmployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.DataBound
        Try         'Hemant (13 Aug 2020)
        If cboEmployee.Items.Count > 0 Then
            For Each lstItem As ListItem In cboEmployee.Items
                lstItem.Attributes.Add("title", lstItem.Text)
            Next
            cboEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
        End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
            'Gajanan [13-June-2020] -- Start
            'Enhancement NMB Employee Declaration Changes.
                Clear_Controls()
            'Gajanan [13-June-2020] -- End

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    Call Fill_Info(CInt(cboEmployee.SelectedValue))
                    Session("Employeeunkid") = CInt(cboEmployee.SelectedValue)

                Else
                    Call Fill_Info(CInt(Session("Employeeunkid")))

                End If


                If CBool(Session("IsImgInDataBase")) = True Then
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        flUpload.Enabled = CBool(Session("AllowToAddEditImageForESS"))
                        btnRemoveImage.Enabled = False
                    ElseIf (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        flUpload.Enabled = CBool(Session("AllowToAddEditPhoto"))
                        btnRemoveImage.Enabled = CBool(Session("AllowToDeletePhoto"))
                    End If
                End If

            Else
                Clear_Controls()

                flUpload.Enabled = False
                btnRemoveImage.Enabled = False


            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 31 DEC 2013 ] -- START
    Protected Sub chkShowPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowPending.CheckedChanged
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Try
            Session("ShowPending") = chkShowPending.Checked
            If chkShowPending.Checked = True Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"), , , False)

                'S.SANDEEP [19-JAN-2017] -- START
                'ISSUE/ENHANCEMENT : GETTING UNAPPROVED EMPLOYEE
                'dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                '                                    CInt(Session("UserId")), _
                '                                    CInt(Session("Fin_year")), _
                '                                    CInt(Session("CompanyUnkId")), _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                    CStr(Session("UserAccessModeSetting")), False, _
                '                                    CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    CStr(Session("UserAccessModeSetting")), False, _
                                                  CBool(Session("IsIncludeInactiveEmp")), "Employee", True, , , , , , , , , , , , , , , , , , False)
                'S.SANDEEP [19-JAN-2017] -- END

                'Shani(24-Aug-2015) -- End

                Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dtTab
                    .DataBind()
                    Try
                        .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                    Catch ex As Exception
                        .SelectedValue = CStr(0)
                    End Try
                End With
                'btnsave.Text = "Approve"
                'btnClose.Text = "Don't"
            Else
                'btnsave.Text = "Update"
                'btnClose.Text = "Close"
                Session("ShowPending") = Nothing

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
                    dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                            CStr(Session("UserAccessModeSetting")), True, _
                                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                    'Shani(24-Aug-2015) -- End
                    With cboEmployee
                        .DataValueField = "employeeunkid"
                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        '.DataTextField = "employeename"
                        .DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        .DataSource = dsCombos.Tables("Employee")
                        .DataBind()
                        Try
                            .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                        Catch ex As Exception
                            .SelectedValue = CStr(0)
                        End Try
                    End With
                Else
                    Dim objglobalassess = New GlobalAccess
                    objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                    With cboEmployee
                        .DataSource = objglobalassess.ListOfEmployee
                        .DataTextField = "loginname"
                        .DataValueField = "employeeunkid"
                        .DataBind()
                        Try
                            .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                        Catch ex As Exception
                            .SelectedValue = CStr(0)
                        End Try
                    End With
                End If
            End If
            Call cboEmployee_SelectedIndexChanged(New Object, New EventArgs)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [ 31 DEC 2013 ] -- END

#End Region

#Region " Button's Event(s) "

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        Try

            'Sohail (06 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                msg.DisplayMessage("Please select Employee.", Me)
                Exit Sub
            End If
            'Sohail (06 Apr 2013) -- End


            'S.SANDEEP [19-JAN-2017] -- START
            'ISSUE/ENHANCEMENT :
            ''S.SANDEEP [ 11 MAR 2014 ] -- START
            'If Expression.IsMatch(txtcoEmail.Text.Trim) = False Then
            '    msg.DisplayMessage("Invalid Present Email.Please Enter Valid Email", Me)
            '    Exit Sub
            'End If
            ''S.SANDEEP [ 11 MAR 2014 ] -- END
            'S.SANDEEP [19-JAN-2017] -- END





            'S.SANDEEP [ 23 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If txtcoEmail.Text.Trim.Length > 0 Then
                'S.SANDEEP [19-JAN-2017] -- START
                If Expression.IsMatch(txtcoEmail.Text.Trim) = False Then
                    msg.DisplayMessage("Invalid Present Email.Please Enter Valid Email", Me)
                    Exit Sub
                End If
                'S.SANDEEP [19-JAN-2017] -- END

                Dim objMData As New clsMasterData
                'If objEmployee.IsEmailPresent(txtcoEmail.Text, Session("Employeeunkid")) = False Then
                '    msg.DisplayMessage("Sorry, this Company email address is already taken by some employee. Please provide new and unique email address.", Me)
                '    Exit Sub
                'End If
                If objMData.IsEmailPresent("hremployee_master", txtcoEmail.Text, CInt(Session("Employeeunkid")), "employeeunkid") = True Then
                    msg.DisplayMessage("Sorry, this Company email address is already taken by some employee. Please provide new and unique email address.", Me)
                    Exit Sub
                End If
                objMData = Nothing
            End If
            'S.SANDEEP [ 23 NOV 2012 ] -- END

            If txtcoEmail.Text.Trim.Length > 0 Then
                If txtcoEmail.Text.Trim.Contains("@") AndAlso Session("CompanyDomain").ToString().Trim <> "" Then
                    If txtcoEmail.Text.Trim.Substring(txtcoEmail.Text.Trim.IndexOf("@") + 1) <> Session("CompanyDomain").ToString() Then
                        msg.DisplayMessage("Invalid Company Email Domain.Please Enter valid Company Email Domain as per configuration settings.", Me)
                        txtcoEmail.Focus()
                        Exit Sub
                    End If
                End If
            End If


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = Session("Employeeunkid")
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            'Shani(20-Nov-2015) -- End

            objEmployee._Email = txtcoEmail.Text
            'S.SANDEEP [ 19 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
            'S.SANDEEP [ 19 MAR 2013 ] -- END


            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request

            'S.SANDEEP [ 31 DEC 2013 ] -- START
            'objEmployee._Titalunkid = CInt(cboTitle.SelectedValue)
            objEmployee._Titalunkid = CInt(IIf(cboTitle.SelectedValue = "", 0, cboTitle.SelectedValue))
            'S.SANDEEP [ 31 DEC 2013 ] -- END


            'Anjan (25 Oct 2012)-End 

            'S.SANDEEP [ 31 DEC 2013 ] -- START
            If Session("ShowPending") IsNot Nothing Then
                'S.SANDEEP [22-NOV-2018] -- START
                'objEmployee._Isapproved = CBool(Session("AllowToApproveEmployee"))
                If CBool(Session("SkipEmployeeApprovalFlow")) = True Then
                    objEmployee._Isapproved = CBool(Session("AllowToApproveEmployee"))
                Else
                    If objEmployee._Isapproved = True Then
                        objEmployee._Isapproved = objEmployee._Isapproved
                    Else
                        objEmployee._Isapproved = False
                    End If
                End If
                'S.SANDEEP [22-NOV-2018] -- END
            End If
            'S.SANDEEP [ 31 DEC 2013 ] -- END


            'Pinkal (27-Mar-2013) -- Start
            'Enhancement : TRA Changes

            If CBool(Session("IsImgInDataBase")) Then
                If imgEmp.ImageUrl.Trim <> "" AndAlso imgEmp.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False Then
                    objEmployee._Photo = ImageCompression(Server.MapPath(imgEmp.ImageUrl))
                ElseIf imgEmp.ImageUrl.Trim = "" Then
                    objEmployee._Photo = Nothing
                End If
            End If

            'Gajanan [13-June-2020] -- Start
            'Enhancement NMB Employee Declaration Changes.
            If imgSignature.ImageUrl.Trim <> "" AndAlso imgSignature.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False Then
                objEmployee._EmpSignature = ImageCompression(Server.MapPath(imgSignature.ImageUrl))
            ElseIf imgSignature.ImageUrl.Trim = "" Then
                objEmployee._EmpSignature = Nothing
            End If
            'Gajanan [13-June-2020] -- End


            'If objEmployee.Update(, , , Session("UserId"), Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), Session("Fin_year"), Session("AllowToApproveEarningDeduction")) = False Then

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


            'If objEmployee.Update(, , , Session("UserId"), Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), Session("Fin_year"), Session("AllowToApproveEarningDeduction"), CBool(Session("IsImgInDataBase"))) = False Then
            '    msg.DisplayMessage(objEmployee._Message, Me)
            'Else
            '    If CBool(Session("IsImgInDataBase")) Then
            '        DeleteImage()
            '    End If
            '    'S.SANDEEP [ 31 DEC 2013 ] -- START
            '    If Session("ShowPending") IsNot Nothing Then
            '        msg.DisplayMessage("Employee Approved successfully!", Me)
            '    Else
            '        msg.DisplayMessage("Information updated successfully!", Me)
            '    End If
            '    'S.SANDEEP [ 31 DEC 2013 ] -- END

            'End If


            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

            'Dim blnFlag As Boolean = objEmployee.Update(CStr(Session("Database_Name")), _
            '                                            CInt(Session("Fin_year")), _
            '                                            CInt(Session("CompanyUnkId")), _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                            CStr(Session("IsArutiDemo")), _
            '                                            CInt(Session("Total_Active_Employee_ForAllCompany")), _
            '                                            CInt(Session("NoOfEmployees")), _
            '                                            CInt(Session("UserId")), False, _
            '                                            CStr(Session("UserAccessModeSetting")), _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
            '                                            CBool(Session("IsIncludeInactiveEmp")), _
            '                                            CBool(Session("AllowToApproveEarningDeduction")), _
            '                            ConfigParameter._Object._CurrentDateAndTime.Date, , , , , _
            '                                            CStr(Session("EmployeeAsOnDate")), , _
            '                            CBool(Session("IsImgInDataBase")))

            Dim blnFlag As Boolean = objEmployee.Update(CStr(Session("Database_Name")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(Session("IsArutiDemo")), _
                                                        CInt(Session("Total_Active_Employee_ForAllCompany")), _
                                                        CInt(Session("NoOfEmployees")), _
                                                        CInt(Session("UserId")), False, _
                                                        CStr(Session("UserAccessModeSetting")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), _
                                                        CBool(Session("AllowToApproveEarningDeduction")), _
                                                    ConfigParameter._Object._CurrentDateAndTime.Date, CBool(Session("CreateADUserFromEmpMst")), _
                                                    CBool(Session("UserMustchangePwdOnNextLogon")), Nothing, Nothing, False, "", _
                                                        CStr(Session("EmployeeAsOnDate")), , _
                                        CBool(Session("IsImgInDataBase")))


            'Pinkal (18-Aug-2018) -- End

            If blnFlag = False Then
                msg.DisplayMessage(objEmployee._Message, Me)
            Else
                If CBool(Session("IsImgInDataBase")) Then
                    DeleteImage()
                End If
                'S.SANDEEP [ 31 DEC 2013 ] -- START
                If Session("ShowPending") IsNot Nothing Then
                    msg.DisplayMessage("Employee Approved successfully!", Me)
                Else
                    msg.DisplayMessage("Information updated successfully!", Me)
                End If
                'S.SANDEEP [ 31 DEC 2013 ] -- END

            End If

            'Shani(20-Nov-2015) -- End

            'Pinkal (27-Mar-2013) -- End

            'S.SANDEEP [ 31 DEC 2013 ] -- START
            If Session("ShowPending") IsNot Nothing Then
                Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                Session("Employeeunkid") = 0
            End If
            'S.SANDEEP [ 31 DEC 2013 ] -- END


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'S.SANDEEP [ 31 DEC 2013 ] -- START
            'Response.Redirect("~\UserHome.aspx", False)
            If Session("ShowPending") IsNot Nothing Then
                Session("ShowPending") = Nothing
                Session("mainfullurl") = Nothing
                Session("iQueryString") = Nothing
                Response.Redirect("~\Index.aspx", False)

            ElseIf IsNothing(Session("IsfromRecruitment")) = False AndAlso CBool(Session("IsfromRecruitment")) = True Then
                Response.Redirect(Session("rootpath").ToString & "Recruitment/wPgEmployeeUpdate.aspx", False)
                Session.Remove("IsfromRecruitment")
            Else
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                'Response.Redirect("~\UserHome.aspx", False)
                Response.Redirect(mstrURLReferer, False)
                'Sohail (09 Nov 2020) -- End
            End If
            'S.SANDEEP [ 31 DEC 2013 ] -- END
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    ' ''Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    ' ''    Try
    ' ''        'S.SANDEEP [ 31 DEC 2013 ] -- START
    ' ''        'Response.Redirect("~\UserHome.aspx", False)
    ' ''        If Session("ShowPending") IsNot Nothing Then
    ' ''            Session("ShowPending") = Nothing
    ' ''            Session("mainfullurl") = Nothing
    ' ''            Session("iQueryString") = Nothing
    ' ''            Response.Redirect("~\Index.aspx")
    ' ''        Else
    ' ''        Response.Redirect("~\UserHome.aspx", False)
    ' ''        End If
    ' ''        'S.SANDEEP [ 31 DEC 2013 ] -- END
    ' ''    Catch ex As Exception
    ' ''        msg.DisplayError(ex, Me)
    ' ''    Finally
    ' ''    End Try
    ' ''End Sub

    'Pinkal (27-Mar-2013) -- Start
    'Enhancement : TRA Changes

    'S.SANDEEP [16-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 118

    'Gajanan [13-June-2020] -- Start
    'Enhancement NMB Employee Declaration Changes.


    Protected Sub flUploadsig_btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles flUploadsig.btnUpload_Click
        Try
            If flUploadsig.HasFile Then
                Select Case flUploadsig.FileName.Substring(flUploadsig.FileName.LastIndexOf(".") + 1).ToUpper
                    Case "JPG"
                    Case "JPEG"
                    Case "BMP"
                    Case "GIF"
                    Case "PNG"
                    Case Else
                        msg.DisplayMessage("Please select proper Image", Me)
                        Exit Sub
                End Select
                Dim mintByes As Integer = 6291456
                If flUploadsig.FileBytes.LongLength <= mintByes Then   ' 6 MB = 6291456 Bytes
                    Dim mstrPath As String = "../images/" & flUploadsig.FileName
                    flUploadsig.SaveAs(Server.MapPath(mstrPath))
                    imgSignature.ImageUrl = mstrPath
                Else
                    msg.DisplayMessage("Sorry,You cannot upload file greater than " & (mintByes / 1024) / 1024 & " MB.", Me)
                End If
            Else
                msg.DisplayMessage("Please Select the Employee Image.", Me)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnRemoveSig_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveSig.Click
        Try
            If imgSignature.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False AndAlso System.IO.File.Exists(Server.MapPath(imgSignature.ImageUrl)) Then
                Dim mstrFile As String = Server.MapPath(imgSignature.ImageUrl)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Delete_File", "DeleteFile('" & mstrFile.Replace("\", "\\") & "');", True)
            End If
            imgSignature.ImageUrl = ""
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [13-June-2020] -- End
    'S.SANDEEP [16-Jan-2018] -- END

    Protected Sub BtnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles flUpload.btnUpload_Click ' btnUpload.Click
        Try
            If CBool(Session("IsImgInDataBase")) Then
                DeleteImage()
            End If

            If flUpload.HasFile Then

                'Sohail (01 Apr 2013) -- Start
                'TRA - ENHANCEMENT
                Select Case flUpload.FileName.Substring(flUpload.FileName.LastIndexOf(".") + 1).ToUpper
                    Case "JPG"
                    Case "JPEG"
                    Case "BMP"
                    Case "GIF"
                    Case "PNG"
                    Case Else
                        msg.DisplayMessage("Please select proper Image", Me)
                        Exit Sub
                End Select
                'Sohail (01 Apr 2013) -- End

                Dim mintByes As Integer = 6291456
                If flUpload.FileBytes.LongLength <= mintByes Then   ' 6 MB = 6291456 Bytes
                    Dim mstrPath As String = "../images/" & flUpload.FileName
                    flUpload.SaveAs(Server.MapPath(mstrPath))
                    imgEmp.ImageUrl = mstrPath
                Else
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage("Sorry,You cannot upload file greater than " & (mintByes / 1024) / 1024 & " MB.", Me)
                    'Sohail (23 Mar 2019) -- End
                End If
            Else
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(ex, Me)
                msg.DisplayMessage("Please Select the Employee Image.", Me)
                'Sohail (23 Mar 2019) -- End
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (27-Mar-2013) -- End


    'Pinkal (01-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Protected Sub btnRemoveImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveImage.Click
        Try
            'Shani(01-MAR-2017) -- Start
            If imgEmp.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False AndAlso System.IO.File.Exists(Server.MapPath(imgEmp.ImageUrl)) Then
                Dim mstrFile As String = Server.MapPath(imgEmp.ImageUrl)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Delete_File", "DeleteFile('" & mstrFile.Replace("\", "\\") & "');", True)
            End If
            imgEmp.ImageUrl = ""
            'Shani(01-MAR-2017) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (01-Apr-2013) -- End



#End Region

#Region " Links Events "

    Protected Sub lnkViewAllocations_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewAllocations.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If
            Response.Redirect("~\HR\wPgEmployeeAllocation.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkViewDates_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewDates.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If
            Response.Redirect("~\HR\wPgEmployeeDates.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkViewAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewAddress.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If
            Response.Redirect("~\HR\wPgEmployeeAddress.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkViewEmergency_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewEmergency.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If
            Response.Redirect("~\HR\wPgEmpEmerAddress.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkViewPersonal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewPersonal.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If
            Response.Redirect("~\HR\wPgEmployeePersonal.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkViewLeave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewLeave.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If
            Response.Redirect("~\HR\wPgLeaveDetails.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'Pinkal (03-Sep-2013) -- Start
    'Enhancement : TRA Changes

    Protected Sub lnkViewAssessor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewAssessor.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If

            LblReportingTo.Text = "View Assessor/Reviewer"

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Assessor_Reviewer, CInt(cboEmployee.SelectedValue))
            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Assessor_Reviewer, CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))
            'Shani(20-Nov-2015) -- End

            GvReportingTo.PageIndex = 0
            GvReportingTo.DataSource = dsList.Tables(0)
            GvReportingTo.DataBind()

            'If Me.ViewState("List") IsNot Nothing Then
            '    Me.ViewState("List") = Nothing
            'End If
            'Me.ViewState.Add("List", dsList.Tables(0))

            popupEmpReporting.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkViewLeaveApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewLeaveApprover.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If

            LblReportingTo.Text = "View Leave Approver"

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Leave_Approver, CInt(cboEmployee.SelectedValue))
            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Leave_Approver, CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))
            'Shani(20-Nov-2015) -- End

            GvReportingTo.PageIndex = 0
            GvReportingTo.DataSource = dsList.Tables(0)
            GvReportingTo.DataBind()

            'If Me.ViewState("List") IsNot Nothing Then
            '    Me.ViewState("List") = Nothing
            'End If
            'Me.ViewState.Add("List", dsList.Tables(0))

            popupEmpReporting.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkViewReportingTo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewReportingTo.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If

            LblReportingTo.Text = "View Reporting To"

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, CInt(cboEmployee.SelectedValue))
            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))
            'Shani(20-Nov-2015) -- End



            'S.SANDEEP [05-DEC-2017] -- START
            'ISSUE/ENHANCEMENT : Database Columns Visible
            dsList.Tables(0).Columns.Remove("Email")
            dsList.Tables(0).Columns.Remove("ishierarchy")
            dsList.Tables(0).Columns.Remove("reporttoemployeeunkid")
            dsList.Tables(0).Columns.Remove("dEmployee")
            'S.SANDEEP [05-DEC-2017] -- END



            GvReportingTo.PageIndex = 0
            GvReportingTo.DataSource = dsList.Tables(0)
            GvReportingTo.DataBind()

            'If Me.ViewState("List") IsNot Nothing Then
            '    Me.ViewState("List") = Nothing
            'End If
            'Me.ViewState.Add("List", dsList.Tables(0))

            popupEmpReporting.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (03-Sep-2013) -- End

    'Pinkal (05-May-2020) -- Start
    'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .
    Protected Sub lnkViewClaimApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewClaimApprover.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If

            LblReportingTo.Text = "View Claim Request Approver"

            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Claim_Approver, CInt(cboEmployee.SelectedValue) _
                                                                                                  , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                                                                  , CStr(Session("Database_Name")), CBool(Session("PaymentApprovalwithLeaveApproval")))

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("ExpCategroyId") = enExpenseType.EXP_LEAVE)
                If drRow IsNot Nothing AndAlso drRow.Count <= 1 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                    dsList.Tables(0).AcceptChanges()
                End If

                drRow = Nothing
                drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("ExpCategroyId") = enExpenseType.EXP_MEDICAL)
                If drRow IsNot Nothing AndAlso drRow.Count <= 1 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                    dsList.Tables(0).AcceptChanges()
                End If

                drRow = Nothing
                drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("ExpCategroyId") = enExpenseType.EXP_MISCELLANEOUS)
                If drRow IsNot Nothing AndAlso drRow.Count <= 1 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                    dsList.Tables(0).AcceptChanges()
                End If

                drRow = Nothing
                drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("ExpCategroyId") = enExpenseType.EXP_TRAINING)
                If drRow IsNot Nothing AndAlso drRow.Count <= 1 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                    dsList.Tables(0).AcceptChanges()
                End If

                drRow = Nothing
                drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("ExpCategroyId") = enExpenseType.EXP_IMPREST)
                If drRow IsNot Nothing AndAlso drRow.Count <= 1 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                    dsList.Tables(0).AcceptChanges()
                End If

                If dsList.Tables(0).Columns.Contains("ExpCategroyId") Then
                    dsList.Tables(0).Columns.Remove("ExpCategroyId")
                    dsList.Tables(0).AcceptChanges()
                End If
            End If

            GvReportingTo.PageIndex = 0
            GvReportingTo.DataSource = dsList.Tables(0)
            GvReportingTo.DataBind()

            'If Me.ViewState("List") IsNot Nothing Then
            '    Me.ViewState("List") = Nothing
            'End If
            'Me.ViewState.Add("List", dsList.Tables(0))

            popupEmpReporting.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkViewOTApprover_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewOTApprover.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If

            LblReportingTo.Text = "View OT Requisition Approver"

            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.OT_Approver, CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))

            GvReportingTo.PageIndex = 0
            GvReportingTo.DataSource = dsList.Tables(0)
            GvReportingTo.DataBind()

            'If Me.ViewState("List") IsNot Nothing Then
            '    Me.ViewState("List") = Nothing
            'End If
            'Me.ViewState.Add("List", dsList.Tables(0))

            popupEmpReporting.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (05-May-2020) -- End


#End Region

    'Pinkal (03-Sep-2013) -- Start
    'Enhancement : TRA Changes

#Region "Gridview Events"

    Protected Sub GvReportingTo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvReportingTo.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                If e.Row.Cells(0).Text = "&nbsp;" AndAlso e.Row.Cells(1).Text = "&nbsp;" AndAlso e.Row.Cells(2).Text = "&nbsp;" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                    e.Row.Visible = False
                Else
                    If e.Row.Cells(0).Text = "Assessor" OrElse e.Row.Cells(0).Text = "Reviewer" Then
                        e.Row.Cells(0).Font.Bold = True
                        e.Row.Cells(1).Visible = False
                        e.Row.Cells(2).Visible = False
                        e.Row.Cells(3).Visible = False
                        e.Row.Cells(0).ColumnSpan = e.Row.Cells.Count

                        'Pinkal (05-May-2020) -- Start
                        'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .
                    ElseIf e.Row.Cells(0).Text = Language.getMessage("clsExpCommonMethods", 2, "Leave") _
                             OrElse e.Row.Cells(0).Text = Language.getMessage("clsExpCommonMethods", 3, "Medical") OrElse e.Row.Cells(0).Text = Language.getMessage("clsExpCommonMethods", 8, "Miscellaneous") _
                             OrElse e.Row.Cells(0).Text = Language.getMessage("clsExpCommonMethods", 4, "Training") OrElse e.Row.Cells(0).Text = Language.getMessage("clsExpCommonMethods", 9, "Imprest") Then
                        e.Row.Cells(0).Font.Bold = True
                        e.Row.Cells(1).Visible = False
                        e.Row.Cells(2).Visible = False
                        e.Row.Cells(3).Visible = False
                        e.Row.Cells(4).Visible = False
                        e.Row.Cells(0).ColumnSpan = e.Row.Cells.Count
                        'Pinkal (05-May-2020) -- End
                    End If
                End If

            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub GvReportingTo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvReportingTo.PageIndexChanging
    '    Try
    '        GvReportingTo.PageIndex = e.NewPageIndex
    '        GvReportingTo.DataSource = Me.ViewState("List")
    '        GvReportingTo.DataBind()
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '        popupEmpReporting.Show()
    '    End Try
    'End Sub

#End Region

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLnaguae()
        Try         'Hemant (13 Aug 2020)
            Language.setLanguage(mstrModuleName)
            'Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)

            Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.ID, Me.lblGender.Text)
            Me.lblTitle.Text = Language._Object.getCaption(Me.lblTitle.ID, Me.lblTitle.Text)
            Me.lblEmployeeCode.Text = Language._Object.getCaption(Me.lblEmployeeCode.ID, Me.lblEmployeeCode.Text)
            Me.lblOthername.Text = Language._Object.getCaption(Me.lblOthername.ID, Me.lblOthername.Text)
            Me.lblFirstName.Text = Language._Object.getCaption(Me.lblFirstName.ID, Me.lblFirstName.Text)
            Me.lblSurname.Text = Language._Object.getCaption(Me.lblSurname.ID, Me.lblSurname.Text)
            Me.lblIdNo.Text = Language._Object.getCaption(Me.lblIdNo.ID, Me.lblIdNo.Text)
            Me.lblShift.Text = Language._Object.getCaption(Me.lblShift.ID, Me.lblShift.Text)
            Me.lblPayType.Text = Language._Object.getCaption(Me.lblPayType.ID, Me.lblPayType.Text)
            Me.lblPaypoint.Text = Language._Object.getCaption(Me.lblPaypoint.ID, Me.lblPaypoint.Text)
            Me.lblEmploymentType.Text = Language._Object.getCaption(Me.lblEmploymentType.ID, Me.lblEmploymentType.Text)
            Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.ID, Me.lblEmail.Text)

            Me.lnkViewAllocations.Text = Language._Object.getCaption("gbEmployeeAllocation", Me.lnkViewAllocations.Text)
            Me.lnkViewDates.Text = Language._Object.getCaption("gbDatesDetails", Me.lblEmail.Text)
            Me.lnkViewAddress.Text = Language._Object.getCaption("gbAddress", Me.lnkViewAddress.Text)
            Me.lnkViewEmergency.Text = Language._Object.getCaption("tabpEmergencyContact", Me.lnkViewEmergency.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Language.setLanguage(mstrModuleName1)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- End


    'Pinkal (03-Sep-2013) -- End



    '#Region " Private Function(s) & Method(s) "

    '    Private Sub FillCombo()
    '        Dim dsCombos As New DataSet
    '        Dim objCommon As New clsCommon_Master
    '        Dim objMaster As New clsMasterData
    '        Dim objCity As New clscity_master
    '        Dim objState As New clsstate_master
    '        Dim objZipCode As New clszipcode_master
    '        Try

    '            RemoveHandler cboPCountry.SelectedIndexChanged, AddressOf cboPCountry_SelectedIndexChanged
    '            dsCombos = objMaster.getCountryList("List", True)
    '            With cboPCountry
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboPCountry.SelectedIndexChanged, AddressOf cboPCountry_SelectedIndexChanged

    '            RemoveHandler cboDCountry.SelectedIndexChanged, AddressOf cboDCountry_SelectedIndexChanged
    '            With cboDCountry
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboDCountry.SelectedIndexChanged, AddressOf cboDCountry_SelectedIndexChanged

    '            RemoveHandler cboECountry.SelectedIndexChanged, AddressOf cboECountry_SelectedIndexChanged
    '            With cboECountry
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboECountry.SelectedIndexChanged, AddressOf cboECountry_SelectedIndexChanged

    '            RemoveHandler cboECountry2.SelectedIndexChanged, AddressOf cboECountry2_SelectedIndexChanged
    '            With cboECountry2
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboECountry2.SelectedIndexChanged, AddressOf cboECountry2_SelectedIndexChanged

    '            RemoveHandler cboECountry3.SelectedIndexChanged, AddressOf cboECountry3_SelectedIndexChanged
    '            With cboECountry3
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboECountry3.SelectedIndexChanged, AddressOf cboECountry3_SelectedIndexChanged


    '            RemoveHandler cboBirthCounty.SelectedIndexChanged, AddressOf cboBirthCounty_SelectedIndexChanged
    '            With cboBirthCounty
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboBirthCounty.SelectedIndexChanged, AddressOf cboBirthCounty_SelectedIndexChanged

    '            With cboIssueCountry
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            With cboNationality
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            RemoveHandler cboPState.SelectedIndexChanged, AddressOf cboPState_SelectedIndexChanged
    '            dsCombos = objState.GetList("List", True, True)
    '            With cboPState
    '                .DataValueField = "stateunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboPState.SelectedIndexChanged, AddressOf cboPState_SelectedIndexChanged

    '            RemoveHandler cboDState.SelectedIndexChanged, AddressOf cboDState_SelectedIndexChanged
    '            With cboDState
    '                .DataValueField = "stateunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboDState.SelectedIndexChanged, AddressOf cboDState_SelectedIndexChanged

    '            RemoveHandler cboEState.SelectedIndexChanged, AddressOf cboEState_SelectedIndexChanged
    '            With cboEState
    '                .DataValueField = "stateunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboEState.SelectedIndexChanged, AddressOf cboEState_SelectedIndexChanged

    '            RemoveHandler cboEState2.SelectedIndexChanged, AddressOf cboEState2_SelectedIndexChanged
    '            With cboEState2
    '                .DataValueField = "stateunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboEState2.SelectedIndexChanged, AddressOf cboEState2_SelectedIndexChanged

    '            RemoveHandler cboEState3.SelectedIndexChanged, AddressOf cboEState3_SelectedIndexChanged
    '            With cboEState3
    '                .DataValueField = "stateunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboEState3.SelectedIndexChanged, AddressOf cboEState3_SelectedIndexChanged


    '            RemoveHandler cboBirthState.SelectedIndexChanged, AddressOf cboBirthState_SelectedIndexChanged
    '            With cboBirthState
    '                .DataValueField = "stateunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboBirthState.SelectedIndexChanged, AddressOf cboBirthState_SelectedIndexChanged


    '            RemoveHandler cboPCity.SelectedIndexChanged, AddressOf cboPCity_SelectedIndexChanged
    '            dsCombos = objCity.GetList("List", True, True)
    '            With cboPCity
    '                .DataValueField = "cityunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboPCity.SelectedIndexChanged, AddressOf cboPCity_SelectedIndexChanged

    '            RemoveHandler cboDCity.SelectedIndexChanged, AddressOf cboDCity_SelectedIndexChanged
    '            With cboDCity
    '                .DataValueField = "cityunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboDCity.SelectedIndexChanged, AddressOf cboDCity_SelectedIndexChanged

    '            RemoveHandler cboECity.SelectedIndexChanged, AddressOf cboECity_SelectedIndexChanged
    '            With cboECity
    '                .DataValueField = "cityunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboECity.SelectedIndexChanged, AddressOf cboECity_SelectedIndexChanged

    '            RemoveHandler cboECity2.SelectedIndexChanged, AddressOf cboECity2_SelectedIndexChanged
    '            With cboECity2
    '                .DataValueField = "cityunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboECity2.SelectedIndexChanged, AddressOf cboECity2_SelectedIndexChanged

    '            RemoveHandler cboECity3.SelectedIndexChanged, AddressOf cboECity3_SelectedIndexChanged
    '            With cboECity3
    '                .DataValueField = "cityunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboECity3.SelectedIndexChanged, AddressOf cboECity3_SelectedIndexChanged

    '            With cboBirthCity
    '                .DataValueField = "cityunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            dsCombos = objZipCode.GetList("List", True, True)
    '            With cboPZipcode
    '                .DataValueField = "zipcodeunkid"
    '                .DataTextField = "zipcode_no"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            With cboDZipcode
    '                .DataValueField = "zipcodeunkid"
    '                .DataTextField = "zipcode_no"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            With cboEZipcode
    '                .DataValueField = "zipcodeunkid"
    '                .DataTextField = "zipcode_no"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            With cboEZipcode2
    '                .DataValueField = "zipcodeunkid"
    '                .DataTextField = "zipcode_no"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            With cboEZipcode3
    '                .DataValueField = "zipcodeunkid"
    '                .DataTextField = "zipcode_no"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.COMPLEXION, True, "List")
    '            With cboComplexion
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List")
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.BLOOD_GROUP, True, "BldGrp")
    '            With cboBloodGrp
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("BldGrp")
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.EYES_COLOR, True, "EyeColor")
    '            With cboEyeColor
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("EyeColor")
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.ETHNICITY, True, "Ethincity")
    '            With cboEthnicity
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Ethincity")
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.RELIGION, True, "Religion")
    '            With cboReligion
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Religion")
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.HAIR_COLOR, True, "Hair")
    '            With cboHair
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Hair")
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.LANGUAGES, True, "Lang1")
    '            With cboLang1
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Lang1").Copy
    '                .DataBind()
    '            End With

    '            With cboLang2
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Lang1").Copy
    '                .DataBind()
    '            End With

    '            With cboLang3
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Lang1").Copy
    '                .DataBind()
    '            End With

    '            With cboLang4
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Lang1").Copy
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, True, "MaritalStatus")
    '            With cboMaritalStatus
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("MaritalStatus")
    '                .DataBind()
    '            End With

    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '            dsCombos.Dispose() : objCommon = Nothing : objMaster = Nothing : objCity = Nothing : objState = Nothing : objZipCode = Nothing
    '        End Try
    '    End Sub

    '    Private Sub Fill_List()
    '        Dim dsList As New DataSet
    '        Dim objCommon As New clsCommon_Master
    '        Try
    '            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ALLERGIES, False, "List")
    '            With chkLstAllergies
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsList.Tables("List")
    '                .DataBind()
    '            End With

    '            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.DISABILITIES, False, "List")
    '            With chkLstDisabilities
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsList.Tables("List")
    '                .DataBind()
    '            End With


    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '            dsList.Dispose() : objCommon = Nothing
    '        End Try
    '    End Sub

    '    Private Sub Fill_Info()
    '        Dim objCommon As New clsCommon_Master
    '        Dim objShift As New clsshift_master
    '        Dim objPayPoint As New clspaypoint_master

    '        Try
    '            objEmployee._Employeeunkid = Session("Employeeunkid")

    '            txtCodeValue.Text = objEmployee._Employeecode
    '            txtFirstValue.Text = objEmployee._Firstname
    '            txtSurnameValue.Text = objEmployee._Surname
    '            txtOtherNameValue.Text = objEmployee._Othername

    '            objCommon._Masterunkid = objEmployee._Employmenttypeunkid
    '            txtEmploymentTypeValue.Text = objCommon._Name

    '            objCommon._Masterunkid = objEmployee._Paytypeunkid
    '            txtPayValue.Text = objCommon._Name

    '            Select Case objEmployee._Gender
    '                Case 1  'MALE
    '                    txtGenderValue.Text = "Male"
    '                Case 2  'FEMALE
    '                    txtGenderValue.Text = "Female"
    '                Case Else
    '                    txtGenderValue.Text = ""
    '            End Select

    '            objPayPoint._Paypointunkid = objEmployee._Paypointunkid
    '            txtPayPointValue.Text = objPayPoint._Paypointname

    '            objShift._Shiftunkid = objEmployee._Shiftunkid
    '            txtShiftValue.Text = objShift._Shiftname

    '            objCommon._Masterunkid = objEmployee._Titalunkid
    '            txtTitelValue.Text = objCommon._Name

    '            If objEmployee._ImagePath <> "" Then
    '                Dim Path As String = ""

    '                If Aruti.Data.ConfigParameter._Object._PhotoPath.LastIndexOf("\") = Aruti.Data.ConfigParameter._Object._PhotoPath.ToString.Trim.Length - 1 Then
    '                    Path = System.IO.Path.GetFullPath(Aruti.Data.ConfigParameter._Object._PhotoPath & objEmployee._ImagePath)
    '                    Call ImageOperation(Path)
    '                Else
    '                    Path = System.IO.Path.GetFullPath(Aruti.Data.ConfigParameter._Object._PhotoPath & "\" & objEmployee._ImagePath)
    '                    Call ImageOperation(Path)
    '                End If
    '            End If


    '            Dim objEIdTran As New clsIdentity_tran
    '            objEIdTran._EmployeeUnkid = objEmployee._Employeeunkid
    '            Dim dtTable = objEIdTran._DataList
    '            If dtTable.Rows.Count > 0 Then
    '                Dim dtTemp() As DataRow = dtTable.Select("isdefault = true")
    '                If dtTemp.Length > 0 Then
    '                    txtIdValue.Text = dtTemp(0)("identity_no")
    '                End If
    '            Else
    '                txtIdValue.Text = ""
    '            End If

    '            '************************* PRESENT INFO *****************************'
    '            txtPAddress1.Text = objEmployee._Present_Address1
    '            txtPAddress2.Text = objEmployee._Present_Address2
    '            cboPCountry.SelectedValue = objEmployee._Present_Countryunkid
    '            cboPState.SelectedValue = objEmployee._Present_Stateunkid
    '            cboPCity.SelectedValue = objEmployee._Present_Post_Townunkid
    '            cboPZipcode.SelectedValue = objEmployee._Present_Postcodeunkid
    '            txtPRegion.Text = objEmployee._Present_Provicnce
    '            txtPStreet.Text = objEmployee._Present_Road
    '            txtPEState.Text = objEmployee._Present_Estate
    '            txtPPlotNo.Text = objEmployee._Present_Plotno
    '            txtPMobile.Text = objEmployee._Present_Mobile
    '            txtPAltNo.Text = objEmployee._Present_Alternateno
    '            txtPTelNo.Text = objEmployee._Present_Tel_No
    '            txtPFax.Text = objEmployee._Present_Fax
    '            txtPEmail.Text = objEmployee._Present_Email
    '            '************************* PRESENT INFO *****************************'

    '            '************************* DOMICILE INFO *****************************'
    '            txtDAddress1.Text = objEmployee._Domicile_Address1
    '            txtDAddress2.Text = objEmployee._Domicile_Address2
    '            cboDCountry.SelectedValue = objEmployee._Domicile_Countryunkid
    '            cboDState.SelectedValue = objEmployee._Domicile_Stateunkid
    '            cboDCity.SelectedValue = objEmployee._Domicile_Post_Townunkid
    '            cboDZipcode.SelectedValue = objEmployee._Domicile_Postcodeunkid
    '            txtDRegion.Text = objEmployee._Domicile_Provicnce
    '            txtDStreet.Text = objEmployee._Domicile_Road
    '            txtDEState.Text = objEmployee._Domicile_Estate
    '            txtDPlotNo.Text = objEmployee._Domicile_Plotno
    '            txtDMobile.Text = objEmployee._Domicile_Mobile
    '            txtDAltNo.Text = objEmployee._Domicile_Alternateno
    '            txtDTelNo.Text = objEmployee._Domicile_Tel_No
    '            txtDFax.Text = objEmployee._Domicile_Fax
    '            txtDEmail.Text = objEmployee._Domicile_Email
    '            '************************* DOMICILE INFO *****************************'

    '            '************************* EMERGENCY INFO *****************************'
    '            txtEAddress.Text = objEmployee._Emer_Con_Address
    '            txtEAltNo.Text = objEmployee._Emer_Con_Alternateno
    '            txtEEMail.Text = objEmployee._Emer_Con_Email
    '            txtEEState.Text = objEmployee._Emer_Con_Estate
    '            txtEFax.Text = objEmployee._Emer_Con_Fax
    '            txtEFirstname.Text = objEmployee._Emer_Con_Firstname
    '            txtELastname.Text = objEmployee._Emer_Con_Lastname
    '            txtEMobile.Text = objEmployee._Emer_Con_Mobile
    '            txtEPlotNo.Text = objEmployee._Emer_Con_Plotno
    '            cboECity.SelectedValue = objEmployee._Emer_Con_Post_Townunkid
    '            cboEZipcode.SelectedValue = objEmployee._Emer_Con_Postcodeunkid
    '            txtERegion.Text = objEmployee._Emer_Con_Provicnce
    '            txtEStreet.Text = objEmployee._Emer_Con_Road
    '            cboEState.SelectedValue = objEmployee._Emer_Con_State
    '            txtETelNo.Text = objEmployee._Emer_Con_Tel_No
    '            cboECountry.SelectedValue = objEmployee._Emer_Con_Countryunkid

    '            txtEAddress2.Text = objEmployee._Emer_Con_Address2
    '            txtEAltNo2.Text = objEmployee._Emer_Con_Alternateno2
    '            txtEEMail2.Text = objEmployee._Emer_Con_Email2
    '            txtEEState2.Text = objEmployee._Emer_Con_Estate2
    '            txtEFax2.Text = objEmployee._Emer_Con_Fax2
    '            txtEFirstname2.Text = objEmployee._Emer_Con_Firstname2
    '            txtELastname2.Text = objEmployee._Emer_Con_Lastname2
    '            txtEMobile2.Text = objEmployee._Emer_Con_Mobile2
    '            txtEPlotNo2.Text = objEmployee._Emer_Con_Plotno2
    '            cboECity2.SelectedValue = objEmployee._Emer_Con_Post_Townunkid2
    '            cboEZipcode2.SelectedValue = objEmployee._Emer_Con_Postcodeunkid2
    '            txtERegion2.Text = objEmployee._Emer_Con_Provicnce2
    '            txtEStreet2.Text = objEmployee._Emer_Con_Road2
    '            cboEState2.SelectedValue = objEmployee._Emer_Con_State2
    '            txtETelNo2.Text = objEmployee._Emer_Con_Tel_No2
    '            cboECountry2.SelectedValue = objEmployee._Emer_Con_Countryunkid2

    '            txtEAddress3.Text = objEmployee._Emer_Con_Address3
    '            txtEAltNo3.Text = objEmployee._Emer_Con_Alternateno3
    '            txtEEMail3.Text = objEmployee._Emer_Con_Email3
    '            txtEEState3.Text = objEmployee._Emer_Con_Estate3
    '            txtEFax3.Text = objEmployee._Emer_Con_Fax3
    '            txtEFirstname3.Text = objEmployee._Emer_Con_Firstname3
    '            txtELastname3.Text = objEmployee._Emer_Con_Lastname3
    '            txtEMobile3.Text = objEmployee._Emer_Con_Mobile3
    '            txtEPlotNo3.Text = objEmployee._Emer_Con_Plotno3
    '            cboECity3.SelectedValue = objEmployee._Emer_Con_Post_Townunkid3
    '            cboEZipcode3.SelectedValue = objEmployee._Emer_Con_Postcodeunkid3
    '            txtERegion3.Text = objEmployee._Emer_Con_Provicnce3
    '            txtEStreet3.Text = objEmployee._Emer_Con_Road3
    '            cboEState3.SelectedValue = objEmployee._Emer_Con_State3
    '            txtETelNo3.Text = objEmployee._Emer_Con_Tel_No3
    '            cboECountry3.SelectedValue = objEmployee._Emer_Con_Countryunkid3
    '            '************************* EMERGENCY INFO *****************************'

    '            '************************* BIRTH INFO *****************************'
    '            cboBirthCounty.SelectedValue = objEmployee._Birthcountryunkid
    '            cboBirthState.SelectedValue = objEmployee._Birthstateunkid
    '            cboBirthCity.SelectedValue = objEmployee._Birthcityunkid
    '            txtBirthVillage.Text = objEmployee._Birth_Village
    '            txtBirthWard.Text = objEmployee._Birth_Ward
    '            txtBirthCertNo.Text = objEmployee._Birthcertificateno
    '            '************************* BIRTH INFO *****************************'

    '            '************************* WORKING INFO *****************************'
    '            txtWorkPermitNo.Text = objEmployee._Work_Permit_No
    '            cboIssueCountry.SelectedValue = objEmployee._Workcountryunkid
    '            txtIssuePlace.Text = objEmployee._Work_Permit_Issue_Place

    '            If objEmployee._Work_Permit_Issue_Date <> Nothing Then
    '                dtIssueDate.SetDate = objEmployee._Work_Permit_Issue_Date
    '            Else
    '                dtIssueDate.SetDate = Nothing
    '            End If

    '            If objEmployee._Work_Permit_Expiry_Date <> Nothing Then
    '                dtExpiryDate.SetDate = objEmployee._Work_Permit_Expiry_Date
    '            Else
    '                dtExpiryDate.SetDate = Nothing
    '            End If
    '            '************************* WORKING INFO *****************************'

    '            '************************* OTHER INFO *****************************'
    '            cboComplexion.SelectedValue = objEmployee._Complexionunkid
    '            txtExTel.Text = objEmployee._Extra_Tel_No
    '            cboBloodGrp.SelectedValue = objEmployee._Bloodgroupunkid
    '            cboEyeColor.SelectedValue = objEmployee._Eyecolorunkid
    '            cboLang1.SelectedValue = objEmployee._Language1unkid
    '            cboLang2.SelectedValue = objEmployee._Language2unkid
    '            cboLang3.SelectedValue = objEmployee._Language3unkid
    '            cboLang4.SelectedValue = objEmployee._Language4unkid
    '            cboEthnicity.SelectedValue = objEmployee._Ethincityunkid
    '            cboNationality.SelectedValue = objEmployee._Nationalityunkid
    '            cboReligion.SelectedValue = objEmployee._Religionunkid
    '            cboHair.SelectedValue = objEmployee._Hairunkid
    '            txtHeight.Text = objEmployee._Height
    '            txtWeight.Text = objEmployee._Weight
    '            cboMaritalStatus.SelectedValue = objEmployee._Maritalstatusunkid
    '            If objEmployee._Anniversary_Date <> Nothing Then
    '                dtMarriedDate.SetDate = objEmployee._Anniversary_Date
    '            Else
    '                dtMarriedDate.SetDate = Nothing
    '            End If
    '            txtSpHob.Text = objEmployee._Sports_Hobbies

    '            If objEmployee._Allergies.Length > 0 Then
    '                For Each StrId As String In objEmployee._Allergies.Split(",")
    '                    For Each mItem As ListItem In chkLstAllergies.Items
    '                        If CInt(StrId) = CInt(mItem.Value) Then
    '                            mItem.Selected = True
    '                        End If
    '                    Next
    '                Next
    '            End If

    '            If objEmployee._Disabilities.Length > 0 Then
    '                For Each StrId As String In objEmployee._Disabilities.Split(",")
    '                    For Each mItem As ListItem In chkLstDisabilities.Items
    '                        If CInt(StrId) = CInt(mItem.Value) Then
    '                            mItem.Selected = True
    '                        End If
    '                    Next
    '                Next
    '            End If
    '            '************************* OTHER INFO *****************************'

    '            GridView1.DataSource = clsuser.LeaveBalances
    '            GridView1.DataBind()

    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub ImageOperation(ByVal imgFullPath As String)
    '        Dim f As New System.IO.FileInfo(imgFullPath)
    '        If System.IO.File.Exists(Server.MapPath("~\images\" & f.Name)) = False Then
    '            Dim rbyte() As Byte = IO.File.ReadAllBytes(imgFullPath)
    '            Dim mem As New System.IO.MemoryStream(rbyte)
    '            Drawing.Image.FromStream(mem).Save(Server.MapPath("~\images\" & f.Name))
    '            If f.Name <> objEmployee._ImagePath Then
    '                Drawing.Image.FromStream(mem).Save(ConfigParameter._Object._PhotoPath & "\" & f.Name)
    '                Me.ViewState("OldImage") = Me.ViewState("ImageName")
    '            End If
    '        End If
    '        imgEmp.ImageUrl = "~\images\" & f.Name
    '        Me.ViewState("ImageName") = f.Name
    '    End Sub

    '    Private Sub ClearObjects()
    '        Try
    '            txtPAddress1.Text = ""
    '            txtPAddress2.Text = ""
    '            cboPCountry.SelectedValue = 0
    '            cboPState.SelectedValue = 0
    '            cboPCity.SelectedValue = 0
    '            cboPZipcode.SelectedValue = 0
    '            txtPRegion.Text = ""
    '            txtPStreet.Text = ""
    '            txtPEState.Text = ""
    '            txtPPlotNo.Text = ""
    '            txtPMobile.Text = ""
    '            txtPAltNo.Text = ""
    '            txtPTelNo.Text = ""
    '            txtPFax.Text = ""
    '            txtPEmail.Text = ""
    '            txtDAddress1.Text = ""
    '            txtDAddress2.Text = ""
    '            cboDCountry.SelectedValue = 0
    '            cboDState.SelectedValue = 0
    '            cboDCity.SelectedValue = 0
    '            cboDZipcode.SelectedValue = 0
    '            txtDRegion.Text = ""
    '            txtDStreet.Text = ""
    '            txtDEState.Text = ""
    '            txtDPlotNo.Text = ""
    '            txtDMobile.Text = ""
    '            txtDAltNo.Text = ""
    '            txtDTelNo.Text = ""
    '            txtDFax.Text = ""
    '            txtDEmail.Text = ""
    '            txtEAddress.Text = ""
    '            txtEAltNo.Text = ""
    '            txtEEMail.Text = ""
    '            txtEEState.Text = ""
    '            txtEFax.Text = ""
    '            txtEFirstname.Text = ""
    '            txtELastname.Text = ""
    '            txtEMobile.Text = ""
    '            txtEPlotNo.Text = ""
    '            cboECity.SelectedValue = 0
    '            cboEZipcode.SelectedValue = 0
    '            txtERegion.Text = ""
    '            txtEStreet.Text = ""
    '            cboEState.SelectedValue = 0
    '            txtETelNo.Text = ""
    '            cboECountry.SelectedValue = 0

    '            cboBirthCounty.SelectedValue = 0
    '            cboBirthState.SelectedValue = 0
    '            cboBirthCity.SelectedValue = 0
    '            txtBirthWard.Text = ""
    '            txtBirthCertNo.Text = ""
    '            txtBirthVillage.Text = ""
    '            txtWorkPermitNo.Text = ""
    '            cboIssueCountry.SelectedValue = 0
    '            dtExpiryDate.SetDate = Nothing
    '            dtIssueDate.SetDate = Nothing
    '            txtIssuePlace.Text = ""
    '            cboComplexion.SelectedValue = 0
    '            txtExTel.Text = ""
    '            cboBloodGrp.Text = ""
    '            cboEyeColor.SelectedValue = 0
    '            cboLang1.SelectedValue = 0
    '            cboLang2.SelectedValue = 0
    '            cboLang3.SelectedValue = 0
    '            cboLang4.SelectedValue = 0
    '            cboEthnicity.SelectedValue = 0
    '            cboNationality.SelectedValue = 0
    '            cboReligion.SelectedValue = 0
    '            cboHair.SelectedValue = 0
    '            txtHeight.Text = 0
    '            txtWeight.Text = 0
    '            cboMaritalStatus.SelectedValue = 0
    '            dtMarriedDate.SetDate = Nothing
    '            txtSpHob.Text = ""

    '            txtEAddress2.Text = ""
    '            txtEAltNo2.Text = ""
    '            txtEEMail2.Text = ""
    '            txtEEState2.Text = ""
    '            txtEFax2.Text = ""
    '            txtEFirstname2.Text = ""
    '            txtELastname2.Text = ""
    '            txtEMobile2.Text = ""
    '            txtEPlotNo2.Text = ""
    '            cboECity2.SelectedValue = 0
    '            cboEZipcode2.SelectedValue = 0
    '            txtERegion2.Text = ""
    '            txtEStreet2.Text = ""
    '            cboEState2.SelectedValue = 0
    '            txtETelNo2.Text = ""
    '            cboECountry2.SelectedValue = 0

    '            txtEAddress3.Text = ""
    '            txtEAltNo3.Text = ""
    '            txtEEMail3.Text = ""
    '            txtEEState3.Text = ""
    '            txtEFax3.Text = ""
    '            txtEFirstname3.Text = ""
    '            txtELastname3.Text = ""
    '            txtEMobile3.Text = ""
    '            txtEPlotNo3.Text = ""
    '            cboECity3.SelectedValue = 0
    '            cboEZipcode3.SelectedValue = 0
    '            txtERegion3.Text = ""
    '            txtEStreet3.Text = ""
    '            cboEState3.SelectedValue = 0
    '            txtETelNo3.Text = ""
    '            cboECountry3.SelectedValue = 0

    '            For Each mItem As ListItem In chkLstAllergies.Items
    '                mItem.Selected = False
    '            Next

    '            For Each mItem As ListItem In chkLstDisabilities.Items
    '                mItem.Selected = False
    '            Next

    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub SetValue()
    '        Try
    '            '************************* PRESENT INFO *****************************'
    '            objEmployee._Employeeunkid = Session("Employeeunkid")

    '            objEmployee._Present_Address1 = txtPAddress1.Text
    '            objEmployee._Present_Address2 = txtPAddress2.Text
    '            objEmployee._Present_Countryunkid = cboPCountry.SelectedValue
    '            objEmployee._Present_Stateunkid = cboPState.SelectedValue
    '            objEmployee._Present_Post_Townunkid = cboPCity.SelectedValue
    '            objEmployee._Present_Postcodeunkid = cboPZipcode.SelectedValue
    '            objEmployee._Present_Provicnce = txtPRegion.Text
    '            objEmployee._Present_Road = txtPStreet.Text
    '            objEmployee._Present_Estate = txtPEState.Text
    '            objEmployee._Present_Plotno = txtPPlotNo.Text
    '            objEmployee._Present_Mobile = txtPMobile.Text
    '            objEmployee._Present_Alternateno = txtPAltNo.Text
    '            objEmployee._Present_Tel_No = txtPTelNo.Text
    '            objEmployee._Present_Fax = txtPFax.Text
    '            objEmployee._Present_Email = txtPEmail.Text
    '            '************************* PRESENT INFO *****************************'

    '            '************************* DOMICILE INFO *****************************'
    '            objEmployee._Domicile_Address1 = txtDAddress1.Text
    '            objEmployee._Domicile_Address2 = txtDAddress2.Text
    '            objEmployee._Domicile_Countryunkid = cboDCountry.SelectedValue
    '            objEmployee._Domicile_Stateunkid = cboDState.SelectedValue
    '            objEmployee._Domicile_Post_Townunkid = cboDCity.SelectedValue
    '            objEmployee._Domicile_Postcodeunkid = cboDZipcode.SelectedValue
    '            objEmployee._Domicile_Provicnce = txtDRegion.Text
    '            objEmployee._Domicile_Road = txtDStreet.Text
    '            objEmployee._Domicile_Estate = txtDEState.Text
    '            objEmployee._Domicile_Plotno = txtDPlotNo.Text
    '            objEmployee._Domicile_Mobile = txtDMobile.Text
    '            objEmployee._Domicile_Alternateno = txtDAltNo.Text
    '            objEmployee._Domicile_Tel_No = txtDTelNo.Text
    '            objEmployee._Domicile_Fax = txtDFax.Text
    '            objEmployee._Domicile_Email = txtDEmail.Text
    '            '************************* DOMICILE INFO *****************************'

    '            '************************* EMERGENCY INFO *****************************'
    '            objEmployee._Emer_Con_Address = txtEAddress.Text
    '            objEmployee._Emer_Con_Alternateno = txtEAltNo.Text
    '            objEmployee._Emer_Con_Email = txtEEMail.Text
    '            objEmployee._Emer_Con_Estate = txtEEState.Text
    '            objEmployee._Emer_Con_Fax = txtEFax.Text
    '            objEmployee._Emer_Con_Firstname = txtEFirstname.Text
    '            objEmployee._Emer_Con_Lastname = txtELastname.Text
    '            objEmployee._Emer_Con_Mobile = txtEMobile.Text
    '            objEmployee._Emer_Con_Plotno = txtEPlotNo.Text
    '            objEmployee._Emer_Con_Post_Townunkid = cboECity.SelectedValue
    '            objEmployee._Emer_Con_Postcodeunkid = cboEZipcode.SelectedValue
    '            objEmployee._Emer_Con_Provicnce = txtERegion.Text
    '            objEmployee._Emer_Con_Road = txtEStreet.Text
    '            objEmployee._Emer_Con_State = cboEState.SelectedValue
    '            objEmployee._Emer_Con_Tel_No = txtETelNo.Text
    '            objEmployee._Emer_Con_Countryunkid = cboECountry.SelectedValue

    '            objEmployee._Emer_Con_Address2 = txtEAddress2.Text
    '            objEmployee._Emer_Con_Alternateno2 = txtEAltNo2.Text
    '            objEmployee._Emer_Con_Email2 = txtEEMail2.Text
    '            objEmployee._Emer_Con_Estate2 = txtEEState2.Text
    '            objEmployee._Emer_Con_Fax2 = txtEFax2.Text
    '            objEmployee._Emer_Con_Firstname2 = txtEFirstname2.Text
    '            objEmployee._Emer_Con_Lastname2 = txtELastname2.Text
    '            objEmployee._Emer_Con_Mobile2 = txtEMobile2.Text
    '            objEmployee._Emer_Con_Plotno2 = txtEPlotNo2.Text
    '            objEmployee._Emer_Con_Post_Townunkid2 = cboECity2.SelectedValue
    '            objEmployee._Emer_Con_Postcodeunkid2 = cboEZipcode2.SelectedValue
    '            objEmployee._Emer_Con_Provicnce2 = txtERegion2.Text
    '            objEmployee._Emer_Con_Road2 = txtEStreet2.Text
    '            objEmployee._Emer_Con_State2 = cboEState2.SelectedValue
    '            objEmployee._Emer_Con_Tel_No2 = txtETelNo2.Text
    '            objEmployee._Emer_Con_Countryunkid2 = cboECountry2.SelectedValue

    '            objEmployee._Emer_Con_Address3 = txtEAddress3.Text
    '            objEmployee._Emer_Con_Alternateno3 = txtEAltNo3.Text
    '            objEmployee._Emer_Con_Email3 = txtEEMail3.Text
    '            objEmployee._Emer_Con_Estate3 = txtEEState3.Text
    '            objEmployee._Emer_Con_Fax3 = txtEFax3.Text
    '            objEmployee._Emer_Con_Firstname3 = txtEFirstname3.Text
    '            objEmployee._Emer_Con_Lastname3 = txtELastname3.Text
    '            objEmployee._Emer_Con_Mobile3 = txtEMobile3.Text
    '            objEmployee._Emer_Con_Plotno3 = txtEPlotNo3.Text
    '            objEmployee._Emer_Con_Post_Townunkid3 = cboECity3.SelectedValue
    '            objEmployee._Emer_Con_Postcodeunkid3 = cboEZipcode3.SelectedValue
    '            objEmployee._Emer_Con_Provicnce3 = txtERegion3.Text
    '            objEmployee._Emer_Con_Road3 = txtEStreet3.Text
    '            objEmployee._Emer_Con_State3 = cboEState3.SelectedValue
    '            objEmployee._Emer_Con_Tel_No3 = txtETelNo3.Text
    '            objEmployee._Emer_Con_Countryunkid3 = cboECountry3.SelectedValue
    '            '************************* EMERGENCY INFO *****************************'

    '            '************************* BIRTH INFO *****************************'
    '            objEmployee._Birthcountryunkid = cboBirthCounty.SelectedValue
    '            objEmployee._Birthstateunkid = cboBirthState.SelectedValue
    '            objEmployee._Birthcityunkid = cboBirthCity.SelectedValue
    '            objEmployee._Birth_Village = txtBirthVillage.Text
    '            objEmployee._Birth_Ward = txtBirthWard.Text
    '            objEmployee._Birthcertificateno = txtBirthCertNo.Text
    '            '************************* BIRTH INFO *****************************'

    '            '************************* WORKING INFO *****************************'
    '            objEmployee._Work_Permit_No = txtWorkPermitNo.Text
    '            objEmployee._Workcountryunkid = cboIssueCountry.SelectedValue
    '            objEmployee._Work_Permit_Issue_Place = txtIssuePlace.Text
    '            If dtIssueDate.GetDate <> "1/1/2000" Then
    '                objEmployee._Work_Permit_Issue_Date = dtIssueDate.GetDate
    '            End If
    '            If dtExpiryDate.GetDate <> "1/1/2000" Then
    '                objEmployee._Work_Permit_Expiry_Date = dtExpiryDate.GetDate
    '            End If
    '            '************************* WORKING INFO *****************************'

    '            '************************* OTHER INFO *****************************'
    '            objEmployee._Complexionunkid = cboComplexion.SelectedValue
    '            objEmployee._Extra_Tel_No = txtExTel.Text
    '            objEmployee._Bloodgroupunkid = cboBloodGrp.SelectedValue
    '            objEmployee._Eyecolorunkid = cboEyeColor.SelectedValue
    '            objEmployee._Language1unkid = cboLang1.SelectedValue
    '            objEmployee._Language2unkid = cboLang2.SelectedValue
    '            objEmployee._Language3unkid = cboLang3.SelectedValue
    '            objEmployee._Language4unkid = cboLang4.SelectedValue
    '            objEmployee._Ethincityunkid = cboEthnicity.SelectedValue
    '            objEmployee._Nationalityunkid = cboNationality.SelectedValue
    '            objEmployee._Religionunkid = cboReligion.SelectedValue
    '            objEmployee._Hairunkid = cboHair.SelectedValue
    '            objEmployee._Height = IIf(txtHeight.Text = "", 0, txtHeight.Text)
    '            objEmployee._Weight = IIf(txtWeight.Text = "", 0, txtWeight.Text)
    '            objEmployee._Maritalstatusunkid = cboMaritalStatus.SelectedValue
    '            If dtMarriedDate.GetDate <> "1/1/2000" Then
    '                objEmployee._Anniversary_Date = dtMarriedDate.GetDate
    '            End If
    '            objEmployee._Sports_Hobbies = txtSpHob.Text

    '            Dim mStrUnkids As String = String.Empty

    '            For Each mItem As ListItem In chkLstAllergies.Items
    '                If mItem.Selected Then
    '                    mStrUnkids &= "," & mItem.Value
    '                End If
    '            Next
    '            If mStrUnkids.Length > 0 Then mStrUnkids = Mid(mStrUnkids, 2)
    '            objEmployee._Allergies = mStrUnkids

    '            mStrUnkids = ""
    '            For Each mItem As ListItem In chkLstDisabilities.Items
    '                If mItem.Selected Then
    '                    mStrUnkids &= "," & mItem.Value
    '                End If
    '            Next
    '            If mStrUnkids.Length > 0 Then mStrUnkids = Mid(mStrUnkids, 2)
    '            objEmployee._Disabilities = mStrUnkids
    '            '************************* OTHER INFO *****************************'
    '            If imgEmp.ImageUrl <> "" Then
    '                objEmployee._ImagePath = Me.ViewState("ImageName")
    '            End If

    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub DeleteImage(ByVal imgFullPath As String)
    '        Dim f As New System.IO.FileInfo(imgFullPath)
    '        If System.IO.File.Exists(Server.MapPath("~\images\" & f.Name)) Then
    '            System.IO.File.Delete(Server.MapPath("~\images\" & f.Name))
    '        End If
    '    End Sub

    '#End Region

    '#Region " Form's Event(s) "

    '    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '        Try

    '            If Session("clsuser") Is Nothing Then
    '                
    '            End If

    '            If (Page.IsPostBack = False) Then
    '                clsuser = CType(Session("clsuser"), User)
    '                Call FillCombo() : Call Fill_List() : Fill_Info()
    '            End If

    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
    '        If IsPostBack = True And Me.ViewState("OldImage") <> "" Then
    '            Call DeleteImage(Server.MapPath("~\images\" & Me.ViewState("OldImage")))
    '        End If
    '    End Sub

    '#End Region

    '#Region " Button's Event(s) "

    '    'Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
    '    '    Try
    '    '        If imgUpload.PostedFile.FileName <> "" Then
    '    '            Call DeleteImage(Server.MapPath("~\images\" & Me.ViewState("ImageName")))
    '    '            Call ImageOperation(imgUpload.PostedFile.FileName.ToString)
    '    '            Dim nF As New System.IO.FileInfo(imgUpload.PostedFile.FileName)
    '    '            Me.ViewState("NewImg") = nF.Name
    '    '        End If
    '    '    Catch ex As Exception
    '    '        msg.DisplayError(ex, Me)
    '    '    Finally
    '    '    End Try
    '    'End Sub

    '    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
    '        Try

    '            If dtIssueDate.IsNull = False And dtExpiryDate.IsNull = False Then
    '                If dtExpiryDate.GetDate < dtIssueDate.GetDate Then
    '                    msg.DisplayMessage("Expiry Date cannot be less than or equal to Issue Date.", Me)
    '                    Exit Sub
    '                End If
    '            ElseIf dtIssueDate.IsNull = False Then
    '                msg.DisplayMessage("Expiry Date is mandatory information, Please set Expiry Date.", Me)
    '                Exit Sub
    '            ElseIf dtExpiryDate.IsNull = False Then
    '                msg.DisplayMessage("Issue Date is mandatory information, Please set Issue Date.", Me)
    '                Exit Sub
    '            End If

    '            If Expression.IsMatch(txtPEmail.Text.Trim) = False Then
    '                msg.DisplayMessage("Invalid Present Email.Please Enter Valid Email", Me)
    '                Exit Sub
    '            End If

    '            If Expression.IsMatch(txtDEmail.Text.Trim) = False Then
    '                msg.DisplayMessage("Invalid Domicile Email.Please Enter Valid Email", Me)
    '                Exit Sub
    '            End If

    '            If Expression.IsMatch(txtEEMail.Text.Trim) = False Then
    '                msg.DisplayMessage("Invalid Emergency Email.Please Enter Valid Email", Me)
    '                Exit Sub
    '            End If

    '            Call SetValue()

    '            If objEmployee.Update = False Then
    '                msg.DisplayMessage(objEmployee._Message, Me)
    '            Else
    '                msg.DisplayMessage("Information successfully updated.", Me)
    '                Call ClearObjects()
    '            End If

    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '        Try
    '            Response.Redirect("~\UserHome.aspx", False)
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '        Try
    '            Response.Redirect("~\UserHome.aspx", False)
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '#End Region

    '#Region " Control's Event(s) "

    '    Protected Sub cboPCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPCountry.SelectedIndexChanged
    '        Try
    '            If cboPCountry.SelectedValue >= 0 Then
    '                Dim objState As New clsstate_master
    '                Dim dsList As New DataSet
    '                dsList = objState.GetList("List", True, True, cboPCountry.SelectedValue)
    '                With cboPState
    '                    .DataValueField = "stateunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboPState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPState.SelectedIndexChanged
    '        Try
    '            If cboPState.SelectedValue >= 0 Then
    '                Dim objCity As New clscity_master
    '                Dim dsList As DataSet = objCity.GetList("List", True, True, cboPState.SelectedValue)
    '                With cboPCity
    '                    .DataValueField = "cityunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List").Copy
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboPCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPCity.SelectedIndexChanged
    '        Try
    '            If cboPCity.SelectedValue >= 0 Then
    '                Dim objZip As New clszipcode_master
    '                Dim dsList As DataSet = objZip.GetList("List", True, True, cboPCity.SelectedValue)
    '                With cboPZipcode
    '                    .DataValueField = "zipcodeunkid"
    '                    .DataTextField = "zipcode_no"
    '                    .DataSource = dsList.Tables("List").Copy
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboDCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDCountry.SelectedIndexChanged
    '        Try
    '            If cboDCountry.SelectedValue >= 0 Then
    '                Dim objState As New clsstate_master
    '                Dim dsList As New DataSet
    '                dsList = objState.GetList("List", True, True, cboDCountry.SelectedValue)
    '                With cboDState
    '                    .DataValueField = "stateunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboDState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDState.SelectedIndexChanged
    '        Try
    '            If cboDState.SelectedValue >= 0 Then
    '                Dim objCity As New clscity_master
    '                Dim dsList As DataSet = objCity.GetList("List", True, True, cboDState.SelectedValue)
    '                With cboDCity
    '                    .DataValueField = "cityunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List").Copy
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboDCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDCity.SelectedIndexChanged
    '        Try
    '            If cboDCity.SelectedValue >= 0 Then
    '                Dim objZip As New clszipcode_master
    '                Dim dsList As DataSet = objZip.GetList("List", True, True, cboDCity.SelectedValue)
    '                With cboDZipcode
    '                    .DataValueField = "zipcodeunkid"
    '                    .DataTextField = "zipcode_no"
    '                    .DataSource = dsList.Tables("List").Copy
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboECountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECountry.SelectedIndexChanged
    '        Try
    '            If cboECountry.SelectedValue >= 0 Then
    '                Dim objState As New clsstate_master
    '                Dim dsList As New DataSet
    '                dsList = objState.GetList("List", True, True, cboECountry.SelectedValue)
    '                With cboEState
    '                    .DataValueField = "stateunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboECountry2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECountry2.SelectedIndexChanged
    '        Try
    '            If cboECountry2.SelectedValue >= 0 Then
    '                Dim objState As New clsstate_master
    '                Dim dsList As New DataSet
    '                dsList = objState.GetList("List", True, True, cboECountry2.SelectedValue)
    '                With cboEState2
    '                    .DataValueField = "stateunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboECountry3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECountry3.SelectedIndexChanged
    '        Try
    '            If cboECountry3.SelectedValue >= 0 Then
    '                Dim objState As New clsstate_master
    '                Dim dsList As New DataSet
    '                dsList = objState.GetList("List", True, True, cboECountry3.SelectedValue)
    '                With cboEState3
    '                    .DataValueField = "stateunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboEState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEState.SelectedIndexChanged
    '        Try
    '            If cboEState.SelectedValue >= 0 Then
    '                Dim objCity As New clscity_master
    '                Dim dsList As DataSet = objCity.GetList("List", True, True, cboEState.SelectedValue)
    '                With cboECity
    '                    .DataValueField = "cityunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboEState2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEState2.SelectedIndexChanged
    '        Try
    '            If cboEState2.SelectedValue >= 0 Then
    '                Dim objCity As New clscity_master
    '                Dim dsList As DataSet = objCity.GetList("List", True, True, cboEState2.SelectedValue)
    '                With cboECity2
    '                    .DataValueField = "cityunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboEState3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEState3.SelectedIndexChanged
    '        Try
    '            If cboEState3.SelectedValue >= 0 Then
    '                Dim objCity As New clscity_master
    '                Dim dsList As DataSet = objCity.GetList("List", True, True, cboEState3.SelectedValue)
    '                With cboECity3
    '                    .DataValueField = "cityunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboECity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECity.SelectedIndexChanged
    '        Try
    '            If cboECity.SelectedValue >= 0 Then
    '                Dim objZip As New clszipcode_master
    '                Dim dsList As DataSet = objZip.GetList("List", True, True, cboECity.SelectedValue)
    '                With cboEZipcode
    '                    .DataValueField = "zipcodeunkid"
    '                    .DataTextField = "zipcode_no"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboECity2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECity2.SelectedIndexChanged
    '        Try
    '            If cboECity2.SelectedValue >= 0 Then
    '                Dim objZip As New clszipcode_master
    '                Dim dsList As DataSet = objZip.GetList("List", True, True, cboECity2.SelectedValue)
    '                With cboEZipcode2
    '                    .DataValueField = "zipcodeunkid"
    '                    .DataTextField = "zipcode_no"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboECity3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECity3.SelectedIndexChanged
    '        Try
    '            If cboECity3.SelectedValue >= 0 Then
    '                Dim objZip As New clszipcode_master
    '                Dim dsList As DataSet = objZip.GetList("List", True, True, cboECity3.SelectedValue)
    '                With cboEZipcode3
    '                    .DataValueField = "zipcodeunkid"
    '                    .DataTextField = "zipcode_no"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboBirthCounty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBirthCounty.SelectedIndexChanged
    '        Try
    '            If cboBirthCounty.SelectedValue >= 0 Then
    '                Dim objState As New clsstate_master
    '                Dim dsList As New DataSet
    '                dsList = objState.GetList("List", True, True, cboBirthCounty.SelectedValue)
    '                With cboBirthState
    '                    .DataValueField = "stateunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboBirthState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBirthState.SelectedIndexChanged
    '        Try
    '            If cboBirthState.SelectedValue >= 0 Then
    '                Dim objCity As New clscity_master
    '                Dim dsList As DataSet = objCity.GetList("List", True, True, cboBirthState.SelectedValue)
    '                With cboBirthCity
    '                    .DataValueField = "cityunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub lnkCopyAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopyAddress.Click
    '        Try
    '            txtDAddress1.Text = txtPAddress1.Text
    '            txtDAddress2.Text = txtPAddress2.Text
    '            cboDCountry.SelectedValue = cboPCountry.SelectedValue
    '            cboDState.SelectedValue = cboPState.SelectedValue
    '            cboDCity.SelectedValue = cboPCity.SelectedValue
    '            cboDZipcode.SelectedValue = cboPZipcode.SelectedValue
    '            txtDRegion.Text = txtPRegion.Text
    '            txtDStreet.Text = txtPStreet.Text
    '            txtDEState.Text = txtPEState.Text
    '            txtDPlotNo.Text = txtPPlotNo.Text
    '            txtDMobile.Text = txtPMobile.Text
    '            txtDAltNo.Text = txtPAltNo.Text
    '            txtDTelNo.Text = txtPTelNo.Text
    '            txtDFax.Text = txtPFax.Text
    '            txtDEmail.Text = txtPEmail.Text
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub lnkCopyEAddress1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopyEAddress1.Click
    '        Try
    '            txtEAddress2.Text = txtEAddress.Text
    '            txtEAltNo2.Text = txtEAltNo.Text
    '            txtEEMail2.Text = txtEEMail.Text
    '            txtEEState2.Text = txtEEState.Text
    '            txtEFax2.Text = txtEFax.Text
    '            txtEFirstname2.Text = txtEFirstname.Text
    '            txtELastname2.Text = txtELastname.Text
    '            txtEMobile2.Text = txtEMobile.Text
    '            txtEPlotNo2.Text = txtEPlotNo.Text
    '            cboECity2.SelectedValue = cboECity.SelectedValue
    '            cboEZipcode2.SelectedValue = cboEZipcode.SelectedValue
    '            txtERegion2.Text = txtERegion.Text
    '            txtEStreet2.Text = txtEStreet.Text
    '            cboEState2.SelectedValue = cboEState.SelectedValue
    '            txtETelNo2.Text = txtETelNo.Text
    '            cboECountry2.SelectedValue = cboECountry.SelectedValue
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub lnkCopyEAddress2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopyEAddress2.Click
    '        Try
    '            txtEAddress3.Text = txtEAddress.Text
    '            txtEAltNo3.Text = txtEAltNo.Text
    '            txtEEMail3.Text = txtEEMail.Text
    '            txtEEState3.Text = txtEEState.Text
    '            txtEFax3.Text = txtEFax.Text
    '            txtEFirstname3.Text = txtEFirstname.Text
    '            txtELastname3.Text = txtELastname.Text
    '            txtEMobile3.Text = txtEMobile.Text
    '            txtEPlotNo3.Text = txtEPlotNo.Text
    '            cboECity3.SelectedValue = cboECity.SelectedValue
    '            cboEZipcode3.SelectedValue = cboEZipcode.SelectedValue
    '            txtERegion3.Text = txtERegion.Text
    '            txtEStreet3.Text = txtEStreet.Text
    '            cboEState3.SelectedValue = cboEState.SelectedValue
    '            txtETelNo3.Text = txtETelNo.Text
    '            cboECountry3.SelectedValue = cboECountry.SelectedValue
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
    '        If e.Row.RowIndex < 0 Then Exit Sub
    '        If e.Row.Cells(1).Text.Trim <> "" Then
    '            e.Row.Cells(1).Text = eZeeDate.convertDate(e.Row.Cells(1).Text).Date
    '        End If
    '        If e.Row.Cells(2).Text.Trim <> "" Then
    '            e.Row.Cells(2).Text = eZeeDate.convertDate(e.Row.Cells(2).Text).Date
    '        End If
    '    End Sub

    '#End Region

End Class
