﻿Option Strict On 'Shani(19-MAR-2016)
#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region

Partial Class HR_wPgEmpReferee
    Inherits Basepage


#Region " Private Variable(s) "

    Dim DisplayMessage As New CommonCodes
    Private objReferee As clsEmployee_Refree_tran


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeReferee"
    'Pinkal (06-May-2014) -- End


    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintRefreeUnkid As Integer = -1
    Private objARefreeTran As New clsemployee_refree_approval_tran
    Dim Arr() As String
    Dim RefreeApprovalFlowVal As String
    'Gajanan [17-DEC-2018] -- End


    'Gajanan [3-April-2019] -- Start
    Dim mblnisEmployeeApprove As Boolean = False
    'Gajanan [3-April-2019] -- End

    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End


    'Gajanan [21-June-2019] -- Start      
    Private OldData As New clsEmployee_Refree_tran
    'Gajanan [21-June-2019] -- End
    'Sohail (09 Nov 2020) -- Start
    'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
    Private mstrURLReferer As String = ""
    'Sohail (09 Nov 2020) -- End


#End Region

#Region " Display Dialog "

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Try

            'If (Session("loginBy") = Global.User.en_loginby.Employee) Then
            '    cboEmployee.BindEmployee(CInt(Session("Employeeunkid")))
            'Else
            '    cboEmployee.BindEmployee()
            'End If
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsCombos = objEmployee.GetEmployeeList("Employee", True, Not Aruti.Data.ConfigParameter._Object._IsIncludeInactiveEmp)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsCombos = objEmployee.GetEmployeeList("Employee", True, )
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END
                'End If


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .

                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployeeRefereeList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If


                'dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                '                                       CInt(Session("UserId")), _
                '                                       CInt(Session("Fin_year")), _
                '                                       CInt(Session("CompanyUnkId")), _
                '                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                       CStr(Session("UserAccessModeSetting")), True, _
                '                                       CBool(Session("IsIncludeInactiveEmp")), "Employee", True)


                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), _
                                          mblnOnlyApproved, CBool(Session("IsIncludeInactiveEmp")), "Employee", True, _
                                          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "employeename"
                    .DataSource = dsCombos.Tables("Employee")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With

                'Shani(24-Aug-2015) -- End

                'S.SANDEEP [20-JUN-2018] -- End


                'S.SANDEEP [ 27 APRIL 2012 ] -- END
                'S.SANDEEP [ 16 JAN 2014 ] -- START
                'With cboEmployee
                '    .DataValueField = "employeeunkid"
                '    .DataTextField = "employeename"
                '    .DataSource = dsCombos.Tables("Employee")
                '    .DataBind()
                '    .SelectedValue = 0
                'End With


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .




                'If Session("ShowPending") IsNot Nothing Then

                '    'Shani(24-Aug-2015) -- Start
                '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '    'dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"), , , False)
                '    dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                '                                           CInt(Session("UserId")), _
                '                                           CInt(Session("Fin_year")), _
                '                                           CInt(Session("CompanyUnkId")), _
                '                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                           CStr(Session("UserAccessModeSetting")), False, _
                '                                           CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

                '    'Shani(24-Aug-2015) -- End
                '    Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
                '    With cboEmployee
                '        .DataValueField = "employeeunkid"
                '        'Nilay (09-Aug-2016) -- Start
                '        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '        '.DataTextField = "employeename"
                '        .DataTextField = "EmpCodeName"
                '        'Nilay (09-Aug-2016) -- End
                '        .DataSource = dtTab
                '        .DataBind()
                '    End With
                'Else
                '    With cboEmployee
                '        .DataValueField = "employeeunkid"
                '        'Nilay (09-Aug-2016) -- Start
                '        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '        '.DataTextField = "employeename"
                '        .DataTextField = "EmpCodeName"
                '        'Nilay (09-Aug-2016) -- End
                '        .DataSource = dsCombos.Tables("Employee")
                '        .DataBind()
                '        .SelectedValue = CStr(0)
                '    End With
                'End If
                ''S.SANDEEP [ 16 JAN 2014 ] -- END

                'S.SANDEEP [20-JUN-2018] -- End

            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If

            Dim objRefType As New clsCommon_Master
            cboRefType.DataSource = objRefType.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation", , True)
            cboRefType.DataValueField = "masterunkid"
            cboRefType.DataTextField = "name"
            cboRefType.DataBind()

            Dim objMstData As New clsMasterData
            cboCountry.DataSource = objMstData.getCountryList("Country", True)
            cboCountry.DataTextField = "country_name"
            cboCountry.DataValueField = "countryunkid"
            cboCountry.DataBind()

            objMstData = New clsMasterData
            cboGender.DataSource = objMstData.getGenderList("Gender", True)
            cboGender.DataTextField = "Name"
            cboGender.DataValueField = "Id"
            cboGender.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    Private Function Validation() As Boolean
        Try

            'Gajanan [21-June-2019] -- Start  

            If (mintRefreeUnkid > 0) Then
                objReferee._Refereetranunkid = mintRefreeUnkid
                OldData = objReferee
            End If

            If cboState.SelectedValue Is Nothing Or cboState.SelectedValue.ToString() = "" Then
                objARefreeTran._Stateunkid = -1
            Else
                objARefreeTran._Stateunkid = CInt(cboState.SelectedValue)
            End If


            If IsNothing(OldData) = False Then
                If OldData._Name = txtName.Text AndAlso _
                   OldData._Relationunkid = CInt(cboRefType.SelectedValue) AndAlso _
                   OldData._Address = txtAddress.Text AndAlso _
                   OldData._Countryunkid = CInt(IIf(cboCountry.SelectedValue Is Nothing Or cboCountry.SelectedValue.ToString() = "", 0, cboCountry.SelectedValue)) AndAlso _
                   OldData._Stateunkid = CInt(IIf(cboState.SelectedValue Is Nothing Or cboState.SelectedValue.ToString() = "", 0, cboState.SelectedValue)) AndAlso _
                   OldData._Cityunkid = CInt(IIf(cboTown.SelectedValue Is Nothing Or cboTown.SelectedValue.ToString() = "", 0, cboTown.SelectedValue)) AndAlso _
                   OldData._Gender = CInt(cboGender.SelectedValue) AndAlso _
                   OldData._Telephone_No = txtTelNo.Text AndAlso _
                   OldData._Mobile_No = txtMobile.Text AndAlso _
                   OldData._Email = txtEmail.Text AndAlso _
                   OldData._Company = txtCompany.Text AndAlso _
                   OldData._Ref_Position = txtPosition.Text Then

                    If (mintRefreeUnkid > 0) Then
                        objReferee = Nothing
                        OldData = Nothing
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpRefereeList.aspx", False)
                        Return False
                    End If
                Else
                    OldData = Nothing
                End If
            End If
            'Gajanan [21-June-2019] -- End



            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            If txtName.Text.Trim = "" Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Referee Name cannot be blank. Referee Name is compulsory information."), Me)
                txtName.Focus()
                Return False
            End If

            'S.SANDEEP [ 11 MAR 2014 ] -- START
            'Dim strExpression As String = "^([a-zA-Z0-9_\-\.']+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
            Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
            'S.SANDEEP [ 11 MAR 2014 ] -- END
            If txtEmail.Text.Length > 0 Then
                If Expression.IsMatch(txtEmail.Text.Trim) = False Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Invalid Email.Please Enter Valid Email"), Me)
                    'Pinkal (06-May-2014) -- End
                    txtEmail.Focus()
                    Return False
                End If
            End If

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If RefreeApprovalFlowVal Is Nothing Then
            If RefreeApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                If objApprovalData.IsApproverPresent(enScreenName.frmEmployeeRefereeList, CStr(Session("Database_Name")), _
                                                     CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                     CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences), _
                                                     CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(cboEmployee.SelectedValue.ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage(objApprovalData._Message, Me)
                    Return False
                End If
                'Gajanan [17-April-2019] -- End



                If objReferee.isExist(CInt(cboEmployee.SelectedValue), txtName.Text.Trim, mintRefreeUnkid) = True Then
                    DisplayMessage.DisplayMessage(Language.getMessage("clsEmployee_Refree_tran", 1, "This Referee is already defined. Please define new Referee."), Me)
                    Return False
                End If

                Dim intOperationType As Integer = 0
                Dim strMsg As String = ""
                If objARefreeTran.isExist(CInt(cboEmployee.SelectedValue), txtName.Text.Trim, Nothing, -1, "", Nothing, False, intOperationType) = True Then
                    If intOperationType > 0 Then
                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED
                                If mintRefreeUnkid > 0 Then
                                    strMsg = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in edit mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 5, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.DELETED
                                If mintRefreeUnkid > 0 Then
                                    strMsg = Language.getMessage(mstrModuleName, 6, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in deleted mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 7, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.ADDED
                                If mintRefreeUnkid > 0 Then
                                    strMsg = Language.getMessage(mstrModuleName, 8, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in added mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 9, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
                                End If
                        End Select
                    End If
                End If
                If strMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(strMsg, Me)
                    Return False
                End If
            End If
            'Gajanan [17-DEC-2018] -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Sub GetValue()
        Try
            txtAddress.Text = objReferee._Address
            cboTown.SelectedValue = CStr(objReferee._Cityunkid)
            cboCountry.SelectedValue = CStr(objReferee._Countryunkid)

            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            cboCountry_SelectedIndexChanged(New Object(), New EventArgs())
            'Pinkal (28-Dec-2015) -- End

            txtEmail.Text = objReferee._Email
            cboEmployee.SelectedValue = CStr(objReferee._Employeeunkid)
            cboGender.Text = CStr(objReferee._Gender)
            txtMobile.Text = objReferee._Mobile_No
            txtName.Text = objReferee._Name
            cboState.SelectedValue = CStr(objReferee._Stateunkid)

            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            cboState_SelectedIndexChanged(New Object(), New EventArgs())
            'Pinkal (28-Dec-2015) -- End

            txtTelNo.Text = objReferee._Telephone_No
            cboRefType.SelectedValue = CStr(CInt(objReferee._Relationunkid))
            txtCompany.Text = objReferee._Company
            txtPosition.Text = objReferee._Ref_Position
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValu :-  " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function SetValue() As Boolean


        Try
            'Gajanan [3-April-2019] -- Start
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)
            mblnisEmployeeApprove = objEmployee._Isapproved
            'Gajanan [3-April-2019] -- End

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'Gajanan [3-April-2019] -- Start

            'If RefreeApprovalFlowVal Is Nothing Then
            If RefreeApprovalFlowVal Is Nothing AndAlso objEmployee._Isapproved = True Then
                'Gajanan [3-April-2019] -- End
                objARefreeTran._Audittype = enAuditType.ADD
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    objARefreeTran._Audituserunkid = CInt(Session("UserId"))
                Else
                    objARefreeTran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                End If

                objARefreeTran._Address = txtAddress.Text

                If cboTown.SelectedValue Is Nothing Or cboTown.SelectedValue.ToString() = "" Then
                    objARefreeTran._Cityunkid = -1
                Else
                    objARefreeTran._Cityunkid = CInt(cboTown.SelectedValue)
                End If

                If cboCountry.SelectedValue Is Nothing Or cboCountry.SelectedValue.ToString() = "" Then
                    objARefreeTran._Countryunkid = -1
                Else
                    objARefreeTran._Countryunkid = CInt(cboCountry.SelectedValue)
                End If

                If cboState.SelectedValue Is Nothing Or cboState.SelectedValue.ToString() = "" Then
                    objARefreeTran._Stateunkid = -1
                Else
                    objARefreeTran._Stateunkid = CInt(cboState.SelectedValue)
                End If

                objARefreeTran._Email = txtEmail.Text
                objARefreeTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objARefreeTran._Gender = CInt(cboGender.SelectedValue)
                objARefreeTran._Company = txtCompany.Text
                objARefreeTran._Mobile_No = txtMobile.Text
                objARefreeTran._Name = txtName.Text
                objARefreeTran._Telephone_No = txtTelNo.Text
                objARefreeTran._Relationunkid = CInt(cboRefType.SelectedValue)
                objARefreeTran._Ref_Position = txtPosition.Text.Trim
                objARefreeTran._Isvoid = False
                objARefreeTran._Tranguid = Guid.NewGuid.ToString()
                objARefreeTran._Transactiondate = Now
                objARefreeTran._Approvalremark = ""
                objARefreeTran._Isweb = True
                objARefreeTran._Isfinal = False
                objARefreeTran._Ip = Session("IP_ADD").ToString()
                objARefreeTran._Host = Session("HOST_NAME").ToString()
                objARefreeTran._Form_Name = mstrModuleName
                objARefreeTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                If mintRefreeUnkid <= 0 Then
                    objARefreeTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.ADDED
                    objARefreeTran._Refereetranunkid = -1
                Else
                    objARefreeTran._Refereetranunkid = mintRefreeUnkid
                    objARefreeTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.EDITED
                End If
            Else
                objReferee._Address = txtAddress.Text

                If cboTown.SelectedValue Is Nothing Or cboTown.SelectedValue.ToString() = "" Then
                    objReferee._Cityunkid = -1
                Else
                    objReferee._Cityunkid = CInt(cboTown.SelectedValue)
                End If

                If cboCountry.SelectedValue Is Nothing Or cboCountry.SelectedValue.ToString() = "" Then
                    objReferee._Countryunkid = -1
                Else
                    objReferee._Countryunkid = CInt(cboCountry.SelectedValue)
                End If

                If cboState.SelectedValue Is Nothing Or cboState.SelectedValue.ToString() = "" Then
                    objReferee._Stateunkid = -1
                Else
                    objReferee._Stateunkid = CInt(cboState.SelectedValue)
                End If

                objReferee._Email = txtEmail.Text
                objReferee._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objReferee._Gender = CInt(cboGender.Text)
                'objReferee._Identify_No = txtIdNo.Text
                objReferee._Mobile_No = txtMobile.Text
                objReferee._Name = txtName.Text
                objReferee._Telephone_No = txtTelNo.Text
                objReferee._Relationunkid = CInt(cboRefType.SelectedValue)

                'Sohail (21 Mar 2012) -- Start
                'TRA - ENHANCEMENT
                objReferee._Company = txtCompany.Text.Trim
                objReferee._Ref_Position = txtPosition.Text.Trim
                'Sohail (21 Mar 2012) -- End

                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                objReferee._Userunkid = CInt(Session("UserId"))
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
            End If
            'Gajanan [17-DEC-2018] -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue : - " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
        Return True
    End Function

    Private Sub BlankObjects()
        Try
            txtAddress.Text = ""
            If cboTown.Items.Count > 0 Then cboTown.SelectedIndex = 0
            If cboCountry.Items.Count > 0 Then cboCountry.SelectedIndex = 0
            txtEmail.Text = ""


            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes
            'cboEmployee.SelectedIndex = 0
            'Pinkal (22-Nov-2012) -- End



            If cboGender.Items.Count > 0 Then cboGender.SelectedIndex = 0
            txtCompany.Text = ""
            txtMobile.Text = ""
            txtName.Text = ""
            If cboState.Items.Count > 0 Then cboState.SelectedIndex = 0
            txtTelNo.Text = ""
            If cboRefType.Items.Count > 0 Then cboRefType.SelectedIndex = 0
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("BlankObjects :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            objReferee = New clsEmployee_Refree_tran
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmEmployeeReferee"
            StrModuleName2 = "mnuPersonnel"
            StrModuleName3 = "mnuEmployeeData"
            clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))

            'Pinkal (24-Aug-2012) -- Start
            'Enhancement : TRA Changes

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            'Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END


            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            RefreeApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmEmployeeRefereeList)))

            If Session("RefreeTranId") IsNot Nothing Then
                mintRefreeUnkid = CInt(Session("RefreeTranId"))
            End If
            'Gajanan [17-DEC-2018] -- End


            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then

                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Aruti.Data.User._Object._Userunkid = -1
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                'Anjan (28 Feb 2012)-Start
                'ENHANCEMENT : SSRA COMMENTS on Andrew sir's Request

                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'btnSave.Visible = ConfigParameter._Object._AllowEditReference
                btnSaveInfo.Visible = CBool(Session("AllowEditReference"))
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
                'Anjan (28 Feb 2012)-End 

            Else
                objReferee._Userunkid = CInt(Session("UserId"))

                'Anjan (28 Feb 2012)-Start
                'ENHANCEMENT : SSRA COMMENTS on Andrew sir's Request

                'Anjan (30 May 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                'btnSave.Visible = False
                btnSaveInfo.Visible = CBool(Session("EditEmployeeReferee"))
                'Anjan (30 May 2012)-End 


                'Anjan (28 Feb 2012)-End 
            End If


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            'Hemant (31 May 2019) -- Start
            'ISSUE/ENHANCEMENT : UAT Changes
            Call SetMessages()
            Call Language._Object.SaveValue()
            'Hemant (31 May 2019) -- End
            SetLanguage()
            'Pinkal (06-May-2014) -- End


            If Not IsPostBack Then
                FillCombo()

                If mintRefreeUnkid > 0 Then
                    'Gajanan [17-DEC-2018] -- Start
                    'If Session("RefreeTranId") IsNot Nothing Then
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'objReferee._Refereetranunkid = CInt(Session("RefreeTranId"))

                    objReferee._Refereetranunkid = mintRefreeUnkid

                    'Gajanan [17-DEC-2018] -- End

                    GetValue()
                    'SHANI [09 Mar 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                Else
                    If Session("RefreeTran_EmpUnkID") IsNot Nothing Then
                        cboEmployee.SelectedValue = CStr(Session("RefreeTran_EmpUnkID"))
                    End If
                    'SHANI [09 Mar 2015]--END

                    'Gajanan [3-April-2019] -- Start
                    If IsNothing(ViewState("mblnisEmployeeApprove")) = False Then
                        mblnisEmployeeApprove = CBool(ViewState("mblnisEmployeeApprove"))
                    End If
                    'Gajanan [3-April-2019] -- End
                End If

                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                If Request.UrlReferrer IsNot Nothing Then
                    mstrURLReferer = Request.UrlReferrer.AbsoluteUri
                Else
                    mstrURLReferer = Session("rootpath").ToString & "HR/wPgEmpRefereeList.aspx"
                End If
            Else
                mstrURLReferer = ViewState("mstrURLReferer").ToString
                'Sohail (09 Nov 2020) -- End
            End If

            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If Session("RefreeTranId") Is Nothing Or CInt(Session("RefreeTranId")) <= 0 Then
                If mintRefreeUnkid <= 0 Then
                    'Gajanan [17-DEC-2018] -- End
                    btnSaveInfo.Visible = CBool(Session("AddEmployeeReferee"))
                Else
                    btnSaveInfo.Visible = CBool(Session("EditEmployeeReferee"))
                End If


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .

                ''S.SANDEEP [ 16 JAN 2014 ] -- START
                'If Session("ShowPending") IsNot Nothing Then
                '    btnSaveInfo.Visible = False
                'End If
                ''S.SANDEEP [ 16 JAN 2014 ] -- END

                'S.SANDEEP [20-JUN-2018] -- End

            Else

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If Session("RefreeTranId") Is Nothing Or CInt(Session("RefreeTranId")) <= 0 Then
                If mintRefreeUnkid <= 0 Then
                    'Gajanan [17-DEC-2018] -- End

                    btnSaveInfo.Visible = CBool(Session("AllowAddReference"))
                Else
                    btnSaveInfo.Visible = CBool(Session("AllowEditReference"))
                End If
            End If

            'Pinkal (22-Nov-2012) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    ToolbarEntry1.VisibleImageSaprator1(False)
    '    ToolbarEntry1.VisibleImageSaprator2(False)
    '    ToolbarEntry1.VisibleImageSaprator3(False)
    '    ToolbarEntry1.VisibleImageSaprator4(False)
    '    ToolbarEntry1.VisibleSaveButton(False)
    '    ToolbarEntry1.VisibleCancelButton(False)
    '    ToolbarEntry1.VisibleNewButton(False)
    'End Sub
    'Nilay (01-Feb-2015) -- End



    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session("IsFrom_AddEdit") = False
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Gajanan [3-April-2019] -- Start
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mblnisEmployeeApprove") = mblnisEmployeeApprove
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            ViewState("mstrURLReferer") = mstrURLReferer
            'Sohail (09 Nov 2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [3-April-2019] -- End
#End Region

#Region " Button's Event(s) "
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnSaveInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveInfo.Click
        Try
            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)
            mblnisEmployeeApprove = objEmployee._Isapproved
            'Gajanan [17-April-2019] -- End


            Dim blnFlag As Boolean = False
            If Validation() Then


                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    objReferee._LoginEmployeeunkid = CInt(Session("Employeeunkid"))
                Else
                    objReferee._Userunkid = CInt(Session("UserId"))
                End If

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Dim eLMode As Integer
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    eLMode = enLogin_Mode.MGR_SELF_SERVICE
                Else
                    eLMode = enLogin_Mode.EMP_SELF_SERVICE
                End If
                'Gajanan [17-April-2019] -- End




                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                'If Session("RefreeTranId") Is Nothing Then
                If mintRefreeUnkid <= 0 Then
                    'Gajanan [17-DEC-2018] -- End

                    If SetValue() Then
                        'objReferee.Insert()

                        'Gajanan [3-April-2019] -- Start

                        'If RefreeApprovalFlowVal Is Nothing AndAlso mintRefreeUnkid <= 0 Then
                        If RefreeApprovalFlowVal Is Nothing AndAlso mintRefreeUnkid <= 0 AndAlso mblnisEmployeeApprove = True Then
                            'Gajanan [3-April-2019] -- End
                            blnFlag = objARefreeTran.Insert(CInt(Session("Companyunkid")))
                            If blnFlag = False AndAlso objARefreeTran._Message <> "" Then
                                DisplayMessage.DisplayMessage(objARefreeTran._Message, Me)
                                Exit Sub

                                'Gajanan [17-April-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            Else

                                objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                                 CStr(Session("UserAccessModeSetting")), _
                                                                 CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                                 CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences), _
                                                                 enScreenName.frmEmployeeRefereeList, CStr(Session("EmployeeAsOnDate")), _
                                                                 CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                                 Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.ADDED, , cboEmployee.SelectedValue.ToString(), , , _
                                                            "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and name = '" & txtName.Text & "' ", Nothing, False, clsEmployeeAddress_approval_tran.enAddressType.NONE, _
                                                            "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and name = '" & txtName.Text & "' ", Nothing)
                                'Gajanan [17-April-2019] -- End
                            End If
                        Else
                            blnFlag = objReferee.Insert()
                        End If


                        If objReferee._Message.Length > 0 Then
                            DisplayMessage.DisplayMessage("Entry Not Saved Due To " & objReferee._Message, Me)
                        Else
                            'Hemant (31 May 2019) -- Start
                            'ISSUE/ENHANCEMENT : UAT Changes
                            'DisplayMessage.DisplayMessage("Entry Saved Successfully !!!!!", Me)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2001, "Entry Saved Successfully !!!!!"), Me)
                            'Hemant (31 May 2019) -- End
                            BlankObjects()
                        End If

                    End If





                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'ElseIf CInt(Session("RefreeTranId")) > 0 Then
                ElseIf mintRefreeUnkid > 0 Then


                    'objReferee._Refereetranunkid = CInt(Session("RefreeTranId"))
                    objReferee._Refereetranunkid = mintRefreeUnkid

                    'Gajanan [17-DEC-2018] -- End

                    If SetValue() Then
                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'objReferee.Update()

                        'Gajanan [3-April-2019] -- Start



                        'If RefreeApprovalFlowVal Is Nothing AndAlso mintRefreeUnkid > 0 AndAlso mblnisEmployeeApprove = True Then
                        If RefreeApprovalFlowVal Is Nothing AndAlso mintRefreeUnkid > 0 AndAlso mblnisEmployeeApprove = True Then
                            'Gajanan [3-April-2019] -- End
                            blnFlag = objARefreeTran.Insert(CInt(Session("Companyunkid")))
                            If blnFlag = False AndAlso objARefreeTran._Message <> "" Then
                                DisplayMessage.DisplayMessage(objARefreeTran._Message, Me)
                                Exit Sub
                            Else

                                'Gajanan [17-April-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                                 CStr(Session("UserAccessModeSetting")), _
                                                                 CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                                 CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences), _
                                                                 enScreenName.frmEmployeeRefereeList, CStr(Session("EmployeeAsOnDate")), _
                                                                 CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                                 Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.EDITED, , cboEmployee.SelectedValue.ToString(), , , _
                                                            "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and name = '" & txtName.Text & "' ", Nothing, False, clsEmployeeAddress_approval_tran.enAddressType.NONE, _
                                                            "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and name = '" & txtName.Text & "' ", Nothing)
                                'Gajanan [17-April-2019] -- End

                            End If
                        Else
                            objReferee.Update()
                        End If
                        'Gajanan [17-DEC-2018] -- End


                        If objReferee._Message.Length > 0 Then
                            DisplayMessage.DisplayMessage("Entry Not Updated Due To " & objReferee._Message, Me)
                        Else
                            'Hemant (31 May 2019) -- Start
                            'ISSUE/ENHANCEMENT : UAT Changes
                            'DisplayMessage.DisplayMessage("Entry Updated Successfully !!!!!", Me)
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2002, "Entry Updated Successfully !!!!!"), Me)
                            'Hemant (31 May 2019) -- End
                            BlankObjects()

                            'Gajanan [17-DEC-2018] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.

                            'Session("RefreeTranId") = Nothing
                            mintRefreeUnkid = -1
                            'Gajanan [17-DEC-2018] -- End

                            Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpRefereeList.aspx", False)
                        End If

                    End If

                End If

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSaveInfo_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\UserHome.aspx", False)

            'Gajanan [21-June-2019] -- Start      
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpRefereeList.aspx", False)
            Response.Redirect(mstrURLReferer, False)
            'Sohail (09 Nov 2020) -- End
            'Gajanan [21-June-2019] -- End


            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Control's Event(s) "

    Protected Sub cboCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCountry.SelectedIndexChanged
        Try
            If CInt(cboCountry.SelectedValue) > 0 Then
                Dim objState As New clsstate_master
                cboState.DataSource = objState.GetList("State", True, True, CInt(cboCountry.SelectedValue))
                cboState.DataValueField = "stateunkid"
                cboState.DataTextField = "name"
                cboState.DataBind()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboCountry_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboState.SelectedIndexChanged
        Try
            If CInt(cboCountry.SelectedValue) > 0 Then
                Dim objCity As New clscity_master
                cboTown.DataSource = objCity.GetList("City", True, True, CInt(cboState.SelectedValue))
                cboTown.DataValueField = "cityunkid"
                cboTown.DataTextField = "name"
                cboTown.DataBind()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboState_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        Response.Redirect(Session("servername") & "~/HR/wPgEmpRefereeList.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End
        Me.lblAddress.Text = Language._Object.getCaption(Me.lblAddress.ID, Me.lblAddress.Text)
        Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.ID, Me.lblGender.Text)
        Me.lblMobile.Text = Language._Object.getCaption(Me.lblMobile.ID, Me.lblMobile.Text)
        Me.lblEmail.Text = Language._Object.getCaption(Me.lblEmail.ID, Me.lblEmail.Text)
        Me.lblTelNo.Text = Language._Object.getCaption(Me.lblTelNo.ID, Me.lblTelNo.Text)
        Me.lblIDNo.Text = Language._Object.getCaption(Me.lblIDNo.ID, Me.lblIDNo.Text)
        Me.lblPostTown.Text = Language._Object.getCaption(Me.lblPostTown.ID, Me.lblPostTown.Text)
        Me.lblPostCountry.Text = Language._Object.getCaption(Me.lblPostCountry.ID, Me.lblPostCountry.Text)
        Me.lblRefereeName.Text = Language._Object.getCaption(Me.lblRefereeName.ID, Me.lblRefereeName.Text)
        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.lblState.Text = Language._Object.getCaption(Me.lblState.ID, Me.lblState.Text)
        Me.lblRefereeType.Text = Language._Object.getCaption(Me.lblRefereeType.ID, Me.lblRefereeType.Text)
        Me.lblPosition.Text = Language._Object.getCaption(Me.lblPosition.ID, Me.lblPosition.Text)
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Me.btnSaveInfo.Text = Language._Object.getCaption(Me.btnSaveInfo.ID, Me.btnSaveInfo.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Referee Name cannot be blank. Referee Name is compulsory information.")
            Language.setMessage(mstrModuleName, 3, "Invalid Email.Please Enter Valid Email")
            Language.setMessage(mstrModuleName, 4, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in edit mode.")
            Language.setMessage(mstrModuleName, 5, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in edit mode.")
            Language.setMessage(mstrModuleName, 6, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in deleted mode.")
            Language.setMessage(mstrModuleName, 7, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in deleted mode.")
            Language.setMessage(mstrModuleName, 8, "Sorry, you cannot edit seleted information, Reason : Same combination is already present for approval process in added mode.")
            Language.setMessage(mstrModuleName, 9, "Sorry, you cannot add selected information, Reason : Same combination is already present for approval process in added mode.")
            Language.setMessage(mstrModuleName, 2001, "Entry Saved Successfully !!!!!")
            Language.setMessage(mstrModuleName, 2002, "Entry Updated Successfully !!!!!")
            Language.setMessage("clsEmployee_Refree_tran", 1, "This Referee is already defined. Please define new Referee.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
