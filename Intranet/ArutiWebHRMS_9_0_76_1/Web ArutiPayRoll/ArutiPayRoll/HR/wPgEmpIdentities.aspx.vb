﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports System.IO

#End Region

Partial Class wPgEmpIdentities
    Inherits Basepage

#Region " Private Variable(s) "

    Private objEmpIdentity As New clsIdentity_tran
    Dim msg As New CommonCodes
    Dim clsuser As New User


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeMaster"
    Private ReadOnly mstrModuleName1 As String = "frmEmployeeList"
    'Pinkal (06-May-2014) -- End

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private objAIdInfoTran As New clsIdentity_Approval_tran
    Dim Arr() As String
    Dim IdentityApprovalFlowVal As String
    'Gajanan [22-Feb-2019] -- End



    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.

    Dim mdtAttachment As DataTable
    'Gajanan [17-April-2019] -- End

    'S.SANDEEP |26-APR-2019| -- START
    Private mblnAttachmentPopup As Boolean = False
    Private objScanAttachment As New clsScan_Attach_Documents
    Private mintDeleteIndex As Integer = 0
    Private mintAddressTypeId As Integer = 0
    'S.SANDEEP |26-APR-2019| -- END


    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objCMaster As New clsCommon_Master
        Dim objMaster As New clsMasterData
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsCombos = objEmployee.GetEmployeeList("Employee", True, Not Aruti.Data.ConfigParameter._Object._IsIncludeInactiveEmp)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsCombos = objEmployee.GetEmployeeList("Employee", True, )
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END
                'End If
                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                'Shani(24-Aug-2015) -- End

                'S.SANDEEP [ 27 APRIL 2012 ] -- END


                'S.SANDEEP [ 16 JAN 2014 ] -- START
                'With cboEmployee
                '    .DataValueField = "employeeunkid"
                '    .DataTextField = "employeename"
                '    .DataSource = dsCombos.Tables("Employee")
                '    .DataBind()
                '    .SelectedValue = 0
                'End With
                If Session("ShowPending") IsNot Nothing Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"), , , False)
                    dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                           CInt(Session("UserId")), _
                                                           CInt(Session("Fin_year")), _
                                                           CInt(Session("CompanyUnkId")), _
                                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                           eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                           CStr(Session("UserAccessModeSetting")), False, _
                                                           CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                    'Shani(24-Aug-2015) -- End
                    Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
                    With cboEmployee
                        .DataValueField = "employeeunkid"
                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        '.DataTextField = "employeename"
                        .DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        .DataSource = dtTab
                        .DataBind()
                    End With
                Else
                    With cboEmployee
                        .DataValueField = "employeeunkid"
                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        '.DataTextField = "employeename"
                        .DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        .DataSource = dsCombos.Tables("Employee")
                        .DataBind()
                        .SelectedValue = CStr(0)
                    End With
                End If
                'S.SANDEEP [ 16 JAN 2014 ] -- END

            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If

            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "List")
            With cboIdType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With


            dsCombos = objMaster.getCountryList("List", True)
            With cboCountry
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            'S.SANDEEP |26-APR-2019| -- START
            dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With
            'S.SANDEEP |26-APR-2019| -- END

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    

    'Public Function b64decode(ByVal StrDecode As String) As String
    '    Dim decodedString As String = ""
    '    Try
    '        decodedString = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(StrDecode))
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    '    Return decodedString
    'End Function
    'Gajanan [5-Dec-2019] -- End

    Private Function IsFromIdentityList() As Boolean
        Try
            If (Request.QueryString("ProcessId") <> "") Then
                Dim mintIdTranUnkid As Integer = -1
                mintIdTranUnkid = CInt(Val(b64decode(Request.QueryString("ProcessId"))))
                Me.ViewState.Add("Unkid", mintIdTranUnkid)
                Dim dsList As New DataSet
                dsList = objEmpIdentity.GetIdentity_tranList("List", , , mintIdTranUnkid)
                If dsList.Tables(0).Rows.Count > 0 Then
                    Call Fill_Info(dsList)
                End If
            Else
                Me.ViewState.Add("Unkid", -1)
            End If


            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                If Me.ViewState("Unkid") Is Nothing Or CInt(Me.ViewState("Unkid")) <= 0 Then
                    BtnSave.Visible = CBool(Session("AddEmployee"))
                Else
                    BtnSave.Visible = CBool(Session("EditEmployee"))
                End If

                'S.SANDEEP [ 16 JAN 2014 ] -- START
                If Session("ShowPending") IsNot Nothing Then
                    BtnSave.Visible = False
                End If
                'S.SANDEEP [ 16 JAN 2014 ] -- END

            Else

                If Me.ViewState("Unkid") Is Nothing Or CInt(Me.ViewState("Unkid")) <= 0 Then
                    BtnSave.Visible = CBool(Session("AllowAddIdentity"))
                Else
                    BtnSave.Visible = CBool(Session("AllowEditIdentity"))
                End If
            End If

            'Pinkal (22-Nov-2012) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub Fill_Info(ByVal dsData As DataSet)
        Try
            cboEmployee.SelectedValue = dsData.Tables(0).Rows(0)("employeeunkid").ToString
            cboCountry.SelectedValue = dsData.Tables(0).Rows(0)("countryunkid").ToString
            cboIdType.SelectedValue = dsData.Tables(0).Rows(0)("idtypeunkid").ToString
            Me.ViewState.Add("IdType", dsData.Tables(0).Rows(0)("idtypeunkid"))
            txtDLClass.Text = dsData.Tables(0).Rows(0)("dl_class").ToString
            txtIdNo.Text = dsData.Tables(0).Rows(0)("identity_no").ToString
            txtIdSrNo.Text = dsData.Tables(0).Rows(0)("serial_no").ToString
            txtIssuePlace.Text = dsData.Tables(0).Rows(0)("issued_place").ToString
            If IsDBNull(dsData.Tables(0).Rows(0)("expiry_date")) = False Then
                dtpExpiryDate.SetDate = CDate(dsData.Tables(0).Rows(0)("expiry_date")).Date
            Else
                dtpExpiryDate.SetDate = Nothing
            End If

            If IsDBNull(dsData.Tables(0).Rows(0)("issue_date")) = False Then
                dtpIssueDate.SetDate = CDate(dsData.Tables(0).Rows(0)("issue_date")).Date
            Else
                dtpIssueDate.SetDate = Nothing
            End If

            chkMakeAsDefault.Checked = CBool(dsData.Tables(0).Rows(0)("isdefault"))

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function isValidData() As Boolean
        Try
            If CInt(Me.ViewState("IdType")) <> CInt(cboIdType.SelectedValue) Then
                Dim dsValid As New DataSet
                dsValid = objEmpIdentity.GetIdentity_tranList("List", CInt(cboEmployee.SelectedValue), CInt(cboIdType.SelectedValue))

                If dsValid.Tables(0).Rows.Count > 0 Then
                    'Hemant (31 May 2019) -- Start
                    'ISSUE/ENHANCEMENT : UAT Changes
                    'msg.DisplayMessage("Sorry, this Identity is already present for the selected employee. Please add new Identity to continue.", Me)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 2001, "Sorry, this Identity is already present for the selected employee. Please add new Identity to continue."), Me)
                    'Hemant (31 May 2019) -- End
                    Return False
                End If
            End If


            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Hemant (31 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes
                'msg.DisplayMessage("Employee is mandatory information. Please select Employee to continue.", Me)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 2002, "Employee is mandatory information. Please select Employee to continue."), Me)
                'Hemant (31 May 2019) -- End
                Return False
            End If

            If CInt(cboIdType.SelectedValue) <= 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'msg.DisplayMessage("Identity is mandatory information. Please select Identity to continue.", Me)
                Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 39, "Id Type is compulsory information, it can not be blank. Please select a Id Type to continue."), Me)
                'Pinkal (06-May-2014) -- End
                Return False
            End If

            If dtpIssueDate.IsNull = False AndAlso dtpExpiryDate.IsNull = False Then
                If dtpExpiryDate.GetDate.Date <= dtpIssueDate.GetDate.Date Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 38, "Expiry date cannot be less then or equal to Issue date."), Me)
                    'Pinkal (06-May-2014) -- End
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    'Sohail (02 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub BlankObjects()
        Try

            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            'cboEmployee.SelectedIndex = 0

            'Pinkal (22-Nov-2012) -- End

            cboIdType.SelectedValue = CStr(0)
            cboCountry.SelectedValue = CStr(0)
            txtIdNo.Text = ""
            txtIdSrNo.Text = ""
            txtIssuePlace.Text = ""
            txtDLClass.Text = ""
            chkMakeAsDefault.Checked = False
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (02 May 2012) -- End


    'S.SANDEEP |26-APR-2019| -- START
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtAttachment.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtAttachment.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
                dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dRow("filename") = f.Name
                dRow("scanattachrefid") = mintAddressTypeId
                dRow("modulerefid") = enImg_Email_RefId.Employee_Module
                dRow("userunkid") = Session("UserId")
                dRow("AUD") = "A"
                dRow("GUID") = Guid.NewGuid.ToString
                dRow("orgfilepath") = strfullpath
                dRow("valuename") = cboIdType.SelectedItem.Text
                dRow("transactionunkid") = CInt(Me.ViewState("Unkid"))
                dRow("attached_date") = Today.Date
                dRow("filepath") = strfullpath
                dRow("filename") = f.Name
                dRow("filesize") = f.Length / 1024
                dRow("attached_date") = Today.Date
                dRow("orgfilepath") = strfullpath
                dRow("form_name") = mstrModuleName
                dRow("userunkid") = CInt(Session("userid"))
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
                Dim objEmployee As New clsEmployee_Master
                Dim blnIsInApproval As Boolean = False : objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmployee.SelectedValue)
                If IdentityApprovalFlowVal Is Nothing AndAlso objEmployee._Isapproved Then
                    blnIsInApproval = True
                End If
                objEmployee = Nothing
                dRow("isinapproval") = blnIsInApproval
            Else
                'Hemant (31 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes
                'msg.DisplayMessage("Selected information is already present for particular employee.", Me)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 2003, "Selected information is already present for particular employee."), Me)
                'Hemant (31 May 2019) -- End
                Exit Sub
            End If
            mdtAttachment.Rows.Add(dRow)
            Call FillAttachment()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            If mdtAttachment Is Nothing Then Exit Sub
            'S.SANDEEP |31-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Employee Bio Data UAT]
            'dtView = New DataView(mdtAttachment, "AUD <> 'D' AND AUD <> '' ", "", DataViewRowState.CurrentRows)
            If CInt(Me.ViewState("Unkid")) <= 0 Then
                dtView = New DataView(mdtAttachment, "AUD <> 'D' AND AUD <> '' ", "", DataViewRowState.CurrentRows)
            Else
                dtView = New DataView(mdtAttachment, "AUD <> 'D' AND transactionunkid = '" & CInt(Me.ViewState("Unkid")) & "' ", "", DataViewRowState.CurrentRows)
            End If
            'S.SANDEEP |31-MAY-2019| -- END
            dgv_Attchment.AutoGenerateColumns = False
            dgv_Attchment.DataSource = dtView
            dgv_Attchment.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SaveDocumentIIS()
        Try
            If mdtAttachment IsNot Nothing Then
                Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                For Each dRow As DataRow In mdtAttachment.Rows
                    Dim iScanRefId As Integer = CInt(dRow.Item("scanattachrefid"))
                    Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = iScanRefId) Select (p.Item("Name").ToString)).FirstOrDefault

                    If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                        Dim strFileName As String = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                        If File.Exists(CStr(dRow("orgfilepath"))) Then
                            Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                            If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                            End If

                            File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strPath

                            dRow.AcceptChanges()
                        Else
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 999, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFilepath As String = dRow("filepath").ToString
                        Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                        If strFilepath.Contains(strArutiSelfService) Then
                            strFilepath = strFilepath.Replace(strArutiSelfService, "")
                            If Strings.Left(strFilepath, 1) <> "/" Then
                                strFilepath = "~/" & strFilepath
                            Else
                                strFilepath = "~" & strFilepath
                            End If

                            If File.Exists(Server.MapPath(strFilepath)) Then
                                File.Delete(Server.MapPath(strFilepath))
                            Else
                                msg.DisplayMessage(Language.getMessage(mstrModuleName, 999, "Configuration Path does not Exist."), Me)
                                Exit Sub
                            End If
                        Else
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 999, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |26-APR-2019| -- END

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmEmployeeMaster"
            StrModuleName2 = "mnuCoreSetups"
            clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))

            'Pinkal (24-Aug-2012) -- Start
            'Enhancement : TRA Changes

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            'Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            IdentityApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmIdentityInfoList)))
            'Gajanan [22-Feb-2019] -- End

            'Hemant (31 May 2019) -- Start
            'ISSUE/ENHANCEMENT : UAT Changes
            Call SetMessages()
            Call Language._Object.SaveValue()
            'Hemant (31 May 2019) -- End
            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            If (Page.IsPostBack = False) Then
                clsuser = CType(Session("clsuser"), User)

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then


                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = Session("UserId")
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END




                    'Anjan (30 May 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'BtnSave.Visible = False
                    'BtnSave.Visible = False
                    BtnSave.Visible = CBool(Session("EditEmployee"))
                    'Anjan (30 May 2012)-End 


                    'S.SANDEEP [ 16 JAN 2014 ] -- START
                    If Session("ShowPending") IsNot Nothing Then
                        BtnSave.Visible = False
                    End If
                    'S.SANDEEP [ 16 JAN 2014 ] -- END

                Else

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = -1
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'BtnSave.Visible = False


                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'BtnSave.Visible = ConfigParameter._Object._AllowEditIdentity
                    BtnSave.Visible = CBool(Session("AllowEditIdentity"))
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                    'Anjan (02 Mar 2012)-End 


                End If

                Call FillCombo()

                'Nilay (02-Mar-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                If Request.QueryString.Count <= 0 Then
                    If Session("identitiesEmpunkid") IsNot Nothing Then
                        cboEmployee.SelectedValue = CStr(CInt(Session("identitiesEmpunkid")))
                    End If
                End If
                'Nilay (02-Mar-2015) -- End

                'S.SANDEEP |26-APR-2019| -- START
            Else
                mdtAttachment = CType(Me.ViewState("FieldAttachement"), DataTable)
                mblnAttachmentPopup = CBool(Me.ViewState("mblnAttachmentPopup"))
                mintAddressTypeId = CInt(Me.ViewState("mintAddressTypeId"))
                mintDeleteIndex = CInt(Me.ViewState("mintDeleteIndex"))
                'S.SANDEEP |26-APR-2019| -- END
            End If


            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Form)
            'SHANI [01 FEB 2015]--END 


            'Pinkal (22-Mar-2012) -- End


            'S.SANDEEP |26-APR-2019| -- START
            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If
            'S.SANDEEP |26-APR-2019| -- END

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [01 FEB 2015]--END 
        End Try
    End Sub

    Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
        Try
            If Not IsPostBack Then
                IsFromIdentityList()
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)
        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'ToolbarEntry1.VisibleSaveButton(False)
        'ToolbarEntry1.VisibleDeleteButton(False)
        'ToolbarEntry1.VisibleCancelButton(False)
        'ToolbarEntry1.VisibleNewButton(False)
        'ToolbarEntry1.VisibleModeFormButton(False)
        'ToolbarEntry1.VisibleExitButton(False)
        'SHANI [01 FEB 2015]--END

        'S.SANDEEP |26-APR-2019| -- START
        Me.ViewState("FieldAttachement") = mdtAttachment
        Me.ViewState("mblnAttachmentPopup") = mblnAttachmentPopup
        Me.ViewState("mintAddressTypeId") = mintAddressTypeId
        Me.ViewState("mintDeleteIndex") = mintDeleteIndex
        'S.SANDEEP |26-APR-2019| -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub
    'Pinkal (22-Mar-2012) -- End
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Try
            If isValidData() = False Then Exit Sub

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Dim mdTran As New DataTable
            'objEmpIdentity._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
            'mdTran = objEmpIdentity._DataList

            ''Shani(22-Oct-2015) -- Start
            ''Issue : 
            'If chkMakeAsDefault.Checked Then
            '    If mdTran.Select("isdefault = 'True' AND identitytranunkid <> " & CInt(Me.ViewState("Unkid"))).Length > 0 Then
            '        msg.DisplayError(ex, Me)
            '        Exit Sub
            '    End If
            'End If
            ''Shani(22-OCt-2015) -- End

            'If CInt(Me.ViewState("Unkid")) <= 0 Then
            '    Dim dtIdRow As DataRow
            '    dtIdRow = mdTran.NewRow
            '    dtIdRow.Item("identitytranunkid") = -1
            '    dtIdRow.Item("employeeunkid") = cboEmployee.SelectedValue
            '    dtIdRow.Item("idtypeunkid") = CInt(cboIdType.SelectedValue)
            '    dtIdRow.Item("identity_no") = txtIdNo.Text
            '    dtIdRow.Item("countryunkid") = CInt(cboCountry.SelectedValue)
            '    dtIdRow.Item("serial_no") = txtIdSrNo.Text
            '    dtIdRow.Item("issued_place") = txtIssuePlace.Text
            '    dtIdRow.Item("dl_class") = txtDLClass.Text

            '    If dtpIssueDate.IsNull = False Then
            '        dtIdRow.Item("issue_date") = dtpIssueDate.GetDate.Date
            '    Else
            '        dtIdRow.Item("issue_date") = DBNull.Value
            '    End If
            '    If dtpExpiryDate.IsNull = False Then
            '        dtIdRow.Item("expiry_date") = dtpExpiryDate.GetDate.Date
            '    Else
            '        dtIdRow.Item("expiry_date") = DBNull.Value
            '    End If
            '    dtIdRow.Item("isdefault") = chkMakeAsDefault.Checked
            '    dtIdRow.Item("GUID") = Guid.NewGuid().ToString
            '    dtIdRow.Item("AUD") = "A"

            '    mdTran.Rows.Add(dtIdRow)
            'Else
            '    Dim dtIdRow() As DataRow = mdTran.Select("identitytranunkid = '" & Me.ViewState("Unkid").ToString & "'")
            '    If dtIdRow.Length <= 0 Then Exit Sub
            '    dtIdRow(0).Item("identitytranunkid") = Me.ViewState("Unkid")
            '    dtIdRow(0).Item("employeeunkid") = cboEmployee.SelectedValue
            '    dtIdRow(0).Item("idtypeunkid") = CInt(cboIdType.SelectedValue)
            '    dtIdRow(0).Item("identity_no") = txtIdNo.Text
            '    dtIdRow(0).Item("countryunkid") = CInt(cboCountry.SelectedValue)
            '    dtIdRow(0).Item("serial_no") = txtIdSrNo.Text
            '    dtIdRow(0).Item("issued_place") = txtIssuePlace.Text
            '    dtIdRow(0).Item("dl_class") = txtDLClass.Text
            '    If dtpIssueDate.IsNull = False Then
            '        dtIdRow(0).Item("issue_date") = dtpIssueDate.GetDate.Date
            '    Else
            '        dtIdRow(0).Item("issue_date") = DBNull.Value
            '    End If
            '    If dtpExpiryDate.IsNull = False Then
            '        dtIdRow(0).Item("expiry_date") = dtpExpiryDate.GetDate.Date
            '    Else
            '        dtIdRow(0).Item("expiry_date") = DBNull.Value
            '    End If
            '    dtIdRow(0).Item("isdefault") = chkMakeAsDefault.Checked
            '    dtIdRow(0).Item("GUID") = Guid.NewGuid().ToString
            '    dtIdRow(0).Item("AUD") = "U"
            'End If

            'If CInt(Me.ViewState("Unkid")) <= 0 Then
            '    objEmpIdentity._DataList = mdTran

            '    'Pinkal (28-Dec-2015) -- Start
            '    'Enhancement - Working on Changes in SS for Employee Master.
            '    'If objEmpIdentity.InsertUpdateDelete_IdentityTran(Session("UserId")) = False Then
            '    '        msg.DisplayMessage("Problem in Inserting Identity.", Me)
            '    '    Else
            '    '    Call BlankObjects()
            '    '        msg.DisplayMessage("Identity Successfully Inserted.", Me)
            '    '    End If
            '    If objEmpIdentity.InsertUpdateDelete_IdentityTran(Nothing, CInt(Session("UserId"))) = False Then
            '        msg.DisplayMessage("Problem in Inserting Identity.", Me)
            '    Else
            '        Call BlankObjects()
            '        msg.DisplayMessage("Identity Successfully Inserted.", Me)
            '    End If
            '    'Pinkal (28-Dec-2015) -- End
            'Else
            '    objEmpIdentity._DataList = mdTran

            '    'Pinkal (28-Dec-2015) -- Start
            '    'Enhancement - Working on Changes in SS for Employee Master.
            '    'If objEmpIdentity.InsertUpdateDelete_IdentityTran(Session("UserId")) = False Then
            '    If objEmpIdentity.InsertUpdateDelete_IdentityTran(Nothing, CInt(Session("UserId"))) = False Then
            '        'Pinkal (28-Dec-2015) -- End
            '        msg.DisplayMessage("Problem in updating Identity.", Me)
            '    Else
            '        msg.DisplayMessage("Identity Successfully Updated.", Me)
            '        'Nilay (02-Mar-2015) -- Start
            '        'Enhancement - REDESIGN SELF SERVICE.
            '        'Response.Redirect("~\HR\wPgEmpIdentities_List.aspx")
            '        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpIdentities_List.aspx")
            '        'Nilay (02-Mar-2015) -- End

            '    End If
            'End If


            'Gajanan [1-NOV-2019] -- Start    
            'Enhancement:Worked On NMB ESS Comment  
            Dim isChange = False

            If (Request.QueryString("ProcessId") <> "") Then
                Dim dsList As New DataSet
                dsList = objEmpIdentity.GetIdentity_tranList("List", , , CInt(ViewState("Unkid")))

                If CInt(cboEmployee.SelectedValue) <> CInt(dsList.Tables(0).Rows(0)("employeeunkid").ToString) Then
                    isChange = True
                ElseIf CInt(cboCountry.SelectedValue) <> CInt(dsList.Tables(0).Rows(0)("countryunkid").ToString) Then
                    isChange = True
                ElseIf CInt(cboIdType.SelectedValue) <> CInt(dsList.Tables(0).Rows(0)("idtypeunkid").ToString) Then
                    isChange = True
                ElseIf txtDLClass.Text <> dsList.Tables(0).Rows(0)("dl_class").ToString Then
                    isChange = True
                ElseIf txtIdNo.Text <> dsList.Tables(0).Rows(0)("identity_no").ToString Then
                    isChange = True
                ElseIf txtIdSrNo.Text <> dsList.Tables(0).Rows(0)("serial_no").ToString Then
                    isChange = True
                ElseIf txtIssuePlace.Text <> dsList.Tables(0).Rows(0)("issued_place").ToString Then
                    isChange = True
                ElseIf dtpExpiryDate.GetDate <> CDate(dsList.Tables(0).Rows(0)("expiry_date")).Date Then
                    isChange = True
                ElseIf dtpIssueDate.GetDate <> CDate(dsList.Tables(0).Rows(0)("issue_date")).Date Then
                    isChange = True
                ElseIf chkMakeAsDefault.Checked <> CBool(dsList.Tables(0).Rows(0)("isdefault")) Then
                    isChange = True
                End If

                If mdtAttachment IsNot Nothing AndAlso mdtAttachment.Rows.Count > 0 Then
                    If mdtAttachment.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD").Trim <> "").Count > 0 Then
                        isChange = True
                    End If
                End If
            Else
                isChange = True
            End If

            If isChange Then
                If CBool(Session("IdentityDocsAttachmentMandatory")) Then
                    If mdtAttachment Is Nothing OrElse mdtAttachment.Select("AUD <> 'D' and scanattachrefid = " & enScanAttactRefId.IDENTITYS & " ").Length <= 0 Then
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 304, "Scan/Document(s) is mandatory information for Identity.Please attach Scan/Document(s) in order to perform operation."), Me)
                        Exit Sub
                    End If
                End If
            End If

            'Gajanan [1-NOV-2019] -- End



            If isChange Then

                'S.SANDEEP |26-APR-2019| -- START
                Call SaveDocumentIIS()
                'S.SANDEEP |26-APR-2019| -- END

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Dim eLMode As Integer
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    eLMode = enLogin_Mode.MGR_SELF_SERVICE
                Else
                    eLMode = enLogin_Mode.EMP_SELF_SERVICE
                End If
                'Gajanan [17-April-2019] -- End

                Dim mdTran As New DataTable : Dim blnIsApproved As Boolean = False
                Dim dtApprTran As New DataTable
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmployee.SelectedValue)
                blnIsApproved = objEmp._Isapproved

                Dim dtIdRow As DataRow = Nothing

                objEmpIdentity._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
                mdTran = objEmpIdentity._DataList

                If IdentityApprovalFlowVal Is Nothing AndAlso blnIsApproved = True Then
                    objAIdInfoTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
                    dtApprTran = objAIdInfoTran._DataTable
                    dtIdRow = dtApprTran.NewRow()
                Else
                    dtIdRow = mdTran.NewRow()
                End If

                If chkMakeAsDefault.Checked Then
                    If mdTran.Select("isdefault = 'True' AND identitytranunkid <> " & CInt(Me.ViewState("Unkid"))).Length > 0 Then
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'msg.DisplayError(ex, Me)
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 37, "There can be only one default Identity. Please remove that if you want to add new default Identity."), Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                End If

                If CInt(Me.ViewState("Unkid")) <= 0 Then
                    If IdentityApprovalFlowVal Is Nothing AndAlso blnIsApproved = True Then
                        dtIdRow.Item("tranguid") = Guid.NewGuid.ToString()
                        dtIdRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
                        dtIdRow.Item("mappingunkid") = 0
                        dtIdRow.Item("approvalremark") = ""
                        dtIdRow.Item("isfinal") = False
                        dtIdRow.Item("statusunkid") = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval

                        'Gajanan [19-NOV-2019] -- Start
                        'dtIdRow.Item("loginemployeeunkid") = 0
                        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                            dtIdRow.Item("loginemployeeunkid") = CInt(Session("Employeeunkid"))
                        Else
                            dtIdRow.Item("loginemployeeunkid") = 0
                        End If
                        'Gajanan [19-NOV-2019] -- End

                        dtIdRow.Item("isvoid") = False
                        dtIdRow.Item("voidreason") = ""
                        dtIdRow.Item("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                        dtIdRow.Item("audittype") = enAuditType.ADD
                        dtIdRow.Item("audituserunkid") = Session("UserId")
                        dtIdRow.Item("ip") = CStr(Session("IP_ADD"))
                        dtIdRow.Item("host") = CStr(Session("HOST_NAME"))
                        dtIdRow.Item("form_name") = mstrModuleName
                        dtIdRow.Item("isweb") = True
                        dtIdRow.Item("isprocessed") = False
                        dtIdRow.Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.ADDED
                        dtIdRow.Item("operationtype") = Language.getMessage("clsEmployeeDataApproval", 62, "Newly Added Information")
                    End If
                    dtIdRow.Item("identitytranunkid") = -1
                    dtIdRow.Item("employeeunkid") = cboEmployee.SelectedValue
                    dtIdRow.Item("idtypeunkid") = CInt(cboIdType.SelectedValue)
                    dtIdRow.Item("identity_no") = txtIdNo.Text
                    dtIdRow.Item("countryunkid") = CInt(cboCountry.SelectedValue)
                    dtIdRow.Item("serial_no") = txtIdSrNo.Text
                    dtIdRow.Item("issued_place") = txtIssuePlace.Text
                    dtIdRow.Item("dl_class") = txtDLClass.Text
                    If dtpIssueDate.IsNull = False Then
                        dtIdRow.Item("issue_date") = dtpIssueDate.GetDate.Date
                    Else
                        dtIdRow.Item("issue_date") = DBNull.Value
                    End If
                    If dtpExpiryDate.IsNull = False Then
                        dtIdRow.Item("expiry_date") = dtpExpiryDate.GetDate.Date
                    Else
                        dtIdRow.Item("expiry_date") = DBNull.Value
                    End If
                    dtIdRow.Item("isdefault") = chkMakeAsDefault.Checked
                    dtIdRow.Item("GUID") = Guid.NewGuid().ToString
                    dtIdRow.Item("AUD") = "A"
                    If IdentityApprovalFlowVal Is Nothing AndAlso blnIsApproved = True Then
                        dtApprTran.Rows.Add(dtIdRow)
                    Else
                        mdTran.Rows.Add(dtIdRow)
                    End If
                Else
                    Dim dtEIdRow() As DataRow = mdTran.Select("identitytranunkid = '" & Me.ViewState("Unkid").ToString & "'")
                    If dtEIdRow.Length <= 0 Then Exit Sub
                    If IdentityApprovalFlowVal Is Nothing AndAlso blnIsApproved = True Then
                        dtIdRow = dtApprTran.NewRow()

                        dtIdRow.Item("tranguid") = Guid.NewGuid.ToString()
                        dtIdRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
                        dtIdRow.Item("mappingunkid") = 0
                        dtIdRow.Item("approvalremark") = ""
                        dtIdRow.Item("isfinal") = False
                        dtIdRow.Item("statusunkid") = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        'Gajanan [19-NOV-2019] -- Start
                        'dtIdRow.Item("loginemployeeunkid") = 0
                        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                            dtIdRow.Item("loginemployeeunkid") = CInt(Session("Employeeunkid"))
                        Else
                            dtIdRow.Item("loginemployeeunkid") = 0
                        End If
                        'Gajanan [19-NOV-2019] -- End
                        dtIdRow.Item("isvoid") = False
                        dtIdRow.Item("voidreason") = ""
                        dtIdRow.Item("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                        dtIdRow.Item("audittype") = enAuditType.ADD
                        dtIdRow.Item("audituserunkid") = Session("UserId")
                        dtIdRow.Item("ip") = CStr(Session("IP_ADD"))
                        dtIdRow.Item("host") = CStr(Session("HOST_NAME"))
                        dtIdRow.Item("form_name") = mstrModuleName
                        dtIdRow.Item("isweb") = True
                        dtIdRow.Item("isprocessed") = False
                        dtIdRow.Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.EDITED
                        dtIdRow.Item("operationtype") = Language.getMessage("clsEmployeeDataApproval", 62, "Newly Added Information")
                        dtIdRow.Item("identitytranunkid") = Me.ViewState("Unkid")
                        dtIdRow.Item("employeeunkid") = cboEmployee.SelectedValue
                        dtIdRow.Item("idtypeunkid") = CInt(cboIdType.SelectedValue)
                        dtIdRow.Item("identity_no") = txtIdNo.Text
                        dtIdRow.Item("countryunkid") = CInt(cboCountry.SelectedValue)
                        dtIdRow.Item("serial_no") = txtIdSrNo.Text
                        dtIdRow.Item("issued_place") = txtIssuePlace.Text
                        dtIdRow.Item("dl_class") = txtDLClass.Text
                        If dtpIssueDate.IsNull = False Then
                            dtIdRow.Item("issue_date") = dtpIssueDate.GetDate.Date
                        Else
                            dtIdRow.Item("issue_date") = DBNull.Value
                        End If
                        If dtpExpiryDate.IsNull = False Then
                            dtIdRow.Item("expiry_date") = dtpExpiryDate.GetDate.Date
                        Else
                            dtIdRow.Item("expiry_date") = DBNull.Value
                        End If
                        dtIdRow.Item("isdefault") = chkMakeAsDefault.Checked
                        dtIdRow.Item("AUD") = "A"

                        dtApprTran.Rows.Add(dtIdRow)
                    Else
                        dtEIdRow(0).Item("identitytranunkid") = Me.ViewState("Unkid")
                        dtEIdRow(0).Item("employeeunkid") = cboEmployee.SelectedValue
                        dtEIdRow(0).Item("idtypeunkid") = CInt(cboIdType.SelectedValue)
                        dtEIdRow(0).Item("identity_no") = txtIdNo.Text
                        dtEIdRow(0).Item("countryunkid") = CInt(cboCountry.SelectedValue)
                        dtEIdRow(0).Item("serial_no") = txtIdSrNo.Text
                        dtEIdRow(0).Item("issued_place") = txtIssuePlace.Text
                        dtEIdRow(0).Item("dl_class") = txtDLClass.Text
                        If dtpIssueDate.IsNull = False Then
                            dtEIdRow(0).Item("issue_date") = dtpIssueDate.GetDate.Date
                        Else
                            dtEIdRow(0).Item("issue_date") = DBNull.Value
                        End If
                        If dtpExpiryDate.IsNull = False Then
                            dtEIdRow(0).Item("expiry_date") = dtpExpiryDate.GetDate.Date
                        Else
                            dtEIdRow(0).Item("expiry_date") = DBNull.Value
                        End If
                        dtEIdRow(0).Item("isdefault") = chkMakeAsDefault.Checked
                        dtEIdRow(0).Item("GUID") = Guid.NewGuid().ToString
                        dtEIdRow(0).Item("AUD") = "U"
                    End If
                End If

                If IdentityApprovalFlowVal Is Nothing AndAlso blnIsApproved = True Then

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    If objApprovalData.IsApproverPresent(enScreenName.frmIdentityInfoList, CStr(Session("Database_Name")), _
                                                          CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                          CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeIdentities), _
                                                          CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(cboEmployee.SelectedValue.ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                        msg.DisplayMessage(objApprovalData._Message, Me)
                        Exit Sub
                    End If
                    'Gajanan [17-April-2019] -- End


                    'S.SANDEEP |26-APR-2019| -- START
                    'If objAIdInfoTran.InsertUpdateDelete_IdentityTran(dtApprTran, Nothing, CInt(Session("UserId")), CInt(Session("CompanyUnkId").ToString()), mdtAttachment) Then
                    Dim oData As DataTable = Nothing
                    If mdtAttachment IsNot Nothing Then
                        oData = New DataView(mdtAttachment, "isinapproval=true", "", DataViewRowState.CurrentRows).ToTable()
                        Dim strPGuid As String = String.Empty
                        strPGuid = dtApprTran.Rows(0)("GUID").ToString()
                        For Each row As DataRow In oData.Rows
                            row("PGUID") = strPGuid
                        Next
                        oData.AcceptChanges()
                    End If
                    If objAIdInfoTran.InsertUpdateDelete_IdentityTran(dtApprTran, Nothing, CInt(Session("UserId")), CInt(Session("CompanyUnkId").ToString()), oData) Then
                        'S.SANDEEP |26-APR-2019| -- END

                        If IsNothing(dtApprTran) = False AndAlso dtApprTran.Select("AUD <> ''").Length > 0 Then
                            objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                         CStr(Session("UserAccessModeSetting")), _
                                                         CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                         CInt(enUserPriviledge.AllowToApproveRejectEmployeeIdentities), _
                                                         enScreenName.frmIdentityInfoList, CStr(Session("EmployeeAsOnDate")), _
                                                         CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                         Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.ADDED, , cboEmployee.SelectedValue.ToString(), , , _
                                                         "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " ", dtApprTran, False, clsEmployeeAddress_approval_tran.enAddressType.NONE, "")
                        End If


                        If CInt(Me.ViewState("Unkid")) <= 0 Then
                            Call BlankObjects()
                            'Hemant (31 May 2019) -- Start
                            'ISSUE/ENHANCEMENT : UAT Changes
                            'msg.DisplayMessage("Identity Successfully Inserted.", Me)
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 2004, "Identity Successfully Inserted."), Me)
                            'Hemant (31 May 2019) -- End
                        Else
                            'Hemant (31 May 2019) -- Start
                            'ISSUE/ENHANCEMENT : UAT Changes
                            'msg.DisplayMessage("Identity Successfully Inserted.", Me)

                            'Gajanan [1-NOV-2019] -- Start    
                            'Enhancement:Worked On NMB ESS Comment  


                            'msg.DisplayMessage(Language.getMessage(mstrModuleName, 2004, "Identity Successfully Inserted."), Me)
                            ''Hemant (31 May 2019) -- End
                            'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpIdentities_List.aspx")
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 2004, "Identity Successfully Inserted."), Me, Session("rootpath").ToString & "HR/wPgEmpIdentities_List.aspx")
                            'Gajanan [1-NOV-2019] -- End

                        End If
                    Else
                        msg.DisplayMessage("Problem in updating Identity.", Me)
                    End If
                Else
                    'S.SANDEEP |26-APR-2019| -- START
                    Dim oData As New DataTable
                    If mdtAttachment IsNot Nothing Then
                        oData = New DataView(mdtAttachment, "isinapproval=false", "", DataViewRowState.CurrentRows).ToTable()
                        Dim strPGuid As String = String.Empty
                        If dtApprTran IsNot Nothing AndAlso dtApprTran.Rows.Count > 0 Then
                        strPGuid = dtApprTran.Rows(0)("GUID").ToString()
                        For Each row As DataRow In oData.Rows
                            row("PGUID") = strPGuid
                        Next
                        oData.AcceptChanges()
                        End If                        
                    End If
                    'S.SANDEEP |26-APR-2019| -- END

                    If CInt(Me.ViewState("Unkid")) <= 0 Then
                        objEmpIdentity._DataList = mdTran
                        'S.SANDEEP |26-APR-2019| -- START
                        If objEmpIdentity.InsertUpdateDelete_IdentityTran(Nothing, CInt(Session("UserId")), oData) = False Then
                            'If objEmpIdentity.InsertUpdateDelete_IdentityTran(Nothing, CInt(Session("UserId"))) = False Then
                            'S.SANDEEP |26-APR-2019| -- END
                            msg.DisplayMessage("Problem in Inserting Identity.", Me)
                        Else
                            Call BlankObjects()
                            'Hemant (31 May 2019) -- Start
                            'ISSUE/ENHANCEMENT : UAT Changes
                            'msg.DisplayMessage("Identity Successfully Inserted.", Me)
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 2004, "Identity Successfully Inserted."), Me)
                            'Hemant (31 May 2019) -- End
                        End If
                    Else
                        objEmpIdentity._DataList = mdTran
                        'S.SANDEEP |26-APR-2019| -- START
                        If objEmpIdentity.InsertUpdateDelete_IdentityTran(Nothing, CInt(Session("UserId")), oData) = False Then
                            'If objEmpIdentity.InsertUpdateDelete_IdentityTran(Nothing, CInt(Session("UserId"))) = False Then
                            'S.SANDEEP |26-APR-2019| -- END
                            msg.DisplayMessage("Problem in updating Identity.", Me)
                        Else
                            'Hemant (31 May 2019) -- Start
                            'ISSUE/ENHANCEMENT : UAT Changes
                            'msg.DisplayMessage("Identity Successfully Updated.", Me)

                            'Gajanan [1-NOV-2019] -- Start    
                            'Enhancement:Worked On NMB ESS Comment  
                            'msg.DisplayMessage(Language.getMessage(mstrModuleName, 2005, "Identity Successfully Updated."), Me)
                            ''Hemant (31 May 2019) -- End
                            'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpIdentities_List.aspx")
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 2005, "Identity Successfully Updated."), Me, Session("rootpath").ToString & "HR/wPgEmpIdentities_List.aspx")
                            'Gajanan [1-NOV-2019] -- End

                        End If
                    End If
                End If
            End If

            If isChange = False Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 2004, "Identity Successfully Inserted."), Me, Session("rootpath").ToString & "HR/wPgEmpIdentities_List.aspx")
            End If
            'Gajanan [22-Feb-2019] -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Nilay (02-Mar-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click, Closebutton1.CloseButton_click
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        'Nilay (02-Mar-2015) -- End
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            '        Response.Redirect("~\UserHome.aspx", False)
            Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpIdentities_List.aspx", False)
            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnClose_Click Event : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub
    'SHANI [01 FEB 2015]--END


    'S.SANDEEP |26-APR-2019| -- START
    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                'mdtAttachement = CType(Me.ViewState("FieldAttachement"), DataTable)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    msg.DisplayMessage(Language.getMessage("frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    popup_ScanAttchment.Show()
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                AddDocumentAttachment(f, f.FullName)
                popup_ScanAttchment.Show()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_AttachementYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonNo_Click
        Try
            If mintDeleteIndex > 0 Then mintDeleteIndex = 0
            popup_ScanAttchment.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_AttachementYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonYes_Click
        Try
            If mintDeleteIndex >= 0 Then
                mdtAttachment.Rows(mintDeleteIndex)("AUD") = "D"
                mdtAttachment.AcceptChanges()
                mintDeleteIndex = 0
                Call FillAttachment()
                popup_ScanAttchment.Show()
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |26-APR-2019| -- END

    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgv_Attchment.Items.Count <= 0 Then
                msg.DisplayMessage("No Files to download.", Me)
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & cboEmployee.SelectedItem.Text.Replace(" ", "") + ".zip", mdtAttachment, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

#End Region

#Region "ToolBar Event"

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        Response.Redirect("~\HR\wPgEmpIdentities_List.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region

    'S.SANDEEP |26-APR-2019| -- START
#Region " Link Event(s) "

    Protected Sub lnkScanAttachPersonal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkScanAttachPersonal.Click
        Try
            mintAddressTypeId = 0
            If CInt(cboIdType.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 39, "Id Type is compulsory information, it can not be blank. Please select a Id Type to continue."), Me)
                Exit Sub
            End If

            objScanAttachment.GetList(Session("Document_Path").ToString(), "List", "", CInt(cboEmployee.SelectedValue))
            If mdtAttachment Is Nothing Then
                mdtAttachment = objScanAttachment._Datatable
            End If

            If mdtAttachment IsNot Nothing Then
                mintAddressTypeId = CInt(enScanAttactRefId.IDENTITYS)
            End If
            mblnAttachmentPopup = True
            Call FillAttachment()
            popup_ScanAttchment.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " GridViewEvent "

    Protected Sub dgv_Attchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgv_Attchment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow = Nothing

                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                'If CInt(e.Item.Cells(3).Text) > 0 Then
                '    xrow = mdtAttachment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(3).Text) & "")
                'Else
                '    xrow = mdtAttachment.Select("GUID = '" & e.Item.Cells(2).Text & "'")
                'End If
                'If e.CommandName = "Delete" Then
                '    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                '        popup_AttachementYesNo.Message = "Are you sure you want to delete this attachment?"
                '        mintDeleteIndex = mdtAttachment.Rows.IndexOf(xrow(0))
                '        popup_AttachementYesNo.Show()
                '    End If
                'End If
                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtAttachment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtAttachment.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhGUID", False, True)).Text & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        popup_AttachementYesNo.Message = "Are you sure you want to delete this attachment?"
                        mintDeleteIndex = mdtAttachment.Rows.IndexOf(xrow(0))
                        popup_AttachementYesNo.Show()
                    End If
                ElseIf e.CommandName = "Download" Then
                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        Dim xPath As String = ""

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            Try
                                xPath = Server.MapPath(xPath)
                            Catch ex As Exception
                                msg.DisplayMessage("File does not Exist...", Me)
                                Exit Sub
                            End Try
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("orgfilepath").ToString
                        End If

                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                msg.DisplayMessage("File does not Exist...", Me)
                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & xrow(0)("filename").ToString() & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
            End If
            'S.SANDEEP |16-MAY-2019| -- END
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'S.SANDEEP |26-APR-2019| -- END


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption("gbIDInformation", Me.Title)
        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebutton1.PageHeading = Language._Object.getCaption("gbIDInformation", Me.Closebutton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption("gbIDInformation", Me.lblPageHeader.Text)
        'SHANI [01 FEB 2015]--END 
        Me.lblDLClass.Text = Language._Object.getCaption(Me.lblDLClass.ID, Me.lblDLClass.Text)
        Me.lblIdPlaceOfIssue.Text = Language._Object.getCaption(Me.lblIdPlaceOfIssue.ID, Me.lblIdPlaceOfIssue.Text)
        Me.lblIdExpiryDate.Text = Language._Object.getCaption(Me.lblIdExpiryDate.ID, Me.lblIdExpiryDate.Text)
        Me.lblIdIssueDate.Text = Language._Object.getCaption(Me.lblIdIssueDate.ID, Me.lblIdIssueDate.Text)
        Me.lblIdIssueCountry.Text = Language._Object.getCaption(Me.lblIdIssueCountry.ID, Me.lblIdIssueCountry.Text)
        Me.lblIdentityNo.Text = Language._Object.getCaption(Me.lblIdentityNo.ID, Me.lblIdentityNo.Text)
        Me.lblIdSerialNo.Text = Language._Object.getCaption(Me.lblIdSerialNo.ID, Me.lblIdSerialNo.Text)
        Me.lblIdType.Text = Language._Object.getCaption(Me.lblIdType.ID, Me.lblIdType.Text)
        Me.chkMakeAsDefault.Text = Language._Object.getCaption(Me.chkMakeAsDefault.ID, Me.chkMakeAsDefault.Text)

        Me.BtnSave.Text = Language._Object.getCaption("btnSaveInfo", Me.BtnSave.Text).Replace("&", "")
        Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")

        Language.setLanguage(mstrModuleName1)
        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End


    End Sub

    'Pinkal (06-May-2014) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 37, "There can be only one default Identity. Please remove that if you want to add new default Identity.")
            Language.setMessage(mstrModuleName, 38, "Expiry date cannot be less then or equal to Issue date.")
            Language.setMessage(mstrModuleName, 39, "Id Type is compulsory information, it can not be blank. Please select a Id Type to continue.")
            Language.setMessage("clsEmployeeDataApproval", 62, "Newly Added Information")
            Language.setMessage(mstrModuleName, 999, "Configuration Path does not Exist.")
            Language.setMessage(mstrModuleName, 2001, "Sorry, this Identity is already present for the selected employee. Please add new Identity to continue.")
            Language.setMessage(mstrModuleName, 2002, "Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2003, "Selected information is already present for particular employee.")
            Language.setMessage(mstrModuleName, 2004, "Identity Successfully Inserted.")
            Language.setMessage(mstrModuleName, 2005, "Identity Successfully Updated.")

        Catch Ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
