﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgEmployeeDates.aspx.vb" Inherits="wPgEmployeeDates" Title="Employee Dates" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="pnlDates" runat="server" Style="width: 50%">
            <asp:UpdatePanel ID="uppnl_main" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Dates"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblpnl_Header" runat="server" Text="Employee Date Detail"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 80%" colspan="3">
                                                <asp:TextBox ID="txtEmployee" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lblAppointDate" runat="server" Text="Appoint Date"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <uc2:DateCtrl ID="dtpAppointDate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lblConfirmationDate" runat="server" Text="Conf. Date"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <uc2:DateCtrl ID="dtpConfDate" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lblBirtdate" runat="server" Text="Birthdate"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <uc2:DateCtrl ID="dtpBirthdate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td align="left" style="width: 20%">
                                                &nbsp;
                                            </td>
                                            <td align="left" style="width: 30%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lblSuspendedFrom" runat="server" Text="Susp. From"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <uc2:DateCtrl ID="dtpSuspFrom" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lblSuspendedTo" runat="server" Text="Susp. To"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <uc2:DateCtrl ID="dtpSuspTo" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lblProbationFrom" runat="server" Text="Prob. From"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <uc2:DateCtrl ID="dtpProbFrom" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lblProbationTo" runat="server" Text="Prob. To"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <uc2:DateCtrl ID="dtpProbTo" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lblEmplDate" runat="server" Text="EOC Date"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <uc2:DateCtrl ID="dtpEmplEndDate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lblLeavingDate" runat="server" Text="Leaving Date"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <uc2:DateCtrl ID="dtpLeavingDate" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lblEmplReason" runat="server" Text="EOC Reason"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <asp:DropDownList ID="cboReason" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lblReinstatementDate" runat="server" Text="Reinstatement Date"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <uc2:DateCtrl ID="dtpReinstatement" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 20%">
                                                <asp:Label ID="lblRetirementDate" runat="server" Text="Retr. Date"></asp:Label>
                                            </td>
                                            <td align="left" style="width: 30%">
                                                <uc2:DateCtrl ID="dtpRetrDate" runat="server" AutoPostBack="false" />
                                            </td>
                                            <td align="left" style="width: 20%">
                                                &nbsp;
                                            </td>
                                            <td align="left" style="width: 30%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td align="left" style="width: 20%">
                                                &nbsp;
                                            </td>
                                            <td align="left" style="width: 30%">
                                                &nbsp;
                                            </td>
                                            <td align="left" style="width: 20%">
                                                &nbsp;
                                            </td>
                                            <td align="left" style="width: 30%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" Text="Update" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
