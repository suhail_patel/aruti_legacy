﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region

Partial Class HR_wPgEmpRefereeList
    Inherits Basepage


#Region " Private Variable(s) "

    Dim DisplayMessage As New CommonCodes
    Dim strSearching As String = ""


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeRefereeList"
    'Pinkal (06-May-2014) -- End

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objARefreeTran As New clsemployee_refree_approval_tran

    Dim Arr() As String
    Dim RefreeApprovalFlowVal As String

    Dim blnPendingEmployee As Boolean = False
    'Gajanan [17-DEC-2018] -- End

    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End
#End Region

#Region " Display Dialog "

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Try

            'S.SANDEEP [ 16 JAN 2014 ] -- START
            'If (Session("loginBy") = Global.User.en_loginby.Employee) Then
            '    drpEmployee.BindEmployee(CInt(Session("Employeeunkid")))
            'Else
            '    drpEmployee.BindEmployee()
            'End If
            Dim dsCombos As New DataSet
            Dim objEmployee As New clsEmployee_Master
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                'End If


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .




                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployeeRefereeList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If


                'dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                '                                    CInt(Session("UserId")), _
                '                                    CInt(Session("Fin_year")), _
                '                                    CInt(Session("CompanyUnkId")), _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                    CStr(Session("UserAccessModeSetting")), True, _
                '                                    CBool(Session("IsIncludeInactiveEmp")), "Employee", True)



                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                CStr(Session("UserAccessModeSetting")), _
                                           mblnOnlyApproved, CBool(Session("IsIncludeInactiveEmp")), "Employee", True, _
                                           0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)

                'Shani(24-Aug-2015) -- End

                'S.SANDEEP [20-JUN-2018] -- End


                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombos.Tables("Employee")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()
            End If
            'S.SANDEEP [ 16 JAN 2014 ] -- END

            

            Dim objCountry As New clsMasterData
            drpCountry.DataSource = objCountry.getCountryList("Country", True)
            drpCountry.DataValueField = "countryunkid"
            drpCountry.DataTextField = "country_name"
            drpCountry.DataBind()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    Public Sub GetData()
        Dim strSearching As String = ""
        Try
            Dim objEmpReferee As New clsEmployee_Refree_tran
            Dim dtEmpReferee As DataTable = Nothing
            Dim dsEmpReferee As DataSet = Nothing



            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("ViewEmpReferenceList")) = False Then Exit Sub
            End If

            'Pinkal (22-Nov-2012) -- End



            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsEmpReferee = objEmpReferee.GetList("Referee", , Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"))


            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.

            If CInt(drpEmployee.SelectedValue) > 0 Then
                strSearching += "AND hremployee_master.employeeunkid = " & CInt(drpEmployee.SelectedValue) & " "
            End If

            If CInt(drpCountry.SelectedValue) > 0 Then
                strSearching += "AND hrmsConfiguration..cfcountry_master.countryunkid = " & CInt(drpCountry.SelectedValue) & " "
            End If

            If TxtReferee.Text.Trim.Length > 0 Then
                strSearching += "AND ISNULL(hremployee_referee_tran.name,'') LIKE '%" & TxtReferee.Text.Trim & "%' "
            End If

            If txtCompany.Text.Trim.Length > 0 Then
                strSearching += "AND ISNULL(hremployee_referee_tran.identify_no,'') LIKE '%" & txtCompany.Text.Trim & "%' "
            End If


            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If


            'dsEmpReferee = objEmpReferee.GetList(Session("Database_Name"), _
            '                                     Session("UserId"), _
            '                                     Session("Fin_year"), _
            '                                     Session("CompanyUnkId"), _
            '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                     Session("UserAccessModeSetting"), True, _
            '                                     Session("IsIncludeInactiveEmp"), "Referee")


            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .



            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployeeRefereeList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If



            'dsEmpReferee = objEmpReferee.GetList(CStr(Session("Database_Name")), _
            '                                    CInt(Session("UserId")), _
            '                                    CInt(Session("Fin_year")), _
            '                                    CInt(Session("CompanyUnkId")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                    CStr(Session("UserAccessModeSetting")), True, _
            '                                    CBool(Session("IsIncludeInactiveEmp")), "Referee", False, True, strSearching, CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim mblnisusedmss As Boolean = False
            IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, mblnisusedmss = True, mblnisusedmss = False)
            'Gajanan [17-DEC-2018] -- End

            dsEmpReferee = objEmpReferee.GetList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                        CStr(Session("UserAccessModeSetting")), mblnOnlyApproved, _
                                        CBool(Session("IsIncludeInactiveEmp")), "Referee", , , strSearching, mblnisusedmss, mblnAddApprovalCondition) 'Gajanan [17-DEC-2018] -- Add isusedmss


            'Shani(20-Nov-2015) -- End
            'S.SANDEEP [20-JUN-2018] -- End


            'If strSearching.Length > 0 Then
            '    strSearching = strSearching.Substring(3)
            '    dtEmpReferee = New DataView(dsEmpReferee.Tables("Referee"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtEmpReferee = dsEmpReferee.Tables("Referee")
            'End If



            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim dcol As New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "tranguid"
                .DefaultValue = ""
            End With
            dsEmpReferee.Tables(0).Columns.Add(dcol)

            If RefreeApprovalFlowVal Is Nothing Then
                Dim dsPending As New DataSet

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                strSearching = strSearching.Replace("hremployee_referee_tran", "hremployee_referee_approval_tran")
                'Gajanan [17-April-2019] -- End

                dsPending = objARefreeTran.GetList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), mblnOnlyApproved, _
                                          CBool(Session("IsIncludeInactiveEmp")), "Emp", , , strSearching, mblnisusedmss, mblnAddApprovalCondition) 'Gajanan [17-DEC-2018] -- Add isusedmss

                If dsPending.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In dsPending.Tables(0).Rows
                        dsEmpReferee.Tables(0).ImportRow(row)
                    Next
                End If
            End If
            'Gajanan [17-DEC-2018] -- End




            dtEmpReferee = New DataView(dsEmpReferee.Tables("Referee"), "", "", DataViewRowState.CurrentRows).ToTable
            'Pinkal (28-Dec-2015) -- End

            If (Not dtEmpReferee Is Nothing) Then
                gvReferee.DataSource = dtEmpReferee
                gvReferee.DataKeyField = "RefreeTranId"
                gvReferee.DataBind()
            Else
                DisplayMessage.DisplayMessage("GetData :-  ", Me)
                Exit Sub
            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                gvReferee.CurrentPageIndex = 0
                gvReferee.DataBind()
            Else
                Throw ex
                DisplayMessage.DisplayError(ex, Me)
            End If
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmEmployeeRefereeList"
            StrModuleName2 = "mnuPersonnel"
            StrModuleName3 = "mnuEmployeeData"
            clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))

            'Pinkal (24-Aug-2012) -- Start
            'Enhancement : TRA Changes

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            'Pinkal (24-Aug-2012) -- End


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'lblPendingData.Visible = False
            'btnApprovalinfo.Visible = False
            objtblPanel.Visible = False

            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            RefreeApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmEmployeeRefereeList)))
            'Gajanan [17-DEC-2018] -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END
            If Not IsPostBack Then
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then


                    'Anjan (30 May 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'BtnNew.Visible = False
                    BtnNew.Visible = CBool(Session("AddEmployeeReferee"))
                    'Anjan (30 May 2012)-End 



                    'S.SANDEEP [20-JUN-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow For NMB .
                    'S.SANDEEP [ 16 JAN 2014 ] -- START
                    'chkShowPending.Visible = CBool(Session("AllowToApproveEmployee"))
                    'If Session("ShowPending") IsNot Nothing Then
                    '    chkShowPending.Checked = True
                    'End If
                    'S.SANDEEP [ 16 JAN 2014 ] -- END
                    'S.SANDEEP [20-JUN-2018] -- End

                Else
                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'BtnNew.Visible = False


                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'BtnNew.Visible = ConfigParameter._Object._AllowAddReference
                    BtnNew.Visible = CBool(Session("AllowAddReference"))
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                    'Anjan (02 Mar 2012)-End 


                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.

                    ''S.SANDEEP [ 16 JAN 2014 ] -- START
                    'chkShowPending.Visible = False
                    ''S.SANDEEP [ 16 JAN 2014 ] -- END

                    'Gajanan [17-DEC-2018] -- End

                End If

                'Pinkal (22-Mar-2012) -- Start
                'Enhancement : TRA Changes

                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
                'Nilay (01-Feb-2015) -- End

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    gvReferee.Columns(2).Visible = True
                Else
                    gvReferee.Columns(2).Visible = False
                End If



                'Pinkal (22-Mar-2012) -- End


                FillCombo()


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .

                ''S.SANDEEP [ 16 JAN 2014 ] -- START
                'If Session("ShowPending") IsNot Nothing Then
                '    Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                'End If
                ''S.SANDEEP [ 16 JAN 2014 ] -- END
                'S.SANDEEP [20-JUN-2018] -- End


                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("RefreeTran_EmpUnkID") IsNot Nothing Then
                    drpEmployee.SelectedValue = CStr(CInt(Session("RefreeTran_EmpUnkID")))
                    If CInt(drpEmployee.SelectedValue) > 0 Then
                        Call BtnSearch_Click(BtnSearch, Nothing)
                    End If
                    Session.Remove("RefreeTran_EmpUnkID")
                End If
                'SHANI [09 Mar 2015]--END 
            Else
                blnPendingEmployee = CBool(ViewState("blnPendingEmployee"))
            End If
            If blnPendingEmployee Then
                objtblPanel.Visible = blnPendingEmployee
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'Nilay (01-Feb-2015) -- End
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session("IsFrom_AddEdit") = False
    End Sub

    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Try
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleSaveButton(False)
            'ToolbarEntry1.VisibleDeleteButton(False)
            'ToolbarEntry1.VisibleCancelButton(False)
            'ToolbarEntry1.VisibleExitButton(False)
            'Nilay (01-Feb-2015) -- End
            Me.ViewState.Add("blnPendingEmployee", blnPendingEmployee)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Gajanan [17-DEC-2018] -- End

    End Sub
    'Pinkal (22-Mar-2012) -- End

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "
    'Gajanan [27-May-2019] -- Start              
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If IsNothing(Session("IsfromRecruitment")) = False AndAlso CBool(Session("IsfromRecruitment")) = True Then
                Response.Redirect(Session("rootpath").ToString & "Recruitment/wPgEmployeeUpdate.aspx", False)
                Session.Remove("IsfromRecruitment")
            Else
            Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Gajanan [27-May-2019] -- End

    Protected Sub BtnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNew.Click
        Try
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            Session("RefreeTranId") = Nothing
            'Sohail (02 May 2012) -- End

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            Session("RefreeTran_EmpUnkID") = drpEmployee.SelectedValue
            'SHANI [09 Mar 2015]--END 

            Response.Redirect(CStr(Session("servername")) & "~/HR/wPgEmpReferee.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDelete.Click
    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'Nilay (01-Feb-2015) -- End
        Try
            If (popup_DeleteReason.Reason.Trim = "") Then 'Nilay (01-Feb-2015) -- If (txtreasondel.Text = "")
                DisplayMessage.DisplayMessage("Please enter delete reason.", Me)
                Exit Sub
            End If

            Dim clsExperience As New clsEmployee_Refree_tran

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            clsExperience._Refereetranunkid = CInt(Me.ViewState("RefreeTranId"))
            'Gajanan [17-April-2019] -- End


            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Aruti.Data.User._Object._Userunkid = -1
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
            Else
                clsExperience._Userunkid = CInt(Session("UserId"))
            End If


            With clsExperience
                ._Isvoid = True
                ._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popup_DeleteReason.Reason.Trim 'Nilay (01-Feb-2015) -- txtreasondel.Text

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    ._VoidLoginEmployeeunkid = CInt(Session("Employeeunkid"))
                Else
                    ._Voiduserunkid = CInt(Session("UserId"))
                End If


                If RefreeApprovalFlowVal Is Nothing Then
                    objARefreeTran._Isvoid = True
                    objARefreeTran._Audituserunkid = CInt(Session("UserId"))
                    objARefreeTran._Isweb = False
                    objARefreeTran._Ip = getIP()
                    objARefreeTran._Host = getHostName()
                    objARefreeTran._Form_Name = mstrModuleName

                    If objARefreeTran.Delete(CInt(Me.ViewState("RefreeTranId")), popup_DeleteReason.Reason.Trim, CInt(Session("CompanyUnkId")), Nothing) = False Then
                        If objARefreeTran._Message <> "" Then
                            DisplayMessage.DisplayMessage("You Cannot Delete this Entry." & objARefreeTran._Message, Me)
                        End If
                        Exit Sub
                    End If
                Else
                    .Delete(CInt(Me.ViewState("RefreeTranId")))
                End If

                If (._Message.Length > 0) Then
                    DisplayMessage.DisplayMessage("You Cannot Delete this Entry." & ._Message, Me)
                Else

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.


                    Dim eLMode As Integer
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        eLMode = enLogin_Mode.MGR_SELF_SERVICE
                    Else
                        eLMode = enLogin_Mode.EMP_SELF_SERVICE
                    End If


                    objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                                 CStr(Session("UserAccessModeSetting")), _
                                                                 CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                                 CInt(enUserPriviledge.AllowToApproveRejectEmployeeReferences), _
                                                                 enScreenName.frmEmployeeRefereeList, CStr(Session("EmployeeAsOnDate")), _
                                                                 CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                                 Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.ADDED, , clsExperience._Employeeunkid.ToString(), , , _
                                                            "refereetranunkid= " & ViewState("RefreeTranId").ToString() & " ", Nothing, False, clsEmployeeAddress_approval_tran.enAddressType.NONE, _
                                                            "refereetranunkid= " & ViewState("RefreeTranId").ToString() & " ", Nothing)
                    'Gajanan [17-April-2019] -- End

                    DisplayMessage.DisplayMessage("Entry Successfully deleted." & ._Message, Me)
                    Me.ViewState("RefreeTranId") = Nothing
                    GetData()
                End If
                popup_DeleteReason.Dispose() 'Nilay (01-Feb-2015) -- popup1.Dispose()
                popup_DeleteReason.Reason = "" 'Nilay (01-Feb-2015) -- txtreasondel.Text = "" 
                GetData()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(drpEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If
            'Sohail (02 May 2012) -- End
            GetData()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            drpEmployee.SelectedIndex = 0
            If drpCountry.Items.Count > 0 Then drpCountry.SelectedIndex = 0
            TxtReferee.Text = ""
            txtCompany.Text = ""


            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            'GetData()
            gvReferee.DataSource = Nothing
            gvReferee.DataBind()
            gvReferee.CurrentPageIndex = 0

            'Pinkal (22-Nov-2012) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.

    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnApprovalinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovalinfo.Click
        Try
            Popup_Viewreport._UserId = CInt(Session("UserId"))
            Popup_Viewreport._Priority = 0
            Popup_Viewreport._PrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeReferences
            Popup_Viewreport._FillType = enScreenName.frmEmployeeRefereeList
            Popup_Viewreport._FilterString = "EM.employeeunkid = " & CInt(drpEmployee.SelectedValue)
            Popup_Viewreport._FromApprovalScreen = False
            Popup_Viewreport._OprationType = clsEmployeeDataApproval.enOperationType.NONE
            Popup_Viewreport.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnApprovalinfo_Click Event : " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region " Control's Event(s) "

#Region "GridView Event"

    Protected Sub gvReferee_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gvReferee.ItemCommand
        Dim ds As DataSet = Nothing
        Try
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If gvReferee.Items.Count > 0 Then
                If gvReferee.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Color.LightCoral).Count() > 0 Then
                    Dim item = gvReferee.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Color.LightCoral).FirstOrDefault()
                    If item IsNot Nothing Then item.BackColor = Color.White
                End If
            End If
            'Gajanan [17-DEC-2018] -- End

            If e.CommandName = "Select" Then
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'Session.Add("RefreeTranId", gvReferee.DataKeys(e.Item.ItemIndex))
                ''SHANI [09 Mar 2015]-START
                ''Enhancement - REDESIGN SELF SERVICE.
                'Session("RefreeTran_EmpUnkID") = drpEmployee.SelectedValue
                ''SHANI [09 Mar 2015]--END 
                'Response.Redirect(CStr(Session("servername")) & "~/HR/wPgEmpReferee.aspx", False)
                If RefreeApprovalFlowVal Is Nothing Then
                    Dim item = gvReferee.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(11).Text = e.Item.Cells(11).Text And x.Cells(9).Text.Trim <> "&nbsp;").FirstOrDefault()
                    If item IsNot Nothing Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process."), Me)
                        Exit Sub
                    Else
                        Session.Add("RefreeTranId", gvReferee.DataKeys(e.Item.ItemIndex))
                        Session("RefreeTran_EmpUnkID") = drpEmployee.SelectedValue
                        Response.Redirect(CStr(Session("servername")) & "~/HR/wPgEmpReferee.aspx", False)
                    End If
                Else
                    Session.Add("RefreeTranId", gvReferee.DataKeys(e.Item.ItemIndex))
                    Session("RefreeTran_EmpUnkID") = drpEmployee.SelectedValue
                    Response.Redirect(CStr(Session("servername")) & "~/HR/wPgEmpReferee.aspx", False)
                End If
                'Gajanan [17-DEC-2018] -- End

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
            ElseIf e.CommandName.ToUpper() = "VIEW" Then
                Dim item = gvReferee.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(11).Text = e.Item.Cells(11).Text And x.Cells(9).Text.Trim = "&nbsp;").FirstOrDefault()
                If item IsNot Nothing Then
                    item.BackColor = Color.LightCoral
                    item.ForeColor = Color.Black
                End If
                'Gajanan [17-DEC-2018] -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvReferee_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gvReferee.DeleteCommand
        Try
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Me.ViewState.Add("RefreeTranId", CInt(gvReferee.DataKeys(e.Item.ItemIndex)))
            'popup_DeleteReason.Show() 'Nilay (01-Feb-2015) -- popup1.Show()


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(gvReferee.Items(e.Item.ItemIndex).Cells(10).Text)

            'If RefreeApprovalFlowVal Is Nothing Then
            If RefreeApprovalFlowVal Is Nothing AndAlso objEmployee._Isapproved Then
                'Gajanan [17-April-2019] -- End

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If objApprovalData.IsApproverPresent(enScreenName.frmEmployeeRefereeList, CStr(Session("Database_Name")), _
                                                      CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                      CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                      CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(gvReferee.Items(e.Item.ItemIndex).Cells(10).Text), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    DisplayMessage.DisplayMessage(objApprovalData._Message, Me)
                    Exit Sub
                End If
                'Gajanan [17-April-2019] -- End

                Dim item = gvReferee.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(11).Text = e.Item.Cells(11).Text And x.Cells(9).Text.Trim <> "&nbsp;").FirstOrDefault()
                If item IsNot Nothing Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process."), Me)
                    Exit Sub
                Else
                    Me.ViewState.Add("RefreeTranId", CInt(gvReferee.DataKeys(e.Item.ItemIndex)))
                    popup_DeleteReason.Show()
                End If
            Else
                Me.ViewState.Add("RefreeTranId", CInt(gvReferee.DataKeys(e.Item.ItemIndex)))
                popup_DeleteReason.Show()
            End If
            'Gajanan [17-DEC-2018] -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvReferee_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gvReferee.PageIndexChanged
        Try
            gvReferee.CurrentPageIndex = e.NewPageIndex
            GetData()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Anjan (02 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Protected Sub gvReferee_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gvReferee.ItemDataBound
        Try
            If (e.Item.ItemIndex >= 0) Then
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                '    'e.Item.Cells(0).Visible = True : e.Item.Cells(1).Visible = True


                '    'Anjan (30 May 2012)-Start
                '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                '    'gvReferee.Columns(1).Visible = False 'Anjan (02 Mar 2012)

                '    'Pinkal (22-Nov-2012) -- Start
                '    'Enhancement : TRA Changes
                '    gvReferee.Columns(0).Visible = CBool(Session("EditEmployeeReferee"))
                '    gvReferee.Columns(1).Visible = CBool(Session("DeleteEmployeeReferee"))
                '    'Pinkal (22-Nov-2012) -- End
                '    'Anjan (30 May 2012)-End 


                '    'S.SANDEEP [20-JUN-2018] -- Start
                '    'Enhancement - Implementing Employee Approver Flow For NMB .



                '    ''S.SANDEEP [ 16 JAN 2014 ] -- START
                '    'If Session("ShowPending") IsNot Nothing Then
                '    '    gvReferee.Columns(1).Visible = False
                '    'End If
                '    ''S.SANDEEP [ 16 JAN 2014 ] -- END


                '    'S.SANDEEP [20-JUN-2018] -- End

                'Else
                '    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'gvReferee.Columns(1).Visible = ConfigParameter._Object._AllowDeleteReference

                '    'Pinkal (22-Nov-2012) -- Start
                '    'Enhancement : TRA Changes
                '    gvReferee.Columns(0).Visible = CBool(Session("AllowEditReference"))
                '    gvReferee.Columns(1).Visible = CBool(Session("AllowDeleteReference"))
                '    'Pinkal (22-Nov-2012) -- End


                '    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                'End If

                If RefreeApprovalFlowVal Is Nothing Then
                    Dim blnFlag As Boolean = False
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        blnFlag = CBool(Session("EditEmployeeReferee"))
                    Else
                        blnFlag = CBool(Session("AllowEditReference"))
                    End If
                    If blnFlag = False Then
                        CType(e.Item.Cells(1).FindControl("ImgSelect"), LinkButton).Visible = False
                    End If

                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        blnFlag = CBool(Session("DeleteEmployeeReferee"))
                    Else
                        blnFlag = CBool(Session("AllowDeleteReference"))
                    End If
                    If blnFlag = False Then
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    End If

                    If e.Item.Cells(9).Text <> "&nbsp;" AndAlso e.Item.Cells(9).Text.Length > 0 Then
                        e.Item.BackColor = Color.PowderBlue
                        e.Item.ForeColor = Color.Black

                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'lblPendingData.Visible = True
                        'btnApprovalinfo.Visible = True
                        objtblPanel.Visible = True
                        blnPendingEmployee = True
                        'Gajanan [17-DEC-2018] -- End

                        CType(e.Item.Cells(1).FindControl("ImgSelect"), LinkButton).Visible = False

                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = True
                        If CType(CType(e.Item.Cells(1).FindControl("hfoprationtypeid"), HiddenField).Value, clsEmployeeDataApproval.enOperationType) = clsEmployeeDataApproval.enOperationType.EDITED Then
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = True
                        Else
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = False
                        End If
                        'Gajanan [17-DEC-2018] -- End

                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    End If
                Else
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        gvReferee.Columns(0).Visible = CBool(Session("EditEmployeeReferee"))
                        gvReferee.Columns(1).Visible = CBool(Session("DeleteEmployeeReferee"))
                    Else
                        gvReferee.Columns(0).Visible = CBool(Session("AllowEditReference"))
                        gvReferee.Columns(1).Visible = CBool(Session("AllowDeleteReference"))
                    End If
                End If
                'Gajanan [17-DEC-2018] -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Anjan (02 Mar 2012)-End 

    'S.SANDEEP [20-JUN-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow For NMB .

    ''S.SANDEEP [ 16 JAN 2014 ] -- START
    'Protected Sub chkShowPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowPending.CheckedChanged
    '    Try
    '        Session("ShowPending") = chkShowPending.Checked
    '        Dim dsCombos As New DataSet
    '        Dim objEmp As New clsEmployee_Master
    '        If chkShowPending.Checked = True Then

    '            'Shani(24-Aug-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"), , , False)
    '            dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                                CInt(Session("UserId")), _
    '                                                CInt(Session("Fin_year")), _
    '                                                CInt(Session("CompanyUnkId")), _
    '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                                CStr(Session("UserAccessModeSetting")), False, _
    '                                                CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

    '            'Shani(24-Aug-2015) -- End
    '            Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
    '            With drpEmployee
    '                .DataValueField = "employeeunkid"
    '                'Nilay (09-Aug-2016) -- Start
    '                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
    '                '.DataTextField = "employeename"
    '                .DataTextField = "EmpCodeName"
    '                'Nilay (09-Aug-2016) -- End
    '                .DataSource = dtTab
    '                .DataBind()
    '                Try
    '                    .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
    '                Catch ex As Exception
    '                    .SelectedValue = CStr(0)
    '                End Try
    '            End With
    '            BtnNew.Visible = False
    '        Else
    '            Session("ShowPending") = Nothing
    '            BtnNew.Visible = True
    '            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

    '                'Shani(24-Aug-2015) -- Start
    '                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
    '                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                                    CInt(Session("UserId")), _
    '                                                    CInt(Session("Fin_year")), _
    '                                                    CInt(Session("CompanyUnkId")), _
    '                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                                    CStr(Session("UserAccessModeSetting")), True, _
    '                                                    CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

    '                'Shani(24-Aug-2015) -- End
    '                With drpEmployee
    '                    .DataValueField = "employeeunkid"
    '                    'Nilay (09-Aug-2016) -- Start
    '                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
    '                    '.DataTextField = "employeename"
    '                    .DataTextField = "EmpCodeName"
    '                    'Nilay (09-Aug-2016) -- End
    '                    .DataSource = dsCombos.Tables("Employee")
    '                    .DataBind()
    '                    Try
    '                        .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
    '                    Catch ex As Exception
    '                        .SelectedValue = CStr(0)
    '                    End Try
    '                End With
    '            Else
    '                Dim objglobalassess = New GlobalAccess
    '                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
    '                With drpEmployee
    '                    .DataSource = objglobalassess.ListOfEmployee
    '                    .DataTextField = "loginname"
    '                    .DataValueField = "employeeunkid"
    '                    .DataBind()
    '                    Try
    '                        .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
    '                    Catch ex As Exception
    '                        .SelectedValue = CStr(0)
    '                    End Try
    '                End With
    '            End If
    '        End If
    '        gvReferee.DataSource = Nothing : gvReferee.DataBind()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    ''S.SANDEEP [ 16 JAN 2014 ] -- END

    'S.SANDEEP [20-JUN-2018] -- End

#End Region

#End Region

#Region "ToolBar Event"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try

    '        'Pinkal (22-Nov-2012) -- Start
    '        'Enhancement : TRA Changes

    '        If Session("LoginBy") = Global.User.en_loginby.User Then
    '            If CBool(Session("AddEmployeeReferee")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                Exit Sub
    '            End If
    '        Else
    '            If CBool(Session("AllowAddReference")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                Exit Sub
    '            End If
    '        End If

    '        'Pinkal (22-Nov-2012) -- End

    '        Response.Redirect("~\HR\wPgEmpReferee.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region





    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)

        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End

        Me.lblPendingData.Text = Language._Object.getCaption(Me.lblPendingData.ID, Me.lblPendingData.Text)
        Me.lblParentData.Text = Language._Object.getCaption(Me.lblParentData.ID, Me.lblParentData.Text)

        Me.lblIDNo.Text = Language._Object.getCaption(Me.lblIDNo.ID, Me.lblIDNo.Text)
        Me.lblPostCountry.Text = Language._Object.getCaption(Me.lblPostCountry.ID, Me.lblPostCountry.Text)
        Me.lblRefereeName.Text = Language._Object.getCaption(Me.lblRefereeName.ID, Me.lblRefereeName.Text)
        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)

        Me.gvReferee.Columns(2).HeaderText = Language._Object.getCaption(Me.gvReferee.Columns(2).FooterText, Me.gvReferee.Columns(2).HeaderText)
        Me.gvReferee.Columns(3).HeaderText = Language._Object.getCaption(Me.gvReferee.Columns(3).FooterText, Me.gvReferee.Columns(3).HeaderText)
        Me.gvReferee.Columns(4).HeaderText = Language._Object.getCaption(Me.gvReferee.Columns(4).FooterText, Me.gvReferee.Columns(4).HeaderText)
        Me.gvReferee.Columns(5).HeaderText = Language._Object.getCaption(Me.gvReferee.Columns(5).FooterText, Me.gvReferee.Columns(5).HeaderText)
        Me.gvReferee.Columns(6).HeaderText = Language._Object.getCaption(Me.gvReferee.Columns(6).FooterText, Me.gvReferee.Columns(6).HeaderText)
        Me.gvReferee.Columns(7).HeaderText = Language._Object.getCaption(Me.gvReferee.Columns(7).FooterText, Me.gvReferee.Columns(7).HeaderText)
        Me.gvReferee.Columns(8).HeaderText = Language._Object.getCaption(Me.gvReferee.Columns(8).FooterText, Me.gvReferee.Columns(8).HeaderText)

        Me.BtnNew.Text = Language._Object.getCaption(Me.BtnNew.ID, Me.BtnNew.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 4, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process.")
            Language.setMessage(mstrModuleName, 5, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
