﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgEmpQualification.aspx.vb" Inherits="HR_wPgEmpQualification" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <script>
        function IsValidAttach() {
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>');
            var cboEmp = $('#<%= cboEmployee.ClientID %>');
            var chkOtherQqali = $('#<%= chkOtherQualification.ClientID %>');
            var txtQualiGrp = $('#<%= txtOtherQualificationGrp.ClientID %>');
            var txtQuali = $('#<%= txtOtherQualification.ClientID %>');
            var txtQualiRultCode = $('#<%= txtOtherResultCode.ClientID %>');
            var cboQualiGrp= $('#<%= cboQualifGrp.ClientID %>');
            var cboQuali = $('#<%= cboQualifcation.ClientID %>');
            var cboQualiRultCode = $('#<%= cboResultCode.ClientID %>');
            var cboProvider = $('#<%= cboProvider.ClientID %>');
            var txtGpaCode = $('#<%= txtGPAcode.ClientID %>');
            
            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            if (parseInt(cboEmp.val()) <= 0) {
                alert('Employee is compulsory information. Please select Employee to continue.');
                cboEmp.focus();
                return false;
            }
            if (chkOtherQqali.attr("checked") == "checked") {
                if (txtQualiGrp.val().trim() == "") {
                    alert('Other Qualification Group cannot be blank.Other Qualification Group is compulsory information.');
                    txtQualiGrp.focus();
                    return false;
                }
                if (txtQuali.val().trim() == "") {
                    alert('Other Qualification cannot be blank.Other Qualification is compulsory information.');
                    txtQuali.focus();
                    return false;
                }
                if (txtQualiRultCode.val().trim() == "") {
                    alert('Other Result Code cannot be blank.Other Result Code is compulsory information.');
                    txtQualiRultCode.focus();
                    return false;
                }
            }else{
                if (parseInt(cboQualiGrp.val()) <= 0) {
                    alert('Qualification Group is compulsory information. Please select Qualification Group to continue.');
                    cboQualiGrp.focus();
                    return false;
                }
                if (parseInt(cboQuali.val()) <= 0) {
                    alert('Qualification is compulsory information. Please select Qualification to continue.');
                    cboQuali.focus();
                    return false;
                }
                if (parseInt(cboQualiRultCode.val()) <= 0) {
                    alert('Result Code is compulsory information. Please select Result Code to continue.');
                    cboQualiRultCode.focus();
                    return false;
                }
            }
            if (parseInt(cboProvider.val()) <= 0) {
                alert('Provider is compulsory information. Please select Provider to continue.');
                cboProvider.focus();
                return false;
            }
            if (parseInt(txtGpaCode.val()) > 100) {
                alert('GPA code cannot be greater than 100.');
                txtGpaCode.focus();
                return false;
            }
            return true;
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width","auto");
        }
        
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 80%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Qualification"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Qualification ADD/EDIT"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="text-align: right">
                                                <asp:CheckBox ID="chkOtherQualification" runat="server" Text="Other Qualification"
                                                    AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%;">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 50%">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 40%">
                                                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee :"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 60%">
                                                                        <asp:DropDownList ID="cboEmployee" runat="server">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <asp:MultiView ID="mltView" runat="server" ActiveViewIndex="0">
                                                                    <asp:View ID="mvQualification" runat="server">
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 40%">
                                                                                <asp:Label ID="lblQualificationGroup" runat="server" Text="Qualification Group :"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 60%">
                                                                                <asp:DropDownList ID="cboQualifGrp" runat="server" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 40%">
                                                                                <asp:Label ID="lblQualification" runat="server" Text="Qualification :"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 60%">
                                                                                <asp:DropDownList ID="cboQualifcation" runat="server" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 40%">
                                                                                <asp:Label ID="lblResultCode" runat="server" Text="Result Code :"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 60%">
                                                                                <asp:DropDownList ID="cboResultCode" runat="server" AutoPostBack="true">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </asp:View>
                                                                    <asp:View ID="mvOtherQualification" runat="server">
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 40%">
                                                                                <asp:Label ID="lblOtherQualificationGrp" runat="server" Text=" Other Qualif. Group :"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 60%">
                                                                                <asp:TextBox ID="txtOtherQualificationGrp" runat="server"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 40%">
                                                                                <asp:Label ID="lblOtherQualification" runat="server" Text="Other Qualif. :"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 60%">
                                                                                <asp:TextBox ID="txtOtherQualification" runat="server"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr style="width: 100%">
                                                                            <td style="width: 40%">
                                                                                <asp:Label ID="lblOtherResultCode" runat="server" Text="Other Result Code :"></asp:Label>
                                                                            </td>
                                                                            <td style="width: 60%">
                                                                                <asp:TextBox ID="txtOtherResultCode" runat="server"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                    </asp:View>
                                                                </asp:MultiView>
                                                                <tr>
                                                                    <td style="width: 40%">
                                                                        <asp:Label ID="lblReferenceNo" runat="server" Text="Reference No :"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 60%">
                                                                        <asp:TextBox ID="txtRefNo" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 50%">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 30%">
                                                                        <asp:Label ID="lblAwardDate" runat="server" Text="Date :"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 70%">
                                                                        <uc2:DateCtrl ID="dtQualifDate" runat="server" AutoPostBack="false" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 30%">
                                                                        <asp:Label ID="lblGPAcode" runat="server" Text="GPA/Points :"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 70%">
                                                                        <asp:TextBox ID="txtGPAcode" runat="server" CssClass="RightTextAlign"></asp:TextBox>
                                                                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtGPAcode"
                                                                            CssClass="ErrorControl" ErrorMessage="This Number is Invalid!" Display="Dynamic"
                                                                            ForeColor="White" MaximumValue="99999" MinimumValue="0" Type="Double" SetFocusOnError="True">
                                                                        </asp:RangeValidator>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 30%">
                                                                        <asp:Label ID="lblInstitution" runat="server" Text="Provider :"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 70%">
                                                                        <asp:DropDownList ID="cboProvider" runat="server"></asp:DropDownList>
                                                                        <asp:TextBox ID="txtOtherInstitute" runat="server"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="width: 140px">
                                                                        <asp:Label ID="lblStartDate" runat="server" Text="Start Date :"></asp:Label>
                                                                    </td>
                                                                    <td align="left" style="width: 200px">
                                                                        <uc2:DateCtrl ID="dtAFromDate" runat="server" AutoPostBack="True" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" style="width: 100px">
                                                                        <asp:Label ID="lblEndDate" runat="server" Text="Award Date :"></asp:Label>
                                                                    </td>
                                                                    <td align="left">
                                                                        <uc2:DateCtrl ID="dtAToDate" runat="server" AutoPostBack="True" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td colspan="2" style="width: 100%">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 20%">
                                                                        <asp:Label ID="lblRemark" runat="server" Text="Remark :"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 80%">
                                                                        <asp:TextBox ID="txtRemark" runat="server" Height="45px" TextMode="MultiLine"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" width="100%">
                                                                        <table style="width: 100%">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 11%;padding:0">
                                                                                    <asp:Label ID="lblDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                                                </td>
                                                                                <td style="width: 20%">
                                                                                    <asp:DropDownList ID="cboDocumentType" runat="server" Width="310px"> 
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                                <td style="width: 1%">
                                                                                    <asp:Panel ID="pnl_ImageAdd" runat="server" Width="100%">
                                                                                        <div id="fileuploader">
                                                                                            <input type="button" id="btnAddFile" runat="server" class="btndefault" onclick="return IsValidAttach()"
                                                                                                value="Browse" />
                                                                                        </div>
                                                                                    </asp:Panel>
                                                                                    <%--'Hemant (30 Nov 2018) -- Start
                                                                                    'Enhancement : Including Language Settings For Scan/Attachment Button--%>
                                                                                    <%--<asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" Text="Browse" OnClick="btnSaveAttachment_Click" />--%>
                                                                                    <asp:Button ID="btnAddAttachment" runat="server" Style="display: none" Text="Browse" OnClick="btnSaveAttachment_Click" />
                                                                                    <%--'Hemant (30 Nov 2018) -- End--%>
                                                                                </td>
                                                                                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                                                <td style="width: 15%">
                                                                                    <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                                                                </td>
                                                                                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                                            </tr>
                                                                        </table>
                                                                        <table style="width: 100%;">
                                                                            <tr style="width: 100%">
                                                                                <td style="width: 100%">
                                                                                    <asp:Panel ID="pnl_dgvQualification" runat="server" ScrollBars="Auto" Style="max-height: 300px">
                                                                                        <asp:DataGrid ID="dgvQualification" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                                                            Width="99%" CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                                                            HeaderStyle-Font-Bold="false">
                                                                                            <Columns>
                                                                                                <asp:TemplateColumn FooterText="objcohDelete">
                                                                                                    <ItemTemplate>
                                                                                                        <span class="gridiconbc">
                                                                                                            <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                                                                ToolTip="Delete"></asp:LinkButton>
                                                                                                        </span>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                                                <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                                                                <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center"
                                                                                                    ItemStyle-Font-Size="22px">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Delete">
                                                                                                            <i class="fa fa-download"></i>
                                                                                                        </asp:LinkButton>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateColumn>
                                                                                                <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                                                                    Visible="false" />
                                                                                                <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                                                                    Visible="false" />
                                                                                            </Columns>
                                                                                        </asp:DataGrid>
                                                                                    </asp:Panel>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btndefault" Text="Save" Width="77px" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" Width="77px" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ucCfnYesno:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                </ContentTemplate>
                <Triggers>
                    <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                    <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                    <asp:PostBackTrigger ControlID="btnDownloadAll" />
                    <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <script>
			$(document).ready(function()
			{
				ImageLoad();
				$(".ajax-upload-dragdrop").css("width","auto");
			});
			function ImageLoad(){
			    if ($(".ajax-upload-dragdrop").length <= 0){
			    $("#fileuploader").uploadFile({
				    url: "wPgEmpQualification.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
				    dragDropStr: "",
				    showStatusAfterSuccess:false,
                    showAbort:false,
                    showDone:false,
				    fileName:"myfile",
				    onSuccess:function(path,data,xhr){
				        $("#<%= btnAddAttachment.ClientID %>").click();
				    },
                    onError:function(files,status,errMsg){
	                        alert(errMsg);
                        }
                });
			}
			}
			$('input[type=file]').live("click",function(){
			    return IsValidAttach();
			});
    </script>

</asp:Content>
