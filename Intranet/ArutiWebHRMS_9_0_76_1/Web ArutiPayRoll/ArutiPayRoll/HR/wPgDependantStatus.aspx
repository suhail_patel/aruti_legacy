﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPgDependantStatus.aspx.vb" Inherits="HR_wPgDependantStatus" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/CommonValidationList.ascx" TagName="CommonValidationList" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }

    </script>
    
     <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Dependants List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblGender" runat="server" Text="Gender"></asp:Label>
                                            </td>
                                            <td style="width: 16%">
                                                <asp:DropDownList ID="cboGender" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblBirthdate" runat="server" Text="Birtdate"></asp:Label>
                                            </td>
                                            <td style="width: 16%">
                                                <uc2:DateCtrl ID="dtpBirthdate" runat="server" />
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                            </td>
                                            <td style="width: 16%">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblDependant" runat="server" Text="Dependant"></asp:Label>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:DropDownList ID="cboDependant" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                             <td style="width: 8%">
                                                <asp:Label ID="lblDBRelation" runat="server" Text="Relation"></asp:Label>
                                            </td>
                                            <td style="width: 16%">
                                                <asp:DropDownList ID="cboRelation" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                 <asp:Label ID="lblDBIdNo" runat="server" Text="ID. No."></asp:Label>
                                            </td>
                                            <td style="width: 16%">
                                                <asp:TextBox ID="txtIdNo" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width: 8%">
                                                 &nbsp;
                                            </td>
                                            <td style="width: 16%">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <table style="float:left;">
                                            <tr>
                                                <td style="width:20%;"><asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date"></asp:Label></td>
                                                <td style="width:30%;"><uc2:DateCtrl ID="dtpEffectiveDate" runat="server" Width="100" /></td>
                                                <td style="width:20%;"><asp:Label ID="lblReason" runat="server" Text="Reason"></asp:Label></td>
                                                <td style="width:30%;"><asp:DropDownList ID="cboReason" runat="server">
                                                </asp:DropDownList></td>
                                            </tr>
                                        </table>
                                        
                                        <asp:Button ID="BtnSetActive" runat="server" CssClass="btndefault" Text="Set Active" />
                                        <asp:Button ID="BtnSetInactive" runat="server" CssClass="btndefault" Text="Set Inactive" Visible="false" />
                                        <asp:Button ID="btnSearch" runat="server" CssClass="btndefault" Text="Search" />
                                        <asp:Button ID="BtnReset" runat="server" CssClass="btndefault" Text="Reset" />
                                        <asp:Button ID="BtnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>    
                                <asp:Panel ID="pnl_dgvDependantList" runat="server" ScrollBars="Auto" Width="100%">
                                    <asp:DataGrid ID="dgvDependantList" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                        HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                        HeaderStyle-Font-Bold="false" Width="99%">
                                        <Columns>
                                            <asp:BoundColumn DataField="IsChecked" HeaderText="IsCheck" Visible="false"></asp:BoundColumn><%--0--%>                                            
                                             <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-HorizontalAlign="Center" FooterText="objdgcolhSelect">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" 
                                                            oncheckedchanged="chkSelectAll_CheckedChanged" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                </asp:TemplateColumn><%--1--%>                                            
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                            CommandName="Delete"></asp:LinkButton>
                                                    </span>                                                   
                                                </ItemTemplate>
                                            </asp:TemplateColumn><%-- 2--%>                                            
                                             <asp:BoundColumn DataField="DpndtBefName" HeaderText="Name" ReadOnly="true" FooterText="dgcolhDependant">
                                            </asp:BoundColumn><%-- 3--%>
                                            <asp:BoundColumn DataField="Relation" HeaderText="Relation" ReadOnly="true" FooterText="dgcolhRelation">
                                            </asp:BoundColumn><%-- 4--%>                                            
                                            <asp:BoundColumn DataField="Gender" HeaderText="Gender" ReadOnly="true" FooterText="dgcolhGender">
                                            </asp:BoundColumn><%-- 5--%>                                            
                                            <asp:BoundColumn DataField="dtbirthdate" HeaderText="Birthdate" ReadOnly="true" FooterText="dgcolhBirthDate">
                                            </asp:BoundColumn><%-- 6--%>                                        
                                            <asp:BoundColumn DataField="ROWNO" HeaderText="" ReadOnly="true" FooterText="objdgcolhROWNO" Visible="false">
                                            </asp:BoundColumn><%-- 7--%>                                            
                                            <asp:BoundColumn DataField="EmpId" HeaderText="Employee ID" ReadOnly="true" Visible="false">
                                            </asp:BoundColumn><%-- 8--%>                                           
                                           <asp:BoundColumn DataField="dpndtbeneficestatustranunkid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhDependandstatustranunkid">
                                            </asp:BoundColumn><%-- 9--%>                                            
                                            <asp:BoundColumn DataField="DpndtTranId" HeaderText="DpndtTranId" Visible="false"
                                                FooterText="objdgcolhRefreeTranId"></asp:BoundColumn><%-- 10--%>                                            
                                            <asp:BoundColumn DataField="dteffective_date" HeaderText="Effective Date" Visible="true"
                                                FooterText="dgcolhEffectiveDate"></asp:BoundColumn><%-- 11--%>                                                
                                            <asp:BoundColumn DataField="isactive" HeaderText="Is Active" Visible="true"
                                                FooterText="dgcolhActive"></asp:BoundColumn><%-- 12--%>                                            
                                            <asp:BoundColumn DataField="Reason" HeaderText="Reason" Visible="true"
                                                FooterText="dgcolhReason"></asp:BoundColumn><%-- 13--%>
                                            <asp:BoundColumn DataField="isapproved" Visible="false" ></asp:BoundColumn><%-- 14--%>
                                        </Columns>
                                    </asp:DataGrid>
                                </asp:Panel>                            
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popupDeleteReason" runat="server" />
                    <uc4:CommonValidationList ID="popupValidationList" runat="server" Message="" ShowYesNo="false" />
                    <asp:HiddenField ID="hd1" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
     </center>
    
</asp:Content>

