﻿Option Strict On 'Shani(19-MAR-2016)
#Region " Imports "

Imports System.Data
Imports Aruti.Data

#End Region

Partial Class wPgEmpMemberships
    Inherits Basepage

#Region " Private Variable(s) "

    Private objEmpMembership As New clsMembershipTran
    Dim msg As New CommonCodes
    Dim clsuser As New User

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeMaster"
    Private ReadOnly mstrModuleName1 As String = "frmEmployeeList"
    'Pinkal (06-May-2014) -- End

    'Nilay (31-Aug-2015) -- Start
    Private mintmembershiptranunkid As Integer = -1
    'Nilay (31-Aug-2015) -- End

    'S.SANDEEP [31 AUG 2015] -- START
    'ENHANCEMENT : -------------- LEFT OUT CODE
    Private ReadOnly mstrModuleName2 As String = "frmMembershipHead"
    'S.SANDEEP [31 AUG 2015] -- END

    'S.SANDEEP |15-APR-2019| -- START
    Private objAMemInfoTran As New clsMembership_Approval_Tran
    Dim Arr() As String
    Dim MembershipApprovalFlowVal As String
    'S.SANDEEP |15-APR-2019| -- END

    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End
#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If
            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmEmployeeMaster"
            StrModuleName2 = "mnuCoreSetups"
            clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            'S.SANDEEP |15-APR-2019| -- START
            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            MembershipApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmMembershipInfoList)))
            'S.SANDEEP |15-APR-2019| -- END
            'Hemant (31 May 2019) -- Start
            'ISSUE/ENHANCEMENT : UAT Changes
            Call SetMessages()
            Call Language._Object.SaveValue()
            'Hemant (31 May 2019) -- End
            SetLanguage()

            If (Page.IsPostBack = False) Then
                clsuser = CType(Session("clsuser"), User)

                'Nilay (31-Aug-2015) -- Start
                'If (Session("LoginBy") = Global.User.en_loginby.User) Then

                '    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'Aruti.Data.User._Object._Userunkid = Session("UserId")
                '    'S.SANDEEP [ 27 APRIL 2012 ] -- END


                '    'Anjan (30 May 2012)-Start
                '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                '    'BtnSave.Visible = False
                '    BtnSave.Visible = Session("EditEmployee")
                '    'Anjan (30 May 2012)-End 

                '    'S.SANDEEP [ 16 JAN 2014 ] -- START
                '    If Session("ShowPending") IsNot Nothing Then
                '        BtnSave.Visible = False
                '    End If
                '    'S.SANDEEP [ 16 JAN 2014 ] -- END

                'Else

                '    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'Aruti.Data.User._Object._Userunkid = -1
                '    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                '    'Anjan (02 Mar 2012)-Start
                '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                '    'BtnSave.Visible = False


                '    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'BtnSave.Visible = ConfigParameter._Object._AllowEditMembership
                '    BtnSave.Visible = Session("AllowEditMembership")
                '    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                '    'Anjan (02 Mar 2012)-End 

                'End If
                'Nilay (31-Aug-2015) -- End

                'Nilay (31-Aug-2015) -- Start
                Call FillCombo()
                If Session("membershiptranunkid") IsNot Nothing AndAlso CInt(Session("membershiptranunkid")) > 0 Then
                    mintmembershiptranunkid = CInt(Session("membershiptranunkid"))
                    cboEmployee.Enabled = False
                    cboMemCategory.Enabled = False
                    cboMembership.Enabled = False

                    Call Fill_Info()

                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        If mintmembershiptranunkid <= 0 Then
                            BtnSave.Visible = CBool(Session("AddEmployee"))
                        Else
                            BtnSave.Visible = CBool(Session("EditEmployee"))
                        End If

                        If Session("ShowPending") IsNot Nothing Then
                            BtnSave.Visible = False
                        End If
                    Else
                        If mintmembershiptranunkid <= 0 Then
                            BtnSave.Visible = CBool(Session("AllowAddMembership"))
                        Else
                            BtnSave.Visible = CBool(Session("AllowEditMembership"))
                        End If
                    End If

                Else
                    mintmembershiptranunkid = -1
                    cboEmployee.Enabled = True
                    cboMemCategory.Enabled = True
                    cboMembership.Enabled = True
                End If
                'Nilay (31-Aug-2015) -- End



                'Nilay (02-Mar-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'Nilay (31-Aug-2015) -- Start
                'If Request.QueryString.Count <= 0 Then
                '    If Session("membershipEmpunkid") IsNot Nothing Then
                '        cboEmployee.SelectedValue = CInt(Session("membershipEmpunkid"))
                '    End If
                'End If
                If Session("membershipEmpunkid") IsNot Nothing Then
                    cboEmployee.SelectedValue = CStr(CInt(Session("membershipEmpunkid")))
                End If
                'Nilay (31-Aug-2015) -- End


                'Nilay (31-Aug-2015) -- Start
            Else
                mintmembershiptranunkid = CInt(Me.ViewState("membershiptranunkid"))
                'Nilay (31-Aug-2015) -- End
            End If
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Form)
            'SHANI [01 FEB 2015]--END
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [01 FEB 2015]--END
        End Try
    End Sub

    'Nilay (31-Aug-2015) -- Start
    'Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
    '    Try
    '        If Not IsPostBack Then
    '            IsFromMembershipList()
    '        End If
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'Nilay (31-Aug-2015) -- End


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)
        'Nilay (31-Aug-2015) -- Start
        Me.ViewState.Add("membershiptranunkid", mintmembershiptranunkid)
        'Nilay (31-Aug-2015) -- End

        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'ToolbarEntry1.VisibleSaveButton(False)
        'ToolbarEntry1.VisibleDeleteButton(False)
        'ToolbarEntry1.VisibleCancelButton(False)
        'ToolbarEntry1.VisibleNewButton(False)
        'ToolbarEntry1.VisibleModeFormButton(False)
        'ToolbarEntry1.VisibleExitButton(False)
        'SHANI [01 FEB 2015]--END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objCMaster As New clsCommon_Master
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'dsCombos = objEmployee.GetEmployeeList("Employee", True, Not Aruti.Data.ConfigParameter._Object._IsIncludeInactiveEmp)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsCombos = objEmployee.GetEmployeeList("Employee", True, )
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END
                'End If
                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                            CStr(Session("UserAccessModeSetting")), True, _
                                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                'Shani(24-Aug-2015) -- End


                If Session("ShowPending") IsNot Nothing Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"), , , False)
                    dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(Session("UserAccessModeSetting")), False, _
                                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

                    'Shani(24-Aug-2015) -- End
                    Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
                    With cboEmployee
                        .DataValueField = "employeeunkid"
                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        '.DataTextField = "employeename"
                        .DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        .DataSource = dtTab
                        .DataBind()
                    End With
                Else
                    With cboEmployee
                        .DataValueField = "employeeunkid"
                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        '.DataTextField = "employeename"
                        .DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        .DataSource = dsCombos.Tables("Employee")
                        .DataBind()
                        .SelectedValue = CStr(0)
                    End With
                End If
                'S.SANDEEP [ 16 JAN 2014 ] -- END
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If

            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, True, "List")
            With cboMemCategory
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            cboMemCatagory_SelectedIndexChanged(New Object, New EventArgs())
            'Pinkal (28-Dec-2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Nilay (31-Aug-2015) -- Start
    'Public Function b64decode(ByVal StrDecode As String) As String
    '    Dim decodedString As String = ""
    '    Try
    '        decodedString = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(StrDecode))
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    '    Return decodedString
    'End Function

    'Private Function IsFromMembershipList() As Boolean
    '    Try
    '        If (Request.QueryString("ProcessId") <> "") Then

    '            Dim minMemTranUnkid As Integer = -1
    '            minMemTranUnkid = Val(b64decode(Request.QueryString("ProcessId")))
    '            Me.ViewState.Add("Unkid", minMemTranUnkid)
    '            Dim dsList As New DataSet
    '            dsList = objEmpMembership.GetMembership_TranList("List", , , minMemTranUnkid)
    '            If dsList.Tables(0).Rows.Count > 0 Then
    '                Call Fill_Info(dsList)
    '            End If
    '        Else

    '            Me.ViewState.Add("Unkid", -1)
    '        End If

    '        'Pinkal (22-Nov-2012) -- Start
    '        'Enhancement : TRA Changes

    '        If (Session("LoginBy") = Global.User.en_loginby.User) Then

    '            If Me.ViewState("Unkid") Is Nothing Or Me.ViewState("Unkid") <= 0 Then
    '                BtnSave.Visible = Session("AddEmployee")
    '            Else
    '                BtnSave.Visible = Session("EditEmployee")
    '            End If

    '            'S.SANDEEP [ 16 JAN 2014 ] -- START
    '            If Session("ShowPending") IsNot Nothing Then
    '                BtnSave.Visible = False
    '            End If
    '            'S.SANDEEP [ 16 JAN 2014 ] -- END

    '        Else

    '            If Me.ViewState("Unkid") Is Nothing Or Me.ViewState("Unkid") <= 0 Then
    '                BtnSave.Visible = Session("AllowAddMembership")
    '            Else
    '                BtnSave.Visible = Session("AllowEditMembership")
    '            End If
    '        End If

    '        'Pinkal (22-Nov-2012) -- End

    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Function

    'Nilay (31-Aug-2015) -- End

    Private Sub Fill_Info()
        Try
            Dim dsList As New DataSet
            dsList = CType(objEmpMembership.GetMembership_TranList("List", , , mintmembershiptranunkid), DataSet)

            If dsList.Tables(0).Rows.Count > 0 Then

                cboEmployee.SelectedValue = CStr(dsList.Tables(0).Rows(0)("employeeunkid"))
                cboMemCategory.SelectedValue = CStr(dsList.Tables(0).Rows(0)("membership_categoryunkid"))
                Call cboMemCatagory_SelectedIndexChanged(Nothing, Nothing)
                cboMembership.SelectedValue = CStr(dsList.Tables(0).Rows(0)("membershipunkid"))
                Me.ViewState.Add("MemUnkid", dsList.Tables(0).Rows(0)("membershipunkid"))
                txtMembershipNo.Text = CStr(dsList.Tables(0).Rows(0)("membershipno"))
                txtRemark.Text = CStr(dsList.Tables(0).Rows(0)("remark"))

                If IsDBNull(dsList.Tables(0).Rows(0)("issue_date")) = False Then
                    dtpIssueDate.SetDate = CDate(dsList.Tables(0).Rows(0)("issue_date")).Date
                Else
                    dtpIssueDate.SetDate = Nothing
                End If

                If IsDBNull(dsList.Tables(0).Rows(0)("start_date")) = False Then
                    dtpStartDate.SetDate = CDate(dsList.Tables(0).Rows(0)("start_date")).Date
                Else
                    dtpStartDate.SetDate = Nothing
                End If

                If IsDBNull(dsList.Tables(0).Rows(0)("expiry_date")) = False Then
                    dtpEndDate.SetDate = CDate(dsList.Tables(0).Rows(0)("expiry_date")).Date
                Else
                    dtpEndDate.SetDate = Nothing
                End If
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function isValidData() As Boolean
        Try


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If MembershipApprovalFlowVal Is Nothing = True Then

            End If

            'Gajanan [17-April-2019] -- End



            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            'If Me.ViewState("MemUnkid") <> cboMembership.SelectedValue  Then
            If CInt(Me.ViewState("MemUnkid")) <> CInt(cboMembership.SelectedValue) AndAlso CInt(cboMembership.SelectedValue) > 0 Then
                'Pinkal (28-Dec-2015) -- End
                Dim dsValid As New DataSet
                dsValid = CType(objEmpMembership.GetMembership_TranList("List", CInt(cboEmployee.SelectedValue), CInt(cboMembership.SelectedValue)), DataSet)

                If dsValid.Tables(0).Rows.Count > 0 Then
                    'Hemant (31 May 2019) -- Start
                    'ISSUE/ENHANCEMENT : UAT Changes
                    'msg.DisplayMessage("Sorry, this Membership is already present for the selected employee. Please add new Membership to continue.", Me)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 2006, "Sorry, this Membership is already present for the selected employee. Please add new Membership to continue."), Me)
                    'Hemant (31 May 2019) -- End
                    Return False
                End If
            End If


            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Hemant (31 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes
                'msg.DisplayMessage("Employee is mandatory information. Please select Employee to continue.", Me)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 2002, "Employee is mandatory information. Please select Employee to continue."), Me)
                'Hemant (31 May 2019) -- End
                Return False
            End If

            If CInt(cboMembership.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 46, "Membership is compulsory information, it can not be blank. Please select a Membership to continue."), Me)
                Return False
            End If

            If dtpStartDate.IsNull = False AndAlso dtpIssueDate.IsNull = False Then
                If dtpStartDate.GetDate.Date <= dtpIssueDate.GetDate.Date Then
                    Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 43, "Start date cannot be less then or equal to Issue date."), Me)
                    Return False
                End If
            End If

            If dtpStartDate.IsNull = False AndAlso dtpEndDate.IsNull = False Then
                If dtpEndDate.GetDate.Date <= dtpStartDate.GetDate.Date Then
                    Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 44, "End date cannot be less then or equal to Start date."), Me)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub ClearObjects()
        Try
            cboMembership.SelectedValue = CStr(0)
            txtMembershipNo.Text = ""
            txtRemark.Text = ""
            dtpEndDate.SetDate = Nothing
            dtpIssueDate.SetDate = Nothing
            dtpStartDate.SetDate = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("Procedure ClearObjets : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'S.SANDEEP [31 AUG 2015] -- START
    'ENHANCEMENT : -------------- LEFT OUT CODE


    'S.SANDEEP |15-APR-2019| -- START
    'Private Sub SaveMembership()
    '    Try
    '        Dim mdTran As New DataTable
    '        objEmpMembership._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
    '        mdTran = objEmpMembership._DataList

    '        Dim objMembership As New clsmembership_master
    '        objMembership._Membershipunkid = CInt(cboMembership.SelectedValue)

    '        If mintmembershiptranunkid <= 0 Then
    '            Dim dtMemRow As DataRow
    '            dtMemRow = mdTran.NewRow
    '            dtMemRow.Item("membershiptranunkid") = -1
    '            dtMemRow.Item("employeeunkid") = cboEmployee.SelectedValue
    '            dtMemRow.Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
    '            dtMemRow.Item("membershipunkid") = CInt(cboMembership.SelectedValue)
    '            dtMemRow.Item("membershipno") = txtMembershipNo.Text
    '            If dtpIssueDate.IsNull = False Then
    '                dtMemRow.Item("issue_date") = dtpIssueDate.GetDate.Date
    '            Else
    '                dtMemRow.Item("issue_date") = DBNull.Value
    '            End If

    '            If dtpStartDate.IsNull = False Then
    '                dtMemRow.Item("start_date") = dtpStartDate.GetDate.Date
    '            Else
    '                dtMemRow.Item("start_date") = DBNull.Value
    '            End If

    '            If dtpEndDate.IsNull = False Then
    '                dtMemRow.Item("expiry_date") = dtpEndDate.GetDate.Date
    '            Else
    '                dtMemRow.Item("expiry_date") = DBNull.Value
    '            End If
    '            dtMemRow.Item("remark") = txtRemark.Text
    '            dtMemRow.Item("isactive") = True
    '            dtMemRow.Item("AUD") = "A"
    '            dtMemRow.Item("GUID") = Guid.NewGuid().ToString
    '            If objMembership._ETransHead_Id > 0 Or objMembership._CTransHead_Id > 0 Then
    '                dtMemRow.Item("emptrnheadid") = objMembership._ETransHead_Id
    '                dtMemRow.Item("cotrnheadid") = objMembership._CTransHead_Id
    '                dtMemRow.Item("effetiveperiodid") = CInt(cboEffectivePeriod.SelectedValue)
    '                dtMemRow.Item("copyedslab") = chkCopyPreviousEDSlab.Checked
    '                dtMemRow.Item("overwriteslab") = False
    '                dtMemRow.Item("overwritehead") = chkOverwriteHeads.Checked
    '            Else
    '                dtMemRow.Item("emptrnheadid") = -1
    '                dtMemRow.Item("cotrnheadid") = -1
    '                dtMemRow.Item("effetiveperiodid") = -1
    '            End If
    '            mdTran.Rows.Add(dtMemRow)
    '        Else
    '            Dim dtMemRow() As DataRow = mdTran.Select("membershiptranunkid = '" & mintmembershiptranunkid & "'")
    '            If dtMemRow.Length <= 0 Then Exit Sub

    '            dtMemRow(0).Item("membershiptranunkid") = mintmembershiptranunkid
    '            dtMemRow(0).Item("employeeunkid") = cboEmployee.SelectedValue
    '            dtMemRow(0).Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
    '            dtMemRow(0).Item("membershipunkid") = CInt(cboMembership.SelectedValue)
    '            dtMemRow(0).Item("membershipno") = txtMembershipNo.Text
    '            If dtpIssueDate.IsNull = False Then
    '                dtMemRow(0).Item("issue_date") = dtpIssueDate.GetDate.Date
    '            Else
    '                dtMemRow(0).Item("issue_date") = DBNull.Value
    '            End If

    '            If dtpStartDate.IsNull = False Then
    '                dtMemRow(0).Item("start_date") = dtpStartDate.GetDate.Date
    '            Else
    '                dtMemRow(0).Item("start_date") = DBNull.Value
    '            End If

    '            If dtpEndDate.IsNull = False Then
    '                dtMemRow(0).Item("expiry_date") = dtpEndDate.GetDate.Date
    '            Else
    '                dtMemRow(0).Item("expiry_date") = DBNull.Value
    '            End If
    '            dtMemRow(0).Item("remark") = txtRemark.Text
    '            dtMemRow(0).Item("isactive") = True
    '            dtMemRow(0).Item("AUD") = "U"
    '            dtMemRow(0).Item("GUID") = Guid.NewGuid().ToString
    '            dtMemRow(0).Item("emptrnheadid") = dtMemRow(0).Item("emptrnheadid")
    '            dtMemRow(0).Item("cotrnheadid") = dtMemRow(0).Item("cotrnheadid")
    '            dtMemRow(0).Item("effetiveperiodid") = dtMemRow(0).Item("effetiveperiodid")
    '            dtMemRow(0).Item("copyedslab") = dtMemRow(0).Item("copyedslab")
    '            dtMemRow(0).Item("overwriteslab") = dtMemRow(0).Item("overwriteslab")
    '            dtMemRow(0).Item("overwritehead") = dtMemRow(0).Item("overwritehead")
    '        End If
    '        If mintmembershiptranunkid <= 0 Then
    '            objEmpMembership._DataList = mdTran

    '            'Shani(20-Nov-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'If objEmpMembership.InsertUpdateDelete_MembershipTran(Session("UserId")) = False Then
    '            If objEmpMembership.InsertUpdateDelete_MembershipTran(CStr(Session("Database_Name")), _
    '                                                                  CInt(Session("UserId")), _
    '                                                                  CInt(Session("Fin_year")), _
    '                                                                  CInt(Session("CompanyUnkId")), _
    '                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                                                  CStr(Session("UserAccessModeSetting")), True, _
    '                                                                  CBool(Session("IsIncludeInactiveEmp")), _
    '                                                                  CBool(Session("AllowToApproveEarningDeduction")), _
    '                                                                  ConfigParameter._Object._CurrentDateAndTime, Nothing) = False Then
    '                'Shani(20-Nov-2015) -- End

    '                msg.DisplayMessage("Problem in Inserting Membership.", Me)
    '            Else
    '                msg.DisplayMessage("Membership Successfully Inserted.", Me)
    '            End If
    '            Call ClearObjects()
    '        Else
    '            objEmpMembership._DataList = mdTran

    '            'Shani(20-Nov-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'If objEmpMembership.InsertUpdateDelete_MembershipTran(Session("UserId")) = False Then
    '            If objEmpMembership.InsertUpdateDelete_MembershipTran(CStr(Session("Database_Name")), _
    '                                                                  CInt(Session("UserId")), _
    '                                                                  CInt(Session("Fin_year")), _
    '                                                                  CInt(Session("CompanyUnkId")), _
    '                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                                                  CStr(Session("UserAccessModeSetting")), True, _
    '                                                                  CBool(Session("IsIncludeInactiveEmp")), _
    '                                                                  CBool(Session("AllowToApproveEarningDeduction")), _
    '                                                                  ConfigParameter._Object._CurrentDateAndTime, Nothing) = False Then
    '                'Shani(20-Nov-2015) -- End
    '                msg.DisplayMessage("Problem in updating Membership.", Me)
    '            Else
    '                msg.DisplayMessage("Membership Successfully Updated.", Me)
    '                Session("membershiptranunkid") = Nothing
    '                Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpMembership_List.aspx")
    '            End If
    '        End If
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    Private Sub SaveMembership()
        Try
            Dim mdTran As New DataTable
            Dim objMembership As New clsmembership_master
            objMembership._Membershipunkid = CInt(cboMembership.SelectedValue)
            Dim blnIsApproved As Boolean = False
            Dim objEmp As New clsEmployee_Master : objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmployee.SelectedValue)
            blnIsApproved = objEmp._Isapproved : objEmp = Nothing

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim eLMode As Integer
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                eLMode = enLogin_Mode.MGR_SELF_SERVICE
            Else
                eLMode = enLogin_Mode.EMP_SELF_SERVICE
            End If
            'Gajanan [17-April-2019] -- End

            If MembershipApprovalFlowVal Is Nothing AndAlso blnIsApproved = True Then
                objAMemInfoTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
                mdTran = objAMemInfoTran._DataTable
                Dim strKey As String = ""
                If mintmembershiptranunkid <= 0 Then
                    strKey = "Inserted"
                    Dim dtMemRow As DataRow = mdTran.NewRow
                    dtMemRow.Item("tranguid") = Guid.NewGuid.ToString()
                    dtMemRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        dtMemRow.Item("loginemployeeunkid") = Session("Employeeunkid")
                    End If
                    dtMemRow.Item("mappingunkid") = 0
                    dtMemRow.Item("approvalremark") = ""
                    dtMemRow.Item("isfinal") = False
                    dtMemRow.Item("statusunkid") = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                    dtMemRow.Item("isvoid") = False
                    dtMemRow.Item("voidreason") = ""
                    dtMemRow.Item("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                    dtMemRow.Item("audittype") = enAuditType.ADD
                    dtMemRow.Item("audituserunkid") = Session("UserId")
                    dtMemRow.Item("ip") = CStr(Session("IP_ADD"))
                    dtMemRow.Item("host") = CStr(Session("HOST_NAME"))
                    dtMemRow.Item("form_name") = mstrModuleName
                    dtMemRow.Item("isweb") = True
                    dtMemRow.Item("isprocessed") = False
                    dtMemRow.Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.ADDED
                    dtMemRow.Item("operationtype") = Language.getMessage("clsEmployeeDataApproval", 62, "Newly Added Information")
                    dtMemRow.Item("membership_category") = cboMemCategory.SelectedItem.Text
                    dtMemRow.Item("membershipname") = cboMembership.SelectedItem.Text
                    dtMemRow.Item("membershiptranunkid") = -1
                    dtMemRow.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                    dtMemRow.Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
                    dtMemRow.Item("membershipunkid") = CInt(cboMembership.SelectedValue)
                    dtMemRow.Item("membershipno") = txtMembershipNo.Text
                    If dtpIssueDate.IsNull = False Then
                        dtMemRow.Item("issue_date") = dtpIssueDate.GetDate()
                    Else
                        dtMemRow.Item("issue_date") = DBNull.Value
                    End If
                    If dtpStartDate.IsNull = False Then
                        dtMemRow.Item("start_date") = dtpStartDate.GetDate()
                    Else
                        dtMemRow.Item("start_date") = DBNull.Value
                    End If
                    If dtpEndDate.IsNull = False Then
                        dtMemRow.Item("expiry_date") = dtpEndDate.GetDate()
                    Else
                        dtMemRow.Item("expiry_date") = DBNull.Value
                    End If
                    dtMemRow.Item("remark") = txtRemark.Text
                    dtMemRow.Item("isactive") = True
                    dtMemRow.Item("AUD") = "A"
                    dtMemRow.Item("GUID") = Guid.NewGuid().ToString
                    dtMemRow.Item("ccategory") = Language.getMessage("clsMembershipTran", 1, "Active")
                    dtMemRow.Item("ccategoryid") = 1
                    dtMemRow.Item("isdeleted") = False
                    If objMembership._ETransHead_Id > 0 Or objMembership._CTransHead_Id > 0 Then
                        dtMemRow.Item("emptrnheadid") = objMembership._ETransHead_Id
                        dtMemRow.Item("cotrnheadid") = objMembership._CTransHead_Id
                        dtMemRow.Item("effetiveperiodid") = CInt(cboEffectivePeriod.SelectedValue)
                        dtMemRow.Item("copyedslab") = chkCopyPreviousEDSlab.Checked
                        dtMemRow.Item("overwriteslab") = False
                        dtMemRow.Item("overwritehead") = chkOverwriteHeads.Checked
                    Else
                        dtMemRow.Item("emptrnheadid") = -1
                        dtMemRow.Item("cotrnheadid") = -1
                        dtMemRow.Item("effetiveperiodid") = -1
                    End If
                    mdTran.Rows.Add(dtMemRow)
                Else
                    If mintmembershiptranunkid > 0 Then
                        objEmpMembership._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
                        mdTran = objEmpMembership._DataList
                    End If
                    Dim dtMemRow() As DataRow = mdTran.Select("membershiptranunkid = '" & mintmembershiptranunkid & "'")
                    strKey = "Updated"
                    If dtMemRow.Length <= 0 Then Exit Sub
                    If dtMemRow.Length > 0 Then
                        objAMemInfoTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
                        mdTran = objAMemInfoTran._DataTable
                    End If
                    Dim dtMRow As DataRow = mdTran.NewRow
                    dtMRow.Item("tranguid") = Guid.NewGuid.ToString()
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        dtMRow.Item("loginemployeeunkid") = Session("Employeeunkid")
                    End If
                    dtMRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
                    dtMRow.Item("mappingunkid") = 0
                    dtMRow.Item("approvalremark") = ""
                    dtMRow.Item("isfinal") = False
                    dtMRow.Item("statusunkid") = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                    dtMRow.Item("isvoid") = False
                    dtMRow.Item("voidreason") = ""
                    dtMRow.Item("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                    dtMRow.Item("audittype") = enAuditType.ADD
                    dtMRow.Item("audituserunkid") = Session("UserId")
                    dtMRow.Item("ip") = CStr(Session("IP_ADD"))
                    dtMRow.Item("host") = CStr(Session("HOST_NAME"))
                    dtMRow.Item("form_name") = mstrModuleName
                    dtMRow.Item("isweb") = True
                    dtMRow.Item("isprocessed") = False
                    dtMRow.Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.EDITED
                    dtMRow.Item("operationtype") = Language.getMessage("clsEmployeeDataApproval", 63, "Information Edited")
                    dtMRow.Item("membership_category") = cboMemCategory.SelectedItem.Text
                    dtMRow.Item("membershipname") = cboMembership.SelectedItem.Text
                    dtMRow.Item("membershiptranunkid") = mintmembershiptranunkid
                    dtMRow.Item("employeeunkid") = CInt(cboEmployee.SelectedValue)
                    dtMRow.Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
                    dtMRow.Item("membershipunkid") = CInt(cboMembership.SelectedValue)
                    dtMRow.Item("membershipno") = txtMembershipNo.Text
                    dtMRow.Item("issue_date") = dtMemRow(0).Item("issue_date")
                    dtMRow.Item("start_date") = dtMemRow(0).Item("start_date")
                    dtMRow.Item("expiry_date") = dtMemRow(0).Item("expiry_date")
                    dtMRow.Item("remark") = txtRemark.Text
                    dtMRow.Item("isactive") = True
                    dtMRow.Item("AUD") = "A"
                    dtMRow.Item("GUID") = Guid.NewGuid().ToString
                    dtMRow.Item("ccategory") = Language.getMessage("clsMembershipTran", 1, "Active")
                    dtMRow.Item("ccategoryid") = 1
                    dtMRow.Item("isdeleted") = False
                    dtMRow.Item("emptrnheadid") = dtMemRow(0).Item("emptrnheadid")
                    dtMRow.Item("cotrnheadid") = dtMemRow(0).Item("cotrnheadid")
                    dtMRow.Item("effetiveperiodid") = dtMemRow(0).Item("effetiveperiodid")
                    dtMRow.Item("copyedslab") = dtMemRow(0).Item("copyedslab")
                    dtMRow.Item("overwriteslab") = dtMemRow(0).Item("overwriteslab")
                    dtMRow.Item("overwritehead") = dtMemRow(0).Item("overwritehead")

                    mdTran.Rows.Add(dtMRow)
                End If
                If objAMemInfoTran.InsertUpdateDelete_MembershipTran(mdTran, Nothing, CInt(Session("UserId")), CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp"))) Then

                    If IsNothing(mdTran) = False AndAlso mdTran.Select("AUD <> ''").Length > 0 Then
                        objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                     CStr(Session("UserAccessModeSetting")), _
                                                     CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                     CInt(enUserPriviledge.AllowToApproveRejectEmployeeMemberships), _
                                                     enScreenName.frmMembershipInfoList, CStr(Session("EmployeeAsOnDate")), _
                                                     CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                     Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.ADDED, , cboEmployee.SelectedValue.ToString(), , , _
                                                     "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " ", mdTran, False, clsEmployeeAddress_approval_tran.enAddressType.NONE, "")
                    End If

                    If mintmembershiptranunkid > 0 Then
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("Membership Successfully " & strKey, Me, "wPgEmpMembership_List.aspx")
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 2007, "Membership Successfully ") & strKey, Me, "wPgEmpMembership_List.aspx")
                        'Hemant (31 May 2019) -- End
                    Else
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("Membership Successfully " & strKey, Me)
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 2007, "Membership Successfully ") & strKey, Me)
                        'Hemant (31 May 2019) -- End
                    End If


                Else
                    msg.DisplayMessage("Problem in " & strKey & "Membership.", Me)
                End If
            Else
                objEmpMembership._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
                mdTran = objEmpMembership._DataList

                If mintmembershiptranunkid <= 0 Then
                    Dim dtMemRow As DataRow
                    dtMemRow = mdTran.NewRow
                    dtMemRow.Item("membershiptranunkid") = -1
                    dtMemRow.Item("employeeunkid") = cboEmployee.SelectedValue
                    dtMemRow.Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
                    dtMemRow.Item("membershipunkid") = CInt(cboMembership.SelectedValue)
                    dtMemRow.Item("membershipno") = txtMembershipNo.Text
                    If dtpIssueDate.IsNull = False Then
                        dtMemRow.Item("issue_date") = dtpIssueDate.GetDate.Date
                    Else
                        dtMemRow.Item("issue_date") = DBNull.Value
                    End If

                    If dtpStartDate.IsNull = False Then
                        dtMemRow.Item("start_date") = dtpStartDate.GetDate.Date
                    Else
                        dtMemRow.Item("start_date") = DBNull.Value
                    End If

                    If dtpEndDate.IsNull = False Then
                        dtMemRow.Item("expiry_date") = dtpEndDate.GetDate.Date
                    Else
                        dtMemRow.Item("expiry_date") = DBNull.Value
                    End If
                    dtMemRow.Item("remark") = txtRemark.Text
                    dtMemRow.Item("isactive") = True
                    dtMemRow.Item("AUD") = "A"
                    dtMemRow.Item("GUID") = Guid.NewGuid().ToString
                    If objMembership._ETransHead_Id > 0 Or objMembership._CTransHead_Id > 0 Then
                        dtMemRow.Item("emptrnheadid") = objMembership._ETransHead_Id
                        dtMemRow.Item("cotrnheadid") = objMembership._CTransHead_Id
                        dtMemRow.Item("effetiveperiodid") = CInt(cboEffectivePeriod.SelectedValue)
                        dtMemRow.Item("copyedslab") = chkCopyPreviousEDSlab.Checked
                        dtMemRow.Item("overwriteslab") = False
                        dtMemRow.Item("overwritehead") = chkOverwriteHeads.Checked
                    Else
                        dtMemRow.Item("emptrnheadid") = -1
                        dtMemRow.Item("cotrnheadid") = -1
                        dtMemRow.Item("effetiveperiodid") = -1
                    End If
                    mdTran.Rows.Add(dtMemRow)
                Else
                    Dim dtMemRow() As DataRow = mdTran.Select("membershiptranunkid = '" & mintmembershiptranunkid & "'")
                    If dtMemRow.Length <= 0 Then Exit Sub

                    dtMemRow(0).Item("membershiptranunkid") = mintmembershiptranunkid
                    dtMemRow(0).Item("employeeunkid") = cboEmployee.SelectedValue
                    dtMemRow(0).Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
                    dtMemRow(0).Item("membershipunkid") = CInt(cboMembership.SelectedValue)
                    dtMemRow(0).Item("membershipno") = txtMembershipNo.Text
                    If dtpIssueDate.IsNull = False Then
                        dtMemRow(0).Item("issue_date") = dtpIssueDate.GetDate.Date
                    Else
                        dtMemRow(0).Item("issue_date") = DBNull.Value
                    End If

                    If dtpStartDate.IsNull = False Then
                        dtMemRow(0).Item("start_date") = dtpStartDate.GetDate.Date
                    Else
                        dtMemRow(0).Item("start_date") = DBNull.Value
                    End If

                    If dtpEndDate.IsNull = False Then
                        dtMemRow(0).Item("expiry_date") = dtpEndDate.GetDate.Date
                    Else
                        dtMemRow(0).Item("expiry_date") = DBNull.Value
                    End If
                    dtMemRow(0).Item("remark") = txtRemark.Text
                    dtMemRow(0).Item("isactive") = True
                    dtMemRow(0).Item("AUD") = "U"
                    dtMemRow(0).Item("GUID") = Guid.NewGuid().ToString
                    dtMemRow(0).Item("emptrnheadid") = dtMemRow(0).Item("emptrnheadid")
                    dtMemRow(0).Item("cotrnheadid") = dtMemRow(0).Item("cotrnheadid")
                    dtMemRow(0).Item("effetiveperiodid") = dtMemRow(0).Item("effetiveperiodid")
                    dtMemRow(0).Item("copyedslab") = dtMemRow(0).Item("copyedslab")
                    dtMemRow(0).Item("overwriteslab") = dtMemRow(0).Item("overwriteslab")
                    dtMemRow(0).Item("overwritehead") = dtMemRow(0).Item("overwritehead")
                End If
                If mintmembershiptranunkid <= 0 Then
                    objEmpMembership._DataList = mdTran
                    If objEmpMembership.InsertUpdateDelete_MembershipTran(CStr(Session("Database_Name")), _
                                                                          CInt(Session("UserId")), _
                                                                          CInt(Session("Fin_year")), _
                                                                          CInt(Session("CompanyUnkId")), _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                          CStr(Session("UserAccessModeSetting")), True, _
                                                                          CBool(Session("IsIncludeInactiveEmp")), _
                                                                          CBool(Session("AllowToApproveEarningDeduction")), _
                                                                          ConfigParameter._Object._CurrentDateAndTime, Nothing) = False Then

                        msg.DisplayMessage("Problem in Inserting Membership.", Me)
                    Else
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("Membership Successfully Inserted.", Me)
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 2008, "Membership Successfully Inserted."), Me)
                        'Hemant (31 May 2019) -- End
                    End If
                Else
                    objEmpMembership._DataList = mdTran
                    If objEmpMembership.InsertUpdateDelete_MembershipTran(CStr(Session("Database_Name")), _
                                                                          CInt(Session("UserId")), _
                                                                          CInt(Session("Fin_year")), _
                                                                          CInt(Session("CompanyUnkId")), _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                          CStr(Session("UserAccessModeSetting")), True, _
                                                                          CBool(Session("IsIncludeInactiveEmp")), _
                                                                          CBool(Session("AllowToApproveEarningDeduction")), _
                                                                          ConfigParameter._Object._CurrentDateAndTime, Nothing) = False Then
                        msg.DisplayMessage("Problem in updating Membership.", Me)
                    Else
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("Membership Successfully Updated.", Me)
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 2009, "Membership Successfully Updated."), Me)
                        'Hemant (31 May 2019) -- End
                        Session("membershiptranunkid") = Nothing
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpMembership_List.aspx", False)
                    End If
                End If
            End If
            Call ClearObjects()
            'S.SANDEEP |15-APR-2019| -- END
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |15-APR-2019| -- END


    'S.SANDEEP [31 AUG 2015] -- END

#End Region

#Region " Button's Event(s) "

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Try
            If isValidData() = False Then Exit Sub
            'S.SANDEEP [31 AUG 2015] -- START
            'ENHANCEMENT : -------------- LEFT OUT CODE


            'Dim mdTran As New DataTable
            'objEmpMembership._EmployeeUnkid = cboEmployee.SelectedValue
            'mdTran = objEmpMembership._DataList
            ''Nilay (31-Aug-2015) -- Start
            ''If Me.ViewState("Unkid") <= 0 Then
            'If mintmembershiptranunkid <= 0 Then
            '    'Nilay (31-Aug-2015) -- End
            '    Dim dtMemRow As DataRow
            '    dtMemRow = mdTran.NewRow
            '    dtMemRow.Item("membershiptranunkid") = -1
            '    dtMemRow.Item("employeeunkid") = cboEmployee.SelectedValue
            '    dtMemRow.Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
            '    dtMemRow.Item("membershipunkid") = CInt(cboMembership.SelectedValue)
            '    dtMemRow.Item("membershipno") = txtMembershipNo.Text
            '    If dtpIssueDate.IsNull = False Then
            '        dtMemRow.Item("issue_date") = dtpIssueDate.GetDate.Date
            '    Else
            '        dtMemRow.Item("issue_date") = DBNull.Value
            '    End If

            '    If dtpStartDate.IsNull = False Then
            '        dtMemRow.Item("start_date") = dtpStartDate.GetDate.Date
            '    Else
            '        dtMemRow.Item("start_date") = DBNull.Value
            '    End If

            '    If dtpEndDate.IsNull = False Then
            '        dtMemRow.Item("expiry_date") = dtpEndDate.GetDate.Date
            '    Else
            '        dtMemRow.Item("expiry_date") = DBNull.Value
            '    End If

            '    dtMemRow.Item("remark") = txtRemark.Text
            '    dtMemRow.Item("isactive") = True
            '    dtMemRow.Item("AUD") = "A"
            '    dtMemRow.Item("GUID") = Guid.NewGuid().ToString

            '    mdTran.Rows.Add(dtMemRow)

            'Else

            '    'Nilay (31-Aug-2015) -- Start
            '    'Dim dtMemRow() As DataRow = mdTran.Select("membershiptranunkid = '" & Me.ViewState("Unkid") & "'")
            '    Dim dtMemRow() As DataRow = mdTran.Select("membershiptranunkid = '" & mintmembershiptranunkid & "'")
            '    'Nilay (31-Aug-2015) -- End
            '    If dtMemRow.Length <= 0 Then Exit Sub
            '    'Nilay (31-Aug-2015) -- Start
            '    'dtMemRow(0).Item("membershiptranunkid") = Me.ViewState("Unkid")
            '    dtMemRow(0).Item("membershiptranunkid") = mintmembershiptranunkid
            '    'Nilay (31-Aug-2015) -- End
            '    dtMemRow(0).Item("employeeunkid") = cboEmployee.SelectedValue
            '    dtMemRow(0).Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
            '    dtMemRow(0).Item("membershipunkid") = CInt(cboMembership.SelectedValue)
            '    dtMemRow(0).Item("membershipno") = txtMembershipNo.Text
            '    If dtpIssueDate.IsNull = False Then
            '        dtMemRow(0).Item("issue_date") = dtpIssueDate.GetDate.Date
            '    Else
            '        dtMemRow(0).Item("issue_date") = DBNull.Value
            '    End If

            '    If dtpStartDate.IsNull = False Then
            '        dtMemRow(0).Item("start_date") = dtpStartDate.GetDate.Date
            '    Else
            '        dtMemRow(0).Item("start_date") = DBNull.Value
            '    End If

            '    If dtpEndDate.IsNull = False Then
            '        dtMemRow(0).Item("expiry_date") = dtpEndDate.GetDate.Date
            '    Else
            '        dtMemRow(0).Item("expiry_date") = DBNull.Value
            '    End If

            '    dtMemRow(0).Item("remark") = txtRemark.Text
            '    dtMemRow(0).Item("isactive") = True
            '    dtMemRow(0).Item("AUD") = "U"
            '    dtMemRow(0).Item("GUID") = Guid.NewGuid().ToString
            'End If
            ''Nilay (31-Aug-2015) -- Start
            ''If Me.ViewState("Unkid") <= 0 Then
            'If mintmembershiptranunkid <= 0 Then
            '    'Nilay (31-Aug-2015) -- End
            '    objEmpMembership._DataList = mdTran
            '    If objEmpMembership.InsertUpdateDelete_MembershipTran(Session("UserId")) = False Then
            '        msg.DisplayMessage("Problem in Inserting Membership.", Me)
            '    Else
            '        msg.DisplayMessage("Membership Successfully Inserted.", Me)
            '    End If
            '    Call ClearObjects()
            'Else
            '    objEmpMembership._DataList = mdTran
            '    If objEmpMembership.InsertUpdateDelete_MembershipTran(Session("UserId")) = False Then
            '        msg.DisplayMessage("Problem in updating Membership.", Me)
            '    Else
            '        msg.DisplayMessage("Membership Successfully Updated.", Me)
            '        'Nilay (31-Aug-2015) -- Start
            '        Session("membershiptranunkid") = Nothing
            '        'Nilay (31-Aug-2015) -- End

            '        'Nilay (02-Mar-2015) -- Start
            '        'Enhancement - REDESIGN SELF SERVICE.
            '        'Response.Redirect("~\HR\wPgEmpMembership_List.aspx")
            '        Response.Redirect(Session("rootpath") & "HR/wPgEmpMembership_List.aspx")
            '        'Nilay (02-Mar-2015) -- End
            '    End If
            'End If
            If mintmembershiptranunkid <= 0 Then
                Dim objMembership As New clsmembership_master
                Dim dtMRow() As DataRow = Nothing
                Dim mdtMemTran As New DataTable

                'S.SANDEEP |15-APR-2019| -- START
                'objMembership._Membershipunkid = CInt(cboMembership.SelectedValue)
                'objEmpMembership._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
                'mdtMemTran = objEmpMembership._DataList
                objMembership._Membershipunkid = CInt(cboMembership.SelectedValue)
                Dim blnIsApproved As Boolean = False
                Dim objEmp As New clsEmployee_Master : objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmployee.SelectedValue)
                blnIsApproved = objEmp._Isapproved : objEmp = Nothing
                If MembershipApprovalFlowVal Is Nothing AndAlso blnIsApproved = True Then
                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    If objApprovalData.IsApproverPresent(enScreenName.frmIdentityInfoList, CStr(Session("Database_Name")), _
                                                          CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                          CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeMemberships), _
                                                          CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(cboEmployee.SelectedValue.ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                        msg.DisplayMessage(objApprovalData._Message, Me)
                        Exit Sub
                    End If
                    'Gajanan [17-April-2019] -- End

                    objAMemInfoTran._Employeeunkid = CInt(cboEmployee.SelectedValue)
                    mdtMemTran = objAMemInfoTran._DataTable
                Else
                    objEmpMembership._EmployeeUnkid = CInt(cboEmployee.SelectedValue)
                    mdtMemTran = objEmpMembership._DataList
                End If
                'S.SANDEEP |15-APR-2019| -- END


                If objMembership._CTransHead_Id > 0 Then
                    dtMRow = mdtMemTran.Select("cotrnheadid = " & CInt(objMembership._CTransHead_Id) & " AND AUD <> 'D' AND isactive = True")
                    If dtMRow.Length > 0 Then
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 94, "Sorry, you cannot assign this membership to this employee.") & vbCrLf & _
                                                           Language.getMessage(mstrModuleName, 95, "Reason : Same Employer Contribution Head is mapped with other membership assigned to this employee."), Me)
                        Exit Sub
                    End If
                End If

                Dim iCurrPeriod As Integer = -1
                Dim objPdata As New clscommom_period_Tran
                Dim objMData As New clsMasterData


                'Pinkal (28-Dec-2015) -- Start
                'Enhancement - Working on Changes in SS for Employee Master.
                'iCurrPeriod = objMData.getFirstPeriodID(enModuleReference.Payroll, 1, Session("Fin_year"))
                iCurrPeriod = objMData.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
                'Pinkal (28-Dec-2015) -- End

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objPdata._Periodunkid = iCurrPeriod
                objPdata._Periodunkid(CStr(Session("Database_Name"))) = iCurrPeriod
                'Shani(20-Nov-2015) -- End


                Dim objProcessed As New clsTnALeaveTran
                If objProcessed.IsPayrollProcessDone(iCurrPeriod, CStr(cboEmployee.SelectedValue), objPdata._End_Date.Date) = True Then
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 84, "Sorry, You cannot assign this membership. Reason : Payroll is already processed for the current period."), Me)
                    Exit Sub
                End If
                objProcessed = Nothing

                If objMembership._CTransHead_Id > 0 Then
                    If objEmpMembership.Can_Assign_Membership(objMembership._CTransHead_Id, CInt(cboEmployee.SelectedValue)) = True Then
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 96, "Sorry, you cannot assign this membership to this employee.") & vbCrLf & _
                                                            Language.getMessage(mstrModuleName, 97, "Reason : Same Employer Contribution Head is mapped with other membership assigned to this employee."), Me)
                        Exit Sub
                    End If
                End If

                If objMembership._ETransHead_Id > 0 Or objMembership._CTransHead_Id > 0 Then
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 93, "This membership is mapped with transaction heads which will be posted to Earning and Deduction for this particular employee. If that head is flat rate then it will be posted with ZERO by default."), Me)

                    Dim objTHead As New clsTransactionHead
                    Dim objPeriod As New clscommom_period_Tran
                    Dim dsCombo As New DataSet

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "List", True, 1)
                    dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True, 1)
                    'Shani(20-Nov-2015) -- End

                    With cboEffectivePeriod
                        .DataValueField = "periodunkid"
                        .DataTextField = "name"
                        .DataSource = dsCombo.Tables(0)
                        .DataBind()
                        .SelectedValue = CStr(0)
                    End With
                    objPeriod = Nothing

                    txtAMembership.Text = cboMembership.SelectedItem.Text


                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objTHead._Tranheadunkid = objMembership._ETransHead_Id
                    objTHead._Tranheadunkid(CStr(Session("Database_Name"))) = objMembership._ETransHead_Id
                    'Shani(20-Nov-2015) -- End

                    txtEmpHead.Text = objTHead._Trnheadname


                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objTHead._Tranheadunkid = objMembership._CTransHead_Id
                    objTHead._Tranheadunkid(CStr(Session("Database_Name"))) = objMembership._CTransHead_Id
                    'Shani(20-Nov-2015) -- End

                    txtCoHead.Text = objTHead._Trnheadname

                    objTHead = Nothing

                    chkCopyPreviousEDSlab.Checked = False
                    chkOverwriteHeads.Checked = False

                    popup_active.Show()
                    Exit Sub
                End If
            End If
            Call SaveMembership()
            'S.SANDEEP [31 AUG 2015] -- END
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click, Closebutton1.CloseButton_click
    'SHANI [01 FEB 2015]--END 
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try

            'Nilay (31-Aug-2015) -- Start
            Session("membershiptranunkid") = Nothing
            'Nilay (31-Aug-2015) -- End

            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\UserHome.aspx", False)
            Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpMembership_List.aspx", False)
            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnClose_Click Event : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'S.SANDEEP [31 AUG 2015] -- START
    'ENHANCEMENT : -------------- LEFT OUT CODE
    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If CInt(IIf(cboEffectivePeriod.SelectedValue = "", 0, cboEffectivePeriod.SelectedValue)) <= 0 AndAlso cboEffectivePeriod.Enabled = True Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName2, 1, "Effective Period is mandatory information. Please select Effective Period to continue."), Me)
                popup_active.Show()
                Exit Sub
            End If

            If chkCopyPreviousEDSlab.Checked = True Then
                popup_yesno.Title = "Aruti"
                popup_yesno.Message = Language.getMessage(mstrModuleName2, 2, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?")
                popup_active.Show()
                popup_yesno.Show()
                Exit Sub
            End If
            Call SaveMembership()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub popup_yesno_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_yesno.buttonYes_Click
        Try
            Call SaveMembership()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP [31 AUG 2015] -- END
#End Region

#Region " Controls Events "

    Protected Sub cboMemCatagory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMemCategory.SelectedIndexChanged
        Try
            'If CInt(cboMemCategory.SelectedValue) > 0 Then
            Dim dsCombos As New DataSet
            Dim objMembership As New clsmembership_master

            dsCombos = objMembership.getListForCombo("Membership", True, CInt(cboMemCategory.SelectedValue))
            With cboMembership
                .DataValueField = "membershipunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Membership")
                .DataBind()
            End With
            'End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "ToolBar Event"
    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        Response.Redirect("~\HR\wPgEmpMembership_List.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END 
#End Region

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption("gbMembershipInfo", Me.Title)
        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebutton1.PageHeading = Language._Object.getCaption("gbMembershipInfo", Me.Closebutton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption("gbMembershipInfo", Me.lblPageHeader.Text)
        'SHANI [01 FEB 2015]--END 



        Me.lblMemIssueDate.Text = Language._Object.getCaption(Me.lblMemIssueDate.ID, Me.lblMemIssueDate.Text)
        Me.lblActivationDate.Text = Language._Object.getCaption(Me.lblActivationDate.ID, Me.lblActivationDate.Text)
        Me.lblMemExpiryDate.Text = Language._Object.getCaption(Me.lblMemExpiryDate.ID, Me.lblMemExpiryDate.Text)
        Me.lblMembershipName.Text = Language._Object.getCaption(Me.lblMembershipName.ID, Me.lblMembershipName.Text)
        Me.lblMembershipNo.Text = Language._Object.getCaption(Me.lblMembershipNo.ID, Me.lblMembershipNo.Text)
        Me.lblMembershipRemark.Text = Language._Object.getCaption(Me.lblMembershipRemark.ID, Me.lblMembershipRemark.Text)
        Me.lblMembershipType.Text = Language._Object.getCaption(Me.lblMembershipType.ID, Me.lblMembershipType.Text)


        Me.BtnSave.Text = Language._Object.getCaption("btnSaveInfo", Me.BtnSave.Text).Replace("&", "")
        Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")

        Language.setLanguage(mstrModuleName1)
        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage("clsMembershipTran", 1, "Active")
            Language.setMessage(mstrModuleName2, 2, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?")
            Language.setMessage(mstrModuleName, 43, "Start date cannot be less then or equal to Issue date.")
            Language.setMessage(mstrModuleName, 44, "End date cannot be less then or equal to Start date.")
            Language.setMessage(mstrModuleName, 46, "Membership is compulsory information, it can not be blank. Please select a Membership to continue.")
            Language.setMessage("clsEmployeeDataApproval", 62, "Newly Added Information")
            Language.setMessage("clsEmployeeDataApproval", 63, "Information Edited")
            Language.setMessage(mstrModuleName, 84, "Sorry, You cannot assign this membership. Reason : Payroll is already processed for the current period.")
            Language.setMessage(mstrModuleName, 93, "This membership is mapped with transaction heads which will be posted to Earning and Deduction for this particular employee. If that head is flat rate then it will be posted with ZERO by default.")
            Language.setMessage(mstrModuleName, 94, "Sorry, you cannot assign this membership to this employee.")
            Language.setMessage(mstrModuleName, 95, "Reason : Same Employer Contribution Head is mapped with other membership assigned to this employee.")
            Language.setMessage(mstrModuleName, 96, "Sorry, you cannot assign this membership to this employee.")
            Language.setMessage(mstrModuleName, 97, "Reason : Same Employer Contribution Head is mapped with other membership assigned to this employee.")
            Language.setMessage(mstrModuleName, 2002, "Employee is mandatory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2006, "Sorry, this Membership is already present for the selected employee. Please add new Membership to continue.")
            Language.setMessage(mstrModuleName, 2007, "Membership Successfully")
            Language.setMessage(mstrModuleName, 2008, "Membership Successfully Inserted.")
            Language.setMessage(mstrModuleName, 2009, "Membership Successfully Updated.")
            Language.setMessage(mstrModuleName2, 1, "Effective Period is mandatory information. Please select Effective Period to continue.")

        Catch Ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
