﻿<%@ Page Title="Employee Identity" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgEmpIdentities.aspx.vb" Inherits="wPgEmpIdentities" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%--S.SANDEEP |26-APR-2019| -- START--%>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%--S.SANDEEP |26-APR-2019| -- END--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <%--S.SANDEEP |26-APR-2019| -- START--%>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <%--S.SANDEEP |26-APR-2019| -- END--%>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <%--S.SANDEEP |26-APR-2019| -- START    --%>

    <script type="text/javascript">
        function IsValidAttach() {
            debugger;
            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= popup_AttachementYesNo.ClientID %>_Panel1").css("z-index", "100002");
        }
        
    </script>

    <%--S.SANDEEP |26-APR-2019| -- END--%>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 40%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Identity"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Identity Add/Edit"></asp:Label>
                                    </div>
                                    <%--S.SANDEEP |26-APR-2019| -- START--%>
                                    <asp:LinkButton ID="lnkScanAttachPersonal" runat="server" Text="Attach Document"
                                        Style="padding: 2px; font-weight: bold; margin-top: -3px; float: right;" Font-Underline="false"></asp:LinkButton>
                                    <%--'S.SANDEEP |26-APR-2019| -- END--%>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblIdType" runat="server" Text="Id Type"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboIdType" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblIdentityNo" runat="server" Text="Id. No."></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:TextBox ID="txtIdNo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblIdIssueCountry" runat="server" Text="Issue Country"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:DropDownList ID="cboCountry" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblIdSerialNo" runat="server" Text="Id Serial No"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:TextBox ID="txtIdSrNo" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblIdPlaceOfIssue" runat="server" Text="Place of Issue"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:TextBox ID="txtIssuePlace" runat="server" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblDLClass" runat="server" Text="DL Class"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <asp:TextBox ID="txtDLClass" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblIdIssueDate" runat="server" Text="Issue Date"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <uc2:DateCtrl ID="dtpIssueDate" runat="server" AutoPostBack="false" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                <asp:Label ID="lblIdExpiryDate" runat="server" Text="Expiry Date"></asp:Label>
                                            </td>
                                            <td style="width: 75%">
                                                <uc2:DateCtrl ID="dtpExpiryDate" runat="server" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 25%">
                                                &nbsp;
                                            </td>
                                            <td style="width: 75%">
                                                <asp:CheckBox ID="chkMakeAsDefault" runat="server" Text="Mark As Default" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="btndefault" />
                                        <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                    <%--S.SANDEEP |26-APR-2019| -- START--%>
                                    <div id="ScanAttachment">
                                        <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="ModalPopupBG"
                                            TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" DropShadow="true"
                                            CancelControlID="hdf_ScanAttchment">
                                        </cc1:ModalPopupExtender>
                                        <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="newpopup" Width="600px"
                                            Style="display: none;">
                                            <div class="panel-primary" style="margin: 0px">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div36" class="panel-default">
                                                        <div id="Div37" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 30%">
                                                                        <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 40%">
                                                                        <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server"
                                                                            Width="200px">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <td style="width: 30%">
                                                                        <asp:Panel ID="pnl_ImageAdd" runat="server">
                                                                            <div id="fileuploader">
                                                                                <input type="button" id="btnAddFile" runat="server" class="btndefault" value="Browse"
                                                                                    onclick="return IsValidAttach();" />
                                                                            </div>
                                                                        </asp:Panel>
                                                                        <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                                                            Text="Browse" />
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td colspan="3" style="width: 100%">
                                                                        <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                            HeaderStyle-Font-Bold="false" Width="99%">
                                                                            <Columns>
                                                                                <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                                                                ToolTip="Delete"></asp:LinkButton>
                                                                                        </span>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <%--0--%>
                                                                                <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                                                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                                                <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                                                    <ItemTemplate>
                                                                                        <span class="gridiconbc">
                                                                                            <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                                                        </span>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateColumn>
                                                                                <%--1--%>
                                                                                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                                                <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                                                <%--2--%>
                                                                                <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                                                <%--3--%>
                                                                                <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                                                <%--4--%>
                                                                            </Columns>
                                                                        </asp:DataGrid>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                                <div style="float: left">
                                                                    <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btnDefault" />
                                                                </div>
                                                                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                                <asp:Button ID="btnScanSave" runat="server" Text="Add" CssClass="btnDefault" />
                                                                <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btnDefault" />
                                                                <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <ucCfnYesno:Confirmation ID="popup_AttachementYesNo" runat="server" Message="" Title="Confirmation"
                                            IsFireButtonNoClick="false" />
                                    </div>
                                    <%--S.SANDEEP |26-APR-2019| -- END--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                <Triggers>
                    <asp:PostBackTrigger ControlID="dgv_Attchment" />
                    <asp:PostBackTrigger ControlID="btnDownloadAll" />
                </Triggers>
                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
    <%--S.SANDEEP |26-APR-2019| -- START--%>

    <script type="text/javascript">
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPgEmpIdentities.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $('input[type=file]').live("click", function() {
            debugger;
            return IsValidAttach();
        });
        
    </script>

    <%--S.SANDEEP |26-APR-2019| -- END--%>
</asp:Content>
