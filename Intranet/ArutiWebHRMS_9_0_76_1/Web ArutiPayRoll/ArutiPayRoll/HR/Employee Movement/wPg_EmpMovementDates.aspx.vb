﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Drawing
Imports System

#End Region

Partial Class HR_wPg_EmpMovementDates
    Inherits Basepage

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmEmployeeDates"

    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private objEDates As New clsemployee_dates_tran
    'Gajanan [4-Sep-2020] -- End

    Dim DisplayMessage As New CommonCodes

    Private xCurrentDates As Dictionary(Of Integer, String)
    Private xMasterType As Integer = 0
    Private mintDatesTypeId As enEmp_Dates_Transaction
    Private mstrEmployeeCode As String = ""
    Private mintEmployeeID As Integer = 0

    'Gajanan [12-NOV-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}


    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private objADate As New clsDates_Approval_Tran
    'Gajanan [4-Sep-2020] -- End


    'Gajanan [12-NOV-2018] -- END

    'Gajanan [15-NOV-2018] -- START
    Private mintTransactionId As Integer = 0
    'Gajanan [15-NOV-2018] -- END

    'S.SANDEEP |17-JAN-2019| -- START
    'ENHANCEMENT:Movement Approval Flow Edit / Delete
    Private mdtMovementDateList As DataTable
    'S.SANDEEP |17-JAN-2019| -- END

    'S.SANDEEP |10-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {#0003807}
    Private mintEmployeeUnkid As Integer = 0
    Private mstrEmployeeName As String = String.Empty
    'S.SANDEEP |10-MAY-2019| -- END

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mstrAdvanceFilter As String = ""
    'Gajanan [11-Dec-2019] -- End


    'Gajanan [18-May-2020] -- Start
    'Enhancement:Discipline Module Enhancement NMB
    Private mblnpopupEmployeeDiscipline As Boolean = False

    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private objDiscChargeMaster As New clsDiscipline_file_master
    'Private objDiscChargeTran As New clsDiscipline_file_tran
    'Gajanan [4-Sep-2020] -- End


    Private mdtCharges As DataTable
    Private mintDisciplineunkid As Integer = -1
    'Gajanan [18-May-2020] -- End

#End Region

#Region "Page's Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            SetLanguage()

            'Gajanan [12-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            objlblCaption.Visible = False
            lblPendingData.Visible = False
            'Gajanan [12-NOV-2018] -- END

            If Not IsPostBack Then

                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                GC.Collect()
                'Gajanan [4-Sep-2020] -- End

                If Request.QueryString.Count <= 0 Then Exit Sub
                Dim mstrRequset As String = ""
                Try
                    mstrRequset = b64decode(Server.UrlDecode(Request.QueryString.ToString.Substring(3)))
                Catch
                    Session("clsuser") = Nothing
                    DisplayMessage.DisplayMessage("Invalid Page Address.", Me, "../../index.aspx")
                    Exit Sub
                End Try
                If mstrRequset.Trim.Length > 0 AndAlso mstrRequset.Trim.Contains("|") Then
                    mintDatesTypeId = CType(CInt(mstrRequset.Substring(0, mstrRequset.IndexOf("|"))), enEmp_Dates_Transaction)
                    mintEmployeeID = CInt(mstrRequset.Substring(mstrRequset.IndexOf("|") + 1))
                Else
                    mintDatesTypeId = CType(CInt(mstrRequset), enEmp_Dates_Transaction)
                End If
                Call Set_Form_Controls()
                Call FillCombo()
                If mintEmployeeID > 0 Then
                    cboEmployee.SelectedValue = CStr(mintEmployeeID)
                    cboEmployee_SelectedIndexChanged(sender, e)
                End If
                Fill_Grid()
                lblAppointmentdate.Visible = False
                txtDate.Visible = False
                'Sohail (09 Oct 2019) -- Start
                'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
                lblPaymentDate.Visible = False
                txtPaymentDate.Visible = False
                'Sohail (09 Oct 2019) -- End
            Else
                mintDatesTypeId = CType(Me.ViewState("mintDatesTypeId"), enEmp_Dates_Transaction)
                mstrEmployeeCode = CStr(Me.ViewState("EmployeeCode"))
                'Gajanan [15-NOV-2018] -- START
                mintTransactionId = CInt(Me.ViewState("mintTransactionId"))
                'Gajanan [15-NOV-2018] -- End

                'S.SANDEEP |17-JAN-2019| -- START
                'ENHANCEMENT:Movement Approval Flow Edit / Delete
                mdtMovementDateList = CType(Me.ViewState("mdtMovementDateList"), DataTable)
                'S.SANDEEP |17-JAN-2019| -- END

                'S.SANDEEP |10-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {#0003807}
                mintEmployeeUnkid = CInt(Me.ViewState("mintEmployeeUnkid"))
                mstrEmployeeName = CStr(Me.ViewState("mstrEmployeeName"))
                'S.SANDEEP |10-MAY-2019| -- END
                'Hemant (01 Aug 2019) -- Start
                'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES.
                xMasterType = CInt(Me.ViewState("xMasterType"))
                'Hemant (01 Aug 2019) -- End


                'Gajanan [18-May-2020] -- Start
                'Enhancement:Discipline Module Enhancement NMB
                mblnpopupEmployeeDiscipline = CBool(ViewState("mblnpopupEmployeeDiscipline"))
                'Gajanan [18-May-2020] -- End

            End If


            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            If mblnpopupEmployeeDiscipline Then
                popupViewDiscipline.Show()
            End If
            'Gajanan [18-May-2020] -- End

            SetVisibility()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mintDatesTypeId", mintDatesTypeId)
            Me.ViewState.Add("EmployeeCode", mstrEmployeeCode)
            'Gajanan [15-NOV-2018] -- START
            Me.ViewState.Add("mintTransactionId", mintTransactionId)
            'Gajanan [15-NOV-2018] -- END

            'S.SANDEEP |17-JAN-2019| -- START
            'ENHANCEMENT:Movement Approval Flow Edit / Delete
            Me.ViewState.Add("mdtMovementDateList", mdtMovementDateList)
            'S.SANDEEP |17-JAN-2019| -- END

            'S.SANDEEP |10-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : {#0003807}
            Me.ViewState("mintEmployeeUnkid") = mintEmployeeUnkid
            Me.ViewState("mstrEmployeeName") = mstrEmployeeName
            'S.SANDEEP |10-MAY-2019| -- END
            'Hemant (01 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES.
            Me.ViewState("xMasterType") = xMasterType
            'Hemant (01 Aug 2019) -- End


            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            ViewState("mblnpopupEmployeeDiscipline") = mblnpopupEmployeeDiscipline
            'Gajanan [18-May-2020] -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End            
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreInit:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
            Dim objEmployee As New clsEmployee_Master
            Dim objCMaster As New clsCommon_Master
            Dim objELReason As New clsAction_Reason
            Dim dsComboList As New DataSet
        'Gajanan [4-Sep-2020] -- End

        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
            '    dsComboList = objEmployee.GetEmployeeList("Emp", True, False)
            'Else
            '    If ConfigParameter._Object._IsIncludeInactiveEmp Then
            '        dsComboList = objEmployee.GetEmployeeList("Emp", True, True)
            '    Else
            '        dsComboList = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
            '    End If
            'End If


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB



            'Dim blnActiveEmp As Boolean = True
            'If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
            '    'blnActiveEmp = False

            '    'S.SANDEEP [09-JAN-2017] -- START
            '    'ISSUE/ENHANCEMENT : #0001836
            '    blnActiveEmp = False
            '    'S.SANDEEP [09-JAN-2017] -- END                
            'Else
            '    blnActiveEmp = True
            'End If


            'dsComboList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
            '                                CInt(Session("UserId")), _
            '                                CInt(Session("Fin_year")), _
            '                                CInt(Session("CompanyUnkId")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                CStr(Session("UserAccessModeSetting")), True, _
            '                                blnActiveEmp, "Emp", True, , , , , , , , , , , , , , , , True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END
            ''Shani(24-Aug-2015) -- End

            'With cboEmployee
            '    'Nilay (09-Aug-2016) -- Start
            '    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            '    '.DataTextField = "employeename"
            '    .DataTextField = "EmpCodeName"
            '    'Nilay (09-Aug-2016) -- End
            '    .DataValueField = "employeeunkid"
            '    .DataSource = dsComboList.Tables("Emp")
            '    .DataBind()
            '    .SelectedValue = CStr(0)
            'End With

            FillEmployeeCombo()
            'Gajanan [11-Dec-2019] -- End


            dsComboList = objCMaster.getComboList(CType(xMasterType, clsCommon_Master.enCommonMaster), True, "List")
            With cboChangeReason
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            'dsComboList = objELReason.getComboList("List", True, False)
            'With cboEndEmplReason
            '    .DataValueField = "actionreasonunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsComboList.Tables("List")
            '    .DataBind()
            '    .SelectedValue = 0
            'End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objEmployee = Nothing
            objCMaster = Nothing
            objELReason = Nothing
            If IsNothing(dsComboList) = False Then
                dsComboList.Clear()
                dsComboList = Nothing
            End If
            'Gajanan [4-Sep-2020] -- End

        End Try
    End Sub

    Private Sub Set_Form_Controls()
        Try
            chkExclude.Checked = False : chkExclude.Enabled = False
            Select Case mintDatesTypeId

                Case enEmp_Dates_Transaction.DT_PROBATION
                    lblDetialHeader.Text = Language.getMessage(mstrModuleName, 100, "Probation Date Information")
                    xMasterType = clsCommon_Master.enCommonMaster.PROBATION
                    objlblStartFrom.Text = Language.getMessage(mstrModuleName, 101, "From Date")
                    objlblEndTo.Text = Language.getMessage(mstrModuleName, 102, "To Date")
                    pnl_OprationMenu.Visible = False

                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    lblDetialHeader.Text = Language.getMessage(mstrModuleName, 103, "Suspension Date Information")
                    xMasterType = clsCommon_Master.enCommonMaster.SUSPENSION
                    objlblStartFrom.Text = Language.getMessage(mstrModuleName, 101, "From Date")
                    objlblEndTo.Text = Language.getMessage(mstrModuleName, 102, "To Date")
                    pnl_OprationMenu.Visible = False

                Case enEmp_Dates_Transaction.DT_TERMINATION
                    lblDetialHeader.Text = Language.getMessage(mstrModuleName, 104, "Termination Date Information")
                    objlblStartFrom.Text = Language.getMessage(mstrModuleName, 105, "EOC Date")
                    objlblEndTo.Text = Language.getMessage(mstrModuleName, 106, "Leaving Date")
                    xMasterType = clsCommon_Master.enCommonMaster.TERMINATION
                    cboEndEmplReason.Enabled = True
                    'Hemant (22 June 2019) -- Start
                    'ISSUE/ENHANCEMENT : NMB PAYROLL UAT CHANGES.
                    'chkExclude.Checked = True : chkExclude.Enabled = True
                    chkExclude.Checked = False : chkExclude.Enabled = True
                    'Hemant (22 June 2019) -- End
                    pnl_OprationMenu.Visible = False


                    'Pinkal (07-Mar-2020) -- Start
                    'Enhancement - Changes Related to Payroll UAT for NMB.
                    LblActualDate.Visible = True
                    dtpActualDate.Visible = True
                    dtpActualDate.SetDate = Nothing
                    'Pinkal (07-Mar-2020) -- End

                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    lblDetialHeader.Text = Language.getMessage(mstrModuleName, 107, "Retirement Date Information")
                    objlblStartFrom.Text = Language.getMessage(mstrModuleName, 108, "Retirement Date")
                    dtpEndDate.Enabled = False
                    objlblEndTo.Text = ""
                    xMasterType = clsCommon_Master.enCommonMaster.RETIREMENTS
                    dgvHistory.Columns(5).Visible = False
                    pnl_OprationMenu.Visible = False

                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    lblDetialHeader.Text = Language.getMessage(mstrModuleName, 109, "Confirmation Date Information")
                    objlblStartFrom.Text = Language.getMessage(mstrModuleName, 110, "Confirmation Date")
                    dtpEndDate.Enabled = False
                    objlblEndTo.Text = ""
                    xMasterType = clsCommon_Master.enCommonMaster.CONFIRMATION
                    dgvHistory.Columns(5).Visible = False
                    pnl_OprationMenu.Visible = True

                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    lblDetialHeader.Text = Language.getMessage(mstrModuleName, 13, "Exemption Date Information")
                    xMasterType = clsCommon_Master.enCommonMaster.EXEMPTION
                    objlblStartFrom.Text = Language.getMessage(mstrModuleName, 101, "From Date")
                    objlblEndTo.Text = Language.getMessage(mstrModuleName, 102, "To Date")
                    pnl_OprationMenu.Visible = False
                    'Sohail (21 Oct 2019) -- End

            End Select

            dgvHistory.Columns(4).HeaderText = objlblStartFrom.Text
            dgvHistory.Columns(5).HeaderText = objlblEndTo.Text

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Set_Form_Controls:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsData As New DataSet

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objEDates As clsemployee_dates_tran
        Dim objADate As clsDates_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End


            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objEDates = New clsemployee_dates_tran
            objADate = New clsDates_Approval_Tran
            'Gajanan [4-Sep-2020] -- End

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                dsData = objEDates.GetList("List", CType(mintDatesTypeId, enEmp_Dates_Transaction), CDate(Session("fin_startdate")), CInt(cboEmployee.SelectedValue)).Clone
            Else
                dsData = objEDates.GetList("List", CType(mintDatesTypeId, enEmp_Dates_Transaction), CDate(Session("fin_startdate")), CInt(cboEmployee.SelectedValue))

                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                Dim dcol As New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "tranguid"
                    .DefaultValue = ""
                End With
                dsData.Tables(0).Columns.Add(dcol)

                'S.SANDEEP |17-JAN-2019| -- START
                dcol = New DataColumn
                With dcol
                    .DataType = GetType(System.Int32)
                    .ColumnName = "operationtypeid"
                    .DefaultValue = clsEmployeeMovmentApproval.enOperationType.ADDED
                End With
                dsData.Tables(0).Columns.Add(dcol)

                dcol = New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "OperationType"
                    .DefaultValue = ""
                End With
                dsData.Tables(0).Columns.Add(dcol)
                'S.SANDEEP |17-JAN-2019| -- End

                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim dsPending As New DataSet
                    dsPending = objADate.GetList("List", CType(mintDatesTypeId, enEmp_Dates_Transaction), CDate(Session("fin_startdate")), CInt(cboEmployee.SelectedValue))
                    If dsPending.Tables(0).Rows.Count > 0 Then
                        For Each row As DataRow In dsPending.Tables(0).Rows
                            dsData.Tables(0).ImportRow(row)
                        Next
                    End If
                End If
                'Gajanan [12-NOV-2018] -- END

            End If

            dgvHistory.AutoGenerateColumns = False
            dgvHistory.DataSource = dsData.Tables(0)
            dgvHistory.DataBind()
            'S.SANDEEP |17-JAN-2019| -- START
            'ENHANCEMENT:Movement Approval Flow Edit / Delete
            mdtMovementDateList = dsData.Tables(0)
            'S.SANDEEP |17-JAN-2019| -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Grid:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objEDates = Nothing
            objADate = Nothing
        End Try
    End Sub


    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    Private Sub Set_Dates(ByVal objEDates As clsemployee_dates_tran)
        'Gajanan [4-Sep-2020] -- End

        'Gajanan [18-May-2020] -- Start
        'Enhancement:Discipline Module Enhancement NMB
        Dim objCommon As New clsCommon_Master
        'Gajanan [18-May-2020] -- End

        Try
            Dim dsDate As New DataSet
            dsDate = objEDates.Get_Current_Dates(Now.Date, mintDatesTypeId, CInt(cboEmployee.SelectedValue))
            If dsDate.Tables(0).Rows.Count > 0 Then
                dtpStartDate.SetDate = CDate(dsDate.Tables(0).Rows(0).Item("date1"))
                If IsDBNull(dsDate.Tables(0).Rows(0).Item("date2")) = False Then
                    dtpEndDate.SetDate = CDate(dsDate.Tables(0).Rows(0).Item("date2"))
                End If
            End If

            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            mintDisciplineunkid = -1
            'Gajanan [02-June-2020] -- Start



            'dsDate = objCommon.getDesciplineRefNoComboList(mintEmployeeUnkid, "refnoList")
            Dim objDiscFileMaster As New clsDiscipline_file_master

            'dsComboList = objEmployee.GetEmployeeList(, _
            '                                , _
            '                                , _
            '                                , _
            '                                , _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                , True, _
            '                                blnActiveEmp, "Emp", True, , , , , , , , , , , , , , , , True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END

            dsDate = objDiscFileMaster.GetList(CStr(Session("Database_Name")), _
                                               CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                               CInt(Session("CompanyUnkId")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               CStr(Session("UserAccessModeSetting")), True, _
                                               CBool(Session("IsIncludeInactiveEmp")), "refnoList", , _
                                               "  hrdiscipline_file_master.involved_employeeunkid = '" & mintEmployeeUnkid & "'")
            'Gajanan [02-June-2020] -- End


            If IsNothing(dsDate.Tables("refnoList")) = False AndAlso dsDate.Tables("refnoList").Rows.Count > 0 Then
                lnkSetSuspension.Enabled = True
            Else
                lnkSetSuspension.Enabled = False
            End If
            'Gajanan [18-May-2020] -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Set_Dates:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objCommon = Nothing
        End Try
    End Sub


    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    Private Function Valid_Dates(ByVal objEDates As clsemployee_dates_tran) As Boolean
        'Gajanan [4-Sep-2020] -- End
        Try
            If dtpEffectiveDate.IsNull Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue."), Me)
                dtpEffectiveDate.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, Employee is mandatory information. Please selecte Employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            Select Case mintDatesTypeId
                Case enEmp_Dates_Transaction.DT_PROBATION
                    If dtpStartDate.IsNull AndAlso dtpEndDate.IsNull Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Sorry, Probation start date and end date are mandatory information. Please provide start date and end date."), Me)
                        dtpStartDate.Focus()
                        Return False
                    End If
                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    If dtpStartDate.IsNull AndAlso dtpEndDate.IsNull Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "Sorry, Suspension start date and end date are mandatory information. Please provide start date and end date."), Me)
                        dtpStartDate.Focus()
                        Return False
                    End If

                    'S.SANDEEP |02-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
                    If dtpEndDate.IsNull = False AndAlso dtpStartDate.IsNull = True Then
                        eZeeMsgBox.Show(Language.getMessage("frmEmployeeMaster", 166, "Sorry, suspension from date is mandatory information when suspended to date is ticked."), enMsgBoxStyle.Information)
                        dtpStartDate.Focus()
                        Return False
                    End If
                    'S.SANDEEP |02-MAR-2020| -- END


                    'Pinkal (09-Apr-2015) -- Start
                    'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA

                    'Case enEmp_Dates_Transaction.DT_TERMINATION
                    '    If dtpStartDate.IsNull AndAlso dtpEndDate.IsNull Then
                    '        Language.setLanguage(mstrModuleName)
                    '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Sorry, EOC date and Leaving date are mandatory information. Please provide eoc date and leaving date."), Me)
                    '        dtpStartDate.Focus()
                    '        Return False
                    '    End If

                    'Pinkal (09-Apr-2015) -- End

                    'Pinkal (28-Dec-2015) -- Start
                    'Enhancement - Working on Changes in SS for Employee Master.

                Case enEmp_Dates_Transaction.DT_TERMINATION

                    Dim objEmployee As New clsEmployee_Master
                    If dtpStartDate.IsNull = False Then
                        'Sohail (09 Oct 2019) -- Start
                        'NMB Enhancement # : show employee end date or eoc / leaving date which is less on Payslips.
                        'If objEmployee.IsEmpTerminated(dtpStartDate.GetDate.Date, CInt(cboEmployee.SelectedValue), CStr(Session("Database_Name")), CInt(Session("Fin_year"))) = False Then
                        If objEmployee.IsEmpTerminated(dtpStartDate.GetDate.Date, CInt(cboEmployee.SelectedValue), CStr(Session("Database_Name")), CInt(Session("Fin_year")), blnSkipForLastClosedPeriod:=CBool(Session("AllowToChangeEOCLeavingDateOnClosedPeriod"))) = False Then
                            'Sohail (09 Oct 2019) -- End
                            DisplayMessage.DisplayMessage(objEmployee._Message, Me)
                            Return False
                        End If
                    End If

                    If dtpEndDate.IsNull = False Then
                        'Sohail (09 Oct 2019) -- Start
                        'NMB Enhancement # : show employee end date or eoc / leaving date which is less on Payslips.
                        'If objEmployee.IsEmpTerminated(dtpEndDate.GetDate.Date, CInt(cboEmployee.SelectedValue), CStr(Session("Database_Name")), CInt(Session("Fin_year"))) = False Then
                        If objEmployee.IsEmpTerminated(dtpEndDate.GetDate.Date, CInt(cboEmployee.SelectedValue), CStr(Session("Database_Name")), CInt(Session("Fin_year")), blnSkipForLastClosedPeriod:=CBool(Session("AllowToChangeEOCLeavingDateOnClosedPeriod"))) = False Then
                            'Sohail (09 Oct 2019) -- End
                            DisplayMessage.DisplayMessage(objEmployee._Message, Me)
                            Exit Function
                        End If
                    End If
                    objEmployee = Nothing
                    'Pinkal (28-Dec-2015) -- End

                    'S.SANDEEP [22-MAR-2017] -- START
                    'ISSUE/ENHANCEMENT : Tanapa PE Forms Statutory Report
                    Dim objTnALeaveTran As New clsTnALeaveTran
                    Dim objMData As New clsMasterData : Dim iPeriod As Integer = 0
                    iPeriod = objMData.getCurrentPeriodID(enModuleReference.Payroll, dtpStartDate.GetDate, CInt(Session("Fin_year")), enStatusType.OPEN, True)
                    If iPeriod <= 0 Then
                        objMData.getCurrentPeriodID(enModuleReference.Payroll, dtpEndDate.GetDate, CInt(Session("Fin_year")), enStatusType.OPEN, True)
                    End If
                    'Sohail (09 Oct 2019) -- Start
                    'NMB Enhancement # : show employee end date or eoc / leaving date which is less on Payslips.
                    'If iPeriod > 0 Then
                    'Sohail (13 Jan 2022) -- Start
                    'Enhancement : OLD-216 : Delink the setting "Allow to Terminate Employee If Payment is Done" with privilege "Allow to change EOC on closed period" and allow the setting to terminate employee if payment is done even for users who don’t have that privilege.
                    'If iPeriod > 0 AndAlso CBool(Session("AllowToChangeEOCLeavingDateOnClosedPeriod")) = True Then
                    'Sohail (21 Jan 2022) -- Start
                    'Enhancement : OLD-216 : Delink the setting "Allow to Terminate Employee If Payment is Done" with privilege "Allow to change EOC on closed period" and allow the setting to terminate employee if payment is done even for users who don’t have that privilege.
                    'If iPeriod > 0 AndAlso (CBool(Session("AllowToChangeEOCLeavingDateOnClosedPeriod")) = True OrElse CBool(Session("AllowTerminationIfPaymentDone")) = True) Then
                    If iPeriod > 0 Then
                        'Sohail ((21 Jan 2022) -- End
                        'Sohail (13 Jan 2022) -- End
                        'Sohail (09 Oct 2019) -- End
                        Dim objPrd As New clscommom_period_Tran
                        objPrd._Periodunkid(CStr(Session("Database_Name"))) = iPeriod
                        'S.SANDEEP |27-JUN-2020| -- START
                        'ISSUE/ENHANCEMENT : NMB ENHANCEMENT
                        'If objTnALeaveTran.IsPayrollProcessDone(iPeriod, CStr(cboEmployee.SelectedValue), objPrd._End_Date, enModuleReference.Payroll) Then
                        '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Sorry, you cannot exclude selected employee. Reason:Payroll Process already done for the employee for last date of current open period."), Me)
                        '    Return False
                        'End If
                        If CBool(Session("AllowTerminationIfPaymentDone")) = False Then
                        If objTnALeaveTran.IsPayrollProcessDone(iPeriod, CStr(cboEmployee.SelectedValue), objPrd._End_Date, enModuleReference.Payroll) Then
                            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Sorry, you cannot exclude selected employee. Reason:Payroll Process already done for the employee for last date of current open period."), Me)
                            Return False
                        End If
                    End If
                        'S.SANDEEP |27-JUN-2020| -- END

                    End If
                    objTnALeaveTran = Nothing
                    'S.SANDEEP [22-MAR-2017] -- END

                    'S.SANDEEP [28-Feb-2018] -- START
                    'ISSUE/ENHANCEMENT : {#0002032}
                    If objEDates.IsPendingSalaryIncrement(CInt(cboEmployee.SelectedValue), dtpStartDate.GetDate.Date) = True Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 111, "Sorry, You cannot terminate this employee. Reason there are pending salary increment information present."), Me)
                        Return False
                    End If
                    'S.SANDEEP [28-Feb-2018] -- END

                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    If dtpStartDate.IsNull Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 8, "Sorry, Retirement date is mandatory information. Please provide retirement date."), Me)
                        dtpStartDate.Focus()
                        Return False
                    End If

                    'Pinkal (28-Dec-2015) -- Start
                    'Enhancement - Working on Changes in SS for Employee Master.
                    Dim objEmployee As New clsEmployee_Master
                    If objEmployee.IsEmpTerminated(dtpStartDate.GetDate.Date, CInt(cboEmployee.SelectedValue), CStr(Session("Database_Name")), CInt(Session("Fin_year")), True) = False Then
                        DisplayMessage.DisplayMessage(objEmployee._Message, Me)
                        Return False
                    End If
                    objEmployee = Nothing
                    'Pinkal (28-Dec-2015) -- End

                    'S.SANDEEP [28-Feb-2018] -- START
                    'ISSUE/ENHANCEMENT : {#0002032}
                    If objEDates.IsPendingSalaryIncrement(CInt(cboEmployee.SelectedValue), dtpStartDate.GetDate.Date) = True Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 112, "Sorry, You cannot retire this employee. Reason there are pending salary increment information present."), Me)
                        Return False
                    End If
                    'S.SANDEEP [28-Feb-2018] -- END

                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    If dtpStartDate.IsNull Then
                        Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Sorry, Exemption date is mandatory information. Please provide Exemption date."), Me)
                        dtpStartDate.Focus()
                        Return False
                    End If
                    'Sohail (21 Oct 2019) -- End

            End Select


            If dtpEffectiveDate.GetDate.Date > dtpStartDate.GetDate.Date Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(objlblStartFrom.Text & " " & Language.getMessage(mstrModuleName, 10, "cannot be less then effective date."), Me)
                dtpStartDate.Focus()
                Return False
            End If

            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            'If dtpEndDate.Visible = True Then
            If dtpEndDate.Visible = True AndAlso dtpEndDate.IsNull = False Then
                'Sohail (21 Oct 2019) -- End
                If dtpEndDate.GetDate.Date < dtpStartDate.GetDate.Date Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 5, "Sorry, end date cannot be less then start date."), Me)
                    dtpEndDate.Focus()
                    Return False
                End If
            End If

            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            'If CInt(cboChangeReason.SelectedValue) <= 0 Then
            If CInt(cboChangeReason.SelectedValue) <= 0 AndAlso cboChangeReason.Enabled = True Then
                'Sohail (21 Oct 2019) -- End
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Sorry, Change Reason is mandatory information. Please select Change Reason to continue."), Me)
                cboChangeReason.Focus()
                Return False
            End If

            If CDate(Me.ViewState("AppointmentDate")) <> Nothing Then
                'ToString(Session("DateFormat"))
                If CDate(Me.ViewState("AppointmentDate")).Date <> dtpEffectiveDate.GetDate.Date Then
                    Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 9, "Sorry, This is system generate entry. Please set effective date as employee appointment date."), Me)
                    dtpEffectiveDate.Focus()
                    Return False
                End If
            End If

            'TODO <CHECK APPOINT DATE FOR RETIRE AND LEAVING>


            'Gajanan [12-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                Dim objPMovement As New clsEmployeeMovmentApproval

                Dim intPrivilegeId As Integer = 0
                Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                Select Case mintDatesTypeId
                    Case enEmp_Dates_Transaction.DT_PROBATION
                        intPrivilegeId = 1194 : eMovement = clsEmployeeMovmentApproval.enMovementType.PROBATION
                    Case enEmp_Dates_Transaction.DT_RETIREMENT
                        intPrivilegeId = 1198 : eMovement = clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                    Case enEmp_Dates_Transaction.DT_SUSPENSION
                        intPrivilegeId = 1196 : eMovement = clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                    Case enEmp_Dates_Transaction.DT_TERMINATION
                        intPrivilegeId = 1197 : eMovement = clsEmployeeMovmentApproval.enMovementType.TERMINATION
                    Case enEmp_Dates_Transaction.DT_CONFIRMATION
                        intPrivilegeId = 1195 : eMovement = clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                    Case enEmp_Dates_Transaction.DT_EXEMPTION
                        intPrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeExemption : eMovement = clsEmployeeMovmentApproval.enMovementType.EXEMPTION
                        'Sohail (21 Oct 2019) -- End
                End Select

                Dim strMsg As String = String.Empty
                strMsg = objPMovement.IsOtherMovmentApproverPresent(eMovement, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intPrivilegeId, CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString, CType(mintDatesTypeId, enEmp_Dates_Transaction), CInt(cboEmployee.SelectedValue))
                If strMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(strMsg, Me)
                    objPMovement = Nothing
                    Return False
                End If
                objPMovement = Nothing

                'S.SANDEEP |16-JAN-2019| -- START
                If objEDates.isExist(dtpEffectiveDate.GetDate(), Nothing, Nothing, mintDatesTypeId, CInt(cboEmployee.SelectedValue), mintTransactionId) Then
                    Select Case mintDatesTypeId
                        Case enEmp_Dates_Transaction.DT_PROBATION
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 1, "Sorry, porbation information is already present for the selected effective date."), Me)
                        Case enEmp_Dates_Transaction.DT_CONFIRMATION
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 16, "Sorry, confirmation information is already present for the selected effective date."), Me)
                        Case enEmp_Dates_Transaction.DT_SUSPENSION
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 2, "Sorry, suspension information is already present for the selected effective date."), Me)
                        Case enEmp_Dates_Transaction.DT_TERMINATION
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 3, "Sorry, termination information is already present for the selected effective date."), Me)
                        Case enEmp_Dates_Transaction.DT_REHIRE
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 17, "Sorry, re-hire information is already present for the selected effective date."), Me)
                        Case enEmp_Dates_Transaction.DT_RETIREMENT
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 4, "Sorry, retirement information is already present for the selected effective date."), Me)
                        Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 19, "Sorry, appointment information is already present for the selected effective date."), Me)
                        Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 20, "Sorry, birthdate information is already present for the selected effective date."), Me)
                        Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 21, "Sorry, first appointment date information is already present for the selected effective date."), Me)
                        Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 22, "Sorry, anniversary information is already present for the selected effective date."), Me)
                            'Sohail (21 Oct 2019) -- Start
                            'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                        Case enEmp_Dates_Transaction.DT_EXEMPTION
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 27, "Sorry, exemption information is already present for the selected effective date."), Me)
                            'Sohail (21 Oct 2019) -- End
                    End Select
                    Return False
                End If

                If objEDates.isExist(Nothing, dtpStartDate.GetDate(), dtpEndDate.GetDate(), mintDatesTypeId, CInt(cboEmployee.SelectedValue), mintTransactionId) Then
                    Select Case mintDatesTypeId
                        Case enEmp_Dates_Transaction.DT_PROBATION
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 5, "Sorry, This probation date range is already defined. Please define new date range for this probation."), Me)
                        Case enEmp_Dates_Transaction.DT_CONFIRMATION
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 15, "Sorry, This confirmation information is already present."), Me)
                        Case enEmp_Dates_Transaction.DT_SUSPENSION
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 6, "Sorry, This suspension date range is already defined. Please define new date range for this suspension."), Me)
                        Case enEmp_Dates_Transaction.DT_TERMINATION
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 7, "Sorry, This termination information is already present."), Me)
                        Case enEmp_Dates_Transaction.DT_REHIRE
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 18, "Sorry, This re-hire information is already present."), Me)
                        Case enEmp_Dates_Transaction.DT_RETIREMENT
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 8, "Sorry, This retirement information is already present."), Me)
                        Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 23, "Sorry, This appointment date information is already present."), Me)
                        Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 24, "Sorry, This birthdate date information is already present."), Me)
                        Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 25, "Sorry, This first appointment date information is already present."), Me)
                        Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 26, "Sorry, This anniversary information is already present."), Me)
                            'Sohail (21 Oct 2019) -- Start
                            'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                        Case enEmp_Dates_Transaction.DT_EXEMPTION
                            DisplayMessage.DisplayMessage(Language.getMessage("clsemployee_dates_tran", 28, "Sorry, This exemption information is already present."), Me)
                            'Sohail (21 Oct 2019) -- End
                    End Select
                    Return False
                End If
                'S.SANDEEP |16-JAN-2019| -- END

            End If
            'Gajanan [12-NOV-2018] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
        Return True
    End Function


    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    Private Sub SetValue(ByVal objEDates As clsemployee_dates_tran, ByVal objADate As clsDates_Approval_Tran)
        'Gajanan [4-Sep-2020] -- End

        Try
            If Me.ViewState("Datestranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Datestranunkid")) > 0 Then
                objEDates._Datestranunkid = CInt(Me.ViewState("Datestranunkid"))
            End If
            'objEDates._Effectivedate = dtpEffectiveDate.GetDate.Date
            'objEDates._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'objEDates._Date1 = dtpStartDate.GetDate.Date

            'If dtpEndDate.Visible = True Then
            '    objEDates._Date2 = dtpEndDate.GetDate.Date
            'Else
            '    objEDates._Date2 = Nothing
            'End If
            'If mintDatesTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION Then
            '    objEDates._Isconfirmed = True
            'Else
            '    objEDates._Isconfirmed = False
            'End If

            'If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
            '    objEDates._Actionreasonunkid = CInt(cboChangeReason.SelectedValue)
            '    objEDates._Isexclude_payroll = chkExclude.Checked
            'End If

            'objEDates._Datetypeunkid = mintDatesTypeId

            'objEDates._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
            'objEDates._Isvoid = False
            'objEDates._Statusunkid = 0
            'objEDates._Userunkid = CInt(Session("UserId"))
            'objEDates._Voiddatetime = Nothing
            'objEDates._Voidreason = ""
            'objEDates._Voiduserunkid = -1

            'Blank_ModuleName()
            'objEDates._WebFormName = mstrModuleName
            'objEDates._WebClientIP = CStr(Session("IP_ADD"))
            'objEDates._WebHostName = CStr(Session("HOST_NAME"))
            'StrModuleName2 = "mnuGeneralMaster"
            'StrModuleName3 = "mnuCoreSetups"

            'Gajanan [12-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'Gajanan [15-NOV-2018] -- START
            'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
            If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                'Gajanan [15-NOV-2018] -- End
                objEDates._Effectivedate = dtpEffectiveDate.GetDate.Date
                objEDates._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objEDates._Date1 = dtpStartDate.GetDate.Date

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                'If dtpEndDate.Visible = True Then
                If dtpEndDate.Visible = True AndAlso dtpEndDate.IsNull = False Then
                    'Sohail (21 Oct 2019) -- End
                    objEDates._Date2 = dtpEndDate.GetDate.Date
                Else
                    objEDates._Date2 = Nothing
                End If
                If mintDatesTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION Then
                    objEDates._Isconfirmed = True
                Else
                    objEDates._Isconfirmed = False
                End If

                If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                    objEDates._Actionreasonunkid = CInt(cboChangeReason.SelectedValue)
                    objEDates._Isexclude_payroll = chkExclude.Checked
                End If

                objEDates._Datetypeunkid = mintDatesTypeId

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                'objEDates._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                If cboChangeReason.Enabled = True Then
                    objEDates._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                    objEDates._Leaveissueunkid = 0
                Else
                    objEDates._Changereasonunkid = CInt(cboChangeReason.Attributes("Tag"))
                    objEDates._Leaveissueunkid = CInt(lblReason.Attributes("Tag"))
                End If
                'Sohail (21 Oct 2019) -- End
                objEDates._Isvoid = False
                objEDates._Statusunkid = 0
                objEDates._Userunkid = CInt(Session("UserId"))
                objEDates._Voiddatetime = Nothing
                objEDates._Voidreason = ""
                objEDates._Voiduserunkid = -1

                'Pinkal (07-Mar-2020) -- Start
                'Enhancement - Changes Related to Payroll UAT for NMB.
                If dtpActualDate.IsNull = False Then
                    objEDates._ActualDate = dtpActualDate.GetDate
                End If
                'Pinkal (07-Mar-2020) -- End


                Blank_ModuleName()
                objEDates._WebFormName = mstrModuleName
                objEDates._WebClientIP = CStr(Session("IP_ADD"))
                objEDates._WebHostName = CStr(Session("HOST_NAME"))
                StrModuleName2 = "mnuGeneralMaster"
                StrModuleName3 = "mnuCoreSetups"
            Else

                objADate._Audittype = enAuditType.ADD
                objADate._Audituserunkid = CInt(Session("UserId"))
                objADate._Effectivedate = dtpEffectiveDate.GetDate
                objADate._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objADate._Date1 = dtpStartDate.GetDate

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                'If dtpEndDate.Visible = True Then
                If dtpEndDate.Visible = True AndAlso dtpEndDate.IsNull = False Then
                    'Sohail (21 Oct 2019) -- End
                    objADate._Date2 = dtpEndDate.GetDate
                Else
                    objADate._Date2 = Nothing
                End If

                If mintDatesTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION Then
                    objADate._Isconfirmed = True
                Else
                    objADate._Isconfirmed = False
                End If
                objADate._Datetypeunkid = mintDatesTypeId
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                'objADate._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                If cboChangeReason.Enabled = True Then
                    objADate._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                    objADate._Leaveissueunkid = 0
                Else
                    objADate._Changereasonunkid = CInt(cboChangeReason.Attributes("Tag"))
                    objADate._Leaveissueunkid = CInt(lblReason.Attributes("Tag"))
                End If
                'Sohail (21 Oct 2019) -- End
                objADate._Isvoid = False
                objADate._Statusunkid = 0
                objADate._Voiddatetime = Nothing
                objADate._Voidreason = ""
                objADate._Voiduserunkid = -1

                If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                    objADate._Actionreasonunkid = CInt(cboChangeReason.SelectedValue)
                    objADate._Isexclude_Payroll = chkExclude.Checked
                End If
                objADate._Isvoid = False
                objADate._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                objADate._Voiddatetime = Nothing
                objADate._Voidreason = ""
                objADate._Voiduserunkid = -1
                objADate._Tranguid = Guid.NewGuid.ToString()
                objADate._Transactiondate = Now
                objADate._Remark = ""
                objADate._Rehiretranunkid = 0
                objADate._Mappingunkid = 0
                objADate._Isweb = True
                objADate._Isfinal = False
                objADate._Ip = Session("IP_ADD").ToString()
                objADate._Hostname = Session("HOST_NAME").ToString()
                objADate._Form_Name = mstrModuleName

                'Pinkal (07-Mar-2020) -- Start
                'Enhancement - Changes Related to Payroll UAT for NMB.
                If dtpActualDate.IsNull = False Then
                    objADate._ActualDate = dtpActualDate.GetDate
                End If
                'Pinkal (07-Mar-2020) -- End

                'S.SANDEEP |17-JAN-2019| -- START
                If Me.ViewState("Datestranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Datestranunkid")) > 0 Then

                    'Pinkal (07-Mar-2020) -- Start
                    'Enhancement - Changes Related to Payroll UAT for NMB.
                    '  objADate._Datestranunkid = mintTransactionId
                    objADate._Datestranunkid = objEDates._Datestranunkid
                    'Pinkal (07-Mar-2020) -- End
                    objADate._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    objADate._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                'S.SANDEEP |17-JAN-2019| -- END

                Blank_ModuleName()
                StrModuleName2 = "mnuGeneralMaster"
                StrModuleName3 = "mnuCoreSetups"
            End If
            'Gajanan [12-NOV-2018] -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub


    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    Private Sub SetEditValue(ByVal objEDates As clsemployee_dates_tran)
        'Gajanan [4-Sep-2020] -- End
        Try
            If Me.ViewState("Datestranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Datestranunkid")) > 0 Then
                objEDates._Datestranunkid = CInt(Me.ViewState("Datestranunkid"))

                'Gajanan [15-NOV-2018] -- START
                mintTransactionId = objEDates._Datestranunkid
                'Gajanan [15-NOV-2018] -- END
            End If

            dtpEffectiveDate.SetDate = objEDates._Effectivedate
            cboEmployee.SelectedValue = CStr(objEDates._Employeeunkid)
            dtpStartDate.SetDate = objEDates._Date1

            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            'If dtpEndDate.Visible = True Then
            If dtpEndDate.Visible = True AndAlso objEDates._Date2 <> Nothing Then
                'Sohail (21 Oct 2019) -- End
                dtpEndDate.SetDate = objEDates._Date2
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            Else
                dtpEndDate.SetDate = Nothing
                'Sohail (21 Oct 2019) -- End
            End If
            cboEndEmplReason.SelectedValue = CStr(objEDates._Actionreasonunkid)
            chkExclude.Checked = objEDates._Isexclude_payroll
            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : Employee exemption start date cannot be changed if that transaction is from leave for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            'cboChangeReason.SelectedValue = CStr(objEDates._Changereasonunkid)
            If objEDates._Leaveissueunkid > 0 Then
                cboChangeReason.SelectedValue = "0"
                cboChangeReason.Attributes("Tag") = CInt(objEDates._Changereasonunkid).ToString
                lblReason.Attributes("Tag") = CInt(objEDates._Leaveissueunkid).ToString
                cboChangeReason.Enabled = False
                'objbtnAddReason.Enabled = False
                'objSearchReason.Enabled = False
                dtpEffectiveDate.Enabled = False
                dtpStartDate.Enabled = False
            Else
                cboChangeReason.SelectedValue = objEDates._Changereasonunkid.ToString
                cboChangeReason.Attributes("Tag") = "0"
                lblReason.Attributes("Tag") = "0" 'Leaveissueunkid
                cboChangeReason.Enabled = True
                'objbtnAddReason.Enabled = True
                'objSearchReason.Enabled = True
                dtpEffectiveDate.Enabled = True
                dtpStartDate.Enabled = True
            End If
            'Sohail (21 Oct 2019) -- End


            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            If dtpActualDate.Visible AndAlso objEDates._ActualDate <> Nothing Then
                dtpActualDate.SetDate = objEDates._ActualDate
            End If
            'Pinkal (07-Mar-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetEditValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            dtpEffectiveDate.SetDate = Nothing
            dtpStartDate.SetDate = Nothing
            dtpEndDate.SetDate = Nothing
            cboChangeReason.SelectedValue = CStr(0)
            txtDate.Text = ""
            lblAppointmentdate.Visible = False
            txtDate.Visible = False

            'Pinkal (07-Mar-2020) -- Start
            'Enhancement - Changes Related to Payroll UAT for NMB.
            dtpActualDate.SetDate = Nothing
            'Pinkal (07-Mar-2020) -- End

            'mdtAppointmentDate = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ClearControls:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub SetDatesForMail()
        Try
            xCurrentDates = New Dictionary(Of Integer, String)
            Select Case mintDatesTypeId
                Case enEmp_Dates_Transaction.DT_PROBATION
                    If dtpStartDate.IsNull = False AndAlso dtpEndDate.IsNull = False Then
                        xCurrentDates.Add(enEmployeeDates.PROBATION_DATE, dtpStartDate.GetDate.Date.ToShortDateString & " - " & dtpEndDate.GetDate.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.PROBATION_DATE, "&nbsp;")
                    End If
                Case enEmp_Dates_Transaction.DT_SUSPENSION
                    If dtpStartDate.IsNull = False AndAlso dtpEndDate.IsNull = False Then
                        xCurrentDates.Add(enEmployeeDates.SUSPENSION_DATE, dtpStartDate.GetDate.Date.ToShortDateString & " - " & dtpEndDate.GetDate.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.SUSPENSION_DATE, "&nbsp;")
                    End If
                Case enEmp_Dates_Transaction.DT_TERMINATION
                    If dtpStartDate.IsNull = False Then
                        xCurrentDates.Add(enEmployeeDates.EOC_DATE, dtpStartDate.GetDate.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.EOC_DATE, "&nbsp;")
                    End If

                    If dtpEndDate.IsNull = False Then
                        xCurrentDates.Add(enEmployeeDates.LEAVING_DATE, dtpEndDate.GetDate.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.LEAVING_DATE, "&nbsp;")
                    End If

                Case enEmp_Dates_Transaction.DT_RETIREMENT
                    If dtpStartDate.IsNull = False Then
                        xCurrentDates.Add(enEmployeeDates.RETIREMENT_DATE, dtpStartDate.GetDate.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.RETIREMENT_DATE, "&nbsp;")
                    End If

                Case enEmp_Dates_Transaction.DT_CONFIRMATION
                    If dtpStartDate.IsNull = False Then
                        xCurrentDates.Add(enEmployeeDates.CONFIRM_DATE, dtpStartDate.GetDate.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.CONFIRM_DATE, "&nbsp;")
                    End If

                    'Sohail (21 Oct 2019) -- Start
                    'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Case enEmp_Dates_Transaction.DT_EXEMPTION
                    If dtpStartDate.IsNull = False AndAlso dtpEndDate.IsNull = False Then
                        xCurrentDates.Add(enEmployeeDates.EXEMPTION_DATE, dtpStartDate.GetDate.Date.ToShortDateString & " - " & dtpEndDate.GetDate.Date.ToShortDateString)
                    ElseIf dtpStartDate.IsNull = False Then
                        xCurrentDates.Add(enEmployeeDates.EXEMPTION_DATE, dtpStartDate.GetDate.Date.ToShortDateString)
                    Else
                        xCurrentDates.Add(enEmployeeDates.EXEMPTION_DATE, "&nbsp;")
                    End If
                    'Sohail (21 Oct 2019) -- End

            End Select

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetDatesForMail:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            lnkSetSuspension.Visible = False

            If mintDatesTypeId = enEmp_Dates_Transaction.DT_PROBATION Then
                chkExclude.Visible = False

                'Shani (08-Dec-2016) -- Start
                'Enhancement -  Add Employee Allocaion/Date Privilage
                btnSave.Enabled = CBool(Session("SetEmpProbationDate"))
                'Shani (08-Dec-2016) -- End
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                dgvHistory.Columns(1).Visible = CBool(Session("AllowToEditProbationEmployeeDetails"))
                dgvHistory.Columns(2).Visible = CBool(Session("AllowToDeleteProbationEmployeeDetails"))
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}     
                dgvHistory.Columns(0).Visible = Not CBool(Session("SkipEmployeeMovementApprovalFlow"))
                'Gajanan [12-NOV-2018] -- End
                'Varsha Rana (17-Oct-2017) -- End

            ElseIf mintDatesTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION OrElse mintDatesTypeId = enEmp_Dates_Transaction.DT_RETIREMENT Then
                chkExclude.Visible = False
                objlblEndTo.Visible = False
                dtpEndDate.Visible = False

                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                dgvHistory.Columns(1).Visible = CBool(Session("AllowToEditRehireEmployeeDetails"))
                dgvHistory.Columns(2).Visible = CBool(Session("AllowToDeleteRehireEmployeeDetails"))
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}     
                dgvHistory.Columns(0).Visible = Not CBool(Session("SkipEmployeeMovementApprovalFlow"))

                'Gajanan [12-NOV-2018] -- End

                'Varsha Rana (17-Oct-2017) -- End

                'Shani (08-Dec-2016) -- Start
                'Enhancement -  Add Employee Allocaion/Date Privilage
                If mintDatesTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION Then
                    btnSave.Enabled = CBool(Session("ChangeConfirmationDate"))
                    'Varsha Rana (17-Oct-2017) -- Start
                    'Enhancement - Give user privileges.
                    dgvHistory.Columns(1).Visible = CBool(Session("AllowToEditConfirmationEmployeeDetails"))
                    dgvHistory.Columns(2).Visible = CBool(Session("AllowToDeleteConfirmationEmployeeDetails"))
                    'Gajanan [12-NOV-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#244}     
                    dgvHistory.Columns(0).Visible = Not CBool(Session("SkipEmployeeMovementApprovalFlow"))

                    'Gajanan [12-NOV-2018] -- End


                    mnuSalaryChange.Enabled = CBool(Session("AddSalaryIncrement"))
                    mnuBenefits.Enabled = CBool(Session("AllowToAssignBenefitGroup"))
                    mnuTransfers.Enabled = CBool(Session("AllowToChangeEmpTransfers"))
                    mnuRecategorization.Enabled = CBool(Session("AllowToChangeEmpRecategorize"))

                    'Varsha Rana (17-Oct-2017) -- End
                Else
                    btnSave.Enabled = CBool(Session("SetReinstatementdate"))
                End If
                'Shani (08-Dec-2016) -- End
            ElseIf mintDatesTypeId = enEmp_Dates_Transaction.DT_SUSPENSION Then
                chkExclude.Visible = False

                'Gajanan [18-May-2020] -- Start
                'Enhancement:Discipline Module Enhancement NMB
                lnkSetSuspension.Visible = True
                'Gajanan [18-May-2020] -- End

                'Shani (08-Dec-2016) -- Start
                'Enhancement -  Add Employee Allocaion/Date Privilage
                btnSave.Enabled = CBool(Session("SetEmpSuspensionDate"))

                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                dgvHistory.Columns(1).Visible = CBool(Session("AllowToEditSuspensionEmployeeDetails"))
                dgvHistory.Columns(2).Visible = CBool(Session("AllowToDeleteSuspensionEmployeeDetails"))

                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}     
                dgvHistory.Columns(0).Visible = Not CBool(Session("SkipEmployeeMovementApprovalFlow"))
                'Gajanan [12-NOV-2018] -- End

                'Varsha Rana (17-Oct-2017) -- End

            ElseIf mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                dtpStartDate.Enabled = CBool(Session("SetEmploymentEndDate"))
                dtpEndDate.Enabled = CBool(Session("SetLeavingDate"))
                If dtpStartDate.Enabled = False AndAlso dtpEndDate.Enabled = False Then
                    btnSave.Enabled = False
                End If
                'Shani (08-Dec-2016) -- End
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                dgvHistory.Columns(1).Visible = CBool(Session("AllowToEditTerminationEmployeeDetails"))
                dgvHistory.Columns(2).Visible = CBool(Session("AllowToDeleteTerminationEmployeeDetails"))

                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}     
                dgvHistory.Columns(0).Visible = Not CBool(Session("SkipEmployeeMovementApprovalFlow"))
                'Gajanan [12-NOV-2018] -- End
                'Varsha Rana (17-Oct-2017) -- End

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            ElseIf mintDatesTypeId = enEmp_Dates_Transaction.DT_EXEMPTION Then
                chkExclude.Visible = False
                btnSave.Enabled = CBool(Session("AllowToSetEmployeeExemptionDate"))

                dgvHistory.Columns(1).Visible = CBool(Session("AllowToEditEmployeeExemptionDate"))
                dgvHistory.Columns(2).Visible = CBool(Session("AllowToDeleteEmployeeExemptionDate"))

                dgvHistory.Columns(0).Visible = Not CBool(Session("SkipEmployeeMovementApprovalFlow"))
                'Sohail (21 Oct 2019) -- End

            End If

            'Sohail (09 Oct 2019) -- Start
            'NMB Enhancement # : show employee end date or eoc / retirement date which is less as Payment Date on employee eoc / retirement screen which is not be editable.
            lblPaymentDate.Visible = False
            txtPaymentDate.Visible = False
            If CInt(cboEmployee.SelectedValue) > 0 Then
                If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION OrElse mintDatesTypeId = enEmp_Dates_Transaction.DT_RETIREMENT Then
                    Dim objTnA As New clsTnALeaveTran
                    Dim ds As DataSet = objTnA.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, Session("UserAccessModeSetting").ToString, True, False, True, "List", CInt(cboEmployee.SelectedValue), 0, "prtnaleave_tran.processdate DESC")
                    If ds.Tables(0).Rows.Count > 0 Then
                        txtPaymentDate.Text = eZeeDate.convertDate(ds.Tables(0).Rows(0).Item("employee_enddate").ToString).ToShortDateString
                        lblPaymentDate.Visible = True
                        txtPaymentDate.Visible = True
                    End If
                End If
            End If
            'Sohail (09 Oct 2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetDatesForMail:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    Private Sub SaveEmployeeDates(ByVal objEDates As clsemployee_dates_tran, ByVal objADate As clsDates_Approval_Tran)
        'Gajanan [4-Sep-2020] -- End

        Dim blnFlag As Boolean = False
        Try
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            Call SetValue(objEDates, objADate)
            'Gajanan [4-Sep-2020] -- End

            If Me.ViewState("Datestranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Datestranunkid")) > 0 Then

                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                    blnFlag = objEDates.Update(CBool(Session("CreateADUserFromEmpMst")), CInt(Session("CompanyUnkId")), Nothing)
                Else
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 102, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 103, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 104, "Sorry, re-hire Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 105, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 106, "Sorry, appointment Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 107, "Sorry, birthdate Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 108, "Sorry, first appointment date Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 109, "Sorry, anniversary Information is already present in approval process with selected effective date and allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry, exemption Information is already present in approval process with selected effective date and allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If

                    blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 110, "Sorry, porbation Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 111, "Sorry, confirmation Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 112, "Sorry, suspension Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 113, "Sorry, termination Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 114, "Sorry, re-hire Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 115, "Sorry, retirement Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 116, "Sorry, appointment Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 117, "Sorry, birthdate Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 118, "Sorry, first appointment date Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 119, "Sorry, anniversary Information is already present in approval process with selected effective date."), Me)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, exemption Information is already present in approval process with selected effective date."), Me)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 120, "Sorry, porbation Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 121, "Sorry, confirmation Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 122, "Sorry, suspension Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 123, "Sorry, termination Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 124, "Sorry, re-hire Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 125, "Sorry, retirement Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 126, "Sorry, appointment Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 127, "Sorry, birthdate Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 128, "Sorry, first appointment date Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 129, "Sorry, anniversary Information is already present in approval process with selected allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, exemption Information is already present in approval process with selected allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 102, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 103, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 104, "Sorry, re-hire Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 105, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 106, "Sorry, appointment Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 107, "Sorry, birthdate Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 108, "Sorry, first appointment date Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 109, "Sorry, anniversary Information is already present in approval process with selected effective date and allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry, exemption Information is already present in approval process with selected effective date and allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If

                    blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 110, "Sorry, porbation Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 111, "Sorry, confirmation Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 112, "Sorry, suspension Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 113, "Sorry, termination Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 114, "Sorry, re-hire Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 115, "Sorry, retirement Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 116, "Sorry, appointment Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 117, "Sorry, birthdate Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 118, "Sorry, first appointment date Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 119, "Sorry, anniversary Information is already present in approval process with selected effective date."), Me)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, exemption Information is already present in approval process with selected effective date."), Me)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 120, "Sorry, porbation Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 121, "Sorry, confirmation Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 122, "Sorry, suspension Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 123, "Sorry, termination Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 124, "Sorry, re-hire Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 125, "Sorry, retirement Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 126, "Sorry, appointment Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 127, "Sorry, birthdate Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 128, "Sorry, first appointment date Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 129, "Sorry, anniversary Information is already present in approval process with selected allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, exemption Information is already present in approval process with selected allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    blnFlag = objADate.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED)

                    If blnFlag = False AndAlso objADate._Message <> "" Then
                        DisplayMessage.DisplayMessage(objADate._Message, Me)
                        Exit Sub
                    End If

                End If

            ElseIf Me.ViewState("Datestranunkid") Is Nothing OrElse CInt(Me.ViewState("Datestranunkid")) <= 0 Then

                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                    blnFlag = objEDates.Insert(CBool(Session("CreateADUserFromEmpMst")), CInt(Session("CompanyUnkId")), Nothing)
                Else
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 102, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 103, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 104, "Sorry, re-hire Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 105, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 106, "Sorry, appointment Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 107, "Sorry, birthdate Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 108, "Sorry, first appointment date Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 109, "Sorry, anniversary Information is already present in approval process with selected effective date and allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry, exemption Information is already present in approval process with selected effective date and allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If

                    blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 110, "Sorry, porbation Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 111, "Sorry, confirmation Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 112, "Sorry, suspension Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 113, "Sorry, termination Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 114, "Sorry, re-hire Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 115, "Sorry, retirement Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 116, "Sorry, appointment Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 117, "Sorry, birthdate Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 118, "Sorry, first appointment date Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 119, "Sorry, anniversary Information is already present in approval process with selected effective date."), Me)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, exemption Information is already present in approval process with selected effective date."), Me)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 120, "Sorry, porbation Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 121, "Sorry, confirmation Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 122, "Sorry, suspension Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 123, "Sorry, termination Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 124, "Sorry, re-hire Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 125, "Sorry, retirement Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 126, "Sorry, appointment Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 127, "Sorry, birthdate Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 128, "Sorry, first appointment date Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 129, "Sorry, anniversary Information is already present in approval process with selected allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, exemption Information is already present in approval process with selected allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 102, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 103, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 104, "Sorry, re-hire Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 105, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 106, "Sorry, appointment Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 107, "Sorry, birthdate Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 108, "Sorry, first appointment date Information is already present in approval process with selected effective date and allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 109, "Sorry, anniversary Information is already present in approval process with selected effective date and allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 16, "Sorry, exemption Information is already present in approval process with selected effective date and allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If

                    blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 110, "Sorry, porbation Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 111, "Sorry, confirmation Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 112, "Sorry, suspension Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 113, "Sorry, termination Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 114, "Sorry, re-hire Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 115, "Sorry, retirement Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 116, "Sorry, appointment Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 117, "Sorry, birthdate Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 118, "Sorry, first appointment date Information is already present in approval process with selected effective date."), Me)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 119, "Sorry, anniversary Information is already present in approval process with selected effective date."), Me)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 15, "Sorry, exemption Information is already present in approval process with selected effective date."), Me)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        Select Case mintDatesTypeId
                            Case enEmp_Dates_Transaction.DT_PROBATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 120, "Sorry, porbation Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_CONFIRMATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 121, "Sorry, confirmation Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_SUSPENSION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 122, "Sorry, suspension Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_TERMINATION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 123, "Sorry, termination Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_REHIRE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 124, "Sorry, re-hire Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_RETIREMENT
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 125, "Sorry, retirement Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 126, "Sorry, appointment Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_BIRTH_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 127, "Sorry, birthdate Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 128, "Sorry, first appointment date Information is already present in approval process with selected allocation combination."), Me)
                            Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 129, "Sorry, anniversary Information is already present in approval process with selected allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- Start
                                'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                            Case enEmp_Dates_Transaction.DT_EXEMPTION
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 17, "Sorry, exemption Information is already present in approval process with selected allocation combination."), Me)
                                'Sohail (21 Oct 2019) -- End
                        End Select

                        Exit Sub
                    End If


                    blnFlag = objADate.Insert(CInt(Session("CompanyUnkId")), clsEmployeeMovmentApproval.enOperationType.ADDED)

                    If blnFlag = False AndAlso objADate._Message <> "" Then
                        DisplayMessage.DisplayMessage(objADate._Message, Me)
                        Exit Sub
                    End If
                End If

            End If

            Dim intSelectedEmployee As Integer = 0
            If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                intSelectedEmployee = CInt(cboEmployee.SelectedValue)
            End If

            If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                Call FillCombo()
                cboEmployee.SelectedValue = "0"
            End If

            If blnFlag = False AndAlso objEDates._Message <> "" Then
                DisplayMessage.DisplayMessage(objEDates._Message, Me)
                Exit Sub
            Else
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                'Call Fill_Grid()
                'Sohail (21 Oct 2019) -- End

                'Gajanan [9-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                Call SetDatesForMail()
                'Gajanan [9-July-2019] -- End

                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then

                    'Pinkal (18-May-2021) -- Start
                    'Enhancement On Configuration -   Working on Configuration for Employee Dates Notification.
                    'Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, CStr(Session("Notify_Dates")), dtpEffectiveDate.GetDate.Date, CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("CompanyUnkId")))

                    Dim mstrUserNotificationIds As String = ""
                    Select Case mintDatesTypeId
                        Case enEmp_Dates_Transaction.DT_PROBATION
                            mstrUserNotificationIds = CStr(Session("ProbationDateUserNotification"))

                        Case enEmp_Dates_Transaction.DT_SUSPENSION
                            mstrUserNotificationIds = CStr(Session("SuspensionDateUserNotification"))

                        Case enEmp_Dates_Transaction.DT_TERMINATION
                            'EOC DATE
                            xCurrentDates.Clear()
                            If dtpStartDate.IsNull = False Then
                                xCurrentDates.Add(enEmployeeDates.EOC_DATE, dtpStartDate.GetDate.Date.ToShortDateString)
                            Else
                                xCurrentDates.Add(enEmployeeDates.EOC_DATE, "&nbsp;")
                            End If
                            Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, CStr(Session("EocDateUserNotification")), dtpEffectiveDate.GetDate.Date, CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("CompanyUnkId")))

                            'LEAVING DATE
                            xCurrentDates.Clear()
                            If dtpEndDate.IsNull = False Then
                                xCurrentDates.Add(enEmployeeDates.LEAVING_DATE, dtpEndDate.GetDate.Date.ToShortDateString)
                            Else
                                xCurrentDates.Add(enEmployeeDates.LEAVING_DATE, "&nbsp;")
                            End If
                            Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, CStr(Session("LeavingDateUserNotification")), dtpEffectiveDate.GetDate.Date, CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("CompanyUnkId")))

                        Case enEmp_Dates_Transaction.DT_RETIREMENT
                            mstrUserNotificationIds = CStr(Session("RetirementDateUserNotification"))

                        Case enEmp_Dates_Transaction.DT_CONFIRMATION
                            mstrUserNotificationIds = CStr(Session("ConfirmDateUserNotification"))

                        Case enEmp_Dates_Transaction.DT_EXEMPTION
                            mstrUserNotificationIds = CStr(Session("ExemptionDateUserNotification"))
                    End Select

                    If mintDatesTypeId <> enEmp_Dates_Transaction.DT_TERMINATION Then
                        Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, mstrUserNotificationIds, dtpEffectiveDate.GetDate.Date, CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("CompanyUnkId")))
                    End If

                    'Pinkal (18-May-2021) -- End


                Else
                    Dim objPMovement As New clsEmployeeMovmentApproval
                    Dim intPrivilegeId As Integer = 0
                    Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                    Select Case mintDatesTypeId
                        Case enEmp_Dates_Transaction.DT_PROBATION
                            intPrivilegeId = 1194 : eMovement = clsEmployeeMovmentApproval.enMovementType.PROBATION
                        Case enEmp_Dates_Transaction.DT_RETIREMENT
                            intPrivilegeId = 1198 : eMovement = clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                        Case enEmp_Dates_Transaction.DT_SUSPENSION
                            intPrivilegeId = 1196 : eMovement = clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                        Case enEmp_Dates_Transaction.DT_TERMINATION
                            intPrivilegeId = 1197 : eMovement = clsEmployeeMovmentApproval.enMovementType.TERMINATION
                        Case enEmp_Dates_Transaction.DT_CONFIRMATION
                            intPrivilegeId = 1195 : eMovement = clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                            'Sohail (21 Oct 2019) -- Start
                            'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                        Case enEmp_Dates_Transaction.DT_EXEMPTION
                            intPrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeExemption : eMovement = clsEmployeeMovmentApproval.enMovementType.EXEMPTION
                            'Sohail (21 Oct 2019) -- End
                    End Select

                    Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                    If Me.ViewState("Datestranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Datestranunkid")) > 0 Then
                        eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                    Else
                        eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                    End If

                    objPMovement.SendNotification(1, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intPrivilegeId, eMovement, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLogin_Mode.MGR_SELF_SERVICE, CStr(Session("UserName")), eOperType, False, CType(mintDatesTypeId, enEmp_Dates_Transaction), 0, intSelectedEmployee.ToString(), "")
                    objPMovement = Nothing
                End If

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Call Fill_Grid()
                'Sohail (21 Oct 2019) -- End

                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                Call Set_Dates(objEDates)
                'Gajanan [4-Sep-2020] -- End


            End If
            Me.ViewState("Datestranunkid") = Nothing
            Call ClearControls()
            cboEmployee.Enabled = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub FillEmployeeCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Try

            Dim blnActiveEmp As Boolean = True
            If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                blnActiveEmp = False
            Else
                blnActiveEmp = CBool(Session("IsIncludeInactiveEmp"))
            End If


            dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          blnActiveEmp, "Emp", True, , , , , , , , , , , , , , , mstrAdvanceFilter, True)

            With cboEmployee
                .DataTextField = "EmpCodeName"
                .DataValueField = "employeeunkid"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = CStr(0)
                .DataBind()
            End With
            'Gajanan [18-May-2020] -- Start
            'Enhancement:Discipline Module Enhancement NMB
            lnkSetSuspension.Enabled = False
            'Gajanan [18-May-2020] -- End

        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            ' DisplayError.Show("-1", ex.Message, "fillEmployeeCombo", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End            
        Finally
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End
#End Region

#Region "Button's Event"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objEDates As New clsemployee_dates_tran
        Dim objADate As New clsDates_Approval_Tran
        'Gajanan [4-Sep-2020] -- End


        Try

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            If Valid_Dates(objEDates) = False Then Exit Sub
            'Gajanan [4-Sep-2020] -- End

            'Hemant (01 Aug 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES.
            'Call SetValue()
            'If Me.ViewState("Datestranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Datestranunkid")) > 0 Then
            '    'Pinkal (18-Aug-2018) -- Start
            '    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            '    'blnFlag = objEDates.Update(CInt(Session("CompanyUnkId")), Nothing)

            '    'S.SANDEEP |17-JAN-2019| -- START
            '    'ENHANCEMENT:Movement Approval Flow Edit / Delete
            '    'blnFlag = objEDates.Update(CBool(Session("CreateADUserFromEmpMst")), CInt(Session("CompanyUnkId")), Nothing)
            '    If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
            '        blnFlag = objEDates.Update(CBool(Session("CreateADUserFromEmpMst")), CInt(Session("CompanyUnkId")), Nothing)
            '    Else
            '        'Check For Entry is Available In Approval For Delete
            '        blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
            '        If blnFlag Then
            '            Select Case mintDatesTypeId
            '                Case enEmp_Dates_Transaction.DT_PROBATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_SUSPENSION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 102, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_TERMINATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 103, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_REHIRE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 104, "Sorry, re-hire Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_RETIREMENT
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 105, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 106, "Sorry, appointment Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 107, "Sorry, birthdate Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 108, "Sorry, first appointment date Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 109, "Sorry, anniversary Information is already present in approval process with selected effective date and allocation combination."), Me)
            '            End Select

            '            Exit Sub
            '        End If

            '        blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
            '        If blnFlag Then
            '            Select Case mintDatesTypeId
            '                Case enEmp_Dates_Transaction.DT_PROBATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 110, "Sorry, porbation Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 111, "Sorry, confirmation Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_SUSPENSION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 112, "Sorry, suspension Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_TERMINATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 113, "Sorry, termination Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_REHIRE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 114, "Sorry, re-hire Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_RETIREMENT
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 115, "Sorry, retirement Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 116, "Sorry, appointment Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 117, "Sorry, birthdate Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 118, "Sorry, first appointment date Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 119, "Sorry, anniversary Information is already present in approval process with selected effective date."), Me)
            '            End Select

            '            Exit Sub
            '        End If


            '        blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
            '        If blnFlag Then
            '            Select Case mintDatesTypeId
            '                Case enEmp_Dates_Transaction.DT_PROBATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 120, "Sorry, porbation Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 121, "Sorry, confirmation Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_SUSPENSION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 122, "Sorry, suspension Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_TERMINATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 123, "Sorry, termination Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_REHIRE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 124, "Sorry, re-hire Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_RETIREMENT
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 125, "Sorry, retirement Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 126, "Sorry, appointment Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 127, "Sorry, birthdate Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 128, "Sorry, first appointment date Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 129, "Sorry, anniversary Information is already present in approval process with selected allocation combination."), Me)
            '            End Select

            '            Exit Sub
            '        End If


            '        'Check For Entry is Available In Approval For Edit

            '        blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
            '        If blnFlag Then
            '            Select Case mintDatesTypeId
            '                Case enEmp_Dates_Transaction.DT_PROBATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_SUSPENSION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 102, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_TERMINATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 103, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_REHIRE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 104, "Sorry, re-hire Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_RETIREMENT
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 105, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 106, "Sorry, appointment Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 107, "Sorry, birthdate Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 108, "Sorry, first appointment date Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 109, "Sorry, anniversary Information is already present in approval process with selected effective date and allocation combination."), Me)
            '            End Select

            '            Exit Sub
            '        End If

            '        blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
            '        If blnFlag Then
            '            Select Case mintDatesTypeId
            '                Case enEmp_Dates_Transaction.DT_PROBATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 110, "Sorry, porbation Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 111, "Sorry, confirmation Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_SUSPENSION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 112, "Sorry, suspension Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_TERMINATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 113, "Sorry, termination Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_REHIRE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 114, "Sorry, re-hire Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_RETIREMENT
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 115, "Sorry, retirement Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 116, "Sorry, appointment Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 117, "Sorry, birthdate Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 118, "Sorry, first appointment date Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 119, "Sorry, anniversary Information is already present in approval process with selected effective date."), Me)
            '            End Select

            '            Exit Sub
            '        End If


            '        blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
            '        If blnFlag Then
            '            Select Case mintDatesTypeId
            '                Case enEmp_Dates_Transaction.DT_PROBATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 120, "Sorry, porbation Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 121, "Sorry, confirmation Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_SUSPENSION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 122, "Sorry, suspension Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_TERMINATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 123, "Sorry, termination Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_REHIRE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 124, "Sorry, re-hire Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_RETIREMENT
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 125, "Sorry, retirement Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 126, "Sorry, appointment Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 127, "Sorry, birthdate Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 128, "Sorry, first appointment date Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 129, "Sorry, anniversary Information is already present in approval process with selected allocation combination."), Me)
            '            End Select

            '            Exit Sub
            '        End If


            '        blnFlag = objADate.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED)

            '        If blnFlag = False AndAlso objADate._Message <> "" Then
            '            DisplayMessage.DisplayMessage(objADate._Message, Me)
            '            Exit Sub
            '        End If

            '        'S.SANDEEP |17-JAN-2019| -- END
            '    End If

            '    'Pinkal (18-Aug-2018) -- End
            'ElseIf Me.ViewState("Datestranunkid") Is Nothing OrElse CInt(Me.ViewState("Datestranunkid")) <= 0 Then
            '    'Gajanan [12-NOV-2018] -- START
            '    'ISSUE/ENHANCEMENT : {Ref#244}    

            '    'Pinkal (18-Aug-2018) -- Start
            '    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            '    'blnFlag = objEDates.Insert(CInt(Session("CompanyUnkId")), Nothing)

            '    'blnFlag = objEDates.Insert(CBool(Session("CreateADUserFromEmpMst")), CInt(Session("CompanyUnkId")), Nothing)
            '    If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
            '        blnFlag = objEDates.Insert(CBool(Session("CreateADUserFromEmpMst")), CInt(Session("CompanyUnkId")), Nothing)
            '    Else
            '        'Check For Entry is Available In Approval For Delete
            '        blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
            '        If blnFlag Then
            '            Select Case mintDatesTypeId
            '                Case enEmp_Dates_Transaction.DT_PROBATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_SUSPENSION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 102, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_TERMINATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 103, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_REHIRE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 104, "Sorry, re-hire Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_RETIREMENT
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 105, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 106, "Sorry, appointment Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 107, "Sorry, birthdate Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 108, "Sorry, first appointment date Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 109, "Sorry, anniversary Information is already present in approval process with selected effective date and allocation combination."), Me)
            '            End Select

            '            Exit Sub
            '        End If

            '        blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
            '        If blnFlag Then
            '            Select Case mintDatesTypeId
            '                Case enEmp_Dates_Transaction.DT_PROBATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 110, "Sorry, porbation Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 111, "Sorry, confirmation Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_SUSPENSION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 112, "Sorry, suspension Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_TERMINATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 113, "Sorry, termination Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_REHIRE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 114, "Sorry, re-hire Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_RETIREMENT
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 115, "Sorry, retirement Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 116, "Sorry, appointment Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 117, "Sorry, birthdate Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 118, "Sorry, first appointment date Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 119, "Sorry, anniversary Information is already present in approval process with selected effective date."), Me)
            '            End Select

            '            Exit Sub
            '        End If


            '        blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
            '        If blnFlag Then
            '            Select Case mintDatesTypeId
            '                Case enEmp_Dates_Transaction.DT_PROBATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 120, "Sorry, porbation Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 121, "Sorry, confirmation Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_SUSPENSION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 122, "Sorry, suspension Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_TERMINATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 123, "Sorry, termination Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_REHIRE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 124, "Sorry, re-hire Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_RETIREMENT
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 125, "Sorry, retirement Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 126, "Sorry, appointment Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 127, "Sorry, birthdate Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 128, "Sorry, first appointment date Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 129, "Sorry, anniversary Information is already present in approval process with selected allocation combination."), Me)
            '            End Select

            '            Exit Sub
            '        End If


            '        'Check For Entry is Available In Approval For Edit

            '        blnFlag = objADate.isExist(objADate._Effectivedate, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
            '        If blnFlag Then
            '            Select Case mintDatesTypeId
            '                Case enEmp_Dates_Transaction.DT_PROBATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Sorry, porbation Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 101, "Sorry, confirmation Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_SUSPENSION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 102, "Sorry, suspension Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_TERMINATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 103, "Sorry, termination Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_REHIRE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 104, "Sorry, re-hire Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_RETIREMENT
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 105, "Sorry, retirement Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 106, "Sorry, appointment Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 107, "Sorry, birthdate Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 108, "Sorry, first appointment date Information is already present in approval process with selected effective date and allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 109, "Sorry, anniversary Information is already present in approval process with selected effective date and allocation combination."), Me)
            '            End Select

            '            Exit Sub
            '        End If

            '        blnFlag = objADate.isExist(objADate._Effectivedate, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
            '        If blnFlag Then
            '            Select Case mintDatesTypeId
            '                Case enEmp_Dates_Transaction.DT_PROBATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 110, "Sorry, porbation Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 111, "Sorry, confirmation Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_SUSPENSION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 112, "Sorry, suspension Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_TERMINATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 113, "Sorry, termination Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_REHIRE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 114, "Sorry, re-hire Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_RETIREMENT
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 115, "Sorry, retirement Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 116, "Sorry, appointment Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 117, "Sorry, birthdate Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 118, "Sorry, first appointment date Information is already present in approval process with selected effective date."), Me)
            '                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 119, "Sorry, anniversary Information is already present in approval process with selected effective date."), Me)
            '            End Select

            '            Exit Sub
            '        End If


            '        blnFlag = objADate.isExist(Nothing, objADate._Date1, objADate._Date2, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
            '        If blnFlag Then
            '            Select Case mintDatesTypeId
            '                Case enEmp_Dates_Transaction.DT_PROBATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 120, "Sorry, porbation Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 121, "Sorry, confirmation Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_SUSPENSION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 122, "Sorry, suspension Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_TERMINATION
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 123, "Sorry, termination Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_REHIRE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 124, "Sorry, re-hire Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_RETIREMENT
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 125, "Sorry, retirement Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_APPOINTED_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 126, "Sorry, appointment Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_BIRTH_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 127, "Sorry, birthdate Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_FIRST_APP_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 128, "Sorry, first appointment date Information is already present in approval process with selected allocation combination."), Me)
            '                Case enEmp_Dates_Transaction.DT_MARRIGE_DATE
            '                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 129, "Sorry, anniversary Information is already present in approval process with selected allocation combination."), Me)
            '            End Select

            '            Exit Sub
            '        End If


            '        blnFlag = objADate.Insert(CInt(Session("CompanyUnkId")), clsEmployeeMovmentApproval.enOperationType.ADDED)

            '        If blnFlag = False AndAlso objADate._Message <> "" Then
            '            DisplayMessage.DisplayMessage(objADate._Message, Me)
            '            Exit Sub
            '        End If
            '    End If
            '    'Pinkal (18-Aug-2018) -- End
            '    'Gajanan [12-NOV-2018] -- END

            'End If

            ''Gajanan [12-NOV-2018] -- START
            ''ISSUE/ENHANCEMENT : {Ref#244}
            'Dim intSelectedEmployee As Integer = 0
            'If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
            '    intSelectedEmployee = CInt(cboEmployee.SelectedValue)
            'End If
            ''Gajanan [12-NOV-2018] -- END

            ''S.SANDEEP [02-NOV-2017] -- START
            'If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
            '    Call FillCombo()
            '    cboEmployee.SelectedValue = "0"
            'End If
            ''S.SANDEEP [02-NOV-2017] -- END

            'If blnFlag = False AndAlso objEDates._Message <> "" Then
            '    DisplayMessage.DisplayMessage(objEDates._Message, Me)
            '    Exit Sub
            'Else
            '    Call Fill_Grid()
            '    'Gajanan [12-NOV-2018] -- START
            '    'ISSUE/ENHANCEMENT : {Ref#244}

            '    'Call SetDatesForMail()
            '    ''Sohail (30 Nov 2017) -- Start
            '    ''SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            '    ''Call objEDates.SendEmails(mintDatesTypeId, CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.SelectedItem.Text, xCurrentDates, CStr(Session("Notify_Dates")), dtpEffectiveDate.GetDate.Date, CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE)
            '    'Call objEDates.SendEmails(mintDatesTypeId, CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.SelectedItem.Text, xCurrentDates, CStr(Session("Notify_Dates")), dtpEffectiveDate.GetDate.Date, CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("CompanyUnkId")))
            '    ''Sohail (30 Nov 2017) -- End

            '    'Gajanan [15-NOV-2018] -- START
            '    'If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
            '    If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
            '        'Gajanan [15-NOV-2018] -- END

            '        'S.SANDEEP |10-MAY-2019| -- START
            '        'ISSUE/ENHANCEMENT : {#0003807}
            '        'Call objEDates.SendEmails(mintDatesTypeId, CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.SelectedItem.Text, xCurrentDates, CStr(Session("Notify_Dates")), dtpEffectiveDate.GetDate.Date, CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("CompanyUnkId")))
            '        Call objEDates.SendEmails(mintDatesTypeId, mintEmployeeUnkid, mstrEmployeeCode, mstrEmployeeName, xCurrentDates, CStr(Session("Notify_Dates")), dtpEffectiveDate.GetDate.Date, CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("CompanyUnkId")))
            '        'S.SANDEEP |10-MAY-2019| -- END
            '    Else
            '        Dim objPMovement As New clsEmployeeMovmentApproval
            '        Dim intPrivilegeId As Integer = 0
            '        Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
            '        Select Case mintDatesTypeId
            '            Case enEmp_Dates_Transaction.DT_PROBATION
            '                intPrivilegeId = 1194 : eMovement = clsEmployeeMovmentApproval.enMovementType.PROBATION
            '            Case enEmp_Dates_Transaction.DT_RETIREMENT
            '                intPrivilegeId = 1198 : eMovement = clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
            '            Case enEmp_Dates_Transaction.DT_SUSPENSION
            '                intPrivilegeId = 1196 : eMovement = clsEmployeeMovmentApproval.enMovementType.SUSPENSION
            '            Case enEmp_Dates_Transaction.DT_TERMINATION
            '                intPrivilegeId = 1197 : eMovement = clsEmployeeMovmentApproval.enMovementType.TERMINATION
            '            Case enEmp_Dates_Transaction.DT_CONFIRMATION
            '                intPrivilegeId = 1195 : eMovement = clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
            '        End Select

            '        'S.SANDEEP |17-JAN-2019| -- START
            '        'objPMovement.SendNotification(1, FinancialYear._Object._DatabaseName, ConfigParameter._Object._UserAccessModeSetting, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, intPrivilegeId, eMovement, ConfigParameter._Object._EmployeeAsOnDate, User._Object._Userunkid, mstrModuleName, enLogin_Mode.DESKTOP, User._Object._Username, False, CType(mintDatesTypeId, enEmp_Dates_Transaction), 0, intSelectedEmployee.ToString(), "")
            '        Dim eOperType As clsEmployeeMovmentApproval.enOperationType
            '        If Me.ViewState("Datestranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Datestranunkid")) > 0 Then
            '            eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
            '        Else
            '            eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
            '        End If

            '        objPMovement.SendNotification(1, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), intPrivilegeId, eMovement, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLogin_Mode.MGR_SELF_SERVICE, CStr(Session("UserName")), eOperType, False, CType(mintDatesTypeId, enEmp_Dates_Transaction), 0, intSelectedEmployee.ToString(), "")
            '        'S.SANDEEP |17-JAN-2019| -- END
            '        objPMovement = Nothing
            '    End If
            '    'Gajanan [12-NOV-2018] -- End


            '    Call Set_Dates()
            'End If
            'Me.ViewState("Datestranunkid") = Nothing
            'Call ClearControls()
            ''S.SANDEEP [07-Feb-2018] -- START
            ''ISSUE/ENHANCEMENT : {#0001988}
            'cboEmployee.Enabled = True
            ''S.SANDEEP [07-Feb-2018] -- END
            If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION AndAlso chkExclude.Checked = False Then
                Language.setLanguage(mstrModuleName)
                Confirmation.Message = Language.getMessage(mstrModuleName, 142, "Exclude from payroll process is not set.") & "<br>" & "<br>" & Language.getMessage(mstrModuleName, 143, "Are you sure you do not want to Exclude Payroll Process for this Employee?")
                Confirmation.Show()
            Else
                SaveEmployeeDates(objEDates, objADate)
            End If
            'Hemant (01 Aug 2019) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Gajanan [15-NOV-2018] -- START
            mintTransactionId = 0
            'Gajanan [15-NOV-2018] -- END
        End Try
    End Sub

    Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objEDates As New clsemployee_dates_tran
        Dim objADate As New clsDates_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try

            Blank_ModuleName()
            objEDates._WebFormName = mstrModuleName
            objEDates._WebClientIP = CStr(Session("IP_ADD"))
            objEDates._WebHostName = CStr(Session("HOST_NAME"))
            StrModuleName2 = "mnuGeneralMaster"
            StrModuleName3 = "mnuCoreSetups"

            objEDates._Isvoid = True
            objEDates._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objEDates._Voidreason = popup_DeleteReason.Reason
            objEDates._Voiduserunkid = CInt(Session("UserId"))
            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'If objEDates.Delete(CInt(Me.ViewState("Datestranunkid")), CInt(Session("CompanyUnkId")), Nothing) = False Then

            'S.SANDEEP |17-JAN-2019| -- START
            'If objEDates.Delete(CBool(Session("CreateADUserFromEmpMst")), CInt(Me.ViewState("Datestranunkid")), CInt(Session("CompanyUnkId")), Nothing) = False Then
            '    'Pinkal (18-Aug-2018) -- End

            '    If objEDates._Message <> "" Then
            '        DisplayMessage.DisplayMessage(objEDates._Message, Me)
            '    End If
            '    Exit Sub
            '    popup_DeleteReason.Hide()
            'End If

            If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                'Gajanan [9-July-2019] -- Start      
                'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
                'objADate._Isvoid = True
                objADate._Isvoid = False
                'Gajanan [9-July-2019] -- End
                objADate._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objADate._Voidreason = popup_DeleteReason.Reason
                objADate._Voiduserunkid = -1
                objADate._Audituserunkid = CInt(Session("UserId"))
                objADate._Isweb = True
                objADate._Ip = Session("IP_ADD").ToString()
                objADate._Hostname = Session("HOST_NAME").ToString()
                objADate._Form_Name = mstrModuleName

                If objADate.Delete(CInt(Me.ViewState("Datestranunkid")), clsEmployeeMovmentApproval.enOperationType.DELETED, CInt(Session("CompanyUnkId")), CBool(Session("CreateADUserFromEmpMst")), Nothing) = False Then
                    If objADate._Message <> "" Then
                        DisplayMessage.DisplayMessage(objADate._Message, Me)
                    End If
                    Exit Sub
                    popup_DeleteReason.Hide()

                End If

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set

                Dim objPMovement As New clsEmployeeMovmentApproval
                Dim intPrivilegeId As Integer = 0
                Dim eMovement As clsEmployeeMovmentApproval.enMovementType = Nothing
                Select Case mintDatesTypeId
                    Case enEmp_Dates_Transaction.DT_PROBATION
                        intPrivilegeId = 1194 : eMovement = clsEmployeeMovmentApproval.enMovementType.PROBATION
                    Case enEmp_Dates_Transaction.DT_RETIREMENT
                        intPrivilegeId = 1198 : eMovement = clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                    Case enEmp_Dates_Transaction.DT_SUSPENSION
                        intPrivilegeId = 1196 : eMovement = clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                    Case enEmp_Dates_Transaction.DT_TERMINATION
                        intPrivilegeId = 1197 : eMovement = clsEmployeeMovmentApproval.enMovementType.TERMINATION
                    Case enEmp_Dates_Transaction.DT_CONFIRMATION
                        intPrivilegeId = 1195 : eMovement = clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                    Case enEmp_Dates_Transaction.DT_EXEMPTION
                        intPrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeExemption : eMovement = clsEmployeeMovmentApproval.enMovementType.EXEMPTION
                End Select

                objPMovement.SendNotification(1, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), _
                                              CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                              intPrivilegeId, eMovement, Session("EmployeeAsOnDate").ToString, _
                                              CInt(Session("UserId")), mstrModuleName, enLogin_Mode.MGR_SELF_SERVICE, _
                                              Session("UserName").ToString, clsEmployeeMovmentApproval.enOperationType.DELETED, _
                                              False, CType(mintDatesTypeId, enEmp_Dates_Transaction), 0, cboEmployee.SelectedValue.ToString, "")

                'Sohail (21 Oct 2019) -- End
            Else
                If objEDates.Delete(CBool(Session("CreateADUserFromEmpMst")), CInt(Me.ViewState("Datestranunkid")), CInt(Session("CompanyUnkId")), Nothing) = False Then
                    If objEDates._Message <> "" Then
                        DisplayMessage.DisplayMessage(objEDates._Message, Me)
                    End If
                    Exit Sub
                    popup_DeleteReason.Hide()
                End If
            End If

            'S.SANDEEP |17-JAN-2019| -- End


            Call Fill_Grid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popup_DeleteReason_buttonDelReasonYes_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objADate = Nothing
            objEDates = Nothing
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Hemant (01 Aug 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  PAYROLL UAT CHANGES.
    Protected Sub Confirmation_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Confirmation.buttonYes_Click

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objEDates As New clsemployee_dates_tran
        Dim objADate As New clsDates_Approval_Tran
        'Gajanan [4-Sep-2020] -- End
        Try
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            SaveEmployeeDates(objEDates, objADate)
            'Gajanan [4-Sep-2020] -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (01 Aug 2019) -- End


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            FillEmployeeCombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End


    'Gajanan [18-May-2020] -- Start
    'Enhancement:Discipline Module Enhancement NMB
    Protected Sub lnkSetSuspension_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSetSuspension.Click
        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objDiscChargeMaster As New clsDiscipline_file_master
        Dim objDiscChargeTran As New clsDiscipline_file_tran
        'Gajanan [4-Sep-2020] -- End

        Try
            mblnpopupEmployeeDiscipline = True
            popupViewDiscipline.Show()
            FillDisciplineCombo()

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            GetValue(objDiscChargeMaster, objDiscChargeTran)
            'Gajanan [4-Sep-2020] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objDiscChargeMaster = Nothing
            objDiscChargeTran = Nothing
        End Try
    End Sub

    'Gajanan [18-May-2020] -- End

#End Region

#Region " Combobox Event(s) "

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmployee As New clsEmployee_Master

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim dsComboList As DataSet = objEmployee.GetEmployeeList("EmployeeList", False, True, CInt(cboEmployee.SelectedValue), , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , , , , )
                Dim dsComboList As DataSet = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), "EmployeeList", False, CInt(cboEmployee.SelectedValue))
                'Shani(24-Aug-2015) -- End
                If dsComboList IsNot Nothing AndAlso dsComboList.Tables(0).Rows.Count > 0 Then
                    mstrEmployeeCode = CStr(dsComboList.Tables(0).Rows(0)("employeecode"))

                    'S.SANDEEP |10-MAY-2019| -- START
                    'ISSUE/ENHANCEMENT : {#0003807}
                    mintEmployeeUnkid = CInt(cboEmployee.SelectedValue)
                    mstrEmployeeName = CStr(dsComboList.Tables(0).Rows(0)("employeename"))
                    'S.SANDEEP |10-MAY-2019| -- END

                End If
                dsComboList.Clear()
                dsComboList = Nothing
                objEmployee = Nothing
                Call ClearControls()
                Call Fill_Grid()


                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                Dim objEDates As New clsemployee_dates_tran

                Call Set_Dates(objEDates)

                objEDates = Nothing
                'Gajanan [4-Sep-2020] -- End
            Else
                dgvHistory.DataSource = Nothing
                dgvHistory.DataBind()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboEmployee_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

#End Region

#Region " Datepicker Event(s) "

    Private Sub dtpStartDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpStartDate.TextChanged
        Try
            If dtpEndDate.Visible Then
                'S.SANDEEP |02-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PAYROLL UAT -- MAKE SUSP. END DATE OPTIONAL
                'dtpEndDate.SetDate = dtpStartDate.GetDate.Date
                If mintDatesTypeId <> enEmp_Dates_Transaction.DT_SUSPENSION Then
                dtpEndDate.SetDate = dtpStartDate.GetDate.Date
            End If
                'S.SANDEEP |02-MAR-2020| -- END
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dtpStartDate_TextChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

#End Region

#Region "Datagrid Events"

    Protected Sub dgvHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvHistory.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If (e.Item.ItemIndex >= 0) Then
                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'If e.Item.Cells(2).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(2).Text.Trim <> "" Then
                '    e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).ToString(Session("DateFormat").ToString)
                'End If
                'If e.Item.Cells(3).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(3).Text.Trim <> "" Then
                '    e.Item.Cells(3).Text = CDate(e.Item.Cells(3).Text).ToString(Session("DateFormat").ToString)
                'End If
                'If e.Item.Cells(4).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(4).Text.Trim <> "" Then
                '    e.Item.Cells(4).Text = CDate(e.Item.Cells(4).Text).ToString(Session("DateFormat").ToString)
                'End If

                If e.Item.Cells(3).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(3).Text.Trim <> "" Then
                    e.Item.Cells(3).Text = CDate(e.Item.Cells(3).Text).ToShortDateString
                End If
                If e.Item.Cells(4).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(4).Text.Trim <> "" Then
                    e.Item.Cells(4).Text = CDate(e.Item.Cells(4).Text).ToShortDateString
                End If
                If e.Item.Cells(5).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(5).Text.Trim <> "" Then
                    e.Item.Cells(5).Text = CDate(e.Item.Cells(5).Text).ToShortDateString
                End If
                'Pinkal (16-Apr-2016) -- End

                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                If e.Item.Cells(13).Text.Trim <> "&nbsp;" AndAlso CStr(e.Item.Cells(13).Text).Trim.Length > 0 Then
                    e.Item.BackColor = Drawing.Color.PowderBlue
                    e.Item.ForeColor = System.Drawing.Color.Black
                    CType(e.Item.Cells(1).FindControl("imgEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False

                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = True
                    lblPendingData.Visible = True


                    'S.SANDEEP |17-JAN-2019| -- START
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).ToolTip = e.Item.Cells(14).Text
                    If CInt(e.Item.Cells(15).Text) = clsEmployeeMovmentApproval.enOperationType.EDITED Then
                        CType(e.Item.Cells(0).FindControl("imgDetail"), LinkButton).Visible = True
                        CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END


                Else
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = False

                    If e.Item.Cells(8).Text.Trim <> "&nbsp;" AndAlso CBool(e.Item.Cells(8).Text) = True Then
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    End If
                End If
                'Gajanan [12-NOV-2018] -- END



                'Gajanan (28-May-2018) -- Start
                'Issue - Rehire Employee Data Also Edit And Delete.
                If Convert.ToInt32(e.Item.Cells(12).Text) > 0 Then
                    CType(e.Item.Cells(1).FindControl("imgEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = False
                    e.Item.BackColor = System.Drawing.Color.Orange
                End If
                'Gajanan (28-May-2018) -- End

                If e.Item.Cells(8).Text.Trim <> "&nbsp;" AndAlso CBool(e.Item.Cells(8).Text) = True Then
                    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvHistory_ItemDataBound :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvHistory_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvHistory.ItemCommand

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objEDates As New clsemployee_dates_tran
        Dim objADate As New clsDates_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try
            lblAppointmentdate.Visible = False
            txtDate.Visible = False
            txtDate.Text = ""
            Me.ViewState("Datestranunkid") = 0
            Me.ViewState.Add("Datestranunkid", CInt(e.Item.Cells(7).Text))
            If e.CommandName = "Edit" Then

                'S.SANDEEP |17-JAN-2019| -- START
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim Flag As Boolean = objADate.isExist(Nothing, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, CInt(ViewState("Datestranunkid")))
                    If Flag Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 140, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), Me)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END

                'S.SANDEEP |27-JUN-2020| -- START
                'ISSUE/ENHANCEMENT : NMB ENHANCEMENT
                If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                    Dim objTnALeaveTran As New clsTnALeaveTran
                    Dim objMData As New clsMasterData : Dim iPeriod As Integer = 0
                    iPeriod = objMData.getCurrentPeriodID(enModuleReference.Payroll, dtpEffectiveDate.GetDate, CInt(Session("Fin_year")), enStatusType.OPEN, True)
                    If iPeriod <= 0 Then
                        iPeriod = objMData.getCurrentPeriodID(enModuleReference.Payroll, dtpStartDate.GetDate, CInt(Session("Fin_year")), enStatusType.OPEN, True)
                    End If
                    If iPeriod <= 0 Then
                        objMData.getCurrentPeriodID(enModuleReference.Payroll, dtpEndDate.GetDate, CInt(Session("Fin_year")), enStatusType.OPEN, True)
                    End If
                    'Sohail (13 Jan 2022) -- Start
                    'Enhancement : OLD-216 : Delink the setting "Allow to Terminate Employee If Payment is Done" with privilege "Allow to change EOC on closed period" and allow the setting to terminate employee if payment is done even for users who don’t have that privilege.
                    'If iPeriod > 0 AndAlso CBool(Session("AllowToChangeEOCLeavingDateOnClosedPeriod")) = True Then
                    'Sohail (21 Jan 2022) -- Start
                    'Enhancement : OLD-216 : Delink the setting "Allow to Terminate Employee If Payment is Done" with privilege "Allow to change EOC on closed period" and allow the setting to terminate employee if payment is done even for users who don’t have that privilege.
                    'If iPeriod > 0 AndAlso (CBool(Session("AllowToChangeEOCLeavingDateOnClosedPeriod")) = True OrElse CBool(Session("AllowTerminationIfPaymentDone")) = True) Then
                    If iPeriod > 0 Then
                        'Sohail ((21 Jan 2022) -- End
                        'Sohail (13 Jan 2022) -- End
                        Dim objPrd As New clscommom_period_Tran
                        objPrd._Periodunkid(CStr(Session("Database_Name"))) = iPeriod
                        If CBool(Session("AllowTerminationIfPaymentDone")) = False Then
                            If objTnALeaveTran.IsPayrollProcessDone(iPeriod, CStr(cboEmployee.SelectedValue), objPrd._End_Date, enModuleReference.Payroll) Then
                                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 100, "Sorry, you cannot exclude selected employee. Reason:Payroll Process already done for the employee for last date of current open period."), Me)
                                Exit Sub
                            End If
                        End If
                    End If
                    objTnALeaveTran = Nothing
                End If
                'S.SANDEEP |27-JUN-2020| -- END


                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                Call SetEditValue(objEDates)
                'Gajanan [4-Sep-2020] -- End

                Me.ViewState("AppointmentDate") = Nothing
                If CBool(e.Item.Cells(8).Text) = True Then
                    Me.ViewState.Add("AppointmentDate", eZeeDate.convertDate(e.Item.Cells(11).Text))
                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    'txtDate.Text = CDate(Me.ViewState("AppointmentDate")).ToString(Session("DateFormat").ToString)
                    txtDate.Text = CDate(Me.ViewState("AppointmentDate")).ToShortDateString
                    'Pinkal (16-Apr-2016) -- End
                    lblAppointmentdate.Visible = True
                    txtDate.Visible = True
                End If
                'S.SANDEEP [07-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001988}
                cboEmployee.Enabled = False
                'S.SANDEEP [07-Feb-2018] -- END
            ElseIf e.CommandName = "Delete" Then
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                objEDates._Datestranunkid = CInt(ViewState("Datestranunkid"))
                If objEDates._Leaveissueunkid > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 190, "Sorry, You can not perform any opration, Reason:This is auto generated entry from leave module."), Me)
                    Exit Sub
                End If
                'Sohail (21 Oct 2019) -- End

                'S.SANDEEP |17-JAN-2019| -- START
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim Flag As Boolean = objADate.isExist(Nothing, Nothing, Nothing, mintDatesTypeId, objADate._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, CInt(ViewState("Datestranunkid")))
                    If Flag Then
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 140, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), Me)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END


                If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                    Dim objMaster As New clsMasterData
                    Dim mintPeriodID As Integer = 0

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'mintPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, CDate(e.Item.Cells(3).Text).ToString(Session("DateFormat")), , CInt(Session("Fin_year")))
                    'If mintPeriodID <= 0 Then
                    '    mintPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, CDate(e.Item.Cells(4).Text).ToString(Session("DateFormat")), , CInt(Session("Fin_year")))
                    'End If
                    mintPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, CDate(e.Item.Cells(4).Text).Date, CInt(Session("Fin_year")))
                    If mintPeriodID <= 0 Then
                        mintPeriodID = objMaster.getCurrentPeriodID(enModuleReference.Payroll, CDate(e.Item.Cells(5).Text).Date, CInt(Session("Fin_year")))
                    End If
                    'Shani(20-Nov-2015) -- End

                    Dim objPeriod As New clscommom_period_Tran

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objPeriod._Periodunkid = mintPeriodID
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = mintPeriodID
                    'Shani(20-Nov-2015) -- End

                    If objPeriod._Statusid = 2 Then  '2=Close
                        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 11, "You cannot delete this transaction.Reason: This transaction is falling in a period which is already closed."), Me)
                        Exit Sub
                    End If
                    objPeriod = Nothing
                    Me.ViewState("StartDate") = Nothing
                    Me.ViewState("EndDate") = Nothing
                End If

                popup_DeleteReason.Reason = ""
                popup_DeleteReason.Show()


                'S.SANDEEP |17-JAN-2019| -- START
            ElseIf e.CommandName.ToUpper = "VIEW" Then
                If CInt(e.Item.Cells(15).Text) > 0 Then
                    Dim dr As DataRow() = mdtMovementDateList.Select(CType(Me.dgvHistory.Columns(7), BoundColumn).DataField & " = " & CInt(e.Item.Cells(7).Text) & " AND " & CType(Me.dgvHistory.Columns(13), BoundColumn).DataField & "= ''")
                    If dr.Length > 0 Then
                        Dim index As Integer = mdtMovementDateList.Rows.IndexOf(dr(0))
                        dgvHistory.SelectedIndex = index
                        dgvHistory.SelectedItemStyle.BackColor = Color.Blue
                        dgvHistory.SelectedItemStyle.ForeColor = Color.White
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END

            ElseIf e.CommandName.ToUpper = "VIEWREPORT" Then
                Dim intMovementtypeId As Integer = 0
                Dim intPrivilegeId As Integer = 0
                Select Case mintDatesTypeId
                    Case enEmp_Dates_Transaction.DT_PROBATION
                        intMovementtypeId = clsEmployeeMovmentApproval.enMovementType.PROBATION
                        intPrivilegeId = 1194
                    Case enEmp_Dates_Transaction.DT_CONFIRMATION
                        intMovementtypeId = clsEmployeeMovmentApproval.enMovementType.CONFIRMATION
                        intPrivilegeId = 1195
                    Case enEmp_Dates_Transaction.DT_RETIREMENT
                        intMovementtypeId = clsEmployeeMovmentApproval.enMovementType.RETIREMENTS
                        intPrivilegeId = 1198
                    Case enEmp_Dates_Transaction.DT_TERMINATION
                        intMovementtypeId = clsEmployeeMovmentApproval.enMovementType.TERMINATION
                        intPrivilegeId = 1197
                    Case enEmp_Dates_Transaction.DT_SUSPENSION
                        intMovementtypeId = clsEmployeeMovmentApproval.enMovementType.SUSPENSION
                        intPrivilegeId = 1196
                        'Sohail (21 Oct 2019) -- Start
                        'NMB Enhancement # : New screen employee exemption will be added on employee movement with end date as optional with approval flow for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                    Case enEmp_Dates_Transaction.DT_EXEMPTION
                        intMovementtypeId = clsEmployeeMovmentApproval.enMovementType.EXEMPTION
                        intPrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeExemption
                        'Sohail (21 Oct 2019) -- End
                End Select

                Popup_report._UserId = CInt(Session("UserId"))
                Popup_report._Priority = 0
                Popup_report._PrivilegeId = intPrivilegeId
                Popup_report._FillType = CType(intMovementtypeId, clsEmployeeMovmentApproval.enMovementType)
                Popup_report._DtType = CType(mintDatesTypeId, enEmp_Dates_Transaction)
                Popup_report._IsResidentPermit = False
                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                If Me.ViewState("Datestranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Datestranunkid")) > 0 Then
                    eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                Popup_report._OprationType = eOperType
                'Sohail (21 Oct 2019) -- End
                Popup_report._FilterString = "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue)
                Popup_report.Show()
                'Gajanan [12-NOV-2018] -- START

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvHistory_ItemCommand :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End

        Finally

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objEDates = Nothing
            objADate = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

#End Region

#Region " Context Menu Events "

    Protected Sub mnuTransfers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuTransfers.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Employee is mandatory information.Please Select employee to continue.", Me)
                cboEmployee.Focus()
                Exit Sub
            End If
            Response.Redirect("~/HR/Employee Movement/wPg_EmployeeTransfers.aspx?ID=" & Server.UrlEncode(b64encode(CStr(CInt(cboEmployee.SelectedValue)))), False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("mnuTransfers_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally

        End Try
    End Sub

    Protected Sub mnuRecategorization_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuRecategorization.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Employee is mandatory information.Please Select employee to continue.", Me)
                cboEmployee.Focus()
                Exit Sub
            End If
            Response.Redirect("~/HR/Employee Movement/wPg_EmpRecategorize.aspx?ID=" & Server.UrlEncode(b64encode(CStr(CInt(cboEmployee.SelectedValue)))), False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("mnuRecategorization_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally

        End Try
    End Sub

#End Region

#Region "Discipline Popup"

#Region " Private Methods "

    Private Sub FillDisciplineCombo()
        Dim dsCombo As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objDiscFileMaster As New clsDiscipline_file_master

        Try

            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)), _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), "EmployeeList", True)
            With cboDisciplineEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "employeename"
                .DataSource = dsCombo.Tables("EmployeeList")
                .DataBind()
                .SelectedValue = mintEmployeeUnkid.ToString()
            End With


            'Gajanan [02-June-2020] -- Start
            'dsCombo = objCommon.getDesciplineRefNoComboList(mintEmployeeUnkid, "refnoList")

            dsCombo = objDiscFileMaster.GetComboList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), "refnoList", , , mintEmployeeUnkid)
            'Gajanan [02-June-2020] -- End

            With cboReferenceNo
                .DataValueField = "disciplinefileunkid"
                .DataTextField = "reference_no"
                .DataSource = dsCombo.Tables("refnoList")
                .DataBind()
                .SelectedIndex = 0
            End With


        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End            
        Finally
            dsCombo.Dispose() : objEmp = Nothing
        End Try
    End Sub


    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    Private Sub GetValue(ByVal objDiscChargeMaster As clsDiscipline_file_master, ByVal objDiscChargeTran As clsDiscipline_file_tran)
        'Gajanan [4-Sep-2020] -- End

        Try
            objDiscChargeMaster._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)

            If objDiscChargeMaster._Chargedate <> Nothing Then
                txtchargeDate.Text = objDiscChargeMaster._Chargedate.ToShortDateString()
            End If

            If objDiscChargeMaster._Interdictiondate <> Nothing Then
                txtinterdictionDate.Text = objDiscChargeMaster._Interdictiondate.ToShortDateString()
            End If

            txtChargeDescription.Text = objDiscChargeMaster._Charge_Description
            cboReferenceNo.SelectedItem.Text = objDiscChargeMaster._Reference_No.ToString()
            cboDisciplineEmployee.SelectedValue = objDiscChargeMaster._Involved_Employeeunkid.ToString()
            cboDisciplineEmployee_SelectedIndexChanged(Nothing, Nothing)
            mintDisciplineunkid = CInt(cboReferenceNo.SelectedValue)

            objDiscChargeTran._Disciplinefileunkid = CInt(cboReferenceNo.SelectedValue)
            mdtCharges = objDiscChargeTran._ChargesTable
            Call FillCharges_Tran()

        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End            
        Finally
        End Try
    End Sub

    Private Sub FillCharges_Tran()
        Try
            dgvData.DataSource = mdtCharges
            dgvData.DataBind()
        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "FillCharges_Tran", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End            
        Finally
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboDisciplineEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDisciplineEmployee.SelectedIndexChanged
        Try
            If CInt(cboDisciplineEmployee.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(CDate(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))) = CInt(cboDisciplineEmployee.SelectedValue)
                Dim objDept As New clsDepartment
                Dim objJobs As New clsJobs
                objDept._Departmentunkid = objEmp._Departmentunkid
                objJobs._Jobunkid = objEmp._Jobunkid
                txtDepartment.Text = objDept._Name
                txtJobTitle.Text = objJobs._Job_Name
                objDept = Nothing : objJobs = Nothing : objEmp = Nothing
            Else
                txtDepartment.Text = "" : txtJobTitle.Text = ""
            End If
        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End           
        Finally
        End Try
    End Sub

    Private Sub cboReferenceNo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboReferenceNo.SelectedIndexChanged

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objDiscChargeMaster As New clsDiscipline_file_master
        Dim objDiscChargeTran As New clsDiscipline_file_tran
        'Gajanan [4-Sep-2020] -- End

        Try
            mintDisciplineunkid = CInt(cboReferenceNo.SelectedValue)

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            GetValue(objDiscChargeMaster, objDiscChargeTran)
            'Gajanan [4-Sep-2020] -- End

        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "cboReferenceNo_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End            
        Finally

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objDiscChargeMaster = Nothing
            objDiscChargeTran = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try

    End Sub

#End Region

#Region "Button Event"
    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            mblnpopupEmployeeDiscipline = False
            popupViewDiscipline.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub btnClosePopup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClosePopup.Click
        Try
            mblnpopupEmployeeDiscipline = False
            mintDisciplineunkid = -1
            popupViewDiscipline.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

#End Region

#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.Title)

            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.ID, Me.lblEffectiveDate.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblAppointmentdate.Text = Language._Object.getCaption(Me.lblAppointmentdate.ID, Me.lblAppointmentdate.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.ID, Me.lblReason.Text)
            Me.lblEmplReason.Text = Language._Object.getCaption(Me.lblEmplReason.ID, Me.lblEmplReason.Text)
            Me.chkExclude.Text = Language._Object.getCaption(Me.chkExclude.ID, Me.chkExclude.Text)
            Me.mnuSalaryChange.Text = Language._Object.getCaption(Me.mnuSalaryChange.ID, Me.mnuSalaryChange.Text)
            Me.mnuBenefits.Text = Language._Object.getCaption(Me.mnuBenefits.ID, Me.mnuBenefits.Text)
            Me.mnuTransfers.Text = Language._Object.getCaption(Me.mnuTransfers.ID, Me.mnuTransfers.Text)
            Me.mnuRecategorization.Text = Language._Object.getCaption(Me.mnuRecategorization.ID, Me.mnuRecategorization.Text)
            Me.btnOperations.Text = Language._Object.getCaption(Me.btnOperations.ID, Me.btnOperations.Text).Replace("&", "")
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvHistory.Columns(3).HeaderText = Language._Object.getCaption(Me.dgvHistory.Columns(3).FooterText, Me.dgvHistory.Columns(3).HeaderText)
            Me.dgvHistory.Columns(4).HeaderText = Language._Object.getCaption(Me.dgvHistory.Columns(4).FooterText, Me.dgvHistory.Columns(4).HeaderText)
            Me.dgvHistory.Columns(5).HeaderText = Language._Object.getCaption(Me.dgvHistory.Columns(5).FooterText, Me.dgvHistory.Columns(5).HeaderText)
            Me.dgvHistory.Columns(6).HeaderText = Language._Object.getCaption(Me.dgvHistory.Columns(6).FooterText, Me.dgvHistory.Columns(6).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


End Class
