﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/home.master" CodeFile="wPg_EmployeeOtherDetails.aspx.vb" Inherits="HR_Employee_Movement_wPg_EmployeeOtherDetails" Title="Cost Center Information" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewMovementApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>
    
       <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Cost Center Information"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Cost Center Information"></asp:Label>
                                    </div>
                                    
                                      <div style="text-align: right">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" CssClass="lnkhover"
                                          Style="color: Blue; vertical-align: top"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%; margin-bottom: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date"></asp:Label>
                                            </td>
                                            <td style="width: 15%">
                                                <uc1:DateCtrl ID="dtpEffectiveDate" runat="server" AutoPostBack ="false" />
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblEmployee" Style="margin-left: 10px" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 55%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; margin-top: 10px; margin-bottom: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="objlblCaption" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td style="width: 85%">
                                                <asp:DropDownList ID="cboCCTrnHead" runat="server"></asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="border: 1px solid #DDD">
                                    </div>
                                    <table style="width: 100%; margin-top: 10px; margin-bottom: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                                <asp:Label ID="lblReason" runat="server" Text="Change Reason"></asp:Label>
                                            </td>
                                            <td style="width: 30%;">
                                                <asp:DropDownList ID="cboChangeReason" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 55%" align="right">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%">
                                                            <asp:Label ID="lblAppointDate" runat="server" Style="margin-left: 10px" Visible="false" Text="Appoint Date"></asp:Label>
                                                        </td>
                                                        <td style="width: 25%">
                                                            <asp:TextBox ID="txtAppointDate" runat="server" Visible="false" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 15%" align="right">
                                                            <asp:Button ID="btnSave" runat="server" CssClass="btndefault" Text="Save Changes" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div style="border: 1px solid #DDD">
                                    </div>
                                    <table style="width: 100%; margin-top: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <div id="scrollable-container" class="gridscroll" style="vertical-align: top; overflow: auto;
                                                    max-height: 350px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                    <asp:DataGrid ID="dgvHistory" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:TemplateColumn  HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                ItemStyle-HorizontalAlign="Center" FooterText="brnView">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="imgView" runat="server" ToolTip="View" CommandName="ViewReport">
                                                                        <i class="fa fa-exclamation-circle" style="font-size: 18px;"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="brnEdit">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit"></asp:LinkButton>
                                                                        <asp:LinkButton ID="imgDetail" runat="server" ToolTip="View Detail" CommandName="View"
                                                                            Visible="false"><i class="fa fa-eye" style="font-size: 18px;"></i> </asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="btnDelete">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                            CommandName="Delete" CssClass="griddelete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="EffDate" HeaderText="Effective Date" ReadOnly="true"
                                                                FooterText="dgcolhChangeDate"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="DispValue" HeaderText="" ReadOnly="true" FooterText="objdgcolhValue">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="CReason" HeaderText="Reason" ReadOnly="true" FooterText="dgcolhReason">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="isfromemployee" HeaderText="objdgcolhFromEmp" ReadOnly="true"
                                                                FooterText="objdgcolhFromEmp" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="cctranheadunkid" HeaderText="objdgcolhdetailtranunkid"
                                                                ReadOnly="true" FooterText="objdgcolhdetailtranunkid" Visible="false"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="adate" HeaderText="objdgcolhAppointDate" ReadOnly="true"
                                                                FooterText="objdgcolhAppointDate" Visible="false"></asp:BoundColumn>
                                                                 <asp:BoundColumn DataField="rehiretranunkid" HeaderText="rehiretranunkid" Visible="false">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" Visible="false"
                                                                FooterText="objdgcolhOperationType"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="operationtypeid" HeaderText="Operationtype Id" Visible="false"
                                                                FooterText="objdgcolhOperationTypeId"></asp:BoundColumn>
                                                            <%--12--%>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Label ID="lblRehireEmployeeData" runat="server" Text="Employee Rehired" BackColor="Orange"
                                            Style="padding: 2px; font-weight: bold"></asp:Label>
                                        <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" BackColor="PowderBlue"
                                            Style="padding: 2px; font-weight: bold"></asp:Label>
                                        <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                    <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To Delete?:" />
                    <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
      
    
    </asp:Content>
