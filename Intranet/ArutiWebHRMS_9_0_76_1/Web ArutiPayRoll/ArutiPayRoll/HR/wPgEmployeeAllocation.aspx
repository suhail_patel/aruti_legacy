﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false" CodeFile="wPgEmployeeAllocation.aspx.vb"
    Inherits="wPgEmployeeAllocation" Title="Allocations" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <div>
            <asp:Panel ID="pnlAllocations" runat="server" Width="70%">
                <asp:UpdatePanel ID="uppnl_main" runat="server">
                    <ContentTemplate>
                        <div class="panel-primary">
                            <div class="panel-heading">
                                <asp:Label ID="lblPageHeader" runat="server" Text="Allocations"></asp:Label>
                            </div>
                            <div class="panel-body">
                                <div id="FilterCriteria" class="panel-default">
                                    <div id="FilterCriteriaTitle" class="panel-heading-default">
                                        <div style="float: left;">
                                            <asp:Label ID="lblpnl_Header" runat="server" Text="Allocation Detail"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="FilterCriteriaBody" class="panel-body-default">
                                        <table style="width: 100%">
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td style="width: 50%" colspan="2">
                                                </td>
                                                <%--<td style="width: 15%">
                                                    &nbsp;
                                                </td>
                                                <td style="width: 35%">
                                                    &nbsp;
                                                </td>--%>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblBranch" runat="server" Text="Branch"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboBranch" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                             
                                                  <td style="width: 15%">
                                                    <asp:Label ID="lblJobGroup" runat="server" Text="Job Group"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboJobGroup" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblDepartmentGroup" runat="server" Text="Dept. Group"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboDeptGrp" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                 <td style="width: 15%">
                                                    <asp:Label ID="lblJob" runat="server" Text="Job"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboJob" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                
                                              
                                                
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblDepartment" runat="server" Text="Department"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:DropDownList ID="cboDepartment" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                 <td style="width: 15%">
                                                    <asp:Label ID="lblScale" runat="server" Text="Scale"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:TextBox ID="txtScale" runat="server" Style="text-align: right" ReadOnly="True"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblSectionGroup" runat="server" Text="Section Group"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboSecGroup" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                 <td style="width: 15%">
                                                    <asp:Label ID="lblClassGroup" runat="server" Text="Class Group"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboClassGrp" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblSection" runat="server" Text="Section"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboSection" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                 <td style="width: 15%">
                                                    <asp:Label ID="lblClass" runat="server" Text="Class"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboClass" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblUnitGroup" runat="server" Text="Unit Group"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboUnitGrp" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblGradeGroup" runat="server" Text="Grade Group"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboGradeGrp" runat="server" AutoPostBack="True" Enabled="False">
                                                    </asp:DropDownList>
                                                </td>
                                                
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblUnits" runat="server" Text="Unit"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboUnit" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                               <asp:Panel ID="pnlgrade" runat="server">
                                                    <td style="width: 15%">
                                                        <asp:Label ID="lblGrade" runat="server" Text="Grade"></asp:Label>
                                                    </td>
                                                    <td style="width: 35%">
                                                        <asp:DropDownList ID="cboGrade" runat="server" AutoPostBack="True" Enabled="False">
                                                        </asp:DropDownList>
                                                    </td>
                                                </asp:Panel>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%">
                                                    <asp:Label ID="lblTeam" runat="server" Text="Team"></asp:Label>
                                                </td>
                                                <td style="width: 35%">
                                                    <asp:DropDownList ID="cboTeam" runat="server" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </td>
                                                <asp:Panel ID="pnlgradegrp" runat="server">
                                                    <td style="width: 15%">
                                                        <asp:Label ID="lblGradeLevel" runat="server" Text="Grade Level"></asp:Label>
                                                    </td>
                                                    <td style="width: 35%">
                                                        <asp:DropDownList ID="cboGradeLevel" runat="server" AutoPostBack="True" Enabled="False">
                                                        </asp:DropDownList>
                                                    </td>
                                                </asp:Panel>
                                            </tr>
                                            <tr style="width: 100%">
                                                <td style="width: 15%;">
                                                    <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:DropDownList ID="cboCostcenter" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 50%" colspan="2">
                                                </td>
                                                <%--<td style="width: 15%;">
                                                &nbsp;
                                                </td>
                                                <td style="width: 35%;">
                                                &nbsp;
                                                </td>--%>
                                            </tr>
                                        </table>
                                        <div class="btn-default">
                                            <asp:Button ID="btnsave" runat="server" CssClass="btndefault" Text="Update" />
                                            <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
        </div>
    </center>
</asp:Content>
