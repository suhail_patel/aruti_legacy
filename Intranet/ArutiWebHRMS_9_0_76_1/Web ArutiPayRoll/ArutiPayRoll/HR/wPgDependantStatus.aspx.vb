﻿Option Strict On

#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Threading
Imports System.IO
Imports System.Drawing

#End Region

Partial Class HR_wPgDependantStatus
    Inherits Basepage

#Region " Private Variable(s) "
    Private ReadOnly mstrModuleName As String = "frmDependantStatus"
    Private msg As New CommonCodes
    Private clsuser As New User
    Private objEmployee As New clsEmployee_Master
    Private objDependants_Benefice As New clsDependants_Beneficiary_tran
    Private mdtView As DataTable

    Private DependantApprovalFlowVal As String
    Private mintItemIndex As Integer = -1

#End Region

#Region " Private Enum "
    Private Enum enGridColumn
        IsChecked = 0
        ChkSelect = 1
        Delete = 2
        DpndtBefName = 3
        Relation = 4
        Gender = 5
        dtbirthdate = 6
        ROWNO = 7
        EmpId = 8
        dpndtbeneficestatustranunkid = 9
        DpndtTranId = 10
        dteffective_date = 11
        isactive = 12
        Reason = 13
        isapproved = 14
    End Enum
#End Region

#Region " Private Functions "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objCountry As New clsMasterData
        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master
        Dim objMembership As New clsmembership_master
        Dim objBenefit As New clsbenefitplan_master
        Dim objMaster As New clsMasterData
        Try


            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True


            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
            
                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "Emp", True, _
                                              0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "employeename"
                    .DataSource = dsCombos.Tables("Emp")
                    .DataBind()
                    .SelectedValue = "0"
                    Call cboEmployee_SelectedIndexChanged(cboEmployee, New System.EventArgs)
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation")
            With cboRelation
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Relation")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.DEPENDANT_ACTIVE_INACTIVE_REASON, True, "DepReason")
            With cboReason
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("DepReason")
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombos = objMaster.getGenderList("List", True)
            With cboGender
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            dsCombos = objMaster.getComboListTranHeadActiveInActive("List", True)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
                If .Items.Count > 0 Then .SelectedValue = "0"
            End With

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim StrApproverSearching As String = String.Empty
        Try
            BtnSetActive.Visible = False
            BtnSetInactive.Visible = False
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 1111, "Please Select Employee to Continue"), Me)
                Exit Sub
            End If

            Dim dtAsOnDate As Date = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
            If CInt(cboDependant.SelectedValue) > 0 Then
                cboStatus.SelectedValue = "0"
                dtAsOnDate = Nothing
            End If
            Select Case CInt(cboStatus.SelectedValue)
                Case enEDHeadApprovalStatus.Approved
                    BtnSetInactive.Visible = True
                Case 2
                    BtnSetActive.Visible = True
            End Select

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("AllowToViewDependentStatus")) = False Then Exit Sub
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND hrdependants_beneficiaries_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboRelation.SelectedValue) > 0 Then
                StrSearching &= "AND cfcommon_master.masterunkid = " & CInt(cboRelation.SelectedValue) & " "
            End If


            If txtIdNo.Text.Trim <> "" Then
                StrSearching &= "AND ISNULL(hrdependants_beneficiaries_tran.identify_no,'') LIKE '%" & txtIdNo.Text & "%'" & " "
            End If

            If CInt(cboGender.SelectedValue) > 0 Then
                StrSearching &= "AND hrdependants_beneficiaries_tran.gender = '" & CInt(cboGender.SelectedValue) & "' "
            End If

            If dtpBirthdate.IsNull = False Then
                StrSearching &= "AND ISNULL(CONVERT(CHAR(8),hrdependants_beneficiaries_tran.birthdate,112),'') = '" & eZeeDate.convertDate(dtpBirthdate.GetDate) & "' "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If Session("PendingEmployeeScreenIDs").ToString.Trim.Length > 0 Then
                If Session("PendingEmployeeScreenIDs").ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If


            dsList = objDependants_Benefice.GetList(CStr(Session("Database_Name")), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    dtAsOnDate, _
                                                    dtAsOnDate, _
                                                    CStr(Session("UserAccessModeSetting")), mblnOnlyApproved, _
                                                    CBool(Session("IsIncludeInactiveEmp")), "List", , CInt(cboEmployee.SelectedValue), StrSearching, CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)), mblnAddApprovalCondition, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), CInt(cboStatus.SelectedValue), False, CInt(cboDependant.SelectedValue))

            Dim dcol As New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "tranguid"
                .DefaultValue = ""
            End With
            dsList.Tables(0).Columns.Add(dcol)

            mdtView = dsList.Tables(0)

            dgvDependantList.DataSource = mdtView
            dgvDependantList.DataKeyField = "dpndtbeneficestatustranunkid"
            dgvDependantList.DataBind()


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetVisibility()

        Try

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                BtnSetActive.Enabled = CBool(Session("AllowToSetDependentActive"))
                BtnSetInactive.Enabled = CBool(Session("AllowToSetDependentInactive"))
            Else
                BtnSetActive.Enabled = CBool(Session("AllowEditDependants"))
                BtnSetInactive.Enabled = CBool(Session("AllowEditDependants"))
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try

    End Sub

    Private Function IsValidated(ByVal btn As Button) As Boolean
        Try
            If mdtView Is Nothing Then Exit Try

            If dtpEffectiveDate.IsNull = True Then
                Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 33, "Please set effective date."), Me)
                dtpEffectiveDate.Focus()
                Return False
            ElseIf eZeeDate.convertDate(dtpEffectiveDate.GetDate.Date) > eZeeDate.convertDate(DateAndTime.Today.Date) Then
                Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 34, "Effective date should not be greater than current date."), Me)
                dtpEffectiveDate.Focus()
                Return False
            ElseIf CInt(cboReason.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 35, "Please select Reason."), Me)
                cboReason.Focus()
                Return False
            ElseIf mdtView.Select("IsGrp = 0 AND IsChecked = 1").Length <= 0 Then
                Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 36, "Please tick atleast one dependant."), Me)
                dgvDependantList.Focus()
                Return False
            Else
                '***
                Dim dtMax As String = (From p In mdtView Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("effective_date").ToString)).Max
                If eZeeDate.convertDate(dtpEffectiveDate.GetDate.Date) <= dtMax Then
                    Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 37, "Sorry, Effective date should be greater than last effective date") & " " & eZeeDate.convertDate(dtMax).ToString("dd-MMM-yyyy"), Me)
                    dtpEffectiveDate.Focus()
                    Return False
                End If

                Dim arrIDs() As String = (From p In mdtView Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("DpndtTranId").ToString)).ToArray
                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True

                If DependantApprovalFlowVal Is Nothing Then

                    '***
                    If Session("PendingEmployeeScreenIDs").ToString.Trim.Length > 0 Then
                        If Session("PendingEmployeeScreenIDs").ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                            mblnOnlyApproved = False
                            mblnAddApprovalCondition = False
                        End If
                    End If
                    Dim objDBApproval As New clsDependant_beneficiaries_approval_tran
                    Dim dsPending As DataSet = objDBApproval.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), CStr(Session("UserAccessModeSetting")), mblnOnlyApproved, CBool(Session("IsIncludeInactiveEmp")), "Emp", False, , , , mblnAddApprovalCondition)

                    If dsPending.Tables(0).Rows.Count > 0 Then

                        Dim intCount As Integer = (From p In dsPending.Tables(0) Where (arrIDs.Contains(p.Item("DpndtTranId").ToString) = True) Select (p)).Count

                        If intCount > 0 Then
                            Language.setLanguage(mstrModuleName)
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 38, "Sorry, You cannot do any operation. Reason: Some of the transactions are in approval process.") & "\n" & "\n" & Language.getMessage(mstrModuleName, 39, "Please Approve / Reject transction."), Me)
                            dgvDependantList.Focus()
                            Return False
                        End If
                    End If
                End If


                '***
                Dim dsList As DataSet
                dsList = objDependants_Benefice.GetList(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                       eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), _
                                                       CStr(Session("UserAccessModeSetting")), mblnOnlyApproved, _
                                                       CBool(Session("IsIncludeInactiveEmp")), "List", , , "hrdependants_beneficiaries_tran.dpndtbeneficetranunkid IN (" & String.Join(",", arrIDs) & ")", , mblnAddApprovalCondition, eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), enEDHeadApprovalStatus.All, False)

                If btn.ID = BtnSetInactive.ID Then
                    If dsList.Tables(0).Select("isactive = 0").Length > 0 Then
                        Language.setLanguage(mstrModuleName)
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 40, "Sorry, Some of the dependants are already inactive. Please refresh the dependant list and the try again."), Me)
                        dgvDependantList.Focus()
                        Return False
                    End If
                ElseIf btn.ID = BtnSetActive.ID Then
                    If dsList.Tables(0).Select("isactive = 1").Length > 0 Then
                        Language.setLanguage(mstrModuleName)
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 41, "Sorry, Some of the dependants are already active. Please refresh the dependant list and the try again."), Me)
                        dgvDependantList.Focus()
                        Return False
                    End If
                End If

            End If

            Return True

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Function

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)

            If mdtView.Rows.Count <= 0 Then Exit Sub

            For Each item As DataGridItem In dgvDependantList.Items
                CType(item.Cells(0).FindControl("chkSelect"), CheckBox).Checked = chkBox.Checked
                Dim dRow() As DataRow = mdtView.Select("dpndtbeneficestatustranunkid = " & item.Cells(enGridColumn.dpndtbeneficestatustranunkid).Text)
                If dRow.Length > 0 Then
                    dRow(0).Item("ischecked") = chkBox.Checked
                End If
            Next
            mdtView.AcceptChanges()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkBox.NamingContainer, DataGridItem)

            Dim dRow() As DataRow = mdtView.Select("dpndtbeneficestatustranunkid = " & item.Cells(enGridColumn.dpndtbeneficestatustranunkid).Text)
            If dRow.Length > 0 Then
                dRow(0).Item("ischecked") = chkBox.Checked
            End If
            mdtView.AcceptChanges()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mdtView") = mdtView
            Me.ViewState("DependantApprovalFlowVal") = DependantApprovalFlowVal
            Me.ViewState("mintItemIndex") = mintItemIndex
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("clsuser") Is Nothing Then

            Exit Sub
        End If

        Try

            'Call SetMessages()
            Call Language._Object.SaveValue()
            'SetLanguage()


            If (Page.IsPostBack = False) Then
                clsuser = CType(Session("clsuser"), User)
                Call FillCombo()

                If Request.QueryString.Count <= 0 Then
                    If Session("dependantsEmpunkid") IsNot Nothing Then
                        cboEmployee.SelectedValue = CStr(CInt(Session("dependantsEmpunkid")))
                        Call cboEmployee_SelectedIndexChanged(cboEmployee, New System.EventArgs)
                    End If
                End If

                Dim Arr() As String = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
                DependantApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmDependantsAndBeneficiariesList)))

                Call SetVisibility()

            Else
                mdtView = CType(ViewState("mdtView"), DataTable)
                If ViewState("DependantApprovalFlowVal") IsNot Nothing Then
                    DependantApprovalFlowVal = Me.ViewState("DependantApprovalFlowVal").ToString
                End If
                mintItemIndex = CInt(Me.ViewState("mintItemIndex"))
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim dsCombos As DataSet
        Try
            dsCombos = objDependants_Benefice.GetListForCombo(CInt(cboEmployee.SelectedValue), True, Nothing, eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), False)
            With cboDependant
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            Response.Redirect(Session("rootpath").ToString & "HR/wPgDependantsList.aspx", False)

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 1111, "Please Select Employee to Continue"), Me)
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub btnSetActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSetActive.Click
        Dim objDBStatus As New clsDependant_Benefice_Status_tran
        Dim objADependant As New clsDependant_beneficiaries_approval_tran
        Dim blnFlag As Boolean = False
        Try
            If IsValidated(btnSetActive) = False Then Exit Try

            If DependantApprovalFlowVal Is Nothing Then

                Dim objApprovalData As New clsEmployeeDataApproval
                Dim dtEmp As DataTable = New DataView(mdtView, "IsChecked = 1 AND IsGrp = 0", "", DataViewRowState.CurrentRows).ToTable(True, "EmpId", "EmpName")
                dtEmp.Columns.Add("Reason", System.Type.GetType("System.String")).DefaultValue = ""
                dtEmp.Columns.Add("IsValid", System.Type.GetType("System.Boolean")).DefaultValue = True
                For Each dtRow As DataRow In dtEmp.Rows
                    If objApprovalData.IsApproverPresent(enScreenName.frmDependantsAndBeneficiariesList, CStr(Session("Database_Name")), _
                                                    CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                    CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                    CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(dtRow.Item("EmpId")), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then

                        dtRow.Item("Reason") = objApprovalData._Message
                        dtRow.Item("IsValid") = False
                    End If
                Next
                dtEmp.AcceptChanges()

                Dim strInvalidEmpIDs As String = "-999"
                If dtEmp.Select("IsValid = 0").Length > 0 Then
                    strInvalidEmpIDs = String.Join(",", (From p In dtEmp Where (CBool(p.Item("IsValid")) = False) Select (p.Item("EmpId").ToString)).ToArray)

                    dtEmp.Columns.Remove("EmpId")
                    dtEmp.Columns.Remove("IsValid")

                    popupValidationList.Message = Language.getMessage(mstrModuleName, 11, "Sorry, No approver defined for the employee(s) given below! Do you want to proceed for the rest of the employees ?")
                    popupValidationList.Data_Table = dtEmp
                    popupValidationList.Show()

                    Exit Try

                End If

                Call Save(strInvalidEmpIDs, True)
            Else

                With objDBStatus
                    ._Effective_Date = dtpEffectiveDate.GetDate
                    ._Isactive = True
                    ._Reasonunkid = CInt(cboReason.SelectedValue)
                    ._Userunkid = CInt(Session("UserId"))
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        ._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                    Else
                        ._Loginemployeeunkid = 0
                    End If
                    ._Isweb = True
                    ._Isvoid = False
                    ._FormName = mstrModuleName
                    ._ClientIP = Session("IP_ADD").ToString
                    ._HostName = Session("HOST_NAME").ToString
                    ._CompanyUnkid = CInt(Session("CompanyUnkId"))
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._xDataOp = Nothing

                    Dim dtTable As DataTable = mdtView.Select("IsGrp = 0 AND IsChecked = 1").CopyToDataTable

                    blnFlag = .InsertAll(dtTable)

                End With

                If blnFlag = False And objDBStatus._Message <> "" Then
                    msg.DisplayMessage(objDBStatus._Message, Me)
                End If

            End If


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objDBStatus = Nothing
            objADependant = Nothing
        End Try
    End Sub

    Private Sub Save(ByVal strInvalidEmpIDs As String, ByVal blnIsActive As Boolean)
        Dim objApprovalData As New clsEmployeeDataApproval
        Dim objADependant As New clsDependant_beneficiaries_approval_tran
        Dim objDBStatus As New clsDependant_Benefice_Status_tran
        Dim blnFlag As Boolean = False
        Try
            Dim dtApproved As DataTable = Nothing
            Dim dtNotApproved As DataTable = Nothing
            Dim dr() As DataRow = mdtView.Select("IsGrp = 0 AND IsChecked = 1 AND isapproved = 1 AND EmpId NOT IN (" & strInvalidEmpIDs & ")")
            If dr.Length > 0 Then
                dtApproved = dr.CopyToDataTable
            End If
            dr = mdtView.Select("IsGrp = 0 AND IsChecked = 1 AND isapproved = 0 AND EmpId NOT IN (" & strInvalidEmpIDs & ")")
            If dr.Length > 0 Then
                dtNotApproved = dr.CopyToDataTable
            End If

            Dim eLMode As Integer
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                eLMode = enLogin_Mode.MGR_SELF_SERVICE
            Else
                eLMode = enLogin_Mode.EMP_SELF_SERVICE
            End If

            If dtApproved IsNot Nothing AndAlso dtApproved.Rows.Count > 0 Then

                objADependant._Audittype = enAuditType.ADD
                objADependant._Audituserunkid = CInt(Session("UserId"))
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    objADependant._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                Else
                    objADependant._Loginemployeeunkid = 0
                End If
                objADependant._Isvoid = False
                objADependant._Isactive = blnIsActive
                objADependant._ReasonUnkid = CInt(cboReason.SelectedValue)
                objADependant._Tranguid = Guid.NewGuid.ToString()
                objADependant._Transactiondate = Now
                objADependant._Approvalremark = ""
                objADependant._Isweb = True
                objADependant._Isfinal = False
                objADependant._Effective_date = dtpEffectiveDate.GetDate
                objADependant._Ip = Session("IP_ADD").ToString
                objADependant._Host = Session("HOST_NAME").ToString
                objADependant._Form_Name = mstrModuleName
                objADependant._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
                objADependant._Audittype = enAuditType.EDIT
                objADependant._Isprocessed = False
                objADependant._CompanyId = CInt(Session("CompanyUnkId"))
                objADependant._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                objADependant._Operationtypeid = clsEmployeeDataApproval.enOperationType.EDITED
                objADependant._Newattacheddocumnetid = ""
                objADependant._Deletedattachdocumnetid = ""
                objADependant._ImagePath = ""
                objADependant._IsFromStatus = True

                blnFlag = objADependant.InsertAll(dtTable:=dtApproved, blnGetLatestStatus:=False)

                If blnFlag = False And objDBStatus._Message <> "" Then
                    msg.DisplayMessage(objDBStatus._Message, Me)
                Else

                    Dim arrIDs() As String = (From p In dtApproved Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("DpndtTranId").ToString)).ToArray
                    Dim arrEmpIDs() As String = (From p In dtApproved Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("EmpId").ToString)).ToArray

                    For Each dtRow As DataRow In dtApproved.Rows
                        dtRow.Item("dteffective_date") = dtpEffectiveDate.GetDate
                        dtRow.Item("effective_date") = eZeeDate.convertDate(dtpEffectiveDate.GetDate)
                        dtRow.Item("isactive") = blnIsActive
                        dtRow.Item("reasonunkid") = CInt(cboReason.SelectedValue)
                        dtRow.Item("reason") = cboReason.Text
                    Next
                    dtApproved.AcceptChanges()

                    'Dim objApprovalData As New clsEmployeeDataApproval
                    objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                        CStr(Session("UserAccessModeSetting")), _
                                                        CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                        CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                        enScreenName.frmDependantStatus, CStr(Session("EmployeeAsOnDate")), _
                                                        CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                        Session("UserName").ToString, clsEmployeeDataApproval.enOperationType.EDITED, 0, String.Join(",", arrEmpIDs), "", Nothing, _
                                                        "dpndtbeneficetranunkid IN (" & String.Join(",", arrIDs) & ") ", dtApproved)

                End If
            End If

            If dtNotApproved IsNot Nothing AndAlso dtNotApproved.Rows.Count > 0 Then

                With objDBStatus
                    ._Effective_Date = dtpEffectiveDate.GetDate
                    ._Isactive = blnIsActive
                    ._Reasonunkid = CInt(cboReason.SelectedValue)
                    ._Userunkid = CInt(Session("UserId"))
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        ._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                    Else
                        ._Loginemployeeunkid = 0
                    End If
                    ._Isweb = True
                    ._Isvoid = False
                    ._FormName = mstrModuleName
                    ._ClientIP = Session("IP_ADD").ToString
                    ._HostName = Session("HOST_NAME").ToString
                    ._CompanyUnkid = CInt(Session("CompanyUnkId"))
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._xDataOp = Nothing

                    blnFlag = .InsertAll(dtNotApproved)

                End With

                If blnFlag = False And objDBStatus._Message <> "" Then
                    msg.DisplayMessage(objDBStatus._Message, Me)
                End If

            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objADependant = Nothing
        End Try
    End Sub
    Private Sub btnSetInactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSetInactive.Click
        Dim objDBStatus As New clsDependant_Benefice_Status_tran
        Dim objADependant As New clsDependant_beneficiaries_approval_tran
        Dim blnFlag As Boolean = False
        Try
            If IsValidated(btnSetInactive) = False Then Exit Try

            If DependantApprovalFlowVal Is Nothing Then

                Dim objApprovalData As New clsEmployeeDataApproval
                Dim dtEmp As DataTable = New DataView(mdtView, "IsChecked = 1 AND IsGrp = 0", "", DataViewRowState.CurrentRows).ToTable(True, "EmpId", "EmpName")
                dtEmp.Columns.Add("Reason", System.Type.GetType("System.String")).DefaultValue = ""
                dtEmp.Columns.Add("IsValid", System.Type.GetType("System.Boolean")).DefaultValue = True
                For Each dtRow As DataRow In dtEmp.Rows
                    If objApprovalData.IsApproverPresent(enScreenName.frmDependantsAndBeneficiariesList, CStr(Session("Database_Name")), _
                                                    CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                    CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                    CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(dtRow.Item("EmpId")), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then

                        dtRow.Item("Reason") = objApprovalData._Message
                        dtRow.Item("IsValid") = False
                    End If
                Next
                dtEmp.AcceptChanges()

                Dim strInvalidEmpIDs As String = "-999"
                If dtEmp.Select("IsValid = 0").Length > 0 Then
                    strInvalidEmpIDs = String.Join(",", (From p In dtEmp Where (CBool(p.Item("IsValid")) = False) Select (p.Item("EmpId").ToString)).ToArray)

                    dtEmp.Columns.Remove("EmpId")
                    dtEmp.Columns.Remove("IsValid")

                    popupValidationList.Message = Language.getMessage(mstrModuleName, 11, "Sorry, No approver defined for the employee(s) given below! Do you want to proceed for the rest of the employees ?")
                    popupValidationList.Data_Table = dtEmp
                    popupValidationList.Show()

                    Exit Try

                End If

                Call Save(strInvalidEmpIDs, False)
            Else

                With objDBStatus
                    ._Effective_Date = dtpEffectiveDate.GetDate
                    ._Isactive = False
                    ._Reasonunkid = CInt(cboReason.SelectedValue)
                    ._Userunkid = CInt(Session("UserId"))
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        ._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                    Else
                        ._Loginemployeeunkid = 0
                    End If
                    ._Isweb = True
                    ._Isvoid = False
                    ._FormName = mstrModuleName
                    ._ClientIP = Session("IP_ADD").ToString
                    ._HostName = Session("HOST_NAME").ToString
                    ._CompanyUnkid = CInt(Session("CompanyUnkId"))
                    ._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                    ._xDataOp = Nothing

                    Dim dtTable As DataTable = mdtView.Select("IsGrp = 0 AND IsChecked = 1").CopyToDataTable

                    blnFlag = .InsertAll(dtTable)

                End With

                If blnFlag = False And objDBStatus._Message <> "" Then
                    msg.DisplayMessage(objDBStatus._Message, Me)
                End If

            End If


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objDBStatus = Nothing
            objADependant = Nothing
        End Try
    End Sub

    Protected Sub popupDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupDeleteReason.buttonDelReasonYes_Click
        Try
            If CInt(dgvDependantList.Items(mintItemIndex).Cells(enGridColumn.ROWNO).Text) = 1 Then
                Dim objApprovalData As New clsEmployeeDataApproval
                Dim objDependants_Beneficiary_tran As New clsDependants_Beneficiary_tran
                objDependants_Beneficiary_tran._Dpndtbeneficetranunkid = CInt(dgvDependantList.Items(mintItemIndex).Cells(enGridColumn.DpndtTranId).Text)

                Dim objDPStatus As New clsDependant_Benefice_Status_tran
                objDPStatus._Dpndtbeneficestatustranunkid = CInt(dgvDependantList.Items(mintItemIndex).Cells(enGridColumn.dpndtbeneficestatustranunkid).Text)
                objDPStatus._Isvoid = True
                objDPStatus._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objDPStatus._Voiduserunkid = CInt(Session("UserId"))
                objDPStatus._CompanyUnkid = CInt(Session("CompanyUnkId"))
                objDPStatus._ClientIP = Session("IP_ADD").ToString
                objDPStatus._FormName = mstrModuleName
                objDPStatus._HostName = Session("HOST_NAME").ToString
                objDPStatus._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                objDPStatus._AuditUserId = CInt(Session("UserId"))
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    objDPStatus._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                Else
                    objDPStatus._Loginemployeeunkid = 0
                End If

                Dim mstrVoidReason As String = popupDeleteReason.Reason
                objDPStatus._Voidreason = mstrVoidReason

                Dim eLMode As Integer
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    eLMode = enLogin_Mode.MGR_SELF_SERVICE
                Else
                    eLMode = enLogin_Mode.EMP_SELF_SERVICE
                End If

                If DependantApprovalFlowVal Is Nothing Then
                    Dim objADependantTran As New clsDependant_beneficiaries_approval_tran

                    objADependantTran._Isvoid = True
                    objADependantTran._Audituserunkid = CInt(Session("UserId"))
                    objADependantTran._Isweb = True
                    objADependantTran._Ip = Session("IP_ADD").ToString
                    objADependantTran._Host = Session("HOST_NAME").ToString
                    objADependantTran._Form_Name = mstrModuleName
                    objADependantTran._Audittype = enAuditType.DELETE
                    objADependantTran._Audituserunkid = CInt(Session("UserId"))
                    objADependantTran._Address = objDependants_Beneficiary_tran._Address
                    objADependantTran._Birthdate = objDependants_Beneficiary_tran._Birthdate
                    objADependantTran._Cityunkid = objDependants_Beneficiary_tran._Cityunkid
                    objADependantTran._Countryunkid = objDependants_Beneficiary_tran._Countryunkid
                    objADependantTran._Email = objDependants_Beneficiary_tran._Email
                    objADependantTran._Employeeunkid = objDependants_Beneficiary_tran._Employeeunkid
                    objADependantTran._First_Name = objDependants_Beneficiary_tran._First_Name
                    objADependantTran._Identify_No = objDependants_Beneficiary_tran._Identify_No
                    objADependantTran._Isdependant = objDependants_Beneficiary_tran._Isdependant

                    objADependantTran._Last_Name = objDependants_Beneficiary_tran._Last_Name
                    objADependantTran._Middle_Name = objDependants_Beneficiary_tran._Middle_Name
                    objADependantTran._Mobile_No = objDependants_Beneficiary_tran._Mobile_No
                    objADependantTran._Nationalityunkid = objDependants_Beneficiary_tran._Nationalityunkid
                    objADependantTran._Post_Box = objDependants_Beneficiary_tran._Post_Box
                    objADependantTran._Relationunkid = objDependants_Beneficiary_tran._Relationunkid
                    objADependantTran._Stateunkid = objDependants_Beneficiary_tran._Stateunkid
                    objADependantTran._Telephone_No = objDependants_Beneficiary_tran._Telephone_No
                    objADependantTran._Zipcodeunkid = objDependants_Beneficiary_tran._Zipcodeunkid
                    objADependantTran._Gender = objDependants_Beneficiary_tran._Gender
                    objADependantTran._CompanyId = CInt(Session("CompanyUnkId"))
                    objADependantTran._ImagePath = objDependants_Beneficiary_tran._ImagePath
                    objADependantTran._Dpndtbeneficetranunkid = CInt(dgvDependantList.Items(mintItemIndex).Cells(enGridColumn.DpndtTranId).Text)
                    objADependantTran._Isprocessed = False
                    objADependantTran._Isvoid = False
                    objADependantTran._Tranguid = Guid.NewGuid.ToString()
                    objADependantTran._Transactiondate = Now
                    objADependantTran._Approvalremark = ""
                    objADependantTran._Mappingunkid = 0
                    objADependantTran._Isfinal = False
                    objADependantTran._Operationtypeid = clsEmployeeDataApproval.enOperationType.DELETED
                    objADependantTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                    objADependantTran._IsFromStatus = True

                    objADependantTran._Effective_date = objDependants_Beneficiary_tran._Effective_date
                    objADependantTran._Isactive = objDependants_Beneficiary_tran._Isactive
                    objADependantTran._ReasonUnkid = objDependants_Beneficiary_tran._Reasonunkid
                    objADependantTran._DeletedpndtbeneficestatustranIDs = dgvDependantList.Items(mintItemIndex).Cells(enGridColumn.dpndtbeneficestatustranunkid).Text
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        objADependantTran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                    Else
                        objADependantTran._Loginemployeeunkid = 0
                    End If
                    objADependantTran._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime

                    If objADependantTran.DeleteStatus(CInt(dgvDependantList.Items(mintItemIndex).Cells(enGridColumn.dpndtbeneficestatustranunkid).Text), mstrVoidReason, CInt(Session("CompanyUnkId"))) = False Then
                        If objADependantTran._Message <> "" Then
                            msg.DisplayMessage(objADependantTran._Message, Me)
                        End If
                        Exit Sub

                    Else
                        objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                             CStr(Session("UserAccessModeSetting")), _
                                                             CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                             CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                             enScreenName.frmDependantsAndBeneficiariesList, CStr(Session("EmployeeAsOnDate")), _
                                                             CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                             Session("UserName").ToString, clsEmployeeDataApproval.enOperationType.DELETED, , objDependants_Beneficiary_tran._Employeeunkid.ToString, , , _
                                                             " dpndtbeneficetranunkid = " & objDependants_Beneficiary_tran._Dpndtbeneficetranunkid, Nothing, , , _
                                                             " dpndtbeneficetranunkid = " & objDependants_Beneficiary_tran._Dpndtbeneficetranunkid, Nothing)

                    End If

                Else

                    If objDPStatus.Delete() = False Then
                        If objDPStatus._Message <> "" Then
                            msg.DisplayMessage(objDPStatus._Message, Me)
                        End If
                        Exit Sub
                    End If
                End If

                Call FillGrid()
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DataGrid Events"

    Protected Sub dgvDependantList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvDependantList.ItemCommand
        Try
            If e.CommandName.ToUpper = "DELETE" Then

                mintItemIndex = e.Item.ItemIndex

                If mdtView.Select("IsGrp = 0 AND DpndtTranId = " & CInt(e.Item.Cells(enGridColumn.DpndtTranId).Text) & " ").Length <= 1 Then
                    Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 12, "Sorry, You cannot delete default transaction."), Me)
                    dgvDependantList.Focus()
                    Exit Try
                ElseIf CInt(e.Item.Cells(enGridColumn.ROWNO).Text) <> 1 Then
                    Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 13, "Sorry, You can delete last tranasction only."), Me)
                    dgvDependantList.Focus()
                    Exit Try
                End If

                Dim arrIDs() As String = {CInt(e.Item.Cells(enGridColumn.DpndtTranId).Text).ToString}
                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                Dim objApprovalData As New clsEmployeeDataApproval
                If DependantApprovalFlowVal Is Nothing AndAlso CBool(e.Item.Cells(enGridColumn.isapproved).Text) = True Then

                    If Session("PendingEmployeeScreenIDs").ToString.Trim.Length > 0 Then
                        If Session("PendingEmployeeScreenIDs").ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                            mblnOnlyApproved = False
                            mblnAddApprovalCondition = False
                        End If
                    End If
                    Dim objDBApproval As New clsDependant_beneficiaries_approval_tran
                    Dim dsPending As DataSet = objDBApproval.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), CStr(Session("UserAccessModeSetting")), mblnOnlyApproved, CBool(Session("IsIncludeInactiveEmp")), "Emp", False, , , , mblnAddApprovalCondition)

                    If dsPending.Tables(0).Rows.Count > 0 Then

                        Dim intCount As Integer = (From p In dsPending.Tables(0) Where (arrIDs.Contains(p.Item("DpndtTranId").ToString) = True) Select (p)).Count

                        If intCount > 0 Then
                            Language.setLanguage(mstrModuleName)
                            msg.DisplayMessage(Language.getMessage(mstrModuleName, 14, "Sorry, You cannot do any operation. Reason: Some of the transactions are in approval process.") & "\n" & "\n" & Language.getMessage(mstrModuleName, 15, "Please Approve / Reject transction."), Me)
                            dgvDependantList.Focus()
                            Exit Try
                        End If
                    End If

                    Dim dtEmp As DataTable = New DataView(mdtView, "IsGrp = 0 AND DpndtTranId = " & CInt(e.Item.Cells(enGridColumn.DpndtTranId).Text) & " ", "", DataViewRowState.CurrentRows).ToTable(True, "EmpId", "EmpName")
                    dtEmp.Columns.Add("Reason", System.Type.GetType("System.String")).DefaultValue = ""
                    dtEmp.Columns.Add("IsValid", System.Type.GetType("System.Boolean")).DefaultValue = True
                    For Each dtRow As DataRow In dtEmp.Rows

                        If objApprovalData.IsApproverPresent(enScreenName.frmDependantsAndBeneficiariesList, CStr(Session("Database_Name")), _
                                                        CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                        CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                        CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(dtRow.Item("EmpId")), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then

                            dtRow.Item("Reason") = objApprovalData._Message
                            dtRow.Item("IsValid") = False
                        End If
                    Next
                    dtEmp.AcceptChanges()

                    Dim strInvalidEmpIDs As String = "-999"
                    If dtEmp.Select("IsValid = 0").Length > 0 Then
                        strInvalidEmpIDs = String.Join(",", (From p In dtEmp Where (CBool(p.Item("IsValid")) = False) Select (p.Item("EmpId").ToString)).ToArray)

                        dtEmp.Columns.Remove("EmpId")
                        dtEmp.Columns.Remove("IsValid")

                        popupValidationList.Message = Language.getMessage(mstrModuleName, 16, "Sorry, No approver defined for the employee(s) given below! Do you want to proceed for the rest of the employees ?")
                        popupValidationList.Data_Table = dtEmp
                        popupValidationList.Show()

                        Exit Try
                    End If

                End If

                If CInt(e.Item.Cells(enGridColumn.ROWNO).Text) = 1 Then
                    popupDeleteReason.Reason = ""
                    popupDeleteReason.Title = Language.getMessage(mstrModuleName, 17, "Are you sure you want to delete this transaction?")

                    popupDeleteReason.Show()

                End If

            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvDependantList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvDependantList.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                Call SetDateFormat()
                dgvDependantList.Columns(enGridColumn.Delete).Visible = False

                If CInt(cboDependant.SelectedValue) > 0 Then
                    If mdtView.Select("IsGrp = 0 AND ROWNO = 2").Length > 0 Then
                        If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                            If CBool(Session("AllowToDeleteDependentStatus")) = True Then dgvDependantList.Columns(enGridColumn.Delete).Visible = True
                        Else
                            If CBool(Session("AllowDeleteDependants")) = True Then dgvDependantList.Columns(enGridColumn.Delete).Visible = True
                        End If
                    End If
                End If

                If e.Item.Cells(enGridColumn.dtbirthdate).Text <> "&nbsp;" AndAlso e.Item.Cells(enGridColumn.dtbirthdate).Text.ToString.Trim.Length > 0 Then
                    e.Item.Cells(enGridColumn.dtbirthdate).Text = CDate(e.Item.Cells(enGridColumn.dtbirthdate).Text).Date.ToShortDateString
                End If
                If e.Item.Cells(enGridColumn.dteffective_date).Text <> "&nbsp;" AndAlso e.Item.Cells(enGridColumn.dteffective_date).Text.ToString.Trim.Length > 0 Then
                    e.Item.Cells(enGridColumn.dteffective_date).Text = CDate(e.Item.Cells(enGridColumn.dteffective_date).Text).Date.ToShortDateString
                End If

                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If Session("PendingEmployeeScreenIDs").ToString.Trim.Length > 0 Then
                    If Session("PendingEmployeeScreenIDs").ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If
                Dim objDBApproval As New clsDependant_beneficiaries_approval_tran
                Dim dsPending As DataSet = objDBApproval.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), eZeeDate.convertDate(CStr(Session("EmployeeAsOnDate"))), CStr(Session("UserAccessModeSetting")), mblnOnlyApproved, CBool(Session("IsIncludeInactiveEmp")), "Emp", False, CInt(e.Item.Cells(enGridColumn.EmpId).Text), "dpndtbeneficetranunkid = " & CInt(e.Item.Cells(enGridColumn.DpndtTranId).Text) & " ", , mblnAddApprovalCondition)
                If dsPending.Tables(0).Rows.Count > 0 Then
                    e.Item.BackColor = Color.PowderBlue
                    e.Item.ForeColor = Color.Black
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)

            Me.lblDependant.Text = Language._Object.getCaption(Me.lblDependant.ID, Me.lblDependant.Text)
            Me.lblBirthdate.Text = Language._Object.getCaption(Me.lblBirthdate.ID, Me.lblBirthdate.Text)
            Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.ID, Me.lblGender.Text)
            'Me.lnkAllocations.Text = Language._Object.getCaption(Me.lnkAllocations.ID, Me.lnkAllocations.Text)
            Me.lblDBIdNo.Text = Language._Object.getCaption(Me.lblDBIdNo.ID, Me.lblDBIdNo.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblDBRelation.Text = Language._Object.getCaption(Me.lblDBRelation.ID, Me.lblDBRelation.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)
            Me.BtnSetActive.Text = Language._Object.getCaption(Me.BtnSetActive.ID, Me.BtnSetActive.Text).Replace("&", "")
            Me.BtnSetInactive.Text = Language._Object.getCaption(Me.BtnSetInactive.ID, Me.BtnSetInactive.Text).Replace("&", "")
            Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.ID, Me.lblEffectiveDate.Text)
            Me.lblReason.Text = Language._Object.getCaption(Me.lblReason.ID, Me.lblReason.Text)

            dgvDependantList.Columns(enGridColumn.DpndtBefName).HeaderText = Language._Object.getCaption(dgvDependantList.Columns(enGridColumn.DpndtBefName).FooterText, dgvDependantList.Columns(enGridColumn.DpndtBefName).HeaderText)
            dgvDependantList.Columns(enGridColumn.Relation).HeaderText = Language._Object.getCaption(dgvDependantList.Columns(enGridColumn.Relation).FooterText, dgvDependantList.Columns(enGridColumn.Relation).HeaderText)
            dgvDependantList.Columns(enGridColumn.Gender).HeaderText = Language._Object.getCaption(dgvDependantList.Columns(enGridColumn.Gender).FooterText, dgvDependantList.Columns(enGridColumn.Gender).HeaderText)
            dgvDependantList.Columns(enGridColumn.dtbirthdate).HeaderText = Language._Object.getCaption(dgvDependantList.Columns(enGridColumn.dtbirthdate).FooterText, dgvDependantList.Columns(enGridColumn.dtbirthdate).HeaderText)
            dgvDependantList.Columns(enGridColumn.dteffective_date).HeaderText = Language._Object.getCaption(dgvDependantList.Columns(enGridColumn.dteffective_date).FooterText, dgvDependantList.Columns(enGridColumn.dteffective_date).HeaderText)
            dgvDependantList.Columns(enGridColumn.isactive).HeaderText = Language._Object.getCaption(dgvDependantList.Columns(enGridColumn.isactive).FooterText, dgvDependantList.Columns(enGridColumn.isactive).HeaderText)
            dgvDependantList.Columns(enGridColumn.Reason).HeaderText = Language._Object.getCaption(dgvDependantList.Columns(enGridColumn.Reason).FooterText, dgvDependantList.Columns(enGridColumn.Reason).HeaderText)

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    
End Class
