﻿<%@ Page Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPgBenefitList.aspx.vb" Inherits="wPgBenefitList" Title="Employee Benefits" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Benefits"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table width="100%">
                                        <tr style="width: 100%">
                                            <td colspan="4">
                                                <asp:CheckBox ID="chkShowPending" runat="server" Text="Show Pending Employee" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="drpEmployee" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblBenefitType" runat="server" Text="Benefit Group"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="drpBenefitGroup" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblBenefitPlan" runat="server" Text="Plan"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="drpPlan" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblTransactionHead" runat="server" Text="Tran.Head"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <asp:DropDownList ID="DrpTranHead" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td colspan="4" style="width: 100%">
                                                <asp:RadioButtonList ID="radiolist1" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="0">Show Void</asp:ListItem>
                                                    <asp:ListItem Selected="true" Value="1">Show Active</asp:ListItem>
                                                    <asp:ListItem Value="2">Show All</asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnnew" runat="server" Text="New" CssClass="btndefault" Visible="False" />
                                        <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnreset" runat="server" Text="Reset" CssClass="btndefault" />
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                        
                                    </div>
                                </div>
                                <asp:Panel ID="pnl_dgvBenefit" runat="server" ScrollBars="Auto">
                                    <asp:DataGrid ID="dgvBenefit" runat="server" AutoGenerateColumns="False" Width="99%"
                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                        AllowPaging="false" HeaderStyle-Font-Bold="false">
                                        <Columns>
                                            <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgSelect" runat="server" CssClass="gridedit" ToolTip="Select"
                                                            CommandName="Select"></asp:LinkButton>
                                                    </span>
                                                    <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn Visible="false" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                            CommandName="Delete"></asp:LinkButton>
                                                    </span>
                                                    <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="BGroupName" HeaderText="Benefit Group" ReadOnly="True"
                                                FooterText="colhBenefitGroup"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="BPlanName" HeaderText="Benefit Plan" ReadOnly="True"
                                                FooterText="colhBenefitPlan"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="TranHead" HeaderText="Transaction Head" ReadOnly="True"
                                                FooterText="colhTransactionHead"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="isvoid" HeaderText="isvoid" ReadOnly="True" Visible="false">
                                            </asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
