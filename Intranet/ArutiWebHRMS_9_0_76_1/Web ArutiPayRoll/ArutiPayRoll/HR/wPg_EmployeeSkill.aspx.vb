﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports Aruti.Data

#End Region

Partial Class HR_wPg_EmployeeSkill
    Inherits Basepage

#Region " Private Variable(s) "

    Private objEmpSkill As New clsEmployee_Skill_Tran
    Dim msg As New CommonCodes
    Dim clsuser As New User


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeSkills_AddEdit"
    'Pinkal (06-May-2014) -- End

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintSkillUnkid As Integer = -1

    Private mintTransactionId As Integer = 0
    Private objASkillTran As New clsEmployeeSkill_Approval_Tran

    Dim Arr() As String
    Dim SkillApprovalFlowVal As String
    'Gajanan [17-DEC-2018] -- End


    'Gajanan [3-April-2019] -- Start
    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Dim isEmployeeApprove As Boolean = False    
    Dim mblnisEmployeeApprove As Boolean = False
    'Gajanan [22-Feb-2019] -- End
    'Gajanan [3-April-2019] -- End

    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End


    'Gajanan [21-June-2019] -- Start      
    Private OldData As New clsEmployee_Skill_Tran
    'Gajanan [21-June-2019] -- End
    'Sohail (09 Nov 2020) -- Start
    'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
    Private mstrURLReferer As String = ""
    'Sohail (09 Nov 2020) -- End

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objCMaster As New clsCommon_Master
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsCombos = objEmployee.GetEmployeeList("Emp", True, )
                '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END
                'End If


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .



                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                'If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                If Session("PendingEmployeeScreenIDs").ToString.Trim.Length > 0 Then
                    'If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployee_Skill_List))) Then
                    If Session("PendingEmployeeScreenIDs").ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployee_Skill_List))) Then
                        'Gajanan [22-Feb-2019] -- End
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If



                'dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                '                                       CInt(Session("UserId")), _
                '                                       CInt(Session("Fin_year")), _
                '                                       CInt(Session("CompanyUnkId")), _
                '                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                       CStr(Session("UserAccessModeSetting")), True, _
                '                                       CBool(Session("IsIncludeInactiveEmp")), "Emp", True)

                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       CStr(Session("UserAccessModeSetting")), _
                                                       mblnOnlyApproved, CBool(Session("IsIncludeInactiveEmp")), "Emp", True, _
                                                       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


                'Shani(20-Nov-2015) -- End
                'S.SANDEEP [20-JUN-2018] -- End


                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombos.Tables("Emp")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()
            End If

            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "List")
            With drpSkillCategory
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            Dim objSMaster As New clsskill_master
            dsCombos = objSMaster.getComboList("List", True, CInt(drpSkillCategory.SelectedValue))
            With drpSkill
                .DataValueField = "skillunkid"
                .DataTextField = "NAME"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Public Function b64decode(ByVal StrDecode As String) As String
        Dim decodedString As String = ""
        Try
            decodedString = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(StrDecode))
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        Return decodedString
    End Function

    Private Function IsFromSkillList() As Boolean
        Try


            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If (Request.QueryString("ProcessId") <> "") Then
            If (mintSkillUnkid > 0) Then

                'Dim mintSkillUnkid As Integer = -1
                'mintSkillUnkid = CInt(Val(b64decode(Request.QueryString("ProcessId"))))
                'Me.ViewState.Add("SkillUnkid", mintSkillUnkid)
                objEmpSkill._Skillstranunkid = mintSkillUnkid
                Call Fill_Info()


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                drpEmployee.Enabled = False
                'Gajanan [17-DEC-2018] -- End

            Else
                'Me.ViewState.Add("SkillUnkid", -1)
                drpEmployee.Enabled = True
            End If
            'Gajanan [17-DEC-2018] -- End

            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                'If Me.ViewState("SkillUnkid") Is Nothing Or CInt(Me.ViewState("SkillUnkid")) <= 0 Then

                If mintSkillUnkid <= 0 Then
                    btnSaveInfo.Visible = CBool(Session("AddEmployeeSkill"))
                Else
                    btnSaveInfo.Visible = CBool(Session("EditEmployeeSkill"))
                End If
                'Gajanan [17-DEC-2018] -- End

            Else

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If Me.ViewState("SkillUnkid") Is Nothing Or CInt(Me.ViewState("SkillUnkid")) <= 0 Then

                If mintSkillUnkid <= 0 Then
                    btnSaveInfo.Visible = CBool(Session("AllowAddSkills"))
                Else
                    btnSaveInfo.Visible = CBool(Session("AllowEditSkills"))
                End If
            End If

            'Pinkal (22-Nov-2012) -- End
            'Gajanan [17-DEC-2018] -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub Fill_Info()
        Try
            drpEmployee.SelectedValue = CStr(objEmpSkill._Emp_App_Unkid)
            drpSkillCategory.SelectedValue = objEmpSkill._Skillcategoryunkid.ToString

            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            drpSkillCategory_SelectedIndexChanged(New Object, New EventArgs())
            'Pinkal (28-Dec-2015) -- End

            drpSkill.SelectedValue = objEmpSkill._Skillunkid.ToString
            txtDescription.Text = objEmpSkill._Description


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(drpEmployee.SelectedValue)
            mblnisEmployeeApprove = objEmployee._Isapproved
            'Gajanan [17-April-2019] -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'Dim objEmployee As New clsEmployee_Master
            'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(drpEmployee.SelectedValue)
            ''Gajanan [3-April-2019] -- Start
            'mblnisEmployeeApprove = objEmployee._Isapproved
            ''Gajanan [3-April-2019] -- End

            'If SkillApprovalFlowVal Is Nothing Then
            If SkillApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then
                'Gajanan [22-Feb-2019] -- End
                objASkillTran._Audittype = enAuditType.ADD
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    objASkillTran._Audituserunkid = CInt(Session("UserId"))
                Else
                    objASkillTran._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                End If
                objASkillTran._Description = txtDescription.Text
                objASkillTran._Emp_App_Unkid = CInt(drpEmployee.SelectedValue)
                objASkillTran._Isapplicant = False

                objASkillTran._Skillcategoryunkid = CInt(drpSkillCategory.SelectedValue)
                objASkillTran._Skillunkid = CInt(drpSkill.SelectedValue)
                objASkillTran._Isvoid = False
                objASkillTran._Tranguid = Guid.NewGuid.ToString()
                objASkillTran._Transactiondate = Now
                objASkillTran._Approvalremark = ""
                objASkillTran._Isweb = True
                objASkillTran._Isfinal = False
                objASkillTran._Ip = Session("IP_ADD").ToString()
                objASkillTran._Host = Session("HOST_NAME").ToString()
                objASkillTran._Form_Name = mstrModuleName
                objASkillTran._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                If mintSkillUnkid <= 0 Then
                    objASkillTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.ADDED
                    objASkillTran._Skillstranunkid = -1
                Else
                    objASkillTran._Skillstranunkid = mintSkillUnkid
                    objASkillTran._OperationTypeId = clsEmployeeDataApproval.enOperationType.EDITED
                End If
            Else
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If CInt(Me.ViewState("SkillUnkid")) <= 0 Then

                If mintSkillUnkid <= 0 Then
                    objEmpSkill._Skillstranunkid = -1
                Else
                    'objEmpSkill._Skillstranunkid = CInt(Me.ViewState("SkillUnkid"))
                    objEmpSkill._Skillstranunkid = mintSkillUnkid
                End If
                'Gajanan [17-DEC-2018] -- End
                objEmpSkill._Description = txtDescription.Text
                objEmpSkill._Emp_App_Unkid = CInt(drpEmployee.SelectedValue)
                objEmpSkill._Isapplicant = False
                objEmpSkill._Isvoid = False
                objEmpSkill._Skillcategoryunkid = CInt(drpSkillCategory.SelectedValue)
                objEmpSkill._Skillunkid = CInt(drpSkill.SelectedValue)
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    objEmpSkill._Userunkid = CInt(Session("UserId"))
                Else
                    objEmpSkill._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                End If
                objEmpSkill._Voiddatetime = Nothing
                objEmpSkill._Voidreason = ""
                objEmpSkill._Voiduserunkid = -1

                'S.SANDEEP [ 04 JULY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES

                Blank_ModuleName()
                objEmpSkill._WebFormName = "frmEmployeeSkills_AddEdit"
                StrModuleName2 = "mnuPersonnel"
                StrModuleName3 = "mnuEmployeeData"
                objEmpSkill._WebClientIP = CStr(Session("IP_ADD"))
                objEmpSkill._WebHostName = CStr(Session("HOST_NAME"))
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    objEmpSkill._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                End If

                'S.SANDEEP [ 04 JULY 2012 ] -- END
            End If

            'Gajanan [17-DEC-2018] -- End




        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ClearObject()
        Try
            txtDescription.Text = "" : drpSkill.SelectedValue = CStr(0) : drpSkillCategory.SelectedValue = CStr(0)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function IsDataValid() As Boolean
        Try



            'Gajanan [21-June-2019] -- Start      

            If (mintSkillUnkid > 0) Then
                objEmpSkill._Skillstranunkid = mintSkillUnkid
                OldData = objEmpSkill
            End If

            If IsNothing(OldData) = False Then
                If OldData._Skillcategoryunkid = CInt(drpSkillCategory.SelectedValue) AndAlso _
                                   OldData._Skillunkid = CInt(drpSkill.SelectedValue) AndAlso _
                                   OldData._Description = txtDescription.Text Then

                    If (mintSkillUnkid > 0) Then
                        objEmpSkill = Nothing
                        OldData = Nothing
                        Response.Redirect(Session("rootpath").ToString & "HR/wPg_EmployeeSkillList.aspx", False)
                        Return False
                    End If
                Else
                    OldData = Nothing
                End If
            End If
            'Gajanan [21-June-2019] -- End

            If CInt(drpEmployee.SelectedValue) <= 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), Me)
                'Pinkal (06-May-2014) -- End
                drpEmployee.Focus()
                Return False
            End If

            If CInt(drpSkillCategory.SelectedValue) <= 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Skill Category is compulsory information. Please select Skill Category to continue."), Me)
                'Pinkal (06-May-2014) -- End
                drpSkillCategory.Focus()
                Return False
            End If

            If CInt(drpSkill.SelectedValue) <= 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Language.getMessage(mstrModuleName, 3, "Skill is compulsory information. Please select Skill to continue."), Me)
                'Pinkal (06-May-2014) -- End
                drpSkill.Focus()
                Return False
            End If

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.





            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'If SkillApprovalFlowVal Is Nothing Then
            If SkillApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then
                'Gajanan [17-April-2019] -- End



                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                If objApprovalData.IsApproverPresent(enScreenName.frmEmployee_Skill_List, CStr(Session("Database_Name")), _
                                                      CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                      CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                      CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(drpEmployee.SelectedValue.ToString()), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    msg.DisplayMessage(objApprovalData._Message, Me)
                    Return False
                End If
                'Gajanan [17-April-2019] -- End


                If objEmpSkill.isExist(CInt(drpEmployee.SelectedValue), CInt(drpSkill.SelectedValue), False, mintSkillUnkid) Then
                    msg.DisplayMessage(Language.getMessage(mstrModuleName, 106, "Selected skill is already assigned to the employee. Please assign new skill."), Me)
                    Return False
                End If
                Dim intOperationType As Integer = 0 : Dim strMsg As String = String.Empty
                If objASkillTran.isExist(CInt(drpEmployee.SelectedValue), False, Nothing, CInt(drpSkill.SelectedValue), "", Nothing, True, intOperationType) Then
                    If intOperationType > 0 Then
                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED
                                If mintSkillUnkid > 0 Then
                                    strMsg = Language.getMessage(mstrModuleName, 100, "Sorry, you cannot edit seleted information, Reason : Same skill is already present in approval process in edit mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 101, "Sorry, you cannot add seleted information, Reason : Same skill is already present in approval process in edit mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.DELETED
                                If mintSkillUnkid > 0 Then
                                    strMsg = Language.getMessage(mstrModuleName, 102, "Sorry, you cannot edit seleted information, Reason : Same skill is already present in approval process in deleted mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 103, "Sorry, you cannot add seleted information, Reason : Same skill is already present in approval process in deleted mode.")
                                End If
                            Case clsEmployeeDataApproval.enOperationType.ADDED
                                If mintSkillUnkid > 0 Then
                                    strMsg = Language.getMessage(mstrModuleName, 104, "Sorry, you cannot edit seleted information, Reason : Same skill is already present in approval process in added mode.")
                                Else
                                    strMsg = Language.getMessage(mstrModuleName, 105, "Sorry, you cannot add seleted information, Reason : Same skill is already present in approval process in added mode.")
                                End If
                        End Select
                    End If
                End If
                If strMsg.Trim.Length > 0 Then
                    msg.DisplayMessage(strMsg, Me)
                    Return False
                End If
            End If
            'Gajanan [17-DEC-2018] -- End

            Return True
        Catch ex As Exception
            msg.DisplayError(ex, Me)
            Return False
        Finally
        End Try
    End Function

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                'Anjan [20 February 2016] -- End
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            'Hemant (31 May 2019) -- Start
            'ISSUE/ENHANCEMENT : UAT Changes
            Call SetMessages()
            Call Language._Object.SaveValue()
            'Hemant (31 May 2019) -- End
            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End


            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If (Request.QueryString("ProcessId") <> Nothing) Then
                mintSkillUnkid = CInt(Val(b64decode(Request.QueryString("ProcessId"))))
            End If
            'Gajanan [17-DEC-2018] -- End


            If (Page.IsPostBack = False) Then
                clsuser = CType(Session("clsuser"), User)

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    Aruti.Data.User._Object._Userunkid = CInt(Session("UserId"))


                    'Anjan (30 May 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'BtnSave.Visible = False
                    btnSaveInfo.Visible = CBool(Session("EditEmployeeSkill"))
                    'Anjan (30 May 2012)-End 
                Else

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = -1
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request


                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'BtnSave.Visible = ConfigParameter._Object._AllowEditSkills
                    btnSaveInfo.Visible = CBool(Session("AllowEditSkills"))
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-End 

                    'Gajanan [3-April-2019] -- Start
                    If IsNothing(ViewState("mblnisEmployeeApprove")) = False Then
                        mblnisEmployeeApprove = CBool(ViewState("mblnisEmployeeApprove"))
                    End If
                    'Gajanan [3-April-2019] -- End
                End If

                Call FillCombo()

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'If (Request.QueryString("ProcessId") = "") Then
                If mintSkillUnkid > 0 Then
                    If Session("Skill_EmpUnkID") IsNot Nothing Then
                        drpEmployee.SelectedValue = CStr(Session("Skill_EmpUnkID"))
                    End If
                End If
                'SHANI [09 Mar 2015]--END 

                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                If Request.UrlReferrer IsNot Nothing Then
                    mstrURLReferer = Request.UrlReferrer.AbsoluteUri
                Else
                    mstrURLReferer = Session("rootpath").ToString & "HR/wPg_EmployeeSkillList.aspx"
                End If
            Else
                mstrURLReferer = ViewState("mstrURLReferer").ToString
                'Sohail (09 Nov 2020) -- End
            End If

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            SkillApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmEmployee_Skill_List)))
            'Gajanan [17-DEC-2018] -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'Nilay (01-Feb-2015) -- End
        End Try
    End Sub

    Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
        Try
            If Not IsPostBack Then
                IsFromSkillList()
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    ToolbarEntry1.VisibleSaveButton(False)
    '    ToolbarEntry1.VisibleCancelButton(False)
    '    ToolbarEntry1.VisibleNewButton(False)
    'End Sub
    'Nilay (01-Feb-2015) -- End
    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session("IsFrom_AddEdit") = False
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub


    'Gajanan [3-April-2019] -- Start
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mblnisEmployeeApprove") = mblnisEmployeeApprove
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            ViewState("mstrURLReferer") = mstrURLReferer
            'Sohail (09 Nov 2020) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [3-April-2019] -- End

#End Region

#Region " Button's Event(s) "

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveInfo.Click
        Dim blnFlag As Boolean = False
        Try

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(drpEmployee.SelectedValue)
            mblnisEmployeeApprove = objEmployee._Isapproved
            'Gajanan [17-April-2019] -- End


            If IsDataValid() = False Then Exit Sub

            Call SetValue()


            'Gajanan [3-April-2019] -- Start
            ''Gajanan [22-Feb-2019] -- Start
            ''Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Dim objEmployee As New clsEmployee_Master
            'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(drpEmployee.SelectedValue)
            ''Gajanan [22-Feb-2019] -- End
            'Gajanan [3-April-2019] -- End

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If CInt(Me.ViewState("SkillUnkid")) <= 0 Then

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim eLMode As Integer
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                eLMode = enLogin_Mode.MGR_SELF_SERVICE
            Else
                eLMode = enLogin_Mode.EMP_SELF_SERVICE
            End If
            'Gajanan [17-April-2019] -- End


            If mintSkillUnkid <= 0 Then
                'Gajanan [17-DEC-2018] -- End


                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.




                'If objEmpSkill.Insert = False Then
                '    msg.DisplayMessage(objEmpSkill._Message, Me)
                'Else
                '    msg.DisplayMessage("Skill successfully added.", Me)
                'End If 

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If SkillApprovalFlowVal Is Nothing Then

                'Gajanan [3-April-2019] -- Start
                'If SkillApprovalFlowVal Is Nothing AndAlso isEmployeeApprove = True Then
                If SkillApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then
                    'Gajanan [3-April-2019] -- End

                    'Gajanan [22-Feb-2019] -- End
                    blnFlag = objASkillTran.Insert(CInt(Session("Companyunkid")))
                    If blnFlag = False Then
                        msg.DisplayMessage(objASkillTran._Message, Me)
                        Exit Sub
                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    Else
                        objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                         CStr(Session("UserAccessModeSetting")), _
                                                         CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                         CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                         enScreenName.frmEmployee_Skill_List, CStr(Session("EmployeeAsOnDate")), _
                                                         CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                         Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.ADDED, , drpEmployee.SelectedValue.ToString(), , , _
                                                         "employeeunkid= " & drpEmployee.SelectedValue.ToString() & " and skillunkid = " & drpSkill.SelectedValue.ToString(), Nothing, , , _
                                                         "emp_app_unkid= " & drpEmployee.SelectedValue.ToString() & " and skillunkid = " & drpSkill.SelectedValue.ToString(), Nothing)
                        'Gajanan [17-April-2019] -- End

                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("Skill successfully added.", Me)
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 2001, "Skill successfully added."), Me)
                        'Hemant (31 May 2019) -- End
                    End If
                Else
                    blnFlag = objEmpSkill.Insert()

                    If blnFlag = False Then
                        msg.DisplayMessage(objEmpSkill._Message, Me)
                    Else
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("Skill successfully added.", Me)
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 2001, "Skill successfully added."), Me)
                        'Hemant (31 May 2019) -- End
                    End If
                End If

                'Gajanan [17-DEC-2018] -- End

                Call ClearObject()
            Else
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If objEmpSkill.Update = False Then
                '    msg.DisplayMessage(objEmpSkill._Message, Me)
                'Else
                '    msg.DisplayMessage("Skill successfully updated", Me)
                '    Response.Redirect("wPg_EmployeeSkillList.aspx")
                'End If


                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If SkillApprovalFlowVal Is Nothing AndAlso mintSkillUnkid > 0 Then

                'Gajanan [3-April-2019] -- Start



                'If SkillApprovalFlowVal Is Nothing AndAlso mintSkillUnkid > 0 AndAlso isEmployeeApprove = True Then
                If SkillApprovalFlowVal Is Nothing AndAlso mintSkillUnkid > 0 AndAlso mblnisEmployeeApprove = True Then
                    'Gajanan [3-April-2019] -- End

                    'Gajanan [22-Feb-2019] -- End
                    blnFlag = objASkillTran.Insert(CInt(Session("Companyunkid")))
                    If blnFlag = False Then
                        msg.DisplayMessage(objASkillTran._Message, Me)
                        Exit Sub
                    Else

                        objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                                 CStr(Session("UserAccessModeSetting")), _
                                                                 CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                                 CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                                 enScreenName.frmEmployee_Skill_List, CStr(Session("EmployeeAsOnDate")), _
                                                                 CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                                 Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.EDITED, , drpEmployee.SelectedValue.ToString(), , , _
                                                                 "employeeunkid= " & drpEmployee.SelectedValue.ToString() & " and skillunkid = " & drpSkill.SelectedValue.ToString(), Nothing, , , _
                                                                 "emp_app_unkid= " & drpEmployee.SelectedValue.ToString() & " and skillunkid = " & drpSkill.SelectedValue.ToString(), Nothing)

                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'msg.DisplayMessage("Skill successfully added.", Me)
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("Skill successfully updated", Me)
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 2002, "Skill successfully updated"), Me)
                        'Hemant (31 May 2019) -- End
                        Response.Redirect("wPg_EmployeeSkillList.aspx", False)
                        'Gajanan [17-April-2019] -- End

                    End If
                Else
                    If objEmpSkill.Update = False Then
                        msg.DisplayMessage(objEmpSkill._Message, Me)
                    Else
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("Skill successfully updated", Me)
                        msg.DisplayMessage(Language.getMessage(mstrModuleName, 2002, "Skill successfully updated"), Me)
                        'Hemant (31 May 2019) -- End
                        Response.Redirect("wPg_EmployeeSkillList.aspx", False)
                    End If
                End If
                'Gajanan [17-DEC-2018] -- End
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click, Closebotton1.CloseButton_click
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        'Nilay (01-Feb-2015) -- End
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\UserHome.aspx", False)
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            'Response.Redirect(Session("rootpath").ToString & "HR/wPg_EmployeeSkillList.aspx", False)
            Response.Redirect(mstrURLReferer, False)
            'Sohail (09 Nov 2020) -- End
            'Nilay (02-Mar-2015) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnClose_Click Event : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page. 

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region " Control Event(s) "

    Protected Sub drpSkillCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpSkillCategory.SelectedIndexChanged
        Try
            Dim dsCombos As New DataSet
            Dim objSMaster As New clsskill_master
            dsCombos = objSMaster.getComboList("List", True, CInt(drpSkillCategory.SelectedValue))
            With drpSkill
                .DataValueField = "skillunkid"
                .DataTextField = "NAME"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        Response.Redirect("wPg_EmployeeSkillList.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        Language.setLanguage(mstrModuleName)
        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        ' Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End
        Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.lblSkillcategory.Text = Language._Object.getCaption(Me.lblSkillcategory.ID, Me.lblSkillcategory.Text)
        Me.lblSkillDescription.Text = Language._Object.getCaption(Me.lblSkillDescription.ID, Me.lblSkillDescription.Text)
        Me.lblSkill.Text = Language._Object.getCaption(Me.lblSkill.ID, Me.lblSkill.Text)

        Me.btnSaveInfo.Text = Language._Object.getCaption(Me.btnSaveInfo.ID, Me.btnSaveInfo.Text).Replace("&", "")
        Me.BtnClose.Text = Language._Object.getCaption(Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End


    End Sub

    'Pinkal (06-May-2014) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Skill Category is compulsory information. Please select Skill Category to continue.")
            Language.setMessage(mstrModuleName, 3, "Skill is compulsory information. Please select Skill to continue.")
            Language.setMessage(mstrModuleName, 100, "Sorry, you cannot edit seleted information, Reason : Same skill is already present in approval process in edit mode.")
            Language.setMessage(mstrModuleName, 101, "Sorry, you cannot add seleted information, Reason : Same skill is already present in approval process in edit mode.")
            Language.setMessage(mstrModuleName, 102, "Sorry, you cannot edit seleted information, Reason : Same skill is already present in approval process in deleted mode.")
            Language.setMessage(mstrModuleName, 103, "Sorry, you cannot add seleted information, Reason : Same skill is already present in approval process in deleted mode.")
            Language.setMessage(mstrModuleName, 104, "Sorry, you cannot edit seleted information, Reason : Same skill is already present in approval process in added mode.")
            Language.setMessage(mstrModuleName, 105, "Sorry, you cannot add seleted information, Reason : Same skill is already present in approval process in added mode.")
            Language.setMessage(mstrModuleName, 106, "Selected skill is already assigned to the employee. Please assign new skill.")
            Language.setMessage(mstrModuleName, 2001, "Skill successfully added.")
            Language.setMessage(mstrModuleName, 2002, "Skill successfully updated")

        Catch Ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
