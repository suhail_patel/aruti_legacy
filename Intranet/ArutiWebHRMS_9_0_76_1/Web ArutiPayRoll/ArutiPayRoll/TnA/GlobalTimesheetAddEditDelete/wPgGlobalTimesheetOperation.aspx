<%@ Page Title="Global Timesheet Add/Edit/Delete" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgGlobalTimesheetOperation.aspx.vb" Inherits="wPgGlobalTimesheetOperation" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/TimeCtrl.ascx" TagName="TimeCtrl" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_employee/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <center>
        <asp:Panel ID="pnlMain" runat="server" Style="width: 50%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Global Timesheet Edit/Delete"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%; margin-bottom: 10px">
                                            <td style="width: 20%; vertical-align: top">
                                                <asp:Label ID="lblOprType" runat="server" Text="Operation Type"></asp:Label>
                                            </td>
                                            <td style="width: 30%; vertical-align: top">
                                                <asp:RadioButtonList ID="radOperation" runat="server" RepeatDirection="Vertical"
                                                    AutoPostBack="true">
                                                    <asp:ListItem Text="Add Timesheet" Selected="True" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Edit Timesheet" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="Delete Timesheet" Value="3"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </td>
                                            <td style="width: 50%" colspan="2">
                                                <asp:CheckBox ID="chkOverWriteIfExist" runat="server" Text="Overwrite If Exist" />
                                                </br>
                                                <asp:CheckBox ID="chkIncludeHoliday" runat="server" Text="Include Holiday" />
                                                </br>
                                                <asp:CheckBox ID="chkIncludeWeekend" runat="server" Text="Include Weekend" />
                                                </br>
                                                <asp:CheckBox ID="chkIncludeDayOff" runat="server" Text="Include DayOff" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblFdate" runat="server" Text="From Date"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <uc2:DateCtrl ID="dtpFromDate" runat="server" AutoPostBack="true" />
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblInTime" runat="server" Text="In Time"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <uc4:TimeCtrl ID="dtpInTime" runat="server" AutoPostBack="true" />
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblToDate" runat="server" Text="To Date"></asp:Label>
                                            </td>
                                            <td style="width: 35%">
                                                <uc2:DateCtrl AutoPostBack="true" runat="server" ID="dtpToDate" />
                                            </td>
                                            <td style="width: 15%">
                                                <asp:Label ID="lblOutTime" runat="server" Text="Out Time"></asp:Label>
                                            </td>
                                            <td style="width: 30%">
                                                <uc4:TimeCtrl ID="dtpOutTime" runat="server" AutoPostBack="true"></uc4:TimeCtrl>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 20%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboEmplyee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblShift" runat="server" Text="Shift"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboShift" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%">
                                                <asp:Label ID="lblPolicy" runat="server" Text="Policy"></asp:Label>
                                            </td>
                                            <td style="width: 80%" colspan="3">
                                                <asp:DropDownList ID="cboPolicy" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnSearch" runat="server" CssClass="btnDefault" Text="Search" />
                                        <asp:Button ID="btnReset" runat="server" CssClass="btnDefault" Text="Reset" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-body-default" style="position: relative">
                                    <table style="width: 100%; margin-top: 5px">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <asp:TextBox ID="txtSearch" style="margin-left:3px" runat="server" AutoPostBack="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <asp:Panel ID="pnlGrid" ScrollBars="Auto" runat="server" Style="margin-top: 5px;
                                                    max-height: 300px">
                                                    <asp:GridView ID="dgvEmp" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                        HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                        HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkHeder1" runat="server" AutoPostBack="true" Enabled="true" OnCheckedChanged="chkHeder1_CheckedChanged" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkbox1" runat="server" AutoPostBack="true" CommandArgument="<%# Container.DataItemIndex %>"
                                                                        Enabled="true" OnCheckedChanged="chkbox1_CheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode" />
                                                            <asp:BoundField DataField="employeename" HeaderText="Employee" FooterText="dgcolhEName" />
                                                            <asp:BoundField DataField="employeeunkid" HeaderText="employeeunkid" Visible="false" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="btnfixedbottom" class="btn-default">
                                        <asp:Button ID="btnSave" runat="server" CssClass="btnDefault" Text="Save" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="btnDefault" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc8:DeleteReason ID="voidReason" runat="server" Title="Are you sure you want to delete selected employee data for the selected date range?" />
                    <uc9:Confirmation ID="DateCofirmantion" runat="server" Title="Confirmation" Message="Out Time is less than In time.So Out Time will be considered for next day.Are you sure you want to continue ?" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
