﻿Option Strict On

#Region " Imports "

Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.Data

#End Region

Partial Class TnA_Shift_Policy_Global_Assign_wPg_PolicyGlobalAssign
    Inherits Basepage

#Region " Private Variable "
    Dim DisplayMessage As New CommonCodes
    Dim objEmp_Policy As clsemployee_policy_tran

    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private mstrModuleName As String = "frmShiftPolicyGlobalAssign"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            objEmp_Policy = New clsemployee_policy_tran


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End


            If Not IsPostBack Then
                Fill_Combo()
                dtpEffectiveDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpEffectiveDate_TextChanged(New Object, New EventArgs())
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub Fill_Combo()
        Dim dsCombo As New DataSet
        Dim objPolicy As New clspolicy_master
        Try
            dsCombo = objPolicy.getListForCombo("List", True)
            With cboPolicy
                .DataValueField = "policyunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Pinkal (06-Nov-2017) -- Start
            'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
            With cboFilterPolicy
                .DataValueField = "policyunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List").Copy()
                .DataBind()
                .SelectedValue = "0"
            End With
            'Pinkal (06-Nov-2017) -- End

            Me.ViewState.Add("GetShiftList", dsCombo.Tables(0))

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Combo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objPolicy = Nothing
        End Try
    End Sub

    Private Function isValid_Data() As Boolean
        Try
            Dim mdtEmployee As DataTable = Nothing
            Dim objMaster As New clsMasterData
            Dim iPeriod As Integer = 0
            Dim objPrd As New clscommom_period_Tran

            iPeriod = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpEffectiveDate.GetDate.Date)

            If iPeriod > 0 Then
                objPrd._Periodunkid(Session("Database_Name").ToString()) = iPeriod
                If objPrd._Statusid = 2 Then  ' FOR ENSTATUSTYPE = 1 OPEN AND ENSTATUSTYPE  = 2 CLOSE
                    DisplayMessage.DisplayMessage("Sorry, you cannot assign policy. Reason : Period is already closed for the selected date.", Me)
                    Return False
                End If
            End If
            objMaster = Nothing


            mdtEmployee = CType(Me.ViewState("EmployeeList"), DataTable)
            Dim dtmp() As DataRow = mdtEmployee.Select("ischeck=true")

            If dtmp.Length <= 0 Then
                DisplayMessage.DisplayMessage("Please check atleast one employee in order to assign policy to employee(s).", Me)
                Return False
            End If

            If iPeriod > 0 Then
                Dim objTnALvTran As New clsTnALeaveTran
                Dim sValue As String = String.Empty
                Dim selectedTags As List(Of Integer) = dtmp.Select(Function(x) x.Field(Of Integer)("employeeunkid")).Distinct.ToList
                Dim result() As String = selectedTags.Select(Function(x) x.ToString()).ToArray()
                sValue = String.Join(", ", result)

                Dim mdtTnADate As DateTime = Nothing
                If objPrd._TnA_EndDate.Date < dtpEffectiveDate.GetDate.Date Then
                    mdtTnADate = dtpEffectiveDate.GetDate.Date
                Else
                    mdtTnADate = objPrd._TnA_EndDate.Date
                End If

                If objTnALvTran.IsPayrollProcessDone(iPeriod, sValue, mdtTnADate.Date, enModuleReference.TnA) = True Then
                    DisplayMessage.DisplayMessage("Sorry, You cannot assigned policy for the selected date.Reason : Payroll is already processed for the last date of selected date period.", Me)
                    Return False
                End If

            End If

            If CInt(cboPolicy.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Policy is mandatory information. Please select Policy to continue.", Me)
                cboPolicy.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("isVaild_Data :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function

    Private Sub Fill_Emp_Grid()
        Dim dView As DataView = Nothing
        Dim mdtEmployee As DataTable = Nothing
        Dim sValue As String = String.Empty
        Dim objEmp As New clsEmployee_Master
        Try
            Dim dsEmp As New DataSet

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If dtpEffectiveDate.Enabled = True Then
            '    dsEmp = objEmp.GetList("List", True, , dtpEffectiveDate.GetDate.Date, dtpEffectiveDate.GetDate.Date, , Session("AccessLevelFilterString"))
            'Else
            '    dsEmp = objEmp.GetList("List", True, , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , Session("AccessLevelFilterString"))
            '    sValue = objEmp_Policy.Assigned_EmpIds()
            'End If
            'If sValue.Trim.Length > 0 Then
            '    mdtEmployee = New DataView(dsEmp.Tables("List"), "employeeunkid NOT IN(" & sValue & ")", "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    mdtEmployee = New DataView(dsEmp.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
            'End If
            Dim dtEffectivedate As Date = Nothing
            Dim strSearching As String = ""
            If dtpEffectiveDate.Enabled = True Then
                dtEffectivedate = dtpEffectiveDate.GetDate.Date
            Else
                dtEffectivedate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date
                sValue = objEmp_Policy.Assigned_EmpIds()
            End If

            If sValue.Trim.Length > 0 Then
                strSearching &= "AND hremployee_master.employeeunkid  NOT IN(" & sValue & ") "
            End If

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If


            'Pinkal (06-Nov-2017) -- Start
            'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.

            'dsEmp = objEmp.GetList(Session("Database_Name").ToString(), _
            '                       CInt(Session("UserId")), _
            '                       CInt(Session("Fin_year")), _
            '                       CInt(Session("CompanyUnkId")), _
            '                       dtEffectivedate, _
            '                       dtEffectivedate, _
            '                       Session("UserAccessModeSetting").ToString(), True, _
            '                        False, "List", _
            '                       CBool(Session("ShowFirstAppointmentDate")), , , _
            '                       strSearching)

            Dim StrCheck_Fields As String = String.Empty
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Appointed_Date & "," & clsEmployee_Master.EmpColEnum.Col_PolicyName

            dsEmp = objEmp.GetListForDynamicField(StrCheck_Fields, _
                                           Session("Database_Name").ToString(), _
                                   CInt(Session("UserId")), _
                                   CInt(Session("Fin_year")), _
                                   CInt(Session("CompanyUnkId")), _
                                   dtEffectivedate, _
                                   dtEffectivedate, _
                                   Session("UserAccessModeSetting").ToString(), True, _
                                            False, "List", , , strSearching)



            If CInt(cboFilterPolicy.SelectedValue) > 0 Then
                mdtEmployee = New DataView(dsEmp.Tables("List"), "policyunkid = " & CInt(cboFilterPolicy.SelectedValue), "", DataViewRowState.CurrentRows).ToTable()
            Else
                mdtEmployee = New DataView(dsEmp.Tables("List"), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            mdtEmployee.Columns(1).ColumnName = "employeecode"
            mdtEmployee.Columns(2).ColumnName = "name"

            'Pinkal (06-Nov-2017) -- Start



            mdtEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean"))
            mdtEmployee.Columns("ischeck").DefaultValue = False

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Emp_Grid :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally

            If mdtEmployee Is Nothing OrElse mdtEmployee.Rows.Count <= 0 Then
                mdtEmployee = New DataTable("List")
                mdtEmployee.Columns.Add("employeecode", Type.GetType("System.String"))
                mdtEmployee.Columns.Add("name", Type.GetType("System.String"))
                mdtEmployee.Columns.Add("employeeunkid", Type.GetType("System.Int32"))

                Dim drRow As DataRow = mdtEmployee.NewRow
                drRow("employeecode") = "None"
                drRow("name") = "None"
                drRow("employeeunkid") = -1
                mdtEmployee.Rows.Add(drRow)
            End If

            If mdtEmployee.Columns.Contains("IsCheck") = False Then
                Dim dc As New DataColumn
                dc.ColumnName = "IsCheck"
                dc.DataType = Type.GetType("System.Boolean")
                dc.DefaultValue = False
                mdtEmployee.Columns.Add(dc)
            End If

            dView = mdtEmployee.DefaultView
            dgvEmp.DataSource = dView
            dgvEmp.DataBind()

            If Me.ViewState("EmployeeList") Is Nothing Then
                Me.ViewState.Add("EmployeeList", dView.ToTable)
            Else
                Me.ViewState("EmployeeList") = dView.ToTable
            End If

            dView = Nothing
            objEmp = Nothing
        End Try
    End Sub

#End Region

#Region "Button Event"
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page. 

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~/TnA/Shift_Policy_Global_Assign/wPgViewAssignShiftPolicy.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If isValid_Data() = False Then Exit Sub

            Dim mdtEmployee As DataTable = CType(Me.ViewState("EmployeeList"), DataTable)
            Dim dtmp() As DataRow = mdtEmployee.Select("ischeck=true")

            Dim lRows As IEnumerable(Of GridViewRow) = From row In dgvEmp.Rows Select CType(row, GridViewRow)
            Dim idx As Integer = -1


            Dim blnFlag As Boolean = False
            Dim mblnInsertFlag As Boolean = False
            For i As Integer = 0 To dtmp.Length - 1

                Dim iLoop As Integer = i
                Dim rows = lRows.Cast(Of GridViewRow)().Where(Function(row) row.Cells(1).ToString().Equals(dtmp(iLoop).Item("employeecode")))
                idx = -1

                'Pinkal (06-Nov-2017) -- Start
                'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
                'If objEmp_Policy.isExist(CInt(cboPolicy.SelectedValue), CDate(IIf(dtpEffectiveDate.Enabled = True, dtpEffectiveDate.GetDate.Date, eZeeDate.convertDate(dtmp(i).Item("appointeddate").ToString))), CInt(dtmp(i).Item("employeeunkid"))) = True Then
                If objEmp_Policy.isExist(CInt(cboPolicy.SelectedValue), CDate(IIf(dtpEffectiveDate.Enabled = True, dtpEffectiveDate.GetDate.Date, eZeeDate.convertDate(dtmp(i).Item("Appointed Date").ToString))), CInt(dtmp(i).Item("employeeunkid"))) = True Then
                    'Pinkal (06-Nov-2017) -- End
                    If blnFlag = False Then
                        If chkFirstPolicy.Checked = True Then
                            DisplayMessage.DisplayMessage("Sorry, policy cannot be assigned to some of the selected employee(s). Reason : Policy is already assigned to them and will be highlighted in red.", Me)
                        Else
                            DisplayMessage.DisplayMessage("Sorry, policy cannot be assigned to some of the selected employee(s). Reason : Policy is already assigned to them for the selected date and will be highlighted in red.", Me)
                        End If


                        If rows.Count > 0 Then
                            idx = rows(0).RowIndex
                            If idx >= 0 Then
                                dgvEmp.Rows(idx).ForeColor = Drawing.Color.Red
                                blnFlag = True
                            End If
                        End If
                        Continue For

                    Else
                        If rows.Count > 0 Then
                            idx = rows(0).RowIndex
                            If idx >= 0 Then
                                dgvEmp.Rows(idx).ForeColor = Drawing.Color.Red
                            End If
                        End If
                        Continue For

                    End If

                Else

                    'Pinkal (06-Nov-2017) -- Start
                    'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
                    'If objEmp_Policy.isExist(CDate(IIf(dtpEffectiveDate.Enabled = True, dtpEffectiveDate.GetDate.Date, eZeeDate.convertDate(dtmp(i).Item("appointeddate").ToString))), CInt(dtmp(i).Item("employeeunkid"))) > 0 Then
                    If objEmp_Policy.isExist(CDate(IIf(dtpEffectiveDate.Enabled = True, dtpEffectiveDate.GetDate.Date, eZeeDate.convertDate(dtmp(i).Item("Appointed Date").ToString))), CInt(dtmp(i).Item("employeeunkid"))) > 0 Then
                        'Pinkal (06-Nov-2017) -- End

                        If blnFlag = False Then
                            If chkFirstPolicy.Checked = True Then
                                DisplayMessage.DisplayMessage("Sorry, policy cannot be assigned to some of the selected employee(s). Reason : Policy is already assigned to them and will be highlighted in red.", Me)
                            Else
                                DisplayMessage.DisplayMessage("Sorry, policy cannot be assigned to some of the selected employee(s). Reason : Policy is already assigned to them for the selected date and will be highlighted in red.", Me)
                            End If
                            If rows.Count > 0 Then
                                idx = rows(0).RowIndex
                                If idx >= 0 Then
                                    dgvEmp.Rows(idx).ForeColor = Drawing.Color.Red
                                    blnFlag = True
                                End If
                            End If
                            Continue For
                        Else
                            If rows.Count > 0 Then
                                idx = rows(0).RowIndex
                                If idx >= 0 Then
                                    dgvEmp.Rows(idx).ForeColor = Drawing.Color.Red
                                End If
                            End If
                            Continue For
                        End If

                    End If

                    'Pinkal (06-Nov-2017) -- Start
                    'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.
                    'objEmp_Policy._Effectivedate = CDate(IIf(dtpEffectiveDate.Enabled = True, dtpEffectiveDate.GetDate.Date, eZeeDate.convertDate(dtmp(i).Item("appointeddate").ToString)))
                    objEmp_Policy._Effectivedate = CDate(IIf(dtpEffectiveDate.Enabled = True, dtpEffectiveDate.GetDate.Date, eZeeDate.convertDate(dtmp(i).Item("Appointed Date").ToString)))
                    'Pinkal (06-Nov-2017) -- End
                    objEmp_Policy._Employeeunkid = CInt(dtmp(i).Item("employeeunkid"))
                    objEmp_Policy._Isvoid = False
                    objEmp_Policy._Policyunkid = CInt(cboPolicy.SelectedValue)
                    objEmp_Policy._Userunkid = CInt(Session("UserId"))
                    objEmp_Policy._Voiddatetime = Nothing
                    objEmp_Policy._Voidreason = ""
                    objEmp_Policy._Voiduserunkid = -1
                    mblnInsertFlag = objEmp_Policy.Insert()
                    If mblnInsertFlag = False Then
                        DisplayMessage.DisplayMessage("Problem in assigning policy.", Me)
                        Exit For
                    End If
                End If

            Next
            If mblnInsertFlag = True Then
                DisplayMessage.DisplayMessage("Policy assigned successfully.", Me)
                dtpEffectiveDate.Enabled = True
                cboPolicy.SelectedIndex = 0
                If chkFirstPolicy.Checked Then chkFirstPolicy.Checked = False
                Fill_Emp_Grid()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSave_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~/TnA/Shift_Policy_Global_Assign/wPgViewAssignShiftPolicy.aspx")
            Response.Redirect(Session("rootpath").ToString() & "TnA/Shift_Policy_Global_Assign/wPgViewAssignShiftPolicy.aspx")
            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Protected Sub chkFirstPolicy_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkFirstPolicy.CheckedChanged
        Try
            If chkFirstPolicy.Checked = True Then
                dtpEffectiveDate.Enabled = False
            Else
                dtpEffectiveDate.Enabled = True
            End If
            Call Fill_Emp_Grid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkFirstPolicy_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Sub chkEmpSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try

            Dim dtTab As DataTable = CType(Me.ViewState("EmployeeList"), DataTable)

            For i As Integer = 0 To dgvEmp.Rows.Count - 1
                Dim gvRow As GridViewRow = dgvEmp.Rows(i)
                CType(gvRow.FindControl("chkEmpSelect"), CheckBox).Checked = cb.Checked
                Dim drRow() As DataRow = dtTab.Select("employeecode = '" & dgvEmp.Rows(i).Cells(1).Text.Trim & "'")
                If drRow.Length > 0 Then
                    drRow(0)("ischeck") = cb.Checked
                End If
            Next
            dtTab.AcceptChanges()

            Me.ViewState("EmployeeList") = dtTab

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkEmpSelectAll_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Sub chkEmpSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dvTable As DataView = Nothing
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If cb Is Nothing Then Exit Try


            Dim dtTab As DataTable = CType(Me.ViewState("EmployeeList"), DataTable)

            Dim gvRow As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
            CType(gvRow.FindControl("chkEmpSelect"), CheckBox).Checked = cb.Checked

            Dim drRow() As DataRow = dtTab.Select("employeecode = '" & gvRow.Cells(1).Text.Trim & "'")

            If drRow.Length > 0 Then
                drRow(0)("IsCheck") = cb.Checked
                drRow(0).AcceptChanges()
            End If

            Me.ViewState("EmployeeList") = dtTab

            dvTable = CType(Me.ViewState("EmployeeList"), DataTable).DefaultView

            If txtSearch.Text.Trim.Length > 0 Then
                If dvTable IsNot Nothing Then
                    If dvTable.Table.Rows.Count > 0 Then
                        dvTable.RowFilter = "employeecode like '%" & txtSearch.Text.Trim & "%'  OR name like '%" & txtSearch.Text.Trim & "%' "
                    End If
                End If
            End If

            Dim drcheckedRow() As DataRow = dvTable.ToTable.Select("Ischeck = True")

            If drcheckedRow.Length = dvTable.ToTable.Rows.Count Then
                CType(dgvEmp.HeaderRow.FindControl("chkEmpSelectAll"), CheckBox).Checked = True
            Else
                CType(dgvEmp.HeaderRow.FindControl("chkEmpSelectAll"), CheckBox).Checked = False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkEmpSelect_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "DatePicker Event"

    Protected Sub dtpEffectiveDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpEffectiveDate.TextChanged
        Try
            Fill_Emp_Grid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dtpEffectiveDate_TextChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Textbox Event"

    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Dim dvTable As DataView = Nothing
        Try
            dvTable = CType(Me.ViewState("EmployeeList"), DataTable).DefaultView
            If dvTable IsNot Nothing Then
                If dvTable.Table.Rows.Count > 0 Then
                    dvTable.RowFilter = "employeecode like '%" & txtSearch.Text.Trim & "%'  OR name like '%" & txtSearch.Text.Trim & "%' "
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtSearch_TextChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            Dim mdtemployee As DataTable = dvTable.ToTable
            If mdtemployee.Rows.Count <= 0 Then
                Dim drRow As DataRow = mdtemployee.NewRow
                drRow("employeecode") = "None"
                drRow("name") = "None"
                drRow("employeeunkid") = -1
                mdtemployee.Rows.Add(drRow)
            End If
            dvTable = mdtemployee.DefaultView
            dgvEmp.DataSource = dvTable
            dgvEmp.DataBind()
        End Try
    End Sub

#End Region

#Region "Gridview Event"

    Protected Sub dgvEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvEmp.RowDataBound
        Try
            If e.Row.RowIndex < 0 Then Exit Sub

            If e.Row.Cells(1).Text = "None" AndAlso e.Row.Cells(2).Text = "None" Then
                e.Row.Visible = False
                CType(dgvEmp.HeaderRow.FindControl("chkEmpSelectAll"), CheckBox).Enabled = False
            Else
                CType(dgvEmp.HeaderRow.FindControl("chkEmpSelectAll"), CheckBox).Enabled = True
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvEmp_RowDataBound :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Pinkal (06-Nov-2017) -- Start
    'Enhancement - Working on B5 Plus Day Start Overlap issue & Shift/policy Filteration on Shift/Policy Assignment.

    Protected Sub cboFilterPolicy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFilterPolicy.SelectedIndexChanged
        Try
            Fill_Emp_Grid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboFilterPolicy_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (06-Nov-2017) -- End

    Private Sub SetLanguage()
        Try

        Language.setLanguage(mstrModuleName)

        Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Language._Object.getCaption(mstrModuleName, Me.Title)
        Me.lblPageHeader.Text = Language._Object.getCaption(mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End
        Me.LblEffectiveDate.Text = Language._Object.getCaption(Me.LblEffectiveDate.ID, Me.LblEffectiveDate.Text)
        Me.lblPolicy.Text = Language._Object.getCaption(Me.lblPolicy.ID, Me.lblPolicy.Text)
        Me.chkFirstPolicy.Text = Language._Object.getCaption(Me.chkFirstPolicy.ID, Me.chkFirstPolicy.Text)
        Me.dgvEmp.Columns(1).HeaderText = Language._Object.getCaption(Me.dgvEmp.Columns(1).FooterText, Me.dgvEmp.Columns(1).HeaderText)
        Me.dgvEmp.Columns(2).HeaderText = Language._Object.getCaption(Me.dgvEmp.Columns(2).FooterText, Me.dgvEmp.Columns(2).HeaderText)

        Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
        Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


End Class
