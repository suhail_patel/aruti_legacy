﻿<%@ Page Title="Employee OT Assignment List" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="OT_EmployeeAssignmentList.aspx.vb" Inherits="TnA_OT_Requisition_OT_EmployeeAssignmentList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee OT Assignment List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                    <div style="text-align: right;">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocations" Style="font-weight: bold;
                                            font-size: 12px; vertical-align: top; margin-right: 10px" CssClass="lnkhover"></asp:LinkButton>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 8%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 92%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" Width= "400px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div3" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 100%">
                                                <div id="scrollable-container" class="gridscroll" style="vertical-align: top; overflow: auto;
                                                    max-height: 400px" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                    <asp:DataGrid ID="dgvEmployeeOTList" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                        CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" DataKeyField = "tnaemployeeotunkid"
                                                        AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="btnEdit" Visible ="false">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit" CssClass="gridedit"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="btnDelete">
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                            CommandName="Delete" CssClass="griddelete"></asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="EmpcodeName" HeaderText="Employee" ReadOnly="true" FooterText="dgcolhEmployee" />
                                                            <asp:BoundColumn DataField="employeeunkid" HeaderText="Employee Id" ReadOnly="true" Visible ="false" FooterText="objdgcolhemployeeunkid" />
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <uc2:DeleteReason ID="popDeleteReason" runat="server" />
                    <uc3:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
