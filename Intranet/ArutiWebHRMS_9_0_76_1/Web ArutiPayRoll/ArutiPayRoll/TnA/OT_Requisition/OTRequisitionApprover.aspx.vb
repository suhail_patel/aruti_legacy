﻿Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing
Partial Class TnA_OT_Requisition_OTRequisitionApprover
    Inherits Basepage

#Region "Private Variable"

    Private DisplayMessage As New CommonCodes

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private objclsTnaapprover_master As New clsTnaapprover_master
    'Private objclsTnaapprover_tran As New clsTnaapprover_Tran
    'Pinkal (03-Sep-2020) -- End

    Private mstrModuleName As String = "frmOTRequisitionApproverList"
    Private mstrModuleName1 As String = "frmOTRequisitionApproverAddedit"
    Private mstrEmployeeIDs As String = ""
    Dim mblnpopupOTApproverAddEdit As Boolean = False
    Private dtAssignEmpView As DataView = Nothing
    Private mintTnaMappingunkid As Integer
    Private mdtApproverList As DataTable
    Dim mdtPopupEmpAddEditList As DataTable
    Dim mdtPopupAddeditSelectEmpList As DataTable
    Private currentId As String = ""
    Private Index As Integer
    Private mintActiveDeactiveApprId As Integer = 0
    Private mblActiveDeactiveApprStatus As Boolean

    'Pinkal (27-Dec-2019) -- Start
    'Enhancement - Changes related To OT NMB Testing.
    Private mstrAdvanceSearch As String = ""
    'Pinkal (27-Dec-2019) -- End


#End Region

#Region "Form Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Disciplinary_Cases_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End

                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()

                ListFillCombo()
                FillList(True)
                SetVisibility()

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                Dim objclsTnaapprover_master As New clsTnaapprover_master
                Dim objclsTnaapprover_tran As New clsTnaapprover_Tran
                objclsTnaapprover_master._Tnamappingunkid = mintTnaMappingunkid
                objclsTnaapprover_tran._TnAMappingUnkId = mintTnaMappingunkid
                objclsTnaapprover_tran.Get_Data()
                mdtPopupAddeditSelectEmpList = objclsTnaapprover_tran._DataTable
                objclsTnaapprover_tran = Nothing
                objclsTnaapprover_master = Nothing
                'Pinkal (03-Sep-2020) -- End
            Else

                mintActiveDeactiveApprId = CInt(ViewState("mintActiveDeactiveApprId"))
                mblActiveDeactiveApprStatus = CBool(ViewState("mblActiveDeactiveApprStatus"))

                mblnpopupOTApproverAddEdit = ViewState("blnpopupOTApproverAddEdit")
                If ViewState("EmpList") IsNot Nothing Then
                    mdtPopupEmpAddEditList = CType(ViewState("EmpList"), DataTable)
                End If

                If ViewState("SelectedEmpList") IsNot Nothing Then
                    mdtPopupAddeditSelectEmpList = CType(ViewState("SelectedEmpList"), DataTable)
                End If

                If ViewState("mintTnaMappingunkid") IsNot Nothing Then
                    mintTnaMappingunkid = CType(ViewState("mintTnaMappingunkid"), Integer)
                End If

                If ViewState("mdtApproverList") IsNot Nothing Then
                    mdtApproverList = CType(ViewState("mdtApproverList"), DataTable)
                    If mdtApproverList.Rows.Count > 0 AndAlso CInt(mdtApproverList.Rows(0)("tnamappingunkid").ToString) > 0 Then
                        gvApproverList.DataSource = mdtApproverList
                        gvApproverList.DataBind()
                    End If
                End If


                'Pinkal (27-Dec-2019) -- Start
                'Enhancement - Changes related To OT NMB Testing.
                If Me.ViewState("EmployeeIDs") IsNot Nothing Then
                    mstrEmployeeIDs = Me.ViewState("EmployeeIDs").ToString()
                End If
                'Pinkal (27-Dec-2019) -- End



                If mblnpopupOTApproverAddEdit Then
                    popupOTApproverAddEdit.Show()
                    'drpApproverUseraccess_user_SelectedIndexChanged(sender, e)
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("blnpopupOTApproverAddEdit") = mblnpopupOTApproverAddEdit
            ViewState("EmpList") = mdtPopupEmpAddEditList
            ViewState("SelectedEmpList") = mdtPopupAddeditSelectEmpList
            ViewState("mdtApproverList") = mdtApproverList
            ViewState("mintTnaMappingunkid") = mintTnaMappingunkid

            ViewState("mintActiveDeactiveApprId") = mintActiveDeactiveApprId
            ViewState("mblActiveDeactiveApprStatus") = mblActiveDeactiveApprStatus

            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            Me.ViewState("EmpAdvanceSearch") = mstrAdvanceSearch
            Me.ViewState("EmployeeIDs") = mstrEmployeeIDs
            'Pinkal (27-Dec-2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "   For List"

#Region "   Private Function"

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsApproverList As New DataSet
        Dim dtApprover As DataTable
        Dim strSearching As String = ""

        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTnaapprover_master As clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End

        Try

            If isblank OrElse CBool(Session("AllowToViewOTRequisitionApprover")) = False Then
                strSearching = "AND 1 = 2 "
            End If

            'Pinkal (29-Apr-2020) -- Start
            'Error  -  Solved Error when User Filter anything from list screen and search it was giving error.
            If CInt(drpLevel.SelectedValue) > 0 Then
                strSearching &= "AND tnaapprover_master.tnalevelunkid = " & CInt(drpLevel.SelectedValue) & " "
            End If

            If CInt(drpApprover.SelectedValue) > 0 Then
                'strSearching &= "AND tnaapprover_master.mapuserunkid = " & CInt(drpApprover.SelectedValue) & " "
                strSearching &= "AND tnaapprover_master.approveremployeeunkid = " & CInt(drpApprover.SelectedValue) & " "
            End If
            'Pinkal (29-Apr-2020) -- End

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Substring(3)
            End If

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objclsTnaapprover_master = New clsTnaapprover_master
            'Pinkal (03-Sep-2020) -- End

            dsApproverList = objclsTnaapprover_master.GetList("List", Session("Database_Name").ToString(), enTnAApproverType.OT_Requisition_Approver _
                                                                                      , Session("EmployeeAsOnDate").ToString(), False, IIf(drpStatus.SelectedIndex = 0, True, False), Nothing, strSearching, 0, True)


            If dsApproverList.Tables(0).Rows.Count <= 0 Then
                isblank = True
                Dim drRow As DataRow = dsApproverList.Tables(0).NewRow()
                drRow("tnalevelname") = ""
                drRow("tnamappingunkid") = 0
                dsApproverList.Tables(0).Rows.Add(drRow)
            End If

            If dsApproverList IsNot Nothing Then
                dtApprover = New DataView(dsApproverList.Tables("List"), "", "tnalevelname", DataViewRowState.CurrentRows).ToTable()

                Dim strLeaveName As String = ""
                Dim dtTable As DataTable = dtApprover.Clone
                dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))
                Dim dtRow As DataRow = Nothing
                For Each drow As DataRow In dtApprover.Rows
                    If CStr(drow("tnalevelname")).Trim <> strLeaveName.Trim Then
                        dtRow = dtTable.NewRow
                        dtRow("IsGrp") = True
                        dtRow("tnamappingunkid") = drow("tnamappingunkid")
                        dtRow("tnalevelname") = drow("tnalevelname")
                        strLeaveName = drow("tnalevelname").ToString()
                        dtTable.Rows.Add(dtRow)
                    End If
                    dtRow = dtTable.NewRow
                    For Each dtcol As DataColumn In dtApprover.Columns
                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next

                dtTable.AcceptChanges()
                gvApproverList.DataSource = dtTable
                gvApproverList.DataBind()
                mdtApproverList = dtTable

                If isblank = True Then
                    gvApproverList.Rows(0).Visible = False
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTnaapprover_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Visible = CBool(Session("AllowToAddOTRequisitionApprover"))
            Dim strCapOTHrsForHODApprovers As String = Session("CapOTHrsForHODApprovers").ToString
            Dim arrCapOTHrsForHODApprovers As String() = strCapOTHrsForHODApprovers.Split(New Char() {":"c})
            Dim intTotalMins As Integer = 0
            If arrCapOTHrsForHODApprovers.Length > 0 Then
                intTotalMins = (Convert.ToInt32(arrCapOTHrsForHODApprovers(0)) * 60) + Convert.ToInt32(arrCapOTHrsForHODApprovers(1))
            End If
            If Convert.ToBoolean(Session("SetOTRequisitionMandatory")) = True AndAlso intTotalMins > 0 Then
                chkHodCapForOT.Visible = True
            Else
                chkHodCapForOT.Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            rw.Visible = False
            Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
            row.BackColor = ColorTranslator.FromHtml("#ECECEC")
            Dim cell As TableCell = New TableCell()
            cell.Text = title
            cell.Font.Bold = True
            cell.ColumnSpan = gd.Columns.Count
            row.Cells.Add(cell)
            gd.Controls(0).Controls.Add(row)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ListFillCombo()
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objMaster As clsMasterData
        Dim objLevel As clsTna_approverlevel_master
        'Pinkal (03-Sep-2020) -- End
        Try

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objMaster = New clsMasterData
            'Pinkal (03-Sep-2020) -- End
            Dim dsCombo As DataSet = objMaster.getComboListTranHeadActiveInActive("List", False)
            With drpStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "1"
            End With


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objLevel = New clsTna_approverlevel_master
            'Pinkal (03-Sep-2020) -- End
            Dim dsList As DataSet = objLevel.getListForCombo("List", True)
            drpLevel.DataTextField = "name"
            drpLevel.DataValueField = "tnalevelunkid"
            drpLevel.DataSource = dsList.Tables(0)
            drpLevel.DataBind()
            drpLevel_SelectedIndexChanged(New Object(), New EventArgs())

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objMaster = Nothing
            objLevel = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "    Button Event"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            chkAddEditExtApprover_CheckedChanged(New Object(), New EventArgs())
            txtSearchEmployee.Text = ""
            txtAssignedEmpSearch.Text = ""
            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            mstrEmployeeIDs = ""
            'Pinkal (27-Dec-2019) -- End
            mblnpopupOTApproverAddEdit = True
            popupOTApproverAddEdit.Show()
            chkHodCapForOT.Checked = False
            chkAddEditExtApprover.Enabled = True
            drpAddEditApprover.Enabled = True
            FillEmployeeList(True)
            SelectedEmplyeeList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            drpLevel.SelectedIndex = 0
            drpApprover.SelectedIndex = 0
            drpStatus.SelectedIndex = 0
            Call FillList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTnaapprover_master As clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End

        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objclsTnaapprover_master = New clsTnaapprover_master
            'Pinkal (03-Sep-2020) -- End

            objclsTnaapprover_master._Tnamappingunkid = lnkedit.CommandArgument.ToString()
            mintTnaMappingunkid = lnkedit.CommandArgument.ToString()


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'SetEditValue()
            SetEditValue(objclsTnaapprover_master)
            'Pinkal (03-Sep-2020) -- End

            popupOTApproverAddEdit.Show()
            mblnpopupOTApproverAddEdit = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTnaapprover_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTnaapprover_master As clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objclsTnaapprover_master = New clsTnaapprover_master
            'Pinkal (03-Sep-2020) -- End

            objclsTnaapprover_master._Tnamappingunkid = CInt(lnkdelete.CommandArgument.ToString())
            mintTnaMappingunkid = CInt(lnkdelete.CommandArgument.ToString())

            If objclsTnaapprover_master.isUsed(CInt(lnkdelete.CommandArgument.ToString())) Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, You cannot delete this Approver . Reason: This Approver is in use."), Me)
                Exit Sub
            End If

            confirmapproverdelete.Show()
            confirmapproverdelete.Title = Language.getMessage(mstrModuleName, 2, "Confirmation")
            confirmapproverdelete.Message = Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this approver ?")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTnaapprover_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub lnkActive_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkActive As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkActive).NamingContainer, GridViewRow)
            Dim blnFlag As Boolean = False

            mintActiveDeactiveApprId = CInt(lnkActive.CommandArgument)
            mblActiveDeactiveApprStatus = True

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'objclsTnaapprover_master._Tnamappingunkid = CInt(lnkActive.CommandArgument.ToString())
            'Pinkal (03-Sep-2020) -- End

            popupconfirmActiveAppr.Title = Language.getMessage(mstrModuleName, 2, "Confirmation")
            popupconfirmActiveAppr.Message = Language.getMessage(mstrModuleName, 8, "Are you sure you want to active this approver?")
            popupconfirmActiveAppr.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDeActive_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkDeActive As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkDeActive).NamingContainer, GridViewRow)

            mintActiveDeactiveApprId = CInt(lnkDeActive.CommandArgument)
            mblActiveDeactiveApprStatus = False

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'objclsTnaapprover_master._Tnamappingunkid = CInt(lnkDeActive.CommandArgument)
            'Pinkal (03-Sep-2020) -- End

            popupconfirmDeactiveAppr.Title = Language.getMessage(mstrModuleName, 2, "Confirmation")
            popupconfirmDeactiveAppr.Message = Language.getMessage(mstrModuleName, 7, "Are you sure you want to de-active this approver?")
            popupconfirmDeactiveAppr.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupconfirmActiveAppr_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmActiveAppr.buttonYes_Click
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTnaapprover_master As New clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End
        Try
            Dim blnFlag As Boolean = False

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            ' SetAtValue(mstrModuleName)
            objclsTnaapprover_master = New clsTnaapprover_master
            SetAtValue(mstrModuleName, objclsTnaapprover_master)
            'Pinkal (03-Sep-2020) -- End

            blnFlag = objclsTnaapprover_master.InActiveApprover(mintActiveDeactiveApprId, enTnAApproverType.OT_Requisition_Approver, True)

            If blnFlag = False And objclsTnaapprover_master._Message <> "" Then
                DisplayMessage.DisplayMessage(objclsTnaapprover_master._Message, Me)
                Exit Sub
            Else
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                objclsTnaapprover_master = Nothing
                'Pinkal (03-Sep-2020) -- End
                FillList(False)
                mintActiveDeactiveApprId = 0
                mblActiveDeactiveApprStatus = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTnaapprover_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try

    End Sub

    Protected Sub popupconfirmDeactiveAppr_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmDeactiveAppr.buttonYes_Click
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTnaapprover_master As clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End
        Try
            Dim blnFlag As Boolean = False

            'Pinkal (10-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion for NMB.
            Dim objOTRequisition As New clsOT_Requisition_Tran
            If objOTRequisition.GetApproverPendingOTRequisition(mintActiveDeactiveApprId, "").Trim.Length > 0 Then
                Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 10, "You cannot inactive this approver.Reason :This Approver has Pending ot requisition application(s)."), Me)
                Exit Sub
            End If
            objOTRequisition = Nothing
            'Pinkal (10-Jan-2020) -- End

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            ''SetAtValue(mstrModuleName)
            objclsTnaapprover_master = New clsTnaapprover_master
            SetAtValue(mstrModuleName, objclsTnaapprover_master)
            'Pinkal (03-Sep-2020) -- End

            blnFlag = objclsTnaapprover_master.InActiveApprover(mintActiveDeactiveApprId, enTnAApproverType.OT_Requisition_Approver, False)

            If blnFlag = False And objclsTnaapprover_master._Message <> "" Then
                DisplayMessage.DisplayMessage(objclsTnaapprover_master._Message, Me)
                Exit Sub
            Else
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                objclsTnaapprover_master = Nothing
                'Pinkal (03-Sep-2020) -- End
                FillList(False)
                mintActiveDeactiveApprId = 0
                mblActiveDeactiveApprStatus = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTnaapprover_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub DeleteApprovalReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DeleteApprovalReason.buttonDelReasonYes_Click
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTnaapprover_master As clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End
        Try
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'SetAtValue(mstrModuleName)
            objclsTnaapprover_master = New clsTnaapprover_master
            SetAtValue(mstrModuleName, objclsTnaapprover_master)
            'Pinkal (03-Sep-2020) -- End

            objclsTnaapprover_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objclsTnaapprover_master._Voidreason = DeleteApprovalReason.Reason
            objclsTnaapprover_master._FormName = mstrModuleName

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objclsTnaapprover_master._Voiduserunkid = CInt(Session("UserId"))
            End If

            If objclsTnaapprover_master.Delete(enTnAApproverType.OT_Requisition_Approver, mintTnaMappingunkid, ConfigParameter._Object._CurrentDateAndTime.Date) = False Then
                DisplayMessage.DisplayMessage(objclsTnaapprover_master._Message, Me)
                Exit Sub
            End If

            mintTnaMappingunkid = 0

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objclsTnaapprover_master = Nothing
            'Pinkal (03-Sep-2020) -- End


            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTnaapprover_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "   Dropdown Events"

    Protected Sub drpLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpLevel.SelectedIndexChanged
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTnaapprover_master As clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End`
        Try

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objclsTnaapprover_master = New clsTnaapprover_master
            'Pinkal (03-Sep-2020) -- End

            Dim dsApprList As DataSet = objclsTnaapprover_master.GetList("List", Session("Database_Name").ToString(), enTnAApproverType.OT_Requisition_Approver _
                                                                                                      , Session("EmployeeAsOnDate").ToString(), False, IIf(drpStatus.SelectedIndex = 0, True, False), Nothing, "", 0)

            If dsApprList Is Nothing Then Exit Sub

            Dim dtTable As DataTable = Nothing
            If CInt(drpLevel.SelectedValue) > 0 Then
                dtTable = New DataView(dsApprList.Tables(0), "tnalevelunkid = " & CInt(drpLevel.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = dsApprList.Tables(0).DefaultView.ToTable(True, "approveremployeeunkid", "name", "isexternalapprover")
            End If

            Dim dr As DataRow = dtTable.NewRow
            dr("approveremployeeunkid") = 0
            dr("name") = "Select"
            dr("isexternalapprover") = False
            dtTable.Rows.InsertAt(dr, 0)

            drpApprover.DataTextField = "name"
            drpApprover.DataValueField = "approveremployeeunkid"
            drpApprover.DataSource = dtTable
            drpApprover.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTnaapprover_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "  Gridview Events"

    Protected Sub gvApproverList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApproverList.RowDataBound
        Dim oldid As String
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table
                If CInt(gvApproverList.DataKeys(e.Row.RowIndex)("tnamappingunkid").ToString) > 0 Then

                    oldid = dt.Rows(e.Row.RowIndex)("tnalevelname").ToString()
                    If dt.Rows(e.Row.RowIndex)("IsGrp").ToString() = True AndAlso oldid <> currentId Then
                        Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("tnalevelname").ToString(), gvApproverList)
                        currentId = oldid
                    Else
                        Dim lnkactive As LinkButton = TryCast(e.Row.FindControl("lnkactive"), LinkButton)
                        Dim lnkDeactive As LinkButton = TryCast(e.Row.FindControl("lnkDeactive"), LinkButton)
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)


                        lnkactive.ToolTip = Language.getMessage(mstrModuleName, 4, "Active")
                        lnkDeactive.ToolTip = Language.getMessage(mstrModuleName, 5, "DeActive")
                        lnkdelete.ToolTip = Language.getMessage(mstrModuleName, 6, "Delete")

                        If CBool(Session("AllowToDeleteOTRequisitionApprover")) Then
                            lnkdelete.Visible = True
                        Else
                            lnkdelete.Visible = False
                        End If

                        If CBool(Session("AllowToActivateOTRequisitionApprover")) Then
                            lnkactive.Visible = True
                        Else
                            lnkactive.Visible = False
                        End If

                        If CBool(Session("AllowToInActivateOTRequisitionApprover")) Then
                            lnkDeactive.Visible = True
                        Else
                            lnkDeactive.Visible = False
                        End If

                        If dt.Rows(e.Row.RowIndex)("isactive").ToString() <> "" Then
                            If dt.Rows(e.Row.RowIndex)("isactive").ToString() = True Then
                                lnkactive.Visible = False
                                lnkDeactive.Visible = True
                            Else
                                lnkactive.Visible = True
                                lnkDeactive.Visible = False
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            currentId = ""
            oldid = ""
        End Try
    End Sub

#End Region

#End Region

#Region "   For AddEdit Approver"

#Region "Private Function"

    Private Sub FillAddEditCombo()
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        'Dim objEmployee As New clsEmployee_Master
        'Dim objGrievanceApproverLevel As New clsGrievanceApproverLevel
        'Dim objUser As New clsUserAddEdit
        'Dim dsList As DataSet
        Dim objEmployee As clsEmployee_Master
        Dim objLevel As clsTna_approverlevel_master
        Dim objUser As clsUserAddEdit
        Dim dsList As DataSet = Nothing
        'Pinkal (03-Sep-2020) -- End
        Try
            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objLevel = New clsTna_approverlevel_master
            objUser = New clsUserAddEdit
            'Pinkal (03-Sep-2020) -- End

            dsList = objLevel.getListForCombo("List", True)
            drpAddEditApproverLevel.DataSource = dsList
            drpAddEditApproverLevel.DataTextField = "name"
            drpAddEditApproverLevel.DataValueField = "tnalevelunkid"
            drpAddEditApproverLevel.DataBind()


            If chkAddEditExtApprover.Checked = False Then
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                objEmployee = New clsEmployee_Master
                'Pinkal (03-Sep-2020) -- End
                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), _
                                                     CInt(Session("UserId")), _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                     Session("UserAccessModeSetting").ToString(), True, _
                                                     CBool(Session("IsIncludeInactiveEmp")), "Employee", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)
                drpAddEditApprover.DataSource = dsList
                drpAddEditApprover.DataTextField = "EmpCodeName"
                drpAddEditApprover.DataValueField = "employeeunkid"
                drpAddEditApprover.DataBind()
            Else
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                'Dim objUser As New clsUserAddEdit
                'Pinkal (03-Sep-2020) -- End

                dsList = objUser.GetExternalApproverList("List", _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, enUserPriviledge.AllowtoApproveOTRequisition)

                Dim drRow As DataRow = dsList.Tables("List").NewRow
                drRow("name") = "Select"
                drRow("userunkid") = 0
                dsList.Tables("List").Rows.InsertAt(drRow, 0)

                drpAddEditApprover.DataSource = dsList.Tables("List")
                drpAddEditApprover.DataTextField = "name"
                drpAddEditApprover.DataValueField = "userunkid"
                drpAddEditApprover.DataBind()
            End If


            dsList = Nothing
            dsList = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), enUserPriviledge.AllowtoApproveOTRequisition, CInt(Session("Fin_year")), True)
            drpAddEditUser.DataSource = dsList.Tables("User")
            drpAddEditUser.DataTextField = "name"
            drpAddEditUser.DataValueField = "userunkid"
            drpAddEditUser.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList.Dispose()
            objLevel = Nothing
            objEmployee = Nothing
            objUser = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Private Sub FillEmployeeList(ByVal isblank As Boolean)
        Dim dsEmployee As DataSet = Nothing
        Dim strSearch As String = String.Empty
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objEmployee As clsEmployee_Master
        'Pinkal (03-Sep-2020) -- End

        Try
            Dim blnInActiveEmp As Boolean = False


            If isblank Then
                strSearch = "AND 1=2"
            End If

            If chkAddEditExtApprover.Checked = False Then
                If CInt(drpAddEditApprover.SelectedValue) > 0 Then
                    strSearch &= "AND hremployee_master.employeeunkid <> " & CInt(drpAddEditApprover.SelectedValue) & " "
                End If
            End If

            If CInt(drpAddEditApprover.SelectedValue) <= 0 Then
                strSearch &= "AND hremployee_master.employeeunkid = " & CInt(drpAddEditApprover.SelectedValue) & " "
            End If


            'Pinkal (03-Mar-2020) -- Start
            'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.
            'If mstrEmployeeIDs.Trim.Length > 0 Then
            '    strSearch &= "AND hremployee_master.employeeunkid not in ( " & mstrEmployeeIDs.Trim() & " )"
            'End If
            'Pinkal (03-Mar-2020) -- End




            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            If mstrAdvanceSearch.Trim.Length > 0 Then
                strSearch &= "AND " & mstrAdvanceSearch
            End If
            'Pinkal (27-Dec-2019) -- End



            If strSearch.Trim.Length > 0 Then
                strSearch = strSearch.Trim.Substring(3)
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objEmployee = New clsEmployee_Master
            'Pinkal (03-Sep-2020) -- End


            Dim strfield As String = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Job & _
                                              "," & clsEmployee_Master.EmpColEnum.Col_Station & "," & clsEmployee_Master.EmpColEnum.Col_Dept_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section & _
                                              "," & clsEmployee_Master.EmpColEnum.Col_Unit_Group & "," & clsEmployee_Master.EmpColEnum.Col_Unit & "," & clsEmployee_Master.EmpColEnum.Col_Team & "," & clsEmployee_Master.EmpColEnum.Col_Job_Group & _
                                              "," & clsEmployee_Master.EmpColEnum.Col_Cost_Center



            dsEmployee = objEmployee.GetListForDynamicField(strfield, Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                            blnInActiveEmp, _
                                         "Employee", -1, False, strSearch, CBool(Session("ShowFirstAppointmentDate")), False, False, True)

            dsEmployee.Tables(0).Columns(1).ColumnName = "employeecode"
            dsEmployee.Tables(0).Columns(2).ColumnName = "name"

            dsEmployee.Tables(0).Columns(3).ColumnName = "departmentname"
            dsEmployee.Tables(0).Columns(4).ColumnName = "jobname"


            If dsEmployee.Tables(0).Columns.Contains("IsCheck") = False Then
                Dim dccolumn As New DataColumn("IsCheck")
                dccolumn.DataType = Type.GetType("System.Boolean")
                dccolumn.DefaultValue = False
                dsEmployee.Tables(0).Columns.Add(dccolumn)
            End If


            'Pinkal (03-Mar-2020) -- Start
            'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.
            If mstrEmployeeIDs.Trim.Length > 0 Then
                mdtPopupEmpAddEditList = New DataView(dsEmployee.Tables(0), "employeeunkid not in (" & mstrEmployeeIDs.Trim & ")", "", DataViewRowState.CurrentRows).ToTable()
            Else
                mdtPopupEmpAddEditList = dsEmployee.Tables(0)
            End If

            'If dsEmployee.Tables(0).Rows.Count <= 0 Then
            '    mdtPopupEmpAddEditList.Rows.Add(dsEmployee.Tables(0).NewRow())
            '    isblank = True
            'End If

            If mdtPopupEmpAddEditList.Rows.Count <= 0 Then
                mdtPopupEmpAddEditList.Rows.Add(mdtPopupEmpAddEditList.NewRow())
                isblank = True
            End If

            'Pinkal (03-Mar-2020) -- End

            gvAddEditEmployee.DataSource = mdtPopupEmpAddEditList
            gvAddEditEmployee.DataBind()

            If isblank Then
                gvAddEditEmployee.Rows(0).Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objEmployee = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(drpAddEditApprover.SelectedValue) <= 0 Then
                'Pinkal (10-Jan-2020) -- Start
                'Enhancements -  Working on OT Requisistion for NMB.
                'Language.setLanguage(mstrModuleName)
                Language.setLanguage(mstrModuleName1)
                'Pinkal (10-Jan-2020) -- End
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "OT Approver is mandatory information. Please provide OT Approver to continue."), Me)
                drpAddEditApprover.Focus()
                Return False
            End If

            If CInt(drpAddEditApproverLevel.SelectedValue) <= 0 Then

                'Pinkal (10-Jan-2020) -- Start
                'Enhancements -  Working on OT Requisistion for NMB.
                'Language.setLanguage(mstrModuleName)
                Language.setLanguage(mstrModuleName1)
                'Pinkal (10-Jan-2020) -- End
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 2, "OT Approver Level is mandatory information. Please provide OT Approver Level to continue"), Me)
                drpAddEditApproverLevel.Focus()
                Return False
            End If

            If chkAddEditExtApprover.Checked = False Then
                If CInt(drpAddEditUser.SelectedValue) <= 0 Then
                    'Pinkal (10-Jan-2020) -- Start
                    'Enhancements -  Working on OT Requisistion for NMB.
                    'Language.setLanguage(mstrModuleName)
                    Language.setLanguage(mstrModuleName1)
                    'Pinkal (10-Jan-2020) -- End
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 3, "User is mandatory information. Please provide User to continue"), Me)
                    drpAddEditUser.Focus()
                    Return False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Private Sub SelectedEmplyeeList(ByVal IsBlank As Boolean)
        Dim strSearch As String = String.Empty
        Try

            If mdtPopupAddeditSelectEmpList Is Nothing Then Exit Sub

            If mdtPopupAddeditSelectEmpList.Columns.Contains("IsCheck") = False Then
                mdtPopupAddeditSelectEmpList.Columns.Add("IsCheck", Type.GetType("System.Boolean"))
                mdtPopupAddeditSelectEmpList.Columns("IsCheck").DefaultValue = False
            End If

            If mdtPopupAddeditSelectEmpList.Rows.Count <= 0 Then
                mdtPopupAddeditSelectEmpList.Rows.Add(mdtPopupAddeditSelectEmpList.NewRow())
            End If

            dtAssignEmpView = mdtPopupAddeditSelectEmpList.DefaultView

            If IsBlank = False Then dtAssignEmpView.RowFilter = " AUD <> 'D' "

            GvSelectedEmployee.AutoGenerateColumns = False

            If txtAssignedEmpSearch.Text.Trim.Length > 0 Then
                'Pinkal (10-Jan-2020) -- Start
                'Enhancements -  Working on OT Requisistion for NMB.
                If dtAssignEmpView.RowFilter.Length > 0 Then
                    dtAssignEmpView.RowFilter &= " AND ename like '%" & txtAssignedEmpSearch.Text.Trim() & "%' "
                Else
                    dtAssignEmpView.RowFilter = "ename like '%" & txtAssignedEmpSearch.Text.Trim() & "%' "
                End If
                'Pinkal (10-Jan-2020) -- End
            End If

            GvSelectedEmployee.DataSource = dtAssignEmpView
            GvSelectedEmployee.DataBind()

            If IsBlank Then
                GvSelectedEmployee.Rows(0).Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private Sub SetValue()
    Private Sub SetValue(ByVal objclsTnaapprover_master As clsTnaapprover_master)
        'Pinkal (03-Sep-2020) -- End
        Try
            If mintTnaMappingunkid > 0 Then
                objclsTnaapprover_master._Tnamappingunkid = mintTnaMappingunkid
            End If

            objclsTnaapprover_master._Tnalevelunkid = Convert.ToInt32(drpAddEditApproverLevel.SelectedValue)

            If chkAddEditExtApprover.Checked = False Then
                objclsTnaapprover_master._ApproverEmployeeId = CInt(drpAddEditApprover.SelectedValue)
                objclsTnaapprover_master._Mapuserunkid = Convert.ToInt32(drpAddEditUser.SelectedValue)
            Else
                'Pinkal (03-Mar-2020) -- Start
                'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.
                'objclsTnaapprover_master._ApproverEmployeeId = 0
                objclsTnaapprover_master._ApproverEmployeeId = Convert.ToInt32(drpAddEditApprover.SelectedValue)
                'Pinkal (03-Mar-2020) -- End
                objclsTnaapprover_master._Mapuserunkid = Convert.ToInt32(drpAddEditApprover.SelectedValue)
            End If

            objclsTnaapprover_master._Tnatypeid = enTnAApproverType.OT_Requisition_Approver
            objclsTnaapprover_master._Isotcap_Hod = Convert.ToBoolean(chkHodCapForOT.Checked)
            objclsTnaapprover_master._Userunkid = CInt(Session("UserId"))
            objclsTnaapprover_master._Isactive = True
            objclsTnaapprover_master._IsSwap = False
            objclsTnaapprover_master._IsExternalApprover = chkAddEditExtApprover.Checked

            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'Call SetAtValue(mstrModuleName1)
            Call SetAtValue(mstrModuleName1, objclsTnaapprover_master)
            'Pinkal (03-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Protected Sub SetEditValue()
    Protected Sub SetEditValue(ByVal objclsTnaapprover_master As clsTnaapprover_master)
        Dim objclsTnaapprover_tran As clsTnaapprover_Tran
        'Pinkal (03-Sep-2020) -- End
        Try
            chkAddEditExtApprover.Checked = objclsTnaapprover_master._IsExternalApprover
            chkAddEditExtApprover_CheckedChanged(New Object(), New EventArgs())

            If chkAddEditExtApprover.Checked Then
                drpAddEditApprover.SelectedValue = objclsTnaapprover_master._Mapuserunkid
            Else
                drpAddEditApprover.SelectedValue = objclsTnaapprover_master._ApproverEmployeeId
                drpAddEditUser.SelectedValue = objclsTnaapprover_master._Mapuserunkid
            End If

            drpAddEditApprover.Enabled = False
            chkAddEditExtApprover.Enabled = False

            drpAddEditApproverLevel.SelectedValue = objclsTnaapprover_master._Tnalevelunkid
            chkHodCapForOT.Checked = objclsTnaapprover_master._Isotcap_Hod


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objclsTnaapprover_tran = New clsTnaapprover_Tran
            'Pinkal (03-Sep-2020) -- End

            objclsTnaapprover_tran._TnAMappingUnkId = mintTnaMappingunkid
            objclsTnaapprover_tran._EmployeeAsonDate = ConfigParameter._Object._CurrentDateAndTime.Date

            'Pinkal (08-Jan-2020) -- Start
            'Enhancement - NMB - Working on NMB OT Requisition Requirement.
            drpAddEditApprover_SelectedIndexChanged(New Object, New EventArgs())
            'Pinkal (08-Jan-2020) -- End

            objclsTnaapprover_tran.Get_Data()
            mdtPopupAddeditSelectEmpList = objclsTnaapprover_tran._DataTable

            SelectedEmplyeeList(False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTnaapprover_tran = Nothing
            'Pinkal (03-Sep-2020) -- End   
        End Try
    End Sub

    Private Sub CheckAll(ByVal gv As GridView, ByVal dt As DataTable, ByVal chkid As String, ByVal filter As String)
        Try
            Dim head As GridViewRow = gv.HeaderRow
            Dim chkall As CheckBox = TryCast(head.FindControl(chkid), CheckBox)
            If gv.Rows.Count <> dt.Select(filter + "=True").Length Then
                chkall.Checked = False
            ElseIf gv.Rows.Count = dt.Select(filter + "=True").Length Then
                chkall.Checked = True
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub emptyGrid(ByVal gv As GridView, ByVal dt As DataTable)
        Try
            If dt Is Nothing Then
                dt.Rows.Add(dt.NewRow())
                gv.DataSource = dt
                gv.DataBind()
                gv.Rows(0).Visible = False
            Else
                If dt.Rows.Count <= 0 Then
                    dt.Rows.Add(dt.NewRow())
                    gv.DataSource = dt
                    gv.DataBind()
                    gv.Rows(0).Visible = False
                Else
                    gv.DataSource = dt
                    gv.DataBind()
                    gv.Rows(0).Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private Sub SetAtValue(ByVal xFormName As String)
    Private Sub SetAtValue(ByVal xFormName As String, ByVal objclsTnaapprover_master As clsTnaapprover_master)
        'Pinkal (03-Sep-2020) -- End
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objclsTnaapprover_master._AuditUserId = CInt(Session("UserId"))
            End If
            objclsTnaapprover_master._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objclsTnaapprover_master._ClientIP = CStr(Session("IP_ADD"))
            objclsTnaapprover_master._LoginEmployeeunkid = -1
            objclsTnaapprover_master._HostName = CStr(Session("HOST_NAME"))
            objclsTnaapprover_master._FormName = xFormName
            objclsTnaapprover_master._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetAddedit()
        Try
            chkAddEditExtApprover.Checked = False
            drpAddEditApprover.SelectedIndex = 0
            drpAddEditApproverLevel.SelectedIndex = 0
            drpAddEditUser.SelectedIndex = 0
            chkHodCapForOT.Checked = False
            popupOTApproverAddEdit.Hide()
            mblnpopupOTApproverAddEdit = False
            FillList(False)
            SelectedEmplyeeList(True)
            mintTnaMappingunkid = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Add_DataRow(ByVal dRow As DataRow)
        Try
            If mdtPopupAddeditSelectEmpList Is Nothing Then Exit Sub
            Dim mdtRow As DataRow = Nothing
            Dim dtAssignEmp() As DataRow = Nothing
            dtAssignEmp = mdtPopupAddeditSelectEmpList.Select("employeeunkid = '" & CInt(dRow.Item("employeeunkid")) & "' AND AUD <> 'D' ")
            If dtAssignEmp.Length <= 0 Then
                mdtRow = mdtPopupAddeditSelectEmpList.NewRow
                mdtRow.Item("ischeck") = False
                mdtRow.Item("tnaapprovertranunkid") = -1
                mdtRow.Item("tnamappingunkid") = mintTnaMappingunkid
                mdtRow.Item("employeeunkid") = dRow.Item("employeeunkid")
                mdtRow.Item("userunkid") = CInt(Session("UserId"))
                mdtRow.Item("isvoid") = False
                mdtRow.Item("voiddatetime") = DBNull.Value
                mdtRow.Item("voiduserunkid") = -1
                mdtRow.Item("voidreason") = ""
                mdtRow.Item("AUD") = "A"
                mdtRow.Item("GUID") = Guid.NewGuid.ToString
                mdtRow.Item("ename") = dRow.Item("employeecode").ToString() + " - " + dRow.Item("name").ToString()
                mdtRow.Item("edept") = dRow.Item("departmentname")
                mdtRow.Item("ejob") = dRow.Item("jobname")
                mdtPopupAddeditSelectEmpList.Rows.Add(mdtRow)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Dropdown Event"

    Protected Sub drpAddEditApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpAddEditApprover.SelectedIndexChanged
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTnaapprover_master As New clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End
        Try
            mstrEmployeeIDs = objclsTnaapprover_master.GetApproverEmployeeId(CInt(drpAddEditApprover.SelectedValue), chkAddEditExtApprover.Checked, chkHodCapForOT.Checked)
            FillEmployeeList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTnaapprover_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region "Checkbox Event"

    Protected Sub chkAddEditExtApprover_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAddEditExtApprover.CheckedChanged
        Try
            PnlApproverUser.Visible = Not chkAddEditExtApprover.Checked
            drpAddEditUser.SelectedValue = "0"
            Call FillAddEditCombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkSelectAddEditEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chk As CheckBox = TryCast((sender), CheckBox)
            Dim gvRow As GridViewRow = TryCast((chk).NamingContainer, GridViewRow)

            Dim drRow() As DataRow = CType(mdtPopupEmpAddEditList, DataTable).Select("employeecode = '" & gvAddEditEmployee.DataKeys(gvRow.RowIndex)("employeecode").ToString() & "'")
            If drRow.Length > 0 Then
                drRow(0)("ischeck") = chk.Checked
                drRow(0).AcceptChanges()
            End If
            'CheckAll(gvAddEditEmployee, mdtPopupEmpAddEditList, "ChkAllAddEditEmployee", "ischeck")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkAllAddEditEmployee_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If gvAddEditEmployee.Rows.Count <= 0 Then Exit Sub
            If gvAddEditEmployee.Rows.Count > 0 Then
                For i As Integer = 0 To gvAddEditEmployee.Rows.Count - 1
                    If mdtPopupEmpAddEditList.Rows.Count - 1 < i Then Exit For
                    Dim drRow As DataRow() = mdtPopupEmpAddEditList.Select("employeecode = '" & gvAddEditEmployee.Rows(i).Cells(1).Text.Trim & "'")
                    If drRow.Length > 0 Then
                        drRow(0)("IsCheck") = cb.Checked
                        Dim gvRow As GridViewRow = gvAddEditEmployee.Rows(i)
                        CType(gvRow.FindControl("ChkSelectAddEditEmployee"), CheckBox).Checked = cb.Checked
                    End If
                    mdtPopupEmpAddEditList.AcceptChanges()
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelectedEmp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chk As CheckBox = TryCast((sender), CheckBox)
            Dim gvRow As GridViewRow = TryCast((chk).NamingContainer, GridViewRow)

            Dim drRow() As DataRow = CType(mdtPopupAddeditSelectEmpList, DataTable).Select("employeeunkid = '" & GvSelectedEmployee.DataKeys(gvRow.RowIndex)("employeeunkid").ToString() & "'")
            If drRow.Length > 0 Then
                drRow(0)("ischeck") = chk.Checked
                drRow(0).AcceptChanges()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChkAllSelectedEmp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If GvSelectedEmployee.Rows.Count <= 0 Then Exit Sub
            If GvSelectedEmployee.Rows.Count > 0 Then
                For i As Integer = 0 To GvSelectedEmployee.Rows.Count - 1
                    If mdtPopupAddeditSelectEmpList.Rows.Count - 1 < i Then Exit For

                    'Pinkal (27-Dec-2019) -- Start
                    'Enhancement - Changes related To OT NMB Testing.
                    'Dim drRow As DataRow() = mdtPopupAddeditSelectEmpList.Select("ename = '" & gvAddEditEmployee.Rows(i).cells(1).Text & "'")
                    Dim drRow As DataRow() = mdtPopupAddeditSelectEmpList.Select("employeeunkid = '" & CInt(GvSelectedEmployee.DataKeys(i)("employeeunkid")) & "'")
                    'Pinkal (27-Dec-2019) -- End

                    If drRow.Length > 0 Then
                        drRow(0)("IsCheck") = cb.Checked
                        Dim gvRow As GridViewRow = GvSelectedEmployee.Rows(i)
                        CType(gvRow.FindControl("chkSelectedEmp"), CheckBox).Checked = cb.Checked
                    End If
                    mdtPopupAddeditSelectEmpList.AcceptChanges()
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Button Event"

    Protected Sub btnAddEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddEmployee.Click
        Try
            If Validation() = False Then Exit Sub

            'Pinkal (10-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion for NMB.

            'Dim drCheck = mdtPopupEmpAddEditList.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True)

            'If drCheck.Count > 0 Then
            '    For i As Integer = 0 To drCheck.Count - 1
            '        Call Add_DataRow(drCheck(i))
            '    Next
            'End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = gvAddEditEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkSelectAddEditEmployee"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
                Exit Sub
            End If

            Dim xCount As Integer = -1
            For i As Integer = 0 To gRow.Count - 1
                xCount = i
                Dim dRow As DataRow() = mdtPopupEmpAddEditList.Select("employeeunkid = " & CInt(gvAddEditEmployee.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")))
                If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
                    Add_DataRow(dRow(0))
                End If
            Next

            'Pinkal (10-Jan-2020) -- End

            SelectedEmplyeeList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDeleteEmployee_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteEmployee.Click
        Try
            If mdtPopupEmpAddEditList Is Nothing Then Exit Sub
            confirmationSelectedEmp.Title = "Aruti"
            Language.setLanguage(mstrModuleName)
            confirmationSelectedEmp.Message = Language.getMessage(mstrModuleName1, 4, "Are you sure you want to delete Selected Employee?")
            confirmationSelectedEmp.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnpopupclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnpopupclose.Click
        Try

            'Pinkal (10-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion for NMB.
            txtAssignedEmpSearch.Text = ""
            'Pinkal (10-Jan-2020) -- End

            mblnpopupOTApproverAddEdit = False
            popupOTApproverAddEdit.Hide()
            chkHodCapForOT.Checked = False
            mdtPopupEmpAddEditList.Rows.Clear()
            mdtPopupAddeditSelectEmpList.Rows.Clear()
            ResetAddedit()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnpopupsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnpopupsave.Click
        Dim blnFlag As Boolean = False
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objclsTnaapprover_master As clsTnaapprover_master
        'Pinkal (03-Sep-2020) -- End

        Try
            If Validation() = False Then Exit Sub

            Dim xcount As Integer = mdtPopupAddeditSelectEmpList.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").DefaultIfEmpty().Count

            If xcount = 0 OrElse mdtPopupAddeditSelectEmpList.Rows.Count <= 0 OrElse GvSelectedEmployee.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 5, "Employee is compulsory information.Please assigned atleast One Employee to this approver."), Me)
                Exit Sub
            End If


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'SetValue()
            objclsTnaapprover_master = New clsTnaapprover_master
            SetValue(objclsTnaapprover_master)
            'Pinkal (03-Sep-2020) -- End

            If mintTnaMappingunkid > 0 Then
                blnFlag = objclsTnaapprover_master.Update(mdtPopupAddeditSelectEmpList)
            Else
                blnFlag = objclsTnaapprover_master.Insert(mdtPopupAddeditSelectEmpList)
            End If

            If blnFlag = False And objclsTnaapprover_master._Message <> "" Then
                DisplayMessage.DisplayMessage(objclsTnaapprover_master._Message, Me)
                Exit Sub
            End If

            If blnFlag Then
                mdtPopupEmpAddEditList.Rows.Clear()
                mdtPopupAddeditSelectEmpList.Rows.Clear()
                dtAssignEmpView = Nothing
                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                'ResetAddedit()
                'Pinkal (03-Sep-2020) -- End
            End If

            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 6, "Approver Saved Successfully."), Me)
            ResetAddedit()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objclsTnaapprover_master = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub confirmapproverdelete_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles confirmapproverdelete.buttonYes_Click
        Try
            DeleteApprovalReason.Title = Language.getMessage(mstrModuleName, 9, "Are you sure you want delete approver ?")
            DeleteApprovalReason.Reason = ""
            DeleteApprovalReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub confirmationSelectedEmp_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles confirmationSelectedEmp.buttonYes_Click
        Try


            'Pinkal (10-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion for NMB.
            'dtAssignEmpView = mdtPopupAddeditSelectEmpList.DefaultView
            'Dim drRow() As DataRow = dtAssignEmpView.Table.Select("ischeck=True AND AUD <> 'D' ")
            'If drRow.Length <= 0 Then
            '    Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 7, "Please check atleast one of the employee to unassigned."), Me)
            '    Exit Sub
            'End If


            'Dim blnFlag As Boolean = False

            ''Dim objLoan As New clsProcess_pending_loan
            'Dim mblnFlag As Boolean = True

            'For i As Integer = 0 To drRow.Length - 1
            '    If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
            '        'If objLoan.GetApproverPendingLoanFormCount(mintapproverunkid, drRow(i)("employeeunkid").ToString()) <= 0 Then
            '        drRow(i).Item("AUD") = "D"
            '        drRow(i).Item("isvoid") = True
            '        drRow(i).Item("voiddatetime") = DateAndTime.Now
            '        drRow(i).Item("voiduserunkid") = Session("UserId")
            '        drRow(i).Item("voidreason") = ""
            '        'Else
            '        mblnFlag = False
            '        'End If
            '        drRow(i).AcceptChanges()
            '    End If
            'Next
            'dtAssignEmpView.Table.AcceptChanges()

            ''If mblnFlag = False Then
            ''    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 14, "This Employee(s) has pending loan application(s).You cannot delete this employee(s)."), Me)
            ''End If
            ''objLoan = Nothing


            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = GvSelectedEmployee.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkSelectedEmp"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 7, "Please check atleast one of the employee to unassigned."), Me)
                Exit Sub
            End If

            Dim drRow As DataRow() = Nothing
            Dim xCount As Integer = -1
            Dim objOtRequisition As New clsOT_Requisition_Tran

            For i As Integer = 0 To gRow.Count - 1
                xCount = i
                If objOtRequisition.GetApproverPendingOTRequisition(CInt(GvSelectedEmployee.DataKeys(gRow(xCount).DataItemIndex)("tnamappingunkid")), CInt(GvSelectedEmployee.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")).ToString()).Trim().Length <= 0 Then
                    drRow = mdtPopupAddeditSelectEmpList.Select("employeeunkid = " & CInt(GvSelectedEmployee.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")) & " AND AUD <> 'D'")
                Else
                    Language.setLanguage(mstrModuleName1)
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 8, "This Employee has Pending OT Requisition application form.You cannot delete this employee."), Me)
                    Exit For
                End If

                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    drRow(0).Item("AUD") = "D"
                    drRow(0).Item("isvoid") = True
                    drRow(0).Item("voiddatetime") = DateAndTime.Now
                    drRow(0).Item("voiduserunkid") = Session("UserId")
                    drRow(0).Item("voidreason") = ""
                    drRow(0).AcceptChanges()
                End If

            Next
            mdtPopupAddeditSelectEmpList.AcceptChanges()


            'Pinkal (10-Jan-2020) -- End

            SelectedEmplyeeList(False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (27-Dec-2019) -- Start
    'Enhancement - Changes related To OT NMB Testing.

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceSearch = popupAdvanceFilter._GetFilterString
            FillEmployeeList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (27-Dec-2019) -- End

#End Region

#Region "Gridview Event"



    '    Protected Sub TvApproverUseraccess_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles TvApproverUseraccess.RowDataBound
    '        Dim oldid As String
    '        Try
    '            If e.Row.RowType = DataControlRowType.DataRow Then
    '                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

    '                oldid = dt.Rows(e.Row.RowIndex)("UserAccess").ToString()
    '                If dt.Rows(e.Row.RowIndex)("AllocationLevel").ToString() = -1 AndAlso oldid <> currentId Then
    '                    Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("UserAccess").ToString(), TvApproverUseraccess)
    '                    currentId = oldid
    '                Else
    '                    Dim statusCell As TableCell = e.Row.Cells(0)
    '                    statusCell.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dt.Rows(e.Row.RowIndex)("UserAccess").ToString
    '                End If
    '            End If
    '        Catch ex As Exception
    '            DisplayMessage.DisplayError(ex, Me)
    '        Finally
    '            currentId = ""
    '            oldid = ""
    '        End Try
    '    End Sub


#End Region

#Region "TextBox Event"

    Protected Sub txtSearchEmployee_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmployee.TextChanged
        Try
            If mdtPopupEmpAddEditList IsNot Nothing Then
                Dim dvEmployee As DataView = mdtPopupEmpAddEditList.DefaultView()
                If dvEmployee.Table.Rows.Count > 0 Then
                    If txtSearchEmployee.Text.Trim.Length > 0 Then
                        dvEmployee.RowFilter = "employeecode like '%" & txtSearchEmployee.Text.Trim() & "%' OR name like '%" & txtSearchEmployee.Text.Trim() & "%' "
                    End If
                    'Pinkal (09-Mar-2020) -- Start
                    'Enhancement OT Requisition  - OT Requisition Enhancement given by NMB .
                    'dvEmployee.RowFilter &= " AND AUD <> 'D' "
                    'Pinkal (09-Mar-2020) -- End
                    gvAddEditEmployee.DataSource = dvEmployee.ToTable()
                    gvAddEditEmployee.DataBind()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtAssignedEmpSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAssignedEmpSearch.TextChanged
        Try
            SelectedEmplyeeList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "LinkButton Event"

    'Pinkal (27-Dec-2019) -- Start
    'Enhancement - Changes related To OT NMB Testing.
    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            If CInt(drpAddEditApprover.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName1, 1, "OT Approver is mandatory information. Please provide OT Approver to continue."), Me)
                Exit Sub
            Else
                popupAdvanceFilter.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (27-Dec-2019) -- End

    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
        Try
            txtSearchEmployee.Text = ""
            If mdtPopupEmpAddEditList IsNot Nothing Then
                Dim dRow() As DataRow = mdtPopupEmpAddEditList.Select("isCheck = True")
                If dRow.Length > 0 Then
                    For i As Integer = 0 To dRow.Length - 1
                        dRow(i).Item("isCheck") = False
                        dRow(i).AcceptChanges()
                    Next
                End If
            End If

            'Pinkal (27-Dec-2019) -- Start
            'Enhancement - Changes related To OT NMB Testing.
            mstrAdvanceSearch = ""
            If gvAddEditEmployee.Rows.Count > 0 Then
                Call FillEmployeeList(False)
            Else
                Call FillEmployeeList(True)
            End If
            'Pinkal (27-Dec-2019) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Language._Object.setCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Language._Object.setCaption(Me.lblLevel.ID, Me.lblLevel.Text)
            Language._Object.setCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Language._Object.setCaption(Me.lblStatus.ID, Me.lblStatus.Text)

            Language._Object.setCaption(Me.gvApproverList.Columns(3).FooterText, Me.gvApproverList.Columns(3).HeaderText)
            Language._Object.setCaption(Me.gvApproverList.Columns(4).FooterText, Me.gvApproverList.Columns(4).HeaderText)
            Language._Object.setCaption(Me.gvApproverList.Columns(5).FooterText, Me.gvApproverList.Columns(5).HeaderText)
            Language._Object.setCaption(Me.gvApproverList.Columns(6).FooterText, Me.gvApproverList.Columns(6).HeaderText)
            Language._Object.setCaption(Me.gvApproverList.Columns(7).FooterText, Me.gvApproverList.Columns(7).HeaderText)

            Language._Object.setCaption(Me.btnNew.ID, Me.btnNew.Text.Replace("&", ""))
            Language._Object.setCaption(Me.btnSearch.ID, Me.btnSearch.Text.Replace("&", ""))
            Language._Object.setCaption(Me.btnReset.ID, Me.btnReset.Text.Replace("&", ""))



            Language.setLanguage(mstrModuleName1)
            Language._Object.setCaption(Me.lblPageHeader1.ID, Me.lblPageHeader1.Text)
            Language._Object.setCaption(Me.lblApproverInfo.ID, Me.lblApproverInfo.Text)
            Language._Object.setCaption(Me.chkAddEditExtApprover.ID, Me.chkAddEditExtApprover.Text)
            Language._Object.setCaption(Me.lblApproverName.ID, Me.lblApproverName.Text)
            Language._Object.setCaption(Me.lblApproverlevel.ID, Me.lblApproverlevel.Text)
            Language._Object.setCaption(Me.lblApproverUser.ID, Me.lblApproverUser.Text)
            Language._Object.setCaption(Me.chkHodCapForOT.ID, Me.chkHodCapForOT.Text)
            Language._Object.setCaption(Me.lnkReset.ID, Me.lnkReset.Text)

            Language._Object.setCaption(Me.gvAddEditEmployee.Columns(1).FooterText, Me.gvAddEditEmployee.Columns(1).HeaderText)
            Language._Object.setCaption(Me.gvAddEditEmployee.Columns(2).FooterText, Me.gvAddEditEmployee.Columns(2).HeaderText)

            Language._Object.setCaption(Me.GvSelectedEmployee.Columns(1).FooterText, Me.GvSelectedEmployee.Columns(1).HeaderText)
            Language._Object.setCaption(Me.GvSelectedEmployee.Columns(2).FooterText, Me.GvSelectedEmployee.Columns(2).HeaderText)
            Language._Object.setCaption(Me.GvSelectedEmployee.Columns(3).FooterText, Me.GvSelectedEmployee.Columns(3).HeaderText)

            Language._Object.setCaption(Me.btnAddEmployee.ID, Me.btnAddEmployee.Text.Replace("&", ""))
            Language._Object.setCaption(Me.btnDeleteEmployee.ID, Me.btnDeleteEmployee.Text.Replace("&", ""))

            Language._Object.setCaption(Me.btnpopupsave.ID, Me.btnpopupsave.Text.Replace("&", ""))
            Language._Object.setCaption(Me.btnpopupclose.ID, Me.btnpopupclose.Text.Replace("&", ""))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.lblPageHeader.Text = Language._Object.getCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)
            Me.lblCaption.Text = Language._Object.getCaption(Me.lblCaption.ID, Me.lblCaption.Text)
            Me.lblLevel.Text = Language._Object.getCaption(Me.lblLevel.ID, Me.lblLevel.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.ID, Me.lblStatus.Text)

            Me.gvApproverList.Columns(3).HeaderText = Language._Object.getCaption(Me.gvApproverList.Columns(3).FooterText, Me.gvApproverList.Columns(3).HeaderText)
            Me.gvApproverList.Columns(4).HeaderText = Language._Object.getCaption(Me.gvApproverList.Columns(4).FooterText, Me.gvApproverList.Columns(4).HeaderText)
            Me.gvApproverList.Columns(5).HeaderText = Language._Object.getCaption(Me.gvApproverList.Columns(5).FooterText, Me.gvApproverList.Columns(5).HeaderText)
            Me.gvApproverList.Columns(6).HeaderText = Language._Object.getCaption(Me.gvApproverList.Columns(6).FooterText, Me.gvApproverList.Columns(6).HeaderText)
            Me.gvApproverList.Columns(7).HeaderText = Language._Object.getCaption(Me.gvApproverList.Columns(7).FooterText, Me.gvApproverList.Columns(7).HeaderText)

            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.ID, Me.btnNew.Text.Replace("&", ""))
            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text.Replace("&", ""))
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text.Replace("&", ""))



            Language.setLanguage(mstrModuleName1)
            Me.lblPageHeader1.Text = Language._Object.getCaption(Me.lblPageHeader1.ID, Me.lblPageHeader1.Text)
            Me.lblApproverInfo.Text = Language._Object.getCaption(Me.lblApproverInfo.ID, Me.lblApproverInfo.Text)
            Me.chkAddEditExtApprover.Text = Language._Object.getCaption(Me.chkAddEditExtApprover.ID, Me.chkAddEditExtApprover.Text)
            Me.lblApproverName.Text = Language._Object.getCaption(Me.lblApproverName.ID, Me.lblApproverName.Text)
            Me.lblApproverlevel.Text = Language._Object.getCaption(Me.lblApproverlevel.ID, Me.lblApproverlevel.Text)
            Me.lblApproverUser.Text = Language._Object.getCaption(Me.lblApproverUser.ID, Me.lblApproverUser.Text)
            Me.chkHodCapForOT.Text = Language._Object.getCaption(Me.chkHodCapForOT.ID, Me.chkHodCapForOT.Text)
            Me.lnkReset.Text = Language._Object.getCaption(Me.lnkReset.ID, Me.lnkReset.Text)

            Me.gvAddEditEmployee.Columns(1).HeaderText = Language._Object.getCaption(Me.gvAddEditEmployee.Columns(1).FooterText, Me.gvAddEditEmployee.Columns(1).HeaderText)
            Me.gvAddEditEmployee.Columns(2).HeaderText = Language._Object.getCaption(Me.gvAddEditEmployee.Columns(2).FooterText, Me.gvAddEditEmployee.Columns(2).HeaderText)

            Me.GvSelectedEmployee.Columns(1).HeaderText = Language._Object.getCaption(Me.GvSelectedEmployee.Columns(1).FooterText, Me.GvSelectedEmployee.Columns(1).HeaderText)
            Me.GvSelectedEmployee.Columns(2).HeaderText = Language._Object.getCaption(Me.GvSelectedEmployee.Columns(2).FooterText, Me.GvSelectedEmployee.Columns(2).HeaderText)
            Me.GvSelectedEmployee.Columns(3).HeaderText = Language._Object.getCaption(Me.GvSelectedEmployee.Columns(3).FooterText, Me.GvSelectedEmployee.Columns(3).HeaderText)

            Me.btnAddEmployee.Text = Language._Object.getCaption(Me.btnAddEmployee.ID, Me.btnAddEmployee.Text.Replace("&", ""))
            Me.btnDeleteEmployee.Text = Language._Object.getCaption(Me.btnDeleteEmployee.ID, Me.btnDeleteEmployee.Text.Replace("&", ""))

            Me.btnpopupsave.Text = Language._Object.getCaption(Me.btnpopupsave.ID, Me.btnpopupsave.Text.Replace("&", ""))
            Me.btnpopupclose.Text = Language._Object.getCaption(Me.btnpopupclose.ID, Me.btnpopupclose.Text.Replace("&", ""))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, You cannot delete this Approver . Reason: This Approver is in use.")
            Language.setMessage(mstrModuleName, 2, "Confirmation")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this approver ?")
            Language.setMessage(mstrModuleName, 4, "Active")
            Language.setMessage(mstrModuleName, 5, "DeActive")
            Language.setMessage(mstrModuleName, 6, "Delete")
            Language.setMessage(mstrModuleName, 7, "Are you sure you want to de-active this approver?")
            Language.setMessage(mstrModuleName, 8, "Are you sure you want to active this approver?")
            Language.setMessage(mstrModuleName, 9, "Are you sure you want delete approver ?")
            Language.setMessage(mstrModuleName, 10, "You cannot inactive this approver.Reason :This Approver has Pending ot requisition application(s).")

            Language.setMessage(mstrModuleName1, 1, "OT Approver is mandatory information. Please provide OT Approver to continue.")
            Language.setMessage(mstrModuleName1, 2, "OT Approver Level is mandatory information. Please provide OT Approver Level to continue")
            Language.setMessage(mstrModuleName1, 3, "User is mandatory information. Please provide User to continue")
            Language.setMessage(mstrModuleName1, 4, "Are you sure you want to delete Selected Employee?")
            Language.setMessage(mstrModuleName1, 5, "Employee is compulsory information.Please assigned atleast One Employee to this approver.")
            Language.setMessage(mstrModuleName1, 6, "Approver Saved Successfully.")
            Language.setMessage(mstrModuleName1, 7, "Please check atleast one of the employee to unassigned.")
            Language.setMessage(mstrModuleName1, 8, "This Employee has Pending OT Requisition application form.You cannot delete this employee.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>





End Class
