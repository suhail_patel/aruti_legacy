﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
#End Region

Partial Class TnA_OT_Requisition_OT_EmployeeAssignment
    Inherits Basepage

#Region "Private Variables"
    Private Shared ReadOnly mstrModuleName As String = "frmOTEmployeeAssignment"
    Dim DisplayMessage As New CommonCodes

    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private objAssignOT As New clsassignemp_ot
    'Pinkal (03-Sep-2020) -- End

    Private mintEmployeeId As Integer = -1 'FOR ESS
    Private mstrAdvanceFilter As String = String.Empty
    Private mintTnAEmployeeOTunkid As Integer = -1
    Private mdtFromDate As Date = Nothing
    Private mdtToDate As Date = Nothing
    Private mdtEmployee As DataTable = Nothing
#End Region

#Region "Page's Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                'Pinkal (03-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
                GC.Collect()
                'Pinkal (03-Sep-2020) -- End

                If Session("tnaemployeeotunkid") IsNot Nothing AndAlso CInt(Session("tnaemployeeotunkid")) > 0 Then
                    mintTnAEmployeeOTunkid = CInt(Session("tnaemployeeotunkid"))
                End If

                Call SetControlCaptions()
                Call SetMessages()
                Call Language._Object.SaveValue()
                Call SetLanguage()

                Call FillCombo()
                Call GetValue()
                Call FillList()

                dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
                dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime
                lblValue.Text = dtpFromDate.GetDate.Date & " - " & dtpToDate.GetDate.Date
                If mintTnAEmployeeOTunkid > 0 Then lnkAllocation.Visible = False
            Else
                mintTnAEmployeeOTunkid = CInt(Me.ViewState("tnaemployeeotunkid"))
                mintEmployeeId = CInt(Me.ViewState("EmployeeId"))
                mintTnAEmployeeOTunkid = CInt(Me.ViewState("TnAEmployeeOTunkid"))
                mdtFromDate = CDate(Me.ViewState("FromDate"))
                mdtToDate = CDate(Me.ViewState("ToDate"))
                mdtEmployee = CType(Me.ViewState("mdsEmployee"), DataTable)
                mstrAdvanceFilter = CStr(Me.ViewState("AdvanceFilter"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("tnaemployeeotunkid") = mintTnAEmployeeOTunkid
            Me.ViewState("EmployeeId") = mintEmployeeId
            Me.ViewState("TnAEmployeeOTunkid") = mintTnAEmployeeOTunkid
            Me.ViewState("FromDate") = mdtFromDate
            Me.ViewState("ToDate") = mdtToDate
            Me.ViewState("mdsEmployee") = mdtEmployee
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsFill As DataSet = Nothing
            Dim dtFill As DataTable = Nothing
            Dim objMaster As New clsMasterData

            dsFill = objMaster.getGenderList("Gender", True)
            With cboGender
                .DataTextField = "Name"
                .DataValueField = "id"
                .DataSource = dsFill.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsFill = objMaster.GetCondition(True, True, False, False, False)
            With cboFromcondition
                .DataTextField = "Name"
                .DataValueField = "id"
                .DataSource = dsFill.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            With cboTocondition
                .DataTextField = "Name"
                .DataValueField = "id"
                .DataSource = dsFill.Tables(0).Copy
                .DataBind()
                .SelectedValue = CStr(0)
            End With


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            objMaster = Nothing
            If dtFill IsNot Nothing Then dtFill.Clear()
            dtFill = Nothing
            If dsFill IsNot Nothing Then dsFill.Clear()
            dsFill = Nothing
            'Pinkal (03-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim dsList As DataSet = Nothing
        Dim objAssignOT As New clsassignemp_ot
        Dim objEmployee As New clsEmployee_Master
        'Pinkal (03-Sep-2020) -- End
        Try

            Dim strSearching As String = ""

            Dim dtFromDate As Date = Nothing
            Dim dtToDate As Date = Nothing
            Dim blnReinstatement As Boolean = False

            If radAppointedDate.Checked = True Then
                dtFromDate = dtpFromDate.GetDate.Date
                dtToDate = dtpToDate.GetDate.Date
            Else
                dtFromDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                dtToDate = eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)
                blnReinstatement = True
            End If

            If CInt(cboGender.SelectedValue) > 0 Then
                strSearching &= "AND hremployee_master.gender = " & CInt(cboGender.SelectedValue) & " "
            End If

            If radExpYear.Checked = True Then
                If CInt(txtFromYear.Text) > 0 Or CInt(txtFromMonth.Text) > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboFromcondition.SelectedItem.Text & " '" & eZeeDate.convertDate(mdtToDate) & "' "
                End If

                If CInt(txtToYear.Text) > 0 Or CInt(txtToMonth.Text) > 0 Then
                    strSearching &= "AND hremployee_master.appointeddate " & cboTocondition.SelectedItem.Text & " '" & eZeeDate.convertDate(mdtFromDate) & "' "
                End If
            End If

            If radProbationDate.Checked Then
                strSearching &= "AND Convert(char(8),EPROB.probation_from_date,112) >= '" & eZeeDate.convertDate(dtpFromDate.GetDate.Date) & "' AND Convert(char(8),EPROB.probation_to_date,112) <= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            End If

            If radConfirmationDate.Checked Then
                strSearching &= "AND Convert(char(8),ECNF.confirmation_date,112) >= '" & eZeeDate.convertDate(dtpFromDate.GetDate.Date) & "' AND Convert(char(8),ECNF.confirmation_date,112) <= '" & eZeeDate.convertDate(dtpToDate.GetDate.Date) & "' "
            End If


            'Pinkal (03-Mar-2020) -- Start
            'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.
            'Dim mstrEmployeIds As String = objAssignOT.GetAssignedEmployeeIds()
            'If mstrEmployeIds.Trim.Length > 0 Then
            '    strSearching &= "AND hremployee_master.employeeunkid not in (" & mstrEmployeIds & ")"
            'End If
            'Pinkal (03-Mar-2020) -- End



            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearching &= "AND " & mstrAdvanceFilter
            End If

            If strSearching.Trim.Length > 0 Then
                strSearching = strSearching.Trim.Substring(3)
            End If


            'Pinkal (29-Apr-2020) -- Start
            'Error  -  Solved Error when User Filter anything from list screen and search it was giving error.

            Dim StrCheck_Fields As String = String.Empty
            StrCheck_Fields = clsEmployee_Master.EmpColEnum.Col_Code & "," & clsEmployee_Master.EmpColEnum.Col_Employee_Name & "," & clsEmployee_Master.EmpColEnum.Col_Station & "," & clsEmployee_Master.EmpColEnum.Col_Dept_Group & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Department & "," & clsEmployee_Master.EmpColEnum.Col_Section_Group & "," & clsEmployee_Master.EmpColEnum.Col_Section & "," & clsEmployee_Master.EmpColEnum.Col_Unit_Group & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Unit & "," & clsEmployee_Master.EmpColEnum.Col_Team & "," & clsEmployee_Master.EmpColEnum.Col_Job_Group & "," & clsEmployee_Master.EmpColEnum.Col_Job & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Class_Group & "," & clsEmployee_Master.EmpColEnum.Col_Class & "," & clsEmployee_Master.EmpColEnum.Col_Grade_Group & "," & clsEmployee_Master.EmpColEnum.Col_Grade_Level & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Grade & "," & clsEmployee_Master.EmpColEnum.Col_Cost_Center & "," & clsEmployee_Master.EmpColEnum.Col_Employement_Type & "," & clsEmployee_Master.EmpColEnum.Col_Gender_Type & _
                                       "," & clsEmployee_Master.EmpColEnum.Col_Appointed_Date & "," & clsEmployee_Master.EmpColEnum.Col_Cofirmation_Date & "," & clsEmployee_Master.EmpColEnum.Col_Probation_From_Date & "," & clsEmployee_Master.EmpColEnum.Col_Probation_To_Date



            dsList = objEmployee.GetListForDynamicField(StrCheck_Fields, CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
                                                                            , dtFromDate.Date, dtToDate.Date, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")), "Employee" _
                                                                            , mintEmployeeId, False, strSearching, False, False, False, True, Nothing, True)

            'dsList = objEmployee.GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
            '                                         , dtFromDate.Date, dtToDate.Date, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp")) _
            '                                         , "Employee", False, mintEmployeeId, False, strSearching, False, True, True)


            'Pinkal (29-Apr-2020) -- End

            mdtEmployee = dsList.Tables("Employee")

            If mdtEmployee.Columns.Contains("ischecked") = False Then
                Dim dcColumn As New DataColumn("ischecked", Type.GetType("System.Boolean"))
                dcColumn.DefaultValue = False
                mdtEmployee.Columns.Add(dcColumn)
            End If


            'Pinkal (03-Mar-2020) -- Start
            'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.
            Dim mstrEmployeIds As String = objAssignOT.GetAssignedEmployeeIds()
            If mstrEmployeIds.Trim.Length > 0 Then
                mdtEmployee = New DataView(mdtEmployee, "employeeunkid not in (" & mstrEmployeIds & ")", "", DataViewRowState.CurrentRows).ToTable()
            End If
            'Pinkal (03-Mar-2020) -- End

            dgvEmployee.AutoGenerateColumns = False
            dgvEmployee.DataSource = mdtEmployee
            dgvEmployee.DataBind()

            If radExpYear.Checked = True Then
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhAppointedDate", False, True)).Visible = True
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhConfirmationDate", False, True)).Visible = False
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationFromDate", False, True)).Visible = False
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationToDate", False, True)).Visible = False

            ElseIf radAppointedDate.Checked = True Then
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhAppointedDate", False, True)).Visible = True
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhConfirmationDate", False, True)).Visible = False
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationFromDate", False, True)).Visible = False
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationToDate", False, True)).Visible = False

            ElseIf radProbationDate.Checked = True Then
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhAppointedDate", False, True)).Visible = False
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhConfirmationDate", False, True)).Visible = False
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationFromDate", False, True)).Visible = True
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationToDate", False, True)).Visible = True

            ElseIf radConfirmationDate.Checked = True Then
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhAppointedDate", False, True)).Visible = False
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhConfirmationDate", False, True)).Visible = True
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationFromDate", False, True)).Visible = False
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationToDate", False, True)).Visible = False

            Else
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhAppointedDate", False, True)).Visible = True
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhConfirmationDate", False, True)).Visible = False
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationFromDate", False, True)).Visible = False
                dgvEmployee.Columns(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationToDate", False, True)).Visible = False
            End If


            lblEmployeeCount.Text = Language.getMessage(mstrModuleName, 5, "Employee Count : ") & " " & dgvEmployee.Items.Count

            If radAppointedDate.Checked OrElse radConfirmationDate.Checked OrElse radProbationDate.Checked Then
                lblValue.Text = dtpFromDate.GetDate.Date & " - " & dtpToDate.GetDate.Date
            Else
                If mdtFromDate.Date = Nothing Then mdtFromDate = ConfigParameter._Object._CurrentDateAndTime.Date
                If mdtToDate.Date = Nothing Then mdtToDate = ConfigParameter._Object._CurrentDateAndTime.Date
                lblValue.Text = mdtFromDate.Date & " - " & mdtToDate.Date
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objEmployee = Nothing
            objAssignOT = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub

    Private Sub GetValue()
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objAssignOT As New clsassignemp_ot
        'Pinkal (03-Sep-2020) -- End
        Try
            objAssignOT._Tnaemployeeotunkid = mintTnAEmployeeOTunkid
            mintEmployeeId = objAssignOT._Employeeunkid
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Finally
            objAssignOT = Nothing
            'Pinkal (03-Sep-2020) -- End
        End Try
    End Sub


    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Private Sub SetValue()
    Private Sub SetValue(ByVal objAssignOT As clsassignemp_ot)
        'Pinkal (03-Sep-2020) -- End
        Try
            If mintTnAEmployeeOTunkid > 0 Then
                objAssignOT._Tnaemployeeotunkid = mintTnAEmployeeOTunkid
            End If
            objAssignOT._Userunkid = CInt(Session("UserId"))
            objAssignOT._WebFormName = mstrModuleName
            objAssignOT._WebClientIP = CStr(Session("IP_ADD"))
            objAssignOT._WebHostName = CStr(Session("HOST_NAME"))
            objAssignOT._IsFromWeb = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetValue()
        Try
            dtpFromDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpToDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            cboGender.SelectedValue = CStr(0)
            txtFromYear.Text = "0"
            txtFromMonth.Text = "0"
            txtToYear.Text = "0"
            txtToMonth.Text = "0"
            cboFromcondition.SelectedValue = CStr(0)
            cboTocondition.SelectedValue = CStr(0)
            mstrAdvanceFilter = ""
            radExpYear.Checked = False
            radAppointedDate.Checked = False
            radProbationDate.Checked = False
            radConfirmationDate.Checked = False
            pnl_FromYear.Enabled = True
            pnl_ToYear.Enabled = True
            pnl_Date.Enabled = True

            lblValue.Text = dtpFromDate.GetDate.Date & " - " & dtpToDate.GetDate.Date
            lblEmployeeCount.Text = Language.getMessage(mstrModuleName, 5, "Employee Count : ") & "0"

            If mdtEmployee IsNot Nothing Then mdtEmployee.Rows.Clear()
            dgvEmployee.DataSource = mdtEmployee
            dgvEmployee.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If radAppointedDate.Checked = True Then
                If dtpToDate.GetDate.Date < dtpFromDate.GetDate.Date Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, To date cannot ne less than From date."), Me)
                    Exit Sub
                End If
            ElseIf radExpYear.Checked = True Then
                If (CInt(txtFromYear.Text) > 0 Or CInt(txtFromMonth.Text) > 0) And CInt(cboFromcondition.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 3, "From Condition is compulsory information.Please Select From Condition."), Me)
                    cboFromcondition.Focus()
                    Exit Sub
                ElseIf (CInt(txtToYear.Text) > 0 Or CInt(txtToMonth.Text) > 0) And CInt(cboTocondition.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 4, "To Condition is compulsory information.Please Select To Condition."), Me)
                    cboTocondition.Focus()
                    Exit Sub
                End If
            End If

            Call FillList()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        'Pinkal (03-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
        Dim objAssignOT As clsassignemp_ot
        'Pinkal (03-Sep-2020) -- End
        Try

            'Pinkal (10-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion for NMB.

            'If mdtEmployee IsNot Nothing Then
            '    If mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischecked") = True).Count <= 0 Then
            '        DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Check atleast One Employee from the List."), Me)
            '        dgvEmployee.Focus()
            '        Exit Sub
            '    End If
            'End If

            ' Dim dtTable As DataTable = mdtEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischecked") = True).ToList().CopyToDataTable()

            Dim dgRow As IEnumerable(Of DataGridItem) = Nothing
            dgRow = dgvEmployee.Items.Cast(Of DataGridItem).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True)

            If dgRow Is Nothing OrElse dgRow.Count <= 0 Then
                DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information.Please Check atleast One Employee from the List."), Me)
                dgvEmployee.Focus()
                Exit Sub
            End If

            Dim dtTable As DataTable = mdtEmployee.Clone
            Dim xCount As Integer = -1
            For i As Integer = 0 To dgRow.Count - 1
                xCount = i
                Dim dRow As DataRow() = mdtEmployee.Select("employeeunkid = " & CInt(dgvEmployee.DataKeys(dgRow(xCount).ItemIndex)))
                If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
                    dtTable.ImportRow(dRow(0))
                End If
            Next
            'Pinkal (10-Jan-2020) -- End


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'Call SetValue()
            objAssignOT = New clsassignemp_ot
            Call SetValue(objAssignOT)
            'Pinkal (03-Sep-2020) -- End

            If mintTnAEmployeeOTunkid > 0 Then
                blnFlag = objAssignOT.Update()
            Else
                blnFlag = objAssignOT.Insert(dtTable)
            End If

            If blnFlag = False Then
                If objAssignOT._Message <> "" Then
                    DisplayMessage.DisplayMessage(objAssignOT._Message, Me)
                    Exit Sub
                End If
            Else
                If mintTnAEmployeeOTunkid > 0 Then
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 7, "Employee(s) OT Assignment updated successfully."), Me.Page, Session("rootpath").ToString & "TnA/OT_Requisition/OT_EmployeeAssignmentList.aspx")
                Else
                    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Employee(s) OT Assignment saved successfully."), Me.Page) ', Session("rootpath").ToString & "TnA/OT_Requisition/OT_EmployeeAssignmentList.aspx")
                End If
                Call ResetValue()
                Session.Remove("tnaemployeeotunkid")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objAssignOT = Nothing
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session.Remove("tnaemployeeotunkid")
            Response.Redirect(Session("rootpath").ToString & "TnA/OT_Requisition/OT_EmployeeAssignmentList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " TextBox's Events "

    Protected Sub txtFromYear_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFromYear.TextChanged
        Try
            mdtFromDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(txtFromYear.Text) * -1).AddMonths(CInt(txtFromYear.Text) * -1)
            txtToYear.Text = txtFromYear.Text
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtToYear_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtToYear.TextChanged
        Try
            mdtToDate = ConfigParameter._Object._CurrentDateAndTime.Date.AddYears(CInt(txtToYear.Text) * -1).AddMonths(CInt(txtToYear.Text) * -1)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (29-Apr-2020) -- Start
    'Error  -  Solved Error when User Filter anything from list screen and search it was giving error.
    'Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
    '    Try
    '        If mdtEmployee IsNot Nothing Then
    '            Dim dvEmployee As DataView = mdtEmployee.DefaultView
    '            If dvEmployee.Table.Rows.Count > 0 Then
    '                If txtSearch.Text.Trim.Length > 0 Then
    '                    'dvEmployee.RowFilter = "employeecode like '%" & txtSearch.Text.Trim & "%' OR name like '%" & txtSearch.Text.Trim & "%' "
    '                    dvEmployee.RowFilter = "Code like '%" & txtSearch.Text.Trim & "%' OR [Employee Name] like '%" & txtSearch.Text.Trim & "%' "
    '                End If
    '                If mstrAdvanceFilter.Trim.Length > 0 Then
    '                    dvEmployee.RowFilter &= IIf(dvEmployee.RowFilter.Trim.Length > 0, " AND ", "").ToString & mstrAdvanceFilter
    '                End If
    '                dgvEmployee.DataSource = dvEmployee.ToTable
    '                dgvEmployee.DataBind()

    '                'Pinkal (14-Nov-2019) -- Start
    '                'Enhancement NMB - Working On OT & Imprest Enhancement for NMB.
    '                lblEmployeeCount.Text = Language.getMessage(mstrModuleName, 5, "Employee Count : ") & " " & dgvEmployee.Items.Count
    '                'Pinkal (14-Nov-2019) -- End
    '            End If
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Pinkal (29-Apr-2020) -- End

#End Region

#Region " RadioButton's Events "

    Protected Sub radExpYear_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radExpYear.CheckedChanged, _
                                                                                                         radAppointedDate.CheckedChanged, _
                                                                                                         radProbationDate.CheckedChanged, _
                                                                                                         radConfirmationDate.CheckedChanged
        Try
            If CType(sender, RadioButton).ID.ToUpper = "RADEXPYEAR" Then
                pnl_FromYear.Enabled = True
                pnl_ToYear.Enabled = True
                pnl_Date.Enabled = False

            ElseIf CType(sender, RadioButton).ID.ToUpper = "RADAPPOINTEDDATE" Then
                pnl_Date.Enabled = True
                pnl_FromYear.Enabled = False
                pnl_ToYear.Enabled = False

            ElseIf CType(sender, RadioButton).ID.ToUpper = "RADPROBATIONDATE" Then
                pnl_Date.Enabled = True
                pnl_FromYear.Enabled = False
                pnl_ToYear.Enabled = False

            ElseIf CType(sender, RadioButton).ID.ToUpper = "RADCONFIRMATIONDATE" Then
                pnl_Date.Enabled = True
                pnl_FromYear.Enabled = False
                pnl_ToYear.Enabled = False

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " LinkButton's Events "

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " DataGrid's Events "

    Protected Sub dgvEmployee_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvEmployee.ItemDataBound
        Try
            SetDateFormat()

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgColhEmployee", False, True)).Text = info1.ToTitleCase(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgColhEmployee", False, True)).Text.ToLower())
                e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhjob", False, True)).Text = info1.ToTitleCase(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhjob", False, True)).Text.ToLower())

                If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhAppointedDate", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhAppointedDate", False, True)).Text.Trim <> "&nbsp;" Then
                    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhAppointedDate", False, True)).Text = eZeeDate.convertDate(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhAppointedDate", False, True)).Text).ToShortDateString()
                End If
                If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhConfirmationDate", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhConfirmationDate", False, True)).Text.Trim <> "&nbsp;" Then
                    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhConfirmationDate", False, True)).Text = eZeeDate.convertDate(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhConfirmationDate", False, True)).Text).ToShortDateString()
                End If
                If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationFromDate", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationFromDate", False, True)).Text.Trim <> "&nbsp;" Then
                    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationFromDate", False, True)).Text = eZeeDate.convertDate(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationFromDate", False, True)).Text).ToShortDateString()
                End If
                If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationToDate", False, True)).Text.Trim <> "" AndAlso e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationToDate", False, True)).Text.Trim <> "&nbsp;" Then
                    e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationToDate", False, True)).Text = eZeeDate.convertDate(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvEmployee, "dgcolhProbationToDate", False, True)).Text).ToShortDateString()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayMessage("dgvEmployee_ItemDataBound :- " & ex.Message, Me)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "

    'Pinkal (10-Jan-2020) -- Start
    'Enhancements -  Working on OT Requisistion for NMB.

    'Protected Sub ChkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim chkBox As CheckBox = CType(sender, CheckBox)
    '        Dim dvEmployee As DataView = mdtEmployee.DefaultView


    '        For i As Integer = 0 To dvEmployee.ToTable.Rows.Count - 1
    '            Dim gvRow As DataGridItem = dgvEmployee.Items(i)
    '            CType(gvRow.FindControl("ChkgvSelect"), CheckBox).Checked = chkBox.Checked
    '            Dim dRow() As DataRow = mdtEmployee.Select("employeeunkid = '" & CInt(dgvEmployee.DataKeys(gvRow.ItemIndex)) & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("ischecked") = chkBox.Checked
    '            End If
    '            dvEmployee.Table.AcceptChanges()
    '        Next
    '        mdtEmployee = dvEmployee.Table()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub ChkgvSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim chkBox As CheckBox = CType(sender, CheckBox)
    '        Dim item As DataGridItem = CType(chkBox.NamingContainer, DataGridItem)

    '        Dim drRow() As DataRow = mdtEmployee.Select("employeeunkid = " & CInt(dgvEmployee.DataKeys(item.ItemIndex)))
    '        If drRow.Length > 0 Then
    '            drRow(0).Item("ischecked") = chkBox.Checked
    '        End If
    '        mdtEmployee.AcceptChanges()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Pinkal (10-Jan-2020) -- End

#End Region


    Private Sub SetControlCaptions()
        Try
            Language.setLanguage(mstrModuleName)
            Language._Object.setCaption(mstrModuleName, Me.Title)
            Language._Object.setCaption(Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Language._Object.setCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Language._Object.setCaption(Me.radExpYear.ID, Me.radExpYear.Text)
            Language._Object.setCaption(Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Language._Object.setCaption(Me.radAppointedDate.ID, Me.radAppointedDate.Text)
            Language._Object.setCaption(Me.radProbationDate.ID, Me.radProbationDate.Text)
            Language._Object.setCaption(Me.radConfirmationDate.ID, Me.radConfirmationDate.Text)
            Language._Object.setCaption(Me.lblFromYear.ID, Me.lblFromYear.Text)
            Language._Object.setCaption(Me.lblToYear.ID, Me.lblToYear.Text)
            Language._Object.setCaption(Me.lblFromMonth.ID, Me.lblFromMonth.Text)
            Language._Object.setCaption(Me.lblToMonth.ID, Me.lblToMonth.Text)
            Language._Object.setCaption(Me.lblFrom.ID, Me.lblFrom.Text)
            Language._Object.setCaption(Me.lblEmployeeCount.ID, Me.lblEmployeeCount.Text)
            Language._Object.setCaption(Me.lblFromDate.ID, Me.lblFromDate.Text)
            Language._Object.setCaption(Me.lblToDate.ID, Me.lblToDate.Text)
            Language._Object.setCaption(Me.lblGender.ID, Me.lblGender.Text)

            Language._Object.setCaption(Me.btnSearch.ID, Me.btnSearch.Text)
            Language._Object.setCaption(Me.btnReset.ID, Me.btnReset.Text)
            Language._Object.setCaption(Me.btnSave.ID, Me.btnSave.Text)
            Language._Object.setCaption(Me.btnClose.ID, Me.btnClose.Text)

            Language._Object.setCaption(dgvEmployee.Columns(1).FooterText, dgvEmployee.Columns(1).HeaderText)
            Language._Object.setCaption(dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)
            Language._Object.setCaption(dgvEmployee.Columns(3).FooterText, dgvEmployee.Columns(3).HeaderText)
            Language._Object.setCaption(dgvEmployee.Columns(4).FooterText, dgvEmployee.Columns(4).HeaderText)
            Language._Object.setCaption(dgvEmployee.Columns(5).FooterText, dgvEmployee.Columns(5).HeaderText)
            Language._Object.setCaption(dgvEmployee.Columns(6).FooterText, dgvEmployee.Columns(6).HeaderText)
            Language._Object.setCaption(dgvEmployee.Columns(7).FooterText, dgvEmployee.Columns(7).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            Language.setLanguage(mstrModuleName)
            Me.Title = Language._Object.getCaption(mstrModuleName, Me.Title)
            Me.lblDetialHeader.Text = Language._Object.getCaption(Me.lblDetialHeader.ID, Me.lblDetialHeader.Text)
            Me.lblPageHeader.Text = Language._Object.getCaption(Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            Me.radExpYear.Text = Language._Object.getCaption(Me.radExpYear.ID, Me.radExpYear.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            Me.radAppointedDate.Text = Language._Object.getCaption(Me.radAppointedDate.ID, Me.radAppointedDate.Text)
            Me.radProbationDate.Text = Language._Object.getCaption(Me.radProbationDate.ID, Me.radProbationDate.Text)
            Me.radConfirmationDate.Text = Language._Object.getCaption(Me.radConfirmationDate.ID, Me.radConfirmationDate.Text)
            Me.lblFromYear.Text = Language._Object.getCaption(Me.lblFromYear.ID, Me.lblFromYear.Text)
            Me.lblToYear.Text = Language._Object.getCaption(Me.lblToYear.ID, Me.lblToYear.Text)
            Me.lblFromMonth.Text = Language._Object.getCaption(Me.lblFromMonth.ID, Me.lblFromMonth.Text)
            Me.lblToMonth.Text = Language._Object.getCaption(Me.lblToMonth.ID, Me.lblToMonth.Text)
            Me.lblFrom.Text = Language._Object.getCaption(Me.lblFrom.ID, Me.lblFrom.Text)
            Me.lblEmployeeCount.Text = Language._Object.getCaption(Me.lblEmployeeCount.ID, Me.lblEmployeeCount.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.ID, Me.lblFromDate.Text)
            Me.lblToDate.Text = Language._Object.getCaption(Me.lblToDate.ID, Me.lblToDate.Text)
            Me.lblGender.Text = Language._Object.getCaption(Me.lblGender.ID, Me.lblGender.Text)

            Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            dgvEmployee.Columns(1).HeaderText = Language._Object.getCaption(dgvEmployee.Columns(1).FooterText, dgvEmployee.Columns(1).HeaderText)
            dgvEmployee.Columns(2).HeaderText = Language._Object.getCaption(dgvEmployee.Columns(2).FooterText, dgvEmployee.Columns(2).HeaderText)
            dgvEmployee.Columns(3).HeaderText = Language._Object.getCaption(dgvEmployee.Columns(3).FooterText, dgvEmployee.Columns(3).HeaderText)
            dgvEmployee.Columns(4).HeaderText = Language._Object.getCaption(dgvEmployee.Columns(4).FooterText, dgvEmployee.Columns(4).HeaderText)
            dgvEmployee.Columns(5).HeaderText = Language._Object.getCaption(dgvEmployee.Columns(5).FooterText, dgvEmployee.Columns(5).HeaderText)
            dgvEmployee.Columns(6).HeaderText = Language._Object.getCaption(dgvEmployee.Columns(6).FooterText, dgvEmployee.Columns(6).HeaderText)
            dgvEmployee.Columns(7).HeaderText = Language._Object.getCaption(dgvEmployee.Columns(7).FooterText, dgvEmployee.Columns(7).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information.Please Check atleast One Employee from the List.")
            Language.setMessage(mstrModuleName, 2, "Sorry, To date cannot ne less than From date.")
            Language.setMessage(mstrModuleName, 3, "From Condition is compulsory information.Please Select From Condition.")
            Language.setMessage(mstrModuleName, 4, "To Condition is compulsory information.Please Select To Condition.")
            Language.setMessage(mstrModuleName, 5, "Employee Count :")
            Language.setMessage(mstrModuleName, 6, "Employee(s) OT Assignment saved successfully.")
            Language.setMessage(mstrModuleName, 7, "Employee(s) OT Assignment updated successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
