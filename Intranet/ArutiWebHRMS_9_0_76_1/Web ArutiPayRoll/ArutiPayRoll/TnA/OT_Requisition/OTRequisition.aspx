﻿<%@ Page Title="OT Requisition List" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="OTRequisition.aspx.vb" Inherits="OT_OTRequisition" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
        
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="OT Requisition List"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblCaption" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <div class="row2">
                                        <%-- <div style="width: 10%;" class="ib">
                                            <asp:Label ID="lblListPeriod" runat="server" Text="Period" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 25%;" class="ib">
                                            <asp:DropDownList ID="cboListPeriod" runat="server" AutoPostBack="true" Width="225px">
                                            </asp:DropDownList>
                                        </div>--%>
                                       <div style="width: 14%;" class="ib">
                                            <asp:Label ID="lblListOTReqFromDate" runat="server" Text="Req. From Date " Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 15%;" class="ib">
                                            <uc1:DateCtrl ID="dtpListOTReqFromDate" runat="server" AutoPostBack="false" />
                                        </div>
                                           <div style="width: 8%;" class="ib">
                                            <asp:Label ID="lblListEmployee" runat="server" Text="Employee" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 25%;" class="ib">
                                            <asp:DropDownList ID="cboListEmployee" runat="server" Width="260px">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 14%;" class="ib">
                                            <asp:Label ID="lblListOTReqToDate" runat="server" Text="Req. To Date " Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 15%;" class="ib">
                                            <uc1:DateCtrl ID="dtpListOTReqToDate" runat="server" AutoPostBack="false" />
                                        </div>
                                        <div style="width: 8%;" class="ib">
                                            <asp:Label ID="lblListStatus" runat="server" Text="Status" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 25%;" class="ib">
                                            <asp:DropDownList ID="cboListStatus" runat="server" Width="260px">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="btn-default">
                                        <asp:Button ID="btnListNew" runat="server" Text="New" CssClass="btndefault" />
                                        <asp:Button ID="btnListSearch" runat="server" Text="Search" CssClass="btndefault" />
                                        <asp:Button ID="btnListReset" runat="server" Text="Reset" CssClass="btndefault" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="scrollable-container" style="width: 100%; height: 350px; overflow: auto">
                            <asp:GridView ID="gvOTRequisitionList" runat="server" DataKeyNames="otrequisitiontranunkid, empcodename, issubmit_approval"
                                AutoGenerateColumns="False" Width="120%" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                RowStyle-CssClass="griviewitem" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                ShowFooter="false">
                                <Columns>
                                    <%--'Pinkal (23-Nov-2019) -- Start
                                      'Enhancement NMB - Working On OT Enhancement for NMB.--%>
                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhedit" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Width="35px" HeaderStyle-Width="35px">
                                        <ItemTemplate>
                                            <span class="gridiconbc" style="padding: 0 10px; display: block">
                                                <asp:LinkButton ID="lnkedit" runat="server" CssClass="gridedit" CommandArgument='<%#Eval("otrequisitiontranunkid")%>'
                                                    ToolTip="Edit" CommandName="Change" OnClick="lnkedit_Click">
                                                </asp:LinkButton>
                                            </span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" FooterText="colhdelete" ItemStyle-HorizontalAlign="Center"
                                        ItemStyle-Width="35px" HeaderStyle-Width="35px">
                                        <ItemTemplate>
                                            <span class="gridiconbc" style="padding: 0 10px; display: block">
                                                <asp:LinkButton ID="lnkdelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                    CommandArgument='<%#Eval("otrequisitiontranunkid")%>' CommandName="Remove" OnClick="lnkdelete_Click">
                                                </asp:LinkButton>
                                            </span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--'Pinkal (23-Nov-2019) -- End    --%>
                                    <asp:BoundField HeaderText="Employee" DataField="empcodename" ItemStyle-VerticalAlign="Top"
                                        Visible="false" FooterText="colhotreqemp" />
                                    <asp:BoundField HeaderText="Request Date" DataField="requestdate" ItemStyle-VerticalAlign="Top"
                                        FooterText="colhotrequestdate" />
                                    <asp:BoundField HeaderText="Planned Start Time" DataField="plannedstart_time" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="150px" FooterText="colhotreqplannedstart_time" />
                                    <asp:BoundField HeaderText="Planned End Time" DataField="plannedend_time" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="150px" FooterText="colhotreqplannedend_time" />
                                    <asp:BoundField HeaderText="Planned OT Hours" DataField="PlannedworkedDuration" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="130px" FooterText="colhotreqplannedot_hours" />
                                    <asp:BoundField HeaderText="Actual Start Time" DataField="actualstart_time" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="150px" FooterText="colhotreqactualstart_time" />
                                    <asp:BoundField HeaderText="Actual End Time" DataField="actualend_time" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="150px" FooterText="colhotreqactualend_time" />
                                    <asp:BoundField HeaderText="Actual OT Hours" DataField="ActualworkedDuration" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-Width="130px" FooterText="colhotreqactualot_hours" />
                                    <asp:BoundField HeaderText="Status" DataField="status" ItemStyle-VerticalAlign="Top"
                                        FooterText="colhotreqstatus" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                    <cc1:ModalPopupExtender ID="popupOTRequisition" BackgroundCssClass="modalBackground"
                        TargetControlID="lblEmployee" runat="server" PopupControlID="pnlOTRequisition"
                        DropShadow="false" CancelControlID="lblPageHeader">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlOTRequisition" runat="server" CssClass="newpopup" Style="display: none;
                        width: 750px">
                        <div class="panel-primary" style="margin-bottom: 0px">
                            <div class="panel-heading">
                                <asp:Label ID="lbladdeditPageHeader" Text="OT Requisition Add/Edit" runat="server" />
                            </div>
                            <div class="panel-body">
                                <div id="Div20" class="panel-body-default">
                                    <div class="row2">
                                            <div style="width: 18%;" class="ib">
                                                <asp:Label ID="lblRequestDate" runat="server" Text="Request Date" Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 25%;" class="ib">
                                                <uc1:DateCtrl ID="dtpRequestDate" runat="server" AutoPostBack="True" />
                                            </div>
                                        <div style="width: 16%;" class="ib">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee" Width="100%"></asp:Label>
                                       </div>
                                        <div style="width: 28%;" class="ib">
                                            <asp:DropDownList ID="cboEmployee" runat="server" Width="250px" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 18%;" class="ib">
                                            <asp:Label ID="lblPlannedStartTime" runat="server" Text="Planned Start Time" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 25%;" class="ib">
                                            <div style="width: 30%;" class="ib">
                                                <asp:DropDownList ID="cboPlannedStartTimehr" runat="server" Width="60px">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 1%;" class="ib">
                                                <asp:Label ID="lblPlannedStarTimecolon" runat="server" Text=" : "></asp:Label></div>
                                            <div style="width: 30%;" class="ib">
                                                <asp:DropDownList ID="cboPlannedStartTimemin" runat="server" Width="60px">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div style="width: 16%;" class="ib">
                                            <asp:Label ID="lblPlannedEndTime" runat="server" Text="Planned End Time" Width="100%"></asp:Label>
                                        </div>
                                        <div style="width: 25%; margin: 0" class="ibwm">
                                            <div style="width: 30%;" class="ib">
                                                <asp:DropDownList ID="cboPlannedEndTimehr" runat="server" Width="60px">
                                                </asp:DropDownList>
                                            </div>
                                            <div style="width: 1%;" class="ib">
                                                <asp:Label ID="lblPlannedEndTimecolon" runat="server" Text=" : "></asp:Label></div>
                                            <div style="width: 30%;" class="ib">
                                                <asp:DropDownList ID="cboPlannedEndTimemin" runat="server" Width="60px">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div style="width: 18%;" class="ib">
                                            <asp:Label ID="lblRequestReason" runat="server" Text="Reason For Request"></asp:Label></div>
                                        <div style="width: 80%;" class="ibwm no-padding">
                                            <asp:TextBox ID="txtRequestReason" runat="server" Width="99%" TextMode="MultiLine"></asp:TextBox></div>
                                    </div>
                                    <asp:Panel ID="pnlActualTime" runat="server">
                                        <div class="row2">
                                            <div style="width: 18%;" class="ib">
                                                <asp:Label ID="lblActualStartTime" runat="server" Text="Actual Start Time" Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 25%; margin: 0" class="ibwm">
                                                <div style="width: 30%;" class="ib">
                                                    <asp:DropDownList ID="cboActualStartTimehr" runat="server" Width="60px">
                                                    </asp:DropDownList>
                                                </div>
                                                <div style="width: 1%;" class="ib">
                                                    <asp:Label ID="lblActualStarTimecolon" runat="server" Text=" : "></asp:Label>
                                                </div>
                                                <div style="width: 30%;" class="ib">
                                                    <asp:DropDownList ID="cboActualStartTimemin" runat="server" Width="60px">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div style="width: 18%;" class="ib">
                                                <asp:Label ID="lblActualEndTime" runat="server" Text="Actual End Time" Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 25%; margin: 0" class="ibwm">
                                                <div style="width: 30%;" class="ib">
                                                    <asp:DropDownList ID="cboActualEndTimehr" runat="server" Width="60px">
                                                    </asp:DropDownList>
                                                </div>
                                                <div style="width: 1%;" class="ib">
                                                    <asp:Label ID="lblActualEndTimecolon" runat="server" Text=" : "></asp:Label>
                                                </div>
                                                <div style="width: 30%;" class="ib">
                                                    <asp:DropDownList ID="cboActualEndTimemin" runat="server" Width="60px">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div style="width: 18%;" class="ib ">
                                                <asp:Label ID="lblReasonForAdjustment" runat="server" Text="Reason For Adjustment"></asp:Label></div>
                                            <div style="width: 80%;" class="ibwm no-padding">
                                                <asp:TextBox ID="txtReasonForAdjustment" runat="server" Width="99%" TextMode="MultiLine"></asp:TextBox></div>
                                        </div>
                                    </asp:Panel>
                                    <div class="btn-default">
                                        <div class="row2" style="display: flex; justify-content: space-between;">
                                            <div>
                                                <b>
                                                    <asp:Label ID="lblPeriodStartDate" runat="server" Text="Start Date: " Style="text-align: Center;"></asp:Label>
                                                </b>
                                                <br />
                                                <b>
                                                    <asp:Label ID="lblPeriodEndDate" runat="server" Text="End Date: " Style="text-align: Center;"></asp:Label>
                                                </b>
                                            </div>
                                            <div>
                                                <asp:Button ID="btnSaveOTRequiApprover" runat="server" CssClass="btndefault" Text="Save"
                                                    Style="margin-left: 36px;" />
                                                <asp:Button ID="btnSaveSubmitOTRequiApprover" runat="server" CssClass="btndefault"
                                                    Text="Save And Submit" />
                                                <asp:Button ID="btnCloseOTRequiApprover" runat="server" CssClass="btndefault" Text="Close" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%--'Pinkal (23-Nov-2019) -- Start
                     'Enhancement NMB - Working On OT Enhancement for NMB.--%>
                    <uc7:Confirmation ID="popupConfirmationForOTCap" Title="" runat="server" Message="" />
                    <%--  'Pinkal (23-Nov-2019) -- End--%>
                    <uc7:Confirmation ID="popupConfirmationOTRequiReason" Title="" runat="server" Message="" />
                    <ucDel:DeleteReason ID="popupDeleteOTRequiReason" runat="server" Title="" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
