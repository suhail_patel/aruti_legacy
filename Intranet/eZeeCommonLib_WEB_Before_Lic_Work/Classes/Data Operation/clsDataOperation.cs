//************************************************************************************************************************************
//Class Name : clsDataOperation.vb
//Purpose    : All Data Level Opration like addParameter, Execute Procedure, Insert Query, Update Query, Delete Query
//Date       : 08 Feb 2008
//Written By : Jitendra N Jariwala
//Modified   : Naimish
//************************************************************************************************************************************

using System;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.IO;
using System.Configuration;
//using System.Windows.Forms;
using System.Drawing;
using System.Web;
using System.Web.SessionState;

namespace eZeeCommonLib
{
    public class clsDataOperation : IDisposable
    {

        private SqlCommand oCmd;
        public string ErrorMessage = "";
        public string ErrorNumber = "0";
        private string cnnstr;
        private bool mblnLocalBindTran = true;
        public int mintFolioTranUnkid = 0; 
        
        /**********/
        private static string strDatabaseName = "hrmsConfiguration";
        public System.Data.SqlClient.SqlConnection gConn;
        public System.Data.SqlClient.SqlTransaction gsqlTransaction;
        public bool gblnBindTransaction = false;
               
        /**********/
        
        public System.Data.ConnectionState ConnState
        {
            get
            {

                //if (modGlobal.gConn == null)
                //{ return ConnectionState.Closed;}
                //else
                //{ return modGlobal.gConn.State; }
                if (gConn == null)
                { return ConnectionState.Closed; }
                else
                { return gConn.State; }
            }
        }

        public clsDataOperation()
        {
            oCmd = new SqlCommand();
            //try
            //{
            //    /*
            //   if (gConn.State == ConnectionState.Closed | modGlobal.gConn.State == ConnectionState.Broken)               
            //    {
            //        modGlobal.gConn.Open();                   
            //    }
                              
            //    oCmd.Connection = modGlobal.gConn;
                                
            //    if (modGlobal.gblnBindTransaction == true)
            //    {
            //        oCmd.Transaction = modGlobal.gsqlTransaction;
            //    }                
            //     */
            //    // new clsDataOperation(true);
            //    cnnstr = Convert.ToString(ConfigurationManager.ConnectionStrings["paydb"].ConnectionString);
            //    //Anjan (21 Dec 2012)-Start
            //    string dbPwd = ConfigurationManager.AppSettings["dbpassword"].ToString();
            //    cnnstr = cnnstr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString());
            //    //Anjan (21 Dec 2012)-End

            //    if (HttpContext.Current.Session != null && HttpContext.Current.Session["mdbname"] != null)
            //    {
            //        if (HttpContext.Current.Session["mdbname"].ToString() != "hrmsConfiguration")
            //        {
            //            cnnstr = cnnstr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" + HttpContext.Current.Session["mdbname"].ToString() + ";");
            //        }
            //    }


            //    //Sohail (28 Oct 2013) --  Get Connection Object from Session Variable to Avoid frequent Connections and Memory Issue   -- START
            //    if (HttpContext.Current.Session != null && HttpContext.Current.Session["gConn"] != null)
            //    {
            //        gConn = (SqlConnection)HttpContext.Current.Session["gConn"];
            //    }
            //    //Sohail (28 Oct 2013) --  Get Connection Object from Session Variable to Avoid frequent Connections and Memory Issue   -- END
                              
                                
            //    if (gConn != null)
            //    {
            //        if (gConn.State == ConnectionState.Closed | gConn.State == ConnectionState.Broken)
            //        {
            //            gConn.ConnectionString = cnnstr;
            //            gConn.Open();

            //            //if (HttpContext.Current.Session != null && HttpContext.Current.Session["mdbname"] != null)
            //            //{
            //            //    if (HttpContext.Current.Session["mdbname"].ToString() != gConn.Database.ToString())
            //            //        gConn.ChangeDatabase(HttpContext.Current.Session["mdbname"].ToString());
            //            //}
            //        }

            //    }
            //    else
            //    {
            //        System.Data.SqlClient.SqlConnection.ClearAllPools();
            //        gConn = new System.Data.SqlClient.SqlConnection();
            //        gConn.ConnectionString = cnnstr;
            //        gConn.Open();

            //        //if (HttpContext.Current.Session != null && HttpContext.Current.Session["mdbname"] != null)
            //        //{
            //        //    if (HttpContext.Current.Session["mdbname"].ToString() != gConn.Database.ToString())
            //        //        gConn.ChangeDatabase(HttpContext.Current.Session["mdbname"].ToString());
            //        //}
            //    }
            //    oCmd.Connection = gConn;

            //}
            //catch (SqlException SqlEx)
            //{
            //    ErrorMessage = "SqlException:- " + SqlEx.Message;
            //    ErrorNumber = SqlEx.Number.ToString();
            //}
            //catch (Exception ex)
                        //{
            //    ErrorMessage = "Exception:- " + ex.Message;
                        //}
                    }
                    
        public clsDataOperation(Boolean blnIsWeb)
                {
            ////myconnection();
            ////return;

            oCmd = new SqlCommand();
            //try
            //{
            //    cnnstr = Convert.ToString(ConfigurationManager.ConnectionStrings["paydb"].ConnectionString);
            //    //Anjan (21 Dec 2012)-Start
            //    string dbPwd = ConfigurationManager.AppSettings["dbpassword"].ToString();
            //    cnnstr = cnnstr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString());
            //    //Anjan (21 Dec 2012)-End

                    //if (HttpContext.Current.Session != null && HttpContext.Current.Session["mdbname"] != null)
                    //{
            //        if (HttpContext.Current.Session["mdbname"].ToString() != "hrmsConfiguration")
            //        {
            //            cnnstr = cnnstr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" + HttpContext.Current.Session["mdbname"].ToString() + ";");
            //        }
            //    }


            //    //Sohail (28 Oct 2013) --  Get Connection Object from Session Variable to Avoid frequent Connections and Memory Issue   -- START
            //    if (HttpContext.Current.Session != null && HttpContext.Current.Session["gConn"] != null)
            //    {
            //        gConn = (SqlConnection)HttpContext.Current.Session["gConn"];
            //    }
            //    //Sohail (28 Oct 2013) --  Get Connection Object from Session Variable to Avoid frequent Connections and Memory Issue   -- END


            //    //if (modGlobal.gConn != null)
            //    if (gConn != null)
            //    {
            //        /* 
            //        if (modGlobal.gConn.State == ConnectionState.Closed | modGlobal.gConn.State == ConnectionState.Broken)
            //        {
            //            modGlobal.gConn.ConnectionString = cnnstr;
            //            modGlobal.gConn.Open();
            //        }
            //        */
            //        if (gConn.State == ConnectionState.Closed | gConn.State == ConnectionState.Broken)
            //        {
            //            gConn.ConnectionString = cnnstr;
            //            gConn.Open();

            //            //if (HttpContext.Current.Session != null && HttpContext.Current.Session["mdbname"] != null)
            //            //{
            //            //    if (HttpContext.Current.Session["mdbname"].ToString() != gConn.Database.ToString())
            //            //        gConn.ChangeDatabase(HttpContext.Current.Session["mdbname"].ToString());
            //            //}
            //        }

                    //}
            //    else
            //    {
            //        /*
            //        modGlobal.gConn = new System.Data.SqlClient.SqlConnection();
            //        modGlobal.gConn.ConnectionString = cnnstr;
            //        modGlobal.gConn.Open();
            //         */
            //        System.Data.SqlClient.SqlConnection.ClearAllPools();
            //        gConn = new System.Data.SqlClient.SqlConnection();
            //        gConn.ConnectionString = cnnstr;
            //        gConn.Open();

            //        //if (HttpContext.Current.Session != null && HttpContext.Current.Session["mdbname"] != null)
            //        //{
            //        //    if (HttpContext.Current.Session["mdbname"].ToString() != gConn.Database.ToString())
            //        //        gConn.ChangeDatabase(HttpContext.Current.Session["mdbname"].ToString());
            //        //}
            //    }
            //    //oCmd.Connection = modGlobal.gConn;
            //    oCmd.Connection = gConn;
            //}
            //catch (SqlException SqlEx)
            //{
            //    ErrorMessage = "SqlException:- " + SqlEx.Message;
            //    ErrorNumber = SqlEx.Number.ToString();
            //    throw SqlEx;
            //}
            //catch (Exception ex)
            //{
            //    ErrorMessage = "Exception:- " + ex.Message;
            //    throw ex;
            //}
        }
                
        //'Sohail (28 Oct 2013) - Check Connection while ExecuteQuery/NonQuery function called. -- START
        private void CheckConnection()
        {
            //oCmd = new SqlCommand();
            try
            {
                cnnstr = Convert.ToString(ConfigurationManager.ConnectionStrings["paydb"].ConnectionString);
                string dbPwd = ConfigurationManager.AppSettings["dbpassword"].ToString();
                cnnstr = cnnstr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString());

                if (HttpContext.Current.Session != null && HttpContext.Current.Session["mdbname"] != null)
                {
                    if (HttpContext.Current.Session["mdbname"].ToString() != "hrmsConfiguration")
                    {
                        cnnstr = cnnstr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" + HttpContext.Current.Session["mdbname"].ToString() + ";");
                    }
                }


                //Sohail (28 Oct 2013) --  Get Connection Object from Session Variable to Avoid frequent Connections and Memory Issue   -- START
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["gConn"] != null)
                {
                    gConn = (SqlConnection)HttpContext.Current.Session["gConn"];
                    //S.SANDEEP |17-MAR-2020| -- START
                    //ISSUE/ENHANCEMENT : WHEN DATABASE IS NOT CONFIGURATION WE ARE GETTING ERROR SO CHANGED THE CURRENT DATABASE WHICH IS IN SESSION
                    if (HttpContext.Current.Session["mdbname"] != null) //SANDEEP - HttpContext.Current.Session["mdbname"] IT WAS COME NULL
                    {
                        if (HttpContext.Current.Session["mdbname"].ToString() != "hrmsConfiguration")
                        {
                            gConn.ChangeDatabase(HttpContext.Current.Session["mdbname"].ToString());
                        }
                    }                  
                    //S.SANDEEP |17-MAR-2020| -- END
                }
                //Sohail (28 Oct 2013) --  Get Connection Object from Session Variable to Avoid frequent Connections and Memory Issue   -- END


                //if (modGlobal.gConn != null)
                if (gConn != null)
                {
                    /* 
                    if (modGlobal.gConn.State == ConnectionState.Closed | modGlobal.gConn.State == ConnectionState.Broken)
                    {
                        modGlobal.gConn.ConnectionString = cnnstr;
                        modGlobal.gConn.Open();
                    }
                    */
                    if (gConn.State == ConnectionState.Closed | gConn.State == ConnectionState.Broken)
                    {
                        gConn.ConnectionString = cnnstr;
                        gConn.Open();

                        //if (HttpContext.Current.Session != null && HttpContext.Current.Session["mdbname"] != null)
                        //{
                        //    if (HttpContext.Current.Session["mdbname"].ToString() != gConn.Database.ToString())
                        //        gConn.ChangeDatabase(HttpContext.Current.Session["mdbname"].ToString());
                        //}
                    }

                }
                else
                {
                    /*
                    modGlobal.gConn = new System.Data.SqlClient.SqlConnection();
                    modGlobal.gConn.ConnectionString = cnnstr;
                    modGlobal.gConn.Open();
                     */
                    System.Data.SqlClient.SqlConnection.ClearAllPools();
                    gConn = new System.Data.SqlClient.SqlConnection();
                    gConn.ConnectionString = cnnstr;
                    gConn.Open();
                    
                    //if (HttpContext.Current.Session != null && HttpContext.Current.Session["mdbname"] != null)
                    //{
                    //    if (HttpContext.Current.Session["mdbname"].ToString() != gConn.Database.ToString())
                    //        gConn.ChangeDatabase(HttpContext.Current.Session["mdbname"].ToString());
                    //}
                }
                //oCmd.Connection = modGlobal.gConn;
                oCmd.Connection = gConn;

                //'Sohail (20 Nov 2013) - Start
                if (HttpContext.Current.Session != null && HttpContext.Current.Session["gsqlTransaction"] != null)
                {
                    gsqlTransaction = (SqlTransaction)HttpContext.Current.Session["gsqlTransaction"];
                }

                if (HttpContext.Current.Session != null && HttpContext.Current.Session["gblnBindTransaction"] != null)
                {
                    gblnBindTransaction = (bool)HttpContext.Current.Session["gblnBindTransaction"];
                }

                if (gblnBindTransaction == true)
                {
                    oCmd.Transaction = gsqlTransaction;
                }
                //'Sohail (20 Nov 2013) - End
            }
            catch (SqlException SqlEx)
            {
                ErrorMessage = "SqlException:- " + SqlEx.Message;
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- " + ex.Message;
                throw ex;
            }
        }

        //'Sohail (28 Oct 2013) - Check Connection while ExecuteQuery/NonQuery function called. -- END

        public void BindTransaction()
        {
            try
            {
                //if (modGlobal.gblnBindTransaction == false)
                //{
                //    modGlobal.gsqlTransaction = modGlobal.gConn.BeginTransaction(IsolationLevel.ReadCommitted);
                //    oCmd.Transaction = modGlobal.gsqlTransaction;
                //    modGlobal.gblnBindTransaction = true;
                //    mblnLocalBindTran = false;
                //}
                if (gblnBindTransaction == false)
                {
                    CheckConnection();//'Sohail (28 Oct 2013)

                    gsqlTransaction = gConn.BeginTransaction(IsolationLevel.ReadCommitted);
                    oCmd.Transaction = gsqlTransaction;
                    gblnBindTransaction = true;
                    mblnLocalBindTran = false;

                    //'Sohail (20 Nov 2013) - Start
                    HttpContext.Current.Session["gsqlTransaction"] = gsqlTransaction;
                    HttpContext.Current.Session["gblnBindTransaction"] = true;
                    //'Sohail (20 Nov 2013) - End
                    
                }
                
                }
            catch (SqlException SqlEx)
            {
                ErrorMessage = "SqlException:- " + SqlEx.Message;
                ErrorNumber = SqlEx.Number.ToString();
                //Throw ex
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- " + ex.Message;
                //Throw ex
            }
        }

        public void ReleaseTransaction(bool blnCommit)
        {
            try
            {
                //if (modGlobal.gblnBindTransaction & mblnLocalBindTran == false)
                //{
                //    if (blnCommit)
                //    {
                //        modGlobal.gsqlTransaction.Commit();
                //    }
                //    else
                //    {
                //        modGlobal.gsqlTransaction.Rollback();
                //    }
                //    modGlobal.gblnBindTransaction = false;
                //    modGlobal.gsqlTransaction = null;
                //    mblnLocalBindTran = true;
                //}
                if (gblnBindTransaction & mblnLocalBindTran == false)
                {
                    if (blnCommit)
                    {
                        gsqlTransaction.Commit();
                    }
                    else
                    {
                        gsqlTransaction.Rollback();
                    }
                    gblnBindTransaction = false;
                    gsqlTransaction = null;
                    mblnLocalBindTran = true;
                    //'Sohail (20 Nov 2013) - Start
                    HttpContext.Current.Session["gsqlTransaction"] = null;
                    HttpContext.Current.Session["gblnBindTransaction"] = false;
                    //'Sohail (20 Nov 2013) - End
                }
            }
            catch (SqlException SqlEx)
            {
                ErrorMessage = "SqlException:- " + SqlEx.Message;
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- " + ex.Message;
                throw ex;
            }
        }

        public void change_database(string DatabaseName)
        {
            if (DatabaseName != "")
                strDatabaseName = DatabaseName;

            if (strDatabaseName != "")
                gConn.ChangeDatabase(strDatabaseName);
               
        }

        [System.Web.Services.WebMethod(EnableSession = true)]
        private void myconnection()
        {
            oCmd = new SqlCommand();
            try
            {
                cnnstr = Convert.ToString(ConfigurationManager.ConnectionStrings["paydb"].ConnectionString);
                //Anjan (21 Dec 2012)-Start
                string dbPwd = ConfigurationManager.AppSettings["dbpassword"].ToString();
                cnnstr = cnnstr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString());
                //Anjan (21 Dec 2012)-End

                if (gConn != null)
                {                    
                    if (gConn.State == ConnectionState.Closed | gConn.State == ConnectionState.Broken)
                    {
                        gConn.ConnectionString = cnnstr;
                        gConn.Open();

                        if (HttpContext.Current.Session["databasename"] != null)
                        {
                            gConn.ChangeDatabase(HttpContext.Current.Session["databasename"].ToString());
                        }
                    }

                }
                else
                {                 
                    gConn = new System.Data.SqlClient.SqlConnection();
                    gConn.ConnectionString = cnnstr;
                    gConn.Open();

                    HttpSessionState mysession = HttpContext.Current.Session;
                    if (mysession != null && Convert.ToString(mysession["databasename"].ToString()) != null)
                    {
                        string s = HttpContext.Current.Session["databasename"].ToString();
                        gConn.ChangeDatabase(HttpContext.Current.Session["databasename"].ToString());
                    }
                }               
                oCmd.Connection = gConn;
            }
            catch (SqlException SqlEx)
            {
                ErrorMessage = "SqlException:- " + SqlEx.Message;
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- " + ex.Message;
                throw ex;
            }
        }

#region  Parameters

        /// <summary>
        /// Removes all the System.Data.SqlClient.SqlParameter objects from the System.Data.SqlClient.SqlParameterCollection.
        /// </summary>
        public void ClearParameters()
        {
            try
            {
                oCmd.Parameters.Clear();
            }
            catch (SqlException SqlEx)
            {
                throw SqlEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Removes the specified System.Data.SqlClient.SqlParameter from the collection.
        /// </summary>
        /// <param name="parameterName">The name of the parameter to retrieve. </param>
        public void RemoveParameters(string parameterName)
        {
            try
            {
                if (oCmd.Parameters.Contains(parameterName))
                {
                oCmd.Parameters.Remove(oCmd.Parameters[parameterName]);
                }
            }
            catch (SqlException SqlEx)
            {
                throw SqlEx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

#region  AddParameter
        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.String that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, string oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.String that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        /// <remarks> Test </remarks>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, string oValue, ParameterDirection oParamDirection)
        {
            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, iSize);

            try
            {
                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);
            }
            catch (SqlException SqlEx)
            {
                ErrorMessage = string.Format("SqlException:- [{0}] {1}", strName, SqlEx.Message);
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = string.Format("Exception:- [{0}] {1}", strName, ex.Message);
                throw ex;
            }
            finally
            {
                oSqlParameter = null;
            }
        }



        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An Array of System.Byte that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, byte[] oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An Array of System.Byte that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, byte[] oValue, ParameterDirection oParamDirection)
        {
            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, iSize);
            try
            {

                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);

            }
            catch (SqlException SqlEx)
            {
                ErrorMessage = string.Format("SqlException:- [{0}] {1}", strName, SqlEx.Message);
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = string.Format("Exception:- [{0}] {1}", strName, ex.Message);
                throw ex;
            }
            finally
            {
                oSqlParameter = null;
            }
        }



        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Boolean that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, bool oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Boolean that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, bool oValue, ParameterDirection oParamDirection)
        {

            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, System.Convert.ToInt32(iSize));

            try
            {

                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);

            }
            catch (SqlException SqlEx)
            {
                ErrorMessage = string.Format("SqlException:- [{0}] {1}", strName, SqlEx.Message);
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = string.Format("Exception:- [{0}] {1}", strName, ex.Message);
                throw ex;
            }
            finally
            {
                oSqlParameter = null;
            }
        }



        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.DBNull that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, DBNull oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.DBNull that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, DBNull oValue, ParameterDirection oParamDirection)
        {

            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, iSize);

            try
            {
                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);
            }
            catch (SqlException SqlEx)
            {
                ErrorMessage = "SqlException:- " + SqlEx.Message;
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- " + ex.Message;
                throw ex;
            }
            finally
            {
                oSqlParameter = null;
            }
        }



        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Image that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, Image oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Image that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, Image oValue, ParameterDirection oParamDirection)
        {
            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, System.Convert.ToInt32(iSize));

            try
            {

                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);

            }
            catch (SqlException SqlEx)
            {
                ErrorMessage = "SqlException:- " + SqlEx.Message;
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- " + ex.Message;
                throw ex;
            }
            finally
            {
                oSqlParameter = null;
            }
        }



        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Guid that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, Guid oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Guid that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, Guid oValue, ParameterDirection oParamDirection)
        {

            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, System.Convert.ToInt32(iSize));

            try
            {
                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);
            }
            catch (SqlException SqlEx)
            {
                ErrorMessage = "SqlException:- " + SqlEx.Message;
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = "Exception:- " + ex.Message;
                throw ex;
            }
            finally
            {
                oSqlParameter = null;
            }
        }



        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Integer that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, int oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Integer that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, int oValue, ParameterDirection oParamDirection)
        {
            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, iSize);

            try
            {
                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);

        }
            catch (SqlException SqlEx)
            {
                ErrorMessage = string.Format("SqlException:- [{0}] {1}", strName, SqlEx.Message);
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = string.Format("Exception:- [{0}] {1}", strName, ex.Message);
                throw ex;
            }
            finally
            {
                oSqlParameter = null;
            }
        }



        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Double that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, double oValue)    
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Double that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, double oValue, ParameterDirection oParamDirection)
        {
            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, iSize);

            try
            {
                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);

        }
            catch (SqlException SqlEx)
            {
                ErrorMessage = string.Format("SqlException:- [{0}] {1}", strName, SqlEx.Message);
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = string.Format("Exception:- [{0}] {1}", strName, ex.Message);
                throw ex;
            }
            finally
            {
                oSqlParameter = null;
            }
        }



        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.DateTime that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, DateTime oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.DateTime that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, DateTime oValue, ParameterDirection oParamDirection)
        {
            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, iSize);

            try
            {
                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);

            }
            catch (SqlException SqlEx)
            {
                ErrorMessage = string.Format("SqlException:- [{0}] {1}", strName, SqlEx.Message);
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = string.Format("Exception:- [{0}] {1}", strName, ex.Message);
                throw ex;
            }
            finally
            {
                oSqlParameter = null;
            }
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Object that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, object oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        /// <summary>
        /// Add a new instance of the System.Data.SqlClient.SqlParameter class into active SqlCommand.
        /// </summary>
        /// <param name="strName">The name of the parameter to map.</param>
        /// <param name="oSqlDbType"> One of the System.Data.SqlDbType values.</param>
        /// <param name="iSize">The length of the parameter.</param>
        /// <param name="oValue">An System.Object that is the value of the System.Data.SqlClient.SqlParameter.</param>
        /// <param name="oParamDirection">One of the System.Data.ParameterDirection values.</param>
        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, object oValue, ParameterDirection oParamDirection)
        {
            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, iSize);

            try
            {
                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);

            }
            catch (SqlException SqlEx)
            {
                ErrorMessage = string.Format("SqlException:- [{0}] {1}", strName, SqlEx.Message);
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = string.Format("Exception:- [{0}] {1}", strName, ex.Message);
                throw ex;
            }
            finally
            {
                oSqlParameter = null;
            }
        }

        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, decimal oValue)
        {
            AddParameter(strName, oSqlDbType, iSize, oValue, ParameterDirection.Input);
        }

        public void AddParameter(string strName, SqlDbType oSqlDbType, int iSize, decimal oValue, ParameterDirection oParamDirection)
        {
            SqlParameter oSqlParameter = new SqlParameter(strName, oSqlDbType, iSize);

            try
            {
                oSqlParameter.Value = oValue;
                oSqlParameter.Direction = oParamDirection;
                oCmd.Parameters.Add(oSqlParameter);

            }
            catch (SqlException SqlEx)
            {
                ErrorMessage = string.Format("SqlException:- [{0}] {1}", strName, SqlEx.Message);
                ErrorNumber = SqlEx.Number.ToString();
                throw SqlEx;
            }
            catch (Exception ex)
            {
                ErrorMessage = string.Format("Exception:- [{0}] {1}", strName, ex.Message);
                throw ex;
            }
            finally
            {
                oSqlParameter = null;
            }
        }
#endregion

        public object GetParameterValue(string strName)
        {
            return oCmd.Parameters[strName].Value;
        }

#endregion

        /// <summary>
        /// For execute a stored procedure.
        /// </summary>
        /// <param name="StoredProcedure"> A stored procedure name.</param>
        /// <returns>Integer</returns>
        /// <remarks></remarks>
        public int ExecStoredProcedure(string StoredProcedure)
        {
            int iEffectedRows = 0;
            int intID = 0;

            if (!(this.ErrorMessage.Equals("")))
            {
                return iEffectedRows;
            }

            oCmd.CommandText = StoredProcedure;
            oCmd.CommandType = CommandType.StoredProcedure;
            oCmd.CommandTimeout = 0;

            try
            {
                iEffectedRows = oCmd.ExecuteNonQuery();
                if (oCmd.Parameters.Contains("@intUnkid"))
                {
                    try
                    {
                        if (oCmd.Parameters.Contains("@foliotranunkid"))
                        {
                            if (!(System.Convert.IsDBNull(oCmd.Parameters["@foliotranunkid"].Value)))
                            {
                                mintFolioTranUnkid = (int)oCmd.Parameters["@foliotranunkid"].Value;
                            }
                        }
                        intID = (int)oCmd.Parameters["@intUnkid"].Value;  	
                    }
                    catch
                    {
                        intID = 0;
                    }
                }
                else if (oCmd.Parameters.Contains("@intseasonunkid"))
                {
                    try
                    {
                        intID = (int)oCmd.Parameters["@intseasonunkid"].Value;
                    }
                    catch 
                    {
                        intID = 0;
                    }
                }
                else if (oCmd.Parameters.Contains("@foliotranunkid"))
                {
                    mintFolioTranUnkid = (int)oCmd.Parameters["@foliotranunkid"].Value;
                }
                this.ErrorNumber = oCmd.Parameters["@status"].Value.ToString();
            }
            catch (SqlException SqlEx)
            {
                this.ErrorMessage = SqlEx.Message;
                //Me.ErrorNumber = -1
                iEffectedRows = 0;
                throw SqlEx;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
                //Me.ErrorNumber = -1
                iEffectedRows = 0;
                throw ex;
            }
            return intID;
        }

        /// <summary>
        /// For execute a stored procedure and return a dataset.
        /// </summary>
        /// <param name="strProcName">A stored procedure name.</param>
        /// <param name="strTableName">A data table name.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public DataSet ExecStoredProcedure(string strProcName, string strTableName)
        {

            DataSet oDataSet = new DataSet();
            SqlDataAdapter oDa = null;
            try
            {

                if (!(this.ErrorMessage.Equals("")))
                {
                    throw new Exception(this.ErrorNumber + " " + this.ErrorMessage);
                }

                oCmd.CommandText = strProcName;
                oCmd.CommandType = CommandType.StoredProcedure;

                oDa = new SqlDataAdapter();

                oDa.SelectCommand = oCmd;
                oDa.Fill(oDataSet, strTableName);

                return oDataSet;

            }
            catch (SqlException SqlEx)
            {
                this.ErrorMessage = "SqlException: " + SqlEx.Message;
                this.ErrorNumber = SqlEx.Number.ToString();

                string strXml = "<NewDataSet><" + strTableName + ">" + "<ISSUCCESS>101</ISSUCCESS>" + "<ERRORMESSAGE>" + this.ErrorMessage + "</ERRORMESSAGE>" + "</" + strTableName + ">" + "</NewDataSet>";

                StringReader oSr = new StringReader(strXml);

                try
                {
                    oDataSet.ReadXml(oSr);
                }
                finally
                {
                    if (oSr != null)
                    {
                        oSr.Dispose();
                    }
                    oSr = null;
                }

                return oDataSet;

            }
            catch (Exception ex)
            {
                this.ErrorMessage = "Not SQLlException: " + ex.Message;
                string strXml = "<NewDataSet><" + strTableName + ">" + "<ISSUCCESS>101</ISSUCCESS>" + "<ERRORMESSAGE>" + this.ErrorMessage + "</ERRORMESSAGE>" + "</" + strTableName + ">" + "</NewDataSet>";

                StringReader oSr = new StringReader(strXml);

                try
                {
                    oDataSet.ReadXml(oSr);
                }
                finally
                {
                    if (oSr != null)
                    {
                        oSr.Dispose();
                    }
                    oSr = null;
                }
                return oDataSet;

            }
            finally
            {
                if (oDataSet != null)
                {
                    oDataSet.Dispose();
                }
                oDataSet = null;

                if (oDa != null)
                {
                    oDa.Dispose();
                }
                oDa = null;
            }
        }

        /// <summary>
        /// Sends the SqlCommand.CommandText to 
        /// the SqlCommand.Connection 
        /// and builds a DataSet.
        /// </summary>
        /// <param name="strQuery">A Transact-SQL statement to execute at the data source.</param>
        /// <param name="strTableName">The name of the source table to use for table mapping.</param>
        /// <returns>A System.Data.DataSet to fill with records and, if necessary, schema.</returns>
        /// <remarks></remarks>
        public DataSet ExecQuery(string strQuery, string strTableName)
        {
            DataSet oDataSet = new DataSet();
            SqlDataAdapter oDa = null;

                if (!(this.ErrorMessage.Equals("")))
                {
                    throw new Exception(this.ErrorNumber + " " + this.ErrorMessage);
                }

                CheckConnection();//'Sohail (28 Oct 2013)
//S.SANDEEP |14-MAR-2020| -- START
        //ISSUE/ENHANCEMENT : STRING OR BINARY DATA WOULD BE TRUNCATED. SUPPRRESSING THIS ERROR AS SQL IS AUTO TRUNCATING THE DATA AND ALSO GIVING WARNING MESSAGES AS ERROR.
        B:
            //'S.SANDEEP |14-MAR-2020| -- END
                oCmd.CommandText = strQuery;
                oCmd.CommandType = CommandType.Text;
                oCmd.CommandTimeout = 0;

        A:
            try
            {
                oDa = new SqlDataAdapter();
                //oDa.ReturnProviderSpecificTypes = true;
                oDa.SelectCommand = oCmd;
                oDa.Fill(oDataSet, strTableName);
                return oDataSet;

            }
            catch (SqlException SqlEx)
            {
                //if (SqlEx.Number == 233 && modGlobal.gsqlTransaction == null)
                //    goto A;
                //else if (SqlEx.Number == 233 && modGlobal.gsqlTransaction != null)
                //{
                //    modGlobal.gblnBindTransaction = false;
                //    modGlobal.gsqlTransaction = null;
                //}
//S.SANDEEP |14-MAR-2020| -- START
                //ISSUE/ENHANCEMENT : STRING OR BINARY DATA WOULD BE TRUNCATED. SUPPRRESSING THIS ERROR AS SQL IS AUTO TRUNCATING THE DATA AND ALSO GIVING WARNING MESSAGES AS ERROR.
                if (SqlEx.Number == 8152)
                {
                    if (strQuery.Trim().Length > 0)
                    {
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        sb.AppendLine("SET ANSI_WARNINGS OFF");
                        sb.AppendLine(strQuery);
                        sb.AppendLine("SET ANSI_WARNINGS ON");
                        strQuery = sb.ToString();
                        goto B;
                    }
                }
                //'S.SANDEEP |14-MAR-2020| -- END
                if (SqlEx.Number == 233 && gsqlTransaction == null)
                    goto A;
                else if (SqlEx.Number == 233 && gsqlTransaction != null)
                {
                    gblnBindTransaction = false;
                    gsqlTransaction = null;
                    //'Sohail (20 Nov 2013) - Start
                    HttpContext.Current.Session["gsqlTransaction"] = null;
                    HttpContext.Current.Session["gblnBindTransaction"] = false;
                    //'Sohail (20 Nov 2013) - End
                }
                

                this.ErrorMessage = "SqlException: " + SqlEx.Message + " // SqlErrorNumber:- " + SqlEx.Number.ToString();
                this.ErrorNumber = SqlEx.Number.ToString();

                string strXml = "<NewDataSet><" + strTableName + ">" + "<ISSUCCESS>101</ISSUCCESS>" + "<ERRORMESSAGE>" + this.ErrorMessage + "</ERRORMESSAGE>" + "</" + strTableName + ">" + "</NewDataSet>";

                StringReader oSr = new StringReader(strXml);

                try
                {
                    oDataSet.ReadXml(oSr);
                }
                finally
                {
                    if (oSr != null)
                    {
                        oSr.Dispose();
                    }
                    oSr = null;
                }

                return oDataSet;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = "Not SQLlException: " + ex.Message;
                string strXml = "<NewDataSet><" + strTableName + ">" + "<ISSUCCESS>101</ISSUCCESS>" + "<ERRORMESSAGE>" + this.ErrorMessage + "</ERRORMESSAGE>" + "</" + strTableName + ">" + "</NewDataSet>";

                StringReader oSr = new StringReader(strXml);

                try
                {
                    oDataSet.ReadXml(oSr);
                }
                finally
                {
                    if (oSr != null)
                    {
                        oSr.Dispose();
                    }
                    oSr = null;
                }

                return oDataSet;
            }
            finally
            {
                if (oDataSet != null)
                {
                    oDataSet.Dispose();
                }
                oDataSet = null;

                if (oDa != null)
                {
                    oDa.Dispose();
                }
                oDa = null;
            }
        }

        /// <summary>
        /// Executes a Transact-SQL statement against the connection and returns the number of rows affected.
        /// </summary>
        /// <param name="strQuery">A Transact-SQL statement to execute at the data source.</param>
        /// <returns>Intege, number of rows affected.</returns>
        /// <remarks></remarks>
        public int ExecNonQuery(string strQuery)
        {
            int iEffectedRows = 0;

            if (!(this.ErrorMessage.Equals("")))
            {
                return iEffectedRows;
            }

            CheckConnection();//'Sohail (28 Oct 2013)

//S.SANDEEP |14-MAR-2020| -- START
        //ISSUE/ENHANCEMENT : STRING OR BINARY DATA WOULD BE TRUNCATED. SUPPRRESSING THIS ERROR AS SQL IS AUTO TRUNCATING THE DATA AND ALSO GIVING WARNING MESSAGES AS ERROR.
        B:
            //'S.SANDEEP |14-MAR-2020| -- END

            oCmd.CommandText = strQuery;
            oCmd.CommandType = CommandType.Text;
            oCmd.CommandTimeout = 0;

            A:

            try
            {
                 
                iEffectedRows = oCmd.ExecuteNonQuery();

                return iEffectedRows;

            }
            catch (SqlException SqlEx)
            {
                //if (SqlEx.Number == 233 && modGlobal.gsqlTransaction == null)
                //    goto A;
                //else if (SqlEx.Number == 233 && modGlobal.gsqlTransaction != null)
                //{
                //    modGlobal.gblnBindTransaction = false;
                //    modGlobal.gsqlTransaction = null;
                //}

//S.SANDEEP |14-MAR-2020| -- START
                //ISSUE/ENHANCEMENT : STRING OR BINARY DATA WOULD BE TRUNCATED. SUPPRRESSING THIS ERROR AS SQL IS AUTO TRUNCATING THE DATA AND ALSO GIVING WARNING MESSAGES AS ERROR.
                if (SqlEx.Number == 8152)
                {
                    if (strQuery.Trim().Length > 0)
                    {
                        System.Text.StringBuilder sb = new System.Text.StringBuilder();
                        sb.AppendLine("SET ANSI_WARNINGS OFF");
                        sb.AppendLine(strQuery);
                        sb.AppendLine("SET ANSI_WARNINGS ON");
                        strQuery = sb.ToString();
                        goto B;
                    }
                }
                //'S.SANDEEP |14-MAR-2020| -- END

                if (SqlEx.Number == 233 && gsqlTransaction == null)
                    goto A;
                else if (SqlEx.Number == 233 && gsqlTransaction != null)
                {
                    gblnBindTransaction = false;
                    gsqlTransaction = null;
                    //'Sohail (20 Nov 2013) - Start
                    HttpContext.Current.Session["gsqlTransaction"] = null;
                    HttpContext.Current.Session["gblnBindTransaction"] = false;
                    //'Sohail (20 Nov 2013) - End
                }

                iEffectedRows = -1;
                this.ErrorMessage = SqlEx.Message;
                this.ErrorNumber = SqlEx.Number.ToString();
                return 0;
            }
            catch (Exception ex)
            {
                iEffectedRows = -2;
                this.ErrorMessage = ex.Message;
                this.ErrorNumber = "0";
                return 0;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Executes a Transact-SQL statement against the connection and returns the number of rows count.
        /// </summary>
        /// <param name="strQuery">A Transact-SQL statement to execute at the data source.</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public int RecordCount(string strQuery)
        {

            if (!(this.ErrorMessage.Equals("")))
            {
                return -1;
            }

            DataSet oDataSet = new DataSet();
            SqlDataAdapter oDa = new SqlDataAdapter();

            CheckConnection();//'Sohail (28 Oct 2013)

                oCmd.CommandText = strQuery;
                oCmd.CommandType = CommandType.Text;

        A:

            try
            {
                oDa.SelectCommand = oCmd;
                oDa.Fill(oDataSet, "List");

                return oDataSet.Tables[0].Rows.Count;

            }
            catch (SqlException SqlEx)
            {
                //if (SqlEx.Number == 233 && modGlobal.gsqlTransaction == null)
                //    goto A;
                //else if (SqlEx.Number == 233 && modGlobal.gsqlTransaction != null)
                //{
                //    modGlobal.gblnBindTransaction = false;
                //    modGlobal.gsqlTransaction = null;
                //}
                if (SqlEx.Number == 233 && gsqlTransaction == null)
                    goto A;
                else if (SqlEx.Number == 233 && gsqlTransaction != null)
                {
                    gblnBindTransaction = false;
                    gsqlTransaction = null;
                    //'Sohail (20 Nov 2013) - Start
                    HttpContext.Current.Session["gsqlTransaction"] = null;
                    HttpContext.Current.Session["gblnBindTransaction"] = false;
                    //'Sohail (20 Nov 2013) - End
                }

                this.ErrorMessage = "SqlException: " + SqlEx.Message;
                return -1;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
                return -1;
            }
            finally
            {
                if (oDataSet != null)
                {
                    oDataSet.Dispose();
                }
                oDataSet = null;
                if (oDa != null)
                {
                    oDa.Dispose();
                }
                oDa = null;
            }
        }

        //public SqlDataReader ExecReader(string strQuery)
        //{
        //    SqlCommand objCmd = new SqlCommand();
        //    SqlDataReader oReader = null;

        //        if (!(this.ErrorMessage.Equals("")))
        //        {
        //            throw new Exception(this.ErrorNumber + " " + this.ErrorMessage);
        //        }

        //        objCmd = new SqlCommand();

        //        objCmd.Connection = modGlobal.gConn;
        //        objCmd.CommandText = strQuery;
        //        objCmd.CommandType = CommandType.Text;

        //A:

        //    try
        //    {
        //        oReader = objCmd.ExecuteReader();

        //        return oReader;
        //    }
        //    catch (SqlException SqlEx)
        //    {
        //        if (SqlEx.Number == 233 && modGlobal.gsqlTransaction == null)
        //            goto A;
        //        else if (SqlEx.Number == 233 && modGlobal.gsqlTransaction != null)
        //    {
        //            modGlobal.gblnBindTransaction = false;
        //            modGlobal.gsqlTransaction = null;
        //        }

        //        this.ErrorMessage = "SqlException: " + SqlEx.Message + " // SqlErrorNumber:- " + SqlEx.Number.ToString();
        //        this.ErrorNumber = SqlEx.Number.ToString();

        //        return null;
        //    }
        //    catch (Exception ex)
        //    {
        //        this.ErrorMessage = "Not SQLlException: " + ex.Message;

        //        return null;
        //    }
        //    finally
        //    {
        //        objCmd = null;
        //    }
        //}

        public SqlCommand SQL_Command(string strQuery)
        {
            try
            {
                CheckConnection();//'Sohail (28 Oct 2013)

                //return new SqlCommand(strQuery, modGlobal.gConn);
                return new SqlCommand(strQuery, gConn);

            }
            catch (SqlException SqlEx)
            {
                this.ErrorMessage = "SqlException: " + SqlEx.Message;
                return null;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
                return null;
            }
            finally
            {

            }
        }

        public string getVersion()
        {
            int iEffectedRows = 0;

            SqlDataReader oDataReader = null;
            string strVersion = "";

            if (!(this.ErrorMessage.Equals("")))
            {
                return iEffectedRows.ToString();
            }

            oCmd.CommandText = "version";
            oCmd.CommandType = CommandType.StoredProcedure;

        A:
            try
            {
                oDataReader = oCmd.ExecuteReader();
                if (oDataReader.HasRows)
                {
                    while (oDataReader.Read())
                    {
                        strVersion = oDataReader[0].ToString();
                    }
                }
                else
                {
                    strVersion = "9.0.0.999";
                }
                oDataReader.Close();
                this.ErrorNumber = "0";
            }
            catch (SqlException SqlEx)
            {
                if (SqlEx.Number == 233)
                    goto A;
                else
                {
                    this.ErrorMessage = SqlEx.Message;
                    this.ErrorNumber = "-1";
                    iEffectedRows = 0;
                    strVersion = "9.0.0.999";
                }
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
                this.ErrorNumber = "-1";
                iEffectedRows = 0;
                strVersion = "9.0.0.999";
            }
            finally
            {
                oDataReader = null;
            }
            return strVersion;
        }

        #region  IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        // IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    if (oCmd != null)
                    {
                        oCmd.Dispose();
                    }
                    oCmd = null;
                }
                // TODO: free shared unmanaged resources
            }
            this.disposedValue = true;
        }

        // This code added by Visual Basic to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        public int ExecStoredProcedureByBrain(string StoredProcedure)
        {
            int iEffectedRows = 0;
            if (!(this.ErrorMessage.Equals("")))
            {
                return iEffectedRows;
            }
            oCmd.CommandText = StoredProcedure;
            oCmd.CommandType = CommandType.StoredProcedure;
            try
            {
                iEffectedRows = oCmd.ExecuteNonQuery();
            }
            catch (SqlException SqlEx)
            {
                this.ErrorMessage = SqlEx.Message;
                iEffectedRows = 0;
                throw SqlEx;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
                iEffectedRows = 0;
                throw ex;
            }
            return iEffectedRows;
        }


        /// <summary>
        /// Sends the SqlCommand.CommandText to 
        /// the SqlCommand.Connection 
        /// and builds a DataSet.
        /// </summary>
        /// <param name="strQuery">A Transact-SQL statement to execute at the data source.</param>
        /// <param name="strTableName">The name of the source table to use for table mapping.</param>
        /// <returns>A System.Data.DataSet to fill with records and, if necessary, schema.</returns>
        /// <remarks></remarks>
        public DataSet WExecQuery(string strQuery, string strTableName)
        {
            DataSet oDataSet = new DataSet();
            SqlDataAdapter oDa = null;

            if (!(this.ErrorMessage.Equals("")))
            {
                throw new Exception(this.ErrorNumber + " " + this.ErrorMessage);
            }

            if (oCmd == null) { oCmd = new SqlCommand(); } //'SANDEEP - DUE TO OBJECT REFERENCE ERROR [2020-08-13]

            CheckConnection();//'Sohail (28 Oct 2013)

            oCmd.CommandText = strQuery;
            oCmd.CommandType = CommandType.Text;
            oCmd.CommandTimeout = 0;

        A:
            try
            {
                oDa = new SqlDataAdapter();
                //oDa.ReturnProviderSpecificTypes = true;
                oDa.SelectCommand = oCmd;
                //if (modGlobal.gblnBindTransaction)
                //{
                //    oCmd.Transaction = modGlobal.gsqlTransaction;
                //    oDa.SelectCommand.Transaction = modGlobal.gsqlTransaction;
                //}
                if (gblnBindTransaction)
                {
                    oCmd.Transaction = gsqlTransaction;
                    oDa.SelectCommand.Transaction = gsqlTransaction;
                }
                oDa.Fill(oDataSet, strTableName);
                return oDataSet;

            }
            catch (SqlException SqlEx)
            {
                //if (SqlEx.Number == 233 && modGlobal.gsqlTransaction == null)
                //    goto A;
                //else if (SqlEx.Number == 233 && modGlobal.gsqlTransaction != null)
                //{
                //    modGlobal.gblnBindTransaction = false;
                //    modGlobal.gsqlTransaction = null;
                //}
                if (SqlEx.Number == 233 && gsqlTransaction == null)
                    goto A;
                else if (SqlEx.Number == 233 && gsqlTransaction != null)
                {
                    gblnBindTransaction = false;
                    gsqlTransaction = null;
                    //'Sohail (20 Nov 2013) - Start
                    HttpContext.Current.Session["gsqlTransaction"] = null;
                    HttpContext.Current.Session["gblnBindTransaction"] = false;
                    //'Sohail (20 Nov 2013) - End
                }


                this.ErrorMessage = "SqlException: " + SqlEx.Message + " // SqlErrorNumber:- " + SqlEx.Number.ToString();
                this.ErrorNumber = SqlEx.Number.ToString();

                string strXml = "<NewDataSet><" + strTableName + ">" + "<ISSUCCESS>101</ISSUCCESS>" + "<ERRORMESSAGE>" + this.ErrorMessage + "</ERRORMESSAGE>" + "</" + strTableName + ">" + "</NewDataSet>";

                StringReader oSr = new StringReader(strXml);

                try
                {
                    oDataSet.ReadXml(oSr);
                }
                finally
                {
                    if (oSr != null)
                    {
                        oSr.Dispose();
                    }
                    oSr = null;
                }

                return oDataSet;
            }
            catch (Exception ex)
            {
                this.ErrorMessage = "Not SQLlException: " + ex.Message;
                string strXml = "<NewDataSet><" + strTableName + ">" + "<ISSUCCESS>101</ISSUCCESS>" + "<ERRORMESSAGE>" + this.ErrorMessage + "</ERRORMESSAGE>" + "</" + strTableName + ">" + "</NewDataSet>";

                StringReader oSr = new StringReader(strXml);

                try
                {
                    oDataSet.ReadXml(oSr);
                }
                finally
                {
                    if (oSr != null)
                    {
                        oSr.Dispose();
                    }
                    oSr = null;
                }

                return oDataSet;
            }
            finally
            {
                if (oDataSet != null)
                {
                    oDataSet.Dispose();
                }
                oDataSet = null;

                if (oDa != null)
                {
                    oDa.Dispose();
                }
                oDa = null;
            }
        }
        
        
    }

    
}