using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;

namespace TaskScheduler.Definition
{
	internal class DateConvert
	{
		private const int TASK_MAX_RUN_TIMES = 1440;

		[System.Runtime.InteropServices.DllImport("oleaut32.dll", EntryPoint="SystemTimeToVariantTime", ExactSpelling=true, CharSet=System.Runtime.InteropServices.CharSet.Ansi, SetLastError=true)]
		public static extern int SystemTimeToVariantTime(ref SYSTEMTIME lpSystemTime, ref double pvtime);
	}
}
