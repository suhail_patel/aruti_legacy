﻿namespace ExcelWriter.Schemas
{
    using ExcelWriter;
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Xml;

    public sealed class SchemaTypeCollection : CollectionBase, IWriter
    {
        internal SchemaTypeCollection()
        {
        }

        public int Add(SchemaType schemaType)
        {
            return base.InnerList.Add(schemaType);
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
        }

        public bool Contains(SchemaType item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(SchemaType[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(SchemaType item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, SchemaType item)
        {
            base.InnerList.Insert(index, item);
        }

        public void Remove(SchemaType item)
        {
            base.InnerList.Remove(item);
        }

        public SchemaType this[int index]
        {
            get
            {
                return (SchemaType) base.InnerList[index];
            }
        }
    }
}

