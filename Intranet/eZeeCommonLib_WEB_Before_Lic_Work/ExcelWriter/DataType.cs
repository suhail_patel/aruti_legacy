﻿namespace ExcelWriter
{
    using System;

    public enum DataType
    {
        Boolean = 4,
        DateTime = 3,
        Error = 0x3e7,
        Integer = 5,
        NotSet = 0,
        Number = 2,
        String = 1
    }
}

