﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Collections.Specialized;
    using System.Xml;

    public sealed class Worksheet : IWriter, IReader, ICodeWriter
    {
        private WorksheetAutoFilter _autoFilter;
        private string _name;
        private WorksheetNamedRangeCollection _names;
        private WorksheetOptions _options;
        private ExcelWriter.PivotTable _pivotTable;
        private bool _protected;
        private StringCollection _sorting;
        private WorksheetTable _table;
        internal static CodeVariableDeclarationStatement cellDeclaration;

        internal Worksheet(string name)
        {
            this._name = name;
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._protected)
            {
                Util.AddAssignment(method, targetObject, "Protected", true);
            }
            if (this._names != null)
            {
                Util.Traverse(type, this._names, method, targetObject, "Names");
            }
            if (this._table != null)
            {
                Util.Traverse(type, this._table, method, targetObject, "Table");
            }
            if (this._options != null)
            {
                Util.Traverse(type, this._options, method, targetObject, "Options");
            }
            if (this._autoFilter != null)
            {
                Util.Traverse(type, this._autoFilter, method, targetObject, "AutoFilter");
            }
            if (this._sorting != null)
            {
                foreach (string str in this._sorting)
                {
                    method.Statements.Add(new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(targetObject, "Sorting"), "Add", new CodeExpression[] { new CodePrimitiveExpression(str) }));
                }
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._name = Util.GetAttribute(element, "Name", "urn:schemas-microsoft-com:office:spreadsheet");
            this._protected = Util.GetAttribute(element, "Protected", "urn:schemas-microsoft-com:office:spreadsheet", false);
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if (element2 != null)
                {
                    if (WorksheetTable.IsElement(element2))
                    {
                        ((IReader) this.Table).ReadXml(element2);
                    }
                    else
                    {
                        if (WorksheetNamedRangeCollection.IsElement(element2))
                        {
                            ((IReader) this.Names).ReadXml(element2);
                            continue;
                        }
                        if (WorksheetAutoFilter.IsElement(element2))
                        {
                            ((IReader) this.AutoFilter).ReadXml(element2);
                            continue;
                        }
                        if (WorksheetOptions.IsElement(element2))
                        {
                            ((IReader) this.Options).ReadXml(element2);
                            continue;
                        }
                        if (Util.IsElement(element2, "Sorting", "urn:schemas-microsoft-com:office:excel"))
                        {
                            foreach (XmlElement element3 in element2.ChildNodes)
                            {
                                if (Util.IsElement(element3, "Sort", "urn:schemas-microsoft-com:office:excel"))
                                {
                                    this.Sorting.Add(element3.InnerText);
                                }
                            }
                            continue;
                        }
                    }
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Worksheet", "urn:schemas-microsoft-com:office:spreadsheet");
            if (this._name != null)
            {
                writer.WriteAttributeString("Name", "urn:schemas-microsoft-com:office:spreadsheet", this._name);
            }
            if (this._protected)
            {
                writer.WriteAttributeString("Protected", "urn:schemas-microsoft-com:office:spreadsheet", "1");
            }
            if (this._names != null)
            {
                ((IWriter) this._names).WriteXml(writer);
            }
            if (this._table != null)
            {
                ((IWriter) this._table).WriteXml(writer);
            }
            if (this._options != null)
            {
                ((IWriter) this._options).WriteXml(writer);
            }
            if (this._autoFilter != null)
            {
                ((IWriter) this._autoFilter).WriteXml(writer);
            }
            if (this._pivotTable != null)
            {
                ((IWriter) this._pivotTable).WriteXml(writer);
            }
            if (this._sorting != null)
            {
                writer.WriteStartElement("x", "Sorting", "urn:schemas-microsoft-com:office:excel");
                foreach (string str in this._sorting)
                {
                    writer.WriteElementString("Sort", "urn:schemas-microsoft-com:office:excel", str);
                }
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Worksheet", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public WorksheetAutoFilter AutoFilter
        {
            get
            {
                if (this._autoFilter == null)
                {
                    this._autoFilter = new WorksheetAutoFilter();
                }
                return this._autoFilter;
            }
        }

        public string Name
        {
            get
            {
                return this._name;
            }
        }

        public WorksheetNamedRangeCollection Names
        {
            get
            {
                if (this._names == null)
                {
                    this._names = new WorksheetNamedRangeCollection();
                }
                return this._names;
            }
        }

        public WorksheetOptions Options
        {
            get
            {
                if (this._options == null)
                {
                    this._options = new WorksheetOptions();
                }
                return this._options;
            }
        }

        public ExcelWriter.PivotTable PivotTable
        {
            get
            {
                if (this._pivotTable == null)
                {
                    this._pivotTable = new ExcelWriter.PivotTable();
                }
                return this._pivotTable;
            }
        }

        public bool Protected
        {
            get
            {
                return this._protected;
            }
            set
            {
                this._protected = value;
            }
        }

        public StringCollection Sorting
        {
            get
            {
                if (this._sorting == null)
                {
                    this._sorting = new StringCollection();
                }
                return this._sorting;
            }
        }

        public WorksheetTable Table
        {
            get
            {
                if (this._table == null)
                {
                    this._table = new WorksheetTable();
                }
                return this._table;
            }
        }
    }
}

