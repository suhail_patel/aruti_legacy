﻿<%@ Page Title="Salary Change List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_SalaryIncrementList.aspx.vb" Inherits="HR_wPg_SalaryIncrementList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                <div class="block-header">
                    <h2>
                            <asp:Label ID="lblPageHeader" runat="server" Text="Salary Change List"></asp:Label>
                    </h2>
                        </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                                    </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                                <asp:DropDownList ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAccess" runat="server" Text="Pay Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                                <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblSalaryChangeApprovalStatus" runat="server" Text="Approval Status" />
                                        <div class="form-group">
                                                <asp:DropDownList ID="cboSalaryChangeApprovalStatus" runat="server">
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" Visible="false" CssClass="btn btn-primary" />
                                <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-default" />
                                <asp:Button ID="btnDisapprove" runat="server" Text="Disapprove" CssClass="btn btn-default" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 275px">
                                        <asp:GridView ID="dgSalaryIncrement" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                                                ShowFooter="False" CssClass="table table-bordered" DataKeyNames="IsGrp">
                                            <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                    <HeaderTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" OnCheckedChanged="chkSelectAll_CheckedChanged"
                                                                    AutoPostBack="true" Text=" " />
                                                        </span>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:CheckBox ID="chkSelect" runat="server" OnCheckedChanged="chkSelect_CheckedChanged"
                                                                    AutoPostBack="true" Text=" " />
                                                        </span>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit" HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                            <asp:LinkButton ID="ImgSelect" runat="server" ToolTip="Edit" CommandName="Change"
                                                                CommandArgument="<%# Container.DataItemIndex %>">
                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                            </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete" HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                    <ItemStyle HorizontalAlign="Center" />
                                                    <ItemTemplate>
                                                            <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Remove"
                                                                CommandArgument="<%# Container.DataItemIndex %>">
                                                            <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="period_name" HeaderText="Pay Period" ReadOnly="True" FooterText="colhPayPeriod" />
                                                <asp:BoundField DataField="incrementdate" HeaderText="Increment Date" ReadOnly="True"
                                                    ItemStyle-Width="150px" FooterText="colhIncrDate" />
                                                <asp:BoundField DataField="currentscale" HeaderText="Current Scale" ReadOnly="True"
                                                    ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Right" FooterText="colhScale" />
                                                <asp:BoundField DataField="increment" HeaderText="Change Amount" ReadOnly="true"
                                                    ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Right" FooterText="colhIncrementAmt" />
                                                <asp:BoundField DataField="newscale" HeaderText="New Scale" ReadOnly="True" ItemStyle-Width="100px"
                                                    ItemStyle-HorizontalAlign="Right" FooterText="colhNewScale" />
                                                <asp:BoundField DataField="isapproved" HeaderText="Status" ReadOnly="True" ItemStyle-Width="60px"
                                                    ItemStyle-HorizontalAlign="Center" FooterText="colhApprovePending" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        </div>
                    </div>
                    <uc9:ConfirmYesNo ID="popupApprove" runat="server" Title="Approve Salary Change" />
                    <uc9:ConfirmYesNo ID="popupDisapprove" runat="server" Title="Disapprove Salary Change" />
                    <uc10:DeleteReason ID="popRejectReason" runat="server" Title="Comments" ValidationGroup="popRejectReason" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
</asp:Content>
