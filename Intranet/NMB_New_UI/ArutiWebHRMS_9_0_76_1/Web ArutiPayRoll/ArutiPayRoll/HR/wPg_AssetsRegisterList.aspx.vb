﻿Option Strict On 'Shani(19-MAR-2016)

#Region "Imports"

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data

#End Region

Partial Class wPg_AssetsRegisterList
    Inherits Basepage

#Region " Private Varaibles "

    Private objAsset As clsEmployee_Assets_Tran
    Dim DisplayMessage As New CommonCodes


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmAssetsRegisterList"
    Private ReadOnly mstrModuleName1 As String = "frmEmpAssetStatus_AddEdit"
    Private ReadOnly mstrModuleName2 As String = "frmEmpAssetStatus_AddEdit"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            objAsset = New clsEmployee_Assets_Tran

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmAssetsRegisterList"
            'StrModuleName2 = "mnuPersonnel"
            'StrModuleName3 = "mnuEmployeeData"
            'objAsset._WebClientIP = Session("IP_ADD")
            'objAsset._WebHostName = Session("HOST_NAME")
            'If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    objAsset._LoginEmployeeUnkid = Session("Employeeunkid")
            'End If

            'S.SANDEEP [ 04 JULY 2012 ] -- END

            If (Page.IsPostBack = False) Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLanguage()
                'Pinkal (06-May-2014) -- End


                Dim clsuser As New User
                clsuser = CType(Session("clsuser"), User)
                If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.UserID
                    Me.ViewState("Empunkid") = Session("UserId")
                    'Sohail (30 Mar 2015) -- End
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = Session("UserId")
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                    BtnAssign.Visible = CBool(Session("AddEmployeeAssets"))

                    'S.SANDEEP [ 16 JAN 2014 ] -- START
                    chkShowPending.Visible = CBool(Session("AllowToApproveEmployee"))
                    If Session("ShowPending") IsNot Nothing Then
                        chkShowPending.Checked = True
                        BtnAssign.Visible = False
                    End If
                    'S.SANDEEP [ 16 JAN 2014 ] -- END

                Else
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = -1
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                    Me.ViewState("Empunkid") = Session("Employeeunkid")
                    'Sohail (30 Mar 2015) -- End
                    BtnAssign.Visible = False



                    'S.SANDEEP [ 16 JAN 2014 ] -- START
                    chkShowPending.Visible = False
                    'S.SANDEEP [ 16 JAN 2014 ] -- END

                End If

                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
                'Nilay (01-Feb-2015) -- End
                FillCombo()
                'S.SANDEEP [ 16 JAN 2014 ] -- START
                If Session("ShowPending") IsNot Nothing Then
                    Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                End If
                'S.SANDEEP [ 16 JAN 2014 ] -- END

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("Asset_EmpUnkID") IsNot Nothing Then
                    drpEmployee.SelectedValue = CStr(Session("Asset_EmpUnkID"))
                    Session.Remove("Asset_EmpUnkID")
                    If CInt(drpEmployee.SelectedValue) > 0 Then
                        Call BtnSearch_Click(BtnSearch, Nothing)
                    End If
                End If
                'SHANI [09 Mar 2015]--END 
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'Nilay (01-Feb-2015) -- End


        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    ToolbarEntry1.VisibleSaveButton(False)
    '    ToolbarEntry1.VisibleDeleteButton(False)
    '    ToolbarEntry1.VisibleCancelButton(False)
    '    ToolbarEntry1.VisibleExitButton(False)
    '    If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
    '        ToolbarEntry1.VisibleModeFormButton(False)
    '        ToolbarEntry1.VisibleModeGridButton(False)
    '    End If

    'End Sub
    'Nilay (01-Feb-2015) -- End

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Functions "

    Public Shared Function b64encode(ByVal StrEncode As String) As String
        Try
            Dim encodedString As String
            encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
            Return (encodedString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub FillCombo()
        Dim objCommon As New clsCommon_Master
        Dim objEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Dim objMaster As New clsMasterData
        Try
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ASSET_CONDITION, True, "Condition")
            With drpCondition
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Condition")
                .DataBind()
            End With

            dsList = objMaster.getComboListForEmployeeAseetStatus("Status", True)
            With drpStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("Status")
                .DataBind()
            End With

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ASSETS, True, "Assets")
            With drpAsset
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Assets")
                .DataBind()
            End With

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsList = objEmp.GetEmployeeList("Emp", True, )
                '    dsList = objEmp.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END
                'End If

                dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Emp", True)

                'Shani(24-Aug-2015) -- End

                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsList.Tables("Emp")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objCommon = Nothing
            objEmp = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dsList As New DataSet
        Dim StrSearching As String = String.Empty
        Dim dtTable As DataTable
        Try


            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("ViewCompanyAssetList")) = False Then Exit Sub
            End If

            'Pinkal (22-Nov-2012) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''S.SANDEEP [ 27 APRIL 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''dsList = objAsset.GetList("AssetList", CInt(drpEmployee.SelectedValue), CInt(drpAsset.SelectedValue), CInt(drpCondition.SelectedValue), CInt(drpStatus.SelectedValue), txtAssetno.Text.Trim, "")
            'dsList = objAsset.GetList("AssetList", CInt(drpEmployee.SelectedValue), CInt(drpAsset.SelectedValue), CInt(drpCondition.SelectedValue), CInt(drpStatus.SelectedValue), txtAssetno.Text.Trim, "", Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"))
            ''S.SANDEEP [ 27 APRIL 2012 ] -- END



            ''If dtpdate.GetDate.Date.ToString <> CDate("01/01/2000") And dtpdate.GetDate.Date.ToString <> "" Then
            ''    StrSearching &= "AND Date='" & eZeeDate.convertDate(dtpdate.GetDate.Date) & "' "
            ''End If

            'If dtpdate.IsNull = False Then
            '    StrSearching &= "AND Date='" & eZeeDate.convertDate(dtpdate.GetDate.Date) & "' "
            'End If


            'If StrSearching.Length > 0 Then
            '    StrSearching = StrSearching.Substring(3)
            '    dtTable = New DataView(dsList.Tables("AssetList"), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsList.Tables("AssetList"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            'End If


            If dtpdate.IsNull = False Then
                StrSearching &= "AND CONVERT(CHAR(8),hremployee_assets_tran.assign_return_date,112) ='" & eZeeDate.convertDate(dtpdate.GetDate.Date) & "' "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            'dsList = objAsset.GetList(Session("Database_Name"), _
            '              Session("UserId"), _
            '              Session("Fin_year"), _
            '              Session("CompanyUnkId"), _
            '              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '              Session("UserAccessModeSetting"), True, _
            '              Session("IsIncludeInactiveEmp"), "AssetList", , _
            '              CInt(drpEmployee.SelectedValue), StrSearching, CInt(drpAsset.SelectedValue), _
            '              CInt(drpCondition.SelectedValue), CInt(drpStatus.SelectedValue), txtAssetno.Text.Trim)

            dsList = objAsset.GetList(CStr(Session("Database_Name")), _
                          CInt(Session("UserId")), _
                          CInt(Session("Fin_year")), _
                          CInt(Session("CompanyUnkId")), _
                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                          CStr(Session("UserAccessModeSetting")), True, _
                          CBool(Session("IsIncludeInactiveEmp")), "AssetList", , _
                          CInt(drpEmployee.SelectedValue), StrSearching, CInt(drpAsset.SelectedValue), _
                          CInt(drpCondition.SelectedValue), CInt(drpStatus.SelectedValue), txtAssetno.Text.Trim, CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))


            'Pinkal (06-Jan-2016) -- End


            dtTable = New DataView(dsList.Tables("AssetList"), "", "EmpName", DataViewRowState.CurrentRows).ToTable

            'Shani(24-Aug-2015) -- End

            If dtTable IsNot Nothing Then
                dgView.DataSource = dtTable
                dgView.DataKeyField = "assetstranunkid"
                dgView.DataBind()
            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                dgView.CurrentPageIndex = 0
                dgView.DataBind()
            Else
                DisplayMessage.DisplayError(ex, Me)
            End If
        End Try
    End Sub


#End Region

#Region "Button's Event"
    'Gajanan [27-May-2019] -- Start              
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            If IsNothing(Session("Isfromprofile")) = False AndAlso CBool(Session("Isfromprofile")) = True Then
                Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeDetail.aspx", False)
                Session.Remove("Isfromprofile")
            Else
                Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Gajanan [27-May-2019] -- End

    Protected Sub btnStatusSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStatusSave.Click
        Try
            Dim objAssetStatus As New clsEmployee_Assets_Status_Tran
            Dim dtpStatus As Date = Nothing
            Dim dsList As DataSet = objAssetStatus.GetLastStatus("Status", CInt(Me.ViewState("AssestTranunkid")))
            If dsList.Tables("Status").Rows.Count > 0 Then
                With dsList.Tables("Status").Rows(0)
                    dtpStatus = CDate(.Item("status_date").ToString)
                End With
                If dtpStatus.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage("Sorry, Status date should be greater than last change status date : " & dtpStatus.Date, Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
            End If

            objAssetStatus._Isvoid = False
            objAssetStatus._Assetstranunkid = CInt(Me.ViewState("AssestTranunkid"))

            objAssetStatus._Remark = txtStatusRemark.Text.Trim
            objAssetStatus._Status_Date = ConfigParameter._Object._CurrentDateAndTime.Date
            objAssetStatus._Voiddatetime = Nothing
            objAssetStatus._Voiduserunkid = -1
            objAssetStatus._Statusunkid = CInt(Me.ViewState("StatusID"))

            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objAssetStatus._Userunkid = CInt(Session("UserId"))
            'S.SANDEEP [ 27 APRIL 2012 ] -- END



            'S.SANDEEP [ 27 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim blnFlag As Boolean = objAssetStatus.Insert()
            Dim blnFlag As Boolean = objAssetStatus.Insert(True)
            'S.SANDEEP [ 27 AUG 2012 ] -- END



            If blnFlag = False And objAssetStatus._Message <> "" Then
                DisplayMessage.DisplayMessage(objAssetStatus._Message, Me)
                Exit Sub
            Else

                'S.SANDEEP [ 27 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                objAsset._Assetstranunkid = CInt(Me.ViewState("AssestTranunkid"))
                Blank_ModuleName()
                objAsset._WebFormName = "frmAssetsRegisterList"
                StrModuleName2 = "mnuPersonnel"
                StrModuleName3 = "mnuEmployeeData"
                objAsset._WebClientIP = CStr(Session("IP_ADD"))
                objAsset._WebHostName = CStr(Session("HOST_NAME"))
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    objAsset._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                    objAsset._Userunkid = -1
                Else
                    objAsset._LoginEmployeeUnkid = -1
                    objAsset._Userunkid = CInt(Session("UserId"))
                End If
                objAsset._Statusunkid = CInt(Me.ViewState("StatusID"))
                objAsset.Update()
                'S.SANDEEP [ 27 AUG 2012 ] -- END



                DisplayMessage.DisplayMessage("Status Changed successfully.", Me)
                Me.ViewState("AssestTranunkid") = Nothing
                FillGrid()
                Exit Sub
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAssign.Click
        Try
            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            Session("Asset_EmpUnkID") = drpEmployee.SelectedValue
            'SHANI [09 Mar 2015]--END 
            Response.Redirect("~/HR/wPg_EmployeeAsset.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDel.Click
    Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'Nilay (01-Feb-2015) -- End


        Try
            If (popup_DeleteReason.Reason.Trim = "") Then 'Nilay (01-Feb-2015) -- If (txtreasondel.Text = "")
                DisplayMessage.DisplayMessage(" Please enter delete reason.", Me)

                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'txtreasondel.Focus()
                'popupDelete.Show()
                popup_DeleteReason.Show()
                'Nilay (01-Feb-2015) -- End
                Exit Sub
            End If

            'S.SANDEEP [ 27 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Blank_ModuleName()
            objAsset._WebFormName = "frmAssetsRegisterList"
            StrModuleName2 = "mnuPersonnel"
            StrModuleName3 = "mnuEmployeeData"
            objAsset._WebClientIP = CStr(Session("IP_ADD"))
            objAsset._WebHostName = CStr(Session("HOST_NAME"))
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                objAsset._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                objAsset._LoginEmployeeUnkid = -1
            End If
            'S.SANDEEP [ 27 AUG 2012 ] -- END



            With objAsset
                ._Isvoid = True
                ._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popup_DeleteReason.Reason.Trim     'Nilay (01-Feb-2015) -- txtreasondel.Text.Trim
                ._Voiduserunkid = CInt(Session("UserId"))
                .Delete(CInt(Me.ViewState("AssestTranunkid")))

                If (._Message.Length > 0) Then
                    DisplayMessage.DisplayMessage("You Cannot Delete this Entry.Reason : " & ._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Successfully deleted." & ._Message, Me)
                    Me.ViewState("AssestTranunkid") = Nothing
                    FillGrid()
                End If

                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'popupDelete.Dispose()
                'txtreasondel.Text = ""
                popup_DeleteReason.Dispose()
                'Nilay (01-Feb-2015) -- End

            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(drpEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If
            'Sohail (02 May 2012) -- End
            FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            drpEmployee.SelectedIndex = 0
            drpAsset.SelectedIndex = 0
            drpCondition.SelectedIndex = 0
            drpStatus.SelectedIndex = 0
            dtpdate.SetDate = Nothing
            txtAssetno.Text = ""
            'Shani [ 24 DEC 2014 ] -- START
            'Implement Close Button Code on Each Page.
            'FillGrid()
            dgView.DataSource = New List(Of String)
            dgView.DataBind()
            'Shani [ 24 DEC 2014 ] -- END


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Shani [ 24 DEC 2014 ] -- END
#End Region

#Region "DataGrid Event"

    Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.ItemCommand
        Try

            If e.CommandName = "Returned" Or e.CommandName = "Writtenoff" Or e.CommandName = "Lost" Then

                If CInt(dgView.Items(e.Item.ItemIndex).Cells(12).Text) = 0 Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry! Current Status not found."), Me)
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                End If

                Me.ViewState.Add("AssestTranunkid", dgView.DataKeys(e.Item.ItemIndex))

                Dim menStatus As enEmpAssetStatus

                If e.CommandName = "Returned" Then
                    menStatus = enEmpAssetStatus.Returned
                ElseIf e.CommandName = "Writtenoff" Then
                    menStatus = enEmpAssetStatus.WrittenOff
                ElseIf e.CommandName = "Lost" Then
                    menStatus = enEmpAssetStatus.Lost
                End If

                Me.ViewState.Add("StatusID", menStatus)

                If CInt(dgView.Items(e.Item.ItemIndex).Cells(12).Text) = menStatus Then

                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry! New Status should be different."), Me)
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                End If

                If CInt(dgView.Items(e.Item.ItemIndex).Cells(12).Text) = enEmpAssetStatus.Returned OrElse CInt(dgView.Items(e.Item.ItemIndex).Cells(12).Text) = enEmpAssetStatus.WrittenOff Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry! You can not change status when status is Returned OR Written Off."), Me)
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                End If

                txtStatusEmp.Text = dgView.Items(e.Item.ItemIndex).Cells(4).Text.Trim
                txtStatusAsset.Text = dgView.Items(e.Item.ItemIndex).Cells(5).Text.Trim
                txtStatusRemark.Text = ""

                Dim objMaster As New clsMasterData
                Dim drrow As DataRow() = objMaster.getComboListForEmployeeAseetStatus("Status", True).Tables(0).Select("Id = " & menStatus)

                If drrow.Length > 0 Then
                    txtAssetStatus.Text = drrow(0)("Name").ToString()
                End If

                popupStatus.Show()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.DeleteCommand
        Try
            Me.ViewState.Add("AssestTranunkid", dgView.DataKeys(e.Item.ItemIndex))

            If CInt(dgView.Items(e.Item.DataSetIndex).Cells(12).Text) > 0 AndAlso CInt(dgView.Items(e.Item.DataSetIndex).Cells(12).Text) <> enEmpAssetStatus.Assigned Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry! You can delete this transaction only when status is Assigned."), Me)
                'Pinkal (06-May-2014) -- End
                Exit Sub
            End If
            'Nilay (01-Feb-2015) -- txtreasondel.Text = "" 
            popup_DeleteReason.Show() 'Nilay (01-Feb-2015) -- popupDelete.Show()
            FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgView.PageIndexChanged
        Try
            dgView.CurrentPageIndex = e.NewPageIndex
            FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgView.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If e.Item.ItemIndex >= 0 Then
                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'If e.Item.Cells(8).Text <> "" Then
                '    e.Item.Cells(8).Text = CStr(eZeeDate.convertDate(e.Item.Cells(8).Text).Date)
                'End If
                If e.Item.Cells(8).Text <> "&nbsp;" AndAlso e.Item.Cells(8).Text <> "" Then
                    e.Item.Cells(8).Text = eZeeDate.convertDate(e.Item.Cells(8).Text).ToShortDateString
                End If
                'Pinkal (16-Apr-2016) -- End

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    dgView.Columns(0).Visible = True
                    dgView.Columns(1).Visible = True
                    dgView.Columns(2).Visible = True


                    'Anjan (30 May 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'dgView.Columns(0).Visible = True
                    'dgView.Columns(1).Visible = True
                    'dgView.Columns(2).Visible = True
                    'dgView.Columns(3).Visible = True
                    dgView.Columns(0).Visible = CBool(Session("EditEmployeeAssets"))
                    dgView.Columns(1).Visible = CBool(Session("EditEmployeeAssets"))
                    dgView.Columns(2).Visible = CBool(Session("EditEmployeeAssets"))
                    dgView.Columns(3).Visible = CBool(Session("DeleteEmployeeAssets"))
                    'Anjan (30 May 2012)-End 

                    'S.SANDEEP [ 16 JAN 2014 ] -- START
                    If Session("ShowPending") IsNot Nothing Then
                        dgView.Columns(0).Visible = False
                        dgView.Columns(1).Visible = False
                        dgView.Columns(2).Visible = False
                        dgView.Columns(3).Visible = False
                        dgView.Columns(4).Visible = False
                    End If
                    'S.SANDEEP [ 16 JAN 2014 ] -- END

                Else
                    dgView.Columns(0).Visible = False
                    dgView.Columns(1).Visible = False
                    dgView.Columns(2).Visible = False
                    dgView.Columns(3).Visible = False
                    dgView.Columns(4).Visible = False
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP [ 16 JAN 2014 ] -- START
    Protected Sub chkShowPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowPending.CheckedChanged
        Try
            Session("ShowPending") = chkShowPending.Checked
            Dim dsCombos As New DataSet
            Dim objEmp As New clsEmployee_Master
            If chkShowPending.Checked = True Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"), , , False)
                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), False, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                'Shani(24-Aug-2015) -- End
                Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dtTab
                    .DataBind()
                    Try
                        .SelectedValue = IIf(CInt(Session("Employeeunkid")) <= 0, 0, CInt(Session("Employeeunkid"))).ToString
                    Catch ex As Exception
                        .SelectedValue = CStr(0)
                    End Try
                End With
                BtnAssign.Visible = False
            Else
                Session("ShowPending") = Nothing
                BtnAssign.Visible = True
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
                    dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                    'Shani(24-Aug-2015) -- End
                    With drpEmployee
                        .DataValueField = "employeeunkid"
                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        '.DataTextField = "employeename"
                        .DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        .DataSource = dsCombos.Tables("Employee")
                        .DataBind()
                        Try
                            .SelectedValue = IIf(CInt(Session("Employeeunkid")) <= 0, 0, CInt(Session("Employeeunkid"))).ToString
                        Catch ex As Exception
                            .SelectedValue = CStr(0)
                        End Try
                    End With
                Else
                    Dim objglobalassess = New GlobalAccess
                    objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                    With drpEmployee
                        .DataSource = objglobalassess.ListOfEmployee
                        .DataTextField = "loginname"
                        .DataValueField = "employeeunkid"
                        .DataBind()
                        Try
                            .SelectedValue = IIf(CInt(Session("Employeeunkid")) <= 0, 0, CInt(Session("Employeeunkid"))).ToString
                        Catch ex As Exception
                            .SelectedValue = CStr(0)
                        End Try
                    End With
                End If
            End If
            dgView.DataSource = Nothing : dgView.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [ 16 JAN 2014 ] -- END

#End Region

#Region "ToolBar Event"


    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.

    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try

    '        'Pinkal (22-Nov-2012) -- Start
    '        'Enhancement : TRA Changes

    '        If Session("LoginBy") = Global.User.en_loginby.User Then
    '            If CBool(Session("AddEmployeeAssets")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                Exit Sub
    '            End If
    '        End If

    '        'Pinkal (22-Nov-2012) -- End`

    '        Response.Redirect("~\HR\wPg_EmployeeAsset.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblpopupHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblpopupHeader.Text)
            'Nilay (01-Feb-2015) -- End

            Me.LblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.LblAsset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblAsset.ID, Me.LblAsset.Text)
            Me.lblAssetNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAssetNo.ID, Me.lblAssetNo.Text)
            Me.LblDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblDate.ID, Me.LblDate.Text)
            Me.LblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblStatus.ID, Me.LblStatus.Text)
            Me.lblCodition.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCodition.ID, Me.lblCodition.Text)
            Me.BtnAssign.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnAssign.ID, Me.BtnAssign.Text).Replace("&", "")

            dgView.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(0).FooterText, dgView.Columns(0).HeaderText).Replace("&", "")
            dgView.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(1).FooterText, dgView.Columns(1).HeaderText).Replace("&", "")
            dgView.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(2).FooterText, dgView.Columns(2).HeaderText).Replace("&", "")
            dgView.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(3).FooterText, dgView.Columns(3).HeaderText).Replace("&", "")
            dgView.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(4).FooterText, dgView.Columns(4).HeaderText)
            dgView.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(5).FooterText, dgView.Columns(5).HeaderText)
            dgView.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(6).FooterText, dgView.Columns(6).HeaderText)
            dgView.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(7).FooterText, dgView.Columns(7).HeaderText)
            dgView.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(8).FooterText, dgView.Columns(8).HeaderText)
            dgView.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(9).FooterText, dgView.Columns(9).HeaderText)
            dgView.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(10).FooterText, dgView.Columns(10).HeaderText)
            dgView.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(11).FooterText, dgView.Columns(11).HeaderText)

            'Language.setLanguage(mstrModuleName1)
            'Me.lblChangeStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, Me.lblChangeStatus.Text)
            Me.lblStatusAsset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "LblAsset", Me.lblStatusAsset.Text)
            Me.lblStatusEmp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "LblEmployee", Me.lblStatusAsset.Text)
            Me.lblAssetStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "LblStatus", Me.lblStatusAsset.Text)
            Me.lblStatusRemarks.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "lblCodition", Me.lblStatusRemarks.Text)
            Me.btnStatusSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "btnSave", Me.btnStatusSave.Text).Replace("&", "")
            Me.btnStatusClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "btnClose", Me.btnStatusClose.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- End


End Class
