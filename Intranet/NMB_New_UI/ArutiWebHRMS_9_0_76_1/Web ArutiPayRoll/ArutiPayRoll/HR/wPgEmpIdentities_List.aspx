﻿<%@ Page Title="Employee Identity List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPgEmpIdentities_List.aspx.vb" Inherits="wPgEmpIdentities_List" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%;">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Identity List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkShowPending" runat="server" CssClass="filled-in" Text="Show Pending Employee"
                                            AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpEmployee" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblIdType" runat="server" Text="Id. types" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboIdType" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnNew" runat="server" CssClass="btn btn-primary" Text="New" />
                                <asp:Button ID="BtnSearch" runat="server" CssClass="btn btn-default" Text="Search" />
                                <asp:Button ID="BtnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                <asp:Panel ID="objtblPanel" runat="server" Style="float: left;">
                                    <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btn btn-primary" Text="View Detail" />
                                    <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" CssClass="label label-primary bg-pw"></asp:Label>
                                    <asp:Label ID="lblParentData" runat="server" Text="Parent Detail" CssClass="label label-danger bg-lc"></asp:Label>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive">
                                    <asp:DataGrid ID="dgvIdentity" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                        AllowPaging="false">
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgSelect" runat="server" ToolTip="Select" CommandName="Select">
                                                            
                                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                            
                                                        </asp:LinkButton>
                                                        <%--'Gajanan [22-Feb-2019] -- Start--%>
                                                        <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                        <asp:LinkButton ID="imgDetail" runat="server" ToolTip="Highlight Original Details"
                                                            CommandName="View" Visible="false"><i class="fas fa-eye" ></i> </asp:LinkButton>
                                                        <asp:HiddenField ID="hfoprationtypeid" runat="server" Value='<%#Eval("operationtypeid") %>' />
                                                        <%--'Gajanan [22-Feb-2019] -- End--%>
                                                    </span>
                                                    <%-- <asp:ImageButton ID="ImgSelect" runat="server" ImageUrl="~/images/edit.png" ToolTip="Select"
                                                        CommandName="Select" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--0--%>
                                            <asp:TemplateColumn HeaderStyle-Width="25px" ItemStyle-Width="25px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                            <i class="fas fa-trash text-danger"></i>
                                                        </asp:LinkButton>
                                                    </span>
                                                    <%--  <asp:ImageButton ID="ImgDelete" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                        CommandName="Delete" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--1--%>
                                            <asp:BoundColumn DataField="identities" HeaderText="Identity Type" ReadOnly="True"
                                                FooterText="colhIdType"></asp:BoundColumn>
                                            <%--2--%>
                                            <asp:BoundColumn DataField="serial_no" HeaderText="Serial No" ReadOnly="True" FooterText="colhIdSerialNo">
                                            </asp:BoundColumn>
                                            <%--3--%>
                                            <asp:BoundColumn DataField="identity_no" HeaderText="Identity No" ReadOnly="True"
                                                FooterText="colhIdNo"></asp:BoundColumn>
                                            <%--4--%>
                                            <asp:BoundColumn DataField="country" HeaderText="Country" ReadOnly="True" FooterText="colhIssueCountry">
                                            </asp:BoundColumn>
                                            <%--5--%>
                                            <asp:BoundColumn DataField="issued_place" HeaderText="Place of Issue" ReadOnly="True"
                                                FooterText="colhPlaceOfIssue"></asp:BoundColumn>
                                            <%--6--%>
                                            <asp:BoundColumn DataField="issue_date" HeaderText="IssueDate" ReadOnly="True" FooterText="colhIssueDate">
                                            </asp:BoundColumn>
                                            <%--7--%>
                                            <asp:BoundColumn DataField="expiry_date" HeaderText="ExpiryDate" ReadOnly="True"
                                                FooterText="colhExpiryDate"></asp:BoundColumn>
                                            <%--8--%>
                                            <asp:BoundColumn DataField="isdefault" HeaderText="isdefault" ReadOnly="True" Visible="false">
                                            </asp:BoundColumn>
                                            <%--9--%>
                                            <%--'Gajanan [22-Feb-2019] -- Start--%>
                                            <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                            <asp:BoundColumn DataField="operationtypeid" HeaderText="operationtypeid" ReadOnly="true"
                                                Visible="false"></asp:BoundColumn>
                                            <%--10--%>
                                            <asp:BoundColumn DataField="operationtype" HeaderText="Operation Type" ReadOnly="true">
                                            </asp:BoundColumn>
                                            <%--11--%>
                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" ReadOnly="true" Visible="false">
                                            </asp:BoundColumn>
                                            <%--12--%>
                                            <asp:BoundColumn DataField="identitytranunkid" HeaderText="identitytranunkid" Visible="false">
                                            </asp:BoundColumn>
                                            <%--<13>--%>
                                            <asp:BoundColumn DataField="employeeunkid" HeaderText="employeeunkid" Visible="false">
                                            </asp:BoundColumn>
                                            <%--<14>--%>
                                            <%--'Gajanan [22-Feb-2019] -- End--%>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are you Sure You Want To delete?:" />
                <%--'Gajanan [22-Feb-2019] -- Start--%>
                <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                <%--'Gajanan [22-Feb-2019] -- End--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
