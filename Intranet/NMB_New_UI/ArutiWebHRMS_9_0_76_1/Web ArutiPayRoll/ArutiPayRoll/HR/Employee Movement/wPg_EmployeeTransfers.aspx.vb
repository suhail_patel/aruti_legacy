﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Drawing

#End Region

Partial Class HR_wPg_EmployeeTransfers
    Inherits Basepage

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmEmployeeTransfers"
    Dim DisplayMessage As New CommonCodes


    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private objETransfers As New clsemployee_transfer_tran
    'Gajanan [4-Sep-2020] -- End

    'Gajanan [12-NOV-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}

    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private objATransfers As New clsTransfer_Approval_Tran
    'Gajanan [4-Sep-2020] -- End

    'Gajanan [12-NOV-2018] -- END
    Private mdtAppointmentDate As New Date
    Private mAction As enAction = enAction.ADD_CONTINUE
    Private xCurrentAllocation As Dictionary(Of Integer, String)
    Private mintTransferunkid As Integer = -1
    Private mstrEmployeeCode As String = ""

    'Gajanan [15-NOV-2018] -- START
    Private mintTransactionId As Integer = 0
    'Gajanan [15-NOV-2018] -- END

    Private mdtTransferList As DataTable


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mstrAdvanceFilter As String = ""
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region "Page's Event"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If



            'Gajanan [12-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            objlblCaption.Visible = False
            lblPendingData.Visible = False
            'Gajanan [12-NOV-2018] -- END
            If Not IsPostBack Then
                Call SetLanguage()
                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                GC.Collect()
                'Gajanan [4-Sep-2020] -- End

                Call FillCombo()
                If Request.QueryString.Count > 0 Then
                    Dim mintEmployeeID As Integer = 0
                    Try
                        mintEmployeeID = CInt(b64decode(Server.UrlDecode(Request.QueryString.ToString.Substring(3))))
                    Catch
                        Session("clsuser") = Nothing
                        DisplayMessage.DisplayMessage("Invalid Page Address.", Me, "../../index.aspx")
                        Exit Sub
                    End Try
                    cboEmployee.SelectedValue = CStr(mintEmployeeID)
                    cboEmployee_SelectedIndexChanged(sender, e)
                End If
                Fill_Grid()
                pnldata.Visible = False

                'SHANI (13 APR 2015)-START
                If CBool(Session("IsAllocation_Hierarchy_Set")) = True Then
                    cboBranch.AutoPostBack = True
                    cboDeptGroup.AutoPostBack = True
                    cboDepartment.AutoPostBack = True
                    cboSecGroup.AutoPostBack = True
                    cboSections.AutoPostBack = True
                    cboUnitGroup.AutoPostBack = True
                    cboUnits.AutoPostBack = True
                    cboClassGroup.AutoPostBack = True
                End If

                'SHANI (13 APR 2015)-END
            Else
                mAction = CType(Me.ViewState("mAction"), enAction)
                xCurrentAllocation = CType(Me.ViewState("xCurrentAllocation"), Dictionary(Of Integer, String))
                mdtAppointmentDate = CDate(Me.ViewState("mdtAppointmentDate"))
                mintTransferunkid = CInt(Me.ViewState("mintTransferunkid"))
                mstrEmployeeCode = CStr(Me.ViewState("EmployeeCode"))
                'Gajanan [15-NOV-2018] -- START
                mintTransactionId = CInt(Me.ViewState("mintTransactionId"))
                'Gajanan [15-NOV-2018] -- End
                mdtTransferList = CType(Me.ViewState("mdtTransferList"), DataTable)

            End If

            'Shani (08-Dec-2016) -- Start
            'Enhancement -  Add Employee Allocaion/Date Privilage
            btnSaveChanges.Enabled = CBool(Session("AllowToChangeEmpTransfers"))
            'Shani (08-Dec-2016) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("mAction", mAction)
            Me.ViewState.Add("xCurrentAllocation", xCurrentAllocation)
            Me.ViewState.Add("mdtAppointmentDate", mdtAppointmentDate)
            Me.ViewState.Add("mintTransferunkid", mintTransferunkid)
            Me.ViewState.Add("EmployeeCode", mstrEmployeeCode)
            'Gajanan [15-NOV-2018] -- START
            Me.ViewState.Add("mintTransactionId", mintTransactionId)
            'Gajanan [15-NOV-2018] -- END
            Me.ViewState.Add("mdtTransferList", mdtTransferList)



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"

    Private Sub FillCombo()

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objEmployee As New clsEmployee_Master
        Dim dsComboList As New DataSet
        Dim objBranch As New clsStation
        Dim objDeptGroup As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSecGroup As New clsSectionGroup
        Dim objSections As New clsSections
        Dim objUnitGroup As New clsUnitGroup
        Dim objUnits As New clsUnits
        Dim objTeam As New clsTeams
        Dim objClassGroup As New clsClassGroup
        Dim objClass As New clsClass
        Dim objCMaster As New clsCommon_Master
        'Gajanan [4-Sep-2020] -- End

        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsComboList = objEmployee.GetEmployeeList("EmployeeList", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , , , , , )


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB


            'dsComboList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
            '                                CInt(Session("UserId")), _
            '                                CInt(Session("Fin_year")), _
            '                                CInt(Session("CompanyUnkId")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                CStr(Session("UserAccessModeSetting")), True, _
            '                                CBool(Session("IsIncludeInactiveEmp")), "EmployeeList", True, , , , , , , , , , , , , , , , True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END
            ''Shani(24-Aug-2015) -- End
            'With cboEmployee
            '    'Nilay (09-Aug-2016) -- Start
            '    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            '    '.DataTextField = "employeename"
            '    .DataTextField = "EmpCodeName"
            '    'Nilay (09-Aug-2016) -- End
            '    .DataValueField = "employeeunkid"
            '    .DataSource = dsComboList.Tables("EmployeeList")
            '    .SelectedValue = CStr(0)
            '    .DataBind()
            'End With
            FillEmployeeCombo()
            'Gajanan [11-Dec-2019] -- End


            dsComboList = objBranch.getComboList("Branch", True)
            With cboBranch
                .DataTextField = "name"
                .DataValueField = "stationunkid"
                .DataSource = dsComboList.Tables("Branch")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objDeptGroup.getComboList("DeptGroup", True)
            With cboDeptGroup
                .DataTextField = "name"
                .DataValueField = "deptgroupunkid"
                .DataSource = dsComboList.Tables("DeptGroup")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objDepartment.getComboList("Department", True)
            With cboDepartment
                .DataTextField = "name"
                .DataValueField = "departmentunkid"
                .DataSource = dsComboList.Tables("Department")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objSecGroup.getComboList("SectionGroup", True)
            With cboSecGroup
                .DataTextField = "name"
                .DataValueField = "sectiongroupunkid"
                .DataSource = dsComboList.Tables("SectionGroup")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objSections.getComboList("Sections", True)
            With cboSections
                .DataTextField = "name"
                .DataValueField = "sectionunkid"
                .DataSource = dsComboList.Tables("Sections")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objUnitGroup.getComboList("UnitGroup", True)
            With cboUnitGroup
                .DataTextField = "name"
                .DataValueField = "unitgroupunkid"
                .DataSource = dsComboList.Tables("UnitGroup")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objUnits.getComboList("Units", True)
            With cboUnits
                .DataTextField = "name"
                .DataValueField = "unitunkid"
                .DataSource = dsComboList.Tables("Units")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objTeam.getComboList("Team", True)
            With cboTeam
                .DataTextField = "name"
                .DataValueField = "teamunkid"
                .DataSource = dsComboList.Tables("Team")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objClassGroup.getComboList("ClassGroup", True)
            With cboClassGroup
                .DataTextField = "name"
                .DataValueField = "classgroupunkid"
                .DataSource = dsComboList.Tables("ClassGroup")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objClass.getComboList("Class", True)
            With cboClass
                .DataTextField = "name"
                .DataValueField = "classesunkid"
                .DataSource = dsComboList.Tables("Class")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRANSFERS, True, "List")
            With cboChangeReason
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        Finally

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objEmployee = Nothing : objBranch = Nothing : objDeptGroup = Nothing
            objDepartment = Nothing : objSecGroup = Nothing : objSections = Nothing
            objUnitGroup = Nothing : objUnits = Nothing : objTeam = Nothing
            objClassGroup = Nothing : objClass = Nothing : objCMaster = Nothing

            If IsNothing(dsComboList) = False Then
                dsComboList.Clear()
                dsComboList = Nothing
            End If
            'Gajanan [4-Sep-2020] -- End

        End Try
    End Sub

    Private Sub ClearControls()
        Try
            dtEffectiveDate.SetDate = Nothing
            cboBranch.SelectedValue = CStr(0)
            cboDeptGroup.SelectedValue = CStr(0)
            cboDepartment.SelectedValue = CStr(0)
            cboSecGroup.SelectedValue = CStr(0)
            cboSections.SelectedValue = CStr(0)
            cboUnitGroup.SelectedValue = CStr(0)
            cboUnits.SelectedValue = CStr(0)
            cboTeam.SelectedValue = CStr(0)
            cboClassGroup.SelectedValue = CStr(0)
            cboClass.SelectedValue = CStr(0)
            cboChangeReason.SelectedValue = CStr(0)
            pnldata.Visible = False
            mdtAppointmentDate = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsData As New DataSet

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objETransfers As New clsemployee_transfer_tran
        Dim objATransfers As New clsTransfer_Approval_Tran
        'Gajanan [4-Sep-2020] -- End


        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                dsData = objETransfers.GetList("List", CInt(cboEmployee.SelectedValue)).Clone
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}                
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                'dgTransfersList.Columns(0).Visible = False
                'dgTransfersList.Columns(1).Visible = False
                dgTransfersList.Columns(1).Visible = False
                dgTransfersList.Columns(2).Visible = False
                'Varsha Rana (17-Oct-2017) -- End
                dgTransfersList.Columns(0).Visible = False
                'Gajanan [12-NOV-2018] -- END
            Else
                dsData = objETransfers.GetList("List", CInt(cboEmployee.SelectedValue))
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                'dgTransfersList.Columns(0).Visible = CBool(Session("AllowToEditTransferEmployeeDetails"))
                'dgTransfersList.Columns(1).Visible = CBool(Session("AllowToDeleteTransferEmployeeDetails"))

                dgTransfersList.Columns(1).Visible = CBool(Session("AllowToEditTransferEmployeeDetails"))
                dgTransfersList.Columns(2).Visible = CBool(Session("AllowToDeleteTransferEmployeeDetails"))
                'Varsha Rana (17-Oct-2017) -- End


                dgTransfersList.Columns(0).Visible = Not CBool(Session("SkipEmployeeMovementApprovalFlow"))


                Dim dcol As New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "tranguid"
                    .DefaultValue = ""
                End With
                dsData.Tables(0).Columns.Add(dcol)

                'S.SANDEEP |17-JAN-2019| -- START
                dcol = New DataColumn
                With dcol
                    .DataType = GetType(System.Int32)
                    .ColumnName = "operationtypeid"
                    .DefaultValue = clsEmployeeMovmentApproval.enOperationType.ADDED
                End With
                dsData.Tables(0).Columns.Add(dcol)

                dcol = New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "OperationType"
                    .DefaultValue = ""
                End With
                dsData.Tables(0).Columns.Add(dcol)
                'S.SANDEEP |17-JAN-2019| -- End


                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim dsPending As New DataSet
                    dsPending = objATransfers.GetList("List", CInt(cboEmployee.SelectedValue))
                    If dsPending.Tables(0).Rows.Count > 0 Then
                        For Each row As DataRow In dsPending.Tables(0).Rows
                            dsData.Tables(0).ImportRow(row)
                        Next
                    End If
                End If
                'Gajanan [12-NOV-2018] -- END

            End If
            dgTransfersList.AutoGenerateColumns = False
            dgTransfersList.DataSource = dsData.Tables(0)
            dgTransfersList.DataBind()
            mdtTransferList = dsData.Tables(0)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objATransfers = Nothing
            objETransfers = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

    Private Sub Set_Transfers(ByVal objETransfers As clsemployee_transfer_tran)
        Try
            Dim dsTransfer As New DataSet
            dsTransfer = objETransfers.Get_Current_Allocation(Now.Date, CInt(cboEmployee.SelectedValue))
            If dsTransfer.Tables(0).Rows.Count > 0 Then


                If IsNothing(cboBranch) = False Then
                    cboBranch.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("stationunkid").ToString()
                End If
                If IsNothing(cboDeptGroup) = False Then
                    cboDeptGroup.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("deptgroupunkid").ToString()
                End If
                If IsNothing(cboDepartment) = False Then
                    cboDepartment.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("departmentunkid").ToString()
                End If
                If IsNothing(cboSecGroup) = False Then
                    cboSecGroup.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("sectiongroupunkid").ToString()
                End If
                If IsNothing(cboSections) = False Then
                    cboSections.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("sectionunkid").ToString()
                End If
                If IsNothing(cboUnitGroup) = False Then
                    cboUnitGroup.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("unitgroupunkid").ToString()
                End If
                If IsNothing(cboUnits) = False Then
                    cboUnits.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("unitunkid").ToString()
                End If
                If IsNothing(cboTeam) = False Then
                    cboTeam.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("teamunkid").ToString()
                End If
                If IsNothing(cboClassGroup) = False Then
                    cboClassGroup.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("classgroupunkid").ToString()
                End If

                Call cboClassGroup_SelectedIndexChanged(cboClassGroup, New EventArgs)
                cboClass.SelectedValue = dsTransfer.Tables(0).Rows(0).Item("classunkid").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetEditValue(ByVal objETransfers As clsemployee_transfer_tran)
        Try
            dtEffectiveDate.SetDate = objETransfers._Effectivedate
            cboEmployee.SelectedValue = objETransfers._Employeeunkid.ToString()

            'SHANI (13 APR 2015)-START
            'cboBranch.SelectedValue = objETransfers._Stationunkid
            'cboDeptGroup.SelectedValue = objETransfers._Deptgroupunkid
            'cboDepartment.SelectedValue = objETransfers._Departmentunkid
            'cboSecGroup.SelectedValue = objETransfers._Sectiongroupunkid
            'cboSections.SelectedValue = objETransfers._Sectionunkid
            'cboUnitGroup.SelectedValue = objETransfers._Unitgroupunkid
            'cboUnits.SelectedValue = objETransfers._Unitunkid
            'cboTeam.SelectedValue = objETransfers._Teamunkid
            'cboClassGroup.SelectedValue = objETransfers._Classgroupunkid
            'cboClass.SelectedValue = objETransfers._Classunkid
            Try
                cboBranch.SelectedValue = objETransfers._Stationunkid.ToString()
                If CBool(Session("IsAllocation_Hierarchy_Set")) = True Then
                    Call cboBranch_SelectedIndexChanged(cboBranch, Nothing)
                End If
            Catch ex As Exception
                cboBranch.SelectedValue = CStr(0)
            End Try
            cboBranch.SelectedValue = objETransfers._Stationunkid.ToString()
            Try
                cboDeptGroup.SelectedValue = objETransfers._Deptgroupunkid.ToString()
                If CBool(Session("IsAllocation_Hierarchy_Set")) = True Then
                    cboDeptGroup_SelectedIndexChanged(cboDeptGroup, Nothing)
                End If
            Catch ex As Exception
                cboDeptGroup.SelectedValue = CStr(0)
            End Try
            Try
                cboDepartment.SelectedValue = objETransfers._Departmentunkid.ToString()
                If CBool(Session("IsAllocation_Hierarchy_Set")) = True Then
                    Call cboDepartment_SelectedIndexChanged(cboDepartment, Nothing)
                End If
            Catch ex As Exception
                cboDepartment.SelectedValue = CStr(0)
            End Try
            Try
                cboSecGroup.SelectedValue = objETransfers._Sectiongroupunkid.ToString()
                If CBool(Session("IsAllocation_Hierarchy_Set")) = True Then
                    Call cboSecGroup_SelectedIndexChanged(cboSecGroup, Nothing)
                End If
            Catch ex As Exception
                cboSecGroup.SelectedValue = CStr(0)
            End Try
            Try
                cboSections.SelectedValue = objETransfers._Sectionunkid.ToString()
                If CBool(Session("IsAllocation_Hierarchy_Set")) = True Then
                    Call cboSections_SelectedIndexChanged(cboSections, Nothing)
                End If
            Catch ex As Exception
                cboSections.SelectedValue = CStr(0)
            End Try
            Try
                cboUnitGroup.SelectedValue = objETransfers._Unitgroupunkid.ToString()
                If CBool(Session("IsAllocation_Hierarchy_Set")) = True Then
                    Call cboUnitGroup_SelectedIndexChanged(cboUnitGroup, Nothing)
                End If
            Catch ex As Exception
                cboUnitGroup.SelectedValue = CStr(0)
            End Try
            Try
                cboUnits.SelectedValue = objETransfers._Unitunkid.ToString()
                If CBool(Session("IsAllocation_Hierarchy_Set")) = True Then
                    Call cboUnits_SelectedIndexChanged(cboUnits, Nothing)
                End If
            Catch ex As Exception
                cboUnits.SelectedValue = CStr(0)
            End Try
            Try
                cboTeam.SelectedValue = objETransfers._Teamunkid.ToString()
            Catch ex As Exception
                cboTeam.SelectedValue = CStr(0)
            End Try

            Try
                cboClassGroup.SelectedValue = objETransfers._Classgroupunkid.ToString()
                Call cboClassGroup_SelectedIndexChanged(cboClassGroup, Nothing)
            Catch ex As Exception
                cboClassGroup.SelectedValue = CStr(0)
            End Try
            Try
                cboClass.SelectedValue = objETransfers._Classunkid.ToString()
            Catch ex As Exception
                cboClass.SelectedValue = CStr(0)
            End Try
            'SHANI (13 APR 2015)--END 
            cboChangeReason.SelectedValue = objETransfers._Changereasonunkid.ToString()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function Valid_Transfer(ByVal objETransfers As clsemployee_transfer_tran) As Boolean
        Try
            If dtEffectiveDate.IsNull = True Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue."), Me)
                dtEffectiveDate.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, Employee is mandatory information. Please selecte Employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboBranch.SelectedValue) <= 0 AndAlso _
               CInt(cboDeptGroup.SelectedValue) <= 0 AndAlso _
               CInt(cboDepartment.SelectedValue) <= 0 AndAlso _
               CInt(cboSecGroup.SelectedValue) <= 0 AndAlso _
               CInt(cboSections.SelectedValue) <= 0 AndAlso _
               CInt(cboUnitGroup.SelectedValue) <= 0 AndAlso _
               CInt(cboUnits.SelectedValue) <= 0 AndAlso _
               CInt(cboTeam.SelectedValue) <= 0 AndAlso _
               CInt(cboClassGroup.SelectedValue) <= 0 AndAlso _
               CInt(cboClass.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, Allocations are mandatory information. Please selecte atleast one Allocation to continue."), Me)
                Return False
            End If

            'S.SANDEEP [18-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Department Was not Mandatory}
            If CInt(cboDepartment.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Sorry, Department is mandatory information. Please select Department to continue."), Me)
                Return False
            End If
            'S.SANDEEP [18-May-2018] -- END

            If CInt(cboChangeReason.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, Change Reason is mandatory information. Please selecte Change Reason to continue."), Me)
                cboChangeReason.Focus()
                Return False
            End If

            If mdtAppointmentDate <> Nothing Then
                If mdtAppointmentDate <> dtEffectiveDate.GetDate Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, This is system generate entry. Please set effective date as employee appointment date."), Me)
                    dtEffectiveDate.Focus()
                    Return False
                End If
            End If


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If objETransfers.isExist(dtEffectiveDate.GetDate(), CInt(cboBranch.SelectedValue), CInt(cboDeptGroup.SelectedValue), _
                   CInt(cboDepartment.SelectedValue), CInt(cboSecGroup.SelectedValue), CInt(cboSections.SelectedValue), _
                   CInt(cboUnitGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboTeam.SelectedValue), CInt(cboClassGroup.SelectedValue), CInt(cboClass.SelectedValue), CInt(cboEmployee.SelectedValue), 0) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_transfer_tran", 2, "Sorry, transfer information is already present for the selected combination for the selected effective date."), Me)
                Return False
            End If
            'Gajanan [11-Dec-2019] -- End


            'Gajanan [12-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                Dim strMsg As String = String.Empty
                Dim objPMovement As New clsEmployeeMovmentApproval
                strMsg = objPMovement.IsOtherMovmentApproverPresent(clsEmployeeMovmentApproval.enMovementType.TRANSFERS, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), 1192, CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString, Nothing, CInt(cboEmployee.SelectedValue), Nothing)
                If strMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(strMsg, Me)
                    objPMovement = Nothing
                    Return False
                End If
                objPMovement = Nothing

                'S.SANDEEP |16-JAN-2019| -- START
                If objETransfers.isExist(dtEffectiveDate.GetDate(), -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, CInt(cboEmployee.SelectedValue), mintTransactionId) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_transfer_tran", 3, "Sorry, transfer information is already present for the selected effective date."), Me)
                    Return False
                End If

                Dim dsList As New DataSet
                dsList = objETransfers.Get_Current_Allocation(dtEffectiveDate.GetDate(), CInt(cboEmployee.SelectedValue))
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    If CInt(dsList.Tables(0).Rows(0)("stationunkid")) = CInt(cboBranch.SelectedValue) AndAlso CInt(dsList.Tables(0).Rows(0)("deptgroupunkid")) = CInt(cboDeptGroup.SelectedValue) _
                      AndAlso CInt(dsList.Tables(0).Rows(0)("departmentunkid")) = CInt(cboDepartment.SelectedValue) AndAlso CInt(dsList.Tables(0).Rows(0)("sectiongroupunkid")) = CInt(cboSecGroup.SelectedValue) _
                      AndAlso CInt(dsList.Tables(0).Rows(0)("sectionunkid")) = CInt(cboSections.SelectedValue) AndAlso CInt(dsList.Tables(0).Rows(0)("unitgroupunkid")) = CInt(cboUnitGroup.SelectedValue) _
                      AndAlso CInt(dsList.Tables(0).Rows(0)("unitunkid")) = CInt(cboUnits.SelectedValue) AndAlso CInt(dsList.Tables(0).Rows(0)("teamunkid")) = CInt(cboTeam.SelectedValue) _
                      AndAlso CInt(dsList.Tables(0).Rows(0)("classgroupunkid")) = CInt(cboClassGroup.SelectedValue) AndAlso CInt(dsList.Tables(0).Rows(0)("classunkid")) = CInt(cboClass.SelectedValue) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_transfer_tran", 2, "Sorry, transfer information is already present for the selected combination for the selected effective date."), Me)
                        Return False
                    End If
                End If
                dsList = Nothing

                If objETransfers.isExist(dtEffectiveDate.GetDate(), CInt(cboBranch.SelectedValue), CInt(cboDeptGroup.SelectedValue), _
                   CInt(cboDepartment.SelectedValue), CInt(cboSecGroup.SelectedValue), CInt(cboSections.SelectedValue), _
                   CInt(cboUnitGroup.SelectedValue), CInt(cboUnits.SelectedValue), CInt(cboTeam.SelectedValue), CInt(cboClassGroup.SelectedValue), CInt(cboClass.SelectedValue), CInt(cboEmployee.SelectedValue), mintTransactionId) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_transfer_tran", 2, "Sorry, transfer information is already present for the selected combination for the selected effective date."), Me)
                    Return False
                End If
                'S.SANDEEP |16-JAN-2019| -- END

            End If
            'Gajanan [12-NOV-2018] -- End

            'Pinkal (18-Mar-2021) -- Start 
            'NMB Enhancmenet : AD Enhancement for NMB.
            If CBool(Session("CreateADUserFromEmpMst")) Then
                Dim objAttribute As New clsADAttribute_mapping
                Dim dtTable As DataTable = objAttribute.GetList("List", CInt(Session("CompanyUnkId")), True, Nothing, "attributename = '" & clsADAttribute_mapping.enAttributes.Transfer_Allocation.ToString().Replace("_", " ") & "'")
                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    Dim mintAllocationId As Integer = CInt(dtTable.Rows(0)("mappingunkid"))

                    Select Case mintAllocationId

                        Case CInt(enAllocation.BRANCH)

                            If CInt(cboBranch.SelectedValue) <= 0 Then
                                DisplayMessage.DisplayMessage(lblBranch.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), Me)
                                cboBranch.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.DEPARTMENT_GROUP)

                            If CInt(cboDeptGroup.SelectedValue) <= 0 Then
                                DisplayMessage.DisplayMessage(lblDepartmentGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), Me)
                                cboDeptGroup.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.DEPARTMENT)

                            If CInt(cboDepartment.SelectedValue) <= 0 Then
                                DisplayMessage.DisplayMessage(lblDepartment.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), Me)
                                cboDepartment.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.SECTION_GROUP)

                            If CInt(cboSecGroup.SelectedValue) <= 0 Then
                                DisplayMessage.DisplayMessage(lblSectionGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), Me)
                                cboSecGroup.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.SECTION)

                            If CInt(cboSections.SelectedValue) <= 0 Then
                                DisplayMessage.DisplayMessage(lblSection.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), Me)
                                cboSections.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.UNIT_GROUP)

                            If CInt(cboUnitGroup.SelectedValue) <= 0 Then
                                DisplayMessage.DisplayMessage(lblUnitGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), Me)
                                cboUnitGroup.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.UNIT)

                            If CInt(cboUnits.SelectedValue) <= 0 Then
                                DisplayMessage.DisplayMessage(lblUnits.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), Me)
                                cboUnits.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.TEAM)

                            If CInt(cboTeam.SelectedValue) <= 0 Then
                                DisplayMessage.DisplayMessage(lblTeam.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), Me)
                                cboTeam.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.CLASS_GROUP)

                            If CInt(cboClassGroup.SelectedValue) <= 0 Then
                                DisplayMessage.DisplayMessage(lblClassGroup.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), Me)
                                cboClassGroup.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                        Case CInt(enAllocation.CLASSES)

                            If CInt(cboClass.SelectedValue) <= 0 Then
                                DisplayMessage.DisplayMessage(lblClass.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "is compulsory information.Reason : This Allocation is mapped as Transfer Allocation on configuration."), Me)
                                cboClass.Focus()
                                objAttribute = Nothing
                                Return False
                            End If

                    End Select

                End If
                objAttribute = Nothing
            End If
            'Pinkal (18-Mar-2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
        Return True
    End Function

    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    Private Sub SetValue(ByVal objETransfers As clsemployee_transfer_tran, ByVal objATransfers As clsTransfer_Approval_Tran)
        'Gajanan [4-Sep-2020] -- End
        Try
            'Gajanan [12-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'objETransfers._Transferunkid = mintTransferunkid
            'objETransfers._Effectivedate = dtEffectiveDate.GetDate
            'objETransfers._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'objETransfers._Stationunkid = CInt(cboBranch.SelectedValue)
            'objETransfers._Deptgroupunkid = CInt(cboDeptGroup.SelectedValue)
            'objETransfers._Departmentunkid = CInt(cboDepartment.SelectedValue)
            'objETransfers._Sectiongroupunkid = CInt(cboSecGroup.SelectedValue)
            'objETransfers._Sectionunkid = CInt(cboSections.SelectedValue)
            'objETransfers._Unitgroupunkid = CInt(cboUnitGroup.SelectedValue)
            'objETransfers._Unitunkid = CInt(cboUnits.SelectedValue)
            'objETransfers._Teamunkid = CInt(cboTeam.SelectedValue)
            'objETransfers._Classgroupunkid = CInt(cboClassGroup.SelectedValue)
            'objETransfers._Classunkid = CInt(cboClass.SelectedValue)
            'objETransfers._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
            'objETransfers._Isvoid = False
            'objETransfers._Statusunkid = 0
            'objETransfers._Userunkid = CInt(Session("UserId"))
            'objETransfers._Voiddatetime = Nothing
            'objETransfers._Voidreason = ""
            'objETransfers._Voiduserunkid = -1

            'Blank_ModuleName()
            'objETransfers._WebFormName = mstrModuleName
            'objETransfers._WebClientIP = CStr(Session("IP_ADD"))
            'objETransfers._WebHostName = CStr(Session("HOST_NAME"))
            'StrModuleName2 = "mnuGeneralMaster"
            'StrModuleName3 = "mnuCoreSetups"

            'Gajanan [15-NOV-2018] -- START
            'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
            'S.SANDEEP |17-JAN-2019| -- START
            'If CBool(Session("SkipEmployeeMovementApprovalFlow")) Or mintTransactionId > 0 Then
            If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                'S.SANDEEP |17-JAN-2019| -- END

                'Gajanan [15-NOV-2018] -- End
                objETransfers._Transferunkid = mintTransferunkid
                objETransfers._Effectivedate = dtEffectiveDate.GetDate
                objETransfers._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objETransfers._Stationunkid = CInt(cboBranch.SelectedValue)
                objETransfers._Deptgroupunkid = CInt(cboDeptGroup.SelectedValue)
                objETransfers._Departmentunkid = CInt(cboDepartment.SelectedValue)
                objETransfers._Sectiongroupunkid = CInt(cboSecGroup.SelectedValue)
                objETransfers._Sectionunkid = CInt(cboSections.SelectedValue)
                objETransfers._Unitgroupunkid = CInt(cboUnitGroup.SelectedValue)
                objETransfers._Unitunkid = CInt(cboUnits.SelectedValue)
                objETransfers._Teamunkid = CInt(cboTeam.SelectedValue)
                objETransfers._Classgroupunkid = CInt(cboClassGroup.SelectedValue)
                objETransfers._Classunkid = CInt(cboClass.SelectedValue)
                objETransfers._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                objETransfers._Isvoid = False
                objETransfers._Statusunkid = 0
                objETransfers._Userunkid = CInt(Session("UserId"))
                objETransfers._Voiddatetime = Nothing
                objETransfers._Voidreason = ""
                objETransfers._Voiduserunkid = -1

                Blank_ModuleName()
                objETransfers._WebFormName = mstrModuleName
                objETransfers._WebClientIP = CStr(Session("IP_ADD"))
                objETransfers._WebHostName = CStr(Session("HOST_NAME"))
                StrModuleName2 = "mnuGeneralMaster"
                StrModuleName3 = "mnuCoreSetups"
            Else
                objATransfers._Audittype = enAuditType.ADD
                objATransfers._Audituserunkid = CInt(Session("UserId"))
                objATransfers._Effectivedate = dtEffectiveDate.GetDate
                objATransfers._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objATransfers._Stationunkid = CInt(cboBranch.SelectedValue)
                objATransfers._Deptgroupunkid = CInt(cboDeptGroup.SelectedValue)
                objATransfers._Departmentunkid = CInt(cboDepartment.SelectedValue)
                objATransfers._Sectiongroupunkid = CInt(cboSecGroup.SelectedValue)
                objATransfers._Sectionunkid = CInt(cboSections.SelectedValue)
                objATransfers._Unitgroupunkid = CInt(cboUnitGroup.SelectedValue)
                objATransfers._Unitunkid = CInt(cboUnits.SelectedValue)
                objATransfers._Teamunkid = CInt(cboTeam.SelectedValue)
                objATransfers._Classgroupunkid = CInt(cboClassGroup.SelectedValue)
                objATransfers._Classunkid = CInt(cboClass.SelectedValue)
                objATransfers._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                objATransfers._Isvoid = False
                objATransfers._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                objATransfers._Voiddatetime = Nothing
                objATransfers._Voidreason = ""
                objATransfers._Voiduserunkid = -1
                objATransfers._Tranguid = Guid.NewGuid.ToString()
                objATransfers._Transactiondate = Now
                objATransfers._Remark = ""
                objATransfers._Rehiretranunkid = 0
                objATransfers._Mappingunkid = 0
                objATransfers._Isweb = True
                objATransfers._Isfinal = False
                objATransfers._Ip = Session("IP_ADD").ToString()
                objATransfers._Hostname = Session("HOST_NAME").ToString()
                objATransfers._Form_Name = mstrModuleName
                'S.SANDEEP |17-JAN-2019| -- START
                If mAction = enAction.EDIT_ONE Then
                    objATransfers._Transferunkid = mintTransactionId
                    objATransfers._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    objATransfers._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                'S.SANDEEP |17-JAN-2019| -- END


                Blank_ModuleName()
                StrModuleName2 = "mnuGeneralMaster"
                StrModuleName3 = "mnuCoreSetups"
            End If

            'Gajanan [12-NOV-2018] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetAllocationForMail()
        Try
            xCurrentAllocation = New Dictionary(Of Integer, String)
            '''''''''''' BRANCH
            If CInt(cboBranch.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.BRANCH, cboBranch.Text)
            Else
                xCurrentAllocation.Add(enAllocation.BRANCH, "&nbsp;")
            End If

            '''''''''''' DEPARTMENT GROUP
            If CInt(cboDeptGroup.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.DEPARTMENT_GROUP, cboDeptGroup.Text)
            Else
                xCurrentAllocation.Add(enAllocation.DEPARTMENT_GROUP, "&nbsp;")
            End If

            '''''''''''' DEPARTMENT
            If CInt(cboDepartment.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.DEPARTMENT, cboDepartment.Text)
            Else
                xCurrentAllocation.Add(enAllocation.DEPARTMENT, "&nbsp;")
            End If

            '''''''''''' SECTION GROUP
            If CInt(cboSecGroup.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.SECTION_GROUP, cboSecGroup.Text)
            Else
                xCurrentAllocation.Add(enAllocation.SECTION_GROUP, "&nbsp;")
            End If

            '''''''''''' SECTION
            If CInt(cboSections.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.SECTION, cboSections.Text)
            Else
                xCurrentAllocation.Add(enAllocation.SECTION, "&nbsp;")
            End If

            '''''''''''' UNIT GROUP
            If CInt(cboUnitGroup.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.UNIT_GROUP, cboUnitGroup.Text)
            Else
                xCurrentAllocation.Add(enAllocation.UNIT_GROUP, "&nbsp;")
            End If

            '''''''''''' UNIT
            If CInt(cboUnits.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.UNIT, cboUnits.Text)
            Else
                xCurrentAllocation.Add(enAllocation.UNIT, "&nbsp;")
            End If

            '''''''''''' TEAM
            If CInt(cboTeam.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.TEAM, cboTeam.Text)
            Else
                xCurrentAllocation.Add(enAllocation.TEAM, "&nbsp;")
            End If

            '''''''''''' CLASS GROUP
            If CInt(cboClassGroup.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.CLASS_GROUP, cboClassGroup.Text)
            Else
                xCurrentAllocation.Add(enAllocation.CLASS_GROUP, "&nbsp;")
            End If

            '''''''''''' CLASSES
            If CInt(cboClass.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.CLASSES, cboClass.Text)
            Else
                xCurrentAllocation.Add(enAllocation.CLASSES, "&nbsp;")
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub FillEmployeeCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Try

            dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          CBool(Session("IsIncludeInactiveEmp")), "Emp", True, , , , , , , , , , , , , , , mstrAdvanceFilter, True)

            With cboEmployee
                .DataTextField = "EmpCodeName"
                .DataValueField = "employeeunkid"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "fillEmployeeCombo", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End            
        Finally
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End


#End Region

#Region "Button's Events"

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveChanges.Click
        Dim blnFlag As Boolean = False

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objETransfers As New clsemployee_transfer_tran
        Dim objATransfers As clsTransfer_Approval_Tran
        'Gajanan [4-Sep-2020] -- End


        Try
            If Valid_Transfer(objETransfers) = False Then Exit Sub


            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objATransfers = New clsTransfer_Approval_Tran

            Call SetValue(objETransfers, objATransfers)
            'Gajanan [4-Sep-2020] -- End

            If mAction = enAction.EDIT_ONE Then

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'blnFlag = objETransfers.Update(CInt(Session("CompanyUnkId")), Nothing)
                'S.SANDEEP |17-JAN-2019| -- START
                'blnFlag = objETransfers.Update(CBool(Session("CreateADUserFromEmpMst")), CInt(Session("CompanyUnkId")), Nothing)

                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                    blnFlag = objETransfers.Update(CBool(Session("CreateADUserFromEmpMst")), CInt(Session("CompanyUnkId")), Nothing)
                Else
                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(Nothing, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(Nothing, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If


                    'Pinkal (12-Oct-2020) -- Start
                    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                    'blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, CBool(Session("CreateADUserFromEmpMst")))
                    blnFlag = objATransfers.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, CBool(Session("CreateADUserFromEmpMst")), CStr(Session("Database_Name")))
                    'Pinkal (12-Oct-2020) -- End



                    If blnFlag = False AndAlso objATransfers._Message <> "" Then
                        DisplayMessage.DisplayMessage(objATransfers._Message, Me)
                        Exit Sub
                    End If

                End If


                'S.SANDEEP |17-JAN-2019| -- END



                'Pinkal (18-Aug-2018) -- End
            Else
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'blnFlag = objETransfers.Insert(CInt(Session("CompanyUnkId")), Nothing)
                'blnFlag = objETransfers.Insert(CBool(Session("CreateADUserFromEmpMst")), CInt(Session("CompanyUnkId")), Nothing)
                'Pinkal (18-Aug-2018) -- End


                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                    'Pinkal (12-Oct-2020) -- Start
                    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                    'blnFlag = objETransfers.Insert(CBool(Session("CreateADUserFromEmpMst")), CInt(Session("CompanyUnkId")), Nothing)
                    blnFlag = objETransfers.Insert(CStr(Session("Database_Name")), CBool(Session("CreateADUserFromEmpMst")), CInt(Session("CompanyUnkId")), Nothing)
                    'Pinkal (12-Oct-2020) -- End

                Else
                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(Nothing, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If


                    blnFlag = objATransfers.isExist(objATransfers._Effectivedate, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objATransfers.isExist(Nothing, objATransfers._Stationunkid, objATransfers._Deptgroupunkid, objATransfers._Departmentunkid, objATransfers._Sectiongroupunkid, objATransfers._Sectionunkid, objATransfers._Unitgroupunkid, objATransfers._Unitunkid, objATransfers._Teamunkid, objATransfers._Classgroupunkid, objATransfers._Classunkid, objATransfers._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, mintTransactionId)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If

                    'Pinkal (12-Oct-2020) -- Start
                    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                    'blnFlag = objATransfers.Insert(CInt(Session("CompanyUnkId")), clsEmployeeMovmentApproval.enOperationType.ADDED, CBool(Session("CreateADUserFromEmpMst")))
                    blnFlag = objATransfers.Insert(CInt(Session("CompanyUnkId")), clsEmployeeMovmentApproval.enOperationType.ADDED, CBool(Session("CreateADUserFromEmpMst")), CStr(Session("Database_Name")))
                    'Pinkal (12-Oct-2020) -- End

                    If blnFlag = False AndAlso objATransfers._Message <> "" Then
                        DisplayMessage.DisplayMessage(objATransfers._Message, Me)
                        Exit Sub
                    End If

                End If
            End If
            'Gajanan [12-NOV-2018] -- End

            If blnFlag = False AndAlso objETransfers._Message <> "" Then
                DisplayMessage.DisplayMessage(objETransfers._Message, Me)
                Exit Sub
            Else
                If mAction = enAction.EDIT_ONE Then mAction = enAction.ADD_CONTINUE
                Call Fill_Grid()
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}


                'Call SetAllocationForMail()
                ''Sohail (30 Nov 2017) -- Start
                ''SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                ''Call objETransfers.SendEmails(CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.SelectedItem.Text, xCurrentAllocation, CStr(Session("Notify_Allocation")), dtEffectiveDate.GetDate.Date, CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE)
                'Call objETransfers.SendEmails(CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.SelectedItem.Text, xCurrentAllocation, CStr(Session("Notify_Allocation")), dtEffectiveDate.GetDate.Date, CInt(Session("CompanyUnkId")), CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE)
                ''Sohail (30 Nov 2017) -- End

                'Gajanan [15-NOV-2018] -- START
                'If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                    'Gajanan [15-NOV-2018] -- END
                    Call SetAllocationForMail()
                    Call objETransfers.SendEmails(CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.SelectedItem.Text, xCurrentAllocation, CStr(Session("Notify_Allocation")), dtEffectiveDate.GetDate.Date, CInt(Session("CompanyUnkId")), CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE)
                Else
                    Dim objPMovement As New clsEmployeeMovmentApproval
                    'S.SANDEEP |17-JAN-2019| -- START
                    Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                    If mAction = enAction.EDIT_ONE Then
                        eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                    Else
                        eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END
                    objPMovement.SendNotification(1, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), 1192, clsEmployeeMovmentApproval.enMovementType.TRANSFERS, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLogin_Mode.MGR_SELF_SERVICE, CStr(Session("UserName")), eOperType, False, Nothing, 0, CInt(cboEmployee.SelectedValue).ToString(), "")
                    objPMovement = Nothing
                End If
                'Gajanan [12-NOV-2018] -- End


                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                Call Set_Transfers(objETransfers)
                'Gajanan [4-Sep-2020] -- End
            End If
            Call ClearControls()

            'S.SANDEEP [07-Feb-2018] -- START
            'ISSUE/ENHANCEMENT : {#0001988}
            cboEmployee.Enabled = True
            'S.SANDEEP [07-Feb-2018] -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Gajanan [15-NOV-2018] -- START
            'S.SANDEEP |17-JAN-2019| -- START
            'mintTransactionId = 0
            'S.SANDEEP |17-JAN-2019| -- END
            'Gajanan [15-NOV-2018] -- END

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objATransfers = Nothing
            objETransfers = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

    Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objETransfers As clsemployee_transfer_tran
        Dim objATransfers As clsTransfer_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try
            If popup_DeleteReason.Reason.Trim.Length <= 0 Then Exit Sub


            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objETransfers = New clsemployee_transfer_tran
            'Gajanan [4-Sep-2020] -- End

            Blank_ModuleName()
            objETransfers._WebFormName = mstrModuleName
            objETransfers._WebClientIP = CStr(Session("IP_ADD"))
            objETransfers._WebHostName = CStr(Session("HOST_NAME"))
            StrModuleName2 = "mnuGeneralMaster"
            StrModuleName3 = "mnuCoreSetups"

            objETransfers._Isvoid = True
            objETransfers._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objETransfers._Voidreason = popup_DeleteReason.Reason
            objETransfers._Voiduserunkid = CInt(Session("UserId"))
            objETransfers._Userunkid = CInt(Session("UserId"))
            'S.SANDEEP |17-JAN-2019| -- START
            'If objETransfers.Delete(CInt(mintTransferunkid), CInt(Session("CompanyUnkId")), Nothing) = False Then
            '    If objETransfers._Message <> "" Then
            '        DisplayMessage.DisplayMessage(objETransfers._Message, Me)
            '    End If
            '    Exit Sub
            'End If

            If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then

                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                objATransfers = New clsTransfer_Approval_Tran
                'Gajanan [4-Sep-2020] -- End

                objATransfers._Isvoid = True
                objATransfers._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objATransfers._Voidreason = popup_DeleteReason.Reason
                objATransfers._Voiduserunkid = -1
                objATransfers._Audituserunkid = CInt(Session("UserId"))
                objATransfers._Isweb = True
                objATransfers._Ip = getIP()
                objATransfers._Hostname = getHostName()
                objATransfers._Form_Name = mstrModuleName
                If objATransfers.Delete(CInt(mintTransferunkid), clsEmployeeMovmentApproval.enOperationType.DELETED, CInt(Session("CompanyUnkId")), CBool(Session("CreateADUserFromEmpMst")), Nothing) = False Then
                    If objATransfers._Message <> "" Then
                        DisplayMessage.DisplayMessage(objATransfers._Message, Me)
                    End If
                    Exit Sub
                End If
            Else

                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                'If objETransfers.Delete(CInt(mintTransferunkid), CInt(Session("CompanyUnkId")), CBool(Session("CreateADUserFromEmpMst"))) = False Then
                If objETransfers.Delete(CInt(mintTransferunkid), CInt(Session("CompanyUnkId")), CBool(Session("CreateADUserFromEmpMst")), CStr(Session("Database_Name"))) = False Then
                    'Pinkal (12-Oct-2020) -- End
                    If objETransfers._Message <> "" Then
                        DisplayMessage.DisplayMessage(objETransfers._Message, Me)
                    End If
                    Exit Sub
                End If
            End If
            'S.SANDEEP |17-JAN-2019| -- End
            Call Fill_Grid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objETransfers = Nothing
            objATransfers = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Me.Request.QueryString.Count > 0 Then
                Response.Redirect("~/HR/Employee Movement/wPg_EmpMovementDates.aspx?ID=" & Server.UrlEncode(b64encode(enEmp_Dates_Transaction.DT_CONFIRMATION & "|" & CInt(cboEmployee.SelectedValue))), False)
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            FillEmployeeCombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region "Combobox Event(S)"

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmployee As New clsEmployee_Master

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim dsComboList As DataSet = objEmployee.GetEmployeeList("EmployeeList", False, True, CInt(cboEmployee.SelectedValue), , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , , , , , )
                Dim dsComboList As DataSet = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), "EmployeeList", False, CInt(cboEmployee.SelectedValue))
                'Shani(24-Aug-2015) -- End
                If dsComboList IsNot Nothing AndAlso dsComboList.Tables(0).Rows.Count > 0 Then
                    mstrEmployeeCode = CStr(dsComboList.Tables(0).Rows(0)("employeecode"))
                End If
                dsComboList.Clear()
                dsComboList = Nothing
                objEmployee = Nothing
                Call ClearControls()
                Call Fill_Grid()

                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                Dim objETransfers As New clsemployee_transfer_tran
                'Gajanan [4-Sep-2020] -- End

                Call Set_Transfers(objETransfers)

                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                objETransfers = Nothing
                'Gajanan [4-Sep-2020] -- End

            Else
                dgTransfersList.DataSource = New List(Of String)
                dgTransfersList.DataBind()
                'Pinkal (18-Mar-2021) -- Start 
                'NMB Enhancmenet : AD Enhancement for NMB.
                ClearControls()
                'Pinkal (18-Mar-2021) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI (13 APR 2015)-START

    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso CStr(Session("Allocation_Hierarchy")).Contains(CStr(enAllocation.DEPARTMENT_GROUP)) = True Then
                Dim objDeptGrp As New clsDepartmentGroup
                Dim dsCombos As New DataSet

                dsCombos = objDeptGrp.getComboList(CInt(cboBranch.SelectedValue), "DeptGrp", True)
                With cboDeptGroup
                    .DataValueField = "deptgroupunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("DeptGrp")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub cboDeptGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDeptGroup.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso CStr(Session("Allocation_Hierarchy")).Contains(CStr(enAllocation.DEPARTMENT)) = True Then
                Dim objDepartment As New clsDepartment
                Dim dsCombos As New DataSet

                dsCombos = objDepartment.getComboList(CInt(cboDeptGroup.SelectedValue), "Department", True)
                With cboDepartment
                    .DataValueField = "departmentunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("Department")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub cboDepartment_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDepartment.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso CStr(Session("Allocation_Hierarchy")).Contains(CStr(enAllocation.SECTION_GROUP)) = True Then
                Dim objSectionGrp As New clsSectionGroup
                Dim dsCombos As New DataSet

                dsCombos = objSectionGrp.getComboList(CInt(cboDepartment.SelectedValue), "List", True)
                With cboSecGroup
                    .DataValueField = "sectiongroupunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("List")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub cboSecGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSecGroup.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso CStr(Session("Allocation_Hierarchy")).Contains(CStr(enAllocation.SECTION)) = True Then
                Dim objSection As New clsSections
                Dim dsCombos As New DataSet

                dsCombos = objSection.getComboList(CInt(cboSecGroup.SelectedValue), "Section", True)
                With cboSections
                    .DataValueField = "sectionunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("Section")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub cboSections_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSections.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso CStr(Session("Allocation_Hierarchy")).Contains(CStr(enAllocation.UNIT_GROUP)) = True Then

                Dim objUnitGroup As New clsUnitGroup
                Dim dsCombos As New DataSet

                dsCombos = objUnitGroup.getComboList("List", True, CInt(cboSections.SelectedValue))
                With cboUnitGroup
                    .DataValueField = "unitgroupunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("List")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub cboUnitGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnitGroup.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso CStr(Session("Allocation_Hierarchy")).Contains(CStr(enAllocation.UNIT_GROUP)) = True Then
                Dim dsCombos As New DataSet
                Dim objUnit As New clsUnits
                dsCombos = objUnit.getComboList("Unit", True, , CInt(cboUnitGroup.SelectedValue))
                With cboUnits
                    .DataValueField = "unitunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("Unit")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub cboUnits_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnits.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso CStr(Session("Allocation_Hierarchy")).Contains(CStr(enAllocation.TEAM)) = True Then
                Dim dsCombos As New DataSet
                Dim objTeam As New clsTeams
                dsCombos = objTeam.getComboList("List", True, CInt(cboUnits.SelectedValue))
                With cboTeam
                    .DataValueField = "teamunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("List")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'SHANI (13 APR 2015)--END

    Protected Sub cboClassGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboClassGroup.SelectedIndexChanged
        Dim objClass As New clsClass
        Dim dsComboList As New DataSet
        Try
            dsComboList = objClass.getComboList("Class", True, CInt(cboClassGroup.SelectedValue))
            With cboClass
                .DataTextField = "name"
                .DataValueField = "classesunkid"
                .DataSource = dsComboList.Tables("Class")
                .SelectedValue = CStr(0)
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "GridView Event(S)"

    Protected Sub dgTransfersList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTransfersList.ItemCommand

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objETransfers As New clsemployee_transfer_tran
        Dim objATransfers As New clsTransfer_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try
            pnldata.Visible = False
            txtAppointDate.Text = ""
            'Gajanan [12-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            If e.CommandName.ToUpper = "EDIT" Then
                mAction = enAction.EDIT_ONE
                'objETransfers._Transferunkid = CInt(e.Item.Cells(14).Text)
                'mintTransferunkid = CInt(e.Item.Cells(14).Text)
                objETransfers._Transferunkid = CInt(e.Item.Cells(15).Text)

                'Gajanan [15-NOV-2018] -- START
                mintTransactionId = objETransfers._Transferunkid
                'Gajanan [15-NOV-2018] -- END

                mintTransferunkid = CInt(e.Item.Cells(15).Text)

                'S.SANDEEP |17-JAN-2019| -- START
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim Flag As Boolean = objATransfers.isExist(Nothing, objATransfers._Employeeunkid, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, mintTransactionId)
                    If Flag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), Me)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END

                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                Call SetEditValue(objETransfers)
                'Gajanan [4-Sep-2020] -- End

                mdtAppointmentDate = Nothing
                'If CBool(e.Item.Cells(15).Text) = True Then
                If CBool(e.Item.Cells(16).Text) = True Then
                    mdtAppointmentDate = eZeeDate.convertDate(e.Item.Cells(17).Text.ToString)
                    txtAppointDate.Text = mdtAppointmentDate.Date.ToShortDateString
                    pnldata.Visible = True
                End If
                'S.SANDEEP [07-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001988}
                cboEmployee.Enabled = False
                'S.SANDEEP [07-Feb-2018] -- END
            ElseIf e.CommandName.ToUpper = "DELETE" Then
                mAction = enAction.ADD_ONE
                'mintTransferunkid = CInt(e.Item.Cells(14).Text)
                mintTransferunkid = CInt(e.Item.Cells(15).Text)

                'S.SANDEEP |17-JAN-2019| -- START
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim Flag As Boolean = objATransfers.isExist(Nothing, objATransfers._Employeeunkid, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, mintTransferunkid)
                    If Flag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), Me)
                        Exit Sub
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END
                End If

                popup_DeleteReason.Reason = ""
                popup_DeleteReason.Show()

                'S.SANDEEP |17-JAN-2019| -- START
            ElseIf e.CommandName.ToUpper = "VIEW" Then
                If CInt(e.Item.Cells(15).Text) > 0 Then
                    Dim dr As DataRow() = mdtTransferList.Select(CType(Me.dgTransfersList.Columns(15), BoundColumn).DataField & " = " & CInt(e.Item.Cells(15).Text) & " AND " & CType(Me.dgTransfersList.Columns(19), BoundColumn).DataField & "= ''")
                    If dr.Length > 0 Then
                        Dim index As Integer = mdtTransferList.Rows.IndexOf(dr(0))
                        dgTransfersList.SelectedIndex = index
                        dgTransfersList.SelectedItemStyle.BackColor = Color.LightCoral
                        dgTransfersList.SelectedItemStyle.ForeColor = Color.White
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END

            ElseIf e.CommandName.ToUpper = "VIEWREPORT" Then
                mAction = enAction.EDIT_ONE
                Popup_Viewreport._UserId = CInt(Session("UserId"))
                Popup_Viewreport._Priority = 0
                'Gajanan [19-Nov-2018] -- Start
                'Popup_Viewreport._PrivilegeId = 1193
                Popup_Viewreport._PrivilegeId = 1192
                'Popup_Viewreport._FillType = clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE 
                Popup_Viewreport._FillType = clsEmployeeMovmentApproval.enMovementType.TRANSFERS
                'Gajanan [19-Nov-2018] -- End
                Popup_Viewreport._DtType = Nothing
                Popup_Viewreport._IsResidentPermit = False
                Popup_Viewreport._FilterString = "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue)


                Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                If mAction = enAction.EDIT_ONE Then
                    eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                Popup_Viewreport._FromApprovalScreen = False
                Popup_Viewreport._OprationType = eOperType
                Popup_Viewreport.Show()
                'Gajanan [12-NOV-2018] -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objETransfers = Nothing
            objATransfers = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

    Protected Sub dgTransfersList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTransfersList.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                ' If CBool(e.Item.Cells(16).Text) = True Then
                '    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                'End If
                If e.Item.Cells(16).Text.Trim <> "&nbsp;" AndAlso CBool(e.Item.Cells(16).Text) = True Then
                    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                End If
                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'If e.Item.Cells(2).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(2).Text.Trim <> "" Then
                '    e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).ToString(Session("DateFormat").ToString)
                'End If
                If e.Item.Cells(3).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(3).Text.Trim <> "" Then
                    e.Item.Cells(3).Text = CDate(e.Item.Cells(3).Text).ToShortDateString
                End If
                'Pinkal (16-Apr-2016) -- End



                'If CBool(xdgvr.Cells(objdgcolhFromEmp.Index).Value) = True Then
                '    xdgvr.Cells(objdgcolhDelete.Index).Value = imgBlank
                'End If
                If e.Item.Cells(19).Text.Trim <> "&nbsp;" AndAlso CStr(e.Item.Cells(19).Text).Trim.Length > 0 Then
                    e.Item.BackColor = System.Drawing.Color.PowderBlue
                    e.Item.ForeColor = System.Drawing.Color.Black
                    CType(e.Item.Cells(1).FindControl("imgEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    'S.SANDEEP [09-AUG-2018] -- START
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = True
                    lblPendingData.Visible = True
                    'S.SANDEEP [09-AUG-2018] -- END 

                    'S.SANDEEP |17-JAN-2019| -- START

                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).ToolTip = e.Item.Cells(20).Text
                    If CInt(e.Item.Cells(21).Text) = clsEmployeeMovmentApproval.enOperationType.EDITED Then
                        CType(e.Item.Cells(0).FindControl("imgDetail"), LinkButton).Visible = True
                        CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END

                Else
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = False

                    If e.Item.Cells(16).Text.Trim <> "&nbsp;" AndAlso CBool(e.Item.Cells(16).Text) = True Then
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    End If
                End If
                'Gajanan [12-NOV-2018] -- END


                'Gajanan (28-May-2018) -- Start
                'Issue - Rehire Employee Data Also Edit And Delete.

                'If Convert.ToInt32(e.Item.Cells(17).Text) > 0 Then
                '   CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                '   CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False
                '   e.Item.BackColor = System.Drawing.Color.Orange
                'End If

                If Convert.ToInt32(e.Item.Cells(18).Text) > 0 Then
                    CType(e.Item.Cells(1).FindControl("imgEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = False
                    e.Item.BackColor = System.Drawing.Color.Orange
                    objlblCaption.Visible = True
                End If
                'Gajanan (28-May-2018) -- End
                'Gajanan [12-NOV-2018] -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbTransferInformation", lblPageHeader.Text)

            Me.lblBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBranch.ID, Me.lblBranch.Text)
            Me.lblUnitGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblUnitGroup.ID, Me.lblUnitGroup.Text)
            Me.lblUnits.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblUnits.ID, Me.lblUnits.Text)
            Me.lblDepartmentGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDepartmentGroup.ID, Me.lblDepartmentGroup.Text)
            Me.lblDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDepartment.ID, Me.lblDepartment.Text)
            Me.lblTeam.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTeam.ID, Me.lblTeam.Text)
            Me.lblSectionGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSectionGroup.ID, Me.lblSectionGroup.Text)
            Me.lblSection.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSection.ID, Me.lblSection.Text)
            Me.lblClassGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblClassGroup.ID, Me.lblClassGroup.Text)
            Me.lblClass.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblClass.ID, Me.lblClass.Text)

            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblEffectiveDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEffectiveDate.ID, Me.lblEffectiveDate.Text)

            Me.lblallocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "elLine1", Me.lblallocation.Text)

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.btnSaveChanges.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "btnSave", Me.btnSaveChanges.Text).Replace("&", "")
            Me.lblChangeReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblChangeReason.ID, Me.lblChangeReason.Text)
            Me.dgTransfersList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgTransfersList.Columns(2).FooterText, Me.dgTransfersList.Columns(2).HeaderText)
            Me.dgTransfersList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgTransfersList.Columns(3).FooterText, Me.dgTransfersList.Columns(3).HeaderText)
            Me.dgTransfersList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgTransfersList.Columns(4).FooterText, Me.dgTransfersList.Columns(4).HeaderText)
            Me.dgTransfersList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgTransfersList.Columns(5).FooterText, Me.dgTransfersList.Columns(5).HeaderText)
            Me.dgTransfersList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgTransfersList.Columns(6).FooterText, Me.dgTransfersList.Columns(6).HeaderText)
            Me.dgTransfersList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgTransfersList.Columns(7).FooterText, Me.dgTransfersList.Columns(7).HeaderText)
            Me.dgTransfersList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgTransfersList.Columns(8).FooterText, Me.dgTransfersList.Columns(8).HeaderText)
            Me.dgTransfersList.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgTransfersList.Columns(9).FooterText, Me.dgTransfersList.Columns(9).HeaderText)
            Me.dgTransfersList.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgTransfersList.Columns(10).FooterText, Me.dgTransfersList.Columns(10).HeaderText)
            Me.dgTransfersList.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgTransfersList.Columns(11).FooterText, Me.dgTransfersList.Columns(11).HeaderText)
            Me.dgTransfersList.Columns(12).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgTransfersList.Columns(12).FooterText, Me.dgTransfersList.Columns(12).HeaderText)
            Me.dgTransfersList.Columns(13).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgTransfersList.Columns(13).FooterText, Me.dgTransfersList.Columns(13).HeaderText)
            Me.lblAppointmentdate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAppointmentdate.ID, Me.lblAppointmentdate.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

End Class


