﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
#End Region


Partial Class HR_Employee_Movement_wPg_EmployeeOtherDetails
    Inherits Basepage

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmEmployeeOtherDetails"
    Dim DisplayMessage As New CommonCodes

    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private objECCT As clsemployee_cctranhead_tran
    'Gajanan [4-Sep-2020] -- End

    Private mstrEmployeeCode As String = String.Empty
    Private mblnIsCostCenter As Boolean = False
    Private xMasterType As clsCommon_Master.enCommonMaster
    Private mdtAppointmentDate As Date = Nothing
    'Gajanan [12-NOV-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}

    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private objACCT As New clsCCTranhead_Approval_Tran
    'Gajanan [4-Sep-2020] -- End

    'Gajanan [12-NOV-2018] -- END


    'Gajanan [15-NOV-2018] -- START
    Private mintTransactionId As Integer = 0
    'Gajanan [15-NOV-2018] -- END

    'S.SANDEEP |17-JAN-2019| -- START
    'ENHANCEMENT:Movement Approval Flow Edit / Delete
    Private mdtCostCenterList As DataTable
    'S.SANDEEP |17-JAN-2019| -- END

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mstrAdvanceFilter As String = ""
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region "Page's Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

       

            'Gajanan [12-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            lblRehireEmployeeData.Visible = False
            lblPendingData.Visible = False
            'Gajanan [12-NOV-2018] -- END


            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            'objECCT = New clsemployee_cctranhead_tran
            'Gajanan [4-Sep-2020] -- End

            If Not IsPostBack Then

                SetLanguage()

                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                GC.Collect()
                'Gajanan [4-Sep-2020] -- End

                If Request.QueryString.Count <= 0 Then Exit Sub
                Dim mstrRequset As String = ""
                Try
                    mstrRequset = b64decode(Server.UrlDecode(Request.QueryString.ToString.Substring(3)))
                Catch
                    Session("clsuser") = Nothing
                    DisplayMessage.DisplayMessage("Invalid Page Address.", Me, "../../index.aspx")
                    Exit Sub
                End Try
                If mstrRequset.Trim.Length > 0 Then
                    If CBool(mstrRequset) Then
                        xMasterType = clsCommon_Master.enCommonMaster.COST_CENTER
                        mblnIsCostCenter = CBool(mstrRequset)
                    End If
                End If
                Set_Form_Controls()
                FillCombo()
                Fill_Grid()
            Else
                mblnIsCostCenter = CBool(Me.ViewState("IsCostCenter"))
                xMasterType = CType(Me.ViewState("MasterType"), clsCommon_Master.enCommonMaster)
                mdtAppointmentDate = CDate(Me.ViewState("mdtAppointmentDate"))
                mstrEmployeeCode = CStr(Me.ViewState("EmployeeCode"))
                'Gajanan [15-NOV-2018] -- START
                mintTransactionId = CInt(Me.ViewState("mintTransactionId"))
                'Gajanan [15-NOV-2018] -- End

                'S.SANDEEP |17-JAN-2019| -- START
                'ENHANCEMENT:Movement Approval Flow Edit / Delete
                mdtCostCenterList = CType(Me.ViewState("mdtCostCenterList"), DataTable)
                'S.SANDEEP |17-JAN-2019| -- END

            End If

            'Shani (08-Dec-2016) -- Start
            'Enhancement -  Add Employee Allocaion/Date Privilage
            btnSave.Enabled = CBool(Session("AllowtoChangeCostCenter"))
            'Shani (08-Dec-2016) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("IsCostCenter", mblnIsCostCenter)
            Me.ViewState.Add("MasterType", xMasterType)
            Me.ViewState.Add("mdtAppointmentDate", mdtAppointmentDate)
            Me.ViewState.Add("EmployeeCode", mstrEmployeeCode)

            'Gajanan [15-NOV-2018] -- START
            Me.ViewState.Add("mintTransactionId", mintTransactionId)
            'Gajanan [15-NOV-2018] -- END

            'S.SANDEEP |17-JAN-2019| -- START
            'ENHANCEMENT:Movement Approval Flow Edit / Delete
            Me.ViewState.Add("mdtCostCenterList", mdtCostCenterList)
            'S.SANDEEP |17-JAN-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objCMaster As New clsCommon_Master
        Dim objCC As New clscostcenter_master
        Dim objTrnHead As New clsTransactionHead
        Dim dsCombos As New DataSet
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If CBool(Session("IsIncludeInactiveEmp")) Then
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, True, , , , , , , , , , , , , , , , , , , , )
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            'End If

            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            'dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
            '                                CInt(Session("UserId")), _
            '                                CInt(Session("Fin_year")), _
            '                                CInt(Session("CompanyUnkId")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                CStr(Session("UserAccessModeSetting")), True, _
            '                                CBool(Session("IsIncludeInactiveEmp")), "Emp", True, , , , , , , , , , , , , , , , True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END
            ''Shani(24-Aug-2015) -- End
            'With cboEmployee
            '    .DataValueField = "employeeunkid"
            '    'Nilay (09-Aug-2016) -- Start
            '    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            '    '.DataTextField = "employeename"
            '    .DataTextField = "EmpCodeName"
            '    'Nilay (09-Aug-2016) -- End
            '    .DataSource = dsCombos.Tables("Emp")
            '    .DataBind()
            '    .SelectedValue = CStr(0)
            '    .Text = ""
            'End With

            FillEmployeeCombo()
            'Gajanan [11-Dec-2019] -- End


            dsCombos = objCMaster.getComboList(CType(xMasterType, clsCommon_Master.enCommonMaster), True, "List")
            With cboChangeReason
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            If mblnIsCostCenter = True Then
                dsCombos = objCC.getComboList("CostCenter", True)
                With cboCCTrnHead
                    .DataValueField = "costcenterunkid"
                    .DataTextField = "costcentername"
                    .DataSource = dsCombos.Tables("CostCenter")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            Else
                'Sohail (03 Feb 2016) -- Start
                'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                'dsCombos = objTrnHead.getComboList("TranHead", True, 0, 0, enTypeOf.Salary)
                dsCombos = objTrnHead.getComboList(Session("Database_Name").ToString, "TranHead", True, 0, 0, enTypeOf.Salary)
                'Sohail (03 Feb 2016) -- End
                With cboCCTrnHead
                    .DataValueField = "tranheadunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("TranHead")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsCombos.Dispose() : objEmployee = Nothing : objCMaster = Nothing
            objCC = Nothing : objTrnHead = Nothing
        End Try
    End Sub

    Private Sub Set_Form_Controls()
        Try
            If mblnIsCostCenter = True Then
                lblDetialHeader.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Cost Center Information")
                objlblCaption.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Cost Center")
                lblPageHeader.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Cost Center Information")
                xMasterType = clsCommon_Master.enCommonMaster.COST_CENTER
            Else
                lblDetialHeader.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Transaction Head Information")
                objlblCaption.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Transaction Head")
                lblPageHeader.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Transaction Head Information")
            End If
            dgvHistory.Columns(4).HeaderText = objlblCaption.Text
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsData As New DataSet

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objECCT As clsemployee_cctranhead_tran
        Dim objACCT As clsCCTranhead_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try
            'Varsha Rana (17-Oct-2017) -- Start
            'Enhancement - Give user privileges.
            Call SetVisibility()
            'Varsha Rana (17-Oct-2017) -- End


            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objECCT = New clsemployee_cctranhead_tran
            'Gajanan [4-Sep-2020] -- End

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                dsData = objECCT.GetList("List", mblnIsCostCenter, CInt(cboEmployee.SelectedValue)).Clone
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}     
                dgvHistory.Columns(1).Visible = False
                dgvHistory.Columns(2).Visible = False
                dgvHistory.Columns(0).Visible = False
                'Gajanan [12-NOV-2018] -- END
            Else
                dsData = objECCT.GetList("List", mblnIsCostCenter, CInt(cboEmployee.SelectedValue))
                dgvHistory.Columns(1).Visible = CBool(Session("EditEmployeeCostCenter"))
                dgvHistory.Columns(2).Visible = CBool(Session("DeleteEmployeeCostCenter"))
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}     
                dgvHistory.Columns(0).Visible = Not CBool(Session("SkipEmployeeMovementApprovalFlow"))

                Dim dcol As New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "tranguid"
                    .DefaultValue = ""
                End With
                dsData.Tables(0).Columns.Add(dcol)

                'S.SANDEEP |17-JAN-2019| -- START
                dcol = New DataColumn
                With dcol
                    .DataType = GetType(System.Int32)
                    .ColumnName = "operationtypeid"
                    .DefaultValue = clsEmployeeMovmentApproval.enOperationType.ADDED
                End With
                dsData.Tables(0).Columns.Add(dcol)

                dcol = New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "OperationType"
                    .DefaultValue = ""
                End With
                dsData.Tables(0).Columns.Add(dcol)
                'S.SANDEEP |17-JAN-2019| -- End


                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                objACCT = New clsCCTranhead_Approval_Tran
                'Gajanan [4-Sep-2020] -- End

                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim dsPending As New DataSet
                    dsPending = objACCT.GetList("List", mblnIsCostCenter, CInt(cboEmployee.SelectedValue))
                    If dsPending.Tables(0).Rows.Count > 0 Then
                        For Each row As DataRow In dsPending.Tables(0).Rows
                            dsData.Tables(0).ImportRow(row)
                        Next
                    End If
                End If
                'Gajanan [12-NOV-2018] -- END

            End If
            dgvHistory.AutoGenerateColumns = False
            dgvHistory.DataSource = dsData.Tables(0)
            dgvHistory.DataBind()
            mdtCostCenterList = dsData.Tables(0)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objECCT = Nothing
            objACCT = Nothing
            dsData.Dispose()
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

    Private Sub Set_Details()

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objECCT As New clsemployee_cctranhead_tran
        'Gajanan [4-Sep-2020] -- End

        Try
            Dim dsList As New DataSet
            dsList = objECCT.Get_Current_CostCenter_TranHeads(Now.Date, mblnIsCostCenter, CInt(cboEmployee.SelectedValue))
            If dsList.Tables(0).Rows.Count > 0 Then
                cboCCTrnHead.SelectedValue = CStr(dsList.Tables(0).Rows(0).Item("cctranheadvalueid"))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objECCT = Nothing
        End Try
    End Sub

    Private Function IsValidDetails() As Boolean

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objECCT As New clsemployee_cctranhead_tran
        'Gajanan [4-Sep-2020] -- End

        Try

            If dtpEffectiveDate.IsNull Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue."), Me)
                dtpEffectiveDate.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Sorry, Employee is mandatory information. Please selecte Employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            If CInt(cboCCTrnHead.SelectedValue) <= 0 Then
                If mblnIsCostCenter = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, Cost center is mandatory information. Please select cost center to continue."), Me)
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Sorry, Transaction head is mandatory information. Please select transaction head to continue."), Me)
                End If
                cboCCTrnHead.Focus()
                Return False
            End If

            If CInt(cboChangeReason.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, Change Reason is mandatory information. Please selecte Change Reason to continue."), Me)
                cboChangeReason.Focus()
                Return False
            End If

            If mdtAppointmentDate <> Nothing Then
                If mdtAppointmentDate <> dtpEffectiveDate.GetDate.Date Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, This is system generate entry. Please set effective date as employee appointment date."), Me)
                    dtpEffectiveDate.Focus()
                    Return False
                End If
            End If


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If objECCT.isExist(dtpEffectiveDate.GetDate(), CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, CInt(cboEmployee.SelectedValue), 0) Then
                If mblnIsCostCenter = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsemployee_cctranhead_tran", 1, "Sorry, transaction head information is already present for the selected effective date."), Me)
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsemployee_cctranhead_tran", 2, "Sorry, cost center information is already present for the selected effective date."), Me)
                End If
                Return False
            End If
            'Gajanan [11-Dec-2019] -- End



            'Gajanan [15-NOV-2018] -- Start
            'ISSUE/ENHANCEMENT : {Ref#244}
            If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                Dim objPMovement As New clsEmployeeMovmentApproval
                Dim strMsg As String = String.Empty
                strMsg = objPMovement.IsOtherMovmentApproverPresent(clsEmployeeMovmentApproval.enMovementType.COSTCENTER, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), 1201, CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString, Nothing, CInt(cboEmployee.SelectedValue))
                If strMsg.Trim.Length > 0 Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(strMsg, Me)
                    'Sohail (23 Mar 2019) -- End
                    objPMovement = Nothing
                    Return False
                End If
                objPMovement = Nothing

                'S.SANDEEP |16-JAN-2019| -- START
                If objECCT.isExist(dtpEffectiveDate.GetDate(), -1, mblnIsCostCenter, CInt(cboEmployee.SelectedValue), mintTransactionId) Then
                    If mblnIsCostCenter = False Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsemployee_cctranhead_tran", 1, "Sorry, transaction head information is already present for the selected effective date."), Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsemployee_cctranhead_tran", 2, "Sorry, cost center information is already present for the selected effective date."), Me)
                    End If
                    Return False
                End If

                Dim dsList As New DataSet
                dsList = objECCT.Get_Current_CostCenter_TranHeads(dtpEffectiveDate.GetDate(), mblnIsCostCenter, CInt(cboEmployee.SelectedValue))
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    If CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid")) = CInt(cboCCTrnHead.SelectedValue) Then
                        If mblnIsCostCenter = False Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsemployee_cctranhead_tran", 1, "Sorry, transaction head information is already present for the selected effective date."), Me)
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsemployee_cctranhead_tran", 2, "Sorry, cost center information is already present for the selected effective date."), Me)
                        End If
                        Return False
                    End If
                End If

                If objECCT.isExist(dtpEffectiveDate.GetDate(), CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, CInt(cboEmployee.SelectedValue), mintTransactionId) Then
                    If mblnIsCostCenter = False Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsemployee_cctranhead_tran", 1, "Sorry, transaction head information is already present for the selected effective date."), Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsemployee_cctranhead_tran", 2, "Sorry, cost center information is already present for the selected effective date."), Me)
                    End If
                    Return False
                End If
                'S.SANDEEP |16-JAN-2019| -- END

            End If
            'Gajanan [15-NOV-2018] -- END

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "IsValidDetails", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objECCT = Nothing
        End Try
        Return True
    End Function



    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private Sub SetValue()
    Private Sub SetValue(ByVal objECCT As clsemployee_cctranhead_tran, ByVal objACCT As clsCCTranhead_Approval_Tran)
        'Gajanan [4-Sep-2020] -- End
        Try
            If Me.ViewState("Cctranheadunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Cctranheadunkid")) > 0 Then
                objECCT._Cctranheadunkid = CInt(Me.ViewState("Cctranheadunkid"))
            End If

            'Gajanan [12-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'objECCT._Cctranheadvalueid = CInt(cboCCTrnHead.SelectedValue)
            'objECCT._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
            'objECCT._Effectivedate = dtpEffectiveDate.GetDate.Date
            'objECCT._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'objECCT._Istransactionhead = Not mblnIsCostCenter
            'objECCT._Isvoid = False
            'objECCT._Rehiretranunkid = 0
            'objECCT._Statusunkid = 0
            'objECCT._Userunkid = CInt(Session("UserId"))
            'objECCT._Voiddatetime = Nothing
            'objECCT._Voidreason = ""
            'objECCT._Voiduserunkid = -1

            'Blank_ModuleName()
            'objECCT._WebFormName = mstrModuleName
            'objECCT._WebClientIP = CStr(Session("IP_ADD"))
            'objECCT._WebHostName = CStr(Session("HOST_NAME"))
            'StrModuleName2 = "mnuGeneralMaster"
            'StrModuleName3 = "mnuCoreSetups"

            'Gajanan [15-NOV-2018] -- START
            'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then

            'S.SANDEEP |17-JAN-2019| -- START
            'ENHANCEMENT:Movement Approval Flow Edit / Delete
            'If CBool(Session("SkipEmployeeMovementApprovalFlow")) Or mintTransactionId > 0 Then
            If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                'S.SANDEEP |17-JAN-2019| -- END
                'Gajanan [15-NOV-2018] -- End
                objECCT._Cctranheadvalueid = CInt(cboCCTrnHead.SelectedValue)
                objECCT._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                objECCT._Effectivedate = dtpEffectiveDate.GetDate.Date
                objECCT._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objECCT._Istransactionhead = Not mblnIsCostCenter
                objECCT._Isvoid = False
                objECCT._Rehiretranunkid = 0
                objECCT._Statusunkid = 0
                objECCT._Userunkid = CInt(Session("UserId"))
                objECCT._Voiddatetime = Nothing
                objECCT._Voidreason = ""
                objECCT._Voiduserunkid = -1

                Blank_ModuleName()
                objECCT._WebFormName = mstrModuleName
                objECCT._WebClientIP = CStr(Session("IP_ADD"))
                objECCT._WebHostName = CStr(Session("HOST_NAME"))
                StrModuleName2 = "mnuGeneralMaster"
                StrModuleName3 = "mnuCoreSetups"
            Else
                objACCT._Audittype = enAuditType.ADD
                objACCT._Audituserunkid = CInt(Session("UserId"))

                objACCT._Cctranheadvalueid = CInt(cboCCTrnHead.SelectedValue)
                objACCT._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                objACCT._Effectivedate = dtpEffectiveDate.GetDate()
                objACCT._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objACCT._Istransactionhead = Not mblnIsCostCenter
                objACCT._Isvoid = False
                objACCT._Rehiretranunkid = 0
                objACCT._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                objACCT._Voiddatetime = Nothing
                objACCT._Voidreason = ""
                objACCT._Voiduserunkid = -1

                objACCT._Tranguid = Guid.NewGuid.ToString()
                objACCT._Transactiondate = Now
                objACCT._Remark = ""
                objACCT._Rehiretranunkid = 0
                objACCT._Mappingunkid = 0
                objACCT._Isweb = True
                objACCT._Isfinal = False
                objACCT._Ip = Session("IP_ADD").ToString()
                objACCT._Hostname = Session("HOST_NAME").ToString()
                objACCT._Form_Name = mstrModuleName
                'S.SANDEEP |17-JAN-2019| -- START

                If Me.ViewState("Cctranheadunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Cctranheadunkid")) > 0 Then
                    objACCT._Cctranheadunkid = mintTransactionId
                    objACCT._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    objACCT._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                'S.SANDEEP |17-JAN-2019| -- END


                Blank_ModuleName()
                StrModuleName2 = "mnuGeneralMaster"
                StrModuleName3 = "mnuCoreSetups"
            End If
            'Gajanan [12-NOV-2018] -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub SetEditValue(ByVal objECCT As clsemployee_cctranhead_tran)
        Try
            If Me.ViewState("Cctranheadunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Cctranheadunkid")) > 0 Then
                objECCT._Cctranheadunkid = CInt(Me.ViewState("Cctranheadunkid"))
                'Gajanan [15-NOV-2018] -- START
                mintTransactionId = objECCT._Cctranheadunkid
                'Gajanan [15-NOV-2018] -- END
            End If
            dtpEffectiveDate.SetDate = objECCT._Effectivedate
            cboEmployee.SelectedValue = CStr(objECCT._Employeeunkid)
            cboChangeReason.SelectedValue = CStr(objECCT._Changereasonunkid)
            cboCCTrnHead.SelectedValue = CStr(objECCT._Cctranheadvalueid)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetEditValue", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            dtpEffectiveDate.SetDate = Nothing
            cboChangeReason.SelectedValue = CStr(0)
            cboCCTrnHead.SelectedValue = CStr(0)
            lblAppointDate.Visible = False
            txtAppointDate.Visible = False
            txtAppointDate.Text = ""
            mdtAppointmentDate = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "ClearControls", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'Varsha Rana (17-Oct-2017) -- Start
    'Enhancement - Give user privileges.
    Private Sub SetVisibility()
        Try
            btnSave.Enabled = CBool(Session("AllowtoChangeCostCenter"))
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Varsha Rana (17-Oct-2017) -- End

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub FillEmployeeCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Try

            dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          CBool(Session("IsIncludeInactiveEmp")), "Emp", True, , , , , , , , , , , , , , , mstrAdvanceFilter, True)

            With cboEmployee
                .DataTextField = "EmpCodeName"
                .DataValueField = "employeeunkid"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = CStr(0)
                .DataBind()
            End With


        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "fillEmployeeCombo", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End
        Finally
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region "Combobox Event"

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmployee As New clsEmployee_Master

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim dsCombos As DataSet = objEmployee.GetEmployeeList("Emp", False, True, CInt(cboEmployee.SelectedValue), , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")))
                Dim dsCombos As DataSet
                Dim blnSelect As Boolean = True
                Dim intEmpId As Integer = CInt(cboEmployee.SelectedValue)
                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), "List", False, intEmpId)
                'Shani(24-Aug-2015) -- End
                If dsCombos IsNot Nothing AndAlso dsCombos.Tables(0).Rows.Count > 0 Then
                    mstrEmployeeCode = CStr(dsCombos.Tables(0).Rows(0)("employeecode"))
                End If
                Call ClearControls()
                Call Fill_Grid()
                Call Set_Details()
            Else
                dgvHistory.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "Button Event"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objECCT As clsemployee_cctranhead_tran
        Dim objACCT As clsCCTranhead_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try
            If IsValidDetails() = False Then Exit Sub


            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objECCT = New clsemployee_cctranhead_tran
            objACCT = New clsCCTranhead_Approval_Tran
            'Gajanan [4-Sep-2020] -- End


            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            Call SetValue(objECCT, objACCT)
            'Gajanan [4-Sep-2020] -- End

            If Me.ViewState("Cctranheadunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Cctranheadunkid")) > 0 Then
                'S.SANDEEP |17-JAN-2019| -- START
                'ENHANCEMENT:Movement Approval Flow Edit / Delete
                'blnFlag = objECCT.Update()

                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                    blnFlag = objECCT.Update()
                Else
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objACCT.isExist(objACCT._Effectivedate, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If


                    blnFlag = objACCT.isExist(objACCT._Effectivedate, 0, mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objACCT.isExist(Nothing, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", , Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit
                    blnFlag = objACCT.isExist(objACCT._Effectivedate, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objACCT.isExist(objACCT._Effectivedate, 0, mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objACCT.isExist(Nothing, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If


                    blnFlag = objACCT.Insert(clsEmployeeMovmentApproval.enOperationType.EDITED)

                    If blnFlag = False AndAlso objACCT._Message <> "" Then
                        DisplayMessage.DisplayMessage(objACCT._Message, Me)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END

            ElseIf Me.ViewState("Cctranheadunkid") Is Nothing OrElse CInt(Me.ViewState("Cctranheadunkid")) <= 0 Then
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'blnFlag = objECCT.Insert()

                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                    blnFlag = objECCT.Insert()
                Else
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objACCT.isExist(objACCT._Effectivedate, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", , Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If


                    blnFlag = objACCT.isExist(objACCT._Effectivedate, 0, mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", , Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objACCT.isExist(Nothing, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", , Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objACCT.isExist(objACCT._Effectivedate, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If


                    blnFlag = objACCT.isExist(objACCT._Effectivedate, 0, mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objACCT.isExist(Nothing, CInt(cboCCTrnHead.SelectedValue), mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END
                    blnFlag = objACCT.Insert(clsEmployeeMovmentApproval.enOperationType.ADDED)
                    If blnFlag = False AndAlso objACCT._Message <> "" Then
                        DisplayMessage.DisplayMessage(objACCT._Message, Me)
                        Exit Sub
                    End If

                End If
                'Gajanan [12-NOV-2018] -- End

            End If
            If blnFlag = False AndAlso objECCT._Message <> "" Then
                DisplayMessage.DisplayMessage(objECCT._Message, Me)
                Exit Sub
            Else
                Call Fill_Grid()
                Call Set_Details()
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'Gajanan [15-NOV-2018] -- START
                'If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                    'Gajanan [15-NOV-2018] -- END
                    Dim objPMovement As New clsEmployeeMovmentApproval

                    'S.SANDEEP |17-JAN-2019| -- START
                    'objPMovement.SendNotification(1, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), 1201, clsEmployeeMovmentApproval.enMovementType.COSTCENTER, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLogin_Mode.MGR_SELF_SERVICE, CStr(Session("UserName")), False, Nothing, 0, CInt(cboEmployee.SelectedValue).ToString(), "")
                    Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                    If Me.ViewState("Cctranheadunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Cctranheadunkid")) > 0 Then
                        eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                    Else
                        eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                    End If
                    objPMovement.SendNotification(1, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), 1201, clsEmployeeMovmentApproval.enMovementType.COSTCENTER, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLogin_Mode.MGR_SELF_SERVICE, CStr(Session("UserName")), eOperType, False, Nothing, 0, CInt(cboEmployee.SelectedValue).ToString(), "")
                    'S.SANDEEP |17-JAN-2019| -- END

                    objPMovement = Nothing
                End If
                'Gajanan [12-NOV-2018] -- End
            End If
            Me.ViewState("Cctranheadunkid") = Nothing
            Call ClearControls()
            'S.SANDEEP [07-Feb-2018] -- START
            'ISSUE/ENHANCEMENT : {#0001988}
            cboEmployee.Enabled = True
            'S.SANDEEP [07-Feb-2018] -- END
        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End          
        Finally
            'Gajanan [15-NOV-2018] -- START
            'S.SANDEEP |17-JAN-2019| -- START
            'mintTransactionId = 0
            'S.SANDEEP |17-JAN-2019| -- END
            'Gajanan [15-NOV-2018] -- END

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objECCT = Nothing
            objACCT = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

    Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objECCT As New clsemployee_cctranhead_tran
        Dim objACCT As New clsCCTranhead_Approval_Tran
        'Gajanan [4-Sep-2020] -- End
        Try
            Blank_ModuleName()
            objECCT._WebFormName = mstrModuleName
            objECCT._WebClientIP = CStr(Session("IP_ADD"))
            objECCT._WebHostName = CStr(Session("HOST_NAME"))
            StrModuleName2 = "mnuGeneralMaster"
            StrModuleName3 = "mnuCoreSetups"

            objECCT._Isvoid = True
            objECCT._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objECCT._Voidreason = popup_DeleteReason.Reason
            objECCT._Voiduserunkid = CInt(Session("UserId"))
            'S.SANDEEP |17-JAN-2019| -- START

            'If objECCT.Delete(CInt(Me.ViewState("Cctranheadunkid"))) = False Then
            '    If objECCT._Message <> "" Then
            '        DisplayMessage.DisplayMessage(objECCT._Message, Me)
            '    End If
            '    Exit Sub
            '    popup_DeleteReason.Hide()
            'End If

            If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                objACCT._Isvoid = True
                objACCT._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objACCT._Voidreason = popup_DeleteReason.Reason
                objACCT._Voiduserunkid = -1
                objACCT._Audituserunkid = CInt(Session("UserId"))
                objACCT._Isweb = True
                objACCT._Ip = CStr(Session("IP_ADD"))
                objACCT._Hostname = CStr(Session("HOST_NAME"))
                objACCT._Form_Name = mstrModuleName
                If objACCT.Delete(CInt(Me.ViewState("Cctranheadunkid")), clsEmployeeMovmentApproval.enOperationType.DELETED, Nothing) = False Then
                    If objACCT._Message <> "" Then
                        DisplayMessage.DisplayMessage(objACCT._Message, Me)
                    End If
                    Exit Sub
                End If
            Else
                If objECCT.Delete(CInt(Me.ViewState("Cctranheadunkid")), Nothing) = False Then
                    If objECCT._Message <> "" Then
                        DisplayMessage.DisplayMessage(objECCT._Message, Me)
                    End If
                    Exit Sub
                popup_DeleteReason.Hide()
                End If
            End If
            Call Fill_Grid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popup_DeleteReason_buttonDelReasonYes_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objECCT = Nothing
            objACCT = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            FillEmployeeCombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End
#End Region

#Region "Datagrid Events"

    Protected Sub dgvHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvHistory.ItemDataBound
        Try
            'Gajanan (28-May-2018) -- Start
            'Issue - Rehire Employee Data Also Edit And Delete.
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If (e.Item.ItemIndex >= 0) Then
                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'If e.Item.Cells(2).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(2).Text.Trim <> "" Then
                '    e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).ToString(Session("DateFormat").ToString)
                'End If
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'If e.Item.Cells(2).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(2).Text.Trim <> "" Then
                'e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).ToShortDateString
                If e.Item.Cells(3).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(3).Text.Trim <> "" Then
                    e.Item.Cells(3).Text = CDate(e.Item.Cells(3).Text).ToShortDateString
                End If
                'Gajanan [12-NOV-2018] -- End

                'Pinkal (16-Apr-2016) -- End


                If e.Item.Cells(10).Text.Trim <> "&nbsp;" AndAlso CStr(e.Item.Cells(10).Text).Trim.Length > 0 Then
                    e.Item.BackColor = Drawing.Color.PowderBlue
                    e.Item.ForeColor = System.Drawing.Color.Black
                    CType(e.Item.Cells(1).FindControl("imgEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = True
                    lblPendingData.Visible = True


                    'S.SANDEEP |17-JAN-2019| -- START
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).ToolTip = e.Item.Cells(11).Text
                    If CInt(e.Item.Cells(12).Text) = clsEmployeeMovmentApproval.enOperationType.EDITED Then
                        CType(e.Item.Cells(0).FindControl("imgDetail"), LinkButton).Visible = True
                        CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END

                Else
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = False

                    If e.Item.Cells(6).Text.Trim <> "&nbsp;" AndAlso CBool(e.Item.Cells(6).Text) = True Then
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    End If
                End If


                'Gajanan (28-May-2018) -- Start
                'Issue - Rehire Employee Data Also Edit And Delete.
                If Convert.ToInt32(e.Item.Cells(9).Text) > 0 Then
                    CType(e.Item.Cells(1).FindControl("imgEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = False
                    e.Item.BackColor = System.Drawing.Color.Orange
                End If
                'Gajanan (28-May-2018) -- End

                'If CBool(e.Item.Cells(5).Text) Then
                '    Dim lnkButton As LinkButton = CType(e.Item.Cells(5).FindControl("ImgDelete"), LinkButton)
                '    lnkButton.Visible = False
                'End If
                'Gajanan (28-May-2018) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvHistory_ItemDataBound :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvHistory_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvHistory.ItemCommand

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objACCT As New clsCCTranhead_Approval_Tran
        Dim objECCT As clsemployee_cctranhead_tran
        'Gajanan [4-Sep-2020] -- End

        Try
            lblAppointDate.Visible = False
            txtAppointDate.Visible = False
            txtAppointDate.Text = ""
            Me.ViewState("Cctranheadunkid") = 0
            Me.ViewState.Add("Cctranheadunkid", CInt(e.Item.Cells(7).Text))
            If e.CommandName = "Edit" Then

                'S.SANDEEP |17-JAN-2019| -- START
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim Flag As Boolean = objACCT.isExist(Nothing, 0, mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True, CInt(Me.ViewState("Cctranheadunkid")))
                    If Flag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 113, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), Me)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END


                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                objECCT = New clsemployee_cctranhead_tran

                Call SetEditValue(objECCT)
                'Gajanan (28-May-2018) -- Start

                'Issue Appointment Date Was Not Clear On Edit So Next Data Will Not Edit
                mdtAppointmentDate = Nothing
                'Gajanan (28-May-2018) -- End
                Me.ViewState("AppointmentDate") = Nothing
                If CBool(e.Item.Cells(6).Text) = True Then

                    'Pinkal (28-Dec-2015) -- Start
                    'Enhancement - Working on Changes in SS for Employee Master.
                    'Me.ViewState.Add("AppointmentDate", eZeeDate.convertDate(e.Item.Cells(7).Text))
                    'txtAppointDate.Text = CDate(Me.ViewState("AppointmentDate")).ToString(Session("DateFormat"))
                    mdtAppointmentDate = eZeeDate.convertDate(e.Item.Cells(8).Text)
                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    'txtAppointDate.Text = CDate(mdtAppointmentDate).ToString(Session("DateFormat").ToString)
                    txtAppointDate.Text = CDate(mdtAppointmentDate).ToShortDateString
                    'Pinkal (16-Apr-2016) -- End
                    'Pinkal (28-Dec-2015) -- End

                    lblAppointDate.Visible = True
                    txtAppointDate.Visible = True
                End If
                'S.SANDEEP [07-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001988}
                cboEmployee.Enabled = False
                'S.SANDEEP [07-Feb-2018] -- END
            ElseIf e.CommandName = "Delete" Then
                'S.SANDEEP |17-JAN-2019| -- START
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim Flag As Boolean = objACCT.isExist(Nothing, 0, mblnIsCostCenter, objACCT._Employeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", , Nothing, True, CInt(Me.ViewState("Cctranheadunkid")))
                    If Flag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 113, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), Me)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END

                popup_DeleteReason.Reason = ""
                popup_DeleteReason.Show()

                'S.SANDEEP |17-JAN-2019| -- START
            ElseIf e.CommandName.ToUpper = "VIEW" Then
                If CInt(e.Item.Cells(7).Text) > 0 Then
                    Dim dr As DataRow() = mdtCostCenterList.Select(CType(Me.dgvHistory.Columns(7), BoundColumn).DataField & " = " & CInt(e.Item.Cells(7).Text) & " AND " & CType(Me.dgvHistory.Columns(10), BoundColumn).DataField & "= ''")
                    If dr.Length > 0 Then
                        Dim index As Integer = mdtCostCenterList.Rows.IndexOf(dr(0))
                        dgvHistory.SelectedIndex = index
                        dgvHistory.SelectedItemStyle.BackColor = Drawing.Color.LightCoral
                        dgvHistory.SelectedItemStyle.ForeColor = Drawing.Color.White
                    End If
                End If

                'S.SANDEEP |17-JAN-2019| -- START


                'S.SANDEEP |17-JAN-2019| -- END


            ElseIf e.CommandName.ToUpper = "VIEWREPORT" Then
                Popup_Viewreport._UserId = CInt(Session("UserId"))
                Popup_Viewreport._Priority = 0
                Popup_Viewreport._PrivilegeId = 1201
                Popup_Viewreport._FillType = clsEmployeeMovmentApproval.enMovementType.COSTCENTER
                Popup_Viewreport._DtType = Nothing
                Popup_Viewreport._IsResidentPermit = False
                Popup_Viewreport._FilterString = "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue)

                Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                If Me.ViewState("Cctranheadunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Cctranheadunkid")) > 0 Then
                    eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If

                Popup_Viewreport.Show()


                'Gajanan [12-NOV-2018] -- START
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvHistory_ItemCommand :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objACCT = Nothing
            objECCT = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)

            Me.lblAppointDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblAppointDate.ID, Me.lblAppointDate.Text)
            Me.lblReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblReason.ID, Me.lblReason.Text)
            Me.lblEffectiveDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEffectiveDate.ID, Me.lblEffectiveDate.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.dgvHistory.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvHistory.Columns(3).FooterText, Me.dgvHistory.Columns(3).HeaderText)
            Me.dgvHistory.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.dgvHistory.Columns(5).FooterText, Me.dgvHistory.Columns(5).HeaderText)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
   
End Class
