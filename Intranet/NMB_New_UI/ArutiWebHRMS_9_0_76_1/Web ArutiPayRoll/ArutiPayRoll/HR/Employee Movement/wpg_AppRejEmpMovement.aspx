﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wpg_AppRejEmpMovement.aspx.vb"
    Inherits="HR_Employee_Movement_wpg_AppRejEmpMovement" Title="Appove/Reject Employee Movement" %>

<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Cnf_YesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ViewMovementApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

 <script type="text/javascript">

        $("body").on("click", "[id*=ChkAll]", function() {
            var chkHeader = $(this);
            var grid = $(this).closest("table");
            $("[id*=ChkSelect]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=ChkSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAll]", grid);
            debugger;
            if ($("[id*=ChkSelect]", grid).length == $("[id*=ChkSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });
              
    </script>


    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Appove/Reject Employee Movement"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApprover" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <asp:HiddenField ID="hfApprover" runat="server" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtLevel" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <asp:HiddenField ID="hfLevel" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblMovement" runat="server" Text="Movement" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpMovement" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblSelectOperationType" runat="server" Text="Opration Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboOperationType" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtsearch" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 250px">
                                            <asp:GridView ID="gvApproveRejectMovement" runat="server" AutoGenerateColumns="False"
                                                Width="99%" CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="employeeunkid">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="ChkAll" runat="server" Text=" " />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="ChkSelect" runat="server" Text=" " />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Code" DataField="ecode" FooterText="dgcolhecode" />
                                                    <asp:BoundField HeaderText="Employee" DataField="ename" FooterText="dgcolhename" />
                                                    <asp:BoundField HeaderText="Effective Date" HeaderStyle-Width="130px" DataField="EffDate"
                                                        FooterText="dgcolhEffDate" />
                                                    <asp:BoundField HeaderText="Branch" DataField="branch" FooterText="dgcolhbranch" />
                                                    <asp:BoundField HeaderText="Department Group" DataField="deptgroup" FooterText="dgcolhdeptgroup" />
                                                    <asp:BoundField HeaderText="Department" DataField="dept" FooterText="dgcolhdept" />
                                                    <asp:BoundField HeaderText="Section Group" DataField="secgroup" FooterText="dgcolhsecgroup" />
                                                    <asp:BoundField HeaderText="Section" DataField="section" FooterText="dgcolhsection" />
                                                    <asp:BoundField HeaderText="Unit Group" DataField="unitgrp" FooterText="dgcolhunitgrp" />
                                                    <asp:BoundField HeaderText="Unit" DataField="unit" FooterText="dgcolhunit" />
                                                    <asp:BoundField HeaderText="Team" DataField="team" FooterText="dgcolhteam" />
                                                    <asp:BoundField HeaderText="Class Group" DataField="classgrp" FooterText="dgcolhclassgrp" />
                                                    <asp:BoundField HeaderText="Class" DataField="class" FooterText="dgcolhclass" />
                                                    <asp:BoundField HeaderText="Reason" DataField="CReason" FooterText="dgcolhCReason" />
                                                    <asp:BoundField HeaderText="Job Group" DataField="JobGroup" FooterText="dgcolhJobGroup" />
                                                    <asp:BoundField HeaderText="Job" DataField="Job" FooterText="dgcolhJob" />
                                                    <asp:BoundField HeaderText="Date1" DataField="dDate1" FooterText="dgcolhdDate1" />
                                                    <asp:BoundField HeaderText="Date2" DataField="dDate2" FooterText="dgcolhdDate2" />
                                                    <asp:BoundField HeaderText="Permit No." DataField="work_permit_no" FooterText="dgcolhwork_permit_no" />
                                                    <asp:BoundField HeaderText="Issue Place" DataField="issue_place" FooterText="dgcolhissue_place" />
                                                    <asp:BoundField HeaderText="Issue Date" DataField="IDate" FooterText="dgcolhIDate" />
                                                    <asp:BoundField HeaderText="Expiry Date" DataField="EDate" FooterText="dgcolhExDate" />
                                                    <asp:BoundField HeaderText="Country" DataField="Country" FooterText="dgcolhCountry" />
                                                    <asp:BoundField HeaderText="CostCenter" DataField="DispValue" FooterText="dgcolhDispValue" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRemark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-danger" />
                                <asp:Button ID="btnShowMyReport" runat="server" Text="My Report" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc2:Cnf_YesNo ID="Cnf_WithoutRemark" runat="server" Title="Aruti" />
                <uc3:Pop_report ID="Pop_report" runat="server" />
                <uc2:Cnf_YesNo ID="Cnf_LeaveIssued" runat="server" Title="Aruti" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
