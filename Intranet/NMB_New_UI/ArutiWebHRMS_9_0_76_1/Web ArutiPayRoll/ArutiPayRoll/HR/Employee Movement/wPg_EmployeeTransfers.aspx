﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_EmployeeTransfers.aspx.vb"
    Inherits="HR_wPg_EmployeeTransfers" Title="Employee Transfers" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewMovementApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Transfer Information"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                                        <i class="fas fa-sliders-h"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtEffectiveDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="divider divider-with-text-left">
                                            <span class="divider-inner-text">
                                                <asp:Label ID="lblallocation" runat="server" Text="Allocations"></asp:Label>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBranch" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboBranch" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblUnitGroup" runat="server" Text="Unit Group" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboUnitGroup" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDepartmentGroup" runat="server" Text="Dept. Group" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboDeptGroup" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblUnits" runat="server" Text="Units" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboUnits" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboDepartment" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTeam" runat="server" Text="Team" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboTeam" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblSectionGroup" runat="server" Text="Sec. Group" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboSecGroup" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblClassGroup" runat="server" Text="Class Group" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboClassGroup" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblSection" runat="server" Text="Sections" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboSections" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblClass" runat="server" Text="Class" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboClass" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="divider">
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblChangeReason" runat="server" Text="Change Reason" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboChangeReason" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnldata" runat="server">
                                            <asp:Label ID="lblAppointmentdate" runat="server" Text="Appoint Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtAppointDate" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSaveChanges" runat="server" CssClass="btn btn-primary" Text="Save Changes" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive" style="height: 350px;">
                                    <asp:DataGrid ID="dgTransfersList" runat="server" AutoGenerateColumns="False"  CssClass="table table-hover table-bordered"
                                        AllowPaging="false">
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" FooterText="brnView">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="imgView" runat="server" ToolTip="View" CommandName="ViewReport">
                                                                        <i class="fas fa-exclamation-circle text-info"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" FooterText="brnEdit">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit">
                                                           <i class="fas fa-pencil-alt text-primary"></i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="imgDetail" runat="server" ToolTip="View Detail" CommandName="View"
                                                        Visible="false">
                                                            <i class="fa fa-eye text-primary"></i> 
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                            <i class="fa fa-trash text-danger"></i> 
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn HeaderStyle-Width="100px" DataField="EffDate" HeaderText="Effective Date"
                                                ReadOnly="true" FooterText="dgcolhChangeDate"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="branch" HeaderText="Branch" ReadOnly="true" FooterText="dgcolhBranch">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="deptgroup" HeaderText="Dept. Group" ReadOnly="true" FooterText="dgcolhDepartmentGrp">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="dept" HeaderText="Department" ReadOnly="true" FooterText="dgcolhDepartment">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="secgroup" HeaderText="Sec. Group" ReadOnly="true" FooterText="dgcolhSecGroup">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="section" HeaderText="Sections" ReadOnly="true" FooterText="dgcolhSection">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="unitname" HeaderText="Unit Group" ReadOnly="true" FooterText="dgcolhUnitGrp">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="unit" HeaderText="Unit" ReadOnly="true" FooterText="dgcolhUnit">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="team" HeaderText="Team" ReadOnly="true" FooterText="dgcolhTeam">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="classgrp" HeaderText="Class Group" ReadOnly="true" FooterText="dgcolhClsGrp">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="class" HeaderText="Class" ReadOnly="true" FooterText="dgcolhClass">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="CReason" HeaderText="Change Reason" ReadOnly="true" FooterText="dgcolhReason">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="transferunkid" HeaderText="objdgcolhtransferunkid" Visible="false"
                                                FooterText="objdgcolhtransferunkid"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="isfromemployee" HeaderText="objdgcolhFromEmp" Visible="false"
                                                FooterText="objdgcolhFromEmp"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="adate" HeaderText="dgcolhAppointdate" Visible="false"
                                                FooterText="objdgcolhAppointdate"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="rehiretranunkid" HeaderText="rehiretranunkid" Visible="false">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" Visible="false"
                                                FooterText="objdgcolhOperationType"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="operationtypeid" HeaderText="Operationtype Id" Visible="false"
                                                FooterText="objdgcolhOperationTypeId"></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Label ID="objlblCaption" runat="server" Text="Employee Rehired" CssClass="label label-warning"></asp:Label>
                                <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" CssClass="label label-info bg-pw"></asp:Label>
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To Delete?:" />
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
