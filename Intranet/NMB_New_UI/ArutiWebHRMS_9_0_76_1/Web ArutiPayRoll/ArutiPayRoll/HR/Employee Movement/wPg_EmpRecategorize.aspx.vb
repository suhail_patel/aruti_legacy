﻿Option Strict On 'Shani(19-MAR-2016)
#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
Imports System.Drawing
#End Region

Partial Class HR_wPg_EmpRecategorize
    Inherits Basepage

#Region "Private Variables"

    Private Shared ReadOnly mstrModuleName As String = "frmEmployeeRecategorize"
    Dim DisplayMessage As New CommonCodes


    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private objCategorize As New clsemployee_categorization_Tran
    'Gajanan [4-Sep-2020] -- End

    'S.SANDEEP [20-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#244}

    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private objACategorization As New clsCategorization_Approval_Tran
    'Gajanan [4-Sep-2020] -- End

    'S.SANDEEP [20-JUN-2018] -- END
    Private mintEmployeeID As Integer = -1
    Private mstrEmployeeCode As String = ""
    Private xCurrentAllocation As Dictionary(Of Integer, String)

    'Gajanan [15-NOV-2018] -- START
    Private mintTransactionId As Integer = 0
    'Gajanan [15-NOV-2018] -- END

    'S.SANDEEP |17-JAN-2019| -- START
    'ENHANCEMENT:Movement Approval Flow Edit / Delete
    Private mdtRecategorizeList As DataTable
    'S.SANDEEP |17-JAN-2019| -- END


    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private mstrAdvanceFilter As String = ""
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region "Page's Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

          
            'Gajanan [12-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            objlblCaption.Visible = False
            lblPendingData.Visible = False
            'Gajanan [12-NOV-2018] -- END
            If Not IsPostBack Then

                SetLanguage()

                'Gajanan [4-Sep-2020] -- Start
                'Performance Change: Memory Leak Issue 
                GC.Collect()
                'Gajanan [4-Sep-2020] -- End

                SetVisibility()
                Call FillCombo()
                If Me.Request.QueryString.Count > 0 Then
                    Dim mintEmployeeID As Integer = 0
                    Try
                        mintEmployeeID = CInt(b64decode(Server.UrlDecode(Request.QueryString.ToString.Substring(3))))
                    Catch
                        Session("clsuser") = Nothing
                        DisplayMessage.DisplayMessage("Invalid Page Address.", Me, "../../index.aspx")
                        Exit Sub
                    End Try
                    cboEmployee.SelectedValue = CStr(mintEmployeeID)
                    cboEmployee.Enabled = False
                    cboEmployee_SelectedIndexChanged(sender, e)
                End If
                Fill_Grid()
            Else
                mstrEmployeeCode = CStr(Me.ViewState("EmployeeCode"))
                'Gajanan [15-NOV-2018] -- START
                mintTransactionId = CInt(Me.ViewState("mintTransactionId"))
                'Gajanan [15-NOV-2018] -- End

                'S.SANDEEP |17-JAN-2019| -- START
                'ENHANCEMENT:Movement Approval Flow Edit / Delete
                mdtRecategorizeList = CType(Me.ViewState("mdtRecategorizeList"), DataTable)
                'S.SANDEEP |17-JAN-2019| -- END
            End If

            'Shani (08-Dec-2016) -- Start
            'Enhancement -  Add Employee Allocaion/Date Privilage
            btnSave.Enabled = CBool(Session("AllowToChangeEmpRecategorize"))
            'Shani (08-Dec-2016) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)
            Me.ViewState.Add("EmployeeCode", mstrEmployeeCode)
            'Gajanan [15-NOV-2018] -- START
            Me.ViewState.Add("mintTransactionId", mintTransactionId)
            'Gajanan [15-NOV-2018] -- END

            'S.SANDEEP |17-JAN-2019| -- START
            'ENHANCEMENT:Movement Approval Flow Edit / Delete


            Me.ViewState.Add("mdtRecategorizeList", mdtRecategorizeList)
            'S.SANDEEP |17-JAN-2019| -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region "Private Methods"

    Private Sub SetVisibility()
        Dim objJob As New clsJobs
        Try
            'Gajanan (29-May-2018) -- Start
            'Enhancement -Job Filter.
            'lblGrade.Enabled = objJob.IsJobsByGrades
            'cboGrade.Enabled = objJob.IsJobsByGrades
            'lblGrade.Enabled = False
            'cboGrade.Enabled = False
            'Gajanan (29-May-2018) -- End
            lblGradeLevel.Visible = False
            cboGradeLevel.Visible = False
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetVisibility:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objJob = Nothing
        End Try
    End Sub

    Private Sub FillCombo()

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objEmployee As New clsEmployee_Master
        Dim dsComboList As New DataSet
        Dim objJobGroup As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objGrade As New clsGrade
        Dim objGradeLevel As New clsGradeLevel
        Dim objCommon As New clsCommon_Master
        'Gajanan (29-May-2018) -- Start
        'Enhancement -Job Filter.
        Dim objBranch As New clsStation
        Dim objUnitGroup As New clsUnitGroup
        Dim objDeptGroup As New clsDepartmentGroup
        Dim objUnit As New clsUnits
        Dim objDepartment As New clsDepartment
        Dim objTeam As New clsTeams
        Dim objSectionGrp As New clsSectionGroup
        Dim objClassGroup As New clsClassGroup
        Dim objSection As New clsSections
        Dim objClass As New clsClass
        'Gajanan (29-May-2018) -- End
        'Gajanan [4-Sep-2020] -- End

        Try
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsComboList = objEmployee.GetEmployeeList("EmployeeList", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB



            'dsComboList = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
            '                                CInt(Session("UserId")), _
            '                                CInt(Session("Fin_year")), _
            '                                CInt(Session("CompanyUnkId")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                CStr(Session("UserAccessModeSetting")), True, _
            '                                CBool(Session("IsIncludeInactiveEmp")), "EmployeeList", True, , , , , , , , , , , , , , , , True) 'S.SANDEEP [05-Mar-2018] -- START {#ARUTI-18} {Reinstatement Date Included} -- END
            ''Shani(24-Aug-2015) -- End
            'With cboEmployee
            '    'Nilay (09-Aug-2016) -- Start
            '    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            '    '.DataTextField = "employeename"
            '    .DataTextField = "EmpCodeName"
            '    'Nilay (09-Aug-2016) -- End
            '    .DataValueField = "employeeunkid"
            '    .DataSource = dsComboList.Tables("EmployeeList")
            '    .SelectedValue = CStr(0)
            '    .DataBind()
            'End With

            FillEmployeeCombo()
            'Gajanan [11-Dec-2019] -- End


            dsComboList = objJobGroup.getComboList("JobGroup", True)
            With cboJobGroup
                .DataValueField = "jobgroupunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objJob.getComboList("Job", True)
            With cboJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objGrade.getComboList("Grade", True)
            With cboGrade
                .DataValueField = "gradeunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
                'Gajanan (29-May-2018) -- Start
                'Issue: Clear Combobox Text unnecessary line
                '.SelectedItem.Text = ""
                'Gajanan (29-May-2018) -- End

            End With

            'Gajanan (29-May-2018) -- Start
            'Issue/Enhancement - No Filter For Job.
            dsComboList = objBranch.getComboList("List", True)
            With cbobranch
                .DataValueField = "stationunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objUnitGroup.getComboList("List", True)
            With cbounitgroup
                .DataValueField = "unitgroupunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objDeptGroup.getComboList("List", True)
            With cbodeptgroup
                .DataValueField = "deptgroupunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objUnit.getComboList("List", True)
            With cbounit
                .DataValueField = "unitunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With


            dsComboList = objDepartment.getComboList("List", True)
            With cbodepartment
                .DataValueField = "departmentunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objTeam.getComboList("List", True)
            With cboteam
                .DataValueField = "teamunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objSectionGrp.getComboList("List", True)
            With cbosectiongroup
                .DataValueField = "sectiongroupunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objClassGroup.getComboList("List", True)
            With cboclassgroup
                .DataValueField = "classgroupunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objSection.getComboList("List", True)
            With cbosection
                .DataValueField = "sectionunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objClass.getComboList("List", True)
            With cboclass
                .DataValueField = "classesunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With
            'Gajanan (23-May-2018) -- End

            dsComboList = objGradeLevel.getComboList("GradeLevel", True)
            With cboGradeLevel
                .DataValueField = "gradelevelunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables(0)
                .SelectedValue = CStr(0)
                .DataBind()
            End With

            dsComboList = objCommon.getComboList(clsCommon_Master.enCommonMaster.RECATEGORIZE, True, "List")
            With cboChangeReason
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsComboList.Tables("List")
                .SelectedValue = CStr(0)
                .DataBind()
            End With

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objEmployee = Nothing : objJobGroup = Nothing : objJob = Nothing : objGrade = Nothing
            objGradeLevel = Nothing : objCommon = Nothing : objBranch = Nothing : objUnitGroup = Nothing
            objDeptGroup = Nothing : objUnit = Nothing : objDepartment = Nothing : objTeam = Nothing
            objSectionGrp = Nothing : objClassGroup = Nothing : objSection = Nothing : objClass = Nothing

            If IsNothing(dsComboList) = False Then
                dsComboList.Clear()
                dsComboList = Nothing
            End If
            'Gajanan [4-Sep-2020] -- End

        End Try
    End Sub

    Private Sub Set_ReCategorization()

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objCategorize As New clsemployee_categorization_Tran
        Dim dsCategorize As New DataSet
        'Gajanan [4-Sep-2020] -- End

        Try
            dsCategorize = objCategorize.Get_Current_Job(Now.Date, CInt(cboEmployee.SelectedValue))
            If dsCategorize.Tables(0).Rows.Count > 0 Then
                'cboJobGroup.SelectedValue = CInt(dsCategorize.Tables(0).Rows(0).Item("jobgroupunkid")).ToString
                'cboJobGroup_SelectedIndexChanged(New Object(), New EventArgs())
                cboJob.SelectedValue = CInt(dsCategorize.Tables(0).Rows(0).Item("jobunkid")).ToString
                cboJob_SelectedIndexChanged(New Object(), New EventArgs())

                If CInt(dsCategorize.Tables(0).Rows(0).Item("gradeunkid")) > 0 Then
                    cboGrade.SelectedValue = CInt(dsCategorize.Tables(0).Rows(0).Item("gradeunkid")).ToString
                Else
                    cboGrade.SelectedItem.Text = ""
                End If
                If CInt(dsCategorize.Tables(0).Rows(0).Item("gradelevelunkid")) > 0 Then
                    cboGradeLevel.SelectedValue = CInt(dsCategorize.Tables(0).Rows(0).Item("gradelevelunkid")).ToString
                Else
                    cboGradeLevel.SelectedItem.Text = ""
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Set_ReCategorization:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objCategorize = Nothing
            If IsNothing(dsCategorize) = False Then
                dsCategorize.Clear()
                dsCategorize = Nothing
            End If
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dsData As New DataSet

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objCategorize As New clsemployee_categorization_Tran
        Dim objACategorization As New clsCategorization_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                dsData = objCategorize.GetList("List", CInt(cboEmployee.SelectedValue)).Clone
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                dgvHistory.Columns(1).Visible = False
                dgvHistory.Columns(2).Visible = False
                'Varsha Rana (17-Oct-2017) -- End

                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                dgvHistory.Columns(0).Visible = False
                'Gajanan [12-NOV-2018] -- END
            Else
                dsData = objCategorize.GetList("List", CInt(cboEmployee.SelectedValue))
                'Varsha Rana (17-Oct-2017) -- Start
                'Enhancement - Give user privileges.
                dgvHistory.Columns(1).Visible = CBool(Session("AllowToEditRecategorizeEmployeeDetails"))
                dgvHistory.Columns(2).Visible = CBool(Session("AllowToDeleteRecategorizeEmployeeDetails"))
                'Varsha Rana (17-Oct-2017) -- End

                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                dgvHistory.Columns(0).Visible = Not CBool(Session("SkipEmployeeMovementApprovalFlow"))

                Dim dcol As New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "tranguid"
                    .DefaultValue = ""
                End With
                dsData.Tables(0).Columns.Add(dcol)

                'S.SANDEEP |17-JAN-2019| -- START
                'ENHANCEMENT:Movement Approval Flow Edit / Delete
                dcol = New DataColumn
                With dcol
                    .DataType = GetType(System.Int32)
                    .ColumnName = "operationtypeid"
                    .DefaultValue = clsEmployeeMovmentApproval.enOperationType.ADDED
                End With
                dsData.Tables(0).Columns.Add(dcol)

                dcol = New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "OperationType"
                    .DefaultValue = ""
                End With
                dsData.Tables(0).Columns.Add(dcol)
                'S.SANDEEP |17-JAN-2019| -- End


                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim dsPending As New DataSet
                    dsPending = objACategorization.GetList("List", CInt(cboEmployee.SelectedValue))
                    If dsPending.Tables(0).Rows.Count > 0 Then
                        For Each row As DataRow In dsPending.Tables(0).Rows
                            dsData.Tables(0).ImportRow(row)
                        Next
                    End If
                End If
                'Gajanan [12-NOV-2018] -- END
            End If
            dgvHistory.DataSource = dsData.Tables(0)
            dgvHistory.DataBind()
            mdtRecategorizeList = dsData.Tables(0)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Grid:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objCategorize = Nothing
            objACategorization = Nothing
            If IsNothing(dsData) = False Then
                dsData.Clear()
                dsData = Nothing
            End If
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub


    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    Private Function Validation(ByVal objCategorize As clsemployee_categorization_Tran) As Boolean
        'Gajanan [29-Oct-2020] -- Start   
        'Enhancement:Worked On Succession Module
        Dim objjob As New clsJobs
        'Gajanan [29-Oct-2020] -- End

        'Gajanan [4-Sep-2020] -- End
        Try
            If dtEffectiveDate.IsNull Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue."), Me)
                dtEffectiveDate.Focus()
                Return False
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry, Employee is mandatory information. Please selecte Employee to continue."), Me)
                cboEmployee.Focus()
                Return False
            End If

            'If CInt(cboJobGroup.SelectedValue) <= 0 Then
            '    'Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, Job Group is mandatory information. Please selecte atleast Job Group to continue."), Me)
            '    cboJobGroup.Focus()
            '    Return False
            'End If

            If CInt(cboJob.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, Job is mandatory information. Please selecte atleast Job to continue."), Me)
                cboJob.Focus()
                Return False
            End If

            If CInt(cboChangeReason.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, Change Reason is mandatory information. Please selecte Change Reason to continue."), Me)
                cboChangeReason.Focus()
                Return False
            End If

            If CDate(Me.ViewState("AppointmentDate")) <> Nothing Then
                If CDate(Me.ViewState("AppointmentDate")).Date <> dtEffectiveDate.GetDate.Date Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Sorry, This is system generate entry. Please set effective date as employee appointment date."), Me)
                    dtEffectiveDate.Focus()
                    Return False
                End If
            End If


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB
            If objCategorize.isExist(dtEffectiveDate.GetDate(), CInt(cboEmployee.SelectedValue), CInt(cboJobGroup.SelectedValue), CInt(cboJob.SelectedValue), -1, -1, 0) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_categorization_Tran", 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date."), Me)
                Return False
            End If
            'Gajanan [11-Dec-2019] -- End


            'Gajanan [29-Oct-2020] -- Start   
            'Enhancement:Worked On Succession Module
            If objjob.IsJobAssignAsKeyRoleToEmployee(CInt(cboJob.SelectedValue), CInt(cboEmployee.SelectedValue), dtEffectiveDate.GetDate()) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, you can't assign this job to multiple employee. Reason:This job is declare as key-role and it can be assign to one and only one employee."), Me)
                Return False
            End If
            'Gajanan [29-Oct-2020] -- End

            'Gajanan [12-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                Dim strMsg As String = String.Empty
                Dim objPMovement As New clsEmployeeMovmentApproval
                strMsg = objPMovement.IsOtherMovmentApproverPresent(clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), 1193, CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString, Nothing, CInt(cboEmployee.SelectedValue), Nothing)
                If strMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(strMsg, Me)
                    objPMovement = Nothing
                    Return False
                End If
                objPMovement = Nothing

                'S.SANDEEP |16-JAN-2019| -- START
                If objCategorize.isExist(dtEffectiveDate.GetDate(), CInt(cboEmployee.SelectedValue), -1, -1, -1, -1, mintTransactionId, ) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_categorization_Tran", 1, "Sorry, Re-Categorize information is already present for the selected effective date."), Me)
                    Return False
                End If

                Dim dsList As New DataSet
                dsList = objCategorize.Get_Current_Job(dtEffectiveDate.GetDate(), CInt(cboEmployee.SelectedValue))
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    If CInt(dsList.Tables(0).Rows(0)("jobunkid")) = CInt(cboJob.SelectedValue) AndAlso CInt(dsList.Tables(0).Rows(0)("jobgroupunkid")) = CInt(cboJobGroup.SelectedValue) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_categorization_Tran", 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date."), Me)
                        Return False
                    End If
                End If
                dsList = Nothing

                If objCategorize.isExist(dtEffectiveDate.GetDate(), CInt(cboEmployee.SelectedValue), CInt(cboJobGroup.SelectedValue), CInt(cboJob.SelectedValue), -1, -1, mintTransactionId) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsemployee_categorization_Tran", 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date."), Me)
                    Return False
                End If
                'S.SANDEEP |16-JAN-2019| -- END

                'Gajanan [29-Oct-2020] -- Start   
                'Enhancement:Worked On Succession Module
                If objjob.IsJobAsKeyRoleToIntoApprovalFlow(CInt(cboJob.SelectedValue), CInt(cboEmployee.SelectedValue)) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Sorry, you can't assign this job to multiple employee. Reason:This job is declare as key-role and it is already in approver. "), Me)
                    Return False
                End If
                'Gajanan [29-Oct-2020] -- End

            End If
            'Gajanan [12-NOV-2018] -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
        Return True
    End Function

    Private Sub SetEditValue()

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objCategorize As New clsemployee_categorization_Tran
        'Gajanan [4-Sep-2020] -- End

        Try
            If Me.ViewState("Categorizationtranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Categorizationtranunkid")) > 0 Then
                objCategorize._Categorizationtranunkid = CInt(Me.ViewState("Categorizationtranunkid"))
                'Gajanan [15-NOV-2018] -- START
                mintTransactionId = objCategorize._Categorizationtranunkid
                'Gajanan [15-NOV-2018] -- END
            End If

            dtEffectiveDate.SetDate = CDate(objCategorize._Effectivedate).Date
            cboEmployee.SelectedValue = objCategorize._Employeeunkid.ToString
            cboJobGroup.SelectedValue = objCategorize._JobGroupunkid.ToString
            'cboJobGroup_SelectedIndexChanged(New Object(), New EventArgs())
            cboJob.SelectedValue = objCategorize._Jobunkid.ToString
            cboJob_SelectedIndexChanged(New Object(), New EventArgs())
            cboGrade.SelectedValue = objCategorize._Gradeunkid.ToString
            cboGradeLevel.SelectedValue = objCategorize._Gradelevelunkid.ToString
            cboChangeReason.SelectedValue = objCategorize._Changereasonunkid.ToString
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetEditValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objCategorize = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

    'Gajanan [4-Sep-2020] -- Start
    'Performance Change: Memory Leak Issue 
    'Private Sub SetValue()
    Private Sub SetValue(ByVal objCategorize As clsemployee_categorization_Tran, _
                         ByVal objACategorization As clsCategorization_Approval_Tran)
        'Gajanan [4-Sep-2020] -- End
        Try

            If Me.ViewState("Categorizationtranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Categorizationtranunkid")) > 0 Then
                objCategorize._Categorizationtranunkid = CInt(Me.ViewState("Categorizationtranunkid"))
            End If
            'Gajanan [12-NOV-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            'objCategorize._Effectivedate = dtEffectiveDate.GetDate
            'objCategorize._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'objCategorize._JobGroupunkid = CInt(cboJobGroup.SelectedValue)
            'objCategorize._Jobunkid = CInt(cboJob.SelectedValue)
            'objCategorize._Gradeunkid = CInt(cboGrade.SelectedValue)
            'objCategorize._Gradelevelunkid = CInt(cboGradeLevel.SelectedValue)
            'objCategorize._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
            'objCategorize._Isvoid = False
            'objCategorize._Statusunkid = 0
            'objCategorize._Userunkid = CInt(Session("UserId"))
            'objCategorize._Voiddatetime = Nothing
            'objCategorize._Voidreason = ""
            'objCategorize._Voiduserunkid = -1

            'Blank_ModuleName()
            'objCategorize._WebFormName = mstrModuleName
            'objCategorize._WebClientIP = CStr(Session("IP_ADD"))
            'objCategorize._WebHostName = CStr(Session("HOST_NAME"))
            'StrModuleName2 = "mnuGeneralMaster"
            'StrModuleName3 = "mnuCoreSetups"

            'Gajanan [15-NOV-2018] -- START
            'If ConfigParameter._Object._SkipEmployeeMovementApprovalFlow Then
            If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                'Gajanan [15-NOV-2018] -- End
                objCategorize._Effectivedate = dtEffectiveDate.GetDate
                objCategorize._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objCategorize._JobGroupunkid = CInt(cboJobGroup.SelectedValue)
                objCategorize._Jobunkid = CInt(cboJob.SelectedValue)
                objCategorize._Gradeunkid = CInt(cboGrade.SelectedValue)
                objCategorize._Gradelevelunkid = CInt(cboGradeLevel.SelectedValue)
                objCategorize._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                objCategorize._Isvoid = False
                objCategorize._Statusunkid = 0
                objCategorize._Userunkid = CInt(Session("UserId"))
                objCategorize._Voiddatetime = Nothing
                objCategorize._Voidreason = ""
                objCategorize._Voiduserunkid = -1

                Blank_ModuleName()
                objCategorize._WebFormName = mstrModuleName
                objCategorize._WebClientIP = CStr(Session("IP_ADD"))
                objCategorize._WebHostName = CStr(Session("HOST_NAME"))
                StrModuleName2 = "mnuGeneralMaster"
                StrModuleName3 = "mnuCoreSetups"
            Else
                objACategorization._Audittype = enAuditType.ADD
                objACategorization._Audituserunkid = CInt(Session("UserId"))
                objACategorization._Effectivedate = dtEffectiveDate.GetDate
                objACategorization._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objACategorization._Jobgroupunkid = CInt(cboJobGroup.SelectedValue)
                objACategorization._Jobunkid = CInt(cboJob.SelectedValue)
                objACategorization._Gradeunkid = CInt(cboGrade.SelectedValue)
                objACategorization._Gradelevelunkid = CInt(cboGradeLevel.SelectedValue)
                objACategorization._Changereasonunkid = CInt(cboChangeReason.SelectedValue)
                objACategorization._Isvoid = False
                objACategorization._Statusunkid = 0
                objACategorization._Voiddatetime = Nothing
                objACategorization._Voidreason = ""
                objACategorization._Voiduserunkid = -1
                objACategorization._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                objACategorization._Tranguid = Guid.NewGuid.ToString()
                objACategorization._Transactiondate = Now
                objACategorization._Remark = ""
                objACategorization._Rehiretranunkid = 0
                objACategorization._Mappingunkid = 0
                objACategorization._Isweb = True
                objACategorization._Isfinal = False
                objACategorization._Ip = Session("IP_ADD").ToString()
                objACategorization._Hostname = Session("HOST_NAME").ToString()
                objACategorization._Form_Name = mstrModuleName

                'S.SANDEEP |17-JAN-2019| -- START
                'ENHANCEMENT:Movement Approval Flow Edit / Delete
                If Me.ViewState("Categorizationtranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Categorizationtranunkid")) > 0 Then
                    objACategorization._Categorizationtranunkid = mintTransactionId
                    objACategorization._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    objACategorization._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                'S.SANDEEP |17-JAN-2019| -- END

                Blank_ModuleName()
                StrModuleName2 = "mnuGeneralMaster"
                StrModuleName3 = "mnuCoreSetups"
            End If
            'Gajanan [12-NOV-2018] -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            dtEffectiveDate.SetDate = Nothing
            cboJobGroup.SelectedValue = CStr(0)
            cboJob.SelectedValue = CStr(0)
            cboGrade.SelectedValue = CStr(0)
            cboGradeLevel.SelectedValue = CStr(0)
            cboChangeReason.SelectedValue = CStr(0)
            SetVisibility()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ClearControls:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub SetAllocationForMail()
        Try
            xCurrentAllocation = New Dictionary(Of Integer, String)

            '''''''''''' JOB GROUP
            If CInt(cboJobGroup.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.JOB_GROUP, cboJobGroup.SelectedItem.Text)
            Else
                xCurrentAllocation.Add(enAllocation.JOB_GROUP, "&nbsp;")
            End If

            '''''''''''' JOB
            If CInt(cboJob.SelectedValue) > 0 Then
                xCurrentAllocation.Add(enAllocation.JOBS, cboJob.SelectedItem.Text)
            Else
                xCurrentAllocation.Add(enAllocation.JOBS, "&nbsp;")
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetAllocationForMail:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Private Sub FillEmployeeCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Try

            dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          CBool(Session("IsIncludeInactiveEmp")), "Emp", True, , , , , , , , , , , , , , , mstrAdvanceFilter, True)

            With cboEmployee
                .DataTextField = "EmpCodeName"
                .DataValueField = "employeeunkid"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = CStr(0)
                .DataBind()
            End With


        Catch ex As Exception
            'Hemant (13 Aug 2020) -- Start
            'DisplayError.Show("-1", ex.Message, "fillEmployeeCombo", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Hemant (13 Aug 2020) -- End
        Finally
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region " Combobox Event(s) "

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objCategorize As New clsemployee_categorization_Tran
        Dim objACategorization As New clsCategorization_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Dim objEmployee As New clsEmployee_Master

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim dsComboList As DataSet = objEmployee.GetEmployeeList("EmployeeList", False, True, CInt(cboEmployee.SelectedValue), , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))
                Dim dsComboList As DataSet = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                CStr(Session("UserAccessModeSetting")), True, _
                                                CBool(Session("IsIncludeInactiveEmp")), "EmployeeList", False, _
                                                CInt(cboEmployee.SelectedValue))
                'Shani(24-Aug-2015) -- End
                If dsComboList IsNot Nothing AndAlso dsComboList.Tables(0).Rows.Count > 0 Then
                    mstrEmployeeCode = CStr(dsComboList.Tables(0).Rows(0)("employeecode"))
                End If
                dsComboList.Clear()
                dsComboList = Nothing
                objEmployee = Nothing
                Call Fill_Grid()
                Set_ReCategorization()
            Else
                Fill_Grid()
                ClearControls()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboEmployee_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'Private Sub cboJobGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJobGroup.SelectedIndexChanged, _
    '                                                                                                                 cbobranch.SelectedIndexChanged, _
    '                                                                                                                 cbodeptgroup.SelectedIndexChanged, _
    '                                                                                                                 cbodepartment.SelectedIndexChanged, _
    '                                                                                                                 cbosectiongroup.SelectedIndexChanged, _
    '                                                                                                                 cbosection.SelectedIndexChanged, _
    '                                                                                                                 cbounitgroup.SelectedIndexChanged, _
    '                                                                                                                 cbounit.SelectedIndexChanged, _
    '                                                                                                                 cboteam.SelectedIndexChanged, _
    '                                                                                                                 cboclassgroup.SelectedIndexChanged, _
    '                                                                                                                 cboclass.SelectedIndexChanged, _
    '                                                                                                                 cboGrade.SelectedIndexChanged
    '    Dim objCategorize As New clsemployee_categorization_Tran
    '    Dim objJob As New clsJobs
    '    Dim dsCombos As New DataSet
    '    Try
    '        dsCombos = objCategorize.getJobComboList("List", True, _
    '                                                 CInt(cbobranch.SelectedValue), _
    '                                                 CInt(cbodeptgroup.SelectedValue), _
    '                                                 CInt(cbodepartment.SelectedValue), _
    '                                                 CInt(cbosectiongroup.SelectedValue), _
    '                                                 CInt(cbosection.SelectedValue), _
    '                                                 CInt(cbounitgroup.SelectedValue), _
    '                                                 CInt(cbounit.SelectedValue), _
    '                                                 CInt(cboteam.SelectedValue), _
    '                                                 CInt(cboclassgroup.SelectedValue), _
    '                                                 CInt(cboclass.SelectedValue), _
    '                                                 CInt(cboGrade.SelectedValue), _
    '                                                 CInt(cboJobGroup.SelectedValue))

    '        Dim intProvJobUnkId As String = cboJob.SelectedValue

    '        With cboJob
    '            .DataValueField = "jobunkid"
    '            .DataTextField = "name"
    '            .DataSource = dsCombos.Tables("List")
    '            .DataBind()
    '            Try
    '                .SelectedValue = intProvJobUnkId
    '            Catch ex As Exception
    '                .SelectedValue = "0"
    '            End Try
    '            If .SelectedValue Is Nothing Then
    '                .SelectedValue = "0"
    '            End If
    '        End With

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("cboEmployee_SelectedIndexChanged:- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    Finally
    '        objJob = Nothing
    '        dsCombos = Nothing
    '    End Try
    'End Sub

    Private Sub cboJob_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboJob.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objJob As New clsJobs
        Try
            Dim objGrade As New clsGrade
            Dim dsComboList As DataSet = objGrade.getComboList("Grade", False)
            If CInt(cboJob.SelectedValue) > 0 Then

                dsCombos = objJob.GetJobsByGrades(CInt(cboJob.SelectedValue))

                If dsCombos IsNot Nothing AndAlso dsCombos.Tables(0).Rows.Count > 0 Then

                    Dim dtTable As DataTable = New DataView(dsComboList.Tables(0), "gradeunkid = " & CInt(dsCombos.Tables(0).Rows(0)("jobgradeunkid")), "", DataViewRowState.CurrentRows).ToTable
                    If dtTable.Rows.Count > 0 Then
                        cboGrade.SelectedValue = CStr(CInt(dtTable.Rows(0)("gradeunkid")))
                    Else
                        cboGrade.SelectedValue = CStr(0)
                        cboGrade.SelectedItem.Text = ""
                    End If
                Else
                    cboGrade.SelectedValue = CStr(0)
                    cboGrade.SelectedItem.Text = ""
                End If
            Else
                If dsComboList IsNot Nothing Then
                    cboGrade.SelectedValue = CStr(0)
                    cboGrade.SelectedItem.Text = ""
                End If
            End If
            dsComboList.Clear()
            dsComboList = Nothing
            objGrade = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboJob_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objJob = Nothing
            dsCombos.Dispose()
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objCategorize As New clsemployee_categorization_Tran
        Dim objACategorization As New clsCategorization_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            'If Validation() = False Then Exit Sub
            'Call SetValue()

            If Validation(objCategorize) = False Then Exit Sub
            Call SetValue(objCategorize, objACategorization)
            'Gajanan [4-Sep-2020] -- End

            If Me.ViewState("Categorizationtranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Categorizationtranunkid")) > 0 Then

                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'blnFlag = objCategorize.Update(CInt(Session("CompanyUnkId")), Nothing)

                'S.SANDEEP |17-JAN-2019| -- START
                'ENHANCEMENT:Movement Approval Flow Edit / Delete

                'blnFlag = objCategorize.Update(CInt(Session("CompanyUnkId")), CBool(Session("CreateADUserFromEmpMst")), Nothing)

                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then

                    'Pinkal (12-Oct-2020) -- Start
                    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                    'blnFlag = objCategorize.Update(CInt(Session("CompanyUnkId")), CBool(Session("CreateADUserFromEmpMst")))
                    blnFlag = objCategorize.Update(CInt(Session("CompanyUnkId")), CBool(Session("CreateADUserFromEmpMst")), CStr(Session("Database_Name")))
                    'Pinkal (12-Oct-2020) -- End


                Else
                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(Nothing, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(Nothing, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If


                    'Pinkal (12-Oct-2020) -- Start
                    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                    'blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, CBool(Session("CreateADUserFromEmpMst")))
                    blnFlag = objACategorization.Insert(Company._Object._Companyunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, CBool(Session("CreateADUserFromEmpMst")), CStr(Session("Database_Name")))
                    'Pinkal (12-Oct-2020) -- End



                    If blnFlag = False AndAlso objACategorization._Message <> "" Then
                        DisplayMessage.DisplayMessage(objACategorization._Message, Me)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END
                'Pinkal (18-Aug-2018) -- End

            ElseIf Me.ViewState("Categorizationtranunkid") Is Nothing OrElse CInt(Me.ViewState("Categorizationtranunkid")) <= 0 Then
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'Pinkal (18-Aug-2018) -- Start
                'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
                'blnFlag = objCategorize.Insert(CInt(Session("CompanyUnkId")), Nothing)
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                    blnFlag = objCategorize.Insert(CBool(Session("CreateADUserFromEmpMst")), CInt(Session("CompanyUnkId")), Nothing)
                Else

                    'S.SANDEEP |17-JAN-2019| -- START

                    'Check For Entry is Available In Approval For Delete
                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 110, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 111, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(Nothing, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 112, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If


                    'Check For Entry is Available In Approval For Edit

                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 110, "Sorry, Information is already present in approval process with selected date and allocation combination."), Me)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(objACategorization._Effectivedate, objACategorization._Employeeunkid, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 111, "Sorry, Information is already present in approval process with selected date."), Me)
                        Exit Sub
                    End If

                    blnFlag = objACategorization.isExist(Nothing, objACategorization._Employeeunkid, objACategorization._Jobgroupunkid, objACategorization._Jobunkid, objACategorization._Gradeunkid, objACategorization._Gradelevelunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True)
                    If blnFlag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 112, "Sorry, Information is already present in approval process with selected allocation combination."), Me)
                        Exit Sub
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END


                    'Pinkal (12-Oct-2020) -- Start
                    'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                    'blnFlag = objACategorization.Insert(CInt(Session("CompanyUnkId")), clsEmployeeMovmentApproval.enOperationType.ADDED, CBool(Session("CreateADUserFromEmpMst")))
                    blnFlag = objACategorization.Insert(CInt(Session("CompanyUnkId")), clsEmployeeMovmentApproval.enOperationType.ADDED, CBool(Session("CreateADUserFromEmpMst")), CStr(Session("Database_Name")))
                    'Pinkal (12-Oct-2020) -- End



                    If blnFlag = False AndAlso objACategorization._Message <> "" Then
                        DisplayMessage.DisplayMessage(objACategorization._Message, Me)
                        Exit Sub
                    End If

                End If
                'Pinkal (18-Aug-2018) -- End
                'Gajanan [12-NOV-2018] -- End

            End If
            If blnFlag = False AndAlso objCategorize._Message <> "" Then
                DisplayMessage.DisplayMessage(objCategorize._Message, Me)
                Exit Sub
            Else
                Call Fill_Grid()

                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'Call objCategorize.SendEmails(CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.SelectedItem.Text, xCurrentAllocation, CStr(Session("Notify_Allocation")), dtEffectiveDate.GetDate.Date, CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE)
                'Call objCategorize.SendEmails(CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.SelectedItem.Text, xCurrentAllocation, CStr(Session("Notify_Allocation")), dtEffectiveDate.GetDate.Date, CInt(Session("CompanyUnkId")), CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE)
                'Sohail (30 Nov 2017) -- End
                'Gajanan [15-NOV-2018] -- START
                'If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) Then
                    'Gajanan [15-NOV-2018] -- END
                    Call SetAllocationForMail()
                    Call objCategorize.SendEmails(CInt(cboEmployee.SelectedValue), mstrEmployeeCode, cboEmployee.SelectedItem.Text, xCurrentAllocation, CStr(Session("Notify_Allocation")), dtEffectiveDate.GetDate.Date, CInt(Session("CompanyUnkId")), CStr(Session("HOST_NAME")), CStr(Session("IP_ADD")), CStr(Session("UserName")), CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE)
                Else
                    Dim objPMovement As New clsEmployeeMovmentApproval

                    Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                    If Me.ViewState("Categorizationtranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Categorizationtranunkid")) > 0 Then
                        eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                    Else
                        eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                    End If

                    objPMovement.SendNotification(1, CStr(Session("Database_Name")), CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), 1193, clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE, Session("EmployeeAsOnDate").ToString, CInt(Session("UserId")), mstrModuleName, enLogin_Mode.MGR_SELF_SERVICE, CStr(Session("UserName")), eOperType, False, Nothing, 0, CInt(cboEmployee.SelectedValue).ToString(), "")
                    objPMovement = Nothing
                End If
                'Gajanan [12-NOV-2018] -- End
                Call Set_ReCategorization()
            End If
            Me.ViewState("Categorizationtranunkid") = Nothing
            Call ClearControls()
            'S.SANDEEP [07-Feb-2018] -- START
            'ISSUE/ENHANCEMENT : {#0001988}
            cboEmployee.Enabled = True
            'S.SANDEEP [07-Feb-2018] -- END
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Gajanan [15-NOV-2018] -- START
            'mintTransactionId = 0
            'Gajanan [15-NOV-2018] -- END

            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objACategorization = Nothing
            objCategorize = Nothing
            'Gajanan [4-Sep-2020] -- End

        End Try
    End Sub

    Protected Sub popup_DeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objCategorize As New clsemployee_categorization_Tran
        Dim objACategorization As New clsCategorization_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try

            Blank_ModuleName()
            objCategorize._WebFormName = mstrModuleName
            objCategorize._WebClientIP = CStr(Session("IP_ADD"))
            objCategorize._WebHostName = CStr(Session("HOST_NAME"))
            StrModuleName2 = "mnuGeneralMaster"
            StrModuleName3 = "mnuCoreSetups"


            objCategorize._Isvoid = True
            objCategorize._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objCategorize._Voidreason = popup_DeleteReason.Reason
            objCategorize._Voiduserunkid = CInt(Session("UserId"))
            'S.SANDEEP |17-JAN-2019| -- START

            'If objCategorize.Delete(CInt(Me.ViewState("Categorizationtranunkid")), CInt(Session("CompanyUnkId")), Nothing) = False Then
            '    If objCategorize._Message <> "" Then
            '        DisplayMessage.DisplayMessage(objCategorize._Message, Me)
            '    End If
            '    Exit Sub
            '    popup_DeleteReason.Hide()
            'End If

            If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                objACategorization._Isvoid = True
                objACategorization._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objACategorization._Voidreason = popup_DeleteReason.Reason
                objACategorization._Voiduserunkid = -1
                objACategorization._Audituserunkid = CInt(Session("UserId"))
                objACategorization._Isweb = True
                objACategorization._Ip = getIP()
                objACategorization._Hostname = getHostName()
                objACategorization._Form_Name = mstrModuleName
                If objACategorization.Delete(CInt(Me.ViewState("Categorizationtranunkid")), clsEmployeeMovmentApproval.enOperationType.DELETED, CInt(Session("CompanyUnkId")), CBool(Session("CreateADUserFromEmpMst")), Nothing) = False Then
                    If objACategorization._Message <> "" Then
                        DisplayMessage.DisplayMessage(objACategorization._Message, Me)
                    End If
                    Exit Sub
                    popup_DeleteReason.Hide()
                End If
            Else

                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                'If objCategorize.Delete(CInt(Me.ViewState("Categorizationtranunkid")), CInt(Session("CompanyUnkId")), Nothing) = False Then
                If objCategorize.Delete(CInt(Me.ViewState("Categorizationtranunkid")), CInt(Session("CompanyUnkId")), CBool(Session("CreateADUserFromEmpMst")), CStr(Session("Database_Name")), Nothing) = False Then
                    'Pinkal (12-Oct-2020) -- End
                    If objCategorize._Message <> "" Then
                        DisplayMessage.DisplayMessage(objCategorize._Message, Me)
                    End If
                    Exit Sub
                    popup_DeleteReason.Hide()
                End If
            End If

            'S.SANDEEP |17-JAN-2019| -- End

            Call Fill_Grid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popup_DeleteReason_buttonDelReasonYes_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Gajanan [4-Sep-2020] -- Start
            'Performance Change: Memory Leak Issue 
            objACategorization = Nothing
            objCategorize = Nothing
            'Gajanan [4-Sep-2020] -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Me.Request.QueryString.Count > 0 Then
                Response.Redirect("~/HR/Employee Movement/wPg_EmpMovementDates.aspx?ID=" & Server.UrlEncode(b64encode(enEmp_Dates_Transaction.DT_CONFIRMATION & "|" & CInt(cboEmployee.SelectedValue))), False)
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Gajanan [11-Dec-2019] -- Start   
    'Enhancement:Worked On December Cut Over Enhancement For NMB
    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            popupAdvanceFilter.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            popupAdvanceFilter.Hide()
            FillEmployeeCombo()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [11-Dec-2019] -- End

#End Region

#Region "DataGrid Events"

    Protected Sub dgvHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvHistory.ItemDataBound
        Try

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If (e.Item.ItemIndex >= 0) Then
                'Gajanan [12-NOV-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#244}
                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'e.Item.Cells(2).Text = CDate(e.Item.Cells(2).Text).ToString(Session("DateFormat").ToString)
                e.Item.Cells(3).Text = CDate(e.Item.Cells(3).Text).ToShortDateString
                'Pinkal (16-Apr-2016) -- End


                If e.Item.Cells(11).Text.Trim <> "&nbsp;" AndAlso CStr(e.Item.Cells(11).Text).Trim.Length > 0 Then
                    e.Item.BackColor = Drawing.Color.PowderBlue
                    e.Item.ForeColor = System.Drawing.Color.Black
                    CType(e.Item.Cells(1).FindControl("imgEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = True
                    lblPendingData.Visible = True
                    'S.SANDEEP |17-JAN-2019| -- START

                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).ToolTip = e.Item.Cells(12).Text
                    If CInt(e.Item.Cells(13).Text) = clsEmployeeMovmentApproval.enOperationType.EDITED Then
                        CType(e.Item.Cells(0).FindControl("imgDetail"), LinkButton).Visible = True
                        CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END
                Else
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = False

                    If e.Item.Cells(8).Text.Trim <> "&nbsp;" AndAlso CBool(e.Item.Cells(8).Text) = True Then
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    End If
                End If


                'If CBool(e.Item.Cells(8).Text) Then
                '    Dim lnkButton As LinkButton = CType(e.Item.Cells(8).FindControl("ImgDelete"), LinkButton)
                '    lnkButton.Visible = False
                'End If

                'Gajanan (28-May-2018) -- Start
                'Issue - Rehire Employee Data Also Edit And Delete.
                If Convert.ToInt32(e.Item.Cells(10).Text) > 0 Then
                    CType(e.Item.Cells(1).FindControl("imgEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    CType(e.Item.Cells(0).FindControl("imgView"), LinkButton).Visible = False
                    e.Item.BackColor = System.Drawing.Color.Orange
                End If
                'Gajanan (28-May-2018) -- End
                'Gajanan [12-NOV-2018] -- END

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvHistory_ItemDataBound :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvHistory_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvHistory.ItemCommand

        'Gajanan [4-Sep-2020] -- Start
        'Performance Change: Memory Leak Issue 
        Dim objCategorize As New clsemployee_categorization_Tran
        Dim objACategorization As New clsCategorization_Approval_Tran
        'Gajanan [4-Sep-2020] -- End

        Try
            lblAppointmentdate.Visible = False
            txtAppointDate.Visible = False
            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            pnlAppointmentdate.Visible = False
            'Gajanan [17-Sep-2020] -- End
            txtAppointDate.Text = ""
            Me.ViewState("Categorizationtranunkid") = 0
            Me.ViewState.Add("Categorizationtranunkid", CInt(e.Item.Cells(7).Text))
            If e.CommandName = "Edit" Then

                'S.SANDEEP |17-JAN-2019| -- START
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim Flag As Boolean = objACategorization.isExist(Nothing, objACategorization._Employeeunkid, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, CInt(ViewState("Categorizationtranunkid")))
                    If Flag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 113, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process."), Me)
                        Exit Sub
                    End If
                    'S.SANDEEP |17-JAN-2019| -- END
                End If

                Call SetEditValue()

                Me.ViewState("AppointmentDate") = Nothing
                If CBool(e.Item.Cells(8).Text) = True Then
                    Me.ViewState.Add("AppointmentDate", eZeeDate.convertDate(e.Item.Cells(9).Text))
                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    'txtAppointDate.Text = CDate(Me.ViewState("AppointmentDate")).ToString(Session("DateFormat").ToString)
                    txtAppointDate.Text = CDate(Me.ViewState("AppointmentDate")).ToShortDateString
                    'Pinkal (16-Apr-2016) -- End

                    lblAppointmentdate.Visible = True
                    txtAppointDate.Visible = True
                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    pnlAppointmentdate.Visible = True
                    'Gajanan [17-Sep-2020] -- End

                End If
                'S.SANDEEP [07-Feb-2018] -- START
                'ISSUE/ENHANCEMENT : {#0001988}
                cboEmployee.Enabled = False
                'S.SANDEEP [07-Feb-2018] -- END
            ElseIf e.CommandName = "Delete" Then
                'S.SANDEEP |17-JAN-2019| -- START
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim Flag As Boolean = objACategorization.isExist(Nothing, objACategorization._Employeeunkid, 0, 0, 0, 0, clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, CInt(ViewState("Categorizationtranunkid")))
                    If Flag Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 114, "Sorry, You can not perform delete opration, Reason:This Entry Is Already Present In Approval Process."), Me)
                        Exit Sub
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END


                popup_DeleteReason.Reason = ""
                popup_DeleteReason.Show()

                'S.SANDEEP |17-JAN-2019| -- START
            ElseIf e.CommandName.ToUpper = "VIEW" Then
                If CInt(e.Item.Cells(10).Text) > 0 Then
                    Dim dr As DataRow() = mdtRecategorizeList.Select(CType(Me.dgvHistory.Columns(10), BoundColumn).DataField & " = " & CInt(e.Item.Cells(10).Text) & " AND " & CType(Me.dgvHistory.Columns(11), BoundColumn).DataField & "= ''")
                    If dr.Length > 0 Then
                        Dim index As Integer = mdtRecategorizeList.Rows.IndexOf(dr(0))
                        dgvHistory.SelectedIndex = index
                        dgvHistory.SelectedItemStyle.BackColor = Color.LightCoral
                        dgvHistory.SelectedItemStyle.ForeColor = Color.White
                    End If
                End If
                'S.SANDEEP |17-JAN-2019| -- END



            ElseIf e.CommandName.ToUpper = "VIEWREPORT" Then
                Popup_Viewreport._UserId = CInt(Session("UserId"))
                Popup_Viewreport._Priority = 0
                'Gajanan [19-Nov-2018] -- Start
                'Popup_Viewreport._PrivilegeId = 1192
                'Popup_Viewreport._FillType = clsEmployeeMovmentApproval.enMovementType.TRANSFERS
                Popup_Viewreport._PrivilegeId = 1193
                Popup_Viewreport._FillType = clsEmployeeMovmentApproval.enMovementType.RECATEGORIZE
                'Gajanan [19-Nov-2018] -- END
                Popup_Viewreport._DtType = Nothing
                Popup_Viewreport._IsResidentPermit = False
                Popup_Viewreport._FilterString = "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue)

                Dim eOperType As clsEmployeeMovmentApproval.enOperationType
                If Me.ViewState("Categorizationtranunkid") IsNot Nothing AndAlso CInt(Me.ViewState("Categorizationtranunkid")) > 0 Then
                    eOperType = clsEmployeeMovmentApproval.enOperationType.EDITED
                Else
                    eOperType = clsEmployeeMovmentApproval.enOperationType.ADDED
                End If
                Popup_Viewreport._OprationType = eOperType
                Popup_Viewreport.Show()
                'Gajanan [12-NOV-2018] -- End
            End If


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvHistory_ItemCommand :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


#End Region

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.lblEffectiveDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEffectiveDate.ID, Me.lblEffectiveDate.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJob.ID, Me.lblJob.Text)
            Me.lblChangeReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblChangeReason.ID, Me.lblChangeReason.Text)
            Me.lblGrade.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGrade.ID, Me.lblGrade.Text)
            Me.lblAppointmentdate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAppointmentdate.ID, Me.lblAppointmentdate.Text)
            Me.lblJobGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblJobGroup.ID, Me.lblJobGroup.Text)
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")


            Me.dgvHistory.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvHistory.Columns(3).FooterText, Me.dgvHistory.Columns(3).HeaderText)
            Me.dgvHistory.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvHistory.Columns(4).FooterText, Me.dgvHistory.Columns(4).HeaderText)
            Me.dgvHistory.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvHistory.Columns(5).FooterText, Me.dgvHistory.Columns(5).HeaderText)
            Me.dgvHistory.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvHistory.Columns(6).FooterText, Me.dgvHistory.Columns(6).HeaderText)
        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetLanguage:- " & Ex.Message, Me)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Gajanan [26-Dec-2019] -- Start   
    'Enhancement:Worked On Grievance Reporting To Testing Document
    Protected Sub btnOkFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOkFilter.Click
        pnlJobFilter.CssClass = "newpopup fixPopup"
        Dim objCategorize As New clsemployee_categorization_Tran
        Dim objJob As New clsJobs
        Dim dsCombos As New DataSet
        Try
            dsCombos = objCategorize.getJobComboList("List", True, _
                                                     CInt(cbobranch.SelectedValue), _
                                                     CInt(cbodeptgroup.SelectedValue), _
                                                     CInt(cbodepartment.SelectedValue), _
                                                     CInt(cbosectiongroup.SelectedValue), _
                                                     CInt(cbosection.SelectedValue), _
                                                     CInt(cbounitgroup.SelectedValue), _
                                                     CInt(cbounit.SelectedValue), _
                                                     CInt(cboteam.SelectedValue), _
                                                     CInt(cboclassgroup.SelectedValue), _
                                                     CInt(cboclass.SelectedValue), _
                                                     CInt(cboGrade.SelectedValue), , _
                                                     CInt(cboJobGroup.SelectedValue))

            Dim intProvJobUnkId As String = cboJob.SelectedValue

            With cboJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                Try
                    .SelectedValue = intProvJobUnkId
                Catch ex As Exception
                    .SelectedValue = "0"
                End Try
                If .SelectedValue Is Nothing Then
                    .SelectedValue = "0"
                End If
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objJob = Nothing
            dsCombos = Nothing
        End Try
    End Sub
    Protected Sub btnCloseFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseFilter.Click
        Try
            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'pnlJobFilter.CssClass = "newpopup fixPopup"
            modalJobFilter.Hide()
            'Gajanan [17-Sep-2020] -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub lnkJobfilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkJobfilter.Click
        Try
            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'pnlJobFilter.CssClass = "newpopup fixPopup open"
            modalJobFilter.Show()
            'Gajanan [17-Sep-2020] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [26-Dec-2019] -- End
    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Sorry, Effective Date is mandatory information. Please set Effective Date to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Sorry, Employee is mandatory information. Please selecte Employee to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, Job is mandatory information. Please selecte atleast Job to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, Change Reason is mandatory information. Please selecte Change Reason to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Sorry, This is system generate entry. Please set effective date as employee appointment date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Sorry, you can't assign this job to multiple employee. Reason:This job is declare as key-role and it can be assign to one and only one employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Sorry, you can't assign this job to multiple employee. Reason:This job is declare as key-role and it is already in approver.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Sorry, Information is already present in approval process with selected date and allocation combination.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Sorry, Information is already present in approval process with selected date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Sorry, Information is already present in approval process with selected allocation combination.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 110, "Sorry, Information is already present in approval process with selected date and allocation combination.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 111, "Sorry, Information is already present in approval process with selected date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 112, "Sorry, Information is already present in approval process with selected allocation combination.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 113, "Sorry, You can not perform edit opration, Reason:This Entry Is Already Present In Approval Process.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 114, "Sorry, You can not perform delete opration, Reason:This Entry Is Already Present In Approval Process.")
            Language.setMessage("clsemployee_categorization_Tran", 1, "Sorry, Re-Categorize information is already present for the selected effective date.")
            Language.setMessage("clsemployee_categorization_Tran", 2, "Sorry, Re-Categorize information is already present for the selected combination for the selected effective date.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
