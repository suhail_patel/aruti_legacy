﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_EmpMovementDates.aspx.vb"
    Inherits="HR_wPg_EmpMovementDates" Title="Employee Dates" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/OperationButton.ascx" TagName="OperationButton" TagPrefix="uc5" %>
<%@ Register Src="~/Controls/ViewMovementApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--  <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>
    <%--    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.min.js"></script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <%-- <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            removeclasscss();
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
        function removeclasscss() {
            $('.click-nav > ul').toggleClass('no-js js');
            $('.click-nav .js ul').hide();
            $('.click-nav .js').click(function(e) {
                $('.click-nav .js ul').slideToggle(200);
                $('.clicker').addClass('active');
                e.stopPropagation();
            });
            if ($('.click-nav .js ul').is(':visible')) {
                alert('hii');
                $('.click-nav .js ul', this).slideUp();
                $('.clicker').removeClass('active');
            }
        }
                
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Dates"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Dates"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                                        <i class="fas fa-sliders-h"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpEffectiveDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="objlblStartFrom" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpStartDate" runat="server" AutoPostBack="true" />
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 m-t-35">
                                        <asp:CheckBox ID="chkExclude" runat="server" CssClass="filled-in" Text="Exclude From Payroll Process For EOC Date/Leaving Date">
                                        </asp:CheckBox>
                                        <asp:LinkButton ID="lnkSetSuspension" runat="server" Text="Set Suspension For Descipline"></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlEndDate" runat="server" CssClass="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="objlblEndTo" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpEndDate" runat="server" AutoPostBack="false" />
                                    </asp:Panel>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlother" runat="server" Visible="false">
                                            <asp:Label ID="lblEmplReason" runat="server" Text="EOC Reason" Visible="false" CssClass="form-label"></asp:Label>
                                            <asp:Label ID="lblPaymentDate" runat="server" Text="Payment Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboEndEmplReason" Enabled="false" runat="server"
                                                    Visible="false">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtPaymentDate" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblActualDate" runat="server" Text="Actual Date" Visible="false" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtpActualDate" runat="server" AutoPostBack="false" Visible="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="divider">
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReason" runat="server" Text="Change Reason" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboChangeReason" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Panel ID="pnlAppointmentdate" runat="server" Visible="false">
                                            <asp:Label ID="lblAppointmentdate" runat="server" Visible="False" Text="Appoint Date"
                                                CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtDate" runat="server" Visible="false" ReadOnly="true"></asp:TextBox>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save Changes" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive">
                                            <asp:DataGrid ID="dgvHistory" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" FooterText="brnView">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="imgView" runat="server" ToolTip="View" CommandName="ViewReport">
                                                                        <i class="fas fa-exclamation-circle text-info" ></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn FooterText="brnEdit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                        HeaderText="" ItemStyle-Width="30px">
                                                        <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="imgEdit" runat="server" CommandName="Edit" ToolTip="Edit">
                                                                        <i class="fas fa-pencil-alt text-primary" ></i>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="imgDetail" runat="server" ToolTip="View Detail" CommandName="View"
                                                                Visible="false"><i class="fas fa-eye text-primary"></i> 
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                        HeaderText="" ItemStyle-Width="30px">
                                                        <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" ToolTip="Delete">
                                                                    <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="EffDate" FooterText="dgcolhChangeDate" HeaderText="Effective Date"
                                                        ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="dDate1" FooterText="objdgcolhStartDate" HeaderText=""
                                                        ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="dDate2" FooterText="objdgcolhEndDate" HeaderText="" ReadOnly="true">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="CReason" FooterText="dgcolhReason" HeaderText="Reason"
                                                        ReadOnly="true"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="datestranunkid" FooterText="objdgcolhdatetranunkid" HeaderText="Reason"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isfromemployee" FooterText="objdgcolhFromEmp" HeaderText="Reason"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="actionreasonunkid" FooterText="objdgcolhActionReasonId"
                                                        HeaderText="Reason" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="isexclude_payroll" FooterText="objdgcolhExclude" HeaderText="Reason"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="adate" FooterText="objdgcolhAppointdate" HeaderText="Reason"
                                                        ReadOnly="true" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="rehiretranunkid" HeaderText="rehiretranunkid" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" Visible="false"
                                                        FooterText="objdgcolhOperationType"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="operationtypeid" HeaderText="Operationtype Id" Visible="false"
                                                        FooterText="objdgcolhOperationTypeId"></asp:BoundColumn>
                                                    <%--15--%>
                                                </Columns>
                                                <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div class="btn-group pull-left">
                                    <asp:LinkButton ID="btnOperations" runat="server" class="btn btn-primary dropdown-toggle"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Oprations<span class="caret"></span></asp:LinkButton>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <asp:LinkButton ID="mnuSalaryChange" runat="server" Text="Salary Change" Visible="false"></asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="mnuBenefits" runat="server" Text="Benefits" Visible="false"></asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="mnuTransfers" runat="server" Text="Transfers"></asp:LinkButton>
                                        </li>
                                        <li>
                                            <asp:LinkButton ID="mnuRecategorization" runat="server" Text="Re-Categorize"></asp:LinkButton>
                                        </li>
                                    </ul>
                                </div>
                                <asp:Label ID="objlblCaption" runat="server" Text="Employee Rehired" CssClass="label label-warning"></asp:Label>
                                <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" CssClass="label label-primary"></asp:Label>
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To Delete?:" />
                <uc3:Pop_report ID="Popup_report" runat="server" />
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <uc7:Confirmation ID="Confirmation" runat="server" Title="Confirmation" Message="Exclude from payroll process is not set. Are you sure you do not want to Exclude Payroll Process for this Employee?" />
                <cc1:ModalPopupExtender ID="popupViewDiscipline" BackgroundCssClass="modalBackground"
                    TargetControlID="lblPersonalInvolved" runat="server" PopupControlID="pnlGreApprover"
                    DropShadow="false" CancelControlID="btnHiddenLvCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlGreApprover" runat="server" CssClass="newpopup" Style="display: none;
                    width: 850px">
                    <div class="panel-primary" style="margin-bottom: 0px">
                        <div class="panel-heading">
                            <asp:Label ID="lblCancelText1" Text="View Discipline Charges" runat="server" />
                        </div>
                        <div class="panel-body">
                            <div id="Div20" class="panel-body-default">
                                <div class="row2">
                                    <div style="width: 48%;" class="ibwm">
                                        <div class="row2">
                                            <div style="width: 30%;" class="ib">
                                                <asp:Label ID="lblPersonalInvolved" runat="server" Text="Person Involved" Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 60%;" class="ib">
                                                <asp:DropDownList ID="cboDisciplineEmployee" runat="server" Width="220px" Enabled="false">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="row2">
                                                <div style="width: 30%;" class="ib">
                                                    <asp:Label ID="lblReferenceNo" runat="server" Text="Referance No" Width="100%"></asp:Label>
                                                </div>
                                                <div style="width: 60%;" class="ib">
                                                    <asp:DropDownList ID="cboReferenceNo" runat="server" Width="220px" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="row2">
                                                <div style="width: 30%;" class="ib">
                                                    <asp:Label ID="lblJobTitle" runat="server" Text="Job Title" Width="100%"></asp:Label>
                                                </div>
                                                <div style="width: 60%;" class="ib">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtJobTitle" runat="server" ReadOnly="true">
                                                            </asp:TextBox></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div class="row2">
                                                <div style="width: 30%;" class="ib">
                                                    <asp:Label ID="lblDepartment" runat="server" Text="Department" Width="100%"></asp:Label>
                                                </div>
                                                <div style="width: 60%;" class="ib">
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtDepartment" runat="server" ReadOnly="true">
                                                            </asp:TextBox></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="width: 48%; vertical-align: top" class="ibwm">
                                        <div class="row2">
                                            <div style="width: 30%;" class="ib">
                                                <asp:Label ID="lblDate" runat="server" Text="Charge Date" Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 60%;" class="ib">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtchargeDate" runat="server" ReadOnly="true">
                                                        </asp:TextBox></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div style="width: 30%;" class="ib">
                                                <asp:Label ID="lblInterdictionDate" runat="server" Text="Interdiction Date" Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 60%;" class="ib">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtinterdictionDate" runat="server" ReadOnly="true">
                                                        </asp:TextBox></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row2">
                                            <div style="width: 30%;" class="ib">
                                                <asp:Label ID="lblGeneralChargeDescr" runat="server" Text="General Charge Description"
                                                    Width="100%"></asp:Label>
                                            </div>
                                            <div style="width: 60%;" class="ib">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtChargeDescription" runat="server" TextMode="MultiLine" Rows="4"
                                                            ReadOnly="true"> 
                                                        </asp:TextBox></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row2">
                                    <div id="Div1" class="gridscroll" style="vertical-align: top; overflow: auto; max-height: 200px">
                                        <asp:GridView ID="dgvData" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                            AllowPaging="false" HeaderStyle-Font-Bold="false" Width="99%">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Count" FooterText="dgcolhCount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcount" runat="server" Text='<%#Container.DataItemIndex + 1%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="charge_category" FooterText="dgcolhOffCategory" HeaderText="Off. Category"
                                                    ReadOnly="true"></asp:BoundField>
                                                <asp:BoundField DataField="charge_descr" FooterText="dgcolhOffence" HeaderText="Offence"
                                                    ReadOnly="true"></asp:BoundField>
                                                <asp:BoundField DataField="incident_description" FooterText="dgcolhIncident" HeaderText="Incident"
                                                    ReadOnly="true"></asp:BoundField>
                                            </Columns>
                                            <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="btn-default">
                                    <asp:Button ID="btnOk" runat="server" CssClass="btndefault" Text="Ok" />
                                    <asp:Button ID="btnClosePopup" runat="server" CssClass="btndefault" Text="Close" />
                                    <asp:HiddenField ID="btnHiddenLvCancel" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
