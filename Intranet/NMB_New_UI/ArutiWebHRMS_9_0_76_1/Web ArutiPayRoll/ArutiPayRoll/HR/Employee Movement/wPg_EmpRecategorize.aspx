﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_EmpRecategorize.aspx.vb"
    Inherits="HR_wPg_EmpRecategorize" Title="Re-Categorize Employee" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewMovementApproval.ascx" TagName="Pop_report" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
    }
}
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Re-Categorize Information"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Re-Categorize Information"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" ToolTip="Advance Filter">
                                        <i class="fas fa-sliders-h"></i>
                                        </asp:LinkButton>
                                    </li>
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkJobfilter" runat="server" ToolTip="Job Filter">
                                        <i class="fas fa-filter"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date" CssClass="form-label"></asp:Label>
                                        <uc1:DateCtrl ID="dtEffectiveDate" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblJob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboJob" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblChangeReason" runat="server" Text="Change Reason" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboChangeReason" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnlAppointmentdate" runat="server" Visible="false">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAppointmentdate" runat="server" Visible="false" Text="Appoint Date"
                                                CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtAppointDate" runat="server" Visible="false" ReadOnly="true"></asp:TextBox></div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save Changes" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive" style="height: 250px">
                                    <asp:DataGrid ID="dgvHistory" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                        AllowPaging="false">
                                        <ItemStyle CssClass="griviewitem" />
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" FooterText="brnView">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="imgView" runat="server" ToolTip="View" CommandName="ViewReport">
                                                      <i class="fas fa-exclamation-circle text-info"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn FooterText="brnEdit" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                HeaderText="" ItemStyle-Width="30px">
                                                <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="imgEdit" runat="server" CommandName="Edit" ToolTip="Edit">
                                                        <i class="fa fa-pencil-alt text-primary"></i>
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="imgDetail" runat="server" ToolTip="View Detail" CommandName="View"
                                                            Visible="false">
                                                            <i class="fas fa-eye text-primary"></i> 
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn FooterText="btnDelete" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                HeaderText="" ItemStyle-Width="30px">
                                                <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" ToolTip="Delete">
                                                    <i class="fas fa-trash text-danger"></i>
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="effectivedate" FooterText="dgcolhChangeDate" HeaderText="Effective Date"
                                                ReadOnly="true"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="JobGroup" FooterText="dgcolhJobGroup" HeaderText="Job Group"
                                                ReadOnly="true" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Job" FooterText="dgcolhJob" HeaderText="Job" ReadOnly="true">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="Reason" FooterText="dgcolhReason" HeaderText="Reason"
                                                ReadOnly="true"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="categorizationtranunkid" FooterText="objdgcolhrecategorizeunkid"
                                                HeaderText="objdgcolhrecategorizeunkid" ReadOnly="true" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="isfromemployee" FooterText="objdgcolhFromEmp" HeaderText="objdgcolhFromEmp"
                                                ReadOnly="true" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="adate" FooterText="objdgcolhAppointdate" HeaderText="objdgcolhAppointdate"
                                                ReadOnly="true" Visible="false"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="rehiretranunkid" HeaderText="rehiretranunkid" Visible="false">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" Visible="false"
                                                FooterText="objdgcolhOperationType"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="operationtypeid" HeaderText="Operationtype Id" Visible="false"
                                                FooterText="objdgcolhOperationTypeId"></asp:BoundColumn>
                                            <%--13--%>
                                        </Columns>
                                        <HeaderStyle CssClass="griviewheader" Font-Bold="False" />
                                    </asp:DataGrid>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Label ID="objlblCaption" runat="server" Text="Employee Rehired" CssClass="label label-warning"></asp:Label>
                                <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" CssClass="label label-primary"></asp:Label>
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are You Sure You Want To Delete?:" />
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <cc1:ModalPopupExtender ID="modalJobFilter" runat="server" BackgroundCssClass="modal-backdrop"
                    PopupControlID="pnlJobFilter" TargetControlID="lblCancelText1">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlJobFilter" runat="server" CssClass="card modal-dialog">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCancelText1" Text="Job Filter" runat="server" />
                        </h2>
                    </div>
                    <div class="body" style="height: 350px">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblbranch" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbobranch" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblunitgroup" Style="margin-left: 10px" runat="server" Text="Unit Group"
                                    CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbounitgroup" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lbldeptgroup" runat="server" Text="Dept. Group" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbodeptgroup" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblunit" Style="margin-left: 10px" runat="server" Text="Units" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbounit" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lbldepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbodepartment" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblteam" Style="margin-left: 10px" runat="server" Text="Team" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cboteam" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblsectiongroup" runat="server" Text="Sec. Group" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbosectiongroup" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblclassgroup" Style="margin-left: 10px" runat="server" Text="Class Group">
                                </asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cboclassgroup" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblsection" runat="server" Text="Sections" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cbosection" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblclass" Style="margin-left: 10px" runat="server" Text="Class" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cboclass" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblJobGroup" runat="server" Text="Job Group" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cboJobGroup" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblGrade" Style="margin-left: 10px" runat="server" Text="Grade" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cboGrade" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <asp:Label ID="lblGradeLevel" Style="margin-left: 10px" runat="server" Text="Grade Level">
                                </asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList data-live-search="true" ID="cboGradeLevel" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnOkFilter" runat="server" CssClass="btn btn-primary" Text="Ok" />
                        <asp:Button ID="btnCloseFilter" runat="server" CssClass="btn btn-default" Text="Close" />
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
