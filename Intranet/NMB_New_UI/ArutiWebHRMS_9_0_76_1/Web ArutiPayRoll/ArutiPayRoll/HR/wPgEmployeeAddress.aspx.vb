﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.IO

#End Region
Partial Class wPgEmployeeAddress
    Inherits Basepage

#Region " Private Variable(s) "

    Private msg As New CommonCodes
    Private clsuser As New User
    Private objEmployee As New clsEmployee_Master
    'S.SANDEEP [ 11 MAR 2014 ] -- START
    'Dim strExpression As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
    'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
    Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
    'S.SANDEEP [ 11 MAR 2014 ] -- END

    Private ReadOnly mstrModuleName As String = "frmEmployeeMaster"
    Private ReadOnly mstrModuleName1 As String = "frmEmployeeList"



    'Gajanan [18-Mar-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Dim Arr() As String
    Private objA_Address As New clsEmployeeAddress_approval_tran
    Private AddressApprovalFlowVal As String
    'Gajanan [18-Mar-2019] -- End


    'S.SANDEEP |26-APR-2019| -- START
    Private mdtAttachement As DataTable = Nothing
    Private mdtFullAttachment As DataTable = Nothing
    Private mblnAttachmentPopup As Boolean = False
    Private objScanAttachment As New clsScan_Attach_Documents
    Private mintDeleteIndex As Integer = 0
    Private mintAddressTypeId As Integer = 0
    'S.SANDEEP |26-APR-2019| -- END


    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [17-April-2019] -- End
    'Sohail (09 Nov 2020) -- Start
    'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
    Private mstrURLReferer As String = ""
    'Sohail (09 Nov 2020) -- End
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objMaster As New clsMasterData
        Dim objCity As New clscity_master
        Dim objState As New clsstate_master
        Dim objZipCode As New clszipcode_master
        'Gajanan [18-Mar-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Dim objCommon As New clsCommon_Master
        'Gajanan [18-Mar-2019] -- End


        Try
            RemoveHandler cboPCountry.SelectedIndexChanged, AddressOf cboPCountry_SelectedIndexChanged
            dsCombos = objMaster.getCountryList("List", True)
            With cboPCountry
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboPCountry.SelectedIndexChanged, AddressOf cboPCountry_SelectedIndexChanged

            RemoveHandler cboDCountry.SelectedIndexChanged, AddressOf cboDCountry_SelectedIndexChanged
            With cboDCountry
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboDCountry.SelectedIndexChanged, AddressOf cboDCountry_SelectedIndexChanged

            RemoveHandler cboRCountry.SelectedIndexChanged, AddressOf cboRCountry_SelectedIndexChanged
            With cboRCountry
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboRCountry.SelectedIndexChanged, AddressOf cboRCountry_SelectedIndexChanged


            RemoveHandler cboPState.SelectedIndexChanged, AddressOf cboPState_SelectedIndexChanged
            dsCombos = objState.GetList("List", True, True)
            With cboPState
                .DataValueField = "stateunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboPState.SelectedIndexChanged, AddressOf cboPState_SelectedIndexChanged

            RemoveHandler cboDState.SelectedIndexChanged, AddressOf cboDState_SelectedIndexChanged
            With cboDState
                .DataValueField = "stateunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboDState.SelectedIndexChanged, AddressOf cboDState_SelectedIndexChanged

            RemoveHandler cboRState.SelectedIndexChanged, AddressOf cboRState_SelectedIndexChanged
            With cboRState
                .DataValueField = "stateunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboRState.SelectedIndexChanged, AddressOf cboRState_SelectedIndexChanged

            RemoveHandler cboPCity.SelectedIndexChanged, AddressOf cboPCity_SelectedIndexChanged
            dsCombos = objCity.GetList("List", True, True)
            With cboPCity
                .DataValueField = "cityunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboPCity.SelectedIndexChanged, AddressOf cboPCity_SelectedIndexChanged

            RemoveHandler cboDCity.SelectedIndexChanged, AddressOf cboDCity_SelectedIndexChanged
            With cboDCity
                .DataValueField = "cityunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboDCity.SelectedIndexChanged, AddressOf cboDCity_SelectedIndexChanged

            RemoveHandler cboRCity.SelectedIndexChanged, AddressOf cboRCity_SelectedIndexChanged
            With cboRCity
                .DataValueField = "cityunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboRCity.SelectedIndexChanged, AddressOf cboRCity_SelectedIndexChanged

            dsCombos = objZipCode.GetList("List", True, True)
            With cboPZipcode
                .DataValueField = "zipcodeunkid"
                .DataTextField = "zipcode_no"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With

            With cboDZipcode
                .DataValueField = "zipcodeunkid"
                .DataTextField = "zipcode_no"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With

            With cboRZipcode
                .DataValueField = "zipcodeunkid"
                .DataTextField = "zipcode_no"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With


            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.TOWN_1, True, "Town1")

            With cboPresentTown1
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Town1").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With
            With cboDomicileTown1
                .DataValueField = "masterunkid"
                .DataTextField = "name" 'Gajanan [17-April-2019]
                .DataSource = dsCombos.Tables("Town1").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With
            With cboRecruitmentTown1
                .DataValueField = "masterunkid"
                .DataTextField = "name" 'Gajanan [17-April-2019]
                .DataSource = dsCombos.Tables("Town1").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.CHIEFDOM, True, "Chiefdom")

            With cboPresentChiefdom
                .DataValueField = "masterunkid"
                .DataTextField = "name" 'Gajanan [17-April-2019]
                .DataSource = dsCombos.Tables("Chiefdom").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With
            With cboDomicileChiefdom
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Chiefdom").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With
            With cboRecruitmentChiefdom
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Chiefdom").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.VILLAGE, True, "Village")

            With cboPresentVillage
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Village").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With
            With cboDomicilevillage
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Village").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With
            With cboRecruitmentVillage
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Village").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.PROV_REGION_1, True, "Region1")
            With cboPresentProvRegion1
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Region1").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With
            With cboDomicileProvince1
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Region1").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With
            With cboRecruitmentProvince1
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Region1").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.ROAD_STREET_1, True, "Street1")
            With cboPresentRoadStreet1
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Street1").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With
            With cboDomicileRoadStreet1
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Street1").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With
            With cboRecruitmentRoadStreet1
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Street1").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With

            'Gajanan [18-Mar-2019] -- End

            'S.SANDEEP |26-APR-2019| -- START
            dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With
            'S.SANDEEP |26-APR-2019| -- END

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetInfo()
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Change in Employee Profile Pages due to wrong assignment of session("employeeunkid").
            'objEmployee._Employeeunkid = Session("Employeeunkid")

            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = IIf((Session("LoginBy") = Global.User.en_loginby.User), Session("EmpUnkid"), Session("Employeeunkid"))
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            'Pinkal (28-Dec-2015) -- End


            'Shani(24-Aug-2015) -- End



            txtEmployee.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname

            '************************* PRESENT INFO *****************************'


            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.




            'txtPAddress1.Text = objEmployee._Present_Address1
            'txtPAddress2.Text = objEmployee._Present_Address2
            'cboPCountry.SelectedValue = CStr(objEmployee._Present_Countryunkid)
            ''Shani [ 24 DEC 2014 ] -- START
            ''Implement Close Button Code on Each Page.
            ''cboPState.SelectedValue = objEmployee._Present_Stateunkid
            ''cboPCity.SelectedValue = objEmployee._Present_Post_Townunkid
            'Call cboPCountry_SelectedIndexChanged(cboPCountry, Nothing)
            'cboPState.SelectedValue = CStr(objEmployee._Present_Stateunkid)
            'Call cboPState_SelectedIndexChanged(cboPState, Nothing)
            'cboPCity.SelectedValue = CStr(objEmployee._Present_Post_Townunkid)
            'Call cboPCity_SelectedIndexChanged(cboPCity, Nothing)
            ''Shani [ 24 DEC 2014 ] -- END
            'cboPZipcode.SelectedValue = CStr(objEmployee._Present_Postcodeunkid)
            'txtPRegion.Text = objEmployee._Present_Provicnce
            'txtPStreet.Text = objEmployee._Present_Road
            'txtPEState.Text = objEmployee._Present_Estate
            'txtPPlotNo.Text = objEmployee._Present_Plotno
            'txtPMobile.Text = objEmployee._Present_Mobile
            'txtPAltNo.Text = objEmployee._Present_Alternateno
            'txtPTelNo.Text = objEmployee._Present_Tel_No
            'txtPFax.Text = objEmployee._Present_Fax
            'txtPEmail.Text = objEmployee._Present_Email


            If AddressApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                If objApprovalData.IsApproverPresent(enScreenName.frmAddressList, CStr(Session("Database_Name")), _
                                                      CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                      CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectAddress), _
                                                      CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(Session("Employeeunkid")), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    msg.DisplayMessage(objApprovalData._Message, Me)
                    Exit Sub
                End If

                'Gajanan [17-April-2019] -- End


                objA_Address._Adddresstype = CInt(clsEmployeeAddress_approval_tran.enAddressType.PRESENT)
                objA_Address._Employeeunkid = CInt(Session("Employeeunkid"))

                If objA_Address._Tranguid.Length > 0 Then
                    GbPresentAddress.Enabled = False
                    lblpresentaddressapproval.Visible = True
                Else
                    lblpresentaddressapproval.Visible = False
                End If

                If objA_Address._Tranguid.Length > 0 AndAlso objA_Address._Operationtypeid = clsEmployeeDataApproval.enOperationType.ADDED Then
                    txtPAddress1.Text = objA_Address._Address1
                    txtPAddress2.Text = objA_Address._Address2
                    cboPCountry.SelectedValue = CStr(objA_Address._Countryunkid)
                    Call cboPCountry_SelectedIndexChanged(cboPCountry, Nothing)
                    cboPState.SelectedValue = CStr(objA_Address._Stateunkid)
                    Call cboPState_SelectedIndexChanged(cboPState, Nothing)
                    cboPCity.SelectedValue = CStr(objA_Address._Post_Townunkid)
                    Call cboPCity_SelectedIndexChanged(cboPCity, Nothing)
                    cboPZipcode.SelectedValue = CStr(objA_Address._Postcodeunkid)
                    txtPRegion.Text = objA_Address._Provicnce
                    txtPStreet.Text = objA_Address._Road
                    txtPEState.Text = objA_Address._Estate
                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    cboPresentProvRegion1.SelectedValue = CStr(objA_Address._Provinceunkid)
                    cboPresentRoadStreet1.SelectedValue = CStr(objA_Address._Roadunkid)
                    cboPresentChiefdom.SelectedValue = CStr(objA_Address._Chiefdomunkid)
                    cboPresentVillage.SelectedValue = CStr(objA_Address._Villageunkid)
                    cboPresentTown1.SelectedValue = CStr(objA_Address._Town1unkid)

                    'Gajanan [17-April-2019] -- End
                    txtPPlotNo.Text = objA_Address._Plotno
                    txtPMobile.Text = objA_Address._Mobile
                    txtPAltNo.Text = objA_Address._Alternateno
                    txtPTelNo.Text = objA_Address._Tel_No
                    txtPFax.Text = objA_Address._Fax
                    txtPEmail.Text = objA_Address._Email

                Else
                    txtPAddress1.Text = objEmployee._Present_Address1
                    txtPAddress2.Text = objEmployee._Present_Address2
                    cboPCountry.SelectedValue = CStr(objEmployee._Present_Countryunkid)
                    Call cboPCountry_SelectedIndexChanged(cboPCountry, Nothing)
                    cboPState.SelectedValue = CStr(objEmployee._Present_Stateunkid)
                    Call cboPState_SelectedIndexChanged(cboPState, Nothing)
                    cboPCity.SelectedValue = CStr(objEmployee._Present_Post_Townunkid)
                    Call cboPCity_SelectedIndexChanged(cboPCity, Nothing)
                    cboPZipcode.SelectedValue = CStr(objEmployee._Present_Postcodeunkid)
                    txtPRegion.Text = objEmployee._Present_Provicnce
                    txtPStreet.Text = objEmployee._Present_Road
                    txtPEState.Text = objEmployee._Present_Estate
                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    cboPresentProvRegion1.SelectedValue = CStr(objEmployee._Present_Provinceunkid)
                    cboPresentRoadStreet1.SelectedValue = CStr(objEmployee._Present_Roadunkid)
                    cboPresentChiefdom.SelectedValue = CStr(objEmployee._Present_Chiefdomunkid)
                    cboPresentVillage.SelectedValue = CStr(objEmployee._Present_Villageunkid)
                    cboPresentTown1.SelectedValue = CStr(objEmployee._Present_Town1unkid)

                    'Gajanan [17-April-2019] -- End


                    txtPPlotNo.Text = objEmployee._Present_Plotno
                    txtPMobile.Text = objEmployee._Present_Mobile
                    txtPAltNo.Text = objEmployee._Present_Alternateno
                    txtPTelNo.Text = objEmployee._Present_Tel_No
                    txtPFax.Text = objEmployee._Present_Fax
                    txtPEmail.Text = objEmployee._Present_Email
                End If
            Else
                txtPAddress1.Text = objEmployee._Present_Address1
                txtPAddress2.Text = objEmployee._Present_Address2
                cboPCountry.SelectedValue = CStr(objEmployee._Present_Countryunkid)
                Call cboPCountry_SelectedIndexChanged(cboPCountry, Nothing)
                cboPState.SelectedValue = CStr(objEmployee._Present_Stateunkid)
                Call cboPState_SelectedIndexChanged(cboPState, Nothing)
                cboPCity.SelectedValue = CStr(objEmployee._Present_Post_Townunkid)
                Call cboPCity_SelectedIndexChanged(cboPCity, Nothing)
                cboPZipcode.SelectedValue = CStr(objEmployee._Present_Postcodeunkid)
                txtPRegion.Text = objEmployee._Present_Provicnce
                txtPStreet.Text = objEmployee._Present_Road
                txtPEState.Text = objEmployee._Present_Estate
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                cboPresentProvRegion1.SelectedValue = CStr(objEmployee._Present_Provinceunkid)
                cboPresentRoadStreet1.SelectedValue = CStr(objEmployee._Present_Roadunkid)
                cboPresentChiefdom.SelectedValue = CStr(objEmployee._Present_Chiefdomunkid)
                cboPresentVillage.SelectedValue = CStr(objEmployee._Present_Villageunkid)
                cboPresentTown1.SelectedValue = CStr(objEmployee._Present_Town1unkid)
                'Gajanan [17-April-2019] -- End
                txtPPlotNo.Text = objEmployee._Present_Plotno
                txtPMobile.Text = objEmployee._Present_Mobile
                txtPAltNo.Text = objEmployee._Present_Alternateno
                txtPTelNo.Text = objEmployee._Present_Tel_No
                txtPFax.Text = objEmployee._Present_Fax
                txtPEmail.Text = objEmployee._Present_Email
            End If

            'Gajanan [18-Mar-2019] -- End
            '************************* PRESENT INFO *****************************'

            '************************* DOMICILE INFO *****************************'


            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'txtDAddress1.Text = objEmployee._Domicile_Address1
            'txtDAddress2.Text = objEmployee._Domicile_Address2
            'cboDCountry.SelectedValue = CStr(objEmployee._Domicile_Countryunkid)
            ''Shani [ 24 DEC 2014 ] -- START
            ''Implement Close Button Code on Each Page.
            ''cboDState.SelectedValue = objEmployee._Domicile_Stateunkid
            ''cboDCity.SelectedValue = objEmployee._Domicile_Post_Townunkid
            'Call cboDCountry_SelectedIndexChanged(cboDCountry, Nothing)
            'cboDState.SelectedValue = CStr(objEmployee._Domicile_Stateunkid)
            'Call cboDState_SelectedIndexChanged(cboDState, Nothing)
            'cboDCity.SelectedValue = CStr(objEmployee._Domicile_Post_Townunkid)
            'Call cboDCity_SelectedIndexChanged(cboDCity, Nothing)
            ''Shani [ 24 DEC 2014 ] -- END
            'cboDZipcode.SelectedValue = CStr(objEmployee._Domicile_Postcodeunkid)
            'txtDRegion.Text = objEmployee._Domicile_Provicnce
            'txtDStreet.Text = objEmployee._Domicile_Road
            'txtDEState.Text = objEmployee._Domicile_Estate
            'txtDPlotNo.Text = objEmployee._Domicile_Plotno
            'txtDMobile.Text = objEmployee._Domicile_Mobile
            'txtDAltNo.Text = objEmployee._Domicile_Alternateno
            'txtDTelNo.Text = objEmployee._Domicile_Tel_No
            'txtDFax.Text = objEmployee._Domicile_Fax
            'txtDEmail.Text = objEmployee._Domicile_Email

            If AddressApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then
                objA_Address._Adddresstype = CInt(clsEmployeeAddress_approval_tran.enAddressType.DOMICILE)
                objA_Address._Employeeunkid = CInt(Session("Employeeunkid"))

                If objA_Address._Tranguid.Length > 0 Then
                    GbDomicileAddress.Enabled = False
                    lbldomicileaddressapproval.Visible = True
                Else
                    lbldomicileaddressapproval.Visible = False
                End If

                If objA_Address._Tranguid.Length > 0 AndAlso objA_Address._Operationtypeid = clsEmployeeDataApproval.enOperationType.ADDED Then

                    txtDAddress1.Text = objA_Address._Address1
                    txtDAddress2.Text = objA_Address._Address2
                    cboDCountry.SelectedValue = CStr(objA_Address._Countryunkid)
                    Call cboDCountry_SelectedIndexChanged(cboDCountry, Nothing)
                    cboDState.SelectedValue = CStr(objA_Address._Stateunkid)
                    Call cboDState_SelectedIndexChanged(cboDState, Nothing)
                    cboDCity.SelectedValue = CStr(objA_Address._Post_Townunkid)
                    Call cboDCity_SelectedIndexChanged(cboDCity, Nothing)
                    cboDZipcode.SelectedValue = CStr(objA_Address._Postcodeunkid)
                    txtDRegion.Text = objA_Address._Provicnce
                    txtDStreet.Text = objA_Address._Road
                    txtDEState.Text = objA_Address._Estate
                    cboDomicileProvince1.SelectedValue = CStr(objA_Address._Provinceunkid)
                    cboDomicileRoadStreet1.SelectedValue = CStr(objA_Address._Roadunkid)
                    cboDomicileChiefdom.SelectedValue = CStr(objA_Address._Chiefdomunkid)
                    cboDomicilevillage.SelectedValue = CStr(objA_Address._Villageunkid)
                    cboDomicileTown1.SelectedValue = CStr(objA_Address._Town1unkid)
                    txtDPlotNo.Text = objA_Address._Plotno
                    txtDMobile.Text = objA_Address._Mobile
                    txtDAltNo.Text = objA_Address._Alternateno
                    txtDTelNo.Text = objA_Address._Tel_No
                    txtDFax.Text = objA_Address._Fax
                    txtDEmail.Text = objA_Address._Email
                Else
                    txtDAddress1.Text = objEmployee._Domicile_Address1
                    txtDAddress2.Text = objEmployee._Domicile_Address2
                    cboDCountry.SelectedValue = CStr(objEmployee._Domicile_Countryunkid)
                    Call cboDCountry_SelectedIndexChanged(cboDCountry, Nothing)
                    cboDState.SelectedValue = CStr(objEmployee._Domicile_Stateunkid)
                    Call cboDState_SelectedIndexChanged(cboDState, Nothing)
                    cboDCity.SelectedValue = CStr(objEmployee._Domicile_Post_Townunkid)
                    Call cboDCity_SelectedIndexChanged(cboDCity, Nothing)
                    cboDZipcode.SelectedValue = CStr(objEmployee._Domicile_Postcodeunkid)
                    txtDRegion.Text = objEmployee._Domicile_Provicnce
                    txtDStreet.Text = objEmployee._Domicile_Road
                    txtDEState.Text = objEmployee._Domicile_Estate
                    'Gajanan [18-Mar-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.



                    'cboDomicileProvince1.SelectedValue = CStr(objEmployee._Present_Provinceunkid)
                    'cboDomicileRoadStreet1.SelectedValue = CStr(objEmployee._Present_Roadunkid)
                    'cboDomicileChiefdom.SelectedValue = CStr(objEmployee._Present_Chiefdomunkid)
                    'cboDomicilevillage.SelectedValue = CStr(objEmployee._Present_Villageunkid)
                    'cboDomicileTown1.SelectedValue = CStr(objEmployee._Present_Town1unkid)

                    cboDomicileProvince1.SelectedValue = CStr(objEmployee._domicile_provinceunkid)
                    cboDomicileRoadStreet1.SelectedValue = CStr(objEmployee._domicile_roadunkid)
                    cboDomicileChiefdom.SelectedValue = CStr(objEmployee._Domicile_Chiefdomunkid)
                    cboDomicilevillage.SelectedValue = CStr(objEmployee._Domicile_Villageunkid)
                    cboDomicileTown1.SelectedValue = CStr(objEmployee._Domicile_Town1unkid)

                    'Gajanan [17-April-2019] -- End
                    'Gajanan [18-Mar-2019] -- End
                    txtDPlotNo.Text = objEmployee._Domicile_Plotno
                    txtDMobile.Text = objEmployee._Domicile_Mobile
                    txtDAltNo.Text = objEmployee._Domicile_Alternateno
                    txtDTelNo.Text = objEmployee._Domicile_Tel_No
                    txtDFax.Text = objEmployee._Domicile_Fax
                    txtDEmail.Text = objEmployee._Domicile_Email
                End If
            Else
                txtDAddress1.Text = objEmployee._Domicile_Address1
                txtDAddress2.Text = objEmployee._Domicile_Address2
                cboDCountry.SelectedValue = CStr(objEmployee._Domicile_Countryunkid)
                Call cboDCountry_SelectedIndexChanged(cboDCountry, Nothing)
                cboDState.SelectedValue = CStr(objEmployee._Domicile_Stateunkid)
                Call cboDState_SelectedIndexChanged(cboDState, Nothing)
                cboDCity.SelectedValue = CStr(objEmployee._Domicile_Post_Townunkid)
                Call cboDCity_SelectedIndexChanged(cboDCity, Nothing)
                cboDZipcode.SelectedValue = CStr(objEmployee._Domicile_Postcodeunkid)
                txtDRegion.Text = objEmployee._Domicile_Provicnce
                txtDStreet.Text = objEmployee._Domicile_Road
                txtDEState.Text = objEmployee._Domicile_Estate
                'Gajanan [18-Mar-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                'cboDomicileProvince1.SelectedValue = CStr(objEmployee._Present_Provinceunkid)
                'cboDomicileRoadStreet1.SelectedValue = CStr(objEmployee._Present_Roadunkid)
                'cboDomicileChiefdom.SelectedValue = CStr(objEmployee._Present_Chiefdomunkid)
                'cboDomicilevillage.SelectedValue = CStr(objEmployee._Present_Villageunkid)
                'cboDomicileTown1.SelectedValue = CStr(objEmployee._Present_Town1unkid)

                cboDomicileProvince1.SelectedValue = CStr(objEmployee._domicile_provinceunkid)
                cboDomicileRoadStreet1.SelectedValue = CStr(objEmployee._domicile_roadunkid)
                cboDomicileChiefdom.SelectedValue = CStr(objEmployee._Domicile_Chiefdomunkid)
                cboDomicilevillage.SelectedValue = CStr(objEmployee._Domicile_Villageunkid)
                cboDomicileTown1.SelectedValue = CStr(objEmployee._Domicile_Town1unkid)
                'Gajanan [17-April-2019] -- End
                'Gajanan [18-Mar-2019] -- End
                txtDPlotNo.Text = objEmployee._Domicile_Plotno
                txtDMobile.Text = objEmployee._Domicile_Mobile
                txtDAltNo.Text = objEmployee._Domicile_Alternateno
                txtDTelNo.Text = objEmployee._Domicile_Tel_No
                txtDFax.Text = objEmployee._Domicile_Fax
                txtDEmail.Text = objEmployee._Domicile_Email
            End If
            'Gajanan [18-Mar-2019] -- End


            '************************* DOMICILE INFO *****************************'

            'Pinkal (25-Apr-2014) -- Start
            'Enhancement : TANAPA Changes Requested By Mr.Andrew

            '************************* RECRUITMENT INFO *****************************'


            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.


            'txtRAddress1.Text = objEmployee._Recruitment_Address1
            'txtRAddress2.Text = objEmployee._Recruitment_Address2
            'cboRCountry.SelectedValue = CStr(objEmployee._Recruitment_Countryunkid)
            ''Shani [ 24 DEC 2014 ] -- START
            ''Implement Close Button Code on Each Page.
            ''cboRState.SelectedValue = objEmployee._Recruitment_Stateunkid
            ''cboRCity.SelectedValue = objEmployee._Recruitment_Post_Townunkid
            'Call cboRCountry_SelectedIndexChanged(cboRCountry, Nothing)
            'cboRState.SelectedValue = CStr(objEmployee._Recruitment_Stateunkid)
            'Call cboRState_SelectedIndexChanged(cboRState, Nothing)
            'cboRCity.SelectedValue = CStr(objEmployee._Recruitment_Post_Townunkid)
            'Call cboRCity_SelectedIndexChanged(cboRCity, Nothing)
            ''Shani [ 24 DEC 2014 ] -- END
            'cboRZipcode.SelectedValue = CStr(objEmployee._Recruitment_Postcodeunkid)
            'txtRRegion.Text = objEmployee._Recruitment_Province
            'txtRStreet.Text = objEmployee._Recruitment_Road
            'txtREState.Text = objEmployee._Recruitment_Estate
            'txtRPlotNo.Text = objEmployee._Recruitment_Plotno
            'txtRTelNo.Text = objEmployee._Recruitment_Tel_No

            If AddressApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then
                objA_Address._Adddresstype = CInt(clsEmployeeAddress_approval_tran.enAddressType.RECRUITMENT)
                objA_Address._Employeeunkid = CInt(Session("Employeeunkid"))

                If objA_Address._Tranguid.Length > 0 Then
                    gbRecruitmentAddress.Enabled = False
                    lblrecruitmentaddressapproval.Visible = True
                Else
                    lblrecruitmentaddressapproval.Visible = False
                End If

                If objA_Address._Tranguid.Length > 0 AndAlso objA_Address._Operationtypeid = clsEmployeeDataApproval.enOperationType.ADDED Then

                    txtRAddress1.Text = objA_Address._Address1
                    txtRAddress2.Text = objA_Address._Address2
                    cboRCountry.SelectedValue = CStr(objA_Address._Countryunkid)
                    Call cboRCountry_SelectedIndexChanged(cboRCountry, Nothing)
                    cboRState.SelectedValue = CStr(objA_Address._Stateunkid)
                    Call cboRState_SelectedIndexChanged(cboRState, Nothing)
                    cboRCity.SelectedValue = CStr(objA_Address._Post_Townunkid)
                    Call cboRCity_SelectedIndexChanged(cboRCity, Nothing)
                    cboRZipcode.SelectedValue = CStr(objA_Address._Postcodeunkid)
                    txtRRegion.Text = objA_Address._Provicnce
                    txtRStreet.Text = objA_Address._Road
                    txtREState.Text = objA_Address._Estate
                    cboRecruitmentProvince1.SelectedValue = CStr(objA_Address._Provinceunkid)
                    cboRecruitmentRoadStreet1.SelectedValue = CStr(objA_Address._Roadunkid)
                    cboRecruitmentChiefdom.SelectedValue = CStr(objA_Address._Chiefdomunkid)
                    cboRecruitmentVillage.SelectedValue = CStr(objA_Address._Villageunkid)
                    cboRecruitmentTown1.SelectedValue = CStr(objA_Address._Town1unkid)
                    txtRPlotNo.Text = objEmployee._Recruitment_Plotno
                    txtRTelNo.Text = objEmployee._Recruitment_Tel_No
                Else
                    txtRAddress1.Text = objEmployee._Recruitment_Address1
                    txtRAddress2.Text = objEmployee._Recruitment_Address2
                    cboRCountry.SelectedValue = CStr(objEmployee._Recruitment_Countryunkid)
                    Call cboRCountry_SelectedIndexChanged(cboRCountry, Nothing)
                    cboRState.SelectedValue = CStr(objEmployee._Recruitment_Stateunkid)
                    Call cboRState_SelectedIndexChanged(cboRState, Nothing)
                    cboRCity.SelectedValue = CStr(objEmployee._Recruitment_Post_Townunkid)
                    Call cboRCity_SelectedIndexChanged(cboRCity, Nothing)
                    cboRZipcode.SelectedValue = CStr(objEmployee._Recruitment_Postcodeunkid)
                    txtRRegion.Text = objEmployee._Recruitment_Province
                    txtRStreet.Text = objEmployee._Recruitment_Road
                    txtREState.Text = objEmployee._Recruitment_Estate
                    'Gajanan [18-Mar-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    cboRecruitmentProvince1.SelectedValue = CStr(objEmployee._Recruitment_Provinceunkid)
                    cboRecruitmentRoadStreet1.SelectedValue = CStr(objEmployee._Recruitment_Roadunkid)
                    cboRecruitmentChiefdom.SelectedValue = CStr(objEmployee._Recruitment_Chiefdomunkid)
                    cboRecruitmentVillage.SelectedValue = CStr(objEmployee._Recruitment_Villageunkid)
                    cboRecruitmentTown1.SelectedValue = CStr(objEmployee._Recruitment_Town1unkid)
                    'Gajanan [18-Mar-2019] -- End
                    txtRPlotNo.Text = objEmployee._Recruitment_Plotno
                    txtRTelNo.Text = objEmployee._Recruitment_Tel_No
                End If
            Else
                txtRAddress1.Text = objEmployee._Recruitment_Address1
                txtRAddress2.Text = objEmployee._Recruitment_Address2
                cboRCountry.SelectedValue = CStr(objEmployee._Recruitment_Countryunkid)
                Call cboRCountry_SelectedIndexChanged(cboRCountry, Nothing)
                cboRState.SelectedValue = CStr(objEmployee._Recruitment_Stateunkid)
                Call cboRState_SelectedIndexChanged(cboRState, Nothing)
                cboRCity.SelectedValue = CStr(objEmployee._Recruitment_Post_Townunkid)
                Call cboRCity_SelectedIndexChanged(cboRCity, Nothing)
                cboRZipcode.SelectedValue = CStr(objEmployee._Recruitment_Postcodeunkid)
                txtRRegion.Text = objEmployee._Recruitment_Province
                txtRStreet.Text = objEmployee._Recruitment_Road
                txtREState.Text = objEmployee._Recruitment_Estate
                'Gajanan [18-Mar-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                cboRecruitmentProvince1.SelectedValue = CStr(objEmployee._Recruitment_Provinceunkid)
                cboRecruitmentRoadStreet1.SelectedValue = CStr(objEmployee._Recruitment_Roadunkid)
                cboRecruitmentChiefdom.SelectedValue = CStr(objEmployee._Recruitment_Chiefdomunkid)
                cboRecruitmentVillage.SelectedValue = CStr(objEmployee._Recruitment_Villageunkid)
                cboRecruitmentTown1.SelectedValue = CStr(objEmployee._Recruitment_Town1unkid)
                'Gajanan [18-Mar-2019] -- End
                txtRPlotNo.Text = objEmployee._Recruitment_Plotno
                txtRTelNo.Text = objEmployee._Recruitment_Tel_No
            End If

            'Gajanan [18-Mar-2019] -- End
            '************************* RECRUITMENT INFO *****************************'

            'Pinkal (25-Apr-2014) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Change in Employee Profile Pages due to wrong assignment of session("employeeunkid").
            'objEmployee._Employeeunkid = Session("Employeeunkid")

            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = IIf((Session("LoginBy") = Global.User.en_loginby.User), Session("EmpUnkid"), Session("Employeeunkid"))
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            'Pinkal (28-Dec-2015) -- End


            'Shani(24-Aug-2015) -- End

            '************************* PRESENT INFO *****************************'
            objEmployee._Present_Address1 = txtPAddress1.Text
            objEmployee._Present_Address2 = txtPAddress2.Text
            objEmployee._Present_Countryunkid = CInt(cboPCountry.SelectedValue)
            objEmployee._Present_Stateunkid = CInt(cboPState.SelectedValue)
            objEmployee._Present_Post_Townunkid = CInt(cboPCity.SelectedValue)
            objEmployee._Present_Postcodeunkid = CInt(cboPZipcode.SelectedValue)
            objEmployee._Present_Provicnce = txtPRegion.Text
            objEmployee._Present_Road = txtPStreet.Text
            objEmployee._Present_Estate = txtPEState.Text
            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            objEmployee._Present_Provinceunkid = CInt(cboPresentProvRegion1.SelectedValue)
            objEmployee._Present_Roadunkid = CInt(cboPresentRoadStreet1.SelectedValue)
            objEmployee._Present_Chiefdomunkid = CInt(cboPresentChiefdom.SelectedValue)
            objEmployee._Present_Villageunkid = CInt(cboPresentVillage.SelectedValue)
            objEmployee._Present_Town1unkid = CInt(cboPresentTown1.SelectedValue)
            'Gajanan [17-April-2019] -- End
            objEmployee._Present_Plotno = txtPPlotNo.Text
            objEmployee._Present_Mobile = txtPMobile.Text
            objEmployee._Present_Alternateno = txtPAltNo.Text
            objEmployee._Present_Tel_No = txtPTelNo.Text
            objEmployee._Present_Fax = txtPFax.Text
            objEmployee._Present_Email = txtPEmail.Text
            '************************* PRESENT INFO *****************************'

            '************************* DOMICILE INFO *****************************'
            objEmployee._Domicile_Address1 = txtDAddress1.Text
            objEmployee._Domicile_Address2 = txtDAddress2.Text
            objEmployee._Domicile_Countryunkid = CInt(cboDCountry.SelectedValue)
            objEmployee._Domicile_Stateunkid = CInt(cboDState.SelectedValue)
            objEmployee._Domicile_Post_Townunkid = CInt(cboDCity.SelectedValue)
            objEmployee._Domicile_Postcodeunkid = CInt(cboDZipcode.SelectedValue)
            objEmployee._Domicile_Provicnce = txtDRegion.Text
            objEmployee._Domicile_Road = txtDStreet.Text
            objEmployee._Domicile_Estate = txtDEState.Text
            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            objEmployee._domicile_provinceunkid = CInt(cboDomicileProvince1.SelectedValue)
            objEmployee._domicile_roadunkid = CInt(cboDomicileRoadStreet1.SelectedValue)
            objEmployee._Domicile_Chiefdomunkid = CInt(cboDomicileChiefdom.SelectedValue)
            objEmployee._Domicile_Villageunkid = CInt(cboDomicilevillage.SelectedValue)
            objEmployee._Domicile_Town1unkid = CInt(cboDomicileTown1.SelectedValue)
            'Gajanan [17-April-2019] -- End
            objEmployee._Domicile_Plotno = txtDPlotNo.Text
            objEmployee._Domicile_Mobile = txtDMobile.Text
            objEmployee._Domicile_Alternateno = txtDAltNo.Text
            objEmployee._Domicile_Tel_No = txtDTelNo.Text
            objEmployee._Domicile_Fax = txtDFax.Text
            objEmployee._Domicile_Email = txtDEmail.Text

            '************************* DOMICILE INFO *****************************'

            'Pinkal (25-Apr-2014) -- Start
            'Enhancement : TANAPA Changes Requested By Mr.Andrew

            '************************* RECRUITMENT INFO *****************************'
            objEmployee._Recruitment_Address1 = txtRAddress1.Text.Trim
            objEmployee._Recruitment_Address2 = txtRAddress2.Text.Trim
            objEmployee._Recruitment_Countryunkid = CInt(cboRCountry.SelectedValue)
            objEmployee._Recruitment_Stateunkid = CInt(cboRState.SelectedValue)
            objEmployee._Recruitment_Post_Townunkid = CInt(cboRCity.SelectedValue)
            objEmployee._Recruitment_Postcodeunkid = CInt(cboRZipcode.SelectedValue)
            objEmployee._Recruitment_Province = txtRRegion.Text.Trim
            objEmployee._Recruitment_Road = txtRStreet.Text.Trim
            objEmployee._Recruitment_Estate = txtREState.Text.Trim

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            objEmployee._Recruitment_Provinceunkid = CInt(cboRecruitmentProvince1.SelectedValue)
            objEmployee._Recruitment_Roadunkid = CInt(cboRecruitmentRoadStreet1.SelectedValue)
            objEmployee._Recruitment_Chiefdomunkid = CInt(cboRecruitmentChiefdom.SelectedValue)
            objEmployee._Recruitment_Villageunkid = CInt(cboRecruitmentVillage.SelectedValue)
            objEmployee._Recruitment_Town1unkid = CInt(cboRecruitmentTown1.SelectedValue)
            'Gajanan [17-April-2019] -- End

            objEmployee._Recruitment_Plotno = txtRPlotNo.Text.Trim
            objEmployee._Recruitment_Tel_No = txtRTelNo.Text.Trim
            '************************* RECRUITMENT INFO *****************************'

            'Pinkal (25-Apr-2014) -- End


            'S.SANDEEP [ 19 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
            'S.SANDEEP [ 19 MAR 2013 ] -- END


            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            Blank_ModuleName()
            objEmployee._WebFormName = "frmEmployeeMaster"
            StrModuleName2 = "mnuCoreSetups"
            objEmployee._WebClientIP = CStr(Session("IP_ADD"))
            objEmployee._WebHostName = CStr(Session("HOST_NAME"))
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                objEmployee._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If


            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim eLMode As Integer
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                eLMode = enLogin_Mode.MGR_SELF_SERVICE
            Else
                eLMode = enLogin_Mode.EMP_SELF_SERVICE
            End If
            'Gajanan [18-Mar-2019] -- End


            'S.SANDEEP [ 04 JULY 2012 ] -- END

            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objEmployee.Update(, , , Session("UserId"), Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), Session("Fin_year"), Session("AllowToApproveEarningDeduction")) = False Then
            '    'If objEmployee.Update = False Then
            '    'Sohail (23 Apr 2012) -- End
            '    msg.DisplayMessage(objEmployee._Message, Me)
            'End If

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

            'Dim blnFlag As Boolean = objEmployee.Update(CStr(Session("Database_Name")), _
            '                                            CInt(Session("Fin_year")), _
            '                                            CInt(Session("CompanyUnkId")), _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                            CStr(Session("IsArutiDemo")), _
            '                                            CInt(Session("Total_Active_Employee_ForAllCompany")), _
            '                                            CInt(Session("NoOfEmployees")), _
            '                                            CInt(Session("UserId")), False, _
            '                                            CStr(Session("UserAccessModeSetting")), _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
            '                                            CBool(Session("IsIncludeInactiveEmp")), _
            '                                            CBool(Session("AllowToApproveEarningDeduction")), _
            '                             ConfigParameter._Object._CurrentDateAndTime.Date, , , , , _
            '                                            CStr(Session("EmployeeAsOnDate")), , _
            '                                            CBool(Session("IsImgInDataBase")))




            'S.SANDEEP |26-APR-2019| -- START
            Call SaveDocumentIIS()
            'S.SANDEEP |26-APR-2019| -- END


            Dim blnFlag As Boolean = objEmployee.Update(CStr(Session("Database_Name")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(Session("IsArutiDemo")), _
                                                        CInt(Session("Total_Active_Employee_ForAllCompany")), _
                                                        CInt(Session("NoOfEmployees")), _
                                                        CInt(Session("UserId")), False, _
                                                        CStr(Session("UserAccessModeSetting")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), _
                                                        CBool(Session("AllowToApproveEarningDeduction")), _
                                                    ConfigParameter._Object._CurrentDateAndTime.Date, CBool(Session("CreateADUserFromEmpMst")), _
                                                    CBool(Session("UserMustchangePwdOnNextLogon")), Nothing, Nothing, False, "", _
                                                        CStr(Session("EmployeeAsOnDate")), , _
                                                        CBool(Session("IsImgInDataBase")), , Nothing, , , mdtFullAttachment, Session("HOST_NAME").ToString(), Session("IP_ADD").ToString(), Session("UserName").ToString(), CType(eLMode, enLogin_Mode), False, False, mstrModuleName) 'S.SANDEEP |26-APR-2019| -- START {Nothing  -> mdtFullAttachment} -- END

            'Pinkal (18-Aug-2018) -- End

            If blnFlag = False Then
                msg.DisplayMessage(objEmployee._Message, Me)
            End If
            'Shani(24-Aug-2015) -- End



        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'S.SANDEEP |26-APR-2019| -- START
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Dim dtRow() As DataRow = mdtFullAttachment.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            Dim dtRow() As DataRow = mdtFullAttachment.Select("filename = '" & f.Name & "' AND AUD <> 'D' AND scanattachrefid = " & mintAddressTypeId & " ")
            'Gajanan [17-April-2019] -- End
            If dtRow.Length <= 0 Then
                dRow = mdtFullAttachment.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
                dRow("employeeunkid") = CInt(CInt(Session("Employeeunkid")))
                dRow("filename") = f.Name
                dRow("scanattachrefid") = mintAddressTypeId
                dRow("modulerefid") = enImg_Email_RefId.Employee_Module
                dRow("userunkid") = Session("UserId")
                dRow("AUD") = "A"
                dRow("GUID") = Guid.NewGuid.ToString
                dRow("orgfilepath") = strfullpath
                dRow("valuename") = ""
                dRow("transactionunkid") = -1
                dRow("attached_date") = Today.Date
                dRow("filepath") = strfullpath
                dRow("filename") = f.Name
                dRow("filesize") = f.Length / 1024
                dRow("attached_date") = Today.Date
                dRow("orgfilepath") = strfullpath
                dRow("form_name") = mstrModuleName
                dRow("userunkid") = CInt(Session("userid"))
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
                Dim blnIsInApproval As Boolean = False : objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
                If AddressApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then
                    blnIsInApproval = True
                End If
                dRow("isinapproval") = blnIsInApproval
            Else
                msg.DisplayMessage("Selected information is already present for particular employee.", Me)
                Exit Sub
            End If
            mdtFullAttachment.Rows.Add(dRow)
            mdtAttachement = New DataView(mdtFullAttachment, "scanattachrefid = '" & mintAddressTypeId & "'", "", DataViewRowState.CurrentRows).ToTable()
            Call FillAttachment()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            If mdtAttachement Is Nothing Then Exit Sub
            dtView = New DataView(mdtAttachement, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgv_Attchment.AutoGenerateColumns = False
            dgv_Attchment.DataSource = dtView
            dgv_Attchment.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SaveDocumentIIS()
        Try
            If mdtFullAttachment IsNot Nothing Then
                Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                For Each dRow As DataRow In mdtFullAttachment.Rows
                    Dim iScanRefId As Integer = CInt(dRow.Item("scanattachrefid"))
                    Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = iScanRefId) Select (p.Item("Name").ToString)).FirstOrDefault

                    If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                        Dim strFileName As String = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                        If File.Exists(CStr(dRow("orgfilepath"))) Then
                            Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                            If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                            End If
                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            'File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                            File.Copy(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                            'Gajanan [17-April-2019] -- End
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strPath

                            dRow.AcceptChanges()
                        Else
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 999, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFilepath As String = dRow("filepath").ToString
                        Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                        If strFilepath.Contains(strArutiSelfService) Then
                            strFilepath = strFilepath.Replace(strArutiSelfService, "")
                            If Strings.Left(strFilepath, 1) <> "/" Then
                                strFilepath = "~/" & strFilepath
                            Else
                                strFilepath = "~" & strFilepath
                            End If

                            If File.Exists(Server.MapPath(strFilepath)) Then
                                File.Delete(Server.MapPath(strFilepath))
                            Else
                                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 999, "Configuration Path does not Exist."), Me)
                                Exit Sub
                            End If
                        Else
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 999, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |26-APR-2019| -- END

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If




            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            AddressApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmAddressList)))
            'Gajanan [18-Mar-2019] -- End

            If (Page.IsPostBack = False) Then
                SetLanguage()

                clsuser = CType(Session("clsuser"), User)
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    btnsave.Visible = CBool(Session("EditEmployee"))
                    If Session("ShowPending") IsNot Nothing Then
                        btnsave.Visible = Not CBool(Session("ShowPending"))
                    End If
                Else
                    lblEmployee.Visible = False : txtEmployee.Visible = False
                    'Hemant (01 Jan 2021) -- Start
                    'Enhancement #OLD-238 - Improved Employee Profile page
                    pnl_Employee.Visible = False
                    'Hemant (01 Jan 2021) -- End
                    btnsave.Visible = CBool(Session("AllowEditAddress"))
                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.



                    'GbPresentAddress.Enabled = CBool(Session("AllowToAddEditEmployeePresentAddress"))
                    'GbDomicileAddress.Enabled = CBool(Session("AllowToAddEditEmployeeDomicileAddress"))
                    'gbRecruitmentAddress.Enabled = CBool(Session("AllowToAddEditEmployeeRecruitmentAddress"))

                End If
                GbPresentAddress.Enabled = CBool(Session("AllowToAddEditEmployeePresentAddress"))
                GbDomicileAddress.Enabled = CBool(Session("AllowToAddEditEmployeeDomicileAddress"))
                gbRecruitmentAddress.Enabled = CBool(Session("AllowToAddEditEmployeeRecruitmentAddress"))



                'Gajanan [17-April-2019] -- End
                Call FillCombo()
                Call SetInfo()
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                If Request.UrlReferrer IsNot Nothing Then
                    mstrURLReferer = Request.UrlReferrer.AbsoluteUri
                Else
                    mstrURLReferer = Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx"
                End If
                'Sohail (09 Nov 2020) -- End
                'S.SANDEEP |26-APR-2019| -- START
            Else
                mdtAttachement = CType(Me.ViewState("FieldAttachement"), DataTable)
                mblnAttachmentPopup = CBool(Me.ViewState("mblnAttachmentPopup"))
                mdtFullAttachment = CType(Me.ViewState("mdtFullAttachment"), DataTable)
                mintAddressTypeId = CInt(Me.ViewState("mintAddressTypeId"))
                mintDeleteIndex = CInt(Me.ViewState("mintDeleteIndex"))
                'S.SANDEEP |26-APR-2019| -- END
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                mstrURLReferer = ViewState("mstrURLReferer").ToString
                'Sohail (09 Nov 2020) -- End
            End If




            'S.SANDEEP |26-APR-2019| -- START
            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If
            'S.SANDEEP |26-APR-2019| -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'S.SANDEEP |26-APR-2019| -- START
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("FieldAttachement") = mdtAttachement
            Me.ViewState("mblnAttachmentPopup") = mblnAttachmentPopup
            Me.ViewState("mdtFullAttachment") = mdtFullAttachment
            Me.ViewState("mintAddressTypeId") = mintAddressTypeId
            Me.ViewState("mintDeleteIndex") = mintDeleteIndex
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            ViewState("mstrURLReferer") = mstrURLReferer
            'Sohail (09 Nov 2020) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |26-APR-2019| -- END


#End Region

#Region " Controls Events "

    Protected Sub cboPCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPCountry.SelectedIndexChanged
        Try
            If CInt(cboPCountry.SelectedValue) >= 0 Then
                Dim objState As New clsstate_master
                Dim dsList As New DataSet
                dsList = objState.GetList("List", True, True, CInt(cboPCountry.SelectedValue))
                With cboPState
                    .DataValueField = "stateunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboPState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPState.SelectedIndexChanged
        Try
            If CInt(cboPState.SelectedValue) >= 0 Then
                Dim objCity As New clscity_master
                Dim dsList As DataSet = objCity.GetList("List", True, True, CInt(cboPState.SelectedValue))
                With cboPCity
                    .DataValueField = "cityunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List").Copy
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboPCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPCity.SelectedIndexChanged
        Try
            If CInt(cboPCity.SelectedValue) >= 0 Then
                Dim objZip As New clszipcode_master
                Dim dsList As DataSet = objZip.GetList("List", True, True, CInt(cboPCity.SelectedValue))
                With cboPZipcode
                    .DataValueField = "zipcodeunkid"
                    .DataTextField = "zipcode_no"
                    .DataSource = dsList.Tables("List").Copy
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboDCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDCountry.SelectedIndexChanged
        Try
            If CInt(cboDCountry.SelectedValue) >= 0 Then
                Dim objState As New clsstate_master
                Dim dsList As New DataSet
                dsList = objState.GetList("List", True, True, CInt(cboDCountry.SelectedValue))
                With cboDState
                    .DataValueField = "stateunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboDState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDState.SelectedIndexChanged
        Try
            If CInt(cboDState.SelectedValue) >= 0 Then
                Dim objCity As New clscity_master
                Dim dsList As DataSet = objCity.GetList("List", True, True, CInt(cboDState.SelectedValue))
                With cboDCity
                    .DataValueField = "cityunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List").Copy
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboDCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDCity.SelectedIndexChanged
        Try
            If CInt(cboDCity.SelectedValue) >= 0 Then
                Dim objZip As New clszipcode_master
                Dim dsList As DataSet = objZip.GetList("List", True, True, CInt(cboDCity.SelectedValue))
                With cboDZipcode
                    .DataValueField = "zipcodeunkid"
                    .DataTextField = "zipcode_no"
                    .DataSource = dsList.Tables("List").Copy
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboRCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRCountry.SelectedIndexChanged
        Try
            If CInt(cboRCountry.SelectedValue) >= 0 Then
                Dim objState As New clsstate_master
                Dim dsList As New DataSet
                dsList = objState.GetList("List", True, True, CInt(cboRCountry.SelectedValue))
                With cboRState
                    .DataValueField = "stateunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboRState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRState.SelectedIndexChanged
        Try
            If CInt(cboRState.SelectedValue) >= 0 Then
                Dim objCity As New clscity_master
                Dim dsList As DataSet = objCity.GetList("List", True, True, CInt(cboRState.SelectedValue))
                With cboRCity
                    .DataValueField = "cityunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List").Copy
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboRCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRCity.SelectedIndexChanged
        Try
            If CInt(cboRCity.SelectedValue) >= 0 Then
                Dim objZip As New clszipcode_master
                Dim dsList As DataSet = objZip.GetList("List", True, True, CInt(cboRCity.SelectedValue))
                With cboRZipcode
                    .DataValueField = "zipcodeunkid"
                    .DataTextField = "zipcode_no"
                    .DataSource = dsList.Tables("List").Copy
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

    'S.SANDEEP |26-APR-2019| -- START
#Region " Link Event(s) "

    Protected Sub lnkScanAttachPersonal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkScanAttachPersonal.Click, lnkAttachDomicile.Click, lnkAttachRecruitment.Click
        Try
            mintAddressTypeId = 0
            Select Case CType(sender, LinkButton).ID
                Case lnkScanAttachPersonal.ID
                    If txtPAddress1.Text.Trim.Length <= 0 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 156, "Sorry, In order to proceed with document attachment, Please provide present address1 to continue."), Me)
                        Exit Sub
                    End If
                Case lnkAttachDomicile.ID
                    If txtDAddress1.Text.Trim.Length <= 0 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 157, "Sorry, In order to proceed with document attachment, Please provide domicile address1 to continue."), Me)
                        Exit Sub
                    End If
                Case lnkAttachRecruitment.ID
                    If txtRAddress1.Text.Length <= 0 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 158, "Sorry, In order to proceed with document attachment, Please provide recruitment address1 to continue."), Me)
                        Exit Sub
                    End If
            End Select

            objScanAttachment.GetList(Session("Document_Path").ToString(), "List", "", CInt(Session("Employeeunkid")))
            If mdtFullAttachment Is Nothing Then
                mdtFullAttachment = objScanAttachment._Datatable
            End If

            If mdtFullAttachment IsNot Nothing Then
                Select Case CType(sender, LinkButton).ID
                    Case lnkScanAttachPersonal.ID
                        lblScanHeader.Text = objlblCaption.Text & " (" & lnPresentAddress.Text & ")"
                        mintAddressTypeId = CInt(enScanAttactRefId.PERSONAL_ADDRESS_TYPE)
                    Case lnkAttachDomicile.ID
                        lblScanHeader.Text = objlblCaption.Text & " (" & lnDomicileAddress.Text & ")"
                        mintAddressTypeId = CInt(enScanAttactRefId.DOMICILE_ADDRESS_TYPE)
                    Case lnkAttachRecruitment.ID
                        lblScanHeader.Text = objlblCaption.Text & " (" & lnRecruitmentAddress.Text & ")"
                        mintAddressTypeId = CInt(enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE)
                End Select
                mdtAttachement = New DataView(mdtFullAttachment, "scanattachrefid = '" & mintAddressTypeId & "'", "", DataViewRowState.CurrentRows).ToTable()
            End If
            mblnAttachmentPopup = True
            Call FillAttachment()
            popup_ScanAttchment.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " GridViewEvent "

    Protected Sub dgv_Attchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgv_Attchment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow = Nothing
                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                'If CInt(e.Item.Cells(3).Text) > 0 Then
                '    xrow = mdtFullAttachment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(3).Text) & "")
                'Else
                '    xrow = mdtFullAttachment.Select("GUID = '" & e.Item.Cells(2).Text & "'")
                'End If
                'If e.CommandName = "Delete" Then
                '    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                '        popup_AttachementYesNo.Message = "Are you sure you want to delete this attachment?"
                '        mintDeleteIndex = mdtFullAttachment.Rows.IndexOf(xrow(0))
                '        popup_AttachementYesNo.Show()
                '    End If
                'End If

                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtFullAttachment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtFullAttachment.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhGUID", False, True)).Text & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        popup_AttachementYesNo.Message = "Are you sure you want to delete this attachment?"
                        mintDeleteIndex = mdtFullAttachment.Rows.IndexOf(xrow(0))
                        popup_AttachementYesNo.Show()
                    End If
                ElseIf e.CommandName = "Download" Then
                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        Dim xPath As String = ""

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("orgfilepath").ToString
                        End If

                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                msg.DisplayMessage("File does not Exist...", Me)
                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & xrow(0)("filename").ToString() & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
            End If
            'S.SANDEEP |16-MAY-2019| -- END
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'S.SANDEEP |26-APR-2019| -- END

#Region " Button's Events "

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            Dim objMaster As New clsMasterData
            If txtPEmail.Text.Trim.Length > 0 Then
                If Expression.IsMatch(txtPEmail.Text.Trim) = False Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "Invalid Present Email.Please Enter Valid Email"), Me)
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                End If

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Change in Employee Profile Pages due to wrong assignment of session("employeeunkid").
                If objMaster.IsEmailPresent("hremployee_master", txtPEmail.Text.Trim, CInt(Session("Employeeunkid")), "employeeunkid") = True Then
                    'If objMaster.IsEmailPresent("hremployee_master", txtPEmail.Text.Trim, CInt(IIf((CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User), Session("EmpUnkid"), Session("Employeeunkid"))), "employeeunkid") = True Then
                    'Shani(24-Aug-2015) -- End

                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 59, "Sorry, this present email address is already used by some employee. please provide another present email address."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                End If
            End If
            If txtDEmail.Text.Trim.Length > 0 Then
                If Expression.IsMatch(txtDEmail.Text.Trim) = False Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 29, "Invalid Domicile Email.Please Enter Valid Email"), Me)
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                End If

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Change in Employee Profile Pages due to wrong assignment of session("employeeunkid").
                If objMaster.IsEmailPresent("hremployee_master", txtDEmail.Text.Trim, CInt(Session("Employeeunkid")), "employeeunkid") = True Then
                    'If objMaster.IsEmailPresent("hremployee_master", txtDEmail.Text.Trim, CInt(IIf((CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User), Session("EmpUnkid"), Session("Employeeunkid"))), "employeeunkid") = True Then
                    'Shani(24-Aug-2015) -- End
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 61, "Sorry, this domicile email address is already used by some employee. please provide another domicile email address."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                End If
            End If

            'Gajanan [27-May-2019] -- Start              


            'Gajanan [1-NOV-2019] -- Start    
            'Enhancement:Worked On NMB ESS Comment  


            'If CBool(Session("DomicileDocsAttachmentMandatory")) Then
            '    If mdtFullAttachment Is Nothing OrElse mdtFullAttachment.Select("AUD <> 'D' and scanattachrefid = " & enScanAttactRefId.DOMICILE_ADDRESS_TYPE & " ").Length <= 0 Then
            '        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 157, "Scan/Document(s) is mandatory information for Domicile Address.Please attach Scan/Document(s) in order to perform operation. "), Me)
            '        Exit Sub
            '    End If
            'End If

            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            Dim isAddressChange = False

            If objEmployee._Present_Address1 <> txtPAddress1.Text Then
                isAddressChange = True
            ElseIf objEmployee._Present_Address2 <> txtPAddress2.Text Then
                isAddressChange = True
            ElseIf objEmployee._Present_Countryunkid <> CInt(cboPCountry.SelectedValue) Then
                isAddressChange = True
            ElseIf objEmployee._Present_Stateunkid <> CInt(cboPState.SelectedValue) Then
                isAddressChange = True
            ElseIf objEmployee._Present_Post_Townunkid <> CInt(cboPCity.SelectedValue) Then
                isAddressChange = True
            ElseIf objEmployee._Present_Postcodeunkid <> CInt(cboPZipcode.SelectedValue) Then
                isAddressChange = True
            ElseIf objEmployee._Present_Provicnce <> txtPRegion.Text Then
                isAddressChange = True
            ElseIf objEmployee._Present_Road <> txtPStreet.Text Then
                isAddressChange = True
            ElseIf objEmployee._Present_Estate <> txtPEState.Text Then
                isAddressChange = True
            ElseIf objEmployee._Present_Provinceunkid <> CInt(cboPresentProvRegion1.SelectedValue) Then
                isAddressChange = True
            ElseIf objEmployee._Present_Roadunkid <> CInt(cboPresentRoadStreet1.SelectedValue) Then
                isAddressChange = True
            ElseIf objEmployee._Present_Chiefdomunkid <> CInt(cboPresentChiefdom.SelectedValue) Then
                isAddressChange = True
            ElseIf objEmployee._Present_Villageunkid <> CInt(cboPresentVillage.SelectedValue) Then
                isAddressChange = True
            ElseIf objEmployee._Present_Town1unkid <> CInt(cboPresentTown1.SelectedValue) Then
                isAddressChange = True
            ElseIf objEmployee._Present_Plotno <> txtPPlotNo.Text Then
                isAddressChange = True
            ElseIf objEmployee._Present_Mobile <> txtPMobile.Text Then
                isAddressChange = True
            ElseIf objEmployee._Present_Alternateno <> txtPAltNo.Text Then
                isAddressChange = True
            ElseIf objEmployee._Present_Tel_No <> txtPTelNo.Text Then
                isAddressChange = True
            ElseIf objEmployee._Present_Fax <> txtPFax.Text Then
                isAddressChange = True
            ElseIf objEmployee._Present_Email <> txtPEmail.Text Then
                isAddressChange = True
            End If


            If isAddressChange Then
                If CBool(Session("PresentAddressDocsAttachmentMandatory")) Then
                    If mdtFullAttachment Is Nothing OrElse mdtFullAttachment.Select("AUD <> 'D' and scanattachrefid = " & enScanAttactRefId.PERSONAL_ADDRESS_TYPE & " ").Length <= 0 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 300, "Scan/Document(s) is mandatory information for Present Address.Please attach Scan/Document(s) in order to perform operation."), Me)
                        Exit Sub
                    End If
                End If
            End If
            isAddressChange = False


            If txtDAddress1.Text <> objEmployee._Domicile_Address1 Then
                isAddressChange = True
            ElseIf txtDAddress2.Text <> objEmployee._Domicile_Address2 Then
                isAddressChange = True
            ElseIf CInt(cboDCountry.SelectedValue) <> objEmployee._Domicile_Countryunkid Then
                isAddressChange = True
            ElseIf CInt(cboDState.SelectedValue) <> objEmployee._Domicile_Stateunkid Then
                isAddressChange = True
            ElseIf CInt(cboDCity.SelectedValue) <> objEmployee._Domicile_Post_Townunkid Then
                isAddressChange = True
            ElseIf CInt(cboDZipcode.SelectedValue) <> objEmployee._Domicile_Postcodeunkid Then
                isAddressChange = True
            ElseIf txtDRegion.Text <> objEmployee._Domicile_Provicnce Then
                isAddressChange = True
            ElseIf txtDStreet.Text <> objEmployee._Domicile_Road Then
                isAddressChange = True
            ElseIf txtDEState.Text <> objEmployee._Domicile_Estate Then
                isAddressChange = True
            ElseIf CInt(cboDomicileProvince1.SelectedValue) <> objEmployee._domicile_provinceunkid Then
                isAddressChange = True
            ElseIf CInt(cboDomicileRoadStreet1.SelectedValue) <> objEmployee._domicile_roadunkid Then
                isAddressChange = True
            ElseIf CInt(cboDomicileChiefdom.SelectedValue) <> objEmployee._Domicile_Chiefdomunkid Then
                isAddressChange = True
            ElseIf CInt(cboDomicilevillage.SelectedValue) <> objEmployee._Domicile_Villageunkid Then
                isAddressChange = True
            ElseIf CInt(cboDomicileTown1.SelectedValue) <> objEmployee._Domicile_Town1unkid Then
                isAddressChange = True
            ElseIf txtDPlotNo.Text <> objEmployee._Domicile_Plotno Then
                isAddressChange = True
            ElseIf txtDMobile.Text <> objEmployee._Domicile_Mobile Then
                isAddressChange = True
            ElseIf txtDAltNo.Text <> objEmployee._Domicile_Alternateno Then
                isAddressChange = True
            ElseIf txtDTelNo.Text <> objEmployee._Domicile_Tel_No Then
                isAddressChange = True
            ElseIf txtDFax.Text <> objEmployee._Domicile_Fax Then
                isAddressChange = True
            ElseIf txtDEmail.Text <> objEmployee._Domicile_Email Then
                isAddressChange = True
            End If

            If isAddressChange Then
                If CBool(Session("DomicileDocsAttachmentMandatory")) Then
                    If mdtFullAttachment Is Nothing OrElse mdtFullAttachment.Select("AUD <> 'D' and scanattachrefid = " & enScanAttactRefId.DOMICILE_ADDRESS_TYPE & " ").Length <= 0 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 301, "Scan/Document(s) is mandatory information for Domicile Address.Please attach Scan/Document(s) in order to perform operation."), Me)
                        Exit Sub
                    End If
                End If
            End If
            isAddressChange = False




            If txtRAddress1.Text <> objEmployee._Recruitment_Address1 Then
                isAddressChange = True
            ElseIf txtRAddress2.Text <> objEmployee._Recruitment_Address2 Then
                isAddressChange = True
            ElseIf CInt(cboRCountry.SelectedValue) <> objEmployee._Recruitment_Countryunkid Then
                isAddressChange = True
            ElseIf CInt(cboRState.SelectedValue) <> objEmployee._Recruitment_Stateunkid Then
                isAddressChange = True
            ElseIf CInt(cboRCity.SelectedValue) <> objEmployee._Recruitment_Post_Townunkid Then
                isAddressChange = True
            ElseIf CInt(cboRZipcode.SelectedValue) <> objEmployee._Recruitment_Postcodeunkid Then
                isAddressChange = True
            ElseIf txtRRegion.Text <> objEmployee._Recruitment_Province Then
                isAddressChange = True
            ElseIf txtRStreet.Text <> objEmployee._Recruitment_Road Then
                isAddressChange = True
            ElseIf txtREState.Text <> objEmployee._Recruitment_Estate Then
                isAddressChange = True
            ElseIf CInt(cboRecruitmentProvince1.SelectedValue) <> objEmployee._Recruitment_Provinceunkid Then
                isAddressChange = True
            ElseIf CInt(cboRecruitmentRoadStreet1.SelectedValue) <> objEmployee._Recruitment_Roadunkid Then
                isAddressChange = True
            ElseIf CInt(cboRecruitmentChiefdom.SelectedValue) <> objEmployee._Recruitment_Chiefdomunkid Then
                isAddressChange = True
            ElseIf CInt(cboRecruitmentVillage.SelectedValue) <> objEmployee._Recruitment_Villageunkid Then
                isAddressChange = True
            ElseIf CInt(cboRecruitmentTown1.SelectedValue) <> objEmployee._Recruitment_Town1unkid Then
                isAddressChange = True
            ElseIf txtRPlotNo.Text <> objEmployee._Recruitment_Plotno Then
                isAddressChange = True
            ElseIf txtRTelNo.Text <> objEmployee._Recruitment_Tel_No Then
                isAddressChange = True
            End If

            If isAddressChange Then
                If CBool(Session("RecruitmentAddressDocsAttachmentMandatory")) Then
                    If mdtFullAttachment Is Nothing OrElse mdtFullAttachment.Select("AUD <> 'D' and scanattachrefid = " & enScanAttactRefId.RECRUITMENT_ADDRESS_TYPE & " ").Length <= 0 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 302, "Scan/Document(s) is mandatory information for Recruitment Address.Please attach Scan/Document(s) in order to perform operation."), Me)
                        Exit Sub
                    End If
                End If
            End If
            'Gajanan [1-NOV-2019] -- End

            'Gajanan [27-May-2019] -- End

            Call SetValue()

            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\HR\wPgEmployeeProfile.aspx")

            'Gajanan [27-May-2019] -- Start              
            'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx")

            'S.SANDEEP |31-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Employee Bio Data UAT]
            'msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 156, "Save Successfully"), Me, Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx")
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            'msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 502, "Saved Successfully"), Me, Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx")
            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 502, "Saved Successfully"), Me, mstrURLReferer)
            'Sohail (09 Nov 2020) -- End
            'S.SANDEEP |31-MAY-2019| -- END

            'Gajanan [27-May-2019] -- End


            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'S.SANDEEP |26-APR-2019| -- START
    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                'mdtAttachement = CType(Me.ViewState("FieldAttachement"), DataTable)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    popup_ScanAttchment.Show()
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                AddDocumentAttachment(f, f.FullName)
                popup_ScanAttchment.Show()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_AttachementYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonNo_Click
        Try
            If mintDeleteIndex > 0 Then mintDeleteIndex = 0
            popup_ScanAttchment.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_AttachementYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonYes_Click
        Try
            If mintDeleteIndex >= 0 Then
                mdtFullAttachment.Rows(mintDeleteIndex)("AUD") = "D"
                mdtFullAttachment.AcceptChanges()
                mintDeleteIndex = 0
                mdtAttachement = New DataView(mdtFullAttachment, "scanattachrefid = '" & mintAddressTypeId & "'", "", DataViewRowState.CurrentRows).ToTable()
                Call FillAttachment()
                popup_ScanAttchment.Show()
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |26-APR-2019| -- END

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Change for Approv Employee Query Strings Session.
            'Response.Redirect(Session("rootpath") & "HR/wPgEmployeeProfile.aspx")
            If Session("EmpiQueryString") IsNot Nothing AndAlso Session("EmpiQueryString").ToString().Trim.Length > 0 Then
                Dim strEmpQuery As String = Session("EmpiQueryString").ToString.Trim
                Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx?" & strEmpQuery, False)

            ElseIf IsNothing(Session("IsfromRecruitment")) = False AndAlso CBool(Session("IsfromRecruitment")) = True Then
                Response.Redirect(Session("rootpath").ToString & "Recruitment/wPgEmployeeUpdate.aspx", False)
                Session.Remove("IsfromRecruitment")
            Else
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx", False)
                Response.Redirect(mstrURLReferer, False)
                'Sohail (09 Nov 2020) -- End
            End If
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\HR\wPgEmployeeProfile.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END 


    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgv_Attchment.Items.Count <= 0 Then
                msg.DisplayMessage("No Files to download.", Me)
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & lblScanHeader.Text.Replace(" ", "") + ".zip", mdtAttachement, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

#End Region

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "tabpPersonalInfo", Me.Title)
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"tabpPersonalInfo", Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "tabpPersonalInfo", Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END 

            Me.lnPresentAddress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnPresentAddress.ID, Me.lnPresentAddress.Text)
            Me.lblFax.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFax.ID, Me.lblFax.Text)
            Me.lblEstate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEstate.ID, Me.lblEstate.Text)
            Me.lblProvince.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblProvince.ID, Me.lblProvince.Text)
            Me.lblRoad.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRoad.ID, Me.lblRoad.Text)
            Me.lblPloteNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPloteNo.ID, Me.lblPloteNo.Text)
            Me.lblMobile.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMobile.ID, Me.lblMobile.Text)
            Me.lblEmail.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmail.ID, Me.lblEmail.Text)
            Me.lblTelNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTelNo.ID, Me.lblTelNo.Text)
            Me.lblPostTown.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPostTown.ID, Me.lblPostTown.Text)
            Me.lblPresentState.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPresentState.ID, Me.lblPresentState.Text)
            Me.lblPostCountry.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPostCountry.ID, Me.lblPostCountry.Text)
            Me.lblPostcode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPostcode.ID, Me.lblPostcode.Text)
            Me.lblAddress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAddress.ID, Me.lblAddress.Text)
            Me.lblAddress2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAddress2.ID, Me.lblAddress2.Text)
            Me.lblAlternativeNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAlternativeNo.ID, Me.lblAlternativeNo.Text)

            Me.lnDomicileAddress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnDomicileAddress.ID, Me.lnDomicileAddress.Text)
            Me.lblDomicileFax.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicileFax.ID, Me.lblDomicileFax.Text)
            Me.lblDomicileMobileNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicileMobileNo.ID, Me.lblDomicileMobileNo.Text)
            Me.lblDomicileTelNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicileTelNo.ID, Me.lblDomicileTelNo.Text)
            Me.lblDomicileRoad.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicileRoad.ID, Me.lblDomicileRoad.Text)
            Me.lblDomicileEState.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicileEState.ID, Me.lblDomicileEState.Text)
            Me.lblDomicileEmail.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicileEmail.ID, Me.lblDomicileEmail.Text)
            Me.lblDomicilePostTown.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicilePostTown.ID, Me.lblDomicilePostTown.Text)
            Me.lblDomicilePostCountry.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicilePostCountry.ID, Me.lblDomicilePostCountry.Text)
            Me.lblDomicileAddress1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicileAddress1.ID, Me.lblDomicileAddress1.Text)
            Me.lblDomicileAddress2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicileAddress2.ID, Me.lblDomicileAddress2.Text)
            Me.lblDomicilePlotNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicilePlotNo.ID, Me.lblDomicilePlotNo.Text)
            Me.lblDomicileProvince.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicileProvince.ID, Me.lblDomicileProvince.Text)
            Me.lblDomicileState.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicileState.ID, Me.lblDomicileState.Text)
            Me.lblDomicilePostCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicilePostCode.ID, Me.lblDomicilePostCode.Text)
            Me.lblDomicileAltNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDomicileAltNo.ID, Me.lblDomicileAltNo.Text)

            Me.lnRecruitmentAddress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnRecruitmentAddress.ID, Me.lnRecruitmentAddress.Text)
            Me.lblRPlotNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRPlotNo.ID, Me.lblRPlotNo.Text)
            Me.lblRTelNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRTelNo.ID, Me.lblRTelNo.Text)
            Me.lblRRegion.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRRegion.ID, Me.lblRRegion.Text)
            Me.lblRStreet.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRStreet.ID, Me.lblRStreet.Text)
            Me.lblREState.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblREState.ID, Me.lblREState.Text)
            Me.lblRState.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRState.ID, Me.lblRState.Text)
            Me.lblRCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRCode.ID, Me.lblRCode.Text)
            Me.lblRCity.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRCity.ID, Me.lblRCity.Text)
            Me.lblRAddress2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRAddress2.ID, Me.lblRAddress2.Text)
            Me.lblRCountry.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRCountry.ID, Me.lblRCountry.Text)
            Me.lblRAddress1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblRAddress1.ID, Me.lblRAddress1.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            Me.lblpresentaddressapproval.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblpresentaddressapproval.ID, Me.lblpresentaddressapproval.Text)
            Me.lbldomicileaddressapproval.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lbldomicileaddressapproval.ID, Me.lbldomicileaddressapproval.Text)
            Me.lblrecruitmentaddressapproval.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblrecruitmentaddressapproval.ID, Me.lblrecruitmentaddressapproval.Text)

            'S.SANDEEP |31-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Employee Bio Data UAT]
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 502, "Saved Successfully")
            'S.SANDEEP |31-MAY-2019| -- END

            'Language.setLanguage(mstrModuleName1)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)

            'S.SANDEEP |31-MAY-2019| -- START
            'ISSUE/ENHANCEMENT : [Employee Bio Data UAT]
            'Language._Object.SaveValue()
            'S.SANDEEP |31-MAY-2019| -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 28, "Invalid Present Email.Please Enter Valid Email")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 29, "Invalid Domicile Email.Please Enter Valid Email")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 59, "Sorry, this present email address is already used by some employee. please provide another present email address.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 61, "Sorry, this domicile email address is already used by some employee. please provide another domicile email address.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 156, "Sorry, In order to proceed with document attachment, Please provide present address1 to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 157, "Sorry, In order to proceed with document attachment, Please provide domicile address1 to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 158, "Sorry, In order to proceed with document attachment, Please provide recruitment address1 to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 300, "Scan/Document(s) is mandatory information for Present Address.Please attach Scan/Document(s) in order to perform operation.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 301, "Scan/Document(s) is mandatory information for Domicile Address.Please attach Scan/Document(s) in order to perform operation.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 302, "Scan/Document(s) is mandatory information for Recruitment Address.Please attach Scan/Document(s) in order to perform operation.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 502, "Saved Successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 999, "Configuration Path does not Exist.")

        Catch Ex As Exception
            msg.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
