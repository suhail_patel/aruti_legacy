﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports System.Drawing

#End Region

Partial Class HR_wPgEmpQualificationList
    Inherits Basepage

#Region " Private Variable(s) "

    Private objEmpQualif As New clsEmp_Qualification_Tran
    Dim msg As New CommonCodes
    Dim clsuser As New User


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmQualificationsList"
    'Pinkal (06-May-2014) -- End

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objAQualificationTran As New clsEmp_Qualification_Approval_Tran

    Dim Arr() As String
    Dim QualificationApprovalFlowVal As String

    Dim blnPendingEmployee As Boolean = False
    'Gajanan [17-DEC-2018] -- End


    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End

    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mblnisEmployeeApprove As Boolean = False
    'Gajanan [17-April-2019] -- End
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objQGMaster As New clsCommon_Master
        Dim objQMaster As New clsqualification_master
        Dim objInstitute As New clsinstitute_master
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsCombos = objEmployee.GetEmployeeList("Employee", True, Not Aruti.Data.ConfigParameter._Object._IsIncludeInactiveEmp)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsCombos = objEmployee.GetEmployeeList("Employee", True, )
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END
                'End If


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .

                'dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                '                                    CInt(Session("UserId")), _
                '                                    CInt(Session("Fin_year")), _
                '                                    CInt(Session("CompanyUnkId")), _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                    CStr(Session("UserAccessModeSetting")), True, _
                '                                    CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

                ''Shani(24-Aug-2015) -- End

                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True
                If Session("PendingEmployeeScreenIDs").ToString.Trim.Length > 0 Then
                    If Session("PendingEmployeeScreenIDs").ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmQualificationsList))) Then
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If

                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                           CStr(Session("UserAccessModeSetting")), mblnOnlyApproved, _
                                           CBool(Session("IsIncludeInactiveEmp")), "Employee", True, _
                                         0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


                'S.SANDEEP [ 27 APRIL 2012 ] -- END
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombos.Tables("Employee")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With

                'S.SANDEEP [20-JUN-2018] -- End

            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If

            dsCombos = objQGMaster.getComboList(clsCommon_Master.enCommonMaster.QUALIFICATION_COURSE_GROUP, True, "List")
            With cboQGrp
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombos = objQMaster.GetComboList("List", True)
            With cboQualification
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombos = objInstitute.getListForCombo(False, "List", True, )
            With cboInstitute
                .DataValueField = "instituteunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Public Shared Function b64encode(ByVal StrEncode As String) As String
        Try
            Dim encodedString As String
            encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
            Return (encodedString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub delete()
        Try
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            'popup1.Show()
            DeleteReason1.Show()
            'Sohail (02 May 2012) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearObject()
        Try
            cboInstitute.SelectedValue = CStr(0) : cboQGrp.SelectedValue = CStr(0) : cboQualification.SelectedValue = CStr(0) : txtRefNo.Text = ""
            dtAwarddtFrom.SetDate = Nothing : dtAwarddtTo.SetDate = Nothing

            'Pinkal (25-APR-2012) -- Start
            'Enhancement : TRA Changes
            txtOtherQGrp.Text = ""
            txtOtherQualification.Text = ""
            chkOtherQualification.Checked = False
            chkOtherQualification_CheckedChanged(New Object(), New EventArgs())
            'Pinkal (25-APR-2012) -- End

            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            cboEmployee.SelectedIndex = 0
            'Sohail (02 May 2012) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim StrSearching As String = String.Empty
        Try


            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("ViewEmpQualificationList")) = False Then Exit Sub
            End If

            'Pinkal (22-Nov-2012) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            ''S.SANDEEP [ 27 APRIL 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''dsList = objEmpQualif.GetList("QualifyList")
            'dsList = objEmpQualif.GetList("QualifyList", Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"))
            ''S.SANDEEP [ 27 APRIL 2012 ] -- END


            ''Pinkal (25-APR-2012) -- Start
            ''Enhancement : TRA Changes

            'If txtOtherQGrp.Text.Trim.Trim.Length > 0 Then
            '    StrSearching &= "AND other_qualificationgrp LIKE '%" & txtOtherQGrp.Text.Trim & "%' "
            'End If

            'If txtOtherQualification.Text.Trim.Trim.Length > 0 Then
            '    StrSearching &= "AND other_qualification LIKE '%" & txtOtherQualification.Text.Trim & "%' "
            'End If

            ''Pinkal (25-APR-2012) -- End


            'If CInt(cboQGrp.SelectedValue) > 0 Then
            '    StrSearching &= "AND QGrpId = " & CInt(cboQGrp.SelectedValue) & " "
            'End If

            'If CInt(cboQualification.SelectedValue) > 0 Then
            '    StrSearching &= "AND QualifyId = " & CInt(cboQualification.SelectedValue) & " "
            'End If

            'If CInt(cboEmployee.SelectedValue) > 0 Then
            '    StrSearching &= "AND EmpId = " & CInt(cboEmployee.SelectedValue) & " "
            'End If

            'If CInt(cboInstitute.SelectedValue) > 0 Then
            '    StrSearching &= "AND InstituteId = " & CInt(cboInstitute.SelectedValue)
            'End If

            'If txtRefNo.Text.Trim <> "" Then
            '    StrSearching &= "AND RefNo LIKE '%" & txtRefNo.Text & "%'" & " "
            'End If

            'If dtAwarddtFrom.IsNull = False AndAlso dtAwarddtTo.IsNull = False Then
            '    StrSearching &= "AND StDate >= '" & eZeeDate.convertDate(dtAwarddtFrom.GetDate.Date) & "' AND EnDate <= '" & eZeeDate.convertDate(dtAwarddtTo.GetDate.Date) & "'" & " "
            'End If

            'If StrSearching.Length > 0 Then
            '    StrSearching = StrSearching.Substring(3)
            '    dtTable = New DataView(dsList.Tables("QualifyList"), StrSearching, "EmpName", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsList.Tables("QualifyList"), "", "EmpName", DataViewRowState.CurrentRows).ToTable
            'End If

            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            If txtOtherInstitute.Text.Trim.Length > 0 Then
                StrSearching &= "AND ISNULL(hremp_qualification_tran.other_institute,'') LIKE '%" & txtOtherInstitute.Text.Trim & "%' "
            End If
            'S.SANDEEP [23 JUL 2016] -- END

            If txtOtherQGrp.Text.Trim.Trim.Length > 0 Then
                StrSearching &= "AND ISNULL(hremp_qualification_tran.other_qualificationgrp,'') LIKE '%" & txtOtherQGrp.Text.Trim & "%' "
            End If

            If txtOtherQualification.Text.Trim.Trim.Length > 0 Then
                StrSearching &= "AND ISNULL(hremp_qualification_tran.other_qualification,'') LIKE '%" & txtOtherQualification.Text.Trim & "%' "
            End If

            If CInt(cboQGrp.SelectedValue) > 0 Then
                StrSearching &= "AND ISNULL(cfcommon_master.masterunkid,0) = " & CInt(cboQGrp.SelectedValue) & " "
            End If

            If CInt(cboQualification.SelectedValue) > 0 Then
                StrSearching &= "AND ISNULL(hrqualification_master.qualificationunkid,0) = " & CInt(cboQualification.SelectedValue) & " "
            End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboInstitute.SelectedValue) > 0 Then
                StrSearching &= "AND hrinstitute_master.instituteunkid = " & CInt(cboInstitute.SelectedValue)
            End If

            If txtRefNo.Text.Trim <> "" Then
                StrSearching &= "AND ISNULL(hremp_qualification_tran.reference_no, '') LIKE '%" & txtRefNo.Text & "%'" & " "
            End If

            If dtAwarddtFrom.IsNull = False AndAlso dtAwarddtTo.IsNull = False Then
                StrSearching &= "AND CONVERT(CHAR(8), hremp_qualification_tran.award_start_date, 112) >= '" & eZeeDate.convertDate(dtAwarddtFrom.GetDate.Date) & "' AND CONVERT(CHAR(8), hremp_qualification_tran.award_end_date, 112) <= '" & eZeeDate.convertDate(dtAwarddtTo.GetDate.Date) & "'" & " "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If


            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.

            'dsList = objEmpQualif.GetList(Session("Database_Name"), _
            '                                      Session("UserId"), _
            '                                      Session("Fin_year"), _
            '                                      Session("CompanyUnkId"), _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                      Session("UserAccessModeSetting"), True, _
            '                                      Session("IsIncludeInactiveEmp"), "QualifyList", , StrSearching)



            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .

            'dsList = objEmpQualif.GetList(CStr(Session("Database_Name")), _
            '                                     CInt(Session("UserId")), _
            '                                     CInt(Session("Fin_year")), _
            '                                     CInt(Session("CompanyUnkId")), _
            '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                     CStr(Session("UserAccessModeSetting")), True, _
            '                                   CBool(Session("IsIncludeInactiveEmp")), "QualifyList", , StrSearching, CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))


            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If Session("PendingEmployeeScreenIDs").ToString.Trim.Length > 0 Then
                If Session("PendingEmployeeScreenIDs").ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmQualificationsList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim mblnisusedmss As Boolean = False
            IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, mblnisusedmss = True, mblnisusedmss = False)

            dsList = objEmpQualif.GetList(CStr(Session("Database_Name")), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                  CStr(Session("UserAccessModeSetting")), True, _
                                         CBool(Session("IsIncludeInactiveEmp")), "QualifyList", True, _
                                         StrSearching, mblnisusedmss, mblnAddApprovalCondition)
            'Gajanan [17-DEC-2018] -- End
            'S.SANDEEP [20-JUN-2018] -- End


            'Pinkal (28-Dec-2015) -- End

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim dcol As New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "tranguid"
                .DefaultValue = ""
            End With
            dsList.Tables(0).Columns.Add(dcol)

            If QualificationApprovalFlowVal Is Nothing AndAlso mintTransactionId <= 0 Then
                Dim dsPending As New DataSet
                dsPending = objAQualificationTran.GetList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), mblnOnlyApproved, _
                                          CBool(Session("IsIncludeInactiveEmp")), "Emp", , StrSearching, _
                                          mblnisusedmss, mblnAddApprovalCondition)



                If dsPending.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In dsPending.Tables(0).Rows
                        dsList.Tables(0).ImportRow(row)
                    Next
                End If
            Else



            End If

            'Gajanan [17-DEC-2018] -- End

            dtTable = New DataView(dsList.Tables("QualifyList"), "", "EmpName", DataViewRowState.CurrentRows).ToTable

            'Shani(24-Aug-2015) -- End

            objtblPanel.Visible = False
            If dtTable IsNot Nothing Then
                dgView.DataSource = dtTable
                dgView.DataKeyField = "QulifyTranId"
                dgView.DataBind()
            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                dgView.CurrentPageIndex = 0
                dgView.DataBind()
            Else
                msg.DisplayError(ex, Me)
            End If
        End Try
    End Sub

    'Private Sub SetPrevious_State()
    '    Try
    '        Session("Prev_State") = cboEmployee.SelectedValue & "|" & _
    '                                cboQGrp.SelectedValue & "|" & _
    '                                cboQualification.SelectedValue & "|" & _
    '                                cboInstitute.SelectedValue & "|" & _
    '                                dgView.CurrentPageIndex()
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub GetPrevious_State()
    '    Try
    '        Dim StrState() As String = Nothing
    '        StrState = CType(Session("Prev_State"), String).Split("|")

    '        If StrState.Length > 0 Then
    '            cboEmployee.SelectedValue = StrState(0)
    '            cboQGrp.SelectedValue = StrState(1)
    '            cboQualification.SelectedValue = StrState(2)
    '            cboInstitute.SelectedValue = StrState(3)
    '            dgView.CurrentPageIndex = StrState(4)
    '        End If

    '        Session("IsFrom_AddEdit") = False

    '        Call FillGrid()

    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                'Anjan [20 February 2016] -- End
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmQualifications"
            'StrModuleName2 = "mnuPersonnel"
            'StrModuleName3 = "mnuEmployeeData"
            'clsCommonATLog._WebClientIP = Session("IP_ADD")
            'clsCommonATLog._WebHostName = Session("HOST_NAME")

            ''Pinkal (24-Aug-2012) -- Start
            ''Enhancement : TRA Changes

            'If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            'Pinkal (24-Aug-2012) -- End
            'S.SANDEEP [ 04 JULY 2012 ] -- END


            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Aruti.Data.FinancialYear._Object._YearUnkid = Session("Fin_year")
            'S.SANDEEP [ 27 APRIL 2012 ] -- END


            Dim clsuser As New User
            If (Page.IsPostBack = False) Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLanguage()
                'Pinkal (06-May-2014) -- End


                clsuser = CType(Session("clsuser"), User)
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.UserID
                    Me.ViewState("Empunkid") = Session("UserId")
                    'Sohail (30 Mar 2015) -- End


                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = Session("UserId")
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END


                    'Anjan (30 May 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'btnNew.Visible = False
                    btnNew.Visible = CBool(Session("AddEmployeeQualification"))
                    'Anjan (30 May 2012)-End 


                    'S.SANDEEP [20-JUN-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow For NMB .

                    ''S.SANDEEP [ 16 JAN 2014 ] -- START
                    'chkShowPending.Visible = CBool(Session("AllowToApproveEmployee"))
                    'If Session("ShowPending") IsNot Nothing Then
                    '    chkShowPending.Checked = True
                    'End If
                    ''S.SANDEEP [ 16 JAN 2014 ] -- END

                    'S.SANDEEP [20-JUN-2018] -- End
                Else
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = -1
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                    Me.ViewState("Empunkid") = Session("Employeeunkid")
                    'Sohail (30 Mar 2015) -- End

                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'btnNew.Visible = ConfigParameter._Object._AllowEditQualifications
                    btnNew.Visible = CBool(Session("AllowEditQualifications"))
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-End 


                    'S.SANDEEP [20-JUN-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow For NMB .

                    ''S.SANDEEP [ 16 JAN 2014 ] -- START
                    'chkShowPending.Visible = False
                    ''S.SANDEEP [ 16 JAN 2014 ] -- END

                    'S.SANDEEP [20-JUN-2018] -- End

                End If

                dtAwarddtFrom.SetDate = Nothing
                dtAwarddtTo.SetDate = Nothing

                Call FillCombo()


                'Pinkal (22-Mar-2012) -- Start
                'Enhancement : TRA Changes
                'Call FillGrid()
                'Pinkal (22-Mar-2012) -- End



                'If Session("IsFrom_AddEdit") = True Then
                '    Call GetPrevious_State()
                'End If


                'Pinkal (25-APR-2012) -- Start
                'Enhancement : TRA Changes
                chkOtherQualification_CheckedChanged(sender, e)
                'Pinkal (25-APR-2012) -- End


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .

                ''S.SANDEEP [ 16 JAN 2014 ] -- START
                'If Session("ShowPending") IsNot Nothing Then
                '    Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                'End If
                ''S.SANDEEP [ 16 JAN 2014 ] -- END

                'S.SANDEEP [20-JUN-2018] -- End



                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("qualificationEmpunkid") IsNot Nothing Then
                    cboEmployee.SelectedValue = CStr(Session("qualificationEmpunkid"))
                    Session.Remove("qualificationEmpunkid")
                    If CInt(cboEmployee.SelectedValue) > 0 Then
                        Call btnSearch_Click(btnSearch, Nothing)
                    End If
                End If
                'SHANI [09 Mar 2015]--END 

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Else
                blnPendingEmployee = CBool(ViewState("blnPendingEmployee"))


                'Gajanan [17-DEC-2018] -- End

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If IsNothing(ViewState("mblnisEmployeeApprove")) = False Then
                    mblnisEmployeeApprove = CBool(ViewState("mblnisEmployeeApprove"))
                End If
                'Gajanan [17-April-2019] -- End

            End If
            'Pinkal (22-Mar-2012) -- Start
            'Enhancement : TRA Changes

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
            'SHANI [01 FEB 2015]--END
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                dgView.Columns(2).Visible = True
            Else
                dgView.Columns(2).Visible = False
            End If

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'lblPendingData.Visible = False
            'btnApprovalinfo.Visible = False
            objtblPanel.Visible = False

            If blnPendingEmployee Then
                objtblPanel.Visible = blnPendingEmployee
            End If

            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            QualificationApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmQualificationsList)))
            'Gajanan [17-DEC-2018] -- End

            'Pinkal (22-Mar-2012) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [01 FEB 2015]--END
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session("IsFrom_AddEdit") = False
    End Sub

    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Try
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleSaveButton(False)
            'ToolbarEntry1.VisibleDeleteButton(False)
            'ToolbarEntry1.VisibleCancelButton(False)
            'ToolbarEntry1.VisibleExitButton(False)
            'SHANI [01 FEB 2015]--END
            Me.ViewState.Add("blnPendingEmployee", blnPendingEmployee)

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Me.ViewState.Add("mblnisEmployeeApprove", mblnisEmployeeApprove)
            'Gajanan [17-April-2019] -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Gajanan [17-DEC-2018] -- End

    End Sub
    'Pinkal (22-Mar-2012) -- End

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "
    'Gajanan [27-May-2019] -- Start              
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If IsNothing(Session("Isfromprofile")) = False AndAlso CBool(Session("Isfromprofile")) = True Then
                Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeDetail.aspx", False)
                Session.Remove("Isfromprofile")

            ElseIf IsNothing(Session("IsfromRecruitment")) = False AndAlso CBool(Session("IsfromRecruitment")) = True Then
                Response.Redirect(Session("rootpath").ToString & "Recruitment/wPgEmployeeUpdate.aspx", False)
                Session.Remove("IsfromRecruitment")
            Else

                Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Gajanan [27-May-2019] -- End

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                msg.DisplayMessage("Please Select atleast one Employee.", Me)
                Exit Sub
            End If


            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            If CInt(cboEmployee.SelectedValue) > 0 Then
                Session("qualificationEmpunkid") = cboEmployee.SelectedValue
            End If
            'SHANI [09 Mar 2015]--END 
            Response.Redirect("wPgEmpQualification.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnNew_Click : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            Call ClearObject()


            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes
            'Call FillGrid()
            dgView.DataSource = Nothing
            dgView.DataBind()
            dgView.CurrentPageIndex = 0
            'Pinkal (22-Nov-2012) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnReset_Click Event : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DeleteReason1.buttonDelReasonYes_Click 'Sohail (02 May 2012) - [ButDel.Click]
        Try
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            'If (txtreason.Text = "") Then
            '    msg.DisplayMessage(" Please enter delete reason.", Me)
            '    Exit Sub
            'End If
            'Sohail (02 May 2012) -- End

            Dim objQTran As New clsEmp_Qualification_Tran
            objQTran._Qualificationtranunkid = CInt(Me.ViewState("Unkid"))


            'S.SANDEEP [ 27 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Blank_ModuleName()
            objQTran._WebFormName = "frmQualificationsList"
            StrModuleName2 = "mnuPersonnel"
            StrModuleName3 = "mnuEmployeeData"
            objQTran._WebClientIP = CStr(Session("IP_ADD"))
            objQTran._WebHostName = CStr(Session("HOST_NAME"))

            'Pinkal (24-Aug-2012) -- Start
            'Enhancement : TRA Changes

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                objQTran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                objQTran._Userunkid = -1
            Else
                objQTran._Loginemployeeunkid = -1
                objQTran._Userunkid = CInt(Session("UserId"))
            End If
            'S.SANDEEP [ 27 AUG 2012 ] -- END


            With objQTran
                ._Isvoid = True
                ._Voiddatetime = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime
                'Sohail (02 May 2012) -- Start
                'TRA - ENHANCEMENT
                '._Voidreason = txtreason.Text
                ._Voidreason = DeleteReason1.Reason
                'Sohail (02 May 2012) -- End
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    ._Voiduserunkid = CInt(Session("UserId"))
                Else
                    ._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))
                End If


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                'If QualificationApprovalFlowVal Is Nothing Then
                If QualificationApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then
                    'Gajanan [17-April-2019] -- End

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    Dim eLMode As Integer
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        eLMode = enLogin_Mode.MGR_SELF_SERVICE
                    Else
                        eLMode = enLogin_Mode.EMP_SELF_SERVICE
                    End If

                    Dim objEmp_Qualification_Tran As New clsEmp_Qualification_Tran
                    objEmp_Qualification_Tran._Qualificationtranunkid = CInt(Me.ViewState("Unkid"))

                    'Gajanan [17-April-2019] -- End

                    objAQualificationTran._Isvoid = True
                    objAQualificationTran._Audituserunkid = CInt(Session("UserId"))
                    objAQualificationTran._Isweb = True
                    objAQualificationTran._Ip = getIP()
                    objAQualificationTran._Host = getHostName()
                    objAQualificationTran._Form_Name = mstrModuleName
                    If objAQualificationTran.Delete(CInt(Me.ViewState("Unkid")), DeleteReason1.Reason, CInt(Session("CompanyUnkId")), Nothing) = False Then
                        If objAQualificationTran._Message <> "" Then
                            msg.DisplayMessage(objAQualificationTran._Message, Me)
                        Else
                            Call FillGrid()
                            Me.ViewState("Unkid") = Nothing
                        End If

                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    Else
                        objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                         CStr(Session("UserAccessModeSetting")), _
                                                         CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                         CInt(enUserPriviledge.AllowToApproveRejectEmployeeQualifications), _
                                                         enScreenName.frmQualificationsList, CStr(Session("EmployeeAsOnDate")), _
                                                         CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                         Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.DELETED, , objEmp_Qualification_Tran._Employeeunkid.ToString(), , , _
                                                         " qualificationtranunkid = " & CInt(Me.ViewState("Unkid")), Nothing, , , _
                                                         " qualificationtranunkid = " & CInt(Me.ViewState("Unkid")), Nothing)
                        'Gajanan [17-April-2019] -- End
                        Exit Sub
                    End If
                Else
                    If .Delete(CInt(Me.ViewState("Unkid"))) = False Then
                        msg.DisplayMessage(._Message, Me)
                    Else
                        Call FillGrid()
                        Me.ViewState("Unkid") = Nothing
                        'txtreason.Text = "" 'Sohail (02 May 2012) 
                    End If
                End If


                'If .Delete(CInt(Me.ViewState("Unkid"))) = False Then
                '    msg.DisplayMessage(._Message, Me)
                'Else
                '    Call FillGrid()
                '    Me.ViewState("Unkid") = Nothing
                '    'txtreason.Text = "" 'Sohail (02 May 2012) 
                'End If
            End With

            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            'popup1.Dispose()
            DeleteReason1.Dispose()
            'Sohail (02 May 2012) -- End

            objQTran = Nothing

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    Protected Sub btnApprovalinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovalinfo.Click
        Try
            Popup_Viewreport._UserId = CInt(Session("UserId"))
            Popup_Viewreport._Priority = 0
            Popup_Viewreport._PrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeQualifications
            Popup_Viewreport._FillType = enScreenName.frmQualificationsList
            Popup_Viewreport._FilterString = "EM.employeeunkid = " & CInt(cboEmployee.SelectedValue)
            Popup_Viewreport._FromApprovalScreen = False
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Popup_Viewreport._OprationType = clsEmployeeDataApproval.enOperationType.NONE
            'Gajanan [17-DEC-2018] -- End
            Popup_Viewreport.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me) 'Hemant (13 Aug 2020)
        End Try
    End Sub
#End Region

#Region " Control's Event(s) "

    Protected Sub dgView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.DeleteCommand
        Try
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Me.ViewState.Add("Unkid", dgView.DataKeys(e.Item.ItemIndex))
            ''txtreason.Visible = True 'Sohail (02 May 2012) 
            'delete()
            'FillGrid()

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(e.Item.Cells(11).Text)
            mblnisEmployeeApprove = objEmployee._Isapproved


            'If QualificationApprovalFlowVal Is Nothing Then
            If QualificationApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then

                If objApprovalData.IsApproverPresent(enScreenName.frmEmployee_Skill_List, CStr(Session("Database_Name")), _
                                                     CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                     CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                     CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(e.Item.Cells(11).Text), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    msg.DisplayMessage(objApprovalData._Message, Me)
                    Exit Sub
                End If
                'Gajanan [17-April-2019] -- End

                Dim item = dgView.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(12).Text = e.Item.Cells(12).Text And x.Cells(10).Text.Trim <> "&nbsp;").FirstOrDefault()
                If item IsNot Nothing Then
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process."), Me)
                    Exit Sub
                Else
                    Me.ViewState.Add("Unkid", CInt(dgView.DataKeys(e.Item.ItemIndex)))
                    delete()
                End If
            Else
                Me.ViewState.Add("Unkid", dgView.DataKeys(e.Item.ItemIndex))
                delete()
            End If
            FillGrid()
            'Gajanan [17-DEC-2018] -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.ItemCommand
        Try
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If dgView.Items.Count > 0 Then
                If dgView.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Color.LightCoral).Count() > 0 Then
                    Dim item = dgView.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Color.LightCoral).FirstOrDefault()
                    If item IsNot Nothing Then item.BackColor = Color.White
                End If
            End If
            'Session("IsFrom_AddEdit") = True

            '' Call SetPrevious_State()

            'If e.CommandName = "Select" Then

            '    'SHANI [09 Mar 2015]-START
            '    'Enhancement - REDESIGN SELF SERVICE.
            '    If CInt(cboEmployee.SelectedValue) > 0 Then
            '        Session("qualificationEmpunkid") = cboEmployee.SelectedValue
            '    End If
            '    'SHANI [09 Mar 2015]--END 
            '    Response.Redirect("wPgEmpQualification.aspx?ProcessId=" & b64encode(CStr(Val(dgView.DataKeys(e.Item.ItemIndex)))))
            'End If
            If e.CommandName = "Select" Then
                If QualificationApprovalFlowVal Is Nothing Then
                    Dim item = dgView.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(12).Text = e.Item.Cells(12).Text And x.Cells(10).Text.Trim <> "&nbsp;").FirstOrDefault()
                    If item IsNot Nothing Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process."), Me)
                        Exit Sub
                    Else
                        Session("IsFrom_AddEdit") = True
                        If e.CommandName = "Select" Then
                            If CInt(cboEmployee.SelectedValue) > 0 Then
                                Session("qualificationEmpunkid") = cboEmployee.SelectedValue
                            End If

                            'Gajanan [3-April-2019] -- Start
                            'Response.Redirect("wPgEmpQualification.aspx?ProcessId=" & b64encode(CStr(Val(dgView.DataKeys(e.Item.ItemIndex)))))
                            Response.Redirect("wPgEmpQualification.aspx?ProcessId=" & b64encode(CStr(Val(dgView.DataKeys(e.Item.ItemIndex)))), False)
                            'Gajanan [3-April-2019] -- End
                        End If
                    End If
                Else
                    Session("IsFrom_AddEdit") = True
                    If e.CommandName = "Select" Then
                        If CInt(cboEmployee.SelectedValue) > 0 Then
                            Session("qualificationEmpunkid") = cboEmployee.SelectedValue
                        End If
                        Response.Redirect("wPgEmpQualification.aspx?ProcessId=" & b64encode(CStr(Val(dgView.DataKeys(e.Item.ItemIndex)))), False)
                    End If
                End If
            ElseIf e.CommandName.ToUpper = "VIEW" Then
                Dim item = dgView.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(12).Text = e.Item.Cells(12).Text And x.Cells(10).Text.Trim = "&nbsp;").FirstOrDefault()
                If item IsNot Nothing Then
                    item.BackColor = Color.LightCoral
                    item.ForeColor = Color.Black
                End If
            End If
            'Gajanan [17-DEC-2018] -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgView.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If (e.Item.ItemIndex >= 0) Then

                If chkOtherQualification.Checked = False AndAlso CInt(e.Item.Cells(9).Text) <= 0 Then
                    e.Item.Visible = False
                ElseIf chkOtherQualification.Checked = True AndAlso CInt(e.Item.Cells(9).Text) > 0 Then
                    e.Item.Visible = False
                End If
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                'If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                '    'e.Item.Cells(0).Visible = True : e.Item.Cells(1).Visible = True

                '    'Anjan (30 May 2012)-Start
                '    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                '    'dgView.Columns(1).Visible = False 'Anjan (02 Mar 2012)


                '    'Pinkal (22-Nov-2012) -- Start
                '    'Enhancement : TRA Changes
                '    dgView.Columns(0).Visible = CBool(Session("EditEmployeeQualification"))
                '    dgView.Columns(1).Visible = CBool(Session("DeleteEmployeeQualification"))

                '    'Pinkal (22-Nov-2012) -- End

                '    'Anjan (30 May 2012)-End 


                '    'S.SANDEEP [20-JUN-2018] -- Start
                '    'Enhancement - Implementing Employee Approver Flow For NMB .

                '    ''S.SANDEEP [ 16 JAN 2014 ] -- START
                '    'If Session("ShowPending") IsNot Nothing Then
                '    '    dgView.Columns(1).Visible = False
                '    'End If
                '    ''S.SANDEEP [ 16 JAN 2014 ] -- END

                '    'S.SANDEEP [20-JUN-2018] -- End

                'Else
                '    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dgView.Columns(1).Visible = ConfigParameter._Object._AllowDeleteQualifications


                '    'Pinkal (22-Nov-2012) -- Start
                '    'Enhancement : TRA Changes

                '    dgView.Columns(0).Visible = CBool(Session("AllowEditQualifications"))
                '    dgView.Columns(1).Visible = CBool(Session("AllowDeleteQualifications"))

                '    'Pinkal (22-Nov-2012) -- End


                '    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                'End If

                ''S.SANDEEP [ 31 DEC 2013 ] -- START
                ''If e.Item.Cells(5).Text.Trim.Length > 0 AndAlso e.Item.Cells(5).Text <> "&nbsp;" Then
                ''    e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text)
                ''End If

                ''If e.Item.Cells(6).Text.Trim.Length > 0 AndAlso e.Item.Cells(6).Text <> "&nbsp;" Then
                ''    e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text)
                ''End If

                ''Pinkal (16-Apr-2016) -- Start
                ''Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                ''If e.Item.Cells(5).Text.Trim.Length > 0 AndAlso e.Item.Cells(5).Text <> "&nbsp;" Then
                ''    If Session("DateFormat").ToString <> Nothing Then
                ''        e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).Date.ToString(Session("DateFormat").ToString)
                ''    Else
                ''        e.Item.Cells(5).Text = CStr(eZeeDate.convertDate(e.Item.Cells(5).Text))
                ''    End If
                ''End If
                ''If e.Item.Cells(6).Text.Trim.Length > 0 AndAlso e.Item.Cells(6).Text <> "&nbsp;" Then
                ''    If Session("DateFormat").ToString <> Nothing Then
                ''        e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text).Date.ToString(Session("DateFormat").ToString)
                ''    Else
                ''        e.Item.Cells(6).Text = CStr(eZeeDate.convertDate(e.Item.Cells(6).Text))
                ''    End If
                ''End If
                'If e.Item.Cells(5).Text.Trim.Length > 0 AndAlso e.Item.Cells(5).Text <> "&nbsp;" Then
                '    e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).Date.ToShortDateString
                'End If
                'If e.Item.Cells(6).Text.Trim.Length > 0 AndAlso e.Item.Cells(6).Text <> "&nbsp;" Then
                '    e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text).Date.ToShortDateString
                'End If
                ''Pinkal (16-Apr-2016) -- End

                ''S.SANDEEP [ 31 DEC 2013 ] -- END

                If QualificationApprovalFlowVal Is Nothing Then
                    Dim blnFlag As Boolean = False
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        blnFlag = CBool(Session("EditEmployeeQualification"))
                    Else
                        blnFlag = CBool(Session("AllowEditQualifications"))
                    End If
                    If blnFlag = False Then
                        CType(e.Item.Cells(1).FindControl("EditImg"), LinkButton).Visible = False
                    End If
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        blnFlag = CBool(Session("DeleteEmployeeQualification"))
                    Else
                        blnFlag = CBool(Session("AllowDeleteQualifications"))
                    End If
                    If blnFlag = False Then
                        CType(e.Item.Cells(2).FindControl("DeleteImg"), LinkButton).Visible = False
                    End If

                    If e.Item.Cells(10).Text <> "&nbsp;" AndAlso e.Item.Cells(10).Text.Trim.Length > 0 Then
                        e.Item.BackColor = Color.PowderBlue
                        e.Item.ForeColor = Color.Black

                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'lblPendingData.Visible = True
                        'btnApprovalinfo.Visible = True
                        objtblPanel.Visible = True
                        blnPendingEmployee = True
                        'Gajanan [17-DEC-2018] -- End

                        CType(e.Item.Cells(1).FindControl("EditImg"), LinkButton).Visible = False
                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = True
                        If CType(CType(e.Item.Cells(1).FindControl("hfoprationtypeid"), HiddenField).Value, clsEmployeeDataApproval.enOperationType) = clsEmployeeDataApproval.enOperationType.EDITED Then
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = True
                        Else
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = False
                        End If
                        'Gajanan [17-DEC-2018] -- End
                        CType(e.Item.Cells(2).FindControl("DeleteImg"), LinkButton).Visible = False
                    End If

                Else
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        dgView.Columns(0).Visible = CBool(Session("EditEmployeeQualification"))
                        dgView.Columns(1).Visible = CBool(Session("DeleteEmployeeQualification"))
                    Else
                        dgView.Columns(0).Visible = CBool(Session("AllowEditQualifications"))
                        dgView.Columns(1).Visible = CBool(Session("AllowDeleteQualifications"))
                    End If
                End If
                If e.Item.Cells(5).Text.Trim.Length > 0 AndAlso e.Item.Cells(5).Text <> "&nbsp;" Then
                    e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).Date.ToShortDateString
                End If
                If e.Item.Cells(6).Text.Trim.Length > 0 AndAlso e.Item.Cells(6).Text <> "&nbsp;" Then
                    e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text).Date.ToShortDateString
                End If
                'Gajanan [17-DEC-2018] -- End
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgView.PageIndexChanged
        Try
            dgView.CurrentPageIndex = e.NewPageIndex
            FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'S.SANDEEP [20-JUN-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow For NMB .

    ''S.SANDEEP [ 16 JAN 2014 ] -- START
    'Protected Sub chkShowPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowPending.CheckedChanged
    '    Try
    '        Session("ShowPending") = chkShowPending.Checked
    '        Dim dsCombos As New DataSet
    '        Dim objEmp As New clsEmployee_Master
    '        If chkShowPending.Checked = True Then

    '            'Shani(24-Aug-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"), , , False)

    '            dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                                CInt(Session("UserId")), _
    '                                                CInt(Session("Fin_year")), _
    '                                                CInt(Session("CompanyUnkId")), _
    '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                                CStr(Session("UserAccessModeSetting")), False, _
    '                                                CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

    '            'Shani(24-Aug-2015) -- End
    '            Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
    '            With cboEmployee
    '                .DataValueField = "employeeunkid"
    '                'Nilay (09-Aug-2016) -- Start
    '                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
    '                '.DataTextField = "employeename"
    '                .DataTextField = "EmpCodeName"
    '                'Nilay (09-Aug-2016) -- End
    '                .DataSource = dtTab
    '                .DataBind()
    '                Try
    '                    .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
    '                Catch ex As Exception
    '                    .SelectedValue = CStr(0)
    '                End Try
    '            End With
    '            btnNew.Visible = False
    '        Else
    '            Session("ShowPending") = Nothing
    '            btnNew.Visible = True
    '            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

    '                'Shani(24-Aug-2015) -- Start
    '                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
    '                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                                    CInt(Session("UserId")), _
    '                                                    CInt(Session("Fin_year")), _
    '                                                    CInt(Session("CompanyUnkId")), _
    '                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                                    CStr(Session("UserAccessModeSetting")), True, _
    '                                                    CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

    '                'Shani(24-Aug-2015) -- End
    '                With cboEmployee
    '                    .DataValueField = "employeeunkid"
    '                    'Nilay (09-Aug-2016) -- Start
    '                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
    '                    '.DataTextField = "employeename"
    '                    .DataTextField = "EmpCodeName"
    '                    'Nilay (09-Aug-2016) -- End
    '                    .DataSource = dsCombos.Tables("Employee")
    '                    .DataBind()
    '                    Try
    '                        .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
    '                    Catch ex As Exception
    '                        .SelectedValue = CStr(0)
    '                    End Try
    '                End With
    '            Else
    '                Dim objglobalassess = New GlobalAccess
    '                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
    '                With cboEmployee
    '                    .DataSource = objglobalassess.ListOfEmployee
    '                    .DataTextField = "loginname"
    '                    .DataValueField = "employeeunkid"
    '                    .DataBind()
    '                    Try
    '                        .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
    '                    Catch ex As Exception
    '                        .SelectedValue = CStr(0)
    '                    End Try
    '                End With
    '            End If
    '        End If
    '        dgView.DataSource = Nothing : dgView.DataBind()
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    ''S.SANDEEP [ 16 JAN 2014 ] -- END

    'S.SANDEEP [20-JUN-2018] -- End

#End Region

#Region "CheckBox Event"

    Protected Sub chkOtherQualification_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOtherQualification.CheckedChanged
        Try
            txtOtherQGrp.Text = ""
            txtOtherQualification.Text = ""
            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            txtOtherInstitute.Text = ""
            'S.SANDEEP [23 JUL 2016] -- END
            If chkOtherQualification.Checked Then
                txtOtherQGrp.Enabled = True
                txtOtherQualification.Enabled = True
                'S.SANDEEP [23 JUL 2016] -- START
                'CCK- OTHER QUALIFICATION CHANGE
                txtOtherInstitute.Enabled = True
                'S.SANDEEP [23 JUL 2016] -- END
            Else
                txtOtherQGrp.Enabled = False
                txtOtherQualification.Enabled = False
                'S.SANDEEP [23 JUL 2016] -- START
                'CCK- OTHER QUALIFICATION CHANGE
                txtOtherInstitute.Enabled = False
                'S.SANDEEP [23 JUL 2016] -- END
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "ToolBar Event"

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try

    '        'Pinkal (22-Nov-2012) -- Start
    '        'Enhancement : TRA Changes

    '        If Session("LoginBy") = Global.User.en_loginby.User Then
    '            If CBool(Session("AddEmployeeQualification")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                Exit Sub
    '            End If
    '        Else
    '            If CBool(Session("AllowAddQualifications")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                Exit Sub
    '            End If
    '        End If

    '        'Pinkal (22-Nov-2012) -- End

    '        Response.Redirect("~\HR\wPgEmpQualification.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

#End Region

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END

            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblQualification.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQualification.ID, Me.lblQualification.Text)
            Me.lblQualificationGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblQualificationGroup.ID, Me.lblQualificationGroup.Text)
            Me.lblAwardDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAwardDate.ID, Me.lblAwardDate.Text)
            Me.lblTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTo.ID, Me.lblTo.Text)
            Me.lblReferenceNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblReferenceNo.ID, Me.lblReferenceNo.Text)
            Me.lblInstitute.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblInstitute.ID, Me.lblInstitute.Text)
            'S.SANDEEP [23 JUL 2016] -- START
            'CCK- OTHER QUALIFICATION CHANGE
            Me.lblOtherInstitute.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblOtherInstitute.ID, Me.lblOtherInstitute.Text)
            'S.SANDEEP [23 JUL 2016] -- END
            Me.lblOtherQualificationGrp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblOtherQualificationGrp.ID, Me.lblOtherQualificationGrp.Text)
            Me.lblOtherQualification.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblOtherQualification.ID, Me.lblOtherQualification.Text)
            Me.chkOtherQualification.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkOtherQualification.ID, Me.chkOtherQualification.Text)

            dgView.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(2).FooterText, dgView.Columns(2).HeaderText)
            dgView.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(3).FooterText, dgView.Columns(3).HeaderText)
            dgView.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(4).FooterText, dgView.Columns(4).HeaderText)
            dgView.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(5).FooterText, dgView.Columns(5).HeaderText)
            dgView.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(6).FooterText, dgView.Columns(6).HeaderText)
            dgView.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(7).FooterText, dgView.Columns(7).HeaderText)
            dgView.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(8).FooterText, dgView.Columns(8).HeaderText)

            Me.lblPendingData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPendingData.ID, Me.lblPendingData.Text)
            Me.lblParentData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblParentData.ID, Me.lblParentData.Text)

            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    'Pinkal (06-May-2014) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process.")

        Catch Ex As Exception
            msg.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
