﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports Aruti.Data

#End Region

Partial Class wPgEmpIdentities_List
    Inherits Basepage

#Region " Private Variable(s) "

    Private objIdentities As New clsIdentity_tran
    Dim msg As New CommonCodes

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeMaster"
    Private ReadOnly mstrModuleName1 As String = "frmEmployeeList"
    'Pinkal (06-May-2014) -- End

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private objAIdInfoTran As New clsIdentity_Approval_tran
    Dim Arr() As String
    Dim IdentityApprovalFlowVal As String
    Dim blnPendingEmployee As Boolean = False
    'Gajanan [22-Feb-2019] -- End



    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Dim mdtAttachment As DataTable = Nothing
    'Gajanan [17-April-2019] -- End


    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End

    'Gajanan [3-April-2019] -- Start
    Dim mblnisEmployeeApprove As Boolean = False
    'Gajanan [3-April-2019] -- End

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objCMaster As New clsCommon_Master
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsCombos = objEmployee.GetEmployeeList("Employee", True, Not Aruti.Data.ConfigParameter._Object._IsIncludeInactiveEmp)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsCombos = objEmployee.GetEmployeeList("Employee", True, )
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END                    
                'End If
                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                       CStr(Session("UserAccessModeSetting")), True, _
                                                       CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                'Shani(24-Aug-2015) -- End

                'S.SANDEEP [ 27 APRIL 2012 ] -- END
                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombos.Tables("Employee")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()
            End If

            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.IDENTITY_TYPES, True, "List")
            With cboIdType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ClearObject()
        Try
            cboIdType.SelectedValue = CStr(0)
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            drpEmployee.SelectedIndex = 0
            'Sohail (02 May 2012) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dtList As New DataTable
        Dim dTable As DataTable
        Dim StrSearching As String = ""
        Try


            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("ViewEmployee")) = False Then Exit Sub
            End If

            'Pinkal (22-Nov-2012) -- End

            objIdentities._EmployeeUnkid = CInt(drpEmployee.SelectedValue)
            dtList = objIdentities._DataList

            If CInt(drpEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND employeeunkid = '" & drpEmployee.SelectedValue & "' "
            End If

            If CInt(cboIdType.SelectedValue) > 0 Then
                StrSearching &= "AND idtypeunkid = '" & cboIdType.SelectedValue & "' "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dTable = New DataView(dtList, StrSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dTable = New DataView(dtList, "", "", DataViewRowState.CurrentRows).ToTable
            End If

            'Gajanan [3-April-2019] -- Start
            If IdentityApprovalFlowVal Is Nothing = False Then
                Dim dcol As New DataColumn
                With dcol
                    .ColumnName = "operationtypeid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                End With
                dTable.Columns.Add(dcol)

                dcol = New DataColumn
                With dcol
                    .ColumnName = "operationtype"
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                dTable.Columns.Add(dcol)


                dcol = New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "tranguid"
                    .DefaultValue = ""
                End With
                dTable.Columns.Add(dcol)
                'Gajanan [3-April-2019] -- End
            End If

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If IdentityApprovalFlowVal Is Nothing Then
                objAIdInfoTran._Employeeunkid = CInt(drpEmployee.SelectedValue)
                dtList = objAIdInfoTran._DataTable
                Dim dcol As New DataColumn
                With dcol
                    .ColumnName = "operationtypeid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                End With
                dTable.Columns.Add(dcol)

                dcol = New DataColumn
                With dcol
                    .ColumnName = "operationtype"
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                dTable.Columns.Add(dcol)

                dcol = New DataColumn
                With dcol
                    .ColumnName = "tranguid"
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                dTable.Columns.Add(dcol)

                For Each dr As DataRow In dtList.Rows
                    Dim dtRow As DataRow = dTable.NewRow()
                    dtRow("identities") = dr("identities")
                    dtRow("serial_no") = dr("serial_no")
                    dtRow("identity_no") = dr("identity_no")
                    dtRow("country") = dr("country")
                    dtRow("issued_place") = dr("issued_place")
                    dtRow("issue_date") = dr("issue_date")
                    dtRow("expiry_date") = dr("expiry_date")
                    dtRow("operationtypeid") = dr("operationtypeid")
                    dtRow("operationtype") = dr("operationtype")
                    dtRow("employeeunkid") = dr("employeeunkid")
                    dtRow("idtypeunkid") = dr("idtypeunkid")
                    dtRow("identitytranunkid") = dr("identitytranunkid")
                    dtRow("tranguid") = dr("tranguid")
                    dTable.Rows.Add(dtRow)
                Next

            End If
            'Gajanan [22-Feb-2019] -- End

            If dTable IsNot Nothing Then
                dgvIdentity.DataSource = dTable
                dgvIdentity.DataKeyField = "identitytranunkid"
                dgvIdentity.DataBind()
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    'Public Shared Function b64encode(ByVal StrEncode As String) As String
    '    Try
    '        Dim encodedString As String
    '        encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
    '        Return (encodedString)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    'Gajanan [5-Dec-2019] -- End

    Public Sub delete()
        Try
            popup_DeleteReason.Show() 'SHANI [01 FEB 2015]--popup1.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Private Sub SetPrevious_State()
    '    Try
    '        Session("Prev_State") = drpEmployee.SelectedValue & "|" & _
    '                                cboIdType.SelectedValue & "|" & _
    '                                dgvIdentity.CurrentPageIndex()
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub GetPrevious_State()
    '    Try
    '        Dim StrState() As String = Nothing
    '        StrState = CType(Session("Prev_State"), String).Split("|")

    '        If StrState.Length > 0 Then
    '            drpEmployee.SelectedValue = StrState(0)
    '            cboIdType.SelectedValue = StrState(1)
    '            dgvIdentity.CurrentPageIndex = StrState(2)
    '        End If

    '        Session("IsFrom_AddEdit") = False

    '        Call FillGrid()

    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmEmployeeMaster"
            StrModuleName2 = "mnuCoreSetups"
            clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))

            'Pinkal (24-Aug-2012) -- Start
            'Enhancement : TRA Changes

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            'Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END


            'Gajanan [3-April-2019] -- Start
            objtblPanel.Visible = False
            'Gajanan [3-April-2019] -- End

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            IdentityApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmIdentityInfoList)))
            If IdentityApprovalFlowVal Is Nothing Then
                btnApprovalinfo.Visible = CBool(Session("AllowToApproveRejectEmployeeReferences"))
            Else
                btnApprovalinfo.Visible = False
            End If
            'Gajanan [22-Feb-2019] -- End



            Dim clsuser As New User
            If (Page.IsPostBack = False) Then


                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLanguage()
                'Pinkal (06-May-2014) -- End


                clsuser = CType(Session("clsuser"), User)
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.UserID
                    Me.ViewState("Empunkid") = Session("UserId")
                    'Sohail (30 Mar 2015) -- End

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = Session("UserId")
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END


                    'Anjan (30 May 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'BtnNew.Visible = True
                    'BtnNew.Visible = False
                    BtnNew.Visible = CBool(Session("AddEmployee"))
                    'Anjan (30 May 2012)-End 



                    'S.SANDEEP [ 16 JAN 2014 ] -- START
                    chkShowPending.Visible = CBool(Session("AllowToApproveEmployee"))
                    If Session("ShowPending") IsNot Nothing Then
                        chkShowPending.Checked = True
                    End If
                    'S.SANDEEP [ 16 JAN 2014 ] -- END




                Else
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                    Me.ViewState("Empunkid") = Session("Employeeunkid")
                    'Sohail (30 Mar 2015) -- End
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = -1
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END


                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'BntNew.Visible = False

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'BtnNew.Visible = ConfigParameter._Object._AllowAddIdentity
                    BtnNew.Visible = CBool(Session("AllowAddIdentity"))
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-End 

                    'S.SANDEEP [ 16 JAN 2014 ] -- START
                    chkShowPending.Visible = False
                    'S.SANDEEP [ 16 JAN 2014 ] -- END

                    'Gajanan [22-Feb-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    blnPendingEmployee = CBool(ViewState("blnPendingEmployee"))
                    'Gajanan [22-Feb-2019] -- End

                End If
                Call FillCombo()
                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If blnPendingEmployee Then
                    objtblPanel.Visible = blnPendingEmployee
                End If
                'Gajanan [22-Feb-2019] -- End

                If CInt(drpEmployee.SelectedValue) > 0 Then
                    Call FillGrid()
                End If


                'Pinkal (22-Mar-2012) -- Start
                'Enhancement : TRA Changes
                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
                'SHANI [01 FEB 2015]--END 
                'Pinkal (22-Mar-2012) -- End


                'If Session("IsFrom_AddEdit") = True Then
                '    Call GetPrevious_State()
                '    Session("IsFrom_AddEdit") = False
                'End If

                'S.SANDEEP [ 16 JAN 2014 ] -- START
                If Session("ShowPending") IsNot Nothing Then
                    Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                End If
                'S.SANDEEP [ 16 JAN 2014 ] -- END

                'Nilay (02-Mar-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("identitiesEmpunkid") IsNot Nothing Then
                    If CInt(Session("identitiesEmpunkid")) > 0 Then
                        drpEmployee.SelectedValue = CStr(CInt(Session("identitiesEmpunkid")))


                        'Gajanan [27-May-2019] -- Start              
                        'Call btnSearch_Click(BtnSearch, Nothing)
                        'Gajanan [27-May-2019] -- End

                        Session.Remove("identitiesEmpunkid")
                    End If
                End If
                'Nilay (02-Mar-2015) -- End
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [01 FEB 2015]--END
        End Try
    End Sub

    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'ToolbarEntry1.VisibleSaveButton(False)
            'ToolbarEntry1.VisibleDeleteButton(False)
            'ToolbarEntry1.VisibleCancelButton(False)
            ''ToolbarEntry1.VisibleExitButton(False)
            'SHANI [01 FEB 2015]--END 

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Me.ViewState.Add("blnPendingEmployee", blnPendingEmployee)
            'Gajanan [22-Feb-2019] -- End

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Me.ViewState.Add("mblnisEmployeeApprove", mblnisEmployeeApprove)
            'Gajanan [17-April-2019] -- End
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End


    End Sub

    'Pinkal (22-Mar-2012) -- End

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "
    'Gajanan [27-May-2019] -- Start              
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If IsNothing(Session("Isfromprofile")) = False AndAlso CBool(Session("Isfromprofile")) = True Then
                Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeDetail.aspx", False)
                Session.Remove("Isfromprofile")
            Else
                Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Gajanan [27-May-2019] -- End


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            If CInt(drpEmployee.SelectedValue) <= 0 Then
                msg.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNew.Click
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            If CInt(drpEmployee.SelectedValue) > 0 Then
                Session("identitiesEmpunkid") = drpEmployee.SelectedValue
            End If
            'Nilay (02-Mar-2015) -- End

            Response.Redirect("~\HR\wPgEmpIdentities.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnNew_Click : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            Call ClearObject()
            Call FillGrid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnReset_Click Event : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END 


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDel.Click
    'SHANI [01 FEB 2015]--END
    Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        Try
            If (popup_DeleteReason.Reason.Trim = "") Then 'SHANI [01 FEB 2015]--If (txtreason.Text = "") Then
                msg.DisplayMessage(" Please enter delete reason.", Me)
                Exit Sub
            End If
            Dim dTable As New DataTable
            Dim objEmpIdentity As New clsIdentity_tran
            objEmpIdentity._EmployeeUnkid = CInt(IIf(drpEmployee.SelectedValue = "", 0, drpEmployee.SelectedValue))
            dTable = objEmpIdentity._DataList.Copy
            Dim dRow() As DataRow = dTable.Select("identitytranunkid = '" & Me.ViewState("Unkid").ToString & "'")
            If dRow.Length > 0 Then
                dRow(0)("AUD") = "D"
                dTable.AcceptChanges()
                objEmpIdentity._DataList = dTable

                'Pinkal (28-Dec-2015) -- Start
                'Enhancement - Working on Changes in SS for Employee Master.
                'If objEmpIdentity.InsertUpdateDelete_IdentityTran(Session("UserId")) = False Then
                '    msg.DisplayMessage("Problem in removing the identity.", Me)
                'End If
                If objEmpIdentity.InsertUpdateDelete_IdentityTran(Nothing, CInt(Session("UserId"))) = False Then
                    msg.DisplayMessage("Problem in removing the identity.", Me)
                End If
                'Pinkal (28-Dec-2015) -- End


            End If
            popup_DeleteReason.Dispose() 'SHANI [01 FEB 2015]--popup1.Dispose()
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Protected Sub btnApprovalinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovalinfo.Click
        Try
            Popup_Viewreport._UserId = CInt(Session("UserId"))
            Popup_Viewreport._Priority = 0
            Popup_Viewreport._PrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeIdentities
            Popup_Viewreport._FillType = enScreenName.frmIdentityInfoList
            Popup_Viewreport._FilterString = "EM.employeeunkid = " & CInt(drpEmployee.SelectedValue)
            Popup_Viewreport._FromApprovalScreen = False
            Popup_Viewreport._OprationType = clsEmployeeDataApproval.enOperationType.NONE
            Popup_Viewreport.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnApprovalinfo_Click Event : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Gajanan [22-Feb-2019] -- End


#End Region

#Region " Control's Event(s) "

    Protected Sub dgView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvIdentity.DeleteCommand
        Try
            Me.ViewState.Add("Unkid", dgvIdentity.DataKeys(e.Item.ItemIndex))

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'txtreason.Visible = True
            'SHANI [01 FEB 2015]--END

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'delete()
            Dim objEmp As New clsEmployee_Master : Dim blnIsApproved As Boolean = False
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(e.Item.Cells(14).Text)
            blnIsApproved = objEmp._Isapproved

            If IdentityApprovalFlowVal Is Nothing AndAlso blnIsApproved = True Then

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                Dim eLMode As Integer
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    eLMode = enLogin_Mode.MGR_SELF_SERVICE
                Else
                    eLMode = enLogin_Mode.EMP_SELF_SERVICE
                End If
                'Gajanan [17-April-2019] -- End


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If objApprovalData.IsApproverPresent(enScreenName.frmIdentityInfoList, CStr(Session("Database_Name")), _
                                                      CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                      CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeIdentities), _
                                                      CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(e.Item.Cells(14).Text), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    msg.DisplayMessage(objApprovalData._Message, Me)
                    Exit Sub
                End If
                'Gajanan [17-April-2019] -- End


                Dim item = dgvIdentity.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(13).Text = e.Item.Cells(13).Text And x.Cells(12).Text.Trim <> "&nbsp;").FirstOrDefault()
                If item IsNot Nothing Then
                    msg.DisplayMessage("Sorry, you cannot edit this information. Reason, this particular information is already in approval process.", Me)
                    Exit Sub
                Else
                    objIdentities._EmployeeUnkid = CInt(e.Item.Cells(14).Text)
                    Dim mdTran As DataTable = objIdentities._DataList
                    Dim dtEIdRow() As DataRow = mdTran.Select("identitytranunkid = '" & e.Item.Cells(13).Text & "'")
                    If dtEIdRow.Length > 0 Then
                        Dim dtApprTran As New DataTable
                        objAIdInfoTran._Employeeunkid = CInt(e.Item.Cells(14).Text)
                        dtApprTran = objAIdInfoTran._DataTable
                        Dim dtIdRow As DataRow = dtApprTran.NewRow()
                        dtIdRow.Item("tranguid") = Guid.NewGuid.ToString()
                        dtIdRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
                        dtIdRow.Item("mappingunkid") = 0
                        dtIdRow.Item("approvalremark") = ""
                        dtIdRow.Item("isfinal") = False
                        dtIdRow.Item("statusunkid") = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                        dtIdRow.Item("loginemployeeunkid") = 0
                        dtIdRow.Item("isvoid") = False
                        dtIdRow.Item("voidreason") = ""
                        dtIdRow.Item("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                        dtIdRow.Item("audittype") = enAuditType.ADD
                        dtIdRow.Item("audituserunkid") = Session("UserId")
                        dtIdRow.Item("ip") = CStr(Session("IP_ADD"))
                        dtIdRow.Item("host") = CStr(Session("HOST_NAME"))
                        dtIdRow.Item("form_name") = mstrModuleName1
                        dtIdRow.Item("isweb") = True
                        dtIdRow.Item("isprocessed") = False
                        dtIdRow.Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.DELETED
                        dtIdRow.Item("identitytranunkid") = dtEIdRow(0).Item("identitytranunkid")
                        dtIdRow.Item("employeeunkid") = CInt(e.Item.Cells(14).Text)
                        dtIdRow.Item("idtypeunkid") = dtEIdRow(0).Item("idtypeunkid")
                        dtIdRow.Item("identity_no") = dtEIdRow(0).Item("identity_no")
                        dtIdRow.Item("countryunkid") = dtEIdRow(0).Item("countryunkid")
                        dtIdRow.Item("serial_no") = dtEIdRow(0).Item("serial_no")
                        dtIdRow.Item("issued_place") = dtEIdRow(0).Item("issued_place")
                        dtIdRow.Item("dl_class") = dtEIdRow(0).Item("dl_class")
                        dtIdRow.Item("issue_date") = dtEIdRow(0).Item("issue_date")
                        dtIdRow.Item("expiry_date") = dtEIdRow(0).Item("expiry_date")
                        dtIdRow.Item("isdefault") = dtEIdRow(0).Item("isdefault")
                        dtIdRow.Item("GUID") = Guid.NewGuid().ToString
                        dtIdRow.Item("AUD") = "A"
                        dtApprTran.Rows.Add(dtIdRow)

                        'Dim objScanAttachment As New clsScan_Attach_Documents
                        'objScanAttachment.GetList(Session("Document_Path").ToString, "List", "", CInt(e.Item.Cells(14).Text))
                        'mdtAttachment = New DataView(objScanAttachment._Datatable, "transactionunkid = '" & dtEIdRow(0).Item("identitytranunkid").ToString & "' AND scanattachrefid = '" & CInt(enScanAttactRefId.IDENTITYS) & "'", "", DataViewRowState.CurrentRows).ToTable()  'objScanAttachment.GetQulificationAttachment(CInt(e.Item.Cells(14).Text), enImg_Email_RefId.Employee_Module, CInt(dtEIdRow(0).Item("identitytranunkid")), Session("Document_Path").ToString(), 1, "", False)
                        'objScanAttachment = Nothing

                        'If mdtAttachment IsNot Nothing Then
                        '    Dim strPGuid As String = String.Empty
                        '    strPGuid = dtApprTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("identitytranunkid") = CInt(e.Item.Cells(13).Text)).Select(Function(x) x.Field(Of String)("GUID")).FirstOrDefault()
                        '    For Each row As DataRow In mdtAttachment.Rows
                        '        row("PGUID") = strPGuid
                        '        row("AUD") = "D"
                        '    Next
                        '    mdtAttachment.AcceptChanges()
                        'End If

                        If objAIdInfoTran.InsertUpdateDelete_IdentityTran(dtApprTran, Nothing, CInt(Session("UserId")), CInt(Session("CompanyUnkId").ToString()), mdtAttachment) Then

                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                     CStr(Session("UserAccessModeSetting")), _
                                                     CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                     CInt(enUserPriviledge.AllowToApproveRejectEmployeeIdentities), _
                                                     enScreenName.frmIdentityInfoList, CStr(Session("EmployeeAsOnDate")), _
                                                     CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                     Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.DELETED, , e.Item.Cells(14).Text, , , _
                                                     "employeeunkid= " & e.Item.Cells(14).Text & " ", dtApprTran, False, clsEmployeeAddress_approval_tran.enAddressType.NONE, "")
                            'Gajanan [17-April-2019] -- End

                            msg.DisplayMessage("Identity Successfully deleted.", Me)
                        Else
                            msg.DisplayMessage("Problem in updating Identity.", Me)
                        End If
                    End If
                End If
            Else
                delete()
            End If
            'Gajanan [22-Feb-2019] -- End
            FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvIdentity.ItemCommand
        Try
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If dgvIdentity.Items.Count > 0 Then
                If dgvIdentity.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Drawing.Color.LightCoral).Count() > 0 Then
                    Dim item = dgvIdentity.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Drawing.Color.LightCoral).FirstOrDefault()
                    If item IsNot Nothing Then item.BackColor = Drawing.Color.White
                End If
            End If
            'Gajanan [22-Feb-2019] -- End

            Session("IsFrom_AddEdit") = True
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            Session("identitiesEmpunkid") = CInt(drpEmployee.SelectedValue)
            'Nilay (02-Mar-2015) -- End

            ' Call SetPrevious_State()

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If e.CommandName = "Select" Then
            '    Response.Redirect("~\HR\wPgEmpIdentities.aspx?ProcessId=" & b64encode(CStr(Val(dgvIdentity.DataKeys(e.Item.ItemIndex)))))
            If e.CommandName = "Select" Then
                If IdentityApprovalFlowVal Is Nothing Then
                    Dim item = dgvIdentity.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(13).Text = e.Item.Cells(13).Text And x.Cells(12).Text.Trim <> "&nbsp;").FirstOrDefault()
                    If item IsNot Nothing Then
                        msg.DisplayMessage("Sorry, you cannot edit this information. Reason, this particular information is already in approval process.", Me)
                        Exit Sub
                    Else
                        Response.Redirect("~\HR\wPgEmpIdentities.aspx?ProcessId=" & b64encode(CStr(Val(dgvIdentity.DataKeys(e.Item.ItemIndex)))), False)
                    End If
                Else
                    Response.Redirect("~\HR\wPgEmpIdentities.aspx?ProcessId=" & b64encode(CStr(Val(dgvIdentity.DataKeys(e.Item.ItemIndex)))), False)
                End If
            ElseIf e.CommandName.ToUpper() = "VIEW" Then
                Dim item = dgvIdentity.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(13).Text = e.Item.Cells(13).Text And x.Cells(12).Text.Trim = "&nbsp;").FirstOrDefault()
                If item IsNot Nothing Then
                    item.BackColor = Drawing.Color.LightCoral
                    item.ForeColor = Drawing.Color.Black
                End If
                'Gajanan [22-Feb-2019] -- End
            End If


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgvIdentity.PageIndexChanged
        Try
            dgvIdentity.CurrentPageIndex = e.NewPageIndex
            FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvIdentity.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End
            If (e.Item.ItemIndex >= 0) Then

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    'e.Item.Cells(0).Visible = True : e.Item.Cells(1).Visible = True

                    'Anjan (30 May 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'dgvIdentity.Columns(0).Visible = True : dgvIdentity.Columns(1).Visible = False 'Anjan (02 Mar 2012)


                    'Pinkal (22-Nov-2012) -- Start
                    'Enhancement : TRA Changes

                    'dgvIdentity.Columns(0).Visible = True : dgvIdentity.Columns(1).Visible = Session("DeleteEmployee")
                    dgvIdentity.Columns(0).Visible = CBool(Session("EditEmployee"))
                    dgvIdentity.Columns(1).Visible = CBool(Session("DeleteEmployee"))

                    'Pinkal (22-Nov-2012) -- End
                    'Anjan (30 May 2012)-End 


                    'S.SANDEEP [ 16 JAN 2014 ] -- START
                    If Session("ShowPending") IsNot Nothing Then
                        dgvIdentity.Columns(1).Visible = False
                    End If
                    'S.SANDEEP [ 16 JAN 2014 ] -- END

                Else
                    'e.Item.Cells(0).Visible = False : e.Item.Cells(1).Visible = False

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'dgvIdentity.Columns(0).Visible = True : dgvIdentity.Columns(1).Visible = ConfigParameter._Object._AllowDeleteIdentity


                    'Pinkal (22-Nov-2012) -- Start
                    'Enhancement : TRA Changes

                    'dgvIdentity.Columns(0).Visible = True : dgvIdentity.Columns(1).Visible = Session("AllowDeleteIdentity")
                    dgvIdentity.Columns(0).Visible = CBool(Session("AllowEditIdentity"))
                    dgvIdentity.Columns(1).Visible = CBool(Session("AllowDeleteIdentity"))

                    'Pinkal (22-Nov-2012) -- End


                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                End If

                'Nilay (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'If e.Item.Cells(7).Text <> "&nbsp;" AndAlso e.Item.Cells(7).Text.ToString.Trim.Length > 0 Then
                '    e.Item.Cells(7).Text = CStr(CDate(e.Item.Cells(7).Text).Date)
                'End If
                'If e.Item.Cells(8).Text <> "&nbsp;" AndAlso e.Item.Cells(8).Text.ToString.Trim.Length > 0 Then
                '    e.Item.Cells(8).Text = CStr(CDate(e.Item.Cells(8).Text).Date)
                'End If
                If e.Item.Cells(7).Text <> "&nbsp;" AndAlso e.Item.Cells(7).Text.ToString.Trim.Length > 0 Then
                    e.Item.Cells(7).Text = CDate(e.Item.Cells(7).Text).Date.ToShortDateString
                End If
                If e.Item.Cells(8).Text <> "&nbsp;" AndAlso e.Item.Cells(8).Text.ToString.Trim.Length > 0 Then
                    e.Item.Cells(8).Text = CDate(e.Item.Cells(8).Text).Date.ToShortDateString
                End If
                'Nilay (16-Apr-2016) -- End

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If IdentityApprovalFlowVal Is Nothing Then
                    If CInt(e.Item.Cells(10).Text) > 0 Then
                        e.Item.BackColor = Drawing.Color.PowderBlue
                        e.Item.ForeColor = Drawing.Color.Black

                        objtblPanel.Visible = True
                        blnPendingEmployee = True

                        CType(e.Item.Cells(1).FindControl("ImgSelect"), LinkButton).Visible = False
                        If CType(CType(e.Item.Cells(1).FindControl("hfoprationtypeid"), HiddenField).Value, clsEmployeeDataApproval.enOperationType) = clsEmployeeDataApproval.enOperationType.EDITED Then
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = True
                        Else
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = False
                        End If
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                    End If
                End If
                'Gajanan [22-Feb-2019] -- End


            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP [ 16 JAN 2014 ] -- START
    Protected Sub chkShowPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowPending.CheckedChanged
        Try
            Session("ShowPending") = chkShowPending.Checked
            Dim dsCombos As New DataSet
            Dim objEmp As New clsEmployee_Master
            If chkShowPending.Checked = True Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"), , , False)
                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                  CStr(Session("UserAccessModeSetting")), False, _
                                                  CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                'Shani(24-Aug-2015) -- End
                Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dtTab
                    .DataBind()
                    Try
                        .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                    Catch ex As Exception
                        .SelectedValue = CStr(0)
                    End Try
                End With
                BtnNew.Visible = False
            Else
                Session("ShowPending") = Nothing
                BtnNew.Visible = True
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
                    dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "EmployeeList", True)
                    'Shani(24-Aug-2015) -- End
                    With drpEmployee
                        .DataValueField = "employeeunkid"
                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        '.DataTextField = "employeename"
                        .DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        .DataSource = dsCombos.Tables("Employee")
                        .DataBind()
                        Try
                            .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                        Catch ex As Exception
                            .SelectedValue = CStr(0)
                        End Try
                    End With
                Else
                    Dim objglobalassess = New GlobalAccess
                    objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                    With drpEmployee
                        .DataSource = objglobalassess.ListOfEmployee
                        .DataTextField = "loginname"
                        .DataValueField = "employeeunkid"
                        .DataBind()
                        Try
                            .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                        Catch ex As Exception
                            .SelectedValue = CStr(0)
                        End Try
                    End With
                End If
            End If
            dgvIdentity.DataSource = Nothing : dgvIdentity.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [ 16 JAN 2014 ] -- END

#End Region

#Region "ToolBar Event"
    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try

    '        'Pinkal (22-Nov-2012) -- Start
    '        'Enhancement : TRA Changes

    '        If Session("LoginBy") = Global.User.en_loginby.User Then
    '            If CBool(Session("AddEmployee")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                Exit Sub
    '            End If
    '        Else
    '            If CBool(Session("AllowAddMembership")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)

    '                Exit Sub
    '            End If
    '        End If

    '        'Pinkal (22-Nov-2012) -- End

    '        Response.Redirect("~\HR\wPgEmpIdentities.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END 
#End Region

    Protected Sub drpEmployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpEmployee.DataBound
        Try         'Hemant (13 Aug 2020)
            If drpEmployee.Items.Count > 0 Then
                For Each lstItem As ListItem In drpEmployee.Items
                    lstItem.Attributes.Add("title", lstItem.Text)
                Next
                drpEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
            End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbIDInformation", Me.Title)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbIDInformation", Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbIDInformation", Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END 


            Me.lblIdType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIdType.ID, Me.lblIdType.Text)

            Me.lblPendingData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPendingData.ID, Me.lblPendingData.Text)
            Me.lblParentData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblParentData.ID, Me.lblParentData.Text)

            dgvIdentity.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvIdentity.Columns(2).FooterText, dgvIdentity.Columns(2).HeaderText)
            dgvIdentity.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvIdentity.Columns(3).FooterText, dgvIdentity.Columns(3).HeaderText)
            dgvIdentity.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvIdentity.Columns(4).FooterText, dgvIdentity.Columns(4).HeaderText)
            dgvIdentity.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvIdentity.Columns(5).FooterText, dgvIdentity.Columns(5).HeaderText)
            dgvIdentity.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvIdentity.Columns(6).FooterText, dgvIdentity.Columns(6).HeaderText)
            dgvIdentity.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvIdentity.Columns(7).FooterText, dgvIdentity.Columns(7).HeaderText)
            dgvIdentity.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvIdentity.Columns(8).FooterText, dgvIdentity.Columns(8).HeaderText)

            Me.lblPendingData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPendingData.ID, Me.lblPendingData.Text)
            Me.lblParentData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblParentData.ID, Me.lblParentData.Text)


            'Language.setLanguage(mstrModuleName1)
            Me.LblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.BtnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.BtnNew.ID, Me.BtnNew.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- End



End Class
