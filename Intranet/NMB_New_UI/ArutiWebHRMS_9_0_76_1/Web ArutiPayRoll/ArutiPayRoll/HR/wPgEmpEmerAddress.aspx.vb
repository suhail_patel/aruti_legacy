﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.IO

#End Region

Partial Class wPgEmpEmerAddress
    Inherits Basepage

#Region " Private Variable(s) "

    Private msg As New CommonCodes
    Private clsuser As New User
    Private objEmployee As New clsEmployee_Master
    'S.SANDEEP [ 11 MAR 2014 ] -- START
    'Dim strExpression As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
    'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
    Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
    'S.SANDEEP [ 11 MAR 2014 ] -- END


    Private ReadOnly mstrModuleName As String = "frmEmployeeMaster"
    Private ReadOnly mstrModuleName1 As String = "frmEmployeeList"

    'Gajanan [18-Mar-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Dim Arr() As String
    Private objA_EmergencyAddress As New clsEmployee_emergency_address_approval
    Private EmergencyAddressApprovalFlowVal As String
    'Gajanan [18-Mar-2019] -- End

    'S.SANDEEP |26-APR-2019| -- START
    Private mdtAttachement As DataTable = Nothing
    Private mdtFullAttachment As DataTable = Nothing
    Private mblnAttachmentPopup As Boolean = False
    Private objScanAttachment As New clsScan_Attach_Documents
    Private mintDeleteIndex As Integer = 0
    Private mintAddressTypeId As Integer = 0
    'S.SANDEEP |26-APR-2019| -- END



    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [17-April-2019] -- End

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objMaster As New clsMasterData
        Dim objCity As New clscity_master
        Dim objState As New clsstate_master
        Dim objZipCode As New clszipcode_master
        Try
            dsCombos = objMaster.getCountryList("List", True)
            RemoveHandler cboECountry.SelectedIndexChanged, AddressOf cboECountry_SelectedIndexChanged
            With cboECountry
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboECountry.SelectedIndexChanged, AddressOf cboECountry_SelectedIndexChanged

            RemoveHandler cboECountry2.SelectedIndexChanged, AddressOf cboECountry2_SelectedIndexChanged
            With cboECountry2
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboECountry2.SelectedIndexChanged, AddressOf cboECountry2_SelectedIndexChanged

            RemoveHandler cboECountry3.SelectedIndexChanged, AddressOf cboECountry3_SelectedIndexChanged
            With cboECountry3
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboECountry3.SelectedIndexChanged, AddressOf cboECountry3_SelectedIndexChanged
            dsCombos = objState.GetList("List", True, True)
            RemoveHandler cboEState.SelectedIndexChanged, AddressOf cboEState_SelectedIndexChanged
            With cboEState
                .DataValueField = "stateunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboEState.SelectedIndexChanged, AddressOf cboEState_SelectedIndexChanged

            RemoveHandler cboEState2.SelectedIndexChanged, AddressOf cboEState2_SelectedIndexChanged
            With cboEState2
                .DataValueField = "stateunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboEState2.SelectedIndexChanged, AddressOf cboEState2_SelectedIndexChanged

            RemoveHandler cboEState3.SelectedIndexChanged, AddressOf cboEState3_SelectedIndexChanged
            With cboEState3
                .DataValueField = "stateunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboEState3.SelectedIndexChanged, AddressOf cboEState3_SelectedIndexChanged
            dsCombos = objCity.GetList("List", True, True)
            RemoveHandler cboECity.SelectedIndexChanged, AddressOf cboECity_SelectedIndexChanged
            With cboECity
                .DataValueField = "cityunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboECity.SelectedIndexChanged, AddressOf cboECity_SelectedIndexChanged

            RemoveHandler cboECity2.SelectedIndexChanged, AddressOf cboECity2_SelectedIndexChanged
            With cboECity2
                .DataValueField = "cityunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboECity2.SelectedIndexChanged, AddressOf cboECity2_SelectedIndexChanged

            RemoveHandler cboECity3.SelectedIndexChanged, AddressOf cboECity3_SelectedIndexChanged
            With cboECity3
                .DataValueField = "cityunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboECity3.SelectedIndexChanged, AddressOf cboECity3_SelectedIndexChanged

            'S.SANDEEP |26-APR-2019| -- START
            dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With
            'S.SANDEEP |26-APR-2019| -- END

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetInfo()
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = Session("Employeeunkid")


            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = IIf((Session("LoginBy") = Global.User.en_loginby.User), Session("EmpUnkid"), Session("Employeeunkid"))
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            'Pinkal (28-Dec-2015) -- End

            'Shani(24-Aug-2015) -- End


            txtEmployee.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname

            '************************* EMERGENCY INFO *****************************'


            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.




            'txtEAddress.Text = objEmployee._Emer_Con_Address
            'txtEAltNo.Text = objEmployee._Emer_Con_Alternateno
            'txtEEMail.Text = objEmployee._Emer_Con_Email
            'txtEEState.Text = objEmployee._Emer_Con_Estate
            'txtEFax.Text = objEmployee._Emer_Con_Fax
            'txtEFirstname.Text = objEmployee._Emer_Con_Firstname
            'txtELastname.Text = objEmployee._Emer_Con_Lastname
            'txtEMobile.Text = objEmployee._Emer_Con_Mobile
            'txtEPlotNo.Text = objEmployee._Emer_Con_Plotno
            ''Shani [ 24 DEC 2014 ] -- START
            ''Implement Close Button Code on Each Page.
            ''cboECity.SelectedValue = objEmployee._Emer_Con_Post_Townunkid
            ''cboEZipcode.SelectedValue = objEmployee._Emer_Con_Postcodeunkid
            ''txtERegion.Text = objEmployee._Emer_Con_Provicnce
            ''txtEStreet.Text = objEmployee._Emer_Con_Road
            ''cboEState.SelectedValue = objEmployee._Emer_Con_State
            ''txtETelNo.Text = objEmployee._Emer_Con_Tel_No
            ''cboECountry.SelectedValue = objEmployee._Emer_Con_Countryunkid
            'txtERegion.Text = objEmployee._Emer_Con_Provicnce
            'txtEStreet.Text = objEmployee._Emer_Con_Road
            'txtETelNo.Text = objEmployee._Emer_Con_Tel_No
            'cboECountry.SelectedValue = CStr(objEmployee._Emer_Con_Countryunkid)
            'Call cboECountry_SelectedIndexChanged(cboECountry, Nothing)
            'cboEState.SelectedValue = CStr(objEmployee._Emer_Con_State)
            'Call cboEState_SelectedIndexChanged(cboEState, Nothing)
            'cboECity.SelectedValue = CStr(objEmployee._Emer_Con_Post_Townunkid)
            'Call cboECity_SelectedIndexChanged(cboECity, Nothing)
            'cboEZipcode.SelectedValue = CStr(objEmployee._Emer_Con_Postcodeunkid)
            ''Shani [ 24 DEC 2014 ] -- END

            If EmergencyAddressApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                If objApprovalData.IsApproverPresent(enScreenName.frmEmergencyAddressList, CStr(Session("Database_Name")), _
                                                      CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                      CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmergencyAddress), _
                                                      CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(Session("Employeeunkid")), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    msg.DisplayMessage(objApprovalData._Message, Me)
                    Exit Sub
                End If

                objA_EmergencyAddress._Adddresstype = CInt(clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS1)
                objA_EmergencyAddress._Employeeunkid = CInt(Session("Employeeunkid"))

                If objA_EmergencyAddress._Tranguid.Length > 0 Then
                    GbEmergencyContact1.Enabled = False
                    lblEAddressapproval1.Visible = True
                Else
                    lblEAddressapproval1.Visible = False
                End If

                If objA_EmergencyAddress._Tranguid.Length > 0 AndAlso objA_EmergencyAddress._Operationtypeid = clsEmployeeDataApproval.enOperationType.ADDED Then
                    txtEAddress.Text = objA_EmergencyAddress._Address
                    txtEAltNo.Text = objA_EmergencyAddress._Alternateno
                    txtEEMail.Text = objA_EmergencyAddress._Email
                    txtEEState.Text = objA_EmergencyAddress._Estate
                    txtEFax.Text = objA_EmergencyAddress._Fax
                    txtEFirstname.Text = objA_EmergencyAddress._Firstname
                    txtELastname.Text = objA_EmergencyAddress._Lastname
                    txtEMobile.Text = objA_EmergencyAddress._Mobile
                    txtEPlotNo.Text = objA_EmergencyAddress._Plotno
                    txtERegion.Text = objA_EmergencyAddress._Provicnce
                    txtEStreet.Text = objA_EmergencyAddress._Road
                    txtETelNo.Text = objA_EmergencyAddress._Tel_No
                    cboECountry.SelectedValue = CStr(objA_EmergencyAddress._Countryunkid)
                    Call cboECountry_SelectedIndexChanged(cboECountry, Nothing)
                    cboEState.SelectedValue = CStr(objA_EmergencyAddress._Stateunkid)
                    Call cboEState_SelectedIndexChanged(cboEState, Nothing)
                    cboECity.SelectedValue = CStr(objA_EmergencyAddress._Townunkid)
                    Call cboECity_SelectedIndexChanged(cboECity, Nothing)
                    cboEZipcode.SelectedValue = CStr(objA_EmergencyAddress._Postcodeunkid)
                Else
                    txtEAddress.Text = objEmployee._Emer_Con_Address
                    txtEAltNo.Text = objEmployee._Emer_Con_Alternateno
                    txtEEMail.Text = objEmployee._Emer_Con_Email
                    txtEEState.Text = objEmployee._Emer_Con_Estate
                    txtEFax.Text = objEmployee._Emer_Con_Fax
                    txtEFirstname.Text = objEmployee._Emer_Con_Firstname
                    txtELastname.Text = objEmployee._Emer_Con_Lastname
                    txtEMobile.Text = objEmployee._Emer_Con_Mobile
                    txtEPlotNo.Text = objEmployee._Emer_Con_Plotno
                    txtERegion.Text = objEmployee._Emer_Con_Provicnce
                    txtEStreet.Text = objEmployee._Emer_Con_Road
                    txtETelNo.Text = objEmployee._Emer_Con_Tel_No
                    cboECountry.SelectedValue = CStr(objEmployee._Emer_Con_Countryunkid)
                    Call cboECountry_SelectedIndexChanged(cboECountry, Nothing)
                    cboEState.SelectedValue = CStr(objEmployee._Emer_Con_State)
                    Call cboEState_SelectedIndexChanged(cboEState, Nothing)
                    cboECity.SelectedValue = CStr(objEmployee._Emer_Con_Post_Townunkid)
                    Call cboECity_SelectedIndexChanged(cboECity, Nothing)
                    cboEZipcode.SelectedValue = CStr(objEmployee._Emer_Con_Postcodeunkid)
                End If
            Else
                txtEAddress.Text = objEmployee._Emer_Con_Address
                txtEAltNo.Text = objEmployee._Emer_Con_Alternateno
                txtEEMail.Text = objEmployee._Emer_Con_Email
                txtEEState.Text = objEmployee._Emer_Con_Estate
                txtEFax.Text = objEmployee._Emer_Con_Fax
                txtEFirstname.Text = objEmployee._Emer_Con_Firstname
                txtELastname.Text = objEmployee._Emer_Con_Lastname
                txtEMobile.Text = objEmployee._Emer_Con_Mobile
                txtEPlotNo.Text = objEmployee._Emer_Con_Plotno
                txtERegion.Text = objEmployee._Emer_Con_Provicnce
                txtEStreet.Text = objEmployee._Emer_Con_Road
                txtETelNo.Text = objEmployee._Emer_Con_Tel_No
                cboECountry.SelectedValue = CStr(objEmployee._Emer_Con_Countryunkid)
                Call cboECountry_SelectedIndexChanged(cboECountry, Nothing)
                cboEState.SelectedValue = CStr(objEmployee._Emer_Con_State)
                Call cboEState_SelectedIndexChanged(cboEState, Nothing)
                cboECity.SelectedValue = CStr(objEmployee._Emer_Con_Post_Townunkid)
                Call cboECity_SelectedIndexChanged(cboECity, Nothing)
                cboEZipcode.SelectedValue = CStr(objEmployee._Emer_Con_Postcodeunkid)
            End If

            'Gajanan [18-Mar-2019] -- End



            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.




            'txtEAddress2.Text = objEmployee._Emer_Con_Address2
            'txtEAltNo2.Text = objEmployee._Emer_Con_Alternateno2
            'txtEEMail2.Text = objEmployee._Emer_Con_Email2
            'txtEEState2.Text = objEmployee._Emer_Con_Estate2
            'txtEFax2.Text = objEmployee._Emer_Con_Fax2
            'txtEFirstname2.Text = objEmployee._Emer_Con_Firstname2
            'txtELastname2.Text = objEmployee._Emer_Con_Lastname2
            'txtEMobile2.Text = objEmployee._Emer_Con_Mobile2
            'txtEPlotNo2.Text = objEmployee._Emer_Con_Plotno2
            ''Shani [ 24 DEC 2014 ] -- START
            ''Implement Close Button Code on Each Page.
            ''cboECity2.SelectedValue = objEmployee._Emer_Con_Post_Townunkid2
            ''cboEZipcode2.SelectedValue = objEmployee._Emer_Con_Postcodeunkid2
            ''txtERegion2.Text = objEmployee._Emer_Con_Provicnce2
            ''txtEStreet2.Text = objEmployee._Emer_Con_Road2
            ''cboEState2.SelectedValue = objEmployee._Emer_Con_State2
            ''txtETelNo2.Text = objEmployee._Emer_Con_Tel_No2
            ''cboECountry2.SelectedValue = objEmployee._Emer_Con_Countryunkid2
            'txtERegion2.Text = objEmployee._Emer_Con_Provicnce2
            'txtEStreet2.Text = objEmployee._Emer_Con_Road2
            'txtETelNo2.Text = objEmployee._Emer_Con_Tel_No2
            'cboECountry2.SelectedValue = CStr(objEmployee._Emer_Con_Countryunkid2)
            'Call cboECountry2_SelectedIndexChanged(cboECountry2, Nothing)
            'cboEState2.SelectedValue = CStr(objEmployee._Emer_Con_State2)
            'Call cboEState2_SelectedIndexChanged(cboEState2, Nothing)
            'cboECity2.SelectedValue = CStr(objEmployee._Emer_Con_Post_Townunkid2)
            'Call cboECity2_SelectedIndexChanged(cboECity2, Nothing)
            'cboEZipcode2.SelectedValue = CStr(objEmployee._Emer_Con_Postcodeunkid2)
            ''Shani [ 24 DEC 2014 ] -- END


            If EmergencyAddressApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then
                objA_EmergencyAddress._Adddresstype = CInt(clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS2)
                objA_EmergencyAddress._Employeeunkid = CInt(Session("Employeeunkid"))

                If objA_EmergencyAddress._Tranguid.Length > 0 Then
                    GbEmergencyContact2.Enabled = False
                    lblEAddressapproval2.Visible = True
                Else
                    lblEAddressapproval2.Visible = False
                End If

                If objA_EmergencyAddress._Tranguid.Length > 0 AndAlso objA_EmergencyAddress._Operationtypeid = clsEmployeeDataApproval.enOperationType.ADDED Then
                    txtEAddress2.Text = objA_EmergencyAddress._Address
                    txtEAltNo2.Text = objA_EmergencyAddress._Alternateno
                    txtEEMail2.Text = objA_EmergencyAddress._Email
                    txtEEState2.Text = objA_EmergencyAddress._Estate
                    txtEFax2.Text = objA_EmergencyAddress._Fax
                    txtEFirstname2.Text = objA_EmergencyAddress._Firstname
                    txtELastname2.Text = objA_EmergencyAddress._Lastname
                    txtEMobile2.Text = objA_EmergencyAddress._Mobile
                    txtEPlotNo2.Text = objA_EmergencyAddress._Plotno
                    txtERegion2.Text = objA_EmergencyAddress._Provicnce
                    txtEStreet2.Text = objA_EmergencyAddress._Road
                    txtETelNo2.Text = objA_EmergencyAddress._Tel_No
                    cboECountry2.SelectedValue = CStr(objA_EmergencyAddress._Countryunkid)
                    Call cboECountry2_SelectedIndexChanged(cboECountry2, Nothing)
                    cboEState2.SelectedValue = CStr(objA_EmergencyAddress._Stateunkid)
                    Call cboEState2_SelectedIndexChanged(cboEState2, Nothing)
                    cboECity2.SelectedValue = CStr(objA_EmergencyAddress._Townunkid)
                    Call cboECity2_SelectedIndexChanged(cboECity2, Nothing)
                    cboEZipcode2.SelectedValue = CStr(objA_EmergencyAddress._Postcodeunkid)
                Else
                    txtEAddress2.Text = objEmployee._Emer_Con_Address2
                    txtEAltNo2.Text = objEmployee._Emer_Con_Alternateno2
                    txtEEMail2.Text = objEmployee._Emer_Con_Email2
                    txtEEState2.Text = objEmployee._Emer_Con_Estate2
                    txtEFax2.Text = objEmployee._Emer_Con_Fax2
                    txtEFirstname2.Text = objEmployee._Emer_Con_Firstname2
                    txtELastname2.Text = objEmployee._Emer_Con_Lastname2
                    txtEMobile2.Text = objEmployee._Emer_Con_Mobile2
                    txtEPlotNo2.Text = objEmployee._Emer_Con_Plotno2
                    txtERegion2.Text = objEmployee._Emer_Con_Provicnce2
                    txtEStreet2.Text = objEmployee._Emer_Con_Road2
                    txtETelNo2.Text = objEmployee._Emer_Con_Tel_No2
                    cboECountry2.SelectedValue = CStr(objEmployee._Emer_Con_Countryunkid2)
                    Call cboECountry2_SelectedIndexChanged(cboECountry2, Nothing)
                    cboEState2.SelectedValue = CStr(objEmployee._Emer_Con_State2)
                    Call cboEState2_SelectedIndexChanged(cboEState2, Nothing)
                    cboECity2.SelectedValue = CStr(objEmployee._Emer_Con_Post_Townunkid2)
                    Call cboECity2_SelectedIndexChanged(cboECity2, Nothing)
                    cboEZipcode2.SelectedValue = CStr(objEmployee._Emer_Con_Postcodeunkid2)

                End If
            Else
                txtEAddress2.Text = objEmployee._Emer_Con_Address2
                txtEAltNo2.Text = objEmployee._Emer_Con_Alternateno2
                txtEEMail2.Text = objEmployee._Emer_Con_Email2
                txtEEState2.Text = objEmployee._Emer_Con_Estate2
                txtEFax2.Text = objEmployee._Emer_Con_Fax2
                txtEFirstname2.Text = objEmployee._Emer_Con_Firstname2
                txtELastname2.Text = objEmployee._Emer_Con_Lastname2
                txtEMobile2.Text = objEmployee._Emer_Con_Mobile2
                txtEPlotNo2.Text = objEmployee._Emer_Con_Plotno2
                txtERegion2.Text = objEmployee._Emer_Con_Provicnce2
                txtEStreet2.Text = objEmployee._Emer_Con_Road2
                txtETelNo2.Text = objEmployee._Emer_Con_Tel_No2
                cboECountry2.SelectedValue = CStr(objEmployee._Emer_Con_Countryunkid2)
                Call cboECountry2_SelectedIndexChanged(cboECountry2, Nothing)
                cboEState2.SelectedValue = CStr(objEmployee._Emer_Con_State2)
                Call cboEState2_SelectedIndexChanged(cboEState2, Nothing)
                cboECity2.SelectedValue = CStr(objEmployee._Emer_Con_Post_Townunkid2)
                Call cboECity2_SelectedIndexChanged(cboECity2, Nothing)
                cboEZipcode2.SelectedValue = CStr(objEmployee._Emer_Con_Postcodeunkid2)
            End If

            'Gajanan [18-Mar-2019] -- End


            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'txtEAddress3.Text = objEmployee._Emer_Con_Address3
            'txtEAltNo3.Text = objEmployee._Emer_Con_Alternateno3
            'txtEEMail3.Text = objEmployee._Emer_Con_Email3
            'txtEEState3.Text = objEmployee._Emer_Con_Estate3
            'txtEFax3.Text = objEmployee._Emer_Con_Fax3
            'txtEFirstname3.Text = objEmployee._Emer_Con_Firstname3
            'txtELastname3.Text = objEmployee._Emer_Con_Lastname3
            'txtEMobile3.Text = objEmployee._Emer_Con_Mobile3
            'txtEPlotNo3.Text = objEmployee._Emer_Con_Plotno3
            ''Shani [ 24 DEC 2014 ] -- START
            ''Implement Close Button Code on Each Page.
            ''cboECity3.SelectedValue = objEmployee._Emer_Con_Post_Townunkid3
            ''cboEZipcode3.SelectedValue = objEmployee._Emer_Con_Postcodeunkid3
            ''txtERegion3.Text = objEmployee._Emer_Con_Provicnce3
            ''txtEStreet3.Text = objEmployee._Emer_Con_Road3
            ''cboEState3.SelectedValue = objEmployee._Emer_Con_State3
            ''txtETelNo3.Text = objEmployee._Emer_Con_Tel_No3
            ''cboECountry3.SelectedValue = objEmployee._Emer_Con_Countryunkid3
            'txtERegion3.Text = objEmployee._Emer_Con_Provicnce3
            'txtEStreet3.Text = objEmployee._Emer_Con_Road3
            'txtETelNo3.Text = objEmployee._Emer_Con_Tel_No3
            'cboECountry3.SelectedValue = CStr(objEmployee._Emer_Con_Countryunkid3)
            'Call cboECountry3_SelectedIndexChanged(cboECountry3, Nothing)
            'cboEState3.SelectedValue = CStr(objEmployee._Emer_Con_State3)
            'Call cboEState3_SelectedIndexChanged(cboEState3, Nothing)
            'cboECity3.SelectedValue = CStr(objEmployee._Emer_Con_Post_Townunkid3)
            'Call cboECity3_SelectedIndexChanged(cboECity3, Nothing)
            'cboEZipcode3.SelectedValue = CStr(objEmployee._Emer_Con_Postcodeunkid3)
            ''Shani [ 24 DEC 2014 ] -- END

            If EmergencyAddressApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then
                objA_EmergencyAddress._Adddresstype = CInt(clsEmployee_emergency_address_approval.enEmeAddressType.EMERGENCY_ADDRESS3)
                objA_EmergencyAddress._Employeeunkid = CInt(Session("Employeeunkid"))

                If objA_EmergencyAddress._Tranguid.Length > 0 Then
                    GbEmergencyContact3.Enabled = False
                    lblEAddressapproval3.Visible = True
                Else
                    lblEAddressapproval3.Visible = False
                End If

                If objA_EmergencyAddress._Tranguid.Length > 0 AndAlso objA_EmergencyAddress._Operationtypeid = clsEmployeeDataApproval.enOperationType.ADDED Then
                    txtEAddress3.Text = objA_EmergencyAddress._Address
                    txtEAltNo3.Text = objA_EmergencyAddress._Alternateno
                    txtEEMail3.Text = objA_EmergencyAddress._Email
                    txtEEState3.Text = objA_EmergencyAddress._Estate
                    txtEFax3.Text = objA_EmergencyAddress._Fax
                    txtEFirstname3.Text = objA_EmergencyAddress._Firstname
                    txtELastname3.Text = objA_EmergencyAddress._Lastname
                    txtEMobile3.Text = objA_EmergencyAddress._Mobile
                    txtEPlotNo3.Text = objA_EmergencyAddress._Plotno
                    txtERegion3.Text = objA_EmergencyAddress._Provicnce
                    txtEStreet3.Text = objA_EmergencyAddress._Road
                    txtETelNo3.Text = objA_EmergencyAddress._Tel_No
                    cboECountry3.SelectedValue = CStr(objA_EmergencyAddress._Countryunkid)
                    Call cboECountry3_SelectedIndexChanged(cboECountry3, Nothing)
                    cboEState3.SelectedValue = CStr(objA_EmergencyAddress._Stateunkid)
                    Call cboEState3_SelectedIndexChanged(cboEState3, Nothing)
                    cboECity3.SelectedValue = CStr(objA_EmergencyAddress._Townunkid)
                    Call cboECity3_SelectedIndexChanged(cboECity3, Nothing)
                    cboEZipcode3.SelectedValue = CStr(objA_EmergencyAddress._Postcodeunkid)
                Else
                    txtEAddress3.Text = objEmployee._Emer_Con_Address3
                    txtEAltNo3.Text = objEmployee._Emer_Con_Alternateno3
                    txtEEMail3.Text = objEmployee._Emer_Con_Email3
                    txtEEState3.Text = objEmployee._Emer_Con_Estate3
                    txtEFax3.Text = objEmployee._Emer_Con_Fax3
                    txtEFirstname3.Text = objEmployee._Emer_Con_Firstname3
                    txtELastname3.Text = objEmployee._Emer_Con_Lastname3
                    txtEMobile3.Text = objEmployee._Emer_Con_Mobile3
                    txtEPlotNo3.Text = objEmployee._Emer_Con_Plotno3
                    txtERegion3.Text = objEmployee._Emer_Con_Provicnce3
                    txtEStreet3.Text = objEmployee._Emer_Con_Road3
                    txtETelNo3.Text = objEmployee._Emer_Con_Tel_No3
                    cboECountry3.SelectedValue = CStr(objEmployee._Emer_Con_Countryunkid3)
                    Call cboECountry3_SelectedIndexChanged(cboECountry3, Nothing)
                    cboEState3.SelectedValue = CStr(objEmployee._Emer_Con_State3)
                    Call cboEState3_SelectedIndexChanged(cboEState3, Nothing)
                    cboECity3.SelectedValue = CStr(objEmployee._Emer_Con_Post_Townunkid3)
                    Call cboECity3_SelectedIndexChanged(cboECity3, Nothing)
                    cboEZipcode3.SelectedValue = CStr(objEmployee._Emer_Con_Postcodeunkid3)

                End If
            Else
                txtEAddress3.Text = objEmployee._Emer_Con_Address3
                txtEAltNo3.Text = objEmployee._Emer_Con_Alternateno3
                txtEEMail3.Text = objEmployee._Emer_Con_Email3
                txtEEState3.Text = objEmployee._Emer_Con_Estate3
                txtEFax3.Text = objEmployee._Emer_Con_Fax3
                txtEFirstname3.Text = objEmployee._Emer_Con_Firstname3
                txtELastname3.Text = objEmployee._Emer_Con_Lastname3
                txtEMobile3.Text = objEmployee._Emer_Con_Mobile3
                txtEPlotNo3.Text = objEmployee._Emer_Con_Plotno3
                txtERegion3.Text = objEmployee._Emer_Con_Provicnce3
                txtEStreet3.Text = objEmployee._Emer_Con_Road3
                txtETelNo3.Text = objEmployee._Emer_Con_Tel_No3
                cboECountry3.SelectedValue = CStr(objEmployee._Emer_Con_Countryunkid3)
                Call cboECountry3_SelectedIndexChanged(cboECountry3, Nothing)
                cboEState3.SelectedValue = CStr(objEmployee._Emer_Con_State3)
                Call cboEState3_SelectedIndexChanged(cboEState3, Nothing)
                cboECity3.SelectedValue = CStr(objEmployee._Emer_Con_Post_Townunkid3)
                Call cboECity3_SelectedIndexChanged(cboECity3, Nothing)
                cboEZipcode3.SelectedValue = CStr(objEmployee._Emer_Con_Postcodeunkid3)
            End If





            '************************* EMERGENCY INFO *****************************'
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Change in Employee Profile Pages due to wrong assignment of session("employeeunkid").
            'objEmployee._Employeeunkid = Session("Employeeunkid")

            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            'objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = IIf((Session("LoginBy") = Global.User.en_loginby.User), Session("EmpUnkid"), Session("Employeeunkid"))
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            'Pinkal (28-Dec-2015) -- End


            'Shani(24-Aug-2015) -- End

            '************************* EMERGENCY INFO *****************************'
            objEmployee._Emer_Con_Address = txtEAddress.Text
            objEmployee._Emer_Con_Alternateno = txtEAltNo.Text
            objEmployee._Emer_Con_Email = txtEEMail.Text
            objEmployee._Emer_Con_Estate = txtEEState.Text
            objEmployee._Emer_Con_Fax = txtEFax.Text
            objEmployee._Emer_Con_Firstname = txtEFirstname.Text
            objEmployee._Emer_Con_Lastname = txtELastname.Text
            objEmployee._Emer_Con_Mobile = txtEMobile.Text
            objEmployee._Emer_Con_Plotno = txtEPlotNo.Text
            objEmployee._Emer_Con_Post_Townunkid = CInt(cboECity.SelectedValue)
            objEmployee._Emer_Con_Postcodeunkid = CInt(IIf(cboEZipcode.SelectedValue = "", 0, cboEZipcode.SelectedValue))
            objEmployee._Emer_Con_Provicnce = txtERegion.Text
            objEmployee._Emer_Con_Road = txtEStreet.Text
            objEmployee._Emer_Con_State = CInt(cboEState.SelectedValue)
            objEmployee._Emer_Con_Tel_No = txtETelNo.Text
            objEmployee._Emer_Con_Countryunkid = CInt(cboECountry.SelectedValue)

            objEmployee._Emer_Con_Address2 = txtEAddress2.Text
            objEmployee._Emer_Con_Alternateno2 = txtEAltNo2.Text
            objEmployee._Emer_Con_Email2 = txtEEMail2.Text
            objEmployee._Emer_Con_Estate2 = txtEEState2.Text
            objEmployee._Emer_Con_Fax2 = txtEFax2.Text
            objEmployee._Emer_Con_Firstname2 = txtEFirstname2.Text
            objEmployee._Emer_Con_Lastname2 = txtELastname2.Text
            objEmployee._Emer_Con_Mobile2 = txtEMobile2.Text
            objEmployee._Emer_Con_Plotno2 = txtEPlotNo2.Text
            objEmployee._Emer_Con_Post_Townunkid2 = CInt(cboECity2.SelectedValue)
            objEmployee._Emer_Con_Postcodeunkid2 = CInt(IIf(cboEZipcode2.SelectedValue = "", 0, cboEZipcode2.SelectedValue))
            objEmployee._Emer_Con_Provicnce2 = txtERegion2.Text
            objEmployee._Emer_Con_Road2 = txtEStreet2.Text
            objEmployee._Emer_Con_State2 = CInt(cboEState2.SelectedValue)
            objEmployee._Emer_Con_Tel_No2 = txtETelNo2.Text
            objEmployee._Emer_Con_Countryunkid2 = CInt(cboECountry2.SelectedValue)

            objEmployee._Emer_Con_Address3 = txtEAddress3.Text
            objEmployee._Emer_Con_Alternateno3 = txtEAltNo3.Text
            objEmployee._Emer_Con_Email3 = txtEEMail3.Text
            objEmployee._Emer_Con_Estate3 = txtEEState3.Text
            objEmployee._Emer_Con_Fax3 = txtEFax3.Text
            objEmployee._Emer_Con_Firstname3 = txtEFirstname3.Text
            objEmployee._Emer_Con_Lastname3 = txtELastname3.Text
            objEmployee._Emer_Con_Mobile3 = txtEMobile3.Text
            objEmployee._Emer_Con_Plotno3 = txtEPlotNo3.Text
            objEmployee._Emer_Con_Post_Townunkid3 = CInt(cboECity3.SelectedValue)
            objEmployee._Emer_Con_Postcodeunkid3 = CInt(IIf(cboEZipcode3.SelectedValue = "", 0, cboEZipcode3.SelectedValue))
            objEmployee._Emer_Con_Provicnce3 = txtERegion3.Text
            objEmployee._Emer_Con_Road3 = txtEStreet3.Text
            objEmployee._Emer_Con_State3 = CInt(cboEState3.SelectedValue)
            objEmployee._Emer_Con_Tel_No3 = txtETelNo3.Text
            objEmployee._Emer_Con_Countryunkid3 = CInt(cboECountry3.SelectedValue)
            'S.SANDEEP [ 19 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
            'S.SANDEEP [ 19 MAR 2013 ] -- END
            '************************* EMERGENCY INFO *****************************'


            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            Blank_ModuleName()
            objEmployee._WebFormName = "frmEmployeeMaster"
            StrModuleName2 = "mnuCoreSetups"
            objEmployee._WebClientIP = CStr(Session("IP_ADD"))
            objEmployee._WebHostName = CStr(Session("HOST_NAME"))
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                objEmployee._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If

            'S.SANDEEP [ 04 JULY 2012 ] -- END

            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim eLMode As Integer
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                eLMode = enLogin_Mode.MGR_SELF_SERVICE
            Else
                eLMode = enLogin_Mode.EMP_SELF_SERVICE
            End If
            'Gajanan [18-Mar-2019] -- End


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''Sohail (23 Apr 2012) -- Start
            ''TRA - ENHANCEMENT
            'If objEmployee.Update(, , , Session("UserId"), Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), Session("Fin_year"), Session("AllowToApproveEarningDeduction")) = False Then
            '    'If objEmployee.Update = False Then
            '    'Sohail (23 Apr 2012) -- End
            '    msg.DisplayError(ex, Me)
            'End If
            Dim blnFlag As Boolean = False

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

            'blnFlag = objEmployee.Update(CStr(Session("Database_Name")), _
            '                             CInt(Session("Fin_year")), _
            '                             CInt(Session("CompanyUnkId")), _
            '                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                             CStr(Session("IsArutiDemo")), _
            '                             CInt(Session("Total_Active_Employee_ForAllCompany")), _
            '                             CInt(Session("NoOfEmployees")), _
            '                             CInt(Session("UserId")), False, _
            '                             CStr(Session("UserAccessModeSetting")), _
            '                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
            '                             CBool(Session("IsIncludeInactiveEmp")), _
            '                             CBool(Session("AllowToApproveEarningDeduction")), _
            '                             ConfigParameter._Object._CurrentDateAndTime, _
            '                             Nothing, Nothing, _
            '                             False, "", "", "", _
            '                             CBool(Session("IsImgInDataBase")))

            'blnFlag = objEmployee.Update(CStr(Session("Database_Name")), _
            '                             CInt(Session("Fin_year")), _
            '                             CInt(Session("CompanyUnkId")), _
            '                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                             CStr(Session("IsArutiDemo")), _
            '                             CInt(Session("Total_Active_Employee_ForAllCompany")), _
            '                             CInt(Session("NoOfEmployees")), _
            '                             CInt(Session("UserId")), False, _
            '                             CStr(Session("UserAccessModeSetting")), _
            '                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
            '                             CBool(Session("IsIncludeInactiveEmp")), _
            '                             CBool(Session("AllowToApproveEarningDeduction")), _
            '                             ConfigParameter._Object._CurrentDateAndTime, _
            '                            CBool(Session("CreateADUserFromEmpMst")), CBool(Session("UserMustchangePwdOnNextLogon")), Nothing, Nothing, _
            '                             False, "", "", "", _
            '                             CBool(Session("IsImgInDataBase")))

            ''Pinkal (18-Aug-2018) -- End



            'S.SANDEEP |26-APR-2019| -- START
            Call SaveDocumentIIS()
            'S.SANDEEP |26-APR-2019| -- END


            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            blnFlag = objEmployee.Update(CStr(Session("Database_Name")), _
                                         CInt(Session("Fin_year")), _
                                         CInt(Session("CompanyUnkId")), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                         CStr(Session("IsArutiDemo")), _
                                         CInt(Session("Total_Active_Employee_ForAllCompany")), _
                                         CInt(Session("NoOfEmployees")), _
                                         CInt(Session("UserId")), False, _
                                         CStr(Session("UserAccessModeSetting")), _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                         CBool(Session("IsIncludeInactiveEmp")), _
                                         CBool(Session("AllowToApproveEarningDeduction")), _
                                                        ConfigParameter._Object._CurrentDateAndTime.Date, CBool(Session("CreateADUserFromEmpMst")), _
                                                        CBool(Session("UserMustchangePwdOnNextLogon")), Nothing, Nothing, False, "", _
                                                        CStr(Session("EmployeeAsOnDate")), , _
                                         CBool(Session("IsImgInDataBase")), , Nothing, , , mdtFullAttachment, Session("HOST_NAME").ToString(), Session("IP_ADD").ToString(), Session("UserName").ToString(), CType(eLMode, enLogin_Mode), False, False, mstrModuleName) 'S.SANDEEP |26-APR-2019| -- START {Nothing  -> mdtFullAttachment} -- END

            'Gajanan [18-Mar-2019] -- End



            If blnFlag = False Then
                msg.DisplayMessage(objEmployee._Message, Me)
            End If
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'S.SANDEEP |26-APR-2019| -- START
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'Dim dtRow() As DataRow = mdtFullAttachment.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            Dim dtRow() As DataRow = mdtFullAttachment.Select("filename = '" & f.Name & "' AND AUD <> 'D' AND scanattachrefid = " & mintAddressTypeId & " ")
            'Gajanan [17-April-2019] -- End
            If dtRow.Length <= 0 Then
                dRow = mdtFullAttachment.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
                dRow("employeeunkid") = CInt(CInt(Session("Employeeunkid")))
                dRow("filename") = f.Name
                dRow("scanattachrefid") = mintAddressTypeId
                dRow("modulerefid") = enImg_Email_RefId.Employee_Module
                dRow("userunkid") = Session("UserId")
                dRow("AUD") = "A"
                dRow("GUID") = Guid.NewGuid.ToString
                dRow("orgfilepath") = strfullpath
                dRow("valuename") = ""
                dRow("transactionunkid") = -1
                dRow("attached_date") = Today.Date
                dRow("filepath") = strfullpath
                dRow("filename") = f.Name
                dRow("filesize") = f.Length / 1024
                dRow("attached_date") = Today.Date
                dRow("orgfilepath") = strfullpath
                dRow("form_name") = mstrModuleName
                dRow("userunkid") = CInt(Session("userid"))
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
                Dim blnIsInApproval As Boolean = False : objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
                If EmergencyAddressApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then
                    blnIsInApproval = True
                End If
                dRow("isinapproval") = blnIsInApproval
            Else
                msg.DisplayMessage("Selected information is already present for particular employee.", Me)
                Exit Sub
            End If
            mdtFullAttachment.Rows.Add(dRow)
            mdtAttachement = New DataView(mdtFullAttachment, "scanattachrefid = '" & mintAddressTypeId & "'", "", DataViewRowState.CurrentRows).ToTable()
            Call FillAttachment()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            If mdtAttachement Is Nothing Then Exit Sub
            dtView = New DataView(mdtAttachement, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgv_Attchment.AutoGenerateColumns = False
            dgv_Attchment.DataSource = dtView
            dgv_Attchment.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SaveDocumentIIS()
        Try
            If mdtFullAttachment IsNot Nothing Then
                Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                For Each dRow As DataRow In mdtFullAttachment.Rows
                    Dim iScanRefId As Integer = CInt(dRow.Item("scanattachrefid"))
                    Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = iScanRefId) Select (p.Item("Name").ToString)).FirstOrDefault

                    If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                        Dim strFileName As String = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                        If File.Exists(CStr(dRow("orgfilepath"))) Then
                            Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                            If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                            End If
                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.

                            'File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                            File.Copy(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                            'Gajanan [17-April-2019] -- End
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strPath

                            dRow.AcceptChanges()
                        Else
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 999, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFilepath As String = dRow("filepath").ToString
                        Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                        If strFilepath.Contains(strArutiSelfService) Then
                            strFilepath = strFilepath.Replace(strArutiSelfService, "")
                            If Strings.Left(strFilepath, 1) <> "/" Then
                                strFilepath = "~/" & strFilepath
                            Else
                                strFilepath = "~" & strFilepath
                            End If

                            If File.Exists(Server.MapPath(strFilepath)) Then
                                File.Delete(Server.MapPath(strFilepath))
                            Else
                                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 999, "Configuration Path does not Exist."), Me)
                                Exit Sub
                            End If
                        Else
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 999, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |26-APR-2019| -- END

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If
            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            EmergencyAddressApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmEmergencyAddressList)))
            'Gajanan [18-Mar-2019] -- End

            If (Page.IsPostBack = False) Then


                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLanguage()
                'Pinkal (06-May-2014) -- End


                clsuser = CType(Session("clsuser"), User)
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    'Aruti.Data.User._Object._Userunkid = Session("UserId") 'Sohail (23 Apr 2012)


                    'Anjan (30 May 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'btnsave.Visible = False
                    btnsave.Visible = CBool(Session("EditEmployee"))

                    'Anjan (30 May 2012)-End 

                    'S.SANDEEP [ 31 DEC 2013 ] -- START
                    If Session("ShowPending") IsNot Nothing Then
                        btnsave.Visible = Not CBool(Session("ShowPending"))
                    End If
                    'S.SANDEEP [ 31 DEC 2013 ] -- END

                Else
                    'Aruti.Data.User._Object._Userunkid = -1 'Sohail (23 Apr 2012)
                    lblEmployee.Visible = False : txtEmployee.Visible = False
                    'Hemant (01 Jan 2021) -- Start
                    'Enhancement #OLD-238 - Improved Employee Profile page
                    pnl_Employee.Visible = False
                    'Hemant (01 Jan 2021) -- End
                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'btnsave.Visible = ConfigParameter._Object._AllowEditEmergencyAddress
                    btnsave.Visible = CBool(Session("AllowEditEmergencyAddress"))
                    'Sohail (23 Apr 2012) -- End
                    'Anjan (02 Mar 2012)-End 
                End If

                Call FillCombo()
                Call SetInfo()
                'S.SANDEEP |26-APR-2019| -- START
            Else
                mdtAttachement = CType(Me.ViewState("FieldAttachement"), DataTable)
                mblnAttachmentPopup = CBool(Me.ViewState("mblnAttachmentPopup"))
                mdtFullAttachment = CType(Me.ViewState("mdtFullAttachment"), DataTable)
                mintAddressTypeId = CInt(Me.ViewState("mintAddressTypeId"))
                mintDeleteIndex = CInt(Me.ViewState("mintDeleteIndex"))
                'S.SANDEEP |26-APR-2019| -- END

            End If

            'S.SANDEEP |26-APR-2019| -- START
            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If
            'S.SANDEEP |26-APR-2019| -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'S.SANDEEP |26-APR-2019| -- START
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("FieldAttachement") = mdtAttachement
            Me.ViewState("mblnAttachmentPopup") = mblnAttachmentPopup
            Me.ViewState("mdtFullAttachment") = mdtFullAttachment
            Me.ViewState("mintAddressTypeId") = mintAddressTypeId
            Me.ViewState("mintDeleteIndex") = mintDeleteIndex
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |26-APR-2019| -- END

#End Region

#Region " Controls Events "

    Protected Sub cboECountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECountry.SelectedIndexChanged
        Try
            If CInt(cboECountry.SelectedValue) >= 0 Then
                Dim objState As New clsstate_master
                Dim dsList As New DataSet
                dsList = objState.GetList("List", True, True, CInt(cboECountry.SelectedValue))
                With cboEState
                    .DataValueField = "stateunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboECountry2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECountry2.SelectedIndexChanged
        Try
            If CInt(cboECountry2.SelectedValue) >= 0 Then
                Dim objState As New clsstate_master
                Dim dsList As New DataSet
                dsList = objState.GetList("List", True, True, CInt(cboECountry2.SelectedValue))
                With cboEState2
                    .DataValueField = "stateunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboECountry3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECountry3.SelectedIndexChanged
        Try
            If CInt(cboECountry3.SelectedValue) >= 0 Then
                Dim objState As New clsstate_master
                Dim dsList As New DataSet
                dsList = objState.GetList("List", True, True, CInt(cboECountry3.SelectedValue))
                With cboEState3
                    .DataValueField = "stateunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboEState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEState.SelectedIndexChanged
        Try
            If CInt(cboEState.SelectedValue) >= 0 Then
                Dim objCity As New clscity_master
                Dim dsList As DataSet = objCity.GetList("List", True, True, CInt(cboEState.SelectedValue))
                With cboECity
                    .DataValueField = "cityunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboEState2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEState2.SelectedIndexChanged
        Try
            If CInt(cboEState2.SelectedValue) >= 0 Then
                Dim objCity As New clscity_master
                Dim dsList As DataSet = objCity.GetList("List", True, True, CInt(cboEState2.SelectedValue))
                With cboECity2
                    .DataValueField = "cityunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboEState3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEState3.SelectedIndexChanged
        Try
            If CInt(cboEState3.SelectedValue) >= 0 Then
                Dim objCity As New clscity_master
                Dim dsList As DataSet = objCity.GetList("List", True, True, CInt(cboEState3.SelectedValue))
                With cboECity3
                    .DataValueField = "cityunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboECity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECity.SelectedIndexChanged
        Try
            If CInt(cboECity.SelectedValue) >= 0 Then
                Dim objZip As New clszipcode_master
                Dim dsList As DataSet = objZip.GetList("List", True, True, CInt(cboECity.SelectedValue))
                With cboEZipcode
                    .DataValueField = "zipcodeunkid"
                    .DataTextField = "zipcode_no"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboECity2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECity2.SelectedIndexChanged
        Try
            If CInt(cboECity2.SelectedValue) >= 0 Then
                Dim objZip As New clszipcode_master
                Dim dsList As DataSet = objZip.GetList("List", True, True, CInt(cboECity2.SelectedValue))
                With cboEZipcode2
                    .DataValueField = "zipcodeunkid"
                    .DataTextField = "zipcode_no"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboECity3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECity3.SelectedIndexChanged
        Try
            If CInt(cboECity3.SelectedValue) >= 0 Then
                Dim objZip As New clszipcode_master
                Dim dsList As DataSet = objZip.GetList("List", True, True, CInt(cboECity3.SelectedValue))
                With cboEZipcode3
                    .DataValueField = "zipcodeunkid"
                    .DataTextField = "zipcode_no"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkCopyEAddress1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopyEAddress2.Click
        Try
            txtEAddress2.Text = txtEAddress.Text
            txtEAltNo2.Text = txtEAltNo.Text
            txtEEMail2.Text = txtEEMail.Text
            txtEEState2.Text = txtEEState.Text
            txtEFax2.Text = txtEFax.Text
            txtEFirstname2.Text = txtEFirstname.Text
            txtELastname2.Text = txtELastname.Text
            txtEMobile2.Text = txtEMobile.Text
            txtEPlotNo2.Text = txtEPlotNo.Text
            cboECity2.SelectedValue = cboECity.SelectedValue
            cboEZipcode2.SelectedValue = cboEZipcode.SelectedValue
            txtERegion2.Text = txtERegion.Text
            txtEStreet2.Text = txtEStreet.Text
            cboEState2.SelectedValue = cboEState.SelectedValue
            txtETelNo2.Text = txtETelNo.Text
            cboECountry2.SelectedValue = cboECountry.SelectedValue
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkCopyEAddress2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopyEAddress3.Click
        Try
            txtEAddress3.Text = txtEAddress.Text
            txtEAltNo3.Text = txtEAltNo.Text
            txtEEMail3.Text = txtEEMail.Text
            txtEEState3.Text = txtEEState.Text
            txtEFax3.Text = txtEFax.Text
            txtEFirstname3.Text = txtEFirstname.Text
            txtELastname3.Text = txtELastname.Text
            txtEMobile3.Text = txtEMobile.Text
            txtEPlotNo3.Text = txtEPlotNo.Text
            cboECity3.SelectedValue = cboECity.SelectedValue
            cboEZipcode3.SelectedValue = cboEZipcode.SelectedValue
            txtERegion3.Text = txtERegion.Text
            txtEStreet3.Text = txtEStreet.Text
            cboEState3.SelectedValue = cboEState.SelectedValue
            txtETelNo3.Text = txtETelNo.Text
            cboECountry3.SelectedValue = cboECountry.SelectedValue
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            Dim objMaster As New clsMasterData
            If txtEEMail.Text.Trim.Length > 0 Then
                If Expression.IsMatch(txtEEMail.Text.Trim) = False Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 30, "Invalid Emergency Email.Please Enter Valid Email"), Me)
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                End If

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Change in Employee Profile Pages due to wrong assignment of session("employeeunkid").
                If objMaster.IsEmailPresent("hremployee_master", txtEEMail.Text.Trim, CInt(Session("Employeeunkid")), "employeeunkid") = True Then
                    'If objMaster.IsEmailPresent("hremployee_master", txtEEMail.Text.Trim, CInt(IIf((CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User), Session("EmpUnkid"), Session("Employeeunkid"))), "employeeunkid") = True Then
                    'Shani(24-Aug-2015) -- End

                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 62, "Sorry, this Emergency email address is already used by some employee. please provide another Emergency email address."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                End If
            End If
            If txtEEMail2.Text.Trim.Length > 0 Then
                If Expression.IsMatch(txtEEMail2.Text.Trim) = False Then
                    msg.DisplayMessage("Invalid Emergency2 Email.Please Enter Valid Email", Me)
                    Exit Sub
                End If

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Change in Employee Profile Pages due to wrong assignment of session("employeeunkid").
                If objMaster.IsEmailPresent("hremployee_master", txtEEMail2.Text.Trim, CInt(Session("Employeeunkid")), "employeeunkid") = True Then
                    'If objMaster.IsEmailPresent("hremployee_master", txtEEMail2.Text.Trim, CInt(IIf((CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User), Session("EmpUnkid"), Session("Employeeunkid"))), "employeeunkid") = True Then
                    'Shani(24-Aug-2015) -- End

                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 63, "Sorry, this Emergency2 email address is already used by some employee. please provide another Emergency2 email address."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                End If
            End If

            If txtEEMail3.Text.Trim.Length > 0 Then
                If Expression.IsMatch(txtEEMail3.Text.Trim) = False Then
                    msg.DisplayMessage("Invalid Emergency3 Email.Please Enter Valid Email", Me)
                    Exit Sub
                End If

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Change in Employee Profile Pages due to wrong assignment of session("employeeunkid").
                If objMaster.IsEmailPresent("hremployee_master", txtEEMail3.Text.Trim, CInt(Session("Employeeunkid")), "employeeunkid") = True Then
                    'If objMaster.IsEmailPresent("hremployee_master", txtEEMail3.Text.Trim, CInt(IIf((CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User), Session("EmpUnkid"), Session("Employeeunkid"))), "employeeunkid") = True Then
                    'Shani(24-Aug-2015) -- End

                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 64, "Sorry, this Emergency3 email address is already used by some employee. please provide another Emergency3 email address."), Me)
                    'Sohail (23 Mar 2019) -- End
                    'Pinkal (06-May-2014) -- End
                    Exit Sub
                End If
            End If

            Call SetValue()

            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.

            'Gajanan [1-NOV-2019] -- Start    
            'Enhancement:Worked On NMB ESS Comment  
            'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx", False)
            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 502, "Saved Successfully"), Me, Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx")
            'Gajanan [1-NOV-2019] -- End

            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Change for Approv Employee Query Strings Session.
            'Response.Redirect(Session("rootpath") & "HR/wPgEmployeeProfile.aspx")
            If Session("EmpiQueryString") IsNot Nothing AndAlso Session("EmpiQueryString").ToString().Trim.Length > 0 Then
                Dim strEmpQuery As String = Session("EmpiQueryString").ToString.Trim
                'Hemant (01 Jan 2021) -- Start
                'Enhancement #OLD-238 - Improved Employee Profile page
                'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx?" & strEmpQuery)
                Response.Redirect(Session("ReturnURL").ToString & strEmpQuery, False)
                Session("ReturnURL") = Nothing
                'Hemant (01 Jan 2021) -- End               
            Else
                'Hemant (01 Jan 2021) -- Start
                'Enhancement #OLD-238 - Improved Employee Profile page
                'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx")
                Response.Redirect(Session("ReturnURL").ToString, False)
                Session("ReturnURL") = Nothing
                'Hemant (01 Jan 2021) -- End
            End If
            'Shani(24-Aug-2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\HR\wPgEmployeeProfile.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END 

    'S.SANDEEP |26-APR-2019| -- START
    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                'mdtAttachement = CType(Me.ViewState("FieldAttachement"), DataTable)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    popup_ScanAttchment.Show()
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                AddDocumentAttachment(f, f.FullName)
                popup_ScanAttchment.Show()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_AttachementYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonNo_Click
        Try
            If mintDeleteIndex > 0 Then mintDeleteIndex = 0
            popup_ScanAttchment.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_AttachementYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonYes_Click
        Try
            If mintDeleteIndex >= 0 Then
                mdtFullAttachment.Rows(mintDeleteIndex)("AUD") = "D"
                mdtFullAttachment.AcceptChanges()
                mintDeleteIndex = 0
                mdtAttachement = New DataView(mdtFullAttachment, "scanattachrefid = '" & mintAddressTypeId & "'", "", DataViewRowState.CurrentRows).ToTable()
                Call FillAttachment()
                popup_ScanAttchment.Show()
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |26-APR-2019| -- END


    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgv_Attchment.Items.Count <= 0 Then
                msg.DisplayMessage("No Files to download.", Me)
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & lblScanHeader.Text.Replace(" ", "") + ".zip", mdtAttachement, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

#End Region

    'S.SANDEEP |26-APR-2019| -- START
#Region " Link Event(s) "

    Protected Sub lnkScanAttachEmerContact1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkScanAttachEmerContact1.Click, lnkScanAttachEmerContact2.Click, lnkScanAttachEmerContact3.Click
        Try
            mintAddressTypeId = 0
            Select Case CType(sender, LinkButton).ID
                Case lnkScanAttachEmerContact1.ID
                    If txtEFirstname.Text.Trim.Length <= 0 AndAlso txtELastname.Text.Trim.Length <= 0 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 159, "Sorry, In order to proceed with document attachment, Please provide emergency contact1 firstname and lastname continue."), Me)
                        Exit Sub
                    End If
                Case lnkScanAttachEmerContact2.ID
                    If txtEFirstname2.Text.Trim.Length <= 0 AndAlso txtELastname2.Text.Trim.Length <= 0 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 160, "Sorry, In order to proceed with document attachment, Please provide emergency contact2 firstname and lastname continue."), Me)
                        Exit Sub
                    End If
                Case lnkScanAttachEmerContact3.ID
                    If txtEFirstname3.Text.Trim.Length <= 0 AndAlso txtELastname3.Text.Trim.Length <= 0 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 161, "Sorry, In order to proceed with document attachment, Please provide emergency contact3 firstname and lastname continue."), Me)
                        Exit Sub
                    End If
            End Select

            objScanAttachment.GetList(Session("Document_Path").ToString(), "List", "", CInt(Session("Employeeunkid")))
            If mdtFullAttachment Is Nothing Then
                mdtFullAttachment = objScanAttachment._Datatable
            End If

            If mdtFullAttachment IsNot Nothing Then
                Select Case CType(sender, LinkButton).ID
                    Case lnkScanAttachEmerContact1.ID
                        lblScanHeader.Text = objlblCaption.Text & " (" & lnEmergencyContact1.Text & ")"
                        mintAddressTypeId = CInt(enScanAttactRefId.EMERGENCY_CONTACT1)
                    Case lnkScanAttachEmerContact2.ID
                        lblScanHeader.Text = objlblCaption.Text & " (" & lnEmergencyContact2.Text & ")"
                        mintAddressTypeId = CInt(enScanAttactRefId.EMERGENCY_CONTACT2)
                    Case lnkScanAttachEmerContact3.ID
                        lblScanHeader.Text = objlblCaption.Text & " (" & lnEmergencyContact3.Text & ")"
                        mintAddressTypeId = CInt(enScanAttactRefId.EMERGENCY_CONTACT3)
                End Select
                mdtAttachement = New DataView(mdtFullAttachment, "scanattachrefid = '" & mintAddressTypeId & "'", "", DataViewRowState.CurrentRows).ToTable()
            End If
            mblnAttachmentPopup = True
            Call FillAttachment()
            popup_ScanAttchment.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " GridViewEvent "

    Protected Sub dgv_Attchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgv_Attchment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow = Nothing

                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                'If CInt(e.Item.Cells(3).Text) > 0 Then
                '    xrow = mdtFullAttachment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(3).Text) & "")
                'Else
                '    xrow = mdtFullAttachment.Select("GUID = '" & e.Item.Cells(2).Text & "'")
                'End If
                'If e.CommandName = "Delete" Then
                '    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                '        popup_AttachementYesNo.Message = "Are you sure you want to delete this attachment?"
                '        mintDeleteIndex = mdtFullAttachment.Rows.IndexOf(xrow(0))
                '        popup_AttachementYesNo.Show()
                '    End If
                'End If

                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtFullAttachment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtFullAttachment.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhGUID", False, True)).Text & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        popup_AttachementYesNo.Message = "Are you sure you want to delete this attachment?"
                        mintDeleteIndex = mdtFullAttachment.Rows.IndexOf(xrow(0))
                        popup_AttachementYesNo.Show()
                    End If
                ElseIf e.CommandName = "Download" Then
                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        Dim xPath As String = ""

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("orgfilepath").ToString
                        End If

                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                msg.DisplayMessage("File does not Exist...", Me)
                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & xrow(0)("filename").ToString() & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
            End If
            'S.SANDEEP |16-MAY-2019| -- END
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'S.SANDEEP |26-APR-2019| -- END


    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "tabpEmergencyContact", Me.Title)
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"tabpEmergencyContact", Me.Closebutton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "tabpEmergencyContact", Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END 

            Me.lblEmgAltNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgAltNo.ID, Me.lblEmgAltNo.Text)
            Me.lblEmgPlotNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgPlotNo.ID, Me.lblEmgPlotNo.Text)
            Me.lblEmgProvince.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgProvince.ID, Me.lblEmgProvince.Text)
            Me.lblEmgFax.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgFax.ID, Me.lblEmgFax.Text)
            Me.lblEmgMobile.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgMobile.ID, Me.lblEmgMobile.Text)
            Me.lblEmgTelNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgTelNo.ID, Me.lblEmgTelNo.Text)
            Me.lblEmgRoad.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgRoad.ID, Me.lblEmgRoad.Text)
            Me.lblEmgEstate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgEstate.ID, Me.lblEmgEstate.Text)
            Me.lblEmgEmail.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgEmail.ID, Me.lblEmgEmail.Text)
            Me.lblEmgPostCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgPostCode.ID, Me.lblEmgPostCode.Text)
            Me.lblEmgAddress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgAddress.ID, Me.lblEmgAddress.Text)
            Me.lblEmgState.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgState.ID, Me.lblEmgState.Text)
            Me.lblEmgPostTown.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgPostTown.ID, Me.lblEmgPostTown.Text)
            Me.lblEmgPostCountry.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgPostCountry.ID, Me.lblEmgPostCountry.Text)
            Me.lnEmergencyContact1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnEmergencyContact1.ID, Me.lnEmergencyContact1.Text)
            Me.lblEmgLastName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgLastName.ID, Me.lblEmgLastName.Text)
            Me.lblEmgFirstName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgFirstName.ID, Me.lblEmgFirstName.Text)

            Me.lnkCopyEAddress2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkCopyEAddress2.ID, Me.lnkCopyEAddress2.Text)
            Me.lnEmergencyContact2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnEmergencyContact2.ID, Me.lnEmergencyContact2.Text)
            Me.lblEmgEmail2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgEmail2.ID, Me.lblEmgEmail2.Text)
            Me.lblEmgFax2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgFax2.ID, Me.lblEmgFax2.Text)
            Me.lblEmgTelNo2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgTelNo2.ID, Me.lblEmgTelNo2.Text)
            Me.lblEmgAltNo2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgAltNo2.ID, Me.lblEmgAltNo2.Text)
            Me.lblEmgFirstName2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgFirstName2.ID, Me.lblEmgFirstName2.Text)
            Me.lblEmgMobile2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgMobile2.ID, Me.lblEmgMobile2.Text)
            Me.lblEmgLastName2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgLastName2.ID, Me.lblEmgLastName2.Text)
            Me.lblEmgPlotNo2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgPlotNo2.ID, Me.lblEmgPlotNo2.Text)
            Me.lblEmgProvince2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgProvince2.ID, Me.lblEmgProvince2.Text)
            Me.lblEmgAddress2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgAddress2.ID, Me.lblEmgAddress2.Text)
            Me.lblEmgPostCountry2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgPostCountry2.ID, Me.lblEmgPostCountry2.Text)
            Me.lblEmgEstate2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgEstate2.ID, Me.lblEmgEstate2.Text)
            Me.lblEmgPostTown2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgPostTown2.ID, Me.lblEmgPostTown2.Text)
            Me.lblEmgState2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgState2.ID, Me.lblEmgState2.Text)
            Me.lblEmgPostCode2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgPostCode2.ID, Me.lblEmgPostCode2.Text)
            Me.lblEmgRoad2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgRoad2.ID, Me.lblEmgRoad2.Text)


            Me.lnkCopyEAddress3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkCopyEAddress3.ID, Me.lnkCopyEAddress3.Text)
            Me.lblEmgEmail3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgEmail3.ID, Me.lblEmgEmail3.Text)
            Me.lblEmgFax3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgFax3.ID, Me.lblEmgFax3.Text)
            Me.lblEmgTelNo3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgTelNo3.ID, Me.lblEmgTelNo3.Text)
            Me.lblEmgAltNo3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgAltNo3.ID, Me.lblEmgAltNo3.Text)
            Me.lblEmgFirstName3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgFirstName3.ID, Me.lblEmgFirstName3.Text)
            Me.lblEmgMobile3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgMobile3.ID, Me.lblEmgMobile3.Text)
            Me.lblEmgLastName3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgLastName3.ID, Me.lblEmgLastName3.Text)
            Me.lblEmgPlotNo3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgPlotNo3.ID, Me.lblEmgPlotNo3.Text)
            Me.lblEmgProvince3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgProvince3.ID, Me.lblEmgProvince3.Text)
            Me.lblEmgAddress3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgAddress3.ID, Me.lblEmgAddress3.Text)
            Me.lblEmgPostCountry3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgPostCountry3.ID, Me.lblEmgPostCountry3.Text)
            Me.lblEmgEstate3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgEstate3.ID, Me.lblEmgEstate3.Text)
            Me.lblEmgPostTown3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgPostTown3.ID, Me.lblEmgPostTown3.Text)
            Me.lblEmgState3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgState3.ID, Me.lblEmgState3.Text)
            Me.lblEmgPostCode3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgPostCode3.ID, Me.lblEmgPostCode3.Text)
            Me.lblEmgRoad3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmgRoad3.ID, Me.lblEmgRoad3.Text)
            Me.lnEmergencyContact3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnEmergencyContact3.ID, Me.lnEmergencyContact3.Text)

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")


            Me.lblEAddressapproval1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEAddressapproval1.ID, Me.lblEAddressapproval1.Text)
            Me.lblEAddressapproval2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEAddressapproval2.ID, Me.lblEAddressapproval2.Text)
            Me.lblEAddressapproval3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEAddressapproval3.ID, Me.lblEAddressapproval3.Text)

            'Language.setLanguage(mstrModuleName1)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    'Pinkal (06-May-2014) -- End


End Class
