﻿Option Strict On 'Shani(19-MAR-2016)
#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.Web.Configuration
Imports System.Data.SqlClient 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
Imports System.Net.Dns 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
Imports System.IO

#End Region

Partial Class HR_wPgEmployeeDetail
    Inherits Basepage

#Region " Private Variable(s) "

    Private msg As New CommonCodes
    Private clsuser As New User
    Private objEmployee As New clsEmployee_Master

    'S.SANDEEP [ 11 MAR 2014 ] -- START
    'Dim strExpression As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
    'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
    Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
    'S.SANDEEP [ 11 MAR 2014 ] -- END

    'S.SANDEEP [ 31 DEC 2013 ] -- START
    Private objCONN As SqlConnection
    'S.SANDEEP [ 31 DEC 2013 ] -- END

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeMaster"
    Private ReadOnly mstrModuleName1 As String = "frmEmployeeList"
    Private ReadOnly mstrModuleName2 As String = "frmJobs_AddEdit"
    'Pinkal (06-May-2014) -- End
    'Sohail (09 Nov 2020) -- Start
    'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
    Private mstrURLReferer As String = ""
    'Sohail (09 Nov 2020) -- End
    Private blnpopupNominateEmployee As Boolean = False
    Private mintNominationunkid As Integer = -1

    'Hemant (01 Jan 2021) -- Start
    'Enhancement #OLD-238 - Improved Employee Profile page
    Dim Arr() As String
    Dim DependantApprovalFlowVal As String
    'Hemant (01 Jan 2021) -- End

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objcommonmaster As New clsCommon_Master
        Dim objjob As New clsJobs
        'Hemant (01 Jan 2021) -- Start
        'Enhancement #OLD-238 - Improved Employee Profile page
        Dim objAction As New clsAction_Reason
        'Hemant (01 Jan 2021) -- End

        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'Sohail (23 Apr 2012) -- Start
                'TRA - ENHANCEMENT
                'dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                'Shani(24-Aug-2015) -- End

                'Sohail (23 Apr 2012) -- End
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombos.Tables("Employee")
                    .DataBind()

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Change in Employee Profile Pages due to wrong assignment of session("employeeunkid").
                    '.SelectedValue = Session("Employeeunkid")
                    .SelectedValue = CStr(Session("EmpUnkid"))
                    'Shani(24-Aug-2015) -- End
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                With cboEmployee
                    .DataSource = objglobalassess.ListOfEmployee
                    .DataTextField = "loginname"
                    .DataValueField = "employeeunkid"
                    .DataBind()
                    .SelectedValue = CStr(Session("Employeeunkid"))
                End With
            End If

            With cboTitle
                .DataSource = objcommonmaster.getComboList(clsCommon_Master.enCommonMaster.TITLE, True, "list")
                .DataTextField = "name"
                .DataValueField = "masterunkid"
                .DataBind()
                .SelectedValue = CStr(0)
            End With


            dsCombos = objjob.getComboList("keyJob", True, , , , , , , , , , , , , , , , True)
            With drpNominateEmployeeJob
                .DataSource = dsCombos.Tables("keyJob")
                .DataTextField = "name"
                .DataValueField = "jobunkid"
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objjob = Nothing
            objAction = Nothing
        End Try
    End Sub

    'Pinkal (27-Mar-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub DeleteImage()
        If imgEmp.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False AndAlso System.IO.File.Exists(Server.MapPath(imgEmp.ImageUrl)) Then
            Try


                Dim mstrFile As String = Server.MapPath(imgEmp.ImageUrl)
                imgEmp.ImageUrl = ""
                'Shani(01-MAR-2017) -- Start
                'System.IO.File.Delete(mstrFile)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Delete_File", "DeleteFile('" & mstrFile.Replace("\", "\\") & "');", True)
                'Shani(01-MAR-2017) -- End
                Fill_Info(CInt(cboEmployee.SelectedValue))
            Catch ex As IO.IOException
                If ex.Message.ToString.Trim.Contains("used by another process") Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage("File is used by another process or File is already open.", Me)
                    'Sohail (23 Mar 2019) -- End
                End If
            Catch ex As Exception
                msg.DisplayError(ex, Me)
            End Try
        End If
    End Sub

    'Pinkal (27-Mar-2013) -- End

    'Private Sub ImageOperation(ByVal imgFullPath As String)
    '    Try
    '        Dim f As New System.IO.FileInfo(imgFullPath)
    '        If System.IO.File.Exists(Server.MapPath("~\images\" & f.Name)) = False Then
    '            Dim rbyte() As Byte = IO.File.ReadAllBytes(imgFullPath)
    '            Dim mem As New System.IO.MemoryStream(rbyte)
    '            Drawing.Image.FromStream(mem).Save(Server.MapPath("~\images\" & f.Name))
    '            If f.Name <> objEmployee._ImagePath Then
    '                Drawing.Image.FromStream(mem).Save(ConfigParameter._Object._PhotoPath & "\" & f.Name)
    '                Me.ViewState("OldImage") = Me.ViewState("ImageName")
    '            End If
    '        End If
    '        imgEmp.ImageUrl = "~\images\" & f.Name
    '        Me.ViewState("ImageName") = f.Name
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    Private Sub Clear_Controls()
        Try
            txtCodeValue.Text = ""
            txtEmploymentTypeValue.Text = ""
            txtFirstValue.Text = ""
            txtGenderValue.Text = ""
            txtIdValue.Text = ""
            txtOtherNameValue.Text = ""
            txtPayPointValue.Text = ""
            txtPayValue.Text = ""
            txtShiftValue.Text = ""
            txtSurnameValue.Text = ""
            imgEmp.ImageUrl = "data:image/png;base64," & ImageToBase64()
            txtcoEmail.Text = ""

            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes

            'If CBool(Session("IsImgInDataBase")) Then
            '    imgEmp.ImageUrl = ""
            'End If

            'Pinkal (01-Apr-2013) -- End
            'Gajanan [13-June-2020] -- Start
            'Enhancement NMB Employee Declaration Changes.
            imgSignature.ImageUrl = "data:image/png;base64," & SignatureImageToBase64()
            'Gajanan [13-June-2020] -- End
            'Hemant (01 Jan 2021) -- Start
            'Enhancement #OLD-238 - Improved Employee Profile page
            objlblEmployeeName.Text = ""
            objlblJob.Text = ""
            objlblDepartment.Text = ""
            txtmobile.Text = ""
            txtBirthDate.Text = ""
            txtAge.Text = ""
            txtHiredon.Text = ""
            txtYearsOfRetirement.Text = ""
            lnklblQualification.Text = ""
            txtWorkingStation.Text = ""
            txtMaritalStatus.Text = ""
            lnkViewAddress.Text = ""
            lnkViewEmergency.Text = ""
            lnkViewPersonal.Text = ""
            'Hemant (01 Jan 2021) -- End
            'Hemant (15 Jan 2021) -- Start
            'Enhancement #OLD-238 - Improved Employee Profile page
            lnkNominateEmp.Text = ""
            'Hemant (15 Jan 2021) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Public Function ImageToBase64() As String
        Dim base64String As String = String.Empty
        Dim path As String = Server.MapPath("../images/ChartUser.png")

        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

            Using m As MemoryStream = New MemoryStream()
                image.Save(m, image.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                base64String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using
    End Function

    Public Function SignatureImageToBase64() As String
        Dim base64String As String = String.Empty
        Dim path As String = Server.MapPath("../images/no-image-available.png")

        Using image As System.Drawing.Image = System.Drawing.Image.FromFile(path)

            Using m As MemoryStream = New MemoryStream()
                image.Save(m, image.RawFormat)
                Dim imageBytes As Byte() = m.ToArray()
                base64String = Convert.ToBase64String(imageBytes)
                Return base64String
            End Using
        End Using
    End Function

    Private Sub Fill_Info(ByVal intEmpId As Integer)
        Dim objCommon As New clsCommon_Master
        'Hemant (01 Jan 2021) -- Start
        'Enhancement #OLD-238 - Improved Employee Profile page
        Dim objJob As New clsJobs
        Dim objDep As New clsDepartment
        Dim objCity As New clscity_master
        Dim objState As New clsstate_master
        Dim objMaster As New clsMasterData
        Dim objEmpQualification As New clsEmp_Qualification_Tran
        'Hemant (01 Jan 2021) -- End
        'Pinkal (03-Jul-2013) -- Start
        'Enhancement : TRA Changes

        'Dim objShift As New clsshift_master
        Dim objShift As New clsNewshift_master

        'Pinkal (03-Jul-2013) -- End


        Dim objPayPoint As New clspaypoint_master
        Try


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = intEmpId
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = intEmpId
            'Shani(20-Nov-2015) -- End

            'Hemant (01 Jan 2021) -- Start
            'Enhancement #OLD-238 - Improved Employee Profile page
            objlblEmployeeName.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
            objJob._Jobunkid = objEmployee._Jobunkid
            objlblJob.Text = objJob._Job_Name

            objDep._Departmentunkid = objEmployee._Departmentunkid
            objlblDepartment.Text = objDep._Name

            txtmobile.Text = objEmployee._Present_Mobile
            txtBirthDate.Text = objEmployee._Birthdate.Date.ToShortDateString()

            Dim intYear, intMonth, intDay As Integer
            If objEmployee._Birthdate.Date <> Nothing Then
                GetDateDiff(objEmployee._Birthdate.Date, ConfigParameter._Object._CurrentDateAndTime.Date, intYear, intMonth, intDay)
                txtAge.Text = intYear & " Years"
            End If
            txtHiredon.Text = objEmployee._Appointeddate.Date.ToShortDateString()

            If objEmployee._Termination_To_Date <> Nothing Then
                GetDateDiff(ConfigParameter._Object._CurrentDateAndTime.Date, objEmployee._Termination_To_Date, intYear, intMonth, intDay)
                txtYearsOfRetirement.Text = intYear & " Years"
            End If

            Dim dsMarital As DataSet = objCommon.getComboList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, True, "List")
            If CInt(objEmployee._Maritalstatusunkid) > 0 Then
                txtMaritalStatus.Text = dsMarital.Tables(0).Select("masterunkid = " & CInt(objEmployee._Maritalstatusunkid) & " ")(0).Item("Name").ToString
            End If

            Dim strViewAddress As String = String.Empty
            strViewAddress = objEmployee._Present_Road
            Dim dsCity As DataSet = objCity.GetList("City", , True)
            Dim dsState As DataSet = objState.GetList("State", , True)
            Dim dsCountry As DataSet = objMaster.getCountryList("Country", True)
            Dim dsLanguage As DataSet = objCommon.getComboList(clsCommon_Master.enCommonMaster.LANGUAGES, True, "Language")

            If CInt(objEmployee._Present_Post_Townunkid) > 0 Then
                If dsCity.Tables(0).Select("cityunkid = " & CInt(objEmployee._Present_Post_Townunkid) & " ").Length > 0 Then
                    Dim strCity As String = dsCity.Tables(0).Select("cityunkid = " & CInt(objEmployee._Present_Post_Townunkid) & " ")(0).Item("Name").ToString
                    strViewAddress &= CStr(IIf(strViewAddress.Length > 0, "," & strCity, strCity))
                End If
            End If
            If CInt(objEmployee._Present_Stateunkid) > 0 Then
                If dsState.Tables(0).Select("stateunkid = " & CInt(objEmployee._Present_Stateunkid) & " ").Length > 0 Then
                    Dim strState As String = dsState.Tables(0).Select("stateunkid = " & CInt(objEmployee._Present_Stateunkid) & " ")(0).Item("Name").ToString
                    strViewAddress &= CStr(IIf(strViewAddress.Length > 0, "," & strState, strState))
                End If
            End If
            If CInt(objEmployee._Present_Countryunkid) > 0 Then
                If dsCountry.Tables(0).Select("Countryunkid = " & CInt(objEmployee._Present_Countryunkid) & " ").Length > 0 Then
                    Dim strCountry As String = dsCountry.Tables(0).Select("Countryunkid = " & CInt(objEmployee._Present_Countryunkid) & " ")(0).Item("Country_Name").ToString
                    strViewAddress &= CStr(IIf(strViewAddress.Length > 0, "," & strCountry, strCountry))
                End If
            End If
            lnkViewAddress.Text = CStr(IIf(strViewAddress.Length > 0, strViewAddress, "Data Not Available"))

            Dim strViewEmergency As String = String.Empty
            strViewEmergency = objEmployee._Emer_Con_Firstname & " " & objEmployee._Emer_Con_Lastname
            If (objEmployee._Emer_Con_Mobile.Length > 0) Then
                strViewEmergency &= CStr(IIf(strViewEmergency.Trim.Length > 0, "," & objEmployee._Emer_Con_Mobile, objEmployee._Emer_Con_Mobile))
            End If
            lnkViewEmergency.Text = CStr(IIf(strViewEmergency.Trim.Length > 0, strViewEmergency, "Data Not Available"))

            Dim strViewPersonal As String = String.Empty
            Dim strLanguage As String = String.Empty
            If objEmployee._Birthcountryunkid > 0 Then
                Dim strCountry As String = dsCountry.Tables(0).Select("Countryunkid = " & CInt(objEmployee._Birthcountryunkid) & " ")(0).Item("Country_Name").ToString
                strViewPersonal = "Born in " & strCountry
            End If

            If objEmployee._Language1unkid > 0 Then
                Dim strLanguage1 As String = dsLanguage.Tables(0).Select("masterunkid = " & CInt(objEmployee._Language1unkid) & " ")(0).Item("Name").ToString
                strLanguage &= CStr(IIf(strLanguage.Length > 0, ", " & strLanguage1, strLanguage1))
            End If
            If objEmployee._Language2unkid > 0 Then
                Dim strLanguage2 As String = dsLanguage.Tables(0).Select("masterunkid = " & CInt(objEmployee._Language2unkid) & " ")(0).Item("Name").ToString
                strLanguage &= CStr(IIf(strLanguage.Length > 0, ", " & strLanguage2, strLanguage2))
            End If
            If objEmployee._Language3unkid > 0 Then
                Dim strLanguage3 As String = dsLanguage.Tables(0).Select("masterunkid = " & CInt(objEmployee._Language3unkid) & " ")(0).Item("Name").ToString
                strLanguage &= CStr(IIf(strLanguage.Length > 0, ", " & strLanguage3, strLanguage3))
            End If
            If objEmployee._Language4unkid > 0 Then
                Dim strLanguage4 As String = dsLanguage.Tables(0).Select("masterunkid = " & CInt(objEmployee._Language4unkid) & " ")(0).Item("Name").ToString
                strLanguage &= CStr(IIf(strLanguage.Length > 0, ", " & strLanguage4, strLanguage4))
            End If
            If strLanguage.Length > 0 Then
                strViewPersonal = strViewPersonal & " Speaks " & strLanguage
            End If
            lnkViewPersonal.Text = CStr(IIf(strViewPersonal.Length > 0, strViewPersonal, "Data Not Available"))

            Dim dsEmpQualificatonList As DataSet = (New clsEmp_Qualification_Tran).GetList(CStr(Session("Database_Name")), CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), DateAndTime.Now.Date, DateAndTime.Now.Date, CStr(Session("UserAccessModeSetting")), True, False, "List", False, " hremp_qualification_tran.employeeunkid = " & CInt(intEmpId) & " ", False, False)
            Dim dtEmpQualificaton As DataTable
            dsEmpQualificatonList.Tables(0).DefaultView.Sort = "qlevel desc"
            dtEmpQualificaton = dsEmpQualificatonList.Tables(0).DefaultView.ToTable
            If dtEmpQualificaton.Rows.Count > 0 Then
                lnklblQualification.Text = dtEmpQualificaton.Rows(0).Item("Qualify").ToString
            End If

            txtWorkingStation.Text = GetWorkStationInfo(CInt(Session("AllocationWorkStation")), objEmployee)
            'Hemant (15 Jan 2021) -- Start
            'Enhancement #OLD-238 - Improved Employee Profile page
            lnkNominateEmp.Text = "Nominate as a Successor"
            'Hemant (15 Jan 2021) -- End
            'Hemant (01 Jan 2021) -- End

            txtCodeValue.Text = objEmployee._Employeecode
            txtFirstValue.Text = objEmployee._Firstname
            txtSurnameValue.Text = objEmployee._Surname
            txtOtherNameValue.Text = objEmployee._Othername

            objCommon._Masterunkid = objEmployee._Employmenttypeunkid
            txtEmploymentTypeValue.Text = objCommon._Name

            If objEmployee._Paytypeunkid > 0 Then
                objCommon._Masterunkid = objEmployee._Paytypeunkid
                txtPayValue.Text = objCommon._Name
            Else
                txtPayValue.Text = ""
            End If
            If objEmployee._Email <> "" Then
                txtcoEmail.Text = objEmployee._Email
            Else
                txtcoEmail.Text = ""
            End If


            Select Case objEmployee._Gender
                Case 1  'MALE
                    txtGenderValue.Text = "Male"
                Case 2  'FEMALE
                    txtGenderValue.Text = "Female"
                Case Else
                    txtGenderValue.Text = ""
            End Select

            objPayPoint._Paypointunkid = objEmployee._Paypointunkid
            txtPayPointValue.Text = objPayPoint._Paypointname

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            'objShift._Shiftunkid = objEmployee._Shiftunkid
            'txtShiftValue.Text = objShift._Shiftname
            Dim objShiftTran As New clsEmployee_Shift_Tran
            Dim iShiftUnkid As Integer = objShiftTran.GetEmployee_Current_ShiftId(ConfigParameter._Object._CurrentDateAndTime, intEmpId)
            objShift._Shiftunkid = iShiftUnkid
            txtShiftValue.Text = objShift._Shiftname
            'S.SANDEEP [ 22 OCT 2013 ] -- END





            'Anjan (24 Nov 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request
            'If objEmployee._Titalunkid > 0 Then
            '    objCommon._Masterunkid = objEmployee._Titalunkid
            '    txtTitelValue.Text = objCommon._Name
            'Else
            '    txtTitelValue.Text = ""
            'End If


            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes

            If objEmployee._Titalunkid < 0 Then
                cboTitle.SelectedValue = CStr(0)
            Else
                cboTitle.SelectedValue = CStr(objEmployee._Titalunkid)
            End If




            'Anjan (24 Nov 2012)-End 


            'If objEmployee._ImagePath <> "" Then
            '    Dim Path As String = ""

            '    If Aruti.Data.ConfigParameter._Object._PhotoPath.LastIndexOf("\") = Aruti.Data.ConfigParameter._Object._PhotoPath.ToString.Trim.Length - 1 Then
            '        Path = System.IO.Path.GetFullPath(Aruti.Data.ConfigParameter._Object._PhotoPath & objEmployee._ImagePath)
            '        Call ImageOperation(Path)
            '    Else
            '        Path = System.IO.Path.GetFullPath(Aruti.Data.ConfigParameter._Object._PhotoPath & "\" & objEmployee._ImagePath)
            '        Call ImageOperation(Path)
            '    End If
            'End If


            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes

            If objEmployee._blnImgInDb Then

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objEmployee._Employeeunkid > 0 Then
                If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) > 0 Then
                    'Shani(20-Nov-2015) -- End

                    If objEmployee._Photo IsNot Nothing Then
                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'imgEmp.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid & "&ModeID=1"
                        imgEmp.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=1"
                        'Shani(20-Nov-2015) -- End
                    Else
                        imgEmp.ImageUrl = "data:image/png;base64," & ImageToBase64()
                    End If
                End If
            End If

            'Pinkal (01-Apr-2013) -- End

            'S.SANDEEP [16-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 118
            If objEmployee._EmpSignature IsNot Nothing Then
                imgSignature.ImageUrl = "~\GetImageHandler.ashx?id=" & objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) & "&ModeID=3"
            Else
                imgSignature.ImageUrl = "data:image/png;base64," & SignatureImageToBase64()

            End If
            'S.SANDEEP [16-Jan-2018] -- END

            Dim objEIdTran As New clsIdentity_tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEIdTran._EmployeeUnkid = objEmployee._Employeeunkid
            objEIdTran._EmployeeUnkid = objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date)
            'Shani(20-Nov-2015) -- End

            Dim dtTable = objEIdTran._DataList
            If dtTable.Rows.Count > 0 Then
                Dim dtTemp() As DataRow = dtTable.Select("isdefault = true")
                If dtTemp.Length > 0 Then
                    txtIdValue.Text = CStr(dtTemp(0)("identity_no"))
                End If
            Else
                txtIdValue.Text = ""
            End If

            'Hemant (01 Jan 2021) -- Start
            'Enhancement #OLD-238 - Improved Employee Profile page
            SetEmployeeDatesInfo(objEmployee)
            SetAllocationInfo(objEmployee)
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) > 0 Then
                Call FillDependantsGrid()
                Call FillMyReportingGrids()
            End If
            'Hemant (01 Jan 2021) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            'Hemant (01 Jan 2021) -- Start
            'Enhancement #OLD-238 - Improved Employee Profile page
            objJob = Nothing
            objDep = Nothing
            objCity = Nothing
            objState = Nothing
            objMaster = Nothing
            objEmpQualification = Nothing
            'Hemant (01 Jan 2021) -- End
        End Try
    End Sub

    Private Sub SetNominationValue(ByVal objEmployee_nomination_master As clsEmployee_nomination_master)
        Try
            objEmployee_nomination_master._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmployee_nomination_master._Jobunkid = CInt(drpNominateEmployeeJob.SelectedValue)
            objEmployee_nomination_master._Isvoid = False
            objEmployee_nomination_master._Ismatch = True
            SetATNominationValue(objEmployee_nomination_master)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetATNominationValue(ByVal objEmployee_nomination_master As clsEmployee_nomination_master)
        Try
            objEmployee_nomination_master._AuditUserId = CInt(Session("UserId"))
            objEmployee_nomination_master._ClientIP = CStr(Session("IP_ADD"))
            objEmployee_nomination_master._FormName = mstrModuleName
            objEmployee_nomination_master._FromWeb = True
            objEmployee_nomination_master._HostName = CStr(Session("HOST_NAME"))
            objEmployee_nomination_master._DatabaseName = CStr(Session("Database_Name"))
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillNomination()
        Dim objEmployee_nomination_master As New clsEmployee_nomination_master
        Dim dsList As New DataSet
        Try
            dsList = objEmployee_nomination_master.GetList("NominationList", CInt(cboEmployee.SelectedValue))
            dgvNominateEmployee.DataSource = dsList.Tables("NominationList")
            dgvNominateEmployee.DataBind()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objEmployee_nomination_master = Nothing
        End Try
    End Sub

    'Hemant (01 Jan 2021) -- Start
    'Enhancement #OLD-238 - Improved Employee Profile page
    Private Function GetWorkStationInfo(ByVal intAllocationId As Integer, ByVal objEmp As clsEmployee_Master) As String
        Dim strWorkStation As String = String.Empty

        Dim objStation As New clsStation
        Dim objDeptGroup As New clsDepartmentGroup
        Dim objDept As New clsDepartment
        Dim objSectionGroup As New clsSectionGroup
        Dim objSection As New clsSections
        Dim objUnitGroup As New clsUnitGroup
        Dim objUnit As New clsUnits
        Dim objTeam As New clsTeams
        Dim objJobGroup As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objClassGroup As New clsClassGroup
        Dim objClass As New clsClass
        Dim objCostCenter As New clscostcenter_master

        Try
            Select Case intAllocationId
                Case enAllocation.BRANCH
                    objStation._Stationunkid = objEmp._Stationunkid
                    strWorkStation = objStation._Name
                Case enAllocation.DEPARTMENT_GROUP
                    objDeptGroup._Deptgroupunkid = objEmp._Deptgroupunkid
                    strWorkStation = objDeptGroup._Name
                Case enAllocation.DEPARTMENT
                    objDept._Departmentunkid = objEmp._Departmentunkid
                    strWorkStation = objDept._Name
                Case enAllocation.SECTION_GROUP
                    objSectionGroup._Sectiongroupunkid = objEmp._Sectiongroupunkid
                    strWorkStation = objSectionGroup._Name
                Case enAllocation.SECTION
                    objSection._Sectionunkid = objEmp._Sectionunkid
                    strWorkStation = objSection._Name
                Case enAllocation.UNIT_GROUP
                    objUnitGroup._Unitgroupunkid = objEmp._Unitgroupunkid
                    strWorkStation = objUnitGroup._Name
                Case enAllocation.UNIT
                    objUnit._Unitunkid = objEmp._Unitunkid
                    strWorkStation = objUnit._Name
                Case enAllocation.TEAM
                    objTeam._Teamunkid = objEmp._Teamunkid
                    strWorkStation = objTeam._Name
                Case enAllocation.JOB_GROUP
                    objJobGroup._Jobgroupunkid = objEmp._Jobgroupunkid
                    strWorkStation = objJobGroup._Name
                Case enAllocation.JOBS
                    objJob._Jobunkid = objEmp._Jobunkid
                    strWorkStation = objJob._Job_Name
                Case enAllocation.CLASS_GROUP
                    objClassGroup._Classgroupunkid = objEmp._Classgroupunkid
                    strWorkStation = objClassGroup._Name
                Case enAllocation.CLASSES
                    objClass._Classesunkid = objEmp._Classunkid
                    strWorkStation = objClass._Name
                Case enAllocation.COST_CENTER
                    objCostCenter._Costcenterunkid = objEmp._Costcenterunkid
                    strWorkStation = objCostCenter._Costcentername
                Case Else
                    objDept._Departmentunkid = objEmp._Departmentunkid
                    strWorkStation = objDept._Name
            End Select
            Return strWorkStation
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objStation = Nothing
            objDeptGroup = Nothing
            objDept = Nothing
            objSectionGroup = Nothing
            objSection = Nothing
            objUnitGroup = Nothing
            objUnit = Nothing
            objTeam = Nothing
            objJobGroup = Nothing
            objJob = Nothing
            objClassGroup = Nothing
            objClass = Nothing
            objCostCenter = Nothing
        End Try
    End Function
    'Hemant (01 Jan 2021) -- End

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            'S.SANDEEP [ 31 DEC 2013 ] -- START
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then

                'Sohail (02 Apr 2019) -- Start
                'NMB Issue - 74.1 - Error "On Load Event !! Bad Data" on clicking any page with link after session get expired.
                If Request.QueryString.Count <= 0 Then Exit Sub
                'Sohail (02 Apr 2019) -- End

                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    'constr = constr.Replace("Initial Catalog=hrmsConfiguration;", "Initial Catalog=" & Session("mdbname").ToString & ";")
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                If arr.Length = 4 Then
                    Session("iQueryString") = Request.QueryString.ToString
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    Blank_ModuleName()
                    objEmployee._WebFormName = "frmEmployeeMaster"
                    StrModuleName2 = "mnuCoreSetups"
                    objEmployee._WebClientIP = CStr(Session("IP_ADD"))
                    objEmployee._WebHostName = CStr(Session("HOST_NAME"))
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        objEmployee._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                    End If

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    HttpContext.Current.Session("UserId") = CInt(arr(1))
                    HttpContext.Current.Session("ShowPending") = CInt(arr(3))

                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Dim objCommon As New CommonCodes
                    'objCommon.GetCompanyYearInfo(CInt(Session("CompanyUnkId")))
                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If
                    'Sohail (30 Mar 2015) -- End
                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    Session("IsImgInDataBase") = gobjConfigOptions._IsImgInDataBase
                    Session("CompanyDomain") = gobjConfigOptions._CompanyDomain

                    'S.SANDEEP [16-Jan-2018] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 118
                    Session("AllowtoViewSignatureESS") = gobjConfigOptions._AllowtoViewSignatureESS
                    'S.SANDEEP [16-Jan-2018] -- END

                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))

                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management)) = False Then
                        msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If
                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            msg.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Sub
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                msg.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Sub
                            End If
                        End If
                    End If
                    Dim clsConfig As New clsConfigOptions
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))

                    HttpContext.Current.Session("Login") = True

                    'Sohail (21 Mar 2015) -- Start
                    'Enhancement - New UI Notification Link Changes.
                    'Dim clsuser As New User(objUser._Username, objUser._Password, Global.User.en_loginby.User, Session("mdbname"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, CStr(Session("mdbname")))
                    'Sohail (21 Mar 2015) -- End
                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        msg.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                        Exit Sub
                    End If
                    'Sohail (30 Mar 2015) -- End

                    Dim objUserPrivilege As New clsUserPrivilege
                    objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                    Session("AllowToApproveEmployee") = objUserPrivilege._AllowToApproveEmployee
                    Session("EditEmployee") = objUserPrivilege._EditEmployee
                    Session("AllowtoChangeCompanyEmail") = objUserPrivilege._AllowtoChangeCompanyEmail
                    Session("ViewEmployee") = objUserPrivilege._AllowToViewEmpList
                    Session("ViewScale") = objUserPrivilege._AllowTo_View_Scale
                    Session("AllowtoChangeBranch") = objUserPrivilege._AllowtoChangeBranch
                    Session("AllowtoChangeDepartmentGroup") = objUserPrivilege._AllowtoChangeDepartmentGroup
                    Session("AllowtoChangeDepartment") = objUserPrivilege._AllowtoChangeDepartment
                    Session("AllowtoChangeSectionGroup") = objUserPrivilege._AllowtoChangeSectionGroup
                    Session("AllowtoChangeSection") = objUserPrivilege._AllowtoChangeSection
                    Session("AllowtoChangeUnitGroup") = objUserPrivilege._AllowtoChangeUnitGroup
                    Session("AllowtoChangeUnit") = objUserPrivilege._AllowtoChangeUnit
                    Session("AllowtoChangeTeam") = objUserPrivilege._AllowtoChangeTeam
                    Session("AllowtoChangeJobGroup") = objUserPrivilege._AllowtoChangeJobGroup
                    Session("AllowtoChangeJob") = objUserPrivilege._AllowtoChangeJob
                    Session("AllowtoChangeGradeGroup") = objUserPrivilege._AllowtoChangeGradeGroup
                    Session("AllowtoChangeGrade") = objUserPrivilege._AllowtoChangeGrade
                    Session("AllowtoChangeGradeLevel") = objUserPrivilege._AllowtoChangeGradeLevel
                    Session("AllowtoChangeClassGroup") = objUserPrivilege._AllowtoChangeClassGroup
                    Session("AllowtoChangeClass") = objUserPrivilege._AllowtoChangeClass
                    Session("AllowtoChangeCostCenter") = objUserPrivilege._AllowtoChangeCostCenter
                    Session("SetReinstatementdate") = objUserPrivilege._AllowtoSetEmpReinstatementDate
                    Session("ChangeConfirmationDate") = objUserPrivilege._AllowtoChangeConfirmationDate
                    Session("ChangeAppointmentDate") = objUserPrivilege._AllowtoChangeAppointmentDate
                    Session("SetEmployeeBirthDate") = objUserPrivilege._AllowtoSetEmployeeBirthDate
                    Session("SetEmpSuspensionDate") = objUserPrivilege._AllowtoSetEmpSuspensionDate
                    Session("SetEmpProbationDate") = objUserPrivilege._AllowtoSetEmpProbationDate
                    Session("SetEmploymentEndDate") = objUserPrivilege._AllowtoSetEmploymentEndDate
                    Session("SetLeavingDate") = objUserPrivilege._AllowtoSetLeavingDate
                    Session("ChangeRetirementDate") = objUserPrivilege._AllowtoChangeRetirementDate

                    'S.SANDEEP [16-Jan-2018] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 118
                    Session("AllowtoAddEmployeeSignature") = objUserPrivilege._AllowtoAddEmployeeSignature
                    Session("AllowtoDeleteEmployeeSignature") = objUserPrivilege._AllowtoDeleteEmployeeSignature
                    Session("AllowtoSeeEmployeeSignature") = objUserPrivilege._AllowtoSeeEmployeeSignature
                    'S.SANDEEP [16-Jan-2018] -- END

                    If CBool(Session("AllowToApproveEmployee")) = False Then
                        Session("ShowPending") = Nothing
                    End If

                    If Session("ShowPending") IsNot Nothing Then
                        chkShowPending.Checked = CBool(Session("ShowPending"))
                        Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                        chkShowPending.Enabled = False
                    End If

                    flUpload.Visible = CBool(Session("IsImgInDataBase"))
                    btnRemoveImage.Visible = CBool(Session("IsImgInDataBase"))
                    imgEmp.Visible = CBool(Session("IsImgInDataBase"))

                    'Gajanan [13-June-2020] -- Start
                    'Enhancement NMB Employee Declaration Changes.
                    flUploadsig.Visible = CBool(Session("AllowtoAddEmployeeSignature"))
                    btnRemoveSig.Visible = CBool(Session("AllowtoDeleteEmployeeSignature"))
                    imgSignature.Visible = CBool(Session("AllowtoSeeEmployeeSignature"))
                    'Gajanan [13-June-2020] -- End

                    cboEmployee.Enabled = CBool(Session("ViewEmployee"))
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                    'Sohail (30 Mar 2015) -- End
                    Exit Sub
                End If
            Else
                If Session("iQueryString") IsNot Nothing AndAlso Session("iQueryString").ToString <> "" Then
                    chkShowPending.Enabled = False
                End If
            End If
            'S.SANDEEP [ 31 DEC 2013 ] -- END


            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            Blank_ModuleName()
            objEmployee._WebFormName = "frmEmployeeMaster"
            StrModuleName2 = "mnuCoreSetups"
            objEmployee._WebClientIP = CStr(Session("IP_ADD"))
            objEmployee._WebHostName = CStr(Session("HOST_NAME"))
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                objEmployee._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                'Hemant (15 Jan 2021) -- Start
                'Enhancement #OLD-238 - Improved Employee Profile page
                pnlNominateEmp.Visible = False
                'Hemant (15 Jan 2021) -- End
            End If

            'S.SANDEEP [ 04 JULY 2012 ] -- END



            'Pinkal (27-Mar-2013) -- Start
            'Enhancement : TRA Changes
            objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
            objEmployee._blnImgInDb = CBool(Session("IsImgInDataBase"))
            'Pinkal (27-Mar-2013) -- End


         

            If (Page.IsPostBack = False) Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLnaguae()
                'Pinkal (06-May-2014) -- End


                clsuser = CType(Session("clsuser"), User)
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Aruti.Data.User._Object._Userunkid = Session("UserId")
                    'Sohail (23 Apr 2012) -- End
                    pnlCombo.Visible = True
                    'Pinkal (22-Nov-2012) -- Start
                    'Enhancement : TRA Changes

                    ' txtcoEmail.ReadOnly = True
                    ' btnsave.Visible = False

                    btnsave.Visible = CBool(Session("EditEmployee"))


                    'S.SANDEEP |18-JUN-2021| -- START
                    'ISSUE/ENHANCEMENT : PRIVILEGE ARE NOT KEPT
                    lblScale.Visible = CBool(Session("ViewScale"))
                    txtScale.Visible = CBool(Session("ViewScale"))
                    cboBranch.Enabled = CBool(Session("AllowtoChangeBranch"))
                    cboDeptGrp.Enabled = CBool(Session("AllowtoChangeDepartmentGroup"))
                    cboDepartment.Enabled = CBool(Session("AllowtoChangeDepartment"))
                    cboSecGroup.Enabled = CBool(Session("AllowtoChangeSectionGroup"))
                    cboSection.Enabled = CBool(Session("AllowtoChangeSection"))
                    cboUnitGrp.Enabled = CBool(Session("AllowtoChangeUnitGroup"))
                    cboUnit.Enabled = CBool(Session("AllowtoChangeUnit"))
                    cboTeam.Enabled = CBool(Session("AllowtoChangeTeam"))
                    cboJobGroup.Enabled = CBool(Session("AllowtoChangeJobGroup"))
                    cboJob.Enabled = CBool(Session("AllowtoChangeJob"))
                    cboGradeGrp.Enabled = CBool(Session("AllowtoChangeGradeGroup"))
                    cboGrade.Enabled = CBool(Session("AllowtoChangeGrade"))
                    cboGradeLevel.Enabled = CBool(Session("AllowtoChangeGradeLevel"))
                    cboClassGrp.Enabled = CBool(Session("AllowtoChangeClassGroup"))
                    cboClass.Enabled = CBool(Session("AllowtoChangeClass"))
                    cboCostcenter.Enabled = CBool(Session("AllowtoChangeCostCenter"))
                    'S.SANDEEP |18-JUN-2021| -- END


                    'Pinkal (22-Nov-2012) -- End
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    Page.Title = "Employee Profile"
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END


                    'Anjan (03 Dec 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew's Request
                    txtcoEmail.ReadOnly = Not CBool(Session("AllowtoChangeCompanyEmail"))
                    'Anjan (03 Dec 2012)-End 

                    'S.SANDEEP [ 31 DEC 2013 ] -- START
                    chkShowPending.Visible = CBool(Session("AllowToApproveEmployee"))
                    'S.SANDEEP [ 31 DEC 2013 ] -- END

                    'S.SANDEEP [16-Jan-2018] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 118
                    '*************************** CURRENTLY GIVEN TO VIEW ONLY NOT ADD/DELETE
                    'flUploadsig.Visible = False
                    'btnRemoveSig.Visible = False
                    'flUploadsig.Visible = CBool(Session("AllowtoAddEmployeeSignature"))
                    'btnRemoveSig.Visible = CBool(Session("AllowtoDeleteEmployeeSignature"))
                    'S.SANDEEP [16-Jan-2018] -- END

                    flUploadsig.Visible = CBool(Session("AllowtoAddEmployeeSignature"))
                    btnRemoveSig.Visible = CBool(Session("AllowtoDeleteEmployeeSignature"))
                    imgSignature.Visible = CBool(Session("AllowtoSeeEmployeeSignature"))
                    'lblESign.Visible = imgSignature.Visible



                    pnlEmployeeQuickLinks.Visible = False

                Else
                    'Sohail (23 Apr 2012) -- Start
                    'TRA - ENHANCEMENT
                    'Aruti.Data.User._Object._Userunkid = -1

                    'Sohail (23 Apr 2012) -- End
                    pnlCombo.Visible = False
                    txtcoEmail.ReadOnly = False
                    btnsave.Visible = True
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    Page.Title = "MY Profile"
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'S.SANDEEP [ 06 DEC 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    txtcoEmail.ReadOnly = Not CBool(Session("AllowChangeCompanyEmail"))
                    'S.SANDEEP [ 06 DEC 2012 ] -- END

                    'S.SANDEEP [ 31 DEC 2013 ] -- START
                    chkShowPending.Visible = False
                    'S.SANDEEP [ 31 DEC 2013 ] -- END

                    'S.SANDEEP [16-Jan-2018] -- START
                    'ISSUE/ENHANCEMENT : REF-ID # 118
                    imgSignature.Visible = CBool(Session("AllowtoViewSignatureESS"))

                    'Gajanan [13-June-2020] -- Start
                    'Enhancement NMB Employee Declaration Changes.
                    UPUploadSig.Visible = CBool(Session("AllowEmployeeToAddEditSignatureESS"))
                    btnRemoveSig.Visible = CBool(Session("AllowEmployeeToAddEditSignatureESS"))

                    imgEmp.Visible = CBool(Session("AllowToAddEditImageForESS"))
                    flUpload.Visible = CBool(Session("AllowToAddEditImageForESS"))
                    'Gajanan [13-June-2020] -- End

                    'flUploadsig.Visible = False
                    'btnRemoveSig.Visible = False
                    'lblESign.Visible = imgSignature.Visible
                    'S.SANDEEP [16-Jan-2018] -- END


                    pnlEmployeeQuickLinks.Visible = True

                End If

                'S.SANDEEP [ 31 DEC 2013 ] -- START
                If CBool(Session("AllowToApproveEmployee")) = False Then
                    Session("ShowPending") = Nothing
                End If

                If Session("ShowPending") Is Nothing Then
                    Call FillCombo()
                    'Hemant (01 Jan 2021) -- Start
                    'Enhancement #OLD-238 - Improved Employee Profile page
                    Call FillEmployeeDatesCombo()
                    Call FillAllocationCombo()
                    'Hemant (01 Jan 2021) -- End
                Else
                    chkShowPending.Checked = CBool(Session("ShowPending"))
                    Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                End If
                'S.SANDEEP [ 31 DEC 2013 ] -- END

                'Pinkal (22-Nov-2012) -- Start
                'Enhancement : TRA Changes
                cboEmployee.Enabled = CBool(Session("ViewEmployee"))
                'Pinkal (22-Nov-2012) -- End


                'Pinkal (01-Apr-2013) -- Start
                'Enhancement : TRA Changes
                flUpload.Visible = CBool(Session("IsImgInDataBase"))
                btnRemoveImage.Visible = CBool(Session("IsImgInDataBase"))
                imgEmp.Visible = CBool(Session("IsImgInDataBase"))
                'lblEPhoto.Visible = imgEmp.Visible
                'Pinkal (01-Apr-2013) -- End

                'Hemant (01 Jan 2021) -- Start
                'Enhancement #OLD-238 - Improved Employee Profile page
                Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
                DependantApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmDependantsAndBeneficiariesList)))
                'Hemant (01 Jan 2021) -- End


                'Hemant (15 Jan 2021) -- Start
                'Enhancement #OLD-238 - Improved Employee Profile page
                'Call Fill_Info(CInt(Session("Employeeunkid")))
                'Hemant (15 Jan 2021) -- End
                Call cboEmployee_SelectedIndexChanged(sender, e)


                'Pinkal (03-Sep-2013) -- Start
                'Enhancement : TRA Changes

                'Anjan [20 February 2016] -- Start
                'ENHANCEMENT : Changed parameter of demo license with session parameter. 
                'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                    'Anjan [20 February 2016] -- End
                    'Hemant (01 Jan 2021) -- Start
                    'Enhancement #OLD-238 - Improved Employee Profile page
                    pnl_ViewLeaveApprover.Visible = False
                    'Hemant (01 Jan 2021) -- End
                End If

                'Anjan [20 February 2016] -- Start
                'ENHANCEMENT : Changed parameter of demo license with session parameter. 
                If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                    'Anjan [20 February 2016] -- End
                    'Hemant (01 Jan 2021) -- Start
                    'Enhancement #OLD-238 - Improved Employee Profile page
                    pnl_ViewAssessorReviewer.Visible = False
                    'Hemant (01 Jan 2021) -- End
                End If
                'Pinkal (03-Sep-2013) -- End


                'Pinkal (05-May-2020) -- Start
                'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .
                If (CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) = False) OrElse CBool(Session("SetOTRequisitionMandatory")) = False Then
                    'Hemant (01 Jan 2021) -- Start
                    'Enhancement #OLD-238 - Improved Employee Profile page
                    pnl_ViewOTApprover.Visible = False
                    'Hemant (01 Jan 2021) -- End
                End If
                'Pinkal (05-May-2020) -- End

                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                If Request.UrlReferrer IsNot Nothing Then
                    mstrURLReferer = Request.UrlReferrer.AbsoluteUri
                Else
                    mstrURLReferer = "~\UserHome.aspx"
                End If
            Else
                mstrURLReferer = ViewState("mstrURLReferer").ToString
                'Sohail (09 Nov 2020) -- End

                If CBool(ViewState("blnpopupNominateEmployee")) Then
                    blnpopupNominateEmployee = CBool(ViewState("blnpopupNominateEmployee"))
                End If

                mintNominationunkid = CInt(ViewState("mintNominationunkid"))

                If blnpopupNominateEmployee Then
                    popupNominateEmployee.Show()
                End If
            End If

            'S.SANDEEP [19-JAN-2017] -- START
            'ISSUE/ENHANCEMENT :
            If chkShowPending.Checked = True Then
                'S.SANDEEP [22-NOV-2018] -- START
                'btnsave.Text = "Approve"
                'btnClose.Text = "Don't"
                If CBool(Session("SkipEmployeeApprovalFlow")) = True Then
                    btnsave.Text = "Approve"
                    btnClose.Text = "Don't"
                End If
                'S.SANDEEP [22-NOV-2018] -- END
            Else
                btnsave.Text = "Update"
                btnClose.Text = "Close"
            End If
            'S.SANDEEP [19-JAN-2017] -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End


    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        'If IsPostBack = True And Me.ViewState("OldImage") <> "" Then
        '    Call DeleteImage(Server.MapPath("~\images\" & Me.ViewState("OldImage")))
        'End If
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try         'Hemant (13 Aug 2020)
            'S.SANDEEP [ 31 DEC 2013 ] -- START
            'Me.IsLoginRequired = True
            If Request.QueryString.Count <= 0 Then
                Me.IsLoginRequired = True
            End If
            'S.SANDEEP [ 31 DEC 2013 ] -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    'Sohail (09 Nov 2020) -- Start
    'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("mstrURLReferer") = mstrURLReferer
            ViewState("blnpopupNominateEmployee") = blnpopupNominateEmployee
            ViewState("mintNominationunkid") = mintNominationunkid
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (09 Nov 2020) -- End


#End Region

#Region " Controls Events "

    Protected Sub cboEmployee_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.DataBound
        Try         'Hemant (13 Aug 2020)
            If cboEmployee.Items.Count > 0 Then
                For Each lstItem As ListItem In cboEmployee.Items
                    lstItem.Attributes.Add("title", lstItem.Text)
                Next
                cboEmployee.Attributes.Add("onmouseover", "this.title=this.options[this.selectedIndex].title")
            End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then
                'Gajanan [13-June-2020] -- Start
                'Enhancement NMB Employee Declaration Changes.
                Clear_Controls()
                'Gajanan [13-June-2020] -- End

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    Call Fill_Info(CInt(cboEmployee.SelectedValue))
                    Session("Employeeunkid") = CInt(cboEmployee.SelectedValue)

                Else
                    Call Fill_Info(CInt(Session("Employeeunkid")))

                End If


                If CBool(Session("IsImgInDataBase")) = True Then
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        flUpload.Enabled = CBool(Session("AllowToAddEditImageForESS"))
                        btnRemoveImage.Enabled = False
                    ElseIf (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        flUpload.Enabled = CBool(Session("AllowToAddEditPhoto"))
                        btnRemoveImage.Enabled = CBool(Session("AllowToDeletePhoto"))
                    End If
                End If

            Else
                Clear_Controls()

                flUpload.Enabled = False
                btnRemoveImage.Enabled = False


            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'S.SANDEEP [ 31 DEC 2013 ] -- START
    Protected Sub chkShowPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowPending.CheckedChanged
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Try
            Session("ShowPending") = chkShowPending.Checked
            If chkShowPending.Checked = True Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"), , , False)

                'S.SANDEEP [19-JAN-2017] -- START
                'ISSUE/ENHANCEMENT : GETTING UNAPPROVED EMPLOYEE
                'dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                '                                    CInt(Session("UserId")), _
                '                                    CInt(Session("Fin_year")), _
                '                                    CInt(Session("CompanyUnkId")), _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                    CStr(Session("UserAccessModeSetting")), False, _
                '                                    CBool(Session("IsIncludeInactiveEmp")), "Employee", True)

                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                    CInt(Session("UserId")), _
                                                    CInt(Session("Fin_year")), _
                                                    CInt(Session("CompanyUnkId")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                    CStr(Session("UserAccessModeSetting")), False, _
                                                  CBool(Session("IsIncludeInactiveEmp")), "Employee", True, , , , , , , , , , , , , , , , , , False)
                'S.SANDEEP [19-JAN-2017] -- END

                'Shani(24-Aug-2015) -- End

                Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dtTab
                    .DataBind()
                    Try
                        .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                    Catch ex As Exception
                        .SelectedValue = CStr(0)
                    End Try
                End With
                'btnsave.Text = "Approve"
                'btnClose.Text = "Don't"
            Else
                'btnsave.Text = "Update"
                'btnClose.Text = "Close"
                Session("ShowPending") = Nothing

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
                    dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                            CStr(Session("UserAccessModeSetting")), True, _
                                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                    'Shani(24-Aug-2015) -- End
                    With cboEmployee
                        .DataValueField = "employeeunkid"
                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        '.DataTextField = "employeename"
                        .DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        .DataSource = dsCombos.Tables("Employee")
                        .DataBind()
                        Try
                            .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                        Catch ex As Exception
                            .SelectedValue = CStr(0)
                        End Try
                    End With
                Else
                    Dim objglobalassess = New GlobalAccess
                    objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                    With cboEmployee
                        .DataSource = objglobalassess.ListOfEmployee
                        .DataTextField = "loginname"
                        .DataValueField = "employeeunkid"
                        .DataBind()
                        Try
                            .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                        Catch ex As Exception
                            .SelectedValue = CStr(0)
                        End Try
                    End With
                End If
            End If
            Call cboEmployee_SelectedIndexChanged(New Object, New EventArgs)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [ 31 DEC 2013 ] -- END

#End Region

#Region " Button's Event(s) "

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        Try

            'Sohail (06 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                msg.DisplayMessage("Please select Employee.", Me)
                Exit Sub
            End If
            'Sohail (06 Apr 2013) -- End


            'S.SANDEEP [19-JAN-2017] -- START
            'ISSUE/ENHANCEMENT :
            ''S.SANDEEP [ 11 MAR 2014 ] -- START
            'If Expression.IsMatch(txtcoEmail.Text.Trim) = False Then
            '    msg.DisplayMessage("Invalid Present Email.Please Enter Valid Email", Me)
            '    Exit Sub
            'End If
            ''S.SANDEEP [ 11 MAR 2014 ] -- END
            'S.SANDEEP [19-JAN-2017] -- END





            'S.SANDEEP [ 23 NOV 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If txtcoEmail.Text.Trim.Length > 0 Then
                'S.SANDEEP [19-JAN-2017] -- START
                If Expression.IsMatch(txtcoEmail.Text.Trim) = False Then
                    msg.DisplayMessage("Invalid Present Email.Please Enter Valid Email", Me)
                    Exit Sub
                End If
                'S.SANDEEP [19-JAN-2017] -- END

                Dim objMData As New clsMasterData
                'If objEmployee.IsEmailPresent(txtcoEmail.Text, Session("Employeeunkid")) = False Then
                '    msg.DisplayMessage("Sorry, this Company email address is already taken by some employee. Please provide new and unique email address.", Me)
                '    Exit Sub
                'End If
                If objMData.IsEmailPresent("hremployee_master", txtcoEmail.Text, CInt(Session("Employeeunkid")), "employeeunkid") = True Then
                    msg.DisplayMessage("Sorry, this Company email address is already taken by some employee. Please provide new and unique email address.", Me)
                    Exit Sub
                End If
                objMData = Nothing
            End If
            'S.SANDEEP [ 23 NOV 2012 ] -- END

            If txtcoEmail.Text.Trim.Length > 0 Then
                If txtcoEmail.Text.Trim.Contains("@") AndAlso Session("CompanyDomain").ToString().Trim <> "" Then
                    If txtcoEmail.Text.Trim.Substring(txtcoEmail.Text.Trim.IndexOf("@") + 1) <> Session("CompanyDomain").ToString() Then
                        msg.DisplayMessage("Invalid Company Email Domain.Please Enter valid Company Email Domain as per configuration settings.", Me)
                        txtcoEmail.Focus()
                        Exit Sub
                    End If
                End If
            End If


            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = Session("Employeeunkid")
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            'Shani(20-Nov-2015) -- End

            objEmployee._Email = txtcoEmail.Text
            'S.SANDEEP [ 19 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
            'S.SANDEEP [ 19 MAR 2013 ] -- END


            'Anjan (25 Oct 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew's Request

            'S.SANDEEP [ 31 DEC 2013 ] -- START
            'objEmployee._Titalunkid = CInt(cboTitle.SelectedValue)
            objEmployee._Titalunkid = CInt(IIf(cboTitle.SelectedValue = "", 0, cboTitle.SelectedValue))
            'S.SANDEEP [ 31 DEC 2013 ] -- END


            'Anjan (25 Oct 2012)-End 

            'S.SANDEEP [ 31 DEC 2013 ] -- START
            If Session("ShowPending") IsNot Nothing Then
                'S.SANDEEP [22-NOV-2018] -- START
                'objEmployee._Isapproved = CBool(Session("AllowToApproveEmployee"))
                If CBool(Session("SkipEmployeeApprovalFlow")) = True Then
                    objEmployee._Isapproved = CBool(Session("AllowToApproveEmployee"))
                Else
                    If objEmployee._Isapproved = True Then
                        objEmployee._Isapproved = objEmployee._Isapproved
                    Else
                        objEmployee._Isapproved = False
                    End If
                End If
                'S.SANDEEP [22-NOV-2018] -- END
            End If
            'S.SANDEEP [ 31 DEC 2013 ] -- END


            'Pinkal (27-Mar-2013) -- Start
            'Enhancement : TRA Changes

            If CBool(Session("IsImgInDataBase")) Then
                If imgEmp.ImageUrl.Trim <> "" AndAlso imgEmp.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False Then
                    objEmployee._Photo = ImageCompression(Server.MapPath(imgEmp.ImageUrl))
                ElseIf imgEmp.ImageUrl.Trim = "" Then
                    objEmployee._Photo = Nothing
                End If
            End If

            'Gajanan [13-June-2020] -- Start
            'Enhancement NMB Employee Declaration Changes.
            If imgSignature.ImageUrl.Trim <> "" AndAlso imgSignature.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False Then
                objEmployee._EmpSignature = ImageCompression(Server.MapPath(imgSignature.ImageUrl))
            ElseIf imgSignature.ImageUrl.Trim = "" Then
                objEmployee._EmpSignature = Nothing
            End If
            'Gajanan [13-June-2020] -- End


            'If objEmployee.Update(, , , Session("UserId"), Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), Session("Fin_year"), Session("AllowToApproveEarningDeduction")) = False Then

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


            'If objEmployee.Update(, , , Session("UserId"), Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), Session("Fin_year"), Session("AllowToApproveEarningDeduction"), CBool(Session("IsImgInDataBase"))) = False Then
            '    msg.DisplayMessage(objEmployee._Message, Me)
            'Else
            '    If CBool(Session("IsImgInDataBase")) Then
            '        DeleteImage()
            '    End If
            '    'S.SANDEEP [ 31 DEC 2013 ] -- START
            '    If Session("ShowPending") IsNot Nothing Then
            '        msg.DisplayMessage("Employee Approved successfully!", Me)
            '    Else
            '        msg.DisplayMessage("Information updated successfully!", Me)
            '    End If
            '    'S.SANDEEP [ 31 DEC 2013 ] -- END

            'End If


            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

            'Dim blnFlag As Boolean = objEmployee.Update(CStr(Session("Database_Name")), _
            '                                            CInt(Session("Fin_year")), _
            '                                            CInt(Session("CompanyUnkId")), _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                            CStr(Session("IsArutiDemo")), _
            '                                            CInt(Session("Total_Active_Employee_ForAllCompany")), _
            '                                            CInt(Session("NoOfEmployees")), _
            '                                            CInt(Session("UserId")), False, _
            '                                            CStr(Session("UserAccessModeSetting")), _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
            '                                            CBool(Session("IsIncludeInactiveEmp")), _
            '                                            CBool(Session("AllowToApproveEarningDeduction")), _
            '                            ConfigParameter._Object._CurrentDateAndTime.Date, , , , , _
            '                                            CStr(Session("EmployeeAsOnDate")), , _
            '                            CBool(Session("IsImgInDataBase")))

            Dim blnFlag As Boolean = objEmployee.Update(CStr(Session("Database_Name")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(Session("IsArutiDemo")), _
                                                        CInt(Session("Total_Active_Employee_ForAllCompany")), _
                                                        CInt(Session("NoOfEmployees")), _
                                                        CInt(Session("UserId")), False, _
                                                        CStr(Session("UserAccessModeSetting")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), _
                                                        CBool(Session("AllowToApproveEarningDeduction")), _
                                                    ConfigParameter._Object._CurrentDateAndTime.Date, CBool(Session("CreateADUserFromEmpMst")), _
                                                    CBool(Session("UserMustchangePwdOnNextLogon")), Nothing, Nothing, False, "", _
                                                        CStr(Session("EmployeeAsOnDate")), , _
                                        CBool(Session("IsImgInDataBase")))


            'Pinkal (18-Aug-2018) -- End

            If blnFlag = False Then
                msg.DisplayMessage(objEmployee._Message, Me)
            Else
                If CBool(Session("IsImgInDataBase")) Then
                    DeleteImage()
                End If
                'S.SANDEEP [ 31 DEC 2013 ] -- START
                If Session("ShowPending") IsNot Nothing Then
                    msg.DisplayMessage("Employee Approved successfully!", Me)
                Else
                    msg.DisplayMessage("Information updated successfully!", Me)
                End If
                'S.SANDEEP [ 31 DEC 2013 ] -- END

            End If

            'Shani(20-Nov-2015) -- End

            'Pinkal (27-Mar-2013) -- End

            'S.SANDEEP [ 31 DEC 2013 ] -- START
            If Session("ShowPending") IsNot Nothing Then
                Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                Session("Employeeunkid") = 0
            End If
            'S.SANDEEP [ 31 DEC 2013 ] -- END


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'S.SANDEEP [ 31 DEC 2013 ] -- START
            'Response.Redirect("~\UserHome.aspx", False)
            If Session("ShowPending") IsNot Nothing Then
                Session("ShowPending") = Nothing
                Session("mainfullurl") = Nothing
                Session("iQueryString") = Nothing
                Response.Redirect("~\Index.aspx", False)

            ElseIf IsNothing(Session("IsfromRecruitment")) = False AndAlso CBool(Session("IsfromRecruitment")) = True Then
                Response.Redirect(Session("rootpath").ToString & "Recruitment/wPgEmployeeUpdate.aspx", False)
                Session.Remove("IsfromRecruitment")

            Else
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                'Response.Redirect("~\UserHome.aspx", False)
                Response.Redirect(mstrURLReferer, False)
                'Sohail (09 Nov 2020) -- End
            End If
            'S.SANDEEP [ 31 DEC 2013 ] -- END
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    ' ''Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    ' ''    Try
    ' ''        'S.SANDEEP [ 31 DEC 2013 ] -- START
    ' ''        'Response.Redirect("~\UserHome.aspx", False)
    ' ''        If Session("ShowPending") IsNot Nothing Then
    ' ''            Session("ShowPending") = Nothing
    ' ''            Session("mainfullurl") = Nothing
    ' ''            Session("iQueryString") = Nothing
    ' ''            Response.Redirect("~\Index.aspx")
    ' ''        Else
    ' ''        Response.Redirect("~\UserHome.aspx", False)
    ' ''        End If
    ' ''        'S.SANDEEP [ 31 DEC 2013 ] -- END
    ' ''    Catch ex As Exception
    ' ''        msg.DisplayError(ex, Me)
    ' ''    Finally
    ' ''    End Try
    ' ''End Sub

    'Pinkal (27-Mar-2013) -- Start
    'Enhancement : TRA Changes

    'S.SANDEEP [16-Jan-2018] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 118

    'Gajanan [13-June-2020] -- Start
    'Enhancement NMB Employee Declaration Changes.


    Protected Sub flUploadsig_btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles flUploadsig.btnUpload_Click
        Try
            If flUploadsig.HasFile Then
                Select Case flUploadsig.FileName.Substring(flUploadsig.FileName.LastIndexOf(".") + 1).ToUpper
                    Case "JPG"
                    Case "JPEG"
                    Case "BMP"
                    Case "GIF"
                    Case "PNG"
                    Case Else
                        msg.DisplayMessage("Please select proper Image", Me)
                        Exit Sub
                End Select
                Dim mintByes As Integer = 6291456
                If flUploadsig.FileBytes.LongLength <= mintByes Then   ' 6 MB = 6291456 Bytes
                    Dim mstrPath As String = "../images/" & flUploadsig.FileName
                    flUploadsig.SaveAs(Server.MapPath(mstrPath))
                    imgSignature.ImageUrl = mstrPath
                Else
                    msg.DisplayMessage("Sorry,You cannot upload file greater than " & (mintByes / 1024) / 1024 & " MB.", Me)
                End If
            Else
                msg.DisplayMessage("Please Select the Employee Image.", Me)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnRemoveSig_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveSig.Click
        Try
            If imgSignature.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False AndAlso System.IO.File.Exists(Server.MapPath(imgSignature.ImageUrl)) Then
                Dim mstrFile As String = Server.MapPath(imgSignature.ImageUrl)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Delete_File", "DeleteFile('" & mstrFile.Replace("\", "\\") & "');", True)
            End If
            imgSignature.ImageUrl = ""
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [13-June-2020] -- End
    'S.SANDEEP [16-Jan-2018] -- END

    Protected Sub BtnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles flUpload.btnUpload_Click ' btnUpload.Click
        Try
            If CBool(Session("IsImgInDataBase")) Then
                DeleteImage()
            End If

            If flUpload.HasFile Then

                'Sohail (01 Apr 2013) -- Start
                'TRA - ENHANCEMENT
                Select Case flUpload.FileName.Substring(flUpload.FileName.LastIndexOf(".") + 1).ToUpper
                    Case "JPG"
                    Case "JPEG"
                    Case "BMP"
                    Case "GIF"
                    Case "PNG"
                    Case Else
                        msg.DisplayMessage("Please select proper Image", Me)
                        Exit Sub
                End Select
                'Sohail (01 Apr 2013) -- End

                Dim mintByes As Integer = 6291456
                If flUpload.FileBytes.LongLength <= mintByes Then   ' 6 MB = 6291456 Bytes
                    Dim mstrPath As String = "../images/" & flUpload.FileName
                    flUpload.SaveAs(Server.MapPath(mstrPath))
                    imgEmp.ImageUrl = mstrPath
                Else
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage("Sorry,You cannot upload file greater than " & (mintByes / 1024) / 1024 & " MB.", Me)
                    'Sohail (23 Mar 2019) -- End
                End If
            Else
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(ex, Me)
                msg.DisplayMessage("Please Select the Employee Image.", Me)
                'Sohail (23 Mar 2019) -- End
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (27-Mar-2013) -- End


    'Pinkal (01-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Protected Sub btnRemoveImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveImage.Click
        Try
            'Shani(01-MAR-2017) -- Start
            If imgEmp.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False AndAlso System.IO.File.Exists(Server.MapPath(imgEmp.ImageUrl)) Then
                Dim mstrFile As String = Server.MapPath(imgEmp.ImageUrl)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Delete_File", "DeleteFile('" & mstrFile.Replace("\", "\\") & "');", True)
            End If
            imgEmp.ImageUrl = ""
            'Shani(01-MAR-2017) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (01-Apr-2013) -- End

    'Gajanan [29-Oct-2020] -- Start   
    'Enhancement:Worked On Succession Module
    Protected Sub btnNominateEmployeeSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNominateEmployeeSave.Click
        Dim objEmployee_nomination_master As New clsEmployee_nomination_master
        Dim blnFlag As Boolean = False
        Dim objsucsetting As New clssucsettings_master

        Try

            If CInt(drpNominateEmployeeJob.SelectedValue) <= 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Please select job."), Me)
                Exit Sub
            End If


            Dim mdicSetting As New Dictionary(Of clssucsettings_master.enSuccessionConfiguration, String)

            mdicSetting = objsucsetting.GetSettingFromPeriod()

            If IsNothing(mdicSetting) Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Sorry,Succession Setting Not Available."), Me)
                Exit Sub
            End If

            Dim blnQualification_level As Boolean
            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL) Then
                blnQualification_level = objEmployee_nomination_master.GetEmployeeList(CStr(Session("Database_Name")), _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              CStr(Session("UserAccessModeSetting")), _
                                              True, CBool(Session("IsIncludeInactiveEmp")), _
                                              CInt(Session("Employeeunkid")), _
                                              clssucsettings_master.enSuccessionConfiguration.QUALIFICATION_LEVEL, _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                              "Emp", False)

            Else
                blnQualification_level = True
            End If

            Dim blnPerf_score As Boolean
            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.PERF_SCORE) Then
                blnPerf_score = objEmployee_nomination_master.GetEmployeeList(CStr(Session("Database_Name")), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         CStr(Session("UserAccessModeSetting")), _
                                                         True, CBool(Session("IsIncludeInactiveEmp")), _
                                                         CInt(Session("Employeeunkid")), _
                                                         clssucsettings_master.enSuccessionConfiguration.PERF_SCORE, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         "Emp", False)
            Else
                blnPerf_score = True
            End If

            Dim blnExp_year_no As Boolean
            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO) Then

                blnExp_year_no = objEmployee_nomination_master.GetEmployeeList(CStr(Session("Database_Name")), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         CStr(Session("UserAccessModeSetting")), _
                                                         True, CBool(Session("IsIncludeInactiveEmp")), _
                                                         CInt(Session("Employeeunkid")), _
                                                         clssucsettings_master.enSuccessionConfiguration.EXP_YEAR_NO, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         "Emp", False)

            Else
                blnExp_year_no = True
            End If

            Dim blnMinAge As Boolean
            If mdicSetting.ContainsKey(clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO) Then

                blnMinAge = objEmployee_nomination_master.GetEmployeeList(CStr(Session("Database_Name")), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         CStr(Session("UserAccessModeSetting")), _
                                                         True, CBool(Session("IsIncludeInactiveEmp")), _
                                                         CInt(Session("Employeeunkid")), _
                                                         clssucsettings_master.enSuccessionConfiguration.MAX_AGE_NO, _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                         "Emp", False)

            Else
                blnMinAge = True
            End If


            If blnQualification_level = False OrElse blnPerf_score = False OrElse blnExp_year_no = False OrElse blnMinAge = False Then

                If blnQualification_level Then
                    lblQualificationNominationCheckTrue.Visible = True
                Else
                    lblQualificationNominationCheckFalse.Visible = True
                End If

                If blnPerf_score Then
                    lblPerfNominationCheckTrue.Visible = True
                Else
                    lblPerfNominationCheckFalse.Visible = True
                End If



                If blnExp_year_no Then
                    lblExpNominationCheckTrue.Visible = True
                Else
                    lblExpNominationCheckFalse.Visible = True
                End If

                If blnMinAge Then
                    lblMinimumAgeCheckTrue.Visible = True
                Else
                    lblMinimumAgeCheckFalse.Visible = True
                End If


                popupNominationCancleReason.Show()
                Exit Sub
            End If



            SetNominationValue(objEmployee_nomination_master)

            blnFlag = objEmployee_nomination_master.Insert(Nothing)

            If blnFlag = False AndAlso objEmployee_nomination_master._Message.Trim.Length > 0 Then
                msg.DisplayMessage(objEmployee_nomination_master._Message, Me)
            Else
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1001, "Employee nominated successfully for selected job."), Me)
                FillNomination()
            End If

            objEmployee_nomination_master.Insert()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objEmployee_nomination_master = Nothing
            objsucsetting = Nothing
        End Try
    End Sub

    Protected Sub btnNominateEmployeeClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNominateEmployeeClose.Click
        Try
            blnpopupNominateEmployee = False
            popupNominateEmployee.Hide()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnNominationCancleReasonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNominationCancleReasonClose.Click
        Try
            lblQualificationNominationCheckFalse.Visible = False
            lblExpNominationCheckFalse.Visible = False
            lblMinimumAgeCheckFalse.Visible = False
            lblPerfNominationCheckFalse.Visible = False

            lblQualificationNominationCheckTrue.Visible = False
            lblExpNominationCheckTrue.Visible = False
            lblMinimumAgeCheckTrue.Visible = False
            lblPerfNominationCheckTrue.Visible = False


            popupNominationCancleReason.Hide()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub



    'Gajanan [29-Oct-2020] -- End


#End Region

#Region " Links Events "


    Protected Sub lnkViewAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewAddress.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If
            Response.Redirect("~\HR\wPgEmployeeAddress.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkViewEmergency_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewEmergency.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If
            'Hemant (01 Jan 2021) -- Start
            'Enhancement #OLD-238 - Improved Employee Profile page
            Session("ReturnURL") = Request.Url.AbsoluteUri
            'Hemant (01 Jan 2021) -- End
            Response.Redirect("~\HR\wPgEmpEmerAddress.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkViewPersonal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewPersonal.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If
            'Hemant (01 Jan 2021) -- Start
            'Enhancement #OLD-238 - Improved Employee Profile page
            Session("ReturnURL") = Request.Url.AbsoluteUri
            'Hemant (01 Jan 2021) -- End
            Response.Redirect("~\HR\wPgEmployeePersonal.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'Gajanan [29-Oct-2020] -- Start   
    'Enhancement:Worked On Succession Module
    Protected Sub lnkNominateEmp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNominateEmp.Click
        Dim objemp As New clsEmployee_Master
        Dim objEmployee_nomination_master As New clsEmployee_nomination_master
        Try
            'Gajanan [26-Feb-2021] -- Start

            'If CInt(Session("Employeeunkid")) > 0 Then
            '    objemp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            '    txtNominateEmployee.Text = objemp._Firstname + " " + objemp._Surname

            '    blnpopupNominateEmployee = True
            '    popupNominateEmployee.Show()
            '    FillNomination()
            'End If

            Response.Redirect(Session("rootpath").ToString & "Talent_Succession\wpg_Succession_Nomination.aspx", False)
            'Gajanan [26-Feb-2021] -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objemp = Nothing
        End Try
    End Sub

    Protected Sub lnkNominateEmployeeDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objtlscreening_process_master As New clstlscreening_process_master
        Try
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            mintNominationunkid = CInt(dgvNominateEmployee.DataKeys(row.RowIndex)("nominationunkid"))

            delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 10, "Are you sure you want to delete? Please Specify the delete reason if you want to delete this data then.")
            delReason.Show()
            Exit Sub

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objtlscreening_process_master = Nothing
        End Try
    End Sub
    'Gajanan [29-Oct-2020] -- End

    Protected Sub lnklblQualification_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnklblQualification.Click
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If
            Response.Redirect("~\HR\wPgEmpQualification.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkQuickLink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMySkills.Click, lnkMyIdentities.Click, _
                                                                                                  lnkMyMemberships.Click, lnkMyExperiences.Click, _
                                                                                                  lnkMyReferences.Click, lnkMyBenefits.Click, _
                                                                                                  lnkMyAssignedAssets.Click, lnkMyQualification.Click, _
                                                                                                  lnkMyDependent.Click

        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then

                Dim btnquickLink As LinkButton = TryCast(sender, LinkButton)

                Select Case btnquickLink.ID
                    Case lnkMySkills.ID
                        Response.Redirect(Session("rootpath").ToString & "HR\wPg_EmployeeSkillList.aspx", False)
                    Case lnkMyIdentities.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpIdentities_List.aspx", False)
                    Case lnkMyMemberships.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpMembership_List.aspx", False)
                    Case lnkMyExperiences.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPg_EmployeeExperienceList.aspx", False)
                    Case lnkMyReferences.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpRefereeList.aspx", False)
                    Case lnkMyBenefits.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgBenefitList.aspx", False)
                    Case lnkMyAssignedAssets.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPg_AssetsRegisterList.aspx", False)
                    Case lnkMyQualification.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpQualificationList.aspx", False)
                    Case lnkMyDependent.ID
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgDependantsList.aspx", False)
                End Select
                Session.Add("Isfromprofile", True)

            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region "Control Event"
    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Dim objEmployee_nomination_master As New clsEmployee_nomination_master
            SetATNominationValue(objEmployee_nomination_master)
            objEmployee_nomination_master._Isvoid = True
            objEmployee_nomination_master._Voidreason = delReason.Reason
            objEmployee_nomination_master._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objEmployee_nomination_master._Voiduserunkid = CInt(Session("UserId"))
            blnFlag = objEmployee_nomination_master.Delete(mintNominationunkid)
            If blnFlag = False AndAlso objEmployee_nomination_master._Message.Trim.Length > 0 Then
                msg.DisplayMessage(objEmployee_nomination_master._Message, Me)
            Else
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Screener deleted successfully."), Me)
                FillNomination()
            End If
            objEmployee_nomination_master = Nothing
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

    'Pinkal (03-Sep-2013) -- Start
    'Enhancement : TRA Changes
#Region "Gridview Events"

    Protected Sub GvReportingTo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GvAssessorReviewer.RowDataBound, _
                                                                                                                                        GvLeaveApprover.RowDataBound, _
                                                                                                                                        GvReportingTo.RowDataBound, _
                                                                                                                                        GvClaimApprover.RowDataBound, _
                                                                                                                                        GvOTApprover.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                If e.Row.Cells(0).Text = "&nbsp;" AndAlso e.Row.Cells(1).Text = "&nbsp;" AndAlso e.Row.Cells(2).Text = "&nbsp;" AndAlso e.Row.Cells(3).Text = "&nbsp;" Then
                    e.Row.Visible = False
                Else
                    If e.Row.Cells(0).Text = "Assessor" OrElse e.Row.Cells(0).Text = "Reviewer" Then
                        e.Row.Cells(0).Font.Bold = True
                        e.Row.Cells(1).Visible = False
                        e.Row.Cells(2).Visible = False
                        e.Row.Cells(3).Visible = False
                        e.Row.Cells(0).ColumnSpan = e.Row.Cells.Count

                        'Pinkal (05-May-2020) -- Start
                        'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .
                    ElseIf e.Row.Cells(0).Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsExpCommonMethods", 2, "Leave") _
                             OrElse e.Row.Cells(0).Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsExpCommonMethods", 3, "Medical") OrElse e.Row.Cells(0).Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsExpCommonMethods", 8, "Miscellaneous") _
                             OrElse e.Row.Cells(0).Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsExpCommonMethods", 4, "Training") OrElse e.Row.Cells(0).Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsExpCommonMethods", 9, "Imprest") Then
                        e.Row.Cells(0).Font.Bold = True
                        e.Row.Cells(1).Visible = False
                        e.Row.Cells(2).Visible = False
                        e.Row.Cells(3).Visible = False
                        e.Row.Cells(4).Visible = False
                        e.Row.Cells(0).ColumnSpan = e.Row.Cells.Count
                        'Pinkal (05-May-2020) -- End
                    End If
                End If

            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetDateDiff(ByVal dtFromDate As Date, ByVal dtToDate As Date, ByRef intYears As Integer, ByRef intMonths As Integer, ByRef intDays As Integer)
        Try
            intYears = CInt(Math.Floor(DateDiff(DateInterval.Day, dtFromDate, dtToDate) / 365))
            intMonths = CInt(Math.Floor(DateDiff(DateInterval.Day, dtFromDate.AddYears(intYears), dtToDate) / 30))
            intDays = CInt(DateDiff(DateInterval.Day, dtFromDate.AddYears(intYears).AddMonths(intMonths), dtToDate))
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub



#End Region

    'Hemant (01 Jan 2021) -- Start
    'Enhancement #OLD-238 - Improved Employee Profile page
#Region "Employee Dates"
#Region " Private Function(s) & Method(s) "
    Private Sub FillEmployeeDatesCombo()
        Dim dsCombos As New DataSet
        'Hemant (03 May 2021) -- Start
        'ISSUE : Asset Declaration Report Showing First Year Declaration Data.
        'Dim objAction As New clsAction_Reason
        Dim objCommon As New clsCommon_Master
        'Hemant (15 Apr 2021) -- End

        Try
            'Hemant (03 May 2021) -- Start
            'dsCombos = objAction.getComboList("Reason", True, False)
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.TERMINATION, True, "Reason")
            Dim dshire As New DataSet
            dshire = objCommon.getComboList(clsCommon_Master.enCommonMaster.RE_HIRE, False, "Reason")
            dsCombos.Tables("Reason").Merge(dshire.Tables("Reason"), True)
            'Hemant (15 Apr 2021) -- End

            With cboReason
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Reason")
                .DataBind()
            End With

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            'Hemant (03 May 2021) -- Start
            'ISSUE : Asset Declaration Report Showing First Year Declaration Data.
            'objAction = Nothing
            objCommon = Nothing
            'Hemant (15 Apr 2021) -- End
        End Try
    End Sub

    Private Sub SetEmployeeDatesInfo(ByVal objEmp As clsEmployee_Master)
        Try
            'S.SANDEEP |01-DEC-2021| -- START
            'cboReason.SelectedValue = CStr(objEmployee._Actionreasonunkid)
            Try
                cboReason.SelectedValue = CStr(objEmployee._Actionreasonunkid)
            Catch ex As Exception
                cboReason.SelectedValue = "0"
            End Try
            'S.SANDEEP |01-DEC-2021| -- END
            If objEmployee._Appointeddate <> Nothing Then
                dtpAppointDate.SetDate = objEmployee._Appointeddate.Date
            Else
                dtpAppointDate.SetDate = Nothing
            End If

            If objEmployee._Confirmation_Date <> Nothing Then
                dtpConfDate.SetDate = objEmployee._Confirmation_Date.Date
            Else
                dtpConfDate.SetDate = Nothing
            End If

            If objEmployee._Birthdate <> Nothing Then
                dtpBirthdate.SetDate = objEmployee._Birthdate.Date
            Else
                dtpBirthdate.SetDate = Nothing
            End If

            If objEmployee._Suspende_From_Date <> Nothing Then
                dtpSuspFrom.SetDate = objEmployee._Suspende_From_Date.Date
            Else
                dtpSuspTo.SetDate = Nothing
            End If

            If objEmployee._Suspende_To_Date <> Nothing Then
                dtpSuspTo.SetDate = objEmployee._Suspende_To_Date.Date
            Else
                dtpSuspTo.SetDate = Nothing
            End If

            If objEmployee._Probation_From_Date <> Nothing Then
                dtpProbFrom.SetDate = objEmployee._Probation_From_Date.Date
            Else
                dtpProbFrom.SetDate = Nothing
            End If

            If objEmployee._Probation_To_Date <> Nothing Then
                dtpProbTo.SetDate = objEmployee._Probation_To_Date.Date
            Else
                dtpProbTo.SetDate = Nothing
            End If

            If objEmployee._Empl_Enddate <> Nothing Then
                dtpEmplEndDate.SetDate = objEmployee._Empl_Enddate.Date
            Else
                dtpEmplEndDate.SetDate = Nothing
            End If

            If objEmployee._Termination_From_Date <> Nothing Then
                dtpLeavingDate.SetDate = objEmployee._Termination_From_Date.Date
            Else
                dtpLeavingDate.SetDate = Nothing
            End If

            If objEmployee._Termination_To_Date <> Nothing Then
                dtpRetrDate.SetDate = objEmployee._Termination_To_Date.Date
            Else
                dtpRetrDate.SetDate = Nothing
            End If

            If objEmployee._Reinstatementdate <> Nothing Then
                dtpReinstatement.SetDate = objEmployee._Reinstatementdate.Date
            Else
                dtpReinstatement.SetDate = Nothing
            End If

            If objEmployee._Isapproved Then
                dtpAppointDate.Enabled = False
                dtpBirthdate.Enabled = False
                dtpConfDate.Enabled = False
                dtpEmplEndDate.Enabled = False
                dtpLeavingDate.Enabled = False
                dtpProbFrom.Enabled = False
                dtpProbTo.Enabled = False
                dtpRetrDate.Enabled = False
                dtpSuspFrom.Enabled = False
                dtpSuspTo.Enabled = False
                dtpReinstatement.Enabled = False
                cboReason.Enabled = False
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
#End Region
#End Region

#Region "Employee Allocation"

#Region " Private Function(s) & Method(s) "

    Private Sub FillAllocationCombo()
        Dim dsCombos As New DataSet
        Dim objStation As New clsStation
        Dim objDeptGrp As New clsDepartmentGroup
        Dim objDepartment As New clsDepartment
        Dim objSection As New clsSections
        Dim objUnit As New clsUnits
        Dim objJobGrp As New clsJobGroup
        Dim objJob As New clsJobs
        Dim objGradeGrp As New clsGradeGroup
        Dim objClassGrp As New clsClassGroup
        Dim objCostCenter As New clscostcenter_master
        Dim objSectionGrp As New clsSectionGroup
        Dim objUnitGroup As New clsUnitGroup
        Dim objTeam As New clsTeams
        Try
            dsCombos = objStation.getComboList("Station", True)
            With cboBranch
                .DataValueField = "stationunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Station")
                .DataBind()
            End With
            dsCombos = objDeptGrp.getComboList("DeptGrp", True)
            With cboDeptGrp
                .DataValueField = "deptgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("DeptGrp")
                .DataBind()
            End With
            dsCombos = objDepartment.getComboList("Department", True)
            With cboDepartment
                .DataValueField = "departmentunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Department")
                .DataBind()
            End With

            dsCombos = objSectionGrp.getComboList("List", True)
            With cboSecGroup
                .DataValueField = "sectiongroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = objSection.getComboList("Section", True)
            With cboSection
                .DataValueField = "sectionunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Section")
                .DataBind()
            End With

            dsCombos = objUnitGroup.getComboList("List", True)
            With cboUnitGrp
                .DataValueField = "unitgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = objUnit.getComboList("Unit", True)
            With cboUnit
                .DataValueField = "unitunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Unit")
                .DataBind()
            End With

            dsCombos = objTeam.getComboList("List", True)
            With cboTeam
                .DataValueField = "teamunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = objJobGrp.getComboList("JobGrp", True)
            With cboJobGroup
                .DataValueField = "jobgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("JobGrp")
                .DataBind()
            End With
            dsCombos = objJob.getComboList("Job", True)
            With cboJob
                .DataValueField = "jobunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Job")
                .DataBind()
            End With

            dsCombos = objGradeGrp.getComboList("GradeGrp", True)
            With cboGradeGrp
                .DataValueField = "gradegroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("GradeGrp")
                .DataBind()
            End With

            dsCombos = objClassGrp.getComboList("ClassGrp", True)
            With cboClassGrp
                .DataValueField = "classgroupunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("ClassGrp")
                .DataBind()
            End With

            dsCombos = objCostCenter.getComboList("CostCenter", True)
            With cboCostcenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                .DataSource = dsCombos.Tables("CostCenter")
                .DataBind()
            End With

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetAllocationInfo(ByVal objEmp As clsEmployee_Master)
        Try
            If CInt(cboEmployee.SelectedValue) > 0 Then


                cboBranch.SelectedValue = CStr(objEmp._Stationunkid)
                Call cboBranch_SelectedIndexChanged(Nothing, Nothing)

                cboDeptGrp.SelectedValue = objEmp._Deptgroupunkid.ToString
                Call cboDeptGrp_SelectedIndexChanged(Nothing, Nothing)

                cboDepartment.SelectedValue = objEmp._Departmentunkid.ToString
                Call cboDepartment_SelectedIndexChanged(Nothing, Nothing)

                cboSecGroup.SelectedValue = objEmp._Sectiongroupunkid.ToString
                Call cboSectionGroup_SelectedIndexChanged(Nothing, Nothing)

                cboSection.SelectedValue = objEmp._Sectionunkid.ToString
                Call cboSection_SelectedIndexChanged(Nothing, Nothing)

                cboUnitGrp.SelectedValue = objEmp._Unitgroupunkid.ToString
                Call cboUnitGroup_SelectedIndexChanged(Nothing, Nothing)

                cboUnit.SelectedValue = objEmp._Unitunkid.ToString
                Call cboUnit_SelectedIndexChanged(Nothing, Nothing)

                cboTeam.SelectedValue = objEmp._Teamunkid.ToString
                cboJobGroup.SelectedValue = objEmp._Jobgroupunkid.ToString
                cboJob.SelectedValue = objEmp._Jobunkid.ToString
                cboGradeGrp.SelectedValue = objEmp._Gradegroupunkid.ToString
                Call cboGradeGrp_SelectedIndexChanged(Nothing, Nothing)
                cboGrade.SelectedValue = objEmp._Gradeunkid.ToString
                Call cboGrade_SelectedIndexChanged(Nothing, Nothing)
                cboGradeLevel.SelectedValue = objEmp._Gradelevelunkid.ToString
                Call cboGradeLevel_SelectedIndexChanged(Nothing, Nothing)
                cboClassGrp.SelectedValue = objEmp._Classgroupunkid.ToString
                Call cboClassGrp_SelectedIndexChanged(Nothing, Nothing)
                cboClass.SelectedValue = objEmp._Classunkid.ToString
                cboCostcenter.SelectedValue = objEmp._Costcenterunkid.ToString


                If objEmp._Isapproved Then
                    cboBranch.Enabled = False
                    cboDeptGrp.Enabled = False
                    cboDepartment.Enabled = False
                    cboSecGroup.Enabled = False
                    cboSection.Enabled = False
                    cboUnitGrp.Enabled = False
                    cboUnit.Enabled = False
                    cboTeam.Enabled = False
                    cboClassGrp.Enabled = False
                    cboClass.Enabled = False
                    cboJobGroup.Enabled = False
                    cboJob.Enabled = False
                    cboCostcenter.Enabled = False
                    cboGradeGrp.Enabled = False
                    cboGrade.Enabled = False
                    cboGradeLevel.Enabled = False

                    If CInt(cboBranch.SelectedValue) <= 0 Then cboBranch.SelectedItem.Text = ""
                    If CInt(cboDeptGrp.SelectedValue) <= 0 Then cboDeptGrp.SelectedItem.Text = ""
                    If CInt(cboDepartment.SelectedValue) <= 0 Then cboDepartment.SelectedItem.Text = ""
                    If CInt(cboSecGroup.SelectedValue) <= 0 Then cboSecGroup.SelectedItem.Text = ""
                    If CInt(cboSection.SelectedValue) <= 0 Then cboSection.SelectedItem.Text = ""
                    If CInt(cboUnitGrp.SelectedValue) <= 0 Then cboUnitGrp.SelectedItem.Text = ""
                    If CInt(cboUnit.SelectedValue) <= 0 Then cboUnit.SelectedItem.Text = ""
                    If CInt(cboTeam.SelectedValue) <= 0 Then cboTeam.SelectedItem.Text = ""
                    If CInt(cboClassGrp.SelectedValue) <= 0 Then cboClassGrp.SelectedItem.Text = ""
                    If CInt(cboClass.SelectedValue) <= 0 Then cboClass.SelectedItem.Text = ""
                End If

                If Session("CompCode").ToString.ToUpper = "NMB" Then
                    pnlgrade.Visible = False
                    pnlgradegrp.Visible = False
                Else
                    pnlgrade.Visible = True
                    pnlgradegrp.Visible = True
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Protected Sub cboGradeGrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGradeGrp.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objGrade As New clsGrade
        Try
            If CInt(IIf(cboGradeGrp.SelectedValue = "", 0, cboGradeGrp.SelectedValue)) > 0 Then
                dsCombos = objGrade.getComboList("Grade", True, CInt(IIf(cboGradeGrp.SelectedValue = "", 0, cboGradeGrp.SelectedValue)))
                With cboGrade
                    .DataValueField = "gradeunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("Grade")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboGrade_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGrade.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Dim objGradeLevel As New clsGradeLevel
        Try
            If CInt(IIf(cboGrade.SelectedValue = "", 0, cboGrade.SelectedValue)) > 0 Then
                dsCombos = objGradeLevel.getComboList("GradeLevel", True, CInt(IIf(cboGrade.SelectedValue = "", 0, cboGrade.SelectedValue)))
                With cboGradeLevel
                    .DataValueField = "gradelevelunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("GradeLevel")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboGradeLevel_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGradeLevel.SelectedIndexChanged
        Dim objWagesTran As New clsWagesTran
        Dim decScale As Decimal = 0 'Sohail (11 May 2011)
        Dim objMaster As New clsMasterData
        Dim objWages As New clsWagesTran
        Dim dsList As DataSet
        Try
            objWagesTran.GetSalary(CInt(IIf(cboGradeGrp.SelectedValue = "", 0, cboGradeGrp.SelectedValue)), _
                               CInt(IIf(cboGrade.SelectedValue = "", 0, cboGrade.SelectedValue)), _
                               CInt(IIf(cboGradeLevel.SelectedValue = "", 0, cboGradeLevel.SelectedValue)), _
                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                               decScale)
            dsList = objMaster.Get_Current_Scale("Salary", CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)), ConfigParameter._Object._CurrentDateAndTime.Date)

            If dsList.Tables("Salary").Rows.Count > 0 Then
                decScale = CDec(dsList.Tables("Salary").Rows(0).Item("newscale").ToString)
            End If

            txtScale.Text = CStr(Format(decScale, CStr(Session("fmtCurrency"))))

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.DEPARTMENT_GROUP)) = True Then
                Dim objDeptGrp As New clsDepartmentGroup
                Dim dsCombos As New DataSet

                dsCombos = objDeptGrp.getComboList(CInt(cboBranch.SelectedValue), "DeptGrp", True)
                With cboDeptGrp
                    .DataValueField = "deptgroupunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("DeptGrp")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboDeptGrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDeptGrp.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.DEPARTMENT)) = True Then
                Dim objDepartment As New clsDepartment
                Dim dsCombos As New DataSet

                dsCombos = objDepartment.getComboList(CInt(cboDeptGrp.SelectedValue), "Department", True)
                With cboDepartment
                    .DataValueField = "departmentunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("Department")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboDepartment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepartment.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.SECTION_GROUP)) = True Then
                Dim objSectionGrp As New clsSectionGroup
                Dim dsCombos As New DataSet

                dsCombos = objSectionGrp.getComboList(CInt(cboDepartment.SelectedValue), "List", True)
                With cboSecGroup
                    .DataValueField = "sectiongroupunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboSectionGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSecGroup.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.SECTION)) = True Then
                Dim objSection As New clsSections
                Dim dsCombos As New DataSet

                dsCombos = objSection.getComboList(CInt(cboSecGroup.SelectedValue), "Section", True)
                With cboSection
                    .DataValueField = "sectionunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("Section")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboSection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboSection.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.UNIT_GROUP)) = True Then
                Dim objUnitGroup As New clsUnitGroup
                Dim dsCombos As New DataSet

                dsCombos = objUnitGroup.getComboList("List", True, CInt(cboSection.SelectedValue))
                With cboUnitGrp
                    .DataValueField = "unitgroupunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboUnitGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUnitGrp.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.UNIT)) = True Then
                Dim dsCombos As New DataSet
                Dim objUnit As New clsUnits
                dsCombos = objUnit.getComboList("Unit", True, , CInt(cboUnitGrp.SelectedValue))
                With cboUnit
                    .DataValueField = "unitunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("Unit")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboUnit_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboUnit.SelectedIndexChanged
        Try
            If CBool(Session("IsAllocation_Hierarchy_Set")) = True AndAlso Session("Allocation_Hierarchy").ToString.Contains(CStr(enAllocation.TEAM)) = True Then
                Dim dsCombos As New DataSet
                Dim objTeam As New clsTeams
                dsCombos = objTeam.getComboList("List", True, CInt(cboUnit.SelectedValue))
                With cboTeam
                    .DataValueField = "teamunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboClassGrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboClassGrp.SelectedIndexChanged
        Try
            Dim dsCombos As New DataSet
            Dim objclass As New clsClass
            dsCombos = objclass.getComboList("Classgrp", True, CInt(cboClassGrp.SelectedValue))
            With cboClass
                .DataValueField = "classesunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Classgrp")
                .DataBind()
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "Dependants"

#Region " Private Function(s) & Method(s) "
    Private Sub FillDependantsGrid()
        Dim dsList As New DataSet
        Dim dTable As DataTable
        Dim StrSearching As String = ""
        Dim objDependant As New clsDependants_Beneficiary_tran
        Dim objADependantTran As New clsDependant_beneficiaries_approval_tran
        Dim StrApproverSearching As String = String.Empty
        Try

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("ViewDependant")) = False Then Exit Sub
            End If


            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND hrdependants_beneficiaries_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If


            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If


            If DependantApprovalFlowVal Is Nothing Then
                If CInt(cboEmployee.SelectedValue) > 0 Then
                    StrApproverSearching &= "AND hrdependant_beneficiaries_approval_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If StrSearching.Length > 0 Then
                    StrApproverSearching = StrApproverSearching.Substring(3)
                End If
            End If

            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If Session("PendingEmployeeScreenIDs").ToString.Trim.Length > 0 Then
                If Session("PendingEmployeeScreenIDs").ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmDependantsAndBeneficiariesList))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If


            dsList = objDependant.GetList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          CBool(Session("IsIncludeInactiveEmp")), "List", , CInt(cboEmployee.SelectedValue), StrSearching, _
                                        CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)), mblnAddApprovalCondition, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString))

            Dim dcol As New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "tranguid"
                .DefaultValue = ""
            End With
            dsList.Tables(0).Columns.Add(dcol)

            If DependantApprovalFlowVal Is Nothing Then
                Dim dsPending As New DataSet
                dsPending = objADependantTran.GetList(FinancialYear._Object._DatabaseName, _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          CStr(Session("UserAccessModeSetting")), True, _
                                          CBool(Session("IsIncludeInactiveEmp")), "Emp", , , StrApproverSearching, , mblnAddApprovalCondition)

                If dsPending.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In dsPending.Tables(0).Rows
                        dsList.Tables(0).ImportRow(row)
                    Next
                End If
            End If

            dTable = New DataView(dsList.Tables(0), "", "EmpName", DataViewRowState.CurrentRows).ToTable

            If dTable IsNot Nothing Then
                dgvDependantList.DataSource = dTable
                dgvDependantList.DataKeyField = "DpndtTranId"
                dgvDependantList.DataBind()
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            objDependant = Nothing
            objADependantTran = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Event(s) "
    Protected Sub btnManageDependant_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnManageDependant.Click
        Try
            Session("ReturnURL") = Request.Url.AbsoluteUri
            Response.Redirect(Session("rootpath").ToString & "HR\wPgDependantsList.aspx", False)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Gridview's Event(s)"
    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvDependantList.ItemDataBound
        Try

            Call SetDateFormat()
            If (e.Item.ItemIndex >= 0) Then
                If e.Item.Cells(3).Text <> "&nbsp;" AndAlso e.Item.Cells(3).Text.ToString.Trim.Length > 0 Then
                    e.Item.Cells(3).Text = eZeeDate.convertDate(e.Item.Cells(3).Text).Date.ToShortDateString
                End If
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#End Region

#Region "My Reporting"
#Region " Private Function(s) & Method(s) "
    Private Sub FillMyReportingGrids()
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                If CInt(cboEmployee.SelectedValue) <= 0 Then
                    msg.DisplayMessage("Please select Employee to continue.", Me)
                    Exit Sub
                End If
            End If

            FillAssessorReviewerGrid()
            FillLeaveApproverGrid()
            FillReportingToGrid()
            FillClaimApproverGrid()
            FillOTApproverGrid()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillAssessorReviewerGrid()
        Try

            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Assessor_Reviewer, CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))

            GvAssessorReviewer.PageIndex = 0
            GvAssessorReviewer.DataSource = dsList.Tables(0)
            GvAssessorReviewer.DataBind()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillLeaveApproverGrid()
        Try

            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Leave_Approver, CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))

            GvLeaveApprover.PageIndex = 0
            GvLeaveApprover.DataSource = dsList.Tables(0)
            GvLeaveApprover.DataBind()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillReportingToGrid()
        Try

            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))


            dsList.Tables(0).Columns.Remove("Email")
            dsList.Tables(0).Columns.Remove("ishierarchy")
            dsList.Tables(0).Columns.Remove("reporttoemployeeunkid")
            dsList.Tables(0).Columns.Remove("dEmployee")

            GvReportingTo.PageIndex = 0
            GvReportingTo.DataSource = dsList.Tables(0)
            GvReportingTo.DataBind()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillClaimApproverGrid()
        Try

            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.Claim_Approver, CInt(cboEmployee.SelectedValue) _
                                                                                                , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date _
                                                                                                , CStr(Session("Database_Name")), CBool(Session("PaymentApprovalwithLeaveApproval")))

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("ExpCategroyId") = enExpenseType.EXP_LEAVE)
                If drRow IsNot Nothing AndAlso drRow.Count <= 1 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                    dsList.Tables(0).AcceptChanges()
                End If

                drRow = Nothing
                drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("ExpCategroyId") = enExpenseType.EXP_MEDICAL)
                If drRow IsNot Nothing AndAlso drRow.Count <= 1 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                    dsList.Tables(0).AcceptChanges()
                End If

                drRow = Nothing
                drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("ExpCategroyId") = enExpenseType.EXP_MISCELLANEOUS)
                If drRow IsNot Nothing AndAlso drRow.Count <= 1 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                    dsList.Tables(0).AcceptChanges()
                End If

                drRow = Nothing
                drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("ExpCategroyId") = enExpenseType.EXP_TRAINING)
                If drRow IsNot Nothing AndAlso drRow.Count <= 1 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                    dsList.Tables(0).AcceptChanges()
                End If

                drRow = Nothing
                drRow = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("ExpCategroyId") = enExpenseType.EXP_IMPREST)
                If drRow IsNot Nothing AndAlso drRow.Count <= 1 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                    dsList.Tables(0).AcceptChanges()
                End If

                If dsList.Tables(0).Columns.Contains("ExpCategroyId") Then
                    dsList.Tables(0).Columns.Remove("ExpCategroyId")
                    dsList.Tables(0).AcceptChanges()
                End If
            End If

            GvClaimApprover.PageIndex = 0
            GvClaimApprover.DataSource = dsList.Tables(0)
            GvClaimApprover.DataBind()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillOTApproverGrid()
        Try

            Dim dsList As DataSet = objEmployee.GetEmployeeReporting(enEmployeeReporting.OT_Approver, CInt(cboEmployee.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("Database_Name")))

            GvOTApprover.PageIndex = 0
            GvOTApprover.DataSource = dsList.Tables(0)
            GvOTApprover.DataBind()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region
    'Hemant (01 Jan 2021) -- End

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private Sub SetLnaguae()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)
            'Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)

            Me.lblGender.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGender.ID, Me.lblGender.Text)
            Me.lblTitle.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTitle.ID, Me.lblTitle.Text)
            Me.lblEmployeeCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployeeCode.ID, Me.lblEmployeeCode.Text)
            Me.lblOthername.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblOthername.ID, Me.lblOthername.Text)
            Me.lblFirstName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFirstName.ID, Me.lblFirstName.Text)
            Me.lblSurname.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSurname.ID, Me.lblSurname.Text)
            Me.lblIdNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIdNo.ID, Me.lblIdNo.Text)
            Me.lblShift.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblShift.ID, Me.lblShift.Text)
            Me.lblPayType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPayType.ID, Me.lblPayType.Text)
            Me.lblPaypoint.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPaypoint.ID, Me.lblPaypoint.Text)
            Me.lblEmploymentType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmploymentType.ID, Me.lblEmploymentType.Text)
            Me.lblEmail.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmail.ID, Me.lblEmail.Text)

            Me.lbllnkViewAddress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbAddress", Me.lbllnkViewAddress.Text)
            Me.lbllnkViewEmergency.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "tabpEmergencyContact", Me.lbllnkViewEmergency.Text)

            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")

            'Language.setLanguage(mstrModuleName1)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)

            'S.SANDEEP |18-JUN-2021| -- START
            'ISSUE/ENHANCEMENT : LANGUAGE CHANGES ARE NOT KEPT
            Me.lblBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblBranch.ID, Me.lblBranch.Text)
            Me.lblCostCenter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblCostCenter.ID, Me.lblCostCenter.Text)
            Me.lblDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblDepartment.ID, Me.lblDepartment.Text)
            Me.lblClass.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblClass.ID, Me.lblClass.Text)
            Me.lblDepartmentGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblDepartmentGroup.ID, Me.lblDepartmentGroup.Text)
            Me.lblJobGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblJobGroup.ID, Me.lblJobGroup.Text)
            Me.lblUnits.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblUnits.ID, Me.lblUnits.Text)
            Me.lblSection.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblSection.ID, Me.lblSection.Text)
            Me.lblClassGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblClassGroup.ID, Me.lblClassGroup.Text)
            Me.lblJob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblJob.ID, Me.lblJob.Text)
            Me.lblScale.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblScale.ID, Me.lblScale.Text)
            Me.lblGrade.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblGrade.ID, Me.lblGrade.Text)
            Me.lblGradeGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblGradeGroup.ID, Me.lblGradeGroup.Text)
            Me.lblTeam.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblTeam.ID, Me.lblTeam.Text)
            Me.lblUnitGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblUnitGroup.ID, Me.lblUnitGroup.Text)
            Me.lblSectionGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblSectionGroup.ID, Me.lblSectionGroup.Text)
            Me.lblGradeLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblGradeLevel.ID, Me.lblGradeLevel.Text)
            'S.SANDEEP |18-JUN-2021| -- END

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub
    'Pinkal (06-May-2014) -- End


    'Pinkal (03-Sep-2013) -- End



    '#Region " Private Function(s) & Method(s) "

    '    Private Sub FillCombo()
    '        Dim dsCombos As New DataSet
    '        Dim objCommon As New clsCommon_Master
    '        Dim objMaster As New clsMasterData
    '        Dim objCity As New clscity_master
    '        Dim objState As New clsstate_master
    '        Dim objZipCode As New clszipcode_master
    '        Try

    '            RemoveHandler cboPCountry.SelectedIndexChanged, AddressOf cboPCountry_SelectedIndexChanged
    '            dsCombos = objMaster.getCountryList("List", True)
    '            With cboPCountry
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboPCountry.SelectedIndexChanged, AddressOf cboPCountry_SelectedIndexChanged

    '            RemoveHandler cboDCountry.SelectedIndexChanged, AddressOf cboDCountry_SelectedIndexChanged
    '            With cboDCountry
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboDCountry.SelectedIndexChanged, AddressOf cboDCountry_SelectedIndexChanged

    '            RemoveHandler cboECountry.SelectedIndexChanged, AddressOf cboECountry_SelectedIndexChanged
    '            With cboECountry
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboECountry.SelectedIndexChanged, AddressOf cboECountry_SelectedIndexChanged

    '            RemoveHandler cboECountry2.SelectedIndexChanged, AddressOf cboECountry2_SelectedIndexChanged
    '            With cboECountry2
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboECountry2.SelectedIndexChanged, AddressOf cboECountry2_SelectedIndexChanged

    '            RemoveHandler cboECountry3.SelectedIndexChanged, AddressOf cboECountry3_SelectedIndexChanged
    '            With cboECountry3
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboECountry3.SelectedIndexChanged, AddressOf cboECountry3_SelectedIndexChanged


    '            RemoveHandler cboBirthCounty.SelectedIndexChanged, AddressOf cboBirthCounty_SelectedIndexChanged
    '            With cboBirthCounty
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboBirthCounty.SelectedIndexChanged, AddressOf cboBirthCounty_SelectedIndexChanged

    '            With cboIssueCountry
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            With cboNationality
    '                .DataValueField = "countryunkid"
    '                .DataTextField = "country_name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            RemoveHandler cboPState.SelectedIndexChanged, AddressOf cboPState_SelectedIndexChanged
    '            dsCombos = objState.GetList("List", True, True)
    '            With cboPState
    '                .DataValueField = "stateunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboPState.SelectedIndexChanged, AddressOf cboPState_SelectedIndexChanged

    '            RemoveHandler cboDState.SelectedIndexChanged, AddressOf cboDState_SelectedIndexChanged
    '            With cboDState
    '                .DataValueField = "stateunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboDState.SelectedIndexChanged, AddressOf cboDState_SelectedIndexChanged

    '            RemoveHandler cboEState.SelectedIndexChanged, AddressOf cboEState_SelectedIndexChanged
    '            With cboEState
    '                .DataValueField = "stateunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboEState.SelectedIndexChanged, AddressOf cboEState_SelectedIndexChanged

    '            RemoveHandler cboEState2.SelectedIndexChanged, AddressOf cboEState2_SelectedIndexChanged
    '            With cboEState2
    '                .DataValueField = "stateunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboEState2.SelectedIndexChanged, AddressOf cboEState2_SelectedIndexChanged

    '            RemoveHandler cboEState3.SelectedIndexChanged, AddressOf cboEState3_SelectedIndexChanged
    '            With cboEState3
    '                .DataValueField = "stateunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboEState3.SelectedIndexChanged, AddressOf cboEState3_SelectedIndexChanged


    '            RemoveHandler cboBirthState.SelectedIndexChanged, AddressOf cboBirthState_SelectedIndexChanged
    '            With cboBirthState
    '                .DataValueField = "stateunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboBirthState.SelectedIndexChanged, AddressOf cboBirthState_SelectedIndexChanged


    '            RemoveHandler cboPCity.SelectedIndexChanged, AddressOf cboPCity_SelectedIndexChanged
    '            dsCombos = objCity.GetList("List", True, True)
    '            With cboPCity
    '                .DataValueField = "cityunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboPCity.SelectedIndexChanged, AddressOf cboPCity_SelectedIndexChanged

    '            RemoveHandler cboDCity.SelectedIndexChanged, AddressOf cboDCity_SelectedIndexChanged
    '            With cboDCity
    '                .DataValueField = "cityunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboDCity.SelectedIndexChanged, AddressOf cboDCity_SelectedIndexChanged

    '            RemoveHandler cboECity.SelectedIndexChanged, AddressOf cboECity_SelectedIndexChanged
    '            With cboECity
    '                .DataValueField = "cityunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboECity.SelectedIndexChanged, AddressOf cboECity_SelectedIndexChanged

    '            RemoveHandler cboECity2.SelectedIndexChanged, AddressOf cboECity2_SelectedIndexChanged
    '            With cboECity2
    '                .DataValueField = "cityunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboECity2.SelectedIndexChanged, AddressOf cboECity2_SelectedIndexChanged

    '            RemoveHandler cboECity3.SelectedIndexChanged, AddressOf cboECity3_SelectedIndexChanged
    '            With cboECity3
    '                .DataValueField = "cityunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With
    '            AddHandler cboECity3.SelectedIndexChanged, AddressOf cboECity3_SelectedIndexChanged

    '            With cboBirthCity
    '                .DataValueField = "cityunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            dsCombos = objZipCode.GetList("List", True, True)
    '            With cboPZipcode
    '                .DataValueField = "zipcodeunkid"
    '                .DataTextField = "zipcode_no"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            With cboDZipcode
    '                .DataValueField = "zipcodeunkid"
    '                .DataTextField = "zipcode_no"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            With cboEZipcode
    '                .DataValueField = "zipcodeunkid"
    '                .DataTextField = "zipcode_no"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            With cboEZipcode2
    '                .DataValueField = "zipcodeunkid"
    '                .DataTextField = "zipcode_no"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            With cboEZipcode3
    '                .DataValueField = "zipcodeunkid"
    '                .DataTextField = "zipcode_no"
    '                .DataSource = dsCombos.Tables("List").Copy
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.COMPLEXION, True, "List")
    '            With cboComplexion
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("List")
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.BLOOD_GROUP, True, "BldGrp")
    '            With cboBloodGrp
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("BldGrp")
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.EYES_COLOR, True, "EyeColor")
    '            With cboEyeColor
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("EyeColor")
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.ETHNICITY, True, "Ethincity")
    '            With cboEthnicity
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Ethincity")
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.RELIGION, True, "Religion")
    '            With cboReligion
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Religion")
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.HAIR_COLOR, True, "Hair")
    '            With cboHair
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Hair")
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.LANGUAGES, True, "Lang1")
    '            With cboLang1
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Lang1").Copy
    '                .DataBind()
    '            End With

    '            With cboLang2
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Lang1").Copy
    '                .DataBind()
    '            End With

    '            With cboLang3
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Lang1").Copy
    '                .DataBind()
    '            End With

    '            With cboLang4
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("Lang1").Copy
    '                .DataBind()
    '            End With

    '            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, True, "MaritalStatus")
    '            With cboMaritalStatus
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsCombos.Tables("MaritalStatus")
    '                .DataBind()
    '            End With

    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '            dsCombos.Dispose() : objCommon = Nothing : objMaster = Nothing : objCity = Nothing : objState = Nothing : objZipCode = Nothing
    '        End Try
    '    End Sub

    '    Private Sub Fill_List()
    '        Dim dsList As New DataSet
    '        Dim objCommon As New clsCommon_Master
    '        Try
    '            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ALLERGIES, False, "List")
    '            With chkLstAllergies
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsList.Tables("List")
    '                .DataBind()
    '            End With

    '            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.DISABILITIES, False, "List")
    '            With chkLstDisabilities
    '                .DataValueField = "masterunkid"
    '                .DataTextField = "name"
    '                .DataSource = dsList.Tables("List")
    '                .DataBind()
    '            End With


    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '            dsList.Dispose() : objCommon = Nothing
    '        End Try
    '    End Sub

    '    Private Sub Fill_Info()
    '        Dim objCommon As New clsCommon_Master
    '        Dim objShift As New clsshift_master
    '        Dim objPayPoint As New clspaypoint_master

    '        Try
    '            objEmployee._Employeeunkid = Session("Employeeunkid")

    '            txtCodeValue.Text = objEmployee._Employeecode
    '            txtFirstValue.Text = objEmployee._Firstname
    '            txtSurnameValue.Text = objEmployee._Surname
    '            txtOtherNameValue.Text = objEmployee._Othername

    '            objCommon._Masterunkid = objEmployee._Employmenttypeunkid
    '            txtEmploymentTypeValue.Text = objCommon._Name

    '            objCommon._Masterunkid = objEmployee._Paytypeunkid
    '            txtPayValue.Text = objCommon._Name

    '            Select Case objEmployee._Gender
    '                Case 1  'MALE
    '                    txtGenderValue.Text = "Male"
    '                Case 2  'FEMALE
    '                    txtGenderValue.Text = "Female"
    '                Case Else
    '                    txtGenderValue.Text = ""
    '            End Select

    '            objPayPoint._Paypointunkid = objEmployee._Paypointunkid
    '            txtPayPointValue.Text = objPayPoint._Paypointname

    '            objShift._Shiftunkid = objEmployee._Shiftunkid
    '            txtShiftValue.Text = objShift._Shiftname

    '            objCommon._Masterunkid = objEmployee._Titalunkid
    '            txtTitelValue.Text = objCommon._Name

    '            If objEmployee._ImagePath <> "" Then
    '                Dim Path As String = ""

    '                If Aruti.Data.ConfigParameter._Object._PhotoPath.LastIndexOf("\") = Aruti.Data.ConfigParameter._Object._PhotoPath.ToString.Trim.Length - 1 Then
    '                    Path = System.IO.Path.GetFullPath(Aruti.Data.ConfigParameter._Object._PhotoPath & objEmployee._ImagePath)
    '                    Call ImageOperation(Path)
    '                Else
    '                    Path = System.IO.Path.GetFullPath(Aruti.Data.ConfigParameter._Object._PhotoPath & "\" & objEmployee._ImagePath)
    '                    Call ImageOperation(Path)
    '                End If
    '            End If


    '            Dim objEIdTran As New clsIdentity_tran
    '            objEIdTran._EmployeeUnkid = objEmployee._Employeeunkid
    '            Dim dtTable = objEIdTran._DataList
    '            If dtTable.Rows.Count > 0 Then
    '                Dim dtTemp() As DataRow = dtTable.Select("isdefault = true")
    '                If dtTemp.Length > 0 Then
    '                    txtIdValue.Text = dtTemp(0)("identity_no")
    '                End If
    '            Else
    '                txtIdValue.Text = ""
    '            End If

    '            '************************* PRESENT INFO *****************************'
    '            txtPAddress1.Text = objEmployee._Present_Address1
    '            txtPAddress2.Text = objEmployee._Present_Address2
    '            cboPCountry.SelectedValue = objEmployee._Present_Countryunkid
    '            cboPState.SelectedValue = objEmployee._Present_Stateunkid
    '            cboPCity.SelectedValue = objEmployee._Present_Post_Townunkid
    '            cboPZipcode.SelectedValue = objEmployee._Present_Postcodeunkid
    '            txtPRegion.Text = objEmployee._Present_Provicnce
    '            txtPStreet.Text = objEmployee._Present_Road
    '            txtPEState.Text = objEmployee._Present_Estate
    '            txtPPlotNo.Text = objEmployee._Present_Plotno
    '            txtPMobile.Text = objEmployee._Present_Mobile
    '            txtPAltNo.Text = objEmployee._Present_Alternateno
    '            txtPTelNo.Text = objEmployee._Present_Tel_No
    '            txtPFax.Text = objEmployee._Present_Fax
    '            txtPEmail.Text = objEmployee._Present_Email
    '            '************************* PRESENT INFO *****************************'

    '            '************************* DOMICILE INFO *****************************'
    '            txtDAddress1.Text = objEmployee._Domicile_Address1
    '            txtDAddress2.Text = objEmployee._Domicile_Address2
    '            cboDCountry.SelectedValue = objEmployee._Domicile_Countryunkid
    '            cboDState.SelectedValue = objEmployee._Domicile_Stateunkid
    '            cboDCity.SelectedValue = objEmployee._Domicile_Post_Townunkid
    '            cboDZipcode.SelectedValue = objEmployee._Domicile_Postcodeunkid
    '            txtDRegion.Text = objEmployee._Domicile_Provicnce
    '            txtDStreet.Text = objEmployee._Domicile_Road
    '            txtDEState.Text = objEmployee._Domicile_Estate
    '            txtDPlotNo.Text = objEmployee._Domicile_Plotno
    '            txtDMobile.Text = objEmployee._Domicile_Mobile
    '            txtDAltNo.Text = objEmployee._Domicile_Alternateno
    '            txtDTelNo.Text = objEmployee._Domicile_Tel_No
    '            txtDFax.Text = objEmployee._Domicile_Fax
    '            txtDEmail.Text = objEmployee._Domicile_Email
    '            '************************* DOMICILE INFO *****************************'

    '            '************************* EMERGENCY INFO *****************************'
    '            txtEAddress.Text = objEmployee._Emer_Con_Address
    '            txtEAltNo.Text = objEmployee._Emer_Con_Alternateno
    '            txtEEMail.Text = objEmployee._Emer_Con_Email
    '            txtEEState.Text = objEmployee._Emer_Con_Estate
    '            txtEFax.Text = objEmployee._Emer_Con_Fax
    '            txtEFirstname.Text = objEmployee._Emer_Con_Firstname
    '            txtELastname.Text = objEmployee._Emer_Con_Lastname
    '            txtEMobile.Text = objEmployee._Emer_Con_Mobile
    '            txtEPlotNo.Text = objEmployee._Emer_Con_Plotno
    '            cboECity.SelectedValue = objEmployee._Emer_Con_Post_Townunkid
    '            cboEZipcode.SelectedValue = objEmployee._Emer_Con_Postcodeunkid
    '            txtERegion.Text = objEmployee._Emer_Con_Provicnce
    '            txtEStreet.Text = objEmployee._Emer_Con_Road
    '            cboEState.SelectedValue = objEmployee._Emer_Con_State
    '            txtETelNo.Text = objEmployee._Emer_Con_Tel_No
    '            cboECountry.SelectedValue = objEmployee._Emer_Con_Countryunkid

    '            txtEAddress2.Text = objEmployee._Emer_Con_Address2
    '            txtEAltNo2.Text = objEmployee._Emer_Con_Alternateno2
    '            txtEEMail2.Text = objEmployee._Emer_Con_Email2
    '            txtEEState2.Text = objEmployee._Emer_Con_Estate2
    '            txtEFax2.Text = objEmployee._Emer_Con_Fax2
    '            txtEFirstname2.Text = objEmployee._Emer_Con_Firstname2
    '            txtELastname2.Text = objEmployee._Emer_Con_Lastname2
    '            txtEMobile2.Text = objEmployee._Emer_Con_Mobile2
    '            txtEPlotNo2.Text = objEmployee._Emer_Con_Plotno2
    '            cboECity2.SelectedValue = objEmployee._Emer_Con_Post_Townunkid2
    '            cboEZipcode2.SelectedValue = objEmployee._Emer_Con_Postcodeunkid2
    '            txtERegion2.Text = objEmployee._Emer_Con_Provicnce2
    '            txtEStreet2.Text = objEmployee._Emer_Con_Road2
    '            cboEState2.SelectedValue = objEmployee._Emer_Con_State2
    '            txtETelNo2.Text = objEmployee._Emer_Con_Tel_No2
    '            cboECountry2.SelectedValue = objEmployee._Emer_Con_Countryunkid2

    '            txtEAddress3.Text = objEmployee._Emer_Con_Address3
    '            txtEAltNo3.Text = objEmployee._Emer_Con_Alternateno3
    '            txtEEMail3.Text = objEmployee._Emer_Con_Email3
    '            txtEEState3.Text = objEmployee._Emer_Con_Estate3
    '            txtEFax3.Text = objEmployee._Emer_Con_Fax3
    '            txtEFirstname3.Text = objEmployee._Emer_Con_Firstname3
    '            txtELastname3.Text = objEmployee._Emer_Con_Lastname3
    '            txtEMobile3.Text = objEmployee._Emer_Con_Mobile3
    '            txtEPlotNo3.Text = objEmployee._Emer_Con_Plotno3
    '            cboECity3.SelectedValue = objEmployee._Emer_Con_Post_Townunkid3
    '            cboEZipcode3.SelectedValue = objEmployee._Emer_Con_Postcodeunkid3
    '            txtERegion3.Text = objEmployee._Emer_Con_Provicnce3
    '            txtEStreet3.Text = objEmployee._Emer_Con_Road3
    '            cboEState3.SelectedValue = objEmployee._Emer_Con_State3
    '            txtETelNo3.Text = objEmployee._Emer_Con_Tel_No3
    '            cboECountry3.SelectedValue = objEmployee._Emer_Con_Countryunkid3
    '            '************************* EMERGENCY INFO *****************************'

    '            '************************* BIRTH INFO *****************************'
    '            cboBirthCounty.SelectedValue = objEmployee._Birthcountryunkid
    '            cboBirthState.SelectedValue = objEmployee._Birthstateunkid
    '            cboBirthCity.SelectedValue = objEmployee._Birthcityunkid
    '            txtBirthVillage.Text = objEmployee._Birth_Village
    '            txtBirthWard.Text = objEmployee._Birth_Ward
    '            txtBirthCertNo.Text = objEmployee._Birthcertificateno
    '            '************************* BIRTH INFO *****************************'

    '            '************************* WORKING INFO *****************************'
    '            txtWorkPermitNo.Text = objEmployee._Work_Permit_No
    '            cboIssueCountry.SelectedValue = objEmployee._Workcountryunkid
    '            txtIssuePlace.Text = objEmployee._Work_Permit_Issue_Place

    '            If objEmployee._Work_Permit_Issue_Date <> Nothing Then
    '                dtIssueDate.SetDate = objEmployee._Work_Permit_Issue_Date
    '            Else
    '                dtIssueDate.SetDate = Nothing
    '            End If

    '            If objEmployee._Work_Permit_Expiry_Date <> Nothing Then
    '                dtExpiryDate.SetDate = objEmployee._Work_Permit_Expiry_Date
    '            Else
    '                dtExpiryDate.SetDate = Nothing
    '            End If
    '            '************************* WORKING INFO *****************************'

    '            '************************* OTHER INFO *****************************'
    '            cboComplexion.SelectedValue = objEmployee._Complexionunkid
    '            txtExTel.Text = objEmployee._Extra_Tel_No
    '            cboBloodGrp.SelectedValue = objEmployee._Bloodgroupunkid
    '            cboEyeColor.SelectedValue = objEmployee._Eyecolorunkid
    '            cboLang1.SelectedValue = objEmployee._Language1unkid
    '            cboLang2.SelectedValue = objEmployee._Language2unkid
    '            cboLang3.SelectedValue = objEmployee._Language3unkid
    '            cboLang4.SelectedValue = objEmployee._Language4unkid
    '            cboEthnicity.SelectedValue = objEmployee._Ethincityunkid
    '            cboNationality.SelectedValue = objEmployee._Nationalityunkid
    '            cboReligion.SelectedValue = objEmployee._Religionunkid
    '            cboHair.SelectedValue = objEmployee._Hairunkid
    '            txtHeight.Text = objEmployee._Height
    '            txtWeight.Text = objEmployee._Weight
    '            cboMaritalStatus.SelectedValue = objEmployee._Maritalstatusunkid
    '            If objEmployee._Anniversary_Date <> Nothing Then
    '                dtMarriedDate.SetDate = objEmployee._Anniversary_Date
    '            Else
    '                dtMarriedDate.SetDate = Nothing
    '            End If
    '            txtSpHob.Text = objEmployee._Sports_Hobbies

    '            If objEmployee._Allergies.Length > 0 Then
    '                For Each StrId As String In objEmployee._Allergies.Split(",")
    '                    For Each mItem As ListItem In chkLstAllergies.Items
    '                        If CInt(StrId) = CInt(mItem.Value) Then
    '                            mItem.Selected = True
    '                        End If
    '                    Next
    '                Next
    '            End If

    '            If objEmployee._Disabilities.Length > 0 Then
    '                For Each StrId As String In objEmployee._Disabilities.Split(",")
    '                    For Each mItem As ListItem In chkLstDisabilities.Items
    '                        If CInt(StrId) = CInt(mItem.Value) Then
    '                            mItem.Selected = True
    '                        End If
    '                    Next
    '                Next
    '            End If
    '            '************************* OTHER INFO *****************************'

    '            GridView1.DataSource = clsuser.LeaveBalances
    '            GridView1.DataBind()

    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub ImageOperation(ByVal imgFullPath As String)
    '        Dim f As New System.IO.FileInfo(imgFullPath)
    '        If System.IO.File.Exists(Server.MapPath("~\images\" & f.Name)) = False Then
    '            Dim rbyte() As Byte = IO.File.ReadAllBytes(imgFullPath)
    '            Dim mem As New System.IO.MemoryStream(rbyte)
    '            Drawing.Image.FromStream(mem).Save(Server.MapPath("~\images\" & f.Name))
    '            If f.Name <> objEmployee._ImagePath Then
    '                Drawing.Image.FromStream(mem).Save(ConfigParameter._Object._PhotoPath & "\" & f.Name)
    '                Me.ViewState("OldImage") = Me.ViewState("ImageName")
    '            End If
    '        End If
    '        imgEmp.ImageUrl = "~\images\" & f.Name
    '        Me.ViewState("ImageName") = f.Name
    '    End Sub

    '    Private Sub ClearObjects()
    '        Try
    '            txtPAddress1.Text = ""
    '            txtPAddress2.Text = ""
    '            cboPCountry.SelectedValue = 0
    '            cboPState.SelectedValue = 0
    '            cboPCity.SelectedValue = 0
    '            cboPZipcode.SelectedValue = 0
    '            txtPRegion.Text = ""
    '            txtPStreet.Text = ""
    '            txtPEState.Text = ""
    '            txtPPlotNo.Text = ""
    '            txtPMobile.Text = ""
    '            txtPAltNo.Text = ""
    '            txtPTelNo.Text = ""
    '            txtPFax.Text = ""
    '            txtPEmail.Text = ""
    '            txtDAddress1.Text = ""
    '            txtDAddress2.Text = ""
    '            cboDCountry.SelectedValue = 0
    '            cboDState.SelectedValue = 0
    '            cboDCity.SelectedValue = 0
    '            cboDZipcode.SelectedValue = 0
    '            txtDRegion.Text = ""
    '            txtDStreet.Text = ""
    '            txtDEState.Text = ""
    '            txtDPlotNo.Text = ""
    '            txtDMobile.Text = ""
    '            txtDAltNo.Text = ""
    '            txtDTelNo.Text = ""
    '            txtDFax.Text = ""
    '            txtDEmail.Text = ""
    '            txtEAddress.Text = ""
    '            txtEAltNo.Text = ""
    '            txtEEMail.Text = ""
    '            txtEEState.Text = ""
    '            txtEFax.Text = ""
    '            txtEFirstname.Text = ""
    '            txtELastname.Text = ""
    '            txtEMobile.Text = ""
    '            txtEPlotNo.Text = ""
    '            cboECity.SelectedValue = 0
    '            cboEZipcode.SelectedValue = 0
    '            txtERegion.Text = ""
    '            txtEStreet.Text = ""
    '            cboEState.SelectedValue = 0
    '            txtETelNo.Text = ""
    '            cboECountry.SelectedValue = 0

    '            cboBirthCounty.SelectedValue = 0
    '            cboBirthState.SelectedValue = 0
    '            cboBirthCity.SelectedValue = 0
    '            txtBirthWard.Text = ""
    '            txtBirthCertNo.Text = ""
    '            txtBirthVillage.Text = ""
    '            txtWorkPermitNo.Text = ""
    '            cboIssueCountry.SelectedValue = 0
    '            dtExpiryDate.SetDate = Nothing
    '            dtIssueDate.SetDate = Nothing
    '            txtIssuePlace.Text = ""
    '            cboComplexion.SelectedValue = 0
    '            txtExTel.Text = ""
    '            cboBloodGrp.Text = ""
    '            cboEyeColor.SelectedValue = 0
    '            cboLang1.SelectedValue = 0
    '            cboLang2.SelectedValue = 0
    '            cboLang3.SelectedValue = 0
    '            cboLang4.SelectedValue = 0
    '            cboEthnicity.SelectedValue = 0
    '            cboNationality.SelectedValue = 0
    '            cboReligion.SelectedValue = 0
    '            cboHair.SelectedValue = 0
    '            txtHeight.Text = 0
    '            txtWeight.Text = 0
    '            cboMaritalStatus.SelectedValue = 0
    '            dtMarriedDate.SetDate = Nothing
    '            txtSpHob.Text = ""

    '            txtEAddress2.Text = ""
    '            txtEAltNo2.Text = ""
    '            txtEEMail2.Text = ""
    '            txtEEState2.Text = ""
    '            txtEFax2.Text = ""
    '            txtEFirstname2.Text = ""
    '            txtELastname2.Text = ""
    '            txtEMobile2.Text = ""
    '            txtEPlotNo2.Text = ""
    '            cboECity2.SelectedValue = 0
    '            cboEZipcode2.SelectedValue = 0
    '            txtERegion2.Text = ""
    '            txtEStreet2.Text = ""
    '            cboEState2.SelectedValue = 0
    '            txtETelNo2.Text = ""
    '            cboECountry2.SelectedValue = 0

    '            txtEAddress3.Text = ""
    '            txtEAltNo3.Text = ""
    '            txtEEMail3.Text = ""
    '            txtEEState3.Text = ""
    '            txtEFax3.Text = ""
    '            txtEFirstname3.Text = ""
    '            txtELastname3.Text = ""
    '            txtEMobile3.Text = ""
    '            txtEPlotNo3.Text = ""
    '            cboECity3.SelectedValue = 0
    '            cboEZipcode3.SelectedValue = 0
    '            txtERegion3.Text = ""
    '            txtEStreet3.Text = ""
    '            cboEState3.SelectedValue = 0
    '            txtETelNo3.Text = ""
    '            cboECountry3.SelectedValue = 0

    '            For Each mItem As ListItem In chkLstAllergies.Items
    '                mItem.Selected = False
    '            Next

    '            For Each mItem As ListItem In chkLstDisabilities.Items
    '                mItem.Selected = False
    '            Next

    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub SetValue()
    '        Try
    '            '************************* PRESENT INFO *****************************'
    '            objEmployee._Employeeunkid = Session("Employeeunkid")

    '            objEmployee._Present_Address1 = txtPAddress1.Text
    '            objEmployee._Present_Address2 = txtPAddress2.Text
    '            objEmployee._Present_Countryunkid = cboPCountry.SelectedValue
    '            objEmployee._Present_Stateunkid = cboPState.SelectedValue
    '            objEmployee._Present_Post_Townunkid = cboPCity.SelectedValue
    '            objEmployee._Present_Postcodeunkid = cboPZipcode.SelectedValue
    '            objEmployee._Present_Provicnce = txtPRegion.Text
    '            objEmployee._Present_Road = txtPStreet.Text
    '            objEmployee._Present_Estate = txtPEState.Text
    '            objEmployee._Present_Plotno = txtPPlotNo.Text
    '            objEmployee._Present_Mobile = txtPMobile.Text
    '            objEmployee._Present_Alternateno = txtPAltNo.Text
    '            objEmployee._Present_Tel_No = txtPTelNo.Text
    '            objEmployee._Present_Fax = txtPFax.Text
    '            objEmployee._Present_Email = txtPEmail.Text
    '            '************************* PRESENT INFO *****************************'

    '            '************************* DOMICILE INFO *****************************'
    '            objEmployee._Domicile_Address1 = txtDAddress1.Text
    '            objEmployee._Domicile_Address2 = txtDAddress2.Text
    '            objEmployee._Domicile_Countryunkid = cboDCountry.SelectedValue
    '            objEmployee._Domicile_Stateunkid = cboDState.SelectedValue
    '            objEmployee._Domicile_Post_Townunkid = cboDCity.SelectedValue
    '            objEmployee._Domicile_Postcodeunkid = cboDZipcode.SelectedValue
    '            objEmployee._Domicile_Provicnce = txtDRegion.Text
    '            objEmployee._Domicile_Road = txtDStreet.Text
    '            objEmployee._Domicile_Estate = txtDEState.Text
    '            objEmployee._Domicile_Plotno = txtDPlotNo.Text
    '            objEmployee._Domicile_Mobile = txtDMobile.Text
    '            objEmployee._Domicile_Alternateno = txtDAltNo.Text
    '            objEmployee._Domicile_Tel_No = txtDTelNo.Text
    '            objEmployee._Domicile_Fax = txtDFax.Text
    '            objEmployee._Domicile_Email = txtDEmail.Text
    '            '************************* DOMICILE INFO *****************************'

    '            '************************* EMERGENCY INFO *****************************'
    '            objEmployee._Emer_Con_Address = txtEAddress.Text
    '            objEmployee._Emer_Con_Alternateno = txtEAltNo.Text
    '            objEmployee._Emer_Con_Email = txtEEMail.Text
    '            objEmployee._Emer_Con_Estate = txtEEState.Text
    '            objEmployee._Emer_Con_Fax = txtEFax.Text
    '            objEmployee._Emer_Con_Firstname = txtEFirstname.Text
    '            objEmployee._Emer_Con_Lastname = txtELastname.Text
    '            objEmployee._Emer_Con_Mobile = txtEMobile.Text
    '            objEmployee._Emer_Con_Plotno = txtEPlotNo.Text
    '            objEmployee._Emer_Con_Post_Townunkid = cboECity.SelectedValue
    '            objEmployee._Emer_Con_Postcodeunkid = cboEZipcode.SelectedValue
    '            objEmployee._Emer_Con_Provicnce = txtERegion.Text
    '            objEmployee._Emer_Con_Road = txtEStreet.Text
    '            objEmployee._Emer_Con_State = cboEState.SelectedValue
    '            objEmployee._Emer_Con_Tel_No = txtETelNo.Text
    '            objEmployee._Emer_Con_Countryunkid = cboECountry.SelectedValue

    '            objEmployee._Emer_Con_Address2 = txtEAddress2.Text
    '            objEmployee._Emer_Con_Alternateno2 = txtEAltNo2.Text
    '            objEmployee._Emer_Con_Email2 = txtEEMail2.Text
    '            objEmployee._Emer_Con_Estate2 = txtEEState2.Text
    '            objEmployee._Emer_Con_Fax2 = txtEFax2.Text
    '            objEmployee._Emer_Con_Firstname2 = txtEFirstname2.Text
    '            objEmployee._Emer_Con_Lastname2 = txtELastname2.Text
    '            objEmployee._Emer_Con_Mobile2 = txtEMobile2.Text
    '            objEmployee._Emer_Con_Plotno2 = txtEPlotNo2.Text
    '            objEmployee._Emer_Con_Post_Townunkid2 = cboECity2.SelectedValue
    '            objEmployee._Emer_Con_Postcodeunkid2 = cboEZipcode2.SelectedValue
    '            objEmployee._Emer_Con_Provicnce2 = txtERegion2.Text
    '            objEmployee._Emer_Con_Road2 = txtEStreet2.Text
    '            objEmployee._Emer_Con_State2 = cboEState2.SelectedValue
    '            objEmployee._Emer_Con_Tel_No2 = txtETelNo2.Text
    '            objEmployee._Emer_Con_Countryunkid2 = cboECountry2.SelectedValue

    '            objEmployee._Emer_Con_Address3 = txtEAddress3.Text
    '            objEmployee._Emer_Con_Alternateno3 = txtEAltNo3.Text
    '            objEmployee._Emer_Con_Email3 = txtEEMail3.Text
    '            objEmployee._Emer_Con_Estate3 = txtEEState3.Text
    '            objEmployee._Emer_Con_Fax3 = txtEFax3.Text
    '            objEmployee._Emer_Con_Firstname3 = txtEFirstname3.Text
    '            objEmployee._Emer_Con_Lastname3 = txtELastname3.Text
    '            objEmployee._Emer_Con_Mobile3 = txtEMobile3.Text
    '            objEmployee._Emer_Con_Plotno3 = txtEPlotNo3.Text
    '            objEmployee._Emer_Con_Post_Townunkid3 = cboECity3.SelectedValue
    '            objEmployee._Emer_Con_Postcodeunkid3 = cboEZipcode3.SelectedValue
    '            objEmployee._Emer_Con_Provicnce3 = txtERegion3.Text
    '            objEmployee._Emer_Con_Road3 = txtEStreet3.Text
    '            objEmployee._Emer_Con_State3 = cboEState3.SelectedValue
    '            objEmployee._Emer_Con_Tel_No3 = txtETelNo3.Text
    '            objEmployee._Emer_Con_Countryunkid3 = cboECountry3.SelectedValue
    '            '************************* EMERGENCY INFO *****************************'

    '            '************************* BIRTH INFO *****************************'
    '            objEmployee._Birthcountryunkid = cboBirthCounty.SelectedValue
    '            objEmployee._Birthstateunkid = cboBirthState.SelectedValue
    '            objEmployee._Birthcityunkid = cboBirthCity.SelectedValue
    '            objEmployee._Birth_Village = txtBirthVillage.Text
    '            objEmployee._Birth_Ward = txtBirthWard.Text
    '            objEmployee._Birthcertificateno = txtBirthCertNo.Text
    '            '************************* BIRTH INFO *****************************'

    '            '************************* WORKING INFO *****************************'
    '            objEmployee._Work_Permit_No = txtWorkPermitNo.Text
    '            objEmployee._Workcountryunkid = cboIssueCountry.SelectedValue
    '            objEmployee._Work_Permit_Issue_Place = txtIssuePlace.Text
    '            If dtIssueDate.GetDate <> "1/1/2000" Then
    '                objEmployee._Work_Permit_Issue_Date = dtIssueDate.GetDate
    '            End If
    '            If dtExpiryDate.GetDate <> "1/1/2000" Then
    '                objEmployee._Work_Permit_Expiry_Date = dtExpiryDate.GetDate
    '            End If
    '            '************************* WORKING INFO *****************************'

    '            '************************* OTHER INFO *****************************'
    '            objEmployee._Complexionunkid = cboComplexion.SelectedValue
    '            objEmployee._Extra_Tel_No = txtExTel.Text
    '            objEmployee._Bloodgroupunkid = cboBloodGrp.SelectedValue
    '            objEmployee._Eyecolorunkid = cboEyeColor.SelectedValue
    '            objEmployee._Language1unkid = cboLang1.SelectedValue
    '            objEmployee._Language2unkid = cboLang2.SelectedValue
    '            objEmployee._Language3unkid = cboLang3.SelectedValue
    '            objEmployee._Language4unkid = cboLang4.SelectedValue
    '            objEmployee._Ethincityunkid = cboEthnicity.SelectedValue
    '            objEmployee._Nationalityunkid = cboNationality.SelectedValue
    '            objEmployee._Religionunkid = cboReligion.SelectedValue
    '            objEmployee._Hairunkid = cboHair.SelectedValue
    '            objEmployee._Height = IIf(txtHeight.Text = "", 0, txtHeight.Text)
    '            objEmployee._Weight = IIf(txtWeight.Text = "", 0, txtWeight.Text)
    '            objEmployee._Maritalstatusunkid = cboMaritalStatus.SelectedValue
    '            If dtMarriedDate.GetDate <> "1/1/2000" Then
    '                objEmployee._Anniversary_Date = dtMarriedDate.GetDate
    '            End If
    '            objEmployee._Sports_Hobbies = txtSpHob.Text

    '            Dim mStrUnkids As String = String.Empty

    '            For Each mItem As ListItem In chkLstAllergies.Items
    '                If mItem.Selected Then
    '                    mStrUnkids &= "," & mItem.Value
    '                End If
    '            Next
    '            If mStrUnkids.Length > 0 Then mStrUnkids = Mid(mStrUnkids, 2)
    '            objEmployee._Allergies = mStrUnkids

    '            mStrUnkids = ""
    '            For Each mItem As ListItem In chkLstDisabilities.Items
    '                If mItem.Selected Then
    '                    mStrUnkids &= "," & mItem.Value
    '                End If
    '            Next
    '            If mStrUnkids.Length > 0 Then mStrUnkids = Mid(mStrUnkids, 2)
    '            objEmployee._Disabilities = mStrUnkids
    '            '************************* OTHER INFO *****************************'
    '            If imgEmp.ImageUrl <> "" Then
    '                objEmployee._ImagePath = Me.ViewState("ImageName")
    '            End If

    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Private Sub DeleteImage(ByVal imgFullPath As String)
    '        Dim f As New System.IO.FileInfo(imgFullPath)
    '        If System.IO.File.Exists(Server.MapPath("~\images\" & f.Name)) Then
    '            System.IO.File.Delete(Server.MapPath("~\images\" & f.Name))
    '        End If
    '    End Sub

    '#End Region

    '#Region " Form's Event(s) "

    '    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '        Try

    '            If Session("clsuser") Is Nothing Then
    '                
    '            End If

    '            If (Page.IsPostBack = False) Then
    '                clsuser = CType(Session("clsuser"), User)
    '                Call FillCombo() : Call Fill_List() : Fill_Info()
    '            End If

    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
    '        If IsPostBack = True And Me.ViewState("OldImage") <> "" Then
    '            Call DeleteImage(Server.MapPath("~\images\" & Me.ViewState("OldImage")))
    '        End If
    '    End Sub

    '#End Region

    '#Region " Button's Event(s) "

    '    'Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
    '    '    Try
    '    '        If imgUpload.PostedFile.FileName <> "" Then
    '    '            Call DeleteImage(Server.MapPath("~\images\" & Me.ViewState("ImageName")))
    '    '            Call ImageOperation(imgUpload.PostedFile.FileName.ToString)
    '    '            Dim nF As New System.IO.FileInfo(imgUpload.PostedFile.FileName)
    '    '            Me.ViewState("NewImg") = nF.Name
    '    '        End If
    '    '    Catch ex As Exception
    '    '        msg.DisplayError(ex, Me)
    '    '    Finally
    '    '    End Try
    '    'End Sub

    '    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
    '        Try

    '            If dtIssueDate.IsNull = False And dtExpiryDate.IsNull = False Then
    '                If dtExpiryDate.GetDate < dtIssueDate.GetDate Then
    '                    msg.DisplayMessage("Expiry Date cannot be less than or equal to Issue Date.", Me)
    '                    Exit Sub
    '                End If
    '            ElseIf dtIssueDate.IsNull = False Then
    '                msg.DisplayMessage("Expiry Date is mandatory information, Please set Expiry Date.", Me)
    '                Exit Sub
    '            ElseIf dtExpiryDate.IsNull = False Then
    '                msg.DisplayMessage("Issue Date is mandatory information, Please set Issue Date.", Me)
    '                Exit Sub
    '            End If

    '            If Expression.IsMatch(txtPEmail.Text.Trim) = False Then
    '                msg.DisplayMessage("Invalid Present Email.Please Enter Valid Email", Me)
    '                Exit Sub
    '            End If

    '            If Expression.IsMatch(txtDEmail.Text.Trim) = False Then
    '                msg.DisplayMessage("Invalid Domicile Email.Please Enter Valid Email", Me)
    '                Exit Sub
    '            End If

    '            If Expression.IsMatch(txtEEMail.Text.Trim) = False Then
    '                msg.DisplayMessage("Invalid Emergency Email.Please Enter Valid Email", Me)
    '                Exit Sub
    '            End If

    '            Call SetValue()

    '            If objEmployee.Update = False Then
    '                msg.DisplayMessage(objEmployee._Message, Me)
    '            Else
    '                msg.DisplayMessage("Information successfully updated.", Me)
    '                Call ClearObjects()
    '            End If

    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '        Try
    '            Response.Redirect("~\UserHome.aspx", False)
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '        Try
    '            Response.Redirect("~\UserHome.aspx", False)
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '#End Region

    '#Region " Control's Event(s) "

    '    Protected Sub cboPCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPCountry.SelectedIndexChanged
    '        Try
    '            If cboPCountry.SelectedValue >= 0 Then
    '                Dim objState As New clsstate_master
    '                Dim dsList As New DataSet
    '                dsList = objState.GetList("List", True, True, cboPCountry.SelectedValue)
    '                With cboPState
    '                    .DataValueField = "stateunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboPState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPState.SelectedIndexChanged
    '        Try
    '            If cboPState.SelectedValue >= 0 Then
    '                Dim objCity As New clscity_master
    '                Dim dsList As DataSet = objCity.GetList("List", True, True, cboPState.SelectedValue)
    '                With cboPCity
    '                    .DataValueField = "cityunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List").Copy
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboPCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPCity.SelectedIndexChanged
    '        Try
    '            If cboPCity.SelectedValue >= 0 Then
    '                Dim objZip As New clszipcode_master
    '                Dim dsList As DataSet = objZip.GetList("List", True, True, cboPCity.SelectedValue)
    '                With cboPZipcode
    '                    .DataValueField = "zipcodeunkid"
    '                    .DataTextField = "zipcode_no"
    '                    .DataSource = dsList.Tables("List").Copy
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboDCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDCountry.SelectedIndexChanged
    '        Try
    '            If cboDCountry.SelectedValue >= 0 Then
    '                Dim objState As New clsstate_master
    '                Dim dsList As New DataSet
    '                dsList = objState.GetList("List", True, True, cboDCountry.SelectedValue)
    '                With cboDState
    '                    .DataValueField = "stateunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboDState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDState.SelectedIndexChanged
    '        Try
    '            If cboDState.SelectedValue >= 0 Then
    '                Dim objCity As New clscity_master
    '                Dim dsList As DataSet = objCity.GetList("List", True, True, cboDState.SelectedValue)
    '                With cboDCity
    '                    .DataValueField = "cityunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List").Copy
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboDCity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDCity.SelectedIndexChanged
    '        Try
    '            If cboDCity.SelectedValue >= 0 Then
    '                Dim objZip As New clszipcode_master
    '                Dim dsList As DataSet = objZip.GetList("List", True, True, cboDCity.SelectedValue)
    '                With cboDZipcode
    '                    .DataValueField = "zipcodeunkid"
    '                    .DataTextField = "zipcode_no"
    '                    .DataSource = dsList.Tables("List").Copy
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboECountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECountry.SelectedIndexChanged
    '        Try
    '            If cboECountry.SelectedValue >= 0 Then
    '                Dim objState As New clsstate_master
    '                Dim dsList As New DataSet
    '                dsList = objState.GetList("List", True, True, cboECountry.SelectedValue)
    '                With cboEState
    '                    .DataValueField = "stateunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboECountry2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECountry2.SelectedIndexChanged
    '        Try
    '            If cboECountry2.SelectedValue >= 0 Then
    '                Dim objState As New clsstate_master
    '                Dim dsList As New DataSet
    '                dsList = objState.GetList("List", True, True, cboECountry2.SelectedValue)
    '                With cboEState2
    '                    .DataValueField = "stateunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboECountry3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECountry3.SelectedIndexChanged
    '        Try
    '            If cboECountry3.SelectedValue >= 0 Then
    '                Dim objState As New clsstate_master
    '                Dim dsList As New DataSet
    '                dsList = objState.GetList("List", True, True, cboECountry3.SelectedValue)
    '                With cboEState3
    '                    .DataValueField = "stateunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboEState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEState.SelectedIndexChanged
    '        Try
    '            If cboEState.SelectedValue >= 0 Then
    '                Dim objCity As New clscity_master
    '                Dim dsList As DataSet = objCity.GetList("List", True, True, cboEState.SelectedValue)
    '                With cboECity
    '                    .DataValueField = "cityunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboEState2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEState2.SelectedIndexChanged
    '        Try
    '            If cboEState2.SelectedValue >= 0 Then
    '                Dim objCity As New clscity_master
    '                Dim dsList As DataSet = objCity.GetList("List", True, True, cboEState2.SelectedValue)
    '                With cboECity2
    '                    .DataValueField = "cityunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboEState3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEState3.SelectedIndexChanged
    '        Try
    '            If cboEState3.SelectedValue >= 0 Then
    '                Dim objCity As New clscity_master
    '                Dim dsList As DataSet = objCity.GetList("List", True, True, cboEState3.SelectedValue)
    '                With cboECity3
    '                    .DataValueField = "cityunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboECity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECity.SelectedIndexChanged
    '        Try
    '            If cboECity.SelectedValue >= 0 Then
    '                Dim objZip As New clszipcode_master
    '                Dim dsList As DataSet = objZip.GetList("List", True, True, cboECity.SelectedValue)
    '                With cboEZipcode
    '                    .DataValueField = "zipcodeunkid"
    '                    .DataTextField = "zipcode_no"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboECity2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECity2.SelectedIndexChanged
    '        Try
    '            If cboECity2.SelectedValue >= 0 Then
    '                Dim objZip As New clszipcode_master
    '                Dim dsList As DataSet = objZip.GetList("List", True, True, cboECity2.SelectedValue)
    '                With cboEZipcode2
    '                    .DataValueField = "zipcodeunkid"
    '                    .DataTextField = "zipcode_no"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboECity3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboECity3.SelectedIndexChanged
    '        Try
    '            If cboECity3.SelectedValue >= 0 Then
    '                Dim objZip As New clszipcode_master
    '                Dim dsList As DataSet = objZip.GetList("List", True, True, cboECity3.SelectedValue)
    '                With cboEZipcode3
    '                    .DataValueField = "zipcodeunkid"
    '                    .DataTextField = "zipcode_no"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboBirthCounty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBirthCounty.SelectedIndexChanged
    '        Try
    '            If cboBirthCounty.SelectedValue >= 0 Then
    '                Dim objState As New clsstate_master
    '                Dim dsList As New DataSet
    '                dsList = objState.GetList("List", True, True, cboBirthCounty.SelectedValue)
    '                With cboBirthState
    '                    .DataValueField = "stateunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub cboBirthState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBirthState.SelectedIndexChanged
    '        Try
    '            If cboBirthState.SelectedValue >= 0 Then
    '                Dim objCity As New clscity_master
    '                Dim dsList As DataSet = objCity.GetList("List", True, True, cboBirthState.SelectedValue)
    '                With cboBirthCity
    '                    .DataValueField = "cityunkid"
    '                    .DataTextField = "name"
    '                    .DataSource = dsList.Tables("List")
    '                    .DataBind()
    '                End With
    '            End If
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub lnkCopyAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopyAddress.Click
    '        Try
    '            txtDAddress1.Text = txtPAddress1.Text
    '            txtDAddress2.Text = txtPAddress2.Text
    '            cboDCountry.SelectedValue = cboPCountry.SelectedValue
    '            cboDState.SelectedValue = cboPState.SelectedValue
    '            cboDCity.SelectedValue = cboPCity.SelectedValue
    '            cboDZipcode.SelectedValue = cboPZipcode.SelectedValue
    '            txtDRegion.Text = txtPRegion.Text
    '            txtDStreet.Text = txtPStreet.Text
    '            txtDEState.Text = txtPEState.Text
    '            txtDPlotNo.Text = txtPPlotNo.Text
    '            txtDMobile.Text = txtPMobile.Text
    '            txtDAltNo.Text = txtPAltNo.Text
    '            txtDTelNo.Text = txtPTelNo.Text
    '            txtDFax.Text = txtPFax.Text
    '            txtDEmail.Text = txtPEmail.Text
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub lnkCopyEAddress1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopyEAddress1.Click
    '        Try
    '            txtEAddress2.Text = txtEAddress.Text
    '            txtEAltNo2.Text = txtEAltNo.Text
    '            txtEEMail2.Text = txtEEMail.Text
    '            txtEEState2.Text = txtEEState.Text
    '            txtEFax2.Text = txtEFax.Text
    '            txtEFirstname2.Text = txtEFirstname.Text
    '            txtELastname2.Text = txtELastname.Text
    '            txtEMobile2.Text = txtEMobile.Text
    '            txtEPlotNo2.Text = txtEPlotNo.Text
    '            cboECity2.SelectedValue = cboECity.SelectedValue
    '            cboEZipcode2.SelectedValue = cboEZipcode.SelectedValue
    '            txtERegion2.Text = txtERegion.Text
    '            txtEStreet2.Text = txtEStreet.Text
    '            cboEState2.SelectedValue = cboEState.SelectedValue
    '            txtETelNo2.Text = txtETelNo.Text
    '            cboECountry2.SelectedValue = cboECountry.SelectedValue
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub lnkCopyEAddress2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopyEAddress2.Click
    '        Try
    '            txtEAddress3.Text = txtEAddress.Text
    '            txtEAltNo3.Text = txtEAltNo.Text
    '            txtEEMail3.Text = txtEEMail.Text
    '            txtEEState3.Text = txtEEState.Text
    '            txtEFax3.Text = txtEFax.Text
    '            txtEFirstname3.Text = txtEFirstname.Text
    '            txtELastname3.Text = txtELastname.Text
    '            txtEMobile3.Text = txtEMobile.Text
    '            txtEPlotNo3.Text = txtEPlotNo.Text
    '            cboECity3.SelectedValue = cboECity.SelectedValue
    '            cboEZipcode3.SelectedValue = cboEZipcode.SelectedValue
    '            txtERegion3.Text = txtERegion.Text
    '            txtEStreet3.Text = txtEStreet.Text
    '            cboEState3.SelectedValue = cboEState.SelectedValue
    '            txtETelNo3.Text = txtETelNo.Text
    '            cboECountry3.SelectedValue = cboECountry.SelectedValue
    '        Catch ex As Exception
    '            msg.DisplayError(ex, Me)
    '        Finally
    '        End Try
    '    End Sub

    '    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
    '        If e.Row.RowIndex < 0 Then Exit Sub
    '        If e.Row.Cells(1).Text.Trim <> "" Then
    '            e.Row.Cells(1).Text = eZeeDate.convertDate(e.Row.Cells(1).Text).Date
    '        End If
    '        If e.Row.Cells(2).Text.Trim <> "" Then
    '            e.Row.Cells(2).Text = eZeeDate.convertDate(e.Row.Cells(2).Text).Date
    '        End If
    '    End Sub

    '#End Region


End Class
