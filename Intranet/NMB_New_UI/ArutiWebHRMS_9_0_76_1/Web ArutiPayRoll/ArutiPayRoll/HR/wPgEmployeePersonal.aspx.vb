﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Drawing.Image
Imports System.IO

#End Region

Partial Class wPgEmployeePersonal
    Inherits Basepage

#Region " Private Variable(s) "

    Private msg As New CommonCodes
    Private clsuser As New User
    Private objEmployee As New clsEmployee_Master
    Private ReadOnly mstrModuleName As String = "frmEmployeeMaster"
    Private ReadOnly mstrModuleName1 As String = "frmEmployeeList"

    'Gajanan [18-Mar-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Dim Arr() As String
    Private objA_Personalinfo As New clsEmployeePersonalInfo_Approval


    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Private PersonalinfoApprovalFlowVal As String
    Private BirthInfoApprovalFlowVal As String
    Private OtherInfoApprovalFlowVal As String
    'Gajanan [17-April-2019] -- End

    'Gajanan [18-Mar-2019] -- End

    'S.SANDEEP |26-APR-2019| -- START
    Private mdtAttachement As DataTable = Nothing
    Private mdtFullAttachment As DataTable = Nothing
    Private mblnAttachmentPopup As Boolean = False
    Private objScanAttachment As New clsScan_Attach_Documents
    Private mintDeleteIndex As Integer = 0
    Private mintAddressTypeId As Integer = 0
    'S.SANDEEP |26-APR-2019| -- END

    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [17-April-2019] -- End
    'Sohail (09 Nov 2020) -- Start
    'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
    Private mstrURLReferer As String = ""
    'Sohail (09 Nov 2020) -- End

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objCommon As New clsCommon_Master
        Dim objMaster As New clsMasterData
        Dim objCity As New clscity_master
        Dim objState As New clsstate_master
        Dim objZipCode As New clszipcode_master

        Try

            dsCombos = objMaster.getCountryList("List", True)
            RemoveHandler cboBirthCounty.SelectedIndexChanged, AddressOf cboBirthCounty_SelectedIndexChanged
            With cboBirthCounty
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboBirthCounty.SelectedIndexChanged, AddressOf cboBirthCounty_SelectedIndexChanged

            With cboIssueCountry
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With

            With cboNationality
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With

            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            With cboResidentIssueCountry
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            'S.SANDEEP [04-Jan-2018] -- END

            dsCombos = objState.GetList("List", True, True)
            RemoveHandler cboBirthState.SelectedIndexChanged, AddressOf cboBirthState_SelectedIndexChanged
            With cboBirthState
                .DataValueField = "stateunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With
            AddHandler cboBirthState.SelectedIndexChanged, AddressOf cboBirthState_SelectedIndexChanged

            dsCombos = objCity.GetList("List", True, True)
            With cboBirthCity
                .DataValueField = "cityunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List").Copy
                .DataBind()
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.COMPLEXION, True, "List")
            With cboComplexion
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.BLOOD_GROUP, True, "BldGrp")
            With cboBloodGrp
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("BldGrp")
                .DataBind()
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.EYES_COLOR, True, "EyeColor")
            With cboEyeColor
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("EyeColor")
                .DataBind()
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.ETHNICITY, True, "Ethincity")
            With cboEthnicity
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Ethincity")
                .DataBind()
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.RELIGION, True, "Religion")
            With cboReligion
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Religion")
                .DataBind()
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.HAIR_COLOR, True, "Hair")
            With cboHair
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Hair")
                .DataBind()
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.LANGUAGES, True, "Lang1")
            With cboLang1
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Lang1").Copy
                .DataBind()
            End With

            With cboLang2
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Lang1").Copy
                .DataBind()
            End With

            With cboLang3
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Lang1").Copy
                .DataBind()
            End With

            With cboLang4
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Lang1").Copy
                .DataBind()
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.MARRIED_STATUS, True, "MaritalStatus")
            With cboMaritalStatus
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("MaritalStatus")
                .DataBind()
            End With



            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.TOWN_1, True, "Town1")
            With cboBrithTown
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Town1").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With


            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.CHIEFDOM, True, "Chiefdom")
            With cboBrithChiefdom
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Chiefdom").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.VILLAGE, True, "Village")
            With cboBrithVillage
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Village").Copy
                .DataBind() 'Gajanan [17-April-2019]
                .SelectedValue = CStr("0")
            End With
            'Gajanan [17-April-2019] -- End


            'S.SANDEEP |26-APR-2019| -- START
            dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With
            'S.SANDEEP |26-APR-2019| -- END

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            dsCombos.Dispose() : objCommon = Nothing : objMaster = Nothing : objCity = Nothing : objState = Nothing : objZipCode = Nothing
        End Try
    End Sub

    Private Sub Fill_List()
        Dim dsList As New DataSet
        Dim objCommon As New clsCommon_Master
        Try
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ALLERGIES, False, "List")
            With chkLstAllergies
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.DISABILITIES, False, "List")
            With chkLstDisabilities
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            dsList.Dispose() : objCommon = Nothing
        End Try
    End Sub

    Private Sub SetInfo()
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = Session("Employeeunkid")
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            'Shani(20-Nov-2015) -- End


            txtEmployee.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname

            '************************* BIRTH INFO *****************************'


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'cboBirthCounty.SelectedValue = CStr(objEmployee._Birthcountryunkid)
            'cboBirthState.SelectedValue = CStr(objEmployee._Birthstateunkid)
            'cboBirthCity.SelectedValue = CStr(objEmployee._Birthcityunkid)
            'txtBirthVillage.Text = objEmployee._Birth_Village
            'txtBirthWard.Text = objEmployee._Birth_Ward
            'txtBirthCertNo.Text = objEmployee._Birthcertificateno


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'If PersonalinfoApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then
            If BirthInfoApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then
                'Gajanan [17-April-2019] -- End


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If objApprovalData.IsApproverPresent(enScreenName.frmBirthinfo, CStr(Session("Database_Name")), _
                                                      CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                      CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectPersonalInfo), _
                                                      CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(Session("Employeeunkid")), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    msg.DisplayMessage(objApprovalData._Message, Me)
                    Exit Sub
                End If
                'Gajanan [17-April-2019] -- End

                objA_Personalinfo._IsBirthInfo = True 'Gajanan [17-April-2019]
                objA_Personalinfo._Employeeunkid = CInt(Session("Employeeunkid"))

                If objA_Personalinfo._Tranguid.Length > 0 Then
                    pnlBirthInfo.Enabled = False
                    lblbirthinfoapproval.Visible = True



                    cboBirthCounty.SelectedValue = CStr(objA_Personalinfo._Birthcountryunkid)
                    cboBirthState.SelectedValue = CStr(objA_Personalinfo._Birthstateunkid)
                    cboBirthCity.SelectedValue = CStr(objA_Personalinfo._Birthcityunkid)
                    txtBirthWard.Text = objA_Personalinfo._Birth_Ward
                    txtBirthCertNo.Text = CStr(objA_Personalinfo._Birthcertificateno)
                    txtBirthVillage.Text = objA_Personalinfo._Birth_Village
                    cboBrithTown.SelectedValue = CStr(objA_Personalinfo._Birthtownunkid)
                    cboBrithChiefdom.SelectedValue = CStr(objA_Personalinfo._Birthchiefdomunkid)
                    cboBrithVillage.SelectedValue = CStr(objA_Personalinfo._Birthvillageunkid)
                Else
                    lblbirthinfoapproval.Visible = False
                    lblotherinfoapproval.Visible = False


                    cboBirthCounty.SelectedValue = CStr(objEmployee._Birthcountryunkid)
                    cboBirthState.SelectedValue = CStr(objEmployee._Birthstateunkid)
                    cboBirthCity.SelectedValue = CStr(objEmployee._Birthcityunkid)
                    txtBirthWard.Text = objEmployee._Birth_Ward
                    txtBirthCertNo.Text = CStr(objEmployee._Birthcertificateno)
                    txtBirthVillage.Text = objEmployee._Birth_Village
                    cboBrithTown.SelectedValue = CStr(objEmployee._Birthtownunkid)
                    cboBrithChiefdom.SelectedValue = CStr(objEmployee._Birthchiefdomunkid)
                    cboBrithVillage.SelectedValue = CStr(objEmployee._Birthvillageunkid)
                End If


            Else
                cboBirthCounty.SelectedValue = CStr(objEmployee._Birthcountryunkid)
                cboBirthState.SelectedValue = CStr(objEmployee._Birthstateunkid)
                cboBirthCity.SelectedValue = CStr(objEmployee._Birthcityunkid)
                txtBirthWard.Text = objEmployee._Birth_Ward
                txtBirthCertNo.Text = CStr(objEmployee._Birthcertificateno)
                txtBirthVillage.Text = objEmployee._Birth_Village
                cboBrithTown.SelectedValue = CStr(objEmployee._Birthtownunkid)
                cboBrithChiefdom.SelectedValue = CStr(objEmployee._Birthchiefdomunkid)
                cboBrithVillage.SelectedValue = CStr(objEmployee._Birthvillageunkid)
            End If
            'Gajanan [17-April-2019] -- End

            '************************* BIRTH INFO *****************************'

            '************************* WORKING INFO *****************************'
            txtWorkPermitNo.Text = objEmployee._Work_Permit_No
            cboIssueCountry.SelectedValue = CStr(objEmployee._Workcountryunkid)
            txtIssuePlace.Text = objEmployee._Work_Permit_Issue_Place

            If objEmployee._Work_Permit_Issue_Date <> Nothing Then
                dtIssueDate.SetDate = objEmployee._Work_Permit_Issue_Date
            Else
                dtIssueDate.SetDate = Nothing
            End If

            If objEmployee._Work_Permit_Expiry_Date <> Nothing Then
                dtExpiryDate.SetDate = objEmployee._Work_Permit_Expiry_Date
            Else
                dtExpiryDate.SetDate = Nothing
            End If
            '************************* WORKING INFO *****************************'


            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            '************************* WORKING INFO *****************************'
            txtResidentPermitNo.Text = objEmployee._ResidentPermitNo
            cboResidentIssueCountry.SelectedValue = CStr(objEmployee._ResidentIssueCountryId)
            txtResidentIssuePlace.Text = objEmployee._ResidentIssuePlace

            If objEmployee._ResidentIssueDate <> Nothing Then
                dtpResidentIssueDate.SetDate = objEmployee._ResidentIssueDate
            Else
                dtpResidentIssueDate.SetDate = Nothing
            End If

            If objEmployee._ResidentExpiryDate <> Nothing Then
                dtpResidentExpiryDate.SetDate = objEmployee._ResidentExpiryDate
            Else
                dtpResidentExpiryDate.SetDate = Nothing
            End If
            '************************* WORKING INFO *****************************'
            'S.SANDEEP [04-Jan-2018] -- END


            '************************* OTHER INFO *****************************'

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'cboComplexion.SelectedValue = CStr(objEmployee._Complexionunkid)
            'txtExTel.Text = objEmployee._Extra_Tel_No
            'cboBloodGrp.SelectedValue = CStr(objEmployee._Bloodgroupunkid)
            'cboEyeColor.SelectedValue = CStr(objEmployee._Eyecolorunkid)
            'cboLang1.SelectedValue = CStr(objEmployee._Language1unkid)
            'cboLang2.SelectedValue = CStr(objEmployee._Language2unkid)
            'cboLang3.SelectedValue = CStr(objEmployee._Language3unkid)
            'cboLang4.SelectedValue = CStr(objEmployee._Language4unkid)
            'cboEthnicity.SelectedValue = CStr(objEmployee._Ethincityunkid)
            'cboNationality.SelectedValue = CStr(objEmployee._Nationalityunkid)
            'cboReligion.SelectedValue = CStr(objEmployee._Religionunkid)
            'cboHair.SelectedValue = CStr(objEmployee._Hairunkid)
            'txtHeight.Text = CStr(objEmployee._Height)
            'txtWeight.Text = CStr(objEmployee._Weight)
            'cboMaritalStatus.SelectedValue = CStr(objEmployee._Maritalstatusunkid)
            'If objEmployee._Anniversary_Date <> Nothing Then
            '    dtMarriedDate.SetDate = objEmployee._Anniversary_Date
            'Else
            '    dtMarriedDate.SetDate = Nothing
            'End If
            'txtSpHob.Text = objEmployee._Sports_Hobbies

            'If objEmployee._Allergies.Length > 0 Then
            '    For Each StrId As String In objEmployee._Allergies.Split(CChar(","))
            '        For Each mItem As ListItem In chkLstAllergies.Items
            '            If CInt(StrId) = CInt(mItem.Value) Then
            '                mItem.Selected = True
            '            End If
            '        Next
            '    Next
            'End If

            'If objEmployee._Disabilities.Length > 0 Then
            '    For Each StrId As String In objEmployee._Disabilities.Split(CChar(","))
            '        For Each mItem As ListItem In chkLstDisabilities.Items
            '            If CInt(StrId) = CInt(mItem.Value) Then
            '                mItem.Selected = True
            '            End If
            '        Next
            '    Next
            'End If






            'If PersonalinfoApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then
            If OtherInfoApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then


                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If objApprovalData.IsApproverPresent(enScreenName.frmOtherinfo, CStr(Session("Database_Name")), _
                                                      CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                      CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectPersonalInfo), _
                                                      CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(Session("Employeeunkid")), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    msg.DisplayMessage(objApprovalData._Message, Me)
                    Exit Sub
                End If
                'Gajanan [17-April-2019] -- End

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                objA_Personalinfo._IsBirthInfo = False
                objA_Personalinfo._Employeeunkid = CInt(Session("Employeeunkid"))
                'Gajanan [17-April-2019] -- End
                If objA_Personalinfo._Tranguid.Length > 0 Then
                    pnlOtherinfo.Enabled = False
                    lblotherinfoapproval.Visible = True

                    cboComplexion.SelectedValue = CStr(objA_Personalinfo._Complexionunkid)
                    txtExTel.Text = objA_Personalinfo._Extra_Tel_No
                    cboBloodGrp.SelectedValue = CStr(objA_Personalinfo._Bloodgroupunkid)
                    cboEyeColor.SelectedValue = CStr(objA_Personalinfo._Eyecolorunkid)
                    cboLang1.SelectedValue = CStr(objA_Personalinfo._Language1unkid)
                    cboLang2.SelectedValue = CStr(objA_Personalinfo._Language2unkid)
                    cboLang3.SelectedValue = CStr(objA_Personalinfo._Language3unkid)
                    cboLang4.SelectedValue = CStr(objA_Personalinfo._Language4unkid)
                    cboEthnicity.SelectedValue = CStr(objA_Personalinfo._Ethincityunkid)
                    cboNationality.SelectedValue = CStr(objA_Personalinfo._Nationalityunkid)
                    cboReligion.SelectedValue = CStr(objA_Personalinfo._Religionunkid)
                    cboHair.SelectedValue = CStr(objA_Personalinfo._Hairunkid)
                    txtHeight.Text = CStr(objA_Personalinfo._Height)
                    txtWeight.Text = CStr(objA_Personalinfo._Weight)
                    cboMaritalStatus.SelectedValue = CStr(objA_Personalinfo._Maritalstatusunkid)
                    If objA_Personalinfo._Anniversary_Date <> Nothing Then
                        dtMarriedDate.SetDate = objA_Personalinfo._Anniversary_Date
                    Else
                        dtMarriedDate.SetDate = Nothing
                    End If
                    txtSpHob.Text = objA_Personalinfo._Sports_Hobbies
                    If objA_Personalinfo._Allergiesunkids.Length > 0 Then
                        For Each StrId As String In objA_Personalinfo._Allergiesunkids.Split(CChar(","))
                            For Each mItem As ListItem In chkLstAllergies.Items
                                If CInt(StrId) = CInt(mItem.Value) Then
                                    mItem.Selected = True
                                End If
                            Next
                        Next
                    End If

                    If objA_Personalinfo._Disabilitiesunkids.Length > 0 Then
                        For Each StrId As String In objA_Personalinfo._Disabilitiesunkids.Split(CChar(","))
                            For Each mItem As ListItem In chkLstDisabilities.Items
                                If CInt(StrId) = CInt(mItem.Value) Then
                                    mItem.Selected = True
                                End If
                            Next
                        Next
                    End If
                Else
                    cboComplexion.SelectedValue = CStr(objEmployee._Complexionunkid)
                    txtExTel.Text = objEmployee._Extra_Tel_No
                    cboBloodGrp.SelectedValue = CStr(objEmployee._Bloodgroupunkid)
                    cboEyeColor.SelectedValue = CStr(objEmployee._Eyecolorunkid)
                    cboLang1.SelectedValue = CStr(objEmployee._Language1unkid)
                    cboLang2.SelectedValue = CStr(objEmployee._Language2unkid)
                    cboLang3.SelectedValue = CStr(objEmployee._Language3unkid)
                    cboLang4.SelectedValue = CStr(objEmployee._Language4unkid)
                    cboEthnicity.SelectedValue = CStr(objEmployee._Ethincityunkid)
                    cboNationality.SelectedValue = CStr(objEmployee._Nationalityunkid)
                    cboReligion.SelectedValue = CStr(objEmployee._Religionunkid)
                    cboHair.SelectedValue = CStr(objEmployee._Hairunkid)
                    txtHeight.Text = CStr(objEmployee._Height)
                    txtWeight.Text = CStr(objEmployee._Weight)
                    cboMaritalStatus.SelectedValue = CStr(objEmployee._Maritalstatusunkid)
                    If objEmployee._Anniversary_Date <> Nothing Then
                        dtMarriedDate.SetDate = objEmployee._Anniversary_Date
                    Else
                        dtMarriedDate.SetDate = Nothing
                    End If
                    txtSpHob.Text = objEmployee._Sports_Hobbies

                    If objEmployee._Allergies.Length > 0 Then
                        For Each StrId As String In objEmployee._Allergies.Split(CChar(","))
                            For Each mItem As ListItem In chkLstAllergies.Items
                                If CInt(StrId) = CInt(mItem.Value) Then
                                    mItem.Selected = True
                                End If
                            Next
                        Next
                    End If

                    If objEmployee._Disabilities.Length > 0 Then
                        For Each StrId As String In objEmployee._Disabilities.Split(CChar(","))
                            For Each mItem As ListItem In chkLstDisabilities.Items
                                If CInt(StrId) = CInt(mItem.Value) Then
                                    mItem.Selected = True
                                End If
                            Next
                        Next
                    End If
                End If
            Else
                cboComplexion.SelectedValue = CStr(objEmployee._Complexionunkid)
                txtExTel.Text = objEmployee._Extra_Tel_No
                cboBloodGrp.SelectedValue = CStr(objEmployee._Bloodgroupunkid)
                cboEyeColor.SelectedValue = CStr(objEmployee._Eyecolorunkid)
                cboLang1.SelectedValue = CStr(objEmployee._Language1unkid)
                cboLang2.SelectedValue = CStr(objEmployee._Language2unkid)
                cboLang3.SelectedValue = CStr(objEmployee._Language3unkid)
                cboLang4.SelectedValue = CStr(objEmployee._Language4unkid)
                cboEthnicity.SelectedValue = CStr(objEmployee._Ethincityunkid)
                cboNationality.SelectedValue = CStr(objEmployee._Nationalityunkid)
                cboReligion.SelectedValue = CStr(objEmployee._Religionunkid)
                cboHair.SelectedValue = CStr(objEmployee._Hairunkid)
                txtHeight.Text = CStr(objEmployee._Height)
                txtWeight.Text = CStr(objEmployee._Weight)
                cboMaritalStatus.SelectedValue = CStr(objEmployee._Maritalstatusunkid)
                If objEmployee._Anniversary_Date <> Nothing Then
                    dtMarriedDate.SetDate = objEmployee._Anniversary_Date
                Else
                    dtMarriedDate.SetDate = Nothing
                End If
                txtSpHob.Text = objEmployee._Sports_Hobbies

                If objEmployee._Allergies.Length > 0 Then
                    For Each StrId As String In objEmployee._Allergies.Split(CChar(","))
                        For Each mItem As ListItem In chkLstAllergies.Items
                            If CInt(StrId) = CInt(mItem.Value) Then
                                mItem.Selected = True
                            End If
                        Next
                    Next
                End If

                If objEmployee._Disabilities.Length > 0 Then
                    For Each StrId As String In objEmployee._Disabilities.Split(CChar(","))
                        For Each mItem As ListItem In chkLstDisabilities.Items
                            If CInt(StrId) = CInt(mItem.Value) Then
                                mItem.Selected = True
                            End If
                        Next
                    Next
                End If
            End If

            'Gajanan [17-April-2019] -- End
            '************************* OTHER INFO *****************************'


            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            If objEmployee._Isapproved Then
                txtWorkPermitNo.ReadOnly = True
                txtIssuePlace.ReadOnly = True
                cboIssueCountry.Enabled = False
                dtIssueDate.Enabled = False
                dtExpiryDate.Enabled = False

                'S.SANDEEP [04-Jan-2018] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 120
                txtResidentPermitNo.ReadOnly = True
                txtResidentIssuePlace.ReadOnly = True
                cboResidentIssueCountry.Enabled = False
                dtpResidentExpiryDate.Enabled = False
                dtpResidentIssueDate.Enabled = False
                'S.SANDEEP [04-Jan-2018] -- END

            End If
            'Pinkal (28-Dec-2015) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = Session("Employeeunkid")
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
            'Shani(20-Nov-2015) -- End


            '************************* BIRTH INFO *****************************'
            objEmployee._Birthcountryunkid = CInt(cboBirthCounty.SelectedValue)
            objEmployee._Birthstateunkid = CInt(cboBirthState.SelectedValue)
            objEmployee._Birthcityunkid = CInt(cboBirthCity.SelectedValue)
            objEmployee._Birth_Village = txtBirthVillage.Text
            objEmployee._Birth_Ward = txtBirthWard.Text
            objEmployee._Birthcertificateno = txtBirthCertNo.Text
            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            objEmployee._Birthtownunkid = CInt(cboBrithTown.SelectedValue)
            objEmployee._Birthchiefdomunkid = CInt(cboBrithChiefdom.SelectedValue)
            objEmployee._Birthvillageunkid = CInt(cboBrithVillage.SelectedValue)
            'Gajanan [17-April-2019] -- End
            '************************* BIRTH INFO *****************************'

            '************************* WORKING INFO *****************************'
            objEmployee._Work_Permit_No = txtWorkPermitNo.Text
            objEmployee._Workcountryunkid = CInt(cboIssueCountry.SelectedValue)
            objEmployee._Work_Permit_Issue_Place = txtIssuePlace.Text
            If dtIssueDate.IsNull = False Then
                objEmployee._Work_Permit_Issue_Date = dtIssueDate.GetDate
            Else
                objEmployee._Work_Permit_Issue_Date = Nothing
            End If
            If dtExpiryDate.IsNull = False Then
                objEmployee._Work_Permit_Expiry_Date = dtExpiryDate.GetDate
            Else
                objEmployee._Work_Permit_Expiry_Date = Nothing
            End If
            '************************* WORKING INFO *****************************'

            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            '************************* WORKING INFO *****************************'
            objEmployee._ResidentPermitNo = txtResidentPermitNo.Text
            objEmployee._ResidentIssueCountryId = CInt(cboResidentIssueCountry.SelectedValue)
            objEmployee._ResidentIssuePlace = txtResidentIssuePlace.Text

            If dtIssueDate.IsNull = False Then
                objEmployee._ResidentIssueDate = dtIssueDate.GetDate
            Else
                objEmployee._ResidentIssueDate = Nothing
            End If

            If dtExpiryDate.IsNull = False Then
                objEmployee._ResidentExpiryDate = dtExpiryDate.GetDate
            Else
                objEmployee._ResidentExpiryDate = Nothing
            End If
            '************************* WORKING INFO *****************************'
            'S.SANDEEP [04-Jan-2018] -- END

            '************************* OTHER INFO *****************************'
            objEmployee._Complexionunkid = CInt(cboComplexion.SelectedValue)
            objEmployee._Extra_Tel_No = txtExTel.Text
            objEmployee._Bloodgroupunkid = CInt(cboBloodGrp.SelectedValue)
            objEmployee._Eyecolorunkid = CInt(cboEyeColor.SelectedValue)
            objEmployee._Language1unkid = CInt(cboLang1.SelectedValue)
            objEmployee._Language2unkid = CInt(cboLang2.SelectedValue)
            objEmployee._Language3unkid = CInt(cboLang3.SelectedValue)
            objEmployee._Language4unkid = CInt(cboLang4.SelectedValue)
            objEmployee._Ethincityunkid = CInt(cboEthnicity.SelectedValue)
            objEmployee._Nationalityunkid = CInt(cboNationality.SelectedValue)
            objEmployee._Religionunkid = CInt(cboReligion.SelectedValue)
            objEmployee._Hairunkid = CInt(cboHair.SelectedValue)
            objEmployee._Height = CDbl(IIf(txtHeight.Text = "", 0, txtHeight.Text))
            objEmployee._Weight = CDbl(IIf(txtWeight.Text = "", 0, txtWeight.Text))
            objEmployee._Maritalstatusunkid = CInt(cboMaritalStatus.SelectedValue)
            If dtMarriedDate.IsNull = False Then
                objEmployee._Anniversary_Date = dtMarriedDate.GetDate
            Else
                objEmployee._Anniversary_Date = Nothing
            End If
            objEmployee._Sports_Hobbies = txtSpHob.Text

            Dim mStrUnkids As String = String.Empty

            For Each mItem As ListItem In chkLstAllergies.Items
                If mItem.Selected Then
                    mStrUnkids &= "," & mItem.Value
                End If
            Next
            If mStrUnkids.Length > 0 Then mStrUnkids = Mid(mStrUnkids, 2)
            objEmployee._Allergies = mStrUnkids

            mStrUnkids = ""
            For Each mItem As ListItem In chkLstDisabilities.Items
                If mItem.Selected Then
                    mStrUnkids &= "," & mItem.Value
                End If
            Next
            If mStrUnkids.Length > 0 Then mStrUnkids = Mid(mStrUnkids, 2)
            objEmployee._Disabilities = mStrUnkids
            'S.SANDEEP [ 19 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objEmployee._Companyunkid = CInt(Session("CompanyUnkId"))
            'S.SANDEEP [ 19 MAR 2013 ] -- END
            '************************* OTHER INFO *****************************'

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            Blank_ModuleName()
            objEmployee._WebFormName = "frmEmployeeMaster"
            StrModuleName2 = "mnuCoreSetups"
            objEmployee._WebClientIP = CStr(Session("IP_ADD"))
            objEmployee._WebHostName = CStr(Session("HOST_NAME"))
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                objEmployee._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If

            'S.SANDEEP [ 04 JULY 2012 ] -- END

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''Sohail (23 Apr 2012) -- Start
            ''TRA - ENHANCEMENT
            'If objEmployee.Update(, , , Session("UserId"), Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"), Session("Fin_year"), Session("AllowToApproveEarningDeduction")) = False Then
            '    'If objEmployee.Update = False Then
            '    'Sohail (23 Apr 2012) -- End
            '    msg.DisplayError(ex, Me)
            'End If

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

            'Dim blnFlag As Boolean = objEmployee.Update(CStr(Session("Database_Name")), _
            '                                            CInt(Session("Fin_year")), _
            '                                            CInt(Session("CompanyUnkId")), _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                            CStr(Session("IsArutiDemo")), _
            '                                            CInt(Session("Total_Active_Employee_ForAllCompany")), _
            '                                            CInt(Session("NoOfEmployees")), _
            '                                            CInt(Session("UserId")), False, _
            '                                            CStr(Session("UserAccessModeSetting")), _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
            '                                            CBool(Session("IsIncludeInactiveEmp")), _
            '                                            CBool(Session("AllowToApproveEarningDeduction")), _
            '                            ConfigParameter._Object._CurrentDateAndTime.Date, , , , , _
            '                                            CStr(Session("EmployeeAsOnDate")), , _
            '                                            CBool(Session("IsImgInDataBase")))


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'Dim blnFlag As Boolean = objEmployee.Update(CStr(Session("Database_Name")), _
            '                                            CInt(Session("Fin_year")), _
            '                                            CInt(Session("CompanyUnkId")), _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                            CStr(Session("IsArutiDemo")), _
            '                                            CInt(Session("Total_Active_Employee_ForAllCompany")), _
            '                                            CInt(Session("NoOfEmployees")), _
            '                                            CInt(Session("UserId")), False, _
            '                                            CStr(Session("UserAccessModeSetting")), _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
            '                                            CBool(Session("IsIncludeInactiveEmp")), _
            '                                            CBool(Session("AllowToApproveEarningDeduction")), _
            '                                           ConfigParameter._Object._CurrentDateAndTime.Date, CBool(Session("CreateADUserFromEmpMst")), _
            '                                           CBool(Session("UserMustchangePwdOnNextLogon")), Nothing, Nothing, False, "", _
            '                                            CStr(Session("EmployeeAsOnDate")), , _
            '                                            CBool(Session("IsImgInDataBase")))

            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim eLMode As Integer
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                eLMode = enLogin_Mode.MGR_SELF_SERVICE
            Else
                eLMode = enLogin_Mode.EMP_SELF_SERVICE
            End If
            'Gajanan [18-Mar-2019] -- End


            'S.SANDEEP |26-APR-2019| -- START
            Call SaveDocumentIIS()
            'S.SANDEEP |26-APR-2019| -- END


            Dim blnFlag As Boolean = objEmployee.Update(CStr(Session("Database_Name")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(Session("IsArutiDemo")), _
                                                        CInt(Session("Total_Active_Employee_ForAllCompany")), _
                                                        CInt(Session("NoOfEmployees")), _
                                                        CInt(Session("UserId")), False, _
                                                        CStr(Session("UserAccessModeSetting")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), _
                                                        CBool(Session("AllowToApproveEarningDeduction")), _
                                                       ConfigParameter._Object._CurrentDateAndTime.Date, CBool(Session("CreateADUserFromEmpMst")), _
                                                       CBool(Session("UserMustchangePwdOnNextLogon")), Nothing, Nothing, False, "", _
                                                        CStr(Session("EmployeeAsOnDate")), , _
                                                        CBool(Session("IsImgInDataBase")), , Nothing, , , mdtFullAttachment, _
                                                        Session("HOST_NAME").ToString(), Session("IP_ADD").ToString(), Session("UserName").ToString(), CType(eLMode, enLogin_Mode), False, False, mstrModuleName) 'S.SANDEEP |26-APR-2019| -- START {Nothing  -> mdtFullAttachment} -- END
            'Gajanan [17-April-2019] -- End

            'Pinkal (18-Aug-2018) -- End

            If blnFlag = False Then
                msg.DisplayMessage(objEmployee._Message, Me)
            End If

            'Shani(20-Nov-2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'S.SANDEEP |26-APR-2019| -- START
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'Dim dtRow() As DataRow = mdtFullAttachment.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            Dim dtRow() As DataRow = mdtFullAttachment.Select("filename = '" & f.Name & "' AND AUD <> 'D' AND scanattachrefid = " & mintAddressTypeId & " ")
            'Gajanan [17-April-2019] -- End
            If dtRow.Length <= 0 Then
                dRow = mdtFullAttachment.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
                dRow("employeeunkid") = CInt(CInt(Session("Employeeunkid")))
                dRow("filename") = f.Name
                dRow("scanattachrefid") = mintAddressTypeId
                dRow("modulerefid") = enImg_Email_RefId.Employee_Module
                dRow("userunkid") = Session("UserId")
                dRow("AUD") = "A"
                dRow("GUID") = Guid.NewGuid.ToString
                dRow("orgfilepath") = strfullpath
                dRow("valuename") = ""
                dRow("transactionunkid") = -1
                dRow("attached_date") = Today.Date
                dRow("filepath") = strfullpath
                dRow("filename") = f.Name
                dRow("filesize") = f.Length / 1024
                dRow("attached_date") = Today.Date
                dRow("orgfilepath") = strfullpath
                dRow("form_name") = mstrModuleName
                dRow("userunkid") = CInt(Session("userid"))
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
                Dim blnIsInApproval As Boolean = False : objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
                If mintAddressTypeId = enScanAttactRefId.EMPLOYEE_BIRTHINFO Then
                    If BirthInfoApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then
                        blnIsInApproval = True
                    End If
                ElseIf mintAddressTypeId = enScanAttactRefId.EMPLYOEE_OTHERLDETAILS Then
                    If OtherInfoApprovalFlowVal Is Nothing AndAlso CInt(Session("Employeeunkid")) > 0 AndAlso objEmployee._Isapproved Then
                        blnIsInApproval = True
                    End If
                End If
                dRow("isinapproval") = blnIsInApproval
            Else
                msg.DisplayMessage("Selected information is already present for particular employee.", Me)
                Exit Sub
            End If
            mdtFullAttachment.Rows.Add(dRow)
            mdtAttachement = New DataView(mdtFullAttachment, "scanattachrefid = '" & mintAddressTypeId & "'", "", DataViewRowState.CurrentRows).ToTable()
            Call FillAttachment()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            If mdtAttachement Is Nothing Then Exit Sub
            dtView = New DataView(mdtAttachement, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgv_Attchment.AutoGenerateColumns = False
            dgv_Attchment.DataSource = dtView
            dgv_Attchment.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SaveDocumentIIS()
        Try
            If mdtFullAttachment IsNot Nothing Then
                Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                For Each dRow As DataRow In mdtFullAttachment.Rows
                    Dim iScanRefId As Integer = CInt(dRow.Item("scanattachrefid"))
                    Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = iScanRefId) Select (p.Item("Name").ToString)).FirstOrDefault

                    If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                        Dim strFileName As String = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                        If File.Exists(CStr(dRow("orgfilepath"))) Then
                            Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                strPath += "/"
                            End If
                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                            If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                            End If
                            'Gajanan [17-April-2019] -- Start
                            'Enhancement - Implementing Employee Approver Flow On Employee Data.
                            'File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                            File.Copy(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                            'Gajanan [17-April-2019] -- End
                            dRow("fileuniquename") = strFileName
                            dRow("filepath") = strPath

                            dRow.AcceptChanges()
                        Else
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 999, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                        Dim strFilepath As String = dRow("filepath").ToString
                        Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                        If strFilepath.Contains(strArutiSelfService) Then
                            strFilepath = strFilepath.Replace(strArutiSelfService, "")
                            If Strings.Left(strFilepath, 1) <> "/" Then
                                strFilepath = "~/" & strFilepath
                            Else
                                strFilepath = "~" & strFilepath
                            End If

                            If File.Exists(Server.MapPath(strFilepath)) Then
                                File.Delete(Server.MapPath(strFilepath))
                            Else
                                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 999, "Configuration Path does not Exist."), Me)
                                Exit Sub
                            End If
                        Else
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 999, "Configuration Path does not Exist."), Me)
                            Exit Sub
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |26-APR-2019| -- END

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try         'Hemant (13 Aug 2020)
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If




            'Gajanan [18-Mar-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'PersonalinfoApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmPersonalinfo)))
            BirthInfoApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmBirthinfo)))
            OtherInfoApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmOtherinfo)))
            'Gajanan [17-April-2019] -- End

            If (Page.IsPostBack = False) Then

                SetLnaguae()
                clsuser = CType(Session("clsuser"), User)

                'Gajanan [18-Mar-2019] -- End

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    btnsave.Visible = CBool(Session("EditEmployee"))
                    If Session("ShowPending") IsNot Nothing Then
                        btnsave.Visible = Not CBool(Session("ShowPending"))
                    End If
                Else
                    lblEmployee.Visible = False : txtEmployee.Visible = False : btnsave.Visible = CBool(Session("AllowEditPersonalInfo"))
                    'Hemant (01 Jan 2021) -- Start
                    'Enhancement #OLD-238 - Improved Employee Profile page
                    pnl_Employee.Visible = False
                    'Hemant (01 Jan 2021) -- End
                    'Shani [ 24 DEC 2014 ] -- START
                    'Implement Close Button Code on Each Page.
                    chkLstAllergies.Enabled = CBool(Session("AllowEditPersonalInfo"))
                    chkLstDisabilities.Enabled = CBool(Session("AllowEditPersonalInfo"))
                    'Shani [ 24 DEC 2014 ] -- END

                End If
                Call FillCombo()
                'Shani [ 24 DEC 2014 ] -- START
                'Implement Close Button Code on Each Page.
                Call Fill_List()
                'Shani [ 24 DEC 2014 ] -- END
                Call SetInfo()
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                If Request.UrlReferrer IsNot Nothing Then
                    mstrURLReferer = Request.UrlReferrer.AbsoluteUri
                Else
                    mstrURLReferer = Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx"
                End If
                'Sohail (09 Nov 2020) -- End
                'S.SANDEEP |26-APR-2019| -- START
            Else
                mdtAttachement = CType(Me.ViewState("FieldAttachement"), DataTable)
                mblnAttachmentPopup = CBool(Me.ViewState("mblnAttachmentPopup"))
                mdtFullAttachment = CType(Me.ViewState("mdtFullAttachment"), DataTable)
                mintAddressTypeId = CInt(Me.ViewState("mintAddressTypeId"))
                mintDeleteIndex = CInt(Me.ViewState("mintDeleteIndex"))
                'S.SANDEEP |26-APR-2019| -- END
                'Sohail (09 Nov 2020) -- Start
                'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
                mstrURLReferer = ViewState("mstrURLReferer").ToString
                'Sohail (09 Nov 2020) -- End
            End If



            'S.SANDEEP |26-APR-2019| -- START
            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If
            'S.SANDEEP |26-APR-2019| -- END
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'S.SANDEEP |26-APR-2019| -- START
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("FieldAttachement") = mdtAttachement
            Me.ViewState("mblnAttachmentPopup") = mblnAttachmentPopup
            Me.ViewState("mdtFullAttachment") = mdtFullAttachment
            Me.ViewState("mintAddressTypeId") = mintAddressTypeId
            Me.ViewState("mintDeleteIndex") = mintDeleteIndex
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            ViewState("mstrURLReferer") = mstrURLReferer
            'Sohail (09 Nov 2020) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |26-APR-2019| -- END

#End Region

#Region " Button's Event(s) "

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            Call SetValue()
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\HR\wPgEmployeeProfile.aspx")


            'Gajanan [27-May-2019] -- Start              
            'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx")

            'Gajanan [1-NOV-2019] -- Start    
            'Enhancement:Worked On NMB ESS Comment  
            'msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 156, "Save Successfully"), Me, Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx")
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            'msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 502, "Save Successfully"), Me, Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx")
            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 502, "Save Successfully"), Me, mstrURLReferer)
            'Sohail (09 Nov 2020) -- End
            'Gajanan [1-NOV-2019] -- End

            'Gajanan [27-May-2019] -- End

            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\HR\wPgEmployeeProfile.aspx")
            'Sohail (09 Nov 2020) -- Start
            'NMB Enhancement # : - After filling required details on each required page, Redirct back to search job page on close button click in ESS.
            'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeProfile.aspx", False)
            'Response.Redirect(mstrURLReferer, False)
            'Sohail (09 Nov 2020) -- End
            'Nilay (02-Mar-2015) -- End

            If IsNothing(Session("IsfromRecruitment")) = False AndAlso CBool(Session("IsfromRecruitment")) = True Then
                Response.Redirect(Session("rootpath").ToString & "Recruitment/wPgEmployeeUpdate.aspx", False)
                Session.Remove("IsfromRecruitment")
            Else
                Response.Redirect(mstrURLReferer, False)
            End If


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\HR\wPgEmployeeProfile.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END 


    'S.SANDEEP |26-APR-2019| -- START
    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                'mdtAttachement = CType(Me.ViewState("FieldAttachement"), DataTable)
                AddDocumentAttachment(f, f.FullName)
                popup_ScanAttchment.Show()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_AttachementYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonNo_Click
        Try
            If mintDeleteIndex > 0 Then mintDeleteIndex = 0
            popup_ScanAttchment.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_AttachementYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonYes_Click
        Try
            If mintDeleteIndex >= 0 Then
                mdtFullAttachment.Rows(mintDeleteIndex)("AUD") = "D"
                mdtFullAttachment.AcceptChanges()
                mintDeleteIndex = 0
                mdtAttachement = New DataView(mdtFullAttachment, "scanattachrefid = '" & mintAddressTypeId & "'", "", DataViewRowState.CurrentRows).ToTable()
                Call FillAttachment()
                popup_ScanAttchment.Show()
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |26-APR-2019| -- END


    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgv_Attchment.Items.Count <= 0 Then
                msg.DisplayMessage("No Files to download.", Me)
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & lblScanHeader.Text.Replace(" ", "") + ".zip", mdtAttachement, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

#End Region

    'S.SANDEEP |26-APR-2019| -- START
#Region " Link Event(s) "

    Protected Sub lnkScanAttachBirthInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkScanAttachBirthInfo.Click, lnkAttachOtherInfo.Click
        Try
            mintAddressTypeId = 0
            Select Case CType(sender, LinkButton).ID
                Case lnkScanAttachBirthInfo.ID
                    If CInt(cboBirthCounty.SelectedValue) <= 0 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 163, "Sorry, In order to proceed with document attachment, Please select birth country to continue."), Me)
                        Exit Sub
                    End If
                Case lnkAttachOtherInfo.ID

                    Dim sAllergies As List(Of ListItem) = chkLstAllergies.Items.Cast(Of ListItem)().Where(Function(li) li.Selected).ToList()
                    If sAllergies Is Nothing Then
                        sAllergies = New List(Of ListItem)
                    End If
                    Dim sDisabilities As List(Of ListItem) = chkLstDisabilities.Items.Cast(Of ListItem)().Where(Function(li) li.Selected).ToList()
                    If sDisabilities Is Nothing Then
                        sDisabilities = New List(Of ListItem)
                    End If

                    If CInt(cboComplexion.SelectedValue) <= 0 AndAlso _
                    CInt(cboBloodGrp.SelectedValue) <= 0 AndAlso _
                    CInt(cboEyeColor.SelectedValue) <= 0 AndAlso _
                    CInt(cboNationality.SelectedValue) <= 0 AndAlso _
                    CInt(cboEthnicity.SelectedValue) <= 0 AndAlso _
                    CInt(cboReligion.SelectedValue) <= 0 AndAlso _
                    CInt(cboHair.SelectedValue) <= 0 AndAlso _
                    CInt(cboMaritalStatus.SelectedValue) <= 0 AndAlso _
                    txtExTel.Text.Trim.Length <= 0 AndAlso _
                    CInt(cboLang1.SelectedValue) <= 0 AndAlso _
                    CInt(cboLang2.SelectedValue) <= 0 AndAlso _
                    CInt(cboLang3.SelectedValue) <= 0 AndAlso _
                    CInt(cboLang4.SelectedValue) <= 0 AndAlso _
                    txtHeight.Text.Trim.Length <= 0 AndAlso _
                    txtWeight.Text.Trim.Length <= 0 AndAlso _
                    dtMarriedDate.IsNull = True AndAlso _
                    sAllergies.Count <= 0 AndAlso _
                    sDisabilities.Count <= 0 AndAlso _
                    txtSpHob.Text.Trim.Length <= 0 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 162, "Sorry, In order to proceed with document attachment, Please made some changes in other information."), Me)
                        Exit Sub
                    End If
            End Select

            objScanAttachment.GetList(Session("Document_Path").ToString(), "List", "", CInt(Session("Employeeunkid")))
            If mdtFullAttachment Is Nothing Then
                mdtFullAttachment = objScanAttachment._Datatable
            End If

            If mdtFullAttachment IsNot Nothing Then
                Select Case CType(sender, LinkButton).ID
                    Case lnkScanAttachBirthInfo.ID
                        lblScanHeader.Text = objlblCaption.Text & " (" & gbBirthInfo.Text & ")"
                        mintAddressTypeId = CInt(enScanAttactRefId.EMPLOYEE_BIRTHINFO)
                    Case lnkAttachOtherInfo.ID
                        lblScanHeader.Text = objlblCaption.Text & " (" & gbOtherInfo.Text & ")"
                        mintAddressTypeId = CInt(enScanAttactRefId.EMPLYOEE_OTHERLDETAILS)
                End Select
                mdtAttachement = New DataView(mdtFullAttachment, "scanattachrefid = '" & mintAddressTypeId & "'", "", DataViewRowState.CurrentRows).ToTable()
            End If
            mblnAttachmentPopup = True
            Call FillAttachment()
            popup_ScanAttchment.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " GridViewEvent "

    Protected Sub dgv_Attchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgv_Attchment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow = Nothing
                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                'If CInt(e.Item.Cells(3).Text) > 0 Then
                '    xrow = mdtFullAttachment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(3).Text) & "")
                'Else
                '    xrow = mdtFullAttachment.Select("GUID = '" & e.Item.Cells(2).Text & "'")
                'End If
                'If e.CommandName = "Delete" Then
                '    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                '        popup_AttachementYesNo.Message = "Are you sure you want to delete this attachment?"
                '        mintDeleteIndex = mdtFullAttachment.Rows.IndexOf(xrow(0))
                '        popup_AttachementYesNo.Show()
                '    End If
                'End If

                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtFullAttachment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtFullAttachment.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhGUID", False, True)).Text & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        popup_AttachementYesNo.Message = "Are you sure you want to delete this attachment?"
                        mintDeleteIndex = mdtFullAttachment.Rows.IndexOf(xrow(0))
                        popup_AttachementYesNo.Show()
                    End If
                ElseIf e.CommandName = "Download" Then
                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        Dim xPath As String = ""

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("orgfilepath").ToString
                        End If

                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                msg.DisplayMessage("File does not Exist...", Me)
                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & xrow(0)("filename").ToString() & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
            End If
            'S.SANDEEP |16-MAY-2019| -- END
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'S.SANDEEP |26-APR-2019| -- END

#Region " Controls Events "

    Protected Sub cboBirthCounty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBirthCounty.SelectedIndexChanged
        Try
            If CInt(cboBirthCounty.SelectedValue) >= 0 Then
                Dim objState As New clsstate_master
                Dim dsList As New DataSet
                dsList = objState.GetList("List", True, True, CInt(cboBirthCounty.SelectedValue))
                With cboBirthState
                    .DataValueField = "stateunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboBirthState_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBirthState.SelectedIndexChanged
        Try
            If CInt(cboBirthState.SelectedValue) >= 0 Then
                Dim objCity As New clscity_master
                Dim dsList As DataSet = objCity.GetList("List", True, True, CInt(cboBirthState.SelectedValue))
                With cboBirthCity
                    .DataValueField = "cityunkid"
                    .DataTextField = "name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

    Private Sub SetLnaguae()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)

            Me.gbBirthInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbBirthInfo.ID, Me.gbBirthInfo.Text)
            Me.gbWorkPermit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbWorkPermit.ID, Me.gbWorkPermit.Text)
            Me.lblState.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblState.ID, Me.lblState.Text)
            Me.lblVillage.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblVillage.ID, Me.lblVillage.Text)
            Me.lblWard.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblWard.ID, Me.lblWard.Text)
            Me.lblCountry.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCountry.ID, Me.lblCountry.Text)
            Me.lblCity.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCity.ID, Me.lblCity.Text)
            Me.lblCertificateNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCertificateNo.ID, Me.lblCertificateNo.Text)
            Me.lblPlaceOfIssue.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPlaceOfIssue.ID, Me.lblPlaceOfIssue.Text)
            Me.lblExpiryDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExpiryDate.ID, Me.lblExpiryDate.Text)
            Me.lblIssueDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIssueDate.ID, Me.lblIssueDate.Text)
            Me.lblIssueCountry.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIssueCountry.ID, Me.lblIssueCountry.Text)
            Me.lblWorkPermitNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblWorkPermitNo.ID, Me.lblWorkPermitNo.Text)

            Me.gbOtherInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbOtherInfo.ID, Me.gbOtherInfo.Text)
            Me.lblMaritalDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMaritalDate.ID, Me.lblMaritalDate.Text)
            Me.lblSportsHobbies.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSportsHobbies.ID, Me.lblSportsHobbies.Text)
            Me.lblMaritalStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMaritalStatus.ID, Me.lblMaritalStatus.Text)
            Me.lblDisabilities.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDisabilities.ID, Me.lblDisabilities.Text)
            Me.lblAllergies.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAllergies.ID, Me.lblAllergies.Text)
            Me.lblWeight.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblWeight.ID, Me.lblWeight.Text)
            Me.lblHeight.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblHeight.ID, Me.lblHeight.Text)
            Me.lblLanguage4.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLanguage4.ID, Me.lblLanguage4.Text)
            Me.lblLanguage3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLanguage3.ID, Me.lblLanguage3.Text)
            Me.lblLanguage2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLanguage2.ID, Me.lblLanguage2.Text)
            Me.lblLanguage1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLanguage1.ID, Me.lblLanguage1.Text)
            Me.lblExtraTel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExtraTel.ID, Me.lblExtraTel.Text)
            Me.lblHair.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblHair.ID, Me.lblHair.Text)
            Me.lblReligion.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblReligion.ID, Me.lblReligion.Text)
            Me.lblEthinCity.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEthinCity.ID, Me.lblEthinCity.Text)
            Me.lblNationality.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblNationality.ID, Me.lblNationality.Text)
            Me.lblEyeColor.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEyeColor.ID, Me.lblEyeColor.Text)
            Me.lblBloodGroup.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBloodGroup.ID, Me.lblBloodGroup.Text)
            Me.lblComplexion.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblComplexion.ID, Me.lblComplexion.Text)
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblbirthinfoapproval.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblbirthinfoapproval.ID, Me.lblbirthinfoapproval.Text).Replace("&", "")

            Me.lblbirthinfoapproval.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblbirthinfoapproval.ID, Me.lblbirthinfoapproval.Text)
            Me.lblotherinfoapproval.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblotherinfoapproval.ID, Me.lblotherinfoapproval.Text)


            'Language.setLanguage(mstrModuleName1)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End


    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 162, "Sorry, In order to proceed with document attachment, Please made some changes in other information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 163, "Sorry, In order to proceed with document attachment, Please select birth country to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 502, "Save Successfully")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 999, "Configuration Path does not Exist.")

        Catch Ex As Exception
            msg.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
