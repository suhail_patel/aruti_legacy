﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Data
Imports System.Threading
Imports System.IO

#End Region

Partial Class wPgEmpDepentants
    Inherits Basepage

#Region " Private Variable(s) "

    Private msg As New CommonCodes
    Private clsuser As New User
    Private objEmployee As New clsEmployee_Master
    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.

    Private objDependants_Benefice As New clsDependants_Beneficiary_tran
    'Gajanan [22-Feb-2019] -- End
    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim dicNotification As New Dictionary(Of String, String)
    Private trd As Thread
    'S.SANDEEP [ 18 SEP 2012 ] -- END


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmDependantsAndBeneficiaries"
    'Pinkal (06-May-2014) -- End

    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Private mdtDependants As DataTable = Nothing
    Private objDocument As New clsScan_Attach_Documents
    'SHANI (20 JUN 2015) -- End 

    'Shani(28-FEB-2017) -- Start
    'Enhancement - Add New Bank Payment List Integration for TAMA
    Dim mdtMemTran As DataTable
    Dim dtMemTran As DataTable = Nothing
    Dim mblnMem_Popup As Boolean = False
    Dim mintdpndtmembershiptranunkid As Integer = -1
    Dim mstrDepMembershipGUID As String = ""

    Dim mdtBenefitTran As DataTable
    Dim dtBenefitTran As DataTable = Nothing
    Dim mblnBenefit_Popup As Boolean = False
    Dim mintdpndtbenefittranunkid As Integer = -1
    Dim mstrdpndtbenefitGUID As String = ""
    'Shani(28-FEB-2017) -- End


    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objADependant As New clsDependant_beneficiaries_approval_tran
    'Gajanan [3-April-2019] -- Start
    'Dim Arr() As String = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
    'Dim DependantApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmDependantsAndBeneficiariesList)))
    Dim Arr() As String
    Dim DependantApprovalFlowVal As String
    'Gajanan [3-April-2019] -- End
    Private mstrTranGuid As String = String.Empty
    'Gajanan [22-Feb-2019] -- End


    'Gajanan [3-April-2019] -- Start
    Dim mblnisEmployeeApprove As Boolean = False
    'Gajanan [3-April-2019] -- End



    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objMaster As New clsMasterData
        Dim objEmp As New clsEmployee_Master
        Dim objCommon As New clsCommon_Master
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsCombos = objEmp.GetEmployeeList("Employee", True, )
                '    dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END                        
                'End If
                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                  CStr(Session("UserAccessModeSetting")), True, _
                                                  CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                'Shani(24-Aug-2015) -- End

                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                'S.SANDEEP [ 16 JAN 2014 ] -- START
                'With cboEmployee
                '    .DataValueField = "employeeunkid"
                '    .DataTextField = "employeename"
                '    .DataSource = dsCombos.Tables("Employee")
                '    .DataBind()
                '    .SelectedValue = 0
                'End With
                If Session("ShowPending") IsNot Nothing Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"), , , False)
                    dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), False, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                    'Shani(24-Aug-2015) -- End
                    Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
                    With cboEmployee
                        .DataValueField = "employeeunkid"
                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        '.DataTextField = "employeename"
                        .DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        .DataSource = dtTab
                        .DataBind()
                    End With
                Else
                    With cboEmployee
                        .DataValueField = "employeeunkid"
                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        '.DataTextField = "employeename"
                        .DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        .DataSource = dsCombos.Tables("Employee")
                        .DataBind()
                        .SelectedValue = CStr(0)
                    End With
                End If
                'S.SANDEEP [ 16 JAN 2014 ] -- END

            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If

            dsCombos = objMaster.getCountryList("Country", True)
            With drpcountry
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsCombos.Tables("Country").Copy
                .DataBind()
                .SelectedValue = CStr(0)
            End With
            With drpnationality
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsCombos.Tables("Country").Copy
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation")
            With DrpRelation
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Relation")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombos = objMaster.getGenderList("List", True)
            With drpGender
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables(0)
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            dsCombos = objCommon.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboDocumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With
            'SHANI (20 JUN 2015) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try

            objDependants_Benefice = New clsDependants_Beneficiary_tran

            objDependants_Benefice._CompanyId = CInt(Session("CompanyUnkId"))
            objDependants_Benefice._blnImgInDb = CBool(Session("IsImgInDataBase"))

            objDependants_Benefice._Dpndtbeneficetranunkid = CInt(Me.ViewState("Unkid"))

            'Sohail (04 Apr 2013) -- Start
            'TRA - ISSUE - On edit mode, User can change employee.
            If CInt(Me.ViewState("Unkid")) > 0 Then 'Edit Mode
                cboEmployee.Enabled = False
            End If
            'Sohail (04 Apr 2013) -- End          

            'Pinkal (01-Apr-2013) -- Start
            'Enhancement : TRA Changes

            If objDependants_Benefice._blnImgInDb Then
                If objDependants_Benefice._Dpndtbeneficetranunkid > 0 Then
                    If objDependants_Benefice._Photo IsNot Nothing Then
                        imgDependant.ImageUrl = "~\GetImageHandler.ashx?id=" & objDependants_Benefice._Dpndtbeneficetranunkid & "&ModeID=2"
                    Else
                        imgDependant.ImageUrl = ""
                    End If

                End If
            End If

            'Pinkal (01-Apr-2013) -- End


            TxtAddress.Text = objDependants_Benefice._Address
            If objDependants_Benefice._Birthdate <> Nothing Then
                dtpBirthdate.SetDate = objDependants_Benefice._Birthdate.Date
                Call dtpBirthdate_TextChanged(Nothing, Nothing)
            Else
                dtpBirthdate.SetDate = Nothing
            End If

            drpcountry.SelectedValue = CStr(objDependants_Benefice._Countryunkid)
            Call drpcountry_SelectedIndexChanged(Nothing, Nothing)

            If objDependants_Benefice._Stateunkid > 0 Then
                drpState.SelectedValue = CStr(objDependants_Benefice._Stateunkid)
                Call drpState_SelectedIndexChanged(Nothing, Nothing)
            End If

            If objDependants_Benefice._Cityunkid > 0 Then
                drppostTown.SelectedValue = CStr(objDependants_Benefice._Cityunkid)
                Call drppostTown_SelectedIndexChanged(Nothing, Nothing)
            End If

            If objDependants_Benefice._Zipcodeunkid > 0 Then
                drppostcode.SelectedValue = CStr(objDependants_Benefice._Zipcodeunkid)
            End If


            txtemail.Text = objDependants_Benefice._Email
            cboEmployee.SelectedValue = CStr(objDependants_Benefice._Employeeunkid)
            'Shani(04-MAR-2017) -- Start
            'Enhancement - Add Membership and Benefit in employee dependant's screen on web
            Call cboEmployee_SelectedIndexChanged(cboEmployee, Nothing)
            'Shani(04-MAR-2017) -- End
            txtFirstName.Text = objDependants_Benefice._First_Name
            txtidno.Text = objDependants_Benefice._Identify_No
            objDependants_Benefice._Isdependant = objDependants_Benefice._Isdependant
            txtLastName.Text = objDependants_Benefice._Last_Name
            txtMiddleName.Text = objDependants_Benefice._Middle_Name
            txtmobileno.Text = objDependants_Benefice._Mobile_No
            drpnationality.SelectedValue = CStr(objDependants_Benefice._Nationalityunkid)
            txtpostbox.Text = objDependants_Benefice._Post_Box
            DrpRelation.SelectedValue = CStr(objDependants_Benefice._Relationunkid)
            txtResNo.Text = objDependants_Benefice._Telephone_No
            chkBeneficiaries.Checked = CBool(objDependants_Benefice._Isdependant)
            drpGender.SelectedValue = CStr(objDependants_Benefice._Gender)
            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If objDependants_Benefice._Effective_date <> Nothing Then
                dtpEffectiveDate.SetDate = objDependants_Benefice._Effective_date
            Else
                dtpEffectiveDate.SetDate = Nothing
            End If
            'Sohail (18 May 2019) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue()
        Try

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.


            ''Pinkal (01-Apr-2013) -- Start
            ''Enhancement : TRA Changes

            'objDependants_Benefice._CompanyId = CInt(Session("CompanyUnkId"))
            'objDependants_Benefice._blnImgInDb = CBool(Session("IsImgInDataBase"))

            ''Pinkal (01-Apr-2013) -- End

            'objDependants_Benefice._Dpndtbeneficetranunkid = CInt(Me.ViewState("Unkid"))
            'objDependants_Benefice._Address = TxtAddress.Text

            'If dtpBirthdate.IsNull = False Then
            '    objDependants_Benefice._Birthdate = dtpBirthdate.GetDate.Date
            'Else
            '    objDependants_Benefice._Birthdate = Nothing
            'End If

            'objDependants_Benefice._Countryunkid = CInt(drpcountry.SelectedValue)
            'objDependants_Benefice._Stateunkid = CInt(IIf(drpState.SelectedValue = "", 0, drpState.SelectedValue))
            'objDependants_Benefice._Cityunkid = CInt(IIf(drppostTown.SelectedValue = "", 0, drppostTown.SelectedValue))
            'objDependants_Benefice._Zipcodeunkid = CInt(IIf(drppostcode.SelectedValue = "", 0, drppostcode.SelectedValue))
            'objDependants_Benefice._Email = txtemail.Text
            'objDependants_Benefice._Employeeunkid = CInt(cboEmployee.SelectedValue)
            'objDependants_Benefice._First_Name = txtFirstName.Text
            'objDependants_Benefice._Identify_No = txtidno.Text
            'objDependants_Benefice._Last_Name = txtLastName.Text
            'objDependants_Benefice._Middle_Name = txtMiddleName.Text
            'objDependants_Benefice._Mobile_No = txtmobileno.Text
            'objDependants_Benefice._Nationalityunkid = CInt(drpnationality.SelectedValue)
            'objDependants_Benefice._Post_Box = txtpostbox.Text
            'objDependants_Benefice._Relationunkid = CInt(DrpRelation.SelectedValue)
            'objDependants_Benefice._Telephone_No = txtResNo.Text
            'objDependants_Benefice._Isdependant = chkBeneficiaries.Checked
            'objDependants_Benefice._Gender = CInt(drpGender.SelectedValue)
            'objDependants_Benefice._Userunkid = CInt(Session("UserId"))


            'Blank_ModuleName()


            ''Pinkal (01-Apr-2013) -- Start
            ''Enhancement : TRA Changes

            ''clsCommonATLog._WebFormName = "frmDependantsAndBeneficiaries"
            ''StrModuleName2 = "mnuCoreSetups"
            ''clsCommonATLog._WebClientIP = Session("IP_ADD")
            ''clsCommonATLog._WebHostName = Session("HOST_NAME")

            'objDependants_Benefice._WebFormName = "frmDependantsAndBeneficiaries"
            'StrModuleName2 = "mnuCoreSetups"
            'objDependants_Benefice._WebClientIP = CStr(Session("IP_ADD"))
            'objDependants_Benefice._WebHostName = CStr(Session("HOST_NAME"))

            ''Pinkal (01-Apr-2013) -- End

            ''Pinkal (24-Aug-2012) -- Start
            ''Enhancement : TRA Changes

            'If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
            '    clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            ''Pinkal (24-Aug-2012) -- End



            ''Pinkal (01-Apr-2013) -- Start
            ''Enhancement : TRA Changes

            'If CBool(Session("IsImgInDataBase")) Then
            '    If imgDependant.ImageUrl <> "" AndAlso imgDependant.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False Then
            '        objDependants_Benefice._Photo = ImageCompression(Server.MapPath(imgDependant.ImageUrl))
            '    ElseIf imgDependant.ImageUrl = "" Then
            '        objDependants_Benefice._Photo = Nothing
            '    End If
            'End If

            ''Pinkal (01-Apr-2013) -- End

            'Gajanan [3-April-2019] -- Start
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(cboEmployee.SelectedValue)
            mblnisEmployeeApprove = objEmployee._Isapproved
            'Gajanan [3-April-2019] -- End

            If DependantApprovalFlowVal Is Nothing Then
                objADependant._Audittype = enAuditType.ADD
                objADependant._Audituserunkid = CInt(Me.ViewState("Unkid"))


                objADependant._Address = TxtAddress.Text
                If dtpBirthdate.IsNull = False Then
                    objADependant._Birthdate = dtpBirthdate.GetDate.Date
                Else
                    objADependant._Birthdate = Nothing
                End If

                objADependant._Cityunkid = CInt(IIf(drppostTown.SelectedValue = "", 0, drppostTown.SelectedValue))
                objADependant._Countryunkid = CInt(drpcountry.SelectedValue)
                objADependant._Email = txtemail.Text
                objADependant._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objADependant._First_Name = txtFirstName.Text
                objADependant._Identify_No = txtidno.Text
                objADependant._Isdependant = chkBeneficiaries.Checked

                objADependant._Last_Name = txtLastName.Text
                objADependant._Middle_Name = txtMiddleName.Text
                objADependant._Mobile_No = txtmobileno.Text
                objADependant._Nationalityunkid = CInt(drpnationality.SelectedValue)
                objADependant._Post_Box = txtpostbox.Text
                objADependant._Relationunkid = CInt(DrpRelation.SelectedValue)
                objADependant._Stateunkid = CInt(IIf(drpState.SelectedValue = "", 0, drpState.SelectedValue))
                objADependant._Telephone_No = txtResNo.Text
                objADependant._Zipcodeunkid = CInt(IIf(drppostcode.SelectedValue = "", 0, drppostcode.SelectedValue))
                objADependant._Gender = CInt(drpGender.SelectedValue)
                objADependant._CompanyId = CInt(Session("CompanyUnkId"))
                objADependant._blnImgInDb = CBool(Session("IsImgInDataBase"))


                objADependant._Isvoid = False
                objADependant._Tranguid = Guid.NewGuid.ToString()
                objADependant._Transactiondate = Now
                objADependant._Approvalremark = ""
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                'objADependant._Isweb = False
                objADependant._Isweb = True
                'Sohail (18 May 2019) -- End
                objADependant._Isfinal = False
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                objADependant._Effective_date = dtpEffectiveDate.GetDate
                objADependant._IsFromStatus = False
                'Sohail (18 May 2019) -- End
                objADependant._Ip = CStr(Session("IP_ADD"))
                objADependant._Host = (CStr(Session("HOST_NAME")))
                objADependant._Form_Name = "frmDependantsAndBeneficiaries"
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                objADependant._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
                'Sohail (18 May 2019) -- End
                objADependant._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval

                If CInt(Me.ViewState("Unkid")) <= 0 Then
                    objADependant._Operationtypeid = clsEmployeeDataApproval.enOperationType.ADDED
                    objADependant._Dpndtbeneficetranunkid = -1
                Else
                    objADependant._Dpndtbeneficetranunkid = CInt(Me.ViewState("Unkid"))
                    objADependant._Operationtypeid = clsEmployeeDataApproval.enOperationType.EDITED
                End If

                If CBool(Session("IsImgInDataBase")) Then
                    If imgDependant.ImageUrl <> "" AndAlso imgDependant.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False Then
                        objADependant._Photo = ImageCompression(Server.MapPath(imgDependant.ImageUrl))
                    ElseIf imgDependant.ImageUrl = "" Then
                        objADependant._Photo = Nothing
                    End If
                Else
                    objADependant._ImagePath = ""
                End If


                'Gajanan [19-NOV-2019] -- Start   
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    objADependant._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                Else
                    objADependant._Loginemployeeunkid = -1
                End If
                'Gajanan [19-NOV-2019] -- End

            Else
                objDependants_Benefice._Dpndtbeneficetranunkid = CInt(Me.ViewState("Unkid"))
                objDependants_Benefice._Address = TxtAddress.Text
                If dtpBirthdate.IsNull = False Then
                    objDependants_Benefice._Birthdate = dtpBirthdate.GetDate.Date
                Else
                    objDependants_Benefice._Birthdate = Nothing
                End If

                objDependants_Benefice._Cityunkid = CInt(IIf(drppostTown.SelectedValue = "", 0, drppostTown.SelectedValue))
                objDependants_Benefice._Countryunkid = CInt(drpcountry.SelectedValue)
                objDependants_Benefice._Email = txtemail.Text
                objDependants_Benefice._Employeeunkid = CInt(cboEmployee.SelectedValue)
                objDependants_Benefice._First_Name = txtFirstName.Text
                objDependants_Benefice._Identify_No = txtidno.Text

                objDependants_Benefice._Isdependant = chkBeneficiaries.Checked
                objDependants_Benefice._Last_Name = txtLastName.Text
                objDependants_Benefice._Middle_Name = txtMiddleName.Text
                objDependants_Benefice._Mobile_No = txtmobileno.Text
                objDependants_Benefice._Nationalityunkid = CInt(drpnationality.SelectedValue)
                objDependants_Benefice._Post_Box = txtpostbox.Text
                objDependants_Benefice._Relationunkid = CInt(DrpRelation.SelectedValue)
                objDependants_Benefice._Stateunkid = CInt(IIf(drpState.SelectedValue = "", 0, drpState.SelectedValue))
                objDependants_Benefice._Telephone_No = txtResNo.Text

                objDependants_Benefice._Zipcodeunkid = CInt(IIf(drppostcode.SelectedValue = "", 0, drppostcode.SelectedValue))
                objDependants_Benefice._Gender = CInt(drpGender.SelectedValue)
                objDependants_Benefice._CompanyId = CInt(Session("CompanyUnkId"))
                objDependants_Benefice._blnImgInDb = CBool(Session("IsImgInDataBase"))

                objDependants_Benefice._Isvoid = False
                objDependants_Benefice._Userunkid = CInt(Session("UserId"))
                objDependants_Benefice._Voiddatetime = Nothing
                objDependants_Benefice._Voidreason = ""
                'Sohail (18 May 2019) -- Start
                'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
                objDependants_Benefice._Effective_date = dtpEffectiveDate.GetDate
                objDependants_Benefice._IsFromStatus = False
                'Sohail (18 May 2019) -- End

                Blank_ModuleName()

                objDependants_Benefice._WebFormName = mstrModuleName
                objDependants_Benefice._WebClientIP = CStr(Session("IP_ADD"))
                objDependants_Benefice._WebHostName = CStr(Session("HOST_NAME"))
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                    clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                Else
                    clsCommonATLog._LoginEmployeeUnkid = -1
                End If

                If CBool(Session("IsImgInDataBase")) Then
                    If imgDependant.ImageUrl <> "" AndAlso imgDependant.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False Then
                        objDependants_Benefice._Photo = ImageCompression(Server.MapPath(imgDependant.ImageUrl))
                    ElseIf imgDependant.ImageUrl = "" Then
                        objDependants_Benefice._Photo = Nothing
                    End If
                Else
                    objDependants_Benefice._ImagePath = ""
                End If

            End If
            'Gajanan [22-Feb-2019] -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), Me)
                'Pinkal (06-May-2014) -- End
                Return False
            End If

            If txtFirstName.Text.Trim = "" Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Firstname can not be blank. Firstname is compulsory information."), Me)
                'Pinkal (06-May-2014) -- End
                Return False
            End If

            If txtLastName.Text.Trim = "" Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Lastname can not be blank. Lastname is compulsory information."), Me)
                'Pinkal (06-May-2014) -- End
                Return False
            End If

            If CInt(DrpRelation.SelectedValue) <= 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Relation is compulsory information. Please select relation to continue."), Me)
                'Pinkal (06-May-2014) -- End
                Return False
            End If
            'S.SANDEEP [ 11 MAR 2014 ] -- START
            'Dim strExpression As String = "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"
            'Dim Expression As New System.Text.RegularExpressions.Regex(strExpression)
            Dim Expression As New System.Text.RegularExpressions.Regex(iEmailRegxExpression)
            'S.SANDEEP [ 11 MAR 2014 ] -- END
            If txtemail.Text.Length > 0 Then
                If Expression.IsMatch(txtemail.Text.Trim) = False Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Invalid Email.Please Enter Valid Email."), Me)
                    'Pinkal (06-May-2014) -- End
                    Return False
                End If
            End If

            If dtpBirthdate.IsNull = False Then
                If dtpBirthdate.GetDate.Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then
                    'Pinkal (06-May-2014) -- Start
                    'Enhancement : Language Changes 
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Date of birth cannot be greater than or equal to today date."), Me)
                    'Pinkal (06-May-2014) -- End
                    Return False
                End If
            End If


            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsDependant_AgeLimit_Set Then
            If CBool(Session("IsDependant_AgeLimit_Set")) = True Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
                If dtpBirthdate.IsNull = False Then
                    If CDbl(Me.ViewState("AgeLimit")) > 0 Then
                        If DateDiff(DateInterval.Year, dtpBirthdate.GetDate.Date, ConfigParameter._Object._CurrentDateAndTime.Date) > CDbl(Me.ViewState("AgeLimit")) Then
                            'Pinkal (06-May-2014) -- Start
                            'Enhancement : Language Changes 
                            'Language.setLanguage(mstrModuleName)
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "Sorry, particular person cannot be consider as dependants or beneficairy. Reason : the age is greater than the limit set.") & Me.ViewState("AgeLimit").ToString, Me)
                            'Pinkal (06-May-2014) -- End
                            Return False
                        End If
                    End If
                End If
            End If

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If CInt(Me.ViewState("Unkid")) <= 0 Then
                If dtpEffectiveDate.IsNull = True Then
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 48, "Please set effective date."), Me)
                    dtpEffectiveDate.Focus()
                    Return False
                ElseIf eZeeDate.convertDate(dtpEffectiveDate.GetDate.Date) > eZeeDate.convertDate(DateAndTime.Today.Date) Then
                    'Language.setLanguage(mstrModuleName)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 49, "Effective date should not be greater than current date."), Me)
                    dtpEffectiveDate.Focus()
                    Return False
                End If
            End If
            'Sohail (18 May 2019) -- End

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            If DependantApprovalFlowVal Is Nothing Then
                Dim intOperationType As Integer = 0
                Dim strMsg As String = ""

                'Gajanan [10-June-2019] -- Start      
                'If objDependants_Benefice.isExist(txtFirstName.Text, txtMiddleName.Text, txtLastName.Text, txtidno.Text, CInt(cboEmployee.SelectedValue), CInt(Me.ViewState("Unkid")), Nothing) = True Then
                If objDependants_Benefice.isExist(txtFirstName.Text, txtLastName.Text, txtMiddleName.Text, txtidno.Text, CInt(cboEmployee.SelectedValue), CInt(Me.ViewState("Unkid")), Nothing) = True Then
                    'Gajanan [10-June-2019] -- End
                    If chkBeneficiaries.Checked = False Then
                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1013, "This Dependant is already defined. Please define new Dependant.")
                    Else
                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1014, "This Beneficiary is already defined. Please define new Beneficiary.")
                    End If

                End If


                'Gajanan [10-June-2019] -- Start      
                'If objADependant.isExist(txtFirstName.Text, txtMiddleName.Text, txtLastName.Text, txtidno.Text, CInt(cboEmployee.SelectedValue), "", Nothing, False, intOperationType) = True Then
                If objADependant.isExist(txtFirstName.Text, txtLastName.Text, txtMiddleName.Text, txtidno.Text, CInt(cboEmployee.SelectedValue), "", Nothing, False, intOperationType) = True Then
                    'Gajanan [10-June-2019] -- End
                    If intOperationType > 0 Then
                        Select Case intOperationType
                            Case clsEmployeeDataApproval.enOperationType.EDITED
                                If CInt(Me.ViewState("Unkid")) > 0 Then
                                    If chkBeneficiaries.Checked = False Then
                                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1001, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in edit mode.")

                                        'Gajanan [10-June-2019] -- Start      
                                        msg.DisplayMessage(strMsg, Me)
                                        Return False
                                        'Gajanan [10-June-2019] -- End

                                    Else
                                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1002, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in edit mode.")
                                        'Gajanan [10-June-2019] -- Start      
                                        msg.DisplayMessage(strMsg, Me)
                                        Return False
                                        'Gajanan [10-June-2019] -- End
                                    End If
                                Else
                                    If chkBeneficiaries.Checked = False Then
                                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1003, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in edit mode.")
                                        'Gajanan [10-June-2019] -- Start      
                                        msg.DisplayMessage(strMsg, Me)
                                        Return False
                                        'Gajanan [10-June-2019] -- End
                                    Else
                                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1004, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in edit mode.")
                                        'Gajanan [10-June-2019] -- Start      
                                        msg.DisplayMessage(strMsg, Me)
                                        Return False
                                        'Gajanan [10-June-2019] -- End
                                    End If
                                End If
                            Case clsEmployeeDataApproval.enOperationType.DELETED

                                If CInt(Me.ViewState("Unkid")) > 0 Then
                                    If chkBeneficiaries.Checked = False Then
                                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1005, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in deleted mode.")
                                        'Gajanan [10-June-2019] -- Start      
                                        msg.DisplayMessage(strMsg, Me)
                                        Return False
                                        'Gajanan [10-June-2019] -- End
                                    Else
                                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1006, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in deleted mode.")
                                        'Gajanan [10-June-2019] -- Start      
                                        msg.DisplayMessage(strMsg, Me)
                                        Return False
                                        'Gajanan [10-June-2019] -- End
                                    End If
                                Else
                                    If chkBeneficiaries.Checked = False Then
                                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1007, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in deleted mode.")
                                        'Gajanan [10-June-2019] -- Start      
                                        msg.DisplayMessage(strMsg, Me)
                                        Return False
                                        'Gajanan [10-June-2019] -- End
                                    Else
                                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1008, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in deleted mode.")
                                        'Gajanan [10-June-2019] -- Start      
                                        msg.DisplayMessage(strMsg, Me)
                                        Return False
                                        'Gajanan [10-June-2019] -- End
                                    End If
                                End If


                            Case clsEmployeeDataApproval.enOperationType.ADDED

                                If CInt(Me.ViewState("Unkid")) > 0 Then
                                    If chkBeneficiaries.Checked = False Then
                                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1009, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in added mode.")
                                        'Gajanan [10-June-2019] -- Start      
                                        msg.DisplayMessage(strMsg, Me)
                                        Return False
                                        'Gajanan [10-June-2019] -- End
                                    Else
                                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1010, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in added mode.")
                                        'Gajanan [10-June-2019] -- Start      
                                        msg.DisplayMessage(strMsg, Me)
                                        Return False
                                        'Gajanan [10-June-2019] -- End
                                    End If
                                Else
                                    If chkBeneficiaries.Checked = False Then
                                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1011, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in added mode.")
                                        'Gajanan [10-June-2019] -- Start      
                                        msg.DisplayMessage(strMsg, Me)
                                        Return False
                                        'Gajanan [10-June-2019] -- End
                                    Else
                                        strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1012, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in added mode.")
                                        'Gajanan [10-June-2019] -- Start      
                                        msg.DisplayMessage(strMsg, Me)
                                        Return False
                                        'Gajanan [10-June-2019] -- End
                                    End If
                                End If
                        End Select
                    End If
                End If

                If strMsg.Trim.Length > 0 Then
                    msg.DisplayMessage(strMsg, Me)
                    Return False
                End If
            End If
            'Gajanan [22-Feb-2019] -- End

            Return True

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub ClearObject()
        Try
            TxtAddress.Text = ""
            txtAge.Text = ""
            txtemail.Text = ""
            txtFirstName.Text = ""
            txtidno.Text = ""
            txtLastName.Text = ""
            txtMiddleName.Text = ""
            txtmobileno.Text = ""
            txtpostbox.Text = ""
            txtResNo.Text = ""
            drpcountry.SelectedValue = CStr(0)
            drpGender.SelectedValue = CStr(0)
            drpnationality.SelectedValue = CStr(0)
            drppostcode.SelectedValue = CStr(0)
            drppostTown.SelectedValue = CStr(0)
            DrpRelation.SelectedValue = CStr(0)
            drpState.SelectedValue = CStr(0)

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            dtpAttachment.SetDate = Today.Date
            cboDocumentType.SelectedValue = CStr(0)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

            'Shani(07-Jan-2016) -- Start
            'Passing Qualificiation tranunkid for other qualification, reason the attachment is not coming for Other Qualification.
            'mdtDependants = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.DEPENDANTS, Me.ViewState("Unkid"), Session("Document_Path"))
            mdtDependants = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.DEPENDANTS, CInt(Me.ViewState("Unkid")), CStr(Session("Document_Path"))).Copy
            If CInt(Me.ViewState("Unkid")) <= 0 Then
                mdtDependants.Rows.Clear()
            End If
            'Shani(07-Jan-2016) -- End

            'Shani(20-Nov-2015) -- End

            'SHANI (20 JUN 2015) -- End 

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Public Function b64decode(ByVal StrDecode As String) As String
        Dim decodedString As String = ""
        Try
            decodedString = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(StrDecode))
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        Return decodedString
    End Function

    Private Function IsFromDependantList() As Boolean
        Try
            If (Request.QueryString("ProcessId") <> "") Then
                Dim mintIdTranUnkid As Integer = -1
                mintIdTranUnkid = CInt(Val(b64decode(Request.QueryString("ProcessId"))))
                Me.ViewState.Add("Unkid", mintIdTranUnkid)
                Call GetValue()
            Else
                Me.ViewState.Add("Unkid", -1)
            End If


            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                If Me.ViewState("Unkid") Is Nothing Or CInt(Me.ViewState("Unkid")) <= 0 Then
                    btnsave.Visible = CBool(Session("AddDependant"))
                Else
                    btnsave.Visible = CBool(Session("EditDependant"))
                End If

                'S.SANDEEP [ 16 JAN 2014 ] -- START
                If Session("ShowPending") IsNot Nothing Then
                    btnsave.Visible = False
                End If
                'S.SANDEEP [ 16 JAN 2014 ] -- END

            Else

                If Me.ViewState("Unkid") Is Nothing Or CInt(Me.ViewState("Unkid")) <= 0 Then
                    btnsave.Visible = CBool(Session("AllowAddDependants"))
                Else
                    btnsave.Visible = CBool(Session("AllowEditDependants"))
                End If
            End If

            'Pinkal (22-Nov-2012) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    'S.SANDEEP [ 18 SEP 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    Private Function Set_Notification(ByVal StrUserName As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Dim blnFlag As Boolean = False
        Dim objEmployee As New clsEmployee_Master

        Try
            objDependants_Benefice._Dpndtbeneficetranunkid = CInt(Me.ViewState("Unkid"))

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmployee.SelectedValue)
            'Shani(20-Nov-2015) -- End

            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            If Session("Notify_EmplData").ToString.Trim.Length > 0 Then
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("Dear <b>" & StrUserName & "</b></span></p>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                'S.SANDEEP [ 18 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee : <b>" & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & "</b></span></p>")
                StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that changes have been made for employee dependant(s).</span></p>")
                'S.SANDEEP [ 18 DEC 2012 ] -- END
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

                If CInt(Session("UserId")) > 0 Then
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmployee._Employeecode & "</b>. Following information has been changed by user : <b>" & objUser._Firstname.Trim & " " & objUser._Lastname & "</b></span></p>")
                ElseIf CInt(Session("Employeeunkid")) > 0 Then
                    StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; with employeecode : <b>" & objEmployee._Employeecode & "</b>. Following information has been changed by user : <b>" & objEmployee._Firstname.Trim & " " & objEmployee._Othername & " " & objEmployee._Surname & "</b></span></p>")
                End If


                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; from Machine : <b>" & Session("HOST_NAME").ToString & "</b> and IPAddress : <b>" & Session("IP_ADD").ToString & "</b></span></p>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TABLE border = '1' WIDTH = '50%'>")
                StrMessage.Append(vbCrLf)
                'S.SANDEEP [ 18 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append("<TD align = 'LEFT' COLSPAN = '3'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'> Employee : " & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & "</span></b></TD>")
                StrMessage.Append("</TR>")
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append("<TD align = 'LEFT' COLSPAN = '3'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'> Depandant : " & txtFirstName.Text & " " & IIf(txtMiddleName.Text <> "", txtMiddleName.Text, "").ToString & " " & txtLastName.Text & "</span></b></TD>")
                StrMessage.Append("</TR>")
                'S.SANDEEP [ 18 DEC 2012 ] -- END
                StrMessage.Append("<TR WIDTH = '50%' bgcolor= 'SteelBlue'>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("<TD align = 'LEFT' STYLE='WIDTH:15%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Field" & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Old Value" & "</span></b></TD>")
                StrMessage.Append("<TD align = 'MIDDLE' STYLE='WIDTH:10%'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "New Value" & "</span></b></TD>")
                StrMessage.Append(vbCrLf)
                StrMessage.Append("</TR>")
                For Each sId As String In Session("Notify_EmplData").ToString.Split(CChar("||"))(0).Split(CChar(","))
                    Select Case CInt(sId)
                        Case enEmployeeData.DEPENDENTS

                            If objDependants_Benefice._First_Name.Trim <> txtFirstName.Text.Trim Or objDependants_Benefice._Middle_Name.Trim <> txtMiddleName.Text.Trim Or objDependants_Benefice._Last_Name.Trim <> txtLastName.Text.Trim Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Dependant Full Name" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objDependants_Benefice._First_Name.Trim & " " & objDependants_Benefice._Middle_Name.Trim & " " & objDependants_Benefice._Last_Name.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & txtFirstName.Text.Trim & " " & txtMiddleName.Text.Trim & " " & txtLastName.Text.Trim & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objDependants_Benefice._Birthdate.Date <> dtpBirthdate.GetDate.Date Then
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Birth Date" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(objDependants_Benefice._Birthdate <> Nothing, objDependants_Benefice._Birthdate.Date.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(Not dtpBirthdate.IsNull, dtpBirthdate.GetDate.ToShortDateString, "")) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objDependants_Benefice._Relationunkid <> CInt(DrpRelation.SelectedValue) Then
                                Dim objRelation As New clsCommon_Master
                                objRelation._Masterunkid = objDependants_Benefice._Relationunkid
                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Relation" & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & objRelation._Name.Trim & "</span></TD>")
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(DrpRelation.SelectedValue) <= 0, "", DrpRelation.SelectedItem.Text.Trim)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If

                            If objDependants_Benefice._Gender <> CInt(drpGender.SelectedValue) Then
                                Dim objGender As New clsMasterData
                                Dim dsList As DataSet = objGender.getGenderList("List", False)

                                Dim drRow() As DataRow = dsList.Tables(0).Select("id <> 0 AND id = " & objDependants_Benefice._Gender)

                                StrMessage.Append("<TR WIDTH = '50%'>" & vbCrLf)
                                StrMessage.Append(vbCrLf)
                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '15%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & "Gender" & "</span></TD>")

                                If drRow.Length > 0 Then
                                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & drRow(0)("Name").ToString() & "</span></TD>")
                                Else
                                    StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & " " & "</span></TD>")
                                End If

                                StrMessage.Append("<TD align = 'LEFT' WIDTH = '17%'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>" & CStr(IIf(CInt(drpGender.SelectedValue) <= 0, "", drpGender.SelectedItem.Text.Trim)) & "</span></TD>")
                                StrMessage.Append("</TR>" & vbCrLf)
                                blnFlag = True
                            End If


                    End Select
                Next
            End If
            StrMessage.Append("</TABLE>")
            StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")

            If blnFlag = False Then
                StrMessage = StrMessage.Remove(0, StrMessage.Length)
            End If

            Return StrMessage.ToString
        Catch ex As Exception
            msg.DisplayError(ex, Me)
            Return ""
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If CInt(Me.ViewState("Unkid")) > 0 Then
                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = "Notification of Changes in Employee Dependant(s)."
                            objSendMail._Message = dicNotification(sKey)
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If

            ElseIf CInt(Me.ViewState("Unkid")) <= 0 Then
                If dicNotification.Keys.Count > 0 Then
                    Dim objSendMail As New clsSendMail
                    For Each sKey As String In dicNotification.Keys
                        If dicNotification(sKey).Trim.Length > 0 Then
                            objSendMail._ToEmail = sKey
                            objSendMail._Subject = "Notifications to newly added Employee Dependant(s)."
                            Dim sMsg As String = dicNotification(sKey)

                            If CInt(Session("UserId")) > 0 Then
                                Dim objUser As New clsUserAddEdit
                                objUser._Userunkid = CInt(Session("UserId"))
                                Set_Notification(objUser._Firstname & " " & objUser._Lastname)
                            ElseIf CInt(Session("Employeeunkid")) > 0 Then
                                Dim objEmp As New clsEmployee_Master

                                'Shani(20-Nov-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'objEmp._Employeeunkid = CInt(Session("Employeeunkid"))
                                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(Session("Employeeunkid"))
                                'Shani(20-Nov-2015) -- End

                                Set_Notification(objEmp._Firstname & "  " & objEmp._Surname)
                            End If
                            objSendMail._Message = sMsg
                            'Sohail (30 Nov 2017) -- Start
                            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                            'objSendMail.SendMail()
                            objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                            'Sohail (30 Nov 2017) -- End
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP [ 18 SEP 2012 ] -- END


    'Pinkal (01-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Private Sub DeleteImage()
        If imgDependant.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False AndAlso System.IO.File.Exists(Server.MapPath(imgDependant.ImageUrl)) Then
            Try
                Dim mstrFile As String = Server.MapPath(imgDependant.ImageUrl)
                imgDependant.ImageUrl = ""
                imgDependant.Dispose()
                'Shani(01-MAR-2017) -- Start
                'System.IO.File.Delete(mstrFile)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Delete_File", "DeleteFile('" & mstrFile.Replace("\", "\\") & "');", True)
                'Shani(01-MAR-2017) -- End
            Catch ex As IO.IOException
                If ex.Message.ToString.Trim.Contains("used by another process") Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    'Hemant (31 May 2019) -- Start
                    'ISSUE/ENHANCEMENT : UAT Changes
                    'msg.DisplayMessage("File is used by another process or File is already open.", Me)
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2001, "File is used by another process or File is already open."), Me)
                    'Hemant (31 May 2019) -- End
                    'Sohail (23 Mar 2019) -- End
                End If
            Catch ex As Exception
                msg.DisplayError(ex, Me)
            End Try
        End If
    End Sub

    'Pinkal (01-Apr-2013) -- End

    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtDependants.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtDependants.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Employee_Module
                dRow("scanattachrefid") = enScanAttactRefId.DEPENDANTS
                dRow("transactionunkid") = Me.ViewState("Unkid")
                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = dtpAttachment.GetDate
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"

                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                dRow("filesize_kb") = f.Length / 1024
                'SHANI (16 JUL 2015) -- End 

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                dRow("form_name") = mstrModuleName
                'Gajanan [22-Feb-2019] -- End

                'S.SANDEEP |25-JAN-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
                'S.SANDEEP |25-JAN-2019| -- END
            Else
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtDependants.Rows.Add(dRow)
            Call fillDependantAttachment()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub fillDependantAttachment()
        Dim dtView As DataView
        Try
            If mdtDependants Is Nothing Then Exit Sub

            dtView = New DataView(mdtDependants, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvDependant.AutoGenerateColumns = False
            dgvDependant.DataSource = dtView
            dgvDependant.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub DeleteTempFile()
        Try
            If mdtDependants Is Nothing Then Exit Sub
            Dim xRow() As DataRow = mdtDependants.Select("localpath <> '' ")
            If xRow.Length > 0 Then
                For Each row As DataRow In xRow
                    If System.IO.File.Exists(row("localpath").ToString) Then
                        System.IO.File.Delete(row("localpath").ToString)
                    End If
                Next
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'SHANI (20 JUN 2015) -- End 

    'Hemant (26 Aug 2020) -- Start
    'ISSUE : Unhandled Exceptions
    Public Function FilenameIsOK(ByVal fileNameAndPath As String) As Boolean
        Try
            Dim fileName = Path.GetFileName(fileNameAndPath)
            Dim directory = Path.GetDirectoryName(fileNameAndPath)
            For Each c In Path.GetInvalidFileNameChars()
                If fileName.Contains(c) Then
                    Return False
                End If
            Next
            For Each c In Path.GetInvalidPathChars()
                If directory.Contains(c) Then
                    Return False
                End If
            Next
            Return True
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Function
    'Hemant (26 Aug 2020) -- End


#End Region

#Region " Controls "

    Private Sub drpcountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpcountry.SelectedIndexChanged
        Dim objState As New clsstate_master
        Dim dsCombos As New DataSet
        Try
            If CInt(drpcountry.SelectedValue) > 0 Then
                dsCombos = objState.GetList("State", True, True, CInt(drpcountry.SelectedValue))
                With drpState
                    .DataValueField = "stateunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("State")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub drpState_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpState.SelectedIndexChanged
        Dim objCity As New clscity_master
        Dim dsCombos As New DataSet
        Try
            If CInt(IIf(drpState.SelectedValue = "", 0, drpState.SelectedValue)) > 0 Then
                dsCombos = objCity.GetList("City", True, True, CInt(drpState.SelectedValue))
                With drppostTown
                    .DataValueField = "cityunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombos.Tables("City")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub drppostTown_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drppostTown.SelectedIndexChanged
        Dim objZipcode As New clszipcode_master
        Dim dsCombos As New DataSet
        Try
            If CInt(IIf(drppostTown.SelectedValue = "", 0, drppostTown.SelectedValue)) > 0 Then
                dsCombos = objZipcode.GetList("Zipcode", True, True, CInt(drppostTown.SelectedValue))
                With drppostcode
                    .DataValueField = "zipcodeunkid"
                    .DataTextField = "zipcode_no"
                    .DataSource = dsCombos.Tables("Zipcode")
                    .DataBind()
                End With
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dtpBirthdate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpBirthdate.TextChanged
        Try
            If dtpBirthdate.IsNull = False Then
                If (DateDiff(DateInterval.Year, dtpBirthdate.GetDate.Date, ConfigParameter._Object._CurrentDateAndTime.Date)) > 0 Then
                    txtAge.Text = CStr(DateDiff(DateInterval.Year, dtpBirthdate.GetDate.Date, ConfigParameter._Object._CurrentDateAndTime.Date))
                Else
                    txtAge.Text = ""
                End If
            Else
                txtAge.Text = ""
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub cboDBRelation_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DrpRelation.SelectedIndexChanged
        Try
            If CInt(DrpRelation.SelectedValue) > 0 Then
                Dim objCMaster As New clsCommon_Master
                objCMaster._Masterunkid = CInt(DrpRelation.SelectedValue)
                Me.ViewState.Add("AgeLimit", objCMaster._Dependant_Limit)
                objCMaster = Nothing
            Else
                Me.ViewState.Add("AgeLimit", 0)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkCopyAddress_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopyAddress.Click
        Dim objEmp As New clsEmployee_Master
        Try     'Hemant (13 Aug 2020)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboEmployee.SelectedValue)
            'Shani(20-Nov-2015) -- End

            TxtAddress.Text = objEmp._Present_Address1 & " " & objEmp._Present_Address2
            drpcountry.SelectedValue = CStr(objEmp._Present_Countryunkid)
            Call drpcountry_SelectedIndexChanged(Nothing, Nothing)

            If objEmp._Present_Stateunkid > 0 Then
                drpState.SelectedValue = CStr(objEmp._Present_Stateunkid)
                Call drpState_SelectedIndexChanged(Nothing, Nothing)
            End If

            If objEmp._Present_Post_Townunkid > 0 Then
                drppostTown.SelectedValue = CStr(objEmp._Present_Post_Townunkid)
                Call drppostTown_SelectedIndexChanged(Nothing, Nothing)
            End If

            If objEmp._Present_Postcodeunkid > 0 Then
                drppostcode.SelectedValue = CStr(objEmp._Present_Postcodeunkid)
            End If
            drpnationality.SelectedValue = CStr(objEmp._Nationalityunkid)
            txtResNo.Text = objEmp._Present_Tel_No
            txtmobileno.Text = objEmp._Present_Mobile
            objEmp = Nothing
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
        'Hemant (13 Aug 2020) -- End     

    End Sub

    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Protected Sub dgvDependant_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvDependant.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                If CInt(e.Item.Cells(5).Text) > 0 Then
                    xrow = mdtDependants.Select("scanattachtranunkid = " & CInt(e.Item.Cells(5).Text) & "")
                Else
                    xrow = mdtDependants.Select("GUID = '" & e.Item.Cells(4).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        'SHANI (27 JUL 2015) -- Start
                        'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
                        'xrow(0).Item("AUD") = "D"
                        'Call fillDependantAttachment()
                        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 39, "Are you sure you want to delete this attachment?")
                        Me.ViewState("DeleteAttRowIndex") = mdtDependants.Rows.IndexOf(xrow(0))
                        popup_YesNo.Show()
                        'SHANI (27 JUL 2015) -- End 
                    End If
                ElseIf e.CommandName = "imgdownload" Then
                    Dim xPath As String = ""

                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        'SHANI (16 JUL 2015) -- Start
                        'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                        'Upload Image folder to access those attachemts from Server and 
                        'all client machines if ArutiSelfService is installed otherwise save then on Document path
                        'xPath = xrow(0).Item("file_path").ToString
                        xPath = xrow(0).Item("filepath").ToString
                        xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                        If Strings.Left(xPath, 1) <> "/" Then
                            xPath = "~/" & xPath
                        Else
                            xPath = "~" & xPath
                        End If
                        xPath = Server.MapPath(xPath)
                        'SHANI (16 JUL 2015) -- End 

                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If
                    If xPath.Trim <> "" Then
                        Dim fileInfo As New IO.FileInfo(xPath)
                        If fileInfo.Exists = False Then
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'msg.DisplayError(ex, Me)
                            'Hemant (31 May 2019) -- Start
                            'ISSUE/ENHANCEMENT : UAT Changes
                            'msg.DisplayMessage("File does not Exist...", Me)
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2002, "File does not Exist..."), Me)
                            'Hemant (31 May 2019) -- End
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                        fileInfo = Nothing
                        Dim strFile As String = xPath
                        Response.ContentType = "image/jpg/pdf"
                        Response.AddHeader("Content-Disposition", "attachment;filename=""" & e.Item.Cells(1).Text & """")
                        Response.Clear()
                        Response.TransmitFile(strFile)
                        HttpContext.Current.ApplicationInstance.CompleteRequest()
                    End If
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub dgvDependant_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvDependant.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                DirectCast(Master.FindControl("script1"), ScriptManager).RegisterPostBackControl(e.Item.Cells(3).FindControl("DownloadLink"))
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'SHANI (20 JUN 2015) -- End 

    'SHANI (27 JUL 2015) -- Start
    'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                mdtDependants.Rows(CInt(Me.ViewState("DeleteAttRowIndex")))("AUD") = "D"
                mdtDependants.AcceptChanges()
                Call fillDependantAttachment()
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            If Me.ViewState("DeleteAttRowIndex") IsNot Nothing OrElse CInt(Me.ViewState("DeleteAttRowIndex")) < 0 Then
                Me.ViewState("DeleteAttRowIndex") = Nothing
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'SHANI (27 JUL 2015) -- End 

    'Shani(04-MAR-2017) -- Start
    'Enhancement - Add Membership and Benefit in employee dependant's screen on web

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim intTotalBeneficiary As Integer = 0
        Dim dblTotalPercentage As Double = 0
        Try
            'Pinkal (20-Sep-2017) -- Start
            'Enhancement - Working Dependanct Benefit Plan Changes for PACRA.
            'If objDependants_Benefice Is Nothing Then objDependants_Benefice = New clsDependants_Beneficiary_tran
            'Call objDependants_Benefice.GetAllocate_Percent(CInt(cboEmployee.SelectedValue), intTotalBeneficiary, dblTotalPercentage)
            'txtTotBeneficiaries.Text = intTotalBeneficiary.ToString
            'txtTotalPercentage.Text = dblTotalPercentage.ToString
            'txtRemainingPercent.Text = (CDbl(100) - dblTotalPercentage).ToString
            'Pinkal (20-Sep-2017) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Shani(04-MAR-2017) -- End

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try  'Hemant (13 Aug 2020)
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If


            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmDependantsAndBeneficiaries"
            'StrModuleName2 = "mnuCoreSetups"
            'clsCommonATLog._WebClientIP = Session("IP_ADD")
            'clsCommonATLog._WebHostName = Session("HOST_NAME")

            ''Pinkal (24-Aug-2012) -- Start
            ''Enhancement : TRA Changes

            'If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            'Else
            '    clsCommonATLog._LoginEmployeeUnkid = -1
            'End If

            ''Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END


       

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            DependantApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmDependantsAndBeneficiariesList)))
            'Gajanan [17-DEC-2018] -- End


            If (Page.IsPostBack = False) Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'Hemant (31 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes
                Call SetMessages()
                'Call Language._Object.SaveValue()
                'Hemant (31 May 2019) -- End
                SetLanguage()
                'Pinkal (06-May-2014) -- End

                clsuser = CType(Session("clsuser"), User)
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then


                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = Session("UserId")
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END



                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'btnsave.Visible = False

                    'Anjan (30 May 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'btnsave.Visible = False
                    btnsave.Visible = CBool(Session("EditDependant"))
                    'Anjan (30 May 2012)-End 
                    'Anjan (02 Mar 2012)-End 

                    'S.SANDEEP [ 16 JAN 2014 ] -- START
                    If Session("ShowPending") IsNot Nothing Then
                        btnsave.Visible = False
                    End If
                    'S.SANDEEP [ 16 JAN 2014 ] -- END

                Else


                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = -1
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'btnsave.Visible = False


                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'btnsave.Visible = ConfigParameter._Object._AllowEditDependants
                    btnsave.Visible = CBool(Session("AllowEditDependants"))
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-End 

                    'Gajanan [3-April-2019] -- Start
                    If IsNothing(ViewState("mblnisEmployeeApprove")) = False Then
                        mblnisEmployeeApprove = CBool(ViewState("mblnisEmployeeApprove"))
                    End If
                    'Gajanan [3-April-2019] -- End
                End If


                'Pinkal (22-Mar-2012) -- Start
                'Enhancement : TRA Changes
                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Form)
                'SHANI [01 FEB 2015]--END 
                'Pinkal (22-Mar-2012) -- End


                'Pinkal (01-Apr-2013) -- Start
                'Enhancement : TRA Changes

                flUpload.Visible = CBool(Session("IsImgInDataBase"))
                'btnUpload.Visible = CBool(Session("IsImgInDataBase"))
                btnRemoveImage.Visible = CBool(Session("IsImgInDataBase"))
                imgDependant.Visible = CBool(Session("IsImgInDataBase"))

                If CBool(Session("IsImgInDataBase")) = True Then
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        'btnUpload.Visible = Session("AllowToAddEditImageForESS")
                        flUpload.Enabled = CBool(Session("AllowToAddEditImageForESS"))
                        btnRemoveImage.Visible = False
                    ElseIf (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        'btnUpload.Visible = Session("AllowToAddEditPhoto")
                        flUpload.Enabled = CBool(Session("AllowToAddEditPhoto"))
                        btnRemoveImage.Enabled = CBool(Session("AllowToDeletePhoto"))
                    End If
                End If
                'Pinkal (01-Apr-2013) -- End

                Call FillCombo()

                'Nilay (02-Mar-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                If Request.QueryString.Count <= 0 Then
                    If Session("dependantsEmpunkid") IsNot Nothing Then
                        cboEmployee.SelectedValue = CStr(CInt(Session("dependantsEmpunkid")))
                    End If
                End If
                'Nilay (02-Mar-2015) -- End

                'SHANI (20 JUN 2015) -- Start
                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                Call IsFromDependantList()

                'Shani(07-Jan-2016) -- Start
                'Passing Qualificiation tranunkid for other qualification, reason the attachment is not coming for Other Qualification.
                'If Request.QueryString.Count > 0 Then
                If Request.QueryString.Count <= 0 Then
                    'Shani(07-Jan-2016) -- End
                    If Request.QueryString("uploadimage") Is Nothing Then
                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'mdtDependants = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.DEPENDANTS, Me.ViewState("Unkid"))

                        'Shani(07-Jan-2016) -- Start
                        'Passing Qualificiation tranunkid for other qualification, reason the attachment is not coming for Other Qualification.
                        'mdtDependants = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.DEPENDANTS, Me.ViewState("Unkid"), Session("Document_Path"))
                        mdtDependants = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.DEPENDANTS, CInt(Me.ViewState("Unkid")), CStr(Session("Document_Path"))).Copy
                        'Shani(07-Jan-2016) -- End

                        'Shani(20-Nov-2015) -- End

                        If CInt(Me.ViewState("Unkid")) <= 0 Then
                            mdtDependants.Rows.Clear()
                        End If
                        dtpAttachment.SetDate = Today.Date
                        Call fillDependantAttachment()
                    End If
                Else

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'mdtDependants = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.DEPENDANTS, Me.ViewState("Unkid"))
                    mdtDependants = objDocument.GetQulificationAttachment(CInt(cboEmployee.SelectedValue), enScanAttactRefId.DEPENDANTS, CInt(Me.ViewState("Unkid")), CStr(Session("Document_Path")))
                    'Shani(20-Nov-2015) -- End

                    dtpAttachment.SetDate = Today.Date
                    Call fillDependantAttachment()
                End If
                'SHANI (20 JUN 2015) -- End

                'Shani(28-FEB-2017) -- Start
                'Enhancement - Add New Bank Payment List Integration for TAMA
                Dim objDependantMemberTran As New clsDependants_Membership_tran
                objDependantMemberTran._DependantTranUnkid = CInt(Me.ViewState("Unkid"))
                mdtMemTran = objDependantMemberTran._DataTable

                Dim objDependantBenefitTran As New clsDependants_Benefit_tran
                objDependantBenefitTran._DependantTranUnkid = CInt(Me.ViewState("Unkid"))
                mdtBenefitTran = objDependantBenefitTran._DataTable
                'Shani(28-FEB-2017) -- End

                'SHANI (20 JUN 2015) -- Start
                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            Else
                mdtDependants = CType(Session("mdtDependants"), DataTable)
                'SHANI (20 JUN 2015) -- End 

                'Shani(28-FEB-2017) -- Start
                'Enhancement - Add New Bank Payment List Integration for TAMA
                mdtMemTran = CType(Me.ViewState("mdtMemTran"), DataTable)
                mblnMem_Popup = CBool(Me.ViewState("mblnMem_Popup"))
                dtMemTran = CType(Me.ViewState("dtMemTran"), DataTable)
                mintdpndtmembershiptranunkid = CInt(Me.ViewState("mintdpndtmembershiptranunkid"))
                mstrDepMembershipGUID = Me.ViewState("mstrDepMembershipGUID").ToString

                mdtBenefitTran = CType(Me.ViewState("mdtBenefitTran"), DataTable)
                dtBenefitTran = CType(Me.ViewState("dtBenefitTran"), DataTable)
                mblnBenefit_Popup = CBool(Me.ViewState("mblnBenefit_Popup"))
                mintdpndtbenefittranunkid = CInt(Me.ViewState("mintdpndtbenefittranunkid"))
                mstrdpndtbenefitGUID = Me.ViewState("mstrdpndtbenefitGUID").ToString
                'Shani(28-FEB-2017) -- End
            End If


            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            If CInt(Me.ViewState("Unkid")) <= 0 Then
                dtpEffectiveDate.Enabled = True
            Else
                dtpEffectiveDate.Enabled = False
            End If
            'Sohail (18 May 2019) -- End

            'Shani(28-FEB-2017) -- Start
            'Enhancement - Add New Bank Payment List Integration for TAMA
            If mblnMem_Popup Then
                popup_MembershipInfo.Show()
            End If

            If mblnBenefit_Popup Then
                popup_BenefitInfo.Show()
            End If
            'Shani(28-FEB-2017) -- End

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        'Hemant (26 Aug 2020) -- Start
                        'ISSUE : Unhandled Exceptions
                        'postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        'Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                        If FilenameIsOK(Server.MapPath("~\images\" & postedFile.FileName)) Then
                            postedFile.SaveAs(Server.MapPath("~\images\" & postedFile.FileName))
                            Session.Add("Imagepath", Server.MapPath("~\images\" & postedFile.FileName))
                        Else
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 50, "Filename or Destination File Path is not Correct Format. Please Check!!!"), Me)
                        End If
                        'Hemant (26 Aug 2020) -- End                        
                    End If
                End If
                Exit Sub
            End If
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
        Try

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
            'If Not IsPostBack Then
            '    IsFromDependantList()
            'End If
            'SHANI (20 JUN 2015) -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'ToolbarEntry1.VisibleSaveButton(False)
            'ToolbarEntry1.VisibleDeleteButton(False)
            'ToolbarEntry1.VisibleCancelButton(False)
            'ToolbarEntry1.VisibleNewButton(False)
            'ToolbarEntry1.VisibleModeFormButton(False)
            'ToolbarEntry1.VisibleExitButton(False)
            'SHANI [01 FEB 2015]--END 

            'SHANI (20 JUN 2015) -- Start
            'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") Is Nothing Then
                    If Session("mdtDependants") Is Nothing Then
                        Session.Add("mdtDependants", mdtDependants)
                    Else
                        Session("mdtDependants") = mdtDependants
                    End If
                End If
            Else
                If Session("mdtDependants") Is Nothing Then
                    Session.Add("mdtDependants", mdtDependants)
                Else
                    Session("mdtDependants") = mdtDependants
                End If
            End If
            Me.ViewState("mdtMemTran") = mdtMemTran
            Me.ViewState("mblnMem_Popup") = mblnMem_Popup
            Me.ViewState("dtMemTran") = dtMemTran
            Me.ViewState("mintdpndtmembershiptranunkid") = mintdpndtmembershiptranunkid
            Me.ViewState("mstrDepMembershipGUID") = mstrDepMembershipGUID

            Me.ViewState("mdtBenefitTran") = mdtBenefitTran
            Me.ViewState("dtBenefitTran") = dtBenefitTran
            Me.ViewState("mblnBenefit_Popup") = mblnBenefit_Popup
            Me.ViewState("mintdpndtbenefittranunkid") = mintdpndtbenefittranunkid
            Me.ViewState("mstrdpndtbenefitGUID") = mstrdpndtbenefitGUID

            'SHANI (20 JUN 2015) -- End 

            'Gajanan [3-April-2019] -- Start
            ViewState("mblnisEmployeeApprove") = mblnisEmployeeApprove
            'Gajanan [3-April-2019] -- End

            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    'Pinkal (22-Mar-2012) -- End

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            objDependants_Benefice = New clsDependants_Beneficiary_tran

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim blnFlag As Boolean = False
            'Gajanan [22-Feb-2019] -- End


            If IsValidData() = False Then Exit Sub

            'SHANI (27 JUL 2015) -- Start
            'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
            If CBool(Session("DependantDocsAttachmentMandatory")) Then
                If mdtDependants Is Nothing OrElse mdtDependants.Select("AUD <> 'D'").Length <= 0 Then
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 40, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation. "), Me)
                    Exit Sub
                End If
            End If
            'SHANI (27 JUL 2015) -- End 

            'S.SANDEEP [ 18 SEP 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dicNotification = New Dictionary(Of String, String)
            If CInt(Me.ViewState("Unkid")) > 0 Then
                If Session("Notify_EmplData").ToString.Trim.Length > 0 Then
                    Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                    For Each sId As String In Session("Notify_EmplData").ToString.Split(CChar("||"))(2).Split(CChar(","))
                        objUsr._Userunkid = CInt(sId)
                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)
                        If dicNotification.ContainsKey(objUsr._Email) = False Then
                            dicNotification.Add(objUsr._Email, StrMessage)
                        End If
                    Next
                    objUsr = Nothing
                End If

            ElseIf CInt(Me.ViewState("Unkid")) <= 0 Then
                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                If Session("Notify_EmplData").ToString.Trim.Length > 0 Then
                    For Each sId As String In Session("Notify_EmplData").ToString.Split(CChar("||"))(2).Split(CChar(","))
                        objUsr._Userunkid = CInt(sId)
                        StrMessage = Set_Notification(objUsr._Firstname & " " & objUsr._Lastname)

                        If dicNotification.ContainsKey(objUsr._Email) = False Then
                            dicNotification.Add(objUsr._Email, StrMessage)
                        End If

                    Next
                    objUsr = Nothing
                End If
            End If

            'S.SANDEEP [ 18 SEP 2012 ] -- END


            Call SetValue()

            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            Dim mdsDoc As DataSet
            Dim mstrFolderName As String = ""
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim strFileName As String = ""
            For Each dRow As DataRow In mdtDependants.Rows
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRow("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault

                If dRow("AUD").ToString = "A" AndAlso dRow("localpath").ToString <> "" Then
                    'Pinkal (20-Nov-2018) -- Start
                    'Enhancement - Working on P2P Integration for NMB.
                    'strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("localpath")))
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("localpath")))
                    'Pinkal (20-Nov-2018) -- End
                    If File.Exists(CStr(dRow("localpath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        File.Move(CStr(dRow("localpath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dRow("fileuniquename") = strFileName
                        dRow("filepath") = strPath
                        dRow.AcceptChanges()
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'msg.DisplayError(ex, Me)
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("File does not exist on localpath", Me)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2003, "File does not exist on localpath"), Me)
                        'Hemant (31 May 2019) -- End
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dRow("filepath").ToString
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If File.Exists(Server.MapPath(strFilepath)) Then
                            File.Delete(Server.MapPath(strFilepath))
                        Else
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'msg.DisplayError(ex, Me)
                            'Hemant (31 May 2019) -- Start
                            'ISSUE/ENHANCEMENT : UAT Changes
                            'msg.DisplayMessage("File does not exist on localpath", Me)
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2003, "File does not exist on localpath"), Me)
                            'Hemant (31 May 2019) -- End
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'msg.DisplayError(ex, Me)
                        'Hemant (31 May 2019) -- Start
                        'ISSUE/ENHANCEMENT : UAT Changes
                        'msg.DisplayMessage("File does not exist on localpath", Me)
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2003, "File does not exist on localpath"), Me)
                        'Hemant (31 May 2019) -- End
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                End If

            Next

            'SHANI (16 JUL 2015) -- End 


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim drow_mem As DataRow() = mdtMemTran.Select("AUD <> 'D' and dpndtbeneficetranunkid <= 0")
            If drow_mem.Length > 0 Then
                mdtMemTran = drow_mem.CopyToDataTable()
                drow_mem = Nothing
            End If

            drow_mem = mdtBenefitTran.Select("AUD <> 'D' and dpndtbeneficetranunkid <= 0")
            If drow_mem.Length > 0 Then
                mdtBenefitTran = drow_mem.CopyToDataTable()
                drow_mem = Nothing
            End If
            'Gajanan [22-Feb-2019] -- End


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim eLMode As Integer
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                eLMode = enLogin_Mode.MGR_SELF_SERVICE
            Else
                eLMode = enLogin_Mode.EMP_SELF_SERVICE
            End If
            'Gajanan [17-April-2019] -- End


            If CInt(Me.ViewState("Unkid")) <= 0 Then
                'SHANI (20 JUN 2015) -- Start
                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                'If objDependants_Benefice.Insert() = False Then

                'Shani(04-MAR-2017) -- Start
                'Enhancement - Add Membership and Benefit in employee dependant's screen on web
                'If objDependants_Benefice.Insert(, , mdtDependants) = False Then



                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If objDependants_Benefice.Insert(mdtMemTran, mdtBenefitTran, mdtDependants) = False Then
                '    'Shani(04-MAR-2017) -- End
                '    'SHANI (20 JUN 2015) -- End 
                '    msg.DisplayMessage(objDependants_Benefice._Message, Me)
                'Else
                '    'Pinkal (01-Apr-2013) -- Start
                '    'Enhancement : TRA Changes
                '    If CBool(Session("IsImgInDataBase")) Then
                '        DeleteImage()
                '    End If
                '    'Pinkal (01-Apr-2013) -- End

                '    'SHANI (20 JUN 2015) -- Start
                '    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                '    Call DeleteTempFile()
                '    'SHANI (20 JUN 2015) -- End 

                '    msg.DisplayMessage("Dependant inserted Successfully.", Me)
                '    'S.SANDEEP [ 18 SEP 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    If objDependants_Benefice._Message.Trim.Length <= 0 Then
                '        trd = New Thread(AddressOf Send_Notification)
                '        trd.IsBackground = True
                '        trd.Start()
                '    End If
                '    'S.SANDEEP [ 18 SEP 2012 ] -- END
                '    Call ClearObject()

                '    'SHANI (20 JUN 2015) -- Start
                '    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                '    Call fillDependantAttachment()
                '    'SHANI (20 JUN 2015) -- End 

                'End If


                'Gajanan [3-April-2019] -- Start

                'If DependantApprovalFlowVal Is Nothing Then
                If DependantApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then
                    'Gajanan [3-April-2019] -- End
                    blnFlag = objADependant.Insert(Nothing, mdtMemTran, mdtBenefitTran, mdtDependants)
                    If blnFlag = False AndAlso objADependant._Message <> "" Then
                        msg.DisplayMessage(objADependant._Message, Me)
                        Exit Sub

                    Else

                        objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                       CStr(Session("UserAccessModeSetting")), _
                                                       CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                        CInt(enUserPriviledge.AllowToApproveRejectEmployeeDependants), _
                                                        enScreenName.frmDependantsAndBeneficiariesList, CStr(Session("EmployeeAsOnDate")), _
                                                        CInt(Session("UserId")), mstrModuleName, enLogin_Mode.DESKTOP, _
                                                         Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.ADDED, 0, cboEmployee.SelectedValue.ToString(), "", Nothing, _
                                                        "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and first_name = '" & txtFirstName.Text & "' and last_name ='" & txtLastName.Text & "' and relationunkid =" & DrpRelation.SelectedValue.ToString() & "", Nothing, False, , _
                                                        "employeeunkid= " & cboEmployee.SelectedValue.ToString() & " and first_name = '" & txtFirstName.Text & "' and last_name ='" & txtLastName.Text & "' and relationunkid =" & DrpRelation.SelectedValue.ToString() & "", Nothing)


                    End If
                Else

                    blnFlag = objDependants_Benefice.Insert(mdtMemTran, mdtBenefitTran, mdtDependants) = False

                    If blnFlag = False AndAlso objDependants_Benefice._Message <> "" Then
                        msg.DisplayMessage(objDependants_Benefice._Message, Me)
                        Exit Sub
                    End If

                End If

                If CBool(Session("IsImgInDataBase")) Then
                    DeleteImage()
                End If


                Call DeleteTempFile()

                'Hemant (31 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes
                'msg.DisplayMessage("Dependant inserted Successfully.", Me)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2006, "Dependant inserted Successfully."), Me)
                'Hemant (31 May 2019) -- End

                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If DependantApprovalFlowVal Is Nothing = False Then
                    'Gajanan [22-Feb-2019] -- End
                    If objDependants_Benefice._Message.Trim.Length <= 0 Then
                        trd = New Thread(AddressOf Send_Notification)
                        trd.IsBackground = True
                        trd.Start()
                    End If
                End If


                Call ClearObject()

                Call fillDependantAttachment()

                'Gajanan [22-Feb-2019] -- End


            Else

                'SHANI (20 JUN 2015) -- Start
                'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                'If objDependants_Benefice.Update() = False Then

                'Shani(04-MAR-2017) -- Start
                'Enhancement - Add Membership and Benefit in employee dependant's screen on web
                'If objDependants_Benefice.Update(, , mdtDependants) = False Then


                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If objDependants_Benefice.Update(mdtMemTran, mdtBenefitTran, mdtDependants) = False Then
                '    'Shani(04-MAR-2017) -- End

                '    'SHANI (20 JUN 2015) -- End
                '    msg.DisplayMessage(objDependants_Benefice._Message, Me)
                'Else


                '    'Pinkal (01-Apr-2013) -- Start
                '    'Enhancement : TRA Changes
                '    If CBool(Session("IsImgInDataBase")) Then
                '        DeleteImage()
                '    End If
                '    'Pinkal (01-Apr-2013) -- End

                '    'SHANI (20 JUN 2015) -- Start
                '    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
                '    Call DeleteTempFile()
                '    'SHANI (20 JUN 2015) -- End 

                '    msg.DisplayMessage("Dependant Updated Successfully.", Me)
                '    'S.SANDEEP [ 18 SEP 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    If objDependants_Benefice._Message.Trim.Length <= 0 Then
                '        trd = New Thread(AddressOf Send_Notification)
                '        trd.IsBackground = True
                '        trd.Start()
                '    End If
                '    'S.SANDEEP [ 18 SEP 2012 ] -- END

                '    'Nilay (02-Mar-2015) -- Start
                '    'Enhancement - REDESIGN SELF SERVICE.
                '    'Response.Redirect("~\HR\wPgDependantsList.aspx")
                '    Response.Redirect(Session("rootpath").ToString & "HR/wPgDependantsList.aspx", False)
                '    'Nilay (02-Mar-2015) -- End

                'End If


                'Gajanan [3-April-2019] -- Start
                'If DependantApprovalFlowVal Is Nothing Then
                If DependantApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then
                    'Gajanan [3-April-2019] -- End
                    blnFlag = objADependant.Insert(Nothing, mdtMemTran, mdtBenefitTran, mdtDependants)
                    If blnFlag = False AndAlso objADependant._Message <> "" Then
                        msg.DisplayMessage(objADependant._Message, Me)
                        Exit Sub
                    End If

                Else
                    blnFlag = objDependants_Benefice.Update(mdtMemTran, mdtBenefitTran, mdtDependants)
                    If blnFlag = False AndAlso objDependants_Benefice._Message <> "" Then
                        msg.DisplayMessage(objDependants_Benefice._Message, Me)
                        Exit Sub
                    End If
                End If
                If CBool(Session("IsImgInDataBase")) Then
                    DeleteImage()
                End If
                Call DeleteTempFile()
                'Hemant (31 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes
                'msg.DisplayMessage("Dependant Updated Successfully.", Me)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2007, "Dependant Updated Successfully."), Me)
                'Hemant (31 May 2019) -- End
                If objDependants_Benefice._Message.Trim.Length <= 0 Then
                    trd = New Thread(AddressOf Send_Notification)
                    trd.IsBackground = True
                    trd.Start()
                End If
                Response.Redirect(Session("rootpath").ToString & "HR/wPgDependantsList.aspx", False)
                'Gajanan [22-Feb-2019] -- End

            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\UserHome.aspx", False)
            Response.Redirect(Session("rootpath").ToString & "HR/wPgDependantsList.aspx", False)
            'Nilay (02-Mar-2015) -- End


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END 

    'Pinkal (01-Apr-2013) -- Start
    'Enhancement : TRA Changes

    Protected Sub BtnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles flUpload.btnUpload_Click ' btnUpload.Click
        Try
            If CBool(Session("IsImgInDataBase")) Then
                DeleteImage()
            End If

            If flUpload.HasFile Then
                Dim mintByes As Integer = 6291456
                If flUpload.FileBytes.LongLength <= mintByes Then   ' 6 MB = 6291456 Bytes
                    Dim mstrPath As String = "../images/" & flUpload.FileName
                    flUpload.SaveAs(Server.MapPath(mstrPath))
                    imgDependant.ImageUrl = mstrPath
                Else
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'msg.DisplayError(ex, Me)
                    msg.DisplayMessage("Sorry,You cannot upload file greater than " & (mintByes / 1024) / 1024 & " MB.", Me)
                    'Sohail (23 Mar 2019) -- End
                End If
            Else
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'msg.DisplayError(ex, Me)
                'Hemant (31 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes
                'msg.DisplayMessage("Please Select the Employee Image.", Me)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2005, "Please Select the Employee Image."), Me)
                'Hemant (31 May 2019) -- End
                'Sohail (23 Mar 2019) -- End
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnRemoveImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveImage.Click
        Try
            'Shani(01-MAR-2017) -- Start
            If imgDependant.ImageUrl.Trim.ToString().Contains("GetImageHandler.ashx") = False AndAlso System.IO.File.Exists(Server.MapPath(imgDependant.ImageUrl)) Then
                Dim mstrFile As String = Server.MapPath(imgDependant.ImageUrl)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Delete_File", "DeleteFile('" & mstrFile.Replace("\", "\\") & "');", True)
            End If
            imgDependant.ImageUrl = ""
            'Shani(01-MAR-2017) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (01-Apr-2013) -- End


    'SHANI (20 JUN 2015) -- Start
    'Enhancement - Allow to add attachments for Dependants and provide attachment option on Employee Qualification and Employee Dependant screen
    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If IsValidData() = False Then Exit Sub
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                mdtDependants = CType(Session("mdtDependants"), DataTable)
                AddDocumentAttachment(f, f.FullName)
                Call fillDependantAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'SHANI (20 JUN 2015) -- End 

    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgvDependant.Items.Count <= 0 Then
                'Hemant (31 May 2019) -- Start
                'ISSUE/ENHANCEMENT : UAT Changes
                'msg.DisplayMessage("No Files to download.", Me)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2004, "No Files to download."), Me)
                'Hemant (31 May 2019) -- End
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & cboEmployee.SelectedItem.Text.Replace(" ", "") + ".zip", mdtDependants, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

#End Region

#Region "ToolBar Event"
    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        Response.Redirect("~\HR\wPgDependantsList.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END 
#End Region


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()

        'Language.setLanguage(mstrModuleName)
        Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
        'SHANI [01 FEB 2015]--END 

        Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.lblDBResidentialNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBResidentialNo.ID, Me.lblDBResidentialNo.Text)
        Me.lblDBMobileNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBMobileNo.ID, Me.lblDBMobileNo.Text)
        Me.lblDBAddress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBAddress.ID, Me.lblDBAddress.Text)
        Me.lblDBIdNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBIdNo.ID, Me.lblDBIdNo.Text)
        Me.lblDBEmail.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBEmail.ID, Me.lblDBEmail.Text)
        Me.lblDBPostCountry.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBPostCountry.ID, Me.lblDBPostCountry.Text)
        Me.lblDBNationality.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBNationality.ID, Me.lblDBNationality.Text)
        Me.lblDBPostCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBPostCode.ID, Me.lblDBPostCode.Text)
        Me.lblDBPostTown.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBPostTown.ID, Me.lblDBPostTown.Text)
        Me.lblDBPostBox.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBPostBox.ID, Me.lblDBPostBox.Text)
        Me.lblState.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblState.ID, Me.lblState.Text)
        Me.lblDBFirstName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBFirstName.ID, Me.lblDBFirstName.Text)
        Me.lblDBMiddleName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBMiddleName.ID, Me.lblDBMiddleName.Text)
        Me.lblDBLastName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBLastName.ID, Me.lblDBLastName.Text)
        Me.lblDBRelation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBRelation.ID, Me.lblDBRelation.Text)
        Me.lblDBBirthDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDBBirthDate.ID, Me.lblDBBirthDate.Text)
        Me.chkBeneficiaries.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkBeneficiaries.ID, Me.chkBeneficiaries.Text)
        Me.lnkCopyAddress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkCopyAddress.ID, Me.lnkCopyAddress.Text)
        Me.lblGender.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblGender.ID, Me.lblGender.Text)
        Me.lblEffectiveDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEffectiveDate.ID, Me.lblEffectiveDate.Text)

        Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
        Me.btnsave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnsave.ID, Me.btnsave.Text).Replace("&", "")

        'Hemant (30 Nov 2018) -- Start
        'Enhancement : Including Language Settings For Scan/Attachment Button
        btnAddAttachment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAddAttachment.ID, Me.btnAddAttachment.Text).Replace("&", "")
        btnAddFile.Value = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnAddAttachment.ID, Me.btnAddAttachment.Text).Replace("&", "")
        'Hemant (30 Nov 2018) -- End


    End Sub

    'Pinkal (06-May-2014) -- End

#Region " Membership Information "

#Region " Private Method(S) "

    Public Sub Load_Membership_Inforamation()
        Try
            dtMemTran = mdtMemTran.Copy()
            Call FillMembership_Combo()
            Call Fill_MembershipList()
            Call ResetMemberShip()
            mblnMem_Popup = True
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub Fill_MembershipList()
        Try
            Dim dtView As New DataView(dtMemTran, "AUD <> 'D'", "", DataViewRowState.CurrentRows)
            lvMembershipInfo.DataSource = dtView
            lvMembershipInfo.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub FillMembership_Combo()
        Dim dsCombos As DataSet
        Try
            dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.MEMBERSHIP_CATEGORY, True, "MembershipGroup")
            With cboMemCategory
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("MembershipGroup")
                .DataBind()
                .SelectedValue = "0"
            End With
            Call cboMemCategory_SelectedIndexChanged(cboMemCategory, Nothing)
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetMemberShip()
        Try
            cboMemCategory.SelectedValue = CStr(0)
            Call cboMemCategory_SelectedIndexChanged(cboMemCategory, Nothing)
            cboMedicalNo.SelectedValue = CStr(0)
            txtMembershipNo.Text = ""
            btnAddMembership.Visible = True
            btnEditMembership.Visible = False
            mintdpndtmembershiptranunkid = -1
            mstrDepMembershipGUID = ""
            cboMemCategory.Enabled = True
            cboMedicalNo.Enabled = True
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Membership_IsValid() As Boolean
        Try
            If CInt(cboMemCategory.SelectedValue) <= 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Membership Category is compulsory information. Please select Membership Category to continue."), Me)
                cboMemCategory.Focus()
                Return False
            End If

            If CInt(cboMedicalNo.SelectedValue) <= 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Membership is compulsory information. Please select Membership to continue."), Me)
                cboMedicalNo.Focus()
                Return False
            End If

            If txtMembershipNo.Text.Trim = "" Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Membership No. cannot be blank. Membership No. is compulsory information."), Me)
                txtMembershipNo.Focus()
                Return False
            End If

            Dim strFilter As String = "membershipunkid = " & CInt(cboMedicalNo.SelectedValue) & " AND AUD <> 'D' "
            If mintdpndtmembershiptranunkid > 0 Then
                strFilter &= "AND dpndtmembershiptranunkid <> " & mintdpndtmembershiptranunkid & " "
            ElseIf mstrDepMembershipGUID.Trim.Length > 0 Then
                strFilter &= "AND GUID <> '" & mstrDepMembershipGUID & "' "
            End If
            Dim dtMRow As DataRow() = dtMemTran.Select(strFilter)
            If dtMRow.Length > 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Selected Membership is already added to the list."), Me)
                Return False
            End If
            Return True
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Button Event(S) "
    Protected Sub btnMem_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMem_Save.Click
        Try
            mdtMemTran = dtMemTran.Copy()
            mblnMem_Popup = False
            popup_MembershipInfo.Hide()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnMem_Close_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMem_Close.Click
        Try
            mblnMem_Popup = False
            popup_MembershipInfo.Hide()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEditMembership_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditMembership.Click
        Dim dtRow() As DataRow = Nothing
        Try
            If Membership_IsValid() = False Then Exit Sub
            If mintdpndtmembershiptranunkid > 0 Then
                dtRow = dtMemTran.Select("dpndtmembershiptranunkid = " & mintdpndtmembershiptranunkid)
            ElseIf mstrDepMembershipGUID.Trim.Length > 0 Then
                dtRow = dtMemTran.Select("GUID = '" & mstrDepMembershipGUID & "'")
            End If

            If dtRow IsNot Nothing AndAlso dtRow.Length > 0 Then
                dtRow(0).Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
                dtRow(0).Item("membershipunkid") = CInt(cboMedicalNo.SelectedValue)
                dtRow(0).Item("membershipno") = txtMembershipNo.Text
                dtRow(0).Item("Category") = cboMemCategory.SelectedItem.Text
                dtRow(0).Item("membershipname") = cboMedicalNo.SelectedItem.Text
                If IsDBNull(dtRow(0).Item("AUD")) OrElse dtRow(0).Item("AUD").ToString.Trim = "" Then
                    dtRow(0).Item("AUD") = "U"
                End If
                Call Fill_MembershipList()
                Call ResetMemberShip()
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddMembership_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddMembership.Click
        Try

            If Membership_IsValid() = False Then Exit Sub

            Dim dtMemRow As DataRow
            dtMemRow = dtMemTran.NewRow

            dtMemRow.Item("dpndtmembershiptranunkid") = -1
            dtMemRow.Item("dpndtbeneficetranunkid") = CInt(Me.ViewState("Unkid"))
            dtMemRow.Item("membership_categoryunkid") = CInt(cboMemCategory.SelectedValue)
            dtMemRow.Item("membershipunkid") = CInt(cboMedicalNo.SelectedValue)
            dtMemRow.Item("membershipno") = txtMembershipNo.Text
            dtMemRow.Item("Category") = cboMemCategory.SelectedItem.Text
            dtMemRow.Item("membershipname") = cboMedicalNo.SelectedItem.Text
            dtMemRow.Item("AUD") = "A"
            dtMemRow.Item("GUID") = Guid.NewGuid.ToString

            dtMemTran.Rows.Add(dtMemRow)

            Call Fill_MembershipList()
            Call ResetMemberShip()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnMemClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMemClear.Click
        Try
            Call ResetMemberShip()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region " Control(S) Event(S) "
    Protected Sub mnuMembershipInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuMembershipInfo.Click
        Try
            Call Load_Membership_Inforamation()
            popup_MembershipInfo.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboMemCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMemCategory.SelectedIndexChanged
        Try
            If CInt(cboMemCategory.SelectedValue) > 0 Then

            End If

            Dim dsCombos As New DataSet
            Dim objMembership As New clsmembership_master
            dsCombos = objMembership.getListForCombo("Membership", True, CInt(cboMemCategory.SelectedValue))
            With cboMedicalNo
                .DataValueField = "membershipunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Membership")
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lvMembershipInfo_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvMembershipInfo.ItemCommand
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                If e.CommandName.ToUpper = "OBJEDIT" Then
                    cboMemCategory.SelectedValue = e.Item.Cells(5).Text
                    cboMemCategory_SelectedIndexChanged(cboMemCategory, Nothing)
                    cboMedicalNo.SelectedValue = e.Item.Cells(6).Text
                    txtMembershipNo.Text = e.Item.Cells(4).Text
                    btnAddMembership.Visible = False
                    btnEditMembership.Visible = True
                    mintdpndtmembershiptranunkid = CInt(e.Item.Cells(7).Text)
                    mstrDepMembershipGUID = e.Item.Cells(8).Text
                    cboMemCategory.Enabled = False
                    cboMedicalNo.Enabled = False
                ElseIf e.CommandName.ToUpper = "OBJDELETE" Then
                    Dim drTemp As DataRow()
                    If CInt(e.Item.Cells(7).Text) = -1 Then
                        drTemp = dtMemTran.Select("GUID = '" & e.Item.Cells(8).Text & "'")
                    Else
                        drTemp = dtMemTran.Select("dpndtmembershiptranunkid = " & CInt(e.Item.Cells(7).Text))
                    End If
                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        drTemp(0).Item("isvoid") = True
                        drTemp(0).Item("voiduserunkid") = CInt(Session("UserId"))
                        drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        drTemp(0).Item("voidreason") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "Incorrect Benefit Entry.")
                        Call Fill_MembershipList()
                    End If
                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region " Benefit Information "

#Region " Private Method(s)"

    Public Sub Load_Benefit_Inforamation()
        Try
            dtBenefitTran = mdtBenefitTran.Copy()
            Call FillBenefit_Combo()
            Call Fill_BenefitList()
            Call ResetBenefit()
            mblnBenefit_Popup = True
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub Fill_BenefitList()
        Try

            'Pinkal (20-Sep-2017) -- Start
            'Enhancement - Working Dependanct Benefit Plan Changes for PACRA.

            Dim mstrBenefitGrpId As String = String.Join(",", dtBenefitTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("benefitgroupunkid").ToString()).Distinct().ToArray())

            If mstrBenefitGrpId.Trim.Length > 0 Then
                Dim arBenefitGrp() As String = mstrBenefitGrpId.Split(CChar(","))
                If arBenefitGrp.Length > 0 Then
                    For i As Integer = 0 To arBenefitGrp.Length - 1

                        If dtBenefitTran.Columns.Contains("SortId") = False Then
                            Dim dcColumn As New DataColumn("SortId", Type.GetType("System.Int16"))
                            dcColumn.DefaultValue = 0
                            dtBenefitTran.Columns.Add(dcColumn)
                        End If

                        If dtBenefitTran.Select("dpndtbenefittranunkid = -999 AND benefitgroupunkid = " & CInt(arBenefitGrp(i))).Length <= 0 Then
                            Dim drRow As DataRow = dtBenefitTran.NewRow()
                            drRow("dpndtbenefittranunkid") = -999
                            drRow("benefitplanunkid") = -1
                            drRow("value_id") = 0
                            drRow("AUD") = ""
                            drRow("benefitgroupunkid") = CInt(arBenefitGrp(i))
                            drRow("benefit_group") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 46, "Total")
                            drRow("benefit_percent") = CDec(dtBenefitTran.Compute("SUM(benefit_percent)", "AUD <> 'D' AND benefitgroupunkid = " & CInt(arBenefitGrp(i))))
                            drRow("benefit_amount") = CDec(dtBenefitTran.Compute("SUM(benefit_amount)", "AUD <> 'D' AND benefitgroupunkid = " & CInt(arBenefitGrp(i))))
                            drRow("SortId") = 999
                            dtBenefitTran.Rows.Add(drRow)
                        Else
                            Dim drPercentage() As DataRow = dtBenefitTran.Select("dpndtbenefittranunkid = -999 AND benefitgroupunkid = " & CInt(arBenefitGrp(i)))
                            If drPercentage.Length > 0 Then
                                drPercentage(0)("benefit_percent") = CDec(dtBenefitTran.Compute("SUM(benefit_percent)", "AUD <> 'D' AND dpndtbenefittranunkid <> -999 AND benefitgroupunkid = " & CInt(arBenefitGrp(i))))
                                drPercentage(0)("benefit_amount") = CDec(dtBenefitTran.Compute("SUM(benefit_amount)", "AUD <> 'D' AND dpndtbenefittranunkid <> -999 AND benefitgroupunkid = " & CInt(arBenefitGrp(i))))
                                drPercentage(0)("SortId") = 999
                                drPercentage(0)("AUD") = ""
                                dtBenefitTran.AcceptChanges()
                            End If
                        End If
                    Next

                End If

                dtBenefitTran = New DataView(dtBenefitTran, "", "benefitgroupunkid,SortId", DataViewRowState.CurrentRows).ToTable()

            End If

            'Pinkal (20-Sep-2017) -- End


            Dim dtView As New DataView(dtBenefitTran, "AUD <> 'D'", "", DataViewRowState.CurrentRows)
            lvBenefit.DataSource = dtView
            lvBenefit.DataBind()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub FillBenefit_Combo()
        Dim dsCombos As DataSet
        Try
            dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.BENEFIT_GROUP, True, "BenefitGroup")
            With cboBenefitGroup
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("BenefitGroup")
                .DataBind()
                .SelectedValue = "0"
            End With
            Call cboBenefitGroup_SelectedIndexChanged(cboBenefitGroup, Nothing)

            dsCombos = (New clsMasterData).GetPaymentBy("PayBy")
            With cboValueBasis
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("PayBy")
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ResetBenefit()
        Try
            cboBenefitGroup.SelectedValue = CStr(0)
            Call cboBenefitGroup_SelectedIndexChanged(cboBenefitGroup, Nothing)
            cboBenefitType.SelectedValue = CStr(0)
            cboValueBasis.SelectedValue = "0"
            'Pinkal (20-Sep-2017) -- Start
            'Enhancement - Working Dependanct Benefit Plan Changes for PACRA.
            cboValueBasis_SelectedValueChanged(cboValueBasis, Nothing)
            'Pinkal (20-Sep-2017) -- End
            txtBenefitAmount.Text = "0"
            txtBenefitPercent.Text = "0"

            mintdpndtbenefittranunkid = -1
            mstrdpndtbenefitGUID = ""
            btnAddBenefit.Visible = True
            btnEditBenefit.Visible = False
            cboBenefitGroup.Enabled = True
            cboBenefitType.Enabled = True
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Benefit_IsValid() As Boolean
        Try
            If CInt(cboBenefitGroup.SelectedValue) <= 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 37, "Benefit Group is compulsory information. Please select Benefit Group to continue."), Me)
                cboBenefitGroup.Focus()
                Return False
            End If

            If CInt(cboBenefitType.SelectedValue) <= 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Benefit is compulsory information. Please select Benefit to continue."), Me)
                cboBenefitType.Focus()
                Return False
            End If

            If CInt(cboValueBasis.SelectedValue) <= 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Benefit In is compulsory information. Please select Benefit In to continue."), Me)
                cboValueBasis.Focus()
                Return False
            End If

            Select Case CInt(cboValueBasis.SelectedValue)
                Case 1  'VALUE
                    If CDec(txtBenefitAmount.Text) <= 0 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Amount cannot be blank .Amount is compulsory information."), Me)
                        txtBenefitAmount.Focus()
                        Return False
                    End If
                Case 2  'PERCENTAGE
                    If CDec(txtBenefitPercent.Text) <= 0 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Percent cannot be blank .Percent is compulsory information."), Me)
                        txtBenefitPercent.Focus()
                        Return False
                    End If

                    If CDec(txtBenefitPercent.Text) > 100 Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Percent cannot be greater than 100."), Me)
                        txtBenefitPercent.Text = CStr(0)
                        txtBenefitPercent.Focus()
                        Return False
                    End If

            End Select
            Dim strFilter As String = "benefitplanunkid = " & CInt(cboBenefitType.SelectedValue) & " AND AUD <> 'D' "
            If mintdpndtbenefittranunkid > 0 Then
                strFilter &= "AND dpndtbenefittranunkid <> " & mintdpndtbenefittranunkid & " "
            ElseIf mstrdpndtbenefitGUID.Trim.Length > 0 Then
                strFilter &= "AND GUID <> '" & mstrdpndtbenefitGUID & "'"
            End If

            Dim dtMRow As DataRow() = dtBenefitTran.Select(strFilter)
            If dtMRow.Length > 0 Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Selected Benefit is already added to the list."), Me)
                Return False
            End If

            Return True
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Function

    Private Function IsPercentageProper(ByVal intOperation As Integer) As Boolean
        Try
            If CInt(cboValueBasis.SelectedValue) = 2 Then
                Select Case intOperation
                    Case 1  'ADD
                        If (CDec(IIf(txtTotalPercentage.Text = "", 0, txtTotalPercentage.Text)) + CDec(txtBenefitPercent.Text)) > 100 Then
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Benefit Percentage cannot be greater than 100 Percent."), Me)
                            txtBenefitPercent.Focus()
                            Return False
                        Else
                            txtTotalPercentage.Text = (CDec(IIf(txtTotalPercentage.Text = "", 0, txtTotalPercentage.Text)) + CDec(txtBenefitPercent.Text)).ToString
                            txtRemainingPercent.Text = (100 - CDec(txtTotalPercentage.Text)).ToString
                        End If
                    Case 2  'EDIT
                        Dim arrayPer() As Decimal = Nothing
                        If mintdpndtbenefittranunkid > 0 Then

                            'Pinkal (20-Sep-2017) -- Start
                            'Enhancement - Working Dependanct Benefit Plan Changes for PACRA.
                            'arrayPer = dtBenefitTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of Integer)("dpndtbenefittranunkid") <> mintdpndtbenefittranunkid).Select(Function(x) x.Field(Of Decimal)("benefit_percent")).ToArray
                            arrayPer = dtBenefitTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("benefitgroupunkid") = CInt(cboBenefitGroup.SelectedValue) And _
                                                                                                   x.Field(Of String)("AUD") <> "D" And x.Field(Of Integer)("dpndtbenefittranunkid") <> mintdpndtbenefittranunkid And _
                                                                                                   x.Field(Of Integer)("dpndtbenefittranunkid") <> -999).Select(Function(x) x.Field(Of Decimal)("benefit_percent")).ToArray
                            'Pinkal (20-Sep-2017) -- End
                        ElseIf mstrdpndtbenefitGUID.Trim.Length > 0 Then

                            'Pinkal (20-Sep-2017) -- Start
                            'Enhancement - Working Dependanct Benefit Plan Changes for PACRA.
                            'arrayPer = dtBenefitTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("GUID") <> mstrdpndtbenefitGUID).Select(Function(x) x.Field(Of Decimal)("benefit_percent")).ToArray
                            arrayPer = dtBenefitTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("benefitgroupunkid") = CInt(cboBenefitGroup.SelectedValue) And _
                                                                                                x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("GUID") <> mstrdpndtbenefitGUID And _
                                                                                                x.Field(Of Integer)("dpndtbenefittranunkid") <> -999).Select(Function(x) x.Field(Of Decimal)("benefit_percent")).ToArray
                            'Pinkal (20-Sep-2017) -- End
                        End If

                        Dim dblPer As Decimal = arrayPer.Sum
                        If (CDec(txtBenefitPercent.Text) + dblPer) > 100 Then
                            msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Benefit Percentage cannot be greater than 100 Percent."), Me)
                            txtBenefitPercent.Focus()
                            Return False
                        Else
                            txtTotalPercentage.Text = (CDec(txtBenefitPercent.Text) + dblPer).ToString
                            txtRemainingPercent.Text = (100 - CDec(txtTotalPercentage.Text)).ToString
                        End If
                End Select
            End If

            If intOperation = 3 Then
                Dim arrayPer() As Decimal = Nothing

                'Pinkal (20-Sep-2017) -- Start
                'Enhancement - Working Dependanct Benefit Plan Changes for PACRA.
                'arrayPer = dtBenefitTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Decimal)("benefit_percent")).ToArray
                arrayPer = dtBenefitTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of Integer)("dpndtbenefittranunkid") <> -999).Select(Function(x) x.Field(Of Decimal)("benefit_percent")).ToArray
                'Dim dblPer As Decimal = arrayPer.Sum
                'txtTotalPercentage.Text = dblPer.ToString
                'txtRemainingPercent.Text = (100 - CDec(txtTotalPercentage.Text)).ToString
                'Pinkal (20-Sep-2017) -- End
            End If

            Return True

        Catch ex As Exception
            msg.DisplayError(ex, Me)
            Return False
        End Try
    End Function


#End Region

#Region " Button Event(S) "


    'Pinkal (20-Sep-2017) -- Start
    'Enhancement - Working Dependanct Benefit Plan Changes for PACRA.

    Protected Sub btnBenefit_Close_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBenefit_Close.Click
        Try
            'Dim arrayPer() As Decimal = mdtBenefitTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Decimal)("benefit_percent")).ToArray
            'Dim dblPer As Decimal = arrayPer.Sum
            'txtTotalPercentage.Text = (CDec(txtBenefitPercent.Text) + dblPer).ToString
            'txtRemainingPercent.Text = (100 - CDec(txtTotalPercentage.Text)).ToString
            mblnBenefit_Popup = False
            popup_BenefitInfo.Hide()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (20-Sep-2017) -- End

    Protected Sub btnBenefit_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBenefit_Save.Click
        Try
            mdtBenefitTran = dtBenefitTran.Copy()
            mblnBenefit_Popup = False
            popup_BenefitInfo.Hide()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEditBenefit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditBenefit.Click
        Try
            If Benefit_IsValid() = False Then Exit Sub

            If IsPercentageProper(2) = False Then Exit Sub

            Dim dtRow() As DataRow = Nothing
            If mintdpndtbenefittranunkid > 0 Then
                dtRow = dtBenefitTran.Select("dpndtbenefittranunkid = " & mintdpndtbenefittranunkid)
            ElseIf mstrdpndtbenefitGUID.Trim.Length > 0 Then
                dtRow = dtBenefitTran.Select("GUID = '" & mstrdpndtbenefitGUID & "'")
            End If

            If dtRow IsNot Nothing Then
                dtRow(0).Item("benefitgroupunkid") = CInt(cboBenefitGroup.SelectedValue)
                dtRow(0).Item("benefitplanunkid") = CInt(cboBenefitType.SelectedValue)
                dtRow(0).Item("value_id") = CInt(cboValueBasis.SelectedValue)
                dtRow(0).Item("benefit_percent") = txtBenefitPercent.Text
                dtRow(0).Item("benefit_amount") = txtBenefitAmount.Text
                dtRow(0).Item("benefit_group") = cboBenefitGroup.SelectedItem.Text
                dtRow(0).Item("benefit_type") = cboBenefitType.SelectedItem.Text
                dtRow(0).Item("benefit_value") = cboValueBasis.SelectedItem.Text

                If IsDBNull(dtRow(0).Item("AUD")) OrElse dtRow(0).Item("AUD").ToString = "" Then
                    dtRow(0).Item("AUD") = "U"
                End If

                Call Fill_BenefitList()
                Call ResetBenefit()
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddBenefit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddBenefit.Click
        Try
            If Benefit_IsValid() = False Then Exit Sub


            If IsPercentageProper(1) = False Then Exit Sub

            Dim dtBenfRow As DataRow
            dtBenfRow = dtBenefitTran.NewRow
            dtBenfRow.Item("dpndtbenefittranunkid") = -1
            dtBenfRow.Item("dpndtbeneficetranunkid") = mintdpndtbenefittranunkid
            dtBenfRow.Item("benefitgroupunkid") = CInt(cboBenefitGroup.SelectedValue)
            dtBenfRow.Item("benefitplanunkid") = CInt(cboBenefitType.SelectedValue)
            dtBenfRow.Item("value_id") = CInt(cboValueBasis.SelectedValue)
            dtBenfRow.Item("benefit_percent") = txtBenefitPercent.Text
            dtBenfRow.Item("benefit_amount") = txtBenefitAmount.Text

            dtBenfRow.Item("benefit_group") = cboBenefitGroup.SelectedItem.Text
            dtBenfRow.Item("benefit_type") = cboBenefitType.SelectedItem.Text
            dtBenfRow.Item("benefit_value") = cboValueBasis.SelectedItem.Text

            dtBenfRow.Item("AUD") = "A"
            dtBenfRow.Item("GUID") = Guid.NewGuid.ToString
            dtBenefitTran.Rows.Add(dtBenfRow)
            Call Fill_BenefitList()
            Call ResetBenefit()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnBenefitClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBenefitClear.Click
        Try
            Call ResetBenefit()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Control(S) Event(S)"

    Private Sub cboBenefitGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBenefitGroup.SelectedIndexChanged
        Dim dsCombos As New DataSet
        Try
            dsCombos = (New clsbenefitplan_master).getComboList("Benefit", True, CInt(cboBenefitGroup.SelectedValue))
            With cboBenefitType
                .DataValueField = "benefitplanunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("Benefit")
                .DataBind()
                .SelectedValue = "0"
            End With


            'Pinkal (20-Sep-2017) -- Start
            'Enhancement - Working Dependanct Benefit Plan Changes for PACRA. 
            If CInt(cboBenefitGroup.SelectedValue) > 0 Then
                txtTotalPercentage.Text = "0.00"
                If dtBenefitTran IsNot Nothing AndAlso dtBenefitTran.Rows.Count > 0 Then
                    If Not IsDBNull(dtBenefitTran.Compute("SUM(benefit_percent)", "dpndtbenefittranunkid <> -999 AND AUD <> 'D' AND benefitgroupunkid = " & CInt(cboBenefitGroup.SelectedValue))) Then
                        txtTotalPercentage.Text = Convert.ToDecimal(dtBenefitTran.Compute("SUM(benefit_percent)", "dpndtbenefittranunkid <> -999 AND AUD <> 'D' AND benefitgroupunkid = " & CInt(cboBenefitGroup.SelectedValue))).ToString("#0.00")
                    End If
                End If
                txtRemainingPercent.Text = CDec(100 - CDec(txtTotalPercentage.Text)).ToString("#0.00")
            Else
                txtTotalPercentage.Text = "0.00"
                txtRemainingPercent.Text = "0.00"
            End If
            'Pinkal (20-Sep-2017) -- End


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboValueBasis_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboValueBasis.SelectedIndexChanged
        Dim mintSeletedValue As Integer = -1
        Try
            mintSeletedValue = CInt(cboValueBasis.SelectedValue)

            Select Case CInt(cboValueBasis.SelectedValue)
                Case 1  'Value
                    txtBenefitPercent.Text = "0"
                    txtBenefitPercent.Enabled = False
                    txtBenefitAmount.Enabled = True
                Case 2  'Percent
                    txtBenefitAmount.Text = "0"
                    txtBenefitAmount.Enabled = False
                    txtBenefitPercent.Enabled = True
                    'Pinkal (20-Sep-2017) -- Start
                    'Enhancement - Working Dependanct Benefit Plan Changes for PACRA.
                Case Else
                    txtBenefitAmount.Enabled = True
                    txtBenefitPercent.Enabled = True
                    'Pinkal (20-Sep-2017) -- End
            End Select
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (20-Sep-2017) -- Start
    'Enhancement - Working Dependanct Benefit Plan Changes for PACRA.

    Protected Sub menuBenefitInfor_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles menuBenefitInfor.Click
        Try
            Call Load_Benefit_Inforamation()
            popup_BenefitInfo.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)  'Hemant (13 Aug 2020)
        End Try
    End Sub
    'Pinkal (20-Sep-2017) -- End
#End Region


    'Pinkal (20-Sep-2017) -- Start
    'Enhancement - Working Dependanct Benefit Plan Changes for PACRA.
#Region "Datagrid Events"

    Protected Sub lvBenefit_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lvBenefit.ItemCommand
        Dim drTemp() As DataRow
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                If e.CommandName = "objEdit" Then
                    cboBenefitGroup.SelectedValue = e.Item.Cells(7).Text
                    cboBenefitGroup_SelectedIndexChanged(cboBenefitGroup, Nothing)
                    cboBenefitType.SelectedValue = e.Item.Cells(8).Text
                    cboValueBasis.SelectedValue = e.Item.Cells(9).Text
                    cboValueBasis_SelectedValueChanged(cboValueBasis, Nothing)
                    txtBenefitPercent.Text = e.Item.Cells(5).Text
                    txtBenefitAmount.Text = e.Item.Cells(6).Text
                    mintdpndtbenefittranunkid = CInt(e.Item.Cells(10).Text)
                    mstrdpndtbenefitGUID = e.Item.Cells(11).Text
                    cboBenefitGroup.Enabled = False
                    cboBenefitType.Enabled = False
                    btnAddBenefit.Visible = False
                    btnEditBenefit.Visible = True
                ElseIf e.CommandName = "objDelete" Then
                    If CInt(e.Item.Cells(10).Text) = -1 Then
                        drTemp = dtBenefitTran.Select("GUID = '" & e.Item.Cells(11).Text & "'")
                    Else
                        drTemp = dtBenefitTran.Select("dpndtbenefittranunkid = " & CInt(e.Item.Cells(10).Text))
                    End If
                    If drTemp.Length > 0 Then
                        drTemp(0).Item("AUD") = "D"
                        drTemp(0).Item("isvoid") = True
                        drTemp(0).Item("voiduserunkid") = CInt(Session("UserId"))
                        drTemp(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                        drTemp(0).Item("voidreason") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "Incorrect Benefit Entry.")


                        'Pinkal (20-Sep-2017) -- Start
                        'Enhancement - Working Dependanct Benefit Plan Changes for PACRA.
                        If dtBenefitTran.Select("AUD <> 'D' AND dpndtbenefittranunkid <> -999 AND benefitgroupunkid = " & CInt(e.Item.Cells(7).Text)).Length <= 0 Then
                            Dim drRow As DataRow() = dtBenefitTran.Select("dpndtbenefittranunkid = -999 AND benefitgroupunkid = " & CInt(e.Item.Cells(7).Text))
                            If drRow.Length > 0 Then
                                dtBenefitTran.Rows.Remove(drRow(0))
                                dtBenefitTran.AcceptChanges()
                            End If
                        End If
                        'Pinkal (20-Sep-2017) -- End

                        Call Fill_BenefitList()
                        If IsPercentageProper(3) = False Then Exit Sub
                    End If

                End If
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lvBenefit_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles lvBenefit.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then

                If CInt(e.Item.Cells(10).Text) = -999 Then

                    'Pinkal (30-Jul-2021)-- Start
                    'New UI Issues.
                    CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(1).FindControl("imgDelete"), LinkButton).Visible = False
                    'Pinkal (30-Jul-2021) -- End

                    e.Item.Cells(0).Visible = False
                    e.Item.Cells(1).Visible = False
                    e.Item.Cells(2).ColumnSpan = e.Item.Cells.Count - 7

                    For i = 3 To e.Item.Cells.Count - 8
                        e.Item.Cells(i).Visible = False
                    Next

                    e.Item.Cells(2).CssClass = "GroupHeaderStyleBorderLeft"    'For Benefit Group
                    e.Item.Cells(2).Style.Add("text-align", "left")

                    e.Item.Cells(5).CssClass = "GroupHeaderStyleBorderLeft"    'For Percentage
                    e.Item.Cells(5).Style.Add("text-align", "right")

                    e.Item.Cells(6).CssClass = "GroupHeaderStyleBorderLeft"  'For Amount
                    e.Item.Cells(6).Style.Add("text-align", "right")

                End If

                e.Item.Cells(5).Text = CDec(e.Item.Cells(5).Text).ToString("#0.00")
                e.Item.Cells(6).Text = Format(CDec(e.Item.Cells(6).Text), Session("fmtCurrency").ToString())

            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

#End Region
    'Pinkal (20-Sep-2017) -- End


#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Firstname can not be blank. Firstname is compulsory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Lastname can not be blank. Lastname is compulsory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Relation is compulsory information. Please select relation to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Invalid Email.Please Enter Valid Email.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Selected information is already present for particular employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Benefit is compulsory information. Please select Benefit to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Benefit In is compulsory information. Please select Benefit In to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Amount cannot be blank .Amount is compulsory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Percent cannot be blank .Percent is compulsory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 11, "Percent cannot be greater than 100.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Selected Benefit is already added to the list.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Membership is compulsory information. Please select Membership to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Membership No. cannot be blank. Membership No. is compulsory information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Membership Category is compulsory information. Please select Membership Category to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Selected Membership is already added to the list.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 19, "Date of birth cannot be greater than or equal to today date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "Sorry, particular person cannot be consider as dependants or beneficairy. Reason : the age is greater than the limit set.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 21, "Benefit Percentage cannot be greater than 100 Percent.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 22, "Incorrect Benefit Entry.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 37, "Benefit Group is compulsory information. Please select Benefit Group to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 39, "Are you sure you want to delete this attachment?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 40, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 46, "Total")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 48, "Please set effective date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 49, "Effective date should not be greater than current date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 50, "Filename or Destination File Path is not Correct Format. Please Check!!!")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1001, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in edit mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1002, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in edit mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1003, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in edit mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1004, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in edit mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1005, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in deleted mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1006, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in deleted mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1007, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in deleted mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1008, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in deleted mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1009, "Sorry, you cannot edit Dependant information, Reason : Same Dependant information is already present for approval process in added mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1010, "Sorry, you cannot edit Beneficiary information, Reason : Same Beneficiary information is already present for approval process in added mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1011, "Sorry, you cannot add Dependant information, Reason : Same Dependant information is already present for approval process in added mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1012, "Sorry, you cannot add Beneficiary information, Reason : Same Beneficiary information is already present for approval process in added mode.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1013, "This Dependant is already defined. Please define new Dependant.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1014, "This Beneficiary is already defined. Please define new Beneficiary.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2001, "File is used by another process or File is already open.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2002, "File does not Exist...")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2003, "File does not exist on localpath")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2004, "No Files to download.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2005, "Please Select the Employee Image.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2006, "Dependant inserted Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2007, "Dependant Updated Successfully.")

        Catch Ex As Exception
            msg.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
