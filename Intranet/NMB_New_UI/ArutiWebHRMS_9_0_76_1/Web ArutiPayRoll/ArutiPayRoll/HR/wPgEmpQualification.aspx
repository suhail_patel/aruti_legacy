﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPgEmpQualification.aspx.vb"
    Inherits="HR_wPgEmpQualification" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script>
        function IsValidAttach() {
            var cbodoctype = $('#<%= cboDocumentType.ClientID %>');
            var cboEmp = $('#<%= cboEmployee.ClientID %>');
            var chkOtherQqali = $('#<%= chkOtherQualification.ClientID %>');
            var txtQualiGrp = $('#<%= txtOtherQualificationGrp.ClientID %>');
            var txtQuali = $('#<%= txtOtherQualification.ClientID %>');
            var txtQualiRultCode = $('#<%= txtOtherResultCode.ClientID %>');
            var cboQualiGrp= $('#<%= cboQualifGrp.ClientID %>');
            var cboQuali = $('#<%= cboQualifcation.ClientID %>');
            var cboQualiRultCode = $('#<%= cboResultCode.ClientID %>');
            var cboProvider = $('#<%= cboProvider.ClientID %>');
            var txtGpaCode = $('#<%= txtGPAcode.ClientID %>');
            
            if (parseInt(cbodoctype.val()) <= 0) {
                alert('Please Select Document Type.');
                cbodoctype.focus();
                return false;
            }
            if (parseInt(cboEmp.val()) <= 0) {
                alert('Employee is compulsory information. Please select Employee to continue.');
                cboEmp.focus();
                return false;
            }
            if (chkOtherQqali.attr("checked") == "checked") {
                if (txtQualiGrp.val().trim() == "") {
                    alert('Other Qualification Group cannot be blank.Other Qualification Group is compulsory information.');
                    txtQualiGrp.focus();
                    return false;
                }
                if (txtQuali.val().trim() == "") {
                    alert('Other Qualification cannot be blank.Other Qualification is compulsory information.');
                    txtQuali.focus();
                    return false;
                }
                if (txtQualiRultCode.val().trim() == "") {
                    alert('Other Result Code cannot be blank.Other Result Code is compulsory information.');
                    txtQualiRultCode.focus();
                    return false;
                }
            }else{
                if (parseInt(cboQualiGrp.val()) <= 0) {
                    alert('Qualification Group is compulsory information. Please select Qualification Group to continue.');
                    cboQualiGrp.focus();
                    return false;
                }
                if (parseInt(cboQuali.val()) <= 0) {
                    alert('Qualification is compulsory information. Please select Qualification to continue.');
                    cboQuali.focus();
                    return false;
                }
                if (parseInt(cboQualiRultCode.val()) <= 0) {
                    alert('Result Code is compulsory information. Please select Result Code to continue.');
                    cboQualiRultCode.focus();
                    return false;
                }
            }
            if (parseInt(cboProvider.val()) <= 0) {
                alert('Provider is compulsory information. Please select Provider to continue.');
                cboProvider.focus();
                return false;
            }
            if (parseInt(txtGpaCode.val()) > 100) {
                alert('GPA code cannot be greater than 100.');
                txtGpaCode.focus();
                return false;
            }
            return true;
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width","auto");
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Qualification ADD/EDIT"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkOtherQualification" runat="server" Text="Other Qualification"
                                            AutoPostBack="true" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-l-0">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEmployee" runat="server" Text="Employee :" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <asp:MultiView ID="mltView" runat="server" ActiveViewIndex="0">
                                            <asp:View ID="mvQualification" runat="server">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblQualificationGroup" runat="server" Text="Qualification Group :"
                                                        CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList data-live-search="true" ID="cboQualifGrp" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblQualification" runat="server" Text="Qualification :" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList data-live-search="true" ID="cboQualifcation" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblResultCode" runat="server" Text="Result Code :" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <asp:DropDownList data-live-search="true" ID="cboResultCode" runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </asp:View>
                                            <asp:View ID="mvOtherQualification" runat="server">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblOtherQualificationGrp" runat="server" Text=" Other Qualif. Group :"
                                                        CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtOtherQualificationGrp" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblOtherQualification" runat="server" Text="Other Qualif. :" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtOtherQualification" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblOtherResultCode" runat="server" Text="Other Result Code :" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <asp:TextBox ID="txtOtherResultCode" runat="server" CssClass="form-control"></asp:TextBox></div>
                                                    </div>
                                                </div>
                                            </asp:View>
                                        </asp:MultiView>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblReferenceNo" runat="server" Text="Reference No :" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtRefNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-r-0 ">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblAwardDate" runat="server" Text="Date :" CssClass="form-label"></asp:Label>
                                            <uc2:DateCtrl ID="dtQualifDate" runat="server" AutoPostBack="false" />
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblGPAcode" runat="server" Text="GPA/Points :" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtGPAcode" runat="server" CssClass="form-control text-right"></asp:TextBox></div>
                                            </div>
                                            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtGPAcode"
                                                CssClass="ErrorControl" ErrorMessage="This Number is Invalid!" Display="Dynamic"
                                                ForeColor="White" MaximumValue="99999" MinimumValue="0" Type="Double" SetFocusOnError="True">
                                            </asp:RangeValidator>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblInstitution" runat="server" Text="Provider :" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList data-live-search="true" ID="cboProvider" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                            <asp:Panel ID="pnlOtherInstitute" runat="server">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtOtherInstitute" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date :" CssClass="form-label"></asp:Label>
                                            <uc2:DateCtrl ID="dtAFromDate" runat="server" AutoPostBack="True" />
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblEndDate" runat="server" Text="Award Date :" CssClass="form-label"></asp:Label>
                                            <uc2:DateCtrl ID="dtAToDate" runat="server" AutoPostBack="True" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRemark" runat="server" Text="Remark :" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRemark" runat="server" row="3" TextMode="MultiLine" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix d--f ai--c">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboDocumentType" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-b-0 d--f">
                                        <asp:Panel ID="pnl_ImageAdd" runat="server">
                                            <div id="fileuploader">
                                                <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" onclick="return IsValidAttach()"
                                                    value="Browse" />
                                            </div>
                                        </asp:Panel>
                                        <asp:Button ID="btnAddAttachment" runat="server" Style="display: none" Text="Browse"
                                            OnClick="btnSaveAttachment_Click" />
                                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-default" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 300px">
                                            <asp:DataGrid ID="dgvQualification" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn FooterText="objcohDelete">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete">
                                                                    <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                    <asp:BoundColumn HeaderText="File Size" DataField="filesize" FooterText="colhSize" />
                                                    <asp:TemplateColumn HeaderText="Download" FooterText="objcohDelete" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="DownloadLink" runat="server" CommandName="imgdownload" ToolTip="Delete">
                                                                    <i class="fas fa-download text-primary"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn HeaderText="objcolhGUID" DataField="GUID" FooterText="objcolhGUID"
                                                        Visible="false" />
                                                    <asp:BoundColumn HeaderText="objcolhScanUnkId" DataField="scanattachtranunkid" FooterText="objcolhScanUnkId"
                                                        Visible="false" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />
                                    <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <ucCfnYesno:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
            </ContentTemplate>
            <Triggers>
                <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
			$(document).ready(function()
			{
				ImageLoad();
				$(".ajax-upload-dragdrop").css("width","auto");
			});
			function ImageLoad(){
			    if ($(".ajax-upload-dragdrop").length <= 0){
			    $("#fileuploader").uploadFile({
				    url: "wPgEmpQualification.aspx?uploadimage=mSEfU19VPc4=",
                    method: "POST",
				    dragDropStr: "",
				    showStatusAfterSuccess:false,
                    showAbort:false,
                    showDone:false,
				    fileName:"myfile",
				    onSuccess:function(path,data,xhr){
				        $("#<%= btnAddAttachment.ClientID %>").click();
				    },
                    onError:function(files,status,errMsg){
	                        alert(errMsg);
                        }
                });
			}
			}
			//$('input[type=file]').live("click",function(){
			$("body").on("click", 'input[type=file]', function() {
			    return IsValidAttach();
			});
    </script>

</asp:Content>
