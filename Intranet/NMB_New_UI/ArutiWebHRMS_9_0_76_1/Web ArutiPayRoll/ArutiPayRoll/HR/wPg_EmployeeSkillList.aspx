<%@ Page Language="VB" AutoEventWireup="false" CodeFile="wPg_EmployeeSkillList.aspx.vb"
    Inherits="HR_wPg_EmployeeSkillList" MasterPageFile="~/Home1.master" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report"
    TagPrefix="uc3" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
    <asp:Panel ID="pnlData" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <ucDel:DeleteReason ID="popup_DeleteReason" runat="server" Title="Are ou Sure You Want To delete?:" />
                <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Skill List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpEmployee" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblSkillCategory" runat="server" Text="Skill Category" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpCategory" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblSkill" runat="server" Text="Skill" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpSkill" runat="server" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnNew" runat="server" Text="New" CssClass="btn btn-primary" Width="77px"
                                    Visible="False" />
                                <asp:Button ID="BtnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="BtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                <asp:Panel ID="objtblPanel" runat="server" Style="float: left;">
                                    <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btn btn-primary" Text="View Detail" />
                                    <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" CssClass="label label-primary bg-pw"></asp:Label>
                                    <asp:Label ID="lblParentData" runat="server" Text="Parent Detail" CssClass="label label-danger bg-lc"></asp:Label>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <!-- Task Info -->
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive">
                                            <asp:DataGrid ID="gvSkill" runat="server" Style="margin: auto" AutoGenerateColumns="False"
                                                CssClass="table table-hover table-bordered" AllowPaging="false" Width="99%">
                                                <Columns>
                                                    <asp:TemplateColumn>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="ImgSelect" runat="server" ToolTip="Select" CommandName="Select"><i class="fas fa-pencil-alt"></i> </asp:LinkButton>
                                                            <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/edit.png" ToolTip="Select"
                                                    CommandName="Select" />--%>
                                                            <%--'Gajanan [17-DEC-2018] -- Start--%>
                                                            <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                            <asp:LinkButton ID="imgDetail" runat="server" ToolTip="Highlight Original Details"
                                                                CommandName="View" Visible="false"><i class="fas fa-eye"></i> </asp:LinkButton>
                                                            <asp:HiddenField ID="hfoprationtypeid" runat="server" Value='<%#Eval("operationtypeid") %>' />
                                                            <%--'Gajanan [17-DEC-2018] -- End--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete"><i class="fas fa-trash text-danger"></i> </asp:LinkButton>
                                                            <asp:HiddenField ID="hfemployeeid" runat="server" Value='<%#Eval("empid") %>' />
                                                            <%--<asp:ImageButton ID="" runat="server" ImageUrl="~/images/remove.png" ToolTip="Delete"
                                                    CommandName="Delete" />--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="NAME" HeaderText="Employee" ReadOnly="True" FooterText="colhEmployee">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Category" HeaderText="Skill Category" ReadOnly="True"
                                                        FooterText="colhSkillCategory"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="SkillName" HeaderText="Skill" ReadOnly="True" FooterText="colhSkill">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Description" HeaderText="Description" ReadOnly="True"
                                                        FooterText="colhDescription"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="EmpId" HeaderText="EmpId" Visible="false" FooterText="objdgcolhempid">
                                                    </asp:BoundColumn>
                                                    <%--'Gajanan [17-DEC-2018] -- Start--%>
                                                    <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                                    <asp:BoundColumn DataField="SkillTranId" HeaderText="SkillTranId" Visible="false"
                                                        FooterText="objdgcolhSkillTranId"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" FooterText="colhOperationType">
                                                    </asp:BoundColumn>
                                                    <%--<11>--%>
                                                    <%--'Gajanan [17-DEC-2018] -- End--%>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
