﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports Aruti.Data

#End Region

Partial Class wPgEmpMembership_List
    Inherits Basepage

#Region " Private Variable(s) "

    Private objEmpMember As New clsMembershipTran
    Dim msg As New CommonCodes

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeMaster"
    Private ReadOnly mstrModuleName1 As String = "frmEmployeeList"
    'Pinkal (06-May-2014) -- End

    'S.SANDEEP [31 AUG 2015] -- START
    'ENHANCEMENT : -------------- LEFT OUT CODE
    Private ReadOnly mstrModuleName2 As String = "frmMembershipHead"
    'S.SANDEEP [31 AUG 2015] -- END


    'Nilay (31-Aug-2015) -- Start
    Private mintmembershiptranunkid As Integer = -1
    'Nilay (31-Aug-2015) -- End


    'S.SANDEEP |15-APR-2019| -- START
    Private objAMemInfoTran As New clsMembership_Approval_Tran
    Dim Arr() As String
    Dim MembershipApprovalFlowVal As String
    Dim blnPendingEmployee As Boolean = False
    Dim intEmployeeId As Integer = 0
    'S.SANDEEP |15-APR-2019| -- END



    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objMembership As New clsmembership_master
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsCombos = objEmployee.GetEmployeeList("Employee", True, Not Aruti.Data.ConfigParameter._Object._IsIncludeInactiveEmp)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsCombos = objEmployee.GetEmployeeList("Employee", True, )
                '    dsCombos = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END
                'End If
                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                'Shani(24-Aug-2015) -- End
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombos.Tables("Employee")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()
            End If

            dsCombos = objMembership.getListForCombo("List", True)
            With cboMembership
                .DataValueField = "membershipunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ClearObject()
        Try
            cboMembership.SelectedValue = CStr(0)
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            drpEmployee.SelectedIndex = 0
            'Sohail (02 May 2012) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'S.SANDEEP [31 AUG 2015] -- START
    'ENHANCEMENT : -------------- LEFT OUT CODE
    Private Sub SetMembershipValue()
        Try
            If mintmembershiptranunkid > 0 Then
                Dim dTable As New DataTable
                Dim objEmpMembership As New clsMembershipTran
                '
                'S.SANDEEP |15-APR-2019| -- START
                'objEmpMembership._EmployeeUnkid = CInt(IIf(drpEmployee.SelectedValue = "", 0, drpEmployee.SelectedValue))
                objEmpMembership._EmployeeUnkid = intEmployeeId
                'S.SANDEEP |15-APR-2019| -- END
                dTable = objEmpMembership._DataList.Copy
                Dim drTemp() As DataRow = dTable.Select("membershiptranunkid = '" & mintmembershiptranunkid & "'")
                With drTemp(0)
                    .Item("emptrnheadid") = .Item("emptrnheadid")
                    .Item("cotrnheadid") = .Item("cotrnheadid")
                    .Item("effetiveperiodid") = CInt(cboEffectivePeriod.SelectedValue)
                    .Item("ccategory") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMembershipTran", 1, "Active")
                    .Item("ccategoryid") = 1
                    .Item("isdeleted") = False
                    .Item("isactive") = True
                    .Item("copyedslab") = chkCopyPreviousEDSlab.Checked
                    .Item("overwriteslab") = False
                    .Item("overwritehead") = chkOverwriteHeads.Checked
                    If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
                        .Item("AUD") = "U"
                    End If
                End With
                dTable.AcceptChanges()
                objEmpMembership._DataList = dTable

                'S.SANDEEP |15-APR-2019| -- START
                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objEmpMembership.InsertUpdateDelete_MembershipTran(Session("UserId")) Then
                'If objEmpMembership.InsertUpdateDelete_MembershipTran(CStr(Session("Database_Name")), _
                '                                                      CInt(Session("UserId")), _
                '                                                      CInt(Session("Fin_year")), _
                '                                                      CInt(Session("CompanyUnkId")), _
                '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                                      CStr(Session("UserAccessModeSetting")), True, _
                '                                                      CBool(Session("IsIncludeInactiveEmp")), _
                '                                                      CBool(Session("AllowToApproveEarningDeduction")), _
                '                                                      ConfigParameter._Object._CurrentDateAndTime, Nothing) Then
                '    'Shani(20-Nov-2015) -- End
                'Call FillGrid()
                'End If
                If MembershipApprovalFlowVal Is Nothing Then
                    objAMemInfoTran._Employeeunkid = intEmployeeId
                    Dim mdtApprovalMembershipTran = objAMemInfoTran._DataTable.Copy()
                    Dim dtMemRow = mdtApprovalMembershipTran.NewRow()
                    dtMemRow = mdtApprovalMembershipTran.NewRow
                    dtMemRow.Item("tranguid") = Guid.NewGuid.ToString()
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        dtMemRow.Item("loginemployeeunkid") = Session("Employeeunkid")
                    End If
                    dtMemRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
                    dtMemRow.Item("mappingunkid") = 0
                    dtMemRow.Item("approvalremark") = ""
                    dtMemRow.Item("isfinal") = False
                    dtMemRow.Item("statusunkid") = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                    dtMemRow.Item("isvoid") = False
                    dtMemRow.Item("voidreason") = ""
                    dtMemRow.Item("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                    dtMemRow.Item("audittype") = enAuditType.ADD
                    dtMemRow.Item("audituserunkid") = Session("UserId")
                    dtMemRow.Item("ip") = CStr(Session("IP_ADD"))
                    dtMemRow.Item("host") = CStr(Session("HOST_NAME"))
                    dtMemRow.Item("form_name") = mstrModuleName
                    dtMemRow.Item("isweb") = True
                    dtMemRow.Item("isprocessed") = False
                    dtMemRow.Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.DELETED
                    dtMemRow.Item("operationtype") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsEmployeeDataApproval", 50, "Information Deleted")
                    dtMemRow.Item("membership_category") = ""
                    dtMemRow.Item("membershipname") = ""
                    dtMemRow.Item("membershiptranunkid") = drTemp(0)("membershiptranunkid")
                    dtMemRow.Item("employeeunkid") = drTemp(0)("employeeunkid")
                    dtMemRow.Item("membership_categoryunkid") = drTemp(0)("membership_categoryunkid")
                    dtMemRow.Item("membershipunkid") = drTemp(0)("membershipunkid")
                    dtMemRow.Item("membershipno") = drTemp(0).Item("membershipno")
                    dtMemRow.Item("issue_date") = drTemp(0).Item("issue_date")
                    dtMemRow.Item("start_date") = drTemp(0).Item("start_date")
                    dtMemRow.Item("expiry_date") = drTemp(0).Item("expiry_date")
                    dtMemRow.Item("remark") = drTemp(0).Item("remark")
                    dtMemRow.Item("isactive") = False
                    dtMemRow.Item("AUD") = "A"
                    dtMemRow.Item("GUID") = Guid.NewGuid().ToString
                    dtMemRow.Item("ccategory") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMembershipTran", 1, "Active")
                    dtMemRow.Item("ccategoryid") = 1
                    dtMemRow.Item("isdeleted") = False
                    dtMemRow.Item("emptrnheadid") = drTemp(0).Item("emptrnheadid")
                    dtMemRow.Item("cotrnheadid") = drTemp(0).Item("cotrnheadid")
                    dtMemRow.Item("effetiveperiodid") = CInt(cboEffectivePeriod.SelectedValue)
                    dtMemRow.Item("copyedslab") = drTemp(0).Item("copyedslab")
                    dtMemRow.Item("overwriteslab") = drTemp(0).Item("overwriteslab")
                    dtMemRow.Item("overwritehead") = drTemp(0).Item("overwritehead")
                    mdtApprovalMembershipTran.Rows.Add(dtMemRow)

                    objAMemInfoTran.InsertUpdateDelete_MembershipTran(mdtApprovalMembershipTran, Nothing, _
                                                                      CInt(Session("UserId")), _
                                                                      CStr(Session("Database_Name")), _
                                                                      CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                      CStr(Session("UserAccessModeSetting")), True, _
                                                                      CBool(Session("IsIncludeInactiveEmp")))

                Else
                    objEmpMembership.InsertUpdateDelete_MembershipTran(CStr(Session("Database_Name")), _
                                                                      CInt(Session("UserId")), _
                                                                      CInt(Session("Fin_year")), _
                                                                      CInt(Session("CompanyUnkId")), _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                      CStr(Session("UserAccessModeSetting")), True, _
                                                                      CBool(Session("IsIncludeInactiveEmp")), _
                                                                      CBool(Session("AllowToApproveEarningDeduction")), _
                                                                       ConfigParameter._Object._CurrentDateAndTime, Nothing)
                End If
                'S.SANDEEP |15-APR-2019| -- END




                Call FillGrid()

                objEmpMembership = Nothing
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP [31 AUG 2015] -- END

    Private Sub FillGrid()
        Dim dtList As New DataTable
        Dim dTable As DataTable
        Dim StrSearching As String = ""
        Try


            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("ViewEmployee")) = False Then Exit Sub
            End If

            'Pinkal (22-Nov-2012) -- End


            objEmpMember._EmployeeUnkid = CInt(drpEmployee.SelectedValue)
            dtList = objEmpMember._DataList

            If CInt(drpEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND employeeunkid = '" & drpEmployee.SelectedValue & "' "
            End If

            If CInt(cboMembership.SelectedValue) > 0 Then
                StrSearching &= "AND membershipunkid = '" & cboMembership.SelectedValue & "' "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
                dTable = New DataView(dtList, StrSearching, "ccategoryid", DataViewRowState.CurrentRows).ToTable
            Else
                dTable = New DataView(dtList, "", "ccategoryid", DataViewRowState.CurrentRows).ToTable
            End If

            'S.SANDEEP |15-APR-2019| -- START
            If MembershipApprovalFlowVal Is Nothing = False Then
                Dim dcol As New DataColumn
                With dcol
                    .ColumnName = "operationtypeid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                End With
                dTable.Columns.Add(dcol)

                dcol = New DataColumn
                With dcol
                    .ColumnName = "operationtype"
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                dTable.Columns.Add(dcol)


                dcol = New DataColumn
                With dcol
                    .DataType = GetType(System.String)
                    .ColumnName = "tranguid"
                    .DefaultValue = ""
                End With
                dTable.Columns.Add(dcol)
            End If

            If MembershipApprovalFlowVal Is Nothing Then
                objAMemInfoTran._Employeeunkid = CInt(drpEmployee.SelectedValue)
                dtList = objAMemInfoTran._DataTable

                Dim dcol As New DataColumn
                With dcol
                    .ColumnName = "operationtypeid"
                    .DataType = GetType(System.Int32)
                    .DefaultValue = 0
                End With
                dTable.Columns.Add(dcol)

                dcol = New DataColumn
                With dcol
                    .ColumnName = "operationtype"
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                dTable.Columns.Add(dcol)

                dcol = New DataColumn
                With dcol
                    .ColumnName = "tranguid"
                    .DataType = GetType(System.String)
                    .DefaultValue = ""
                End With
                dTable.Columns.Add(dcol)

                For Each dr As DataRow In dtList.Rows
                    If dTable.AsEnumerable().Where(Function(x) x.Field(Of String)("tranguid") = dr("tranguid").ToString()).Count > 0 Then Continue For
                    Dim dtRow As DataRow = dTable.NewRow()
                    dtRow("catagory") = dr("membership_category")
                    dtRow("Membership") = dr("membershipname")
                    dtRow("membershipno") = dr("membershipno")
                    dtRow("issue_date") = dr("issue_date")
                    dtRow("start_date") = dr("start_date")
                    dtRow("expiry_date") = dr("expiry_date")
                    dtRow("remark") = dr("remark")
                    dtRow("membershiptranunkid") = dr("membershiptranunkid")
                    dtRow("ccategoryid") = dr("ccategoryid")
                    dtRow("membership_categoryunkid") = dr("membership_categoryunkid")
                    dtRow("membershipunkid") = dr("membershipunkid")
                    dtRow("emptrnheadid") = dr("emptrnheadid")
                    dtRow("cotrnheadid") = dr("cotrnheadid")
                    dtRow("operationtypeid") = dr("operationtypeid")
                    dtRow("operationtype") = dr("operationtype")
                    dtRow("tranguid") = dr("tranguid")
                    dtRow("employeeunkid") = dr("employeeunkid")

                    dTable.Rows.Add(dtRow)
                Next

            End If
            'S.SANDEEP |15-APR-2019| -- END

            If dTable IsNot Nothing Then
                'S.SANDEEP [31 AUG 2015] -- START
                'ENHANCEMENT : -------------- LEFT OUT CODE
                'dgvMembership.DataSource = dTable
                'dgvMembership.DataKeyField = "membershiptranunkid"
                'dgvMembership.DataBind()

                Dim dtFinalTab As DataTable = dTable.Clone
                Dim strccategory As String = ""
                Dim dRow As DataRow = Nothing
                dtFinalTab.Columns.Add("IsGrp", System.Type.GetType("System.String")).DefaultValue = ""
                For Each row As DataRow In dTable.Rows
                    If strccategory <> row.Item("ccategory").ToString Then
                        dRow = dtFinalTab.NewRow()
                        dRow.Item("IsGrp") = True
                        dRow.Item("ccategory") = row.Item("ccategory")
                        dtFinalTab.Rows.Add(dRow)
                        strccategory = row.Item("ccategory").ToString
                    End If
                    dRow = dtFinalTab.NewRow()
                    For Each dcol As DataColumn In dTable.Columns
                        dRow.Item(dcol.ColumnName) = row(dcol.ColumnName)
                    Next
                    dRow.Item("IsGrp") = False
                    dtFinalTab.Rows.Add(dRow)
                Next

                dgvMembership.DataSource = dtFinalTab
                dgvMembership.DataKeyField = "membershiptranunkid"

                Session("EmpMemdtFinalTab") = dtFinalTab

                dgvMembership.DataBind()
                'S.SANDEEP [31 AUG 2015] -- END
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Public Shared Function b64encode(ByVal StrEncode As String) As String
        Try
            Dim encodedString As String
            encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
            Return (encodedString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub delete()
        Try
            Dim objEM As New clsMembershipTran
            If CInt(IIf(drpEmployee.SelectedValue = "", 0, drpEmployee.SelectedValue)) <= 0 Then
                msg.DisplayMessage("Please select employee to continue.", Me)
                Exit Sub
            End If

            'Nilay (31-Aug-2015) -- Start
            'If objEM.isUsed(Me.ViewState("Unkid"), drpEmployee.SelectedValue) = False Then
            If objEM.isUsed(mintmembershiptranunkid, CInt(drpEmployee.SelectedValue)) = False Then
                popup_DeleteReason.Reason = ""
                'Nilay (31-Aug-2015) -- End
                popup_DeleteReason.Show() 'Shani (01 FEB 2015) -- Start--popup1.Show()
            Else

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'msg.DisplayMessage("Sorry, you cannot delete this membership as its already used in some transaction.", Me)
                'Language.setLanguage(mstrModuleName)
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 42, "Sorry, You can not delete this Membership information. Reason : This Membership information in use."), Me)
                'Pinkal (06-May-2014) -- End
                Exit Sub
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Private Sub SetPrevious_State()
    '    Try
    '        Session("Prev_State") = drpEmployee.SelectedValue & "|" & _
    '                                cboMembership.SelectedValue & "|" & _
    '                                dgvMembership.CurrentPageIndex()
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub GetPrevious_State()
    '    Try
    '        Dim StrState() As String = Nothing
    '        StrState = CType(Session("Prev_State"), String).Split("|")

    '        If StrState.Length > 0 Then
    '            drpEmployee.SelectedValue = StrState(0)
    '            cboMembership.SelectedValue = StrState(1)
    '            dgvMembership.CurrentPageIndex = StrState(2)
    '        End If

    '        Session("IsFrom_AddEdit") = False

    '        Call FillGrid()

    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmEmployeeMaster"
            StrModuleName2 = "mnuCoreSetups"
            clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))

            'Pinkal (24-Aug-2012) -- Start
            'Enhancement : TRA Changes

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            'Pinkal (24-Aug-2012) -- End

            'S.SANDEEP [ 04 JULY 2012 ] -- END


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            'S.SANDEEP |15-APR-2019| -- START
            objtblPanel.Visible = False
            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            MembershipApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmMembershipInfoList)))
            If MembershipApprovalFlowVal Is Nothing Then
                btnApprovalinfo.Visible = CBool(Session("AllowToApproveRejectEmployeeMembership"))
            Else
                btnApprovalinfo.Visible = False
            End If
            'S.SANDEEP |15-APR-2019| -- END

            Dim clsuser As New User
            If (Page.IsPostBack = False) Then
                'S.SANDEEP [31 AUG 2015] -- START
                'ENHANCEMENT : -------------- LEFT OUT CODE
                If Session("EmpMemdtFinalTab") IsNot Nothing Then
                    Session("EmpMemdtFinalTab") = Nothing
                End If
                'S.SANDEEP [31 AUG 2015] -- END

                clsuser = CType(Session("clsuser"), User)
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.UserID
                    Me.ViewState("Empunkid") = Session("UserId")
                    'Sohail (30 Mar 2015) -- End

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = Session("UserId")
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END


                    'Anjan (30 May 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'BtnNew.Visible = True
                    'BtnNew.Visible = False
                    BtnNew.Visible = CBool(Session("AddEmployee"))
                    'Anjan (30 May 2012)-End 


                    'S.SANDEEP [ 16 JAN 2014 ] -- START
                    chkShowPending.Visible = CBool(Session("AllowToApproveEmployee"))
                    If Session("ShowPending") IsNot Nothing Then
                        chkShowPending.Checked = True
                    End If
                    'S.SANDEEP [ 16 JAN 2014 ] -- END



                Else
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                    Me.ViewState("Empunkid") = Session("Employeeunkid")
                    'Sohail (30 Mar 2015) -- End

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = -1
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'BtnNew.Visible = False

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'BtnNew.Visible = ConfigParameter._Object._AllowAddMembership
                    BtnNew.Visible = CBool(Session("AllowAddMembership"))
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-End 

                    'S.SANDEEP [ 16 JAN 2014 ] -- START
                    chkShowPending.Visible = False
                    'S.SANDEEP [ 16 JAN 2014 ] -- END

                    'S.SANDEEP |15-APR-2019| -- START
                    blnPendingEmployee = CBool(ViewState("blnPendingEmployee"))
                    intEmployeeId = CInt(ViewState("intEmployeeId"))
                    'S.SANDEEP |15-APR-2019| -- END

                End If


                'Pinkal (22-Mar-2012) -- Start
                'Enhancement : TRA Changes

                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
                'SHANI [01 FEB 2015]--END
                'Pinkal (22-Mar-2012) -- End

                Call FillCombo()

                'S.SANDEEP |15-APR-2019| -- START
                If blnPendingEmployee Then
                    objtblPanel.Visible = blnPendingEmployee
                End If
                'S.SANDEEP |15-APR-2019| -- END

                If CDbl(drpEmployee.SelectedValue) > 0 Then
                    Call FillGrid()
                End If

                'If Session("IsFrom_AddEdit") = True Then
                '    Call GetPrevious_State()
                '    Session("IsFrom_AddEdit") = False
                'End If

                'S.SANDEEP [ 16 JAN 2014 ] -- START
                If Session("ShowPending") IsNot Nothing Then
                    Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                End If
                'S.SANDEEP [ 16 JAN 2014 ] -- END

                'Nilay (02-Mar-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("membershipEmpunkid") IsNot Nothing Then
                    If CInt(Session("membershipEmpunkid")) > 0 Then
                        drpEmployee.SelectedValue = CStr(CInt(Session("membershipEmpunkid")))
                        Call btnSearch_Click(BtnSearch, Nothing)
                        Session.Remove("membershipEmpunkid")
                    End If
                End If
                'Nilay (02-Mar-2015) -- End


                'Nilay (31-Aug-2015) -- Start
            Else
                mintmembershiptranunkid = CInt(Me.ViewState("membershiptranunkid"))
                'Nilay (31-Aug-2015) -- End

                'S.SANDEEP [31 AUG 2015] -- START
                'ENHANCEMENT : -------------- LEFT OUT CODE
                If Session("EmpMemdtFinalTab") IsNot Nothing Then
                    dgvMembership.DataSource = CType(Session("EmpMemdtFinalTab"), DataTable)
                    dgvMembership.DataBind()
                End If
                'S.SANDEEP [31 AUG 2015] -- END
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [01 FEB 2015]--END
        End Try
    End Sub

    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try         'Hemant (13 Aug 2020)
            'Nilay (31-Aug-2015) -- Start
            Me.ViewState.Add("membershiptranunkid", mintmembershiptranunkid)
            'Nilay (31-Aug-2015) -- End

            'S.SANDEEP |15-APR-2019| -- START
            Me.ViewState.Add("blnPendingEmployee", blnPendingEmployee)
            Me.ViewState("intEmployeeId") = intEmployeeId
            'S.SANDEEP |15-APR-2019| -- END


            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleSaveButton(False)
            'ToolbarEntry1.VisibleDeleteButton(False)
            'ToolbarEntry1.VisibleCancelButton(False)
            'ToolbarEntry1.VisibleExitButton(False)
            'SHANI [01 FEB 2015]--END 
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (22-Mar-2012) -- End

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    'Gajanan [27-May-2019] -- Start              
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseForm.Click
        Try
            If IsNothing(Session("Isfromprofile")) = False AndAlso CBool(Session("Isfromprofile")) = True Then
                Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeDetail.aspx", False)
                Session.Remove("Isfromprofile")
            Else
                Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Gajanan [27-May-2019] -- End

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            If CInt(drpEmployee.SelectedValue) <= 0 Then
                msg.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNew.Click
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            If CInt(drpEmployee.SelectedValue) > 0 Then
                Session("membershipEmpunkid") = drpEmployee.SelectedValue
                Session("membershiptranunkid") = -1
            End If
            'Nilay (02-Mar-2015) -- End
            Response.Redirect("~\HR\wPgEmpMemberships.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnNew_Click : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            Call ClearObject()
            Call FillGrid()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnReset_Click Event : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDel.Click
    Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'SHANI [01 FEB 2015]--END 
        Try
            If (popup_DeleteReason.Reason.Trim = "") Then 'Shani (01 FEB 2015) -- Start--If (txtreason.Text = "")
                msg.DisplayMessage(" Please enter delete reason.", Me)
                Exit Sub
            End If
            Dim dTable As New DataTable
            Dim objEmpMembership As New clsMembershipTran
            objEmpMembership._EmployeeUnkid = CInt(IIf(drpEmployee.SelectedValue = "", 0, drpEmployee.SelectedValue))
            dTable = objEmpMembership._DataList.Copy

            'Nilay (31-Aug-2015) -- Start
            'Dim dRow() As DataRow = dTable.Select("membershiptranunkid = '" & Me.ViewState("Unkid") & "'")
            Dim dRow() As DataRow = dTable.Select("membershiptranunkid = '" & mintmembershiptranunkid & "'")
            'Nilay (31-Aug-2015) -- End
            If dRow.Length > 0 Then
                dRow(0)("AUD") = "D"
                'Nilay (31-Aug-2015) -- Start
                dRow(0)("isdeleted") = True
                'Nilay (31-Aug-2015) -- End
                dTable.AcceptChanges()
                objEmpMembership._DataList = dTable

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objEmpMembership.InsertUpdateDelete_MembershipTran(Session("UserId")) = False Then
                If objEmpMembership.InsertUpdateDelete_MembershipTran(CStr(Session("Database_Name")), _
                                                                      CInt(Session("UserId")), _
                                                                      CInt(Session("Fin_year")), _
                                                                      CInt(Session("CompanyUnkId")), _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                      CStr(Session("UserAccessModeSetting")), True, _
                                                                      CBool(Session("IsIncludeInactiveEmp")), _
                                                                      CBool(Session("AllowToApproveEarningDeduction")), _
                                                                      ConfigParameter._Object._CurrentDateAndTime, Nothing) = False Then
                    'Shani(20-Nov-2015) -- End
                    msg.DisplayMessage("Problem in removing the Membership.", Me)
                    'Nilay (31-Aug-2015) -- Start
                Else
                    Me.ViewState("membershiptranunkid") = Nothing
                    mintmembershiptranunkid = -1
                    'Nilay (31-Aug-2015) -- End
                End If
            End If

            popup_DeleteReason.Dispose() 'Shani (01 FEB 2015) -- Start--popup1.Dispose()
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub


    'S.SANDEEP [31 AUG 2015] -- START
    'ENHANCEMENT : -------------- LEFT OUT CODE
    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If CInt(IIf(cboEffectivePeriod.SelectedValue = "", 0, cboEffectivePeriod.SelectedValue)) <= 0 AndAlso cboEffectivePeriod.Enabled = True Then
                msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 1, "Effective Period is mandatory information. Please select Effective Period to continue."), Me)
                popup_active.Show()
                Exit Sub
            End If

            If chkCopyPreviousEDSlab.Checked = True Then
                popup_yesno.Title = "Aruti"
                popup_yesno.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, 2, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?")
                popup_active.Show()
                popup_yesno.Show()
                Exit Sub
            End If

            Call SetMembershipValue()

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub popup_yesno_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_yesno.buttonYes_Click
        Try
            Call SetMembershipValue()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP [31 AUG 2015] -- END

    'S.SANDEEP |15-APR-2019| -- START
    Protected Sub btnApprovalinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovalinfo.Click
        Try
            Popup_Viewreport._UserId = CInt(Session("UserId"))
            Popup_Viewreport._Priority = 0
            Popup_Viewreport._PrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeMemberships
            Popup_Viewreport._FillType = enScreenName.frmMembershipInfoList
            Popup_Viewreport._FilterString = "EM.employeeunkid = " & CInt(drpEmployee.SelectedValue)
            Popup_Viewreport._FromApprovalScreen = False
            Popup_Viewreport._OprationType = clsEmployeeDataApproval.enOperationType.NONE
            Popup_Viewreport.Show()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |15-APR-2019| -- END

#End Region

#Region " Control's Event(s) "

    'Nilay (31-Aug-2015) -- Start
    'Protected Sub dgView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvMembership.DeleteCommand
    '    Try

    '        Me.ViewState.Add("Unkid", dgvMembership.DataKeys(e.Item.ItemIndex))
    '        
    '        'SHANI [01 FEB 2015]-START
    '        'Enhancement - REDESIGN SELF SERVICE.
    '        'txtreason.Visible = True 
    '        'SHANI [01 FEB 2015]--END 
    '        delete()
    '        FillGrid()
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
    'Nilay (31-Aug-2015) -- End



    Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvMembership.ItemCommand
        Try
            'S.SANDEEP |15-APR-2019| -- START
            If dgvMembership.Items.Count > 0 Then
                If dgvMembership.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Drawing.Color.LightCoral).Count() > 0 Then
                    Dim item = dgvMembership.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Drawing.Color.LightCoral).FirstOrDefault()
                    If item IsNot Nothing Then item.BackColor = Drawing.Color.White
                End If
            End If
            'S.SANDEEP |15-APR-2019| -- END

            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            Session("membershipEmpunkid") = CInt(drpEmployee.SelectedValue)
            'Nilay (02-Mar-2015) -- End

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Dim eLMode As Integer
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                eLMode = enLogin_Mode.MGR_SELF_SERVICE
            Else
                eLMode = enLogin_Mode.EMP_SELF_SERVICE
            End If
            'Gajanan [17-April-2019] -- End

            'Call SetPrevious_State()

            'Nilay (31-Aug-2015) -- Start
            'Session("IsFrom_AddEdit") = True

            If e.CommandName.ToUpper = "EDIT" Then
                'S.SANDEEP [31 AUG 2015] -- START
                'ENHANCEMENT : -------------- LEFT OUT CODE
                'Session.Add("membershiptranunkid", CInt(e.Item.Cells(9).Text))
                Session.Add("membershiptranunkid", CInt(e.Item.Cells(11).Text))
                'S.SANDEEP [31 AUG 2015] -- END


                'S.SANDEEP |15-APR-2019| -- START
                'Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpMemberships.aspx", False)
                If MembershipApprovalFlowVal Is Nothing Then
                    Dim item = dgvMembership.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(11).Text = e.Item.Cells(11).Text And x.Cells(20).Text.Trim <> "&nbsp;").FirstOrDefault()
                    If item IsNot Nothing Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 140, "Sorry, you cannot perform edit operation. Reason: Another pending operation is already present in the list"), Me)
                        Exit Sub
                    Else
                        Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpMemberships.aspx", False)
                    End If
                Else
                    Response.Redirect(Session("rootpath").ToString & "HR/wPgEmpMemberships.aspx", False)
                End If
                'S.SANDEEP |15-APR-2019| -- END
            End If

            'S.SANDEEP |15-APR-2019| -- START
            If e.CommandName.ToUpper() = "VIEW" Then
                Dim item = dgvMembership.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(11).Text = e.Item.Cells(11).Text And x.Cells(20).Text.Trim = "&nbsp;").FirstOrDefault()
                If item IsNot Nothing Then
                    item.BackColor = Drawing.Color.LightCoral
                    item.ForeColor = Drawing.Color.Black
                End If
            End If
            'If e.CommandName.ToUpper = "DELETE" Then
            '    'S.SANDEEP [31 AUG 2015] -- START
            '    'ENHANCEMENT : -------------- LEFT OUT CODE
            '    'mintmembershiptranunkid = CInt(e.Item.Cells(9).Text)
            '    mintmembershiptranunkid = CInt(e.Item.Cells(11).Text)
            '    'S.SANDEEP [31 AUG 2015] -- END
            '    delete()
            '    'Nilay (31-Aug-2015) -- Start
            '    'FillGrid()
            '    'Nilay (31-Aug-2015) -- End
            'End If

            If e.CommandName.ToUpper = "DELETE" Then
                mintmembershiptranunkid = CInt(e.Item.Cells(11).Text)
                Dim objEmp As New clsEmployee_Master : Dim blnIsApproved As Boolean = False
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(e.Item.Cells(21).Text)
                blnIsApproved = objEmp._Isapproved
                If MembershipApprovalFlowVal Is Nothing AndAlso blnIsApproved = True Then

                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    If objApprovalData.IsApproverPresent(enScreenName.frmIdentityInfoList, CStr(Session("Database_Name")), _
                                                          CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                          CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeIdentities), _
                                                          CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(e.Item.Cells(21).Text), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                        msg.DisplayMessage(objApprovalData._Message, Me)
                        Exit Sub
                    End If
                    'Gajanan [17-April-2019] -- End


                    Dim item = dgvMembership.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(11).Text = e.Item.Cells(11).Text And x.Cells(20).Text.Trim <> "&nbsp;").FirstOrDefault()
                    If item IsNot Nothing Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 141, "Sorry, you cannot perform delete operation. Reason: Another pending operation is already present in the list"), Me)
                        Exit Sub
                    Else
                        objEmpMember._EmployeeUnkid = CInt(e.Item.Cells(21).Text)
                        Dim mdTran As DataTable = objEmpMember._DataList
                        Dim dtEIdRow() As DataRow = mdTran.Select("membershiptranunkid = '" & mintmembershiptranunkid & "'")
                        If dtEIdRow.Length > 0 Then

                            Dim objMembership As New clsmembership_master
                            objMembership._Membershipunkid = CInt(dtEIdRow(0)("membershipunkid"))

                            Dim objCMaster As New clsCommon_Master
                            objCMaster._Masterunkid = CInt(objMembership._Membershipcategoryunkid)

                            Dim dtApprTran As New DataTable
                            objAMemInfoTran._Employeeunkid = CInt(e.Item.Cells(21).Text)
                            dtApprTran = objAMemInfoTran._DataTable
                            Dim dtMemRow As DataRow = dtApprTran.NewRow()
                            dtMemRow.Item("tranguid") = Guid.NewGuid.ToString()
                            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                                dtMemRow.Item("loginemployeeunkid") = Session("Employeeunkid")
                            End If
                            dtMemRow.Item("transactiondate") = ConfigParameter._Object._CurrentDateAndTime
                            dtMemRow.Item("mappingunkid") = 0
                            dtMemRow.Item("approvalremark") = ""
                            dtMemRow.Item("isfinal") = False
                            dtMemRow.Item("statusunkid") = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                            dtMemRow.Item("isvoid") = False
                            dtMemRow.Item("voidreason") = ""
                            dtMemRow.Item("auditdatetime") = ConfigParameter._Object._CurrentDateAndTime
                            dtMemRow.Item("audittype") = enAuditType.ADD
                            dtMemRow.Item("audituserunkid") = Session("UserId")
                            dtMemRow.Item("ip") = CStr(Session("IP_ADD"))
                            dtMemRow.Item("host") = CStr(Session("HOST_NAME"))
                            dtMemRow.Item("form_name") = mstrModuleName
                            dtMemRow.Item("isweb") = True
                            dtMemRow.Item("isprocessed") = False
                            dtMemRow.Item("operationtypeid") = clsEmployeeDataApproval.enOperationType.DELETED
                            dtMemRow.Item("operationtype") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsEmployeeDataApproval", 50, "Information Deleted")
                            dtMemRow.Item("membership_category") = objCMaster._Name
                            dtMemRow.Item("membershipname") = objMembership._Membershipname
                            dtMemRow.Item("membershiptranunkid") = dtEIdRow(0)("membershiptranunkid")
                            dtMemRow.Item("employeeunkid") = dtEIdRow(0)("employeeunkid")
                            dtMemRow.Item("membership_categoryunkid") = dtEIdRow(0)("membership_categoryunkid")
                            dtMemRow.Item("membershipunkid") = dtEIdRow(0)("membershipunkid")
                            dtMemRow.Item("membershipno") = dtEIdRow(0).Item("membershipno")
                            dtMemRow.Item("issue_date") = dtEIdRow(0).Item("issue_date")
                            dtMemRow.Item("start_date") = dtEIdRow(0).Item("start_date")
                            dtMemRow.Item("expiry_date") = dtEIdRow(0).Item("expiry_date")
                            dtMemRow.Item("remark") = dtEIdRow(0).Item("remark")
                            dtMemRow.Item("isactive") = False
                            dtMemRow.Item("AUD") = "A"
                            dtMemRow.Item("GUID") = Guid.NewGuid().ToString
                            dtMemRow.Item("ccategory") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "clsMembershipTran", 2, "Inactive")
                            dtMemRow.Item("ccategoryid") = 2
                            dtMemRow.Item("isdeleted") = False
                            dtMemRow.Item("emptrnheadid") = objMembership._ETransHead_Id
                            dtMemRow.Item("cotrnheadid") = objMembership._CTransHead_Id
                            dtMemRow.Item("effetiveperiodid") = 0
                            dtMemRow.Item("copyedslab") = False
                            dtMemRow.Item("overwriteslab") = False
                            dtMemRow.Item("overwritehead") = False

                            dtApprTran.Rows.Add(dtMemRow)
                            If objAMemInfoTran.InsertUpdateDelete_MembershipTran(dtApprTran, Nothing, CInt(Session("UserId")), CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CStr(Session("UserAccessModeSetting")), True, CBool(Session("IsIncludeInactiveEmp"))) Then


                                'Gajanan [17-April-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                If IsNothing(dtApprTran) = False AndAlso dtApprTran.Select("AUD <> ''").Length > 0 Then
                                    objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                                 CStr(Session("UserAccessModeSetting")), _
                                                                 CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                                 CInt(enUserPriviledge.AllowToApproveRejectEmployeeMemberships), _
                                                                 enScreenName.frmMembershipInfoList, CStr(Session("EmployeeAsOnDate")), _
                                                                 CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                                 Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.DELETED, , e.Item.Cells(21).Text, , , _
                                                                 "employeeunkid= " & e.Item.Cells(21).Text & " ", dtApprTran, False, clsEmployeeAddress_approval_tran.enAddressType.NONE, "")
                                End If
                                'Gajanan [17-April-2019] -- End

                                msg.DisplayMessage("Membership Successfully deleted.", Me)
                            Else
                                msg.DisplayMessage("Problem in deleting Membership.", Me)
                            End If
                        End If
                    End If
                Else
                    delete()
                End If
                Call FillGrid()
            End If
            'S.SANDEEP |15-APR-2019| -- END



            'Nilay (31-Aug-2015) -- End 

            'S.SANDEEP [31 AUG 2015] -- START
            'ENHANCEMENT : -------------- LEFT OUT CODE
            If e.CommandName.ToUpper = "ACTIVE" Then
                mintmembershiptranunkid = CInt(e.Item.Cells(11).Text)

                Dim objTHead As New clsTransactionHead
                Dim objPeriod As New clscommom_period_Tran
                Dim dsCombo As New DataSet

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "List", True, 1)
                dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), CStr(Session("Database_Name")), CDate(Session("fin_startdate")), "List", True, 1)
                'Shani(20-Nov-2015) -- End

                With cboEffectivePeriod
                    .DataValueField = "periodunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
                objPeriod = Nothing

                txtAMembership.Text = e.Item.Cells(5).Text


                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objTHead._Tranheadunkid = e.Item.Cells(16).Text
                objTHead._Tranheadunkid(CStr(Session("Database_Name"))) = CInt(e.Item.Cells(16).Text)
                'Shani(20-Nov-2015) -- End

                txtEmpHead.Text = objTHead._Trnheadname


                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objTHead._Tranheadunkid = e.Item.Cells(17).Text
                objTHead._Tranheadunkid(CStr(Session("Database_Name"))) = CInt(e.Item.Cells(17).Text)
                'Shani(20-Nov-2015) -- End

                txtCoHead.Text = objTHead._Trnheadname

                objTHead = Nothing

                chkCopyPreviousEDSlab.Checked = False
                chkOverwriteHeads.Checked = False

                'S.SANDEEP |15-APR-2019| -- START
                intEmployeeId = CInt(e.Item.Cells(21).Text)
                'S.SANDEEP |15-APR-2019| -- END


                popup_active.Show()
            End If
            'S.SANDEEP [31 AUG 2015] -- END



        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgvMembership.PageIndexChanged
        Try
            dgvMembership.CurrentPageIndex = e.NewPageIndex
            FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP [31 AUG 2015] -- START
    'ENHANCEMENT : -------------- LEFT OUT CODE
    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvMembership.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If e.Item.ItemType = ListItemType.Header Then
                e.Item.Cells(0).Visible = False
                e.Item.Cells(3).Visible = False
            End If

            If (e.Item.ItemIndex >= 0) Then

                If CBool(e.Item.Cells(12).Text) = True Then
                    e.Item.Cells(1).Text = e.Item.Cells(0).Text
                    e.Item.Cells(1).ColumnSpan = e.Item.Cells.Count - 1
                    e.Item.Cells(1).Style.Add("text-align", "left")
                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    'e.Item.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                    e.Item.Cells(1).CssClass = "group-header"
                    'Gajanan [17-Sep-2020] -- End

                    e.Item.Cells(0).Visible = False
                    For i = 2 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next

                Else

                    e.Item.Cells(0).Visible = False

                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                        dgvMembership.Columns(1).Visible = CBool(Session("EditEmployee"))
                        dgvMembership.Columns(2).Visible = CBool(Session("DeleteEmployee"))

                        If Session("ShowPending") IsNot Nothing Then
                            dgvMembership.Columns(2).Visible = False
                        End If
                    Else
                        dgvMembership.Columns(1).Visible = CBool(Session("AllowEditMembership"))
                        dgvMembership.Columns(2).Visible = CBool(Session("AllowDeleteMembership"))
                    End If
                    If CBool(e.Item.Cells(12).Text) = False Then
                        If CInt(e.Item.Cells(13).Text) = 1 Then
                            e.Item.Cells(3).Visible = False
                        ElseIf CInt(e.Item.Cells(13).Text) = 2 Then
                            CType(e.Item.Cells(1).FindControl("ImgSelect"), LinkButton).Visible = False
                            e.Item.Cells(1).ColumnSpan = 2
                            e.Item.Cells(2).Visible = False
                            'S.SANDEEP |15-APR-2019| -- START
                            'e.Item.Cells(1).Controls.Add(CType(e.Item.Cells(3).FindControl("lnkActiveMem"), LinkButton))
                            If MembershipApprovalFlowVal Is Nothing Then
                                If CInt(e.Item.Cells(18).Text) <= 0 Then e.Item.Visible = False
                            Else
                                e.Item.Cells(1).Controls.Add(CType(e.Item.Cells(3).FindControl("lnkActiveMem"), LinkButton))
                            End If
                            'S.SANDEEP |15-APR-2019| -- END
                            e.Item.Cells(3).Visible = False
                        End If
                    End If

                    'Nilay (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    'If e.Item.Cells(7).Text <> "&nbsp;" AndAlso e.Item.Cells(7).Text.ToString.Trim.Length > 0 Then
                    '    e.Item.Cells(7).Text = CStr(CDate(e.Item.Cells(7).Text).Date)
                    'End If
                    'If e.Item.Cells(8).Text <> "&nbsp;" AndAlso e.Item.Cells(8).Text.ToString.Trim.Length > 0 Then
                    '    e.Item.Cells(8).Text = CStr(CDate(e.Item.Cells(8).Text).Date)
                    'End If
                    'If e.Item.Cells(9).Text <> "&nbsp;" AndAlso e.Item.Cells(9).Text.ToString.Trim.Length > 0 Then
                    '    e.Item.Cells(9).Text = CStr(CDate(e.Item.Cells(9).Text).Date)
                    'End If
                    If e.Item.Cells(7).Text <> "&nbsp;" AndAlso e.Item.Cells(7).Text.ToString.Trim.Length > 0 Then
                        e.Item.Cells(7).Text = CDate(e.Item.Cells(7).Text).Date.ToShortDateString
                    End If
                    If e.Item.Cells(8).Text <> "&nbsp;" AndAlso e.Item.Cells(8).Text.ToString.Trim.Length > 0 Then
                        e.Item.Cells(8).Text = CDate(e.Item.Cells(8).Text).Date.ToShortDateString
                    End If
                    If e.Item.Cells(9).Text <> "&nbsp;" AndAlso e.Item.Cells(9).Text.ToString.Trim.Length > 0 Then
                        e.Item.Cells(9).Text = CDate(e.Item.Cells(9).Text).Date.ToShortDateString
                    End If
                    'Nilay (16-Apr-2016) -- End

                End If

                'S.SANDEEP |15-APR-2019| -- START
                If MembershipApprovalFlowVal Is Nothing Then
                    If CInt(e.Item.Cells(18).Text) > 0 Then
                        e.Item.BackColor = Drawing.Color.PowderBlue
                        e.Item.ForeColor = Drawing.Color.Black

                        objtblPanel.Visible = True
                        blnPendingEmployee = True

                        CType(e.Item.Cells(1).FindControl("ImgSelect"), LinkButton).Visible = False
                        If CType(CType(e.Item.Cells(1).FindControl("hfoprationtypeid"), HiddenField).Value, clsEmployeeDataApproval.enOperationType) = clsEmployeeDataApproval.enOperationType.EDITED Then
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = True
                        Else
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = False
                        End If
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = False
                        CType(e.Item.Cells(3).FindControl("lnkActiveMem"), LinkButton).Visible = False
                    End If
                End If
                'S.SANDEEP |15-APR-2019| -- END

            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvMembership.ItemDataBound
    '    Try
    '        If (e.Item.ItemIndex >= 0) Then
    '            If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '                'e.Item.Cells(0).Visible = True : e.Item.Cells(1).Visible = True

    '                'Anjan (30 May 2012)-Start
    '                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    '                'dgvMembership.Columns(0).Visible = True : dgvMembership.Columns(1).Visible = False 'Anjan (02 Mar 2012)


    '                'Pinkal (22-Nov-2012) -- Start
    '                'Enhancement : TRA Changes

    '                'dgvMembership.Columns(0).Visible = True : dgvMembership.Columns(1).Visible = Session("DeleteEmployee")

    '                'S.SANDEEP [31 AUG 2015] -- START
    '                'ENHANCEMENT : -------------- LEFT OUT CODE
    '                'dgvMembership.Columns(0).Visible = Session("EditEmployee")
    '                'dgvMembership.Columns(1).Visible = Session("DeleteEmployee")

    '                dgvMembership.Columns(1).Visible = Session("EditEmployee")
    '                dgvMembership.Columns(2).Visible = Session("DeleteEmployee")
    '                'S.SANDEEP [31 AUG 2015] -- END


    '                'Pinkal (22-Nov-2012) -- End
    '                'Anjan (30 May 2012)-End 

    '                'S.SANDEEP [ 16 JAN 2014 ] -- START
    '                If Session("ShowPending") IsNot Nothing Then
    '                    'S.SANDEEP [31 AUG 2015] -- START
    '                    'ENHANCEMENT : -------------- LEFT OUT CODE
    '                    'dgvMembership.Columns(1).Visible = False
    '                    dgvMembership.Columns(2).Visible = False
    '                    'S.SANDEEP [31 AUG 2015] -- END
    '                End If
    '                'S.SANDEEP [ 16 JAN 2014 ] -- END

    '            Else
    '                'e.Item.Cells(0).Visible = False : e.Item.Cells(1).Visible = False

    '                'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '                'ENHANCEMENT : TRA CHANGES
    '                'dgvMembership.Columns(0).Visible = True : dgvMembership.Columns(1).Visible = ConfigParameter._Object._AllowDeleteMembership


    '                'Pinkal (22-Nov-2012) -- Start
    '                'Enhancement : TRA Changes

    '                'dgvMembership.Columns(0).Visible = True : dgvMembership.Columns(1).Visible = Session("AllowDeleteMembership")

    '                'S.SANDEEP [31 AUG 2015] -- START
    '                'ENHANCEMENT : -------------- LEFT OUT CODE
    '                'dgvMembership.Columns(0).Visible = Session("AllowEditMembership")
    '                'dgvMembership.Columns(1).Visible = Session("AllowDeleteMembership")

    '                dgvMembership.Columns(1).Visible = Session("AllowEditMembership")
    '                dgvMembership.Columns(2).Visible = Session("AllowDeleteMembership")
    '                'S.SANDEEP [31 AUG 2015] -- END


    '                'Pinkal (22-Nov-2012) -- End


    '                'S.SANDEEP [ 27 APRIL 2012 ] -- END
    '            End If

    '            'S.SANDEEP [31 AUG 2015] -- START
    '            'ENHANCEMENT : -------------- LEFT OUT CODE

    '            'If e.Item.Cells(5).Text <> "&nbsp;" AndAlso e.Item.Cells(5).Text.ToString.Trim.Length > 0 Then
    '            '    e.Item.Cells(5).Text = CDate(e.Item.Cells(5).Text).Date
    '            'End If

    '            'If e.Item.Cells(6).Text <> "&nbsp;" AndAlso e.Item.Cells(6).Text.ToString.Trim.Length > 0 Then
    '            '    e.Item.Cells(6).Text = CDate(e.Item.Cells(6).Text).Date
    '            'End If

    '            'If e.Item.Cells(7).Text <> "&nbsp;" AndAlso e.Item.Cells(7).Text.ToString.Trim.Length > 0 Then
    '            '    e.Item.Cells(7).Text = CDate(e.Item.Cells(7).Text).Date
    '            'End If

    '            'S.SANDEEP [31 AUG 2015] -- END


    '        End If
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'S.SANDEEP [31 AUG 2015] -- END


    'S.SANDEEP [ 16 JAN 2014 ] -- START
    Protected Sub chkShowPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowPending.CheckedChanged
        Try
            Session("ShowPending") = chkShowPending.Checked
            Dim dsCombos As New DataSet
            Dim objEmp As New clsEmployee_Master
            If chkShowPending.Checked = True Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"), , , False)
                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                  CStr(Session("UserAccessModeSetting")), False, _
                                                  CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                'Shani(24-Aug-2015) -- End
                Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dtTab
                    .DataBind()
                    Try
                        .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                    Catch ex As Exception
                        .SelectedValue = CStr(0)
                    End Try
                End With
                BtnNew.Visible = False
            Else
                Session("ShowPending") = Nothing
                BtnNew.Visible = True
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
                    dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                                      CInt(Session("UserId")), _
                                                      CInt(Session("Fin_year")), _
                                                      CInt(Session("CompanyUnkId")), _
                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                      CStr(Session("UserAccessModeSetting")), True, _
                                                      CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                    'Shani(24-Aug-2015) -- End
                    With drpEmployee
                        .DataValueField = "employeeunkid"
                        'Nilay (09-Aug-2016) -- Start
                        'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                        '.DataTextField = "employeename"
                        .DataTextField = "EmpCodeName"
                        'Nilay (09-Aug-2016) -- End
                        .DataSource = dsCombos.Tables("Employee")
                        .DataBind()
                        Try
                            .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                        Catch ex As Exception
                            .SelectedValue = CStr(0)
                        End Try
                    End With
                Else
                    Dim objglobalassess = New GlobalAccess
                    objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                    With drpEmployee
                        .DataSource = objglobalassess.ListOfEmployee
                        .DataTextField = "loginname"
                        .DataValueField = "employeeunkid"
                        .DataBind()
                        Try
                            .SelectedValue = CStr(IIf(CInt(Session("Employeeunkid")) <= 0, 0, Session("Employeeunkid")))
                        Catch ex As Exception
                            .SelectedValue = CStr(0)
                        End Try
                    End With
                End If
            End If
            dgvMembership.DataSource = Nothing : dgvMembership.DataBind()
            'S.SANDEEP [31 AUG 2015] -- START
            'ENHANCEMENT : -------------- LEFT OUT CODE
            If Session("EmpMemdtFinalTab") IsNot Nothing Then
                Session("EmpMemdtFinalTab") = Nothing
            End If
            'S.SANDEEP [31 AUG 2015] -- END
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [ 16 JAN 2014 ] -- END

#End Region

#Region "ToolBar Event"

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try


    '        'Pinkal (22-Nov-2012) -- Start
    '        'Enhancement : TRA Changes

    '        If Session("LoginBy") = Global.User.en_loginby.User Then
    '            If CBool(Session("AddEmployee")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                Exit Sub
    '            End If
    '        Else
    '            If CBool(Session("AllowAddIdentity")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                Exit Sub
    '            End If
    '        End If

    '        'Pinkal (22-Nov-2012) -- End

    '        Response.Redirect("~\HR\wPgEmpMemberships.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region

    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbMembershipInfo", Me.Title)
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbMembershipInfo", Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbMembershipInfo", Me.lblPageHeader.Text)
            'SHANI [01 FEB 2015]--END 
            Me.lblMembershipType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMembershipType.ID, Me.lblMembershipType.Text)

            dgvMembership.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvMembership.Columns(4).FooterText, dgvMembership.Columns(4).HeaderText)
            dgvMembership.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvMembership.Columns(5).FooterText, dgvMembership.Columns(5).HeaderText)
            dgvMembership.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvMembership.Columns(6).FooterText, dgvMembership.Columns(6).HeaderText)
            dgvMembership.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvMembership.Columns(7).FooterText, dgvMembership.Columns(7).HeaderText)
            dgvMembership.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvMembership.Columns(8).FooterText, dgvMembership.Columns(8).HeaderText)
            dgvMembership.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvMembership.Columns(9).FooterText, dgvMembership.Columns(9).HeaderText)
            dgvMembership.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgvMembership.Columns(10).FooterText, dgvMembership.Columns(10).HeaderText)

            Me.lblPendingData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPendingData.ID, Me.lblPendingData.Text)
            Me.lblParentData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblParentData.ID, Me.lblParentData.Text)

            'Language.setLanguage(mstrModuleName1)
            Me.LblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.LblEmployee.ID, Me.LblEmployee.Text)
            Me.BtnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.BtnNew.ID, Me.BtnNew.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End
    End Sub

    'Pinkal (06-May-2014) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMembershipTran", 1, "Active")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 2, "You have ticked Copy Previous ED Slab. This will copy Previous Period Slab." & vbCrLf & vbCrLf & "Do you want to continue?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 42, "Sorry, You can not delete this Membership information. Reason : This Membership information in use.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsEmployeeDataApproval", 50, "Information Deleted")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 140, "Sorry, you cannot perform edit operation. Reason: Another pending operation is already present in the list")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 141, "Sorry, you cannot perform delete operation. Reason: Another pending operation is already present in the list")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, 1, "Effective Period is mandatory information. Please select Effective Period to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), "clsMembershipTran", 2, "Inactive")
        Catch Ex As Exception
            msg.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
