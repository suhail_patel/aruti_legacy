﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPgEmployeeDetail.aspx.vb"
    Inherits="HR_wPgEmployeeDetail" Title="Employee Profile" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ZoomImage.ascx" TagName="ZoomImage" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/FileUpload.ascx" TagName="FileUpload" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="der" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        $(document).ready(function () {
            BindControls();
        });

        function BindControls() {
            debugger;
        var img = document.getElementById('<%= imgEmp.ClientID %>' + '_imgZoom');
            if (img != null)
                document.onmousemove = getMouseXY;
                           var cnt = $('.flupload').length;
                        for (i = 0; i < cnt; i++) {
                            var fupld = $('.flupload')[i].id;
                            if (fupld != null)
                                fileUpLoadChange($('.flupload')[i].id);
                        }

        }
        var req = Sys.WebForms.PageRequestManager.getInstance();
            req.add_endRequest(function () {
                BindControls();
        }); 

    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_main" runat="server">
            <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-3">
                        <div class="card profile-card">
                            <div class="profile-header">
                                &nbsp;</div>
                            <div class="profile-body">
                                <div class="image-area">
                                    <%--<img src="../Ui/images/user-lg.jpg" alt="AdminBSB - Profile Image">--%>
                                    <uc8:ZoomImage ID="imgEmp" runat="server" ZoomPercentage="250" />
                                    <asp:UpdatePanel ID="UPUpload" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <uc9:FileUpload ID="flUpload" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <asp:LinkButton ID="btnRemoveImage" runat="server" ToolTip="Remove Image" CssClass="btn btn-danger btn-circle btn-profile-remove-img waves-effect waves-circle waves-float">
                                        <i class="fas fa-trash"></i>
                                    </asp:LinkButton>
                                </div>
                                <div class="content-area">
                                    <h4>
                                        <asp:Label ID="objlblEmployeeName" runat="server" Text=""></asp:Label></h4>
                                    <p>
                                        <asp:Label ID="objlblJob" runat="server" Text=""></asp:Label>
                                    </p>
                                    <p>
                                        <asp:Label ID="objlblDepartment" runat="server" Text=""></asp:Label>
                                    </p>
                                </div>
                            </div>
                            <div class="profile-footer" style="position: relative">
                                <asp:Panel ID="Panel1" runat="server" CssClass="profile-signature d--f ai--fe jc--sb">
                                    <uc8:ZoomImage ID="imgSignature" runat="server" ZoomPercentage="250" />
                                    <asp:UpdatePanel ID="UPUploadSig" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <uc9:FileUpload ID="flUploadsig" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </asp:Panel>
                                <asp:LinkButton ID="btnRemoveSig" runat="server" ToolTip="Remove signature" CssClass="btn btn-danger btn-circle btn-profile-remove-signature waves-effect waves-circle waves-float">
                                        <i class="fas fa-trash"></i>
                                </asp:LinkButton>
                                <ul>
                                    <li>
                                        <asp:Label ID="objlblmobile" runat="server" Text="Mobile No"></asp:Label>
                                        <asp:Label ID="txtmobile" runat="server" Text=""></asp:Label>
                                    </li>
                                    <li>
                                        <asp:Label ID="objlblBirthDate" runat="server" Text="Birth Date"></asp:Label>
                                        <asp:Label ID="txtBirthDate" runat="server" Text=""></asp:Label>
                                    </li>
                                    <li>
                                        <asp:Label ID="objlblAge" runat="server" Text="Age"></asp:Label>
                                        <asp:Label ID="txtAge" runat="server" Text=""></asp:Label>
                                    </li>
                                    <li>
                                        <asp:Label ID="objlblHiredon" runat="server" Text="Hired On"></asp:Label>
                                        <asp:Label ID="txtHiredon" runat="server" Text=""></asp:Label>
                                    </li>
                                    <li>
                                        <asp:Label ID="objlblYearsOfRetirement" runat="server" Text="Years to Retirement"></asp:Label>
                                        <asp:Label ID="txtYearsOfRetirement" runat="server" Text=""></asp:Label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card card-about-me">
                            <div class="body">
                                <ul>
                                    <li>
                                        <div class="title">
                                            <i class="fas fa-book"></i>
                                            <asp:Label ID="objlblQualification" runat="server" Text="Education"></asp:Label>
                                        </div>
                                        <div class="content">
                                            <asp:LinkButton ID="lnklblQualification" runat="server" Text="" CssClass="waves-effect waves-block" />
                                        </div>
                                    </li>
                                    <li>
                                        <div class="title">
                                            <i class="fas fa-briefcase"></i>
                                           <%-- 'Pinkal (09-Aug-2021)-- Start
                                            'NMB New UI Enhancements.--%>
                                            <%--<asp:Label ID="objlblWorkingStation" runat="server" Text="Working Station"></asp:Label>--%>
                                            <asp:Label ID="objlblWorkingStation" runat="server" Text="WorkStation"></asp:Label>
                                            <%--'Pinkal (09-Aug-2021)-- End--%>
                                        </div>
                                        <div class="content">
                                            <asp:Label ID="txtWorkingStation" runat="server" Text="abc"></asp:Label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="title">
                                            <i class="fas fa-user-tag"></i>
                                            <asp:Label ID="objlblMaritalStatus" runat="server" Text="Marital Status"></asp:Label>
                                        </div>
                                        <div class="content">
                                            <asp:Label ID="txtMaritalStatus" runat="server" Text=""></asp:Label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="card card-about-me">
                            <div class="body">
                                <ul>
                                    <li>
                                        <div class="title">
                                            <i class="fas fa-home"></i>
                                            <asp:Label ID="lbllnkViewAddress" runat="server" Text="Addresses"></asp:Label>
                                        </div>
                                        <div class="content">
                                            <asp:LinkButton ID="lnkViewAddress" runat="server" Text="" CssClass="waves-effect waves-block" />
                                        </div>
                                    </li>
                                    <li>
                                        <div class="title">
                                            <i class="fas fa-address-book"></i>
                                            <asp:Label ID="lbllnkViewEmergency" runat="server" Text="Emergency Contacts"></asp:Label>
                                        </div>
                                        <div class="content">
                                            <asp:LinkButton ID="lnkViewEmergency" runat="server" Text="" CssClass="waves-effect waves-block" />
                                        </div>
                                    </li>
                                    <li>
                                        <div class="title">
                                            <i class="fas fa-id-badge"></i>
                                            <asp:Label ID="lbllnkViewPersonal" runat="server" Text="Birth Information"></asp:Label>
                                        </div>
                                        <div class="content">
                                            <asp:LinkButton ID="lnkViewPersonal" runat="server" Text="" CssClass="waves-effect waves-block" />
                                        </div>
                                    </li>
                                    <asp:Panel ID="pnlNominateEmp" runat="server">
                                     <li>
                                        <div class="title">
                                             <i class="fas fa-user-tag"></i>
                                            <asp:Label ID="lbllnkNominateEmp" runat="server" Text="Nominate as a Successor"></asp:Label>
                                        </div>
                                        <div class="content">
                                                <asp:LinkButton ID="lnkNominateEmp" runat="server" Text="Nominate as a Successor"
                                                    CssClass="waves-effect waves-block" />
                                        </div>
                                    </li>
                                    </asp:Panel>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <div class="card">
                            <div class="body">
                                <div>
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#Employee_Detail" aria-controls="Employee_Detail"
                                            role="tab" data-toggle="tab">Employee Detail</a></li>
                                        <li role="presentation"><a href="#Dependents" aria-controls="settings" role="tab"
                                            data-toggle="tab">Dependents</a></li>
                                        <li role="presentation"><a href="#Employee_Dates" aria-controls="settings" role="tab"
                                            data-toggle="tab">Employee Dates</a></li>
                                        <li role="presentation"><a href="#Employee_Allocation" aria-controls="settings" role="tab"
                                            data-toggle="tab">Employee Allocation</a></li>
                                        <li role="presentation"><a href="#MyReporting" aria-controls="settings" role="tab"
                                            data-toggle="tab">My Reporting</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="Employee_Detail">
                                            <div class="card inner-card">
                                                <div class="body">
                                                    <div class="row clearfix">
                                                        <asp:Panel ID="pnlCombo" runat="server">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:CheckBox ID="chkShowPending" runat="server" AutoPostBack="true" Text="Display Pending Employee(s)" />
                                                            </div>
                                                            <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server" AutoPostBack="True">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </asp:Panel>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblEmployeeCode" runat="server" Text="Code" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtCodeValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblTitle" runat="server" Text="Title" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList data-live-search="true" ID="cboTitle" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblFirstName" runat="server" Text="First Name" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtFirstValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblSurname" runat="server" Text="Surname" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtSurnameValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblOthername" runat="server" Text="Other Name" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtOtherNameValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblGender" runat="server" Text="Gender" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtGenderValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblEmploymentType" runat="server" Text="Employment Type" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtEmploymentTypeValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblPaypoint" runat="server" Text="Pay Point" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtPayPointValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblIdNo" runat="server" Text="Identity No" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtIdValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblShift" runat="server" Text="Shift" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtShiftValue" runat="server" ReadOnly="true" Text="" CssClass="form-control"></asp:TextBox></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblPayType" runat="server" Text="Pay Type" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtPayValue" runat="server" ReadOnly="true" CssClass="form-control"
                                                                        Text=""></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblEmail" runat="server" Text="Company Email" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtcoEmail" runat="server" Text="" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <asp:Button ID="btnsave" runat="server" CssClass="btn btn-primary" Text="Update" />
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade in" id="Dependents">
                                            <div class="card inner-card">
                                                <div class="body">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="table-responsive">
                                                                <asp:DataGrid ID="dgvDependantList" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered"
                                                                    AllowPaging="false">
                                                                    <Columns>
                                                                        <asp:BoundColumn DataField="DpndtBefName" HeaderText="Name" ReadOnly="true" FooterText="colhDBName">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Relation" HeaderText="Relation" ReadOnly="true" FooterText="colhRelation">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="Gender" HeaderText="Gender" ReadOnly="true" FooterText="colhGender">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="birthdate" HeaderText="Birthdate" ReadOnly="true" FooterText="colhbirthdate">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="IdNo" HeaderText="Identify No." ReadOnly="true" FooterText="colhIdNo">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="EmpId" HeaderText="Employee ID" ReadOnly="true" Visible="false">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" FooterText="colhOperationType">
                                                                        </asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="DpndtTranId" HeaderText="DpndtTranId" Visible="false"
                                                                            FooterText="objdgcolhRefreeTranId"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="isactive" HeaderText="Is Active" Visible="false" FooterText="colhIsActive">
                                                                        </asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <asp:Button ID="btnManageDependant" runat="server" CssClass="btn btn-primary" Text="Manage Dependant" />
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade in" id="Employee_Dates">
                                            <div class="card inner-card">
                                                <div class="body">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblAppointDate" runat="server" Text="Appoint Date" CssClass="form-label"></asp:Label>
                                                            <uc2:DateCtrl ID="dtpAppointDate" runat="server" AutoPostBack="false" />
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblConfirmationDate" runat="server" Text="Conf. Date" CssClass="form-label"></asp:Label>
                                                            <uc2:DateCtrl ID="dtpConfDate" runat="server" AutoPostBack="false" />
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblBirtdate" runat="server" Text="Birthdate" CssClass="form-label"></asp:Label>
                                                            <uc2:DateCtrl ID="dtpBirthdate" runat="server" AutoPostBack="false" />
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblRetirementDate" runat="server" Text="Retr. Date" CssClass="form-label"></asp:Label>
                                                            <uc2:DateCtrl ID="dtpRetrDate" runat="server" AutoPostBack="false" />
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblSuspendedFrom" runat="server" Text="Susp. From" CssClass="form-label"></asp:Label>
                                                            <uc2:DateCtrl ID="dtpSuspFrom" runat="server" AutoPostBack="false" />
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblSuspendedTo" runat="server" Text="Susp. To" CssClass="form-label"></asp:Label>
                                                            <uc2:DateCtrl ID="dtpSuspTo" runat="server" AutoPostBack="false" />
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblProbationFrom" runat="server" Text="Prob. From" CssClass="form-label"></asp:Label>
                                                            <uc2:DateCtrl ID="dtpProbFrom" runat="server" AutoPostBack="false" />
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblProbationTo" runat="server" Text="Prob. To" CssClass="form-label"></asp:Label>
                                                            <uc2:DateCtrl ID="dtpProbTo" runat="server" AutoPostBack="false" />
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblEmplDate" runat="server" Text="EOC Date" CssClass="form-label"></asp:Label>
                                                            <uc2:DateCtrl ID="dtpEmplEndDate" runat="server" AutoPostBack="false" />
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblLeavingDate" runat="server" Text="Leaving Date" CssClass="form-label"></asp:Label>
                                                            <uc2:DateCtrl ID="dtpLeavingDate" runat="server" AutoPostBack="false" />
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblEmplReason" runat="server" Text="EOC Reason" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboReason" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblReinstatementDate" runat="server" Text="Reinstatement Date" CssClass="form-label"></asp:Label>
                                                            <uc2:DateCtrl ID="dtpReinstatement" runat="server" AutoPostBack="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade in" id="Employee_Allocation">
                                            <div class="card inner-card">
                                                <div class="body">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblBranch" runat="server" Text="Branch" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboBranch" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblJobGroup" runat="server" Text="Job Group" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboJobGroup" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblDepartmentGroup" runat="server" Text="Dept. Group" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboDeptGrp" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblJob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboJob" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboDepartment" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblScale" runat="server" Text="Scale" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtScale" runat="server" Style="text-align: right" ReadOnly="True"
                                                                        CssClass="form-control"></asp:TextBox></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblSectionGroup" runat="server" Text="Section Group" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboSecGroup" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblClassGroup" runat="server" Text="Class Group" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboClassGrp" runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblSection" runat="server" Text="Section" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboSection" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblClass" runat="server" Text="Class" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboClass" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblUnitGroup" runat="server" Text="Unit Group" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboUnitGrp" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblGradeGroup" runat="server" Text="Grade Group" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboGradeGrp" runat="server" AutoPostBack="True" Enabled="False">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblUnits" runat="server" Text="Unit" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboUnit" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Panel ID="pnlgrade" runat="server">
                                                                <asp:Label ID="lblGrade" runat="server" Text="Grade" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboGrade" runat="server" AutoPostBack="True" Enabled="False">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblTeam" runat="server" Text="Team" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboTeam" runat="server" AutoPostBack="True">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                            <asp:Panel ID="pnlgradegrp" runat="server">
                                                                <asp:Label ID="lblGradeLevel" runat="server" Text="Grade Level" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboGradeLevel" runat="server" AutoPostBack="True" Enabled="False">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <asp:DropDownList ID="cboCostcenter" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade in" id="MyReporting">
                                            <div class="card inner-card">
                                                <div class="body">
                                                    <div class="row clearfix">
                                                        <div class="col-xs-12 ol-sm-12 col-md-12 col-lg-12">
                                                            <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
                                                                <asp:Panel ID="pnl_ViewReportingTo" runat="server" Visible="true" CssClass="panel panel-default">
                                                                    <div class="panel-heading" role="tab" id="headingThree_1">
                                                                        <h4 class="panel-title">
                                                                            <a role="button" data-toggle="collapse" data-parent="#accordion_1" href="#collapseThree_1"
                                                                                aria-expanded="true" aria-controls="collapseThree_1">
                                                                                <asp:Label ID="lbllnkViewReportingTo" runat="server" Text="Reporting To" CssClass="form-label"></asp:Label>
                                                                            </a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseThree_1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree_1">
                                                                        <div class="panel-body">
                                                                            <asp:Panel ID="pnlGvReportingTo" ScrollBars="Auto" Style="max-height: 400px" runat="server">
                                                                                <asp:GridView ID="GvReportingTo" runat="server" CssClass="table table-hover table-bordered"
                                                                                    AllowPaging="false">
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnl_ViewLeaveApprover" runat="server" Visible="true" CssClass="panel panel-default">
                                                                    <div class="panel-heading" role="tab" id="headingTwo_1">
                                                                        <h4 class="panel-title">
                                                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1"
                                                                                href="#collapseTwo_1" aria-expanded="false" aria-controls="collapseTwo_1">
                                                                                <asp:Label ID="lblViewLeaveApprover" runat="server" Text="Leave Approver" CssClass="form-label"></asp:Label>
                                                                            </a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseTwo_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_1">
                                                                        <div class="panel-body">
                                                                            <asp:Panel ID="pnlGvLeaveApprover" ScrollBars="Auto" Style="max-height: 400px" runat="server">
                                                                                <asp:GridView ID="GvLeaveApprover" runat="server" CssClass="table table-hover table-bordered"
                                                                                    AllowPaging="false">
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnl_ViewClaimApprover" runat="server" Visible="true" CssClass="panel panel-default">
                                                                    <div class="panel-heading" role="tab" id="headingFour_1">
                                                                        <h4 class="panel-title">
                                                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1"
                                                                                href="#collapseFour_1" aria-expanded="false" aria-controls="collapseFour_1">
                                                                                <asp:Label ID="lbllnkViewClaimApprover" runat="server" Text="Claim Request Approver"
                                                                                    CssClass="form-label"></asp:Label>
                                                                            </a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseFour_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour_1">
                                                                        <div class="panel-body">
                                                                            <asp:Panel ID="pnlGvClaimApprover" ScrollBars="Auto" Style="max-height: 400px" runat="server">
                                                                                <asp:GridView ID="GvClaimApprover" runat="server" CssClass="table table-hover table-bordered"
                                                                                    AllowPaging="false">
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnl_ViewOTApprover" runat="server" Visible="true" CssClass="panel panel-default">
                                                                    <div class="panel-heading" role="tab" id="headingFive_1">
                                                                        <h4 class="panel-title">
                                                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1"
                                                                                href="#collapseFive_1" aria-expanded="false" aria-controls="collapseFive_1">
                                                                                <asp:Label ID="lbllnkViewOTApprover" runat="server" Text="OT Requisition Approver"
                                                                                    CssClass="form-label"></asp:Label>
                                                                            </a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseFive_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive_1">
                                                                        <div class="panel-body">
                                                                            <asp:Panel ID="pnlGvOTApprover" ScrollBars="Auto" Style="max-height: 400px" runat="server">
                                                                                <asp:GridView ID="GvOTApprover" runat="server" CssClass="table table-hover table-bordered"
                                                                                    AllowPaging="false">
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                                <asp:Panel ID="pnl_ViewAssessorReviewer" runat="server" Visible="true" CssClass="panel panel-default">
                                                                    <div class="panel-heading" role="tab" id="headingOne_1">
                                                                        <h4 class="panel-title">
                                                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_1"
                                                                                href="#collapseOne_1" aria-expanded="false" aria-controls="collapseOne_1">
                                                                                <asp:Label ID="lbllnkViewAssessor" runat="server" Text="Assessor/Reviewer" CssClass="form-label"></asp:Label>
                                                                            </a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseOne_1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_1">
                                                                        <div class="panel-body">
                                                                            <asp:Panel ID="pnlGvAssessorReviewer" ScrollBars="Auto" Style="max-height: 400px"
                                                                                runat="server">
                                                                                <asp:GridView ID="GvAssessorReviewer" runat="server" CssClass="table table-hover table-bordered"
                                                                                    AllowPaging="false">
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                        <asp:Panel ID="pnlEmployeeQuickLinks" runat="server" Visible="false">
                            <div class="row clearfix">
                                <asp:LinkButton ID="lnkMyQualification" runat="server" class=" col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">My Qualifications</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-user-graduate col-green"></i>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkMySkills" runat="server" class=" col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">My Skills</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-user-cog col-pink"></i>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkMyIdentities" runat="server" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">My Identities</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-address-card col-purple"></i>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkMyMemberships" runat="server" class=" col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">My Memberships</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-id-card-alt col-green"></i>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                            <div class="row clearfix">
                                <asp:LinkButton ID="lnkMyExperiences" runat="server" class=" col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">My Experiences</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-user-tie col-blue"></i>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkMyReferences" runat="server" class="col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">My References</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-users col-teal"></i>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkMyBenefits" runat="server" class=" col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">My Benefits</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-diagnoses col-orange"></i>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lnkMyAssignedAssets" runat="server" class=" col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">My Assigned Assets</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-clipboard-list col-blue-grey"></i>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                            <div class="row clearfix">
                                <asp:LinkButton ID="lnkMyDependent" runat="server" class=" col-lg-3 col-md-3 col-sm-6 col-xs-12 quick-links sm">
                                    <div class="info-box-4 hover-expand-effect">
                                        <div class="content">
                                            <div class="text">My Dependants</div>
                                        </div>
                                         <div class="icon">
                                            <i class="fas fa-people-arrows col-orange"></i>
                                        </div>
                                    </div>
                                </asp:LinkButton>
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupNominateEmployee" runat="server" CancelControlID="hfNominateEmployee"
                    PopupControlID="pnlNominateEmployee" TargetControlID="lblNominateEmployeeHeader"
                    Drag="true" PopupDragHandleControlID="pnlNominateEmployee" BackgroundCssClass="modal-backdrop">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlNominateEmployee" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblNominateEmployeeHeader" runat="server" Text="Nominate Employee" />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 400px">
                        <div class="inner-card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblNominateEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtNominateEmployee" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblNominateEmployeeJob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpNominateEmployeeJob" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNominateEmployeeSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <div class="inner-card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 250px">
                                            <asp:GridView ID="dgvNominateEmployee" runat="server" AutoGenerateColumns="false"
                                                CssClass="table table-hover table-bordered" AllowPaging="false" DataKeyNames="nominationunkid">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkNominateEmployeeDelete" runat="server" ToolTip="Delete" OnClick="lnkNominateEmployeeDelete_Click">
                                                                        <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="job_name" HeaderText="Job Name" ReadOnly="True" FooterText="colhYear">
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnNominateEmployeeClose" runat="server" Text="Cancel" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hfNominateEmployee" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupNominationCancleReason" runat="server" PopupControlID="pnlNominationCancleReason"
                    TargetControlID="hfNominationCancleReason" Drag="true" PopupDragHandleControlID="pnlNominationCancleReason"
                    BackgroundCssClass="modal-backdrop bd-l2">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlNominationCancleReason" runat="server" CssClass="card modal-dialog modal-l2"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="Label2" runat="server" Text="Succession Qualifying Criteria Info." />
                        </h2>
                    </div>
                    <div class="body" style="max-height: 400px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <ul class="list-group">
                                    <li class="list-group-item">Is Qualification Level Matched?
                                        <asp:Label ID="lblQualificationNominationCheckTrue" Visible="false" runat="server"
                                            Text="" CssClass="fas fa-check-circle text-success pull-right" />
                                        <asp:Label ID="lblQualificationNominationCheckFalse" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-times-circle text-danger pull-right" />
                                    </li>
                                    <li class="list-group-item">Is Minimum Number of Years with Organization Matched?
                                        <asp:Label ID="lblExpNominationCheckTrue" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-check-circle text-success pull-right" />
                                        <asp:Label ID="lblExpNominationCheckFalse" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-times-circle text-danger pull-right" /></li>
                                    <li class="list-group-item">Is Minimum Performance Score Matched?
                                        <asp:Label ID="lblPerfNominationCheckTrue" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-check-circle text-success pull-right" />
                                        <asp:Label ID="lblPerfNominationCheckFalse" runat="server" Text="" Visible="false"
                                            CssClass="fas fa-times-circle text-danger pull-right" /></li>
                                     <li class="list-group-item">Is Minimum Age Matched?
                                        <asp:Label ID="lblMinimumAgeCheckTrue" runat="server" Text="" Visible="false" CssClass="fas fa-check-circle text-success pull-right" />
                                        <asp:Label ID="lblMinimumAgeCheckFalse" runat="server" Text="" Visible="false" CssClass="fas fa-times-circle text-danger pull-right" /></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnNominationCancleReasonClose" runat="server" Text="Cancel" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hfNominationCancleReason" runat="server" />
                    </div>
                </asp:Panel>
                <der:DelReason ID="delReason" runat="server" Title="Aruti" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
