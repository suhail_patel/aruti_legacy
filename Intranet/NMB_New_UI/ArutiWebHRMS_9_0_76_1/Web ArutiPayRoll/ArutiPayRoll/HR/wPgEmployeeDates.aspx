﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPgEmployeeDates.aspx.vb"
    Inherits="wPgEmployeeDates" Title="Employee Dates" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="uppnl_main" runat="server">
        <ContentTemplate>
            <div class="row clearfix d--f jc--c">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblpnl_Header" runat="server" Text="Employee Date Detail"></asp:Label>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <asp:TextBox ID="txtEmployee" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblAppointDate" runat="server" Text="Appoint Date" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpAppointDate" runat="server" AutoPostBack="false" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblConfirmationDate" runat="server" Text="Conf. Date" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpConfDate" runat="server" AutoPostBack="false" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblBirtdate" runat="server" Text="Birthdate" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpBirthdate" runat="server" AutoPostBack="false" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblRetirementDate" runat="server" Text="Retr. Date" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpRetrDate" runat="server" AutoPostBack="false" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblSuspendedFrom" runat="server" Text="Susp. From" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpSuspFrom" runat="server" AutoPostBack="false" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblSuspendedTo" runat="server" Text="Susp. To" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpSuspTo" runat="server" AutoPostBack="false" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblProbationFrom" runat="server" Text="Prob. From" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpProbFrom" runat="server" AutoPostBack="false" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblProbationTo" runat="server" Text="Prob. To" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpProbTo" runat="server" AutoPostBack="false" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblEmplDate" runat="server" Text="EOC Date" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpEmplEndDate" runat="server" AutoPostBack="false" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblLeavingDate" runat="server" Text="Leaving Date" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpLeavingDate" runat="server" AutoPostBack="false" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblEmplReason" runat="server" Text="EOC Reason" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboReason" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblReinstatementDate" runat="server" Text="Reinstatement Date" CssClass="form-label"></asp:Label>
                                    <uc2:DateCtrl ID="dtpReinstatement" runat="server" AutoPostBack="false" />
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnSave" runat="server" Text="Update" CssClass="btn btn-primary" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
