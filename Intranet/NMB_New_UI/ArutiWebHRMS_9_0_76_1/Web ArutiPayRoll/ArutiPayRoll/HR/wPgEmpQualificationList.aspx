﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPgEmpQualificationList.aspx.vb"
    Inherits="HR_wPgEmpQualificationList" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/ViewEmployeeDataApproval.ascx" TagName="Pop_report"
    TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Qualifications List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee : " CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblQualificationGroup" runat="server" Text="Qualification/Award Group :"
                                            CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboQGrp" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblAwardDate" runat="server" Text="Start Date :" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtAwarddtFrom" runat="server" AutoPostBack="false" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblTo" runat="server" Text="Award Date :" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtAwarddtTo" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblInstitute" runat="server" Text="Institute :" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboInstitute" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblQualification" runat="server" Text="Qualification/Award :" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList data-live-search="true" ID="cboQualification" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReferenceNo" runat="server" Text="Ref. No :" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtRefNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12  m-t-35">
                                        <asp:CheckBox ID="chkOtherQualification" runat="server" AutoPostBack="true" Text="Other Qualification / Short Course" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOtherQualificationGrp" runat="server" Text="Other Qualif. Group "
                                            CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtOtherQGrp" runat="server" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOtherQualification" runat="server" Text="Other Qualif." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtOtherQualification" runat="server" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOtherInstitute" runat="server" Text="Other Inst." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtOtherInstitute" runat="server" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" CssClass="btn btn-primary" Text="New" Visible="False" />
                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-default" Text="Search" />
                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                <asp:Panel ID="objtblPanel" runat="server" CssClass="pull-left">
                                    <asp:Button ID="btnApprovalinfo" runat="server" CssClass="btn btn-primary" Text="View Detail" />
                                    <asp:Label ID="lblPendingData" runat="server" Text="Pending Approval" CssClass="label label-primary bg-pw"></asp:Label>
                                    <asp:Label ID="lblParentData" runat="server" Text="Parent Detail" CssClass="label label-danger bg-lc"></asp:Label>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="table-responsive" style="max-height: 300px">
                                    <asp:DataGrid ID="dgView" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="EditImg" runat="server" CommandName="Select" ToolTip="Edit">
                                            <i class="fas fa-pencil-alt text-primary"></i>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="imgDetail" runat="server" ToolTip="Highlight Original Details"
                                                        CommandName="View" Visible="false"><i class="fas fa-eye text-primary"></i> </asp:LinkButton>
                                                    <asp:HiddenField ID="hfoprationtypeid" runat="server" Value='<%#Eval("operationtypeid") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--<0>--%>
                                            <asp:TemplateColumn>
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete">
                                            <i class="fas fa-trash text-danger"></i>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--<1>--%>
                                            <asp:BoundColumn HeaderText="Employee" ReadOnly="True" DataField="EmpName" FooterText="colhEmployee">
                                            </asp:BoundColumn>
                                            <%--<2>--%>
                                            <asp:BoundColumn HeaderText="Qualification Group" ReadOnly="True" DataField="QGrp"
                                                FooterText="colhQualifyGroup"></asp:BoundColumn>
                                            <%--<3>--%>
                                            <asp:BoundColumn HeaderText="Qualification" ReadOnly="True" DataField="Qualify" FooterText="colhQualification">
                                            </asp:BoundColumn>
                                            <%--<4>--%>
                                            <asp:BoundColumn HeaderText="Date" ReadOnly="True" DataField="StDate" FooterText="colhAwardDate">
                                            </asp:BoundColumn>
                                            <%--<5>--%>
                                            <asp:BoundColumn HeaderText="End Date" ReadOnly="True" DataField="EnDate" FooterText="colhAwardToDate">
                                            </asp:BoundColumn>
                                            <%--<6>--%>
                                            <asp:BoundColumn HeaderText="Institute" ReadOnly="True" DataField="Institute" FooterText="colhInstitute">
                                            </asp:BoundColumn>
                                            <%--<7>--%>
                                            <asp:BoundColumn HeaderText="Ref. No" ReadOnly="True" DataField="RefNo" FooterText="colhRefNo">
                                            </asp:BoundColumn>
                                            <%--<8>--%>
                                            <asp:BoundColumn HeaderText="QualifyId" ReadOnly="True" DataField="QualifyId" Visible="false">
                                            </asp:BoundColumn>
                                            <%--<9>--%>
                                            <asp:BoundColumn DataField="tranguid" HeaderText="tranguid" Visible="false" FooterText="objdgcolhtranguid">
                                            </asp:BoundColumn>
                                            <%--<10>--%>
                                            <asp:BoundColumn DataField="EmpId" HeaderText="EmpId" Visible="false" FooterText="objdgcolhempid">
                                            </asp:BoundColumn>
                                            <%--<11>--%>
                                            <%--'Gajanan [17-DEC-2018] -- Start--%>
                                            <%--'Enhancement - Implementing Employee Approver Flow On Employee Data.--%>
                                            <asp:BoundColumn DataField="QulifyTranId" HeaderText="QulifyTranId" Visible="false"
                                                FooterText="objdgcolhRefreeTranId"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="OperationType" HeaderText="Operation Type" FooterText="colhOperationType">
                                            </asp:BoundColumn>
                                            <%--<12>--%>
                                            <%--'Gajanan [17-DEC-2018] -- End--%>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <uc4:DeleteReason ID="DeleteReason1" runat="server" Title="Are you sure you want to delete?"
                    ErrorMessage="Sorry! Reason can not be blank. Please enter delete reason." ErrorControlWidth="300" />
                <uc3:Pop_report ID="Popup_Viewreport" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
