﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgDependantStatus.aspx.vb" Inherits="HR_wPgDependantStatus" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/CommonValidationList.ascx" TagName="CommonValidationList"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:Panel ID="pnlData" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                            <asp:Label ID="lblPageHeader" runat="server" Text="Employee Dependants List"></asp:Label>
                                </h2>
                        </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                    </div>
                                </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGender" runat="server" Text="Gender" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                                <asp:DropDownList ID="cboGender" runat="server">
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblBirthdate" runat="server" Text="Birtdate" CssClass="form-label"></asp:Label>
                                                <uc2:DateCtrl ID="dtpBirthdate" runat="server" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label>
                                        <div class="form-group">
                                                <asp:DropDownList ID="cboStatus" runat="server">
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDependant" runat="server" Text="Dependant" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                                <asp:DropDownList ID="cboDependant" runat="server">
                                                </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBRelation" runat="server" Text="Relation" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                                <asp:DropDownList ID="cboRelation" runat="server">
                                                </asp:DropDownList>
                                    </div>
                                </div>    
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDBIdNo" runat="server" Text="ID. No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtIdNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left">
                                        <asp:Label ID="lblEffectiveDate" runat="server" Text="Effective Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpEffectiveDate" runat="server" Width="100" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-left">
                                        <asp:Label ID="lblReason" runat="server" Text="Reason" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboReason" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Button ID="BtnSetActive" runat="server" CssClass="btn btn-primary" Text="Set Active" />
                                        <asp:Button ID="BtnSetInactive" runat="server" CssClass="btn btn-primary" Text="Set Inactive"
                                            Visible="false" />
                                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-default" Text="Search" />
                                        <asp:Button ID="BtnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                        <asp:Button ID="BtnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive">
                                            <asp:DataGrid ID="dgvDependantList" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                AllowPaging="false">
                                        <Columns>
                                                    <asp:BoundColumn DataField="IsChecked" HeaderText="IsCheck" Visible="false"></asp:BoundColumn>
                                                    <%--0--%>
                                             <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-HorizontalAlign="Center" FooterText="objdgcolhSelect">
                                                    <HeaderTemplate>
                                                            <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" Text=" " OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Text=" " OnCheckedChanged="chkSelect_CheckedChanged" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="30px" />
                                                    <ItemStyle HorizontalAlign="Center" Width="30px" />
                                                    </asp:TemplateColumn>
                                                    <%--1--%>
                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <ItemStyle HorizontalAlign="Center" />
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="ImgDelete" runat="server" CssClass="griddelete" ToolTip="Delete"
                                                            CommandName="Delete"></asp:LinkButton>
                                                    </span>                                                   
                                                </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%-- 2--%>
                                             <asp:BoundColumn DataField="DpndtBefName" HeaderText="Name" ReadOnly="true" FooterText="dgcolhDependant">
                                                    </asp:BoundColumn>
                                                    <%-- 3--%>
                                            <asp:BoundColumn DataField="Relation" HeaderText="Relation" ReadOnly="true" FooterText="dgcolhRelation">
                                                    </asp:BoundColumn>
                                                    <%-- 4--%>
                                            <asp:BoundColumn DataField="Gender" HeaderText="Gender" ReadOnly="true" FooterText="dgcolhGender">
                                                    </asp:BoundColumn>
                                                    <%-- 5--%>
                                            <asp:BoundColumn DataField="dtbirthdate" HeaderText="Birthdate" ReadOnly="true" FooterText="dgcolhBirthDate">
                                                    </asp:BoundColumn>
                                                    <%-- 6--%>
                                                    <asp:BoundColumn DataField="ROWNO" HeaderText="" ReadOnly="true" FooterText="objdgcolhROWNO"
                                                        Visible="false"></asp:BoundColumn>
                                                    <%-- 7--%>
                                            <asp:BoundColumn DataField="EmpId" HeaderText="Employee ID" ReadOnly="true" Visible="false">
                                                    </asp:BoundColumn>
                                                    <%-- 8--%>
                                                    <asp:BoundColumn DataField="dpndtbeneficestatustranunkid" HeaderText="tranguid" Visible="false"
                                                        FooterText="objdgcolhDependandstatustranunkid"></asp:BoundColumn>
                                                    <%-- 9--%>
                                            <asp:BoundColumn DataField="DpndtTranId" HeaderText="DpndtTranId" Visible="false"
                                                        FooterText="objdgcolhRefreeTranId"></asp:BoundColumn>
                                                    <%-- 10--%>
                                            <asp:BoundColumn DataField="dteffective_date" HeaderText="Effective Date" Visible="true"
                                                        FooterText="dgcolhEffectiveDate"></asp:BoundColumn>
                                                    <%-- 11--%>
                                                    <asp:BoundColumn DataField="isactive" HeaderText="Is Active" Visible="true" FooterText="dgcolhActive">
                                                    </asp:BoundColumn>
                                                    <%-- 12--%>
                                                    <asp:BoundColumn DataField="Reason" HeaderText="Reason" Visible="true" FooterText="dgcolhReason">
                                                    </asp:BoundColumn>
                                                    <%-- 13--%>
                                                    <asp:BoundColumn DataField="isapproved" Visible="false"></asp:BoundColumn>
                                                    <%-- 14--%>
                                        </Columns>
                                    </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                            </div>
                            </div>
                        </div>
                    </div>
                    <ucDel:DeleteReason ID="popupDeleteReason" runat="server" />
                    <uc4:CommonValidationList ID="popupValidationList" runat="server" Message="" ShowYesNo="false" />
                    <asp:HiddenField ID="hd1" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
</asp:Content>
