﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region

Partial Class wPg_EmployeeAsset
    Inherits Basepage

#Region " Private Variables "

    Dim DisplayMessage As New CommonCodes
    Private objAsset As clsEmployee_Assets_Tran
    Private mintEmpAssetTranUnkid As Integer = -1
    Private mintSelectedEmployee As Integer = -1


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployeeAssets_AddEdit"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Page's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            objAsset = New clsEmployee_Assets_Tran


            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            Blank_ModuleName()
            objAsset._WebFormName = "frmEmployeeAssets_AddEdit"
            StrModuleName2 = "mnuPersonnel"
            StrModuleName3 = "mnuEmployeeData"
            objAsset._WebClientIP = CStr(Session("IP_ADD"))
            objAsset._WebHostName = CStr(Session("HOST_NAME"))
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                objAsset._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            End If

            'S.SANDEEP [ 04 JULY 2012 ] -- END


            'Pinkal (06-May-2014) -- Start
            'Enhancement : Language Changes 
            SetLanguage()
            'Pinkal (06-May-2014) -- End

            Dim clsuser As New User
            clsuser = CType(Session("clsuser"), User)
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'Sohail (30 Mar 2015) -- Start
                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                'Me.ViewState("Empunkid") = clsuser.UserID
                Me.ViewState("Empunkid") = Session("UserId")
                'Sohail (30 Mar 2015) -- End
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Aruti.Data.User._Object._Userunkid = Session("UserId")
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                'Anjan (30 May 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                btnSaveInfo.Visible = CBool(Session("EditEmployeeAssets"))
                'Anjan (30 May 2012)-End 

            Else
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Aruti.Data.User._Object._Userunkid = -1
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
                'Sohail (30 Mar 2015) -- Start
                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                Me.ViewState("Empunkid") = Session("Employeeunkid")
                'Sohail (30 Mar 2015) -- End
            End If

            If (Page.IsPostBack = False) Then
                FillCombo()
                dtpdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("Asset_EmpUnkID") IsNot Nothing Then
                    drpEmployee.SelectedValue = CStr(Session("Asset_EmpUnkID"))
                End If
                'SHANI [09 Mar 2015]--END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'Nilay (01-Feb-2015) -- End
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    ToolbarEntry1.VisibleSaveButton(False)
    '    ToolbarEntry1.VisibleDeleteButton(False)
    '    ToolbarEntry1.VisibleCancelButton(False)
    '    ToolbarEntry1.VisibleNewButton(False)
    '    ToolbarEntry1.VisibleModeFormButton(False)
    '    ToolbarEntry1.VisibleExitButton(False)
    'End Sub
    'Nilay (01-Feb-2015) -- End



    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Methods "

    Private Sub GetValue()
        Try
            txtAssetno.Text = objAsset._Asset_No
            TxtRemark.Text = objAsset._Remark
            drpAsset.SelectedValue = CStr(objAsset._Assetunkid)
            drpCondition.SelectedValue = CStr(objAsset._Conditionunkid)
            drpEmployee.SelectedValue = CStr(objAsset._Employeeunkid)
            If objAsset._Assign_Return_Date <> Nothing Then
                dtpdate.SetDate = objAsset._Assign_Return_Date.Date
            End If
            TxtSerialno.Text = objAsset._Assetserial_No

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objAsset._Asset_No = txtAssetNo.Text
            objAsset._Remark = TxtRemark.Text
            objAsset._Assetunkid = CInt(drpAsset.SelectedValue)
            objAsset._Conditionunkid = CInt(drpCondition.SelectedValue)
            objAsset._Employeeunkid = CInt(drpEmployee.SelectedValue)

            ' If menAction = enAction.EDIT_ONE Then
            'objAsset._Statusunkid = objAsset._Statusunkid
            'Else
            objAsset._Statusunkid = enEmpAssetStatus.Assigned
            ' End If



            'Pinkal (12-Nov-2012) -- Start
            'Enhancement : TRA Changes
            'objAsset._Assign_Return_Date = dtpdate.GetDate.Date

            If dtpdate.IsNull = True Then
                objAsset._Assign_Return_Date = Nothing
            Else
                objAsset._Assign_Return_Date = dtpdate.GetDate.Date
            End If

            'Pinkal (12-Nov-2012) -- End


            If mintEmpAssetTranUnkid = -1 Then
                objAsset._Isvoid = False
                objAsset._Voiddatetime = Nothing
                objAsset._Voidreason = ""
                objAsset._Voiduserunkid = -1
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'objAsset._Userunkid = 1
                objAsset._Userunkid = CInt(Session("UserId"))
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
            Else
                objAsset._Isvoid = objAsset._Isvoid
                objAsset._Voiddatetime = objAsset._Voiddatetime
                objAsset._Voidreason = objAsset._Voidreason
                objAsset._Voiduserunkid = objAsset._Voiduserunkid
                objAsset._Userunkid = objAsset._Userunkid
            End If
            objAsset._Assetserial_No = TxtSerialno.Text
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objCommon As New clsCommon_Master
        Dim objEmp As New clsEmployee_Master
        Dim dsList As New DataSet
        Dim objMaster As New clsMasterData
        Try
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ASSET_CONDITION, True, "Condition")
            With drpCondition
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Condition")
                .DataBind()
            End With

            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ASSETS, True, "Assets")
            With drpAsset
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Assets")
                .DataBind()
            End With

            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

                Dim objEmployee As New clsEmployee_Master

                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsList = objEmployee.GetEmployeeList("Employee", True, True, , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsList = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsList = objEmp.GetEmployeeList("Employee", True, )
                '    dsList = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END                    
                'End If

                'Shani(24-Aug-2015) -- End
                dsList = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            CStr(Session("UserAccessModeSetting")), True, _
                                            CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsList.Tables(0)
                    .DataBind()
                End With

            Else

                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCommon = Nothing
            objEmp = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try

            If CInt(drpEmployee.SelectedValue) <= 0 Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Employee is compulsory information. Please select Employee to continue."), Me)
                'Pinkal (06-May-2014) -- End
                drpEmployee.Focus()
                Return False
            End If

            If CInt(drpAsset.SelectedValue) <= 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Asset is compulsory information. Please select Asset to continue."), Me)
                'Pinkal (06-May-2014) -- End
                drpAsset.Focus()
                Return False
            End If

            If txtAssetno.Text.Trim = "" Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Asset No. cannot be blank. Asset No. is compulsory information."), Me)
                'Pinkal (06-May-2014) -- End
                txtAssetno.Focus()
                Return False
            End If

            If TxtSerialno.Text.Trim = "" Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Serial No. cannot be blank. Serial No. is compulsory information."), Me)
                'Pinkal (06-May-2014) -- End
                TxtSerialno.Focus()
                Return False
            End If


            If CInt(drpCondition.SelectedValue) <= 0 Then
                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Asset Condition is compulsory information. Please select Asset Condition to continue."), Me)
                'Pinkal (06-May-2014) -- End
                drpCondition.Focus()
                Return False
            End If

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    Private Sub ClearObject()
        Try

            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            'drpEmployee.SelectedIndex = 0

            'Pinkal (22-Nov-2012) -- End



            drpAsset.SelectedIndex = 0
            drpCondition.SelectedIndex = 0
            txtAssetno.Text = ""
            TxtRemark.Text = ""
            TxtSerialno.Text = ""
            dtpdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ClearObject :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Protected Sub btnSaveInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveInfo.Click
        Try

            If Validation() Then

                SetValue()
                If objAsset.Insert = False Then
                    DisplayMessage.DisplayMessage("Entry Not Saved. Reason : " & objAsset._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Saved Sucessfully." & objAsset._Message, Me)
                    ClearObject()
                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnClose.Click
        Try
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Response.Redirect("~\UserHome.aspx", False)
            Response.Redirect(Session("rootpath").ToString & "HR/wPg_AssetsRegisterList.aspx", False)
            'Nilay (02-Mar-2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

#Region "ToolBar Event"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        Response.Redirect("~\HR\wPg_AssetsRegisterList.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

  
    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
        'Language.setLanguage(mstrModuleName)
        Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
        'Nilay (01-Feb-2015) -- Start
        'Enhancement - REDESIGN SELF SERVICE.
        'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
        Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)
        'Nilay (01-Feb-2015) -- End
        Me.lblCondition.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblCondition.ID, Me.lblCondition.Text)
        Me.lblDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblDate.ID, Me.lblDate.Text)
        Me.lblAsset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblAsset.ID, Me.lblAsset.Text)
        Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
        Me.lblAssetNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblAssetNo.ID, Me.lblAssetNo.Text)
        Me.lblRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblRemark.ID, Me.lblRemark.Text)
        Me.lblSerialNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblSerialNo.ID, Me.lblSerialNo.Text)

        Me.btnSaveInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSaveInfo.ID, Me.btnSaveInfo.Text).Replace("&", "")
        Me.BtnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.BtnClose.ID, Me.BtnClose.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub


    'Pinkal (06-May-2014) -- End


  
End Class
