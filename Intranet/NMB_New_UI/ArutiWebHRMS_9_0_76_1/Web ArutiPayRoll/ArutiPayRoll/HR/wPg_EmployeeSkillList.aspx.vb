﻿Option Strict On 'Shani(19-MAR-2016)

#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports System.Drawing

#End Region

Partial Class HR_wPg_EmployeeSkillList
    Inherits Basepage

#Region " Private Variable(s) "

    Private objESkills As New clsEmployee_Skill_Tran
    Dim msg As New CommonCodes


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmEmployee_Skill_List"
    'Pinkal (06-May-2014) -- End

    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mintTransactionId As Integer = 0
    Private objASkillTran As New clsEmployeeSkill_Approval_Tran

    Dim Arr() As String
    Dim SkillApprovalFlowVal As String

    Dim blnPendingEmployee As Boolean = False
    'Gajanan [17-DEC-2018] -- End

    'Gajanan [9-April-2019] -- Start
    Private objApprovalData As New clsEmployeeDataApproval
    'Gajanan [9-April-2019] -- End


    'Gajanan [17-April-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mblnisEmployeeApprove As Boolean = False
    'Gajanan [17-April-2019] -- End

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objCMaster As New clsCommon_Master
        Dim objSMaster As New clsskill_master
        Try
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dsCombos = objEmployee.GetEmployeeList("Employee", True, Not Aruti.Data.ConfigParameter._Object._IsIncludeInactiveEmp)

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), , , Session("AccessLevelFilterString"))
                'Else
                '    'S.SANDEEP [ 04 MAY 2012 ] -- START
                '    'ENHANCEMENT : TRA CHANGES
                '    'dsCombos = objEmployee.GetEmployeeList("Emp", True, )
                '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
                '    'S.SANDEEP [ 04 MAY 2012 ] -- END                        
                'End If



                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .


                Dim mblnOnlyApproved As Boolean = True
                Dim mblnAddApprovalCondition As Boolean = True


                'Gajanan [22-Feb-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If ConfigParameter._Object._PendingEmployeeScreenIDs.Trim.Length > 0 Then
                '    If ConfigParameter._Object._PendingEmployeeScreenIDs.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployee_Skill_List))) Then
                If Session("PendingEmployeeScreenIDs").ToString.Trim.Length > 0 Then
                    If Session("PendingEmployeeScreenIDs").ToString.ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployee_Skill_List))) Then
                        'Gajanan [22-Feb-2019] -- End
                        mblnOnlyApproved = False
                        mblnAddApprovalCondition = False
                    End If
                End If



                'dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                '                            CInt(Session("UserId")), _
                '                            CInt(Session("Fin_year")), _
                '                            CInt(Session("CompanyUnkId")), _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                            CStr(Session("UserAccessModeSetting")), True, _
                '                            CBool(Session("IsIncludeInactiveEmp")), "Emp", True)

                dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                 CStr(Session("UserAccessModeSetting")), _
                                                 mblnOnlyApproved, CBool(Session("IsIncludeInactiveEmp")), "Emp", True, _
                                                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, True, mblnAddApprovalCondition)


                'Shani(24-Aug-2015) -- End
                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                'S.SANDEEP [20-JUN-2018] -- End


                With drpEmployee
                    .DataValueField = "employeeunkid"
                    'Nilay (09-Aug-2016) -- Start
                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                    '.DataTextField = "employeename"
                    .DataTextField = "EmpCodeName"
                    'Nilay (09-Aug-2016) -- End
                    .DataSource = dsCombos.Tables("Emp")
                    .DataBind()
                    .SelectedValue = CStr(0)
                End With
            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                drpEmployee.DataSource = objglobalassess.ListOfEmployee
                drpEmployee.DataTextField = "loginname"
                drpEmployee.DataValueField = "employeeunkid"
                drpEmployee.DataBind()
            End If

            dsCombos = objCMaster.getComboList(clsCommon_Master.enCommonMaster.SKILL_CATEGORY, True, "List")
            With drpCategory
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

            dsCombos = objSMaster.getComboList("List", True)
            With drpSkill
                .DataValueField = "skillunkid"
                .DataTextField = "NAME"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
                .SelectedValue = CStr(0)
            End With

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub ClearObject()
        Try
            drpSkill.SelectedValue = CStr(0) : drpCategory.SelectedValue = CStr(0)
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            drpEmployee.SelectedIndex = 0
            'Sohail (02 May 2012) -- End
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dsList As New DataSet
        Dim dtTable As DataTable
        Dim StrSearching As String = String.Empty
        Try


            If CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                If CBool(Session("ViewEmpSkillList")) = False Then Exit Sub
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''S.SANDEEP [ 27 APRIL 2012 ] -- START
            ''ENHANCEMENT : TRA CHANGES
            ''dsList = objESkills.GetList("Emp", False)
            'dsList = objESkills.GetList("Emp", False, Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"))
            ''S.SANDEEP [ 27 APRIL 2012 ] -- END

            'If CInt(drpEmployee.SelectedValue) > 0 Then
            '    StrSearching &= "AND EmpId = " & CInt(drpEmployee.SelectedValue) & " "
            'End If

            'If CInt(drpCategory.SelectedValue) > 0 Then
            '    StrSearching &= "AND CatId = " & CInt(drpCategory.SelectedValue) & " "
            'End If

            'If CInt(drpSkill.SelectedValue) > 0 Then
            '    StrSearching &= "AND SkillId = " & CInt(drpSkill.SelectedValue) & " "
            'End If

            'If StrSearching.Length > 0 Then
            '    StrSearching = StrSearching.Substring(3)
            '    dtTable = New DataView(dsList.Tables("Emp"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = dsList.Tables("Emp")
            'End If
            If CInt(drpEmployee.SelectedValue) > 0 Then
                StrSearching &= "AND hremployee_master.employeeunkid = " & CInt(drpEmployee.SelectedValue) & " "
            End If

            If CInt(drpCategory.SelectedValue) > 0 Then
                StrSearching &= "AND cfcommon_master.masterunkid = " & CInt(drpCategory.SelectedValue) & " "
            End If

            If CInt(drpSkill.SelectedValue) > 0 Then
                StrSearching &= "AND hrskill_master.skillunkid = " & CInt(drpSkill.SelectedValue) & " "
            End If

            If StrSearching.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If


            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.

            'dsList = objESkills.GetList(Session("Database_Name"), _
            '                              Session("UserId"), _
            '                              Session("Fin_year"), _
            '                              Session("CompanyUnkId"), _
            '                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                              Session("UserAccessModeSetting"), True, _
            '                              Session("IsIncludeInactiveEmp"), "Emp", , StrSearching)

            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .

            Dim mblnOnlyApproved As Boolean = True
            Dim mblnAddApprovalCondition As Boolean = True
            If Session("PendingEmployeeScreenIDs").ToString.Trim.Length > 0 Then
                If Session("PendingEmployeeScreenIDs").ToString.Split(CChar(",")).Contains(CStr(CInt(enScreenName.frmEmployee_Skill_List))) Then
                    mblnOnlyApproved = False
                    mblnAddApprovalCondition = False
                End If
            End If

            'dsList = objESkills.GetList(CStr(Session("Database_Name")), _
            '                              CInt(Session("UserId")), _
            '                              CInt(Session("Fin_year")), _
            '                              CInt(Session("CompanyUnkId")), _
            '                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                              CStr(Session("UserAccessModeSetting")), True, _
            '                              CBool(Session("IsIncludeInactiveEmp")), "Emp", , StrSearching, CBool(IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, True, False)))

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            Dim mblnisusedmss As Boolean = False
            IIf(CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User, mblnisusedmss = True, mblnisusedmss = False)
            'Gajanan [17-DEC-2018] -- End
            dsList = objESkills.GetList(CStr(Session("Database_Name")), _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                        CStr(Session("UserAccessModeSetting")), mblnOnlyApproved, _
                                        CBool(Session("IsIncludeInactiveEmp")), "Emp", , StrSearching, _
                                        mblnisusedmss, mblnAddApprovalCondition) 'Gajanan [17-DEC-2018] -- Add isusedmss

            'Pinkal (28-Dec-2015) -- End

            'S.SANDEEP [20-JUN-2018] -- End



            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim dcol As New DataColumn
            With dcol
                .DataType = GetType(System.String)
                .ColumnName = "tranguid"
                .DefaultValue = ""
            End With
            dsList.Tables(0).Columns.Add(dcol)


            If SkillApprovalFlowVal Is Nothing Then
                Dim dsPending As New DataSet
                dsPending = objASkillTran.GetList(CStr(Session("Database_Name")), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                  CStr(Session("UserAccessModeSetting")), mblnOnlyApproved, _
                                                  CBool(Session("IsIncludeInactiveEmp")), "Emp", , StrSearching, mblnisusedmss, mblnAddApprovalCondition) 'Gajanan [17-DEC-2018] -- Add isusedmss

                If dsPending.Tables(0).Rows.Count > 0 Then
                    For Each row As DataRow In dsPending.Tables(0).Rows
                        dsList.Tables(0).ImportRow(row)
                    Next
                End If
            End If

            'Gajanan [17-DEC-2018] -- End


            dtTable = dsList.Tables("Emp")

            'Shani(24-Aug-2015) -- End

            If dtTable IsNot Nothing Then
                gvSkill.DataSource = dtTable
                gvSkill.DataKeyField = "SkillTranId"
                gvSkill.DataBind()
            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                gvSkill.CurrentPageIndex = 0
                gvSkill.DataBind()
            Else
                msg.DisplayError(ex, Me)
            End If
        Finally
        End Try
    End Sub

    Public Shared Function b64encode(ByVal StrEncode As String) As String
        Try
            Dim encodedString As String
            encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
            Return (encodedString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub delete()
        Try

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'popup1.Show()




            popup_DeleteReason.Show()
            'Nilay (01-Feb-2015) -- End


        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub

    'Private Sub SetPrevious_State()
    '    Try
    '        Session("Prev_State") = drpEmployee.SelectedValue & "|" & _
    '                                drpCategory.SelectedValue & "|" & _
    '                                drpSkill.SelectedValue & "|" & _
    '                                gvSkill.CurrentPageIndex()
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub GetPrevious_State()
    '    Try
    '        Dim StrState() As String = Nothing
    '        StrState = CType(Session("Prev_State"), String).Split("|")

    '        If StrState.Length > 0 Then
    '            drpEmployee.SelectedValue = StrState(0)
    '            drpCategory.SelectedValue = StrState(1)
    '            drpSkill.SelectedValue = StrState(2)
    '            gvSkill.CurrentPageIndex = StrState(3)
    '        End If

    '        Session("IsFrom_AddEdit") = False

    '        Call FillGrid()

    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

#End Region

#Region " Form's Event(s) "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_BioData_Management) = False Then
                'Anjan [20 February 2016] -- End
                msg.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            Aruti.Data.FinancialYear._Object._YearUnkid = CInt(Session("Fin_year"))
            Dim clsuser As New User

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Blank_ModuleName()
            'clsCommonATLog._WebFormName = "frmEmployee_Skill_List"
            'StrModuleName2 = "mnuPersonnel"
            'StrModuleName3 = "mnuEmployeeData"
            'objESkills._WebClientIP = Session("IP_ADD")
            'objESkills._WebHostName = Session("HOST_NAME")
            'If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    objESkills._Loginemployeeunkid = Session("Employeeunkid")
            'End If

            'S.SANDEEP [ 04 JULY 2012 ] -- END

            If (Page.IsPostBack = False) Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLanguage()
                'Pinkal (06-May-2014) -- End

                clsuser = CType(Session("clsuser"), User)

                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.UserID
                    Me.ViewState("Empunkid") = Session("UserId")
                    'Sohail (30 Mar 2015) -- End

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = Session("UserId")
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END


                    'Anjan (30 May 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                    'BtnNew.Visible = False
                    BtnNew.Visible = CBool(Session("AddEmployeeSkill"))
                    'Anjan (30 May 2012)-End 


                    'S.SANDEEP [20-JUN-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow For NMB .



                    ''S.SANDEEP [ 16 JAN 2014 ] -- START
                    'chkShowPending.Visible = CBool(Session("AllowToApproveEmployee"))
                    'If Session("ShowPending") IsNot Nothing Then
                    '    chkShowPending.Checked = True
                    'End If
                    ''S.SANDEEP [ 16 JAN 2014 ] -- END

                    'S.SANDEEP [20-JUN-2018] -- End

                Else
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                    Me.ViewState("Empunkid") = Session("Employeeunkid")
                    'Sohail (30 Mar 2015) -- End

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Aruti.Data.User._Object._Userunkid = -1
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    'Anjan (02 Mar 2012)-Start
                    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request


                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'BtnNew.Visible = ConfigParameter._Object._AllowAddSkills
                    BtnNew.Visible = CBool(Session("AllowAddSkills"))
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END
                    'Anjan (02 Mar 2012)-End 


                    'Gajanan [17-DEC-2018] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.

                    ''S.SANDEEP [ 16 JAN 2014 ] -- START
                    'chkShowPending.Visible = False
                    ''S.SANDEEP [ 16 JAN 2014 ] -- END

                    'Gajanan [17-DEC-2018] -- End
                End If





                Call FillCombo()

                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Call FillGrid()
                'S.SANDEEP [ 27 APRIL 2012 ] -- END





                'Pinkal (22-Mar-2012) -- Start
                'Enhancement : TRA Changes

                'Nilay (01-Feb-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
                'Nilay (01-Feb-2015) -- End
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    gvSkill.Columns(2).Visible = True
                Else
                    gvSkill.Columns(2).Visible = False
                End If

                'Pinkal (22-Mar-2012) -- End

                'If Session("IsFrom_AddEdit") = True Then
                '    Call GetPrevious_State()
                'End If


                'S.SANDEEP [20-JUN-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow For NMB .

                ''S.SANDEEP [ 16 JAN 2014 ] -- START
                'If Session("ShowPending") IsNot Nothing Then
                '    Call chkShowPending_CheckedChanged(New Object, New EventArgs)
                'End If
                ''S.SANDEEP [ 16 JAN 2014 ] -- END
                'S.SANDEEP [20-JUN-2018] -- End


                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("Skill_EmpUnkID") IsNot Nothing Then
                    drpEmployee.SelectedValue = CStr(Session("Skill_EmpUnkID"))
                    Session.Remove("Skill_EmpUnkID")
                    If CInt(drpEmployee.SelectedValue) > 0 Then
                        Call btnSearch_Click(BtnSearch, Nothing)
                    End If
                End If
                'SHANI [09 Mar 2015]--END

            Else
                blnPendingEmployee = CBool(ViewState("blnPendingEmployee"))
                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If IsNothing(ViewState("mblnisEmployeeApprove")) = False Then
                    mblnisEmployeeApprove = CBool(ViewState("mblnisEmployeeApprove"))
                End If
                'Gajanan [17-April-2019] -- End
            End If

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'lblPendingData.Visible = False
            objtblPanel.Visible = False
            'btnApprovalinfo.Visible = False


            Arr = Session("SkipApprovalOnEmpData").ToString().Split(CChar(","))
            SkillApprovalFlowVal = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmEmployee_Skill_List)))

            If blnPendingEmployee Then
                objtblPanel.Visible = blnPendingEmployee
            End If
            'Gajanan [17-DEC-2018] -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.VisibleImageSaprator1(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'Nilay (01-Feb-2015) -- End
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Session("IsFrom_AddEdit") = False
    End Sub


    'Pinkal (22-Mar-2012) -- Start
    'Enhancement : TRA Changes

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '    ToolbarEntry1.VisibleSaveButton(False)
    '    ToolbarEntry1.VisibleDeleteButton(False)
    '    ToolbarEntry1.VisibleCancelButton(False)
    '    ToolbarEntry1.VisibleExitButton(False)
    'End Sub
    'Nilay (01-Feb-2015) -- End

    'Pinkal (22-Mar-2012) -- End
    'Gajanan [17-DEC-2018] -- Start
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Try
            Me.ViewState.Add("blnPendingEmployee", blnPendingEmployee)

            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            Me.ViewState.Add("mblnisEmployeeApprove", mblnisEmployeeApprove)
            'Gajanan [17-April-2019] -- End

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try

    End Sub
    'Gajanan [17-DEC-2018] -- End
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "
    'Gajanan [27-May-2019] -- Start              
    Protected Sub BtnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If IsNothing(Session("Isfromprofile")) = False AndAlso CBool(Session("Isfromprofile")) = True Then
                Response.Redirect(Session("rootpath").ToString & "HR/wPgEmployeeDetail.aspx", False)
                Session.Remove("Isfromprofile")
            ElseIf IsNothing(Session("IsfromRecruitment")) = False AndAlso CBool(Session("IsfromRecruitment")) = True Then
                Response.Redirect(Session("rootpath").ToString & "Recruitment/wPgEmployeeUpdate.aspx", False)
                Session.Remove("IsfromRecruitment")
            Else
                Response.Redirect(Session("rootpath").ToString & "UserHome.aspx", False)
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Gajanan [27-May-2019] -- End

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            'Sohail (02 May 2012) -- Start
            'TRA - ENHANCEMENT
            If CInt(drpEmployee.SelectedValue) <= 0 Then
                msg.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If

            'Sohail (02 May 2012) -- End
            Call FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNew.Click
        Try

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            If CInt(drpEmployee.SelectedValue) > 0 Then
                Session("Skill_EmpUnkID") = drpEmployee.SelectedValue
            End If
            'SHANI [09 Mar 2015]--END

            'Gajanan [3-April-2019] -- Start
            'Response.Redirect("wPg_EmployeeSkill.aspx", False)
            Response.Redirect(CStr(Session("servername")) & "~/HR/wPg_EmployeeSkill.aspx", False)
            'Gajanan [3-April-2019] -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnNew_Click : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            Call ClearObject()


            'Pinkal (22-Nov-2012) -- Start
            'Enhancement : TRA Changes

            'Call FillGrid()

            gvSkill.DataSource = Nothing
            gvSkill.DataBind()
            gvSkill.CurrentPageIndex = 0

            'Gajanan [3-April-2019] -- Start
            blnPendingEmployee = False
            objtblPanel.Visible = False
            'Gajanan [3-April-2019] -- End

            'Pinkal (22-Nov-2012) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnReset_Click Event : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub


    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButDel.Click
    Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteReason.buttonDelReasonYes_Click
        'Nilay (01-Feb-2015) -- End
        Try
            If (popup_DeleteReason.Reason.Trim = "") Then 'Nilay (01-Feb-2015) -- If (txtreasondel.Text = "")
                msg.DisplayMessage(" Please enter delete reason.", Me)
                Exit Sub
            End If

            Dim objSkillTran As New clsEmployee_Skill_Tran
            objSkillTran._Skillstranunkid = CInt(Me.ViewState("Unkid"))

            'S.SANDEEP [ 27 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Blank_ModuleName()
            objSkillTran._WebFormName = "frmEmployee_Skill_List"
            StrModuleName2 = "mnuPersonnel"
            StrModuleName3 = "mnuEmployeeData"
            objSkillTran._WebClientIP = CStr(Session("IP_ADD"))
            objSkillTran._WebHostName = CStr(Session("HOST_NAME"))
            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                objSkillTran._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                objSkillTran._Userunkid = -1
            Else
                objSkillTran._Userunkid = CInt(Session("UserId"))
                objSkillTran._Loginemployeeunkid = -1
            End If
            'S.SANDEEP [ 27 AUG 2012 ] -- END

            With objSkillTran
                ._Isvoid = True
                ._Voiddatetime = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popup_DeleteReason.Reason.Trim 'Nilay (01-Feb-2015) -- txtreason.Text
                If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                    ._Voiduserunkid = CInt(Session("UserId"))
                Else
                    ._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))
                End If





                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                'If SkillApprovalFlowVal Is Nothing Then
                If SkillApprovalFlowVal Is Nothing AndAlso mblnisEmployeeApprove = True Then
                    'Gajanan [17-April-2019] -- End


                    'Gajanan [17-April-2019] -- Start
                    'Enhancement - Implementing Employee Approver Flow On Employee Data.
                    Dim eLMode As Integer
                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                        eLMode = enLogin_Mode.MGR_SELF_SERVICE
                    Else
                        eLMode = enLogin_Mode.EMP_SELF_SERVICE
                    End If
                    'Gajanan [17-April-2019] -- End

                    objASkillTran._Isvoid = True
                    objASkillTran._Audituserunkid = CInt(Session("UserId"))
                    objASkillTran._Isweb = False
                    objASkillTran._Ip = getIP()
                    objASkillTran._Host = getHostName()
                    objASkillTran._Form_Name = mstrModuleName
                    If objASkillTran.Delete(CInt(Me.ViewState("Unkid")), popup_DeleteReason.Reason.Trim, CInt(Session("CompanyUnkId")), Nothing) = False Then
                        If objASkillTran._Message <> "" Then
                            msg.DisplayMessage("You Cannot Delete this Entry." & objASkillTran._Message, Me)

                            Exit Sub
                        End If
                    Else

                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        objApprovalData.SendNotification(1, CStr(Session("Database_Name")), _
                                                         CStr(Session("UserAccessModeSetting")), _
                                                         CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), _
                                                         CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                         enScreenName.frmEmployee_Skill_List, CStr(Session("EmployeeAsOnDate")), _
                                                         CInt(Session("UserId")), mstrModuleName, CType(eLMode, enLogin_Mode), _
                                                         Session("UserName").ToString(), clsEmployeeDataApproval.enOperationType.DELETED, , objSkillTran._Emp_App_Unkid.ToString(), , , _
                                                         " skillstranunkid = " & CInt(Me.ViewState("Unkid")), Nothing, , , _
                                                         " skillstranunkid = " & CInt(Me.ViewState("Unkid")), Nothing)
                        'Gajanan [17-April-2019] -- End


                        msg.DisplayMessage("Entry Successfully deleted." & objASkillTran._Message, Me)

                        'Gajanan [17-April-2019] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'Me.ViewState("ExpId")= Nothing
                        ViewState("Unkid") = Nothing
                        'Gajanan [17-April-2019] -- End

                        Call FillGrid()

                    End If
                Else
                    If .Delete(CInt(Me.ViewState("Unkid")), ._Isvoid, ._Voiduserunkid, ._Voiddatetime, ._Voidreason) = False Then
                        msg.DisplayMessage(._Message, Me)
                    Else
                        Call FillGrid()
                        Me.ViewState("Unkid") = Nothing
                    End If
                End If




                'If .Delete(CInt(Me.ViewState("Unkid")), ._Isvoid, ._Voiduserunkid, ._Voiddatetime, ._Voidreason) = False Then
                '    msg.DisplayMessage(._Message, Me)
                'Else
                '    Call FillGrid()
                '    Me.ViewState("Unkid") = Nothing

                '    'Nilay (01-Feb-2015) -- Start
                '    'Enhancement - REDESIGN SELF SERVICE.
                '    ' txtreason.Text = ""
                '    'Nilay (01-Feb-2015) -- End
                'End If
            End With

            popup_DeleteReason.Dispose() 'Nilay (01-Feb-2015) -- popup1.Dispose()

            objSkillTran = Nothing

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End

    Protected Sub btnApprovalinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovalinfo.Click
        Try
            Popup_Viewreport._UserId = CInt(Session("UserId"))
            Popup_Viewreport._Priority = 0
            Popup_Viewreport._PrivilegeId = enUserPriviledge.AllowToApproveRejectEmployeeSkills
            Popup_Viewreport._FillType = enScreenName.frmEmployee_Skill_List
            Popup_Viewreport._FilterString = "EM.employeeunkid = " & CInt(drpEmployee.SelectedValue)
            Popup_Viewreport._FromApprovalScreen = False
            Popup_Viewreport._OprationType = clsEmployeeDataApproval.enOperationType.NONE

            Popup_Viewreport.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'msg.DisplayMessage("btnApprovalinfo_Click Event : " & ex.Message, Me)
            msg.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region

#Region " Control's Event(s) "

    Protected Sub dgView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gvSkill.DeleteCommand
        Try
            Me.ViewState.Add("Unkid", gvSkill.DataKeys(e.Item.ItemIndex))

            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'txtreason.Visible = True

            'Nilay (01-Feb-2015) -- End

            Dim hfempid As HiddenField = CType(gvSkill.Items(e.Item.ItemIndex).FindControl("hfemployeeid"), HiddenField)

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = CInt(hfempid.Value)


            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            mblnisEmployeeApprove = objEmployee._Isapproved
            'Gajanan [17-April-2019] -- End



            'If SkillApprovalFlowVal Is Nothing Then
            If SkillApprovalFlowVal Is Nothing AndAlso objEmployee._Isapproved Then
                'Gajanan [22-Feb-2019] -- End

                'Gajanan [17-April-2019] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                If objApprovalData.IsApproverPresent(enScreenName.frmEmployee_Skill_List, CStr(Session("Database_Name")), _
                                                      CStr(Session("UserAccessModeSetting")), CInt(Session("CompanyUnkId")), _
                                                      CInt(Session("Fin_year")), CInt(enUserPriviledge.AllowToApproveRejectEmployeeSkills), _
                                                      CInt(Session("UserId")), CStr(Session("EmployeeAsOnDate")), CInt(hfempid.Value), Nothing) = False AndAlso objApprovalData._Message.Length > 0 Then
                    msg.DisplayMessage(objApprovalData._Message, Me)
                    Exit Sub
                End If
                'Gajanan [17-April-2019] -- End


                Dim item = gvSkill.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(8).Text = e.Item.Cells(8).Text And x.Cells(6).Text.Trim <> "&nbsp;").FirstOrDefault()
                If item IsNot Nothing Then
                    msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process."), Me)
                    Exit Sub
                Else
                    Me.ViewState.Add("Unkid", gvSkill.DataKeys(e.Item.ItemIndex))
                    delete()
                End If
            Else
                delete()
            End If



            FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gvSkill.ItemCommand
        Try

            Session("IsFrom_AddEdit") = True

            'Call SetPrevious_State()

            If gvSkill.Items.Count > 0 Then
                If gvSkill.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Color.LightCoral).Count() > 0 Then
                    Dim item = gvSkill.Items().Cast(Of DataGridItem)().Where(Function(x) x.BackColor = Color.LightCoral).FirstOrDefault()
                    If item IsNot Nothing Then item.BackColor = Color.White
                End If
            End If

            If e.CommandName = "Select" Then

                If SkillApprovalFlowVal Is Nothing Then
                    Dim item = gvSkill.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(8).Text = e.Item.Cells(8).Text And x.Cells(6).Text.Trim <> "&nbsp;").FirstOrDefault()
                    If item IsNot Nothing Then
                        msg.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process."), Me)
                        Exit Sub
                    Else
                        Session.Add("skillId", gvSkill.DataKeys(e.Item.ItemIndex))
                        If CInt(drpEmployee.SelectedValue) > 0 Then
                            Session("Skill_EmpUnkID") = drpEmployee.SelectedValue
                        End If

                        'Gajanan [3-April-2019] -- Start
                        'Response.Redirect("wPg_EmployeeSkill.aspx?ProcessId=" & b64encode(CStr(Val(gvSkill.DataKeys(e.Item.ItemIndex)))))
                        Response.Redirect(CStr(Session("servername")) & "~/HR/wPg_EmployeeSkill.aspx?ProcessId=" & b64encode(CStr(Val(gvSkill.DataKeys(e.Item.ItemIndex)))), False)
                        'Gajanan [3-April-2019] -- End

                    End If
                Else
                    'SHANI [09 Mar 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    If CInt(drpEmployee.SelectedValue) > 0 Then
                        Session("Skill_EmpUnkID") = drpEmployee.SelectedValue
                    End If
                    'SHANI [09 Mar 2015]--END
                    Response.Redirect("wPg_EmployeeSkill.aspx?ProcessId=" & b64encode(CStr(Val(gvSkill.DataKeys(e.Item.ItemIndex)))), False)
                End If

            ElseIf e.CommandName.ToUpper = "VIEW" Then
                Dim item = gvSkill.Items().Cast(Of DataGridItem)().Where(Function(x) x.Cells(8).Text = e.Item.Cells(8).Text And x.Cells(6).Text.Trim = "&nbsp;").FirstOrDefault()
                If item IsNot Nothing Then
                    item.BackColor = Color.LightCoral
                    item.ForeColor = Color.Black
                End If
            End If

        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub



    Protected Sub dgView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles gvSkill.PageIndexChanged
        Try
            gvSkill.CurrentPageIndex = e.NewPageIndex
            FillGrid()
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Anjan (02 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Protected Sub gvSkill_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gvSkill.ItemDataBound
        Try
            'If (e.Item.ItemIndex >= 0) Then
            '    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
            '        gvSkill.Columns(1).Visible = CBool(Session("AllowDeleteSkills"))

            '        'Pinkal (22-Nov-2012) -- Start
            '        'Enhancement : TRA Changes
            '        gvSkill.Columns(0).Visible = CBool(Session("AllowEditSkills"))
            '        'Pinkal (22-Nov-2012) -- End


            '        'S.SANDEEP [20-JUN-2018] -- Start
            '        'Enhancement - Implementing Employee Approver Flow For NMB .

            '        ''S.SANDEEP [ 16 JAN 2014 ] -- START
            '        'If Session("ShowPending") IsNot Nothing Then
            '        '    gvSkill.Columns(0).Visible = False
            '        '    gvSkill.Columns(1).Visible = False
            '        'End If
            '        ''S.SANDEEP [ 16 JAN 2014 ] -- END

            '        'S.SANDEEP [20-JUN-2018] -- End



            '    Else
            '        'gvSkill.Columns(1).Visible = False
            '        gvSkill.Columns(1).Visible = CBool(Session("DeleteEmployeeSkill"))

            '        'Pinkal (22-Nov-2012) -- Start
            '        'Enhancement : TRA Changes
            '        gvSkill.Columns(0).Visible = CBool(Session("EditEmployeeSkill"))
            '        'Pinkal (22-Nov-2012) -- End

            '    End If

            '    'Gajanan [17-DEC-2018] -- Start
            '    'Enhancement - Implementing Employee Approver Flow On Employee Data.
            '    If SkillApprovalFlowVal Is Nothing Then
            '        If e.Item.Cells(6).Text <> "&nbsp;" AndAlso e.Item.Cells(6).Text.Length > 0 Then
            '            e.Item.BackColor = Color.PowderBlue
            '            e.Item.ForeColor = Color.Black
            '            lblPendingData.Visible = True
            '            btnApprovalinfo.Visible = True
            '            CType(e.Item.Cells(0).FindControl("ImgSelect"), LinkButton).Visible = False
            '            CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False
            '        End If
            '    End If

            '    'Gajanan [17-DEC-2018] -- End
            'End If


            If (e.Item.ItemIndex >= 0) Then
                If SkillApprovalFlowVal Is Nothing Then
                    Dim blnFlag As Boolean = False
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then
                        CType(e.Item.Cells(1).FindControl("ImgSelect"), LinkButton).Visible = CBool(Session("EditEmployeeSkill"))
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = CBool(Session("DeleteEmployeeSkill"))
                    Else
                        CType(e.Item.Cells(1).FindControl("ImgSelect"), LinkButton).Visible = CBool(Session("AllowEditSkills"))
                        CType(e.Item.Cells(2).FindControl("ImgDelete"), LinkButton).Visible = CBool(Session("AllowDeleteExperience"))
                    End If
                Else
                    If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.Employee) Then
                        gvSkill.Columns(1).Visible = CBool(Session("AllowDeleteSkills"))
                        gvSkill.Columns(0).Visible = CBool(Session("AllowEditSkills"))
                    Else
                        gvSkill.Columns(1).Visible = CBool(Session("DeleteEmployeeSkill"))
                        gvSkill.Columns(0).Visible = CBool(Session("EditEmployeeSkill"))
                    End If
                End If

                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                If SkillApprovalFlowVal Is Nothing Then
                    If e.Item.Cells(6).Text <> "&nbsp;" AndAlso e.Item.Cells(6).Text.Length > 0 Then
                        e.Item.BackColor = Color.PowderBlue
                        e.Item.ForeColor = Color.Black

                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'lblPendingData.Visible = True
                        objtblPanel.Visible = True
                        blnPendingEmployee = True
                        'btnApprovalinfo.Visible = True
                        'Gajanan [17-DEC-2018] -- End



                        CType(e.Item.Cells(0).FindControl("ImgSelect"), LinkButton).Visible = False
                        CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False

                        'Gajanan [17-DEC-2018] -- Start
                        'Enhancement - Implementing Employee Approver Flow On Employee Data.
                        'CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = True
                        If CType(CType(e.Item.Cells(1).FindControl("hfoprationtypeid"), HiddenField).Value, clsEmployeeDataApproval.enOperationType) = clsEmployeeDataApproval.enOperationType.EDITED Then
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = True
                        Else
                            CType(e.Item.Cells(1).FindControl("imgDetail"), LinkButton).Visible = False
                        End If
                        'Gajanan [17-DEC-2018] -- End

                    End If
                End If
                'Gajanan [17-DEC-2018] -- End
            End If
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
    End Sub
    'Anjan (02 Mar 2012)-End 



    'S.SANDEEP [20-JUN-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow For NMB .

    ''S.SANDEEP [ 16 JAN 2014 ] -- START
    'Protected Sub chkShowPending_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowPending.CheckedChanged
    '    Try
    '        Session("ShowPending") = chkShowPending.Checked
    '        Dim dsCombos As New DataSet
    '        Dim objEmp As New clsEmployee_Master
    '        If chkShowPending.Checked = True Then

    '            'Shani(24-Aug-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"), , , False)
    '            dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                        CInt(Session("UserId")), _
    '                                        CInt(Session("Fin_year")), _
    '                                        CInt(Session("CompanyUnkId")), _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                        CStr(Session("UserAccessModeSetting")), False, _
    '                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
    '            'Shani(24-Aug-2015) -- End

    '            Dim dtTab As DataTable = New DataView(dsCombos.Tables("Employee"), "isapproved = 0", "employeename", DataViewRowState.CurrentRows).ToTable
    '            With drpEmployee
    '                .DataValueField = "employeeunkid"
    '                'Nilay (09-Aug-2016) -- Start
    '                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
    '                '.DataTextField = "employeename"
    '                .DataTextField = "EmpCodeName"
    '                'Nilay (09-Aug-2016) -- End
    '                .DataSource = dtTab
    '                .DataBind()
    '                Try
    '                    .SelectedValue = IIf(CInt(Session("Employeeunkid")) <= 0, 0, CInt(Session("Employeeunkid"))).ToString
    '                Catch ex As Exception
    '                    .SelectedValue = CStr(0)
    '                End Try
    '            End With
    '            BtnNew.Visible = False
    '        Else
    '            Session("ShowPending") = Nothing
    '            BtnNew.Visible = True
    '            If (CType(Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User) Then

    '                'Shani(24-Aug-2015) -- Start
    '                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                'dsCombos = objEmp.GetEmployeeList("Employee", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date, , , Session("AccessLevelFilterString"))
    '                dsCombos = objEmp.GetEmployeeList(CStr(Session("Database_Name")), _
    '                                        CInt(Session("UserId")), _
    '                                        CInt(Session("Fin_year")), _
    '                                        CInt(Session("CompanyUnkId")), _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
    '                                        CStr(Session("UserAccessModeSetting")), True, _
    '                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", True)
    '                'Shani(24-Aug-2015) -- End
    '                With drpEmployee
    '                    .DataValueField = "employeeunkid"
    '                    'Nilay (09-Aug-2016) -- Start
    '                    'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
    '                    '.DataTextField = "employeename"
    '                    .DataTextField = "EmpCodeName"
    '                    'Nilay (09-Aug-2016) -- End
    '                    .DataSource = dsCombos.Tables("Employee")
    '                    .DataBind()
    '                    Try
    '                        .SelectedValue = IIf(CInt(Session("Employeeunkid")) <= 0, 0, CInt(Session("Employeeunkid"))).ToString
    '                    Catch ex As Exception
    '                        .SelectedValue = CStr(0)
    '                    End Try
    '                End With
    '            Else
    '                Dim objglobalassess = New GlobalAccess
    '                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
    '                With drpEmployee
    '                    .DataSource = objglobalassess.ListOfEmployee
    '                    .DataTextField = "loginname"
    '                    .DataValueField = "employeeunkid"
    '                    .DataBind()
    '                    Try
    '                        .SelectedValue = IIf(CInt(Session("Employeeunkid")) <= 0, 0, CInt(Session("Employeeunkid"))).ToString
    '                    Catch ex As Exception
    '                        .SelectedValue = CStr(0)
    '                    End Try
    '                End With
    '            End If
    '        End If
    '        gvSkill.DataSource = Nothing : gvSkill.DataBind()
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    ''S.SANDEEP [ 16 JAN 2014 ] -- END

    'S.SANDEEP [20-JUN-2018] -- End

#End Region

#Region "ToolBar Event"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try

    '        'Pinkal (22-Nov-2012) -- Start
    '        'Enhancement : TRA Changes

    '        If Session("LoginBy") = Global.User.en_loginby.User Then
    '            If CBool(Session("AddEmployeeSkill")) = False Then
    '                'Nilay (01-Feb-2015) -- Start
    '                'Enhancement - REDESIGN SELF SERVICE.
    '                'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                'Nilay (01-Feb-2015) -- End
    '                Exit Sub
    '            End If
    '        Else
    '            If CBool(Session("AllowAddSkills")) = False Then
    '                'Nilay (01-Feb-2015) -- Start
    '                'Enhancement - REDESIGN SELF SERVICE.
    '                'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                'Nilay (01-Feb-2015) -- End
    '                Exit Sub
    '            End If
    '        End If

    '        'Pinkal (22-Nov-2012) -- End

    '        Response.Redirect("~\HR\wPg_EmployeeSkill.aspx")
    '    Catch ex As Exception
    '        msg.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 

    Private Sub SetLanguage()
        Try         'Hemant (13 Aug 2020)
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            ' Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebotton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End

            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblSkillCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSkillCategory.ID, Me.lblSkillCategory.Text)
            Me.lblSkill.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSkill.ID, Me.lblSkill.Text)

            Me.lblPendingData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPendingData.ID, Me.lblPendingData.Text)
            Me.lblParentData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblParentData.ID, Me.lblParentData.Text)

            gvSkill.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvSkill.Columns(1).FooterText, gvSkill.Columns(1).HeaderText)
            gvSkill.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvSkill.Columns(2).FooterText, gvSkill.Columns(2).HeaderText)
            gvSkill.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvSkill.Columns(3).FooterText, gvSkill.Columns(3).HeaderText)
            gvSkill.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvSkill.Columns(4).FooterText, gvSkill.Columns(4).HeaderText)
            gvSkill.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), gvSkill.Columns(5).FooterText, gvSkill.Columns(5).HeaderText)

            Me.BtnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnNew.ID, Me.BtnNew.Text).Replace("&", "")
            'Hemant (13 Aug 2020) -- Start
        Catch ex As Exception
            msg.DisplayError(ex, Me)
        End Try
        'Hemant (13 Aug 2020) -- End

    End Sub

    'Pinkal (06-May-2014) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Sorry, you cannot edit this information. Reason, this particular information is already in approval process.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Sorry, you cannot delete this information. Reason, this particular information is already in approval process.")

        Catch Ex As Exception
            msg.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
