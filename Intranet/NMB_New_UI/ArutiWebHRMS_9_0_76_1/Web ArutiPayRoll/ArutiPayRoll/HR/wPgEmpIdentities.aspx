﻿<%@ Page Title="Employee Identity" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPgEmpIdentities.aspx.vb" Inherits="wPgEmpIdentities" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%--S.SANDEEP |26-APR-2019| -- START--%>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%--S.SANDEEP |26-APR-2019| -- END--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <script type="text/javascript">
        function IsValidAttach() {
            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        }
        
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= popup_AttachementYesNo.ClientID %>_Panel1").css("z-index", "100002");
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="row clearfix d--f jc--c ai--c">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Employee Identity Add/Edit"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkScanAttachPersonal" runat="server" ToolTip="Attach Document">
                                              <i class="fas fa-paperclip"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblIdType" runat="server" Text="Id Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboIdType" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblIdentityNo" runat="server" Text="Id. No." CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtIdNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblIdIssueCountry" runat="server" Text="Issue Country" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboCountry" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblIdSerialNo" runat="server" Text="Id Serial No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtIdSrNo" runat="server" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblIdPlaceOfIssue" runat="server" Text="Place of Issue" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtIssuePlace" runat="server" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblDLClass" runat="server" Text="DL Class" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtDLClass" runat="server" CssClass="form-control"></asp:TextBox></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblIdIssueDate" runat="server" Text="Issue Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpIssueDate" runat="server" AutoPostBack="false" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblIdExpiryDate" runat="server" Text="Expiry Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpExpiryDate" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:CheckBox ID="chkMakeAsDefault" runat="server" Text="Mark As Default" CssClass="filled-in" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="BtnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ScanAttachment">
                    <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="modal-backdrop"
                        TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" CancelControlID="hdf_ScanAttchment">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="card modal-dialog" Style="display: none;">
                        <div class="header">
                            <h2>
                                <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                            </h2>
                        </div>
                        <div class="body" style="max-height: 500px">
                            <div class="row clearfix d--f ai--fe">
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                    <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                    <div class="form-group">
                                        <asp:DropDownList ID="cboScanDcoumentType" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m-b-15">
                                    <asp:Panel ID="pnl_ImageAdd" runat="server">
                                        <div id="fileuploader">
                                            <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" value="Browse"
                                                onclick="return IsValidAttach();" />
                                        </div>
                                    </asp:Panel>
                                    <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                        Text="Browse" />
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 250px">
                                        <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered"
                                            AllowPaging="false">
                                            <Columns>
                                                <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="DeleteImg" runat="server" CommandName="Delete" ToolTip="Delete">
                                                   <i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--0--%>
                                                <%--'S.SANDEEP |16-MAY-2019| -- START--%>
                                                <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
                                                <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                    <ItemTemplate>
                                                        <span class="gridiconbc">
                                                            <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download">
                                            <i class="fas fa-download text-primary"></i>
                                                            </asp:LinkButton>
                                                        </span>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <%--1--%>
                                                <%--'S.SANDEEP |16-MAY-2019| -- END--%>
                                                <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                                <%--2--%>
                                                <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                                <%--3--%>
                                                <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                                <%--4--%>
                                            </Columns>
                                        </asp:DataGrid>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary pull-left" />
                            <asp:Button ID="btnScanSave" runat="server" Text="Add" CssClass="btn btn-primary" />
                            <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                        </div>
                    </asp:Panel>
                    <ucCfnYesno:Confirmation ID="popup_AttachementYesNo" runat="server" Message="" Title="Confirmation"
                        IsFireButtonNoClick="false" />
                </div>
            </ContentTemplate>
            <%--'S.SANDEEP |16-MAY-2019| -- START--%>
            <%--'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT--%>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgv_Attchment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
            <%--'S.SANDEEP |16-MAY-2019| -- END--%>
        </asp:UpdatePanel>
    </asp:Panel>
    <%--S.SANDEEP |26-APR-2019| -- START--%>

    <script type="text/javascript">
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPgEmpIdentities.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }

        $('body').on("click","input[type=file]", function() {
            debugger;
            return IsValidAttach();
        });
        
    </script>

    <%--S.SANDEEP |26-APR-2019| -- END--%>
</asp:Content>
