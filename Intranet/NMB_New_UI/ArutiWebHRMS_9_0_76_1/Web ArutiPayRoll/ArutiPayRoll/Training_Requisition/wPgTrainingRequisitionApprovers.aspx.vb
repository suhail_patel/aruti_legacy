﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing

#End Region

Partial Class Training_Requisition_wPgTrainingRequisitionApprovers
    Inherits Basepage

#Region "Private Variable"

    Private DisplayMessage As New CommonCodes
    Private objTrainingApproverMaster As New clstraining_approver_master
    Private ReadOnly mstrModuleName As String = "frmTrainingRequsitionApproverList"
    Private ReadOnly mstrModuleName1 As String = "frmTrainingRequsitionApproverAddedit"
    Private blnpopupApproverUseraccess As Boolean = False
    Private mintMappingUnkid As Integer
    Private currentId As String = ""
    Private Index As Integer
    Private mblActiveDeactiveApprStatus As Boolean
    Private mdtApproverList As DataTable

#End Region

#Region " Form Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.On_Job_Training_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then
                ListFillCombo()
                If CBool(Session("AllowToViewTrainingApprover")) Then
                    FillList(False)
                Else
                    FillList(True)
                End If
                SetVisibility()
            Else
                mintMappingUnkid = CInt(ViewState("mintMappingUnkid"))
                mblActiveDeactiveApprStatus = CBool(ViewState("mblActiveDeactiveApprStatus"))
                blnpopupApproverUseraccess = ViewState("blnpopupApproverUseraccess")

                If ViewState("mdtApproverList") IsNot Nothing Then
                    mdtApproverList = CType(ViewState("mdtApproverList"), DataTable)
                    If mdtApproverList.Rows.Count > 0 AndAlso CInt(mdtApproverList.Rows(0)("mappingunkid").ToString) > 0 Then
                        gvApproverList.DataSource = mdtApproverList
                        gvApproverList.DataBind()
                    End If
                End If

                If blnpopupApproverUseraccess Then
                    popupApproverUseraccess.Show()
                End If

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("blnpopupApproverUseraccess") = blnpopupApproverUseraccess
            ViewState("mintMappingUnkid") = mintMappingUnkid
            ViewState("mblActiveDeactiveApprStatus") = mblActiveDeactiveApprStatus
            ViewState("mdtApproverList") = mdtApproverList
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " List Methods "

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsApproverList As New DataSet
        Dim dtApprover As DataTable
        Dim strSearching As String = ""
        Try
            If CInt(drpLevel.SelectedValue) > 0 Then
                strSearching &= "AND TAM.levelunkid = " & CInt(drpLevel.SelectedValue) & " "
            End If

            If CInt(drpApprover.SelectedValue) > 0 Then
                strSearching &= "AND TAM.mapuserunkid = " & CInt(drpApprover.SelectedValue) & " "
            End If

            If CInt(drpStatus.SelectedValue) = 1 Then
                strSearching &= "AND TAM.isactive  = 1 "
            ElseIf CInt(drpStatus.SelectedValue) = 2 Then
                strSearching &= "AND TAM.isactive  = 0 "
            End If

            If isblank Or CBool(Session("AllowToViewTrainingApprover")) = False Then
                strSearching = "AND 1 = 2 "
            End If

            If strSearching.Trim.Length > 0 Then strSearching = strSearching.Substring(3)

            dsApproverList = objTrainingApproverMaster.GetList("List", True, strSearching)

            If dsApproverList.Tables(0).Rows.Count <= 0 Then
                dsApproverList = objTrainingApproverMaster.GetList("List", True, "1 = 2", 0, Nothing, True)
                isblank = True
            End If

            If dsApproverList IsNot Nothing Then
                dtApprover = New DataView(dsApproverList.Tables("List"), "", "Level", DataViewRowState.CurrentRows).ToTable()
                Dim strLeaveName As String = ""
                Dim dtTable As DataTable = dtApprover.Clone
                dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))
                Dim dtRow As DataRow = Nothing
                For Each drow As DataRow In dtApprover.Rows
                    If CStr(drow("Level")).Trim <> strLeaveName.Trim Then
                        dtRow = dtTable.NewRow
                        dtRow("IsGrp") = True
                        dtRow("mappingunkid") = drow("mappingunkid")
                        dtRow("Level") = drow("Level")
                        strLeaveName = drow("Level").ToString()
                        dtTable.Rows.Add(dtRow)
                    End If
                    dtRow = dtTable.NewRow
                    For Each dtcol As DataColumn In dtApprover.Columns
                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next
                dtTable.AcceptChanges()
                gvApproverList.DataSource = dtTable
                gvApproverList.DataBind()
                mdtApproverList = dtTable
                If isblank = True Then
                    gvApproverList.Rows(0).Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Visible = CBool(Session("AllowToAddTrainingApprover"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            rw.Visible = False
            Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
            row.BackColor = ColorTranslator.FromHtml("#F9F9F9")
            Dim cell As TableCell = New TableCell()
            cell.Text = title
            cell.ColumnSpan = gd.Columns.Count
            row.Cells.Add(cell)
            gd.Controls(0).Controls.Add(row)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub ListFillCombo()
        Try
            Dim objMaster As New clsMasterData
            Dim dsCombo As DataSet = objMaster.getComboListTranHeadActiveInActive("List", False)
            With drpStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "1"
            End With

            Dim objLevel As New clstraining_approverlevel_master
            Dim dsList As DataSet = objLevel.getListForCombo("List", True)
            drpLevel.DataTextField = "name"
            drpLevel.DataValueField = "levelunkid"
            drpLevel.DataSource = dsList.Tables(0).Copy()
            drpLevel.DataBind()

            drpApproverUseraccess_level.DataTextField = "name"
            drpApproverUseraccess_level.DataValueField = "levelunkid"
            drpApproverUseraccess_level.DataSource = dsList.Tables(0).Copy()
            drpApproverUseraccess_level.DataBind()

            Dim objApprover As New clstraining_approver_master
            Dim dsApprList As DataSet

            dsApprList = objApprover.GetList("List", True, "", 0, Nothing, True)

            drpApprover.DataTextField = "approver"
            drpApprover.DataValueField = "mappingunkid"
            drpApprover.DataSource = dsApprList.Tables("List")
            drpApprover.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub SetValueToPopup()
        Try
            drpApproverUseraccess_level.SelectedValue = objTrainingApproverMaster._Levelunkid
            drpApproverUseraccess_user.SelectedValue = objTrainingApproverMaster._Mapuserunkid
            drpApproverUseraccess_user_SelectedIndexChanged(Nothing, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#Region " Button Event "

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            FillAddEditCombo()
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            objTrainingApproverMaster._Mappingunkid = lnkedit.CommandArgument.ToString()
            SetValueToPopup()
            popupApproverUseraccess.Show()
            blnpopupApproverUseraccess = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            objTrainingApproverMaster._Mappingunkid = CInt(lnkdelete.CommandArgument.ToString())
            mintMappingUnkid = CInt(lnkdelete.CommandArgument.ToString())

            'If objTrainingApproverMaster.isUsed(CInt(lnkdelete.CommandArgument.ToString())) Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 1, "Sorry, You cannot delete this Approver . Reason: This Approver is in use."), Me)
            '    Exit Sub
            'End If

            confirmapproverdelete.Show()
            confirmapproverdelete.Title = "Confirmation"
            confirmapproverdelete.Message = Language.getMessage(mstrModuleName1, 5, "Are You Sure You Want To Delete This Approver Level ?")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkActive_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkActive As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkActive).NamingContainer, GridViewRow)
            Dim blnFlag As Boolean = False

            mintMappingUnkid = CInt(lnkActive.CommandArgument)
            mblActiveDeactiveApprStatus = True

            objTrainingApproverMaster._Mappingunkid = CInt(lnkActive.CommandArgument.ToString())
            popupconfirmActiveAppr.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDeActive_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkDeActive As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkDeActive).NamingContainer, GridViewRow)

            mintMappingUnkid = CInt(lnkDeActive.CommandArgument)
            mblActiveDeactiveApprStatus = False

            objTrainingApproverMaster._Mappingunkid = CInt(lnkDeActive.CommandArgument)

            'If objTrainingApproverMaster.isApproverInGrievanceUsed(objTrainingApproverMaster._Mappingunkid) Then
            '    DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Approver . Reason: This Approver is in use."), Me)
            '    Exit Sub
            'End If

            popupconfirmDeactiveAppr.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            FillAddEditCombo()
            blnpopupApproverUseraccess = True
            popupApproverUseraccess.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupconfirmActiveAppr_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmActiveAppr.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False
            SetAtValue()
            blnFlag = objTrainingApproverMaster.InActiveApprover(mintMappingUnkid, CInt(Session("CompanyUnkId")), True)

            If blnFlag = False And objTrainingApproverMaster._Message <> "" Then
                DisplayMessage.DisplayMessage("popupconfirmActiveAppr_buttonYes_Click :- " & objTrainingApproverMaster._Message, Me)
            Else
                FillList(False)
                mintMappingUnkid = 0
                mblActiveDeactiveApprStatus = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupconfirmDeactiveAppr_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmDeactiveAppr.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False
            SetAtValue()
            blnFlag = objTrainingApproverMaster.InActiveApprover(mintMappingUnkid, CInt(Session("CompanyUnkId")), False)
            If blnFlag = False And objTrainingApproverMaster._Message <> "" Then
                DisplayMessage.DisplayMessage("popupconfirmDeactiveAppr_buttonYes_Click :- " & objTrainingApproverMaster._Message, Me)
            Else
                FillList(False)
                mintMappingUnkid = 0
                mblActiveDeactiveApprStatus = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub DeleteApprovalReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DeleteApprovalReason.buttonDelReasonYes_Click
        Try
            SetAtValue()
            objTrainingApproverMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objTrainingApproverMaster._Voidreason = DeleteApprovalReason.Reason
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objTrainingApproverMaster._Voiduserunkid = CInt(Session("UserId"))
            End If

            If objTrainingApproverMaster.Delete(CInt(mintMappingUnkid)) = False Then
                DisplayMessage.DisplayMessage(objTrainingApproverMaster._Message, Me)
                Exit Sub
            End If
            FillList(False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region " AddEdit Methods "

#Region " Private Function "

    Dim dsList As DataSet
    Private Sub FillAddEditCombo()
        Try
            Dim objUser As New clsUserAddEdit
            dsList = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), CStr(1224), CInt(Session("Fin_year")), True)
            drpApproverUseraccess_user.DataSource = dsList.Tables("User")
            drpApproverUseraccess_user.DataTextField = "name"
            drpApproverUseraccess_user.DataValueField = "userunkid"
            drpApproverUseraccess_user.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If drpApproverUseraccess_level.SelectedValue = 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Approver Level cannot be blank. Approver Level is required information.", Me)
                'Sohail (23 Mar 2019) -- End
                Return False
            ElseIf drpApproverUseraccess_user.SelectedValue = 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Approver User cannot be blank. Approver User is required information.", Me)
                'Sohail (23 Mar 2019) -- End
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SetValue()
        Try
            If mintMappingUnkid > 0 Then
                objTrainingApproverMaster._Mappingunkid = mintMappingUnkid
            End If
            objTrainingApproverMaster._Levelunkid = Convert.ToInt32(drpApproverUseraccess_level.SelectedValue)
            objTrainingApproverMaster._Mapuserunkid = Convert.ToInt32(drpApproverUseraccess_user.SelectedValue)
            objTrainingApproverMaster._Isactive = True
            Call SetAtValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetAtValue()
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objTrainingApproverMaster._AuditUserId = CInt(Session("UserId"))
            End If
            objTrainingApproverMaster._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objTrainingApproverMaster._ClientIP = CStr(Session("IP_ADD"))
            objTrainingApproverMaster._HostName = CStr(Session("HOST_NAME"))
            objTrainingApproverMaster._FormName = mstrModuleName1
            objTrainingApproverMaster._IsFromWeb = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub ResetAddedit()
        Try
            drpApproverUseraccess_level.SelectedIndex = 0
            drpApproverUseraccess_user.SelectedIndex = 0
            popupApproverUseraccess.Hide()
            blnpopupApproverUseraccess = False
            FillList(False)
            mintMappingUnkid = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub FillUserAccess(ByVal dt As DataTable)
        Try
            TvApproverUseraccess.DataSource = dt
            TvApproverUseraccess.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Dropdown Event "

    Protected Sub drpApproverUseraccess_user_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpApproverUseraccess_user.SelectedIndexChanged
        Dim objUser As New clsUserAddEdit
        Try
            Dim dtTable As DataTable = Nothing
            If CInt(drpApproverUseraccess_user.SelectedValue) > 0 Then
                dtTable = objUser.GetUserAccessList(Convert.ToString(Session("UserAccessModeSetting")), CInt(drpApproverUseraccess_user.SelectedValue), Convert.ToInt32(Session("CompanyUnkId")))
                dtTable = New DataView(dtTable, "isAssign = 1 OR isGrp = 1", "", DataViewRowState.CurrentRows).ToTable()
            End If
            FillUserAccess(dtTable)
            dtTable = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUser = Nothing
        End Try
    End Sub

#End Region

#Region " Button Event "

    Protected Sub btnApproverUseraccessClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproverUseraccessClose.Click

        Try
            blnpopupApproverUseraccess = False
            popupApproverUseraccess.Hide()
            ResetAddedit()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub btnApproverUseraccessSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproverUseraccessSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then Exit Sub

            SetValue()

            blnFlag = objTrainingApproverMaster.Insert()

            If blnFlag = False And objTrainingApproverMaster._Message <> "" Then
                DisplayMessage.DisplayMessage(objTrainingApproverMaster._Message, Me)
            End If

            DisplayMessage.DisplayMessage(Language.getMessage(mstrModuleName, 6, "Approver Saved Successfully"), Me)
            ResetAddedit()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub confirmapproverdelete_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles confirmapproverdelete.buttonYes_Click
        Try
            DeleteApprovalReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            drpLevel.SelectedIndex = 0
            drpApprover.SelectedIndex = 0
            drpStatus.SelectedIndex = 0
            Call FillList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

#End Region

#Region " Gridview Event "

    Protected Sub gvApproverList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApproverList.RowDataBound
        Dim oldid As String
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table
                If CInt(gvApproverList.DataKeys(e.Row.RowIndex)("mappingunkid").ToString) > 0 Then
                    oldid = dt.Rows(e.Row.RowIndex)("Level").ToString()
                    If dt.Rows(e.Row.RowIndex)("IsGrp").ToString() = True AndAlso oldid <> currentId Then
                        Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("Level").ToString(), gvApproverList)
                        currentId = oldid
                    Else
                        Dim lnkactive As LinkButton = TryCast(e.Row.FindControl("lnkactive"), LinkButton)
                        Dim lnkDeactive As LinkButton = TryCast(e.Row.FindControl("lnkDeactive"), LinkButton)
                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)

                        'If CBool(Session("AllowToEditGrievanceApprover")) Then
                        '    lnkedit.Visible = True
                        'Else
                        '    lnkedit.Visible = False
                        'End If

                        If CBool(Session("AllowToDeleteTrainingApprover")) Then
                            lnkdelete.Visible = True
                        Else
                            lnkdelete.Visible = False
                        End If

                        If CBool(Session("AllowToActivateTrainingApprover")) Then
                            lnkactive.Visible = True
                        Else
                            lnkactive.Visible = False
                        End If

                        If CBool(Session("AllowToDeactivateTrainingApprover")) Then
                            lnkDeactive.Visible = True
                        Else
                            lnkDeactive.Visible = False
                        End If

                        If dt.Rows(e.Row.RowIndex)("isactive").ToString() <> "" Then
                            If dt.Rows(e.Row.RowIndex)("isactive").ToString() = True Then
                                lnkactive.Visible = False
                                lnkDeactive.Visible = True
                            Else
                                lnkactive.Visible = True
                                lnkDeactive.Visible = False
                            End If
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            currentId = ""
            oldid = ""
        End Try
    End Sub

    Protected Sub TvApproverUseraccess_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles TvApproverUseraccess.RowDataBound
        Dim oldid As String
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table

                oldid = dt.Rows(e.Row.RowIndex)("UserAccess").ToString()
                If dt.Rows(e.Row.RowIndex)("AllocationLevel").ToString() = -1 AndAlso oldid <> currentId Then
                    Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("UserAccess").ToString(), TvApproverUseraccess)
                    currentId = oldid
                Else
                    Dim statusCell As TableCell = e.Row.Cells(0)
                    statusCell.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dt.Rows(e.Row.RowIndex)("UserAccess").ToString
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            currentId = ""
            oldid = ""
        End Try
    End Sub

#End Region

#End Region

End Class
