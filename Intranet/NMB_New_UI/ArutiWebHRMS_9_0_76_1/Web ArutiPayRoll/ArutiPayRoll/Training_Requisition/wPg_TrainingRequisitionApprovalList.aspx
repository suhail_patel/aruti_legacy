﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_TrainingRequisitionApprovalList.aspx.vb"
    Inherits="Training_Requisition_wpg_TrainingRequisitionApprovalList" Title="Training Requisition Approval List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>
    <style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>
    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Training Requisition Approval List"
                            CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblUser" runat="server" Text="User" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtUser" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lbllevel" runat="server" Text="Level" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtlevel" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblemp" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpemp" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <div style="float: left">
                                    <asp:CheckBox ID="chkMyApproval" runat="server" Text="My Approval" Checked="true"
                                        Font-Bold="true" />
                                </div>
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 300px;">
                                            <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto">
                                                <asp:GridView ID="gvTrainingApprovalList" runat="server" AutoGenerateColumns="false"
                                                    AllowPaging="false" Width="100%" HeaderStyle-Font-Bold="false" DataKeyNames="mapuserunkid,priority,mappingunkid,iStatusId,iStatus,employeeunkid,linkedmasterid,uempid,ecompid,crmasterunkid"
                                                     CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgSelect" runat="server" ToolTip="Change Status" OnClick="lnkChangeStatus_Click"
                                                                        CommandArgument='<%#Eval("masterguid")%>'>
                                                                        <i class="fas fa-pencil-alt"></i>
                                                                    </asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--<asp:BoundField DataField="ecode" HeaderText="Code" ReadOnly="true" FooterText="colhecode" />--%>
                                                        <asp:BoundField DataField="ename" HeaderText="Employee" ReadOnly="true" FooterText="colhEmployee" />
                                                        <asp:BoundField DataField="CourseMaster" HeaderText="Training Name" ReadOnly="True"
                                                            FooterText="colhtrainingname" />
                                                        <asp:BoundField DataField="institute_name" HeaderText="Vendor" ReadOnly="True" FooterText="colhvendor" />
                                                        <asp:BoundField DataField="TrainingMode" HeaderText="Training Mode" ReadOnly="True"
                                                            FooterText="colhRefNo" />
                                                        <asp:BoundField DataField="training_startdate" HeaderText="Start Date" ReadOnly="True"
                                                            FooterText="colhstartdate" />
                                                        <asp:BoundField DataField="training_enddate" HeaderText="End Date" ReadOnly="True"
                                                            FooterText="colhenddate" />
                                                        <asp:BoundField DataField="Duration" HeaderText="Duration" ReadOnly="True" FooterText="colhduration" />
                                                        <asp:BoundField DataField="training_venue" HeaderText="Venue" ReadOnly="True" FooterText="colhvenue" />
                                                        <asp:BoundField DataField="additonal_comments" HeaderText="Comments" ReadOnly="True"
                                                            FooterText="colhcomments" />
                                                        <asp:BoundField DataField="iStatus" HeaderText="Status" ReadOnly="True" FooterText="colhstatus" />
                                                        <asp:BoundField DataField="username" HeaderText="Approver" ReadOnly="true" FooterText="colhApprover" />
                                                        <asp:BoundField DataField="levelname" HeaderText="Level" ReadOnly="true" FooterText="colhApprovelevel" />
                                                        <%--'S.SANDEEP [07-NOV-2018] -- START--%>
                                                        <asp:BoundField HeaderText="Training Type" DataField="trainingtype" ItemStyle-VerticalAlign="Top"
                                                            FooterText="colhtrainingtype" />
                                                        <%--'S.SANDEEP [07-NOV-2018] -- END--%>
                                                    </Columns>
                                                </asp:GridView>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" Visible="False" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
