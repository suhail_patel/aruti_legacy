﻿#Region " Imports "

Imports Aruti.Data
Imports System.Data
Imports System.Globalization
Imports System.IO

#End Region

Partial Class Training_Requisition_wpg_TrainingRequisitionApprovalList
    Inherits Basepage

#Region " Private Variable "

    Private DisplayMessage As New CommonCodes
    Private objRequisitionApproval As New clstraining_requisition_approval_master
    Private ReadOnly mstrModuleName As String = "frmTrainingRequisitionApprovalList"
    Private mblnIsPreviousPending As Boolean = False
    Private mblnIsPreviousRejected As Boolean = False
    Private mstrRejectedCaption As String = String.Empty
    Private mintEmployeeId As Integer = 0

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objEmp As New clsEmployee_Master
        Dim dsCombo As New DataSet
        Try
            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString(), _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                             Session("UserAccessModeSetting").ToString(), True, _
                                             False, "List", True)
            With drpemp
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetApprovalData()
        Dim objApprover As New clstraining_approver_master
        Dim dsList As New DataSet
        Try
            dsList = objApprover.GetList("List", True, "", CInt(Session("UserId")), Nothing, False)
            If dsList.Tables("List").Rows.Count > 0 Then
                txtUser.Text = dsList.Tables("List").Rows(0).Item("approver").ToString()
                txtlevel.Text = dsList.Tables("List").Rows(0).Item("Level").ToString()
                txtUser.Attributes.Add("mappingunkid", dsList.Tables("List").Rows(0).Item("mappingunkid").ToString())
                txtlevel.Attributes.Add("priority", dsList.Tables("List").Rows(0).Item("priority").ToString())
                txtlevel.Attributes.Add("levelunkid", dsList.Tables("List").Rows(0).Item("levelunkid").ToString())
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objApprover = Nothing
            dsList.Dispose()
        End Try
    End Sub

    Private Sub ClearAttributes()
        Try
            txtUser.Attributes.Remove("mappingunkid") : txtlevel.Attributes.Remove("priority") : txtlevel.Attributes.Remove("levelunkid")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Dim dtData As New DataTable
        Dim blnFlag As Boolean = False
        Try
            If txtUser.Text.Trim.Length > 0 AndAlso txtlevel.Text.Length > 0 Then
                dtData = objRequisitionApproval.GetEmployeeApprovalData(Session("Database_Name").ToString(), _
                                                                    CInt(Session("CompanyUnkId")), _
                                                                    CInt(Session("Fin_year")), _
                                                                    Session("UserAccessModeSetting").ToString(), _
                                                                    1224, CInt(Session("UserId")), _
                                                                    CInt(txtlevel.Attributes("priority")), _
                                                                    False, Session("EmployeeAsOnDate").ToString(), _
                                                                    True, Nothing)
                If dtData.Rows.Count > 0 Then
                    Dim strFilter As String = String.Empty
                    If CInt(IIf(drpemp.SelectedValue = "", 0, drpemp.SelectedValue)) > 0 Then
                        strFilter &= "AND employeeunkid = " & CInt(IIf(drpemp.SelectedValue = "", 0, drpemp.SelectedValue))
                    End If
                    strFilter &= "AND priority <= " & CInt(txtlevel.Attributes("priority"))
                    If strFilter.Length > 0 Then
                        strFilter = strFilter.Substring(3)
                    End If


                    dtData = New DataView(dtData, strFilter, "employeeunkid,priority", DataViewRowState.CurrentRows).ToTable()

                    'If dtData.Rows.Count > 0 Then
                    '    Dim iPriority As List(Of Integer) = dtData.AsEnumerable().Select(Function(x) x.Field(Of Integer)("priority")).Distinct().ToList()
                    '    Dim intSecondLastPriority As Integer = -1
                    '    If iPriority.Min() = CInt(txtlevel.Attributes("priority")) Then
                    '        intSecondLastPriority = CInt(txtlevel.Attributes("priority"))
                    '        blnFlag = True
                    '    Else
                    '        Try
                    '            intSecondLastPriority = iPriority.Item(iPriority.IndexOf(CInt(txtlevel.Attributes("priority"))) - 1)
                    '        Catch ex As Exception
                    '            intSecondLastPriority = CInt(txtlevel.Attributes("priority"))
                    '            blnFlag = True
                    '        End Try
                    '        Dim dr() As DataRow = Nothing
                    '        dr = dtData.Select("priority = '" & intSecondLastPriority & "'", "employeeunkid, priority")
                    '        If dr.Length > 0 Then
                    '            Dim row = dr.AsEnumerable().Where(Function(x) x.Field(Of Integer)("iStatusId") = CInt(clsEmployee_Master.EmpApprovalStatus.Approved))
                    '            Dim strIds As String
                    '            If row.Count() > 0 Then
                    '                strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList().Except(row.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).ToList()).Distinct().ToArray())
                    '                If blnFlag Then
                    '                    strIds = ""
                    '                End If
                    '                If strIds.Trim.Length > 0 Then
                    '                    dtData = New DataView(dtData, "userunkid = " & CInt(Session("UserId")) & " AND iStatusId = 1 AND employeeunkid NOT IN (" & strIds & ") ", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                    '                Else
                    '                    dtData = New DataView(dtData, "userunkid = " & CInt(Session("UserId")) & " AND iStatusId = 1", "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                    '                End If
                    '            Else
                    '                strIds = String.Join(",", dr.Select(Function(x) x.Field(Of Integer)("employeeunkid").ToString()).Distinct().ToArray())
                    '                If blnFlag = False Then
                    '                    strIds = " AND employeeunkid NOT IN(" & strIds & ") "
                    '                Else
                    '                    strIds = ""
                    '                End If
                    '                dtData = New DataView(dtData, "userunkid = " & CInt(Session("UserId")) & " AND iStatusId = 1 " & strIds, "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable()
                    '            End If
                    '        Else
                    '            dtData = New DataView(dtData, "userunkid = " & CInt(Session("UserId")), "employeeunkid, priority", DataViewRowState.CurrentRows).ToTable() '& " AND iStatusId = 1 "
                    '        End If
                    '    End If
                    'End If

                End If

                If dtData.Rows.Count <= 0 Then
                    dtData.Rows.Add(dtData.NewRow())
                    dtData.Rows(0).Item("training_startdate") = ""
                    dtData.Rows(0).Item("training_enddate") = ""
                    dtData.Rows(0).Item("priority") = 0
                    dtData.Rows(0).Item("iStatusId") = 1
                    dtData.Rows(0).Item("employeeunkid") = 0
                    dtData.Rows(0).Item("mapuserunkid") = 0
                    blnFlag = True
                End If

                gvTrainingApprovalList.DataSource = dtData
                gvTrainingApprovalList.DataBind()
                If blnFlag Then
                    gvTrainingApprovalList.Rows(0).Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Form Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If (Page.IsPostBack = False) Then
                FillCombo()
                SetApprovalData()
                FillGrid()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If txtUser.Text.Trim.Length <= 0 AndAlso txtlevel.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage("Sorry, No approver mapped/defined with this logged in user.", Me)
                Exit Sub
            End If
            FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    Protected Sub lnkChangeStatus_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)

            If CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("uempid")) = CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("employeeunkid")) AndAlso CInt(gvTrainingApprovalList.DataKeys(row.RowIndex)("ecompid")) = CInt(Session("CompanyUnkId")) Then
                DisplayMessage.DisplayMessage("Sorry, you cannot approve/reject your own training requisition. Please contact aruti administrator to make new approver with same level.", Me)
            End If

            Session("mblnIsAddMode") = True
            Session("mblnFromApproval") = True
            Session("mintMappingUnkid") = txtUser.Attributes("mappingunkid")
            Session("mintLinkedMasterId") = gvTrainingApprovalList.DataKeys(row.RowIndex)("linkedmasterid")
            Session("RequisitionMasterguid") = Convert.ToString(lnk.CommandArgument.ToString())
            Session("mintClaimRequestMasterId") = gvTrainingApprovalList.DataKeys(row.RowIndex)("crmasterunkid")
            Response.Redirect("~\Training_Requisition\wPgApplyTrainingRequisition.aspx")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Grid Event(s) "

    Protected Sub gvTrainingApprovalList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvTrainingApprovalList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvTrainingApprovalList, "colhstartdate", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvTrainingApprovalList, "colhstartdate", False, True)).Text = eZeeDate.convertDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvTrainingApprovalList, "colhstartdate", False, True)).Text).ToShortDateString()
                End If
                If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvTrainingApprovalList, "colhenddate", False, True)).Text <> "&nbsp;" Then
                    e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvTrainingApprovalList, "colhenddate", False, True)).Text = eZeeDate.convertDate(e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvTrainingApprovalList, "colhenddate", False, True)).Text).ToShortDateString()
                End If
                Dim lnkselect As LinkButton = TryCast(e.Row.FindControl("ImgSelect"), LinkButton)

                If mintEmployeeId <> CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("employeeunkid")) Then
                    mintEmployeeId = CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("employeeunkid"))
                    mblnIsPreviousRejected = False : mblnIsPreviousPending = False : mstrRejectedCaption = ""
                End If

                If CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("priority")) < CInt(txtlevel.Attributes("priority")) Then
                    If CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("iStatusId")) = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval Then
                        lnkselect.Visible = False
                        mblnIsPreviousPending = True
                    ElseIf CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("iStatusId")) <> clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval Then
                        lnkselect.Visible = False
                        If CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("iStatusId")) = clstraining_requisition_approval_master.enApprovalStatus.Rejected Then
                            mblnIsPreviousRejected = True
                            mstrRejectedCaption = gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("iStatus").ToString()
                        End If
                    End If

                    If chkMyApproval.Checked Then
                        If CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("iStatusId")) <> clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval Then
                            e.Row.Visible = False
                        End If
                    End If

                Else
                    If CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("mapuserunkid")) <> CInt(Session("UserId")) Then
                        lnkselect.Visible = False
                    ElseIf CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("mapuserunkid")) = CInt(Session("UserId")) Then
                        If CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("iStatusId")) = clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval Then
                            If mblnIsPreviousPending = True Then lnkselect.Visible = False
                        ElseIf CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("iStatusId")) <> clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval Then
                            lnkselect.Visible = False
                        End If
                    ElseIf CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("iStatusId")) <> clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval Then
                        If mblnIsPreviousPending = True Then lnkselect.Visible = False
                    End If
                    If mblnIsPreviousRejected Then
                        e.Row.Visible = False
                    End If
                    If chkMyApproval.Checked Then
                        If CInt(gvTrainingApprovalList.DataKeys(e.Row.RowIndex)("iStatusId")) <> clstraining_requisition_approval_master.enApprovalStatus.SubmitForApproval Then
                            e.Row.Visible = False
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

End Class
