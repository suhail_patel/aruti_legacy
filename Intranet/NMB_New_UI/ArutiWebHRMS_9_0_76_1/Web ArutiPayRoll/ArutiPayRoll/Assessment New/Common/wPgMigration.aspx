<%@ Page Title="Transfer Assessor/Reviewer Migration" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPgMigration.aspx.vb" Inherits="wPgMigration" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="EmployeeList" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        var scroll2 = {
            Y: '#<%= hfScrollPosition2.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            SetGeidScrolls();
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
                $("#scrollable-container2").scrollTop($(scroll2.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("overflow", "auto");
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("overflow", "none");
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    </script>

    <script language="javascript" type="text/javascript">
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function gridsearching(ctrl) {
            var txtSearch = $(ctrl);
            switch (txtSearch[0].id) {
                case 'txtFrmSearch':
                    if ($(txtSearch).val().length > 0) {
                        $('#<%=dgvFrmEmp.ClientID %> tbody tr').hide();
                        $('#<%=dgvFrmEmp.ClientID %> tbody tr:first').show();
                        $('#<%=dgvFrmEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtFrmSearch').val() + '\')').parent().show();
                    }
                    else if ($(txtSearch).val().length == 0) {
                        $('#txtToSearch').val('');
                        $('#<%=dgvFrmEmp.ClientID %> tr').show();
                        $('#txtFrmSearch').focus();
                    }
                    break;
                case 'txtToSearch':
                    if ($(txtSearch).val().length > 0) {
                        $('#<%=dgvAssignedEmp.ClientID %> tbody tr').hide();
                        $('#<%=dgvAssignedEmp.ClientID %> tbody tr:first').show();
                        $('#<%=dgvAssignedEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtToSearch').val() + '\')').parent().show();
                    }
                    else if ($(txtSearch).val().length == 0) {
                        $('#txtToSearch').val('');
                        $('#<%=dgvAssignedEmp.ClientID %> tr').show();
                        $('#txtToSearch').focus();
                    }
                    break;
            }
        }

        $("body").on("click", "[id*=chkHeder1]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkbox1]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=chkbox1]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeder1]", grid);
            debugger;
            if ($("[id*=chkbox1]", grid).length == $("[id*=chkbox1]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Transfer Assessor/Reviewer"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text=""></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <asp:RadioButtonList ID="radMode" runat="server" RepeatDirection="Horizontal" AutoPostBack="true">
                                            <asp:ListItem Text="Assessor" Value="1" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Reviewer" Value="2"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblFromApprover" runat="server" Text="From " CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboFrmApprover" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="txtFrmSearch" name="txtSearch" placeholder="type search text"
                                                        maxlength="50" style="height: 25px; font: 100" onkeyup="gridsearching(this);"
                                                        class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="height: 447px">
                                                <asp:GridView ID="dgvFrmEmp" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                    Width="99%" AllowPaging="false" DataKeyNames="assessortranunkid,employeeunkid">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" CssClass="filled-in" Text=" " />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" CssClass="filled-in" Text=" "
                                                                    CommandArgument='<%# Container.DataItemIndex %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="employeecode" HeaderText="Code" />
                                                        <asp:BoundField DataField="name" HeaderText="Employee" />
                                                        <asp:BoundField DataField="employeeunkid" HeaderText="employeeunkid" Visible="false" />
                                                        <asp:BoundField DataField="assessormasterunkid" HeaderText="assessormasterunkid"
                                                            Visible="false" />
                                                        <asp:BoundField DataField="assessortranunkid" HeaderText="assessortranunkid" Visible="false" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblToApprover" runat="server" Text="To " CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <asp:DropDownList ID="cboToApprover" runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" id="txtToSearch" name="txtSearch" placeholder="type search text"
                                                        maxlength="50" style="height: 25px; font: 100" onkeyup="gridsearching(this);"
                                                        class="form-control" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="height: 447px">
                                                <asp:GridView ID="dgvAssignedEmp" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                    Width="99%" AllowPaging="false" DataKeyNames="employeeunkid">
                                                    <Columns>
                                                        <asp:BoundField DataField="employeecode" HeaderText="Code" />
                                                        <asp:BoundField DataField="name" HeaderText="Employee" />
                                                        <asp:BoundField DataField="employeeunkid" HeaderText="employeeunkid" Visible="false" />
                                                        <asp:BoundField DataField="assessormasterunkid" HeaderText="assessormasterunkid"
                                                            Visible="false" />
                                                        <asp:BoundField DataField="assessortranunkid" HeaderText="assessortranunkid" Visible="false" />
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <asp:RadioButtonList runat="server" ID="radOperation" RepeatDirection="Vertical"
                                            ForeColor="Red">
                                            <asp:ListItem Text="Overwrite assessment done for migrating assessor/reviewer for the selected employee for all open assessment period."
                                                Value="1" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Void assessment done for migrating assessor/reviewer for the selected employee for all open assessment period."
                                                Value="2"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Label ID="lblNote" runat="server" Text="Please tick desired employee(s) and press transfer button in order to do migration process."
                                    ForeColor="Red" CssClass="form-label pull-left"></asp:Label>
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btnDefault" Visible="false" />
                                <asp:Button ID="btnTransfer" runat="server" Text="Transfer" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="panel-primary">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaBody" class="panel-body-default" style="position: relative">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 45%; vertical-align: top">
                                            <div class="panel-body">
                                                <div id="Div1" class="panel-default">
                                                    <div id="Div2" class="panel-heading-default">
                                                        <div style="float: left;">
                                                        </div>
                                                    </div>
                                                    <div id="Div3" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 25%">
                                                                </td>
                                                                <td style="width: 75%">
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%" colspan="2">
                                                                   
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%" colspan="2">
                                                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="overflow: auto;
                                                                        height: 447px" class="gridscroll">
                                                                        <asp:Panel ID="pnl_dgvFrmEmp" runat="server">
                                                                        </asp:Panel>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="width: 10%; vertical-align: middle">
                                            <table id="tblOperation" style="width: 100%;">
                                                <tr style="width: 100%">
                                                    <td align="center">
                                                        <asp:Button ID="objbtnAssign" runat="server" Text=">>" CssClass="btnDefault" Visible="false" />
                                                    </td>
                                                </tr>
                                                <tr style="width: 100%">
                                                    <td align="center">
                                                        <asp:Button ID="objbtnUnAssign" runat="server" Text="<<" CssClass="btnDefault" Visible="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td style="width: 45%; vertical-align: top">
                                            <div class="panel-body">
                                                <div id="Div4" class="panel-default">
                                                    <div id="Div5" class="panel-heading-default">
                                                        <div style="float: left; margin-top: -5px">
                                                        </div>
                                                    </div>
                                                    <div id="Div6" class="panel-body-default">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 25%">
                                                                </td>
                                                                <td style="width: 75%">
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%" colspan="2">
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr style="width: 100%">
                                                                <td style="width: 100%" colspan="2">
                                                                    <table id="mView" style="width: 100%">
                                                                        <tr style="width: 100%;">
                                                                            <td style="width: 100%">
                                                                                <asp:MultiView ActiveViewIndex="1" ID="mltiview" runat="server">
                                                                                    <asp:View ID="vwStep1" runat="server">
                                                                                        <table style="width: 100%">
                                                                                            <tr style="width: 100%;">
                                                                                                <td style="width: 100%; border-radius: 0px" class="grpheader">
                                                                                                    <h4>
                                                                                                        <asp:Label ID="lblCaption1" runat="server" Text="Migrated Employee"></asp:Label>
                                                                                                    </h4>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr style="width: 100%">
                                                                                                <td style="width: 100%">
                                                                                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="height: 350px;
                                                                                                        overflow: auto" class="gridscroll">
                                                                                                        <asp:Panel ID="pnl_dgvToEmp" ScrollBars="Auto" Style="margin-left: -5px; margin-right: -2px"
                                                                                                            runat="server">
                                                                                                            <asp:GridView ID="dgvToEmp" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                                                                                                                HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                                                HeaderStyle-Font-Bold="false" Width="99%">
                                                                                                                <Columns>
                                                                                                                    <asp:TemplateField ItemStyle-Width="25">
                                                                                                                        <HeaderTemplate>
                                                                                                                            <asp:CheckBox ID="chkHeder2" runat="server" Enabled="true" AutoPostBack="true" />
                                                                                                                        </HeaderTemplate>
                                                                                                                        <ItemTemplate>
                                                                                                                            <asp:CheckBox ID="chkbox2" runat="server" Enabled="true" CommandArgument='<%# Container.DataItemIndex %>'
                                                                                                                                AutoPostBack="true" />
                                                                                                                        </ItemTemplate>
                                                                                                                    </asp:TemplateField>
                                                                                                                    <asp:BoundField DataField="employeecode" HeaderText="Code" />
                                                                                                                    <asp:BoundField DataField="name" HeaderText="Employee" />
                                                                                                                    <asp:BoundField DataField="employeeunkid" HeaderText="employeeunkid" Visible="false" />
                                                                                                                    <asp:BoundField DataField="assessormasterunkid" HeaderText="assessormasterunkid"
                                                                                                                        Visible="false" />
                                                                                                                    <asp:BoundField DataField="assessortranunkid" HeaderText="assessortranunkid" Visible="false" />
                                                                                                                </Columns>
                                                                                                            </asp:GridView>
                                                                                                        </asp:Panel>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:View>
                                                                                    <asp:View ID="vwStep2" runat="server">
                                                                                        <table style="width: 100%">
                                                                                            <tr style="width: 100%;">
                                                                                                <td style="width: 100%; border-radius: 0px" class="grpheader">
                                                                                                    <h4>
                                                                                                        <asp:Label ID="lblCaption2" runat="server" Text="Assigned Employee"></asp:Label>
                                                                                                    </h4>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr style="width: 100%;">
                                                                                                <td style="width: 100%">
                                                                                                    <asp:Panel ID="pnl_dgvAssignedEmp" ScrollBars="Auto" Style="margin-left: -5px; margin-right: -2px"
                                                                                                        Height="350px" runat="server" CssClass="gridscroll">
                                                                                                    </asp:Panel>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </asp:View>
                                                                                </asp:MultiView>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <div class="btn-default" style="display: none">
                                                            <asp:Button ID="btnBack" runat="server" Text="Back" Width="75px" CssClass="btnDefault"
                                                                Visible="false" />
                                                            <asp:Button ID="btnNext" runat="server" Text="Next" Width="75px" CssClass="btnDefault"
                                                                Visible="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 100%">
                                        </td>
                                    </tr>
                                </table>
                                <div id="btnfixedbottom" class="btn-default">
                                    <div style="float: left">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
