﻿#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data

#End Region

Partial Class wPgMigration
    Inherits Basepage

#Region " Private Variables "

    Private clsuser As New User
    Private DisplayMessage As New CommonCodes
    Private dtOldEmp As DataTable
    Private dtNewEmp As DataTable
    Private dtAssignedEmp As DataTable
    Private mstrModuleName As String = "frmMigration"

#End Region

#Region " Page Events "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                'Anjan [20 February 2016] -- End


                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            Blank_ModuleName()
            clsCommonATLog._WEBFormName = "frmMigration"
            StrModuleName1 = "mnuHumanResource"
            StrModuleName2 = "mnuAssessment"
            StrModuleName3 = "mnuSetups"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            'btnSave.Visible = Session("AllowToMigrateAssessor_Reviewer")
            'btnSave.Visible = Session("AllowtoPerformAssessorReviewerMigration")
            btnTransfer.Visible = Session("AllowtoPerformAssessorReviewerMigration")
            'S.SANDEEP [28 MAY 2015] -- END

            If IsPostBack = False Then
                Call radMode_SelectedIndexChanged(New Object, New EventArgs)
                'btnBack.Visible = False
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function & Procedures "

    Private Sub FillCombo()
        Dim objAR As New clsAssessor
        Dim dsList As New DataSet
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If radMode.SelectedValue = "1" Then
            'dsList = objAR.GetList("List", False, True)
            'ElseIf radMode.SelectedValue = "2" Then
            '    dsList = objAR.GetList("List", True, True)
            'End If
            Dim blnReviewerFlag As Boolean = False
            If radMode.SelectedValue = "1" Then
                blnReviewerFlag = False
            ElseIf radMode.SelectedValue = "2" Then
                blnReviewerFlag = True
            End If
            dsList = objAR.GetList(Session("Database_Name"), _
                                   Session("UserId"), _
                                   Session("Fin_year"), _
                                   Session("CompanyUnkId"), _
                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                   Session("UserAccessModeSetting"), True, _
                                   Session("IsIncludeInactiveEmp"), "List", blnReviewerFlag, clsAssessor.enARVisibilityTypeId.VISIBLE, True)
            'Shani (12-Jan-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]

            'Shani(24-Aug-2015) -- End
            With cboFrmApprover
                .DataValueField = "assessormasterunkid"
                .DataTextField = "assessorname"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With
            'If Me.ViewState("cboFrmApprover") Is Nothing Then
            '    Me.ViewState.Add("cboFrmApprover", dsList.Tables(0))
            'Else
            '    Me.ViewState("cboFrmApprover") = dsList.Tables(0)
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_Assessor_Emp()
        Dim objAssessorTran As New clsAssessor_tran
        Try
            If CInt(IIf(cboFrmApprover.SelectedValue = "", 0, cboFrmApprover.SelectedValue)) > 0 Then
                objAssessorTran._EmployeeAsOnDate = Session("EmployeeAsOnDate").ToString
                objAssessorTran._AssessorMasterId = CInt(IIf(cboFrmApprover.SelectedValue = "", 0, cboFrmApprover.SelectedValue))
                dtOldEmp = objAssessorTran._DataTable.Copy
                dgvFrmEmp.AutoGenerateColumns = False
                dgvFrmEmp.DataSource = dtOldEmp
                dgvFrmEmp.DataBind()
                    End If
            'If Me.ViewState("cboFrmApprover") IsNot Nothing Then
            '    Dim itmp() As DataRow = CType(Me.ViewState("cboFrmApprover"), DataTable).Select("assessormasterunkid = '" & CInt(cboFrmApprover.SelectedValue) & "'")
            '    If itmp.Length > 0 Then

            '        'Shani(29-Nov-2016) -- Start
            '        'Issue : TRA fix for assessor migration.
            '        'Shani(01-MAR-2016) -- Start
            '        'Enhancement
            '        'objAssessorTran._AssessorEmployeeId = CInt(itmp(0).Item("employeeunkid"))
            '        Dim evalue As clsAssessor.enAssessorType = clsAssessor.enAssessorType.ALL_DATA
            '        If radMode.SelectedValue = "1" Then
            '            evalue = clsAssessor.enAssessorType.ASSESSOR
            '        ElseIf radMode.SelectedValue = "2" Then
            '            evalue = clsAssessor.enAssessorType.REVIEWER
            '        End If

            '        'Shani (07-Dec-2016) -- Start
            '        'optimization -  optimization to Assessor/Reviewer Add/edit Screen (TRA)
            '        objAssessorTran._EmployeeAsOnDate = Session("EmployeeAsOnDate").ToString
            '        'Shani (07-Dec-2016) -- End


            '        objAssessorTran._AssessorEmployeeId(CBool(itmp(0).Item("isexternalapprover")), evalue) = CInt(itmp(0).Item("employeeunkid"))
            '        'Shani(01-MAR-2016) -- End
            '         'Shani(29-Nov-2016) -- End


            '        dtOldEmp = objAssessorTran._DataTable.Copy
            '    End If
            'End If

            'If radMode.SelectedValue = "1" Then
            '    dtOldEmp = New DataView(dtOldEmp, "isreviewer = 0", "", DataViewRowState.CurrentRows).ToTable
            'ElseIf radMode.SelectedValue = "2" Then
            '    dtOldEmp = New DataView(dtOldEmp, "isreviewer = 1", "", DataViewRowState.CurrentRows).ToTable
            'End If

            'If Me.ViewState("dtOldEmp") Is Nothing Then
            '    Me.ViewState.Add("dtOldEmp", dtOldEmp)
            'Else
            '    Me.ViewState("dtOldEmp") = dtOldEmp
            'End If

            'dtNewEmp = dtOldEmp.Clone
            'dtNewEmp.Rows.Clear()

            'If Me.ViewState("dtNewEmp") Is Nothing Then
            '    Me.ViewState.Add("dtNewEmp", dtNewEmp)
            'Else
            '    Me.ViewState("dtNewEmp") = dtNewEmp
            'End If


            'dgvFrmEmp.AutoGenerateColumns = False
            'dgvFrmEmp.DataSource = dtOldEmp
            'dgvFrmEmp.DataBind()

            'If Me.ViewState("gv1_FirstRecordNo") Is Nothing Then
            '    Me.ViewState.Add("gv1_FirstRecordNo", 0)
            'End If
            'If Me.ViewState("gv1_LastRecordNo") Is Nothing Then
            '    Me.ViewState.Add("gv1_LastRecordNo", dgvFrmEmp.PageSize)
            'End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_Assigned_Emp()
        Dim objAssessorTran As New clsAssessor_tran
        Try
            If CInt(IIf(cboToApprover.SelectedValue = "", 0, cboToApprover.SelectedValue)) > 0 Then
                    objAssessorTran._EmployeeAsOnDate = Session("EmployeeAsOnDate").ToString
                objAssessorTran._AssessorMasterId = CInt(IIf(cboToApprover.SelectedValue = "", 0, cboToApprover.SelectedValue))
                    dtAssignedEmp = objAssessorTran._DataTable.Copy
                dgvAssignedEmp.AutoGenerateColumns = False
                dgvAssignedEmp.DataSource = dtAssignedEmp
                dgvAssignedEmp.DataBind()
            End If
            'If Me.ViewState("cboToApprover") IsNot Nothing Then
            '    Dim itmp() As DataRow = CType(Me.ViewState("cboToApprover"), DataTable).Select("assessormasterunkid = '" & CInt(cboToApprover.SelectedValue) & "'")
            '    If itmp.Length > 0 Then

            '        'Shani(29-Nov-2016) -- Start
            '        'Issue : TRA fix for assessor migration.    
            '        'Shani(01-MAR-2016) -- Start
            '        'Enhancement
            '        'objAssessorTran._AssessorEmployeeId = CInt(itmp(0).Item("employeeunkid"))
            '        Dim evalue As clsAssessor.enAssessorType = clsAssessor.enAssessorType.ALL_DATA
            '        If radMode.SelectedValue = "1" Then
            '            evalue = clsAssessor.enAssessorType.ASSESSOR
            '        ElseIf radMode.SelectedValue = "2" Then
            '            evalue = clsAssessor.enAssessorType.REVIEWER
            '        End If

            '        'Shani (07-Dec-2016) -- Start
            '        'optimization -  optimization to Assessor/Reviewer Add/edit Screen (TRA)
            '        objAssessorTran._EmployeeAsOnDate = Session("EmployeeAsOnDate").ToString
            '        'Shani (07-Dec-2016) -- End
            '        objAssessorTran._AssessorEmployeeId(CBool(itmp(0).Item("isexternalapprover")), evalue) = CInt(itmp(0).Item("employeeunkid"))
            '        'Shani(01-MAR-2016) -- End
            '         'Shani(29-Nov-2016) -- End


            '        dtAssignedEmp = objAssessorTran._DataTable.Copy
            '    End If
            '    If radMode.SelectedValue = "1" Then
            '        dtAssignedEmp = New DataView(dtAssignedEmp, "isreviewer = 0", "", DataViewRowState.CurrentRows).ToTable
            '    ElseIf radMode.SelectedValue = "2" Then
            '        dtAssignedEmp = New DataView(dtAssignedEmp, "isreviewer = 1", "", DataViewRowState.CurrentRows).ToTable
            '    End If

            '    dgvAssignedEmp.AutoGenerateColumns = False
            '    dgvAssignedEmp.DataSource = dtAssignedEmp
            '    dgvAssignedEmp.DataBind()

            '    If Me.ViewState("gv1_FirstRecordNo") Is Nothing Then
            '        Me.ViewState.Add("gv1_FirstRecordNo", 0)
            '    End If
            '    If Me.ViewState("gv1_LastRecordNo") Is Nothing Then
            '        Me.ViewState.Add("gv1_LastRecordNo", dgvFrmEmp.PageSize)
            '    End If

            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Is_Valid_Data(Optional ByVal blnSave As Boolean = False) As Boolean
        Try
            If radMode.SelectedValue = "1" Then
                If CInt(cboFrmApprover.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage("Please select From Assessor in order to migrate data.", Me)
                    Return False
                End If

                If CInt(cboToApprover.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage("Please select To Assessor in order to migrate data.", Me)
                    Return False
                End If
            ElseIf radMode.SelectedValue = "2" Then
                If CInt(cboFrmApprover.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage("Please select From Reviewer in order to migrate data.", Me)
                    Return False
                End If

                If CInt(cboToApprover.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage("Please select To Reviewer in order to migrate data.", Me)
                    Return False
                End If
            End If
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvFrmEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.Cells(0).FindControl("chkbox1"), CheckBox).Checked = True)
            If gRow.Count <= 0 Then
                        DisplayMessage.DisplayMessage("Please Check atleast one employee to migrate.", Me)
                        Return False
                    End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    'Private Sub Fill_Migrated_Emp()
    '    Try
    '        If dtNewEmp Is Nothing Then
    '            dtNewEmp = Me.ViewState("dtNewEmp")
    '        End If

    '        dgvToEmp.AutoGenerateColumns = False
    '        dgvToEmp.DataSource = dtNewEmp
    '        dgvToEmp.DataBind()

    '        If Me.ViewState("gv2_FirstRecordNo") Is Nothing Then
    '            Me.ViewState.Add("gv2_FirstRecordNo", 0)
    '        End If
    '        If Me.ViewState("gv2_LastRecordNo") Is Nothing Then
    '            Me.ViewState.Add("gv2_LastRecordNo", dgvToEmp.PageSize)
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub
    'Private Sub Fill_UnAssigned_Emp()
    '    Try
    '        If Me.ViewState("dtOldEmp") IsNot Nothing Then
    '            dtOldEmp = Me.ViewState("dtOldEmp")
    '        End If

    '        dgvFrmEmp.AutoGenerateColumns = False
    '        dgvFrmEmp.DataSource = dtOldEmp
    '        dgvFrmEmp.DataBind()

    '        If Me.ViewState("gv1_FirstRecordNo") Is Nothing Then
    '            Me.ViewState.Add("gv1_FirstRecordNo", 0)
    '        End If
    '        If Me.ViewState("gv1_LastRecordNo") Is Nothing Then
    '            Me.ViewState.Add("gv1_LastRecordNo", dgvFrmEmp.PageSize)
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub

#End Region

#Region " Button's Events "

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub
    'Nilay (01-Feb-2015) -- End


    'Shani [ 24 DEC 2014 ] -- END

    'Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
    '    Try
    '        If mltiview.ActiveViewIndex > 0 Then
    '            mltiview.ActiveViewIndex -= 1
    '        End If
    '        btnNext.Visible = True : btnBack.Visible = False
    '        'txtToSearch.Text = ""
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub

    'Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
    '    Try
    '        If mltiview.ActiveViewIndex < mltiview.Views.Count Then
    '            mltiview.ActiveViewIndex += 1
    '        End If
    '        btnNext.Visible = False : btnBack.Visible = True
    '        'txtToSearch.Text = ""
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub

    'Protected Sub objbtnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
    '    Try
    '        If Is_Valid_Data() = False Then Exit Sub
    '        Dim objAssessor As New clsAssessor
    '        Dim blnFlag As Boolean = False
    '        If Me.ViewState("dtOldEmp") IsNot Nothing Then
    '            dtOldEmp = Me.ViewState("dtOldEmp")
    '        End If
    '        Dim dTemp() As DataRow = dtOldEmp.Select("ischeck = true")
    '        Dim iEmployeeId As Integer = 0
    '        If Me.ViewState("cboToApprover") IsNot Nothing Then
    '            Dim itmp() As DataRow = CType(Me.ViewState("cboToApprover"), DataTable).Select("assessormasterunkid = '" & CInt(cboToApprover.SelectedValue) & "'")
    '            If itmp.Length > 0 Then
    '                iEmployeeId = CInt(itmp(0).Item("employeeunkid"))
    '            End If
    '        End If
    '        For i As Integer = 0 To dTemp.Length - 1
    '            If iEmployeeId = CInt(dTemp(i)("employeeunkid")) Then
    '                If radMode.SelectedValue = "1" Then
    '                    DisplayMessage.DisplayMessage("Sorry, you cannot make same employee as his own assessor and will not be added to migration list.", Me)
    '                ElseIf radMode.SelectedValue = "2" Then
    '                    DisplayMessage.DisplayMessage("Sorry, you cannot make same employee as his own assessor and will not be added to migration list.", Me)
    '                End If
    '                Continue For
    '            End If
    '            If objAssessor.IsEmpMapped(CInt(IIf(cboToApprover.SelectedValue = "", 0, cboToApprover.SelectedValue)), CInt(dTemp(i)("employeeunkid")), clsAssessor.enARVisibilityTypeId.VISIBLE) Then
    '                If blnFlag = False Then
    '                    If radMode.SelectedValue = "1" Then
    '                        DisplayMessage.DisplayMessage("Sorry, Some of the checked employee is already binded with the selected approver and will not be added to the migration list.", Me)
    '                    ElseIf radMode.SelectedValue = "2" Then
    '                        DisplayMessage.DisplayMessage("Sorry, Some of the checked employee is already binded with the selected reviewer and will not be added to the migration list.", Me)
    '                    End If
    '                    blnFlag = True
    '                    Continue For
    '                Else
    '                    Continue For
    '                End If
    '            End If
    '            dTemp(i)("ischeck") = False
    '            If Me.ViewState("dtNewEmp") IsNot Nothing Then
    '                dtNewEmp = Me.ViewState("dtNewEmp")
    '                dtNewEmp.ImportRow(dTemp(i))
    '                dtOldEmp.Rows.Remove(dTemp(i))
    '            End If
    '        Next
    '        Me.ViewState("dtOldEmp") = dtOldEmp
    '        If Me.ViewState("dtNewEmp") Is Nothing Then
    '            Me.ViewState.Add("dtNewEmp", dtNewEmp)
    '        Else
    '            Me.ViewState("dtNewEmp") = dtNewEmp
    '        End If
    '        'Call Fill_Migrated_Emp() : Call Fill_UnAssigned_Emp()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub

    'Protected Sub objbtnUnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnUnAssign.Click
    '    Try
    '        If Me.ViewState("dtNewEmp") IsNot Nothing Then
    '            dtNewEmp = Me.ViewState("dtNewEmp")
    '        End If
    '        Dim dTemp() As DataRow = dtNewEmp.Select("ischeck = true")
    '        If dTemp.Length <= 0 Then
    '            DisplayMessage.DisplayMessage("Please Check atleast one employee to unassigned.", Me)
    '            Exit Sub
    '        End If
    '        If Me.ViewState("dtOldEmp") IsNot Nothing Then
    '            dtOldEmp = Me.ViewState("dtOldEmp")
    '        End If
    '        For i As Integer = 0 To dTemp.Length - 1
    '            dTemp(i)("ischeck") = False
    '            dtOldEmp.ImportRow(dTemp(i))
    '            dtNewEmp.Rows.Remove(dTemp(i))
    '        Next
    '        Me.ViewState("dtOldEmp") = dtOldEmp
    '        Me.ViewState("dtNewEmp") = dtNewEmp
    '        'Call Fill_UnAssigned_Emp() : Call Fill_Migrated_Emp()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebotton1.CloseButton_click


    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Dim objAssessorTran As New clsAssessor_tran
    '    Try
    '        If Is_Valid_Data(True) = False Then Exit Sub
    '        If Me.ViewState("dtNewEmp") IsNot Nothing Then
    '            dtNewEmp = Me.ViewState("dtNewEmp")
    '        End If
    '        If dtNewEmp.Rows.Count <= 0 Then
    '            DisplayMessage.DisplayMessage("There is no employee to migrate in migration list.", Me)
    '            Exit Sub
    '        End If
    '        If Me.ViewState("dtOldEmp") IsNot Nothing Then
    '            dtOldEmp = Me.ViewState("dtOldEmp")
    '        End If
    '        Dim itmp() As DataRow = CType(Me.ViewState("cboFrmApprover"), DataTable).Select("assessormasterunkid = '" & CInt(cboFrmApprover.SelectedValue) & "'")  'Shani(29-Nov-2016) -- Start

    '        Dim iEmpId, iToArEmpId As Integer : iEmpId = 0 : iToArEmpId = 0
    '        If itmp.Length > 0 Then
    '            iEmpId = CInt(itmp(0).Item("employeeunkid"))
    '        End If

    '        'Shani(01-MAR-2016) -- Start
    '        'Enhancement
    '        'If objAssessorTran.Perform_Migration(CInt(cboFrmApprover.SelectedValue), _
    '        '                                     dtNewEmp, _
    '        '                                     CInt(cboToApprover.SelectedValue), _
    '        '                                     IIf(radMode.SelectedValue = "1", True, False), _
    '        '                                     iEmpId, _
    '        '                                     Session("UserId")) = False Then

    '        'S.SANDEEP [27 DEC 2016] -- START
    '        'ENHANCEMENT : VISIBILITY IMPLEMENTATION IN ASSESSOR/REVIEWER
    '        Dim strVisibleTypeIds As Integer = clsAssessor.enARVisibilityTypeId.VISIBLE

    '        'S.SANDEEP [29-JUN-2017] -- START
    '        'itmp = CType(cboToApprover.DataSource, DataTable).Select("assessormasterunkid = '" & CInt(cboToApprover.SelectedValue) & "'")
    '        itmp = CType(Me.ViewState("cboToApprover"), DataTable).Select("assessormasterunkid = '" & CInt(cboToApprover.SelectedValue) & "'")
    '        'S.SANDEEP [29-JUN-2017] -- END

    '        If itmp.Length > 0 Then
    '            iToArEmpId = CInt(itmp(0).Item("employeeunkid"))
    '        End If
    '        'S.SANDEEP [27 DEC 2016] -- END

    '        'S.SANDEEP [19-NOV-2018] -- START
    '        objAssessorTran._EmployeeAsOnDate = Session("EmployeeAsOnDate").ToString()
    '        'S.SANDEEP [19-NOV-2018] -- END

    '        Dim IFromEmp() As DataRow = CType(Me.ViewState("cboFrmApprover"), DataTable).Select("assessormasterunkid = '" & CInt(cboFrmApprover.SelectedValue) & "'")
            'If objAssessorTran.Perform_Migration(CInt(cboFrmApprover.SelectedValue), _
            '                                     dtNewEmp, _
            '                                     CInt(cboToApprover.SelectedValue), _
            '                                     IIf(radMode.SelectedValue = "1", True, False), _
            '                                     iEmpId, _
    '                                             Session("UserId"), CBool(IFromEmp(0).Item("isexternalapprover")), _
    '                                             Session("EmployeeAsOnDate"), _
    '                                             CType(IIf(radOperation.SelectedValue = 1, clsAssessor_tran.enOperationType.Overwrite, clsAssessor_tran.enOperationType.Void), clsAssessor_tran.enOperationType), _
    '                                             CType(IIf(radMode.SelectedValue = "0", enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT), enAssessmentMode), _
    '                                             iToArEmpId, mstrModuleName) = False Then
    '            'Shani(01-MAR-2016) -- End
    '            DisplayMessage.DisplayMessage("Problem in migration process.", Me)
    '            Exit Sub
    '        Else
    '            DisplayMessage.DisplayMessage("Migration process completed successfully.", Me)
    '            Me.ViewState("dtOldEmp") = Nothing
    '            Me.ViewState("dtNewEmp") = Nothing
    '            Me.ViewState("dtAssignedEmp") = Nothing
    '            Me.ViewState("cboFrmApprover") = Nothing
    '            Me.ViewState("cboToApprover") = Nothing
    '            cboFrmApprover.SelectedValue = 0
    '            cboToApprover.SelectedValue = 0
    '            radMode_SelectedIndexChanged(New Object, New EventArgs)
    '            dgvAssignedEmp.DataSource = Nothing : dgvAssignedEmp.DataBind()
    '            dgvFrmEmp.DataSource = Nothing : dgvAssignedEmp.DataBind()
    '            dgvToEmp.DataSource = Nothing : dgvToEmp.DataBind()
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath") & "Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTransfer.Click
        Dim objAssessorTran As New clsAssessor_tran
        Try
            If Is_Valid_Data(True) = False Then Exit Sub
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvFrmEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.Cells(0).FindControl("chkbox1"), CheckBox).Checked = True)
            Dim iAssessorEmpId As Integer = 0
            Dim objAssessorMst As New clsAssessor
            objAssessorMst._Assessormasterunkid = CInt(cboToApprover.SelectedValue)
            iAssessorEmpId = objAssessorMst._EmployeeId
            If gRow.Count > 0 Then
                Dim iDictionary As New Dictionary(Of Integer, Integer)
                iDictionary = gRow.ToDictionary(Function(x) CInt(dgvFrmEmp.DataKeys(x.RowIndex)("assessortranunkid")), Function(y) CInt(dgvFrmEmp.DataKeys(y.RowIndex)("employeeunkid")))
                If iDictionary.ContainsValue(iAssessorEmpId) = True Then
                    If radMode.SelectedValue = "1" Then
                        DisplayMessage.DisplayMessage("Sorry, you cannot make same employee as his own assessor and will not be added to migration list.", Me)
                        Exit Sub
                    ElseIf radMode.SelectedValue = "2" Then
                        DisplayMessage.DisplayMessage("Sorry, you cannot make same employee as his own assessor and will not be added to migration list.", Me)
                        Exit Sub
                    End If
            End If
            objAssessorTran._EmployeeAsOnDate = Session("EmployeeAsOnDate").ToString()
            If objAssessorTran.Perform_Migration(CInt(cboFrmApprover.SelectedValue), _
                                                 CInt(cboToApprover.SelectedValue), _
                                                     iDictionary, _
                                                 IIf(radMode.SelectedValue = "1", True, False), _
                                                     Session("UserId"), _
                                                     Session("EmployeeAsOnDate").ToString(), _
                                                 CType(IIf(radOperation.SelectedValue = 1, clsAssessor_tran.enOperationType.Overwrite, clsAssessor_tran.enOperationType.Void), clsAssessor_tran.enOperationType), _
                                                 CType(IIf(radMode.SelectedValue = "0", enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT), enAssessmentMode), _
                                                     HttpContext.Current.Session("IP_ADD"), _
                                                     HttpContext.Current.Session("HOST_NAME"), _
                                                     mstrModuleName, Nothing) = False Then
                DisplayMessage.DisplayMessage("Problem in migration process.", Me)
            Else
                DisplayMessage.DisplayMessage("Migration process completed successfully.", Me)
                cboFrmApprover.SelectedValue = 0
                cboToApprover.SelectedValue = 0
                radMode_SelectedIndexChanged(New Object, New EventArgs)
                dgvAssignedEmp.DataSource = Nothing : dgvAssignedEmp.DataBind()
                    dgvFrmEmp.DataSource = Nothing : dgvFrmEmp.DataBind()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objAssessorTran = Nothing
        End Try
    End Sub

#End Region

#Region " Controls "

    Protected Sub radMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radMode.SelectedIndexChanged
        Try
            Select Case radMode.SelectedValue
                Case "1"  'ASSESSOR
                    lblCaption.Text = "Assessor Information"
                    lblFromApprover.Text = "From Assessor"
                    lblToApprover.Text = "To Assessor"
                Case "2"  'REVIEWER
                    lblCaption.Text = "Reviewer Information"
                    lblFromApprover.Text = "From Reviewer"
                    lblToApprover.Text = "To Reviewer"
            End Select
            Call FillCombo()
            Call cboFrmApprover_SelectedIndexChanged(New Object, New EventArgs)
            Call cboToApprover_SelectedIndexChanged(New Object, New EventArgs)

            'Shani(24-Feb-2016) -- Start
            'dgvToEmp.AutoGenerateColumns = False
            'dgvToEmp.DataSource = New List(Of String)
            'dgvToEmp.DataBind()
            'Shani(24-Feb-2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboFrmApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFrmApprover.SelectedIndexChanged
        Try
            Dim objAR As New clsAssessor
            Dim dsList As New DataSet
            Dim dtTable As DataTable

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If radMode.SelectedValue = "1" Then
            'dsList = objAR.GetList("List", False, True)
            'ElseIf radMode.SelectedValue = "2" Then
            '    dsList = objAR.GetList("List", True, True)
            'End If

            'If CInt(cboFrmApprover.SelectedValue) <= 0 Then
            '    dtTable = New DataView(dsList.Tables(0), "assessormasterunkid IN(" & cboFrmApprover.SelectedValue.ToString & ")", "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtTable = New DataView(dsList.Tables(0), "assessormasterunkid NOT IN(" & cboFrmApprover.SelectedValue.ToString & ")", "", DataViewRowState.CurrentRows).ToTable
            'End If

            Dim blnReviewerFlag As Boolean = False
            Dim StrSearch As String = ""
            If radMode.SelectedValue = "1" Then
                blnReviewerFlag = False
            ElseIf radMode.SelectedValue = "2" Then
                blnReviewerFlag = True
            End If

            If CInt(cboFrmApprover.SelectedValue) <= 0 Then
                StrSearch = "hrassessor_master.assessormasterunkid IN(" & cboFrmApprover.SelectedValue.ToString & ")"
            Else
                StrSearch = "hrassessor_master.assessormasterunkid NOT IN(" & cboFrmApprover.SelectedValue.ToString & ")"
            End If

            dsList = objAR.GetList(Session("Database_Name"), _
                                   Session("UserId"), _
                                   Session("Fin_year"), _
                                   Session("CompanyUnkId"), _
                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                   eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                   Session("UserAccessModeSetting"), True, _
                                   Session("IsIncludeInactiveEmp"), "List", blnReviewerFlag, clsAssessor.enARVisibilityTypeId.VISIBLE, True, StrSearch)
            'Shani (12-Jan-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]

            dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable


            'Shani(24-Aug-2015) -- End


            With cboToApprover
                .DataValueField = "assessormasterunkid"
                .DataTextField = "assessorname"
                .DataSource = dtTable
                .DataBind()
                .SelectedValue = 0
            End With

            'If Me.ViewState("cboToApprover") Is Nothing Then
            '    Me.ViewState.Add("cboToApprover", dtTable)
            'Else
            '    Me.ViewState("cboToApprover") = dtTable
            'End If

            Call Fill_Assessor_Emp()

            'If dtNewEmp IsNot Nothing Then dtNewEmp.Rows.Clear()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboToApprover_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboToApprover.SelectedIndexChanged
        Try
            Call Fill_Assigned_Emp()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvAssignedEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvAssignedEmp.RowDataBound
        Try
            If dgvFrmEmp.Rows.Count > 0 Then
                If e.Row.RowIndex > -1 Then
                    'Dim gRow As GridViewRow = dgvFrmEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) x.Cells(1).Text = e.Row.Cells(0).Text).Select(Function(x) x).FirstOrDefault()
                    Dim gRow As GridViewRow = dgvFrmEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) dgvFrmEmp.DataKeys(x.RowIndex)("employeeunkid") = dgvAssignedEmp.DataKeys(e.Row.RowIndex)("employeeunkid")).Select(Function(x) x).FirstOrDefault()
                    If gRow IsNot Nothing Then
                        CType(gRow.FindControl("chkbox1"), CheckBox).Visible = False
                        CType(gRow.FindControl("chkbox1"), CheckBox).Enabled = False
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If dgvFrmEmp.Rows.Count <= 0 Then Exit Sub
    '        Dim j As Integer = 0
    '        Dim dvEmployee As DataView = CType(Me.ViewState("dtOldEmp"), DataTable).DefaultView

    '        'SHANI [01 FEB 2015]-START
    '        'Enhancement - REDESIGN SELF SERVICE.
    '        'For i As Integer = Me.ViewState("gv1_FirstRecordNo") To dvEmployee.ToTable.Rows.Count - 1
    '        For i As Integer = 0 To dgvFrmEmp.Rows.Count - 1
    '            'If j <= dgvFrmEmp.PageSize - 1 Then
    '            'Dim gvRow As GridViewRow = dgvFrmEmp.Rows(j)
    '            Dim gvRow As GridViewRow = dgvFrmEmp.Rows(i)
    '                CType(gvRow.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
    '                Dim dRow() As DataRow = CType(Me.ViewState("dtOldEmp"), DataTable).Select("employeecode = '" & gvRow.Cells(1).Text & "'")
    '                If dRow.Length > 0 Then
    '                    dRow(0).Item("ischeck") = cb.Checked
                'End If
    '                dvEmployee.Table.AcceptChanges()
    '                j += 1
    '            'Else
    '            'Exit For
    '            'End If
    '        Next
    '        'SHANI [01 FEB 2015]--END
    '        Me.ViewState("dtOldEmp") = dvEmployee.ToTable
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkHeder1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
    '        If gvr.Cells.Count > 0 Then
    '            Dim dRow() As DataRow = CType(Me.ViewState("dtOldEmp"), DataTable).Select("employeecode = '" & gvr.Cells(1).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("ischeck") = cb.Checked
    '            End If
    '            CType(Me.ViewState("dtOldEmp"), DataTable).AcceptChanges()
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkbox1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkHeder2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If dgvToEmp.Rows.Count <= 0 Then Exit Sub
    '        Dim j As Integer = 0
    '        Dim dvEmployee As DataView = CType(Me.ViewState("dtNewEmp"), DataTable).DefaultView

    '        'SHANI [01 FEB 2015]-START
    '        'Enhancement - REDESIGN SELF SERVICE.
    '        'For i As Integer = Me.ViewState("gv2_FirstRecordNo") To dvEmployee.ToTable.Rows.Count - 1
    '        For i As Integer = 0 To dgvToEmp.Rows.Count - 1
    '            'If j <= dgvToEmp.PageSize - 1 Then
    '            'Dim gvRow As GridViewRow = dgvToEmp.Rows(j)
    '            Dim gvRow As GridViewRow = dgvToEmp.Rows(i)
    '                CType(gvRow.FindControl("chkbox2"), CheckBox).Checked = cb.Checked
    '                Dim dRow() As DataRow = CType(Me.ViewState("dtNewEmp"), DataTable).Select("employeecode = '" & gvRow.Cells(1).Text & "'")
    '                If dRow.Length > 0 Then
    '                    dRow(0).Item("ischeck") = cb.Checked
                'End If
    '                dvEmployee.Table.AcceptChanges()
    '                j += 1
    '            'Else
    '            'Exit For
    '            'End If
    '        Next
    '        'SHANI [01 FEB 2015]--END
    '        Me.ViewState("dtNewEmp") = dvEmployee.ToTable
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkHeder2_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkbox2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
    '        If gvr.Cells.Count > 0 Then
    '            Dim dRow() As DataRow = CType(Me.ViewState("dtNewEmp"), DataTable).Select("employeecode = '" & gvr.Cells(1).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("ischeck") = cb.Checked
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkbox2_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub txtFrmSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFrmSearch.TextChanged
    '    Try
    '        Dim dView As DataView = CType(Me.ViewState("dtOldEmp"), DataTable).DefaultView
    '        Dim StrSearch As String = String.Empty
    '        If txtFrmSearch.Text.Trim.Length > 0 Then
    '            StrSearch = "employeecode LIKE '%" & txtFrmSearch.Text & "%' OR name LIKE '%" & txtFrmSearch.Text & "%'"
    '        End If
    '        dView.RowFilter = StrSearch
    '        dgvFrmEmp.AutoGenerateColumns = False
    '        dgvFrmEmp.DataSource = dView
    '        dgvFrmEmp.DataBind()
    '        txtFrmSearch.Focus()
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("Procedure txtFrmSearch_TextChanged : " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub txtToSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtToSearch.TextChanged
    '    Try
    '        Dim dView As DataView = Nothing
    '        Dim StrSearch As String = String.Empty
    '        If mltiview.ActiveViewIndex = 0 Then
    '            dView = CType(Me.ViewState("dtNewEmp"), DataTable).DefaultView
    '        ElseIf mltiview.ActiveViewIndex = 1 Then
    '            dView = CType(Me.ViewState("dtAssignedEmp"), DataTable).DefaultView
    '        End If

    '        If txtToSearch.Text.Trim.Length > 0 Then
    '            StrSearch = "employeecode LIKE '%" & txtToSearch.Text & "%' OR name LIKE '%" & txtToSearch.Text & "%'"
    '        End If

    '        dView.RowFilter = StrSearch

    '        If mltiview.ActiveViewIndex = 0 Then
    '            dgvToEmp.AutoGenerateColumns = False
    '            dgvToEmp.DataSource = dView
    '            dgvToEmp.DataBind()
    '        ElseIf mltiview.ActiveViewIndex = 1 Then
    '            dgvAssignedEmp.AutoGenerateColumns = False
    '            dgvAssignedEmp.DataSource = dView
    '            dgvAssignedEmp.DataBind()
    '        End If
    '        txtToSearch.Focus()
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("Procedure txtToSearch_TextChanged : " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex,Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub dgvFrmEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgvFrmEmp.PageIndexChanging
    '    Try
    '        dgvFrmEmp.PageIndex = e.NewPageIndex

    '        If dgvFrmEmp.PageIndex > 0 Then
    '            Me.ViewState("gv1_FirstRecordNo") = (((dgvFrmEmp.PageIndex + 1) * dgvFrmEmp.Rows.Count) - dgvFrmEmp.Rows.Count)
    '        Else
    '            If dgvFrmEmp.Rows.Count < dgvFrmEmp.PageSize Then
    '                Me.ViewState("gv1_FirstRecordNo") = ((1 * dgvFrmEmp.Rows.Count) - dgvFrmEmp.Rows.Count)
    '            Else
    '                Me.ViewState("gv1_FirstRecordNo") = ((1 * dgvFrmEmp.PageSize) - dgvFrmEmp.Rows.Count)
    '            End If
    '        End If
    '        Me.ViewState("gv1_LastRecordNo") = ((dgvFrmEmp.PageIndex + 1) * dgvFrmEmp.Rows.Count)
    '        Fill_Assessor_Emp()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub dgvFrmEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvFrmEmp.RowDataBound
    '    Try
    '        If e.Row.Cells.Count > 1 Then
    '            If e.Row.RowIndex > -1 Then
    '                Dim dRow() As DataRow = CType(Me.ViewState("dtOldEmp"), DataTable).Select("employeecode = '" & e.Row.Cells(1).Text & "'")
    '                If dRow.Length > 0 Then
    '                    If CBool(dRow(0).Item("ischeck")) = True Then
    '                        Dim gvRow As GridViewRow = e.Row
    '                        CType(gvRow.FindControl("chkbox1"), CheckBox).Checked = CBool(dRow(0).Item("ischeck"))
    '                    End If
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub

    'Protected Sub dgvToEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgvToEmp.PageIndexChanging
    '    Try
    '        dgvToEmp.PageIndex = e.NewPageIndex

    '        If dgvToEmp.PageIndex > 0 Then
    '            Me.ViewState("gv2_FirstRecordNo") = (((dgvToEmp.PageIndex + 1) * dgvToEmp.Rows.Count) - dgvToEmp.Rows.Count)
    '        Else
    '            If dgvToEmp.Rows.Count < dgvToEmp.PageSize Then
    '                Me.ViewState("gv2_FirstRecordNo") = ((1 * dgvToEmp.Rows.Count) - dgvToEmp.Rows.Count)
    '            Else
    '                Me.ViewState("gv2_FirstRecordNo") = ((1 * dgvToEmp.PageSize) - dgvToEmp.Rows.Count)
    '            End If
    '        End If
    '        Me.ViewState("gv2_LastRecordNo") = ((dgvToEmp.PageIndex + 1) * dgvToEmp.Rows.Count)
    '        Call Fill_Migrated_Emp()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    Finally
    '    End Try
    'End Sub

    'Protected Sub dgvToEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvToEmp.RowDataBound
    '    Try
    '        If e.Row.Cells.Count > 1 Then
    '            If e.Row.Cells(1).Text = "None" Then
    '                e.Row.Visible = False
    '            End If
    '            If e.Row.RowIndex > -1 Then
    '                If Me.ViewState("dtNewEmp") IsNot Nothing Then
    '                    Dim dRow() As DataRow = CType(Me.ViewState("dtNewEmp"), DataTable).Select("employeecode = '" & e.Row.Cells(1).Text & "' AND employeecode <> 'None'")
    '                    If dRow.Length > 0 Then
    '                        If CBool(dRow(0).Item("ischeck")) = True Then
    '                            Dim gvRow As GridViewRow = e.Row
    '                            CType(gvRow.FindControl("chkbox2"), CheckBox).Checked = CBool(dRow(0).Item("ischeck"))
    '                        End If
    '                    End If
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub

    'Protected Sub dgvAssignedEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgvAssignedEmp.PageIndexChanging
    '    Try
    '        dgvAssignedEmp.PageIndex = e.NewPageIndex
    '        Call Fill_Assigned_Emp()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    Finally
    '    End Try
    'End Sub

#End Region

End Class
