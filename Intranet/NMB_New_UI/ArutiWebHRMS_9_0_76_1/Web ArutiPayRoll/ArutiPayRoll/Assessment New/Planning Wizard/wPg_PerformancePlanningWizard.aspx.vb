﻿#Region " Import "

Imports System.Data
Imports Aruti.Data
Imports System.Drawing
Imports AjaxControlToolkit
Imports System.IO
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Data.SqlClient

#End Region

Partial Class wPg_PerformancePlanningWizard
    Inherits Basepage

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmPerformancePlanningWizard"
    Private ReadOnly mstrModuleName_Emp1 As String = "objfrmAddEditEmpField1"
    Private ReadOnly mstrModuleName_Emp2 As String = "objfrmAddEditEmpField2"
    Private ReadOnly mstrModuleName_Emp3 As String = "objfrmAddEditEmpField3"
    Private ReadOnly mstrModuleName_Emp4 As String = "objfrmAddEditEmpField5"
    Private ReadOnly mstrModuleName_Emp5 As String = "objfrmAddEditEmpField5"
    Private DisplayMessage As New CommonCodes
    Private mdtSelectedEmployee As DataTable
    Private mblnIsValueSet As Boolean = False
    Private mdtGoals_List1 As DataTable
    Private mdtGoals_List2 As DataTable
    Private mintEmployeeId As Integer = 0
    Private mintAssessorEmpId As Integer = 0
    Private mintPastPeriodUnkid As Integer = 0
    Private mDecTotalGoalsWeight As Decimal = 0
    Private mDecTotalGroupWeight As Decimal = 0

    Private objEmpField1 As New clsassess_empfield1_master
    Private objEmpField2 As New clsassess_empfield2_master
    Private objEmpField3 As New clsassess_empfield3_master
    Private objEmpField4 As New clsassess_empfield4_master
    Private objEmpField5 As New clsassess_empfield5_master

    Private mblnpopupShow_EmpField1 As Boolean = False
    Private mblnpopupShow_EmpField2 As Boolean = False
    Private mblnpopupShow_EmpField3 As Boolean = False
    Private mblnpopupShow_EmpField4 As Boolean = False
    Private mblnpopupShow_EmpField5 As Boolean = False

    Private mintMappedFieldId As Integer

#End Region

#Region " Page Events "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            If IsPostBack = False Then
                Call FillCombo()
            Else
                mdtSelectedEmployee = Me.ViewState("mdtSelectedEmployee")
                mblnIsValueSet = Me.ViewState("mblnIsValueSet")
                mdtGoals_List1 = Me.ViewState("mdtGoals_List1")
                mdtGoals_List2 = Me.ViewState("mdtGoals_List2")
                mintEmployeeId = Me.ViewState("mintEmployeeId")
                mintAssessorEmpId = Me.ViewState("mintAssessorEmpId")
                mintPastPeriodUnkid = Me.ViewState("mintPastPeriodUnkid")
                mDecTotalGoalsWeight = Me.ViewState("mDecTotalGoalsWeight")
                mDecTotalGroupWeight = Me.ViewState("mDecTotalGroupWeight")

                mblnpopupShow_EmpField1 = Me.ViewState("popupShow_EmpField1")
                mblnpopupShow_EmpField2 = Me.ViewState("popupShow_EmpField2")
                mblnpopupShow_EmpField3 = Me.ViewState("popupShow_EmpField3")
                mblnpopupShow_EmpField4 = Me.ViewState("popupShow_EmpField4")
                mblnpopupShow_EmpField5 = Me.ViewState("popupShow_EmpField5")

                mintMappedFieldId = Me.ViewState("mintMappedFieldId")

                If mblnpopupShow_EmpField1 Then
                    pop_objfrmAddEditEmpField1.Show()
                End If

                If mblnpopupShow_EmpField2 Then
                    pop_objfrmAddEditEmpField2.Show()
                End If

                If mblnpopupShow_EmpField3 Then
                    pop_objfrmAddEditEmpField3.Show()
                End If

                If mblnpopupShow_EmpField4 Then
                    pop_objfrmAddEditEmpField4.Show()
                End If

                If mblnpopupShow_EmpField5 Then
                    pop_objfrmAddEditEmpField5.Show()
                End If
            End If
            If mvPerformancePlanningWz.ActiveViewIndex <= 0 Then btnPrevious.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mdtSelectedEmployee") = mdtSelectedEmployee
            Me.ViewState("mblnIsValueSet") = mblnIsValueSet
            Me.ViewState("mdtGoals_List1") = mdtGoals_List1
            Me.ViewState("mdtGoals_List2") = mdtGoals_List2
            Me.ViewState("mintEmployeeId") = mintEmployeeId
            Me.ViewState("mintAssessorEmpId") = mintAssessorEmpId
            Me.ViewState("mintPastPeriodUnkid") = mintPastPeriodUnkid
            Me.ViewState("mDecTotalGoalsWeight") = mDecTotalGoalsWeight
            Me.ViewState("mDecTotalGroupWeight") = mDecTotalGroupWeight

            Me.ViewState("popupShow_EmpField1") = mblnpopupShow_EmpField1
            Me.ViewState("popupShow_EmpField2") = mblnpopupShow_EmpField2
            Me.ViewState("popupShow_EmpField3") = mblnpopupShow_EmpField3
            Me.ViewState("popupShow_EmpField4") = mblnpopupShow_EmpField4
            Me.ViewState("popupShow_EmpField5") = mblnpopupShow_EmpField5

            Me.ViewState("mintMappedFieldId") = mintMappedFieldId

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objCMaster As New clsCommon_Master
        Dim dsList As New DataSet
        Try
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            With cboPlanningPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_GROUP, True, "List")
            With cboCompetenceGroup
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.COMPETENCE_CATEGORIES, True, "List")
            With cboCategory
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objPeriod = Nothing : dsList.Dispose() : objCMaster = Nothing
        End Try
    End Sub

    Private Sub FillAssignedEmployee()
        Dim objEvaluation As New clsevaluation_analysis_master
        Dim dsCombo As New DataSet
        Try
            dsCombo = objEvaluation.getAssessorComboList(Session("Database_Name"), _
                                                         Session("UserId"), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                         Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)

            'Shani(14-FEB-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]

            If dsCombo.Tables("List").Rows.Count > 0 Then
                Dim intAssessorId As Integer = dsCombo.Tables("List").Rows(0).Item("Id")
                mintAssessorEmpId = dsCombo.Tables("List").Rows(0).Item("EmpId")

                Dim intTotalReporting, intTotalPlanning As Integer
                intTotalReporting = 0 : intTotalPlanning = 0
                dsCombo = objEvaluation.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                                 Session("UserId"), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                                 Session("IsIncludeInactiveEmp"), intAssessorId, "EList", False, True, _
                                                                 CInt(IIf(cboPlanningPeriod.SelectedValue = "", 0, cboPlanningPeriod.SelectedValue)), _
                                                                 , , , intTotalReporting, intTotalPlanning)

                txtNoOfDirects.Text = intTotalReporting
                txtPlanningDone.Text = intTotalPlanning

                mdtSelectedEmployee = dsCombo.Tables("EList").Copy

                mdtSelectedEmployee.Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False

                For Each xrow As DataRow In mdtSelectedEmployee.Rows
                    xrow("ischeck") = False
                Next

                dgvEmployee.DataSource = mdtSelectedEmployee
                dgvEmployee.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEvaluation = Nothing : dsCombo.Dispose()
        End Try
    End Sub

    Private Function IsValidOperation() As Boolean
        Try
            'PERIOD SELECTION VALIDATION - START
            If CInt(IIf(cboPlanningPeriod.SelectedValue = "", 0, cboPlanningPeriod.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage("Sorry, Period is mandatory information. Please select period to continue.", Me)
                Return False
            End If

            If mdtSelectedEmployee IsNot Nothing Then
                Dim dr() As DataRow = mdtSelectedEmployee.Select("ischeck=true")
                If dr.Length <= 0 Then
                    DisplayMessage.DisplayMessage("Sorry, Employee is mandatory information. Please check employee to continue.", Me)
                    Return False
                End If
            End If
            'PERIOD SELECTION VALIDATION - START

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return True
    End Function

    Private Sub SetDisplayValueData()
        Try
            '********************** COMMON PART ******************************** START
            txtGPeriodDisplay.Text = cboPlanningPeriod.SelectedItem.Text
            txtGRPeriodDisplay.Text = cboPlanningPeriod.SelectedItem.Text
            txtCPeriodDisplay.Text = cboPlanningPeriod.SelectedItem.Text
            txtCRPeriodDisplay.Text = cboPlanningPeriod.SelectedItem.Text
            txtCnfPeriodDisplay.Text = cboPlanningPeriod.SelectedItem.Text
            txtConPeriodDisplay.Text = cboPlanningPeriod.SelectedItem.Text

            txtGEmployeeDisplay.Text = String.Join(vbCrLf, mdtSelectedEmployee.Rows.OfType(Of DataRow)().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).[Select](Function(r) r.Field(Of String)("Code") & "-" & r.Field(Of String)("Name")).ToArray())
            txtGREmployeeDisplay.Text = txtGEmployeeDisplay.Text
            txtCEmployeeDisplay.Text = txtGEmployeeDisplay.Text
            txtCREmployeeDisplay.Text = txtGEmployeeDisplay.Text
            txtCnfEmployeeDisplay.Text = txtGEmployeeDisplay.Text
            '********************** COMMON PART ******************************** END

            '********************** GOALS PART ******************************** START

            Dim intSelectedEmpCount As Integer = 0
            intSelectedEmpCount = mdtSelectedEmployee.Rows.OfType(Of DataRow)().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count
            If intSelectedEmpCount = 1 Then
                mintEmployeeId = mdtSelectedEmployee.Rows.OfType(Of DataRow)().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).[Select](Function(r) r.Field(Of Integer)("Id")).ToArray().GetValue(0)
            End If
            With cboGoalSource
                .Items.Clear()
                .Items.Add("New")
                .Items.Add("Supervisor")
                If intSelectedEmpCount = 1 Then
                    .Items.Add("Past Period")
                End If
                .SelectedIndex = 0
                Call cboGoalSource_SelectedIndexChanged(New Object, New EventArgs())
            End With

            With cboCompetencySource
                .Items.Clear()
                .Items.Add("Company Library")
                .Items.Add("Aruti Library")
                If intSelectedEmpCount = 1 Then
                    .Items.Add("Past Period")
                    .Items.Add("Job Competencies")
                End If
                .SelectedIndex = 0
                Call cboCompetencySource_SelectedIndexChanged(New Object, New EventArgs())
            End With

            If CInt(Session("CascadingTypeId")) <> enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                cboGoalSource.Enabled = False
            Else
                cboGoalSource.Enabled = True
            End If

            If CBool(Session("Self_Assign_Competencies")) = True Then
                cboCompetencySource.Enabled = True
            Else
                cboCompetencySource.Enabled = False
            End If
            '********************** GOALS PART ******************************** END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_Goals(ByVal intEmpId As Integer)
        Dim objEmpField1 As New clsassess_empfield1_master
        Try
            dgvGoalList1.AutoGenerateColumns = False
            'Dim iColName As String = String.Empty
            'Dim iPlan() As String = Nothing
            'If CStr(Session("ViewTitles_InPlanning")).Trim.Length > 0 Then
            '    iPlan = CStr(Session("ViewTitles_InPlanning")).Split("|")
            'End If
            'Dim objFMaster = New clsAssess_Field_Master
            'Dim intTotalWidth As Integer = 0
            'If iPlan IsNot Nothing Then
            '    For index As Integer = 0 To iPlan.Length - 1
            '        intTotalWidth += objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, iPlan(index), Session("CompnayUnkId"))
            '    Next
            'End If

            'mdtGoals_List1 = objEmpField1.GetDisplayList(intEmpId, CInt(cboPlanningPeriod.SelectedValue), "List")
            'mdtGoals_List1.Columns.Add("assign", System.Type.GetType("System.Boolean")).DefaultValue = False
            'mdtGoals_List1.Columns("assign").SetOrdinal(0)
            'For Each dr As DataRow In mdtGoals_List1.Rows
            '    dr("assign") = False
            'Next
            'Dim dgvCol As BoundColumn = Nothing
            'For Each dCol As DataColumn In mdtGoals_List1.Columns
            '    If dCol.ColumnName = "assign" Then Continue For
            '    iColName = "" : iColName = "obj" & dCol.ColumnName
            '    dgvCol = New BoundColumn
            '    dgvCol.ReadOnly = True
            '    dgvCol.HeaderText = dCol.Caption
            '    dgvCol.FooterText = iColName
            '    dgvCol.DataField = dCol.ColumnName

            '    'Dim gridBoundColumns = dgvGoalList1.Columns.OfType(Of BoundField)()
            '    'If gridBoundColumns.Any(Function(bf) bf.DataField.Equals(dgvCol)) = True Then Continue For

            '    If dgvCol.DataField.StartsWith("Owr") Then
            '        dgvCol.Visible = False
            '    End If
            '    If dCol.Caption.Length <= 0 Or dCol.ColumnName = "Emp" Or dCol.ColumnName = "OPeriod" Then
            '        dgvCol.Visible = False
            '    End If
            '    If mdtGoals_List1.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
            '        Dim decColumnWidth As Decimal = 0
            '        If intTotalWidth > 0 Then
            '            decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtGoals_List1.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)) * 95 / intTotalWidth
            '            dgvCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
            '            dgvCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
            '        Else
            '            decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtGoals_List1.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName))
            '            dgvCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
            '            dgvCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
            '        End If

            '        If iPlan IsNot Nothing Then
            '            If Array.IndexOf(iPlan, mdtGoals_List1.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
            '                dgvCol.Visible = False
            '            End If
            '        End If
            '    End If
            '    If dCol.ColumnName.ToUpper = "VUPROGRESS" Or dCol.ColumnName.ToUpper = "VUREMARK" Then
            '        dgvCol.Visible = False
            '    End If

            '    '
            '    Select Case dCol.ColumnName.ToUpper
            '        Case "ASSIGN", "EMPFIELD1UNKID", "EMPFIELD2UNKID", "EMPFIELD3UNKID", "EMPFIELD4UNKID", "EMPFIELD5UNKID"
            '        Case Else
            '            dgvGoalList1.Columns.Add(dgvCol)
            '    End Select
            'Next


            mdtGoals_List1 = objEmpField1.GetDisplayList(intEmpId, mintPastPeriodUnkid, "List")
            mdtGoals_List1.Columns.Add("assign", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtGoals_List1.Columns("assign").SetOrdinal(0)

            For Each dr As DataRow In mdtGoals_List1.Rows
                dr("assign") = False
            Next

            If mdtGoals_List1.Columns.Contains("Field1") Then
                dgvGoalList1.Columns(2).HeaderText = mdtGoals_List1.Columns("Field1").Caption
                dgvGoalList1.Columns(2).Visible = True
            Else
                mdtGoals_List1.Columns.Add("Field1", System.Type.GetType("System.String"))
                dgvGoalList1.Columns(2).Visible = False
            End If
            If mdtGoals_List1.Columns.Contains("Field2") Then
                dgvGoalList1.Columns(3).HeaderText = mdtGoals_List1.Columns("Field2").Caption
                dgvGoalList1.Columns(3).Visible = True
            Else
                mdtGoals_List1.Columns.Add("Field2", System.Type.GetType("System.String"))
                dgvGoalList1.Columns(3).Visible = False
            End If
            If mdtGoals_List1.Columns.Contains("Field3") Then
                dgvGoalList1.Columns(4).HeaderText = mdtGoals_List1.Columns("Field3").Caption
                dgvGoalList1.Columns(4).Visible = True
            Else
                mdtGoals_List1.Columns.Add("Field3", System.Type.GetType("System.String"))
                dgvGoalList1.Columns(4).Visible = False
            End If
            If mdtGoals_List1.Columns.Contains("Field4") Then
                dgvGoalList1.Columns(5).HeaderText = mdtGoals_List1.Columns("Field4").Caption
                dgvGoalList1.Columns(5).Visible = True
            Else
                mdtGoals_List1.Columns.Add("Field4", System.Type.GetType("System.String"))
                dgvGoalList1.Columns(5).Visible = False
            End If
            If mdtGoals_List1.Columns.Contains("Field5") Then
                dgvGoalList1.Columns(6).HeaderText = mdtGoals_List1.Columns("Field5").Caption
                dgvGoalList1.Columns(6).Visible = True
            Else
                mdtGoals_List1.Columns.Add("Field5", System.Type.GetType("System.String"))
                dgvGoalList1.Columns(6).Visible = False
            End If

            dgvGoalList1.DataSource = mdtGoals_List1
            dgvGoalList1.DataBind()

            If cboGoalSource.SelectedIndex > 0 Then
                dgvGoalList2.Columns(0).Visible = False
                dgvGoalList2.Columns(1).Visible = False
                dgvGoalList2.Columns(2).Visible = False
                dgvGoalList2.Columns(3).Visible = True
                btnDeleteGoals.Visible = False
                dgvGoalList1.Enabled = True : btnAddGoals.Enabled = True
            Else
                dgvGoalList1.Enabled = False : btnAddGoals.Enabled = False
                dgvGoalList2.Columns(0).Visible = True
                dgvGoalList2.Columns(1).Visible = True
                dgvGoalList2.Columns(2).Visible = True
                dgvGoalList2.Columns(3).Visible = True
                btnDeleteGoals.Visible = True
                'mdtGoals_List2.Rows.Add(mdtGoals_List2.NewRow)
            End If


            mdtGoals_List2 = objEmpField1.GetGoalsDataTableFormatForPlanningWizard("List", CInt(cboPlanningPeriod.SelectedValue))

            If mdtGoals_List2.Rows.Count <= 0 Then mdtGoals_List2.Rows.Add(mdtGoals_List2.NewRow)

            dgvGoalList2.AutoGenerateColumns = False

            If mdtGoals_List2.Columns.Contains("f1field_data") Then
                dgvGoalList2.Columns(18).HeaderText = mdtGoals_List2.Columns("f1field_data").Caption
                dgvGoalList2.Columns(18).Visible = True
            Else
                dgvGoalList2.Columns(18).Visible = False
            End If

            If mdtGoals_List2.Columns.Contains("f2field_data") Then
                dgvGoalList2.Columns(19).HeaderText = mdtGoals_List2.Columns("f2field_data").Caption
                dgvGoalList2.Columns(19).Visible = True
            Else
                dgvGoalList2.Columns(19).Visible = False
            End If

            If mdtGoals_List2.Columns.Contains("f3field_data") Then
                dgvGoalList2.Columns(20).HeaderText = mdtGoals_List2.Columns("f3field_data").Caption
                dgvGoalList2.Columns(20).Visible = True
            Else
                dgvGoalList2.Columns(20).Visible = False
            End If

            If mdtGoals_List2.Columns.Contains("f4field_data") Then
                dgvGoalList2.Columns(21).HeaderText = mdtGoals_List2.Columns("f4field_data").Caption
                dgvGoalList2.Columns(21).Visible = True
            Else
                dgvGoalList2.Columns(21).Visible = False
            End If

            If mdtGoals_List2.Columns.Contains("f5field_data") Then
                dgvGoalList2.Columns(22).HeaderText = mdtGoals_List2.Columns("f5field_data").Caption
                dgvGoalList2.Columns(22).Visible = True
            Else
                dgvGoalList2.Columns(22).Visible = False
            End If

            If mdtGoals_List2.Columns.Contains("f6field_data") Then
                Dim field As BoundColumn = DirectCast(Me.dgvGoalList2.Columns(23), BoundColumn)
                field.DataField = "f6field_data"
                dgvGoalList2.Columns(23).HeaderText = mdtGoals_List2.Columns("f6field_data").Caption
                dgvGoalList2.Columns(23).Visible = True
            Else
                dgvGoalList2.Columns(23).Visible = False
            End If

            If mdtGoals_List2.Columns.Contains("f7field_data") Then
                Dim field As BoundColumn = DirectCast(Me.dgvGoalList2.Columns(24), BoundColumn)
                field.DataField = "f7field_data"
                dgvGoalList2.Columns(24).HeaderText = mdtGoals_List2.Columns("f7field_data").Caption
                dgvGoalList2.Columns(24).Visible = True
            Else
                dgvGoalList2.Columns(24).Visible = False
            End If

            If mdtGoals_List2.Columns.Contains("f8field_data") Then
                Dim field As BoundColumn = DirectCast(Me.dgvGoalList2.Columns(25), BoundColumn)
                field.DataField = "f7field_data"
                dgvGoalList2.Columns(25).HeaderText = mdtGoals_List2.Columns("f8field_data").Caption
                dgvGoalList2.Columns(25).Visible = True
            Else
                dgvGoalList2.Columns(25).Visible = False
            End If

            If mdtGoals_List2.Columns.Contains("f6fieldunkid") Then
                Dim field As BoundColumn = DirectCast(Me.dgvGoalList2.Columns(15), BoundColumn)
                field.DataField = "f6fieldunkid"
            End If

            If mdtGoals_List2.Columns.Contains("f7fieldunkid") Then
                Dim field As BoundColumn = DirectCast(Me.dgvGoalList2.Columns(16), BoundColumn)
                field.DataField = "f7fieldunkid"
            End If

            If mdtGoals_List2.Columns.Contains("f8fieldunkid") Then

                Dim field As BoundColumn = DirectCast(Me.dgvGoalList2.Columns(17), BoundColumn)
                field.DataField = "f8fieldunkid"
            End If

            Select Case mintMappedFieldId

                Case enWeight_Types.WEIGHT_FIELD1
                    dgvGoalList2.Columns(26).HeaderText = mdtGoals_List2.Columns("f1weight").Caption
                    dgvGoalList2.Columns(26).Visible = True
                    dgvGoalList2.Columns(31).HeaderText = mdtGoals_List2.Columns("f1pct_completed").Caption
                    dgvGoalList2.Columns(31).Visible = True
                    dgvGoalList2.Columns(36).HeaderText = mdtGoals_List2.Columns("f1startdate").Caption
                    dgvGoalList2.Columns(36).Visible = True
                    dgvGoalList2.Columns(41).HeaderText = mdtGoals_List2.Columns("f1enddate").Caption
                    dgvGoalList2.Columns(41).Visible = True
                    dgvGoalList2.Columns(51).HeaderText = mdtGoals_List2.Columns("f1status").Caption
                    dgvGoalList2.Columns(51).Visible = True

                Case enWeight_Types.WEIGHT_FIELD2
                    dgvGoalList2.Columns(27).HeaderText = mdtGoals_List2.Columns("f2weight").Caption
                    dgvGoalList2.Columns(27).Visible = True
                    dgvGoalList2.Columns(32).HeaderText = mdtGoals_List2.Columns("f2pct_completed").Caption
                    dgvGoalList2.Columns(32).Visible = True
                    dgvGoalList2.Columns(37).HeaderText = mdtGoals_List2.Columns("f2startdate").Caption
                    dgvGoalList2.Columns(37).Visible = True
                    dgvGoalList2.Columns(42).HeaderText = mdtGoals_List2.Columns("f2enddate").Caption
                    dgvGoalList2.Columns(42).Visible = True
                    dgvGoalList2.Columns(52).HeaderText = mdtGoals_List2.Columns("f2status").Caption
                    dgvGoalList2.Columns(52).Visible = True

                Case enWeight_Types.WEIGHT_FIELD3
                    dgvGoalList2.Columns(28).HeaderText = mdtGoals_List2.Columns("f3weight").Caption
                    dgvGoalList2.Columns(28).Visible = True
                    dgvGoalList2.Columns(33).HeaderText = mdtGoals_List2.Columns("f3pct_completed").Caption
                    dgvGoalList2.Columns(33).Visible = True
                    dgvGoalList2.Columns(38).HeaderText = mdtGoals_List2.Columns("f3startdate").Caption
                    dgvGoalList2.Columns(38).Visible = True
                    dgvGoalList2.Columns(43).HeaderText = mdtGoals_List2.Columns("f3enddate").Caption
                    dgvGoalList2.Columns(43).Visible = True
                    dgvGoalList2.Columns(53).HeaderText = mdtGoals_List2.Columns("f3status").Caption
                    dgvGoalList2.Columns(53).Visible = True

                Case enWeight_Types.WEIGHT_FIELD4
                    dgvGoalList2.Columns(29).HeaderText = mdtGoals_List2.Columns("f4weight").Caption
                    dgvGoalList2.Columns(29).Visible = True
                    dgvGoalList2.Columns(34).HeaderText = mdtGoals_List2.Columns("f4pct_completed").Caption
                    dgvGoalList2.Columns(34).Visible = True
                    dgvGoalList2.Columns(39).HeaderText = mdtGoals_List2.Columns("f4startdate").Caption
                    dgvGoalList2.Columns(39).Visible = True
                    dgvGoalList2.Columns(44).HeaderText = mdtGoals_List2.Columns("f4enddate").Caption
                    dgvGoalList2.Columns(44).Visible = True
                    dgvGoalList2.Columns(54).HeaderText = mdtGoals_List2.Columns("f4status").Caption
                    dgvGoalList2.Columns(54).Visible = True

                Case enWeight_Types.WEIGHT_FIELD5
                    dgvGoalList2.Columns(30).HeaderText = mdtGoals_List2.Columns("f5weight").Caption
                    dgvGoalList2.Columns(30).Visible = True
                    dgvGoalList2.Columns(35).HeaderText = mdtGoals_List2.Columns("f5pct_completed").Caption
                    dgvGoalList2.Columns(35).Visible = True
                    dgvGoalList2.Columns(40).HeaderText = mdtGoals_List2.Columns("f5startdate").Caption
                    dgvGoalList2.Columns(40).Visible = True
                    dgvGoalList2.Columns(45).HeaderText = mdtGoals_List2.Columns("f5enddate").Caption
                    dgvGoalList2.Columns(45).Visible = True
                    dgvGoalList2.Columns(55).HeaderText = mdtGoals_List2.Columns("f5status").Caption
                    dgvGoalList2.Columns(55).Visible = True

            End Select

            dgvGoalList2.DataSource = mdtGoals_List2
            dgvGoalList2.DataBind()



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmpField1 = Nothing
        End Try
    End Sub

    Private Sub Genrate_AddmanuItem()
        Try
            Dim dsList As New DataSet
            Dim objFMaster As New clsAssess_Field_Master
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            Dim iclmid As New DataColumn("lnkId", Type.GetType("System.String"))
            Dim iclmname As New DataColumn("lnkName", Type.GetType("System.String"))
            Dim iclmtag As New DataColumn("lnkTag", Type.GetType("System.String"))
            dsList.Tables(0).Columns.Add(iclmid)
            dsList.Tables(0).Columns.Add(iclmname)
            dsList.Tables(0).Columns.Add(iclmtag)
            If dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    dsList.Tables(0).Rows(i)("lnkId") = "objbtnAdd" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    dsList.Tables(0).Rows(i)("lnkName") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 26, "Add") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    dsList.Tables(0).Rows(i)("lnkTag") = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                Next
            End If
            dlmnuAdd.DataSource = dsList
            dlmnuAdd.DataBind()

            objFMaster = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Genrate_EditmanuItem()
        Try
            Dim dsList As New DataSet
            Dim objFMaster As New clsAssess_Field_Master
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            Dim iclmid As New DataColumn("lnkId", Type.GetType("System.String"))
            Dim iclmname As New DataColumn("lnkName", Type.GetType("System.String"))
            Dim iclmtag As New DataColumn("lnkTag", Type.GetType("System.String"))
            Dim iclmOrgname As New DataColumn("lnkOriginal", Type.GetType("System.String"))
            dsList.Tables(0).Columns.Add(iclmid)
            dsList.Tables(0).Columns.Add(iclmname)
            dsList.Tables(0).Columns.Add(iclmtag)
            dsList.Tables(0).Columns.Add(iclmOrgname)
            If dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    dsList.Tables(0).Rows(i)("lnkId") = "objbtnEdt" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    dsList.Tables(0).Rows(i)("lnkName") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 27, "Edit") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    dsList.Tables(0).Rows(i)("lnkTag") = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    dsList.Tables(0).Rows(i)("lnkOriginal") = dsList.Tables(0).Rows(i)("fieldcaption").ToString
                Next
            End If
            dlmnuEdit.DataSource = dsList
            dlmnuEdit.DataBind()
            objFMaster = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Genrate_DeletemanuItem()
        Try
            Dim dsList As New DataSet
            Dim objFMaster As New clsAssess_Field_Master
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            Dim iclmid As New DataColumn("lnkId", Type.GetType("System.String"))
            Dim iclmname As New DataColumn("lnkName", Type.GetType("System.String"))
            Dim iclmtag As New DataColumn("lnkTag", Type.GetType("System.String"))
            Dim iclmOrgname As New DataColumn("lnkOriginal", Type.GetType("System.String"))
            dsList.Tables(0).Columns.Add(iclmid)
            dsList.Tables(0).Columns.Add(iclmname)
            dsList.Tables(0).Columns.Add(iclmtag)
            dsList.Tables(0).Columns.Add(iclmOrgname)
            If dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    dsList.Tables(0).Rows(i)("lnkId") = "objbtnDel" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    dsList.Tables(0).Rows(i)("lnkName") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Delete") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    dsList.Tables(0).Rows(i)("lnkTag") = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    dsList.Tables(0).Rows(i)("lnkOriginal") = dsList.Tables(0).Rows(i)("fieldcaption").ToString
                Next
            End If
            dlmnuDelete.DataSource = dsList
            dlmnuDelete.DataBind()

            objFMaster = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub EmpField1_load(ByVal p_mintEmpField1Unkid As Integer, ByVal p_enEmpField1Action As enAction)
        Dim objOwrOwner As New clsassess_empowner_tran
        Dim mintEmpField1Unkid As Integer = p_mintEmpField1Unkid 'Empployee Field1 unkId (Identity Column)
        Dim mdtEmpField1Owner As New DataTable 'Empployee Field1 Table
        Dim enEmpField1Action As enAction = p_enEmpField1Action 'Empployee Field1 Actions
        Dim mdicEmpField1FieldData As New Dictionary(Of Integer, String) 'Empployee Field1 Dictionary
        Dim mintEmpField1FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Empployee Field1 FieldunkID (Assement Caption FieldID)
        Dim mintEmpField1LinkedFieldId As Integer = 0 'Empployee Field1 LinkedFieldID
        Try
            mblnpopupShow_EmpField1 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintEmpField1LinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPlanningPeriod.SelectedValue))
            objMapping = Nothing

            Dim objPrd As New clscommom_period_Tran

            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPlanningPeriod.SelectedValue)

            txtEmpField1Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If mintEmpField1LinkedFieldId <> mintEmpField1FieldUnkid Then
                objEmpField1tabcRemarks.Enabled = False : pnl_RightEmpField1.Enabled = False
            End If
            mdtEmpField1Owner = objOwrOwner.Get_Data(mintEmpField1Unkid, enWeight_Types.WEIGHT_FIELD1)

            If enEmpField1Action = enAction.EDIT_ONE Then
                objEmpField1._Empfield1unkid = mintEmpField1Unkid
            End If

            Me.ViewState.Add("mdtEmpField1Owner", mdtEmpField1Owner)
            Me.ViewState.Add("mintEmpField1Unkid", mintEmpField1Unkid)
            Me.ViewState.Add("mdicEmpField1FieldData", mdicEmpField1FieldData)
            Me.ViewState.Add("mintEmpField1FieldUnkid", mintEmpField1FieldUnkid)
            Me.ViewState.Add("menEmpField1Action", enEmpField1Action)
            Me.ViewState.Add("mintEmpField1LinkedFieldId", mintEmpField1LinkedFieldId)

            Call Set_EmpField1Form_Information()
            Call FillCombo_EmpField1()
            Call Fill_Data_EmpField1()
            Call GetEmpField1Value()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub EmpField2_load(ByVal p_mintEmpField2Unkid As Integer, ByVal p_enEmpField2Action As enAction, Optional ByVal p_mintEmpField2ParentId As Integer = 0)
        Dim objOwrOwner As New clsassess_empowner_tran
        Dim mintEmpField2Unkid As Integer = p_mintEmpField2Unkid 'Empployee Field2 unkId (Identity Column)
        Dim menEmpField2Action As enAction = p_enEmpField2Action 'Empployee Field2 Actions
        Dim mdicEmpField2FieldData As New Dictionary(Of Integer, String) 'Empployee Field2 Dictionary
        Dim mintEmpField2ParentId As Integer = p_mintEmpField2ParentId 'Empployee Field2 ParentID
        Dim mdtEmpField2Owner As DataTable 'Empployee Field2 Table
        Dim mintEmpField2FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Empployee Field2 FieldunkID (Assement Caption FieldID)
        Dim mintEmpField2LinkedFieldId As Integer = -1  'Empployee Field2 LinkedFieldID
        Try
            mblnpopupShow_EmpField2 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintEmpField2LinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPlanningPeriod.SelectedValue))
            objMapping = Nothing

            Dim objPrd As New clscommom_period_Tran

            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPlanningPeriod.SelectedValue)

            txtEmpField2Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If mintEmpField2LinkedFieldId <> mintEmpField2FieldUnkid Then
                objEmpField2tabcRemarks.Enabled = False : pnl_RightEmpField2.Enabled = False
            End If

            If menEmpField2Action = enAction.EDIT_ONE Then
                objEmpField2._Empfield2unkid = mintEmpField2Unkid
            End If

            mdtEmpField2Owner = objOwrOwner.Get_Data(mintEmpField2Unkid, enWeight_Types.WEIGHT_FIELD2)


            Me.ViewState.Add("mdtEmpField2Owner", mdtEmpField2Owner)
            Me.ViewState.Add("mintEmpField2Unkid", mintEmpField2Unkid)
            Me.ViewState.Add("menEmpField2Action", menEmpField2Action)
            Me.ViewState.Add("mdicEmpField2FieldData", mdicEmpField2FieldData)
            Me.ViewState.Add("mintEmpField2FieldUnkid", mintEmpField2FieldUnkid)
            Me.ViewState.Add("mintEmpField2LinkedFieldId", mintEmpField2LinkedFieldId)
            Me.ViewState.Add("mintEmpField2ParentId", mintEmpField2ParentId)


            Call Set_EmpField2Form_Information()
            Call Fill_Data_EmpField2()
            Call FillCombo_EmpField2()
            Call GetEmpField2Value()
            If mintEmpField2ParentId > 0 Then
                Try
                    cboEmpField2EmpFieldValue1.SelectedValue = mintEmpField2ParentId
                    Call cboEmpField2EmpFieldValue1_SelectedIndexChanged(cboEmpField2EmpFieldValue1, Nothing)
                Catch ex As Exception
                    Throw ex
                End Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub EmpField3_load(ByVal p_mintEmpField3Unkid As Integer, ByVal p_enEmpField3Action As enAction, Optional ByVal p_mintEmpField3ParentId As Integer = 0, Optional ByVal p_mintEmpField3MainParentId As Integer = 0)
        Dim objOwrOwner As New clsassess_empowner_tran
        Dim mintEmpField3Unkid As Integer = p_mintEmpField3Unkid 'Empployee Field3 unkId (Identity Column)
        Dim menEmpField3Action As enAction = p_enEmpField3Action 'Empployee Field3 Actions
        Dim mdicEmpField3FieldData As New Dictionary(Of Integer, String) 'Empployee Field3 Dictionary
        Dim mintEmpField3ParentId As Integer = p_mintEmpField3ParentId 'Empployee Field3 ParentID
        Dim mintEmpField3MainParentId As Integer = p_mintEmpField3MainParentId 'Empployee Field3 ParentID
        Dim mdtEmpField3Owner As DataTable 'Empployee Field3 Table
        Dim mintEmpField3FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Empployee Field3 FieldunkID (Assement Caption FieldID)
        Dim mintEmpField3LinkedFieldId As Integer = -1  'Empployee Field3 LinkedFieldID
        Try
            mblnpopupShow_EmpField3 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintEmpField3LinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPlanningPeriod.SelectedValue))
            objMapping = Nothing

            Dim objPrd As New clscommom_period_Tran

            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPlanningPeriod.SelectedValue)

            txtEmpField3Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If mintEmpField3LinkedFieldId <> mintEmpField3FieldUnkid Then
                objEmpField3tabcRemarks.Enabled = False : pnl_RightEmpField2.Enabled = False
            End If

            If menEmpField3Action = enAction.EDIT_ONE Then
                objEmpField3._Empfield3unkid = mintEmpField3Unkid
            End If
            mdtEmpField3Owner = objOwrOwner.Get_Data(mintEmpField3Unkid, enWeight_Types.WEIGHT_FIELD3)

            Me.ViewState.Add("mdtEmpField3Owner", mdtEmpField3Owner)
            Me.ViewState.Add("mintEmpField3Unkid", mintEmpField3Unkid)
            Me.ViewState.Add("menEmpField3Action", menEmpField3Action)
            Me.ViewState.Add("mdicEmpField3FieldData", mdicEmpField3FieldData)
            Me.ViewState.Add("mintEmpField3FieldUnkid", mintEmpField3FieldUnkid)
            Me.ViewState.Add("mintEmpField3LinkedFieldId", mintEmpField3LinkedFieldId)
            Me.ViewState.Add("mintEmpField3ParentId", mintEmpField3ParentId)
            Me.ViewState.Add("mintEmpField3MainParentId", mintEmpField3MainParentId)


            Call Set_EmpField3Form_Information()
            Call Fill_Data_EmpField3()
            Call FillCombo_EmpField3()
            Call GetEmpField3Value()
            If mintEmpField3ParentId > 0 Then
                Try
                    cboEmpField3EmpFieldValue2.SelectedValue = mintEmpField3ParentId
                    Call cboEmpField3EmpFieldValue2_SelectedIndexChanged(cboEmpField3EmpFieldValue2, Nothing)
                Catch ex As Exception
                    Throw ex
                End Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub EmpField4_load(ByVal p_mintEmpField4Unkid As Integer, ByVal p_enEmpField4Action As enAction, Optional ByVal p_mintEmpField4ParentId As Integer = 0, Optional ByVal p_mintEmpField4MainParentId As Integer = 0)
        Dim objOwrOwner As New clsassess_empowner_tran
        Dim mintEmpField4Unkid As Integer = p_mintEmpField4Unkid 'Empployee Field3 unkId (Identity Column)
        Dim menEmpField4Action As enAction = p_enEmpField4Action 'Empployee Field3 Actions
        Dim mdicEmpField4FieldData As New Dictionary(Of Integer, String) 'Empployee Field3 Dictionary
        Dim mintEmpField4ParentId As Integer = p_mintEmpField4ParentId 'Empployee Field3 ParentID
        Dim mintEmpField4MainParentId As Integer = p_mintEmpField4MainParentId 'Empployee Field3 ParentID
        Dim mdtEmpField4Owner As DataTable 'Empployee Field3 Table
        Dim mintEmpField4FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Empployee Field3 FieldunkID (Assement Caption FieldID)
        Dim mintEmpField4LinkedFieldId As Integer = -1  'Empployee Field3 LinkedFieldID
        Try
            mblnpopupShow_EmpField4 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintEmpField4LinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPlanningPeriod.SelectedValue))
            objMapping = Nothing

            Dim objPrd As New clscommom_period_Tran

            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPlanningPeriod.SelectedValue)

            txtEmpField4Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If mintEmpField4LinkedFieldId <> mintEmpField4FieldUnkid Then
                objEmpField4tabcRemarks.Enabled = False : pnl_RightEmpField4.Enabled = False
            End If

            If menEmpField4Action = enAction.EDIT_ONE Then
                objEmpField4._Empfield4unkid = mintEmpField4Unkid
            End If
            mdtEmpField4Owner = objOwrOwner.Get_Data(mintEmpField4Unkid, enWeight_Types.WEIGHT_FIELD3)

            Me.ViewState.Add("mdtEmpField4Owner", mdtEmpField4Owner)
            Me.ViewState.Add("mintEmpField4Unkid", mintEmpField4Unkid)
            Me.ViewState.Add("menEmpField4Action", menEmpField4Action)
            Me.ViewState.Add("mdicEmpField4FieldData", mdicEmpField4FieldData)
            Me.ViewState.Add("mintEmpField4FieldUnkid", mintEmpField4FieldUnkid)
            Me.ViewState.Add("mintEmpField4LinkedFieldId", mintEmpField4LinkedFieldId)
            Me.ViewState.Add("mintEmpField4ParentId", mintEmpField4ParentId)
            Me.ViewState.Add("mintEmpField4MainParentId", mintEmpField4MainParentId)


            Call Set_EmpField4Form_Information()
            Call Fill_Data_EmpField4()
            Call FillCombo_EmpField4()
            Call GetEmpField4Value()
            If mintEmpField4ParentId > 0 Then
                Try
                    cboEmpField4EmpFieldValue3.SelectedValue = mintEmpField4ParentId
                    Call cboEmpField4EmpFieldValue3_SelectedIndexChanged(cboEmpField4EmpFieldValue3, Nothing)
                Catch ex As Exception
                    Throw ex
                End Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub EmpField5_load(ByVal p_mintEmpField5Unkid As Integer, ByVal p_enEmpField5Action As enAction, Optional ByVal p_mintEmpField5ParentId As Integer = 0, Optional ByVal p_mintEmpField5MainParentId As Integer = 0)
        Dim objOwrOwner As New clsassess_empowner_tran
        Dim mintEmpField5Unkid As Integer = p_mintEmpField5Unkid 'Empployee Field3 unkId (Identity Column)
        Dim menEmpField5Action As enAction = p_enEmpField5Action 'Empployee Field3 Actions
        Dim mdicEmpField5FieldData As New Dictionary(Of Integer, String) 'Empployee Field3 Dictionary
        Dim mintEmpField5ParentId As Integer = p_mintEmpField5ParentId 'Empployee Field3 ParentID
        Dim mintEmpField5MainParentId As Integer = p_mintEmpField5MainParentId 'Empployee Field3 ParentID
        Dim mdtEmpField5Owner As DataTable 'Empployee Field3 Table
        Dim mintEmpField5FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Empployee Field3 FieldunkID (Assement Caption FieldID)
        Dim mintEmpField5LinkedFieldId As Integer = -1  'Empployee Field3 LinkedFieldID
        Try
            mblnpopupShow_EmpField5 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintEmpField5LinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPlanningPeriod.SelectedValue))
            objMapping = Nothing

            Dim objPrd As New clscommom_period_Tran

            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPlanningPeriod.SelectedValue)

            txtEmpField5Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If mintEmpField5LinkedFieldId <> mintEmpField5FieldUnkid Then
                objEmpField5tabcRemarks.Enabled = False : pnl_RightEmpField5.Enabled = False
            End If

            If menEmpField5Action = enAction.EDIT_ONE Then
                objEmpField5._Empfield5unkid = mintEmpField5Unkid
            End If
            mdtEmpField5Owner = objOwrOwner.Get_Data(mintEmpField5Unkid, enWeight_Types.WEIGHT_FIELD3)

            Me.ViewState.Add("mdtEmpField5Owner", mdtEmpField5Owner)
            Me.ViewState.Add("mintEmpField5Unkid", mintEmpField5Unkid)
            Me.ViewState.Add("menEmpField5Action", menEmpField5Action)
            Me.ViewState.Add("mdicEmpField5FieldData", mdicEmpField5FieldData)
            Me.ViewState.Add("mintEmpField5FieldUnkid", mintEmpField5FieldUnkid)
            Me.ViewState.Add("mintEmpField5LinkedFieldId", mintEmpField5LinkedFieldId)
            Me.ViewState.Add("mintEmpField5ParentId", mintEmpField5ParentId)
            Me.ViewState.Add("mintEmpField5MainParentId", mintEmpField5MainParentId)


            Call Set_EmpField5Form_Information()
            Call Fill_Data_EmpField5()
            Call FillCombo_EmpField5()
            Call GetEmpField5Value()

            If mintEmpField5ParentId > 0 Then
                Try
                    cboEmpField5EmpFieldValue4.SelectedValue = mintEmpField5ParentId
                    Call cboEmpField5EmpFieldValue4_SelectedIndexChanged(cboEmpField5EmpFieldValue4, Nothing)
                Catch ex As Exception
                    Throw ex
                End Try
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Set_EmpField1Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicEmpField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField1FieldData"), Dictionary(Of Integer, String))
        Try
            lblheaderEmpField1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp1, 1, "Add/Edit Employee") & " " & objFieldMaster._Field1_Caption & " " & _
                      Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Information")

            objEmpField1lblField1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp1, 15, "Goal Owner") & " " & objFieldMaster._Field1_Caption

            objEmpField1lblEmpField1.Text = objFieldMaster._Field1_Caption
            hdf_txtEmpField1EmpField1.Value = objFieldMaster._Field1Unkid

            Select Case Session("CascadingTypeId")
                Case enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT, enPACascading.LOOSE_CASCADING, enPACascading.LOOSE_GOAL_ALIGNMENT
                    cboEmpField1Perspective.Enabled = True
                Case Else
                    cboEmpField1Perspective.Enabled = False
            End Select
            If CInt(Me.ViewState("mintOwrField1FieldUnkid")) = CInt(Me.ViewState("mintOwrField1LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    objEmpField1tabpRemark1.Enabled = False
                Else
                    objEmpField1tabpRemark1.HeaderText = objFieldMaster._Field6_Caption
                    lblEmpField1Remark1.Text = objFieldMaster._Field6_Caption
                    hdf_lblEmpField1Remark1.Value = objFieldMaster._Field6Unkid
                    If mdicEmpField1FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicEmpField1FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    objEmpField1tabpRemark2.Enabled = False
                Else
                    objEmpField1tabpRemark2.HeaderText = objFieldMaster._Field7_Caption
                    hdf_lblEmpField1Remark2.Value = objFieldMaster._Field7Unkid
                    lblEmpField1Remark2.Text = objFieldMaster._Field7_Caption
                    If mdicEmpField1FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicEmpField1FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    objEmpField1tabpRemark3.Enabled = False
                Else
                    objEmpField1tabpRemark3.HeaderText = objFieldMaster._Field8_Caption
                    hdf_lblEmpField1Remark3.Value = objFieldMaster._Field8Unkid
                    lblEmpField1Remark3.Text = objFieldMaster._Field8_Caption
                    If mdicEmpField1FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicEmpField1FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicEmpField1FieldData.Keys.Count > 0 Then objEmpField1tabcRemarks.Enabled = True
                pnl_EmpField1dgvower.Enabled = Session("FollowEmployeeHierarchy")
                txtEmpField1SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objEmpField1tabcRemarks.Enabled = False : pnl_RightEmpField1.Enabled = False
            End If

            Dim objEmp As New clsEmployee_Master

            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(mintEmployeeId)

            hdf_txtEmpField1EmployeeName.Value = CInt(mintEmployeeId)
            If ConfigParameter._Object._FirstNamethenSurname Then
                txtEmpField1EmployeeName.Text = objEmp._Employeecode & " - " & objEmp._Firstname & " " & objEmp._Surname
            Else
                txtEmpField1EmployeeName.Text = objEmp._Employeecode & " - " & objEmp._Surname & " " & objEmp._Firstname
            End If
            objEmp = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Set_EmpField2Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicEmpField2FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField2FieldData"), Dictionary(Of Integer, String))
        Try
            lblHeaderEmpField2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp2, 1, "Add/Edit Employee") & " " & objFieldMaster._Field2_Caption & " " & _
                      Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp2, 2, "Information")

            objEmpField2lblEmpField1.Text = objFieldMaster._Field1_Caption
            hdf_cboEmpField2EmpFieldValue1.Value = objFieldMaster._Field1Unkid

            objEmpField2lblEmpField2.Text = objFieldMaster._Field2_Caption
            hdf_txtEmpField2EmpField2.Value = objFieldMaster._Field2Unkid

            If CInt(Me.ViewState("mintEmpField2FieldUnkid")) = CInt(Me.ViewState("mintEmpField2LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    objEmpField2tabpRemark1.Enabled = False
                Else
                    objEmpField2tabpRemark1.HeaderText = objFieldMaster._Field6_Caption
                    lblEmpField2Remark1.Text = objFieldMaster._Field6_Caption
                    hdf_lblEmpField2Remark1.Value = objFieldMaster._Field6Unkid
                    If mdicEmpField2FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicEmpField2FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    objEmpField2tabpRemark2.Enabled = False
                Else
                    objEmpField2tabpRemark2.HeaderText = objFieldMaster._Field7_Caption
                    lblEmpField2Remark2.Text = objFieldMaster._Field7_Caption
                    hdf_lblEmpField2Remark2.Value = objFieldMaster._Field7Unkid
                    If mdicEmpField2FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicEmpField2FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    objEmpField2tabpRemark3.Enabled = False
                Else
                    objEmpField2tabpRemark3.HeaderText = objFieldMaster._Field8_Caption
                    lblEmpField2Remark3.Text = objFieldMaster._Field8_Caption
                    hdf_lblEmpField2Remark3.Value = objFieldMaster._Field8Unkid

                    If mdicEmpField2FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicEmpField2FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicEmpField2FieldData.Keys.Count > 0 Then objEmpField2tabcRemarks.Enabled = True
                pnl_EmpField2dgvower.Enabled = Session("FollowEmployeeHierarchy")
                txtEmpField2SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objEmpField2tabcRemarks.Enabled = False : pnl_RightEmpField2.Enabled = False : objEmpField2tabcRemarks.Enabled = False
            End If

            Dim objEmp As New clsEmployee_Master

            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(mintEmployeeId)

            If ConfigParameter._Object._FirstNamethenSurname Then
                txtEmpField2EmployeeName.Text = objEmp._Employeecode & " - " & objEmp._Firstname & " " & objEmp._Surname
            Else
                txtEmpField2EmployeeName.Text = objEmp._Employeecode & " - " & objEmp._Surname & " " & objEmp._Firstname
            End If
            objEmp = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Set_EmpField3Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicEmpField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField3FieldData"), Dictionary(Of Integer, String))
        Try
            lblHeaderEmpField3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp3, 1, "Add/Edit Employee") & " " & objFieldMaster._Field3_Caption & " " & _
                      Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp3, 2, "Information")

            objEmpField3lblEmpField1.Text = objFieldMaster._Field1_Caption

            objEmpField3lblEmpField2.Text = objFieldMaster._Field2_Caption
            hdf_cboEmpField3EmpFieldValue2.Value = objFieldMaster._Field2Unkid

            objEmpField3lblOwrField3.Text = objFieldMaster._Field3_Caption
            hdf_txtEmpField3EmpField3.Value = objFieldMaster._Field3Unkid

            If CInt(Me.ViewState("mintEmpField3FieldUnkid")) = CInt(Me.ViewState("mintEmpField3LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    objEmpField3tabpRemark1.Enabled = False
                Else
                    objEmpField3tabpRemark1.HeaderText = objFieldMaster._Field6_Caption
                    lblEmpField3Remark1.Text = objFieldMaster._Field6_Caption
                    hdf_lblEmpField3Remark1.Value = objFieldMaster._Field6Unkid
                    If mdicEmpField3FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicEmpField3FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    objEmpField3tabpRemark2.Enabled = False
                Else
                    objEmpField3tabpRemark2.HeaderText = objFieldMaster._Field7_Caption
                    lblEmpField3Remark2.Text = objFieldMaster._Field7_Caption
                    hdf_lblEmpField3Remark2.Value = objFieldMaster._Field7Unkid
                    If mdicEmpField3FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicEmpField3FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    objEmpField3tabpRemark3.Enabled = False
                Else
                    objEmpField3tabpRemark3.HeaderText = objFieldMaster._Field8_Caption
                    lblEmpField3Remark3.Text = objFieldMaster._Field8_Caption
                    hdf_lblEmpField3Remark3.Value = objFieldMaster._Field8Unkid
                    If mdicEmpField3FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicEmpField3FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicEmpField3FieldData.Keys.Count > 0 Then objEmpField3tabcRemarks.Enabled = True
                pnl_EmpField3dgvOwner.Enabled = Session("FollowEmployeeHierarchy")
                txtEmpField3SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objEmpField3tabcRemarks.Enabled = False : pnl_RightEmpField3.Enabled = False : objEmpField3tabcRemarks.Enabled = False
            End If

            Dim objEmp As New clsEmployee_Master

            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(mintEmployeeId)
            If ConfigParameter._Object._FirstNamethenSurname Then
                txtEmpField3Employee.Text = objEmp._Employeecode & " - " & objEmp._Firstname & " " & objEmp._Surname
            Else
                txtEmpField3Employee.Text = objEmp._Employeecode & " - " & objEmp._Surname & " " & objEmp._Firstname
            End If
            objEmp = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Set_EmpField4Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicEmpField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField4FieldData"), Dictionary(Of Integer, String))
        Try
            lblHeaderEmpField4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp4, 1, "Add/Edit Employee") & " " & objFieldMaster._Field4_Caption & " " & _
                      Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp4, 2, "Information")

            objEmpField4lblEmpField1.Text = objFieldMaster._Field1_Caption
            objEmpField4lblEmpField2.Text = objFieldMaster._Field2_Caption
            objEmpField4lblEmpField3.Text = objFieldMaster._Field3_Caption
            hdf_cboEmpField4EmpFieldValue3.Value = objFieldMaster._Field3Unkid

            objEmpField4lblEmpField4.Text = objFieldMaster._Field4_Caption
            hdf_objEmpField4txtEmpField4.Value = objFieldMaster._Field4Unkid
            If CInt(Me.ViewState("mintEmpField4FieldUnkid")) = CInt(Me.ViewState("mintEmpField4LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    objEmpField4tabpRemark1.Enabled = False
                Else
                    objEmpField4tabpRemark1.HeaderText = objFieldMaster._Field6_Caption
                    lblEmpField4Remark1.Text = objFieldMaster._Field6_Caption
                    hdf_txtEmpField4Remark1.Value = objFieldMaster._Field6Unkid
                    If mdicEmpField4FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicEmpField4FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    objEmpField4tabpRemark2.Enabled = False
                Else
                    objEmpField4tabpRemark2.HeaderText = objFieldMaster._Field7_Caption
                    lblEmpField4Remark2.Text = objFieldMaster._Field7_Caption
                    hdf_txtEmpField4Remark2.Value = objFieldMaster._Field7Unkid
                    If mdicEmpField4FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicEmpField4FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    objEmpField4tabpRemark3.Enabled = False
                Else
                    objEmpField4tabpRemark3.HeaderText = objFieldMaster._Field8_Caption
                    lblEmpField4Remark3.Text = objFieldMaster._Field7_Caption
                    hdf_txtEmpField4Remark3.Value = objFieldMaster._Field8Unkid
                    If mdicEmpField4FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicEmpField4FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicEmpField4FieldData.Keys.Count > 0 Then objEmpField4tabcRemarks.Enabled = True
                pnl_EmpField4dgvOwner.Enabled = Session("FollowEmployeeHierarchy")
                txtEmpField4SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objEmpField4tabcRemarks.Enabled = False : pnl_RightEmpField4.Enabled = False : objEmpField4tabcRemarks.Enabled = False
            End If

            Dim objEmp As New clsEmployee_Master

            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(mintEmployeeId)

            If ConfigParameter._Object._FirstNamethenSurname Then
                txtEmpField4Employee.Text = objEmp._Employeecode & " - " & objEmp._Firstname & " " & objEmp._Surname
            Else
                txtEmpField4Employee.Text = objEmp._Employeecode & " - " & objEmp._Surname & " " & objEmp._Firstname
            End If
            objEmp = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Set_EmpField5Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicEmpField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField5FieldData"), Dictionary(Of Integer, String))
        Try
            lblHeaderEmpField5.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp5, 1, "Add/Edit Employee") & " " & objFieldMaster._Field5_Caption & " " & _
                      Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp5, 2, "Information")

            objEmpField5lblEmpField1.Text = objFieldMaster._Field1_Caption
            objEmpField5lblEmpField2.Text = objFieldMaster._Field2_Caption
            objEmpField5lblEmpField3.Text = objFieldMaster._Field3_Caption
            objEmpField5lblEmpField4.Text = objFieldMaster._Field4_Caption
            hdf_cboEmpField5EmpFieldValue4.Value = objFieldMaster._Field4Unkid

            objEmpField5lblEmpField5.Text = objFieldMaster._Field5_Caption
            hdf_objEmpField5txtEmpField5.Value = objFieldMaster._Field5Unkid

            If CInt(Me.ViewState("mintEmpField5FieldUnkid")) = CInt(Me.ViewState("mintEmpField5LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    objEmpField5tabpRemark1.Enabled = False
                Else
                    objEmpField5tabpRemark1.HeaderText = objFieldMaster._Field6_Caption
                    hdf_txtEmpField5Remark1.Value = objFieldMaster._Field6Unkid
                    If mdicEmpField5FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicEmpField5FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    objEmpField5tabpRemark2.Enabled = False
                Else
                    objEmpField5tabpRemark2.HeaderText = objFieldMaster._Field7_Caption
                    hdf_txtEmpField5Remark2.Value = objFieldMaster._Field7Unkid
                    If mdicEmpField5FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicEmpField5FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    objEmpField5tabpRemark3.Enabled = False
                Else
                    objEmpField5tabpRemark3.HeaderText = objFieldMaster._Field8_Caption
                    hdf_txtEmpField5Remark3.Value = objFieldMaster._Field8Unkid
                    If mdicEmpField5FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicEmpField5FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicEmpField5FieldData.Keys.Count > 0 Then objEmpField5tabcRemarks.Enabled = True
                pnl_EmpField5dgvOwner.Enabled = Session("FollowEmployeeHierarchy")
                txtEmpField5SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objEmpField5tabcRemarks.Enabled = False : pnl_RightEmpField5.Enabled = False : objEmpField5tabcRemarks.Enabled = False
            End If

            Dim objEmp As New clsEmployee_Master

            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(mintEmployeeId)

            If ConfigParameter._Object._FirstNamethenSurname Then
                txtEmpField5Employee.Text = objEmp._Employeecode & " - " & objEmp._Firstname & " " & objEmp._Surname
            Else
                txtEmpField5Employee.Text = objEmp._Employeecode & " - " & objEmp._Surname & " " & objEmp._Firstname
            End If
            objEmp = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo_EmpField1()
        Dim objMData As New clsMasterData
        Dim objOwrField1 As New clsassess_owrfield1_master
        Dim dsList As New DataSet
        Try
            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboEmpField1Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objOwrField1.getComboList(eZeeDate.convertDate(Session("EmployeeAsOnDate")), "List", True, CInt(cboPlanningPeriod.SelectedValue), CInt(mintEmployeeId), , True)

            With cboEmpField1FieldValue1
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            If Session("CascadingTypeId") = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                Dim objPerspective As New clsassess_perspective_master
                dsList = objPerspective.getComboList("List", True)
                With cboEmpField1Perspective
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsList.Tables("List")
                    .DataBind()
                    .SelectedValue = 0
                End With
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo_EmpField2()
        Dim objMData As New clsMasterData
        Dim objEmpField1 As New clsassess_empfield1_master
        Dim dsList As New DataSet
        Try
            dsList = objEmpField1.getComboList(mintEmployeeId, cboPlanningPeriod.SelectedValue, "List", True)
            With cboEmpField2EmpFieldValue1
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboEmpField2Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo_EmpField3()
        Dim objMData As New clsMasterData
        Dim objEmpField2 As New clsassess_empfield2_master
        Dim dsList As New DataSet
        Try
            dsList = objEmpField2.getComboList(mintEmployeeId, cboPlanningPeriod.SelectedValue, CInt(Me.ViewState("mintEmpField3MainParentId")), "List", True)
            With cboEmpField3EmpFieldValue2
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboEmpField3Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

        Finally
        End Try
    End Sub

    Private Sub FillCombo_EmpField4()
        Dim objMData As New clsMasterData
        Dim objEmpField3 As New clsassess_empfield3_master
        Dim dsList As New DataSet
        Try
            dsList = objEmpField3.getComboList(mintEmployeeId, cboPlanningPeriod.SelectedValue, CInt(Me.ViewState("mintEmpField4MainParentId")), "List", True)
            With cboEmpField4EmpFieldValue3
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboEmpField4Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo_EmpField5()
        Dim objMData As New clsMasterData
        Dim objField4 As New clsassess_empfield4_master
        Dim dsList As New DataSet
        Try
            dsList = objField4.getComboList(mintEmployeeId, cboPlanningPeriod.SelectedValue, Me.ViewState("mintEmpField5MainParentId"), "List", True)
            With cboEmpField5EmpFieldValue4
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboEmpField5Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_EmpField1()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim mdtEmpField1Owner As DataTable = CType(Me.ViewState("mdtEmpField1Owner"), DataTable)
        Try
            dList = objEmployee.GetEmployee_Access(mintEmployeeId, 0, Session("Database_Name"), cboPlanningPeriod.SelectedValue)

            If dList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    If mdtEmpField1Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtEmpField1Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,employeename"
            dgvEmpField1Owner.AutoGenerateColumns = False
            If txtEmpField1SearchEmp.Text.Trim.Length > 0 Then
                Dim strSearch As String = "employeecode LIKE '%" & txtEmpField1SearchEmp.Text & "%' OR " & _
                            "employeename LIKE '%" & txtEmpField1SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If
            dgvEmpField1Owner.DataSource = dtOwnerView
            dgvEmpField1Owner.DataBind()
            Me.ViewState("mdtEmpField1Owner") = mdtEmpField1Owner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_EmpField2()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim mdtEmpField2Owner As DataTable = CType(Me.ViewState("mdtEmpField2Owner"), DataTable)
        Try
            dList = objEmployee.GetEmployee_Access(mintEmployeeId, 0, Session("Database_Name"), cboPlanningPeriod.SelectedValue)

            If dList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    If mdtEmpField2Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtEmpField2Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,employeename"
            dgvEmpField2Owner.AutoGenerateColumns = False
            If txtEmpField2SearchEmp.Text.Trim.Length > 0 Then
                Dim strSearch As String = "employeecode LIKE '%" & txtEmpField2SearchEmp.Text & "%' OR " & _
                            "employeename LIKE '%" & txtEmpField2SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If

            dgvEmpField2Owner.DataSource = dtOwnerView
            dgvEmpField2Owner.DataBind()
            Me.ViewState("mdtEmpField2Owner") = mdtEmpField2Owner

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_EmpField3()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim mdtEmpField3Owner As DataTable = CType(Me.ViewState("mdtEmpField3Owner"), DataTable)
        Try

            dList = objEmployee.GetEmployee_Access(mintEmployeeId, 0, Session("Database_Name"), cboPlanningPeriod.SelectedValue)

            If dList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    If mdtEmpField3Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtEmpField3Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,employeename"
            dgvEmpField3Owner.AutoGenerateColumns = False
            If txtEmpField3SearchEmp.Text.Trim.Length > 0 Then
                Dim strSearch As String = "employeecode LIKE '%" & txtEmpField3SearchEmp.Text & "%' OR " & _
                            "employeename LIKE '%" & txtEmpField3SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If
            dgvEmpField3Owner.DataSource = dtOwnerView
            dgvEmpField3Owner.DataBind()
            Me.ViewState("mdtEmpField3Owner") = mdtEmpField3Owner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_EmpField4()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim mdtEmpField4Owner As DataTable = CType(Me.ViewState("mdtEmpField4Owner"), DataTable)
        Try

            dList = objEmployee.GetEmployee_Access(mintEmployeeId, 0, Session("Database_Name"), cboPlanningPeriod.SelectedValue)

            If dList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    If mdtEmpField4Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtEmpField4Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,employeename"

            dgvEmpField4Owner.AutoGenerateColumns = False
            If txtEmpField4SearchEmp.Text.Trim.Length > 0 Then
                Dim strSearch As String = "employeecode LIKE '%" & txtEmpField4SearchEmp.Text & "%' OR " & _
                            "employeename LIKE '%" & txtEmpField4SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If
            dgvEmpField4Owner.DataSource = dtOwnerView
            dgvEmpField4Owner.DataBind()
            Me.ViewState("mdtEmpField4Owner") = mdtEmpField4Owner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_EmpField5()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim mdtEmpField4Owner As DataTable = CType(Me.ViewState("mdtEmpField4Owner"), DataTable)
        Try

            dList = objEmployee.GetEmployee_Access(mintEmployeeId, 0, Session("Database_Name"), cboPlanningPeriod.SelectedValue)

            If dList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    If mdtEmpField4Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtEmpField4Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,employeename"
            dgvEmpField5Owner.AutoGenerateColumns = False
            If txtEmpField5SearchEmp.Text.Trim.Length > 0 Then
                Dim strSearch As String = "employeecode LIKE '%" & txtEmpField5SearchEmp.Text & "%' OR " & _
                            "employeename LIKE '%" & txtEmpField5SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If
            dgvEmpField5Owner.DataSource = dtOwnerView
            dgvEmpField5Owner.DataBind()
            Me.ViewState("mdtEmpField4Owner") = mdtEmpField4Owner
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "Fill_Data", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub GetEmpField1Value()
        Dim enEmpField1Action As enAction = CType(Me.ViewState("menEmpField1Action"), enAction)
        Dim mdicEmpField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField1FieldData"), Dictionary(Of Integer, String))
        Try
            If objEmpField1._Enddate <> Nothing Then
                dtpEmpField1EndDate.SetDate = objEmpField1._Enddate
            End If
            txtEmpField1EmpField1.Text = objEmpField1._Field_Data
            objEmpField1._Isfinal = objEmpField1._Isfinal
            Try
                cboEmpField1FieldValue1.SelectedValue = objEmpField1._Owrfield1unkid
            Catch ex As Exception
                Throw ex
            End Try
            txtEmpField1Percent.Text = objEmpField1._Pct_Completed
            If objEmpField1._Startdate <> Nothing Then
                dtpEmpField1StartDate.SetDate = objEmpField1._Startdate
            End If
            cboEmpField1Status.SelectedValue = IIf(objEmpField1._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objEmpField1._Statusunkid)
            objEmpField1._Userunkid = Session("UserId")
            txtEmpField1Weight.Text = CDec(objEmpField1._Weight)
            If enEmpField1Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_empinfofield_tran
                mdicEmpField1FieldData = objInfoField.Get_Data(CInt(Me.ViewState("mintEmpField1Unkid")), enWeight_Types.WEIGHT_FIELD1)
                If mdicEmpField1FieldData.Keys.Count > 0 Then
                    If mdicEmpField1FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField1Remark1.Value = "", 0, hdf_lblEmpField1Remark1.Value))) Then
                        txtEmpField1Remark1.Text = mdicEmpField1FieldData(CInt(hdf_lblEmpField1Remark1.Value))
                    End If
                    If mdicEmpField1FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField1Remark2.Value = "", 0, hdf_lblEmpField1Remark2.Value))) Then
                        txtEmpField1Remark2.Text = mdicEmpField1FieldData(CInt(hdf_lblEmpField1Remark2.Value))
                    End If
                    If mdicEmpField1FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField1Remark3.Value = "", 0, hdf_lblEmpField1Remark3.Value))) Then
                        txtEmpField1Remark3.Text = mdicEmpField1FieldData(CInt(hdf_lblEmpField1Remark3.Value))
                    End If
                End If
                objInfoField = Nothing
            End If
            If cboEmpField1Perspective.Enabled = True Then
                cboEmpField1Perspective.SelectedValue = objEmpField1._Perspectiveunkid
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GetEmpField2Value()
        Dim enEmpField2Action As enAction = CType(Me.ViewState("menEmpField2Action"), enAction)
        Dim mdicEmpFie2d1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField2FieldData"), Dictionary(Of Integer, String))
        Try
            Try
                cboEmpField2EmpFieldValue1.SelectedValue = objEmpField2._Empfield1unkid
            Catch ex As Exception
                Throw ex
            End Try
            If objEmpField2._Enddate <> Nothing Then
                dtpEmpField2EndDate.SetDate = objEmpField2._Enddate
            End If
            txtEmpField2EmpField2.Text = objEmpField2._Field_Data
            txtEmpField2Percent.Text = objEmpField2._Pct_Completed
            If objEmpField2._Startdate <> Nothing Then
                dtpEmpField2StartDate.SetDate = objEmpField2._Startdate
            End If
            cboEmpField2Status.SelectedValue = IIf(objEmpField2._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objEmpField2._Statusunkid)
            txtEmpField2Weight.Text = CDec(objEmpField2._Weight)
            If enEmpField2Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_empinfofield_tran
                mdicEmpFie2d1FieldData = objInfoField.Get_Data(CInt(Me.ViewState("mintEmpField2Unkid")), enWeight_Types.WEIGHT_FIELD2)
                If mdicEmpFie2d1FieldData.Keys.Count > 0 Then
                    If mdicEmpFie2d1FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField2Remark1.Value = "", 0, hdf_lblEmpField2Remark1.Value))) Then
                        txtEmpField2Remark1.Text = mdicEmpFie2d1FieldData(CInt(hdf_lblEmpField2Remark1.Value))
                    End If
                    If mdicEmpFie2d1FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField2Remark2.Value = "", 0, hdf_lblEmpField2Remark2.Value))) Then
                        txtEmpField2Remark2.Text = mdicEmpFie2d1FieldData(CInt(hdf_lblEmpField2Remark2.Value))
                    End If
                    If mdicEmpFie2d1FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField2Remark3.Value = "", 0, hdf_lblEmpField2Remark3.Value))) Then
                        txtEmpField2Remark3.Text = mdicEmpFie2d1FieldData(CInt(hdf_lblEmpField2Remark3.Value))
                    End If
                End If
                objInfoField = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GetEmpField3Value()
        Dim enEmpField3Action As enAction = CType(Me.ViewState("menEmpField3Action"), enAction)
        Dim mdicEmpField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField3FieldData"), Dictionary(Of Integer, String))
        Try
            Try
                cboEmpField3EmpFieldValue2.SelectedValue = objEmpField3._Empfield2unkid
            Catch ex As Exception
                Throw ex
            End Try
            If objEmpField3._Enddate <> Nothing Then
                dtpEmpField3EndDate.SetDate = objEmpField3._Enddate
            End If
            txtEmpField3EmpField3.Text = objEmpField3._Field_Data
            txtEmpField3Percent.Text = objEmpField3._Pct_Completed
            If objEmpField3._Startdate <> Nothing Then
                dtpEmpField3StartDate.SetDate = objEmpField3._Startdate
            End If
            cboEmpField3Status.SelectedValue = IIf(objEmpField3._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objEmpField3._Statusunkid)
            txtEmpField3Weight.Text = CDec(objEmpField3._Weight)
            If enEmpField3Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_empinfofield_tran
                mdicEmpField3FieldData = objInfoField.Get_Data(CInt(Me.ViewState("mintEmpField3Unkid")), enWeight_Types.WEIGHT_FIELD3)
                If mdicEmpField3FieldData.Keys.Count > 0 Then
                    If mdicEmpField3FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField3Remark1.Value = "", 0, hdf_lblEmpField3Remark1.Value))) Then
                        txtEmpField3Remark1.Text = mdicEmpField3FieldData(CInt(hdf_lblEmpField3Remark1.Value))
                    End If
                    If mdicEmpField3FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField3Remark2.Value = "", 0, hdf_lblEmpField3Remark2.Value))) Then
                        txtEmpField3Remark2.Text = mdicEmpField3FieldData(CInt(hdf_lblEmpField3Remark2.Value))
                    End If
                    If mdicEmpField3FieldData.ContainsKey(CInt(IIf(hdf_lblEmpField3Remark3.Value = "", 0, hdf_lblEmpField3Remark3.Value))) Then
                        txtEmpField3Remark3.Text = mdicEmpField3FieldData(CInt(hdf_lblEmpField3Remark3.Value))
                    End If
                End If
                objInfoField = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GetEmpField4Value()
        Dim enEmpField4Action As enAction = CType(Me.ViewState("menEmpField4Action"), enAction)
        Dim mdicEmpField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField4FieldData"), Dictionary(Of Integer, String))
        Try
            Try
                cboEmpField4EmpFieldValue3.SelectedValue = objEmpField4._Empfield3unkid
            Catch ex As Exception
                Throw ex
            End Try
            If objEmpField4._Enddate <> Nothing Then
                dtpEmpField4EndDate.SetDate = objEmpField4._Enddate
            End If
            objEmpField4txtEmpField4.Text = objEmpField4._Field_Data
            txtEmpField4Percent.Text = objEmpField4._Pct_Completed
            If objEmpField4._Startdate <> Nothing Then
                dtpEmpField4StartDate.SetDate = objEmpField4._Startdate
            End If
            cboEmpField4Status.SelectedValue = IIf(objEmpField4._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objEmpField4._Statusunkid)
            txtEmpField4Weight.Text = CDec(objEmpField4._Weight)
            If enEmpField4Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_empinfofield_tran
                mdicEmpField4FieldData = objInfoField.Get_Data(CInt(Me.ViewState("mintEmpField4FieldUnkid")), enWeight_Types.WEIGHT_FIELD4)
                If mdicEmpField4FieldData.Keys.Count > 0 Then
                    If mdicEmpField4FieldData.ContainsKey(CInt(IIf(hdf_txtEmpField4Remark1.Value = "", 0, hdf_txtEmpField4Remark1.Value))) Then
                        txtEmpField4Remark1.Text = mdicEmpField4FieldData(CInt(hdf_txtEmpField4Remark1.Value))
                    End If
                    If mdicEmpField4FieldData.ContainsKey(CInt(IIf(hdf_txtEmpField4Remark2.Value = "", 0, hdf_txtEmpField4Remark2.Value))) Then
                        txtEmpField4Remark2.Text = mdicEmpField4FieldData(CInt(hdf_txtEmpField4Remark2.Value))
                    End If
                    If mdicEmpField4FieldData.ContainsKey(CInt(IIf(hdf_txtEmpField4Remark3.Value = "", 0, hdf_txtEmpField4Remark3.Value))) Then
                        txtEmpField4Remark3.Text = mdicEmpField4FieldData(CInt(hdf_txtEmpField4Remark3.Value))
                    End If
                End If
                objInfoField = Nothing
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub GetEmpField5Value()
        Dim enEmpField5Action As enAction = CType(Me.ViewState("menEmpField5Action"), enAction)
        Dim mdicEmpField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField5FieldData"), Dictionary(Of Integer, String))
        Try
            Try
                cboEmpField5EmpFieldValue4.SelectedValue = objEmpField5._Empfield4unkid
            Catch ex As Exception
                Throw ex
            End Try
            If objEmpField5._Enddate <> Nothing Then
                dtpEmpField5EndDate.SetDate = objEmpField5._Enddate
            End If
            objEmpField5txtEmpField5.Text = objEmpField5._Field_Data
            txtEmpField5Percent.Text = objEmpField5._Pct_Completed
            If objEmpField5._Startdate <> Nothing Then
                dtpEmpField5StartDate.SetDate = objEmpField5._Startdate
            End If
            cboEmpField5Status.SelectedValue = IIf(objEmpField5._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objEmpField5._Statusunkid)
            txtEmpField5Weight.Text = CDec(objEmpField5._Weight)
            If enEmpField5Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_empinfofield_tran
                mdicEmpField5FieldData = objInfoField.Get_Data(Me.ViewState("mintEmpField5Unkid"), enWeight_Types.WEIGHT_FIELD5)
                If mdicEmpField5FieldData.Keys.Count > 0 Then
                    If mdicEmpField5FieldData.ContainsKey(CInt(IIf(hdf_txtEmpField5Remark1.Value = "", 0, hdf_txtEmpField5Remark1.Value))) Then
                        txtEmpField5Remark1.Text = mdicEmpField5FieldData(CInt(hdf_txtEmpField5Remark1.Value))
                    End If
                    If mdicEmpField5FieldData.ContainsKey(CInt(IIf(hdf_txtEmpField5Remark2.Value = "", 0, hdf_txtEmpField5Remark2.Value))) Then
                        txtEmpField5Remark2.Text = mdicEmpField5FieldData(CInt(hdf_txtEmpField5Remark2.Value))
                    End If
                    If mdicEmpField5FieldData.ContainsKey(CInt(IIf(hdf_txtEmpField5Remark3.Value = "", 0, hdf_txtEmpField5Remark3.Value))) Then
                        txtEmpField5Remark3.Text = mdicEmpField5FieldData(CInt(hdf_txtEmpField5Remark3.Value))
                    End If
                End If
                objInfoField = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function IsValidData_EmpField1() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim enEmpField1Action As enAction = CType(Me.ViewState("menEmpField1Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            Select Case Session("CascadingTypeId")
                Case enPACascading.STRICT_CASCADING, enPACascading.STRICT_GOAL_ALIGNMENT
                    If CInt(cboEmpField1FieldValue1.SelectedValue) <= 0 Then
                        iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp1, 5, "Sorry Owner, ") & objFieldMaster._Field1_Caption & _
                               Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp1, 6, " is mandatory information. Please select ") & objFieldMaster._Field1_Caption & _
                               Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp1, 7, " to continue.")
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        cboEmpField1FieldValue1.Focus()
                        Return False
                    End If
            End Select

            If txtEmpField1EmpField1.Text.Trim.Length <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp1, 8, "Sorry, ") & objFieldMaster._Field1_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp1, 9, " is mandatory information. Please provide ") & objFieldMaster._Field1_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp1, 7, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                txtEmpField1EmpField1.Focus()
                Return False
            End If

            Select Case Session("CascadingTypeId")
                Case enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT, enPACascading.LOOSE_CASCADING, enPACascading.LOOSE_GOAL_ALIGNMENT
                    If CInt(cboEmpField1FieldValue1.SelectedValue) <= 0 Then
                        If CInt(IIf(cboEmpField1Perspective.SelectedValue = "", 0, cboEmpField1Perspective.SelectedValue)) <= 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp1, 16, "Sorry, Perspective is mandatory information. Please select Perspective to continue."), Me)
                            cboEmpField1Perspective.Focus()
                            Return False
                        End If
                    End If
            End Select

            If CInt(Me.ViewState("mintEmpField1FieldUnkid")) = CInt(Me.ViewState("mintEmpField1LinkedFieldId")) Then
                Dim iWeight As Decimal = 0
                Decimal.TryParse(txtEmpField1Weight.Text, iWeight)
                If iWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp1, 11, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    txtEmpField1Weight.Focus()
                    Return False
                End If

                If iWeight > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, enWeight_Types.WEIGHT_FIELD1, iWeight, CInt(cboPlanningPeriod.SelectedValue), CInt(Me.ViewState("mintEmpField1LinkedFieldId")), 0, CInt(mintEmployeeId), enEmpField1Action, CInt(Me.ViewState("mintEmpField1Unkid")))
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        txtEmpField1Weight.Focus()
                        Return False
                    End If
                End If
                iWeight = 0 : Decimal.TryParse(txtEmpField1Percent.Text, iWeight)
                If iWeight > 100 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp1, 18, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                    txtEmpField1Percent.Focus()
                    Return False
                End If

                If CInt(cboEmpField1Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp1, 10, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    cboEmpField1Status.Focus()
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Function IsValidData_EmpField2() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim enEmpField2Action As enAction = CType(Me.ViewState("menEmpField2Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            If CInt(cboEmpField2EmpFieldValue1.SelectedValue) <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp2, 3, "Sorry, ") & objFieldMaster._Field1_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp2, 4, " is mandatory information. Please select ") & objFieldMaster._Field1_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp2, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                cboEmpField2EmpFieldValue1.Focus()
                Return False
            End If

            If txtEmpField2EmpField2.Text.Trim.Length <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp2, 3, "Sorry, ") & objFieldMaster._Field2_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp2, 6, " is mandatory information. Please provide ") & objFieldMaster._Field2_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp2, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                txtEmpField2EmpField2.Focus()
                Return False
            End If

            If CInt(Me.ViewState("mintEmpField2FieldUnkid")) = CInt(Me.ViewState("mintEmpField2LinkedFieldId")) Then
                Dim iWeight As Decimal = 0
                Decimal.TryParse(txtEmpField2Weight.Text, iWeight)
                If iWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp2, 8, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    txtEmpField2Weight.Focus()
                    Return False
                End If

                If iWeight > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, enWeight_Types.WEIGHT_FIELD2, iWeight, cboPlanningPeriod.SelectedValue, CInt(Me.ViewState("mintEmpField2LinkedFieldId")), 0, CInt(mintEmployeeId), enEmpField2Action, Me.ViewState("mintEmpField2Unkid"))
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        txtEmpField2Weight.Focus()
                        Return False
                    End If
                End If

                iWeight = 0 : Decimal.TryParse(txtEmpField2Percent.Text, iWeight)
                If iWeight > 100 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp2, 13, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                    txtEmpField2Percent.Focus()
                    Return False
                End If

                If CInt(cboEmpField2Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp2, 7, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    cboEmpField2Status.Focus()
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Function IsValidData_EmpField3() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim enEmpField3Action As enAction = CType(Me.ViewState("menEmpField3Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            If CInt(cboEmpField3EmpFieldValue2.SelectedValue) <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp3, 3, "Sorry, ") & objFieldMaster._Field2_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp3, 4, " is mandatory information. Please select ") & objFieldMaster._Field2_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp3, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                cboEmpField3EmpFieldValue2.Focus()
                Return False
            End If

            If txtEmpField3EmpField3.Text.Trim.Length <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp3, 3, "Sorry, ") & objFieldMaster._Field3_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp3, 6, " is mandatory information. Please provide ") & objFieldMaster._Field3_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp3, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                txtEmpField3EmpField3.Focus()
                Return False
            End If

            If CInt(Me.ViewState("mintEmpField3FieldUnkid")) = CInt(Me.ViewState("mintEmpField3LinkedFieldId")) Then
                Dim iWeight As Decimal = 0
                Decimal.TryParse(txtEmpField3Weight.Text, iWeight)
                If iWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp3, 8, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    txtEmpField3Weight.Focus()
                    Return False
                End If

                If iWeight > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, enWeight_Types.WEIGHT_FIELD3, iWeight, CInt(cboPlanningPeriod.SelectedValue), CInt(Me.ViewState("mintEmpField3LinkedFieldId")), 0, CInt(mintEmployeeId), enEmpField3Action, CInt(Me.ViewState("mintEmpField3Unkid")))
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        txtEmpField3Weight.Focus()
                        Return False
                    End If
                End If

                iWeight = 0 : Decimal.TryParse(txtEmpField3Percent.Text, 0)
                If iWeight > 100 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp3, 13, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                    txtEmpField3Percent.Focus()
                    Return False
                End If

                If CInt(cboEmpField3Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp3, 7, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    cboEmpField3Status.Focus()
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Function IsValidData_EmpField4() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim enEmpField4Action As enAction = CType(Me.ViewState("menEmpField4Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            If CInt(cboEmpField4EmpFieldValue3.SelectedValue) <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp4, 3, "Sorry, ") & objFieldMaster._Field3_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp4, 4, " is mandatory information. Please select ") & objFieldMaster._Field3_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp4, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                cboEmpField4EmpFieldValue3.Focus()
                Return False
            End If

            If objEmpField4txtEmpField4.Text.Trim.Length <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp4, 3, "Sorry, ") & objFieldMaster._Field4_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp4, 6, " is mandatory information. Please provide ") & objFieldMaster._Field4_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp4, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                objEmpField4txtEmpField4.Focus()
                Return False
            End If

            If CInt(Me.ViewState("mintEmpField4FieldUnkid")) = CInt(Me.ViewState("mintEmpField4LinkedFieldId")) Then
                Dim iWeight As Decimal = 0
                Decimal.TryParse(txtEmpField4Weight.Text, iWeight)
                If iWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp4, 7, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    txtEmpField4Weight.Focus()
                    Return False
                End If

                If iWeight > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, enWeight_Types.WEIGHT_FIELD4, iWeight, CInt(cboPlanningPeriod.SelectedValue), CInt(Me.ViewState("mintEmpField4LinkedFieldId")), 0, CInt(mintEmployeeId), enEmpField4Action, CInt(Me.ViewState("mintEmpField4Unkid")))
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        txtEmpField4Weight.Focus()
                        Return False
                    End If
                End If
                iWeight = 0 : Decimal.TryParse(txtEmpField4Percent.Text, iWeight)
                If iWeight > 100 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp4, 13, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                    txtEmpField4Percent.Focus()
                    Return False
                End If

                If CInt(cboEmpField4Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp4, 8, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    cboEmpField4Status.Focus()
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Function IsValidData_EmpField5() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim menEmpField5Action As enAction = CType(Me.ViewState("menEmpField5Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            If CInt(cboEmpField5EmpFieldValue4.SelectedValue) <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp5, 3, "Sorry, ") & objFieldMaster._Field4_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp5, 4, " is mandatory information. Please select ") & objFieldMaster._Field4_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp5, 5, " to continue.")
                DisplayMessage.DisplayMessage(iMsg, Me)
                cboEmpField5EmpFieldValue4.Focus()
                Return False
            End If

            If objEmpField5txtEmpField5.Text.Trim.Length <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp5, 3, "Sorry, ") & objFieldMaster._Field5_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp5, 6, " is mandatory information. Please provide ") & objFieldMaster._Field5_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp5, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                objEmpField5txtEmpField5.Focus()
                Return False
            End If

            If CInt(Me.ViewState("mintEmpField5FieldUnkid")) = CInt(Me.ViewState("mintEmpField5LinkedFieldId")) Then
                Dim iWeight As Decimal = 0
                Decimal.TryParse(txtEmpField5Weight.Text, iWeight)
                If iWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp5, 7, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    txtEmpField5Weight.Focus()
                    Return False
                End If

                If iWeight > 0 Then
                    Dim objMapping As New clsAssess_Field_Mapping
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, enWeight_Types.WEIGHT_FIELD5, iWeight, CInt(cboPlanningPeriod.SelectedValue), CInt(Me.ViewState("mintEmpField5LinkedFieldId")), 0, CInt(mintEmployeeId), menEmpField5Action, CInt(Me.ViewState("mintEmpField5Unkid")))
                    objMapping = Nothing
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        txtEmpField5Weight.Focus()
                        Return False
                    End If
                End If

                iWeight = 0 : Decimal.TryParse(txtEmpField5Percent.Text, iWeight)
                If iWeight > 100 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp5, 13, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                    txtEmpField5Percent.Focus()
                    Return False
                End If

                If CInt(cboEmpField5Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp5, 8, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    cboEmpField5Status.Focus()
                    Return False
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    'Private Sub SetEmpField1Value()
    '    Try
    '        objEmpField1._Empfield1unkid = CInt(Me.ViewState("mintEmpField1Unkid"))
    '        objEmpField1._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD1
    '        objEmpField1._Employeeunkid = mintEmployeeId
    '        If dtpEmpField1EndDate.IsNull = False Then
    '            objEmpField1._Enddate = dtpEmpField1EndDate.GetDate
    '        End If
    '        objEmpField1._Field_Data = txtEmpField1EmpField1.Text
    '        objEmpField1._Fieldunkid = CInt(Me.ViewState("mintEmpField1FieldUnkid"))
    '        objEmpField1._Isfinal = False
    '        objEmpField1._Isvoid = False
    '        objEmpField1._Owrfield1unkid = CInt(cboEmpField1FieldValue1.SelectedValue)
    '        objEmpField1._Pct_Completed = txtEmpField1Percent.Text
    '        objEmpField1._Periodunkid = CInt(cboPeriod.SelectedValue)
    '        If dtpEmpField1StartDate.IsNull = False Then
    '            objEmpField1._Startdate = dtpEmpField1StartDate.GetDate
    '        End If
    '        objEmpField1._Statusunkid = CInt(cboEmpField1Status.SelectedValue)
    '        objEmpField1._Userunkid = Session("UserId")
    '        objEmpField1._Voiddatetime = Nothing
    '        objEmpField1._Voidreason = ""
    '        objEmpField1._Voiduserunkid = -1
    '        objEmpField1._Weight = txtEmpField1Weight.Text
    '        objEmpField1._Yearunkid = 0
    '        If cboEmpField1Perspective.Enabled = True AndAlso CStr(cboEmpField1Perspective.SelectedValue) <> "" Then
    '            If CInt(cboEmpField1Perspective.SelectedValue) > 0 Then
    '                objEmpField1._Perspectiveunkid = CInt(cboEmpField1Perspective.SelectedValue)
    '            End If
    '        End If

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub SetEmpField2Value()
    '    Try
    '        objEmpField2._Empfield2unkid = Me.ViewState("mintEmpField2Unkid")
    '        objEmpField2._Empfield1unkid = CInt(cboEmpField2EmpFieldValue1.SelectedValue)
    '        objEmpField2._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD2
    '        If dtpEmpField2EndDate.IsNull = False Then
    '            objEmpField2._Enddate = dtpEmpField2EndDate.GetDate
    '        End If
    '        objEmpField2._Field_Data = txtEmpField2EmpField2.Text
    '        objEmpField2._Fieldunkid = Me.ViewState("mintEmpField2FieldUnkid")
    '        objEmpField2._Isvoid = False
    '        objEmpField2._Pct_Completed = txtEmpField2Percent.Text
    '        If dtpEmpField2StartDate.IsNull = False Then
    '            objEmpField2._Startdate = dtpEmpField2StartDate.GetDate
    '        End If
    '        objEmpField2._Statusunkid = CInt(cboEmpField2Status.SelectedValue)
    '        objEmpField2._Userunkid = Session("UserId")
    '        objEmpField2._Voiddatetime = Nothing
    '        objEmpField2._Voidreason = ""
    '        objEmpField2._Voiduserunkid = -1
    '        objEmpField2._Weight = txtEmpField2Weight.Text
    '        objEmpField2._Employeeunkid = mintEmployeeId
    '        objEmpField2._Periodunkid = CInt(cboPeriod.SelectedValue)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub SetEmpField3Value()
    '    Try
    '        objEmpField3._Empfield3unkid = Me.ViewState("mintEmpField3Unkid")
    '        objEmpField3._Empfield2unkid = CInt(cboEmpField3EmpFieldValue2.SelectedValue)
    '        objEmpField3._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD3
    '        If dtpEmpField3EndDate.IsNull = False Then
    '            objEmpField3._Enddate = dtpEmpField3EndDate.GetDate
    '        End If
    '        objEmpField3._Field_Data = txtEmpField3EmpField3.Text
    '        objEmpField3._Fieldunkid = CInt(Me.ViewState("mintEmpField3FieldUnkid"))
    '        objEmpField3._Isvoid = False
    '        objEmpField3._Pct_Completed = txtEmpField3Percent.Text
    '        If dtpEmpField3StartDate.IsNull = False Then
    '            objEmpField3._Startdate = dtpEmpField3StartDate.GetDate
    '        End If
    '        objEmpField3._Statusunkid = CInt(cboEmpField3Status.SelectedValue)
    '        objEmpField3._Userunkid = Session("UserId")
    '        objEmpField3._Voiddatetime = Nothing
    '        objEmpField3._Voidreason = ""
    '        objEmpField3._Voiduserunkid = -1
    '        objEmpField3._Weight = txtEmpField3Weight.Text
    '        objEmpField3._Employeeunkid = mintEmployeeId
    '        objEmpField3._Periodunkid = CInt(cboPeriod.SelectedValue)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub SetEmpField4Value()
    '    Try
    '        objEmpField4._Empfield4unkid = Me.ViewState("mintEmpField4Unkid")
    '        objEmpField4._Empfield3unkid = CInt(cboEmpField4EmpFieldValue3.SelectedValue)
    '        objEmpField4._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD4
    '        If dtpEmpField4EndDate.IsNull = False Then
    '            objEmpField4._Enddate = dtpEmpField4EndDate.GetDate
    '        Else
    '            objEmpField4._Enddate = Nothing
    '        End If
    '        objEmpField4._Field_Data = objEmpField4txtEmpField4.Text
    '        objEmpField4._Fieldunkid = CInt(Me.ViewState("mintEmpField4FieldUnkid"))
    '        objEmpField4._Isvoid = False
    '        objEmpField4._Pct_Completed = txtEmpField4Percent.Text
    '        If dtpEmpField4StartDate.IsNull = False Then
    '            objEmpField4._Startdate = dtpEmpField4StartDate.GetDate
    '        Else
    '            objEmpField4._Startdate = Nothing
    '        End If
    '        objEmpField4._Statusunkid = CInt(cboEmpField4Status.SelectedValue)
    '        objEmpField4._Userunkid = Session("UserId")
    '        objEmpField4._Voiddatetime = Nothing
    '        objEmpField4._Voidreason = ""
    '        objEmpField4._Voiduserunkid = -1
    '        objEmpField4._Weight = txtEmpField4Weight.Text
    '        objEmpField4._Employeeunkid = mintEmployeeId
    '        objEmpField4._Periodunkid = CInt(cboPeriod.SelectedValue)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub SetEmpField5Value()
    '    Try
    '        objEmpField5._Empfield5unkid = Me.ViewState("mintEmpField5Unkid")
    '        objEmpField5._Empfield4unkid = CInt(cboEmpField5EmpFieldValue4.SelectedValue)
    '        objEmpField5._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD5
    '        If dtpEmpField5EndDate.IsNull = False Then
    '            objEmpField5._Enddate = dtpEmpField5EndDate.GetDate
    '        Else
    '            objEmpField5._Enddate = Nothing
    '        End If
    '        objEmpField5._Field_Data = objEmpField5txtEmpField5.Text
    '        objEmpField5._Fieldunkid = CInt(Me.ViewState("mintEmpField5FieldUnkid"))
    '        objEmpField5._Isvoid = False
    '        objEmpField5._Pct_Completed = txtEmpField5Percent.Text
    '        If dtpEmpField5StartDate.IsNull = False Then
    '            objEmpField5._Startdate = dtpEmpField5StartDate.GetDate
    '        Else
    '            objEmpField5._Startdate = Nothing
    '        End If
    '        objEmpField5._Statusunkid = CInt(cboEmpField5Status.SelectedValue)
    '        objEmpField5._Userunkid = Session("UserId")
    '        objEmpField5._Voiddatetime = Nothing
    '        objEmpField5._Voidreason = ""
    '        objEmpField5._Voiduserunkid = -1
    '        objEmpField5._Weight = txtEmpField5Weight.Text
    '        objEmpField5._Employeeunkid = mintEmployeeId
    '        objEmpField5._Periodunkid = mintP
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub GoalEmpOperation_Emp1(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
    '    Try
    '        'Dim mdtEmp As DataTable = CType(Me.ViewState("mdtEmpField1Owner"), DataTable)
    '        If mdtGoals_List2 IsNot Nothing Then
    '            Dim dtmp() As DataRow = mdtGoals_List2.Select("employeeunkid = '" & iTagUnkid & "'")
    '            If dtmp.Length > 0 Then
    '                If iFlag = False Then
    '                    dtmp(0).Item("AUD") = "D"
    '                End If
    '            Else
    '                If iFlag = True Then
    '                    Dim dRow As DataRow = mdtGoals_List2.NewRow
    '                    dRow.Item("ownertranunkid") = -1
    '                    dRow.Item("empfieldunkid") = CInt(Me.ViewState("mintEmpField1Unkid"))
    '                    dRow.Item("employeeunkid") = iTagUnkid
    '                    dRow.Item("empfieldtypeid") = enWeight_Types.WEIGHT_FIELD5
    '                    dRow.Item("AUD") = "A"
    '                    dRow.Item("GUID") = Guid.NewGuid.ToString
    '                    mdtGoals_List2.Rows.Add(dRow)
    '                End If
    '                mdtGoals_List2.AcceptChanges()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub GoalEmpOperation_Emp2(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
    '    Try
    '        Dim mdtEmp As DataTable = CType(Me.ViewState("mdtEmpField2Owner"), DataTable)
    '        If mdtEmp IsNot Nothing Then
    '            Dim dtmp() As DataRow = mdtEmp.Select("employeeunkid = '" & iTagUnkid & "'")
    '            If dtmp.Length > 0 Then
    '                If iFlag = False Then
    '                    dtmp(0).Item("AUD") = "D"
    '                End If
    '            Else
    '                If iFlag = True Then
    '                    Dim dRow As DataRow = mdtEmp.NewRow
    '                    dRow.Item("ownertranunkid") = -1
    '                    dRow.Item("empfieldunkid") = CInt(Me.ViewState("mintEmpField2Unkid"))
    '                    dRow.Item("employeeunkid") = iTagUnkid
    '                    dRow.Item("empfieldtypeid") = enWeight_Types.WEIGHT_FIELD5
    '                    dRow.Item("AUD") = "A"
    '                    dRow.Item("GUID") = Guid.NewGuid.ToString
    '                    mdtEmp.Rows.Add(dRow)
    '                End If
    '                mdtEmp.AcceptChanges()
    '                Me.ViewState("mdtEmpField2Owner") = mdtEmp
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub GoalEmpOperation_Emp3(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
    '    Try
    '        Dim mdtEmp As DataTable = CType(Me.ViewState("mdtEmpField3Owner"), DataTable)
    '        If mdtEmp IsNot Nothing Then
    '            Dim dtmp() As DataRow = mdtEmp.Select("employeeunkid = '" & iTagUnkid & "'")
    '            If dtmp.Length > 0 Then
    '                If iFlag = False Then
    '                    dtmp(0).Item("AUD") = "D"
    '                End If
    '            Else
    '                If iFlag = True Then
    '                    Dim dRow As DataRow = mdtEmp.NewRow
    '                    dRow.Item("ownertranunkid") = -1
    '                    dRow.Item("empfieldunkid") = CInt(Me.ViewState("mintEmpField3Unkid"))
    '                    dRow.Item("employeeunkid") = iTagUnkid
    '                    dRow.Item("empfieldtypeid") = enWeight_Types.WEIGHT_FIELD5
    '                    dRow.Item("AUD") = "A"
    '                    dRow.Item("GUID") = Guid.NewGuid.ToString
    '                    mdtEmp.Rows.Add(dRow)
    '                End If
    '                mdtEmp.AcceptChanges()
    '                Me.ViewState("mdtEmpField3Owner") = mdtEmp
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub GoalEmpOperation_Emp4(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
    '    Try
    '        Dim mdtEmp As DataTable = CType(Me.ViewState("mdtEmpField4Owner"), DataTable)
    '        If mdtEmp IsNot Nothing Then
    '            Dim dtmp() As DataRow = mdtEmp.Select("employeeunkid = '" & iTagUnkid & "'")
    '            If dtmp.Length > 0 Then
    '                If iFlag = False Then
    '                    dtmp(0).Item("AUD") = "D"
    '                End If
    '            Else
    '                If iFlag = True Then
    '                    Dim dRow As DataRow = mdtEmp.NewRow
    '                    dRow.Item("ownertranunkid") = -1
    '                    dRow.Item("empfieldunkid") = CInt(Me.ViewState("mintEmpField4Unkid"))
    '                    dRow.Item("employeeunkid") = iTagUnkid
    '                    dRow.Item("empfieldtypeid") = enWeight_Types.WEIGHT_FIELD5
    '                    dRow.Item("AUD") = "A"
    '                    dRow.Item("GUID") = Guid.NewGuid.ToString
    '                    mdtEmp.Rows.Add(dRow)
    '                End If
    '                mdtEmp.AcceptChanges()
    '                Me.ViewState("mdtEmpField4Owner") = mdtEmp
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    'Private Sub GoalEmpOperation_Emp5(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
    '    Try
    '        Dim mdtEmp As DataTable = CType(Me.ViewState("mdtEmpField5Owner"), DataTable)
    '        If mdtEmp IsNot Nothing Then
    '            Dim dtmp() As DataRow = mdtEmp.Select("employeeunkid = '" & iTagUnkid & "'")
    '            If dtmp.Length > 0 Then
    '                If iFlag = False Then
    '                    dtmp(0).Item("AUD") = "D"
    '                End If
    '            Else
    '                If iFlag = True Then
    '                    Dim dRow As DataRow = mdtEmp.NewRow
    '                    dRow.Item("ownertranunkid") = -1
    '                    dRow.Item("empfieldunkid") = CInt(Me.ViewState("mintEmpField5Unkid"))
    '                    dRow.Item("employeeunkid") = iTagUnkid
    '                    dRow.Item("empfieldtypeid") = enWeight_Types.WEIGHT_FIELD5
    '                    dRow.Item("AUD") = "A"
    '                    dRow.Item("GUID") = Guid.NewGuid.ToString
    '                    mdtEmp.Rows.Add(dRow)
    '                End If
    '                mdtEmp.AcceptChanges()
    '                Me.ViewState("mdtEmpField5Owner") = mdtEmp
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub
#End Region

#Region " Button's Events "

    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
        Try
            If mvPerformancePlanningWz.ActiveViewIndex > 0 Then mvPerformancePlanningWz.ActiveViewIndex -= 1
            If mvPerformancePlanningWz.ActiveViewIndex <= 0 Then
                btnPrevious.Enabled = False
                Exit Sub
            Else
                btnPrevious.Enabled = True
                btnNext.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddGoals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddGoals.Click
        Try
            If mdtGoals_List1 IsNot Nothing Then
                If mdtGoals_List1.Rows.OfType(Of DataRow)().Where(Function(x) x.Field(Of Boolean)("assign") = True).Count() <= 0 Then
                    DisplayMessage.DisplayMessage("Sorry, Please select atleast one goal from the list inorder to add", Me)
                End If

                Dim dr() As DataRow = mdtGoals_List1.Select("assign = true")
                If dr.Length > 0 Then
                    For index As Integer = 0 To dr.Length - 1
                        mdtGoals_List2.ImportRow(dr(index))
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnDeleteGoals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteGoals.Click
        Try
            If mdtGoals_List2 IsNot Nothing Then
                If mdtGoals_List2.Rows.OfType(Of DataRow)().Where(Function(x) x.Field(Of Boolean)("assign") = True).Count() <= 0 Then
                    DisplayMessage.DisplayMessage("Sorry, Please select atleast one goal from the list inorder to remove", Me)
                End If
                Dim dr() As DataRow = mdtGoals_List2.Select("assign = true")
                If dr.Length > 0 Then
                    For index As Integer = 0 To dr.Length - 1
                        mdtGoals_List2.Rows.Remove(dr(index))
                    Next
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            If IsValidOperation() = False Then
                Exit Sub
            End If

            'If mblnIsValueSet = False Then
            Call SetDisplayValueData()
            'mblnIsValueSet = True
            'End If

            mvPerformancePlanningWz.ActiveViewIndex += 1
            If mvPerformancePlanningWz.ActiveViewIndex = mvPerformancePlanningWz.Views.Count - 1 Then
                btnNext.Enabled = False
                Exit Sub
            Else
                btnNext.Enabled = True
                btnPrevious.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath") & "UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEmpField1Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField1Save.Click
        Dim iblnFlag As Boolean = False
        Dim menAction As enAction = CType(Me.ViewState("menEmpField1Action"), enAction)
        'Dim mdtEmpField1Owner As DataTable = CType(Me.ViewState("mdtEmpField1Owner"), DataTable)
        'Dim mdicEmpField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField1FieldData"), Dictionary(Of Integer, String))
        Try
            If IsValidData_EmpField1() = False Then Exit Sub
            Dim dr As DataRow = mdtGoals_List2.NewRow



            mdtGoals_List2.Rows.Add(dr)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEmpField1Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField1Cancel.Click
        Try
            mblnpopupShow_EmpField1 = False
            pop_objfrmAddEditEmpField1.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEmpField2Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField2Save.Click
        Dim iblnFlag As Boolean = False
        Dim menAction As enAction = CType(Me.ViewState("menEmpField2Action"), enAction)
        'Dim mdtEmpField2Owner As DataTable = CType(Me.ViewState("mdtEmpField2Owner"), DataTable)
        'Dim mdicEmpField2FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField2FieldData"), Dictionary(Of Integer, String))
        Try
            If IsValidData_EmpField2() = False Then Exit Sub
            'Call SetEmpField2Value()
            'If menAction = enAction.EDIT_ONE Then
            '    iblnFlag = objEmpField2.Update(mdicEmpField2FieldData, mdtEmpField2Owner)
            'Else
            '    iblnFlag = objEmpField2.Insert(mdicEmpField2FieldData, mdtEmpField2Owner)
            'End If
            'If iblnFlag = False Then
            '    If objEmpField2._Message <> "" Then
            '        DisplayMessage.DisplayMessage(objEmpField2._Message, Me)
            '    Else
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp2, 9, "Sorry, problem in saving Employee Goals."), Me)
            '    End If
            'Else
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp2, 10, "Employee Goals are saved successfully."), Me)
            '    If menAction = enAction.ADD_CONTINUE Then
            '        objEmpField2 = New clsassess_empfield2_master
            '        Call GetEmpField2Value()
            '        txtEmpField2Remark1.Text = "" : txtEmpField2Remark2.Text = "" : txtEmpField2Remark3.Text = "" : txtEmpField2SearchEmp.Text = ""
            '        If CInt(Me.ViewState("mintEmpField2ParentId")) > 0 Then
            '            cboEmpField2EmpFieldValue1.SelectedValue = CInt(Me.ViewState("mintEmpField2ParentId"))
            '        End If
            '        Call Fill_Grid()
            '    Else
            '        Call Fill_Grid()
            '        Call btnEmpField2Cancel_Click(sender, e)
            '    End If
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEmpField2Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField2Cancel.Click
        Try
            mblnpopupShow_EmpField2 = False
            pop_objfrmAddEditEmpField2.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEmpField3Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField3Save.Click
        Dim iblnFlag As Boolean = False
        Dim menAction As enAction = CType(Me.ViewState("menEmpField3Action"), enAction)
        'Dim mdtEmpField3Owner As DataTable = CType(Me.ViewState("mdtEmpField3Owner"), DataTable)
        'Dim mdicEmpField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField3FieldData"), Dictionary(Of Integer, String))
        Try
            If IsValidData_EmpField3() = False Then Exit Sub
            'Call SetEmpField3Value()
            'If menAction = enAction.EDIT_ONE Then
            '    iblnFlag = objEmpField3.Update(mdicEmpField3FieldData, mdtEmpField3Owner)
            'Else
            '    iblnFlag = objEmpField3.Insert(mdicEmpField3FieldData, mdtEmpField3Owner)
            'End If
            'If iblnFlag = False Then
            '    If objEmpField3._Message <> "" Then
            '        DisplayMessage.DisplayMessage(objEmpField3._Message, Me)
            '    Else
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp3, 9, "Sorry, problem in saving Employee Goals."), Me)
            '    End If
            'Else
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp3, 10, "Employee Goals are saved successfully."), Me)
            '    If menAction = enAction.ADD_CONTINUE Then
            '        objEmpField3 = New clsassess_empfield3_master
            '        Call GetEmpField3Value()
            '        txtEmpField3Remark1.Text = "" : txtEmpField3Remark2.Text = "" : txtEmpField3Remark3.Text = ""
            '        If CInt(Me.ViewState("mintEmpField3ParentId")) > 0 Then
            '            cboEmpField3EmpFieldValue2.SelectedValue = CInt(Me.ViewState("mintEmpField3ParentId"))
            '        End If
            '        Call Fill_Grid()
            '    Else
            '        Call Fill_Grid()
            '        Call btnEmpField3Cancel_Click(sender, e)
            '    End If
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEmpField3Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField3Cancel.Click
        Try
            mblnpopupShow_EmpField3 = False
            pop_objfrmAddEditEmpField3.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEmpField4Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField4Save.Click
        Dim iblnFlag As Boolean = False
        Dim menAction As enAction = CType(Me.ViewState("menEmpField4Action"), enAction)
        'Dim mdtEmpField4Owner As DataTable = CType(Me.ViewState("mdtEmpField4Owner"), DataTable)
        'Dim mdicEmpField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField4FieldData"), Dictionary(Of Integer, String))
        Try
            If IsValidData_EmpField4() = False Then Exit Sub
            'Call SetEmpField4Value()
            'If menAction = enAction.EDIT_ONE Then
            '    iblnFlag = objEmpField4.Update(mdicEmpField4FieldData, mdtEmpField4Owner)
            'Else
            '    iblnFlag = objEmpField4.Insert(mdicEmpField4FieldData, mdtEmpField4Owner)
            'End If

            'If iblnFlag = False Then
            '    If objEmpField4._Message <> "" Then
            '        DisplayMessage.DisplayMessage(objEmpField4._Message, Me)
            '    Else
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp4, 9, "Sorry, problem in saving Employee Goals."), Me)
            '    End If
            'Else
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp4, 10, "Employee Goals are saved successfully."), Me)
            '    If menAction = enAction.ADD_CONTINUE Then
            '        objEmpField4 = New clsassess_empfield4_master
            '        Call GetEmpField4Value()
            '        txtEmpField4Remark1.Text = "" : txtEmpField4Remark2.Text = "" : txtEmpField4Remark3.Text = ""
            '        If Me.ViewState("mintEmpField4ParentId") > 0 Then
            '            cboEmpField4EmpFieldValue3.SelectedValue = Me.ViewState("mintEmpField4ParentId")
            '        End If
            '    Else
            '        Fill_Grid()
            '        Call btnEmpField4Cancel_Click(sender, e)
            '    End If
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEmpField4Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField4Cancel.Click
        Try
            mblnpopupShow_EmpField4 = False
            pop_objfrmAddEditEmpField4.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEmpField5Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField5Save.Click
        Dim iblnFlag As Boolean = False
        Dim menAction As enAction = CType(Me.ViewState("menEmpField5Action"), enAction)
        'Dim mdtEmpField5Owner As DataTable = CType(Me.ViewState("mdtEmpField5Owner"), DataTable)
        'Dim mdicEmpField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicEmpField5FieldData"), Dictionary(Of Integer, String))
        Try
            If IsValidData_EmpField5() = False Then Exit Sub
            'Call SetEmpField5Value()
            'If menAction = enAction.EDIT_ONE Then
            '    iblnFlag = objEmpField5.Update(mdicEmpField5FieldData, mdtEmpField5Owner)
            'Else
            '    iblnFlag = objEmpField5.Insert(mdicEmpField5FieldData, mdtEmpField5Owner)
            'End If
            'If iblnFlag = False Then
            '    If objEmpField5._Message <> "" Then
            '        DisplayMessage.DisplayMessage(objEmpField5._Message, Me)
            '    Else
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp5, 9, "Sorry, problem in saving Employee Goals."), Me)
            '    End If
            'Else
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_Emp5, 10, "Employee Goals are saved successfully."), Me)
            '    If menAction = enAction.ADD_CONTINUE Then
            '        objEmpField5 = New clsassess_empfield5_master
            '        Call GetEmpField5Value()
            '        txtEmpField5Remark1.Text = "" : txtEmpField5Remark2.Text = "" : txtEmpField5Remark3.Text = ""
            '        If Me.ViewState("mintEmpField5ParentId") > 0 Then
            '            cboEmpField5EmpFieldValue4.SelectedValue = Me.ViewState("mintEmpField5ParentId")
            '        End If
            '        Fill_Grid()
            '    Else
            '        Fill_Grid()
            '        Call btnEmpField5Cancel_Click(sender, e)
            '    End If
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEmpField5Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmpField5Cancel.Click
        Try
            mblnpopupShow_EmpField5 = False
            pop_objfrmAddEditEmpField5.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Dim mdtFinal As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
        Dim mstrScreenId As String = Me.ViewState("DeleteScreenId")
        Dim mstrScreeName As String = Me.ViewState("DeleteColumnName")
        Try
            If txtMessage.Text.Trim.Length > 0 Then

                'Select Case mstrScreenId.Split("|")(1)
                '    Case 1
                '        If CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield1unkid")) > 0 Then
                '            objEmpField1._Isvoid = True
                '            objEmpField1._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                '            objEmpField1._Voidreason = txtMessage.Text
                '            objEmpField1._Voiduserunkid = Session("UserId")
                '            If objEmpField1.Delete(CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield1unkid"))) = True Then
                '                Call Fill_Grid()
                '                popup_YesNo.Hide()
                '            Else
                '                If objEmpField1._Message <> "" Then
                '                    DisplayMessage.DisplayMessage(objEmpField1._Message, Me)
                '                    Exit Sub
                '                End If
                '            End If
                '        Else
                '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                '                             " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                '                             " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "in order to perform edit operation."), Me)
                '            Exit Sub
                '        End If
                '    Case 2
                '        If CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield2unkid")) > 0 Then
                '            objEmpField2._Isvoid = True
                '            objEmpField2._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                '            objEmpField2._Voidreason = txtMessage.Text
                '            objEmpField2._Voiduserunkid = Session("UserId")
                '            If objEmpField2.Delete(CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield2unkid"))) = True Then
                '                Call Fill_Grid()
                '                popup_YesNo.Hide()
                '            Else
                '                If objEmpField2._Message <> "" Then
                '                    DisplayMessage.DisplayMessage(objEmpField2._Message, Me)
                '                    Exit Sub
                '                End If
                '            End If
                '        Else
                '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                '                            " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                '                            " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "in order to perform edit operation."), Me)
                '            Exit Sub
                '        End If
                '    Case 3
                '        If CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield3unkid")) > 0 Then
                '            objEmpField3._Isvoid = True
                '            objEmpField3._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                '            objEmpField3._Voidreason = txtMessage.Text
                '            objEmpField3._Voiduserunkid = Session("UserId")
                '            If objEmpField3.Delete(CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield3unkid"))) = True Then
                '                Call Fill_Grid()
                '                popup_YesNo.Hide()
                '            Else
                '                If objEmpField3._Message <> "" Then
                '                    DisplayMessage.DisplayMessage(objEmpField3._Message, Me)
                '                    Exit Sub
                '                End If
                '            End If
                '        Else
                '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                '                           " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                '                           " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "in order to perform edit operation."), Me)
                '            Exit Sub
                '        End If
                '    Case 4
                '        If CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield4unkid")) > 0 Then
                '            objEmpField4._Isvoid = True
                '            objEmpField4._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                '            objEmpField4._Voidreason = txtMessage.Text
                '            objEmpField4._Voiduserunkid = Session("UserId")
                '            If objEmpField4.Delete(CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield4unkid"))) = True Then
                '                Call Fill_Grid()
                '                popup_YesNo.Hide()
                '            Else
                '                If objEmpField4._Message <> "" Then
                '                    DisplayMessage.DisplayMessage(objEmpField4._Message, Me)
                '                    Exit Sub
                '                End If
                '            End If
                '        Else
                '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                '                             " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                '                             " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "in order to perform edit operation."), Me)
                '            Exit Sub
                '        End If
                '    Case 5
                '        If CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield5unkid")) > 0 Then
                '            objEmpField5._Isvoid = True
                '            objEmpField5._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                '            objEmpField5._Voidreason = txtMessage.Text
                '            objEmpField5._Voiduserunkid = Session("UserId")
                '            If objEmpField5.Delete(CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("empfield5unkid"))) = True Then
                '                Call Fill_Grid()
                '                popup_YesNo.Hide()
                '            Else
                '                If objEmpField5._Message <> "" Then
                '                    DisplayMessage.DisplayMessage(objEmpField5._Message, Me)
                '                    Exit Sub
                '                End If
                '            End If
                '        Else
                '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                '                           " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                '                           " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "in order to perform edit operation."), Me)
                '            Exit Sub
                '        End If
                'End Select
            Else
                popup_YesNo.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region " ComboBox Events "

    Protected Sub cboPlanningPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPlanningPeriod.SelectedIndexChanged
        Try
            If CInt(IIf(cboPlanningPeriod.SelectedValue = "", 0, cboPlanningPeriod.SelectedValue)) > 0 Then
                Call FillAssignedEmployee()
                Dim objPeriod As New clscommom_period_Tran
                Dim dsList As New DataSet
                dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", False, 1)
                If dsList.Tables("List").Rows.Count > 0 Then
                    mintPastPeriodUnkid = dsList.Tables("List").Rows.OfType(Of DataRow)().Where(Function(x) x.Field(Of Integer)("periodunkid") <> CInt(IIf(cboPlanningPeriod.SelectedValue = "", 0, cboPlanningPeriod.SelectedValue))).OrderByDescending(Function(y) y.Field(Of String)("end_date")).[Select](Function(r) r.Field(Of Integer)("periodunkid")).ToArray().GetValue(0)
                End If
                objPeriod = Nothing
                dsList.Dispose()

                Dim objMap As New clsAssess_Field_Mapping
                Dim intMappingUnkid As Integer = 0
                intMappingUnkid = objMap.Get_MappingUnkId(CInt(IIf(cboPlanningPeriod.SelectedValue = "", 0, cboPlanningPeriod.SelectedValue)))
                If intMappingUnkid > 0 Then
                    objMap._Mappingunkid = intMappingUnkid
                    mDecTotalGoalsWeight = objMap._Weight
                End If
                mintMappedFieldId = objMap.Get_Map_FieldId(CInt(IIf(cboPlanningPeriod.SelectedValue = "", 0, cboPlanningPeriod.SelectedValue)))
                objMap = Nothing

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboGoalSource_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGoalSource.SelectedIndexChanged
        Try
            Select Case cboGoalSource.SelectedIndex
                Case 0  'NEW GOALS TO ADD
                    Call Fill_Goals(0)
                Case 1  'SELECTION FROM SUPERVISOR
                    Call Fill_Goals(mintAssessorEmpId)
                Case 2  'SELECTION FROM PAST PERIOD (ESS OR 1 EMPLOYEE SELECTED)
                    Call Fill_Goals(mintEmployeeId)
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboCompetencySource_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCompetencySource.SelectedIndexChanged
        Try
            Select Case cboCompetencySource.SelectedIndex
                Case 0  'COMPANY DEFINED - IN SYSTEM
                Case 1  'ARUTI LIBRARY [PREDEFINED BY ARUTI]
                Case 2  'MAPPED WITH JOB COMPETENCIES
                Case 3  'PAST PERIOD [ESS OR 1 EMPLOYEE SELECTED]
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboEmpField2EmpFieldValue1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpField2EmpFieldValue1.SelectedIndexChanged
        Try
            Dim mintParentId As Integer
            mintParentId = CInt(cboEmpField2EmpFieldValue1.SelectedValue)
            If CInt(cboEmpField2EmpFieldValue1.SelectedValue) > 0 Then
                Dim objEmpField1 As New clsassess_empfield1_master
                objEmpField1._Empfield1unkid = CInt(cboEmpField2EmpFieldValue1.SelectedValue)
                objEmpField1 = Nothing
            End If
            Me.ViewState("mintEmpField2ParentId") = mintParentId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboEmpField3EmpFieldValue2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpField3EmpFieldValue2.SelectedIndexChanged
        Try
            Dim mintParentId As Integer
            mintParentId = CInt(cboEmpField3EmpFieldValue2.SelectedValue)
            If CInt(cboEmpField3EmpFieldValue2.SelectedValue) > 0 Then
                Dim objEmpField2 As New clsassess_empfield2_master
                Dim objEmpField1 As New clsassess_empfield1_master
                objEmpField2._Empfield2unkid = CInt(cboEmpField3EmpFieldValue2.SelectedValue)
                objEmpField1._Empfield1unkid = objEmpField2._Empfield1unkid
                objEmpField3txtEmpField1.Text = objEmpField1._Field_Data
                objEmpField2 = Nothing : objEmpField2 = Nothing
            Else
                objEmpField3txtEmpField1.Text = ""
            End If
            Me.ViewState("mintEmpField3ParentId") = mintParentId
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "cboEmpFieldValue2_SelectedIndexChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub cboEmpField4EmpFieldValue3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpField4EmpFieldValue3.SelectedIndexChanged
        Try
            Dim mintParentId As Integer
            mintParentId = CInt(cboEmpField4EmpFieldValue3.SelectedValue)
            If CInt(cboEmpField4EmpFieldValue3.SelectedValue) > 0 Then
                Dim objField3 As New clsassess_empfield3_master
                Dim objField2 As New clsassess_empfield2_master
                Dim objField1 As New clsassess_empfield1_master
                objField3._Empfield3unkid = CInt(cboEmpField4EmpFieldValue3.SelectedValue)
                objField2._Empfield2unkid = objField3._Empfield2unkid
                objField1._Empfield1unkid = objField2._Empfield1unkid
                objEmpField4txtEmpField2.Text = objField2._Field_Data
                objEmpField4txtEmpField1.Text = objField1._Field_Data
                objField3 = Nothing : objField2 = Nothing : objField1 = Nothing
            Else
                objEmpField4txtEmpField1.Text = ""
                objEmpField4txtEmpField2.Text = ""
            End If
            Me.ViewState("mintEmpField4ParentId") = mintParentId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboEmpField5EmpFieldValue4_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmpField5EmpFieldValue4.SelectedIndexChanged
        Try
            Dim mintParentId As Integer
            mintParentId = CInt(cboEmpField5EmpFieldValue4.SelectedValue)
            If CInt(cboEmpField5EmpFieldValue4.SelectedValue) > 0 Then
                Dim objField4 As New clsassess_empfield4_master
                Dim objField3 As New clsassess_empfield3_master
                Dim objField2 As New clsassess_empfield2_master
                Dim objField1 As New clsassess_empfield1_master
                objField4._Empfield4unkid = CInt(cboEmpField5EmpFieldValue4.SelectedValue)
                objField3._Empfield3unkid = objField4._Empfield3unkid
                objField2._Empfield2unkid = objField3._Empfield2unkid
                objField1._Empfield1unkid = objField2._Empfield1unkid

                objEmpField5txtEmpField1.Text = objField1._Field_Data
                objEmpField5txtEmpField2.Text = objField2._Field_Data
                objEmpField5txtEmpField3.Text = objField3._Field_Data
                objField3 = Nothing : objField2 = Nothing : objField1 = Nothing
            Else
                objEmpField5txtEmpField1.Text = "" : objEmpField5txtEmpField2.Text = "" : objEmpField5txtEmpField3.Text = ""
            End If
            Me.ViewState("mintEmpField5ParentId") = mintParentId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
#End Region

#Region " Checkbox Event's "

    Protected Sub chkPg1Select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkBox.NamingContainer, DataGridItem)

            Dim xrow() As DataRow = mdtSelectedEmployee.Select("Id=" & item.Cells(5).Text)
            If xrow.Length > 0 Then
                xrow(0).Item("ischeck") = chkBox.Checked
            End If
            mdtSelectedEmployee.AcceptChanges()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkPg1Select_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub chk1Pg2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkBox.NamingContainer, DataGridItem)
            mdtGoals_List1.Rows(item.DataSetIndex).Item("assign") = chkBox.Checked
            mdtGoals_List1.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chk2Pg2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            Dim item As DataGridItem = CType(chkBox.NamingContainer, DataGridItem)
            mdtGoals_List2.Rows(item.DataSetIndex).Item("assign") = chkBox.Checked
            mdtGoals_List2.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkPg1SelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkBox As CheckBox = CType(sender, CheckBox)
            If dgvEmployee.Items.Count <= 0 Then Exit Sub

            For Each item As DataGridItem In dgvEmployee.Items
                CType(item.Cells(0).FindControl("chkPg1Select"), CheckBox).Checked = chkBox.Checked

                Dim xrow() As DataRow = mdtSelectedEmployee.Select("Id=" & item.Cells(5).Text)
                If xrow.Length > 0 Then
                    xrow(0).Item("ischeck") = chkBox.Checked
                    mdtSelectedEmployee.AcceptChanges()
                End If
            Next
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkPg1SelectAll_CheckedChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

    Protected Sub chkEmpField1AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField1All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvEmpField1Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkEmpField1select"), CheckBox)
                chk.Checked = chkOwrField1All.Checked
                'Call GoalEmpOperation_Emp1(iRow.Cells(3).Text, CBool(chkOwrField1All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkEmpField1select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField1 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField1.NamingContainer, DataGridItem)
            If chkOwrField1 IsNot Nothing Then
                'Call GoalEmpOperation_Emp1(dgvEmpField1Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField1.Checked))
            End If
            'Call Fill_Data_EmpField1()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkEmpField2AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField2All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvEmpField2Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkEmpField2select"), CheckBox)
                chk.Checked = chkOwrField2All.Checked
                'Call GoalEmpOperation_Emp2(iRow.Cells(3).Text, CBool(chkOwrField2All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkEmpField2select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField2 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField2.NamingContainer, DataGridItem)
            If chkOwrField2 IsNot Nothing Then
                'Call GoalEmpOperation_Emp2(dgvEmpField2Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField2.Checked))
            End If
            'Call Fill_Data_EmpField2()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkEmpField3AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField3All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvEmpField3Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkEmpField3select"), CheckBox)
                chk.Checked = chkOwrField3All.Checked
                'Call GoalEmpOperation_Emp3(iRow.Cells(3).Text, CBool(chkOwrField3All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkEmpField3select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField3 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField3.NamingContainer, DataGridItem)
            If chkOwrField3 IsNot Nothing Then
                'Call GoalEmpOperation_Emp3(dgvEmpField3Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField3.Checked))
            End If
            'Call Fill_Data_EmpField3()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkEmpField4AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField4All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvEmpField4Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkEmpField4select"), CheckBox)
                chk.Checked = chkOwrField4All.Checked
                'Call GoalEmpOperation_Emp4(iRow.Cells(3).Text, CBool(chkOwrField4All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkEmpField4select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField4 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField4.NamingContainer, DataGridItem)
            If chkOwrField4 IsNot Nothing Then
                'Call GoalEmpOperation_Emp4(dgvEmpField4Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField4.Checked))
            End If
            'Call Fill_Data_EmpField4()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkEmpField5AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField5All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvEmpField5Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkEmpField5select"), CheckBox)
                chk.Checked = chkOwrField5All.Checked
                'Call GoalEmpOperation_Emp5(iRow.Cells(3).Text, CBool(chkOwrField5All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkEmpField5select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField5 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField5.NamingContainer, DataGridItem)
            'If chkOwrField5 IsNot Nothing Then
            '    Call GoalEmpOperation_Emp5(dgvEmpField5Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField5.Checked))
            'End If
            'Call Fill_Data_EmpField5()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkGoalEqweight_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkGoalEqweight.CheckedChanged
        Try
            If mdtGoals_List2 IsNot Nothing Then
                Dim mdblAvgWgt As Double = 0
                mdblAvgWgt = mDecTotalGoalsWeight / mdtGoals_List2.Rows.Count
                For Each dr As DataRow In mdtGoals_List2.Rows
                    dr("Weight") = mdblAvgWgt
                Next
                txtGoalTotal.Text = mdtGoals_List2.Compute("SUM(Weight)", "")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " TextChange Event's "

    Protected Sub txtGoalWeight_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim xRow As DataGridItem = CType(CType(sender, TextBox).NamingContainer, DataGridItem)
            Dim iDecWgt As Decimal = 0
            Dim txt As TextBox = CType(sender, TextBox)
            Decimal.TryParse(txt.Text, iDecWgt)
            If IsNumeric(iDecWgt) Then
                If iDecWgt < 0 Then
                    DisplayMessage.DisplayMessage("Sorry, you cannot set negetive weight.", Me)
                    txt.Text = 0
                    Exit Sub
                End If

                If mdtGoals_List2.Compute("SUM(Weight)", "") > mDecTotalGoalsWeight Then
                    DisplayMessage.DisplayMessage("Sorry, you cannot assign the weight more than" & " " & mDecTotalGoalsWeight, Me)
                    Exit Sub
                End If

                txtGoalTotal.Text = mdtGoals_List2.Compute("SUM(Weight)", "")

            Else
                txt.Text = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Control Event "

    Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As DataGridItem = TryCast(imgAdd.NamingContainer, DataGridItem)
            Me.ViewState("AddRowIndex") = row.ItemIndex
            Genrate_AddmanuItem()
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition('scrollable-container2');", True)
            popAddButton.X = CInt(hdf_locationx.Value)
            popAddButton.Y = CInt(hdf_locationy.Value)
            popAddButton.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub imgEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As DataGridItem = TryCast(imgAdd.NamingContainer, DataGridItem)
            Me.ViewState("EditRowIndex") = row.ItemIndex
            Genrate_EditmanuItem()
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition('scrollable-container2');", True)
            popEditButton.X = CInt(hdf_locationx.Value)
            popEditButton.Y = CInt(hdf_locationy.Value)
            popEditButton.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As DataGridItem = TryCast(imgAdd.NamingContainer, DataGridItem)
            Me.ViewState("DeleteRowIndex") = row.ItemIndex
            Genrate_DeletemanuItem()
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition('scrollable-container2');", True)
            popDeleteButton.X = CInt(hdf_locationx.Value)
            popDeleteButton.Y = CInt(hdf_locationy.Value)
            popDeleteButton.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ilnk As LinkButton = CType(sender, LinkButton)
            'Dim mdtFinal As DataTable = CType(Me.ViewState("mdtGoals_List1"), DataTable)
            'If CInt(cboEmployee.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 15, "Sorry, Employee and Period are mandatory information to performe Add Operation."), Me)
            '    Exit Sub
            'End If

            Select Case CInt(ilnk.CommandArgument.ToString.Split("|")(1))
                Case 1
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call EmpField1_load(-1, enAction.ADD_CONTINUE)
                    pop_objfrmAddEditEmpField1.Show()
                Case 2
                    Dim mintOwrField2ParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield1unkid"))
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call EmpField2_load(-1, enAction.ADD_CONTINUE, mintOwrField2ParentId)
                    pop_objfrmAddEditEmpField2.Show()
                Case 3
                    Dim mintOwrField3ParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield2unkid"))
                    Dim mintOwrField3MainParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield1unkid"))
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call EmpField3_load(-1, enAction.ADD_CONTINUE, mintOwrField3ParentId, mintOwrField3MainParentId)
                    pop_objfrmAddEditEmpField3.Show()
                Case 4
                    Dim mintOwrField4ParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield3unkid"))
                    Dim mintOwrField4MainParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield2unkid"))
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call EmpField4_load(-1, enAction.ADD_CONTINUE, mintOwrField4ParentId, mintOwrField4MainParentId)
                    pop_objfrmAddEditEmpField4.Show()
                Case 5
                    Dim mintOwrField5ParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield4unkid"))
                    Dim mintOwrField5MainParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("AddRowIndex")))("empfield3unkid"))
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call EmpField5_load(-1, enAction.ADD_CONTINUE, mintOwrField5ParentId, mintOwrField5MainParentId)
                    pop_objfrmAddEditEmpField5.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ilnk As LinkButton = CType(sender, LinkButton)
            Dim irow As DataListItem = CType(ilnk.NamingContainer, DataListItem)
            Dim hdf As HiddenField = CType(dlmnuEdit.Items(irow.ItemIndex).FindControl("objEditOrignalName"), HiddenField)
            'Dim mdtFinal As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)

            Select Case CInt(ilnk.CommandArgument.ToString.Split("|")(1))
                Case 1
                    If CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield1unkid")) > 0 Then
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call EmpField1_load(CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield1unkid")), enAction.EDIT_ONE)
                        pop_objfrmAddEditEmpField1.Show()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                         " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
                Case 2
                    If CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield2unkid")) > 0 Then
                        Dim mintOwrField2ParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield1unkid"))
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call EmpField2_load(CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield2unkid")), enAction.EDIT_ONE, mintOwrField2ParentId)
                        pop_objfrmAddEditEmpField2.Show()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                         " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
                Case 3
                    If CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield3unkid")) > 0 Then
                        Dim mintOwrField3ParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield2unkid"))
                        Dim mintOwrField3MainParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield1unkid"))
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call EmpField3_load(CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield3unkid")), enAction.EDIT_ONE, mintOwrField3ParentId, mintOwrField3MainParentId)
                        pop_objfrmAddEditEmpField3.Show()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                        " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                        " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
                Case 4
                    If CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield4unkid")) > 0 Then
                        Dim mintOwrField4ParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield3unkid"))
                        Dim mintOwrField4MainParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield2unkid"))
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call EmpField4_load(CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield4unkid")), enAction.EDIT_ONE, mintOwrField4ParentId, mintOwrField4MainParentId)
                        pop_objfrmAddEditEmpField4.Show()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                        " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                        " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
                Case 5
                    If CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield5unkid")) > 0 Then
                        Dim mintOwrField5ParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield4unkid"))
                        Dim mintOwrField5MainParentId As Integer = CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield3unkid"))
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call EmpField5_load(CInt(mdtGoals_List2.Rows(CInt(Me.ViewState("EditRowIndex")))("empfield5unkid")), enAction.EDIT_ONE, mintOwrField5ParentId, mintOwrField5MainParentId)
                        pop_objfrmAddEditEmpField5.Show()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                        " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                        " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ilnk As LinkButton = CType(sender, LinkButton)
            Dim irow As DataListItem = CType(ilnk.NamingContainer, DataListItem)
            Dim hdf As HiddenField = CType(dlmnuDelete.Items(irow.ItemIndex).FindControl("objDeleteOrignalName"), HiddenField)
            Me.ViewState("DeleteScreenId") = ilnk.CommandArgument.ToString().Trim
            Me.ViewState("DeleteColumnName") = hdf.Value
            lblTitle.Text = "Aruti"
            Dim iMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 19, "You are about to remove the information from employee level, this will delete all child linked to this parent") & vbCrLf & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 20, "Are you sure you want to delete?")
            lblMessage.Text = iMsg
            txtMessage.Text = ""
            popup_YesNo.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class
