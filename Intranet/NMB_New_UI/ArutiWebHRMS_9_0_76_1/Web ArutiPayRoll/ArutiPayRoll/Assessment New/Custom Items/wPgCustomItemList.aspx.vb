﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
Imports System.Drawing

#End Region
Partial Class wPgCustomItemList
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmCustomItemsList"
    Private ReadOnly mstrModuleName1 As String = "frmAddEditCustomItems"
    Private DisplayMessage As New CommonCodes
    Private mblnpopupShow_AddEditCItem As Boolean = False

#End Region

#Region " Page Event(s) "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Call FillCombo()
                Me.ViewState.Add("EditRowIndex", 0)
                Me.ViewState.Add("DeleteRowIndex", 0)
                Me.ViewState.Add("CustomItemUnkid", 0)
                Me.ViewState.Add("customheaderunkid", Session("customheaderunkid"))

                Dim objCHeader As New clsassess_custom_header
                objCHeader._Customheaderunkid = CInt(Me.ViewState("customheaderunkid"))
                'Nilay (02-Mar-2015) -- Start
                'Enhancement - REDESIGN SELF SERVICE.
                'Closebotton1.PageHeading = objCHeader._Name & " Custom Items List "
                lblPageHeader.Text = objCHeader._Name & " Custom Items List "
                'Nilay (02-Mar-2015) -- End
                objCHeader = Nothing

                objlblNotes.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Note: You are allowed to define only 10 Custom Items per Custom Header.")
                objlblpopupNotes.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Note: You are allowed to define only 10 Custom Items per Custom Header.")

                'Shani(06-Feb-2016) -- Start
                'PA Changes Given By CCBRT
                btnNew.Visible = CBool(Session("AllowtoAddCustomItems"))
                'Shani(06-Feb-2016) -- End

            Else
                mblnpopupShow_AddEditCItem = Me.ViewState("popupShow_AddEditCItem")
                If mblnpopupShow_AddEditCItem Then
                    popup_AddCustomItems.Show()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Me.ViewState("popupShow_AddEditCItem") Is Nothing Then
                Me.ViewState.Add("popupShow_AddEditCItem", mblnpopupShow_AddEditCItem)
            Else
                Me.ViewState("popupShow_AddEditCItem") = mblnpopupShow_AddEditCItem
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Session("customheaderunkid") IsNot Nothing Then
                Session("customheaderunkid") = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objCItems As New clsassess_custom_items
        Dim dsCombo As New DataSet
        Try

            'S.SANDEEP [17 NOV 2015] -- START
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            'S.SANDEEP [10 DEC 2015] -- END
            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [17 NOV 2015] -- END

            'Shani (09-May-2016) -- Start
            Dim intCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End


            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = intCurrentPeriodId
                .DataBind()
            End With

            'Shani (09-May-2016) -- Change[0-->intCurrentPeriodId]


            With cbopopPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0).Copy
                .SelectedValue = 0
                .DataBind()
            End With

            dsCombo = objCItems.GetList_CustomTypes("List", True)
            With cboItemType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedIndex = 0
                .DataBind()
            End With
            dsCombo = objCItems.GetList_CustomTypes("List", False)
            With cbopopItemType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedIndex = 0
                .DataBind()
            End With

            dsCombo = objCItems.GetList_SelectionMode("List", True)
            With cboMode
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .SelectedIndex = 0
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillGrid()
        Dim objCFields As New clsassess_custom_items
        Dim iSearch As String = String.Empty
        Dim mdtTable As DataTable
        Dim dsList As New DataSet
        Try
            dsList = objCFields.GetList("List")
            iSearch &= "AND customheaderunkid = '" & CInt(Me.ViewState("customheaderunkid")) & "' "
            If CInt(cboPeriod.SelectedValue) > 0 Then
                iSearch &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
            End If
            If CInt(cboItemType.SelectedValue) > 0 Then
                iSearch &= "AND itemtypeid = '" & CInt(cboItemType.SelectedValue) & "' "
            End If
            Dim strViewFltr As String = String.Empty
            If chkSelfAssessment.Checked = True Then
                strViewFltr &= "," & 0 'INTENSIONALLY PASSED - TO BE ALLOWED FOR ALL
            End If
            If chkAssessorAssessment.Checked = True Then
                strViewFltr &= "," & enAssessmentMode.APPRAISER_ASSESSMENT
            End If
            If chkReviewerAssessment.Checked = True Then
                strViewFltr &= "," & enAssessmentMode.REVIEWER_ASSESSMENT
            End If
            If strViewFltr.Trim.Length > 0 Then
                strViewFltr = Mid(strViewFltr, 2)
                iSearch &= "AND viewmodeid IN(" & strViewFltr & ") "
            End If
            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)
                mdtTable = New DataView(dsList.Tables(0), iSearch, "", DataViewRowState.CurrentRows).ToTable
            Else
                mdtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If
            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'If mdtTable.Rows.Count > 24 Then
            '    pnl_dgvData.Height = Unit.Pixel(490)
            'Else
            '    pnl_dgvData.Height = Unit.Pixel(30 + (20 * mdtTable.Rows.Count))
            'End If
            'Nilay (02-Mar-2015) -- End

            'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            dgvData.AutoGenerateColumns = False
            'Pinkal (16-Apr-2016) -- End


            dgvData.DataSource = mdtTable
            dgvData.DataKeyField = "customitemunkid"
            dgvData.DataBind()

            'Shani(06-Feb-2016) -- Start
            'PA Changes Given By CCBRT
            dgvData.Columns(0).Visible = CBool(Session("AllowtoEditCustomItems"))
            dgvData.Columns(1).Visible = CBool(Session("AllowtoDeleteCustomItems"))
            'Shani(06-Feb-2016) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Display_Popup()
        Try
            Dim objCHeader As New clsassess_custom_header
            objCHeader._Customheaderunkid = CInt(Me.ViewState("customheaderunkid"))
            lblHeadeText.Text = "Add/Edit " & objCHeader._Name & " Custom Items"
            txtCustomHeader.Text = objCHeader._Name
            objCHeader = Nothing
            If Me.ViewState("CustomItemUnkid") IsNot Nothing Then
                Dim objCItems As New clsassess_custom_items
                objCItems._Customitemunkid = CInt(Me.ViewState("CustomItemUnkid"))

                txtCustomItem.Text = objCItems._Custom_Field
                cbopopPeriod.SelectedValue = objCItems._Periodunkid
                cbopopItemType.SelectedValue = objCItems._ItemTypeId
                cboMode.SelectedValue = objCItems._SelectionModeid
                Select Case objCItems._ViewModeId
                    Case 0 'INTENSIONALLY PASSED - TO BE ALLOWED FOR ALL
                        radItemSelection.SelectedValue = 1
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        radItemSelection.SelectedValue = 2
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        radItemSelection.SelectedValue = 3
                End Select
            End If
            If CInt(IIf(cbopopItemType.SelectedValue <= "0", clsassess_custom_items.enCustomType.FREE_TEXT, cbopopItemType.SelectedValue)) = clsassess_custom_items.enCustomType.SELECTION Then
                cboMode.Enabled = True
            Else
                cboMode.Enabled = False
            End If
            popup_AddCustomItems.Show()

            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            hdf_btnCItemFld.Value = 1
            'Nilay (02-Mar-2015) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If CInt(cbopopPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 1, "Period is mandatory information. Please select Period to continue."), Me)
                cbopopPeriod.Focus()
                Return False
            End If

            If txtCustomItem.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 3, "Custom Item is mandatory information. Please provide Custom Item to continue."), Me)
                txtCustomItem.Focus()
                Return False
            End If

            If radItemSelection.Items(0).Selected = False AndAlso _
               radItemSelection.Items(1).Selected = False AndAlso _
               radItemSelection.Items(2).Selected = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 4, "Sorry, Please select atleast one of the visible type in order to view item in performance assessment."), Me)
                Return False
            End If

            Dim xMessage As String = String.Empty
            Dim objCItems As New clsassess_custom_items

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'xMessage = objCItems.IsValidItemCount(CInt(Me.ViewState("customheaderunkid")))
            xMessage = objCItems.IsValidItemCount(CInt(Me.ViewState("customheaderunkid")), CInt(cbopopPeriod.SelectedValue))
            'Shani (26-Sep-2016) -- End


            If xMessage <> "" Then
                DisplayMessage.DisplayMessage(xMessage, Me)
                Return False
            End If
            objCItems = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
        Return True
    End Function

#End Region

#Region " Button's Events "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Period is mandatory information. Please select Period to continue."), Me)
                cboPeriod.Focus()
                Exit Sub
            End If

            Call FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            cboItemType.SelectedValue = 0
            chkAssessorAssessment.Checked = False
            chkReviewerAssessment.Checked = False
            chkSelfAssessment.Checked = False
            dgvData.DataSource = Nothing
            dgvData.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Call Display_Popup()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnCItemSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCItemSave.Click
        Try
            Dim blnFlag As Boolean = False
            If Is_Valid() = False Then
                popup_AddCustomItems.Show()
            End If

            Dim objCItems As New clsassess_custom_items
            If Me.ViewState("CustomItemUnkid") > 0 Then
                objCItems._Customitemunkid = CInt(Me.ViewState("CustomItemUnkid"))
            End If

            objCItems._Custom_Field = txtCustomItem.Text
            objCItems._Customheaderunkid = CInt(Me.ViewState("customheaderunkid"))
            objCItems._Isactive = True
            objCItems._Periodunkid = CInt(cbopopPeriod.SelectedValue)
            objCItems._ItemTypeId = CInt(cbopopItemType.SelectedValue)
            objCItems._SelectionModeid = CInt(cboMode.SelectedValue)
            If radItemSelection.SelectedValue = 1 Then
                objCItems._ViewModeId = 0   'INTENSIONALLY PASSED - TO BE ALLOWED FOR ALL
            ElseIf radItemSelection.SelectedValue = 2 Then
                objCItems._ViewModeId = enAssessmentMode.APPRAISER_ASSESSMENT
            ElseIf radItemSelection.SelectedValue = 3 Then
                objCItems._ViewModeId = enAssessmentMode.REVIEWER_ASSESSMENT
            End If
            If Me.ViewState("CustomItemUnkid") > 0 Then
                blnFlag = objCItems.Update(Session("UserId"))
            Else
                blnFlag = objCItems.Insert(Session("UserId"))
            End If

            If blnFlag = False And objCItems._Message <> "" Then
                DisplayMessage.DisplayMessage(objCItems._Message, Me)
                popup_AddCustomItems.Show()
                Exit Sub
            End If
            Me.ViewState("CustomItemUnkid") = 0
            popup_AddCustomItems.Hide()

            'Nilay (02-Mar-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            hdf_btnCItemFld.Value = "0"
            'Nilay (02-Mar-2015) -- End

            If blnFlag = True Then Call FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Nilay (02-Mar-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'Nilay (02-Mar-2015) -- End

    Protected Sub popConfirm_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popConfirm.buttonYes_Click
        Try
            Dim objCItem As New clsassess_custom_items
            If objCItem.Delete(Me.ViewState("CustomItemUnkid"), Session("UserId")) Then
                Call FillGrid()
            End If
            objCItem = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls Events "

    Protected Sub cbopopItemType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopopItemType.SelectedIndexChanged
        Try
            If CInt(IIf(cbopopItemType.SelectedValue <= "0", clsassess_custom_items.enCustomType.FREE_TEXT, cbopopItemType.SelectedValue)) = clsassess_custom_items.enCustomType.SELECTION Then
                cboMode.Enabled = True
            Else
                cboMode.Enabled = False
            End If

            popup_AddCustomItems.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.ItemCommand
        Try
            If e.CommandName = "objEdit" Then
                If e.Item.Cells.Count > 0 Then
                    If e.Item.Cells(8).Text = 2 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Sorry, You cannot edit this Custom Item. Reason Period is already closed."), Me)
                        Exit Sub
                    End If
                    Me.ViewState("EditRowIndex") = e.Item.ItemIndex
                    Me.ViewState("CustomItemUnkid") = dgvData.DataKeys(e.Item.ItemIndex)
                    Call Display_Popup()
                End If
            ElseIf e.CommandName = "objDelete" Then
                If e.Item.Cells.Count > 0 Then
                    If e.Item.Cells(8).Text = 2 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Sorry, You cannot edit this Custom Item. Reason Period is already closed."), Me)
                        Exit Sub
                    End If
                    Me.ViewState("CustomItemUnkid") = dgvData.DataKeys(e.Item.ItemIndex)
                    popConfirm.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Are you sure you want to delete this Custom Item?")
                    popConfirm.Title = "Aruti"
                    popConfirm.Show()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region
    'Nilay (02-Mar-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    Protected Sub btnCItemCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCItemCancel.Click
        Try
            hdf_btnCItemFld.Value = "0"
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'Nilay (02-Mar-2015) -- End
    End Sub

End Class
