﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgEmployeeEvaluation.aspx.vb" Inherits="Assessment_New_Performance_Evaluation_wPgEmployeeEvaluation" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="CnfCtrl" TagPrefix="uc3" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="NumericText" TagPrefix="uc4" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DelReason" TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (cval.length > 0)
                if (charCode == 45)
                if (cval.indexOf("-") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <%--<style type="text/css">
        .flex-container
        {
            display: flex;
            flex-direction: column;
            align-items: stretch;
        }
        .sub-flex-container
        {
            display: flex;
            flex-direction: column;
            align-items: stretch;
        }
    </style>--%>

    <script type="text/javascript">
        //        function pageLoad(sender, args) {
        //            $("select").searchable();
        //        }

        function ChangeApplicantFilterImage(imgID, divID) {
            var pathname = document.location.href;
            var arr = pathname.split('/');
            var imgURL = document.getElementById(imgID).src.split('/');
            var URL = arr[0] + '/' + arr[1] + '/' + arr[2] + '/' + arr[3] + '/';

            if (imgURL[imgURL.length - 1] == 'plus.png') {
                document.getElementById(imgID).src = URL + "images/minus.png";
                document.getElementById(divID).style.display = 'block';
            }
            if (imgURL[imgURL.length - 1] == 'minus.png') {
                document.getElementById(imgID).src = URL + "images/plus.png";
                document.getElementById(divID).style.display = 'none';
            }
        }

        function setlocation() {
            document.getElementById("divBSC").style.display = "none";
            document.getElementById("divGE").style.display = "none";
            document.getElementById("divCItem").style.display = "none";
            var exOrder = '<%= Session("Perf_EvaluationOrder") %>';

            var res = exOrder.split("|");
            var order = 2;
            if (res.length > 0) {
                for (var i = 0; i < res.length; i++) {
                    if (res[i] == "1") {
                        document.getElementById("divBSC").style.display = "block";
                        document.getElementById("divBSC").style.order = order;
                        order = order + 1;
                    }
                    if (res[i] == "2") {
                        document.getElementById("divGE").style.display = "block";
                        document.getElementById("divGE").style.order = order;
                        order = order + 1;
                    }
                    if (res[i] == "3") {
                        document.getElementById("divCItem").style.display = "block";
                        document.getElementById("divCItem").style.order = order;
                        order = order + 1;
                    }
                }
            }
        }
        $(document).ready(function() {
            setlocation();
        });

        function set_location() {
            setlocation();
        }

        function isvalid_value(ctrl) {

            var prd = document.getElementById("<%=cboPeriod.ClientID%>");
            var emp = document.getElementById("<%=cboEmployee.ClientID%>");
            var ctmid = $(ctrl).closest("tr").find("td[Id='ctmid']").text();

            PageMethods.IsValidCustomValue(ctmid, $(ctrl).val(), $(emp).val(), $(prd).val(), onSuccess, onFailure);

            function onSuccess(str) {
                if (str.toString().length != 0) {
                    $(ctrl).val('')
                    alert(str);
                }
            }

            function onFailure(err) {
                alert(err.get_message());
            }
        }

        function change_event_GE(drp) {

            var ScoringOptionId = '<%= Session("ScoringOptionId") %>';
            var prd = document.getElementById("<%=cboPeriod.ClientID%>");
            var dtp = document.getElementById("<%= dtpAssessdate.ClientID %>" + '_TxtDate');
            var emp = document.getElementById("<%=cboEmployee.ClientID%>");
            var EmployeeAsOnDate = '<%= Session("EmployeeAsOnDate") %>';
            var IsUseAgreedScore = '<%= Session("IsUseAgreedScore") %>';
            var Self_Assign_Competencies = '<%= Session("Self_Assign_Competencies") %>';
            //            var test = document.getElementById("<%= objlblGEScr.ClientID %>");            
            //            var result = 0;
            var row = $(drp).closest("tr");
            var itemid = $(drp).closest("tr").find("td[Id='CItemId']").text();
            var iAGrpId = $(drp).closest("tr").find("td[Id='AGrpId']").text();


            var ivalue = 0;
            var irmark = "";

            if (ScoringOptionId == "1") {
                //Checking for Undefined -- 10-Sep-2020
                if ($(drp).closest("tr").find("input")[0]) {
                    ivalue = $(drp).closest("tr").find("input")[0].value;
                }
            }
            else if (ScoringOptionId == "2") {
                //Checking for Undefined -- 10-Sep-2020
                if ($(drp).closest("tr").find("select")[0]) {
                    ivalue = $(drp).closest("tr").find("select")[0].value;
                }
            }

            //Checking for Undefined -- 10-Sep-2020
            if ($(drp).closest("tr").find("textarea")[0])
            { irmark = $(drp).closest("tr").find("textarea")[0].value; }


            if (ScoringOptionId == "1") {
                var wgt = $(drp).closest("tr").find("td[Id='bWeight']").text();
                //'S.SANDEEP |10-AUG-2020| -- START
                //ISSUE/ENHANCEMENT : SPRINT-4 {2020}
                //if (parseFloat($(drp).val()) > parseFloat(wgt)) {
                //'S.SANDEEP |10-AUG-2020| -- END
                if (parseFloat(ivalue) > parseFloat(wgt)) {
                    alert('Sorry, You cannot enter score beyond the weight set for the selected goal. Please set proper score.');
                    $(drp).val('');
                    $(drp).focus();
                    return;
                }
            }
            else if (ScoringOptionId == "2") {
                if (isNaN(ivalue) == true) {
                    ivalue = 0;
                    return;
                }
            }

            //            var itemWgt = $(drp).closest("tr").find("td[Id='bWeight']").text();
            //            var iMaxScl = $(drp).closest("tr").find("td[Id='bmax']").text();
            //            var btrnId = $(drp).closest("tr").find("td[Id='btrnId']").text();

            //            PageMethods.ComputeScoreGE(ScoringOptionId, $(prd).val(), $(emp).val(), EmployeeAsOnDate, IsUseAgreedScore, Self_Assign_Competencies, parseFloat(ivalue), itemid, iAGrpId, btrnId, irmark, itemWgt, iMaxScl, $(dtp).val(), onSuccess, onFailure);

            //            function onSuccess(str) {
            //                result = parseFloat(str);
            //                var oldr = parseFloat(test.innerHTML);
            //                result = (result + oldr);
            //                test.innerHTML = result.toFixed(2);
            //            }

            //            function onFailure(err) {
            //                alert(err.get_message());
            //            }
        }

        function change_event(drp) {

            var ctrl = document.getElementById(drp);
            if (ctrl === null) { ctrl = drp; }
            if (ctrl != null) {
                var ScoringOptionId = '<%= Session("ScoringOptionId") %>';
                var prd = document.getElementById("<%=cboPeriod.ClientID%>");
                var dtp = document.getElementById("<%= dtpAssessdate.ClientID %>" + '_TxtDate');
                var emp = document.getElementById("<%=cboEmployee.ClientID%>");
                var EmployeeAsOnDate = '<%= Session("EmployeeAsOnDate") %>';
                var IsUseAgreedScore = '<%= Session("IsUseAgreedScore") %>';
                var AutoRating = '<%= Session("EnableBSCAutomaticRating") %>';

                //S.SANDEEP |17-MAY-2021| -- START
                //ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES
                var auto100 = '<%= Session("DontAllowRatingBeyond100") %>';
                var sysgen = '<%= Session("DontAllowToEditScoreGenbySys") %>'
                //S.SANDEEP |17-MAY-2021| -- END

                var Self_Assign_Competencies = '<%= Session("Self_Assign_Competencies") %>';
                //                var test = document.getElementById("<%= objlblBSCScr.ClientID %>");
                //                var result = 0;
                var row = $(ctrl).closest("tr");
                var ilnkid = $(ctrl).closest("tr").find("td[Id='LinkedField']").text();
                var tds = $(ctrl).closest("tr").find("td[Id='objdgcolhempf" + ilnkid + "']").text();

                var ivalue = 0;
                var irmark = "";

                if (ScoringOptionId == "1") {
                    //Checking for Undefined -- 10-Sep-2020
                    if ($(ctrl).closest("tr").find("input")[0]) {
                        ivalue = $(ctrl).closest("tr").find("input")[0].value;
                    }
                }
                else if (ScoringOptionId == "2") {
                    //Checking for Undefined -- 10-Sep-2020
                    if ($(ctrl).closest("tr").find("select")[0]) {
                        ivalue = $(ctrl).closest("tr").find("select")[0].value;
                    }
                }
                //Checking for Undefined -- 10-Sep-2020
                if ($(ctrl).closest("tr").find("textarea")[0])
                { irmark = $(ctrl).closest("tr").find("textarea")[0].value; }

                if (ScoringOptionId == "1") {
                    if (AutoRating == 'False') {
                        var wgt = $(ctrl).closest("tr").find("td[Id='bWeight']").text();
                        if (parseFloat(ivalue) > parseFloat(wgt)) {
                            alert('Sorry, You cannot enter score beyond the weight set for the selected goal. Please set proper score.');
                            $(ctrl).val('');
                            $(ctrl).focus();
                            return;
                        }
                    }
                    else if (AutoRating.toLowerCase() == 'true') {
                        if (auto100.toLowerCase() == 'true') {
                            if (parseFloat(ivalue) > 100) {
                                if ($(ctrl).closest("tr").find("input")[0]) {
                                    $(ctrl).closest("tr").find("input")[0].value = '100';
                                }
                            }
                        }
                    }
                }
                else if (ScoringOptionId == "2") {
                    if (isNaN(ivalue) == true) {
                        ivalue = 0;
                        return;
                    }
                }

                //                var empfld1 = $(ctrl).closest("tr").find("td[Id='objdgcolhempf1']").text();
                //                var empfld2 = $(ctrl).closest("tr").find("td[Id='objdgcolhempf2']").text();
                //                var empfld3 = $(ctrl).closest("tr").find("td[Id='objdgcolhempf3']").text();
                //                var empfld4 = $(ctrl).closest("tr").find("td[Id='objdgcolhempf4']").text();
                //                var empfld5 = $(ctrl).closest("tr").find("td[Id='objdgcolhempf5']").text();
                //                var itemWgt = $(ctrl).closest("tr").find("td[Id='bWeight']").text();
                //                var iMaxScl = $(ctrl).closest("tr").find("td[Id='bmax']").text();
                //                var btrnId = $(ctrl).closest("tr").find("td[Id='btrnId']").text();
                //                var bprsid = $(ctrl).closest("tr").find("td[Id='bpId']").text();

                //                PageMethods.ComputeScoreBSC(ScoringOptionId, $(prd).val(), $(emp).val(), EmployeeAsOnDate, IsUseAgreedScore, Self_Assign_Competencies, ivalue, tds, btrnId, bprsid, empfld1, empfld2, empfld3, empfld4, empfld5, irmark, itemWgt, iMaxScl, $(dtp).val(), onSuccess, onFailure);

                //                function onSuccess(str) {
                //                    result = parseFloat(str);
                //                    var oldr = parseFloat(test.innerHTML);
                //                    result = (result + oldr);
                //                    test.innerHTML = result.toFixed(2);
                //                }

                //                function onFailure(err) {
                //                    alert(err.get_message());
                //                }
            }
        }        
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />
    <asp:HiddenField ID="hdf_topposition" runat="server" />
    <asp:HiddenField ID="hdf_leftposition" runat="server" />
    <asp:HiddenField ID="hdfBSCScore" runat="server" Value="0" />

    <script type="text/javascript">

        $(document).ready(function() {
            $(".lnAdd").html("<i class='fas fa-plus'></i>");
            $(".lnEdit").html("<i class='fas fa-pencil-alt text-primary'></i>");
            $(".lnDelt").html("<i class='fas fa-trash text-danger'></i>");
        });


        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        var scroll2 = {
            Y: '#<%= hfScrollPosition2.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);

                $(".lnAdd").html("<i class='fas fa-plus'></i>");
                $(".lnEdit").html("<i class='fas fa-pencil-alt text-primary'></i>");
                $(".lnDelt").html("<i class='fas fa-trash text-danger'></i>");
            }
        });

        var xPos, yPos, xmainPos, ymainPos;
        function beginRequestHandler(sender, args) {


        }

        function endRequestHandler(sender, args) {
            $(".lnAdd").html("<i class='fas fa-plus'></i>");
            $(".lnEdit").html("<i class='fas fa-pencil-alt text-primary'></i>");
            $(".lnDelt").html("<i class='fas fa-trash text-danger'></i>");
        }
    </script>

    <script type="text/javascript">

        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("height", trheight + "px");
                    $(arrPnl[j]).css("overflow", "auto");
                }
                else {
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%;">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Self Performance Evaluation"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblAssessDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <uc2:DateCtrl ID="dtpAssessdate" runat="server" AutoPostBack="true" />
                                        </div>
                                        <asp:Button ID="BtnSearch" runat="server" CssClass="btndefault" Text="Search" OnClientClick="set_location();"
                                            Visible="false" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body" style="max-height: 1400px; height: auto">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div id="divInstruction" class="header" onclick="ChangeApplicantFilterImage('img1','divInstructionValue');">
                                                <h2>
                                                    <asp:Label ID="lblInstruction" runat="server" Text="Assessment Instruction"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r-10 p-l-0">
                                                    <img id="img1" src="../../images/plus.png" alt="" />
                                                </ul>
                                            </div>
                                            <div class="body" style="height: 400px; display: none" id="divInstructionValue">
                                                <div class="row clearfix" style="height: 100%">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12" style="height: 100%">
                                                        <div class="form-group" style="height: 100%">
                                                            <div class="form-line" style="height: 100%">
                                                                <asp:TextBox ID="txtInstruction" runat="server" TextMode="MultiLine" class="form-control"
                                                                    ReadOnly="true" Height="100%" Style="resize: none"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblAssesmentItemHeader" runat="server" Text="Assessment Items"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body" style="max-height: 1263px; overflow: auto; height: auto">
                                                <div class="row clearfix" id="divBSC" style="display: none">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                        <div class="card inner-card">
                                                            <div class="header">
                                                                <h2>
                                                                    <asp:Label ID="lblBSC" runat="server" Text="Objectives/Goals/Targets"></asp:Label>
                                                                </h2>
                                                                <div style="display: none">
                                                                    <asp:Label ID="objlblBSCWgt" runat="server" Text="Total Weight:" Visible="false"></asp:Label>
                                                                    <asp:Label ID="objlblBSCScrCaption" runat="server" Text="Score : " Visible="false"></asp:Label>
                                                                    <asp:Label ID="objlblBSCScr" runat="server" Visible="false"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="body">
                                                                <div class="row clearfix">
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                        <div class="table-responsive" style="height: 400px">
                                                                            <asp:DataGrid ID="dgvBSC" runat="server" runat="server" AutoGenerateColumns="false"
                                                                                Width="100%" AllowPaging="false" CssClass="table-hover table-bordered" HeaderStyle-Font-Bold="true">
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="Field1" HeaderText="" ReadOnly="true" FooterText="objdgcolhBSCField1" />
                                                                                    <%--0--%>
                                                                                    <asp:BoundColumn DataField="Field2" HeaderText="" ReadOnly="true" FooterText="objdgcolhBSCField2" />
                                                                                    <%--1--%>
                                                                                    <asp:BoundColumn DataField="Field3" HeaderText="" ReadOnly="true" FooterText="objdgcolhBSCField3" />
                                                                                    <%--2--%>
                                                                                    <asp:BoundColumn DataField="Field4" HeaderText="" ReadOnly="true" FooterText="objdgcolhBSCField4" />
                                                                                    <%--3--%>
                                                                                    <asp:BoundColumn DataField="Field5" HeaderText="" ReadOnly="true" FooterText="objdgcolhBSCField5" />
                                                                                    <%--4--%>
                                                                                    <asp:BoundColumn DataField="Field6" HeaderText="" ReadOnly="true" FooterText="objdgcolhBSCField6" />
                                                                                    <%--5--%>
                                                                                    <asp:BoundColumn DataField="Field7" HeaderText="" ReadOnly="true" FooterText="objdgcolhBSCField7" />
                                                                                    <%--6--%>
                                                                                    <asp:BoundColumn DataField="Field8" HeaderText="" ReadOnly="true" FooterText="objdgcolhBSCField8" />
                                                                                    <%--7--%>
                                                                                    <asp:BoundColumn DataField="St_Date" HeaderText="Start Date" ReadOnly="true" FooterText="dgcolhSDate" />
                                                                                    <%--8--%>
                                                                                    <asp:BoundColumn DataField="Ed_Date" HeaderText="End Date" ReadOnly="true" FooterText="dgcolhEDate" />
                                                                                    <%--9--%>
                                                                                    <asp:BoundColumn DataField="pct_complete" HeaderText="% Completed" ReadOnly="true"
                                                                                        FooterText="dgcolhCompleted" />
                                                                                    <%--10--%>
                                                                                    <asp:BoundColumn DataField="dgoaltype" HeaderText="Goal Type" ReadOnly="true" FooterText="dgcolhGoalType">
                                                                                    </asp:BoundColumn>
                                                                                    <%--11--%>
                                                                                    <asp:BoundColumn DataField="dgoalvalue" HeaderText="Goal Value" ReadOnly="true" ItemStyle-HorizontalAlign="Right"
                                                                                        FooterText="dgoalvalue"></asp:BoundColumn>
                                                                                    <%--12--%>
                                                                                    <asp:BoundColumn DataField="CStatus" HeaderText="Status" ReadOnly="true" FooterText="dgcolhStatus" />
                                                                                    <%--13--%>
                                                                                    <asp:BoundColumn DataField="Weight" HeaderText="Weight" ReadOnly="true" FooterText="dgcolhBSCWeight" />
                                                                                    <%--14--%>
                                                                                    <asp:TemplateColumn HeaderText="Result" FooterText="dgcolheselfBSC" HeaderStyle-Width="7%"
                                                                                        ItemStyle-Width="7%">
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="dgcolheselfBSC" runat="server" Width="100%" Text='<%# Eval("eself") %>'
                                                                                                Style="text-align: right; resize: none" Visible="false" onchange="change_event(this);"
                                                                                                class="form-control decimal"></asp:TextBox>
                                                                                            <div class="form-group">
                                                                                                <asp:DropDownList ID="dgcolhSelBSCSel" runat="server" Width="90%" Visible="false"
                                                                                                    onchange="change_event(this);">
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%--15--%>
                                                                                    <asp:TemplateColumn HeaderText="Remark" FooterText="dgcolheremarkBSC" HeaderStyle-Width="350px">
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="dgcolheremarkBSC" runat="server" Width="100%" Style="resize: none"
                                                                                                Text='<%# Eval("eremark") %>' TextMode="MultiLine" Rows="2" onchange="change_event(this);"
                                                                                                class="form-control"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%--16--%>
                                                                                    <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrpBSC" Visible="false" />
                                                                                    <%--17--%>
                                                                                    <asp:BoundColumn DataField="GrpId" FooterText="objdgcolhGrpIdBSC" Visible="false" />
                                                                                    <%--18--%>
                                                                                    <asp:BoundColumn DataField="scalemasterunkid" FooterText="objdgcolhScaleMasterId"
                                                                                        Visible="false" />
                                                                                    <%--19--%>
                                                                                    <asp:BoundColumn DataField="empfield1unkid" FooterText="objdgcolhempfield1unkid" />
                                                                                    <%--20--%>
                                                                                    <asp:BoundColumn DataField="empfield2unkid" FooterText="objdgcolhempfield2unkid" />
                                                                                    <%--21--%>
                                                                                    <asp:BoundColumn DataField="empfield3unkid" FooterText="objdgcolhempfield3unkid" />
                                                                                    <%--22--%>
                                                                                    <asp:BoundColumn DataField="empfield4unkid" FooterText="objdgcolhempfield4unkid" />
                                                                                    <%--23--%>
                                                                                    <asp:BoundColumn DataField="empfield5unkid" FooterText="objdgcolhempfield5unkid" />
                                                                                    <%--24--%>
                                                                                    <asp:BoundColumn DataField="LinkedFieldId" FooterText="objdgcolhLinkedFieldId" />
                                                                                    <%--25--%>
                                                                                    <asp:BoundColumn DataField="" FooterText="objdgcolhMaxScale" />
                                                                                    <%--26--%>
                                                                                    <asp:BoundColumn DataField="analysistranunkid" FooterText="objdgcolhanalysistranunkid">
                                                                                    </asp:BoundColumn>
                                                                                    <%--27--%>
                                                                                    <asp:BoundColumn DataField="perspectiveunkid" FooterText="objdgcolperspectiveunkid">
                                                                                    </asp:BoundColumn>
                                                                                    <%--28--%>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix" id="divGE" style="display: none">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                        <div class="card inner-card">
                                                            <div class="header">
                                                                <h2>
                                                                    <asp:Label ID="lblGE" runat="server" Text="Competencies"></asp:Label>
                                                                </h2>
                                                                <div style="display: none">
                                                                    <asp:Label ID="objlblGEWgt" runat="server" Text="Total Weight :" Visible="false"></asp:Label>
                                                                    <asp:Label ID="objlblGEScrCaption" runat="server" Text="Score : " Visible="false"></asp:Label>
                                                                    <asp:Label ID="objlblGEScr" runat="server" Visible="false"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="body">
                                                                <div class="row clearfix">
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                        <div class="table-responsive" style="height: 400px">
                                                                            <asp:DataGrid ID="dgvGE" runat="server" runat="server" AutoGenerateColumns="false"
                                                                                Width="99%" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                                                <Columns>
                                                                                    <asp:BoundColumn DataField="eval_item" HeaderText="Items" FooterText="dgcolheval_itemGE"
                                                                                        ItemStyle-Width="55%" HeaderStyle-Width="55%" />
                                                                                    <%--0--%>
                                                                                    <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="dgcolhGEWeight"
                                                                                        ItemStyle-Width="10%" HeaderStyle-Width="10%" />
                                                                                    <%--1--%>
                                                                                    <asp:TemplateColumn HeaderText="Result" FooterText="dgcolheselfGE" ItemStyle-Width="6%"
                                                                                        HeaderStyle-Width="6%">
                                                                                        <ItemStyle HorizontalAlign="Right" />
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="dgcolheselfGE" runat="server" Style="text-align: right; resize: none"
                                                                                                Text='<%# Eval("eself") %>' Width="100%" onblur="getGEscrollPosition()" onchange="change_event_GE(this);"
                                                                                                Visible="false" class="form-control decimal"></asp:TextBox>
                                                                                            <div class="form-group">
                                                                                                <asp:DropDownList ID="dgcolhSelGESel" runat="server" Style="width: 98%;" Visible="false"
                                                                                                    onchange="change_event_GE(this);">
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%--2--%>
                                                                                    <asp:BoundColumn DataField="" HeaderText="Final Score" FooterText="objdgcolhedisplayGE"
                                                                                        Visible="false" />
                                                                                    <%--3--%>
                                                                                    <asp:TemplateColumn HeaderText="Self Remark" FooterText="dgcolheremarkGE" ItemStyle-Width="20%"
                                                                                        HeaderStyle-Width="20%">
                                                                                        <ItemStyle HorizontalAlign="Left" />
                                                                                        <ItemTemplate>
                                                                                            <asp:TextBox ID="dgcolheremarkGE" runat="server" Width="98%" onblur="getGEscrollPosition()"
                                                                                                Text='<%# Eval("eremark") %>' Style="resize: none" TextMode="MultiLine" Rows="3"
                                                                                                onchange="change_event_GE(this);" class="form-control"></asp:TextBox>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%--4--%>
                                                                                    <asp:BoundColumn DataField="scalemasterunkid" FooterText="objdgcolhscalemasterunkidGE"
                                                                                        Visible="false" />
                                                                                    <%--5--%>
                                                                                    <asp:BoundColumn DataField="competenciesunkid" FooterText="objdgcolhcompetenciesunkidGE" />
                                                                                    <%--6--%>
                                                                                    <asp:BoundColumn DataField="assessgroupunkid" FooterText="objdgcolhassessgroupunkidGE" />
                                                                                    <%--7--%>
                                                                                    <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrpGE" Visible="false" />
                                                                                    <%--8--%>
                                                                                    <asp:BoundColumn DataField="IsPGrp" FooterText="objdgcolhIsPGrpGE" Visible="false" />
                                                                                    <%--9--%>
                                                                                    <asp:BoundColumn DataField="GrpId" FooterText="objdgcolhGrpIdGE" Visible="false" />
                                                                                    <%--10--%>
                                                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                                        HeaderText="Info." ItemStyle-Width="5%" HeaderStyle-Width="5%" FooterText="objdgcolhInformation">
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="lnkCol" runat="server" CommandName="viewdescription" Font-Underline="false"
                                                                                                Enabled="false"><i class="fa fa-info-circle" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateColumn>
                                                                                    <%--11--%>
                                                                                    <asp:BoundColumn DataField="" FooterText="objdgcolhMaxScale" />
                                                                                    <%--12--%>
                                                                                    <asp:BoundColumn DataField="analysistranunkid" FooterText="objdgcolhanalysistranunkid">
                                                                                    </asp:BoundColumn>
                                                                                    <%--13--%>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix" id="divCItem" style="display: none">
                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                        <asp:Panel ID="cItem" runat="server">
                                                        <div class="card inner-card">
                                                            <div class="header">
                                                                <h2>
                                                                    <asp:Label ID="Label1" runat="server" Text="Custom Section"></asp:Label>
                                                                </h2>
                                                                <ul class="header-dropdown m-r-40 p-l-0">
                                                                    <asp:LinkButton ID="lnkprevious" runat="server" Style="text-align: center; min-width: 30px;
                                                                        margin: 0 10px" ToolTip="Previous"> <i class="fa fa-arrow-circle-left"></i> </asp:LinkButton>
                                                                </ul>
                                                                <ul class="header-dropdown m-r-10 p-l-0">
                                                                    <asp:LinkButton ID="lnkNext" runat="server" Style="text-align: center; min-width: 30px;
                                                                        margin: 0 10px" ToolTip="Next"> <i class="fa fa-arrow-circle-right"></i>
                                                                    </asp:LinkButton>
                                                                </ul>
                                                            </div>
                                                            <div class="body">
                                                                    <div class="row clearfix" style="padding: 10px; background-color: #DDD; color: #000;
                                                                        font-weight: bold">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                            <asp:Label ID="lblCustomHeaderval" runat="server" Text="Section Header(s)"></asp:Label>
                                                                        </div>
                                                                    </div>
                                                                <div class="row clearfix">
                                                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                                                        <div class="table-responsive" style="height: 300px">
                                                                            <asp:GridView ID="dgvItems" runat="server" runat="server" AutoGenerateColumns="false"
                                                                                Width="99%" AllowPaging="false" CssClass="table table-hover table-bordered" DataKeyNames="analysisunkid,viewmodeid,periodunkid,itemtypeid,selectionmodeid,GUID,ismanual,Header_Id,Header_Name,Is_Allow_Multiple">
                                                                                <Columns>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="footer">
                                                            </div>
                                                        </div>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSaveCommit" runat="server" CssClass="btn btn-primary" Text="Submit" />
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popup_CItemAddEdit" runat="server" TargetControlID="hdf_cItem"
                    CancelControlID="hdf_cItem" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_CItemAddEdit"
                    Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_CItemAddEdit" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCItem" runat="server" Text="Custom Section"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblpopupHeader" runat="server" Text="Aruti" CssClass="form-label"></asp:Label>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 360px">
                                    <asp:DataGrid ID="dgv_Citems" runat="server" runat="server" runat="server" AutoGenerateColumns="false"
                                        Width="99%" AllowPaging="false" CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:BoundColumn HeaderText="Custom Items" DataField="custom_item" FooterText="dgcolhItems"
                                                HeaderStyle-Width="450px" ItemStyle-Width="450px"></asp:BoundColumn>
                                            <%--0--%>
                                            <asp:TemplateColumn FooterText="dgcolhValue" HeaderText="Custom Value" HeaderStyle-Width="250px"
                                                ItemStyle-Width="250px">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtFreetext" runat="server" TextMode="MultiLine" Rows="3" Width="100%"
                                                        Text='<%# Eval("custom_value") %>' AutoPostBack="false" Visible="false" class="form-control"></asp:TextBox>
                                                    <div class="form-group">
                                                        <asp:DropDownList ID="cboSelection" runat="server" Width="238px" AutoPostBack="false"
                                                            Visible="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="form-group">
                                                        <uc2:DateCtrl ID="dtpSelection" runat="server" AutoPostBack="false" Visible="false" />
                                                    </div>
                                                    <uc4:NumericText ID="txtNUM" runat="server" Width="100%" AutoPostBack="false" Text='<%# Eval("custom_value") %>'
                                                        Visible="false" class="form-control" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <%--1--%>
                                            <asp:BoundColumn DataField="itemtypeid" Visible="false" FooterText="clmCntType">
                                            </asp:BoundColumn>
                                            <%--2--%>
                                            <asp:BoundColumn DataField="rOnly" Visible="false" FooterText="clmrOnly"></asp:BoundColumn>
                                            <%--3--%>
                                            <asp:BoundColumn DataField="customitemunkid" FooterText="objdgcustomitemunkid"></asp:BoundColumn>
                                            <%--4--%>
                                            <asp:BoundColumn DataField="selectionmodeid" Visible="false"></asp:BoundColumn>
                                            <%--5--%>
                                            <asp:BoundColumn DataField="isdefaultentry" Visible="false"></asp:BoundColumn>
                                            <%--6--%>
                                            <asp:BoundColumn DataField="iscompletedtraining" Visible="false"></asp:BoundColumn>
                                            <%--7--%>
                                            <asp:BoundColumn DataField="selectedid" Visible="false" FooterText="objdgcolhselectedid">
                                            </asp:BoundColumn>
                                            <%--8--%>
                                            <asp:BoundColumn DataField="ddate" Visible="false" FooterText="objdgcolhddate"></asp:BoundColumn>
                                            <%--9--%>
                                            <asp:BoundColumn DataField="iRole" Visible="false" FooterText="objdgcolhiRole"></asp:BoundColumn>
                                            <%--10--%>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_cItem" runat="server" />
                        <asp:Button ID="btnIAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                        <asp:Button ID="btnIClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <uc5:DelReason ID="delCUstomItem" runat="server" />
                <uc3:CnfCtrl ID="cnfSubmit" runat="server" Title="Aruti" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
