﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
Imports System.Drawing
Imports AjaxControlToolkit
Imports System.IO
Imports System.Web.Services
Imports System.Data.SqlClient
Imports System.Net.Dns

#End Region

Partial Class Assessment_New_Performance_Evaluation_wPgReviewerEvaluation
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private mstrModuleName As String = "frmPerformanceEvaluation"
    Private objEAnalysisMst As New clsevaluation_analysis_master
    'Private objGoalsTran As New clsgoal_analysis_tran
    'Private objCAssessTran As New clscompetency_analysis_tran
    'Private objCCustomTran As New clscompeteny_customitem_tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintAssessAnalysisUnkid As Integer = -1
    Private mblnIsMatchCompetencyStructure As Boolean = False
    Private disBSCColumns As Dictionary(Of String, Integer) = Nothing
    Private disGEColumns As Dictionary(Of String, Integer) = Nothing
    Private mintYearUnkid As Integer = 0
    Private Shared iExOrdr As Integer = 0
    Private iLinkedFieldId As Integer
    Private iMappingUnkid As Integer
    Private xTotAssignedWeight As Decimal = 0
    Private xVal As Integer = 1
    Private mblnItemAddEdit As Boolean = False
    Private mstriEditingGUID As String = ""
    Private objCONN As SqlConnection
    Private menAssess As enAssessmentMode = enAssessmentMode.REVIEWER_ASSESSMENT

    'Gajanan [28-May-2020] -- Start
    Private mdicCustomHeader As New Dictionary(Of Integer, String)
    Private mintCustomHeaderVal As Integer = -1
    Private mintCustomHeaderIndex As Integer = 0
    'Gajanan [28-May-2020] -- End
    Private mintEvaluationTypeId As Integer = clsevaluation_analysis_master.enPAEvalTypeId.EO_BOTH 'S.SANDEEP |13-NOV-2020| -- START -- END

#End Region

#Region "Page Event"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            disBSCColumns = dgvBSC.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvBSC.Columns.IndexOf(x))
            disGEColumns = dgvGE.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvGE.Columns.IndexOf(x))
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                If Request.QueryString.Count <= 0 Then Exit Sub
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")
                If arr.Length = 6 Then 'S.SANDEEP |13-NOV-2020| -- START {If arr.Length = 5 Then} -- END
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try
                    Blank_ModuleName()
                    clsCommonATLog._WebFormName = "frmPerformanceEvaluation"
                    StrModuleName2 = "mnuAssessment"
                    StrModuleName3 = "mnuPerformaceEvaluation"
                    clsCommonATLog._WebClientIP = Session("IP_ADD")
                    clsCommonATLog._WebHostName = Session("HOST_NAME")
                    Me.ViewState.Add("IsDirect", True)

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    HttpContext.Current.Session("UserId") = CInt(arr(1))
                    Me.ViewState.Add("employeeunkid", CInt(arr(2)))
                    Me.ViewState.Add("assessormasterunkid", CInt(arr(3)))
                    Me.ViewState.Add("periodid", CInt(arr(4)))

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("Database_Name") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("Database_Name").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Session("IsAllowFinalSave") = clsConfig._IsAllowFinalSave
                    Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp
                    Session("EmployeeAsOnDate") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)
                    Session("IsBSC_ByEmployee") = clsConfig._IsBSC_ByEmployee
                    Session("IsCompanyNeedReviewer") = clsConfig._IsCompanyNeedReviewer
                    Session("Assessment_Instructions") = clsConfig._Assessment_Instructions
                    Session("ViewTitles_InPlanning") = clsConfig._ViewTitles_InPlanning
                    Session("FollowEmployeeHierarchy") = clsConfig._FollowEmployeeHierarchy
                    Session("CascadingTypeId") = clsConfig._CascadingTypeId
                    Session("ScoringOptionId") = clsConfig._ScoringOptionId
                    'Session("Perf_EvaluationOrder") = clsConfig._Perf_EvaluationOrder
                    Session("ViewTitles_InEvaluation") = clsConfig._ViewTitles_InEvaluation
                    Session("ConsiderItemWeightAsNumber") = clsConfig._ConsiderItemWeightAsNumber
                    Session("Self_Assign_Competencies") = clsConfig._Self_Assign_Competencies
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, Session("Database_Name"))
                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName
                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    'S.SANDEEP |13-NOV-2020| -- START
                    'ISSUE/ENHANCEMENT : COMPETENCIES
                    'Session("Perf_EvaluationOrder") = clsConfig._Perf_EvaluationOrder
                    Session("EvaluationTypeId") = clsevaluation_analysis_master.enPAEvalTypeId.EO_BOTH
                    Session("RunCompetenceAssessmentSeparately") = clsConfig._RunCompetenceAssessmentSeparately
                    Session("OrgPerf_EvaluationOrder") = clsConfig._Perf_EvaluationOrder
                    Dim strValue As String = HttpContext.Current.Session("OrgPerf_EvaluationOrder")
                    If CInt(arr(5)) = clsevaluation_analysis_master.enPAEvalTypeId.EO_COMPETENCE Then
                        If strValue IsNot Nothing AndAlso strValue.Trim.Length > 0 Then
                            If Array.IndexOf(strValue.Split(CChar("|")), CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString()) <> -1 Then
                                Dim iarr As String() = strValue.Split(CChar("|"))
                                iarr = iarr.Select(Function(s) s.Replace(CInt(enEvaluationOrder.PE_BSC_SECTION).ToString(), "0")).ToArray()
                                strValue = String.Join("|", iarr)
                            ElseIf Array.IndexOf(strValue.Split(CChar("|")), CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString()) <= -1 Then
                                Dim iarr As String() = strValue.Split(CChar("|"))
                                iarr = iarr.Select(Function(s) s.Replace(CInt(enEvaluationOrder.PE_BSC_SECTION).ToString(), CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString())).ToArray()
                                strValue = String.Join("|", iarr)
                            End If
                        End If
                        HttpContext.Current.Session("EvaluationTypeId") = clsevaluation_analysis_master.enPAEvalTypeId.EO_COMPETENCE
                    Else
                        strValue = HttpContext.Current.Session("OrgPerf_EvaluationOrder")
                        If CBool(HttpContext.Current.Session("RunCompetenceAssessmentSeparately")) = True Then HttpContext.Current.Session("EvaluationTypeId") = clsevaluation_analysis_master.enPAEvalTypeId.EO_SCORE_CARD
                    End If
                    HttpContext.Current.Session("Perf_EvaluationOrder") = strValue
                    'S.SANDEEP |13-NOV-2020| -- END

                    Dim objUserPrivilege As New clsUserPrivilege
                    objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                    Session("AllowtoAddReviewerEvaluation") = objUserPrivilege._AllowtoAddReviewerEvaluation
                    txtInstruction.Text = Session("Assessment_Instructions")
                    Call FillCombo()
                    Call GetValue()
                    cboReviewer.SelectedValue = CInt(Me.ViewState("assessormasterunkid"))
                    Call cboReviewer_SelectedIndexChanged(New Object, New EventArgs)
                    cboReviewer.Enabled = False
                    cboEmployee.SelectedValue = CInt(Me.ViewState("employeeunkid"))
                    cboEmployee.Enabled = False
                    cboPeriod.SelectedValue = CInt(Me.ViewState("periodid"))
                    cboPeriod.Enabled = False
                    Call cboPeriod_SelectedIndexChanged(cboPeriod, Nothing)
                    Me.ViewState.Add("AssessAnalysisUnkid", -1)
                    If objEAnalysisMst.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , Me.ViewState("assessormasterunkid"), Me.ViewState("AssessAnalysisUnkid"), , , False) = True Then
                        DisplayMessage.DisplayMessage("Sorry, You have already reviewed items. Please open in edit mode and add new items.", Me, "../../Index.aspx")
                        Exit Sub
                    End If
                    HttpContext.Current.Session("Login") = True
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "setlocation", "setlocation()", True)
                    GoTo Link
                End If
            End If

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmPerformanceEvaluation"
            StrModuleName2 = "mnuAssessment"
            StrModuleName3 = "mnuPerformaceEvaluation"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")
            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = Session("Employeeunkid")
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            If IsPostBack = False Then
                txtInstruction.Text = Session("Assessment_Instructions")
                If Session("Action") IsNot Nothing AndAlso Session("Unkid") IsNot Nothing Then
                    mintAssessAnalysisUnkid = Session("Unkid")
                    If CInt(Session("Action")) = 0 Then
                        menAction = enAction.ADD_ONE
                    ElseIf CInt(Session("Action")) = 1 Then
                        menAction = enAction.EDIT_ONE
                    ElseIf CInt(Session("Action")) = 2 Then
                        menAction = enAction.ADD_CONTINUE
                    End If
                Else
                    Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_ReviewerEvaluationList.aspx", False)
                End If
                Call FillCombo()
                If menAction = enAction.EDIT_ONE Then
                    objEAnalysisMst._Analysisunkid = mintAssessAnalysisUnkid
                    cboEmployee.Enabled = False
                    cboPeriod.Enabled = False
                    BtnSearch.Enabled = False
                End If
                Call GetValue()
                'Gajanan [28-May-2020] -- Start
                objlblBSCScrCaption.Visible = CBool(Session("AllowToViewScoreWhileDoingAssesment"))
                objlblBSCScr.Visible = CBool(Session("AllowToViewScoreWhileDoingAssesment"))
                'Gajanan [28-May-2020] -- End

            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "setlocation", "setlocation()", True)
            End If


Link:       'S.SANDEEP |13-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            mintEvaluationTypeId = Session("EvaluationTypeId")
            'S.SANDEEP |13-NOV-2020| -- END

            If Me.ViewState("AssessAnalysisUnkid") IsNot Nothing Then
                mintAssessAnalysisUnkid = Me.ViewState("AssessAnalysisUnkid")
            End If

            Call Add_GridColumns()

            If Me.ViewState("iExOrdr") IsNot Nothing Then
                iExOrdr = Me.ViewState("iExOrdr")
            End If

            If Me.ViewState("iLinkedFieldId") IsNot Nothing Then
                iLinkedFieldId = Me.ViewState("iLinkedFieldId")
            End If

            If Me.ViewState("YearUnkid") IsNot Nothing Then
                mintYearUnkid = Me.ViewState("YearUnkid")
            End If

            If Me.ViewState("iMappingUnkid") IsNot Nothing Then
                iMappingUnkid = Me.ViewState("iMappingUnkid")
            End If

            If Me.ViewState("xTotAssignedWeight") IsNot Nothing Then
                xTotAssignedWeight = Me.ViewState("xTotAssignedWeight")
            End If
            If Me.ViewState("ColIndex") IsNot Nothing Then
                xVal = Me.ViewState("ColIndex")
            End If



            mblnIsMatchCompetencyStructure = Me.ViewState("mblnIsMatchCompetencyStructure")
            mstriEditingGUID = Me.ViewState("mstriEditingGUID")

            'Gajanan [28-May-2020] -- Start
            If Me.ViewState("mdicCustomHeader") IsNot Nothing Then
                mdicCustomHeader = CType(Me.ViewState("mdicCustomHeader"), Dictionary(Of Integer, String))
            End If

            If Me.ViewState("mintCustomHeaderVal") IsNot Nothing Then
                mintCustomHeaderVal = CInt(Me.ViewState("mintCustomHeaderVal"))
            End If

            If Me.ViewState("mintCustomHeaderIndex") IsNot Nothing Then
                mintCustomHeaderIndex = CInt(Me.ViewState("mintCustomHeaderIndex"))
            End If
            'Gajanan [28-May-2020] -- End




            If Me.ViewState("ItemAddEdit") IsNot Nothing Then
                mblnItemAddEdit = Me.ViewState("ItemAddEdit")
                If mblnItemAddEdit Then
                    popup_CItemAddEdit.Show()
                End If
            End If

            If menAction <> enAction.EDIT_ONE Then
                If Session("PaAssessPeriodUnkid") IsNot Nothing AndAlso Session("PaAssessEmpUnkid") IsNot Nothing AndAlso Session("PaAssessMstUnkid") IsNot Nothing Then
                    cboReviewer.SelectedValue = Session("PaAssessMstUnkid")
                    Call cboReviewer_SelectedIndexChanged(cboReviewer, Nothing)
                    cboEmployee.SelectedValue = Session("PaAssessEmpUnkid")
                    cboPeriod.SelectedValue = Session("PaAssessPeriodUnkid")
                    Call cboPeriod_SelectedIndexChanged(cboPeriod, Nothing)
                    dtpAssessdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                    cboReviewer.Enabled = False
                    cboEmployee.Enabled = False
                    cboPeriod.Enabled = False
                    Session.Remove("PaAssessPeriodUnkid") : Session.Remove("PaAssessEmpUnkid") : Session.Remove("PaAssessMstUnkid")
                End If
            End If

            If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNeeded Then
                lnkCopyScore.Visible = True
            End If
            'S.SANDEEP |19-AUG-2020| -- START
            'ISSUE/ENHANCEMENT : Language Changes
            Call SetControlCaptions()
            'S.SANDEEP |19-AUG-2020| -- END
            Call SetLanguage()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Me.ViewState("Action") Is Nothing Then
                Me.ViewState.Add("Action", menAction)
            Else
                Me.ViewState("Action") = menAction
            End If

            If Me.ViewState("AssessAnalysisUnkid") Is Nothing Then
                Me.ViewState.Add("AssessAnalysisUnkid", mintAssessAnalysisUnkid)
            Else
                Me.ViewState("AssessAnalysisUnkid") = mintAssessAnalysisUnkid
            End If
            If Me.ViewState("iExOrdr") Is Nothing Then
                Me.ViewState.Add("iExOrdr", iExOrdr)
            Else
                Me.ViewState("iExOrdr") = iExOrdr
            End If

            If Me.ViewState("iLinkedFieldId") Is Nothing Then
                Me.ViewState.Add("iLinkedFieldId", iLinkedFieldId)
            Else
                Me.ViewState("iLinkedFieldId") = iLinkedFieldId
            End If

            If Me.ViewState("iMappingUnkid") Is Nothing Then
                Me.ViewState.Add("iMappingUnkid", iMappingUnkid)
            Else
                Me.ViewState("iMappingUnkid") = iMappingUnkid
            End If

            If Me.ViewState("xTotAssignedWeight") Is Nothing Then
                Me.ViewState.Add("xTotAssignedWeight", xTotAssignedWeight)
            Else
                Me.ViewState("xTotAssignedWeight") = xTotAssignedWeight
            End If

            If Me.ViewState("ColIndex") Is Nothing Then
                Me.ViewState.Add("ColIndex", xVal)
            Else
                Me.ViewState("ColIndex") = xVal
            End If

            Me.ViewState("mblnIsMatchCompetencyStructure") = mblnIsMatchCompetencyStructure
            Me.ViewState("mstriEditingGUID") = mstriEditingGUID

            If Me.ViewState("ItemAddEdit") Is Nothing Then
                Me.ViewState.Add("ItemAddEdit", mblnItemAddEdit)
            Else
                Me.ViewState("ItemAddEdit") = mblnItemAddEdit
            End If

            'Gajanan [28-May-2020] -- Start
            If Me.ViewState("mdicCustomHeader") IsNot Nothing Then
                Me.ViewState("mdicCustomHeader") = mdicCustomHeader
            End If

            If Me.ViewState("mintCustomHeaderVal") IsNot Nothing Then
                Me.ViewState("mintCustomHeaderVal") = mintCustomHeaderVal
            End If

            If Me.ViewState("mintCustomHeaderIndex") IsNot Nothing Then
                Me.ViewState("mintCustomHeaderIndex") = mintCustomHeaderIndex
            End If


            'Gajanan [28-May-2020] -- End



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Session("Unkid") IsNot Nothing Then
                Session.Remove("Unkid")
            End If
            If Session("Action") IsNot Nothing Then
                Session.Remove("Action")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            If Request.QueryString.Count <= 0 Then
                Me.IsLoginRequired = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Try
            'S.SANDEEP |09-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "APeriod", True, 1)

            Dim blnIsCompetence As Boolean = False
            If Session("EvaluationTypeId") = clsevaluation_analysis_master.enPAEvalTypeId.EO_COMPETENCE Then
                blnIsCompetence = True
            End If
            dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "APeriod", True, 1, False, blnIsCompetence, True) 'IIf(blnIsCompetence = True, True, False)
            'S.SANDEEP |09-FEB-2021| -- END

            Dim intCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)

            'S.SANDEEP |04-JAN-2021| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            'With cboPeriod
            '    .DataValueField = "periodunkid"
            '    .DataTextField = "name"
            '    .DataSource = dsCombos.Tables("APeriod")
            '    .DataBind()
            '    .SelectedValue = 0
            'End With
            Dim dtPeriods As New DataTable
            'If menAction <> enAction.EDIT_ONE Then
            '    If CInt(Session("EvaluationTypeId")) <> clsevaluation_analysis_master.enPAEvalTypeId.EO_BOTH Then
            '        Dim strPids As String = String.Empty
            '        strPids = objEAnalysisMst.GetEvalPeriodIds(CInt(Session("EvaluationTypeId")), Nothing)
            '        If strPids Is Nothing Then strPids = ""
            '        If strPids.Trim.Length > 0 Then
            '            dtPeriods = New DataView(dsCombos.Tables(0), "periodunkid NOT IN (" & strPids & ")", "", DataViewRowState.CurrentRows).ToTable
            '            If strPids.Split(",").AsEnumerable().Where(Function(x) CInt(x) = intCurrentPeriodId).Count > 0 Then
            '                intCurrentPeriodId = CInt(strPids.Split(",")(0))
            '            End If
            '        Else
            '            dtPeriods = dsCombos.Tables("APeriod")
            '        End If
            '    Else
            '        dtPeriods = dsCombos.Tables("APeriod")
            '    End If
            'Else
            '    dtPeriods = dsCombos.Tables("APeriod")
            'End If
            dtPeriods = dsCombos.Tables("APeriod")
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dtPeriods
                .DataBind()
                .SelectedValue = intCurrentPeriodId
            End With
            'S.SANDEEP |04-JAN-2021| -- END


            dsCombos = objEAnalysisMst.getAssessorComboList(Session("Database_Name"), _
                                                            Session("UserId"), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), True, _
                                                            Session("IsIncludeInactiveEmp"), "Assessor", _
                                                            clsAssessor.enARVisibilityTypeId.VISIBLE, False, True)
            With cboReviewer
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("Assessor")
                .DataBind()
            End With
            Call cboReviewer_SelectedIndexChanged(cboReviewer, Nothing)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Public Sub FillCustomHeader(ByVal intPeriodId As Integer)
        Dim dsCombos As New DataSet
        Dim objHeader As New clsassess_custom_header
        Try
            'Gajanan [28-May-2020] -- Start
            'dsCombos = objHeader.getComboList(intPeriodId, True, "List", True)
            'With cboCustomHeader
            '    .DataValueField = "Id"
            '    .DataTextField = "name"
            '    .DataSource = dsCombos.Tables(0)
            '    .SelectedValue = 0
            '    .DataBind()
            'End With

            dsCombos = objHeader.getComboList(intPeriodId, False, "List", True)

            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            If Session("EvaluationTypeId") <> clsevaluation_analysis_master.enPAEvalTypeId.EO_COMPETENCE Then
                If CBool(Session("IsPDP_Perf_Integrated")) Then
                    Dim objPDPForm As New clspdpform_master
                    If objPDPForm.IsPDPFormExists(CInt(cboEmployee.SelectedValue), 0, 0) = True Then
                        Dim objPDPCategory As New clspdpcategory_master
                        Dim dsC As New DataSet
                        dsC = objPDPCategory.GetList("List", True, "pdpcategory_master.isincludeinpm = 1", True)
                        If dsC IsNot Nothing AndAlso dsC.Tables.Count > 0 Then
                            For Each iR As DataRow In dsC.Tables(0).Rows
                                Dim xR As DataRow = dsCombos.Tables(0).NewRow
                                xR("Id") = iR("Id")
                                xR("Name") = iR("category")
                                xR("periodunkid") = intPeriodId
                                xR("isinclude_planning") = False
                                dsCombos.Tables(0).Rows.Add(xR)
                            Next
                        End If
                    End If
                End If
            End If
            'S.SANDEEP |03-MAY-2021| -- END

            For Each row As DataRow In dsCombos.Tables(0).Rows
                If mdicCustomHeader.ContainsKey(CInt(row("Id"))) = False Then
                    mdicCustomHeader.Add(CInt(row("Id")), row("name"))
                End If
            Next
            If mdicCustomHeader.Keys.Count > 0 Then
                mintCustomHeaderVal = mdicCustomHeader.Keys.ElementAt(0)
                lblCustomHeaderval.Text = mdicCustomHeader.Values.ElementAt(0)
                mintCustomHeaderIndex = 0

                ViewState("mdicCustomHeader") = mdicCustomHeader
                ViewState("mintCustomHeaderVal") = mintCustomHeaderVal
                ViewState("mintCustomHeaderIndex") = mintCustomHeaderIndex

                setNextPrevious()
            Else
                lnkNext.Visible = False
                lnkprevious.Visible = False
                cItem.Visible = False
            End If



            'Gajanan [28-May-2020] -- End



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objHeader = Nothing
        End Try
    End Sub

    Private Sub Fill_Custom_Grid(ByVal intCustomHeaderId As Integer)
        Try
            Dim dtCustomTabularGrid As New DataTable
            Dim objCHdr As New clsassess_custom_header
            objCHdr._Customheaderunkid = intCustomHeaderId

            mblnIsMatchCompetencyStructure = objCHdr._IsMatch_With_Competency
            objCHdr = Nothing
            If Session("CompanyGroupName") = "NMB PLC" Then
                dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), intCustomHeaderId, mintAssessAnalysisUnkid, enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), enAction.EDIT_ONE)
            Else
                dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), intCustomHeaderId, mintAssessAnalysisUnkid, enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), enAction.EDIT_ONE)
            End If

            If intCustomHeaderId.ToString().StartsWith("9000") = False Then
                Dim iSet As New DataSet
                Dim objItems As New clsassess_custom_items
                iSet = objItems.getComboList(CInt(cboPeriod.SelectedValue), intCustomHeaderId, False, "List", enAssessmentMode.APPRAISER_ASSESSMENT)
                If iSet.Tables.Count > 0 AndAlso iSet.Tables(0).Rows.Count > 0 Then
                    If dtCustomTabularGrid.Rows.Count > 0 Then
                        If IsDBNull(iSet.Tables(0).Compute("MAX(viewmodeid)", "")) = False Then
                            dtCustomTabularGrid.Rows(0)("viewmodeid") = CInt(iSet.Tables(0).Compute("MAX(viewmodeid)", ""))
                        End If
                    End If
                End If
            Else
                If dtCustomTabularGrid.Rows.Count > 0 Then
                    dtCustomTabularGrid.Rows(0)("viewmodeid") = CInt(enAssessmentMode.REVIEWER_ASSESSMENT)
                End If
            End If

            If dtCustomTabularGrid.Rows.Count > 0 Then
                dgvItems.Columns.Clear()
            End If

            Call Add_GridColumns()
            dgvItems.AutoGenerateColumns = False
            Dim iColName As String = String.Empty
            For Each dCol As DataColumn In dtCustomTabularGrid.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                Dim dgvCol As New BoundField()
                dgvCol.FooterText = iColName
                dgvCol.ReadOnly = True
                dgvCol.DataField = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                If dgvItems.Columns.Contains(dgvCol) = True Then Continue For
                If dCol.Caption.Length <= 0 Then
                    dgvCol.Visible = False
                End If
                dgvItems.Columns.Add(dgvCol)
                If dgvCol.FooterText = "objHeader_Name" Then dgvCol.Visible = False
            Next
            Call SetDateFormat()
            dgvItems.DataSource = dtCustomTabularGrid
            dgvItems.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Add_GridColumns()
        Try
            '************* ADD
            Dim iTempField As New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "ObjAdd"
            iTempField.HeaderText = "Add"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            If dgvItems.Columns.Count > 0 Then
                If dgvItems.Columns(0).FooterText <> iTempField.FooterText Then
                    dgvItems.Columns.Add(iTempField)
                End If
            Else
                dgvItems.Columns.Add(iTempField)
            End If

            '************* EDIT
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "objEdit"
            iTempField.HeaderText = "Edit"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            If dgvItems.Columns.Count > 1 Then
                If dgvItems.Columns(1).FooterText <> iTempField.FooterText Then
                    dgvItems.Columns.Add(iTempField)
                End If
            Else
                dgvItems.Columns.Add(iTempField)
            End If

            '************* DELETE
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "objDelete"
            iTempField.HeaderText = "Delete"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            If dgvItems.Columns.Count > 2 Then
                If dgvItems.Columns(2).FooterText <> iTempField.FooterText Then
                    dgvItems.Columns.Add(iTempField)
                End If
            Else
                dgvItems.Columns.Add(iTempField)
            End If

            'S.SANDEEP |03-SEP-2020| -- START
            'ISSUE/ENHANCEMENT : OTHER COMPANY HAS ISSUES WITH NMB SETTING
            'For Each gRow As GridViewRow In dgvItems.Rows
            '    If gRow.Cells(0).FindControl("imgAdd") Is Nothing Then
            '        Dim imgAdd As New ImageButton()
            '        imgAdd.ID = "imgAdd"
            '        imgAdd.Attributes.Add("Class", "objAddBtn")
            '        imgAdd.CommandName = "objAdd"
            '        imgAdd.ImageUrl = "~/images/add_16.png"
            '        imgAdd.ToolTip = "New"
            '        RemoveHandler imgAdd.Click, AddressOf imgAdd_Click
            '        AddHandler imgAdd.Click, AddressOf imgAdd_Click
            '        gRow.Cells(0).Controls.Add(imgAdd)
            '    End If

            '    If gRow.Cells(1).FindControl("imgEdit") Is Nothing Then
            '        Dim imgEdit As New ImageButton()
            '        imgEdit.ID = "imgEdit"
            '        imgEdit.Attributes.Add("Class", "objAddBtn")
            '        imgEdit.CommandName = "objEdit"
            '        imgEdit.ImageUrl = "~/images/Edit.png"
            '        imgEdit.ToolTip = "Edit"
            '        AddHandler imgEdit.Click, AddressOf imgEdit_Click
            '        gRow.Cells(1).Controls.Add(imgEdit)
            '    End If

            '    If gRow.Cells(2).FindControl("imgDelete") Is Nothing Then
            '        Dim imgDelete As New ImageButton()
            '        imgDelete.ID = "imgDelete"
            '        imgDelete.Attributes.Add("Class", "objAddBtn")
            '        imgDelete.CommandName = "objDelete"
            '        imgDelete.ImageUrl = "~/images/remove.png"
            '        imgDelete.ToolTip = "Delete"
            '        AddHandler imgDelete.Click, AddressOf imgDelete_Click
            '        gRow.Cells(2).Controls.Add(imgDelete)
            '    End If
            'Next
            If Session("CompanyGroupName") = "NMB PLC" Then
                For Each gRow As GridViewRow In dgvItems.Rows
                    If CInt(dgvItems.DataKeys(gRow.RowIndex)("viewmodeid")) = 3 AndAlso gRow.Cells(0).FindControl("imgAdd") Is Nothing Then
                        'Dim imgAdd As New ImageButton()
                        'imgAdd.ID = "imgAdd"
                        'imgAdd.Attributes.Add("Class", "objAddBtn")
                        'imgAdd.CommandName = "objAdd"
                        'imgAdd.ImageUrl = "~/images/add_16.png"
                        'imgAdd.ToolTip = "New"
                        'RemoveHandler imgAdd.Click, AddressOf imgAdd_Click
                        'AddHandler imgAdd.Click, AddressOf imgAdd_Click
                        'gRow.Cells(0).Controls.Add(imgAdd)

                        Dim imgAdd As New LinkButton
                        imgAdd.ID = "imgAdd"
                        'imgAdd.Attributes.Add("Class", "objAddBtn")
                        imgAdd.CommandName = "objAdd"
                        'imgAdd.ImageUrl = "~/images/add_16.png"
                        imgAdd.ToolTip = "New"
                        imgAdd.CssClass = "lnAdd"
                        'AddHandler imgAdd.Click, AddressOf imgAdd_Click
                        gRow.Cells(0).Controls.Add(imgAdd)

                    End If

                    If CInt(dgvItems.DataKeys(gRow.RowIndex)("viewmodeid")) = 3 AndAlso gRow.Cells(1).FindControl("imgEdit") Is Nothing Then
                        'Dim imgEdit As New ImageButton()
                        'imgEdit.ID = "imgEdit"
                        'imgEdit.Attributes.Add("Class", "objAddBtn")
                        'imgEdit.CommandName = "objEdit"
                        'imgEdit.ImageUrl = "~/images/Edit.png"
                        'imgEdit.ToolTip = "Edit"
                        'AddHandler imgEdit.Click, AddressOf imgEdit_Click
                        'gRow.Cells(1).Controls.Add(imgEdit)

                        Dim imgEdit As New LinkButton
                        imgEdit.ID = "imgEdit"
                        'imgEdit.Attributes.Add("Class", "objAddBtn")
                        imgEdit.CommandName = "objEdit"
                        'imgEdit.ImageUrl = "~/images/Edit.png"
                        imgEdit.ToolTip = "Edit"
                        imgEdit.CssClass = "lnEdit"
                        'AddHandler imgEdit.Click, AddressOf imgEdit_Click
                        gRow.Cells(1).Controls.Add(imgEdit)
                    End If

                    If CInt(dgvItems.DataKeys(gRow.RowIndex)("viewmodeid")) = 3 AndAlso gRow.Cells(2).FindControl("imgDelete") Is Nothing Then
                        'Dim imgDelete As New ImageButton()
                        'imgDelete.ID = "imgDelete"
                        'imgDelete.Attributes.Add("Class", "objAddBtn")
                        'imgDelete.CommandName = "objDelete"
                        'imgDelete.ImageUrl = "~/images/remove.png"
                        'imgDelete.ToolTip = "Delete"
                        'AddHandler imgDelete.Click, AddressOf imgDelete_Click
                        'gRow.Cells(2).Controls.Add(imgDelete)

                        Dim imgDelete As New LinkButton
                        imgDelete.ID = "imgDelete"
                        'imgDelete.Attributes.Add("Class", "objAddBtn")
                        imgDelete.CommandName = "objDelete"
                        'imgDelete.ImageUrl = "~/images/remove.png"
                        imgDelete.ToolTip = "Delete"
                        imgDelete.CssClass = "lnDelt"
                        'AddHandler imgDelete.Click, AddressOf imgDelete_Click
                        gRow.Cells(2).Controls.Add(imgDelete)
                    End If
                Next
            Else
                For Each gRow As GridViewRow In dgvItems.Rows
                    If (CInt(dgvItems.DataKeys(gRow.RowIndex)("viewmodeid")) <= 1 Or CInt(dgvItems.DataKeys(gRow.RowIndex)("viewmodeid")) = 3) AndAlso gRow.Cells(0).FindControl("imgAdd") Is Nothing Then
                        'Dim imgAdd As New ImageButton()
                        'imgAdd.ID = "imgAdd"
                        'imgAdd.Attributes.Add("Class", "objAddBtn")
                        'imgAdd.CommandName = "objAdd"
                        'imgAdd.ImageUrl = "~/images/add_16.png"
                        'imgAdd.ToolTip = "New"
                        'RemoveHandler imgAdd.Click, AddressOf imgAdd_Click
                        'AddHandler imgAdd.Click, AddressOf imgAdd_Click
                        'gRow.Cells(0).Controls.Add(imgAdd)

                        Dim imgAdd As New LinkButton
                        imgAdd.ID = "imgAdd"
                        'imgAdd.Attributes.Add("Class", "objAddBtn")
                        imgAdd.CommandName = "objAdd"
                        'imgAdd.ImageUrl = "~/images/add_16.png"
                        imgAdd.ToolTip = "New"
                        imgAdd.CssClass = "lnAdd"
                        'AddHandler imgAdd.Click, AddressOf imgAdd_Click
                        gRow.Cells(0).Controls.Add(imgAdd)
                    End If

                    If (CInt(dgvItems.DataKeys(gRow.RowIndex)("viewmodeid")) <= 1 Or CInt(dgvItems.DataKeys(gRow.RowIndex)("viewmodeid")) = 3) AndAlso gRow.Cells(1).FindControl("imgEdit") Is Nothing Then
                        If CInt(dgvItems.DataKeys(gRow.RowIndex)("analysisunkid")) = mintAssessAnalysisUnkid Then
                            'Dim imgEdit As New ImageButton()
                            'imgEdit.ID = "imgEdit"
                            'imgEdit.Attributes.Add("Class", "objAddBtn")
                            'imgEdit.CommandName = "objEdit"
                            'imgEdit.ImageUrl = "~/images/Edit.png"
                            'imgEdit.ToolTip = "Edit"
                            'AddHandler imgEdit.Click, AddressOf imgEdit_Click
                            'gRow.Cells(1).Controls.Add(imgEdit)

                            Dim imgEdit As New LinkButton
                            imgEdit.ID = "imgEdit"
                            'imgEdit.Attributes.Add("Class", "objAddBtn")
                            imgEdit.CommandName = "objEdit"
                            'imgEdit.ImageUrl = "~/images/Edit.png"
                            imgEdit.ToolTip = "Edit"
                            imgEdit.CssClass = "lnEdit"
                            'AddHandler imgEdit.Click, AddressOf imgEdit_Click
                            gRow.Cells(1).Controls.Add(imgEdit)
                        End If
                    End If

                    If (CInt(dgvItems.DataKeys(gRow.RowIndex)("viewmodeid")) <= 1 Or CInt(dgvItems.DataKeys(gRow.RowIndex)("viewmodeid")) = 3) AndAlso gRow.Cells(2).FindControl("imgDelete") Is Nothing Then
                        If CInt(dgvItems.DataKeys(gRow.RowIndex)("analysisunkid")) = mintAssessAnalysisUnkid Then
                            'Dim imgDelete As New ImageButton()
                            'imgDelete.ID = "imgDelete"
                            'imgDelete.Attributes.Add("Class", "objAddBtn")
                            'imgDelete.CommandName = "objDelete"
                            'imgDelete.ImageUrl = "~/images/remove.png"
                            'imgDelete.ToolTip = "Delete"
                            'AddHandler imgDelete.Click, AddressOf imgDelete_Click
                            'gRow.Cells(2).Controls.Add(imgDelete)

                            Dim imgDelete As New LinkButton
                            imgDelete.ID = "imgDelete"
                            'imgDelete.Attributes.Add("Class", "objAddBtn")
                            imgDelete.CommandName = "objDelete"
                            'imgDelete.ImageUrl = "~/images/remove.png"
                            imgDelete.ToolTip = "Delete"
                            imgDelete.CssClass = "lnDelt"
                            'AddHandler imgDelete.Click, AddressOf imgDelete_Click
                            gRow.Cells(2).Controls.Add(imgDelete)
                        End If
                    End If
                Next
            End If
            'S.SANDEEP |03-SEP-2020| -- START

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Generate_Popup_Data(ByVal xHeaderUnkid As Integer, ByVal mstriEditingGUID As String, ByVal intRowIndex As Integer, ByVal xHeaderName As String)
        Dim dtCItems As DataTable
        Try
            If Session("CompanyGroupName") = "NMB PLC" Then
                dtCItems = objEAnalysisMst.Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid, enAssessmentMode.REVIEWER_ASSESSMENT, mstriEditingGUID, True)
            Else
                dtCItems = objEAnalysisMst.Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid, enAssessmentMode.REVIEWER_ASSESSMENT, mstriEditingGUID, False)
            End If
            If dtCItems IsNot Nothing AndAlso dtCItems.Rows.Count <= 0 Then Exit Sub
            dtCItems = dtCItems.Select("", "isdefaultentry DESC").CopyToDataTable
            dgv_Citems.AutoGenerateColumns = False
            dgv_Citems.DataSource = dtCItems
            dgv_Citems.DataBind()
            mblnItemAddEdit = True
            lblpopupHeader.Text = xHeaderName
            popup_CItemAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function Is_Already_Assessed() As Boolean
        Try
            If objEAnalysisMst.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboReviewer.SelectedValue), , mintAssessAnalysisUnkid, , , False) = True Then
                If Request.QueryString.Count > 0 Then
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), Me, "../../Index.aspx")
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 12, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), Me, "../../Index.aspx")
                    'Pinkal (25-Jan-2022) -- End
                Else
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), Me, "wPg_ReviewerEvaluationList.aspx")
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 12, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), Me, "wPg_ReviewerEvaluationList.aspx")
                    'Pinkal (25-Jan-2022) -- End
                End If
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub SetScore()
        Try
            Dim dtComputeScore As DataTable
            dtComputeScore = (New clsComputeScore_master).GetComputeScore(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), clsComputeScore_master.enAssessMode.REVIEWER_ASSESSMENT, False)
            If dtComputeScore IsNot Nothing AndAlso dtComputeScore.Rows.Count > 0 Then
                Dim xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE _
                                                                         And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
                                                         .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                If xScore.Count > 0 Then
                    objlblBSCScr.Text = Format(CDec(xScore(0)), "#######################0.#0")
                End If
            End If

            If dtComputeScore IsNot Nothing AndAlso dtComputeScore.Rows.Count > 0 Then
                Dim xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE _
                                                                         And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
                                                         .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                If xScore.Count > 0 Then
                    objlblGEScr.Text = Format(CDec(xScore(0)), "#######################0.#0")
                End If
            End If

            dtComputeScore = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            cboReviewer.SelectedValue = objEAnalysisMst._Assessormasterunkid
            If cboReviewer.SelectedValue > 0 Then
                Call cboReviewer_SelectedIndexChanged(cboReviewer, Nothing)
            End If
            cboEmployee.SelectedValue = objEAnalysisMst._Assessedemployeeunkid
            cboPeriod.SelectedValue = objEAnalysisMst._Periodunkid
            If menAction = enAction.EDIT_ONE Then
                Call cboPeriod_SelectedIndexChanged(cboPeriod, Nothing)
            End If
            If objEAnalysisMst._Assessmentdate <> Nothing Then
                dtpAssessdate.SetDate = objEAnalysisMst._Assessmentdate
            Else
                dtpAssessdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            mintAssessAnalysisUnkid = objEAnalysisMst.GetAnalusisUnkid(CInt(cboEmployee.SelectedValue), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
            objEAnalysisMst._Analysisunkid = mintAssessAnalysisUnkid
            objEAnalysisMst._Periodunkid = CInt(cboPeriod.SelectedValue)
            objEAnalysisMst._Selfemployeeunkid = -1
            objEAnalysisMst._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
            objEAnalysisMst._Assessormasterunkid = CInt(cboReviewer.SelectedValue)
            Dim intEmployeeId As Integer = -1
            intEmployeeId = objEAnalysisMst.GetAssessorEmpId(CInt(cboReviewer.SelectedValue))
            objEAnalysisMst._Assessoremployeeunkid = intEmployeeId
            objEAnalysisMst._Reviewerunkid = -1
            objEAnalysisMst._Assessmodeid = enAssessmentMode.REVIEWER_ASSESSMENT
            objEAnalysisMst._Assessmentdate = dtpAssessdate.GetDate
            objEAnalysisMst._Userunkid = Session("UserId")
            If objEAnalysisMst._Committeddatetime <> Nothing Then
                objEAnalysisMst._Committeddatetime = objEAnalysisMst._Committeddatetime
            Else
                objEAnalysisMst._Committeddatetime = Nothing
            End If
            objEAnalysisMst._Isvoid = False
            objEAnalysisMst._Voiduserunkid = -1
            objEAnalysisMst._Voiddatetime = Nothing
            objEAnalysisMst._Voidreason = ""
            'S.SANDEEP |13-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            objEAnalysisMst._EvalTypeId = mintEvaluationTypeId
            'S.SANDEEP |13-NOV-2020| -- END

            'S.SANDEEP |03-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : PDP_PM_LINKING
            objEAnalysisMst._ClientIP = CStr(Session("IP_ADD"))
            objEAnalysisMst._FormName = mstrModuleName
            objEAnalysisMst._FromWeb = True
            objEAnalysisMst._HostName = CStr(Session("HOST_NAME"))
            objEAnalysisMst._DatabaseName = CStr(Session("Database_Name"))
            objEAnalysisMst._LoginEmployeeUnkid = CInt(cboEmployee.SelectedValue)
            'S.SANDEEP |03-MAY-2021| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetBSC_GridCols_Tags()
        Try
            Dim iEval() As String = Nothing
            If CStr(Session("ViewTitles_InEvaluation")).Trim.Length > 0 Then
                iEval = CStr(Session("ViewTitles_InEvaluation")).Split("|")
            Else
                Dim iTab As New DataTable : Dim iChkItemIds As String = ""
                Dim objConfig As New clsConfigOptions
                objConfig._Companyunkid = Session("Companyunkid")
                iTab = (New clsAssess_Field_Master).GetFieldsForViewSetting()
                Dim selectedTags = (From p In iTab.AsEnumerable() Select (p.Item("Id").ToString)).ToList
                iChkItemIds = String.Join("|", selectedTags.ToArray())
                objConfig._ViewTitles_InEvaluation = iChkItemIds

                selectedTags = (From p In iTab.AsEnumerable() Select (p.Item("EW").ToString)).ToList
                iChkItemIds = String.Join("|", selectedTags.ToArray())
                objConfig._ColWidth_InPlanning = iChkItemIds
                objConfig.updateParam()
                Session("ViewTitles_InEvaluation") = objConfig._ViewTitles_InEvaluation
                objConfig = Nothing
                iEval = CStr(Session("ViewTitles_InEvaluation")).Split("|")
            End If

            Dim objFMst As New clsAssess_Field_Master
            Dim dFld As New DataSet : dFld.Tables.Add(objFMst.GetFieldsForViewSetting())
            If dFld.Tables(0).Rows.Count > 0 Then
                Dim xCol As DataGridColumn = Nothing
                Dim iExOrder As Integer = -1
                For Each xRow As DataRow In dFld.Tables(0).Rows
                    iExOrder = -1
                    iExOrder = objFMst.Get_Field_ExOrder(xRow.Item("Id"))
                    xCol = Nothing
                    Select Case iExOrder
                        Case 1
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField1"))
                        Case 2
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField2"))
                        Case 3
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField3"))
                        Case 4
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField4"))
                        Case 5
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField5"))
                        Case 6
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField6"))
                        Case 7
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField7"))
                        Case 8
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField8"))
                    End Select
                    Select Case xRow.Item("Id")
                        Case clsAssess_Field_Master.enOtherInfoField.ST_DATE
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhSDate"))
                        Case clsAssess_Field_Master.enOtherInfoField.ED_DATE
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhEDate"))
                        Case clsAssess_Field_Master.enOtherInfoField.PCT_COMPLETE
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhCompleted"))
                        Case clsAssess_Field_Master.enOtherInfoField.STATUS
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhStatus"))
                        Case clsAssess_Field_Master.enOtherInfoField.WEIGHT
                            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
                                'xCol = dgvBSC.Columns(disBSCColumns("dgcolhBSCScore"))
                            Else
                                xCol = dgvBSC.Columns(disBSCColumns("dgcolhBSCWeight"))
                            End If
                        Case clsAssess_Field_Master.enOtherInfoField.SCORE
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolheselfBSC"))
                        Case clsAssess_Field_Master.enOtherInfoField.GOAL_TYPE
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhGoalType"))
                        Case clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE
                            xCol = dgvBSC.Columns(disBSCColumns("dgoalvalue"))
                    End Select
                    If xCol IsNot Nothing Then
                        xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                        xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                        If xRow.Item("Id") = clsAssess_Field_Master.enOtherInfoField.SCORE Then
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhaselfBSC"))
                            xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                            xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))

                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhrselfBSC"))
                            xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                            xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                        End If
                    End If

                    If iEval IsNot Nothing Then
                        If xCol IsNot Nothing Then
                            If Array.IndexOf(iEval, xRow.Item("Id").ToString()) < 0 Then
                                xCol.Visible = False
                            End If
                        End If
                    Else
                        If xCol IsNot Nothing Then xCol.Visible = False
                    End If
                Next
            End If
            objFMst = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_BSC_Evaluation()
        Dim dtBSC_TabularGrid As DataTable
        Try
            'S.SANDEEP |17-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES            
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")), Session("Database_Name").ToString, , Session("fmtCurrency"))
            dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")), Session("Database_Name").ToString, Session("DontAllowRatingBeyond100"), , Session("fmtCurrency"))
            'S.SANDEEP |17-MAY-2021| -- END
            Call SetBSC_GridCols_Tags()

            Dim iCol As New DataColumn
            With iCol
                .DataType = GetType(Int32)
                .ColumnName = "LinkedFieldId"
                .DefaultValue = iLinkedFieldId
            End With
            dtBSC_TabularGrid.Columns.Add(iCol)

            If dgvBSC.Columns(disBSCColumns("objdgcolhBSCField1")).Visible Then
                If dtBSC_TabularGrid.Columns.Contains("Field1") Then
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField1")).HeaderText = dtBSC_TabularGrid.Columns("Field1").Caption
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField1")).Visible = True
                Else
                    dtBSC_TabularGrid.Columns.Add("Field1", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField1")).Visible = False
                End If
            Else
                If dtBSC_TabularGrid.Columns.Contains("Field1") = False Then
                    dtBSC_TabularGrid.Columns.Add("Field1", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField1")).Visible = False
                End If
            End If

            If dgvBSC.Columns(disBSCColumns("objdgcolhBSCField2")).Visible Then
                If dtBSC_TabularGrid.Columns.Contains("Field2") Then
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField2")).HeaderText = dtBSC_TabularGrid.Columns("Field2").Caption
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField2")).Visible = True
                Else
                    dtBSC_TabularGrid.Columns.Add("Field2", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField2")).Visible = False
                End If
            Else
                If dtBSC_TabularGrid.Columns.Contains("Field2") = False Then
                    dtBSC_TabularGrid.Columns.Add("Field2", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField2")).Visible = False
                End If
            End If

            If dgvBSC.Columns(disBSCColumns("objdgcolhBSCField3")).Visible Then
                If dtBSC_TabularGrid.Columns.Contains("Field3") Then
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField3")).HeaderText = dtBSC_TabularGrid.Columns("Field3").Caption
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField3")).Visible = True
                Else
                    dtBSC_TabularGrid.Columns.Add("Field3", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField3")).Visible = False
                End If
            Else
                If dtBSC_TabularGrid.Columns.Contains("Field3") = False Then
                    dtBSC_TabularGrid.Columns.Add("Field3", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField3")).Visible = False
                End If
            End If

            If dgvBSC.Columns(disBSCColumns("objdgcolhBSCField4")).Visible Then
                If dtBSC_TabularGrid.Columns.Contains("Field4") Then
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField4")).HeaderText = dtBSC_TabularGrid.Columns("Field4").Caption
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField4")).Visible = True
                Else
                    dtBSC_TabularGrid.Columns.Add("Field4", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField4")).Visible = False
                End If
            Else
                If dtBSC_TabularGrid.Columns.Contains("Field4") = False Then
                    dtBSC_TabularGrid.Columns.Add("Field4", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField4")).Visible = False
                End If
            End If

            If dgvBSC.Columns(disBSCColumns("objdgcolhBSCField5")).Visible Then
                If dtBSC_TabularGrid.Columns.Contains("Field5") Then
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField5")).HeaderText = dtBSC_TabularGrid.Columns("Field5").Caption
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField5")).Visible = True
                Else
                    dtBSC_TabularGrid.Columns.Add("Field5", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField5")).Visible = False
                End If
            Else
                If dtBSC_TabularGrid.Columns.Contains("Field5") = False Then
                    dtBSC_TabularGrid.Columns.Add("Field5", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField5")).Visible = False
                End If
            End If

            If dgvBSC.Columns(disBSCColumns("objdgcolhBSCField6")).Visible Then
                If dtBSC_TabularGrid.Columns.Contains("Field6") Then
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField6")).HeaderText = dtBSC_TabularGrid.Columns("Field6").Caption
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField6")).Visible = True
                Else
                    dtBSC_TabularGrid.Columns.Add("Field6", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField6")).Visible = False
                End If
            Else
                If dtBSC_TabularGrid.Columns.Contains("Field6") = False Then
                    dtBSC_TabularGrid.Columns.Add("Field6", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField6")).Visible = False
                End If
            End If

            If dgvBSC.Columns(disBSCColumns("objdgcolhBSCField7")).Visible Then
                If dtBSC_TabularGrid.Columns.Contains("Field7") Then
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField7")).HeaderText = dtBSC_TabularGrid.Columns("Field7").Caption
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField7")).Visible = True
                Else
                    dtBSC_TabularGrid.Columns.Add("Field7", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField7")).Visible = False
                End If
            Else
                If dtBSC_TabularGrid.Columns.Contains("Field7") = False Then
                    dtBSC_TabularGrid.Columns.Add("Field7", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField7")).Visible = False
                End If
            End If

            If dgvBSC.Columns(disBSCColumns("objdgcolhBSCField8")).Visible Then
                If dtBSC_TabularGrid.Columns.Contains("Field8") Then
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField8")).HeaderText = dtBSC_TabularGrid.Columns("Field8").Caption
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField8")).Visible = True
                Else
                    dtBSC_TabularGrid.Columns.Add("Field8", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField8")).Visible = False
                End If
            Else
                If dtBSC_TabularGrid.Columns.Contains("Field8") = False Then
                    dtBSC_TabularGrid.Columns.Add("Field8", System.Type.GetType("System.String"))
                    dgvBSC.Columns(disBSCColumns("objdgcolhBSCField8")).Visible = False
                End If
            End If

            dgvBSC.Columns(disBSCColumns("dgcolheselfBSC")).HeaderText = dtBSC_TabularGrid.Columns("eself").Caption
            dgvBSC.Columns(disBSCColumns("dgcolheremarkBSC")).HeaderText = dtBSC_TabularGrid.Columns("eremark").Caption

            If dtBSC_TabularGrid.Columns.Contains("aself") Then
                dgvBSC.Columns(disBSCColumns("dgcolhaselfBSC")).HeaderText = dtBSC_TabularGrid.Columns("aself").Caption
            End If
            If dtBSC_TabularGrid.Columns.Contains("aremark") Then
                dgvBSC.Columns(disBSCColumns("dgcolharemarkBSC")).HeaderText = dtBSC_TabularGrid.Columns("aremark").Caption
            End If
            If dtBSC_TabularGrid.Columns.Contains("rself") Then
                dgvBSC.Columns(disBSCColumns("dgcolhrselfBSC")).HeaderText = dtBSC_TabularGrid.Columns("rself").Caption
            End If
            If dtBSC_TabularGrid.Columns.Contains("rremark") Then
                dgvBSC.Columns(disBSCColumns("dgcolhrremarkBSC")).HeaderText = dtBSC_TabularGrid.Columns("rremark").Caption
            End If

            'If dtBSC_TabularGrid.Columns.Contains("Score") Then
            '    If dgvBSC.Columns(disBSCColumns("dgcolhBSCScore")).Visible Then
            '        dgvBSC.Columns(disBSCColumns("dgcolhBSCScore")).HeaderText = dtBSC_TabularGrid.Columns("Score").Caption
            '    End If
            'End If

            If dgvBSC.Columns(disBSCColumns("objdgcolhBSCField1")).Visible = True Then
                xVal = 1
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField2")).Visible = True Then
                xVal = 2
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField3")).Visible = True Then
                xVal = 3
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField4")).Visible = True Then
                xVal = 4
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField5")).Visible = True Then
                xVal = 5
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField6")).Visible = True Then
                xVal = 6
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField7")).Visible = True Then
                xVal = 7
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField8")).Visible = True Then
                xVal = 8
            End If

            Dim xWidth As Integer = 0
            Dim objFMst As New clsAssess_Field_Master

            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK, Session("Companyunkid"))

            If xWidth > 0 Then
                dgvBSC.Columns(disBSCColumns("dgcolheremarkBSC")).HeaderStyle.Width = Unit.Pixel(xWidth)
                dgvBSC.Columns(disBSCColumns("dgcolheremarkBSC")).ItemStyle.Width = Unit.Pixel(xWidth)
            End If

            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK, Session("Companyunkid"))

            If xWidth > 0 Then
                dgvBSC.Columns(disBSCColumns("dgcolharemarkBSC")).HeaderStyle.Width = Unit.Pixel(xWidth)
                dgvBSC.Columns(disBSCColumns("dgcolharemarkBSC")).ItemStyle.Width = Unit.Pixel(xWidth)
            End If

            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.REV_REMARK, Session("Companyunkid"))
            If xWidth > 0 Then
                dgvBSC.Columns(disBSCColumns("dgcolharemarkBSC")).HeaderStyle.Width = Unit.Pixel(xWidth)
                dgvBSC.Columns(disBSCColumns("dgcolharemarkBSC")).ItemStyle.Width = Unit.Pixel(xWidth)
            End If
            objFMst = Nothing

            Dim intTotalWidth As Integer = 0
            For Each xCol As DataGridColumn In dgvBSC.Columns
                If xCol.Visible Then
                    intTotalWidth += xCol.HeaderStyle.Width.Value
                End If
            Next

            If CBool(Session("IsUseAgreedScore")) Then
                dgvBSC.Columns(disBSCColumns("dgcolhAgreedScoreBSC")).Visible = True
            Else
                dgvBSC.Columns(disBSCColumns("dgcolhAgreedScoreBSC")).Visible = False
            End If

            If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNotNecessary Then
                dgvBSC.Columns(disBSCColumns("dgcolhrselfBSC")).Visible = False
            ElseIf ConfigParameter._Object._ReviewerScoreSetting = enReviewerScoreSetting.ReviewerScoreNeeded Then
                dgvBSC.Columns(disBSCColumns("dgcolhrselfBSC")).Visible = True
            End If

            dgvBSC.Width = Unit.Pixel(intTotalWidth)
            dgvBSC.DataSource = dtBSC_TabularGrid
            dgvBSC.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_GE_Evaluation()
        Dim dtGE_TabularGrid As DataTable
        Try
            dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), Session("ConsiderItemWeightAsNumber"), -1, Session("Self_Assign_Competencies"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("Database_Name").ToString())

            dgvGE.AutoGenerateColumns = False

            dgvGE.Columns(disGEColumns("objdgcolhedisplayGE")).Visible = CBool(Session("ConsiderItemWeightAsNumber"))     'SELF FINAL SCORE
            dgvGE.Columns(disGEColumns("objdgcolhadisplayGE")).Visible = CBool(Session("ConsiderItemWeightAsNumber"))     'ASSESSOR FINAL SCORE
            dgvGE.Columns(disGEColumns("objdgcolhrdisplayGE")).Visible = CBool(Session("ConsiderItemWeightAsNumber"))    'REVIEWER FINAL SCORE

            dgvGE.Columns(disGEColumns("dgcolheselfGE")).HeaderText = dtGE_TabularGrid.Columns("eself").Caption
            dgvGE.Columns(disGEColumns("dgcolheremarkGE")).HeaderText = dtGE_TabularGrid.Columns("eremark").Caption
            dgvGE.Columns(disGEColumns("dgcolhaselfGE")).HeaderText = dtGE_TabularGrid.Columns("aself").Caption
            dgvGE.Columns(disGEColumns("dgcolharemarkGE")).HeaderText = dtGE_TabularGrid.Columns("aremark").Caption
            dgvGE.Columns(disGEColumns("dgcolhrselfGE")).HeaderText = dtGE_TabularGrid.Columns("rself").Caption
            dgvGE.Columns(disGEColumns("dgcolhrremarkGE")).HeaderText = dtGE_TabularGrid.Columns("rremark").Caption

            'If dtGE_TabularGrid.Columns.Contains("Score") Then
            '    If dgvGE.Columns(disGEColumns("dgcolhGEScore")).Visible Then
            '        dgvGE.Columns(disGEColumns("dgcolhGEScore")).HeaderText = dtGE_TabularGrid.Columns("Score").Caption
            '    End If
            'End If

            Dim xWidth As Integer = 0
            Dim objFMst As New clsAssess_Field_Master
            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK, Session("CompanyUnkid"))
            If xWidth > 0 Then dgvGE.Columns(disGEColumns("dgcolheremarkGE")).ItemStyle.Width = xWidth
            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK, Session("CompanyUnkid"))
            If xWidth > 0 Then dgvGE.Columns(disGEColumns("dgcolharemarkGE")).ItemStyle.Width = xWidth
            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.REV_REMARK, Session("CompanyUnkid"))
            If xWidth > 0 Then dgvGE.Columns(disGEColumns("dgcolhrremarkGE")).ItemStyle.Width = xWidth
            objFMst = Nothing

            If CBool(Session("IsUseAgreedScore")) Then
                dgvGE.Columns(disGEColumns("dgcolhAgreedScoreGE")).Visible = True
            Else
                dgvGE.Columns(disGEColumns("dgcolhAgreedScoreGE")).Visible = False
            End If

            If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNotNecessary Then
                dgvGE.Columns(disGEColumns("dgcolhrselfGE")).Visible = False
            ElseIf CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNeeded Then
                dgvGE.Columns(disGEColumns("dgcolhrselfGE")).Visible = True
            End If

            dgvGE.DataSource = dtGE_TabularGrid
            dgvGE.DataBind()

            If dtGE_TabularGrid.Rows.Count > 0 Then
                Dim ival = dtGE_TabularGrid.AsEnumerable().Where(Function(X) X.Field(Of String)("Weight") <> "").Select(Function(x) x.Field(Of String)("Weight")).ToList()
                Dim total = ival.Sum(Function(x) x)

                objlblGEWgt.Text = "Weight :" & " " & CDbl(total).ToString
                objlblGEScr.Text = CDbl(0).ToString
                objlblGEScrCaption.Visible = True
                objlblGEWgt.Visible = True
                objlblGEScr.Visible = True

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    <WebMethod()> _
    Public Shared Function IsValidCustomValue(ByVal intCstmId As Integer, _
                                          ByVal strCstmValue As String, _
                                          ByVal intEmployeeId As Integer, _
                                          ByVal intPeriodId As Integer) As String
        Dim strMsg As String = String.Empty
        Try
            Dim objEvalMst As New clsevaluation_analysis_master
            strMsg = objEvalMst.IsValidCustomValue(intCstmId, strCstmValue, intEmployeeId, intPeriodId)
            objEvalMst = Nothing
            Return strMsg
        Catch ex As Exception
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                'S.SANDEEP |04-MAR-2020| -- START
                '._Error_Message = clsCrypto.Encrypt(ex.Message)
                'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
                Dim S_dispmsg As String = ex.Message & "; " & ex.StackTrace.ToString
                If ex.InnerException IsNot Nothing Then
                    S_dispmsg &= "; " & ex.InnerException.Message
                End If
                'Sohail (01 Feb 2020) -- End
                S_dispmsg = S_dispmsg.Replace("'", "")
                S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                ._Error_Message = S_dispmsg
                'S.SANDEEP |04-MAR-2020| -- END
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("UserId")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With
            Dim strDBName As String = ""
            If HttpContext.Current.Session("Database_Name") IsNot Nothing Then
                If HttpContext.Current.Session("Database_Name").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    strDBName = "hrmsConfiguration"
                End If
            End If
            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            End If
            Throw New Exception("Something went wrong, Please contact administrator!")
        End Try
    End Function

    <WebMethod()> _
Public Shared Function ComputeScoreBSC(ByVal ScoringOptionId As Integer, _
                                       ByVal intPeriod As Integer, _
                                       ByVal intEmployee As Integer, _
                                       ByVal EmployeeAsOnDate As String, _
                                       ByVal IsUseAgreedScore As Boolean, _
                                       ByVal Self_Assign_Competencies As Boolean, _
                                       ByVal iScore As Decimal, _
                                       ByVal itemunkid As Integer, _
                                       ByVal analysistranunkid As Integer, _
                                       ByVal perspectiveunkid As Integer, _
                                       ByVal empfield1unkid As Integer, _
                                       ByVal empfield2unkid As Integer, _
                                       ByVal empfield3unkid As Integer, _
                                       ByVal empfield4unkid As Integer, _
                                       ByVal empfield5unkid As Integer, _
                                       ByVal remark As String, _
                                       ByVal item_weight As Decimal, _
                                       ByVal max_scale As Decimal, _
                                       ByVal dt As DateTime, _
                                       ByVal iUserId As Integer, _
                                       ByVal iAssessorMstId As Integer) As String
        Try
            Dim objGoals As New List(Of clsgoal_analysis_tran)
            objGoals.Add(New clsgoal_analysis_tran(analysistranunkid, 0, perspectiveunkid, empfield1unkid, empfield2unkid, empfield3unkid, empfield4unkid, empfield5unkid, iScore, remark, False, 0, Nothing, "", item_weight, max_scale, 0))
            Dim objEvalMst As New clsevaluation_analysis_master

            objEvalMst._Analysisunkid = 0
            objEvalMst._Periodunkid = intPeriod
            objEvalMst._Selfemployeeunkid = -1
            objEvalMst._Assessedemployeeunkid = intEmployee
            objEvalMst._Assessormasterunkid = iAssessorMstId
            objEvalMst._Assessoremployeeunkid = -1
            Dim intEmployeeId As Integer = -1
            intEmployeeId = objEvalMst.GetAssessorEmpId(iAssessorMstId)
            objEvalMst._Reviewerunkid = intEmployeeId
            objEvalMst._Assessmodeid = enAssessmentMode.REVIEWER_ASSESSMENT
            objEvalMst._Assessmentdate = dt
            objEvalMst._Userunkid = iUserId
            If objEvalMst._Committeddatetime <> Nothing Then
                objEvalMst._Committeddatetime = objEvalMst._Committeddatetime
            Else
                objEvalMst._Committeddatetime = Nothing
            End If
            objEvalMst._Isvoid = False
            objEvalMst._Voiduserunkid = -1
            objEvalMst._Voiddatetime = Nothing
            objEvalMst._Voidreason = ""
            Dim xBSC_Score As Decimal = 0


            'S.SANDEEP |13-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            objEvalMst._EvalTypeId = HttpContext.Current.Session("EvaluationTypeId")
            'S.SANDEEP |13-NOV-2020| -- END

            objEvalMst.InsertUpdate(objGoals, Nothing, Nothing, ScoringOptionId, eZeeDate.convertDate(EmployeeAsOnDate), IsUseAgreedScore, Self_Assign_Competencies, xBSC_Score, 0)
            objEvalMst = Nothing

            Dim objCompute As New clsassess_computation_master
            Dim xFinalScore As Decimal = 0
            xFinalScore += objCompute.Process_Assessment_Formula(CInt(enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE), _
                                                                 intPeriod, _
                                                                 itemunkid, _
                                                                 intEmployee, _
                                                                 ScoringOptionId, _
                                                                 iScore, enAssessmentMode.SELF_ASSESSMENT, _
                                                                 eZeeDate.convertDate(EmployeeAsOnDate), _
                                                                 IsUseAgreedScore, "", 0, Nothing, _
                                                                 Self_Assign_Competencies)
            Return xBSC_Score.ToString()
        Catch ex As Exception
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                'S.SANDEEP |04-MAR-2020| -- START
                '._Error_Message = clsCrypto.Encrypt(ex.Message)
                'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
                Dim S_dispmsg As String = ex.Message & "; " & ex.StackTrace.ToString
                If ex.InnerException IsNot Nothing Then
                    S_dispmsg &= "; " & ex.InnerException.Message
                End If
                'Sohail (01 Feb 2020) -- End
                S_dispmsg = S_dispmsg.Replace("'", "")
                S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                ._Error_Message = S_dispmsg
                'S.SANDEEP |04-MAR-2020| -- END
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("UserId")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With
            Dim strDBName As String = ""
            If HttpContext.Current.Session("Database_Name") IsNot Nothing Then
                If HttpContext.Current.Session("Database_Name").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    strDBName = "hrmsConfiguration"
                End If
            End If
            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            End If
            Throw New Exception("Something went wrong, Please contact administrator!")
        End Try
    End Function

    <WebMethod()> _
    Public Shared Function ComputeScoreGE(ByVal ScoringOptionId As Integer, _
                                          ByVal intPeriod As Integer, _
                                          ByVal intEmployee As Integer, _
                                          ByVal EmployeeAsOnDate As String, _
                                          ByVal IsUseAgreedScore As Boolean, _
                                          ByVal Self_Assign_Competencies As Boolean, _
                                          ByVal iScore As Decimal, _
                                          ByVal itemunkid As Integer, _
                                          ByVal intAssGrpId As Integer, _
                                          ByVal analysistranunkid As Integer, _
                                          ByVal remark As String, _
                                          ByVal item_weight As Decimal, _
                                          ByVal max_scale As Decimal, _
                                          ByVal dt As DateTime, _
                                          ByVal iUserId As Integer, _
                                          ByVal iAssessorMstId As Integer) As String
        Try
            Dim objCmpts As New List(Of clscompetency_analysis_tran)
            objCmpts.Add(New clscompetency_analysis_tran(analysistranunkid, 0, intAssGrpId, itemunkid, iScore, remark, False, 0, Nothing, "", item_weight, max_scale, 0))
            Dim objEvalMst As New clsevaluation_analysis_master

            objEvalMst._Analysisunkid = 0
            objEvalMst._Periodunkid = intPeriod
            objEvalMst._Selfemployeeunkid = -1
            objEvalMst._Assessedemployeeunkid = intEmployee
            objEvalMst._Assessormasterunkid = iAssessorMstId
            objEvalMst._Assessoremployeeunkid = -1
            Dim intEmployeeId As Integer = -1
            intEmployeeId = objEvalMst.GetAssessorEmpId(iAssessorMstId)
            objEvalMst._Reviewerunkid = intEmployeeId
            objEvalMst._Assessmodeid = enAssessmentMode.REVIEWER_ASSESSMENT
            objEvalMst._Assessmentdate = dt
            objEvalMst._Userunkid = iUserId
            If objEvalMst._Committeddatetime <> Nothing Then
                objEvalMst._Committeddatetime = objEvalMst._Committeddatetime
            Else
                objEvalMst._Committeddatetime = Nothing
            End If
            objEvalMst._Isvoid = False
            objEvalMst._Voiduserunkid = -1
            objEvalMst._Voiddatetime = Nothing
            objEvalMst._Voidreason = ""
            Dim xGE_Score As Decimal = 0

            'S.SANDEEP |13-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            objEvalMst._EvalTypeId = HttpContext.Current.Session("EvaluationTypeId")
            'S.SANDEEP |13-NOV-2020| -- END

            objEvalMst.InsertUpdate(Nothing, objCmpts, Nothing, ScoringOptionId, eZeeDate.convertDate(EmployeeAsOnDate), IsUseAgreedScore, Self_Assign_Competencies, 0, xGE_Score)
            objEvalMst = Nothing

            Dim objCompute As New clsassess_computation_master
            Dim xFinalScore As Decimal = 0
            xFinalScore += objCompute.Process_Assessment_Formula(CInt(enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE), _
                                                                 intPeriod, _
                                                                 itemunkid, _
                                                                 intEmployee, _
                                                                 ScoringOptionId, _
                                                                 iScore, enAssessmentMode.SELF_ASSESSMENT, _
                                                                 eZeeDate.convertDate(EmployeeAsOnDate), _
                                                                 IsUseAgreedScore, "", intAssGrpId, Nothing, _
                                                                 Self_Assign_Competencies)
            Return xGE_Score.ToString()
        Catch ex As Exception
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                'S.SANDEEP |04-MAR-2020| -- START
                '._Error_Message = clsCrypto.Encrypt(ex.Message)
                'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
                Dim S_dispmsg As String = ex.Message & "; " & ex.StackTrace.ToString
                If ex.InnerException IsNot Nothing Then
                    S_dispmsg &= "; " & ex.InnerException.Message
                End If
                'Sohail (01 Feb 2020) -- End
                S_dispmsg = S_dispmsg.Replace("'", "")
                S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                ._Error_Message = S_dispmsg
                'S.SANDEEP |04-MAR-2020| -- END
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("UserId")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With
            Dim strDBName As String = ""
            If HttpContext.Current.Session("Database_Name") IsNot Nothing Then
                If HttpContext.Current.Session("Database_Name").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    strDBName = "hrmsConfiguration"
                End If
            End If
            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            End If
            Throw New Exception("Something went wrong, Please contact administrator!")
        End Try
    End Function

    'Gajanan [28-May-2020] -- Start
    Private Sub setNextPrevious()
        Try

            If mintCustomHeaderIndex = 0 Then
                'S.SANDEEP |03-MAY-2021| -- START
                'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                'lnkNext.Visible = True
                If mintCustomHeaderIndex = mdicCustomHeader.Count - 1 Then
                    lnkNext.Visible = False
                Else
                    lnkNext.Visible = True
                End If
                'S.SANDEEP |03-MAY-2021| -- END  
                lnkprevious.Visible = False

            ElseIf mintCustomHeaderIndex = mdicCustomHeader.Count - 1 Then
                lnkNext.Visible = False
                lnkprevious.Visible = True
            Else
                lnkNext.Visible = True
                lnkprevious.Visible = True
            End If
            setCustomeHeaderValueChange()
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "getAssesmentItemPosition", "getAssesmentItemPosition()", True)

        Catch ex As Exception

            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub setCustomeHeaderValueChange()
        Try
            Call Fill_Custom_Grid(mintCustomHeaderVal)
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ChangeApplicantFilterImage", "ChangeApplicantFilterImage('img4','divCItemValue')", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            Call SetScore()
        End Try
    End Sub
    'Gajanan [28-May-2020] -- End

    Private Function IsValidate(ByVal blnIsSubmitted As Boolean) As Boolean 'S.SANDEEP |09-FEB-2021| -- START {blnIsSubmitted} -- END
        Try
            If blnIsSubmitted Then 'S.SANDEEP |09-FEB-2021| -- START {blnIsSubmitted} -- END
                If Session("MakeAsrAssessCommentsMandatory") = True Then
                    If dgvBSC.Items.Count > 0 Then
                        Dim dItem As IEnumerable(Of DataGridItem) = Nothing
                        dItem = dgvBSC.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhIsGrpBSC", False, True)).Text) = False AndAlso _
                                                                                        CType(x.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhrremarkBSC", False, True)).FindControl("dgcolhrremarkBSC"), TextBox).Visible = True AndAlso _
                                                                                        CType(x.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhrremarkBSC", False, True)).FindControl("dgcolhrremarkBSC"), TextBox).Text.Length <= 0)
                        If dItem.Count > 0 Then
                            'Pinkal (25-Jan-2022) -- Start
                            'Enhancement NMB  - Language Change in PM Module.	
                            'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 51, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for Objective/Targets. Please provide remark to continue. "), Me)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 51, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for Objective/Targets. Please provide remark to continue. "), Me)
                            'Pinkal (25-Jan-2022) -- End
                            Return False
                        End If
                    End If
                    If dgvGE.Items.Count > 0 Then
                        Dim dItem As IEnumerable(Of DataGridItem) = Nothing
                        dItem = dgvGE.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsGrpGE", False, True)).Text) = False AndAlso _
                                                                                                   CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsPGrpGE", False, True)).Text) = False AndAlso _
                                                                                                   CType(x.Cells(getColumnId_Datagrid(dgvGE, "dgcolhrremarkGE", False, True)).FindControl("dgcolhrremarkGE"), TextBox).Visible = True AndAlso _
                                                                                                   CType(x.Cells(getColumnId_Datagrid(dgvGE, "dgcolhrremarkGE", False, True)).FindControl("dgcolhrremarkGE"), TextBox).Text.Length <= 0)
                        If dItem.Count > 0 Then
                            'Pinkal (25-Jan-2022) -- Start
                            'Enhancement NMB  - Language Change in PM Module.	
                            'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue. "), Me)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue. "), Me)
                            'Pinkal (25-Jan-2022) -- End
                            Return False
                        End If
                    End If
                End If
            End If

            Dim objPrd As New clscommom_period_Tran
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)

            If dtpAssessdate.GetDate <= objPrd._Start_Date.Date Then
                'Pinkal (25-Jan-2022) -- Start
                'Enhancement NMB  - Language Change in PM Module.	
                'Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 36, "Sorry, assessment date should be greater than") & " ( " & objPrd._Start_Date.Date.ToShortDateString & " )."
                Dim strMsg As String = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 36, "Sorry, assessment date should be greater than") & " ( " & objPrd._Start_Date.Date.ToShortDateString & " )."
                'Pinkal (25-Jan-2022) -- End
                DisplayMessage.DisplayMessage(strMsg, Me)
                dtpAssessdate.Focus()
                objPrd = Nothing
                Return False
            End If
            objPrd = Nothing

            If menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
                If CInt(cboReviewer.SelectedValue) <= 0 Then
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 21, "Reviewer is mandatory information. Please create Reviewer first. Assessment -> Setups -> Reviewer."), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 21, "Reviewer is mandatory information. Please create Reviewer first. Assessment -> Setups -> Reviewer."), Me)
                    'Pinkal (25-Jan-2022) -- End
                    cboReviewer.Focus()
                    Return False
                End If
            End If

            If CInt(cboEmployee.SelectedValue) <= 0 Or _
              CInt(cboPeriod.SelectedValue) <= 0 Then
                'Pinkal (25-Jan-2022) -- Start
                'Enhancement NMB  - Language Change in PM Module.	
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please set following information [Employee,Period] to start assessment."), Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "Please set following information [Employee,Period] to start assessment."), Me)
                'Pinkal (25-Jan-2022) -- End
                Return False
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Function SetAssessmentValues(ByRef objIGoals As List(Of clsgoal_analysis_tran), ByRef objICmpts As List(Of clscompetency_analysis_tran)) As Boolean
        Try
            Dim xRows As IEnumerable(Of DataGridItem) = Nothing
            Dim exOrder As String() = Session("Perf_EvaluationOrder").ToString.Split("|")
            If exOrder Is Nothing Then Exit Function
            Dim objGoals As List(Of clsgoal_analysis_tran) = Nothing
            Dim objCmpts As List(Of clscompetency_analysis_tran) = Nothing
            '************************* BSC ************************************************************ START
            If Array.IndexOf(exOrder, CInt(enEvaluationOrder.PE_BSC_SECTION).ToString()) <> -1 Then

                If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNeeded Then
                    If CInt(Session("ScoringOptionId")) = enScoringOption.SC_WEIGHTED_BASED Then
                        xRows = dgvBSC.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhIsGrpBSC", False, True)).Text) = False AndAlso CType(x.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhrselfBSC", False, True)).FindControl("dgcolhrselfBSC"), TextBox).Text.Length > 0)
                    Else
                        xRows = dgvBSC.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhIsGrpBSC", False, True)).Text) = False AndAlso IsNumeric(CType(x.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhrselfBSC", False, True)).FindControl("dgcolhSelBSCSel"), DropDownList).SelectedValue) = True)
                    End If
                Else
                    xRows = dgvBSC.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhIsGrpBSC", False, True)).Text) = False AndAlso CType(x.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhrremarkBSC", False, True)).FindControl("dgcolhrremarkBSC"), TextBox).Text.Length > 0)
                End If



                If xRows.Count <= 0 Then
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 46, "Sorry, Please assess atleast one item from Balance Score Card Evaluation in order to save."), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 46, "Sorry, Please assess atleast one item from Balance Score Card Evaluation in order to save."), Me)
                    'Pinkal (25-Jan-2022) -- End
                    Return False
                End If

                objGoals = New List(Of clsgoal_analysis_tran)
                For Each row In xRows
                    Dim xResult As Decimal = 0
                    Dim xAgreedResult As Decimal = 0

                    If CInt(Session("ScoringOptionId")) = enScoringOption.SC_WEIGHTED_BASED Then
                        Decimal.TryParse(CType(row.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhrselfBSC", False, True)).FindControl("dgcolhrselfBSC"), TextBox).Text, xResult)
                    Else
                        Decimal.TryParse(CType(row.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhrselfBSC", False, True)).FindControl("dgcolhSelBSCSel"), DropDownList).SelectedItem.Text, xResult)
                    End If
                    Dim xWgt, xMScl As Decimal : xWgt = 0 : xMScl = 0
                    Decimal.TryParse(row.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhBSCWeight", False, True)).Text, xWgt)
                    Decimal.TryParse(row.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhMaxScale", False, True)).Text, xMScl)
                    objGoals.Add(New clsgoal_analysis_tran(CInt(row.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhanalysistranunkid", False, True)).Text), _
                                                           mintAssessAnalysisUnkid, _
                                                           CInt(row.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolperspectiveunkid", False, True)).Text), _
                                                           CInt(row.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield1unkid", False, True)).Text), _
                                                           CInt(row.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield2unkid", False, True)).Text), _
                                                           CInt(row.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield3unkid", False, True)).Text), _
                                                           CInt(row.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield4unkid", False, True)).Text), _
                                                           CInt(row.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield5unkid", False, True)).Text), _
                                                           xResult, _
                                                           CType(row.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhrremarkBSC", False, True)).FindControl("dgcolhrremarkBSC"), TextBox).Text, _
                                                           False, -1, Nothing, "", _
                                                           xWgt, _
                                                           xMScl, _
                                                           xAgreedResult))

                Next

            End If
            '************************* BSC ************************************************************ END

            '************************* CMP ************************************************************ START
            If Array.IndexOf(exOrder, CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString()) <> -1 Then

                If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNeeded Then
                    If CInt(Session("ScoringOptionId")) = enScoringOption.SC_WEIGHTED_BASED Then
                        xRows = dgvGE.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsGrpGE", False, True)).Text) = False AndAlso _
                                                                                                   CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsPGrpGE", False, True)).Text) = False AndAlso _
                                                                                                   CType(x.Cells(getColumnId_Datagrid(dgvGE, "dgcolhrselfGE", False, True)).FindControl("dgcolhrselfGE"), TextBox).Text.Length > 0)
                    Else
                        xRows = dgvGE.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsGrpGE", False, True)).Text) = False AndAlso _
                                                                                                   CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsPGrpGE", False, True)).Text) = False AndAlso _
                                                                                                   IsNumeric(CType(x.Cells(getColumnId_Datagrid(dgvGE, "dgcolhrselfGE", False, True)).FindControl("dgcolhSelGESel"), DropDownList).SelectedValue) = True)
                    End If
                Else
                    xRows = dgvGE.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsGrpGE", False, True)).Text) = False AndAlso _
                                                                                                   CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsPGrpGE", False, True)).Text) = False AndAlso _
                                                                                                   CType(x.Cells(getColumnId_Datagrid(dgvGE, "dgcolhrremarkGE", False, True)).FindControl("dgcolhrremarkGE"), TextBox).Text.Length > 0)
                End If


                If xRows.Count <= 0 Then
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 47, "Sorry, Please assess atleast one item from Competencies Evaluation in order to save."), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 47, "Sorry, Please assess atleast one item from Competencies Evaluation in order to save."), Me)
                    'Pinkal (25-Jan-2022) -- End
                    Return False
                End If

                objCmpts = New List(Of clscompetency_analysis_tran)
                For Each row In xRows
                    Dim xResult As Decimal = 0
                    Dim xAgreedResult As Decimal = 0
                    If CInt(Session("ScoringOptionId")) = enScoringOption.SC_WEIGHTED_BASED Then
                        Decimal.TryParse(CType(row.Cells(getColumnId_Datagrid(dgvGE, "dgcolhrselfGE", False, True)).FindControl("dgcolhrselfGE"), TextBox).Text, xResult)
                    Else
                        Decimal.TryParse(CType(row.Cells(getColumnId_Datagrid(dgvGE, "dgcolhrselfGE", False, True)).FindControl("dgcolhSelGESel"), DropDownList).SelectedItem.Text, xResult)
                    End If

                    Dim xWgt, xMScl As Decimal : xWgt = 0 : xMScl = 0
                    Decimal.TryParse(row.Cells(getColumnId_Datagrid(dgvGE, "dgcolhGEWeight", False, True)).Text, xWgt)
                    Decimal.TryParse(row.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhMaxScale", False, True)).Text, xMScl)
                    objCmpts.Add(New clscompetency_analysis_tran(CInt(row.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhanalysistranunkid", False, True)).Text), _
                                                                 mintAssessAnalysisUnkid, _
                                                                 CInt(row.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhassessgroupunkidGE", False, True)).Text), _
                                                                 CInt(row.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhcompetenciesunkidGE", False, True)).Text), _
                                                                 xResult, _
                                                                 CType(row.Cells(getColumnId_Datagrid(dgvGE, "dgcolhrremarkGE", False, True)).FindControl("dgcolhrremarkGE"), TextBox).Text, _
                                                                 False, -1, Nothing, "", _
                                                                 xWgt, _
                                                                 xMScl, _
                                                                 xAgreedResult))
                Next
            End If
            '************************* CMP ************************************************************ END

            objIGoals = objGoals
            objICmpts = objCmpts

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or _
              CInt(cboPeriod.SelectedValue) <= 0 Then
                'Pinkal (25-Jan-2022) -- Start
                'Enhancement NMB  - Language Change in PM Module.	
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please set following information [Employee,Period] to view assessment."), Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "Please set following information [Employee,Period] to view assessment."), Me)
                'Pinkal (25-Jan-2022) -- End
                Exit Sub
            End If

            If CBool(Session("AllowAssessor_Before_Emp")) = False Then
                Dim blnOnlyCommitted As Boolean = True
                If menAction = enAction.EDIT_ONE Then
                    blnOnlyCommitted = False
                End If
                If objEAnalysisMst.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1, , , blnOnlyCommitted) = False Then
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                    '                                    "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), Me, "wPg_ReviewerEvaluationList.aspx")

                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 13, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                        "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), Me, "wPg_ReviewerEvaluationList.aspx")
                    'Pinkal (25-Jan-2022) -- End
                    Exit Sub
                End If

                If objEAnalysisMst.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1, , , blnOnlyCommitted) = False Then

                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 15, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                    '                                    "Reason: As per the setting set in configuration,You are not allowed to do assessment unless assessor does assessment for the selected employee and period."), Me, "wPg_ReviewerEvaluationList.aspx")
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 15, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                        "Reason: As per the setting set in configuration,You are not allowed to do assessment unless assessor does assessment for the selected employee and period."), Me, "wPg_ReviewerEvaluationList.aspx")
                    'Pinkal (25-Jan-2022) -- End


                    Exit Sub
                End If
            End If

            'S.SANDEEP |13-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            'If CBool(Session("AllowAssessor_Before_Emp")) = False Then
            '    Dim strMsg As String = String.Empty
            '    If menAssess <> enAssessmentMode.SELF_ASSESSMENT Then
            '        If dtpAssessdate.IsNull = False Then
            '            strMsg = objEAnalysisMst.IsValidAssessmentDate(enAssessmentMode.REVIEWER_ASSESSMENT, dtpAssessdate.GetDate, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
            '            If strMsg <> "" Then
            '                DisplayMessage.DisplayMessage(strMsg, Me)
            '                Exit Sub
            '            End If
            '        End If
            '    End If
            'End If
            Dim sMsg As String = String.Empty
            If mintEvaluationTypeId = clsevaluation_analysis_master.enPAEvalTypeId.EO_COMPETENCE Then
                sMsg = objEAnalysisMst.IsValidPeriodSelected(CInt(cboPeriod.SelectedValue), mintEvaluationTypeId, Nothing)
                If sMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(sMsg, Me)
                    dgv_Citems.DataSource = Nothing : dgv_Citems.DataBind()
                    dgvBSC.DataSource = Nothing : dgvBSC.DataBind()
                    dgvGE.DataSource = Nothing : dgvGE.DataBind()
                    cboPeriod.SelectedValue = 0
                    Exit Sub
                End If
            End If
            If mintEvaluationTypeId <> clsevaluation_analysis_master.enPAEvalTypeId.EO_COMPETENCE Then
                If CBool(Session("AllowAssessor_Before_Emp")) = False Then
                    Dim strMsg As String = String.Empty
                    If menAssess <> enAssessmentMode.SELF_ASSESSMENT Then
                        If dtpAssessdate.IsNull = False Then
                            strMsg = objEAnalysisMst.IsValidAssessmentDate(enAssessmentMode.REVIEWER_ASSESSMENT, dtpAssessdate.GetDate, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
                            If strMsg <> "" Then
                                DisplayMessage.DisplayMessage(strMsg, Me)
                                Exit Sub
                            End If
                        End If
                    End If
                End If
            End If
            'S.SANDEEP |13-NOV-2020| -- EMD

            If menAction <> enAction.EDIT_ONE Then
                If Is_Already_Assessed() = False Then
                    Exit Sub
                End If
            End If

            Dim objPrd As New clscommom_period_Tran
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            mintYearUnkid = objPrd._Yearunkid
            objPrd = Nothing
            Dim objMapping As New clsAssess_Field_Mapping
            iLinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            iMappingUnkid = objMapping.Get_MappingUnkId(CInt(cboPeriod.SelectedValue))
            objMapping._Mappingunkid = iMappingUnkid
            xTotAssignedWeight = objMapping._Weight
            objMapping = Nothing
            Dim objFMaster As New clsAssess_Field_Master
            iExOrdr = objFMaster.Get_Field_ExOrder(iLinkedFieldId, True)
            objFMaster = Nothing
            objlblBSCWgt.Text = "Weight :" & " " & CDbl(xTotAssignedWeight).ToString
            objlblBSCScr.Text = CDbl(0).ToString

            'Gajanan [28-May-2020] -- Start

            objlblBSCScrCaption.Visible = CBool(Session("AllowToViewScoreWhileDoingAssesment"))
            objlblBSCWgt.Visible = CBool(Session("AllowToViewScoreWhileDoingAssesment"))
            objlblBSCScr.Visible = CBool(Session("AllowToViewScoreWhileDoingAssesment"))


            Call FillCustomHeader(CInt(cboPeriod.SelectedValue))
            Call Fill_BSC_Evaluation()
            Call Fill_GE_Evaluation()
            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
                objlblBSCWgt.Visible = False : objlblGEWgt.Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            Call SetScore()
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            'S.SANDEEP |09-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'If IsValidate() = False Then Exit Sub
            If IsValidate(False) = False Then Exit Sub
            'S.SANDEEP |09-FEB-2021| -- END

            Dim objGoals As List(Of clsgoal_analysis_tran) = Nothing
            Dim objCmpts As List(Of clscompetency_analysis_tran) = Nothing
            If SetAssessmentValues(objGoals, objCmpts) = False Then Exit Sub

            SetValue()

            'S.SANDEEP |13-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            objEAnalysisMst._EvalTypeId = mintEvaluationTypeId
            'S.SANDEEP |13-NOV-2020| -- END

            blnFlag = objEAnalysisMst.InsertUpdate(objGoals, objCmpts, Nothing, Session("ScoringOptionId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("IsUseAgreedScore"), Session("Self_Assign_Competencies"), 0, 0)

            If blnFlag = False And objEAnalysisMst._Message <> "" Then
                DisplayMessage.DisplayMessage(objEAnalysisMst._Message, Me)
                Exit Sub
            End If

            If blnFlag Then
                If Request.QueryString.Count > 0 Then
                    Response.Redirect("~/Index.aspx", False)
                Else
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 56, "Assessment has been saved successfully."), Me, "wPg_ReviewerEvaluationList.aspx")
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 56, "Assessment has been saved successfully."), Me, "wPg_ReviewerEvaluationList.aspx")
                    'Pinkal (25-Jan-2022) -- End
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveCommit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCommit.Click
        Try
            'S.SANDEEP |09-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'If IsValidate() = False Then Exit Sub
            If IsValidate(True) = False Then Exit Sub
            'S.SANDEEP |09-FEB-2021| -- END

            'mintAssessAnalysisUnkid = objEAnalysisMst.GetAnalusisUnkid(CInt(cboEmployee.SelectedValue), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
            'If mintAssessAnalysisUnkid <= 0 Then
            '    Dim exOrder As String() = Session("Perf_EvaluationOrder").ToString.Split("|")
            '    If exOrder Is Nothing Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 35, "Sorry, Please assess atleast one item from Balance Score Card Or Competencies Evaluation in order to save."), Me)
            '        Exit Sub
            '    Else
            '        If Array.IndexOf(exOrder, CInt(enEvaluationOrder.PE_BSC_SECTION).ToString()) <> -1 AndAlso Array.IndexOf(exOrder, CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString) <> -1 Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 35, "Sorry, Please assess atleast one item from Balance Score Card Or Competencies Evaluation in order to save."), Me)
            '            Exit Sub
            '        ElseIf Array.IndexOf(exOrder, CInt(enEvaluationOrder.PE_BSC_SECTION).ToString()) <> -1 Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 46, "Sorry, Please assess atleast one item from Balance Score Card Evaluation in order to save."), Me)
            '            Exit Sub
            '        ElseIf Array.IndexOf(exOrder, CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString()) <> -1 Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 47, "Sorry, Please assess atleast one item from Competencies Evaluation in order to save."), Me)
            '            Exit Sub
            '        End If
            '    End If
            'End If

            Dim iGItenCount, iCItemCount As Integer
            iGItenCount = 0 : iCItemCount = 0

            If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNeeded Then
                If CInt(Session("ScoringOptionId")) = enScoringOption.SC_WEIGHTED_BASED Then
                    iGItenCount = dgvBSC.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhIsGrpBSC", False, True)).Text) = False AndAlso CType(x.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhrselfBSC", False, True)).FindControl("dgcolhrselfBSC"), TextBox).Text.Length > 0).Count
                Else
                    iGItenCount = dgvBSC.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhIsGrpBSC", False, True)).Text) = False AndAlso IsNumeric(CType(x.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhrselfBSC", False, True)).FindControl("dgcolhSelBSCSel"), DropDownList).SelectedValue) = True).Count
                End If
            Else
                iGItenCount = dgvBSC.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhIsGrpBSC", False, True)).Text) = False AndAlso CType(x.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhrremarkBSC", False, True)).FindControl("dgcolhrremarkBSC"), TextBox).Text.Length > 0).Count
            End If

            If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNeeded Then
                If CInt(Session("ScoringOptionId")) = enScoringOption.SC_WEIGHTED_BASED Then
                    iCItemCount = dgvGE.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsGrpGE", False, True)).Text) = False AndAlso _
                                                                                               CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsPGrpGE", False, True)).Text) = False AndAlso _
                                                                                               CType(x.Cells(getColumnId_Datagrid(dgvGE, "dgcolhrselfGE", False, True)).FindControl("dgcolhrselfGE"), TextBox).Text.Length > 0).Count
                Else
                    iCItemCount = dgvGE.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsGrpGE", False, True)).Text) = False AndAlso _
                                                                                               CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsPGrpGE", False, True)).Text) = False AndAlso _
                                                                                               IsNumeric(CType(x.Cells(getColumnId_Datagrid(dgvGE, "dgcolhrselfGE", False, True)).FindControl("dgcolhSelGESel"), DropDownList).SelectedValue) = True).Count
                End If
            Else
                iCItemCount = dgvGE.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsGrpGE", False, True)).Text) = False AndAlso _
                                                                                               CBool(x.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhIsPGrpGE", False, True)).Text) = False AndAlso _
                                                                                               CType(x.Cells(getColumnId_Datagrid(dgvGE, "dgcolhrremarkGE", False, True)).FindControl("dgcolhrremarkGE"), TextBox).Text.Length > 0).Count
            End If

            Dim iMsg As String = String.Empty
            Dim intMsgType As Integer = 0
            If Session("IsAllowFinalSave") = False Then
                iMsg = objEAnalysisMst.IsValidToSubmitAssessment(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), CInt(cboReviewer.SelectedValue), enAssessmentMode.REVIEWER_ASSESSMENT, mintAssessAnalysisUnkid, eZeeDate.convertDate(Session("EmployeeAsOnDate")), CBool(Session("Self_Assign_Competencies")), Session("CompanyGroupName"), Session("Perf_EvaluationOrder"), intMsgType, iGItenCount, iCItemCount, CBool(Session("IsPDP_Perf_Integrated")))
                If iMsg.Trim.Length > 0 And intMsgType <> enEvaluationOrder.PE_CUSTOM_SECTION Then
                    DisplayMessage.DisplayMessage(iMsg, Me)
                    Exit Sub
                End If
                'S.SANDEEP |10-AUG-2020| -- START
                'ISSUE/ENHANCEMENT : SPRINT-4 {2020}
                If iMsg.Trim.Length > 0 And intMsgType = enEvaluationOrder.PE_CUSTOM_SECTION Then
                    Select Case intMsgType
                        Case enEvaluationOrder.PE_CUSTOM_SECTION
                            If Session("IsAllowCustomItemFinalSave") = False Then
                                If iMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(iMsg, Me)
                                    Exit Sub
                                End If
                            ElseIf Session("IsAllowCustomItemFinalSave") = True Then
                                'Pinkal (25-Jan-2022) -- Start
                                'Enhancement NMB  - Language Change in PM Module.	
                                'iMsg &= vbCrLf & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                                iMsg &= vbCrLf & Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                                'Pinkal (25-Jan-2022) -- End
                                cnfSubmit.Message = iMsg
                                cnfSubmit.Show()
                                Exit Sub
                            End If
                    End Select
                End If
                'S.SANDEEP |10-AUG-2020| -- END
                cnfSubmit_buttonYes_Click(sender, e)
            ElseIf Session("IsAllowFinalSave") = True Then
                iMsg = objEAnalysisMst.IsValidToSubmitAssessment(CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue), CInt(cboReviewer.SelectedValue), enAssessmentMode.REVIEWER_ASSESSMENT, mintAssessAnalysisUnkid, eZeeDate.convertDate(Session("EmployeeAsOnDate")), CBool(Session("Self_Assign_Competencies")), Session("CompanyGroupName"), Session("Perf_EvaluationOrder"), intMsgType, iGItenCount, iCItemCount, CBool(Session("IsPDP_Perf_Integrated")))
                If iMsg.Trim.Length > 0 Then
                    Select Case intMsgType
                        Case enEvaluationOrder.PE_BSC_SECTION
                            'Pinkal (25-Jan-2022) -- Start
                            'Enhancement NMB  - Language Change in PM Module.	
                            'iMsg &= vbCrLf & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                            iMsg &= vbCrLf & Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                            'Pinkal (25-Jan-2022) -- End
                        Case enEvaluationOrder.PE_COMPETENCY_SECTION
                            'Pinkal (25-Jan-2022) -- Start
                            'Enhancement NMB  - Language Change in PM Module.	
                            'iMsg &= vbCrLf & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                            iMsg &= vbCrLf & Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                            'Pinkal (25-Jan-2022) -- End
                        Case enEvaluationOrder.PE_CUSTOM_SECTION
                            If Session("IsAllowCustomItemFinalSave") = False Then
                                If iMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(iMsg, Me)
                                    Exit Sub
                                End If
                            ElseIf Session("IsAllowCustomItemFinalSave") = True Then
                                'Pinkal (25-Jan-2022) -- Start
                                'Enhancement NMB  - Language Change in PM Module.	
                                'iMsg &= vbCrLf & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                                iMsg &= vbCrLf & Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                                'Pinkal (25-Jan-2022) -- End
                            End If
                    End Select
                    cnfSubmit.Message = iMsg
                    cnfSubmit.Show()
                    Exit Sub
                Else
                    cnfSubmit_buttonYes_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            Call SetScore()
        End Try
    End Sub

    Protected Sub cnfSubmit_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfSubmit.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Dim objGoals As List(Of clsgoal_analysis_tran) = Nothing
            Dim objCmpts As List(Of clscompetency_analysis_tran) = Nothing
            If SetAssessmentValues(objGoals, objCmpts) = False Then Exit Sub

            Call SetValue()
            objEAnalysisMst._Iscommitted = True
            objEAnalysisMst._Committeddatetime = ConfigParameter._Object._CurrentDateAndTime

            'S.SANDEEP |13-NOV-2020| -- START
            'ISSUE/ENHANCEMENT : COMPETENCIES
            objEAnalysisMst._EvalTypeId = mintEvaluationTypeId
            'S.SANDEEP |13-NOV-2020| -- END

            blnFlag = objEAnalysisMst.InsertUpdate(objGoals, objCmpts, Nothing, Session("ScoringOptionId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("IsUseAgreedScore"), Session("Self_Assign_Competencies"), 0, 0)

            If blnFlag = False And objEAnalysisMst._Message <> "" Then
                DisplayMessage.DisplayMessage(objEAnalysisMst._Message, Me)
                Exit Sub
            End If

            objEAnalysisMst.Email_Notification(enAssessmentMode.REVIEWER_ASSESSMENT, _
                                                   CInt(cboEmployee.SelectedValue), _
                                                   CInt(cboPeriod.SelectedValue), _
                                                   Session("IsCompanyNeedReviewer"), _
                                                   Session("Database_Name"), _
                                                   Session("CompanyUnkId"), _
                                                   Session("ArutiSelfServiceURL"), _
                                                   CStr(cboReviewer.SelectedItem.Text), _
                                                   enLogin_Mode.MGR_SELF_SERVICE, 0)
            If Request.QueryString.Count > 0 Then
                Response.Redirect("~/Index.aspx", False)
            Else
                'Pinkal (25-Jan-2022) -- Start
                'Enhancement NMB  - Language Change in PM Module.	
                ' DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 54, "Assessment has been submitted successfully."), Me, "wPg_ReviewerEvaluationList.aspx")
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 54, "Assessment has been submitted successfully."), Me, "wPg_ReviewerEvaluationList.aspx")
                'Pinkal (25-Jan-2022) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Request.QueryString.Count > 0 Then
                Response.Redirect("~/Index.aspx", False)
            Else
                Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_ReviewerEvaluationList.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnIClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIClose.Click
        Try
            mblnItemAddEdit = False
            popup_CItemAddEdit.Hide()
            'Gajanan [28-May-2020] -- Start
            'Call cboCustomHeader_SelectedIndexChanged(New Object(), New EventArgs())
            setCustomeHeaderValueChange()
            'Gajanan [28-May-2020] -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnIAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIAdd.Click
        Try
            Dim objItemsTran As New List(Of clscompeteny_customitem_tran)
            Dim strGUIDValue As String = String.Empty
            If mstriEditingGUID.Trim.Length <= 0 Then
                strGUIDValue = Guid.NewGuid.ToString()
            Else
                strGUIDValue = mstriEditingGUID
            End If
            mintAssessAnalysisUnkid = objEAnalysisMst.GetAnalusisUnkid(CInt(cboEmployee.SelectedValue), enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboPeriod.SelectedValue))
            Dim strMsg As String = String.Empty
            For Each dgItem As DataGridItem In dgv_Citems.Items
                If CInt(dgItem.Cells(2).Text) = clsassess_custom_items.enCustomType.FREE_TEXT Then
                    If dgItem.FindControl("txtFreetext") IsNot Nothing Then
                        Dim xtxt As TextBox = CType(dgItem.FindControl("txtFreetext"), TextBox)
                        If xtxt.Visible = True Then
                            If xtxt.Enabled = True AndAlso xtxt.Text.Trim.Length > 0 Then
                                'strMsg = objEAnalysisMst.IsValidCustomValue(CInt(dgItem.Cells(4).Text), xtxt.Text, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
                                If strMsg Is Nothing Then strMsg = ""
                                If strMsg IsNot Nothing AndAlso strMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(strMsg, Me)
                                    dgItem.ForeColor = Color.Red
                                    Exit Sub
                                Else
                                    If dgItem.ForeColor = Color.Red Then
                                        dgItem.ForeColor = Color.Black
                                    End If
                                End If
                                objItemsTran.Add(New clscompeteny_customitem_tran(mintAssessAnalysisUnkid, dgItem.Cells(4).Text, xtxt.Text, False, Nothing, 0, "", CInt(cboPeriod.SelectedValue), strGUIDValue, CInt(cboEmployee.SelectedValue), True))
                            ElseIf xtxt.Enabled = True AndAlso xtxt.ReadOnly = False AndAlso xtxt.Text.Trim.Length <= 0 Then
                                'Pinkal (25-Jan-2022) -- Start
                                'Enhancement NMB  - Language Change in PM Module.	
                                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmAddCustomValue", 3, "Sorry, Please enter all mandatory information mentioned below in order to save."), Me)
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), "frmAddCustomValue", 3, "Sorry, Please enter all mandatory information mentioned below in order to save."), Me)
                                'Pinkal (25-Jan-2022) -- End
                                Exit Sub
                            End If
                        End If
                    End If
                ElseIf CInt(dgItem.Cells(2).Text) = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                    If dgItem.FindControl("dtpSelection") IsNot Nothing Then
                        Dim xdtp As Controls_DateCtrl = CType(dgItem.FindControl("dtpSelection"), Controls_DateCtrl)
                        If xdtp.Visible Then
                            If xdtp.Enabled = True AndAlso xdtp.IsNull = False Then
                                If xdtp.GetDate() <= dtpAssessdate.GetDate() Then
                                    'Pinkal (25-Jan-2022) -- Start
                                    'Enhancement NMB  - Language Change in PM Module.	
                                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmAddCustomValue", 2, "Sorry, Selected date should be greter than the assessment date selected."), Me)
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), "frmAddCustomValue", 2, "Sorry, Selected date should be greter than the assessment date selected."), Me)
                                    'Pinkal (25-Jan-2022) -- End`
                                    Exit Sub
                                End If
                                'strMsg = objEAnalysisMst.IsValidCustomValue(CInt(dgItem.Cells(4).Text), eZeeDate.convertDate(xdtp.GetDate()), CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
                                If strMsg Is Nothing Then strMsg = ""
                                If strMsg IsNot Nothing AndAlso strMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(strMsg, Me)
                                    dgItem.ForeColor = Color.Red
                                    Exit Sub
                                Else
                                    If dgItem.ForeColor = Color.Red Then
                                        dgItem.ForeColor = Color.Black
                                    End If
                                End If
                                objItemsTran.Add(New clscompeteny_customitem_tran(mintAssessAnalysisUnkid, dgItem.Cells(4).Text, eZeeDate.convertDate(xdtp.GetDateTime()).ToString(), False, Nothing, 0, "", CInt(cboPeriod.SelectedValue), strGUIDValue, CInt(cboEmployee.SelectedValue), True))
                            ElseIf xdtp.Enabled = True AndAlso xdtp.IsNull Then
                                'Pinkal (25-Jan-2022) -- Start
                                'Enhancement NMB  - Language Change in PM Module.	
                                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmAddCustomValue", 3, "Sorry, Please enter all mandatory information mentioned below in order to save."), Me)
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), "frmAddCustomValue", 3, "Sorry, Please enter all mandatory information mentioned below in order to save."), Me)
                                'Pinkal (25-Jan-2022) -- End
                                Exit Sub
                            End If
                        End If
                    End If
                ElseIf CInt(dgItem.Cells(2).Text) = clsassess_custom_items.enCustomType.SELECTION Then
                    If dgItem.FindControl("cboSelection") IsNot Nothing Then
                        Dim xCbo As DropDownList = CType(dgItem.FindControl("cboSelection"), DropDownList)
                        If xCbo.Visible Then
                            If xCbo.Enabled = True AndAlso xCbo.SelectedValue > 0 Then
                                'strMsg = objEAnalysisMst.IsValidCustomValue(CInt(dgItem.Cells(4).Text), xCbo.SelectedValue, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))

                                'S.SANDEEP |03-MAY-2021| -- START
                                'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                                If dgItem.Cells(4).Text.Trim().StartsWith("5000") Then
                                    Dim objPDP_Item As New clspdpitem_master
                                    objPDP_Item._Itemunkid = CInt(dgItem.Cells(4).Text.Trim().Replace("5000", ""))
                                    If objPDP_Item._SelectionModeId = clspdpitem_master.enPdpSelectionMode.PERFORMANCE_PERIOD Then
                                        If xCbo.SelectedValue <> cboPeriod.SelectedValue Then
                                            'Pinkal (25-Jan-2022) -- Start
                                            'Enhancement NMB  - Language Change in PM Module.	
                                            'strMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2000, "Sorry, Invalid period selected. Selected period must be same as performance period.")
                                            strMsg = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2000, "Sorry, Invalid period selected. Selected period must be same as performance period.")
                                            'Pinkal (25-Jan-2022) -- End
                                        End If
                                    End If
                                    objPDP_Item = Nothing
                                End If
                                'S.SANDEEP |03-MAY-2021| -- END

                                If strMsg Is Nothing Then strMsg = ""
                                If strMsg IsNot Nothing AndAlso strMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(strMsg, Me)
                                    dgItem.ForeColor = Color.Red
                                    Exit Sub
                                Else
                                    If dgItem.ForeColor = Color.Red Then
                                        dgItem.ForeColor = Color.Black
                                    End If
                                End If
                                objItemsTran.Add(New clscompeteny_customitem_tran(mintAssessAnalysisUnkid, dgItem.Cells(4).Text, xCbo.SelectedValue, False, Nothing, 0, "", CInt(cboPeriod.SelectedValue), strGUIDValue, CInt(cboEmployee.SelectedValue), True))
                            ElseIf xCbo.Enabled = True AndAlso xCbo.SelectedValue <= 0 Then
                                'Pinkal (25-Jan-2022) -- Start
                                'Enhancement NMB  - Language Change in PM Module.	
                                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmAddCustomValue", 3, "Sorry, Please enter all mandatory information mentioned below in order to save."), Me)
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), "frmAddCustomValue", 3, "Sorry, Please enter all mandatory information mentioned below in order to save."), Me)
                                'Pinkal (25-Jan-2022) -- End
                                Exit Sub
                            End If
                        End If
                    End If
                ElseIf CInt(dgItem.Cells(2).Text) = clsassess_custom_items.enCustomType.NUMERIC_DATA Then
                    If dgItem.FindControl("txtNUM") IsNot Nothing Then
                        Dim xtxt As Controls_NumericTextBox = CType(dgItem.FindControl("txtNUM"), Controls_NumericTextBox)
                        If xtxt.Visible Then
                            If xtxt.Enabled = True AndAlso xtxt.Text.Trim.Length > 0 Then
                                'strMsg = objEAnalysisMst.IsValidCustomValue(CInt(dgItem.Cells(4).Text), xtxt.Text, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
                                If strMsg Is Nothing Then strMsg = ""
                                If strMsg IsNot Nothing AndAlso strMsg.Trim.Length > 0 Then
                                    DisplayMessage.DisplayMessage(strMsg, Me)
                                    dgItem.ForeColor = Color.Red
                                    Exit Sub
                                Else
                                    If dgItem.ForeColor = Color.Red Then
                                        dgItem.ForeColor = Color.Black
                                    End If
                                End If
                                objItemsTran.Add(New clscompeteny_customitem_tran(mintAssessAnalysisUnkid, dgItem.Cells(4).Text, xtxt.Text, False, Nothing, 0, "", CInt(cboPeriod.SelectedValue), strGUIDValue, CInt(cboEmployee.SelectedValue), True))
                            ElseIf xtxt.Enabled = True AndAlso xtxt.Read_Only = False AndAlso xtxt.Text.Trim.Length <= 0 Then
                                'Pinkal (25-Jan-2022) -- Start
                                'Enhancement NMB  - Language Change in PM Module.	
                                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmAddCustomValue", 3, "Sorry, Please enter all mandatory information mentioned below in order to save."), Me)
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), "frmAddCustomValue", 3, "Sorry, Please enter all mandatory information mentioned below in order to save."), Me)
                                'Pinkal (25-Jan-2022) -- End
                                Exit Sub
                            End If
                        End If
                    End If
                End If
            Next

            If objItemsTran IsNot Nothing AndAlso objItemsTran.Count > 0 Then
                Dim objEvalMst As New clsevaluation_analysis_master

                'Gajanan [17-July-2020] -- Start
                'Enhancement: #0004797
                If objEvalMst.IsValidCustomValue(objItemsTran, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), If(mstriEditingGUID.Trim.Length > 0, True, False), strGUIDValue) = False Then
                    If objEAnalysisMst._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage(objEvalMst._Message, Me.Page)
                        Exit Sub
                    Else
                        'Pinkal (25-Jan-2022) -- Start
                        'Enhancement NMB  - Language Change in PM Module.	
                        'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, you cannot same custom value again for same employee and same period on same date item."), Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 1, "Sorry, you cannot same custom value again for same employee and same period on same date item."), Me)
                        'Pinkal (25-Jan-2022) -- End
                        Exit Sub
                    End If
                End If
                'Gajanan [17-July-2020] -- End



                objEvalMst._Analysisunkid = mintAssessAnalysisUnkid
                objEvalMst._Periodunkid = CInt(cboPeriod.SelectedValue)
                objEvalMst._Selfemployeeunkid = -1
                objEvalMst._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
                objEvalMst._Assessormasterunkid = CInt(cboReviewer.SelectedValue)
                Dim intEmployeeId As Integer = -1
                intEmployeeId = objEvalMst.GetAssessorEmpId(CInt(cboReviewer.SelectedValue))
                objEvalMst._Assessoremployeeunkid = intEmployeeId
                objEvalMst._Reviewerunkid = -1
                objEvalMst._Assessmodeid = enAssessmentMode.REVIEWER_ASSESSMENT
                objEvalMst._Assessmentdate = dtpAssessdate.GetDate()
                objEvalMst._Userunkid = Session("UserId")
                If objEvalMst._Committeddatetime <> Nothing Then
                    objEvalMst._Committeddatetime = objEvalMst._Committeddatetime
                Else
                    objEvalMst._Committeddatetime = Nothing
                End If
                objEvalMst._Isvoid = False
                objEvalMst._Voiduserunkid = -1
                objEvalMst._Voiddatetime = Nothing
                objEvalMst._Voidreason = ""

                'S.SANDEEP |03-MAY-2021| -- START
                'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                objEvalMst._ClientIP = CStr(Session("IP_ADD"))
                objEvalMst._FormName = mstrModuleName
                objEvalMst._FromWeb = True
                objEvalMst._HostName = CStr(Session("HOST_NAME"))
                objEvalMst._DatabaseName = CStr(Session("Database_Name"))
                objEvalMst._LoginEmployeeUnkid = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP |03-MAY-2021| -- END

                'S.SANDEEP |13-NOV-2020| -- START
                'ISSUE/ENHANCEMENT : COMPETENCIES
                objEvalMst._EvalTypeId = mintEvaluationTypeId
                'S.SANDEEP |13-NOV-2020| -- END


                objEvalMst.InsertUpdate(Nothing, Nothing, objItemsTran, Session("ScoringOptionId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("IsUseAgreedScore"), Session("Self_Assign_Competencies"), 0, 0)
                mintAssessAnalysisUnkid = objEvalMst._Analysisunkid
                objEvalMst = Nothing

                If mstriEditingGUID.Trim.Length <= 0 Then
                    Dim objCHdr As New clsassess_custom_header
                    'Gajanan [28-May-2020] -- Start
                    'objCHdr._Customheaderunkid = CInt(cboCustomHeader.SelectedValue)
                    objCHdr._Customheaderunkid = mintCustomHeaderVal
                    'Gajanan [28-May-2020] -- End

                    If objCHdr._Is_Allow_Multiple Then
                        For Each dgItem As DataGridItem In dgv_Citems.Items
                            If dgItem.FindControl("txtFreetext") IsNot Nothing Then
                                CType(dgItem.FindControl("txtFreetext"), TextBox).Text = ""
                            End If
                            If dgItem.FindControl("dtpSelection") IsNot Nothing Then
                                CType(dgItem.FindControl("dtpSelection"), Controls_DateCtrl).SetDate = Nothing
                            End If
                            If dgItem.FindControl("cboSelection") IsNot Nothing Then
                                If CType(dgItem.FindControl("cboSelection"), DropDownList).Items.Count > 0 Then _
                                CType(dgItem.FindControl("cboSelection"), DropDownList).SelectedIndex = 0
                            End If
                            If dgItem.FindControl("txtNUM") IsNot Nothing Then
                                CType(dgItem.FindControl("txtNUM"), Controls_NumericTextBox).Text = ""
                            End If
                        Next
                    Else
                        mblnItemAddEdit = False
                        popup_CItemAddEdit.Hide()
                        'Gajanan [28-May-2020] -- Start
                        'Call cboCustomHeader_SelectedIndexChanged(New Object(), New EventArgs())
                        setCustomeHeaderValueChange()
                        'Gajanan [28-May-2020] -- End

                    End If
                Else
                    mblnItemAddEdit = False
                    popup_CItemAddEdit.Hide()
                    'Gajanan [28-May-2020] -- Start
                    'Call cboCustomHeader_SelectedIndexChanged(New Object(), New EventArgs())
                    setCustomeHeaderValueChange()
                    'Gajanan [28-May-2020] -- End

                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ChangeApplicantFilterImage", "ChangeApplicantFilterImage('img4','divCItemValue')", True)
            Call SetScore()
        End Try
    End Sub

    Protected Sub delCUstomItem_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delCUstomItem.buttonDelReasonYes_Click
        Try
            If delCUstomItem.Reason.Trim.Length > 0 Then
                Dim objCustomItem As New clscompeteny_customitem_tran

                'S.SANDEEP |03-MAY-2021| -- START
                'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                objCustomItem._ivoiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objCustomItem._AuditUserId = Session("UserId")
                objCustomItem._FormName = mstrModuleName
                objCustomItem._FromWeb = True
                objCustomItem._HostName = CStr(Session("HOST_NAME"))
                objCustomItem._ClientIP = CStr(Session("IP_ADD"))
                objCustomItem._DatabaseName = CStr(Session("Database_Name"))
                objCustomItem._LoginEmployeeUnkid = 0
                'S.SANDEEP |03-MAY-2021| -- END

                objCustomItem.VoidItems(Session("UserId"), mstriEditingGUID, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), delCUstomItem.Reason)
                objCustomItem = Nothing
                'Gajanan [28-May-2020] -- Start
                'Call cboCustomHeader_SelectedIndexChanged(New Object(), New EventArgs())
                setCustomeHeaderValueChange()
                'Gajanan [28-May-2020] -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Gajanan [28-May-2020] -- Start
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNext.Click
        Try

            If mintCustomHeaderIndex < mdicCustomHeader.Count Then
                mintCustomHeaderVal = mdicCustomHeader.Keys.ElementAt(mintCustomHeaderIndex + 1)
                lblCustomHeaderval.Text = mdicCustomHeader.Values.ElementAt(mintCustomHeaderIndex + 1)
                mintCustomHeaderIndex += 1
                setNextPrevious()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkprevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkprevious.Click
        Try
            If mintCustomHeaderIndex < mdicCustomHeader.Count Then
                mintCustomHeaderVal = mdicCustomHeader.Keys.ElementAt(mintCustomHeaderIndex - 1)
                lblCustomHeaderval.Text = mdicCustomHeader.Values.ElementAt(mintCustomHeaderIndex - 1)
                mintCustomHeaderIndex -= 1
                setNextPrevious()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [28-May-2020] -- End


#End Region

#Region " Combobox Event(s) "

    Protected Sub cboReviewer_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReviewer.SelectedIndexChanged
        Try
            If CInt(cboReviewer.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                dsList = objEAnalysisMst.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                                  Session("UserId"), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                                  Session("IsIncludeInactiveEmp"), _
                                                                  CInt(cboReviewer.SelectedValue), "AEmp", True)
                With cboEmployee
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsList.Tables("AEmp")
                    .DataBind()
                    .SelectedValue = 0
                End With
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            BtnSearch_Click(New Object(), New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Gajanan [28-May-2020] -- Start
    'Protected Sub cboCustomHeader_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCustomHeader.SelectedIndexChanged
    '    Try
    '        Call Fill_Custom_Grid(CInt(cboCustomHeader.SelectedValue))
    '        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ChangeApplicantFilterImage", "ChangeApplicantFilterImage('img4','divCItemValue')", True)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        Call SetScore()
    '    End Try
    'End Sub
    'Gajanan [28-May-2020] -- End

    Protected Sub cbo_DataBound(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim drp As DropDownList = CType(sender, DropDownList)
            Dim dt As DataTable = CType(drp.DataSource, DataTable)
            For Each item As ListItem In drp.Items
                Dim irow As DataRow() = Nothing
                irow = dt.Select("name = '" & item.Text & "' ")
                If IsNothing(irow) = False AndAlso irow.Length > 0 Then
                    item.Attributes.Add("title", irow(0)("description").ToString())
                End If
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Link Event(s) "

    'Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    Try
    '        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ChangeApplicantFilterImage", "ChangeApplicantFilterImage('img4','divCItemValue')", True)
    '        Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
    '        Dim xRow As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
    '        If mblnIsMatchCompetencyStructure Then
    '            mstriEditingGUID = dgvItems.DataKeys(xRow.RowIndex)("GUID")
    '        Else
    '            mstriEditingGUID = ""
    '        End If
    '        btnIAdd.Text = "Add"
    '        If CBool(dgvItems.DataKeys(xRow.RowIndex)("Is_Allow_Multiple")) = False Then
    '            If CInt(dgvItems.DataKeys(xRow.RowIndex)("periodunkid")) > 0 Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 118, "Sorry, this particular custom header is not set for allow multiple entries when defined."), Me)
    '                Exit Sub
    '            End If
    '        End If
    '        Call Generate_Popup_Data(dgvItems.DataKeys(xRow.RowIndex)("Header_Id"), mstriEditingGUID, xRow.RowIndex, dgvItems.DataKeys(xRow.RowIndex)("Header_Name"))
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub imgEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    Try
    '        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ChangeApplicantFilterImage", "ChangeApplicantFilterImage('img4','divCItemValue')", True)
    '        Dim imgEdit As ImageButton = TryCast(sender, ImageButton)
    '        Dim row As GridViewRow = TryCast(imgEdit.NamingContainer, GridViewRow)
    '        Me.ViewState("RowIndex") = row.RowIndex

    '        btnIAdd.Text = "Save"
    '        mstriEditingGUID = dgvItems.DataKeys(row.RowIndex)("GUID")

    '        If mstriEditingGUID.Trim.Length <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 57, "Sorry, No comment(s) has been added in order to edit."), Me)
    '            Exit Sub
    '        End If

    '        'S.SANDEEP |01-SEP-2020| -- START
    '        'ISSUE/ENHANCEMENT : NMB DATA ISSUE
    '        'Call Generate_Popup_Data(dgvItems.DataKeys(row.RowIndex)("Header_Id"), mstriEditingGUID, row.RowIndex, dgvItems.DataKeys(row.RowIndex)("Header_Name"))
    '        If CInt(dgvItems.DataKeys(row.RowIndex)("analysisunkid")) = mintAssessAnalysisUnkid Then
    '            Call Generate_Popup_Data(dgvItems.DataKeys(row.RowIndex)("Header_Id"), mstriEditingGUID, row.RowIndex, dgvItems.DataKeys(row.RowIndex)("Header_Name"))
    '        End If
    '        'S.SANDEEP |01-SEP-2020| -- END

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
    '    Try
    '        'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ChangeApplicantFilterImage", "ChangeApplicantFilterImage('img4','divCItemValue')", True)
    '        Dim imgDel As ImageButton = TryCast(sender, ImageButton)
    '        Dim row As GridViewRow = TryCast(imgDel.NamingContainer, GridViewRow)
    '        Me.ViewState("RowIndex") = row.RowIndex
    '        mstriEditingGUID = dgvItems.DataKeys(row.RowIndex)("GUID")
    '        delCUstomItem.Reason = ""
    '        delCUstomItem.Title = "Please enter vaild reason to void following entry."
    '        delCUstomItem.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

#End Region

#Region " GridView Event "

    Protected Sub dgvGE_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvGE.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim intCount As Integer = 1
                If CBool(DataBinder.Eval(e.Item.DataItem, "IsPGrp")) = True Then
                    For i = 1 To dgvGE.Columns.Count - 1
                        If dgvGE.Columns(i).Visible = True Then
                            intCount += 1
                            e.Item.Cells(i).Visible = False
                        End If
                    Next
                    e.Item.Cells(disGEColumns("dgcolheval_itemGE")).ColumnSpan = intCount
                    e.Item.Cells(disGEColumns("dgcolheval_itemGE")).CssClass = "MainGroupHeaderStyle"
                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then
                    intCount = 1
                    For i = 1 To dgvGE.Columns.Count - 3
                        If dgvGE.Columns(i).Visible = True Then
                            If DirectCast(dgvGE.Columns(i), System.Web.UI.WebControls.DataGridColumn).FooterText <> "objdgcolhInformation" Then
                                intCount += 1
                                e.Item.Cells(i).Visible = False
                            End If
                        End If
                    Next
                    If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
                        e.Item.Cells(disGEColumns("dgcolheval_itemGE")).ColumnSpan = intCount - 4
                    Else
                        e.Item.Cells(disGEColumns("dgcolheval_itemGE")).ColumnSpan = intCount - 3
                    End If
                    e.Item.Cells(disGEColumns("dgcolheval_itemGE")).CssClass = "GroupHeaderStylecomp"
                    e.Item.Cells(disGEColumns("objdgcolhInformation")).CssClass = "GroupHeaderStylecomp"

                    Dim objCOMaster As New clsCommon_Master
                    objCOMaster._Masterunkid = CInt(e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhGrpIdGE", False, True)).Text)
                    If objCOMaster._Description <> "" Then
                        e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhInformation", False, True)).ToolTip = objCOMaster._Description
                    End If
                    objCOMaster = Nothing

                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then
                    e.Item.Cells(disGEColumns("dgcolheval_itemGE")).Text = "&nbsp;" & e.Item.Cells(disGEColumns("dgcolheval_itemGE")).Text

                    Dim objCPMsater As New clsassess_competencies_master
                    objCPMsater._Competenciesunkid = CInt(e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhcompetenciesunkidGE", False, True)).Text)
                    If objCPMsater._Description <> "" Then
                        e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhInformation", False, True)).ToolTip = objCPMsater._Description
                    End If
                    objCPMsater = Nothing

                    Select Case CInt(Session("ScoringOptionId"))
                        Case enScoringOption.SC_SCALE_BASED
                            Dim dsScore_Guide As New DataSet
                            Dim objScaleMaster As New clsAssessment_Scale
                            dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhscalemasterunkidGE", False, True)).Text, True)
                            objScaleMaster = Nothing
                            Dim drp As DropDownList = TryCast(e.Item.FindControl("dgcolhSelGESel"), DropDownList)
                            drp.Visible = True
                            With drp
                                .DataSource = dsScore_Guide.Copy
                                .DataTextField = "scale"
                                .DataValueField = "scale"
                                .ToolTip = "description"
                                .DataBind()
                            End With
                            For Each item As ListItem In drp.Items
                                Dim irow As DataRow() = Nothing
                                irow = dsScore_Guide.Tables(0).Select("scale = '" & item.Text & "' ")
                                If IsNothing(irow) = False AndAlso irow.Length > 0 Then
                                    item.Attributes.Add("title", irow(0)("description").ToString())
                                End If
                            Next
                            If IsDBNull(dsScore_Guide.Tables(0).Compute("MAX(scale)", "periodunkid > 0")) = False Then
                                e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhMaxScale", False, True)).Text = dsScore_Guide.Tables(0).Compute("MAX(scale)", "periodunkid > 0")
                            Else
                                e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhMaxScale", False, True)).Text = 0
                            End If
                            Dim txt As TextBox = TryCast(e.Item.FindControl("dgcolhrselfGE"), TextBox)
                            drp.Text = txt.Text

                            If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvGE, "dgcolhGEWeight", False, True)).Text = "&nbsp;" Then
                                drp.Visible = False
                                Dim txtR As TextBox = TryCast(e.Item.FindControl("dgcolhrremarkGE"), TextBox)
                                If txtR IsNot Nothing Then txtR.Visible = False
                            End If

                        Case enScoringOption.SC_WEIGHTED_BASED
                            Dim txt As TextBox = TryCast(e.Item.FindControl("dgcolhrselfGE"), TextBox)
                            txt.Visible = True
                            e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhMaxScale", False, True)).Text = "0"

                            If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvGE, "dgcolhGEWeight", False, True)).Text = "&nbsp;" Then
                                txt.Visible = False
                                Dim txtR As TextBox = TryCast(e.Item.FindControl("dgcolhrremarkGE"), TextBox)
                                If txtR IsNot Nothing Then txtR.Visible = False
                            End If

                    End Select
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If e.Item.ItemType = ListItemType.Header Or _
               e.Item.ItemType = ListItemType.Item Or _
               e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Cells(getColumnId_Datagrid(dgvGE, "dgcolhGEWeight", False, True)).Attributes.Add("Id", "bWeight")
                e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhMaxScale", False, True)).Attributes.Add("Id", "bmax")
                e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhanalysistranunkid", False, True)).Attributes.Add("Id", "btrnId")
                e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhcompetenciesunkidGE", False, True)).Attributes.Add("Id", "CItemId")
                e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhassessgroupunkidGE", False, True)).Attributes.Add("Id", "AGrpId")
                e.Item.Cells(getColumnId_Datagrid(dgvGE, "dgcolhaselfGE", False, True)).Attributes.Add("Id", "aslf")
                e.Item.Cells(getColumnId_Datagrid(dgvGE, "dgcolharemarkGE", False, True)).Attributes.Add("Id", "arem")
                e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhcompetenciesunkidGE", False, True)).Attributes.Add("style", "display:none")
                e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhassessgroupunkidGE", False, True)).Attributes.Add("style", "display:none")
                e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhMaxScale", False, True)).Attributes.Add("style", "display:none")
                e.Item.Cells(getColumnId_Datagrid(dgvGE, "objdgcolhanalysistranunkid", False, True)).Attributes.Add("style", "display:none")
                e.Item.Cells(getColumnId_Datagrid(dgvGE, "dgcolhrselfGE", False, True)).Attributes.Add("Id", "ascr")
                If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
                    e.Item.Cells(getColumnId_Datagrid(dgvGE, "dgcolhGEWeight", False, True)).Attributes.Add("style", "display:none")
                End If
            End If
        End Try
    End Sub

    Protected Sub dgvBSC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvBSC.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Call SetDateFormat()
                If CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then
                    For i = xVal To dgvBSC.Columns.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next
                    e.Item.Cells(xVal - 1).ColumnSpan = dgvBSC.Columns.Count - 1
                    e.Item.Cells(xVal - 1).CssClass = "GroupHeaderStyleBorderLeft"
                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then
                    e.Item.Cells(xVal - 1).Text = "&nbsp;" & e.Item.Cells(0).Text
                    If e.Item.Cells(8).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(8).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(8).Text = CDate(e.Item.Cells(8).Text).Date.ToShortDateString
                    End If

                    If e.Item.Cells(9).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(9).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(9).Text = CDate(e.Item.Cells(9).Text).Date.ToShortDateString
                    End If

                    Select Case CInt(Session("ScoringOptionId"))
                        Case enScoringOption.SC_SCALE_BASED
                            Dim dsScore_Guide As New DataSet
                            Dim objScaleMaster As New clsAssessment_Scale
                            dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhScaleMasterId", False, True)).Text, True)
                            objScaleMaster = Nothing
                            Dim drp As DropDownList = TryCast(e.Item.FindControl("dgcolhSelBSCSel"), DropDownList)
                            drp.Visible = True
                            With drp
                                .DataSource = dsScore_Guide.Copy
                                .DataTextField = "scale"
                                .DataValueField = "scale"
                                .ToolTip = "description"
                                .DataBind()
                            End With
                            For Each item As ListItem In drp.Items
                                Dim irow As DataRow() = Nothing
                                irow = dsScore_Guide.Tables(0).Select("scale = '" & item.Text & "' ")
                                If IsNothing(irow) = False AndAlso irow.Length > 0 Then
                                    item.Attributes.Add("title", irow(0)("description").ToString())
                                End If
                            Next

                            If IsDBNull(dsScore_Guide.Tables(0).Compute("MAX(scale)", "periodunkid > 0")) = False Then
                                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhMaxScale", False, True)).Text = dsScore_Guide.Tables(0).Compute("MAX(scale)", "periodunkid > 0")
                            Else
                                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhMaxScale", False, True)).Text = 0
                            End If
                            Dim txt As TextBox = TryCast(e.Item.FindControl("dgcolhrselfBSC"), TextBox)
                            drp.Text = txt.Text

                            'S.SANDEEP |28-SEP-2020| -- START
                            If CBool(Session("EnableBSCAutomaticRating")) Then
                                If CBool(Session("DontAllowToEditScoreGenbySys")) = True Then drp.Enabled = False
                            End If
                            'S.SANDEEP |28-SEP-2020| -- END

                            If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvBSC, "dgcolhBSCWeight", False, True)).Text = "&nbsp;" Then
                                drp.Visible = False
                                Dim txtR As TextBox = TryCast(e.Item.FindControl("dgcolhrremarkBSC"), TextBox)
                                If txtR IsNot Nothing Then txtR.Visible = False
                            End If

                        Case enScoringOption.SC_WEIGHTED_BASED
                            Dim txt As TextBox = TryCast(e.Item.FindControl("dgcolhrselfBSC"), TextBox)
                            txt.Visible = True
                            e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhMaxScale", False, True)).Text = "0"
                            If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNeeded Then
                                If CBool(Session("EnableBSCAutomaticRating")) Then
                                    If CBool(Session("DontAllowToEditScoreGenbySys")) = True Then txt.ReadOnly = True
                                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "changeevent" + e.Item.ItemIndex.ToString(), "change_event('" & txt.ClientID & "');", True)
                                End If
                            End If

                            If e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgvBSC, "dgcolhBSCWeight", False, True)).Text = "&nbsp;" Then
                                txt.Visible = False
                                Dim txtR As TextBox = TryCast(e.Item.FindControl("dgcolhrremarkBSC"), TextBox)
                                If txtR IsNot Nothing Then txtR.Visible = False
                            End If

                    End Select
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If e.Item.ItemType = ListItemType.Header Or _
               e.Item.ItemType = ListItemType.Item Or _
               e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield1unkid", False, True)).Attributes.Add("Id", "objdgcolhempf1")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield2unkid", False, True)).Attributes.Add("Id", "objdgcolhempf2")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield3unkid", False, True)).Attributes.Add("Id", "objdgcolhempf3")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield4unkid", False, True)).Attributes.Add("Id", "objdgcolhempf4")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield5unkid", False, True)).Attributes.Add("Id", "objdgcolhempf5")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhLinkedFieldId", False, True)).Attributes.Add("Id", "LinkedField")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhBSCWeight", False, True)).Attributes.Add("Id", "bWeight")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhMaxScale", False, True)).Attributes.Add("Id", "bmax")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhanalysistranunkid", False, True)).Attributes.Add("Id", "btrnId")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolperspectiveunkid", False, True)).Attributes.Add("Id", "bpId")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhrselfBSC", False, True)).Attributes.Add("Id", "ascr")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhaselfBSC", False, True)).Attributes.Add("Id", "aslf")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "dgcolharemarkBSC", False, True)).Attributes.Add("Id", "arem")


                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield1unkid", False, True)).Attributes.Add("style", "display:none")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield2unkid", False, True)).Attributes.Add("style", "display:none")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield3unkid", False, True)).Attributes.Add("style", "display:none")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield4unkid", False, True)).Attributes.Add("style", "display:none")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhempfield5unkid", False, True)).Attributes.Add("style", "display:none")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhLinkedFieldId", False, True)).Attributes.Add("style", "display:none")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhMaxScale", False, True)).Attributes.Add("style", "display:none")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolhanalysistranunkid", False, True)).Attributes.Add("style", "display:none")
                e.Item.Cells(getColumnId_Datagrid(dgvBSC, "objdgcolperspectiveunkid", False, True)).Attributes.Add("style", "display:none")
                If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
                    e.Item.Cells(getColumnId_Datagrid(dgvBSC, "dgcolhBSCWeight", False, True)).Attributes.Add("style", "display:none")
                End If
            End If
        End Try
    End Sub

    Protected Sub dgvItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvItems.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Dim imgAdd As New ImageButton()
                'imgAdd.ID = "imgAdd"
                'imgAdd.Attributes.Add("Class", "objAddBtn")
                'imgAdd.CommandName = "objAdd"
                'imgAdd.ImageUrl = "~/images/add_16.png"
                'imgAdd.ToolTip = "New"
                'AddHandler imgAdd.Click, AddressOf imgAdd_Click
                'e.Row.Cells(0).Controls.Add(imgAdd)

                'Dim imgEdit As New ImageButton()
                'imgEdit.ID = "imgEdit"
                'imgEdit.Attributes.Add("Class", "objAddBtn")
                'imgEdit.CommandName = "objEdit"
                'imgEdit.ImageUrl = "~/images/Edit.png"
                'imgEdit.ToolTip = "Edit"
                'AddHandler imgEdit.Click, AddressOf imgEdit_Click
                'e.Row.Cells(1).Controls.Add(imgEdit)

                'Dim imgDelete As New ImageButton()
                'imgDelete.ID = "imgDelete"
                'imgDelete.Attributes.Add("Class", "objAddBtn")
                'imgDelete.CommandName = "objDelete"
                'imgDelete.ImageUrl = "~/images/remove.png"
                'imgDelete.ToolTip = "Delete"
                'AddHandler imgDelete.Click, AddressOf imgDelete_Click
                'e.Row.Cells(2).Controls.Add(imgDelete)

                Dim imgAdd As New LinkButton
                imgAdd.ID = "imgAdd"
                'imgAdd.Attributes.Add("Class", "objAddBtn")
                imgAdd.CommandName = "objAdd"
                'imgAdd.ImageUrl = "~/images/add_16.png"
                imgAdd.ToolTip = "New"
                imgAdd.CssClass = "lnAdd"
                'AddHandler imgAdd.Click, AddressOf imgAdd_Click
                e.Row.Cells(0).Controls.Add(imgAdd)

                Dim imgEdit As New LinkButton
                imgEdit.ID = "imgEdit"
                'imgEdit.Attributes.Add("Class", "objAddBtn")
                imgEdit.CommandName = "objEdit"
                'imgEdit.ImageUrl = "~/images/Edit.png"
                imgEdit.ToolTip = "Edit"
                imgEdit.CssClass = "lnEdit"
                'AddHandler imgEdit.Click, AddressOf imgEdit_Click
                e.Row.Cells(1).Controls.Add(imgEdit)

                Dim imgDelete As New LinkButton
                imgDelete.ID = "imgDelete"
                'imgDelete.Attributes.Add("Class", "objAddBtn")
                imgDelete.CommandName = "objDelete"
                'imgDelete.ImageUrl = "~/images/remove.png"
                imgDelete.ToolTip = "Delete"
                imgDelete.CssClass = "lnDelt"
                'AddHandler imgDelete.Click, AddressOf imgDelete_Click
                e.Row.Cells(2).Controls.Add(imgDelete)

                If CInt(dgvItems.DataKeys(e.Row.RowIndex)("analysisunkid")) <= 0 AndAlso mintAssessAnalysisUnkid <= 0 Then
                ElseIf CInt(dgvItems.DataKeys(e.Row.RowIndex)("analysisunkid")) <> mintAssessAnalysisUnkid Then
                    e.Row.Cells(2).Controls.Remove(imgDelete)
                    e.Row.Cells(1).Controls.Remove(imgEdit)
                End If

                If Session("CompanyGroupName") = "NMB PLC" Then
                    If CInt(dgvItems.DataKeys(e.Row.RowIndex)("viewmodeid")) <> 3 Then
                        e.Row.Cells(0).Controls.Remove(imgAdd)
                        e.Row.Cells(1).Controls.Remove(imgEdit)
                        e.Row.Cells(2).Controls.Remove(imgDelete)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgv_Citems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgv_Citems.ItemDataBound
        Try
            If e.Item.ItemIndex > -1 Then
                If CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.FREE_TEXT Then
                    If e.Item.Cells(1).FindControl("txtFreetext") IsNot Nothing Then
                        Dim txt As TextBox = CType(e.Item.Cells(1).FindControl("txtFreetext"), TextBox)
                        txt.Rows = 7 : txt.Style.Add("resize", "none")
                        txt.Visible = True
                        If CBool(e.Item.Cells(3).Text) Or CBool(e.Item.Cells(6).Text) Then txt.ReadOnly = True
                        'S.SANDEEP |03-MAY-2021| -- START
                        'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                        If CBool(e.Item.Cells(10).Text) Then
                            txt.Rows = 2
                            txt.ReadOnly = True
                            Dim objEmp As New clsEmployee_Master
                            Dim objJob As New clsJobs
                            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), Nothing) = CInt(cboEmployee.SelectedValue)
                            objJob._Jobunkid = objEmp._Jobunkid
                            txt.Text = objJob._Job_Name
                            objJob = Nothing : objEmp = Nothing
                        End If
                        'S.SANDEEP |03-MAY-2021| -- END
                    End If
                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                    If e.Item.Cells(1).FindControl("dtpSelection") IsNot Nothing Then
                        Dim dtp As Controls_DateCtrl = CType(e.Item.Cells(1).FindControl("dtpSelection"), Controls_DateCtrl)
                        If CBool(e.Item.Cells(3).Text) Or CBool(e.Item.Cells(6).Text) Then dtp.Enabled = False
                        dtp.Visible = True
                        If e.Item.Cells(getColumnId_Datagrid(dgv_Citems, "objdgcolhddate", False, True)).Text.Trim.Length > 0 AndAlso _
                           e.Item.Cells(getColumnId_Datagrid(dgv_Citems, "objdgcolhddate", False, True)).Text.Trim <> "&nbsp;" Then
                            dtp.SetDate = CDate(e.Item.Cells(getColumnId_Datagrid(dgv_Citems, "objdgcolhddate", False, True)).Text)
                        End If
                    End If
                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.NUMERIC_DATA Then
                    If e.Item.Cells(1).FindControl("txtNUM") IsNot Nothing Then
                        Dim txt As Controls_NumericTextBox = CType(e.Item.Cells(1).FindControl("txtNUM"), Controls_NumericTextBox)
                        txt.EnableTheming = True
                        If CBool(e.Item.Cells(3).Text) Or CBool(e.Item.Cells(6).Text) Then txt.Enabled = False : txt.Read_Only = True
                        txt.Visible = True
                    End If
                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.SELECTION Then
                    If e.Item.Cells(1).FindControl("cboSelection") IsNot Nothing Then
                        Dim cbo As DropDownList = CType(e.Item.Cells(1).FindControl("cboSelection"), DropDownList)
                        If CBool(e.Item.Cells(3).Text) Or CBool(e.Item.Cells(6).Text) Then cbo.Enabled = False
                        cbo.Visible = True
                        Select Case CInt(e.Item.Cells(5).Text)
                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                                Dim dsList As New DataSet
                                If CBool(IIf(e.Item.Cells(7).Text = "&nbsp;", 0, e.Item.Cells(7).Text)) = False Then
                                    Dim objCMaster As New clsCommon_Master
                                    dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                    objCMaster = Nothing
                                Else
                                    Dim objEvalCItem As New clsevaluation_analysis_master
                                    dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(CInt(cboEmployee.SelectedValue))
                                    objEvalCItem = Nothing
                                End If
                                With cbo
                                    .DataValueField = "masterunkid"
                                    .DataTextField = "name"
                                    .DataSource = dsList.Tables(0)
                                    .ToolTip = "name"
                                    .SelectedValue = 0
                                    .DataBind()
                                End With
                            Case clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES
                                Dim dtab As DataTable = Nothing
                                Dim dsList As New DataSet
                                If CBool(IIf(e.Item.Cells(7).Text = "&nbsp;", 0, e.Item.Cells(7).Text)) = False Then
                                    Dim objCMaster As New clsCommon_Master
                                    dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                    objCMaster = Nothing
                                Else
                                    Dim objEvalCItem As New clsevaluation_analysis_master
                                    dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(CInt(cboEmployee.SelectedValue))
                                    objEvalCItem = Nothing
                                End If

                                If CInt(e.Item.Cells(5).Text) = clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES Then
                                    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,1)", "", DataViewRowState.CurrentRows).ToTable
                                ElseIf CInt(e.Item.Cells(5).Text) = clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                                    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,2)", "", DataViewRowState.CurrentRows).ToTable
                                End If
                                With cbo
                                    .DataValueField = "masterunkid"
                                    .DataTextField = "name"
                                    .DataSource = dtab
                                    .ToolTip = "name"
                                    .SelectedValue = 0
                                    .DataBind()
                                End With
                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                Dim objCompetency As New clsassess_competencies_master
                                Dim dsList As New DataSet
                                dsList = objCompetency.getAssigned_Competencies_List(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), _
                                                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), True)
                                With cbo
                                    .DataValueField = "Id"
                                    .DataTextField = "Name"
                                    .DataSource = dsList.Tables(0)
                                    .ToolTip = "name"
                                    .SelectedValue = 0
                                    .DataBind()
                                End With
                                objCompetency = Nothing
                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                Dim objEmpField1 As New clsassess_empfield1_master
                                Dim dsList As New DataSet
                                dsList = objEmpField1.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", True, True)
                                With cbo
                                    .DataValueField = "Id"
                                    .DataTextField = "Name"
                                    .DataSource = dsList.Tables(0)
                                    .ToolTip = "name"
                                    .SelectedValue = 0
                                    .DataBind()
                                End With
                                objEmpField1 = Nothing

                            Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM

                                Dim dsList As New DataSet
                                If CBool(IIf(e.Item.Cells(7).Text = "&nbsp;", 0, e.Item.Cells(7).Text)) = False Then
                                    Dim objCMaster As New clsCommon_Master
                                    dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.PERFORMANCE_CUSTOM_ITEM, True, "List")
                                    objCMaster = Nothing
                                Else
                                    Dim objEvalCItem As New clsevaluation_analysis_master
                                    dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(CInt(cboEmployee.SelectedValue))
                                    objEvalCItem = Nothing
                                End If

                                With cbo
                                    .DataValueField = "masterunkid"
                                    .DataTextField = "name"
                                    .DataSource = dsList.Tables(0)
                                    .ToolTip = "name"
                                    .SelectedValue = 0
                                    .DataBind()
                                End With
                                'S.SANDEEP |03-MAY-2021| -- START
                                'ISSUE/ENHANCEMENT : PDP_PM_LINKING
                            Case clspdpitem_master.enPdpSelectionMode.PERFORMANCE_PERIOD

                                Dim blnIsCompetence As Boolean = False
                                If Session("EvaluationTypeId") = clsevaluation_analysis_master.enPAEvalTypeId.EO_COMPETENCE Then
                                    blnIsCompetence = True
                                End If

                                Dim dsList As New DataSet
                                Dim objPrd As New clscommom_period_Tran
                                dsList = objPrd.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "APeriod", True, 1, False, blnIsCompetence, True) 'IIf(blnIsCompetence = True, True, False)
                                Dim intCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)

                                With cbo
                                    .DataValueField = "periodunkid"
                                    .DataTextField = "name"
                                    .DataSource = dsList.Tables(0)
                                    .DataBind()
                                    Try
                                        .SelectedValue = CInt(cboPeriod.SelectedValue)
                                    Catch ex As Exception
                                        .SelectedValue = 0
                                    End Try
                                End With
                                objPrd = Nothing
                                'cbo.Enabled = False
                                'S.SANDEEP |03-MAY-2021| -- END
                        End Select
                        If e.Item.Cells(getColumnId_Datagrid(dgv_Citems, "objdgcolhselectedid", False, True)).Text.Trim.Length > 0 AndAlso _
                           e.Item.Cells(getColumnId_Datagrid(dgv_Citems, "objdgcolhselectedid", False, True)).Text.Trim <> "&nbsp;" Then
                            cbo.SelectedValue = CInt(e.Item.Cells(getColumnId_Datagrid(dgv_Citems, "objdgcolhselectedid", False, True)).Text)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If e.Item.ItemType = ListItemType.Header Or _
               e.Item.ItemType = ListItemType.Item Or _
               e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Cells(getColumnId_Datagrid(dgv_Citems, "objdgcustomitemunkid", False, True)).Attributes.Add("Id", "ctmid")
                e.Item.Cells(getColumnId_Datagrid(dgv_Citems, "objdgcustomitemunkid", False, True)).Attributes.Add("style", "display:none")
            End If
        End Try
    End Sub

    Protected Sub dgvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvItems.RowCommand
        Try
            Dim xRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
            Select Case e.CommandName.ToString.ToUpper()
                Case "OBJADD"
                    If mblnIsMatchCompetencyStructure Then
                        mstriEditingGUID = dgvItems.DataKeys(xRow.RowIndex)("GUID")
                    Else
                        mstriEditingGUID = ""
                    End If
                    btnIAdd.Text = "Add"
                    If CBool(dgvItems.DataKeys(xRow.RowIndex)("Is_Allow_Multiple")) = False Then
                        If CInt(dgvItems.DataKeys(xRow.RowIndex)("periodunkid")) > 0 Then
                            'Pinkal (25-Jan-2022) -- Start
                            'Enhancement NMB  - Language Change in PM Module.	
                            'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 118, "Sorry, this particular custom header is not set for allow multiple entries when defined."), Me)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 118, "Sorry, this particular custom header is not set for allow multiple entries when defined."), Me)
                            'Pinkal (25-Jan-2022) -- End
                            Exit Sub
                        End If
                    End If
                    Call Generate_Popup_Data(dgvItems.DataKeys(xRow.RowIndex)("Header_Id"), mstriEditingGUID, xRow.RowIndex, dgvItems.DataKeys(xRow.RowIndex)("Header_Name"))
                Case "OBJEDIT"
                    Me.ViewState("RowIndex") = xRow.RowIndex

                    btnIAdd.Text = "Save"
                    mstriEditingGUID = dgvItems.DataKeys(xRow.RowIndex)("GUID")

                    If mstriEditingGUID.Trim.Length <= 0 Then
                        'Pinkal (25-Jan-2022) -- Start
                        'Enhancement NMB  - Language Change in PM Module.	
                        'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 57, "Sorry, No comment(s) has been added in order to edit."), Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 57, "Sorry, No comment(s) has been added in order to edit."), Me)
                        'Pinkal (25-Jan-2022) -- End
                        Exit Sub
                    End If
                    If CInt(dgvItems.DataKeys(xRow.RowIndex)("analysisunkid")) = mintAssessAnalysisUnkid Then
                        Call Generate_Popup_Data(dgvItems.DataKeys(xRow.RowIndex)("Header_Id"), mstriEditingGUID, xRow.RowIndex, dgvItems.DataKeys(xRow.RowIndex)("Header_Name"))
                    End If

                Case "OBJDELETE"
                    Me.ViewState("RowIndex") = xRow.RowIndex
                    mstriEditingGUID = dgvItems.DataKeys(xRow.RowIndex)("GUID")
                    delCUstomItem.Reason = ""
                    delCUstomItem.Title = "Please enter vaild reason to void following entry."
                    delCUstomItem.Show()

            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'S.SANDEEP |19-AUG-2020| -- START
    'ISSUE/ENHANCEMENT : Language Changes
    Private Sub SetControlCaptions()
        Try
            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            ''Language.setLanguage(mstrModuleName)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblBSC.ID, Me.lblBSC.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblGE.ID, Me.lblGE.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblAssesmentItemHeader.ID, Me.lblAssesmentItemHeader.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblInstruction.ID, Me.lblInstruction.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblCItem.ID, Me.lblCItem.Text)

            Basepage.SetWebCaption(Session("Database_Name").ToString(), mstrModuleName, Me.lblBSC.ID, Me.lblBSC.Text)
            Basepage.SetWebCaption(Session("Database_Name").ToString(), mstrModuleName, Me.lblGE.ID, Me.lblGE.Text)
            Basepage.SetWebCaption(Session("Database_Name").ToString(), mstrModuleName, Me.lblAssesmentItemHeader.ID, Me.lblAssesmentItemHeader.Text)
            Basepage.SetWebCaption(Session("Database_Name").ToString(), mstrModuleName, Me.lblInstruction.ID, Me.lblInstruction.Text)
            Basepage.SetWebCaption(Session("Database_Name").ToString(), mstrModuleName, Me.lblCItem.ID, Me.lblCItem.Text)
            'Pinkal (25-Jan-2022) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |19-AUG-2020| -- END

#Region " Language "

    Private Sub SetLanguage()
        Try

            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	

            ''Language.setLanguage(mstrModuleName)
            'Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)
            'Me.lblAssessDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblAssessDate.ID, Me.lblAssessDate.Text)
            'Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblEmployee.ID, Me.lblEmployee.Text)
            'Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblPeriod.ID, Me.lblPeriod.Text)
            'Me.btnSaveCommit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),btnSaveCommit.ID, Me.btnSaveCommit.Text).Replace("&", "")
            'Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),btnClose.ID, Me.btnClose.Text).Replace("&", "")
            'Me.lblBSC.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblBSC.ID, Me.lblBSC.Text)
            'Me.lblGE.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblGE.ID, Me.lblGE.Text)
            'Me.lblAssesmentItemHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblAssesmentItemHeader.ID, Me.lblAssesmentItemHeader.Text)
            'Me.lblInstruction.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblInstruction.ID, Me.lblInstruction.Text)
            'Me.lblCItem.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblCItem.ID, Me.lblCItem.Text)

            ''Language.setLanguage("frmAddCustomValue")
            'dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhItems", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmAddCustomValue",CInt(HttpContext.Current.Session("LangId")),"dgcolhItems", dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhItems", False, True)).HeaderText)
            'dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhValue", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmAddCustomValue",CInt(HttpContext.Current.Session("LangId")),"dgcolhValue", dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhValue", False, True)).HeaderText)


            Me.Title = Basepage.SetWebLanguage(Session("Database_Name").ToString(), mstrModuleName, CInt(Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            Me.lblAssessDate.Text = Basepage.SetWebLanguage(Session("Database_Name").ToString(), mstrModuleName, CInt(Session("LangId")), lblAssessDate.ID, Me.lblAssessDate.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(Session("Database_Name").ToString(), mstrModuleName, CInt(Session("LangId")), lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(Session("Database_Name").ToString(), mstrModuleName, CInt(Session("LangId")), lblPeriod.ID, Me.lblPeriod.Text)
            Me.btnSaveCommit.Text = Basepage.SetWebLanguage(Session("Database_Name").ToString(), mstrModuleName, CInt(Session("LangId")), btnSaveCommit.ID, Me.btnSaveCommit.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(Session("Database_Name").ToString(), mstrModuleName, CInt(Session("LangId")), btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblBSC.Text = Basepage.SetWebLanguage(Session("Database_Name").ToString(), mstrModuleName, CInt(Session("LangId")), lblBSC.ID, Me.lblBSC.Text)
            Me.lblGE.Text = Basepage.SetWebLanguage(Session("Database_Name").ToString(), mstrModuleName, CInt(Session("LangId")), lblGE.ID, Me.lblGE.Text)
            Me.lblAssesmentItemHeader.Text = Basepage.SetWebLanguage(Session("Database_Name").ToString(), mstrModuleName, CInt(Session("LangId")), lblAssesmentItemHeader.ID, Me.lblAssesmentItemHeader.Text)
            Me.lblInstruction.Text = Basepage.SetWebLanguage(Session("Database_Name").ToString(), mstrModuleName, CInt(Session("LangId")), lblInstruction.ID, Me.lblInstruction.Text)
            Me.lblCItem.Text = Basepage.SetWebLanguage(Session("Database_Name").ToString(), mstrModuleName, CInt(Session("LangId")), lblCItem.ID, Me.lblCItem.Text)


            dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhItems", False, True)).HeaderText = Basepage.SetWebLanguage(Session("Database_Name").ToString(), "frmAddCustomValue", CInt(Session("LangId")), "dgcolhItems", dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhItems", False, True)).HeaderText)
            dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhValue", False, True)).HeaderText = Basepage.SetWebLanguage(Session("Database_Name").ToString(), "frmAddCustomValue", CInt(Session("LangId")), "dgcolhValue", dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhValue", False, True)).HeaderText)
            'Pinkal (25-Jan-2022) -- End
        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub

#End Region

End Class
