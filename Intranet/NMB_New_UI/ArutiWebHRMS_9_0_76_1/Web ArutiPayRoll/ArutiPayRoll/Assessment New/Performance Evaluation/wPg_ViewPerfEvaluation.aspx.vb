﻿#Region " Import "

Imports System.Data
Imports Aruti.Data
Imports System.Drawing
Imports System.Data.SqlClient
Imports System.Net.Dns

#End Region


'Shani (23-Nov-2016) -- Start
'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...

Partial Class wPg_ViewPerfEvaluation
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    'Private objEAnalysisMst As New clsevaluation_analysis_master
    'Private objGoalsTran As New clsgoal_analysis_tran
    'Private objCAssessTran As New clscompetency_analysis_tran
    'Private objCCustomTran As New clscompeteny_customitem_tran
    Private mdtCustomEvaluation As DataTable
    Private dtBSC_TabularGrid As New DataTable
    Private dtGE_TabularGrid As New DataTable

    Private dtCustomTabularGrid As New DataTable
    Private iWeightTotal As Decimal = 0
    Private dsHeaders As New DataSet
    Private iHeaderId As Integer = 0
    Private iExOrdr As Integer = 0
    Private iLinkedFieldId As Integer
    Private iMappingUnkid As Integer
    Private xVal As Integer = 1
    Private dtCItems As New DataTable
    Private objCONN As SqlConnection
    Private mintAssessAnalysisUnkid As Integer = -1
    Private mstrModuleName As String = "frmPerformanceEvaluation"
    Private disBSCColumns As Dictionary(Of String, Integer) = Nothing
    Private disGEColumns As Dictionary(Of String, Integer) = Nothing
#End Region

#Region " Page Event "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim objCCustomTran As New clscompeteny_customitem_tran
        Try
            disBSCColumns = dgvBSC.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvBSC.Columns.IndexOf(x))
            disGEColumns = dgvGE.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvGE.Columns.IndexOf(x))

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                'Sohail (02 Apr 2019) -- Start
                'NMB Issue - 74.1 - Error "On Load Event !! Bad Data" on clicking any page with link after session get expired.
                If Request.QueryString.Count <= 0 Then Exit Sub
                'Sohail (02 Apr 2019) -- End

                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")
                If arr.Length = 4 Then 'S.SANDEEP |13-NOV-2020| -- START {If arr.Length = 3 Then} -- END
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try
                End If

                HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                HttpContext.Current.Session("Employeeunkid") = CInt(arr(1))
                Me.ViewState.Add("periodid", CInt(arr(2)))

                'Sohail (30 Mar 2015) -- Start
                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                'Dim objCommon As New CommonCodes
                'objCommon.GetCompanyYearInfo(CInt(Session("CompanyUnkId")))
                Dim strError As String = ""
                If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                    DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                    Exit Sub
                End If
                'Sohail (30 Mar 2015) -- End
                HttpContext.Current.Session("mdbname") = Session("Database_Name")
                gobjConfigOptions = New clsConfigOptions
                'gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                'Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("Database_Name").ToString)

                ArtLic._Object = New ArutiLic(False)
                If gobjConfigOptions.GetKeyValue(0, "Emp") = "" Then
                    Dim objGroupMaster As New clsGroup_Master
                    objGroupMaster._Groupunkid = 1
                    ArtLic._Object.HotelName = objGroupMaster._Groupname
                End If

                If gobjConfigOptions._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                    Exit Sub
                End If

                If gobjConfigOptions._IsArutiDemo Then
                    If gobjConfigOptions._IsExpire Then
                        DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                        Exit Try
                    Else
                        If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                            DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        End If
                    End If
                End If

                'Dim clsConfig As New clsConfigOptions
                'clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                'If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                '    Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                'Else
                '    Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                'End If

                'Session("IsAllowFinalSave") = clsConfig._IsAllowFinalSave
                'Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp
                'Session("EmployeeAsOnDate") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)
                'Session("IsBSC_ByEmployee") = clsConfig._IsBSC_ByEmployee
                'Session("IsCompanyNeedReviewer") = clsConfig._IsCompanyNeedReviewer
                'Session("Assessment_Instructions") = clsConfig._Assessment_Instructions
                'Session("ViewTitles_InPlanning") = clsConfig._ViewTitles_InPlanning
                'Session("FollowEmployeeHierarchy") = clsConfig._FollowEmployeeHierarchy
                'Session("CascadingTypeId") = clsConfig._CascadingTypeId
                'Session("ScoringOptionId") = clsConfig._ScoringOptionId
                'Session("ViewTitles_InEvaluation") = clsConfig._ViewTitles_InEvaluation
                'Session("ConsiderItemWeightAsNumber") = clsConfig._ConsiderItemWeightAsNumber
                ''S.SANDEEP [09 OCT 2015] -- START
                'Session("Self_Assign_Competencies") = clsConfig._Self_Assign_Competencies
                ''S.SANDEEP [09 OCT 2015] -- END

                ''S.SANDEEP [25-JAN-2017] -- START
                ''ISSUE/ENHANCEMENT : Pefromance Module Left Out Changes
                'Session("IsCompanyNeedReviewer") = clsConfig._IsCompanyNeedReviewer
                ''S.SANDEEP [25-JAN-2017] -- END

                ''S.SANDEEP [29-NOV-2017] -- START
                ''ISSUE/ENHANCEMENT : REF-ID # (38,41)
                'Session("Ntf_FinalAcknowledgementUserIds") = clsConfig._Ntf_FinalAcknowledgementUserIds
                'Session("Ntf_GoalsUnlockUserIds") = clsConfig._Ntf_GoalsUnlockUserIds
                ''S.SANDEEP [29-NOV-2017] -- END

                If gobjConfigOptions._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                    Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                Else
                    Me.ViewState.Add("ArutiSelfServiceURL", gobjConfigOptions._ArutiSelfServiceURL)
                End If

                Session("IsAllowFinalSave") = gobjConfigOptions._IsAllowFinalSave
                Session("IsIncludeInactiveEmp") = gobjConfigOptions._IsIncludeInactiveEmp
                Session("EmployeeAsOnDate") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)
                Session("IsBSC_ByEmployee") = gobjConfigOptions._IsBSC_ByEmployee
                Session("IsCompanyNeedReviewer") = gobjConfigOptions._IsCompanyNeedReviewer
                Session("Assessment_Instructions") = gobjConfigOptions._Assessment_Instructions
                Session("ViewTitles_InPlanning") = gobjConfigOptions._ViewTitles_InPlanning
                Session("FollowEmployeeHierarchy") = gobjConfigOptions._FollowEmployeeHierarchy
                Session("CascadingTypeId") = gobjConfigOptions._CascadingTypeId
                Session("ScoringOptionId") = gobjConfigOptions._ScoringOptionId
                Session("ViewTitles_InEvaluation") = gobjConfigOptions._ViewTitles_InEvaluation
                Session("ConsiderItemWeightAsNumber") = gobjConfigOptions._ConsiderItemWeightAsNumber
                'S.SANDEEP [09 OCT 2015] -- START
                Session("Self_Assign_Competencies") = gobjConfigOptions._Self_Assign_Competencies
                'S.SANDEEP [09 OCT 2015] -- END

                'S.SANDEEP [25-JAN-2017] -- START
                'ISSUE/ENHANCEMENT : Pefromance Module Left Out Changes
                Session("IsCompanyNeedReviewer") = gobjConfigOptions._IsCompanyNeedReviewer
                'S.SANDEEP [25-JAN-2017] -- END

                'S.SANDEEP [29-NOV-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # (38,41)
                Session("Ntf_FinalAcknowledgementUserIds") = gobjConfigOptions._Ntf_FinalAcknowledgementUserIds
                Session("Ntf_GoalsUnlockUserIds") = gobjConfigOptions._Ntf_GoalsUnlockUserIds
                'S.SANDEEP [29-NOV-2017] -- END

                Dim objPwdOpt As New clsPassowdOptions
                Session("IsEmployeeAsUser") = objPwdOpt._IsEmployeeAsUser
                Dim xUName, xPasswd As String : xUName = "" : xPasswd = ""

                'Shani (05-May-2016) -- Start




                'If Session("IsEmployeeAsUser") = True Then
                '    Dim objUser As New clsUserAddEdit

                '    'Shani(19-MAR-2016) -- Start
                '    'objUser._Userunkid = CInt(Me.ViewState("employeeunkid"))
                '    objUser._Userunkid = CInt(HttpContext.Current.Session("employeeunkid"))
                '    'Shani(19-MAR-2016) -- End

                '    xUName = objUser._Username : xPasswd = objUser._Password
                '    objUser = Nothing
                'Else
                '    Dim objEmp As New clsEmployee_Master

                '    'Shani(20-Nov-2015) -- Start
                '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '    'objEmp._Employeeunkid = CInt(Session("Employeeunkid"))
                '    objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(Session("Employeeunkid"))
                '    'Shani(20-Nov-2015) -- End

                '    xUName = objEmp._Displayname : xPasswd = objEmp._Password
                '    objEmp = Nothing
                'End If

                Dim objEmp As New clsEmployee_Master
                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(Session("Employeeunkid"))
                xUName = objEmp._Displayname : xPasswd = objEmp._Password
                'Sohail (24 Nov 2016) -- Start
                'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
                HttpContext.Current.Session("E_Theme_id") = objEmp._Theme_Id
                HttpContext.Current.Session("E_Lastview_id") = objEmp._LastView_Id
                HttpContext.Current.Session("Theme_id") = objEmp._Theme_Id
                HttpContext.Current.Session("Lastview_id") = objEmp._LastView_Id
                'Sohail (24 Nov 2016) -- End
                objEmp = Nothing
                'Shani (05-May-2016) -- End

                'Sohail (21 Mar 2015) -- Start
                'Enhancement - New UI Notification Link Changes.
                'Dim clsuser As New User(xUName, xPasswd, Global.User.en_loginby.Employee, Session("Database_Name"))
                Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                Call GetDatabaseVersion()
                'Shani (28-Jul-2016) -- Start
                'Issue - 
                'Dim clsuser As New User(xUName, xPasswd, Session("Database_Name"))
                'Sohail (21 Mar 2015) -- End
                'HttpContext.Current.Session("clsuser") = clsuser
                'HttpContext.Current.Session("UserName") = clsuser.UserName
                'HttpContext.Current.Session("Firstname") = clsuser.Firstname
                'HttpContext.Current.Session("Surname") = clsuser.Surname
                'HttpContext.Current.Session("MemberName") = clsuser.MemberName
                'Shani (28-Jul-2016) -- End
                HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
                'Shani(19-MAR-2016) -- Start
                HttpContext.Current.Session("DisplayName") = xUName
                'Shani (28-Jul-2016) -- Start
                'Issue - 
                HttpContext.Current.Session("UserName") = xUName
                HttpContext.Current.Session("MemberName") = xUName
                'Shani (28-Jul-2016) -- End
                'Shani(19-MAR-2016) -- End

                'Sohail (30 Mar 2015) -- Start
                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                'Shani (28-Jul-2016) -- Start
                'Issue - 
                'HttpContext.Current.Session("UserId") = clsuser.UserID
                'HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                'HttpContext.Current.Session("Password") = clsuser.password
                'HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                'HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                'Shani (28-Jul-2016) -- End

                strError = ""
                If SetUserSessions(strError) = False Then
                    DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                    Exit Sub
                End If

                strError = ""
                If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                    DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                    Exit Sub
                End If
                'Sohail (30 Mar 2015) -- End

                'S.SANDEEP |13-NOV-2020| -- START
                'ISSUE/ENHANCEMENT : COMPETENCIES
                Call SetPerfSession(gobjConfigOptions, CInt(arr(3)))
                'S.SANDEEP |13-NOV-2020| -- END

                btnNext.Enabled = False : btnBack.Enabled = False
                objlblValue1.Visible = False : objlblValue2.Visible = False
                objlblValue3.Visible = False : objlblValue4.Visible = False

                Call FillCombo() : objlblCaption.Text = ""
                cboEmployee.SelectedValue = CInt(Session("Employeeunkid"))
                cboEmployee.Enabled = False

                cboPeriod.SelectedValue = CInt(Me.ViewState("periodid"))
                Call cboPeriod_SelectedIndexChanged(New Object, New EventArgs)
                cboPeriod.Enabled = False

                objCCustomTran._EmployeeId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue))
                'S.SANDEEP [29 DEC 2015] -- START

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                'objCCustomTran._AllowCustomItemInPlanning = CBool(Session("IncludeCustomItemInPlanning"))
                'Shani (26-Sep-2016) -- End


                objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
                'S.SANDEEP [29 DEC 2015] -- END
                mdtCustomEvaluation = objCCustomTran._DataTable

                btnReset.Enabled = False

                'Sohail (30 Mar 2015) -- Start
                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.EMP_SELF_SERVICE, Session("Employeeunkid"), , Session("Database_Name"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                'Sohail (30 Mar 2015) -- End
                HttpContext.Current.Session("Login") = True

                GoTo Link
            End If

            'Shani (28-Jul-2016) -- Start
            'Issue - 
            'If Session("clsuser") Is Nothing Then
            '    Exit Sub
            'End If
            'Shani (28-Jul-2016) -- End
            If IsPostBack = False Then
                'Shani (28-Jul-2016) -- Start
                'Issue - 
                If Session("clsuser") Is Nothing Then
                    Exit Sub
                End If
                'Shani (28-Jul-2016) -- End
                objlblCaption.Text = ""
                btnNext.Enabled = False : btnBack.Enabled = False
                objlblValue1.Visible = False : objlblValue2.Visible = False
                objlblValue3.Visible = False : objlblValue4.Visible = False

                Call FillCombo()

                objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)
                'S.SANDEEP [29 DEC 2015] -- START

                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                'objCCustomTran._AllowCustomItemInPlanning = CBool(Session("IncludeCustomItemInPlanning"))
                'Shani (26-Sep-2016) -- End


                objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
                'S.SANDEEP [29 DEC 2015] -- END
                mdtCustomEvaluation = objCCustomTran._DataTable
                'Call Fill_BSC_Evaluation()
                'Call Fill_GE_Evaluation()
                'Call Fill_Custom_Grid()

                'S.SANDEEP [04 JUN 2015] -- START
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then

                    'Shani(24-JAN-2017) -- Start
                    'Enhancement : 
                    'Call btnSearch_Click(btnSearch, Nothing)
                    If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                        Call btnSearch_Click(btnSearch, Nothing)
                    End If
                    'Shani(24-JAN-2017) -- End
                End If
                'S.SANDEEP [04 JUN 2015] -- END

            End If

Link:
            'Shani (05-May-2016) -- Start
            If IsPostBack Then
                'Shani (05-May-2016) -- End
                'Gajanan [31-July-2020] -- Start


                'If Me.ViewState("CustomEvaluation") IsNot Nothing Then
                '    mdtCustomEvaluation = Me.ViewState("CustomEvaluation")
                'End If

                'If Me.Session("BSC_TabularGrid") IsNot Nothing Then
                '    dtBSC_TabularGrid = Me.Session("BSC_TabularGrid")
                'End If

                'If Me.Session("GE_TabularGrid") IsNot Nothing Then
                '    dtGE_TabularGrid = Me.Session("GE_TabularGrid")
                'End If

                'If Me.Session("CustomTabularGrid") IsNot Nothing Then
                '    dtCustomTabularGrid = Me.Session("CustomTabularGrid")
                'End If
                'Gajanan [31-July-2020] -- End

                If Me.ViewState("iWeightTotal") IsNot Nothing Then
                    iWeightTotal = Me.ViewState("iWeightTotal")
                End If

                If Me.ViewState("Headers") IsNot Nothing Then
                    dsHeaders = Me.ViewState("Headers")
                End If

                If Me.ViewState("iHeaderId") IsNot Nothing Then
                    iHeaderId = Me.ViewState("iHeaderId")
                End If

                If Me.ViewState("iExOrdr") IsNot Nothing Then
                    iExOrdr = Me.ViewState("iExOrdr")
                End If

                If Me.ViewState("iLinkedFieldId") IsNot Nothing Then
                    iLinkedFieldId = Me.ViewState("iLinkedFieldId")
                End If

                If Me.ViewState("iMappingUnkid") IsNot Nothing Then
                    iMappingUnkid = Me.ViewState("iMappingUnkid")
                End If

                If Me.ViewState("ColIndex") IsNot Nothing Then
                    xVal = Me.ViewState("ColIndex")
                End If

                If objpnlCItems.Visible = True Then
                    dgvItems.DataSource = dtCustomTabularGrid
                    dgvItems.DataBind()
                End If
                'Shani (05-May-2016) -- Start
            End If
            'Shani (05-May-2016) -- End

            'S.SANDEEP [25-JAN-2017] -- START
            'ISSUE/ENHANCEMENT : Pefromance Module Left Out Changes
            If CBool(Session("IsCompanyNeedReviewer")) = False Then
                pnlAppRejAssessment_Reviewer.Visible = False
            End If
            'S.SANDEEP [25-JAN-2017] -- END


            'S.SANDEEP |25-MAR-2019| -- START
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                btnAppRejAssessment.Visible = False
            End If
            'S.SANDEEP |25-MAR-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCCustomTran = Nothing
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try

            'Gajanan [31-July-2020] -- Start

            'If Me.ViewState("CustomEvaluation") Is Nothing Then
            '    Me.ViewState.Add("CustomEvaluation", mdtCustomEvaluation)
            'Else
            '    Me.ViewState("CustomEvaluation") = mdtCustomEvaluation
            'End If

            'If Me.Session("BSC_TabularGrid") Is Nothing Then
            '    Me.Session.Add("BSC_TabularGrid", dtBSC_TabularGrid)
            'Else
            '    Me.Session("BSC_TabularGrid") = dtBSC_TabularGrid
            'End If

            'If Me.Session("GE_TabularGrid") Is Nothing Then
            '    Me.Session.Add("GE_TabularGrid", dtGE_TabularGrid)
            'Else
            '    Me.Session("GE_TabularGrid") = dtGE_TabularGrid
            'End If

            'If Me.Session("CustomTabularGrid") Is Nothing Then
            '    Me.Session.Add("CustomTabularGrid", dtCustomTabularGrid)
            'Else
            '    Me.Session("CustomTabularGrid") = dtCustomTabularGrid
            'End If
            'Gajanan [31-July-2020] -- End

            If Me.ViewState("iWeightTotal") Is Nothing Then
                Me.ViewState.Add("iWeightTotal", iWeightTotal)
            Else
                Me.ViewState("iWeightTotal") = iWeightTotal
            End If

            If Me.ViewState("Headers") Is Nothing Then
                Me.ViewState.Add("Headers", dsHeaders)
            Else
                Me.ViewState("Headers") = dsHeaders
            End If

            If Me.ViewState("iHeaderId") Is Nothing Then
                Me.ViewState.Add("iHeaderId", iHeaderId)
            Else
                Me.ViewState("iHeaderId") = iHeaderId
            End If

            If Me.ViewState("iExOrdr") Is Nothing Then
                Me.ViewState.Add("iExOrdr", iExOrdr)
            Else
                Me.ViewState("iExOrdr") = iExOrdr
            End If

            If Me.ViewState("iLinkedFieldId") Is Nothing Then
                Me.ViewState.Add("iLinkedFieldId", iLinkedFieldId)
            Else
                Me.ViewState("iLinkedFieldId") = iLinkedFieldId
            End If

            If Me.ViewState("iMappingUnkid") Is Nothing Then
                Me.ViewState.Add("iMappingUnkid", iMappingUnkid)
            Else
                Me.ViewState("iMappingUnkid") = iMappingUnkid
            End If

            If Me.ViewState("ColIndex") Is Nothing Then
                Me.ViewState.Add("ColIndex", xVal)
            Else
                Me.ViewState("ColIndex") = xVal
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Try

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    dsCombos = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
            'ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
            '    dsCombos = objEmp.GetEmployeeList("Emp", False, True, Session("Employeeunkid"), , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'End If
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0

            'S.SANDEEP [10 DEC 2015] -- START
            Dim blnApplyUACFilter As Boolean = False 'Shani(14-APR-2016) -- [True] 

            'S.SANDEEP [10 DEC 2015] -- END

            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                'S.SANDEEP [10 DEC 2015] -- START
                blnApplyUACFilter = False
                'S.SANDEEP [10 DEC 2015] -- END
            End If

            'S.SANDEEP [04 Jan 2016] -- START
            Dim strFilterQry As String = String.Empty
            If (Session("LoginBy") = Global.User.en_loginby.User) Then

                'Shani(14-APR-2016) -- Start
                'If Session("SkipApprovalFlowInPlanning") = True Then
                'Shani(14-APR-2016) -- End
                Dim csvIds As String = String.Empty
                Dim dsMapEmp As New DataSet
                Dim objEval As New clsevaluation_analysis_master

                dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
                                                        Session("UserId"), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        True, Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)

                'Shani (12-Jan-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]

                If dsMapEmp.Tables("List").Rows.Count > 0 Then

                    dsCombos = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                              Session("UserId"), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                              Session("IsIncludeInactiveEmp"), _
                                                              CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)


                    strFilterQry = " hremployee_master.employeeunkid IN "
                    csvIds = String.Join(",", dsCombos.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
                    If csvIds.Trim.Length > 0 Then
                        strFilterQry &= "(" & csvIds & ")"
                    Else
                        strFilterQry &= "(0)"
                    End If

                    'Shani(14-APR-2016) -- Start
                Else
                    strFilterQry = " hremployee_master.employeeunkid IN (0) "
                    'Shani(14-APR-2016) -- End 
                End If

            End If
            'Shani(14-APR-2016) -- Start
            'End If
            'Shani(14-APR-2016) -- End

            'S.SANDEEP [04 Jan 2016] -- END

            dsCombos = objEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "List", blnSelect, intEmpId, , , , , , , , , , , , , , strFilterQry, , blnApplyUACFilter) 'S.SANDEEP [10 DEC 2015] -- START {blnApplyUACFilter} -- END
            'S.SANDEEP [04 Jan 2016] -- START {strFilterQry} -- END

            'Shani(24-Aug-2015) -- End
            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombos.Tables(0)
                .DataBind()
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    .SelectedValue = 0
                ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    .SelectedValue = Session("Employeeunkid")
                End If
            End With

            'S.SANDEEP [17 NOV 2015] -- START
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "APeriod", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "APeriod", True, 1, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_enddate"), "APeriod", True, 1)

            'S.SANDEEP |09-FEB-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "APeriod", True, 1)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "APeriod", True, 1, False, False, False)
            'S.SANDEEP |09-FEB-2021| -- END


            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [17 NOV 2015] -- END
            'S.SANDEEP [04 JUN 2015] -- START

            'Shani (09-May-2016) -- Start
            'Dim mintCurrentPeriodId As Integer = 0
            'If dsCombos.Tables(0).Rows.Count > 1 Then
            '    mintCurrentPeriodId = dsCombos.Tables(0).Rows(dsCombos.Tables(0).Rows.Count - 1).Item("periodunkid")
            'End If
            Dim mintCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End

            'S.SANDEEP [04 JUN 2015] -- END
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("APeriod")
                .DataBind()
                If Me.Request.QueryString.Count <= 0 Then
                    'S.SANDEEP [04 JUN 2015] -- START
                    '.SelectedValue = 0
                    .SelectedValue = mintCurrentPeriodId
                    'Shani (28-Jul-2016) -- Start
                    'Issue - 
                    'Call cboPeriod_SelectedIndexChanged(New Object, New EventArgs)
                    'Shani (28-Jul-2016) -- End
                    'S.SANDEEP [04 JUN 2015] -- END
                Else
                    .SelectedValue = Me.ViewState("periodid")
                End If

                'Shani (28-Jul-2016) -- Start
                'Issue - 
                Call cboPeriod_SelectedIndexChanged(New Object, New EventArgs)
                'Shani (28-Jul-2016) -- End
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetTotals(ByVal iDG As DataGrid)
        Try

            Dim dtComputeScore As DataTable = (New clsComputeScore_master).GetComputeScore(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), clsComputeScore_master.enAssessMode.ALL_ASSESSMENT, False)


            If dtComputeScore IsNot Nothing Then
                Select Case iDG.ID.ToUpper
                    Case dgvBSC.ID.ToUpper



                        'Pinkal (25-Jan-2022) -- Start
                        'Enhancement NMB  - Language Change in PM Module.	
                        'objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
                        objlblValue1.Text = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
                        'Pinkal (25-Jan-2022) -- End


                        objlblValue1.Visible = True : objlblValue2.Visible = True : objlblValue3.Visible = True : objlblValue4.Visible = True

                        'S.SANDEEP [21 JAN 2015] -- START
                        'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(escore)", "")).ToString
                        'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(ascore)", "")).ToString
                        'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(rscore)", "")).ToString

                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        '        'Shani(20-Nov-2015) -- Start
                        '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        '        'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                        '        '                                                  True, _
                        '        '                                                  Session("ScoringOptionId"), _
                        '        '                                                  enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                        '        '                                                  cboEmployee.SelectedValue, _
                        '        '                                                  cboPeriod.SelectedValue, , dtBSC_TabularGrid)
                        '        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
                        'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                        '                                                  True, _
                        '                                                  Session("ScoringOptionId"), _
                        '                                                  enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                        '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                        '                                                  cboEmployee.SelectedValue, _
                        '                                      cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
                        '        'Shani(20-Nov-2015) -- End


                        '        'Shani(20-Nov-2015) -- Start
                        '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        '        'objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
                        '        '                              True, _
                        '        '                              Session("ScoringOptionId"), _
                        '        '                              enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
                        '        '                              cboEmployee.SelectedValue, _
                        '        '                              cboPeriod.SelectedValue, , dtBSC_TabularGrid)
                        '        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
                        'objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
                        '                              True, _
                        '                              Session("ScoringOptionId"), _
                        '                              enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
                        '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                        '                              cboEmployee.SelectedValue, _
                        '                                      cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
                        '        'Shani(20-Nov-2015) -- End


                        '        'Shani(20-Nov-2015) -- Start
                        '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        '        'objEAnalysisMst.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
                        '        '                              True, _
                        '        '                              Session("ScoringOptionId"), _
                        '        '                              enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
                        '        '                              cboEmployee.SelectedValue, _
                        '        '                              cboPeriod.SelectedValue, , dtBSC_TabularGrid)
                        '        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & _
                        'objEAnalysisMst.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
                        '                              True, _
                        '                              Session("ScoringOptionId"), _
                        '                              enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
                        '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                        '                              cboEmployee.SelectedValue, _
                        '                                      cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
                        '        'Shani(20-Nov-2015) -- End

                        '        'S.SANDEEP [21 JAN 2015] -- END
                        '    Case dgvGE.ID.ToUpper
                        '        objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
                        '        objlblValue1.Visible = True : objlblValue2.Visible = True : objlblValue3.Visible = True : objlblValue4.Visible = True
                        '        'S.SANDEEP [21 JAN 2015] -- START
                        '        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(escore)", "")).ToString
                        '        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(ascore)", "")).ToString
                        '        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(rscore)", "")).ToString


                        '        'Shani(20-Nov-2015) -- Start
                        '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        '        'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                        '        '                                                 False, _
                        '        '                                                 Session("ScoringOptionId"), _
                        '        '                                                 enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                        '        '                                                 cboEmployee.SelectedValue, _
                        '        '                                                 cboPeriod.SelectedValue, , dtGE_TabularGrid)
                        '        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
                        'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                        '                                                 False, _
                        '                                                 Session("ScoringOptionId"), _
                        '                                                 enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                        '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                        '                                                 cboEmployee.SelectedValue, _
                        '                                      cboPeriod.SelectedValue, , dtGE_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
                        '        'Shani(20-Nov-2015) -- End

                        '        'Shani(20-Nov-2015) -- Start
                        '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        '        'objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
                        '        '                                                  False, _
                        '        '                                                  Session("ScoringOptionId"), _
                        '        '                                                  enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
                        '        '                                                  cboEmployee.SelectedValue, _
                        '        '                                                  cboPeriod.SelectedValue, , dtGE_TabularGrid)
                        '        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
                        'objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
                        '                                                  False, _
                        '                                                  Session("ScoringOptionId"), _
                        '                                                  enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
                        '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                        '                                                  cboEmployee.SelectedValue, _
                        '                                      cboPeriod.SelectedValue, , dtGE_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
                        '        'Shani(20-Nov-2015) -- End


                        '        'Shani(20-Nov-2015) -- Start
                        '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        '        'objEAnalysisMst.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
                        '        '                                                  False, _
                        '        '                                                  Session("ScoringOptionId"), _
                        '        '                                                  enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
                        '        '                                                  cboEmployee.SelectedValue, _
                        '        '                                                  cboPeriod.SelectedValue, , dtGE_TabularGrid)
                        '        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & _
                        'objEAnalysisMst.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
                        '                                                  False, _
                        '                                                  Session("ScoringOptionId"), _
                        '                                                  enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
                        '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                        '                                                  cboEmployee.SelectedValue, _
                        '                                      cboPeriod.SelectedValue, , dtGE_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
                        '        'Shani(20-Nov-2015) -- End

                        '        'S.SANDEEP [21 JAN 2015] -- END

                        '        objlblValue3.Visible = True : objlblValue4.Visible = True
                        'End Select
                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
                        '                                                  True, _
                        '                                                  Session("ScoringOptionId"), _
                        '                                                  enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                        '                                                  cboEmployee.SelectedValue, _
                        '                                                  cboPeriod.SelectedValue, , dtBSC_TabularGrid)

                        'Gajanan [31-July-2020] -- Start


                        'Pinkal (25-Jan-2022) -- Start
                        'Enhancement NMB  - Language Change in PM Module.	
                        'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " "
                        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 6, "Self Score :") & " "
                        'Pinkal (25-Jan-2022) -- End


                        'objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                        '                              IsBalanceScoreCard:=True, _
                        '                              xScoreOptId:=Session("ScoringOptionId"), _
                        '                              xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                        '                              xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                        '                              xEmployeeId:=cboEmployee.SelectedValue, _
                        '                              xPeriodId:=cboPeriod.SelectedValue, _
                        '                              xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                        '                              xDataTable:=dtBSC_TabularGrid, _
                        '                              xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))


                        If dtComputeScore IsNot Nothing AndAlso dtComputeScore.Rows.Count > 0 Then
                            Dim xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE _
                                                                                     And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                                     .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                            If xScore.Count > 0 Then
                                objlblValue2.Text &= xScore(0)
                            End If
                        End If
                        'Gajanan [31-July-2020] -- End


                        'Pinkal (25-Jan-2022) -- Start
                        'Enhancement NMB  - Language Change in PM Module.	
                        'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " "
                        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 7, "Assessor Score :") & " "
                        'Pinkal (25-Jan-2022) -- End


                        'Gajanan [31-July-2020] -- Start
                        'objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                        '                              IsBalanceScoreCard:=True, _
                        '                              xScoreOptId:=Session("ScoringOptionId"), _
                        '                              xCompute_Formula:=enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
                        '                              xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                        '                              xEmployeeId:=cboEmployee.SelectedValue, _
                        '                              xPeriodId:=cboPeriod.SelectedValue, _
                        '                              xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                        '                              xDataTable:=dtBSC_TabularGrid, _
                        '                              xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

                        If dtComputeScore IsNot Nothing AndAlso dtComputeScore.Rows.Count > 0 Then
                            Dim xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE _
                                                                                     And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
                                                                     .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                            If xScore.Count > 0 Then
                                objlblValue3.Text &= xScore(0)
                            End If
                        End If
                        'Gajanan [31-July-2020] -- End


                        'Pinkal (25-Jan-2022) -- Start
                        'Enhancement NMB  - Language Change in PM Module.	
                        'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " "
                        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 8, "Reviewer Score :") & " "
                        'Pinkal (25-Jan-2022) -- End


                        'Gajanan [31-July-2020] -- Start
                        'objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                        '                              IsBalanceScoreCard:=True, _
                        '                              xScoreOptId:=Session("ScoringOptionId"), _
                        '                              xCompute_Formula:=enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
                        '                              xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                        '                              xEmployeeId:=cboEmployee.SelectedValue, _
                        '                              xPeriodId:=cboPeriod.SelectedValue, _
                        '                              xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                        '                              xDataTable:=dtBSC_TabularGrid, _
                        '                              xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
                        If dtComputeScore IsNot Nothing AndAlso dtComputeScore.Rows.Count > 0 Then
                            Dim xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE _
                                                                                     And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.REVIEWER_ASSESSMENT) _
                                                                     .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                            If xScore.Count > 0 Then
                                objlblValue4.Text &= xScore(0)
                            End If
                        End If
                        'Gajanan [31-July-2020] -- End


                    Case dgvGE.ID.ToUpper

                        'Pinkal (25-Jan-2022) -- Start
                        'Enhancement NMB  - Language Change in PM Module.	
                        'objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
                        objlblValue1.Text = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
                        'Pinkal (25-Jan-2022) -- End


                        objlblValue1.Visible = True : objlblValue2.Visible = True : objlblValue3.Visible = True : objlblValue4.Visible = True


                        'Pinkal (25-Jan-2022) -- Start
                        'Enhancement NMB  - Language Change in PM Module.	
                        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " "
                        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 6, "Self Score :") & " "
                        'Pinkal (25-Jan-2022) -- End


                        'Gajanan [31-July-2020] -- Start
                        'objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                        '                              IsBalanceScoreCard:=False, _
                        '                              xScoreOptId:=Session("ScoringOptionId"), _
                        '                              xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                        '                              xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                        '                              xEmployeeId:=cboEmployee.SelectedValue, _
                        '                              xPeriodId:=cboPeriod.SelectedValue, _
                        '                              xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                        '                              xDataTable:=dtGE_TabularGrid, _
                        '                              xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

                        If dtComputeScore IsNot Nothing AndAlso dtComputeScore.Rows.Count > 0 Then
                            Dim xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE _
                                                                                     And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.SELF_ASSESSMENT) _
                                                                     .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                            If xScore.Count > 0 Then
                                objlblValue2.Text &= xScore(0)
                            End If
                        End If

                        'Gajanan [31-July-2020] -- End


                        'Gajanan [31-July-2020] -- Start

                        'Pinkal (25-Jan-2022) -- Start
                        'Enhancement NMB  - Language Change in PM Module.	
                        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " "
                        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 7, "Assessor Score :") & " "
                        'Pinkal (25-Jan-2022) -- End


                        'objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
                        '                              IsBalanceScoreCard:=False, _
                        '                              xScoreOptId:=Session("ScoringOptionId"), _
                        '                              xCompute_Formula:=enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
                        '                              xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                        '                              xEmployeeId:=cboEmployee.SelectedValue, _
                        '                              xPeriodId:=cboPeriod.SelectedValue, _
                        '                              xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                        '                              xDataTable:=dtGE_TabularGrid, _
                        '                              xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))


                        If dtComputeScore IsNot Nothing AndAlso dtComputeScore.Rows.Count > 0 Then
                            Dim xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE _
                                                                                     And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
                                                                     .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                            If xScore.Count > 0 Then
                                objlblValue3.Text &= xScore(0)
                            End If
                        End If
                        'Gajanan [31-July-2020] -- End


                        'Pinkal (25-Jan-2022) -- Start
                        'Enhancement NMB  - Language Change in PM Module.	
                        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " "
                        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 8, "Reviewer Score :") & " "
                        'Pinkal (25-Jan-2022) -- End


                        'Gajanan [31-July-2020] -- Start
                        'objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
                        '                              IsBalanceScoreCard:=False, _
                        '                              xScoreOptId:=Session("ScoringOptionId"), _
                        '                              xCompute_Formula:=enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
                        '                              xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                        '                              xEmployeeId:=cboEmployee.SelectedValue, _
                        '                              xPeriodId:=cboPeriod.SelectedValue, _
                        '                              xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                        '                              xDataTable:=dtGE_TabularGrid, _
                        '                              xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

                        If dtComputeScore IsNot Nothing AndAlso dtComputeScore.Rows.Count > 0 Then
                            Dim xScore = dtComputeScore.AsEnumerable().Where(Function(x) x.Field(Of Integer)("formula_typeid") = enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE _
                                                                                     And x.Field(Of Integer)("assessmodeid") = enAssessmentMode.APPRAISER_ASSESSMENT) _
                                                                     .Select(Function(x) x.Field(Of Decimal)("formula_value")).ToList
                            If xScore.Count > 0 Then
                                objlblValue4.Text &= xScore(0)
                            End If
                        End If
                        'Gajanan [31-July-2020] -- End

                        objlblValue3.Visible = True : objlblValue4.Visible = True
                End Select
            End If


            'Shani (23-Nov123-2016-2016) -- End

            'Shani(19-APR-2016) -- Start

            objlblValue4.Visible = CBool(Session("IsCompanyNeedReviewer"))
            'Shani(19-APR-2016) -- End 


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub PanelVisibility()
        Try
            If iHeaderId >= 0 Then
                objlblCaption.Text = ""
                objlblCaption.Text = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString
                xVal = dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")
            End If
            Select Case xVal
                Case -3  'Balance Score Card
                    objpnlGE.Visible = False
                    objpnlCItems.Visible = False
                    iWeightTotal = 0
                    Call Fill_BSC_Evaluation()
                    objpnlBSC.Visible = True

                Case -2  'Competencies
                    objpnlBSC.Visible = False
                    objpnlCItems.Visible = False
                    iWeightTotal = 0
                    Call Fill_GE_Evaluation()
                    objpnlGE.Visible = True
                Case Else  'Dynamic Custom Headers
                    objpnlBSC.Visible = False
                    objpnlGE.Visible = False
                    Call Fill_Custom_Grid()
                    Call Fill_Custom_Evaluation_Data()
                    objpnlCItems.Visible = True
                    iWeightTotal = 0
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    ''''''''''''''''''''''''''BSC EVOLUTION
    Private Sub SetBSC_GridCols_Tags()
        Try
            'S.SANDEEP |12-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}
            'Dim iEval() As String = Nothing
            'If CStr(Session("ViewTitles_InEvaluation")).Trim.Length > 0 Then
            '    iEval = CStr(Session("ViewTitles_InEvaluation")).Split("|")
            'End If
            'S.SANDEEP |12-MAR-2019| -- END
            Dim objFMst As New clsAssess_Field_Master
            Dim dFld As New DataSet : dFld.Tables.Add(objFMst.GetFieldsForViewSetting())
            If dFld.Tables(0).Rows.Count > 0 Then
                Dim xCol As DataGridColumn = Nothing
                Dim iExOrder As Integer = -1
                For Each xRow As DataRow In dFld.Tables(0).Rows
                    iExOrder = -1
                    iExOrder = objFMst.Get_Field_ExOrder(xRow.Item("Id"))
                    xCol = Nothing
                    Select Case iExOrder
                        Case 1
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField1"))
                        Case 2
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField2"))
                        Case 3
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField3"))
                        Case 4
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField4"))
                        Case 5
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField5"))
                        Case 6
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField6"))
                        Case 7
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField7"))
                        Case 8
                            xCol = dgvBSC.Columns(disBSCColumns("objdgcolhBSCField8"))
                    End Select
                    Select Case xRow.Item("Id")
                        Case clsAssess_Field_Master.enOtherInfoField.ST_DATE
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhSDate"))
                        Case clsAssess_Field_Master.enOtherInfoField.ED_DATE
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhEDate"))
                        Case clsAssess_Field_Master.enOtherInfoField.PCT_COMPLETE
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhCompleted"))
                        Case clsAssess_Field_Master.enOtherInfoField.STATUS
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhStatus"))
                        Case clsAssess_Field_Master.enOtherInfoField.WEIGHT
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhBSCWeight"))
                        Case clsAssess_Field_Master.enOtherInfoField.SCORE
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolheselfBSC"))
                            'S.SANDEEP |12-MAR-2019| -- START
                            'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}

                            'S.SANDEEP |14-MAR-2019| -- START
                            'Case clsAssess_Field_Master.enOtherInfoField.GOAL_TYPE
                            '    xCol = dgvBSC.Columns(disBSCColumns("dgcolhgoaltype"))
                            'Case clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE
                            '    xCol = dgvBSC.Columns(disBSCColumns("dgcolhgoalvalue"))
                            'S.SANDEEP |14-MAR-2019| -- END

                            'S.SANDEEP |12-MAR-2019| -- END
                    End Select
                    'S.SANDEEP |12-MAR-2019| -- START
                    'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}
                    'If xCol IsNot Nothing Then
                    '    xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
                    '    xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
                    '    If xRow.Item("Id") = clsAssess_Field_Master.enOtherInfoField.SCORE Then
                    '        xCol = dgvBSC.Columns(disBSCColumns("dgcolhaselfBSC"))
                    '        xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
                    '        xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
                    '        xCol = dgvBSC.Columns(disBSCColumns("dgcolhrselfBSC"))
                    '        xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
                    '        xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
                    '    End If
                    'End If
                    If xCol IsNot Nothing Then
                        xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                        xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))

                        If xRow.Item("Id") = clsAssess_Field_Master.enOtherInfoField.SCORE Then
                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhaselfBSC"))
                            xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                            xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))

                            xCol = dgvBSC.Columns(disBSCColumns("dgcolhrselfBSC"))
                            xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                            xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                        End If

                        'If xRow.Item("Id") = clsAssess_Field_Master.enOtherInfoField.GOAL_TYPE Then
                        '    xCol = dgvBSC.Columns(disBSCColumns("dgcolhgoaltype"))
                        '    xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                        '    xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                        'End If

                        'If xRow.Item("Id") = clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE Then
                        '    xCol = dgvBSC.Columns(disBSCColumns("dgcolhgoalvalue"))
                        '    xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                        '    xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                        'End If

                    End If

                    'If Array.IndexOf(iEval, xRow.Item("Id").ToString()) < 0 Then
                    '    xCol.Visible = False
                    'End If

                    'S.SANDEEP |12-MAR-2019| -- END
                Next
            End If



            objFMst = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_BSC_Evaluation()
        Dim objEAnalysisMst As New clsevaluation_analysis_master
        Try
            'S.SANDEEP [04 JUN 2015] -- START
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, -1, -1)

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, -1, -1, True)

            'S.SANDEEP [04-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : MAKE COMMON METHOD TO GET EMPLOYEE ASSESSOR/REVIEWER
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, -1, -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")), True)

            'S.SANDEEP |18-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, -1, -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")), Session("Database_Name").ToString(), True)

            'S.SANDEEP |17-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES            
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, -1, -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")), Session("Database_Name").ToString(), True, Session("fmtCurrency"))
            dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, -1, -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")), Session("Database_Name").ToString(), Session("DontAllowRatingBeyond100"), True, Session("fmtCurrency"))
            'S.SANDEEP |17-MAY-2021| -- END

            'S.SANDEEP |18-FEB-2019| -- END

            'S.SANDEEP [04-AUG-2017] -- END

            'Shani (26-Sep-2016) -- End


            'S.SANDEEP [04 JUN 2015] -- END

            Call SetBSC_GridCols_Tags()

            If dtBSC_TabularGrid.Columns.Contains("Field1") Then
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField1")).HeaderText = dtBSC_TabularGrid.Columns("Field1").Caption
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField1")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field1", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField1")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field2") Then
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField2")).HeaderText = dtBSC_TabularGrid.Columns("Field2").Caption
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField2")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field2", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField2")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field3") Then
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField3")).HeaderText = dtBSC_TabularGrid.Columns("Field3").Caption
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField3")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field3", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField3")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field4") Then
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField4")).HeaderText = dtBSC_TabularGrid.Columns("Field4").Caption
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField4")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field4", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField4")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field5") Then
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField5")).HeaderText = dtBSC_TabularGrid.Columns("Field5").Caption
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField5")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field5", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField5")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field6") Then
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField6")).HeaderText = dtBSC_TabularGrid.Columns("Field6").Caption
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField6")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field6", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField6")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field7") Then
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField7")).HeaderText = dtBSC_TabularGrid.Columns("Field7").Caption
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField7")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field7", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField7")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field8") Then
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField8")).HeaderText = dtBSC_TabularGrid.Columns("Field8").Caption
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField8")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field8", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumns("objdgcolhBSCField8")).Visible = False
            End If

            dgvBSC.Columns(disBSCColumns("dgcolheselfBSC")).HeaderText = dtBSC_TabularGrid.Columns("eself").Caption      'SELF ASSESSMENT
            dgvBSC.Columns(disBSCColumns("dgcolheremarkBSC")).HeaderText = dtBSC_TabularGrid.Columns("eremark").Caption    'SELF ASSESSMENT

            dgvBSC.Columns(disBSCColumns("dgcolhaselfBSC")).HeaderText = dtBSC_TabularGrid.Columns("aself").Caption      'ASSESSOR ASSESSMENT
            dgvBSC.Columns(disBSCColumns("dgcolharemarkBSC")).HeaderText = dtBSC_TabularGrid.Columns("aremark").Caption    'ASSESSOR ASSESSMENT

            dgvBSC.Columns(disBSCColumns("dgcolhrselfBSC")).HeaderText = dtBSC_TabularGrid.Columns("rself").Caption      'REVIEWER ASSESSMENT
            dgvBSC.Columns(disBSCColumns("dgcolhrremarkBSC")).HeaderText = dtBSC_TabularGrid.Columns("rremark").Caption    'REVIEWER ASSESSMENT

            If CStr(Session("ViewTitles_InEvaluation")).Trim.Length > 0 Then
                Dim iEval() As String = CStr(Session("ViewTitles_InEvaluation")).Split("|")
                If iEval IsNot Nothing Then
                    For Each xCol As DataGridColumn In dgvBSC.Columns
                        If xCol.FooterText IsNot Nothing Then
                            If IsNumeric(xCol.FooterText) Then
                                If Array.IndexOf(iEval, xCol.FooterText.ToString) < 0 Then
                                    xCol.Visible = False
                                End If
                            End If
                        End If
                    Next
                End If
            End If
            If dgvBSC.Columns(disBSCColumns("objdgcolhBSCField1")).Visible = True Then
                xVal = 1
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField2")).Visible = True Then
                xVal = 2
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField3")).Visible = True Then
                xVal = 3
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField4")).Visible = True Then
                xVal = 4
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField5")).Visible = True Then
                xVal = 5
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField6")).Visible = True Then
                xVal = 6
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField7")).Visible = True Then
                xVal = 7
            ElseIf dgvBSC.Columns(disBSCColumns("objdgcolhBSCField8")).Visible = True Then
                xVal = 8
            End If

            Dim xWidth As Integer = 0
            Dim objFMst As New clsAssess_Field_Master

            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK, Session("Companyunkid"))

            If xWidth > 0 Then
                dgvBSC.Columns(disBSCColumns("dgcolheremarkBSC")).HeaderStyle.Width = Unit.Pixel(xWidth)
                dgvBSC.Columns(disBSCColumns("dgcolheremarkBSC")).ItemStyle.Width = Unit.Pixel(xWidth)
            End If

            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK, Session("Companyunkid"))

            If xWidth > 0 Then
                dgvBSC.Columns(disBSCColumns("dgcolharemarkBSC")).HeaderStyle.Width = Unit.Pixel(xWidth)
                dgvBSC.Columns(disBSCColumns("dgcolharemarkBSC")).ItemStyle.Width = Unit.Pixel(xWidth)
            End If

            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.REV_REMARK, Session("Companyunkid"))

            If xWidth > 0 Then
                dgvBSC.Columns(disBSCColumns("dgcolhrremarkBSC")).HeaderStyle.Width = Unit.Pixel(xWidth)
                dgvBSC.Columns(disBSCColumns("dgcolhrremarkBSC")).ItemStyle.Width = Unit.Pixel(xWidth)
            End If

            objFMst = Nothing

            Dim intTotalWidth As Integer = 0
            For Each xCol As DataGridColumn In dgvBSC.Columns
                If xCol.Visible Then
                    intTotalWidth += xCol.HeaderStyle.Width.Value
                End If
            Next
            'S.SANDEEP |12-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}
            'For Each xCol As DataGridColumn In dgvBSC.Columns
            '    If xCol.Visible Then
            '        Dim decColumnWidth As Decimal = xCol.HeaderStyle.Width.Value * 100 / intTotalWidth
            '        xCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
            '        xCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
            '    End If
            'Next
            'dgvBSC.Width = Unit.Percentage(99.5)
            dgvBSC.Width = Unit.Pixel(intTotalWidth)
            'S.SANDEEP |12-MAR-2019| -- END


            dgvBSC.Columns(disBSCColumns("dgcolhrselfBSC")).Visible = CBool(Session("IsCompanyNeedReviewer"))      'REVIEWER ASSESSMENT
            dgvBSC.Columns(disBSCColumns("dgcolhrremarkBSC")).Visible = CBool(Session("IsCompanyNeedReviewer"))    'REVIEWER ASSESSMENT

            'Shani(14-FEB-2017) -- Start
            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            'dgvBSC.Columns(disBSCColumns("dgcolhAgreedScoreBSC")).Visible = CBool(Session("IsUseAgreedScore"))    'Agreed Score
            If CBool(Session("IsCompanyNeedReviewer")) Then
                'S.SANDEEP |31-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                'If CBool(Session("IsUseAgreedScore")) Then
                '    If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNotNecessary Then
                '        dgvBSC.Columns(disBSCColumns("dgcolhAgreedScoreBSC")).Visible = True
                '        dgvBSC.Columns(disBSCColumns("dgcolhrselfBSC")).Visible = False

                '    ElseIf CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNeeded Then
                '        dgvBSC.Columns(disBSCColumns("dgcolhAgreedScoreBSC")).Visible = False
                '        dgvBSC.Columns(disBSCColumns("dgcolhrselfBSC")).Visible = True
                '        dgvBSC.Columns(disBSCColumns("dgcolhrremarkBSC")).HeaderText = "Agreed Remark"
                '    End If
                '    dgvBSC.Columns(disBSCColumns("dgcolhrselfBSC")).HeaderText = "Agreed Score"
                'End If

                If CBool(Session("IsUseAgreedScore")) Then
                    dgvBSC.Columns(disBSCColumns("dgcolhAgreedScoreBSC")).Visible = True
                Else
                    dgvBSC.Columns(disBSCColumns("dgcolhAgreedScoreBSC")).Visible = False
                End If

                If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNotNecessary Then
                    dgvBSC.Columns(disBSCColumns("dgcolhrselfBSC")).Visible = False
                ElseIf CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNeeded Then
                    dgvBSC.Columns(disBSCColumns("dgcolhrselfBSC")).Visible = True
                End If
            Else
                dgvBSC.Columns(disBSCColumns("dgcolhAgreedScoreBSC")).Visible = CBool(Session("IsUseAgreedScore"))    'Agreed Score
            End If
            'S.SANDEEP |31-MAY-2019| -- END

            'Shani(14-FEB-2017) -- End

            dgvBSC.DataSource = dtBSC_TabularGrid
            dgvBSC.DataBind()
            Call SetTotals(dgvBSC)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEAnalysisMst = Nothing
        End Try
    End Sub


    '''''''''''''''''''''''''' COMPETENCIES/GENERAL EVALUATION METHODS
    Private Sub Fill_GE_Evaluation()
        Dim objEAnalysisMst As New clsevaluation_analysis_master
        Try
            'S.SANDEEP [04-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : MAKE COMMON METHOD TO GET EMPLOYEE ASSESSOR/REVIEWER
            'dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, 0, Session("ConsiderItemWeightAsNumber"), -1, Session("Self_Assign_Competencies"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), True)
            dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, 0, Session("ConsiderItemWeightAsNumber"), -1, Session("Self_Assign_Competencies"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("Database_Name").ToString(), True)
            'S.SANDEEP [04-AUG-2017] -- END

            dgvGE.AutoGenerateColumns = False

            dgvGE.Columns(disGEColumns("objdgcolhedisplayGE")).Visible = CBool(Session("ConsiderItemWeightAsNumber")) 'SELF FINAL SCORE
            dgvGE.Columns(disGEColumns("objdgcolhadisplayGE")).Visible = CBool(Session("ConsiderItemWeightAsNumber")) 'ASSESSOR FINAL SCORE
            dgvGE.Columns(disGEColumns("objdgcolhrdisplayGE")).Visible = CBool(Session("ConsiderItemWeightAsNumber")) 'REVIEWER FINAL SCORE

            dgvGE.Columns(disGEColumns("dgcolheselfGE")).HeaderText = dtGE_TabularGrid.Columns("eself").Caption
            dgvGE.Columns(disGEColumns("dgcolheremarkGE")).HeaderText = dtGE_TabularGrid.Columns("eremark").Caption
            dgvGE.Columns(disGEColumns("dgcolhaselfGE")).HeaderText = dtGE_TabularGrid.Columns("aself").Caption
            dgvGE.Columns(disGEColumns("dgcolharemarkGE")).HeaderText = dtGE_TabularGrid.Columns("aremark").Caption
            dgvGE.Columns(disGEColumns("dgcolhrselfGE")).HeaderText = dtGE_TabularGrid.Columns("rself").Caption
            dgvGE.Columns(disGEColumns("dgcolhrremarkGE")).HeaderText = dtGE_TabularGrid.Columns("rremark").Caption

            dgvGE.Columns(disGEColumns("dgcolhrselfGE")).Visible = CBool(Session("IsCompanyNeedReviewer"))      'REVIEWER GE Score
            dgvGE.Columns(disGEColumns("dgcolhrremarkGE")).Visible = CBool(Session("IsCompanyNeedReviewer"))    'REVIEWER GE Remark

            'Shani(14-FEB-2017) -- Start
            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            'dgvGE.Columns(disGEColumns("dgcolhAgreedScoreGE")).Visible = CBool(Session("IsUseAgreedScore"))    'Agreed Score
            If CBool(Session("IsCompanyNeedReviewer")) Then
                'S.SANDEEP |31-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : [PA SETTING CHANGES]
                'If CBool(Session("IsUseAgreedScore")) Then
                '    If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNotNecessary Then
                '        dgvGE.Columns(disGEColumns("dgcolhAgreedScoreGE")).Visible = True
                '        dgvGE.Columns(disGEColumns("dgcolhrselfGE")).Visible = False

                '    ElseIf CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNeeded Then
                '        dgvGE.Columns(disGEColumns("dgcolhAgreedScoreGE")).Visible = False
                '        dgvGE.Columns(disGEColumns("dgcolhrselfGE")).Visible = True
                '        dgvGE.Columns(disGEColumns("dgcolhrremarkGE")).HeaderText = "Agreed Remark"
                '    End If
                '    dgvGE.Columns(disGEColumns("dgcolhrselfGE")).HeaderText = "Agreed Score"
                'End If

                If CBool(Session("IsUseAgreedScore")) Then
                    dgvGE.Columns(disGEColumns("dgcolhAgreedScoreGE")).Visible = True
                Else
                    dgvGE.Columns(disGEColumns("dgcolhAgreedScoreGE")).Visible = False
                End If

                If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNotNecessary Then
                    dgvGE.Columns(disGEColumns("dgcolhrselfGE")).Visible = False
                ElseIf CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNeeded Then
                    dgvGE.Columns(disGEColumns("dgcolhrselfGE")).Visible = True
                End If
            Else
                dgvGE.Columns(disGEColumns("dgcolhAgreedScoreGE")).Visible = CBool(Session("IsUseAgreedScore"))    'Agreed Score
            End If
            'S.SANDEEP |31-MAY-2019| -- END

            'Shani(14-FEB-2017) -- End



            dgvGE.DataSource = dtGE_TabularGrid
            dgvGE.DataBind()
            Call SetTotals(dgvGE)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEAnalysisMst = Nothing
        End Try
    End Sub


    '''''''''''''''''''''''''' CUSTOM ITEMS EVALUATION METHODS
    Private Sub Fill_Custom_Grid(Optional ByVal iFromAddEdit As Boolean = False)
        Dim objEAnalysisMst As New clsevaluation_analysis_master
        Try
            If iHeaderId >= 0 Then
                If iFromAddEdit = False Then
                    'S.SANDEEP [04 JUN 2015] -- START
                    'dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), CInt(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")), mintAssessAnalysisUnkid, enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), enAction.ADD_ONE)
                    dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), CInt(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")), mintAssessAnalysisUnkid, enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), enAction.ADD_ONE, True)
                    'S.SANDEEP [04 JUN 2015] -- END
                Else
                    Dim iRow() As DataRow = dtCustomTabularGrid.Select("periodunkid <= 0")
                    For i As Integer = 0 To iRow.Length - 1
                        dtCustomTabularGrid.Rows.Remove(iRow(i))
                    Next
                End If
                If dtCustomTabularGrid.Rows.Count > 0 Then
                    If dtCustomTabularGrid.Rows.Count > 0 Then
                        dgvItems.Columns.Clear()
                    End If
                    dgvItems.DataSource = Nothing
                    dgvItems.AutoGenerateColumns = False
                    Dim iColName As String = String.Empty
                    For Each dCol As DataColumn In dtCustomTabularGrid.Columns
                        iColName = "" : iColName = "obj" & dCol.ColumnName
                        Dim dgvCol As New BoundField()
                        dgvCol.FooterText = iColName
                        dgvCol.ReadOnly = True
                        dgvCol.DataField = dCol.ColumnName
                        dgvCol.HeaderText = dCol.Caption
                        If dgvItems.Columns.Contains(dgvCol) = True Then Continue For
                        If dCol.Caption.Length <= 0 Then
                            dgvCol.Visible = False
                        End If
                        dgvItems.Columns.Add(dgvCol)
                        If dgvCol.FooterText = "objHeader_Name" Then dgvCol.Visible = False
                    Next
                    'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    Call SetDateFormat()
                    'Pinkal (16-Apr-2016) -- End
                    dgvItems.DataSource = dtCustomTabularGrid
                    dgvItems.DataBind()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEAnalysisMst = Nothing
        End Try
    End Sub

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)



    'Private Sub Fill_Custom_Evaluation_Data()
    '    Try
    '        If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
    '            Dim dtEval As DataTable
    '            'S.SANDEEP [10 DEC 2015] -- START
    '            'dtEval = New DataView(mdtCustomEvaluation, "custom_header = '" & dsHeaders.Tables(0).Rows(iHeaderId).Item("Name") & "' AND AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable
    '            dtEval = New DataView(mdtCustomEvaluation, "custom_header = '" & dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString.Replace("'", "''") & "' AND AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable
    '            'S.SANDEEP [10 DEC 2015] -- END

    '            dtCustomTabularGrid.Rows.Clear()
    '            Dim dFRow As DataRow = Nothing
    '            Dim iCustomId, iNewCustomId As Integer : iCustomId = 0 : iNewCustomId = 0
    '            For Each dRow As DataRow In dtEval.Rows
    '                If dRow.Item("AUD") = "D" Then Continue For
    '                If iCustomId <= 0 Then iCustomId = dRow.Item("customitemunkid")
    '                iNewCustomId = dRow.Item("customitemunkid")
    '                If iCustomId = iNewCustomId Then
    '                    dFRow = dtCustomTabularGrid.NewRow
    '                    dtCustomTabularGrid.Rows.Add(dFRow)
    '                End If
    '                Select Case CInt(dRow.Item("itemtypeid"))
    '                    Case clsassess_custom_items.enCustomType.FREE_TEXT
    '                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
    '                    Case clsassess_custom_items.enCustomType.SELECTION
    '                        If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
    '                            Select Case CInt(dRow.Item("selectionmodeid"))
    '                                Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
    '                                    Dim objCMaster As New clsCommon_Master
    '                                    objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
    '                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
    '                                    objCMaster = Nothing
    '                                Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
    '                                    Dim objCMaster As New clsassess_competencies_master
    '                                    objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
    '                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
    '                                    objCMaster = Nothing
    '                                Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
    '                                    Dim objEmpField1 As New clsassess_empfield1_master
    '                                    objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
    '                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
    '                                    objEmpField1 = Nothing
    '                            End Select
    '                        End If
    '                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
    '                        If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
    '                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
    '                        End If
    '                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
    '                        If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
    '                            If IsNumeric(dRow.Item("custom_value")) Then
    '                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
    '                            End If
    '                        End If
    '                End Select
    '                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
    '                dFRow.Item("periodunkid") = dRow.Item("periodunkid")
    '                dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
    '                dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
    '                dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
    '                dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
    '            Next
    '            If dtCustomTabularGrid.Rows.Count <= 0 Then
    '                dtCustomTabularGrid.Rows.Add(dtCustomTabularGrid.NewRow)
    '                dtCustomTabularGrid.Rows(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
    '            End If
    '            Call Fill_Custom_Grid(True)
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    Finally
    '    End Try
    'End Sub
    Private Sub Fill_Custom_Evaluation_Data()
        Dim strGUIDArray() As String = Nothing
        Dim objCCustomTran As New clscompeteny_customitem_tran
        Try
            'Gajanan [31-July-2020] -- Start
            objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)
            objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
            mdtCustomEvaluation = objCCustomTran._DataTable
            'Gajanan [31-July-2020] -- End

            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
                Dim dR = mdtCustomEvaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("custom_header") = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name") And x.Field(Of String)("AUD") <> "D")

                If dR.Count > 0 Then
                    strGUIDArray = dR.Select(Function(x) x.Field(Of String)("customanalysistranguid")).Distinct().ToArray
                End If

                Dim dFRow As DataRow = Nothing
                dtCustomTabularGrid.Rows.Clear()
                If strGUIDArray IsNot Nothing AndAlso strGUIDArray.Length > 0 Then
                    For Each Str As String In strGUIDArray
                        dFRow = dtCustomTabularGrid.NewRow
                        dtCustomTabularGrid.Rows.Add(dFRow)
                        For Each dRow As DataRow In mdtCustomEvaluation.Select("customanalysistranguid = '" & Str & "' AND AUD <> 'D'")
                            Select Case CInt(dRow.Item("itemtypeid"))
                                Case clsassess_custom_items.enCustomType.FREE_TEXT
                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
                                Case clsassess_custom_items.enCustomType.SELECTION
                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                        Select Case CInt(dRow.Item("selectionmodeid"))
                                            'S.SANDEEP [06-NOV-2017] -- START
                                            'Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                                'S.SANDEEP [06-NOV-2017] -- END
                                                Dim objCMaster As New clsCommon_Master
                                                objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                                objCMaster = Nothing
                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                                Dim objCMaster As New clsassess_competencies_master
                                                objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                                objCMaster = Nothing
                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                                Dim objEmpField1 As New clsassess_empfield1_master
                                                objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
                                                objEmpField1 = Nothing

                                                'S.SANDEEP |16-AUG-2019| -- START
                                                'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                            Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                                Dim objCMaster As New clsCommon_Master
                                                objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                                objCMaster = Nothing
                                                'S.SANDEEP |16-AUG-2019| -- END
                                        End Select
                                    End If
                                Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                                    End If
                                Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                        If IsNumeric(dRow.Item("custom_value")) Then
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
                                        End If
                                    End If
                            End Select
                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
                            dFRow.Item("periodunkid") = dRow.Item("periodunkid")
                            dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
                            dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
                            dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
                            dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
                        Next
                    Next
                End If

                If dtCustomTabularGrid.Rows.Count <= 0 Then
                    dtCustomTabularGrid.Rows.Add(dtCustomTabularGrid.NewRow)
                    dtCustomTabularGrid.Rows(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                End If
                Call Fill_Custom_Grid(True)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objCCustomTran = Nothing
        End Try
    End Sub
    'Shani (26-Sep-2016) -- End

    'S.SANDEEP |13-NOV-2020| -- START
    'ISSUE/ENHANCEMENT : COMPETENCIES
    Private Sub SetPerfSession(ByVal clsConfig As clsConfigOptions, ByVal intValue As Integer)
        Try
            If clsConfig IsNot Nothing Then
                Session("EvaluationTypeId") = clsevaluation_analysis_master.enPAEvalTypeId.EO_BOTH
                Session("RunCompetenceAssessmentSeparately") = clsConfig._RunCompetenceAssessmentSeparately
                Session("OrgPerf_EvaluationOrder") = clsConfig._Perf_EvaluationOrder
            End If
            Dim strValue As String = HttpContext.Current.Session("OrgPerf_EvaluationOrder")
            If CInt(intValue) = clsevaluation_analysis_master.enPAEvalTypeId.EO_COMPETENCE Then
                If strValue IsNot Nothing AndAlso strValue.Trim.Length > 0 Then
                    If Array.IndexOf(strValue.Split(CChar("|")), CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString()) <> -1 Then
                        Dim iarr As String() = strValue.Split(CChar("|"))
                        iarr = iarr.Select(Function(s) s.Replace(CInt(enEvaluationOrder.PE_BSC_SECTION).ToString(), "0")).ToArray()
                        strValue = String.Join("|", iarr)
                    ElseIf Array.IndexOf(strValue.Split(CChar("|")), CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString()) <= -1 Then
                        Dim iarr As String() = strValue.Split(CChar("|"))
                        iarr = iarr.Select(Function(s) s.Replace(CInt(enEvaluationOrder.PE_BSC_SECTION).ToString(), CInt(enEvaluationOrder.PE_COMPETENCY_SECTION).ToString())).ToArray()
                        strValue = String.Join("|", iarr)
                    End If
                End If
                HttpContext.Current.Session("EvaluationTypeId") = clsevaluation_analysis_master.enPAEvalTypeId.EO_COMPETENCE
            Else
                strValue = HttpContext.Current.Session("OrgPerf_EvaluationOrder")
                If CBool(HttpContext.Current.Session("RunCompetenceAssessmentSeparately")) = True Then HttpContext.Current.Session("EvaluationTypeId") = clsevaluation_analysis_master.enPAEvalTypeId.EO_SCORE_CARD
            End If
            HttpContext.Current.Session("Perf_EvaluationOrder") = strValue
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |13-NOV-2020| -- END

#End Region

#Region " Buttons Event "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or _
               CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then

                'Pinkal (15-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 53, "Please set following information Period to view assessment."), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 53, "Please set following information Period to view assessment."), Me)
                    'Pinkal (25-Jan-2022) -- End
                Else
                    'Pinkal (17-Jan-2022)-- Start
                    'Enhancement : [JIRA] (OLD-265) Allow to Add Leave Accrue for Rehired Staff.
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please set following information [Employee,Period] to view assessment."), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "Please set following information [Employee,Period] to view assessment."), Me)
                    'Pinkal (17-Jan-2022) -- End
                End If
                'Pinkal (15-Mar-2019) -- End
                Exit Sub
            End If

            ''Shani (26-Sep-2016) -- Start
            ''Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'RemoveHandler btnSearch.Click, AddressOf btnSearch_Click
            'Call cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs)
            'AddHandler btnSearch.Click, AddressOf btnSearch_Click
            ''Shani (26-Sep-2016) -- End

            Dim objEvaluation As New clsevaluation_analysis_master
            Dim sMsg As String = objEvaluation.IsPlanningDone(Session("Perf_EvaluationOrder"), CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)), CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)), Session("Self_Assign_Competencies"), enAssessmentMode.SELF_ASSESSMENT, True)

            If sMsg.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage("Sorry, you cannot view assessment as no assessment is done for particular period.", Me)
                objEvaluation = Nothing
                Exit Sub
            End If

            Dim blnIsExistAssessorAssessment As Boolean = False
            Dim blnIsExistSelfAssessment As Boolean = False
            blnIsExistSelfAssessment = objEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False)
            blnIsExistAssessorAssessment = objEvaluation.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False)

            If blnIsExistSelfAssessment = False And blnIsExistAssessorAssessment = False Then
                DisplayMessage.DisplayMessage("Sorry, you cannot view assessment as no assessment is done for particular period.", Me)
                btnNext.Visible = False
                btnBack.Visible = False
                btnAppRejAssessment.Visible = False
                Exit Sub
            Else
                btnNext.Visible = True
                btnBack.Visible = True
                btnAppRejAssessment.Visible = True
            End If

            If objEvaluation IsNot Nothing Then objEvaluation = Nothing

            If dsHeaders.Tables(0).Rows.Count > 0 Then
                btnNext.Enabled = True : iHeaderId = -1 : btnNext_Click(sender, e)

                If iHeaderId > 0 Then
                    btnBack.Enabled = True
                Else
                    btnBack.Enabled = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                If iHeaderId < dsHeaders.Tables(0).Rows.Count - 1 Then
                    iHeaderId += 1
                    btnBack.Enabled = True
                End If
                If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
                    btnNext.Enabled = False
                End If
                objlblValue1.Text = "" : objlblValue2.Text = ""
                objlblValue3.Text = "" : objlblValue4.Text = ""
                Call PanelVisibility()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Try
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                iHeaderId = iHeaderId - 1
                If iHeaderId <= 0 Then
                    btnBack.Enabled = False
                    btnNext.Enabled = True
                End If
                If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
                    btnNext.Enabled = False
                Else
                    btnNext.Enabled = True
                End If
                Call PanelVisibility()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            If Me.Request.QueryString.Count > 0 Then
                Response.Redirect(Session("servername") & "../../Index.aspx", False)
            Else
                Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    If Me.Request.QueryString.Count > 0 Then
    '        Response.Redirect(Session("servername") & "../../Index.aspx", False)
    '    Else
    '        Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
    '    End If
    'End Sub
    'SHANI [01 FEB 2015]--END

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            objlblCaption.Text = ""
            'S.SANDEEP [21 JAN 2015] -- START
            dgvBSC.DataSource = Nothing
            dgvBSC.DataBind()
            dgvGE.DataSource = Nothing
            dgvGE.DataBind()
            dgvItems.DataSource = Nothing
            dgvItems.DataBind()
            'S.SANDEEP [21 JAN 2015] -- END
            btnNext.Enabled = False : btnBack.Enabled = False
            objlblValue1.Visible = False : objlblValue2.Visible = False
            objlblValue3.Visible = False : objlblValue4.Visible = False
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                cboEmployee.SelectedValue = 0
            ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                If Me.Request.QueryString.Count <= 0 Then
                    cboEmployee.SelectedValue = Session("Employeeunkid")
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub



    'Shani(24-JAN-2017) -- Start
    'Enhancement : 
    Protected Sub btnAppRejAssessment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAppRejAssessment.Click
        Dim blnIsExistAssessorAssessment As Boolean = False
        Dim blnIsExistReviewerAssessment As Boolean = True
        Dim objAckEval As New clsevaluation_acknowledgement_tran
        Dim objEAnalysisMst As New clsevaluation_analysis_master
        Try
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or _
              CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then

                'Pinkal (15-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 53, "Please set following information Period to view assessment."), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 53, "Please set following information Period to view assessment."), Me)
                    'Pinkal (25-Jan-2022) -- End
                Else
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please set following information [Employee,Period] to view assessment."), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 2, "Please set following information [Employee,Period] to view assessment."), Me)
                    'Pinkal (25-Jan-2022) -- End
                End If
                'Pinkal (15-Mar-2019) -- End
                Exit Sub
            End If

            blnIsExistAssessorAssessment = objEAnalysisMst.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))

            If CBool(Session("IsCompanyNeedReviewer")) Then
                blnIsExistReviewerAssessment = objEAnalysisMst.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
                pnlAppRejAssessment_Reviewer.Visible = True
            Else
                pnlAppRejAssessment_Reviewer.Visible = False
            End If

            If blnIsExistAssessorAssessment = False And blnIsExistReviewerAssessment = False Then
                'Pinkal (25-Jan-2022) -- Start
                'Enhancement NMB  - Language Change in PM Module.	
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "Assessment Not Exist."), Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 101, "Assessment Not Exist."), Me)
                'Pinkal (25-Jan-2022) -- End
                Exit Sub
            End If

            'S.SANDEEP [25-JAN-2017] -- START
            'ISSUE/ENHANCEMENT : Pefromance Module Left Out Changes
            Dim dtExist As DataTable
            dtExist = objAckEval.IsExists(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
            If dtExist.Rows.Count <= 0 Then
                dtExist.Dispose() : objAckEval = Nothing
                popup_AppRejAssessment.Show()
            Else
                dtExist.Dispose() : objAckEval = Nothing
                cnfEdit.Title = "Aruti"

                'Pinkal (25-Jan-2022) -- Start
                'Enhancement NMB  - Language Change in PM Module.	
                'cnfEdit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 104, "You have already provided the acknowledgement for the selected assessment process. would you like to open it in edit mode?")
                cnfEdit.Message = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 104, "You have already provided the acknowledgement for the selected assessment process. would you like to open it in edit mode?")
                'Pinkal (25-Jan-2022) -- End


                cnfEdit.Show()
            End If
            'S.SANDEEP [25-JAN-2017] -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objAckEval = Nothing
            objEAnalysisMst = Nothing
        End Try
    End Sub
    'Shani(24-JAN-2017) -- End

    'S.SANDEEP [25-JAN-2017] -- START
    'ISSUE/ENHANCEMENT : Pefromance Module Left Out Changes
    Protected Sub btnpostComment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnpostComment.Click
        Dim blnFlag As Boolean = False
        'S.SANDEEP [29-NOV-2017] -- START
        'ISSUE/ENHANCEMENT : REF-ID # (38,41)
        Dim strTransactionIds As String = String.Empty
        'S.SANDEEP [29-NOV-2017] -- END
        Try
            If radAssessment_Assessor_Approve.Checked = False AndAlso radAssessment_Assessor_Reject.Checked = False Then
                'Pinkal (25-Jan-2022) -- Start
                'Enhancement NMB  - Language Change in PM Module.	
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 102, "Sorry, Please select atleast one status to continue."), Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 102, "Sorry, Please select atleast one status to continue."), Me)
                'Pinkal (25-Jan-2022) -- End
                popup_AppRejAssessment.Show()
                Exit Sub
            End If

            If txtAppRejAssessment_Assessor_Remark.Text.Trim.Length <= 0 Then
                'Pinkal (25-Jan-2022) -- Start
                'Enhancement NMB  - Language Change in PM Module.	
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 103, "Sorry, Please provide some remark to justify your selected status."), Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 103, "Sorry, Please provide some remark to justify your selected status."), Me)
                'Pinkal (25-Jan-2022) -- End
                popup_AppRejAssessment.Show()
                Exit Sub
            End If

            If pnlAppRejAssessment_Reviewer.Visible = True Then
                If radAssessment_Reviewer_Approve.Checked = False AndAlso radAssessment_Reviewer_Reject.Checked = False Then
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 102, "Sorry, Please select atleast one status to continue."), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 102, "Sorry, Please select atleast one status to continue."), Me)
                    'Pinkal (25-Jan-2022) -- End
                    popup_AppRejAssessment.Show()
                    Exit Sub
                End If
                If txtAppRejAssessment_Reviewer_Remark.Text.Trim.Length <= 0 Then
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 103, "Sorry, Please provide some remark to justify your selected status."), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 103, "Sorry, Please provide some remark to justify your selected status."), Me)
                    'Pinkal (25-Jan-2022) -- End
                    popup_AppRejAssessment.Show()
                    Exit Sub
                End If
            End If

            Dim objAckEval As New clsevaluation_acknowledgement_tran
            Dim mDicEvalIds As New Dictionary(Of Integer, Integer)
            mDicEvalIds = objAckEval.GetEvalAnalysisIds(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
            Dim dtExist As DataTable
            dtExist = objAckEval.IsExists(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
            Dim blnUpdate As Boolean = False
            If mDicEvalIds.Keys.Count > 0 Then
                For Each iKey As Integer In mDicEvalIds.Keys
                    blnUpdate = False : Dim ival As Integer = iKey
                    If iKey <> enAssessmentMode.SELF_ASSESSMENT Then
                        If dtExist.AsEnumerable().Where(Function(x) x.Field(Of Integer)("analysisunkid") = mDicEvalIds(ival)).Count > 0 Then
                            Dim iackval As Integer = dtExist.AsEnumerable().Where(Function(x) x.Field(Of Integer)("analysisunkid") = mDicEvalIds(ival)).Select(Function(y) y.Field(Of Integer)("acknowledgementtranunkid")).FirstOrDefault()
                            blnUpdate = True
                            'S.SANDEEP [29-NOV-2017] -- START
                            'ISSUE/ENHANCEMENT : REF-ID # (38,41)
                            If strTransactionIds.Trim.Length > 0 Then
                                strTransactionIds &= "," & iackval.ToString()
                            Else
                                strTransactionIds &= iackval.ToString()
                            End If
                            'S.SANDEEP [29-NOV-2017] -- END
                            objAckEval._Acknowledgementtranunkid = iackval
                        End If
                        Select Case iKey
                            Case enAssessmentMode.APPRAISER_ASSESSMENT
                                objAckEval._Acknowledgement_comments = txtAppRejAssessment_Assessor_Remark.Text
                                If radAssessment_Assessor_Approve.Checked = True Then
                                    objAckEval._Statusunkid = clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.AGREE
                                ElseIf radAssessment_Assessor_Reject.Checked = True Then
                                    objAckEval._Statusunkid = clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.DISAGREE
                                End If
                            Case enAssessmentMode.REVIEWER_ASSESSMENT
                                objAckEval._Acknowledgement_comments = txtAppRejAssessment_Reviewer_Remark.Text
                                If radAssessment_Reviewer_Approve.Checked = True Then
                                    objAckEval._Statusunkid = clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.AGREE
                                ElseIf radAssessment_Reviewer_Reject.Checked = True Then
                                    objAckEval._Statusunkid = clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.DISAGREE
                                End If
                        End Select
                        objAckEval._Acknowledgement_date = Now
                        objAckEval._Analysisunkid = mDicEvalIds(iKey)
                        objAckEval._Employeeunkid = CInt(cboEmployee.SelectedValue)
                        objAckEval._IpAddress = Session("IP_ADD")
                        objAckEval._Machine_Name = Session("HOST_NAME")
                        objAckEval._IsVoid = False
                        objAckEval._Loginemployeeunkid = Session("Employeeunkid")
                        objAckEval._Userunkid = Session("UserId")
                        objAckEval._Periodunkid = CInt(cboPeriod.SelectedValue)
                        objAckEval._Voiddatetime = Nothing
                        objAckEval._VoidReason = ""
                        objAckEval._WebForm_Name = mstrModuleName
                        If blnUpdate Then
                            blnFlag = objAckEval.Update()
                        Else
                            blnFlag = objAckEval.Insert()
                            'S.SANDEEP [29-NOV-2017] -- START
                            'ISSUE/ENHANCEMENT : REF-ID # (38,41)
                            If strTransactionIds.Trim.Length > 0 Then
                                strTransactionIds &= "," & objAckEval._Acknowledgementtranunkid.ToString()
                            Else
                                strTransactionIds &= objAckEval._Acknowledgementtranunkid.ToString()
                            End If
                            'S.SANDEEP [29-NOV-2017] -- END
                        End If
                        If blnFlag = False Then
                            Exit For
                        End If
                    End If
                Next
                'S.SANDEEP [29-NOV-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # (38,41)
                If blnFlag AndAlso _
                    CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) > 0 AndAlso _
                    CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) > 0 Then

                    Dim eLMode As Integer
                    If (Session("LoginBy") = Global.User.en_loginby.User) Then
                        eLMode = enLogin_Mode.MGR_SELF_SERVICE
                    Else
                        eLMode = enLogin_Mode.EMP_SELF_SERVICE
                    End If
                    objAckEval.Send_Notification(strTransactionIds, _
                                                 CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()), _
                                                 CType(eLMode, Aruti.Data.enLogin_Mode), _
                                                 Session("UserId"), Session("Ntf_FinalAcknowledgementUserIds").ToString(), _
                                                 Session("Employeeunkid"), mstrModuleName, Session("Database_Name"), Session("CompanyUnkId"))

                    'S.SANDEEP |12-FEB-2019| -- START
                    'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                    DisplayMessage.DisplayMessage("Assessment acknowledgement submitted successfully.", Me)
                    'S.SANDEEP |12-FEB-2019| -- END
                End If
                'S.SANDEEP [29-NOV-2017] -- END
            End If
            radAssessment_Assessor_Approve.Checked = False
            radAssessment_Assessor_Reject.Checked = False
            txtAppRejAssessment_Assessor_Remark.Text = ""
            radAssessment_Reviewer_Approve.Checked = False
            radAssessment_Reviewer_Reject.Checked = False
            txtAppRejAssessment_Reviewer_Remark.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cnfEdit_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfEdit.buttonYes_Click
        Try




            Dim dtExist As DataTable : Dim objAckEval As New clsevaluation_acknowledgement_tran
            dtExist = objAckEval.IsExists(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue))
            If dtExist.Rows.Count > 0 Then
                For Each row As DataRow In dtExist.Rows
                    Select Case row.Item("assessmodeid")
                        Case enAssessmentMode.APPRAISER_ASSESSMENT
                            If row.Item("statusunkid") = clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.AGREE Then
                                radAssessment_Assessor_Approve.Checked = True
                            ElseIf row.Item("statusunkid") = clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.DISAGREE Then
                                radAssessment_Assessor_Reject.Checked = True
                            End If
                            txtAppRejAssessment_Assessor_Remark.Text = row.Item("acknowledgement_comments")
                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                            If row.Item("statusunkid") = clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.AGREE Then
                                radAssessment_Reviewer_Approve.Checked = True
                            ElseIf row.Item("statusunkid") = clsevaluation_acknowledgement_tran.enEvaluationAcknowledgement.DISAGREE Then
                                radAssessment_Reviewer_Reject.Checked = True
                            End If
                            txtAppRejAssessment_Reviewer_Remark.Text = row.Item("acknowledgement_comments")
                    End Select
                Next
            End If
            popup_AppRejAssessment.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP [25-JAN-2017] -- END


#End Region

#Region " GridView Event "

    Protected Sub dgvGE_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvGE.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim intCount As Integer = 1
                If CBool(DataBinder.Eval(e.Item.DataItem, "IsPGrp")) = True Then
                    For i = 1 To dgvGE.Columns.Count - 1
                        If dgvGE.Columns(i).Visible Then
                            e.Item.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Item.Cells(disGEColumns("dgcolheval_itemGE")).ColumnSpan = intCount
                    e.Item.Cells(disGEColumns("dgcolheval_itemGE")).CssClass = "main-group-header"
                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then
                    intCount = 1
                    For i = 1 To dgvGE.Columns.Count - 2
                        If dgvGE.Columns(i).Visible Then
                            e.Item.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Item.Cells(disGEColumns("dgcolheval_itemGE")).ColumnSpan = intCount
                    e.Item.Cells(disGEColumns("dgcolheval_itemGE")).CssClass = "group-header"
                    e.Item.Cells(disGEColumns("objdgcolhInformation")).CssClass = "group-header"
                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then
                    e.Item.Cells(disGEColumns("dgcolheval_itemGE")).Text = "&nbsp;" & e.Item.Cells(disGEColumns("dgcolheval_itemGE")).Text

                    'S.SANDEEP [20-SEP-2017] -- START
                    'ISSUE/ENHANCEMENT : FOUND ISSUES IN C# & FIXED IN VB
                    If CBool(Session("IsUseAgreedScore")) Then
                        If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNeeded Then
                            e.Item.Cells(disGEColumns("dgcolhrselfGE")).Text = e.Item.Cells(disGEColumns("dgcolhAgreedScoreGE")).Text
                        End If
                    End If
                    'S.SANDEEP [20-SEP-2017] -- END


                End If
                If IsNumeric(e.Item.Cells(disGEColumns("dgcolhGEWeight")).Text) Then
                    iWeightTotal = iWeightTotal + CDec(e.Item.Cells(disGEColumns("dgcolhGEWeight")).Text)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgvBSC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvBSC.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Call SetDateFormat()

                If CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then
                    For i = xVal To dgvBSC.Columns.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next
                    e.Item.Cells(xVal - 1).ColumnSpan = dgvBSC.Columns.Count - 1
                    e.Item.Cells(xVal - 1).CssClass = "main-group-header"
                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then
                    e.Item.Cells(xVal - 1).Text = "&nbsp;" & e.Item.Cells(disBSCColumns("objdgcolhBSCField1")).Text
                    'If e.Item.Cells(disBSCColumns("dgcolhSDate")).Text.ToString().Trim <> "" AndAlso e.Item.Cells(disBSCColumns("dgcolhSDate")).Text.Trim <> "&nbsp;" Then
                    '    e.Item.Cells(disBSCColumns("dgcolhSDate")).Text = CDate(e.Item.Cells(disBSCColumns("dgcolhSDate")).Text).Date.ToShortDateString
                    'End If

                    'If e.Item.Cells(disBSCColumns("dgcolhEDate")).Text.ToString().Trim <> "" AndAlso e.Item.Cells(disBSCColumns("dgcolhEDate")).Text.Trim <> "&nbsp;" Then
                    '    e.Item.Cells(disBSCColumns("dgcolhEDate")).Text = CDate(e.Item.Cells(disBSCColumns("dgcolhEDate")).Text).Date.ToShortDateString
                    'End If

                    'S.SANDEEP [20-SEP-2017] -- START
                    'ISSUE/ENHANCEMENT : FOUND ISSUES IN C# & FIXED IN VB
                    If CBool(Session("IsUseAgreedScore")) Then
                        If CInt(Session("ReviewerScoreSetting")) = enReviewerScoreSetting.ReviewerScoreNeeded Then
                            e.Item.Cells(disBSCColumns("dgcolhrselfBSC")).Text = e.Item.Cells(disBSCColumns("dgcolhAgreedScoreBSC")).Text.ToString()
                        End If
                    End If
                    'S.SANDEEP [20-SEP-2017] -- END
                End If
                If e.Item.Cells(disBSCColumns("dgcolhBSCWeight")).Text.Trim <> "" AndAlso e.Item.Cells(disBSCColumns("dgcolhBSCWeight")).Text <> "&nbsp;" Then
                    If IsNumeric(e.Item.Cells(disBSCColumns("dgcolhBSCWeight")).Text) Then
                        iWeightTotal = iWeightTotal + CDec(e.Item.Cells(disBSCColumns("dgcolhBSCWeight")).Text)
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub link_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            txtData.Text = ""
            Dim lnkWhatIsThis As LinkButton = CType(sender, LinkButton)
            Dim xRow As DataGridItem = CType(lnkWhatIsThis.NamingContainer, DataGridItem)
            If CBool(xRow.Cells(disGEColumns("objdgcolhIsGrpGE")).Text) = True Then
                Dim objCOMaster As New clsCommon_Master
                objCOMaster._Masterunkid = CInt(xRow.Cells(disGEColumns("objdgcolhGrpIdGE")).Text)
                If objCOMaster._Description <> "" Then
                    txtData.Text = objCOMaster._Description
                    popup_ComInfo.Show()
                Else
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
                    'Pinkal (25-Jan-2022) -- End
                End If
                objCOMaster = Nothing
            ElseIf CBool(xRow.Cells(disGEColumns("objdgcolhIsGrpGE")).Text) = False Then
                Dim objCPMsater As New clsassess_competencies_master
                objCPMsater._Competenciesunkid = CInt(xRow.Cells(disGEColumns("objdgcolhcompetenciesunkidGE")).Text)
                If objCPMsater._Description <> "" Then
                    txtData.Text = objCPMsater._Description
                    popup_ComInfo.Show()
                Else
                    'Pinkal (25-Jan-2022) -- Start
                    'Enhancement NMB  - Language Change in PM Module.	
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
                    'Pinkal (25-Jan-2022) -- End
                End If
                objCPMsater = Nothing
            End If
            dgvGE.DataSource = dtGE_TabularGrid
            dgvGE.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Shani (31-Aug-2016) -- End
#End Region

#Region " Combobox Event "

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Dim objMapping As New clsAssess_Field_Mapping
        Dim objCCustomTran As New clscompeteny_customitem_tran
        Try

            iLinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            iMappingUnkid = objMapping.Get_MappingUnkId(CInt(cboPeriod.SelectedValue))
            objMapping._Mappingunkid = iMappingUnkid
            objMapping = Nothing

            Dim objFMaster As New clsAssess_Field_Master
            iExOrdr = objFMaster.Get_Field_ExOrder(iLinkedFieldId, True)
            objFMaster = Nothing

            objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)
            objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
            'S.SANDEEP [29 DEC 2015] -- START
            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'objCCustomTran._AllowCustomItemInPlanning = CBool(Session("IncludeCustomItemInPlanning"))
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP [29 DEC 2015] -- END
            mdtCustomEvaluation = objCCustomTran._DataTable

            If CInt(cboPeriod.SelectedValue) > 0 Then

                'S.SANDEEP |13-NOV-2020| -- START
                'ISSUE/ENHANCEMENT : COMPETENCIES
                Dim objEMaster As New clsevaluation_analysis_master
                Dim intAnalysisId As Integer = -1
                intAnalysisId = objEMaster.GetAnalusisUnkid(CInt(cboEmployee.SelectedValue), enAssessmentMode.SELF_ASSESSMENT, CInt(cboPeriod.SelectedValue))
                If intAnalysisId > 0 Then
                    objEMaster._Analysisunkid = intAnalysisId
                    Call SetPerfSession(Nothing, objEMaster._EvalTypeId)
                End If
                objEMaster = Nothing
                'S.SANDEEP |13-NOV-2020| -- END


                If CStr(Session("Perf_EvaluationOrder")).Trim.Length > 0 Then
                    Dim objCHeader As New clsassess_custom_header
                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                    dsHeaders.Tables(0).Rows.Clear()
                    Dim xRow As DataRow = Nothing
                    Dim iOrdr() As String = CStr(Session("Perf_EvaluationOrder")).Split("|")
                    If iOrdr.Length > 0 Then
                        Select Case CInt(iOrdr(0))
                            Case enEvaluationOrder.PE_BSC_SECTION
                                xRow = dsHeaders.Tables(0).NewRow

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                'xRow.Item("Id") = -3 : xRow.Item("Name") = "Goals Evaluation" : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)

                                'Pinkal (25-Jan-2022) -- Start
                                'Enhancement NMB  - Language Change in PM Module.	
                                'xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Pinkal (25-Jan-2022) -- End


                                'Shani (26-Sep-2016) -- End

                                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                                xRow = dsHeaders.Tables(0).NewRow

                                'Pinkal (25-Jan-2022) -- Start
                                'Enhancement NMB  - Language Change in PM Module.	
                                'xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Pinkal (25-Jan-2022) -- End


                                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                            Case enEvaluationOrder.PE_CUSTOM_SECTION
                                dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                        End Select
                        Select Case CInt(iOrdr(1))
                            Case enEvaluationOrder.PE_BSC_SECTION
                                xRow = dsHeaders.Tables(0).NewRow

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                'xRow.Item("Id") = -3 : xRow.Item("Name") = "Goals Evaluation" : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)

                                'Pinkal (25-Jan-2022) -- Start
                                'Enhancement NMB  - Language Change in PM Module.	
                                'xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Pinkal (25-Jan-2022) -- End


                                'Shani (26-Sep-2016) -- End

                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                                xRow = dsHeaders.Tables(0).NewRow

                                'Pinkal (25-Jan-2022) -- Start
                                'Enhancement NMB  - Language Change in PM Module.	
                                'xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Pinkal (25-Jan-2022) -- End

                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_CUSTOM_SECTION
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    Dim dsList As New DataSet
                                    dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                    dsHeaders.Tables(0).Merge(dsList.Tables(0), True)
                                Else
                                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                End If
                        End Select
                        Select Case CInt(iOrdr(2))
                            Case enEvaluationOrder.PE_BSC_SECTION
                                xRow = dsHeaders.Tables(0).NewRow

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                'xRow.Item("Id") = -3 : xRow.Item("Name") = "Goals Evaluation" : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)

                                'Pinkal (25-Jan-2022) -- Start
                                'Enhancement NMB  - Language Change in PM Module.	
                                'xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Pinkal (25-Jan-2022) -- End


                                'Shani (26-Sep-2016) -- End

                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                                xRow = dsHeaders.Tables(0).NewRow

                                'Pinkal (25-Jan-2022) -- Start
                                'Enhancement NMB  - Language Change in PM Module.	
                                'xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(Session("Database_Name").ToString(), CInt(Session("LangId")), mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                'Pinkal (25-Jan-2022) -- End


                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_CUSTOM_SECTION
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    Dim dsList As New DataSet
                                    dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                    dsHeaders.Tables(0).Merge(dsList.Tables(0), True)
                                Else
                                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                End If
                        End Select
                    End If
                    objCHeader = Nothing
                End If
            Else
                If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables.Count > 0 Then
                    dsHeaders.Tables(0).Rows.Clear()
                End If
            End If

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
                btnSearch_Click(New Object(), New EventArgs())
            End If
            'S.SANDEEP |12-FEB-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMapping = Nothing
            objCCustomTran = Nothing
        End Try
    End Sub

#End Region

End Class

'Partial Class wPg_ViewPerfEvaluation
'    Inherits Basepage

'#Region " Private Variables "

'    Private DisplayMessage As New CommonCodes
'    Private objEAnalysisMst As New clsevaluation_analysis_master
'    Private objGoalsTran As New clsgoal_analysis_tran
'    Private objCAssessTran As New clscompetency_analysis_tran
'    Private objCCustomTran As New clscompeteny_customitem_tran
'    Private mdtCustomEvaluation As DataTable
'    Private dtBSC_TabularGrid As New DataTable
'    Private dtGE_TabularGrid As New DataTable
'    Private dtCustomTabularGrid As New DataTable
'    Private iWeightTotal As Decimal = 0
'    Private dsHeaders As New DataSet
'    Private iHeaderId As Integer = 0
'    Private iExOrdr As Integer = 0
'    Private iLinkedFieldId As Integer
'    Private iMappingUnkid As Integer
'    Private xVal As Integer = 1
'    Private dtCItems As New DataTable
'    Private objCONN As SqlConnection
'    Private mintAssessAnalysisUnkid As Integer = -1
'    Private mstrModuleName As String = "frmPerformanceEvaluation"

'#End Region

'#Region " Page Event "

'    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try

'            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
'                objCONN = Nothing
'                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
'                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
'                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
'                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
'                    objCONN = New SqlConnection
'                    objCONN.ConnectionString = constr
'                    objCONN.Open()
'                    HttpContext.Current.Session("gConn") = objCONN
'                End If
'                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")
'                If arr.Length = 3 Then
'                    Try
'                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
'                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
'                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
'                        Else
'                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
'                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
'                        End If

'                    Catch ex As Exception
'                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
'                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
'                    End Try
'                End If

'                HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
'                HttpContext.Current.Session("Employeeunkid") = CInt(arr(1))
'                Me.ViewState.Add("periodid", CInt(arr(2)))

'                'Sohail (30 Mar 2015) -- Start
'                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
'                'Dim objCommon As New CommonCodes
'                'objCommon.GetCompanyYearInfo(CInt(Session("CompanyUnkId")))
'                Dim strError As String = ""
'                If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
'                    DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
'                    Exit Sub
'                End If
'                'Sohail (30 Mar 2015) -- End
'                HttpContext.Current.Session("Database_Name") = Session("Database_Name")
'                gobjConfigOptions = New clsConfigOptions
'                gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
'                ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
'                Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
'                CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("Database_Name").ToString)

'                ArtLic._Object = New ArutiLic(False)
'                If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
'                    Dim objGroupMaster As New clsGroup_Master
'                    objGroupMaster._Groupunkid = 1
'                    ArtLic._Object.HotelName = objGroupMaster._Groupname
'                End If

'                If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
'                    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
'                    Exit Sub
'                End If

'                If ConfigParameter._Object._IsArutiDemo Then
'                    If ConfigParameter._Object._IsExpire Then
'                        DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
'                        Exit Try
'                    Else
'                        If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
'                            DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
'                            Exit Try
'                        End If
'                    End If
'                End If

'                Dim clsConfig As New clsConfigOptions
'                clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
'                If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
'                    Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
'                Else
'                    Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
'                End If

'                Session("IsAllowFinalSave") = clsConfig._IsAllowFinalSave
'                Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp
'                Session("EmployeeAsOnDate") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)
'                Session("IsBSC_ByEmployee") = clsConfig._IsBSC_ByEmployee
'                Session("IsCompanyNeedReviewer") = clsConfig._IsCompanyNeedReviewer
'                Session("Assessment_Instructions") = clsConfig._Assessment_Instructions
'                Session("ViewTitles_InPlanning") = clsConfig._ViewTitles_InPlanning
'                Session("FollowEmployeeHierarchy") = clsConfig._FollowEmployeeHierarchy
'                Session("CascadingTypeId") = clsConfig._CascadingTypeId
'                Session("ScoringOptionId") = clsConfig._ScoringOptionId
'                Session("Perf_EvaluationOrder") = clsConfig._Perf_EvaluationOrder
'                Session("ViewTitles_InEvaluation") = clsConfig._ViewTitles_InEvaluation
'                Session("ConsiderItemWeightAsNumber") = clsConfig._ConsiderItemWeightAsNumber
'                'S.SANDEEP [09 OCT 2015] -- START
'                Session("Self_Assign_Competencies") = clsConfig._Self_Assign_Competencies
'                'S.SANDEEP [09 OCT 2015] -- END


'                Dim objPwdOpt As New clsPassowdOptions
'                Session("IsEmployeeAsUser") = objPwdOpt._IsEmployeeAsUser
'                Dim xUName, xPasswd As String : xUName = "" : xPasswd = ""

'                'Shani (05-May-2016) -- Start




'                'If Session("IsEmployeeAsUser") = True Then
'                '    Dim objUser As New clsUserAddEdit

'                '    'Shani(19-MAR-2016) -- Start
'                '    'objUser._Userunkid = CInt(Me.ViewState("employeeunkid"))
'                '    objUser._Userunkid = CInt(HttpContext.Current.Session("employeeunkid"))
'                '    'Shani(19-MAR-2016) -- End

'                '    xUName = objUser._Username : xPasswd = objUser._Password
'                '    objUser = Nothing
'                'Else
'                '    Dim objEmp As New clsEmployee_Master

'                '    'Shani(20-Nov-2015) -- Start
'                '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                '    'objEmp._Employeeunkid = CInt(Session("Employeeunkid"))
'                '    objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(Session("Employeeunkid"))
'                '    'Shani(20-Nov-2015) -- End

'                '    xUName = objEmp._Displayname : xPasswd = objEmp._Password
'                '    objEmp = Nothing
'                'End If

'                Dim objEmp As New clsEmployee_Master
'                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate"))) = CInt(Session("Employeeunkid"))
'                xUName = objEmp._Displayname : xPasswd = objEmp._Password
'                'Sohail (24 Nov 2016) -- Start
'                'CCBRT Enhancement - 65.1 - store last theme selected and last view selected for user and employee login in Self Service.
'                'HttpContext.Current.Session("E_Theme_id") = objEmp._Theme_Id
'                'HttpContext.Current.Session("E_Lastview_id") = objEmp._LastView_Id
'                'HttpContext.Current.Session("Theme_id") = objEmp._Theme_Id
'                'HttpContext.Current.Session("Lastview_id") = objEmp._LastView_Id
'                'Sohail (24 Nov 2016) -- End
'                objEmp = Nothing
'                'Shani (05-May-2016) -- End

'                'Sohail (21 Mar 2015) -- Start
'                'Enhancement - New UI Notification Link Changes.
'                'Dim clsuser As New User(xUName, xPasswd, Global.User.en_loginby.Employee, Session("Database_Name"))
'                Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
'                Call GetDatabaseVersion()
'                'Shani (28-Jul-2016) -- Start
'                'Issue - 
'                'Dim clsuser As New User(xUName, xPasswd, Session("Database_Name"))
'                'Sohail (21 Mar 2015) -- End
'                'HttpContext.Current.Session("clsuser") = clsuser
'                'HttpContext.Current.Session("UserName") = clsuser.UserName
'                'HttpContext.Current.Session("Firstname") = clsuser.Firstname
'                'HttpContext.Current.Session("Surname") = clsuser.Surname
'                'HttpContext.Current.Session("MemberName") = clsuser.MemberName
'                'Shani (28-Jul-2016) -- End
'                HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.Employee
'                'Shani(19-MAR-2016) -- Start
'                HttpContext.Current.Session("DisplayName") = xUName
'                'Shani (28-Jul-2016) -- Start
'                'Issue - 
'                HttpContext.Current.Session("UserName") = xUName
'                HttpContext.Current.Session("MemberName") = xUName
'                'Shani (28-Jul-2016) -- End
'                'Shani(19-MAR-2016) -- End

'                'Sohail (30 Mar 2015) -- Start
'                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
'                'Shani (28-Jul-2016) -- Start
'                'Issue - 
'                'HttpContext.Current.Session("UserId") = clsuser.UserID
'                'HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
'                'HttpContext.Current.Session("Password") = clsuser.password
'                'HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
'                'HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
'                'Shani (28-Jul-2016) -- End

'                strError = ""
'                If SetUserSessions(strError) = False Then
'                    DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
'                    Exit Sub
'                End If

'                strError = ""
'                If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
'                    DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
'                    Exit Sub
'                End If
'                'Sohail (30 Mar 2015) -- End

'                btnNext.Enabled = False : btnBack.Enabled = False
'                objlblValue1.Visible = False : objlblValue2.Visible = False
'                objlblValue3.Visible = False : objlblValue4.Visible = False

'                Call FillCombo() : objlblCaption.Text = ""
'                cboEmployee.SelectedValue = CInt(Session("Employeeunkid"))
'                cboEmployee.Enabled = False

'                cboPeriod.SelectedValue = CInt(Me.ViewState("periodid"))
'                Call cboPeriod_SelectedIndexChanged(New Object, New EventArgs)
'                cboPeriod.Enabled = False

'                objCCustomTran._EmployeeId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue))
'                'S.SANDEEP [29 DEC 2015] -- START

'                'Shani (26-Sep-2016) -- Start
'                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                'objCCustomTran._AllowCustomItemInPlanning = CBool(Session("IncludeCustomItemInPlanning"))
'                'Shani (26-Sep-2016) -- End


'                objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
'                'S.SANDEEP [29 DEC 2015] -- END
'                mdtCustomEvaluation = objCCustomTran._DataTable

'                btnReset.Enabled = False

'                'Sohail (30 Mar 2015) -- Start
'                'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
'                'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.EMP_SELF_SERVICE, Session("Employeeunkid"), , Session("Database_Name"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
'                'Sohail (30 Mar 2015) -- End
'                HttpContext.Current.Session("Login") = True

'                GoTo Link
'            End If

'            'Shani (28-Jul-2016) -- Start
'            'Issue - 
'            'If Session("clsuser") Is Nothing Then
'            '    Exit Sub
'            'End If
'            'Shani (28-Jul-2016) -- End
'            If IsPostBack = False Then
'                'Shani (28-Jul-2016) -- Start
'                'Issue - 
'                If Session("clsuser") Is Nothing Then
'                    Exit Sub
'                End If
'                'Shani (28-Jul-2016) -- End
'                objlblCaption.Text = ""
'                btnNext.Enabled = False : btnBack.Enabled = False
'                objlblValue1.Visible = False : objlblValue2.Visible = False
'                objlblValue3.Visible = False : objlblValue4.Visible = False

'                Call FillCombo()

'                objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)
'                'S.SANDEEP [29 DEC 2015] -- START

'                'Shani (26-Sep-2016) -- Start
'                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                'objCCustomTran._AllowCustomItemInPlanning = CBool(Session("IncludeCustomItemInPlanning"))
'                'Shani (26-Sep-2016) -- End


'                objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
'                'S.SANDEEP [29 DEC 2015] -- END
'                mdtCustomEvaluation = objCCustomTran._DataTable
'                'Call Fill_BSC_Evaluation()
'                'Call Fill_GE_Evaluation()
'                'Call Fill_Custom_Grid()

'                'S.SANDEEP [04 JUN 2015] -- START
'                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
'                    Call btnSearch_Click(btnSearch, Nothing)
'                End If
'                'S.SANDEEP [04 JUN 2015] -- END

'            End If

'Link:
'            'Shani (05-May-2016) -- Start
'            If IsPostBack Then
'                'Shani (05-May-2016) -- End
'                If Me.Session("CustomEvaluation") IsNot Nothing Then
'                    mdtCustomEvaluation = Me.Session("CustomEvaluation")
'                End If

'                If Me.Session("BSC_TabularGrid") IsNot Nothing Then
'                    dtBSC_TabularGrid = Me.Session("BSC_TabularGrid")
'                End If

'                If Me.Session("GE_TabularGrid") IsNot Nothing Then
'                    dtGE_TabularGrid = Me.Session("GE_TabularGrid")
'                End If

'                If Me.Session("CustomTabularGrid") IsNot Nothing Then
'                    dtCustomTabularGrid = Me.Session("CustomTabularGrid")
'                End If

'                If Me.ViewState("iWeightTotal") IsNot Nothing Then
'                    iWeightTotal = Me.ViewState("iWeightTotal")
'                End If

'                If Me.ViewState("Headers") IsNot Nothing Then
'                    dsHeaders = Me.ViewState("Headers")
'                End If

'                If Me.ViewState("iHeaderId") IsNot Nothing Then
'                    iHeaderId = Me.ViewState("iHeaderId")
'                End If

'                If Me.ViewState("iExOrdr") IsNot Nothing Then
'                    iExOrdr = Me.ViewState("iExOrdr")
'                End If

'                If Me.ViewState("iLinkedFieldId") IsNot Nothing Then
'                    iLinkedFieldId = Me.ViewState("iLinkedFieldId")
'                End If

'                If Me.ViewState("iMappingUnkid") IsNot Nothing Then
'                    iMappingUnkid = Me.ViewState("iMappingUnkid")
'                End If

'                If Me.ViewState("ColIndex") IsNot Nothing Then
'                    xVal = Me.ViewState("ColIndex")
'                End If

'                If objpnlCItems.Visible = True Then
'                    dgvItems.DataSource = dtCustomTabularGrid
'                    dgvItems.DataBind()
'                End If
'                'Shani (05-May-2016) -- Start
'            End If
'            'Shani (05-May-2016) -- End
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
'        Try
'            If Me.Session("CustomEvaluation") Is Nothing Then
'                Me.Session.Add("CustomEvaluation", mdtCustomEvaluation)
'            Else
'                Me.Session("CustomEvaluation") = mdtCustomEvaluation
'            End If

'            If Me.Session("BSC_TabularGrid") Is Nothing Then
'                Me.Session.Add("BSC_TabularGrid", dtBSC_TabularGrid)
'            Else
'                Me.Session("BSC_TabularGrid") = dtBSC_TabularGrid
'            End If

'            If Me.Session("GE_TabularGrid") Is Nothing Then
'                Me.Session.Add("GE_TabularGrid", dtGE_TabularGrid)
'            Else
'                Me.Session("GE_TabularGrid") = dtGE_TabularGrid
'            End If

'            If Me.Session("CustomTabularGrid") Is Nothing Then
'                Me.Session.Add("CustomTabularGrid", dtCustomTabularGrid)
'            Else
'                Me.Session("CustomTabularGrid") = dtCustomTabularGrid
'            End If

'            If Me.ViewState("iWeightTotal") Is Nothing Then
'                Me.ViewState.Add("iWeightTotal", iWeightTotal)
'            Else
'                Me.ViewState("iWeightTotal") = iWeightTotal
'            End If

'            If Me.ViewState("Headers") Is Nothing Then
'                Me.ViewState.Add("Headers", dsHeaders)
'            Else
'                Me.ViewState("Headers") = dsHeaders
'            End If

'            If Me.ViewState("iHeaderId") Is Nothing Then
'                Me.ViewState.Add("iHeaderId", iHeaderId)
'            Else
'                Me.ViewState("iHeaderId") = iHeaderId
'            End If

'            If Me.ViewState("iExOrdr") Is Nothing Then
'                Me.ViewState.Add("iExOrdr", iExOrdr)
'            Else
'                Me.ViewState("iExOrdr") = iExOrdr
'            End If

'            If Me.ViewState("iLinkedFieldId") Is Nothing Then
'                Me.ViewState.Add("iLinkedFieldId", iLinkedFieldId)
'            Else
'                Me.ViewState("iLinkedFieldId") = iLinkedFieldId
'            End If

'            If Me.ViewState("iMappingUnkid") Is Nothing Then
'                Me.ViewState.Add("iMappingUnkid", iMappingUnkid)
'            Else
'                Me.ViewState("iMappingUnkid") = iMappingUnkid
'            End If

'            If Me.ViewState("ColIndex") Is Nothing Then
'                Me.ViewState.Add("ColIndex", xVal)
'            Else
'                Me.ViewState("ColIndex") = xVal
'            End If

'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'#End Region

'#Region " Private Methods "

'    Private Sub FillCombo()
'        Dim dsCombos As New DataSet
'        Dim objEmp As New clsEmployee_Master
'        Dim objPeriod As New clscommom_period_Tran
'        Try

'            'Shani(24-Aug-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
'            '    dsCombos = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))
'            'ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
'            '    dsCombos = objEmp.GetEmployeeList("Emp", False, True, Session("Employeeunkid"), , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
'            'End If
'            Dim blnSelect As Boolean = True
'            Dim intEmpId As Integer = 0

'            'S.SANDEEP [10 DEC 2015] -- START
'            Dim blnApplyUACFilter As Boolean = False 'Shani(14-APR-2016) -- [True] 

'            'S.SANDEEP [10 DEC 2015] -- END

'            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
'                blnSelect = False
'                intEmpId = CInt(Session("Employeeunkid"))
'                'S.SANDEEP [10 DEC 2015] -- START
'                blnApplyUACFilter = False
'                'S.SANDEEP [10 DEC 2015] -- END
'            End If

'            'S.SANDEEP [04 Jan 2016] -- START
'            Dim strFilterQry As String = String.Empty
'            If (Session("LoginBy") = Global.User.en_loginby.User) Then

'                'Shani(14-APR-2016) -- Start
'                'If Session("SkipApprovalFlowInPlanning") = True Then
'                'Shani(14-APR-2016) -- End
'                Dim csvIds As String = String.Empty
'                Dim dsMapEmp As New DataSet
'                Dim objEval As New clsevaluation_analysis_master

'                dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
'                                                        Session("UserId"), _
'                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                        True, Session("IsIncludeInactiveEmp"), "List", False, False)

'                If dsMapEmp.Tables("List").Rows.Count > 0 Then

'                    dsCombos = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
'                                                              Session("UserId"), _
'                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
'                                                              Session("IsIncludeInactiveEmp"), _
'                                                              CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)


'                    strFilterQry = " hremployee_master.employeeunkid IN "
'                    csvIds = String.Join(",", dsCombos.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
'                    If csvIds.Trim.Length > 0 Then
'                        strFilterQry &= "(" & csvIds & ")"
'                    Else
'                        strFilterQry &= "(0)"
'                    End If

'                    'Shani(14-APR-2016) -- Start
'                Else
'                    strFilterQry = " hremployee_master.employeeunkid IN (0) "
'                    'Shani(14-APR-2016) -- End 
'                End If

'            End If

'            'Shani(14-APR-2016) -- Start
'            'End If
'            'Shani(14-APR-2016) -- End

'            'S.SANDEEP [04 Jan 2016] -- END

'            dsCombos = objEmp.GetEmployeeList(Session("Database_Name"), _
'                                            Session("UserId"), _
'                                            Session("Fin_year"), _
'                                            Session("CompanyUnkId"), _
'                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                            Session("UserAccessModeSetting"), True, _
'                                            Session("IsIncludeInactiveEmp"), "List", blnSelect, intEmpId, , , , , , , , , , , , , , strFilterQry, , blnApplyUACFilter) 'S.SANDEEP [10 DEC 2015] -- START {blnApplyUACFilter} -- END
'            'S.SANDEEP [04 Jan 2016] -- START {strFilterQry} -- END

'            'Shani(24-Aug-2015) -- End
'            With cboEmployee
'                .DataValueField = "employeeunkid"
'                'Nilay (09-Aug-2016) -- Start
'                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
'                '.DataTextField = "employeename"
'                .DataTextField = "EmpCodeName"
'                'Nilay (09-Aug-2016) -- End
'                .DataSource = dsCombos.Tables(0)
'                .DataBind()
'                If (Session("LoginBy") = Global.User.en_loginby.User) Then
'                    .SelectedValue = 0
'                ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
'                    .SelectedValue = Session("Employeeunkid")
'                End If
'            End With

'            'S.SANDEEP [17 NOV 2015] -- START
'            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "APeriod", True, 1)

'            'Shani(20-Nov-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "APeriod", True, 1, , , Session("Database_Name"))

'            'S.SANDEEP [10 DEC 2015] -- START
'            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_enddate"), "APeriod", True, 1)
'            dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "APeriod", True, 1)
'            'S.SANDEEP [10 DEC 2015] -- END

'            'Shani(20-Nov-2015) -- End

'            'S.SANDEEP [17 NOV 2015] -- END
'            'S.SANDEEP [04 JUN 2015] -- START

'            'Shani (09-May-2016) -- Start
'            'Dim mintCurrentPeriodId As Integer = 0
'            'If dsCombos.Tables(0).Rows.Count > 1 Then
'            '    mintCurrentPeriodId = dsCombos.Tables(0).Rows(dsCombos.Tables(0).Rows.Count - 1).Item("periodunkid")
'            'End If
'            Dim mintCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
'            'Shani (09-May-2016) -- End

'            'S.SANDEEP [04 JUN 2015] -- END
'            With cboPeriod
'                .DataValueField = "periodunkid"
'                .DataTextField = "name"
'                .DataSource = dsCombos.Tables("APeriod")
'                .DataBind()
'                If Me.Request.QueryString.Count <= 0 Then
'                    'S.SANDEEP [04 JUN 2015] -- START
'                    '.SelectedValue = 0
'                    .SelectedValue = mintCurrentPeriodId
'                    'Shani (28-Jul-2016) -- Start
'                    'Issue - 
'                    'Call cboPeriod_SelectedIndexChanged(New Object, New EventArgs)
'                    'Shani (28-Jul-2016) -- End
'                    'S.SANDEEP [04 JUN 2015] -- END
'                Else
'                    .SelectedValue = Me.ViewState("periodid")
'                End If

'                'Shani (28-Jul-2016) -- Start
'                'Issue - 
'                Call cboPeriod_SelectedIndexChanged(New Object, New EventArgs)
'                'Shani (28-Jul-2016) -- End
'            End With
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Private Sub SetTotals(ByVal iDG As DataGrid)
'        Try
'            Select Case iDG.ID.ToUpper
'                Case dgvBSC.ID.ToUpper
'                    objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
'                    objlblValue1.Visible = True : objlblValue2.Visible = True : objlblValue3.Visible = True : objlblValue4.Visible = True

'                    'S.SANDEEP [21 JAN 2015] -- START
'                    'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(escore)", "")).ToString
'                    'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(ascore)", "")).ToString
'                    'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(rscore)", "")).ToString

'                    'Shani (23-Nov-2016) -- Start
'                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    '        'Shani(20-Nov-2015) -- Start
'                    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    '        'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'                    '        '                                                  True, _
'                    '        '                                                  Session("ScoringOptionId"), _
'                    '        '                                                  enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
'                    '        '                                                  cboEmployee.SelectedValue, _
'                    '        '                                                  cboPeriod.SelectedValue, , dtBSC_TabularGrid)
'                    '        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'                    '        objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'                    '                                      True, _
'                    '                                      Session("ScoringOptionId"), _
'                    '                                      enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
'                    '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                    '                                      cboEmployee.SelectedValue, _
'                    '                                      cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'                    '        'Shani(20-Nov-2015) -- End


'                    '        'Shani(20-Nov-2015) -- Start
'                    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    '        'objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
'                    '        '                              True, _
'                    '        '                              Session("ScoringOptionId"), _
'                    '        '                              enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
'                    '        '                              cboEmployee.SelectedValue, _
'                    '        '                              cboPeriod.SelectedValue, , dtBSC_TabularGrid)
'                    '        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'                    '        objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
'                    '                                      True, _
'                    '                                      Session("ScoringOptionId"), _
'                    '                                      enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
'                    '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                    '                                      cboEmployee.SelectedValue, _
'                    '                                      cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'                    '        'Shani(20-Nov-2015) -- End


'                    '        'Shani(20-Nov-2015) -- Start
'                    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    '        'objEAnalysisMst.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
'                    '        '                              True, _
'                    '        '                              Session("ScoringOptionId"), _
'                    '        '                              enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
'                    '        '                              cboEmployee.SelectedValue, _
'                    '        '                              cboPeriod.SelectedValue, , dtBSC_TabularGrid)
'                    '        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & _
'                    '        objEAnalysisMst.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
'                    '                                      True, _
'                    '                                      Session("ScoringOptionId"), _
'                    '                                      enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
'                    '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                    '                                      cboEmployee.SelectedValue, _
'                    '                                      cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'                    '        'Shani(20-Nov-2015) -- End

'                    '        'S.SANDEEP [21 JAN 2015] -- END
'                    '    Case dgvGE.ID.ToUpper
'                    '        objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
'                    '        objlblValue1.Visible = True : objlblValue2.Visible = True : objlblValue3.Visible = True : objlblValue4.Visible = True
'                    '        'S.SANDEEP [21 JAN 2015] -- START
'                    '        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(escore)", "")).ToString
'                    '        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(ascore)", "")).ToString
'                    '        'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(rscore)", "")).ToString


'                    '        'Shani(20-Nov-2015) -- Start
'                    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    '        'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'                    '        '                                                 False, _
'                    '        '                                                 Session("ScoringOptionId"), _
'                    '        '                                                 enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
'                    '        '                                                 cboEmployee.SelectedValue, _
'                    '        '                                                 cboPeriod.SelectedValue, , dtGE_TabularGrid)
'                    '        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'                    '        objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'                    '                                      False, _
'                    '                                      Session("ScoringOptionId"), _
'                    '                                      enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
'                    '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                    '                                      cboEmployee.SelectedValue, _
'                    '                                      cboPeriod.SelectedValue, , dtGE_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'                    '        'Shani(20-Nov-2015) -- End

'                    '        'Shani(20-Nov-2015) -- Start
'                    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    '        'objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
'                    '        '                                                  False, _
'                    '        '                                                  Session("ScoringOptionId"), _
'                    '        '                                                  enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
'                    '        '                                                  cboEmployee.SelectedValue, _
'                    '        '                                                  cboPeriod.SelectedValue, , dtGE_TabularGrid)
'                    '        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'                    '        objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
'                    '                                      False, _
'                    '                                      Session("ScoringOptionId"), _
'                    '                                      enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
'                    '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                    '                                      cboEmployee.SelectedValue, _
'                    '                                      cboPeriod.SelectedValue, , dtGE_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'                    '        'Shani(20-Nov-2015) -- End


'                    '        'Shani(20-Nov-2015) -- Start
'                    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    '        'objEAnalysisMst.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
'                    '        '                                                  False, _
'                    '        '                                                  Session("ScoringOptionId"), _
'                    '        '                                                  enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
'                    '        '                                                  cboEmployee.SelectedValue, _
'                    '        '                                                  cboPeriod.SelectedValue, , dtGE_TabularGrid)
'                    '        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & _
'                    '        objEAnalysisMst.Compute_Score(enAssessmentMode.REVIEWER_ASSESSMENT, _
'                    '                                      False, _
'                    '                                      Session("ScoringOptionId"), _
'                    '                                      enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
'                    '                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                    '                                      cboEmployee.SelectedValue, _
'                    '                                      cboPeriod.SelectedValue, , dtGE_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'                    '        'Shani(20-Nov-2015) -- End

'                    '        'S.SANDEEP [21 JAN 2015] -- END

'                    '        objlblValue3.Visible = True : objlblValue4.Visible = True
'                    'End Select
'                    'Shani(20-Nov-2015) -- Start
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'                    '                                                  True, _
'                    '                                                  Session("ScoringOptionId"), _
'                    '                                                  enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
'                    '                                                  cboEmployee.SelectedValue, _
'                    '                                                  cboPeriod.SelectedValue, , dtBSC_TabularGrid)

'                    If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'                    objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
'                                                  IsBalanceScoreCard:=True, _
'                                                  xScoreOptId:=Session("ScoringOptionId"), _
'                                                  xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
'                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                  xEmployeeId:=cboEmployee.SelectedValue, _
'                                                  xPeriodId:=cboPeriod.SelectedValue, _
'                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
'                                                  xDataTable:=dtBSC_TabularGrid, _
'                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

'                    If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'                    objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
'                                                  IsBalanceScoreCard:=True, _
'                                                  xScoreOptId:=Session("ScoringOptionId"), _
'                                                  xCompute_Formula:=enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
'                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                  xEmployeeId:=cboEmployee.SelectedValue, _
'                                                  xPeriodId:=cboPeriod.SelectedValue, _
'                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
'                                                  xDataTable:=dtBSC_TabularGrid, _
'                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
'                    If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & _
'                    objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
'                                                  IsBalanceScoreCard:=True, _
'                                                  xScoreOptId:=Session("ScoringOptionId"), _
'                                                  xCompute_Formula:=enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
'                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                  xEmployeeId:=cboEmployee.SelectedValue, _
'                                                  xPeriodId:=cboPeriod.SelectedValue, _
'                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
'                                                  xDataTable:=dtBSC_TabularGrid, _
'                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

'                Case dgvGE.ID.ToUpper
'                    objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
'                    objlblValue1.Visible = True : objlblValue2.Visible = True : objlblValue3.Visible = True : objlblValue4.Visible = True

'                    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'                    objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
'                                                  IsBalanceScoreCard:=False, _
'                                                  xScoreOptId:=Session("ScoringOptionId"), _
'                                                  xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
'                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                  xEmployeeId:=cboEmployee.SelectedValue, _
'                                                  xPeriodId:=cboPeriod.SelectedValue, _
'                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
'                                                  xDataTable:=dtGE_TabularGrid, _
'                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

'                    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'                    objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.APPRAISER_ASSESSMENT, _
'                                                  IsBalanceScoreCard:=False, _
'                                                  xScoreOptId:=Session("ScoringOptionId"), _
'                                                  xCompute_Formula:=enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
'                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                  xEmployeeId:=cboEmployee.SelectedValue, _
'                                                  xPeriodId:=cboPeriod.SelectedValue, _
'                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
'                                                  xDataTable:=dtGE_TabularGrid, _
'                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
'                    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & _
'                    objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.REVIEWER_ASSESSMENT, _
'                                                  IsBalanceScoreCard:=False, _
'                                                  xScoreOptId:=Session("ScoringOptionId"), _
'                                                  xCompute_Formula:=enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
'                                                  xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                  xEmployeeId:=cboEmployee.SelectedValue, _
'                                                  xPeriodId:=cboPeriod.SelectedValue, _
'                                                  xUsedAgreedScore:=Session("IsUseAgreedScore"), _
'                                                  xDataTable:=dtGE_TabularGrid, _
'                                                  xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
'                    objlblValue3.Visible = True : objlblValue4.Visible = True
'            End Select

'            'Shani (23-Nov123-2016-2016) -- End

'            'Shani(19-APR-2016) -- Start
'            objlblValue4.Visible = CBool(Session("IsCompanyNeedReviewer"))
'            'Shani(19-APR-2016) -- End 


'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Private Sub PanelVisibility()
'        Try
'            If iHeaderId >= 0 Then
'                objlblCaption.Text = ""
'                objlblCaption.Text = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString
'                xVal = dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")
'            End If
'            Select Case xVal
'                Case -3  'Balance Score Card
'                    objpnlGE.Visible = False
'                    objpnlCItems.Visible = False
'                    iWeightTotal = 0
'                    Call Fill_BSC_Evaluation()
'                    objpnlBSC.Visible = True
'                Case -2  'Competencies
'                    objpnlBSC.Visible = False
'                    objpnlCItems.Visible = False
'                    iWeightTotal = 0
'                    Call Fill_GE_Evaluation()
'                    objpnlGE.Visible = True
'                Case Else  'Dynamic Custom Headers
'                    objpnlBSC.Visible = False
'                    objpnlGE.Visible = False
'                    Call Fill_Custom_Grid()
'                    Call Fill_Custom_Evaluation_Data()
'                    objpnlCItems.Visible = True
'                    iWeightTotal = 0
'            End Select
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub


'    ''''''''''''''''''''''''''BSC EVOLUTION
'    Private Sub SetBSC_GridCols_Tags()
'        Try
'            Dim objFMst As New clsAssess_Field_Master
'            Dim dFld As New DataSet : dFld.Tables.Add(objFMst.GetFieldsForViewSetting())
'            If dFld.Tables(0).Rows.Count > 0 Then
'                Dim xCol As DataGridColumn = Nothing
'                Dim iExOrder As Integer = -1
'                For Each xRow As DataRow In dFld.Tables(0).Rows
'                    iExOrder = -1
'                    iExOrder = objFMst.Get_Field_ExOrder(xRow.Item("Id"))
'                    xCol = Nothing
'                    Select Case iExOrder
'                        Case 1
'                            dgvBSC.Columns(0).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(0)
'                        Case 2
'                            dgvBSC.Columns(1).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(1)
'                        Case 3
'                            dgvBSC.Columns(2).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(2)
'                        Case 4
'                            dgvBSC.Columns(3).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(3)
'                        Case 5
'                            dgvBSC.Columns(4).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(4)
'                        Case 6
'                            dgvBSC.Columns(5).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(5)
'                        Case 7
'                            dgvBSC.Columns(6).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(6)
'                        Case 8
'                            dgvBSC.Columns(7).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(7)
'                    End Select
'                    Select Case xRow.Item("Id")
'                        Case clsAssess_Field_Master.enOtherInfoField.ST_DATE
'                            dgvBSC.Columns(8).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(8)
'                        Case clsAssess_Field_Master.enOtherInfoField.ED_DATE
'                            dgvBSC.Columns(9).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(9)
'                        Case clsAssess_Field_Master.enOtherInfoField.PCT_COMPLETE
'                            dgvBSC.Columns(10).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(10)
'                        Case clsAssess_Field_Master.enOtherInfoField.STATUS
'                            dgvBSC.Columns(11).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(11)
'                            'S.SANDEEP [04 MAR 2015] -- START
'                        Case clsAssess_Field_Master.enOtherInfoField.WEIGHT
'                            dgvBSC.Columns(12).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(12)
'                        Case clsAssess_Field_Master.enOtherInfoField.SCORE
'                            dgvBSC.Columns(13).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(13)
'                            'S.SANDEEP [04 MAR 2015] -- END
'                    End Select
'                    If xCol IsNot Nothing Then
'                        xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
'                        xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))

'                        If xRow.Item("Id") = clsAssess_Field_Master.enOtherInfoField.SCORE Then
'                            dgvBSC.Columns(15).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(15)
'                            xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
'                            xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))

'                            dgvBSC.Columns(17).FooterText = xRow.Item("Id")
'                            xCol = dgvBSC.Columns(17)
'                            xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
'                            xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
'                        End If

'                    End If
'                Next
'            End If
'            objFMst = Nothing
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Private Sub Fill_BSC_Evaluation()
'        Try
'            'S.SANDEEP [04 JUN 2015] -- START
'            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, -1, -1)

'            'Shani (26-Sep-2016) -- Start
'            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, -1, -1, True)
'            dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), 0, -1, -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")), True)
'            'Shani (26-Sep-2016) -- End


'            'S.SANDEEP [04 JUN 2015] -- END

'            Call SetBSC_GridCols_Tags()

'            If dtBSC_TabularGrid.Columns.Contains("Field1") Then
'                dgvBSC.Columns(0).HeaderText = dtBSC_TabularGrid.Columns("Field1").Caption
'                dgvBSC.Columns(0).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field1", System.Type.GetType("System.String"))
'                dgvBSC.Columns(0).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field2") Then
'                dgvBSC.Columns(1).HeaderText = dtBSC_TabularGrid.Columns("Field2").Caption
'                dgvBSC.Columns(1).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field2", System.Type.GetType("System.String"))
'                dgvBSC.Columns(1).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field3") Then
'                dgvBSC.Columns(2).HeaderText = dtBSC_TabularGrid.Columns("Field3").Caption
'                dgvBSC.Columns(2).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field3", System.Type.GetType("System.String"))
'                dgvBSC.Columns(2).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field4") Then
'                dgvBSC.Columns(3).HeaderText = dtBSC_TabularGrid.Columns("Field4").Caption
'                dgvBSC.Columns(3).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field4", System.Type.GetType("System.String"))
'                dgvBSC.Columns(3).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field5") Then
'                dgvBSC.Columns(4).HeaderText = dtBSC_TabularGrid.Columns("Field5").Caption
'                dgvBSC.Columns(4).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field5", System.Type.GetType("System.String"))
'                dgvBSC.Columns(4).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field6") Then
'                dgvBSC.Columns(5).HeaderText = dtBSC_TabularGrid.Columns("Field6").Caption
'                dgvBSC.Columns(5).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field6", System.Type.GetType("System.String"))
'                dgvBSC.Columns(5).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field7") Then
'                dgvBSC.Columns(6).HeaderText = dtBSC_TabularGrid.Columns("Field7").Caption
'                dgvBSC.Columns(6).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field7", System.Type.GetType("System.String"))
'                dgvBSC.Columns(6).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field8") Then
'                dgvBSC.Columns(7).HeaderText = dtBSC_TabularGrid.Columns("Field8").Caption
'                dgvBSC.Columns(7).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field8", System.Type.GetType("System.String"))
'                dgvBSC.Columns(7).Visible = False
'            End If

'            dgvBSC.Columns(13).HeaderText = dtBSC_TabularGrid.Columns("eself").Caption      'SELF ASSESSMENT
'            dgvBSC.Columns(14).HeaderText = dtBSC_TabularGrid.Columns("eremark").Caption    'SELF ASSESSMENT

'            dgvBSC.Columns(15).HeaderText = dtBSC_TabularGrid.Columns("aself").Caption      'ASSESSOR ASSESSMENT
'            dgvBSC.Columns(16).HeaderText = dtBSC_TabularGrid.Columns("aremark").Caption    'ASSESSOR ASSESSMENT

'            dgvBSC.Columns(17).HeaderText = dtBSC_TabularGrid.Columns("rself").Caption      'REVIEWER ASSESSMENT
'            dgvBSC.Columns(18).HeaderText = dtBSC_TabularGrid.Columns("rremark").Caption    'REVIEWER ASSESSMENT

'            If CStr(Session("ViewTitles_InEvaluation")).Trim.Length > 0 Then
'                Dim iEval() As String = CStr(Session("ViewTitles_InEvaluation")).Split("|")
'                If iEval IsNot Nothing Then
'                    For Each xCol As DataGridColumn In dgvBSC.Columns
'                        If xCol.FooterText IsNot Nothing Then
'                            If IsNumeric(xCol.FooterText) Then
'                                If Array.IndexOf(iEval, xCol.FooterText.ToString) < 0 Then
'                                    xCol.Visible = False
'                                End If
'                            End If
'                        End If
'                    Next
'                End If
'            End If
'            If dgvBSC.Columns(0).Visible = True Then
'                xVal = 1
'            ElseIf dgvBSC.Columns(1).Visible = True Then
'                xVal = 2
'            ElseIf dgvBSC.Columns(2).Visible = True Then
'                xVal = 3
'            ElseIf dgvBSC.Columns(3).Visible = True Then
'                xVal = 4
'            ElseIf dgvBSC.Columns(4).Visible = True Then
'                xVal = 5
'            ElseIf dgvBSC.Columns(5).Visible = True Then
'                xVal = 6
'            ElseIf dgvBSC.Columns(6).Visible = True Then
'                xVal = 7
'            ElseIf dgvBSC.Columns(7).Visible = True Then
'                xVal = 8
'            End If

'            'SHANI [09 Mar 2015]-START
'            'Enhancement - REDESIGN SELF SERVICE.
'            'dgvBSC.DataSource = dtBSC_TabularGrid
'            'SHANI [09 Mar 2015]--END 

'            'S.SANDEEP [04 MAR 2015] -- START
'            Dim xWidth As Integer = 0
'            Dim objFMst As New clsAssess_Field_Master

'            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK)

'            If xWidth > 0 Then
'                dgvBSC.Columns(14).HeaderStyle.Width = Unit.Pixel(xWidth)
'                dgvBSC.Columns(14).ItemStyle.Width = Unit.Pixel(xWidth)
'            End If

'            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK)

'            If xWidth > 0 Then
'                dgvBSC.Columns(16).HeaderStyle.Width = Unit.Pixel(xWidth)
'                dgvBSC.Columns(16).ItemStyle.Width = Unit.Pixel(xWidth)
'            End If

'            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.REV_REMARK)

'            If xWidth > 0 Then
'                dgvBSC.Columns(18).HeaderStyle.Width = Unit.Pixel(xWidth)
'                dgvBSC.Columns(18).ItemStyle.Width = Unit.Pixel(xWidth)
'            End If

'            objFMst = Nothing
'            'S.SANDEEP [04 MAR 2015] -- END

'            'SHANI [09 Mar 2015]-START
'            'Enhancement - REDESIGN SELF SERVICE.
'            Dim intTotalWidth As Integer = 0
'            For Each xCol As DataGridColumn In dgvBSC.Columns
'                If xCol.Visible Then
'                    intTotalWidth += xCol.HeaderStyle.Width.Value
'                End If
'            Next
'            For Each xCol As DataGridColumn In dgvBSC.Columns
'                If xCol.Visible Then
'                    Dim decColumnWidth As Decimal = xCol.HeaderStyle.Width.Value * 100 / intTotalWidth
'                    xCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
'                    xCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
'                End If
'            Next
'            dgvBSC.Width = Unit.Percentage(99.5)
'            'SHANI [09 Mar 2015]--END 

'            'Shani(14-APR-2016) -- Start.
'            dgvBSC.Columns(17).Visible = CBool(Session("IsCompanyNeedReviewer"))      'REVIEWER ASSESSMENT
'            dgvBSC.Columns(18).Visible = CBool(Session("IsCompanyNeedReviewer"))    'REVIEWER ASSESSMENT
'            'Shani(14-APR-2016) -- End 


'            dgvBSC.DataSource = dtBSC_TabularGrid
'            dgvBSC.DataBind()
'            Call SetTotals(dgvBSC)
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub


'    '''''''''''''''''''''''''' COMPETENCIES/GENERAL EVALUATION METHODS
'    Private Sub Fill_GE_Evaluation()
'        Try
'            'S.SANDEEP [29 JAN 2015] -- START
'            'dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, 0, Session("ConsiderItemWeightAsNumber"), -1)

'            'S.SANDEEP [04 JUN 2015] -- START
'            'dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, 0, Session("ConsiderItemWeightAsNumber"), -1, Session("Self_Assign_Competencies"))

'            'Shani(20-Nov-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, 0, Session("ConsiderItemWeightAsNumber"), -1, Session("Self_Assign_Competencies"), True)
'            dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, 0, Session("ConsiderItemWeightAsNumber"), -1, Session("Self_Assign_Competencies"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), True)
'            'Shani(20-Nov-2015) -- End

'            'S.SANDEEP [04 JUN 2015] -- END

'            'S.SANDEEP [29 JAN 2015] -- END
'            dgvGE.AutoGenerateColumns = False

'            dgvGE.Columns(3).Visible = CBool(Session("ConsiderItemWeightAsNumber")) 'SELF FINAL SCORE
'            dgvGE.Columns(6).Visible = CBool(Session("ConsiderItemWeightAsNumber")) 'ASSESSOR FINAL SCORE
'            dgvGE.Columns(9).Visible = CBool(Session("ConsiderItemWeightAsNumber")) 'REVIEWER FINAL SCORE

'            dgvGE.Columns(2).HeaderText = dtGE_TabularGrid.Columns("eself").Caption
'            dgvGE.Columns(4).HeaderText = dtGE_TabularGrid.Columns("eremark").Caption
'            dgvGE.Columns(5).HeaderText = dtGE_TabularGrid.Columns("aself").Caption
'            dgvGE.Columns(7).HeaderText = dtGE_TabularGrid.Columns("aremark").Caption
'            dgvGE.Columns(8).HeaderText = dtGE_TabularGrid.Columns("rself").Caption
'            dgvGE.Columns(10).HeaderText = dtGE_TabularGrid.Columns("rremark").Caption

'            'Shani(14-APR-2016) -- Start
'            dgvGE.Columns(8).Visible = CBool(Session("IsCompanyNeedReviewer"))      'REVIEWER GE Score
'            dgvGE.Columns(10).Visible = CBool(Session("IsCompanyNeedReviewer"))    'REVIEWER GE Remark
'            'Shani(14-APR-2016) -- End 


'            dgvGE.DataSource = dtGE_TabularGrid
'            dgvGE.DataBind()
'            Call SetTotals(dgvGE)
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Fill_GE_Evaluation", mstrModuleName)
'        Finally
'        End Try
'    End Sub


'    '''''''''''''''''''''''''' CUSTOM ITEMS EVALUATION METHODS
'    Private Sub Fill_Custom_Grid(Optional ByVal iFromAddEdit As Boolean = False)
'        Try
'            If iHeaderId >= 0 Then
'                If iFromAddEdit = False Then
'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), CInt(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")), mintAssessAnalysisUnkid, enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), enAction.ADD_ONE)
'                    dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), CInt(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")), mintAssessAnalysisUnkid, enAssessmentMode.REVIEWER_ASSESSMENT, CInt(cboEmployee.SelectedValue), enAction.ADD_ONE, True)
'                    'S.SANDEEP [04 JUN 2015] -- END
'                Else
'                    Dim iRow() As DataRow = dtCustomTabularGrid.Select("periodunkid <= 0")
'                    For i As Integer = 0 To iRow.Length - 1
'                        dtCustomTabularGrid.Rows.Remove(iRow(i))
'                    Next
'                End If
'                If dtCustomTabularGrid.Rows.Count > 0 Then
'                    If dtCustomTabularGrid.Rows.Count > 0 Then
'                        dgvItems.Columns.Clear()
'                    End If
'                    dgvItems.DataSource = Nothing
'                    dgvItems.AutoGenerateColumns = False
'                    Dim iColName As String = String.Empty
'                    For Each dCol As DataColumn In dtCustomTabularGrid.Columns
'                        iColName = "" : iColName = "obj" & dCol.ColumnName
'                        Dim dgvCol As New BoundField()
'                        dgvCol.FooterText = iColName
'                        dgvCol.ReadOnly = True
'                        dgvCol.DataField = dCol.ColumnName
'                        dgvCol.HeaderText = dCol.Caption
'                        If dgvItems.Columns.Contains(dgvCol) = True Then Continue For
'                        If dCol.Caption.Length <= 0 Then
'                            dgvCol.Visible = False
'                        End If
'                        dgvItems.Columns.Add(dgvCol)
'                        If dgvCol.FooterText = "objHeader_Name" Then dgvCol.Visible = False
'                    Next
'                    'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
'                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
'                    Call SetDateFormat()
'                    'Pinkal (16-Apr-2016) -- End
'                    dgvItems.DataSource = dtCustomTabularGrid
'                    dgvItems.DataBind()
'                End If
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    'Shani (26-Sep-2016) -- Start
'    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)



'    'Private Sub Fill_Custom_Evaluation_Data()
'    '    Try
'    '        If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
'    '            Dim dtEval As DataTable
'    '            'S.SANDEEP [10 DEC 2015] -- START
'    '            'dtEval = New DataView(mdtCustomEvaluation, "custom_header = '" & dsHeaders.Tables(0).Rows(iHeaderId).Item("Name") & "' AND AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable
'    '            dtEval = New DataView(mdtCustomEvaluation, "custom_header = '" & dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString.Replace("'", "''") & "' AND AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable
'    '            'S.SANDEEP [10 DEC 2015] -- END

'    '            dtCustomTabularGrid.Rows.Clear()
'    '            Dim dFRow As DataRow = Nothing
'    '            Dim iCustomId, iNewCustomId As Integer : iCustomId = 0 : iNewCustomId = 0
'    '            For Each dRow As DataRow In dtEval.Rows
'    '                If dRow.Item("AUD") = "D" Then Continue For
'    '                If iCustomId <= 0 Then iCustomId = dRow.Item("customitemunkid")
'    '                iNewCustomId = dRow.Item("customitemunkid")
'    '                If iCustomId = iNewCustomId Then
'    '                    dFRow = dtCustomTabularGrid.NewRow
'    '                    dtCustomTabularGrid.Rows.Add(dFRow)
'    '                End If
'    '                Select Case CInt(dRow.Item("itemtypeid"))
'    '                    Case clsassess_custom_items.enCustomType.FREE_TEXT
'    '                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
'    '                    Case clsassess_custom_items.enCustomType.SELECTION
'    '                        If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'    '                            Select Case CInt(dRow.Item("selectionmodeid"))
'    '                                Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
'    '                                    Dim objCMaster As New clsCommon_Master
'    '                                    objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
'    '                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
'    '                                    objCMaster = Nothing
'    '                                Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
'    '                                    Dim objCMaster As New clsassess_competencies_master
'    '                                    objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
'    '                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
'    '                                    objCMaster = Nothing
'    '                                Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
'    '                                    Dim objEmpField1 As New clsassess_empfield1_master
'    '                                    objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
'    '                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
'    '                                    objEmpField1 = Nothing
'    '                            End Select
'    '                        End If
'    '                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
'    '                        If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'    '                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
'    '                        End If
'    '                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
'    '                        If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'    '                            If IsNumeric(dRow.Item("custom_value")) Then
'    '                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
'    '                            End If
'    '                        End If
'    '                End Select
'    '                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
'    '                dFRow.Item("periodunkid") = dRow.Item("periodunkid")
'    '                dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
'    '                dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
'    '                dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
'    '                dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
'    '            Next
'    '            If dtCustomTabularGrid.Rows.Count <= 0 Then
'    '                dtCustomTabularGrid.Rows.Add(dtCustomTabularGrid.NewRow)
'    '                dtCustomTabularGrid.Rows(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'    '            End If
'    '            Call Fill_Custom_Grid(True)
'    '        End If
'    '    Catch ex As Exception
'    '        DisplayMessage.DisplayError(ex,Me)
'    '    Finally
'    '    End Try
'    'End Sub
'    Private Sub Fill_Custom_Evaluation_Data()
'        Dim strGUIDArray() As String = Nothing
'        Try
'            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
'                Dim dR = mdtCustomEvaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("custom_header") = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name") And x.Field(Of String)("AUD") <> "D")

'                If dR.Count > 0 Then
'                    strGUIDArray = dR.Select(Function(x) x.Field(Of String)("customanalysistranguid")).Distinct().ToArray
'                End If

'                Dim dFRow As DataRow = Nothing
'                dtCustomTabularGrid.Rows.Clear()
'                If strGUIDArray IsNot Nothing AndAlso strGUIDArray.Length > 0 Then
'                    For Each Str As String In strGUIDArray
'                        dFRow = dtCustomTabularGrid.NewRow
'                        dtCustomTabularGrid.Rows.Add(dFRow)
'                        For Each dRow As DataRow In mdtCustomEvaluation.Select("customanalysistranguid = '" & Str & "' AND AUD <> 'D'")
'                            Select Case CInt(dRow.Item("itemtypeid"))
'                                Case clsassess_custom_items.enCustomType.FREE_TEXT
'                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
'                                Case clsassess_custom_items.enCustomType.SELECTION
'                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'                                        Select Case CInt(dRow.Item("selectionmodeid"))
'                                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
'                                                Dim objCMaster As New clsCommon_Master
'                                                objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
'                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
'                                                objCMaster = Nothing
'                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
'                                                Dim objCMaster As New clsassess_competencies_master
'                                                objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
'                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
'                                                objCMaster = Nothing
'                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
'                                                Dim objEmpField1 As New clsassess_empfield1_master
'                                                objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
'                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
'                                                objEmpField1 = Nothing
'                                        End Select
'                                    End If
'                                Case clsassess_custom_items.enCustomType.DATE_SELECTION
'                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
'                                    End If
'                                Case clsassess_custom_items.enCustomType.NUMERIC_DATA
'                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'                                        If IsNumeric(dRow.Item("custom_value")) Then
'                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
'                                        End If
'                                    End If
'                            End Select
'                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
'                            dFRow.Item("periodunkid") = dRow.Item("periodunkid")
'                            dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
'                            dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
'                            dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
'                            dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
'                        Next
'                    Next
'                End If

'                If dtCustomTabularGrid.Rows.Count <= 0 Then
'                    dtCustomTabularGrid.Rows.Add(dtCustomTabularGrid.NewRow)
'                    dtCustomTabularGrid.Rows(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                End If
'                Call Fill_Custom_Grid(True)
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub
'    'Shani (26-Sep-2016) -- End

'#End Region

'#Region " Buttons Event "

'    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
'        Try
'            If CInt(cboEmployee.SelectedValue) <= 0 Or _
'              CInt(cboPeriod.SelectedValue) <= 0 Then
'                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please set following information [Employee,Period] to view assessment."), Me)
'                Exit Sub
'            End If

'            'Shani (26-Sep-2016) -- Start
'            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'            Call cboPeriod_SelectedIndexChanged(cboPeriod, New EventArgs)
'            'Shani (26-Sep-2016) -- End

'            If dsHeaders.Tables(0).Rows.Count > 0 Then
'                btnNext.Enabled = True : iHeaderId = -1 : btnNext_Click(sender, e)
'                btnBack.Enabled = True
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
'        Try
'            If dsHeaders.Tables(0).Rows.Count > 0 Then
'                If iHeaderId < dsHeaders.Tables(0).Rows.Count - 1 Then
'                    iHeaderId += 1
'                    btnBack.Enabled = True
'                End If
'                If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
'                    btnNext.Enabled = False
'                End If
'                objlblValue1.Text = "" : objlblValue2.Text = ""
'                objlblValue3.Text = "" : objlblValue4.Text = ""
'                Call PanelVisibility()
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
'        Try
'            If dsHeaders.Tables(0).Rows.Count > 0 Then
'                iHeaderId = iHeaderId - 1
'                If iHeaderId <= 0 Then
'                    btnBack.Enabled = False
'                    btnNext.Enabled = True
'                End If
'                If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
'                    btnNext.Enabled = False
'                Else
'                    btnNext.Enabled = True
'                End If
'                Call PanelVisibility()
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        If Me.Request.QueryString.Count > 0 Then
'            Response.Redirect(Session("servername") & "../../Index.aspx", False)
'        Else
'            Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
'        End If
'    End Sub


'    'SHANI [01 FEB 2015]-START
'    'Enhancement - REDESIGN SELF SERVICE.
'    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
'    '    If Me.Request.QueryString.Count > 0 Then
'    '        Response.Redirect(Session("servername") & "../../Index.aspx", False)
'    '    Else
'    '        Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
'    '    End If
'    'End Sub
'    'SHANI [01 FEB 2015]--END

'    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
'        Try
'            objlblCaption.Text = ""
'            'S.SANDEEP [21 JAN 2015] -- START
'            dgvBSC.DataSource = Nothing
'            dgvBSC.DataBind()
'            dgvGE.DataSource = Nothing
'            dgvGE.DataBind()
'            dgvItems.DataSource = Nothing
'            dgvItems.DataBind()
'            'S.SANDEEP [21 JAN 2015] -- END
'            btnNext.Enabled = False : btnBack.Enabled = False
'            objlblValue1.Visible = False : objlblValue2.Visible = False
'            objlblValue3.Visible = False : objlblValue4.Visible = False
'            If (Session("LoginBy") = Global.User.en_loginby.User) Then
'                cboEmployee.SelectedValue = 0
'            ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
'                If Me.Request.QueryString.Count <= 0 Then
'                    cboEmployee.SelectedValue = Session("Employeeunkid")
'                End If
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'#End Region

'#Region " GridView Event "

'    Protected Sub dgvGE_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvGE.ItemDataBound
'        Try
'            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
'                Dim intCount As Integer = 1
'                If CBool(DataBinder.Eval(e.Item.DataItem, "IsPGrp")) = True Then
'                    For i = 1 To dgvGE.Columns.Count - 1
'                        If dgvGE.Columns(i).Visible Then
'                            e.Item.Cells(i).Visible = False
'                            intCount += 1
'                        End If
'                    Next
'                    e.Item.Cells(0).ColumnSpan = intCount
'                    e.Item.Cells(0).CssClass = "MainGroupHeaderStyle"
'                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then
'                    intCount = 1
'                    For i = 1 To dgvGE.Columns.Count - 2
'                        If dgvGE.Columns(i).Visible Then
'                            e.Item.Cells(i).Visible = False
'                            intCount += 1
'                        End If
'                    Next
'                    e.Item.Cells(0).ColumnSpan = intCount
'                    e.Item.Cells(0).CssClass = "GroupHeaderStylecomp"
'                    e.Item.Cells(17).CssClass = "GroupHeaderStylecomp"
'                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then
'                    e.Item.Cells(0).Text = "&nbsp;" & e.Item.Cells(0).Text
'                End If
'                If IsNumeric(e.Item.Cells(1).Text) Then
'                    iWeightTotal = iWeightTotal + CDec(e.Item.Cells(1).Text)
'                End If
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Protected Sub dgvBSC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvBSC.ItemDataBound
'        Try
'            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
'                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
'                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
'                Call SetDateFormat()
'                'Pinkal (16-Apr-2016) -- End
'                If CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then
'                    For i = xVal To dgvBSC.Columns.Count - 1
'                        e.Item.Cells(i).Visible = False
'                    Next
'                    e.Item.Cells(xVal - 1).ColumnSpan = dgvBSC.Columns.Count - 1
'                    e.Item.Cells(xVal - 1).CssClass = "GroupHeaderStyleBorderLeft"
'                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then
'                    e.Item.Cells(xVal - 1).Text = "&nbsp;" & e.Item.Cells(0).Text

'                    'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
'                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
'                    If e.Item.Cells(8).Text.ToString().Trim <> "" AndAlso e.Item.Cells(8).Text.Trim <> "&nbsp;" Then
'                        e.Item.Cells(8).Text = CDate(e.Item.Cells(8).Text).Date.ToShortDateString
'                    End If

'                    If e.Item.Cells(9).Text.ToString().Trim <> "" AndAlso e.Item.Cells(9).Text.Trim <> "&nbsp;" Then
'                        e.Item.Cells(9).Text = CDate(e.Item.Cells(9).Text).Date.ToShortDateString
'                    End If

'                    'Shani (26-Sep-2016) -- Start
'                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                    'If e.Item.Cells(14).Text.ToString.Trim <> "" AndAlso e.Item.Cells(14).Text.Trim <> "&nbsp;" Then
'                    '    Dim idec As Decimal = 0
'                    '    Decimal.TryParse(e.Item.Cells(14).Text, idec)
'                    '    e.Item.Cells(14).Text = CDbl(idec).ToString
'                    'End If
'                    'Shani (26-Sep-2016) -- End


'                    'Pinkal (16-Apr-2016) -- End
'                End If
'                If IsNumeric(e.Item.Cells(12).Text) Then
'                    iWeightTotal = iWeightTotal + CDec(e.Item.Cells(12).Text)
'                End If
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    'Shani (31-Aug-2016) -- Start
'    'Enhancement - Change Competencies List Design Given by Andrew
'    Protected Sub link_Click(ByVal sender As Object, ByVal e As System.EventArgs)
'        Try
'            txtData.Text = ""
'            Dim lnkWhatIsThis As LinkButton = CType(sender, LinkButton)
'            Dim xRow As DataGridItem = CType(lnkWhatIsThis.NamingContainer, DataGridItem)
'            If CBool(xRow.Cells(14).Text) = True Then
'                Dim objCOMaster As New clsCommon_Master
'                objCOMaster._Masterunkid = CInt(xRow.Cells(16).Text)
'                If objCOMaster._Description <> "" Then
'                    txtData.Text = objCOMaster._Description
'                    popup_ComInfo.Show()
'                Else
'                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
'                End If
'                objCOMaster = Nothing
'            ElseIf CBool(xRow.Cells(14).Text) = False Then
'                Dim objCPMsater As New clsassess_competencies_master
'                objCPMsater._Competenciesunkid = CInt(xRow.Cells(12).Text)
'                If objCPMsater._Description <> "" Then
'                    txtData.Text = objCPMsater._Description
'                    popup_ComInfo.Show()
'                Else
'                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
'                End If
'                objCPMsater = Nothing
'            End If
'            dgvGE.DataSource = dtGE_TabularGrid
'            dgvGE.DataBind()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "link_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'Shani (31-Aug-2016) -- End
'#End Region

'#Region " Combobox Event "

'    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
'        Try
'            Dim objMapping As New clsAssess_Field_Mapping
'            iLinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
'            iMappingUnkid = objMapping.Get_MappingUnkId(CInt(cboPeriod.SelectedValue))
'            objMapping._Mappingunkid = iMappingUnkid
'            objMapping = Nothing

'            Dim objFMaster As New clsAssess_Field_Master
'            iExOrdr = objFMaster.Get_Field_ExOrder(iLinkedFieldId, True)
'            objFMaster = Nothing

'            objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)
'            'S.SANDEEP [29 DEC 2015] -- START
'            'Shani (26-Sep-2016) -- Start
'            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'            'objCCustomTran._AllowCustomItemInPlanning = CBool(Session("IncludeCustomItemInPlanning"))
'            'Shani (26-Sep-2016) -- End

'            objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
'            'S.SANDEEP [29 DEC 2015] -- END
'            mdtCustomEvaluation = objCCustomTran._DataTable

'            If CInt(cboPeriod.SelectedValue) > 0 Then
'                If CStr(Session("Perf_EvaluationOrder")).Trim.Length > 0 Then
'                    Dim objCHeader As New clsassess_custom_header
'                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
'                    dsHeaders.Tables(0).Rows.Clear()
'                    Dim xRow As DataRow = Nothing
'                    Dim iOrdr() As String = CStr(Session("Perf_EvaluationOrder")).Split("|")
'                    If iOrdr.Length > 0 Then
'                        Select Case CInt(iOrdr(0))
'                            Case enEvaluationOrder.PE_BSC_SECTION
'                                xRow = dsHeaders.Tables(0).NewRow

'                                'Shani (26-Sep-2016) -- Start
'                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                                'xRow.Item("Id") = -3 : xRow.Item("Name") = "Goals Evaluation" : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                'Shani (26-Sep-2016) -- End

'                                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
'                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
'                                xRow = dsHeaders.Tables(0).NewRow
'                                xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
'                            Case enEvaluationOrder.PE_CUSTOM_SECTION
'                                dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
'                        End Select
'                        Select Case CInt(iOrdr(1))
'                            Case enEvaluationOrder.PE_BSC_SECTION
'                                xRow = dsHeaders.Tables(0).NewRow

'                                'Shani (26-Sep-2016) -- Start
'                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                                'xRow.Item("Id") = -3 : xRow.Item("Name") = "Goals Evaluation" : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                'Shani (26-Sep-2016) -- End

'                                If dsHeaders.Tables(0).Rows.Count > 0 Then
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
'                                Else
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
'                                End If
'                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
'                                xRow = dsHeaders.Tables(0).NewRow
'                                xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                If dsHeaders.Tables(0).Rows.Count > 0 Then
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
'                                Else
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
'                                End If
'                            Case enEvaluationOrder.PE_CUSTOM_SECTION
'                                If dsHeaders.Tables(0).Rows.Count > 0 Then
'                                    Dim dsList As New DataSet
'                                    dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
'                                    dsHeaders.Tables(0).Merge(dsList.Tables(0), True)
'                                Else
'                                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
'                                End If
'                        End Select
'                        Select Case CInt(iOrdr(2))
'                            Case enEvaluationOrder.PE_BSC_SECTION
'                                xRow = dsHeaders.Tables(0).NewRow

'                                'Shani (26-Sep-2016) -- Start
'                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                                'xRow.Item("Id") = -3 : xRow.Item("Name") = "Goals Evaluation" : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                'Shani (26-Sep-2016) -- End

'                                If dsHeaders.Tables(0).Rows.Count > 0 Then
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
'                                Else
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
'                                End If
'                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
'                                xRow = dsHeaders.Tables(0).NewRow
'                                xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                If dsHeaders.Tables(0).Rows.Count > 0 Then
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
'                                Else
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
'                                End If
'                            Case enEvaluationOrder.PE_CUSTOM_SECTION
'                                If dsHeaders.Tables(0).Rows.Count > 0 Then
'                                    Dim dsList As New DataSet
'                                    dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
'                                    dsHeaders.Tables(0).Merge(dsList.Tables(0), True)
'                                Else
'                                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
'                                End If
'                        End Select
'                    End If
'                    objCHeader = Nothing
'                End If
'            Else
'                If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables.Count > 0 Then
'                    dsHeaders.Tables(0).Rows.Clear()
'                End If
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'#End Region

'End Class

'Shani (23-Nov123-2016-2016) -- End

'Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Balance Score Card Evaluation") -- Later Change it Back
