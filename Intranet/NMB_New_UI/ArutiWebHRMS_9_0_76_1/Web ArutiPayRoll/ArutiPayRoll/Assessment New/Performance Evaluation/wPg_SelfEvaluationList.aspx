﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_SelfEvaluationList.aspx.vb"
    Inherits="wPg_SelfEvaluationList" Title="Self Performance Assessment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>--%>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, args) {
            SetGeidScrolls();
        }
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    </script>

    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Self Performance Assessment"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r-10 p-l-0">
                                    <asp:LinkButton ID="lnkAllocation" runat="server" Tootip="Allocation">
                                                                          <i class="fas fa-sliders-h"></i>
                                    </asp:LinkButton>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                        <asp:Label ID="lblYear" runat="server" Text="Year" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboYear" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 m-t-25">
                                        <asp:CheckBox ID="chkShowcommitted" runat="server" Text="Show committed" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                        <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <uc2:DateCtrl ID="dtpDate" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 m-t-25">
                                        <asp:CheckBox ID="chkShowUncommitted" runat="server" Text="Show Uncommitted" />
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="Start New Assessment" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search for Existing Assessment" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlAssmtList" runat="server" Width="100%" Height="100%" Visible="false">
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:DataGrid ID="lvAssesmentList" runat="server" runat="server" AutoGenerateColumns="false"
                                                Width="99%" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgEdit" runat="server" CssClass="gridedit" CommandName="Edit"
                                                                    ToolTip="Edit"><i class="fas fa-pencil-alt text-primary" ></i></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgDelete" runat="server" CommandName="Delete" ToolTip="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="imgUnlock" runat="server" CommandName="Unlock" ToolTip="Unlock"><i class="fas fa-unlock-alt"></i></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="smode" HeaderText="Mode" FooterText="colhMode"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="YearName" HeaderText="Year" FooterText="colhYear"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="PName" HeaderText="Period" FooterText="colhPeriod"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="assessmentdate" HeaderText="Assessment Date" FooterText="colhAssessmentdate">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="EmpName" HeaderText="Employee" FooterText="objcolhEmployee">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="smodeid" HeaderText="smodeid" FooterText="" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="assessgroupunkid" HeaderText="" FooterText="" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="periodunkid" FooterText="objcolhPeriodId" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="selfemployeeunkid" FooterText="objcolhEmpId" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="iscommitted" FooterText="objcolhiscommitted" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Sid" FooterText="Sid" Visible="false"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="analysisunkid" FooterText="objcolhanalysisunkid" Visible="false">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="" HeaderText="Score" ItemStyle-HorizontalAlign="Right">
                                                    </asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="objcolhViewRating">
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="lnkDescription" runat="server" ToolTip="View Rating Info" OnClick="lnkInfo_Click"><i class="fa fa-info-circle" style="font-size:19px; color:Blue;"></i></asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </asp:Panel>
                <%--'S.SANDEEP |12-APR-2021| -- START--%>
                <%--'ISSUE/ENHANCEMENT : AVG CMP SCORE--%>
                <asp:Panel ID="pnlCmptList" runat="server" Width="100%" Height="100%" Visible="false">
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                            <div class="card">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                            <div class="table-responsive" style="height: 400px">
                                                <asp:DataGrid ID="dgvGE" runat="server" runat="server" AutoGenerateColumns="false"
                                                    Width="99%" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="imgCmptEdit" runat="server" CssClass="gridedit" CommandName="Edit"
                                                                        ToolTip="Edit"><i class="fas fa-pencil-alt text-primary" ></i></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--0--%>
                                                        <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="imgCmptDelete" runat="server" CommandName="Delete" ToolTip="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--1--%>
                                                        <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                            ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="imgCmptUnlock" runat="server" CommandName="Unlock" ToolTip="Unlock"><i class="fas fa-unlock-alt"></i></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--2--%>
                                                        <asp:BoundColumn DataField="eval_item" HeaderText="Items" FooterText="dgcolheval_itemGE"
                                                            ItemStyle-Width="55%" HeaderStyle-Width="55%" />
                                                        <%--3--%>
                                                        <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="dgcolhGEWeight"
                                                            ItemStyle-Width="10%" HeaderStyle-Width="10%" />
                                                        <%--4--%>
                                                        <asp:BoundColumn DataField="eself" HeaderText="Result" FooterText="dgcolheselfGE"
                                                            ItemStyle-Width="6%" HeaderStyle-Width="6%" />
                                                        <%--5--%>
                                                        <asp:BoundColumn DataField="" HeaderText="Final Score" FooterText="objdgcolhedisplayGE"
                                                            Visible="false" />
                                                        <%--6--%>
                                                        <asp:BoundColumn DataField="eremark" HeaderText="Self Remark" FooterText="dgcolheremarkGE"
                                                            ItemStyle-Width="20%" />
                                                        <%--7--%>
                                                        <asp:BoundColumn DataField="scalemasterunkid" FooterText="objdgcolhscalemasterunkidGE"
                                                            Visible="false" />
                                                        <%--6--%>
                                                        <asp:BoundColumn DataField="competenciesunkid" FooterText="objdgcolhcompetenciesunkidGE"
                                                            Visible="false" />
                                                        <%--7--%>
                                                        <asp:BoundColumn DataField="assessgroupunkid" FooterText="objdgcolhassessgroupunkidGE"
                                                            Visible="false" />
                                                        <%--8--%>
                                                        <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrpGE" Visible="false" />
                                                        <%--9--%>
                                                        <asp:BoundColumn DataField="IsPGrp" FooterText="objdgcolhIsPGrpGE" Visible="false" />
                                                        <%--10--%>
                                                        <asp:BoundColumn DataField="GrpId" FooterText="objdgcolhGrpIdGE" Visible="false" />
                                                        <%--11--%>
                                                        <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                            HeaderText="Info." ItemStyle-Width="5%" HeaderStyle-Width="5%" FooterText="objdgcolhInformation">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkCol" runat="server" CommandName="viewdescription" Font-Underline="false"
                                                                    Enabled="false"><i class="fa fa-info-circle" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <%--12--%>
                                                        <asp:BoundColumn DataField="" FooterText="objdgcolhMaxScale" Visible="false" />
                                                        <%--13--%>
                                                        <asp:BoundColumn DataField="analysistranunkid" FooterText="objdgcolhanalysistranunkid"
                                                            Visible="false"></asp:BoundColumn>
                                                        <%--14--%>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <%--'S.SANDEEP |12-APR-2021| -- END--%>
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <cc1:ModalPopupExtender ID="popup_YesNo" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnNo" PopupControlID="pnl_YesNo" TargetControlID="hdf_popupYesNo">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_YesNo" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblMessage" runat="server" Text="Message:" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Style="resize: none"
                                            class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_popupYesNo" runat="server" />
                        <asp:HiddenField ID="hdf_analysisunkid" runat="server" />
                        <asp:HiddenField ID="hdf_PeriodID" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ComputeYesNo" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btn_ValidNo" PopupControlID="pnl_ComputeValid" TargetControlID="hdf_Valid">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ComputeValid" runat="server" CssClass="card modal-dialog" Style="display: none;"
                    DefaultButton="btn_ValidYes">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblComputeTitle" runat="server" Text="Title"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblComputeMessage" runat="server" Text="Message :" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtVoidMessage" runat="server" TextMode="MultiLine" Style="resize: none"
                                            class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btn_ValidYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btn_ValidNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_Valid" runat="server" />
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hdf_ItemIndex" runat="server" />
                <asp:HiddenField ID="hdf_CommondName" runat="server" />
                <cc1:ModalPopupExtender ID="popupinfo" BackgroundCssClass="modalBackground" TargetControlID="lblRatingInfo"
                    runat="server" PopupControlID="pnlRatingInfo" CancelControlID="btnCloseRating" />
                <asp:Panel ID="pnlRatingInfo" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblRatingInfo" runat="server" Text="Ratings Information"></asp:Label>
                        </h2>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" style="height: 200px">
                                        <asp:GridView ID="gvRating" runat="server" AutoGenerateColumns="false" Width="99%"
                                            AllowPaging="false" CssClass="table table-hover table-bordered">
                                            <Columns>
                                                <asp:BoundField DataField="scrf" HeaderText="Score From" FooterText="dgcolhScrFrm"
                                                    HeaderStyle-Width="100px" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="scrt" HeaderText="Score To" FooterText="dgcolhScrTo" HeaderStyle-Width="100px"
                                                    HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
                                                <asp:BoundField DataField="name" HeaderText="Rating" FooterText="dgcolhRating" />
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnCloseRating" runat="server" Text="Close" CssClass="btn btn-primary" />
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
