﻿<%@ Page Title="Performance Evaluation" Language="VB" MasterPageFile="~/home.master"
    AutoEventWireup="false" CodeFile="wPgReviewerEvaluation2.aspx.vb" Inherits="wPgReviewerEvaluation2" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="CnfCtrl" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (cval.length > 0)
                if (charCode == 45)
                if (cval.indexOf("-") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <script type="text/javascript">
        function setscrollPosition(sender) {
            sender.scrollLeft = document.getElementById("<%=hdf_leftposition.ClientID%>").value;
            sender.scrollTop = document.getElementById("<%=hdf_topposition.ClientID%>").value;
        }
        function getBSCscrollPosition() {
            document.getElementById("<%=hdf_topposition.ClientID%>").value = document.getElementById("<%=objpnlBSC.ClientID%>").scrollTop;
            document.getElementById("<%=hdf_leftposition.ClientID%>").value = document.getElementById("<%=objpnlBSC.ClientID%>").scrollLeft;
        }
        function getGEscrollPosition() {
            document.getElementById("<%=hdf_topposition.ClientID%>").value = document.getElementById("<%=objpnlGE.ClientID%>").scrollTop;
            document.getElementById("<%=hdf_leftposition.ClientID%>").value = document.getElementById("<%=objpnlGE.ClientID%>").scrollLeft;
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition3" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        var scroll2 = {
            Y: '#<%= hfScrollPosition2.ClientID %>'
        };
        var scroll3 = {
            Y: '#<%= hfScrollPosition3.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            SetGeidScrolls();
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
                $("#scrollable-container2").scrollTop($(scroll2.Y).val());
                $("#scrollable-container1").scrollLeft($(scroll3.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("overflow", "auto");
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("overflow", "none");
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Reviewer Performace Evaluation"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%; border: none;">
                                        <tr style="width: 100%" id="Combo">
                                            <td style="width: 8%">
                                                <asp:Label ID="lblReviewer" runat="server" Text="Reviewer"></asp:Label>
                                            </td>
                                            <td style="width: 16%">
                                                <asp:DropDownList ID="cboReviewer" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblEmployee" runat="server" Text="Employee"></asp:Label>
                                            </td>
                                            <td style="width: 16%">
                                                <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 8%">
                                                <asp:Label ID="lblPeriod" runat="server" Text="Period"></asp:Label>
                                            </td>
                                            <td style="width: 16%">
                                                <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 6%">
                                                <asp:Label ID="lblAssessDate" runat="server" Text="Date"></asp:Label>
                                            </td>
                                            <td style="width: 18%">
                                                <uc2:DateCtrl ID="dtpAssessdate" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <asp:LinkButton ID="lnkCopyScore" runat="server" Text="Copy Assessor Score" Enabled="false"></asp:LinkButton>
                                        <asp:Button ID="BtnSearch" runat="server" CssClass="btndefault" Text="Search" Width="77px" />
                                        <asp:Button ID="BtnReset" runat="server" CssClass="btndefault" Text="Reset" Width="77px" />
                                    </div>
                                </div>
                            </div>
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-heading-default">
                                    <div style="text-align: center">
                                        <asp:Label ID="objlblCaption" runat="server" Text="#value" Font-Bold="true"></asp:Label>
                                    </div>
                                </div>
                                <div id="Div3" class="panel-body-default">
                                    <table id="tblGrids" width="100%">
                                        <tr style="width: 100%;" id="Grids">
                                            <td style="width: 100%" align="justify">
                                                <asp:Panel ID="objpnlInstruction" runat="server" Width="100%" Height="450px" Visible="false">
                                                    <asp:TextBox ID="txtInstruction" ReadOnly="true" runat="server" TextMode="MultiLine"
                                                        Height="100%" Width="99%" CssClass="textarea" Wrap="true"></asp:TextBox>
                                                    <%--'S.SANDEEP |12-FEB-2019| -- START--%>
                                                    <%--'ISSUE/ENHANCEMENT : {Performance Assessment Changes}--%>
                                                    <%--'ADDED : CssClass="textarea"--%>
                                                    <%--'S.SANDEEP |12-FEB-2019| -- END--%>
                                                </asp:Panel>
                                                <asp:Panel ID="objpnlBSC" runat="server" Width="100%" Visible="false" CssClass="gridscroll"
                                                    Style="max-width: 1130px;" ScrollBars="Auto">
                                                    <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);">
                                                        <asp:DataGrid ID="dgvBSC" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                            HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                            HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:BoundColumn DataField="Field1" HeaderText="" FooterText="objdgcolhBSCField1" />
                                                                <%--0--%>
                                                                <asp:BoundColumn DataField="Field2" HeaderText="" FooterText="objdgcolhBSCField2" />
                                                                <%--1--%>
                                                                <asp:BoundColumn DataField="Field3" HeaderText="" FooterText="objdgcolhBSCField3" />
                                                                <%--2--%>
                                                                <asp:BoundColumn DataField="Field4" HeaderText="" FooterText="objdgcolhBSCField4" />
                                                                <%--3--%>
                                                                <asp:BoundColumn DataField="Field5" HeaderText="" FooterText="objdgcolhBSCField5" />
                                                                <%--4--%>
                                                                <asp:BoundColumn DataField="Field6" HeaderText="" FooterText="objdgcolhBSCField6" />
                                                                <%--5--%>
                                                                <asp:BoundColumn DataField="Field7" HeaderText="" FooterText="objdgcolhBSCField7" />
                                                                <%--6--%>
                                                                <asp:BoundColumn DataField="Field8" HeaderText="" FooterText="objdgcolhBSCField8" />
                                                                <%--7--%>
                                                                <asp:BoundColumn DataField="St_Date" HeaderText="Start Date" ReadOnly="true" FooterText="dgcolhSDate" />
                                                                <%--8--%>
                                                                <asp:BoundColumn DataField="Ed_Date" HeaderText="End Date" ReadOnly="true" FooterText="dgcolhEDate" />
                                                                <%--9--%>
                                                                <asp:BoundColumn DataField="pct_complete" HeaderText="% Completed" FooterText="dgcolhCompleted" />
                                                                <%--10--%>
                                                                <asp:BoundColumn DataField="dgoaltype" HeaderText="Goal Type" ReadOnly="true" FooterText="dgcolhGoalType" />
                                                                <%--11--%>
                                                                <asp:BoundColumn DataField="dgoalvalue" HeaderText="Goal Value" ReadOnly="true" ItemStyle-HorizontalAlign="Right"
                                                                    FooterText="dgoalvalue" />
                                                                <%--12--%>
                                                                <asp:BoundColumn DataField="CStatus" HeaderText="Status" FooterText="dgcolhStatus" />
                                                                <%--13--%>
                                                                <asp:TemplateColumn HeaderText="Score Guide" HeaderStyle-HorizontalAlign="Center"
                                                                    FooterText="dgcolhBSCScore">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="dgcolhBSCScore" runat="server" Width="100%" Text='<%# Eval("score") %>'
                                                                            CommandArgument='<%# Eval("scalemasterunkid") %>' OnClick="dgcolhBSCScore_Click"></asp:LinkButton>
                                                                        <%--'S.SANDEEP |20-SEP-2019| -- START--%>
                                                                        <%--'ISSUE/ENHANCEMENT : {Ref#0004155}--%>
                                                                        <asp:HiddenField ID="dgcolhBSCScoreId" runat="server" Value='<%# Eval("scalemasterunkid") %>' />
                                                                        <%--'S.SANDEEP |20-SEP-2019| -- END--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--14--%>
                                                                <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="dgcolhBSCWeight" />
                                                                <%--15--%>
                                                                <asp:BoundColumn DataField="eself" HeaderText="Self - Score" FooterText="dgcolheselfBSC"
                                                                    ItemStyle-HorizontalAlign="Center" />
                                                                <%--16--%>
                                                                <asp:BoundColumn DataField="eremark" HeaderText="Self - Remark" FooterText="dgcolheremarkBSC" />
                                                                <%--17--%>
                                                                <asp:BoundColumn DataField="aself" HeaderText="Assessor - Score" FooterText="dgcolhaselfBSC" />
                                                                <%--18--%>
                                                                <asp:BoundColumn DataField="aremark" HeaderText="Assessor - Remark" FooterText="dgcolharemarkBSC" />
                                                                <%--19--%>
                                                                <asp:BoundColumn DataField="agreed_score" HeaderText="Agreed Score" FooterText="dgcolhAgreedScoreBSC" />
                                                                <%--20--%>
                                                                <asp:TemplateColumn HeaderText="Result" FooterText="dgcolhrselfBSC">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="dgcolhrselfBSC" runat="server" Text="" AutoPostBack="true" CssClass="removeTextcss decimal"
                                                                            Style="text-align: right" OnTextChanged="TxtValRemarkBSC_TextChanged" onblur="getBSCscrollPosition()"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--21--%>
                                                                <asp:TemplateColumn HeaderText="Remark" FooterText="dgcolhrremarkBSC">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="dgcolhrremarkBSC" runat="server" AutoPostBack="true" CssClass="removeTextcss"
                                                                            Text="" OnTextChanged="TxtValRemarkBSC_TextChanged" onblur="getBSCscrollPosition()"
                                                                            TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <%--22--%>
                                                                <asp:BoundColumn DataField="IsGrp" HeaderText="" FooterText="objdgcolhIsGrpBSC" Visible="false" />
                                                                <%--23--%>
                                                                <asp:BoundColumn DataField="GrpId" HeaderText="" FooterText="objdgcolhGrpIdBSC" Visible="false" />
                                                                <%--24--%>
                                                                <asp:BoundColumn DataField="scalemasterunkid" FooterText="objdgcolhScaleMasterId"
                                                                    Visible="false" />
                                                                <%--25--%>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel ID="objpnlGE" runat="server" Visible="false">
                                                    <div id="scrollable-container1" onscroll="$(scroll1.Y).val(this.scrollTop);$(scroll3.Y).val(this.scrollLeft)"
                                                        style="width: 100%; overflow: auto" class="gridscroll">
                                                        <asp:DataGrid ID="dgvGE" runat="server" Width="99.5%" AutoGenerateColumns="false"
                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                                <asp:BoundColumn DataField="eval_item" HeaderText="Items" FooterText="dgcolheval_itemGE"
                                                                    ItemStyle-Width="30%" HeaderStyle-Width="30%" />
                                                                <asp:BoundColumn DataField="Weight" HeaderText="Weight" FooterText="dgcolhGEWeight"
                                                                    ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                                <asp:TemplateColumn HeaderText="Score Guide" HeaderStyle-HorizontalAlign="Center"
                                                                    ItemStyle-Width="10%" HeaderStyle-Width="10%" FooterText="dgcolhGEScore">
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="dgcolhGEScore" runat="server" Width="100%" Text='<%# Eval("score") %>'
                                                                            CommandArgument='<%# Eval("scalemasterunkid") %>' OnClick="dgcolhGEScore_Click"></asp:LinkButton>
                                                                        <%--'S.SANDEEP |20-SEP-2019| -- START--%>
                                                                        <%--'ISSUE/ENHANCEMENT : {Ref#0004155}--%>
                                                                        <asp:HiddenField ID="dgcolhGEScoreId" runat="server" Value='<%# Eval("scalemasterunkid") %>' />
                                                                        <%--'S.SANDEEP |20-SEP-2019| -- END--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="eself" HeaderText="Self - Score" ItemStyle-HorizontalAlign="Right"
                                                                    FooterText="dgcolheselfGE" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                                <asp:BoundColumn DataField="escore" HeaderText="Final Score" FooterText="objdgcolhedisplayGE"
                                                                    Visible="false" />
                                                                <asp:BoundColumn DataField="eremark" HeaderText="Self - Remark" FooterText="dgcolheremarkGE"
                                                                    ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                                                <asp:BoundColumn DataField="aself" HeaderText="Assessor - Score" ItemStyle-HorizontalAlign="Right"
                                                                    FooterText="dgcolhaselfGE" ItemStyle-Width="5%" HeaderStyle-Width="5%" />
                                                                <asp:BoundColumn DataField="ascore" HeaderText="Final Score" FooterText="objdgcolhadisplayGE"
                                                                    Visible="false" />
                                                                <asp:BoundColumn DataField="aremark" HeaderText="Assessor - Remark" FooterText="dgcolharemarkGE"
                                                                    ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                                                                <asp:BoundColumn DataField="agreed_score" HeaderText="Agreed Score" FooterText="dgcolhAgreedScoreGE" />
                                                                <asp:TemplateColumn HeaderText="Result" FooterText="dgcolhrselfGE" ItemStyle-Width="5%"
                                                                    HeaderStyle-Width="5%">
                                                                    <ItemStyle HorizontalAlign="Right" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="dgcolhrselfGE" runat="server" Text="" AutoPostBack="true" Width="100%"
                                                                            CssClass="removeTextcss" OnTextChanged="TxtValRemarkGE_TextChanged" onblur="getGEscrollPosition()"
                                                                            Style="text-align: right;"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="" HeaderText="Final Score" FooterText="objdgcolhrdisplayGE"
                                                                    Visible="false" />
                                                                <asp:TemplateColumn HeaderText="Remark" FooterText="dgcolhrremarkGE" ItemStyle-Width="15%"
                                                                    HeaderStyle-Width="15%">
                                                                    <ItemStyle HorizontalAlign="Left" />
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="dgcolhrremarkGE" runat="server" Text="" Width="100%" onblur="getGEscrollPosition()"
                                                                            TextMode="MultiLine" Rows="3" CssClass="removeTextcss" OnTextChanged="TxtValRemarkGE_TextChanged"
                                                                            AutoPostBack="true"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                                <asp:BoundColumn DataField="scalemasterunkid" FooterText="objdgcolhscalemasterunkidGE"
                                                                    Visible="false" />
                                                                <asp:BoundColumn DataField="competenciesunkid" FooterText="objdgcolhcompetenciesunkidGE"
                                                                    Visible="false" />
                                                                <asp:BoundColumn DataField="assessgroupunkid" FooterText="objdgcolhassessgroupunkidGE"
                                                                    Visible="false" />
                                                                <asp:BoundColumn DataField="IsGrp" FooterText="objdgcolhIsGrpGE" Visible="false" />
                                                                <asp:BoundColumn DataField="IsPGrp" FooterText="objdgcolhIsPGrpGE" Visible="false" />
                                                                <asp:BoundColumn DataField="GrpId" HeaderText="" FooterText="objdgcolhGrpIdGE" Visible="false" />
                                                                <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                                    HeaderText="Info." ItemStyle-Width="5%" HeaderStyle-Width="5%" FooterText="objdgcolhInformation">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkCol" runat="server" OnClick="link_Click" CommandName="viewdescription"
                                                                            Font-Underline="false"><i class="fa fa-info-circle" style="font-size:20px;color:Blue"></i></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateColumn>
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel ID="objpnlCItems" runat="server" Visible="false">
                                                    <div id="scrollable-container2" onscroll="$(scroll2.Y).val(this.scrollTop);" style="max-width: 1130px;
                                                        height: 450px; overflow: auto" class="gridscroll">
                                                        <asp:GridView ID="dgvItems" runat="server" Width="99%" AutoGenerateColumns="false"
                                                            CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                            AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                            <Columns>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr style="width: 100%;" id="NextPribtn">
                                            <td style="width: 100%">
                                                <table id="Table1" style="width: 100%;" runat="server">
                                                    <tr style="width: 100%;">
                                                        <td align="left" style="width: 43%">
                                                            <table style="width: 100%" border="1" cellspacing="0">
                                                                <tr style="width: 100%;">
                                                                    <td align="left" style="width: 25%">
                                                                        <asp:Label ID="objlblValue1" runat="server" Width="100%" Text="" Font-Bold="true"
                                                                            Visible="false"></asp:Label>
                                                                    </td>
                                                                    <td align="left" style="width: 25%">
                                                                        <asp:Label ID="objlblValue2" runat="server" Width="100%" Text="" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td align="left" style="width: 25%">
                                                                        <asp:Label ID="objlblValue3" runat="server" Width="100%" Text="" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td align="left" style="width: 25%">
                                                                        <asp:Label ID="objlblValue4" runat="server" Width="100%" Text="" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        <div style="float: left">
                                            <asp:Button ID="objbtnBack" runat="server" CssClass="btndefault" Text="Previous" />
                                            <asp:Button ID="objbtnNext" runat="server" CssClass="btndefault" Text="Next" />
                                        </div>
                                        <div>
                                            <asp:Button ID="btnSave" runat="server" CssClass="btndefault" Text="Save" />
                                            <asp:Button ID="btnSaveCommit" runat="server" CssClass="btndefault" Text="Save & Submit" />
                                            <asp:Button ID="btnClose" runat="server" CssClass="btndefault" Text="Close" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <table>
                                <tr id="ScoreGuideBSC">
                                    <td>
                                        <cc1:ModalPopupExtender ID="popup_ViewGuideBSC" runat="server" TargetControlID="hdf_btnScoreGuideBSC"
                                            CancelControlID="btnCancelGuideBSC" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_ViewGuideBSC"
                                            Drag="True">
                                        </cc1:ModalPopupExtender>
                                        <%-- <asp:Panel ID="" runat="server" Width="" Height="265px" Style="display: none;
                                            background-color: #FFFFFF; border-style: solid; border-width: thin; margin-left: auto;
                                            margin-right: auto;">
                                            <div style="margin-left: 25px; margin-right: 25px; border-width: 1px; border-style: solid;
                                                float: left; background-color: #FFFFFF; margin-top: 25px; margin-bottom: 25px;">
                                            </div>
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnl_ViewGuideBSC" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 815px">
                                            <div class="panel-primary" style="margin-bottom: 0px">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblBSCScoreHeading" runat="server" Text="Score Guide"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div4" class="panel-default">
                                                        <div id="Div5" class="panel-heading-default">
                                                            <div style="float: left;">
                                                            </div>
                                                        </div>
                                                        <div id="Div6" class="panel-body-default">
                                                            <table style="width: 100%;">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%;">
                                                                        <asp:Panel ID="pnl_dgvBSCScoreGuide" runat="server" Width="100%" Height="150px" ScrollBars="Auto">
                                                                            <asp:GridView ID="dgvBSCScoreGuide" runat="server" Width="99%" AutoGenerateColumns="false"
                                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="scale" HeaderText="Scale" ReadOnly="true" />
                                                                                    <asp:BoundField DataField="description" HeaderStyle-HorizontalAlign="Left" HeaderText="Description"
                                                                                        ReadOnly="true" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:HiddenField ID="hdf_btnScoreGuideBSC" runat="server" />
                                                                <asp:Button ID="btnCancelGuideBSC" runat="server" Text="Cancel" CssClass="btndefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="ScoreGuideGE">
                                    <td>
                                        <cc1:ModalPopupExtender ID="popup_ViewGuideGE" runat="server" TargetControlID="hdf_btnScoreGuide"
                                            CancelControlID="btnCancelGuide" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_ViewGuide"
                                            Drag="True">
                                        </cc1:ModalPopupExtender>
                                        <%-- <asp:Panel ID="" runat="server" Width="" Height="265px" Style="display: none; background-color: #FFFFFF;
                                            border-style: solid; border-width: thin; margin-left: auto; margin-right: auto;">
                                            <div style="margin-left: 25px; margin-right: 25px; border-width: 1px; border-style: solid;
                                                float: left; background-color: #FFFFFF; margin-top: 25px; margin-bottom: 25px;">
                                            </div>
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnl_ViewGuide" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 815px">
                                            <div class="panel-primary" style="margin-bottom: 0px">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblGEScoreHeading" runat="server" Text="Score Guide"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div7" class="panel-default">
                                                        <div id="Div8" class="panel-heading-default">
                                                            <div style="float: left;">
                                                            </div>
                                                        </div>
                                                        <div id="Div9" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%;">
                                                                        <asp:Panel ID="pnl_dgvGEScoreGuide" runat="server" Width="100%" Height="150px" ScrollBars="Auto">
                                                                            <asp:GridView ID="dgvGEScoreGuide" runat="server" Width="99%" AutoGenerateColumns="false"
                                                                                CssClass="gridview" HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem"
                                                                                AllowPaging="false" HeaderStyle-Font-Bold="false">
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="scale" HeaderText="Scale" ReadOnly="true" />
                                                                                    <asp:BoundField DataField="description" HeaderStyle-HorizontalAlign="Left" HeaderText="Description"
                                                                                        ReadOnly="true" />
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:HiddenField ID="hdf_btnScoreGuide" runat="server" />
                                                                <asp:Button ID="btnCancelGuide" runat="server" Text="Cancel" CssClass="btndefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="popup_AddGE">
                                    <td>
                                        <cc1:ModalPopupExtender ID="popup_CItemAddEdit" runat="server" TargetControlID="hdf_cItem"
                                            CancelControlID="hdf_cItem" DropShadow="true" BackgroundCssClass="modal-backdrop"
                                            PopupControlID="pnl_CItemAddEdit" Drag="True">
                                        </cc1:ModalPopupExtender>
                                        <%--<asp:Panel ID="" runat="server" Width="" Style="display: none; background-color: #FFFFFF;
                                            border-width: thin; margin-left: auto; margin-right: auto;">
                                            <div style="margin-left: 25px; margin-right: 25px; border-width: 1px; border-style: solid;
                                                width: 93%; float: left; background-color: #FFFFFF; margin-top: 25px; margin-bottom: 25px;">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 100%">
                                                            <asp:DataGrid ID="" runat="server" AutoGenerateColumns="false" HeaderStyle-CssClass="GroupHeaderStyle">
                                                                <Columns>
                                                                    <asp:BoundColumn HeaderText="Custom Items" DataField="custom_item" FooterText="clmItem"
                                                                        HeaderStyle-Width="450px" ItemStyle-Width="450px"></asp:BoundColumn>
                                                                    <asp:TemplateColumn FooterText="clmValue" HeaderText="Custom Value" HeaderStyle-Width="250px"
                                                                        ItemStyle-Width="250px"></asp:TemplateColumn>
                                                                    <asp:BoundColumn DataField="itemtypeid" Visible="false" FooterText="clmCntType">
                                                                    </asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="rOnly" Visible="false" FooterText="clmrOnly"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="customitemunkid" Visible="false"></asp:BoundColumn>
                                                                    <asp:BoundColumn DataField="selectionmodeid" Visible="false"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="text-align: right; width: 100%">
                                                            <asp:HiddenField ID="" runat="server" />
                                                            <asp:Button ID="" runat="server" Text="Add" CssClass="btndefault" />
                                                            <asp:Button ID="" runat="server" Text="Close" CssClass="btndefault" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnl_CItemAddEdit" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 750px">
                                            <div class="panel-primary" style="margin-bottom: 0px">
                                                <div class="panel-heading">
                                                    <asp:Label ID="Label4" runat="server" Text="Custom Items"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div10" class="panel-default">
                                                        <div id="Div11" class="panel-heading-default">
                                                            <div style="float: left;">
                                                            </div>
                                                        </div>
                                                        <div id="Div12" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:Panel ID="pnl_dgv_Citems" runat="server" Style="height: 360px !important" ScrollBars="Auto">
                                                                            <asp:DataGrid ID="dgv_Citems" runat="server" AutoGenerateColumns="false" CssClass="gridview"
                                                                                HeaderStyle-CssClass="griviewheader" ItemStyle-CssClass="griviewitem" AllowPaging="false"
                                                                                HeaderStyle-Font-Bold="false">
                                                                                <Columns>
                                                                                    <asp:BoundColumn HeaderText="Custom Items" DataField="custom_item" FooterText="dgcolhItems"
                                                                                        HeaderStyle-Width="450px" ItemStyle-Width="450px"></asp:BoundColumn>
                                                                                    <asp:TemplateColumn FooterText="dgcolhValue" HeaderText="Custom Value" HeaderStyle-Width="250px"
                                                                                        ItemStyle-Width="250px"></asp:TemplateColumn>
                                                                                    <asp:BoundColumn DataField="itemtypeid" Visible="false" FooterText="clmCntType">
                                                                                    </asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="rOnly" Visible="false" FooterText="clmrOnly"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="customitemunkid" Visible="false"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="selectionmodeid" Visible="false"></asp:BoundColumn>
                                                                                    <asp:BoundColumn DataField="isdefaultentry" Visible="false"></asp:BoundColumn>
                                                                                    <%--'S.SANDEEP |08-JAN-2019| -- START--%>
                                                                                    <asp:BoundColumn DataField="iscompletedtraining" Visible="false"></asp:BoundColumn>
                                                                                    <%--'S.SANDEEP |08-JAN-2019| -- END--%>
                                                                                </Columns>
                                                                            </asp:DataGrid>
                                                                        </asp:Panel>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:HiddenField ID="hdf_cItem" runat="server" />
                                                                <asp:Button ID="btnIAdd" runat="server" Text="Add" CssClass="btndefault" />
                                                                <asp:Button ID="btnIClose" runat="server" Text="Close" CssClass="btndefault" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="CItemReason">
                                    <td>
                                        <cc1:ModalPopupExtender ID="popup_CItemReason" runat="server" BackgroundCssClass="modal-backdrop"
                                            CancelControlID="btnNo" PopupControlID="pnl_cItemReason" TargetControlID="hdf_cItemReason">
                                        </cc1:ModalPopupExtender>
                                        <%--<asp:Panel ID="" runat="server" Style="display: none; padding: 10px; width: ; border-style: solid;
                                            border-width: 1px; -moz-box-shadow: 5px 5px 10px #000000; -webkit-box-shadow: 5px 5px 10px #000000;
                                            box-shadow: 5px 5px 10px #000000; -moz-border-radius: 10px; -webkit-border-radius: 10px;
                                            border-radius: 10px; -khtml-border-radius: 10px;" BackColor="#5377A9" DefaultButton="btnYes">
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnl_cItemReason" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 300px">
                                            <div class="panel-primary" style="margin-bottom: 0px">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblTitle" runat="server" Text="Title" />
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div13" class="panel-default">
                                                        <div id="Div15" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:Label ID="lblMessage" runat="server" Text="Message :"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Height="50px" Style="resize: none"></asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                                <asp:Button ID="btnNo" runat="server" Text="No" CssClass="btnDefault" />
                                                                <asp:HiddenField ID="hdf_cItemReason" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="CnfYesNo">
                                    <td>
                                        <cc1:ModalPopupExtender ID="popup_CnfYesNo" runat="server" BackgroundCssClass="modal-backdrop"
                                            CancelControlID="hdf_popupCnfYesNo" PopupControlID="pnl_CnfYesNo" TargetControlID="hdf_popupCnfYesNo">
                                        </cc1:ModalPopupExtender>
                                        <%--<asp:Panel ID="" runat="server" Style="display: none; padding: 10px;
                                            width: ; border-style: solid; border-width: 1px; -moz-box-shadow: 5px 5px 10px #000000;
                                            -webkit-box-shadow: 5px 5px 10px #000000; box-shadow: 5px 5px 10px #000000; -moz-border-radius: 10px;
                                            -webkit-border-radius: 10px; border-radius: 10px; -khtml-border-radius: 10px;"
                                            BackColor="#5377A9" >
                                        </asp:Panel>--%>
                                        <asp:Panel ID="pnl_CnfYesNo" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 300px" DefaultButton="btnYes">
                                            <div class="panel-primary" style="margin-bottom: 0px">
                                                <div class="panel-heading">
                                                    <asp:Label ID="lblConfirmTitle" runat="server" Text="Aruti" />
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div16" class="panel-default">
                                                        <div id="Div18" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:Label ID="lblConfirmMessage" runat="server" Text="Message :" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="btnCnfYes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                                <asp:Button ID="btnCnfNo" runat="server" Text="No" CssClass="btnDefault" />
                                                                <asp:HiddenField ID="hdf_popupCnfYesNo" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr id="uc3">
                                    <td colspan="8">
                                        <uc3:CnfCtrl ID="CnfSaveCommit" runat="server" Title="Aruti" />
                                    </td>
                                </tr>
                                <tr id="HiddenField">
                                    <td colspan="8">
                                        <asp:HiddenField ID="hdf_topposition" runat="server" />
                                        <asp:HiddenField ID="hdf_leftposition" runat="server" />
                                    </td>
                                </tr>
                                <tr id="Comp_Information">
                                    <td>
                                        <cc1:ModalPopupExtender ID="popup_ComInfo" runat="server" BackgroundCssClass="ModalPopupBG2"
                                            CancelControlID="btnSClose" PopupControlID="pnl_CompInfo" TargetControlID="hdnfieldDelReason">
                                        </cc1:ModalPopupExtender>
                                        <asp:Panel ID="pnl_CompInfo" runat="server" CssClass="newpopup" Style="display: none;
                                            width: 450px; z-index: 100002!important;">
                                            <div class="panel-primary" style="margin-bottom: 0px;">
                                                <div class="panel-heading">
                                                    <asp:Label ID="Label2" runat="server" Text="Aruti"></asp:Label>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="Div14" class="panel-default">
                                                        <div id="Div17" class="panel-body-default">
                                                            <table style="width: 100%">
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:Label ID="lblCompInfoHeader" runat="server" Text="Description"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr style="width: 100%">
                                                                    <td style="width: 100%">
                                                                        <asp:TextBox ID="txtData" runat="server" TextMode="MultiLine" Width="97%" Height="100px"
                                                                            Style="margin-bottom: 5px;" ReadOnly="true" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <div class="btn-default">
                                                                <asp:Button ID="btnSClose" runat="server" Text="Close" Width="70px" Style="margin-right: 10px"
                                                                    CssClass="btnDefault" />
                                                                <asp:HiddenField ID="hdnfieldDelReason" runat="server" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
