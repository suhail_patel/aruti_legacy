﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
Imports System.Drawing
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Data.SqlClient
Imports System.Globalization

#End Region

Partial Class wPgAssessorEvaluation
    Inherits Basepage

#Region "Private Variables"
    Private DisplayMessage As New CommonCodes
    Private mstrModuleName As String = "frmPerformanceEvaluation"
    Private objEAnalysisMst As New clsevaluation_analysis_master
    Private objGoalsTran As New clsgoal_analysis_tran
    Private objCAssessTran As New clscompetency_analysis_tran
    Private objCCustomTran As New clscompeteny_customitem_tran
    Private menAction As enAction = enAction.ADD_ONE
    Private mintAssessAnalysisUnkid As Integer = -1
    Private mintYearUnkid As Integer = 0
    Private menAssess As enAssessmentMode = enAssessmentMode.APPRAISER_ASSESSMENT
    Private mdtBSC_Evaluation As DataTable
    Private mdtGE_Evaluation As DataTable
    Private mdtCustomEvaluation As DataTable
    Private dtBSC_TabularGrid As New DataTable
    Private dtGE_TabularGrid As New DataTable
    Private dtCustomTabularGrid As New DataTable
    Private iWeightTotal As Decimal = 0
    Private dsHeaders As New DataSet
    Private iHeaderId As Integer = 0
    Private iExOrdr As Integer = 0
    Private iLinkedFieldId As Integer
    Private iMappingUnkid As Integer
    Private xVal As Integer = 1
    Private xTotAssignedWeight As Decimal = 0
    Private dtCItems As New DataTable
    Private mblnItemAddEdit As Boolean = False
    Private mstriEditingGUID As String = String.Empty
    Private objCONN As SqlConnection
    Private mdecItemWeight As Decimal = 0
    Private mdecMaxScale As Decimal = 0
    Private mblnIsMatchCompetencyStructure As Boolean = False
    Dim disBSCColumn As Dictionary(Of String, Integer) = Nothing
    Dim disGEColumn As Dictionary(Of String, Integer) = Nothing
#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            disBSCColumn = dgvBSC.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvBSC.Columns.IndexOf(x))
            disGEColumn = dgvGE.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvGE.Columns.IndexOf(x))

            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                'Sohail (02 Apr 2019) -- Start
                'NMB Issue - 74.1 - Error "On Load Event !! Bad Data" on clicking any page with link after session get expired.
                If Request.QueryString.Count <= 0 Then Exit Sub
                'Sohail (02 Apr 2019) -- End
                'S.SANDEEP |17-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : PM ERROR
                KillIdleSQLSessions()
                'S.SANDEEP |17-MAR-2020| -- END
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")
                If arr.Length = 5 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try
                    Blank_ModuleName()
                    clsCommonATLog._WebFormName = "frmPerformanceEvaluation"
                    StrModuleName2 = "mnuAssessment"
                    StrModuleName3 = "mnuPerformaceEvaluation"
                    clsCommonATLog._WebClientIP = Session("IP_ADD")
                    clsCommonATLog._WebHostName = Session("HOST_NAME")
                    Me.ViewState.Add("IsDirect", True)

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    HttpContext.Current.Session("UserId") = CInt(arr(1))
                    Me.ViewState.Add("employeeunkid", CInt(arr(2)))
                    Me.ViewState.Add("assessormasterunkid", CInt(arr(3)))
                    Me.ViewState.Add("periodid", CInt(arr(4)))

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If
                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Session("IsAllowFinalSave") = clsConfig._IsAllowFinalSave
                    Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp
                    Session("EmployeeAsOnDate") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)
                    Session("IsBSC_ByEmployee") = clsConfig._IsBSC_ByEmployee
                    Session("IsCompanyNeedReviewer") = clsConfig._IsCompanyNeedReviewer
                    Session("Assessment_Instructions") = clsConfig._Assessment_Instructions
                    Session("ViewTitles_InPlanning") = clsConfig._ViewTitles_InPlanning
                    Session("FollowEmployeeHierarchy") = clsConfig._FollowEmployeeHierarchy
                    Session("CascadingTypeId") = clsConfig._CascadingTypeId
                    Session("ScoringOptionId") = clsConfig._ScoringOptionId
                    Session("Perf_EvaluationOrder") = clsConfig._Perf_EvaluationOrder
                    Session("ViewTitles_InEvaluation") = clsConfig._ViewTitles_InEvaluation
                    Session("ConsiderItemWeightAsNumber") = clsConfig._ConsiderItemWeightAsNumber
                    Session("Self_Assign_Competencies") = clsConfig._Self_Assign_Competencies
                    Session("DateFormat") = clsConfig._CompanyDateFormat
                    Session("DateSeparator") = clsConfig._CompanyDateSeparator
                    SetDateFormat()

                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname"))
                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName
                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    Dim objUserPrivilege As New clsUserPrivilege
                    objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))

                    Session("AllowtoAddAssessorEvaluation") = objUserPrivilege._AllowtoAddAssessorEvaluation

                    txtInstruction.Text = Session("Assessment_Instructions")
                    objlblCaption.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Instructions")
                    objpnlInstruction.Visible = True
                    txtInstruction.Height = Unit.Pixel(430)

                    objbtnNext.Enabled = False : objbtnBack.Enabled = False
                    objlblValue1.Visible = False : objlblValue2.Visible = False
                    objlblValue3.Visible = False : objlblValue4.Visible = False

                    Call FillCombo()

                    Call GetValue()

                    radOption.SelectedValue = 1 : radOption.Enabled = True
                    Call radOption_SelectedIndexChanged(New Object, New EventArgs)

                    cboAssessor.SelectedValue = CInt(Me.ViewState("assessormasterunkid"))
                    Call cboAssessor_SelectedIndexChanged(New Object, New EventArgs)
                    cboAssessor.Enabled = False

                    cboEmployee.SelectedValue = CInt(Me.ViewState("employeeunkid"))
                    cboEmployee.Enabled = False

                    cboPeriod.SelectedValue = CInt(Me.ViewState("periodid"))
                    Call cboPeriod_SelectedIndexChanged(New Object, New EventArgs)
                    cboPeriod.Enabled = False

                    Me.ViewState.Add("AssessAnalysisUnkid", -1)

                    'S.SANDEEP |26-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                    'If objEAnalysisMst.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Me.ViewState("assessormasterunkid"), , Me.ViewState("AssessAnalysisUnkid")) = True Then
                    If objEAnalysisMst.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Me.ViewState("assessormasterunkid"), , Me.ViewState("AssessAnalysisUnkid"), , , False) = True Then
                        'S.SANDEEP |26-AUG-2019| -- END
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry, You have already assessed some items. Please open in edit mode and add new items."), Me, "../../Index.aspx")
                    End If

                    objGoalsTran._AnalysisUnkid = mintAssessAnalysisUnkid
                    mdtBSC_Evaluation = objGoalsTran._DataTable
                    objCAssessTran._ConsiderWeightAsNumber = CBool(Session("ConsiderItemWeightAsNumber"))
                    objCAssessTran._SelfAssignCompetencies = CBool(Session("Self_Assign_Competencies"))
                    objCAssessTran._AnalysisUnkid = mintAssessAnalysisUnkid
                    mdtGE_Evaluation = objCAssessTran._DataTable
                    objCCustomTran._AnalysisUnkid = mintAssessAnalysisUnkid
                    objCCustomTran._EmployeeId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue))
                    objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
                    mdtCustomEvaluation = objCCustomTran._DataTable
                    BtnReset.Enabled = False
                    HttpContext.Current.Session("Login") = True
                    GoTo Link
                End If
            End If

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmPerformanceEvaluation"
            StrModuleName2 = "mnuAssessment"
            StrModuleName3 = "mnuPerformaceEvaluation"
            clsCommonATLog._WebClientIP = Session("IP_ADD")
            clsCommonATLog._WebHostName = Session("HOST_NAME")

            If IsPostBack = False Then
                If Session("Action") IsNot Nothing AndAlso Session("Unkid") IsNot Nothing Then
                    Call ClearForm_Values()
                    mintAssessAnalysisUnkid = Session("Unkid")
                    If CInt(Session("Action")) = 0 Then
                        menAction = enAction.ADD_ONE
                    ElseIf CInt(Session("Action")) = 1 Then
                        menAction = enAction.EDIT_ONE
                    ElseIf CInt(Session("Action")) = 2 Then
                        menAction = enAction.ADD_CONTINUE
                    End If

                    txtInstruction.Text = Session("Assessment_Instructions")
                    objlblCaption.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Instructions")
                    objpnlInstruction.Visible = True
                    txtInstruction.Height = Unit.Pixel(430)

                    objbtnNext.Enabled = False : objbtnBack.Enabled = False
                    objlblValue1.Visible = False : objlblValue2.Visible = False
                    objlblValue3.Visible = False : objlblValue4.Visible = False

                    Call FillCombo()
                    If menAction = enAction.EDIT_ONE Then
                        objEAnalysisMst._Analysisunkid = mintAssessAnalysisUnkid
                        cboEmployee.Enabled = False
                        cboAssessor.Enabled = False
                        cboPeriod.Enabled = False
                        radOption.Enabled = False
                        cboReviewer.Enabled = False
                        BtnSearch.Enabled = False
                        BtnReset.Enabled = False
                    End If
                    Call GetValue()

                    objGoalsTran._AnalysisUnkid = mintAssessAnalysisUnkid
                    mdtBSC_Evaluation = objGoalsTran._DataTable
                    objCAssessTran._SelfAssignCompetencies = CBool(Session("Self_Assign_Competencies"))
                    objCAssessTran._ConsiderWeightAsNumber = CBool(Session("ConsiderItemWeightAsNumber"))
                    objCAssessTran._AnalysisUnkid = mintAssessAnalysisUnkid
                    mdtGE_Evaluation = objCAssessTran._DataTable

                    objCCustomTran._AnalysisUnkid = mintAssessAnalysisUnkid
                    objCCustomTran._EmployeeId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue))
                    objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
                    mdtCustomEvaluation = objCCustomTran._DataTable

                    If menAction = enAction.EDIT_ONE Then
                        Call Fill_BSC_Evaluation()
                        Call Fill_GE_Evaluation()
                        Call Fill_Custom_Grid()
                        Call BtnSearch_Click(sender, e)
                    End If

                    'S.SANDEEP |09-DEC-2019| -- START
                    'ISSUE/ENHANCEMENT : NORMALIZED FORMULA
                    objlblValue1.Visible = False : objlblValue2.Visible = False
                    objlblValue3.Visible = False : objlblValue4.Visible = False
                    'S.SANDEEP |09-DEC-2019| -- END

                    If menAction <> enAction.EDIT_ONE Then
                        If Session("PaAssessPeriodUnkid") IsNot Nothing AndAlso Session("PaAssessEmpUnkid") IsNot Nothing AndAlso Session("PaAssessMstUnkid") IsNot Nothing Then
                            cboAssessor.SelectedValue = Session("PaAssessMstUnkid")
                            Call cboAssessor_SelectedIndexChanged(cboAssessor, Nothing)
                            cboEmployee.SelectedValue = Session("PaAssessEmpUnkid")
                            cboPeriod.SelectedValue = Session("PaAssessPeriodUnkid")
                            Call cboPeriod_SelectedIndexChanged(cboPeriod, Nothing)
                            dtpAssessdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                            cboAssessor.Enabled = False
                            cboEmployee.Enabled = False
                            cboPeriod.Enabled = False

                            Call BtnSearch_Click(BtnSearch, Nothing)
                        End If
                    End If
                Else
                    ''Session is nothing here code
                End If
            End If

Link:
            If Me.ViewState("Action") IsNot Nothing Then
                menAction = Me.ViewState("Action")
            End If

            If Me.ViewState("AssessAnalysisUnkid") IsNot Nothing Then
                mintAssessAnalysisUnkid = Me.ViewState("AssessAnalysisUnkid")
            End If
            If Me.ViewState("Assess") IsNot Nothing Then
                menAssess = enAssessmentMode.APPRAISER_ASSESSMENT
            End If

            If Me.Session("BSC_Evaluation") IsNot Nothing Then
                mdtBSC_Evaluation = Me.Session("BSC_Evaluation")
            End If

            If Me.Session("GE_Evaluation") IsNot Nothing Then
                mdtGE_Evaluation = Me.Session("GE_Evaluation")
            End If

            If Me.Session("CustomEvaluation") IsNot Nothing Then
                mdtCustomEvaluation = Me.Session("CustomEvaluation")
            End If

            If Me.Session("BSC_TabularGrid") IsNot Nothing Then
                dtBSC_TabularGrid = Me.Session("BSC_TabularGrid")
            End If

            If Me.Session("GE_TabularGrid") IsNot Nothing Then
                dtGE_TabularGrid = Me.Session("GE_TabularGrid")
            End If

            If Me.Session("CustomTabularGrid") IsNot Nothing Then
                dtCustomTabularGrid = Me.Session("CustomTabularGrid")
            End If

            If Me.ViewState("iWeightTotal") IsNot Nothing Then
                iWeightTotal = Me.ViewState("iWeightTotal")
            End If

            If Me.ViewState("Headers") IsNot Nothing Then
                dsHeaders = Me.ViewState("Headers")
            End If

            If Me.ViewState("iHeaderId") IsNot Nothing Then
                iHeaderId = Me.ViewState("iHeaderId")
            End If

            If Me.ViewState("iExOrdr") IsNot Nothing Then
                iExOrdr = Me.ViewState("iExOrdr")
            End If

            If Me.ViewState("iLinkedFieldId") IsNot Nothing Then
                iLinkedFieldId = Me.ViewState("iLinkedFieldId")
            End If

            If Me.ViewState("YearUnkid") IsNot Nothing Then
                mintYearUnkid = Me.ViewState("YearUnkid")
            End If

            If Me.ViewState("iMappingUnkid") IsNot Nothing Then
                iMappingUnkid = Me.ViewState("iMappingUnkid")
            End If

            If Me.ViewState("xTotAssignedWeight") IsNot Nothing Then
                xTotAssignedWeight = Me.ViewState("xTotAssignedWeight")
            End If

            If Me.ViewState("ColIndex") IsNot Nothing Then
                xVal = Me.ViewState("ColIndex")
            End If

            If Me.Session("dtCItems") IsNot Nothing Then
                dtCItems = Me.Session("dtCItems")
            End If

            If Me.ViewState("ItemAddEdit") IsNot Nothing Then
                mblnItemAddEdit = Me.ViewState("ItemAddEdit")
                If mblnItemAddEdit Then
                    popup_CItemAddEdit.Show()
                End If
            End If
            mblnIsMatchCompetencyStructure = Me.ViewState("mblnIsMatchCompetencyStructure")
            If objpnlCItems.Visible = True Then
                dgvItems.DataSource = dtCustomTabularGrid
                dgvItems.DataBind()
            End If
            dtCItems = Session("dtCItems")
            If pnl_CItemAddEdit.Visible Then
                If dtCItems IsNot Nothing Then
                    If dtCItems.Rows.Count > 0 Then
                        dgv_Citems.DataSource = dtCItems
                        dgv_Citems.DataBind()
                    End If
                End If
            End If

            If Me.ViewState("iEditingGUID") IsNot Nothing Then
                mstriEditingGUID = Me.ViewState("iEditingGUID")
            End If
            Me.ViewState("mdecItemWeight") = 0
            Me.ViewState("mdecMaxScale") = 0

            'S.SANDEEP |25-MAR-2019| -- START
            SetLanguage()
            'S.SANDEEP |25-MAR-2019| -- END


            'S.SANDEEP |20-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#0004155}
            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
                objlblValue1.Visible = False
            End If

            If CBool(Session("IsUseAgreedScore")) = False Then
                lnkCopyScore.Visible = False
            End If
            'S.SANDEEP |20-SEP-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        'S.SANDEEP |11-APR-2019| -- START
        'If Request.QueryString.Count <= 0 Then
        '    Me.IsLoginRequired = True
        'End If
        Me.IsLoginRequired = True
        'S.SANDEEP |11-APR-2019| -- END
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Me.ViewState("Action") Is Nothing Then
                Me.ViewState.Add("Action", menAction)
            Else
                Me.ViewState("Action") = menAction
            End If

            If Me.ViewState("AssessAnalysisUnkid") Is Nothing Then
                Me.ViewState.Add("AssessAnalysisUnkid", mintAssessAnalysisUnkid)
            Else
                Me.ViewState("AssessAnalysisUnkid") = mintAssessAnalysisUnkid
            End If

            If Me.ViewState("YearUnkid") Is Nothing Then
                Me.ViewState.Add("YearUnkid", mintYearUnkid)
            Else
                Me.ViewState("YearUnkid") = mintYearUnkid
            End If

            If Me.ViewState("Assess") Is Nothing Then
                Me.ViewState.Add("Assess", menAssess)
            Else
                Me.ViewState("Assess") = menAssess
            End If

            If Me.Session("BSC_Evaluation") Is Nothing Then
                Me.Session.Add("BSC_Evaluation", mdtBSC_Evaluation)
            Else
                Me.Session("BSC_Evaluation") = mdtBSC_Evaluation
            End If

            If Me.Session("GE_Evaluation") Is Nothing Then
                Me.Session.Add("GE_Evaluation", mdtGE_Evaluation)
            Else
                Me.Session("GE_Evaluation") = mdtGE_Evaluation
            End If

            If Me.Session("CustomEvaluation") Is Nothing Then
                Me.Session.Add("CustomEvaluation", mdtCustomEvaluation)
            Else
                Me.Session("CustomEvaluation") = mdtCustomEvaluation
            End If

            If Me.Session("BSC_TabularGrid") Is Nothing Then
                Me.Session.Add("BSC_TabularGrid", dtBSC_TabularGrid)
            Else
                Me.Session("BSC_TabularGrid") = dtBSC_TabularGrid
            End If

            If Me.Session("GE_TabularGrid") Is Nothing Then
                Me.Session.Add("GE_TabularGrid", dtGE_TabularGrid)
            Else
                Me.Session("GE_TabularGrid") = dtGE_TabularGrid
            End If

            If Me.Session("CustomTabularGrid") Is Nothing Then
                Me.Session.Add("CustomTabularGrid", dtCustomTabularGrid)
            Else
                Me.Session("CustomTabularGrid") = dtCustomTabularGrid
            End If

            If Me.ViewState("iWeightTotal") Is Nothing Then
                Me.ViewState.Add("iWeightTotal", iWeightTotal)
            Else
                Me.ViewState("iWeightTotal") = iWeightTotal
            End If

            If Me.ViewState("Headers") Is Nothing Then
                Me.ViewState.Add("Headers", dsHeaders)
            Else
                Me.ViewState("Headers") = dsHeaders
            End If

            If Me.ViewState("iHeaderId") Is Nothing Then
                Me.ViewState.Add("iHeaderId", iHeaderId)
            Else
                Me.ViewState("iHeaderId") = iHeaderId
            End If

            If Me.ViewState("iExOrdr") Is Nothing Then
                Me.ViewState.Add("iExOrdr", iExOrdr)
            Else
                Me.ViewState("iExOrdr") = iExOrdr
            End If

            If Me.ViewState("iLinkedFieldId") Is Nothing Then
                Me.ViewState.Add("iLinkedFieldId", iLinkedFieldId)
            Else
                Me.ViewState("iLinkedFieldId") = iLinkedFieldId
            End If

            If Me.ViewState("iMappingUnkid") Is Nothing Then
                Me.ViewState.Add("iMappingUnkid", iMappingUnkid)
            Else
                Me.ViewState("iMappingUnkid") = iMappingUnkid
            End If

            If Me.ViewState("xTotAssignedWeight") Is Nothing Then
                Me.ViewState.Add("xTotAssignedWeight", xTotAssignedWeight)
            Else
                Me.ViewState("xTotAssignedWeight") = xTotAssignedWeight
            End If

            If Me.ViewState("ColIndex") Is Nothing Then
                Me.ViewState.Add("ColIndex", xVal)
            Else
                Me.ViewState("ColIndex") = xVal
            End If

            If Me.ViewState("ItemAddEdit") Is Nothing Then
                Me.ViewState.Add("ItemAddEdit", mblnItemAddEdit)
            Else
                Me.ViewState("ItemAddEdit") = mblnItemAddEdit
            End If

            If Me.Session("dtCItems") Is Nothing Then
                Me.Session.Add("dtCItems", dtCItems)
            End If

            If Me.ViewState("iEditingGUID") Is Nothing Then
                Me.ViewState.Add("iEditingGUID", mstriEditingGUID)
            Else
                Me.ViewState("iEditingGUID") = mstriEditingGUID
            End If

            Me.ViewState("mblnIsMatchCompetencyStructure") = mblnIsMatchCompetencyStructure
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        Try
            If Session("Unkid") IsNot Nothing Then
                Session.Remove("Unkid")
            End If
            If Session("Action") IsNot Nothing Then
                Session.Remove("Action")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

#End Region

#Region "Private Methods"


    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmp As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Try
            dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "APeriod", True, 1)
            Dim intCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("APeriod")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objEAnalysisMst._Analysisunkid = mintAssessAnalysisUnkid
            objEAnalysisMst._Periodunkid = CInt(cboPeriod.SelectedValue)
            objEAnalysisMst._Selfemployeeunkid = -1
            objEAnalysisMst._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
            If radOption.SelectedValue = "1" Then
                objEAnalysisMst._Assessormasterunkid = CInt(cboAssessor.SelectedValue)
                Dim intEmployeeId As Integer = -1
                intEmployeeId = objEAnalysisMst.GetAssessorEmpId(CInt(cboAssessor.SelectedValue))
                objEAnalysisMst._Assessoremployeeunkid = intEmployeeId
            ElseIf radOption.SelectedValue = "1" Then
                objEAnalysisMst._Ext_Assessorunkid = CInt(cboAssessor.SelectedValue)
                objEAnalysisMst._Assessormasterunkid = -1
                objEAnalysisMst._Assessoremployeeunkid = -1
            End If
            objEAnalysisMst._Reviewerunkid = -1
            objEAnalysisMst._Assessmodeid = enAssessmentMode.APPRAISER_ASSESSMENT
            objEAnalysisMst._Assessmentdate = dtpAssessdate.GetDate
            objEAnalysisMst._Userunkid = Session("UserId")
            If objEAnalysisMst._Committeddatetime <> Nothing Then
                objEAnalysisMst._Committeddatetime = objEAnalysisMst._Committeddatetime
            Else
                objEAnalysisMst._Committeddatetime = Nothing
            End If
            objEAnalysisMst._Isvoid = False
            objEAnalysisMst._Voiduserunkid = -1
            objEAnalysisMst._Voiddatetime = Nothing
            objEAnalysisMst._Voidreason = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Private Sub GetValue()
        Try
            If objEAnalysisMst._Ext_Assessorunkid > 0 Then
                radOption.SelectedValue = "2"
                Call radOption_SelectedIndexChanged(radOption, Nothing)
                If objEAnalysisMst._Ext_Assessorunkid > 0 Then
                    cboAssessor.SelectedValue = objEAnalysisMst._Ext_Assessorunkid
                End If
            Else
                radOption.SelectedValue = "1"
                Call radOption_SelectedIndexChanged(radOption, Nothing)
                If objEAnalysisMst._Assessormasterunkid > 0 Then
                    cboAssessor.SelectedValue = objEAnalysisMst._Assessormasterunkid
                    If cboAssessor.SelectedValue > 0 Then
                        Call cboAssessor_SelectedIndexChanged(cboAssessor, Nothing)
                    End If
                End If
            End If
            cboEmployee.SelectedValue = objEAnalysisMst._Assessedemployeeunkid
            cboPeriod.SelectedValue = objEAnalysisMst._Periodunkid
            If menAction = enAction.EDIT_ONE Then
                Call cboPeriod_SelectedIndexChanged(cboPeriod, Nothing)
            End If
            If objEAnalysisMst._Assessmentdate <> Nothing Then
                dtpAssessdate.SetDate = objEAnalysisMst._Assessmentdate
            Else
                dtpAssessdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Private Sub SetTotals(ByVal iDG As DataGrid)
        Try
            Select Case iDG.ID.ToUpper
                Case dgvBSC.ID.ToUpper
                    objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
                    objlblValue1.Visible = True : objlblValue2.Visible = True
                    If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
                        objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                                                      IsBalanceScoreCard:=True, _
                                                      xScoreOptId:=Session("ScoringOptionId"), _
                                                      xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                      xEmployeeId:=cboEmployee.SelectedValue, _
                                                      xPeriodId:=cboPeriod.SelectedValue, _
                                                      xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                                                      xDataTable:=dtBSC_TabularGrid, _
                                                      xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

                        If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                            objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
                            objEAnalysisMst.Compute_Score(xAssessMode:=menAssess, _
                                                          IsBalanceScoreCard:=True, _
                                                          xScoreOptId:=Session("ScoringOptionId"), _
                                                          xCompute_Formula:=enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
                                                          xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                          xEmployeeId:=cboEmployee.SelectedValue, _
                                                          xPeriodId:=cboPeriod.SelectedValue, _
                                                          xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                                                          xDataTable:=mdtBSC_Evaluation, _
                                                          xAssessorReviewerId:=cboAssessor.SelectedValue, _
                                                          xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
                        End If
                        objlblValue3.Visible = True : objlblValue4.Visible = False
                    End If

                Case dgvGE.ID.ToUpper
                    objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
                    objlblValue1.Visible = True : objlblValue2.Visible = True

                    If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
                        objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
                                                      IsBalanceScoreCard:=False, _
                                                      xScoreOptId:=Session("ScoringOptionId"), _
                                                      xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                      xEmployeeId:=cboEmployee.SelectedValue, _
                                                      xPeriodId:=cboPeriod.SelectedValue, _
                                                      xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                                                      xDataTable:=dtGE_TabularGrid, _
                                                      xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
                        objlblValue3.Text = ""
                        If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then

                            objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
                            objEAnalysisMst.Compute_Score(xAssessMode:=menAssess, _
                                                          IsBalanceScoreCard:=False, _
                                                          xScoreOptId:=Session("ScoringOptionId"), _
                                                          xCompute_Formula:=enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
                                                          xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                          xEmployeeId:=cboEmployee.SelectedValue, _
                                                          xPeriodId:=cboPeriod.SelectedValue, _
                                                          xUsedAgreedScore:=Session("IsUseAgreedScore"), _
                                                          xDataTable:=mdtGE_Evaluation, _
                                                          xAssessGrpId:=cboAssessor.SelectedValue, _
                                                          xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

                        End If
                        objlblValue3.Visible = True : objlblValue4.Visible = False
                    End If
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Private Sub PanelVisibility()
        Try
            Dim xVal As Integer = -1
            If iHeaderId >= 0 Then
                objlblCaption.Text = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString
                xVal = dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")
            Else
                objlblCaption.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Instructions")
                xVal = -1
            End If
            Select Case xVal
                Case -1 'Instruction
                    objpnlBSC.Visible = False
                    objpnlGE.Visible = False
                    objpnlCItems.Visible = False
                    objpnlInstruction.Visible = True
                    objpnlInstruction.Height = Unit.Pixel(500)
                    objpnlInstruction.Width = Unit.Percentage(100)
                Case -3  'Balance Score Card
                    objpnlInstruction.Visible = False
                    objpnlGE.Visible = False
                    objpnlCItems.Visible = False
                    Call Fill_BSC_Evaluation()
                    objpnlBSC.Visible = True
                Case -2  'Competencies
                    objpnlInstruction.Visible = False
                    objpnlBSC.Visible = False
                    objpnlCItems.Visible = False
                    Call Fill_GE_Evaluation()
                    objpnlGE.Visible = True
                    objpnlGE.Height = Unit.Percentage(100)
                    objpnlGE.Width = Unit.Percentage(100)
                Case Else  'Dynamic Custom Headers
                    objpnlInstruction.Visible = False
                    objpnlBSC.Visible = False
                    objpnlGE.Visible = False
                    Call Fill_Custom_Grid()
                    Call Fill_Custom_Evaluation_Data()
                    objpnlCItems.Visible = True
                    objpnlCItems.Height = Unit.Percentage(100)
                    objpnlCItems.Width = Unit.Percentage(100)
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 16, "Emplolyee is compulsory information.Please Select Emplolyee."), Me)
                cboEmployee.Focus()
                Return False
            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 17, "Period is compulsory information.Please Select Period."), Me)
                cboPeriod.Focus()
                Return False
            End If
            Dim objPrd As New clscommom_period_Tran
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            If dtpAssessdate.GetDate <= objPrd._Start_Date.Date Then
                Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 36, "Sorry, assessment date should be greater than") & " ( " & objPrd._Start_Date.Date.ToShortDateString & " )."
                DisplayMessage.DisplayMessage(strMsg, Me)
                dtpAssessdate.Focus()
                objPrd = Nothing
                Return False
            End If
            objPrd = Nothing
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Function

    Private Function Is_Already_Assessed() As Boolean
        Try
            'S.SANDEEP |30-JAN-2019| -- START
            'ISSUE/ENHANCEMENT : {#0003446|ARUTI-545}
            'If objEAnalysisMst.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAssessor.SelectedValue), , Me.ViewState("AssessAnalysisUnkid")) = True Then

            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            'If objEAnalysisMst.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAssessor.SelectedValue), , mintAssessAnalysisUnkid) = True Then
            If objEAnalysisMst.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAssessor.SelectedValue), , mintAssessAnalysisUnkid, , , False) = True Then
                'S.SANDEEP |18-JAN-2020| -- END
                'S.SANDEEP |30-JAN-2019| -- END
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), Me, "wPg_AssessorEvaluationList.aspx") 'Shani [23 MAR 2015]-wPg_AssessorEvaluationList.aspx
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Function

    Private Sub ClearForm_Values()
        Try
            Me.ViewState("Action") = Nothing
            Me.ViewState("AssessAnalysisUnkid") = Nothing
            Me.ViewState("YearUnkid") = Nothing
            Me.ViewState("Assess") = Nothing
            Me.Session.Remove("BSC_Evaluation")
            Me.Session.Remove("GE_Evaluation")
            Me.Session.Remove("CustomEvaluation")
            Me.Session.Remove("BSC_TabularGrid")
            Me.Session.Remove("GE_TabularGrid")
            Me.Session.Remove("CustomTabularGrid")
            Me.ViewState("iWeightTotal") = Nothing
            Me.ViewState("Headers") = Nothing
            Me.ViewState("iHeaderId") = Nothing
            Me.ViewState("iExOrdr") = Nothing
            Me.ViewState("iLinkedFieldId") = Nothing
            Me.ViewState("iMappingUnkid") = Nothing
            Me.ViewState("xTotAssignedWeight") = Nothing
            Me.ViewState("ColIndex") = Nothing
            Me.ViewState("ItemAddEdit") = Nothing
            Me.Session.Remove("dtCItems")
            Me.ViewState("iEditingGUID") = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    ''''''''''''''''''''''''''BSC EVOLUTION

    Private Sub SetBSC_GridCols_Tags()
        Try
            'S.SANDEEP |12-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}
            Dim iEval() As String = Nothing
            If CStr(Session("ViewTitles_InEvaluation")).Trim.Length > 0 Then
                iEval = CStr(Session("ViewTitles_InEvaluation")).Split("|")
            End If
            'S.SANDEEP |12-MAR-2019| -- END

            Dim objFMst As New clsAssess_Field_Master
            Dim dFld As New DataSet : dFld.Tables.Add(objFMst.GetFieldsForViewSetting())
            Dim xcolWidth As Integer = 0
            If dFld.Tables(0).Rows.Count > 0 Then
                Dim xCol As DataGridColumn = Nothing
                Dim iExOrder As Integer = -1
                For Each xRow As DataRow In dFld.Tables(0).Rows
                    iExOrder = -1
                    iExOrder = objFMst.Get_Field_ExOrder(xRow.Item("Id"))
                    xCol = Nothing
                    Select Case iExOrder
                        Case 1
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField1"))
                        Case 2
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField2"))
                        Case 3
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField3"))
                        Case 4
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField4"))
                        Case 5
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField5"))
                        Case 6
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField6"))
                        Case 7
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField7"))
                        Case 8
                            xCol = dgvBSC.Columns(disBSCColumn("objdgcolhBSCField8"))
                    End Select
                    Select Case xRow.Item("Id")
                        Case clsAssess_Field_Master.enOtherInfoField.ST_DATE
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolhSDate"))
                        Case clsAssess_Field_Master.enOtherInfoField.ED_DATE
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolhEDate"))
                        Case clsAssess_Field_Master.enOtherInfoField.PCT_COMPLETE
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolhCompleted"))
                        Case clsAssess_Field_Master.enOtherInfoField.STATUS
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolhStatus"))
                        Case clsAssess_Field_Master.enOtherInfoField.WEIGHT
                            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
                                xCol = dgvBSC.Columns(disBSCColumn("dgcolhBSCScore"))
                            Else
                                xCol = dgvBSC.Columns(disBSCColumn("dgcolhBSCWeight"))
                            End If
                        Case clsAssess_Field_Master.enOtherInfoField.SCORE
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolheselfBSC"))
                            'S.SANDEEP [11-OCT-2018] -- START
                        Case clsAssess_Field_Master.enOtherInfoField.GOAL_TYPE
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolhGoalType"))
                        Case clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE
                            xCol = dgvBSC.Columns(disBSCColumn("dgoalvalue"))
                            'S.SANDEEP [11-OCT-2018] -- END
                    End Select
                    If xCol IsNot Nothing Then
                        'S.SANDEEP |14-MAR-2019| -- START
                        'xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
                        'xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
                        'xcolWidth = xcolWidth + objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"))
                        'If xRow.Item("Id") = clsAssess_Field_Master.enOtherInfoField.SCORE Then
                        '    xCol = dgvBSC.Columns(disBSCColumn("dgcolhaselfBSC"))
                        '    xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
                        '    xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
                        '    xcolWidth = xcolWidth + objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"))
                        'End If
                        xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                        xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                        xcolWidth = xcolWidth + objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"))
                        If xRow.Item("Id") = clsAssess_Field_Master.enOtherInfoField.SCORE Then
                            xCol = dgvBSC.Columns(disBSCColumn("dgcolhaselfBSC"))
                            xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                            xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid")))
                            xcolWidth = xcolWidth + objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"), Session("Companyunkid"))
                        End If
                        'S.SANDEEP |14-MAR-2019| -- END
                    End If

                    'S.SANDEEP |12-MAR-2019| -- START
                    'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}
                    If iEval IsNot Nothing Then
                        If Array.IndexOf(iEval, xRow.Item("Id").ToString()) < 0 Then
                            xCol.Visible = False
                        End If
                    Else
                        If xCol IsNot Nothing Then xCol.Visible = False
                    End If
                    'S.SANDEEP |12-MAR-2019| -- END

                Next
                'dgvBSC.Width = xcolWidth
            End If
            objFMst = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_BSC_Evaluation()
        Try

            'S.SANDEEP [04-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : MAKE COMMON METHOD TO GET EMPLOYEE ASSESSOR/REVIEWER
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)), CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")))

            'S.SANDEEP |18-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)), CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")), Session("Database_Name").ToString())
            'S.SANDEEP |17-MAY-2021| -- START
            'ISSUE/ENHANCEMENT : SUNBIRD CHANGES FOR SCORES            
            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)), CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")), Session("Database_Name").ToString(), , Session("fmtCurrency"))
            dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)), CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")), Session("Database_Name").ToString(), Session("DontAllowRatingBeyond100"), , Session("fmtCurrency"))
            'S.SANDEEP |17-MAY-2021| -- END

            'S.SANDEEP |18-FEB-2019| -- END

            'S.SANDEEP [04-AUG-2017] -- END
            Call SetBSC_GridCols_Tags()

            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
                dgvBSC.Columns(disBSCColumn("dgcolhBSCWeight")).Visible = False  'Weight
                dgvBSC.Columns(disBSCColumn("dgcolhBSCScore")).Visible = True   'Score
            Else
                dgvBSC.Columns(disBSCColumn("dgcolhBSCScore")).Visible = False  'Score
                dgvBSC.Columns(disBSCColumn("dgcolhBSCWeight")).Visible = True   'Weight
            End If


            'S.SANDEEP |05-APR-2019| -- START
            'If dtBSC_TabularGrid.Columns.Contains("Field1") Then
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField1")).HeaderText = dtBSC_TabularGrid.Columns("Field1").Caption
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField1")).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field1", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField1")).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field2") Then
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField2")).HeaderText = dtBSC_TabularGrid.Columns("Field2").Caption
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField2")).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field2", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField2")).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field3") Then
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField3")).HeaderText = dtBSC_TabularGrid.Columns("Field3").Caption
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField3")).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field3", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField3")).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field4") Then
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField4")).HeaderText = dtBSC_TabularGrid.Columns("Field4").Caption
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField4")).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field4", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField4")).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field5") Then
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField5")).HeaderText = dtBSC_TabularGrid.Columns("Field5").Caption
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField5")).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field5", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField5")).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field6") Then
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField6")).HeaderText = dtBSC_TabularGrid.Columns("Field6").Caption
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField6")).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field6", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField6")).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field7") Then
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField7")).HeaderText = dtBSC_TabularGrid.Columns("Field7").Caption
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField7")).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field7", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField7")).Visible = False
            'End If

            'If dtBSC_TabularGrid.Columns.Contains("Field8") Then
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField8")).HeaderText = dtBSC_TabularGrid.Columns("Field8").Caption
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField8")).Visible = True
            'Else
            '    dtBSC_TabularGrid.Columns.Add("Field8", System.Type.GetType("System.String"))
            '    dgvBSC.Columns(disBSCColumn("objdgcolhBSCField8")).Visible = False
            'End If

            If dtBSC_TabularGrid.Columns.Contains("Field1") Then
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField1")).HeaderText = dtBSC_TabularGrid.Columns("Field1").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField1")).Visible Then dgvBSC.Columns(disBSCColumn("objdgcolhBSCField1")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field1", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField1")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field2") Then
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField2")).HeaderText = dtBSC_TabularGrid.Columns("Field2").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField2")).Visible Then dgvBSC.Columns(disBSCColumn("objdgcolhBSCField2")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field2", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField2")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field3") Then
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField3")).HeaderText = dtBSC_TabularGrid.Columns("Field3").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField3")).Visible Then dgvBSC.Columns(disBSCColumn("objdgcolhBSCField3")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field3", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField3")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field4") Then
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField4")).HeaderText = dtBSC_TabularGrid.Columns("Field4").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField4")).Visible Then dgvBSC.Columns(disBSCColumn("objdgcolhBSCField4")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field4", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField4")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field5") Then
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField5")).HeaderText = dtBSC_TabularGrid.Columns("Field5").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField5")).Visible Then dgvBSC.Columns(disBSCColumn("objdgcolhBSCField5")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field5", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField5")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field6") Then
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField6")).HeaderText = dtBSC_TabularGrid.Columns("Field6").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField6")).Visible Then dgvBSC.Columns(disBSCColumn("objdgcolhBSCField6")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field6", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField6")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field7") Then
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField7")).HeaderText = dtBSC_TabularGrid.Columns("Field7").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField7")).Visible Then dgvBSC.Columns(disBSCColumn("objdgcolhBSCField7")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field7", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField7")).Visible = False
            End If

            If dtBSC_TabularGrid.Columns.Contains("Field8") Then
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField8")).HeaderText = dtBSC_TabularGrid.Columns("Field8").Caption
                If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField8")).Visible Then dgvBSC.Columns(disBSCColumn("objdgcolhBSCField8")).Visible = True
            Else
                dtBSC_TabularGrid.Columns.Add("Field8", System.Type.GetType("System.String"))
                dgvBSC.Columns(disBSCColumn("objdgcolhBSCField8")).Visible = False
            End If
            'S.SANDEEP |05-APR-2019| -- END


            dgvBSC.Columns(disBSCColumn("dgcolheselfBSC")).HeaderText = dtBSC_TabularGrid.Columns("eself").Caption
            dgvBSC.Columns(disBSCColumn("dgcolheremarkBSC")).HeaderText = dtBSC_TabularGrid.Columns("eremark").Caption

            If dtBSC_TabularGrid.Columns.Contains("aself") Then
                dgvBSC.Columns(disBSCColumn("dgcolhaselfBSC")).HeaderText = dtBSC_TabularGrid.Columns("aself").Caption
            End If
            If dtBSC_TabularGrid.Columns.Contains("aremark") Then
                dgvBSC.Columns(disBSCColumn("dgcolharemarkBSC")).HeaderText = dtBSC_TabularGrid.Columns("aremark").Caption
            End If

            If dtBSC_TabularGrid.Columns.Contains("Score") Then
                If dgvBSC.Columns(disBSCColumn("dgcolhBSCScore")).Visible Then
                    dgvBSC.Columns(disBSCColumn("dgcolhBSCScore")).HeaderText = dtBSC_TabularGrid.Columns("Score").Caption
                End If
            End If

            'S.SANDEEP |05-APR-2019| -- START
            'If CStr(Session("ViewTitles_InEvaluation")).Trim.Length > 0 Then
            '    Dim iEval() As String = CStr(Session("ViewTitles_InEvaluation")).Split("|")
            '    If iEval IsNot Nothing Then
            '        For Each xCol As DataGridColumn In dgvBSC.Columns
            '            If xCol.FooterText IsNot Nothing Then
            '                If IsNumeric(xCol.FooterText) Then
            '                    If Array.IndexOf(iEval, xCol.FooterText.ToString) < 0 Then
            '                        xCol.Visible = False
            '                    End If
            '                End If
            '            End If
            '        Next
            '    End If
            'End If
            'S.SANDEEP |05-APR-2019| -- END


            If dgvBSC.Columns(disBSCColumn("objdgcolhBSCField1")).Visible = True Then
                xVal = 1
            ElseIf dgvBSC.Columns(disBSCColumn("objdgcolhBSCField2")).Visible = True Then
                xVal = 2
            ElseIf dgvBSC.Columns(disBSCColumn("objdgcolhBSCField3")).Visible = True Then
                xVal = 3
            ElseIf dgvBSC.Columns(disBSCColumn("objdgcolhBSCField4")).Visible = True Then
                xVal = 4
            ElseIf dgvBSC.Columns(disBSCColumn("objdgcolhBSCField5")).Visible = True Then
                xVal = 5
            ElseIf dgvBSC.Columns(disBSCColumn("objdgcolhBSCField6")).Visible = True Then
                xVal = 6
            ElseIf dgvBSC.Columns(disBSCColumn("objdgcolhBSCField7")).Visible = True Then
                xVal = 7
            ElseIf dgvBSC.Columns(disBSCColumn("objdgcolhBSCField8")).Visible = True Then
                xVal = 8
            End If

            Dim xWidth As Integer = 0
            Dim objFMst As New clsAssess_Field_Master
            'S.SANDEEP |15-MAR-2019| -- START
            'xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK)
            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK, Session("CompanyUnkid"))
            'S.SANDEEP |15-MAR-2019| -- END
            If xWidth > 0 Then
                dgvBSC.Columns(disBSCColumn("dgcolheremarkBSC")).HeaderStyle.Width = Unit.Pixel(xWidth)
                dgvBSC.Columns(disBSCColumn("dgcolheremarkBSC")).ItemStyle.Width = Unit.Pixel(xWidth)
            End If
            'S.SANDEEP |15-MAR-2019| -- START
            'xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK)
            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK, Session("CompanyUnkid"))
            'S.SANDEEP |15-MAR-2019| -- END

            If xWidth > 0 Then
                dgvBSC.Columns(disBSCColumn("dgcolharemarkBSC")).HeaderStyle.Width = Unit.Pixel(xWidth)
                dgvBSC.Columns(disBSCColumn("dgcolharemarkBSC")).ItemStyle.Width = Unit.Pixel(xWidth)
            End If
            objFMst = Nothing

            Dim intTotalWidth As Integer = 0
            For Each xCol As DataGridColumn In dgvBSC.Columns
                If xCol.Visible Then
                    intTotalWidth += xCol.HeaderStyle.Width.Value
                End If
            Next
            'S.SANDEEP |12-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}
            'For Each xCol As DataGridColumn In dgvBSC.Columns
            '    If xCol.Visible AndAlso xCol.HeaderStyle.Width.Value > 0 Then
            '        Dim decColumnWidth As Decimal = xCol.HeaderStyle.Width.Value * 100 / intTotalWidth
            '        xCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
            '        xCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
            '    End If
            'Next
            'S.SANDEEP |12-MAR-2019| -- END

            dgvBSC.Columns(disBSCColumn("dgcolhAgreedScoreBSC")).Visible = CBool(Session("IsUseAgreedScore"))

            If menAction <> enAction.EDIT_ONE AndAlso CBool(Session("EnableBSCAutomaticRating")) Then
                If mdtBSC_Evaluation.Select("AUD = 'A' AND iEmployeeId = '" & cboEmployee.SelectedValue & "'").Count <= 0 Then
                    Dim dRow As DataRow
                    For Each dtRow As DataRow In dtBSC_TabularGrid.Select("IsGrp = 'False'")
                        dRow = mdtBSC_Evaluation.NewRow
                        dRow.Item("analysistranunkid") = -1
                        dRow.Item("analysisunkid") = -1
                        dRow.Item("empfield1unkid") = CInt(dtRow.Item("empfield1unkid"))
                        dRow.Item("result") = dtRow.Item("aself")
                        dRow.Item("remark") = ""
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        dRow.Item("isvoid") = False
                        dRow.Item("voiduserunkid") = -1
                        dRow.Item("voiddatetime") = DBNull.Value
                        dRow.Item("voidreason") = ""
                        dRow.Item("empfield2unkid") = CInt(dtRow.Item("empfield2unkid"))
                        dRow.Item("empfield3unkid") = CInt(dtRow.Item("empfield3unkid"))
                        dRow.Item("empfield4unkid") = CInt(dtRow.Item("empfield4unkid"))
                        dRow.Item("empfield5unkid") = CInt(dtRow.Item("empfield5unkid"))
                        dRow.Item("perspectiveunkid") = CInt(dtRow.Item("perspectiveunkid"))
                        dRow.Item("item_weight") = dtRow.Item("aitem_weight")
                        dRow.Item("max_scale") = dtRow.Item("amax_scale")
                        dRow.Item("iPeriodId") = cboPeriod.SelectedValue
                        dRow.Item("agreed_score") = dtRow.Item("aself")

                        Select Case iExOrdr
                            Case enWeight_Types.WEIGHT_FIELD1
                                dRow.Item("iItemUnkid") = dRow.Item("empfield1unkid")
                            Case enWeight_Types.WEIGHT_FIELD2
                                dRow.Item("iItemUnkid") = dRow.Item("empfield2unkid")
                            Case enWeight_Types.WEIGHT_FIELD3
                                dRow.Item("iItemUnkid") = dRow.Item("empfield3unkid")
                            Case enWeight_Types.WEIGHT_FIELD4
                                dRow.Item("iItemUnkid") = dRow.Item("empfield4unkid")
                            Case enWeight_Types.WEIGHT_FIELD5
                                dRow.Item("iItemUnkid") = dRow.Item("empfield5unkid")
                        End Select
                        dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
                        dRow.Item("iScore") = dtRow.Item("aself")
                        'Shani (23-Nov-2016) -- Start
                        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
                        If CBool(Session("IsUseAgreedScore")) Then
                            dRow.Item("agreed_score") = dtRow.Item("aself")
                            dtRow.Item("agreed_score") = dtRow.Item("aself")
                            dtRow.Item("agreedscore") = dtRow.Item("aself")
                        End If
                        'Shani (23-Nov-2016) -- End

                        mdtBSC_Evaluation.Rows.Add(dRow)
                        mdecMaxScale = 0 : mdecMaxScale = 0
                    Next
                End If
            End If
            'S.SANDEEP |12-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Action List Phase 2 - 55}
            'dgvBSC.Width = Unit.Percentage(99.5)
            dgvBSC.Width = Unit.Pixel(intTotalWidth)
            'S.SANDEEP |12-MAR-2019| -- END


            dgvBSC.DataSource = dtBSC_TabularGrid
            iWeightTotal = 0
            dgvBSC.DataBind()
            Call SetTotals(dgvBSC)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    '''''''''''''''''''''''''' COMPETENCIES/GENERAL EVALUATION METHODS
    Private Sub Fill_GE_Evaluation()
        Try
            'S.SANDEEP [04-AUG-2017] -- START
            'ISSUE/ENHANCEMENT : MAKE COMMON METHOD TO GET EMPLOYEE ASSESSOR/REVIEWER
            'dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), _
            '                                                          CInt(cboPeriod.SelectedValue), _
            '                                                          CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)), _
            '                                                          CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), _
            '                                                          Session("ConsiderItemWeightAsNumber"), -1, _
            '                                                          Session("Self_Assign_Competencies"), _
            '                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")))
            dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), _
                                                                      CInt(cboPeriod.SelectedValue), _
                                                                      CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)), _
                                                                      CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), _
                                                                      Session("ConsiderItemWeightAsNumber"), -1, _
                                                                      Session("Self_Assign_Competencies"), _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                      Session("Database_Name").ToString())
            'S.SANDEEP [04-AUG-2017] -- END
            dgvGE.AutoGenerateColumns = False
            If Session("ScoringOptionId") = enScoringOption.SC_SCALE_BASED Then
                dgvGE.Columns(disGEColumn("dgcolhGEWeight")).Visible = False    'WEIGHT
                dgvGE.Columns(disGEColumn("dgcolhGEScore")).Visible = True     'SCORE GUIDE
            Else
                dgvGE.Columns(disGEColumn("dgcolhGEWeight")).Visible = True     'WEIGHT
                dgvGE.Columns(disGEColumn("dgcolhGEScore")).Visible = False    'SCORE GUIDE
            End If

            dgvGE.Columns(disGEColumn("dgcolheselfGE")).HeaderText = dtGE_TabularGrid.Columns("eself").Caption
            dgvGE.Columns(disGEColumn("dgcolheremarkGE")).HeaderText = dtGE_TabularGrid.Columns("eremark").Caption

            If dtGE_TabularGrid.Columns.Contains("aself") Then
                dgvGE.Columns(disGEColumn("dgcolhaselfGE")).HeaderText = dtGE_TabularGrid.Columns("aself").Caption
            End If
            If dtGE_TabularGrid.Columns.Contains("aremark") Then
                dgvGE.Columns(disGEColumn("dgcolharemarkGE")).HeaderText = dtGE_TabularGrid.Columns("aremark").Caption
            End If

            If dtGE_TabularGrid.Columns.Contains("Score") Then
                If dgvGE.Columns(disGEColumn("dgcolhGEScore")).Visible Then
                    dgvGE.Columns(disGEColumn("dgcolhGEScore")).HeaderText = dtGE_TabularGrid.Columns("Score").Caption
                End If
            End If
            Dim xWidth As Integer = 0
            Dim objFMst As New clsAssess_Field_Master
            'S.SANDEEP |15-MAR-2019| -- START
            'xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK)
            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK, Session("CompanyUnkid"))
            'S.SANDEEP |15-MAR-2019| -- END

            If xWidth > 0 Then dgvGE.Columns(disGEColumn("dgcolheremarkGE")).ItemStyle.Width = xWidth
            'S.SANDEEP |15-MAR-2019| -- START
            'xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK)
            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK, Session("CompanyUnkid"))
            'S.SANDEEP |15-MAR-2019| -- END

            If xWidth > 0 Then dgvGE.Columns(disGEColumn("dgcolharemarkGE")).ItemStyle.Width = xWidth

            'Shani (23-Nov-2016) -- Start
            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
            dgvGE.Columns(disGEColumn("dgcolhaAgreedScoreGE")).Visible = CBool(Session("IsUseAgreedScore"))
            'Shani (23-Nov123-2016-2016) -- End


            dgvGE.DataSource = dtGE_TabularGrid
            iWeightTotal = 0
            dgvGE.DataBind()
            Call SetTotals(dgvGE)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub


    '''''''''''''''''''''''''' CUSTOM ITEMS EVALUATION METHODS
    Private Sub Fill_Custom_Grid(Optional ByVal iFromAddEdit As Boolean = False)
        Try
            If iHeaderId >= 0 Then

                Dim objCHdr As New clsassess_custom_header
                objCHdr._Customheaderunkid = dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")
                mblnIsMatchCompetencyStructure = objCHdr._IsMatch_With_Competency
                objCHdr = Nothing

                If iFromAddEdit = False Then
                    'S.SANDEEP |14-MAR-2019| -- START
                    'dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), CInt(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")), mintAssessAnalysisUnkid, menAssess, CInt(cboEmployee.SelectedValue), menAction)
                    dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), CInt(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")), mintAssessAnalysisUnkid, menAssess, CInt(cboEmployee.SelectedValue), menAction)
                    'S.SANDEEP |14-MAR-2019| -- END
                Else
                    Dim iRow() As DataRow = dtCustomTabularGrid.Select("periodunkid <= 0")
                    For i As Integer = 0 To iRow.Length - 1
                        dtCustomTabularGrid.Rows.Remove(iRow(i))
                    Next
                End If
                If dtCustomTabularGrid.Rows.Count > 0 Then
                    dgvItems.Columns.Clear()
                End If
                Call Add_GridColumns()
                dgvItems.DataSource = Nothing
                dgvItems.AutoGenerateColumns = False
                Dim iColName As String = String.Empty
                For Each dCol As DataColumn In dtCustomTabularGrid.Columns
                    iColName = "" : iColName = "obj" & dCol.ColumnName
                    Dim dgvCol As New BoundField()
                    dgvCol.FooterText = iColName
                    dgvCol.ReadOnly = True
                    dgvCol.DataField = dCol.ColumnName
                    dgvCol.HeaderText = dCol.Caption
                    If dgvItems.Columns.Contains(dgvCol) = True Then Continue For
                    If dCol.Caption.Length <= 0 Then
                        dgvCol.Visible = False
                    End If
                    dgvItems.Columns.Add(dgvCol)
                    If dgvCol.FooterText = "objHeader_Name" Then dgvCol.Visible = False
                Next

                Call SetDateFormat()
                dgvItems.DataSource = dtCustomTabularGrid
                dgvItems.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Custom_Evaluation_Data()
        Try
            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
                dtCustomTabularGrid.Rows.Clear()
                Dim strGUIDArray() As String = Nothing

                Dim dR = mdtCustomEvaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("custom_header") = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString And x.Field(Of String)("AUD") <> "D")
                If dR.Count > 0 Then
                    strGUIDArray = dR.Cast(Of DataRow).Select(Function(x) x.Field(Of String)("customanalysistranguid")).Distinct().ToArray()
                End If
                If strGUIDArray IsNot Nothing AndAlso strGUIDArray.Length > 0 Then
                    Dim dFRow As DataRow = Nothing
                    For Each Str As String In strGUIDArray
                        dFRow = dtCustomTabularGrid.NewRow
                        dtCustomTabularGrid.Rows.Add(dFRow)
                        For Each dRow As DataRow In mdtCustomEvaluation.Select("customanalysistranguid = '" & Str & "' AND AUD <> 'D' ")
                            Select Case CInt(dRow.Item("itemtypeid"))
                                Case clsassess_custom_items.enCustomType.FREE_TEXT
                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
                                Case clsassess_custom_items.enCustomType.SELECTION
                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                        Select Case CInt(dRow.Item("selectionmodeid"))
                                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
                                                Dim objCMaster As New clsCommon_Master
                                                objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                                objCMaster = Nothing
                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                                Dim objCMaster As New clsassess_competencies_master
                                                objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                                objCMaster = Nothing
                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                                Dim objEmpField1 As New clsassess_empfield1_master
                                                objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
                                                objEmpField1 = Nothing

                                                'S.SANDEEP |16-AUG-2019| -- START
                                                'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                            Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                                Dim objCMaster As New clsCommon_Master
                                                objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                                objCMaster = Nothing
                                                'S.SANDEEP |16-AUG-2019| -- END

                                        End Select
                                    End If
                                Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                                    End If
                                Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                        If IsNumeric(dRow.Item("custom_value")) Then
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
                                        End If
                                    End If
                            End Select
                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
                            dFRow.Item("periodunkid") = dRow.Item("periodunkid")
                            dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
                            dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
                            dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
                            dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
                            dFRow.Item("ismanual") = dRow.Item("ismanual")
                        Next
                    Next
                End If
                mdtCustomEvaluation.AcceptChanges()
                If dtCustomTabularGrid.Rows.Count <= 0 Then
                    dtCustomTabularGrid.Rows.Add(dtCustomTabularGrid.NewRow)
                    dtCustomTabularGrid.Rows(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                End If
                Call Fill_Custom_Grid(True)
                objlblValue1.Text = "&nbsp"
                objlblValue2.Text = "&nbsp"
                objlblValue3.Text = "&nbsp"
                objlblValue4.Text = "&nbsp"
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Private Sub Add_GridColumns()
        Try
            '************* ADD
            Dim iTempField As New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "ObjAdd"
            iTempField.HeaderText = "Add"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            dgvItems.Columns.Add(iTempField)

            '************* EDIT
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "objEdit"
            iTempField.HeaderText = "Edit"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            dgvItems.Columns.Add(iTempField)

            '************* DELETE
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.FooterText = "objDelete"
            iTempField.HeaderText = "Delete"
            iTempField.ItemStyle.Width = Unit.Pixel(40)
            dgvItems.Columns.Add(iTempField)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Private Sub Generate_Popup_Data(ByVal xHeaderUnkid As Integer)
        Try
            Dim dtCItems As DataTable
            'S.SANDEEP |14-MAR-2019| -- START
            'dtCItems = objEAnalysisMst.Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid, menAssess)
            'S.SANDEEP |25-MAR-2019| -- START

            'S.SANDEEP |05-APR-2019| -- START
            'If Session("CompanyGroupName") = "NMB PLC" Then
            '    'If Session("CompCode").ToString.ToUpper = "NMB" Then
            '    'S.SANDEEP |25-MAR-2019| -- END
            '    dtCItems = objEAnalysisMst.Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid, menAssess)
            'Else
            '    dtCItems = objEAnalysisMst.Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid, 0)
            'End If
            dtCItems = objEAnalysisMst.Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid, menAssess)
            'S.SANDEEP |05-APR-2019| -- END


            'S.SANDEEP |14-MAR-2019| -- END

            'S.SANDEEP |09-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : PA CHANGES
            If dtCItems IsNot Nothing AndAlso dtCItems.Rows.Count <= 0 Then Exit Sub
            'S.SANDEEP |09-JUL-2019| -- END

            dtCItems = dtCItems.Select("", "isdefaultentry DESC").CopyToDataTable
            If mstriEditingGUID <> "" Then
                Dim strFilter As String = ""
                If CInt(Me.ViewState("RowIndex")) > -1 Then
                    strFilter = "customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D'"
                Else
                    strFilter = "customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D' AND isdefaultentry = True"
                End If
                Dim dtmp() As DataRow = mdtCustomEvaluation.Select(strFilter)
                If dtmp.Length > 0 Then
                    For iEdit As Integer = 0 To dtmp.Length - 1
                        Dim xRow() As DataRow = dtCItems.Select("customitemunkid = '" & dtmp(iEdit).Item("customitemunkid") & "'")
                        If xRow.Length > 0 Then
                            Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
                                Case clsassess_custom_items.enCustomType.SELECTION
                                    Select Case CInt(dtmp(iEdit).Item("selectionmodeid"))
                                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")

                                            'S.SANDEEP |16-AUG-2019| -- START
                                            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                                        Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
                                            'S.SANDEEP |16-AUG-2019| -- END
                                    End Select
                                Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                    If dtmp(iEdit).Item("custom_value").ToString.Trim.Length > 0 Then
                                        xRow(0).Item("ddate") = eZeeDate.convertDate(dtmp(iEdit).Item("custom_value"))
                                    End If
                            End Select
                            xRow(0).Item("custom_value") = dtmp(iEdit).Item("dispaly_value")
                        End If
                    Next
                End If
            End If

            Me.Session("dtCItems") = dtCItems
            dgv_Citems.AutoGenerateColumns = False
            Dim objCHeader As New clsassess_custom_header
            objCHeader._Customheaderunkid = xHeaderUnkid
            If Me.ViewState("Header") IsNot Nothing Then
                Me.ViewState("Header") = objCHeader._Name
            Else
                Me.ViewState.Add("Header", objCHeader._Name)
            End If
            objCHeader = Nothing
            dgv_Citems.DataSource = dtCItems
            dgv_Citems.DataBind()
            mblnItemAddEdit = True
            'S.SANDEEP |25-MAR-2019| -- START
            Label4.Text = Me.ViewState("Header")
            'S.SANDEEP |25-MAR-2019| -- END
            popup_CItemAddEdit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    'Shani(23-FEB-2017) -- Start
    'Enhancement - Add new custom item saving setting requested by (aga khan)
    Private Function isAll_Assessed_CustomItem(Optional ByVal blnFlag As Boolean = True) As Boolean
        Dim dsCustomItem As DataSet
        Try
            dsCustomItem = (New clsassess_custom_items).GetCutomItemList(CInt(cboPeriod.SelectedValue), menAssess)
            If dsCustomItem IsNot Nothing AndAlso dsCustomItem.Tables(0).Rows.Count > 0 Then
                If mdtCustomEvaluation.Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 42, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), Me)
                    Return False
                End If

                Dim ArrCustomItem = mdtCustomEvaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("dispaly_value") <> "" AndAlso x.Field(Of Boolean)("isvoid") = False).Select(Function(x) x.Field(Of Integer)("customitemunkid")).Distinct.ToArray

                Dim ar = dsCustomItem.Tables(0).AsEnumerable().Where(Function(x) Not ArrCustomItem.Contains(x.Field(Of Integer)("customitemunkid")))

                If ar.Count > 0 Then
                    If blnFlag = True Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 43, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), Me)
                        Return False
                    Else
                        Return False
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Function
    'Shani(23-FEB-2017) -- End

    ''''''''''''''''''''''''''''''''''' ADD EDIT DELETE ROWS IN TABLES
    Private Sub Evaluated_Data_BSC(ByVal iResult As Decimal, ByVal iRemark As String, ByVal iRowIdx As Integer, ByVal iAgreedScore As Decimal, Optional ByVal isRemarkChanged As Boolean = False)
        Try
            Dim dRow As DataRow = Nothing : Dim dTemp() As DataRow = Nothing
            dTemp = GetOldValue_BSC(iRowIdx)
            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_WEIGHTED_BASED Then
                If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                    If CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D' ")) > xTotAssignedWeight Then
                        'S.SANDEEP |08-JAN-2019| -- START
                        'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry you cannot go beyond the total weight assigned."), Me)
                        'Exit Sub
                        If CBool(Session("EnableBSCAutomaticRating")) = False Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry you cannot go beyond the total weight assigned."), Me)
                            Exit Sub
                        End If
                        'S.SANDEEP |08-JAN-2019| -- END
                    End If
                End If
            End If

            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            If mdtBSC_Evaluation Is Nothing Then Exit Sub
            'S.SANDEEP |18-JAN-2020| -- END


            If dTemp IsNot Nothing AndAlso dTemp.Length <= 0 Then
                dRow = mdtBSC_Evaluation.NewRow
                dRow.Item("analysistranunkid") = -1
                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                dRow.Item("empfield1unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield1unkid"))
                If iResult <= 0 Then
                    dRow.Item("result") = dRow.Item("result")
                Else
                    'S.SANDEEP |21-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                    'dRow.Item("result") = iResult
                    dRow.Item("result") = Format(CDec(iResult), "###################.#0")
                    'S.SANDEEP |21-AUG-2019| -- END
                End If
                dRow.Item("remark") = iRemark
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                dRow.Item("empfield2unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield2unkid"))
                dRow.Item("empfield3unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield3unkid"))
                dRow.Item("empfield4unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield4unkid"))
                dRow.Item("empfield5unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield5unkid"))
                dRow.Item("perspectiveunkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("perspectiveunkid"))
                Select Case iExOrdr
                    Case enWeight_Types.WEIGHT_FIELD1
                        If dRow.Item("empfield1unkid") <= 0 Then Exit Sub
                    Case enWeight_Types.WEIGHT_FIELD2
                        If dRow.Item("empfield2unkid") <= 0 Then Exit Sub
                    Case enWeight_Types.WEIGHT_FIELD3
                        If dRow.Item("empfield3unkid") <= 0 Then Exit Sub
                    Case enWeight_Types.WEIGHT_FIELD4
                        If dRow.Item("empfield4unkid") <= 0 Then Exit Sub
                    Case enWeight_Types.WEIGHT_FIELD5
                        If dRow.Item("empfield5unkid") <= 0 Then Exit Sub
                End Select
                If isRemarkChanged = False Then
                    dRow.Item("item_weight") = Me.ViewState("mdecItemWeight")
                    dRow.Item("max_scale") = Me.ViewState("mdecMaxScale")
                End If
                dRow.Item("iPeriodId") = cboPeriod.SelectedValue
                Select Case iExOrdr
                    Case enWeight_Types.WEIGHT_FIELD1
                        dRow.Item("iItemUnkid") = dRow.Item("empfield1unkid")
                    Case enWeight_Types.WEIGHT_FIELD2
                        dRow.Item("iItemUnkid") = dRow.Item("empfield2unkid")
                    Case enWeight_Types.WEIGHT_FIELD3
                        dRow.Item("iItemUnkid") = dRow.Item("empfield3unkid")
                    Case enWeight_Types.WEIGHT_FIELD4
                        dRow.Item("iItemUnkid") = dRow.Item("empfield4unkid")
                    Case enWeight_Types.WEIGHT_FIELD5
                        dRow.Item("iItemUnkid") = dRow.Item("empfield5unkid")
                End Select
                dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dRow.Item("iScore") = iResult
                'dRow.Item("agreed_score") = iAgreedScore
                dRow.Item("iScore") = Format(CDec(iResult), "###################0.#0")
                dRow.Item("agreed_score") = Format(CDec(iAgreedScore), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END
                mdtBSC_Evaluation.Rows.Add(dRow)
                Me.ViewState("mdecItemWeight") = 0 : Me.ViewState("mdecMaxScale") = 0
            Else
                If menAction <> enAction.EDIT_ONE Then
                    dTemp(0).Item("analysistranunkid") = -1
                Else
                    dTemp(0).Item("analysistranunkid") = dTemp(0).Item("analysistranunkid")
                End If
                dTemp(0).Item("analysisunkid") = mintAssessAnalysisUnkid
                dTemp(0).Item("empfield1unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield1unkid"))
                If iResult <= 0 Then
                    dTemp(0).Item("result") = dTemp(0).Item("result")
                Else
                    'S.SANDEEP |21-AUG-2019| -- START
                    'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                    'dTemp(0).Item("result") = iResult
                    dTemp(0).Item("result") = Format(CDec(iResult), "###################0.#0")
                    'S.SANDEEP |21-AUG-2019| -- END
                End If
                dTemp(0).Item("remark") = iRemark
                If IsDBNull(dTemp(0).Item("AUD")) Or CStr(dTemp(0).Item("AUD")).ToString.Trim = "" Then
                    dTemp(0).Item("AUD") = "U"
                End If
                dTemp(0).Item("GUID") = Guid.NewGuid.ToString
                dTemp(0).Item("isvoid") = False
                dTemp(0).Item("voiduserunkid") = -1
                dTemp(0).Item("voiddatetime") = DBNull.Value
                dTemp(0).Item("voidreason") = ""
                dTemp(0).Item("empfield2unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield2unkid"))
                dTemp(0).Item("empfield3unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield3unkid"))
                dTemp(0).Item("empfield4unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield4unkid"))
                dTemp(0).Item("empfield5unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield5unkid"))
                dTemp(0).Item("perspectiveunkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("perspectiveunkid"))

                If isRemarkChanged = False Then
                    dTemp(0).Item("item_weight") = Me.ViewState("mdecItemWeight")
                    dTemp(0).Item("max_scale") = Me.ViewState("mdecMaxScale")
                End If
                dTemp(0).Item("iPeriodId") = cboPeriod.SelectedValue
                Select Case iExOrdr
                    Case enWeight_Types.WEIGHT_FIELD1
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield1unkid")
                    Case enWeight_Types.WEIGHT_FIELD2
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield2unkid")
                    Case enWeight_Types.WEIGHT_FIELD3
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield3unkid")
                    Case enWeight_Types.WEIGHT_FIELD4
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield4unkid")
                    Case enWeight_Types.WEIGHT_FIELD5
                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield5unkid")
                End Select
                dTemp(0).Item("iEmployeeId") = cboEmployee.SelectedValue
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dTemp(0).Item("iScore") = iResult
                'dTemp(0).Item("agreed_score") = iAgreedScore
                dTemp(0).Item("iScore") = Format(CDec(iResult), "###################0.#0")
                dTemp(0).Item("agreed_score") = Format(CDec(iAgreedScore), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END
                mdtBSC_Evaluation.AcceptChanges()
                Me.ViewState("mdecItemWeight") = 0 : Me.ViewState("mdecMaxScale") = 0
            End If
            Call SetTotals(dgvBSC)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Private Sub Evaluated_Data_GE(ByVal iResult As Decimal, ByVal iRemark As String, ByVal iRowIdx As Integer, ByVal xGrdItem As DataGridItem, ByVal iAgreedScore As Decimal, Optional ByVal isRemarkChanged As Boolean = False)
        Dim dRow As DataRow = Nothing
        Dim dTemp() As DataRow = Nothing
        Try
            Dim dtTemp() As DataRow = Nothing
            Dim iDecWeight As Decimal = 0
            Decimal.TryParse(CStr(xGrdItem.Cells(disGEColumn("dgcolhGEWeight")).Text), iDecWeight)  'WEIGHT
            If iDecWeight <= 0 Then iDecWeight = 1
            If Session("ConsiderItemWeightAsNumber") = False Then iDecWeight = 1
            dtTemp = GetOldValue_GE(xGrdItem)

            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            If mdtGE_Evaluation Is Nothing Then Exit Sub
            'S.SANDEEP |18-JAN-2020| -- END

            If dtTemp.Length <= 0 Then
                dRow = mdtGE_Evaluation.NewRow
                dRow.Item("analysistranunkid") = -1
                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                dRow.Item("assessgroupunkid") = CInt(xGrdItem.Cells(disGEColumn("objdgcolhassessgroupunkidGE")).Text)    'assessgroupunkid
                dRow.Item("competenciesunkid") = CInt(xGrdItem.Cells(disGEColumn("objdgcolhcompetenciesunkidGE")).Text)   'competenciesunkid
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dRow.Item("result") = iResult
                dRow.Item("result") = Format(CDec(iResult), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END
                dRow.Item("remark") = iRemark
                dRow.Item("AUD") = "A"
                dRow.Item("GUID") = Guid.NewGuid.ToString
                dRow.Item("isvoid") = False
                dRow.Item("voiduserunkid") = -1
                dRow.Item("voiddatetime") = DBNull.Value
                dRow.Item("voidreason") = ""
                dRow.Item("dresult") = iResult * iDecWeight
                If isRemarkChanged = False Then
                    dRow.Item("item_weight") = Me.ViewState("mdecItemWeight")
                    'S.SANDEEP |16-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                    'dRow.Item("max_scale") = Me.ViewState("mdecMaxScale")
                    If CInt(Me.ViewState("mdecMaxScale")) <= 0 Then
                        Dim dsScore_Guide As New DataSet
                        Dim objScaleMaster As New clsAssessment_Scale
                        dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(xGrdItem.Cells(disGEColumn("objdgcolhscalemasterunkidGE")).Text))
                        objScaleMaster = Nothing
                        If dsScore_Guide.Tables(0).Rows.Count > 0 Then
                            dRow.Item("max_scale") = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
                        End If
                    Else
                        dRow.Item("max_scale") = Me.ViewState("mdecMaxScale")
                    End If
                    'S.SANDEEP |16-JUL-2019| -- END
                End If
                dRow.Item("iPeriodId") = cboPeriod.SelectedValue
                dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dRow.Item("iScore") = iResult
                'dRow.Item("agreed_score") = iAgreedScore
                dRow.Item("iScore") = Format(CDec(iResult), "###################0.#0")
                dRow.Item("agreed_score") = Format(CDec(iAgreedScore), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END
                dRow.Item("iItemUnkid") = dRow.Item("competenciesunkid")
                mdtGE_Evaluation.Rows.Add(dRow)
                Me.ViewState("mdecItemWeight") = 0 : Me.ViewState("mdecMaxScale") = 0
            Else
                dtTemp(0).Item("analysistranunkid") = dtTemp(0).Item("analysistranunkid")
                dtTemp(0).Item("analysisunkid") = mintAssessAnalysisUnkid
                dtTemp(0).Item("assessgroupunkid") = CInt(xGrdItem.Cells(disGEColumn("objdgcolhassessgroupunkidGE")).Text)    'assessgroupunkid
                dtTemp(0).Item("competenciesunkid") = CInt(xGrdItem.Cells(disGEColumn("objdgcolhcompetenciesunkidGE")).Text)   'competenciesunkid
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dtTemp(0).Item("result") = iResult
                dtTemp(0).Item("result") = Format(CDec(iResult), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END
                dtTemp(0).Item("remark") = iRemark
                If IsDBNull(dtTemp(0).Item("AUD")) Or CStr(dtTemp(0).Item("AUD")).ToString.Trim = "" Then
                    dtTemp(0).Item("AUD") = "U"
                End If
                dtTemp(0).Item("GUID") = Guid.NewGuid.ToString
                dtTemp(0).Item("isvoid") = False
                dtTemp(0).Item("voiduserunkid") = -1
                dtTemp(0).Item("voiddatetime") = DBNull.Value
                dtTemp(0).Item("voidreason") = ""
                dtTemp(0).Item("dresult") = iResult * iDecWeight
                If isRemarkChanged = False Then
                    dtTemp(0).Item("item_weight") = Me.ViewState("mdecItemWeight")
                    'S.SANDEEP |16-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : ZRA, SELF SCORE COMPUTATION ISSUE
                    'dtTemp(0).Item("max_scale") = Me.ViewState("mdecMaxScale")
                    If CInt(Me.ViewState("mdecMaxScale")) <= 0 Then
                        Dim dsScore_Guide As New DataSet
                        Dim objScaleMaster As New clsAssessment_Scale
                        dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(xGrdItem.Cells(disGEColumn("objdgcolhscalemasterunkidGE")).Text))
                        objScaleMaster = Nothing
                        If dsScore_Guide.Tables(0).Rows.Count > 0 Then
                            dtTemp(0).Item("max_scale") = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
                        End If
                    Else
                        dtTemp(0).Item("max_scale") = Me.ViewState("mdecMaxScale")
                    End If
                    'S.SANDEEP |16-JUL-2019| -- END
                End If
                dtTemp(0).Item("iPeriodId") = cboPeriod.SelectedValue
                dtTemp(0).Item("iEmployeeId") = cboEmployee.SelectedValue
                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'dtTemp(0).Item("iScore") = iResult
                'dtTemp(0).Item("agreed_score") = iAgreedScore
                dtTemp(0).Item("iScore") = Format(CDec(iResult), "###################0.#0")
                dtTemp(0).Item("agreed_score") = Format(CDec(iAgreedScore), "###################0.#0")
                'S.SANDEEP |21-AUG-2019| -- END
                dtTemp(0).Item("iItemUnkid") = dtTemp(0).Item("competenciesunkid")
                dtTemp(0).AcceptChanges()
                Me.ViewState("mdecItemWeight") = 0 : Me.ViewState("mdecMaxScale") = 0
            End If

            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_WEIGHTED_BASED Then
                If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                    Dim objAGroup As New clsassess_group_master
                    objAGroup._Assessgroupunkid = CInt(xGrdItem.Cells(disGEColumn("objdgcolhassessgroupunkidGE")).Text)
                    If CDbl(mdtGE_Evaluation.Compute("SUM(result)", "assessgroupunkid = '" & CInt(xGrdItem.Cells(disGEColumn("objdgcolhassessgroupunkidGE")).Text) & "' AND AUD <> 'D' ")) > objAGroup._Weight Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 27, "Sorry you cannot go beyond the total weight assigned for the particular assessment group weight."), Me)

                        dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdItem.Cells(disGEColumn("objdgcolhassessgroupunkidGE")).Text) & "' AND competenciesunkid = '" & _
                                                                  CInt(xGrdItem.Cells(disGEColumn("objdgcolhcompetenciesunkidGE")).Text) & "' AND AUD <> 'D' ") '10-> competenciesunkid, 11-> assessgroupunkid

                        If dtTemp.Length > 0 Then
                            dtTemp(0).Item("result") = 0 : CType(xGrdItem.Cells(disGEColumn("dgcolhaselfGE")).Controls(1), TextBox).Text = 0
                            dTemp(0).Item("dresult") = 0
                        End If
                        Exit Sub
                    End If
                    objAGroup = Nothing
                End If
            End If
            Call SetTotals(dgvGE)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Private Function isAll_Assessed_BSC(Optional ByVal blnFlag As Boolean = True) As Boolean
        Try
            If mdtBSC_Evaluation.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 22, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), Me)
                Return False
            End If
            Dim dtmp1() As DataRow = Nothing
            Dim dtmp2() As DataRow = Nothing

            Dim dView As DataView = dtBSC_TabularGrid.DefaultView

            Select Case iExOrdr
                Case enWeight_Types.WEIGHT_FIELD1
                    'S.SANDEEP |09-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'dtmp1 = dView.ToTable(True, "empfield1unkid", "Weight").Select("empfield1unkid > 0 AND Weight <> '' ")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield1unkid > 0")
                    dtmp1 = dView.ToTable(True, "empfield1unkid", "oWeight").Select("empfield1unkid > 0 AND oWeight > 0 ")
                    dtmp2 = mdtBSC_Evaluation.DefaultView.ToTable(True, "empfield1unkid").Select("empfield1unkid > 0")
                    'S.SANDEEP |09-JUL-2019| -- END

                Case enWeight_Types.WEIGHT_FIELD2
                    'S.SANDEEP |09-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'dtmp1 = dView.ToTable(True, "empfield2unkid", "Weight").Select("empfield2unkid > 0 AND Weight <> '' ")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield2unkid > 0")
                    dtmp1 = dView.ToTable(True, "empfield2unkid", "oWeight").Select("empfield2unkid > 0 AND oWeight > 0 ")
                    dtmp2 = mdtBSC_Evaluation.DefaultView.ToTable(True, "empfield2unkid").Select("empfield2unkid > 0")
                    'S.SANDEEP |09-JUL-2019| -- END

                Case enWeight_Types.WEIGHT_FIELD3
                    'S.SANDEEP |09-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'dtmp1 = dView.ToTable(True, "empfield3unkid", "Weight").Select("empfield3unkid > 0 AND Weight <> '' ")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield3unkid > 0")
                    dtmp1 = dView.ToTable(True, "empfield3unkid", "oWeight").Select("empfield3unkid > 0 AND oWeight > 0 ")
                    dtmp2 = mdtBSC_Evaluation.DefaultView.ToTable(True, "empfield3unkid").Select("empfield3unkid > 0")
                    'S.SANDEEP |09-JUL-2019| -- END

                Case enWeight_Types.WEIGHT_FIELD4
                    'S.SANDEEP |09-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'dtmp1 = dView.ToTable(True, "empfield4unkid", "Weight").Select("empfield4unkid > 0 AND Weight <> '' ")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield4unkid > 0")
                    dtmp1 = dView.ToTable(True, "empfield4unkid", "oWeight").Select("empfield4unkid > 0 AND oWeight > 0 ")
                    dtmp2 = mdtBSC_Evaluation.DefaultView.ToTable(True, "empfield4unkid").Select("empfield4unkid > 0")
                    'S.SANDEEP |09-JUL-2019| -- END

                Case enWeight_Types.WEIGHT_FIELD5
                    'S.SANDEEP |09-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'dtmp1 = dView.ToTable(True, "empfield5unkid", "Weight").Select("empfield5unkid > 0 AND Weight <> '' ")
                    'dtmp2 = mdtBSC_Evaluation.Select("empfield5unkid > 0")
                    dtmp1 = dView.ToTable(True, "empfield5unkid", "oWeight").Select("empfield5unkid > 0 AND oWeight > 0 ")
                    dtmp2 = mdtBSC_Evaluation.DefaultView.ToTable(True, "empfield5unkid").Select("empfield5unkid > 0")
                    'S.SANDEEP |09-JUL-2019| -- END

            End Select

            If dtmp1.Length <> dtmp2.Length Then
                If blnFlag = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 22, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), Me)
                    Return False
                Else
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Function

    Private Function isAll_Assessed_GE(Optional ByVal blnFlag As Boolean = True) As Boolean
        Try
            If mdtGE_Evaluation.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 23, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some assessment group."), Me)
                Return False
            End If
            Dim dtmp1() As DataRow = Nothing
            Dim dtmp2() As DataRow = Nothing
            Dim dView As DataView = dtGE_TabularGrid.DefaultView
            Dim dMView As DataView = mdtGE_Evaluation.DefaultView
            dtmp1 = dView.ToTable(True, "assessgroupunkid").Select("assessgroupunkid > 0")
            dtmp2 = dMView.ToTable(True, "assessgroupunkid").Select("assessgroupunkid > 0")
            If dtmp1.Length <> dtmp2.Length Then
                If blnFlag = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 24, "Sorry, you cannot do Save Commit Operation as you have not assessed all assessment group."), Me)
                    Return False
                End If
            End If
            dtmp1 = dView.ToTable.Select("assessgroupunkid > 0 AND IsGrp = False AND IsPGrp = False")

            dtmp2 = dMView.ToTable.Select("assessgroupunkid > 0")
            If dtmp1.Length <> dtmp2.Length Then
                If blnFlag = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 25, "Sorry, you cannot do Save Commit Operation as you have not assessed all item in assessment group."), Me)
                    Return False
                Else
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Function

    Private Function GetOldValue_BSC(ByVal xRowIndex As Integer) As DataRow()
        Dim xTemp() As DataRow = Nothing
        Try
            If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                Select Case CInt(Me.ViewState("iExOrdr"))
                    Case enWeight_Types.WEIGHT_FIELD1
                        xTemp = mdtBSC_Evaluation.Select("empfield1unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield1unkid").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD2
                        xTemp = mdtBSC_Evaluation.Select("empfield2unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield2unkid").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD3
                        xTemp = mdtBSC_Evaluation.Select("empfield3unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield3unkid").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD4
                        xTemp = mdtBSC_Evaluation.Select("empfield4unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield4unkid").ToString & "' AND AUD <> 'D' ")
                    Case enWeight_Types.WEIGHT_FIELD5
                        xTemp = mdtBSC_Evaluation.Select("empfield5unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield5unkid").ToString & "' AND AUD <> 'D' ")
                End Select
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return xTemp
    End Function

    Private Function GetOldValue_GE(ByVal xGrdRow As DataGridItem) As DataRow()
        Dim xTemp() As DataRow = Nothing
        Try
            If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                xTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdRow.Cells(disGEColumn("objdgcolhassessgroupunkidGE")).Text) & "' AND competenciesunkid = '" & _
                                                              CInt(xGrdRow.Cells(disGEColumn("objdgcolhcompetenciesunkidGE")).Text) & "' AND AUD <> 'D' ")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return xTemp
    End Function
#End Region

#Region "Button Event"
    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or _
              CInt(cboPeriod.SelectedValue) <= 0 Then

                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please set following information [Employee,Period] to view assessment."), Me)
                Exit Sub
            End If

            Dim blnOnlyCommitted As Boolean = True
            If menAction = enAction.EDIT_ONE Then
                blnOnlyCommitted = False
            End If

            If CBool(Session("AllowAssessor_Before_Emp")) = False Then
                'S.SANDEEP |26-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
                'If objEAnalysisMst.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1) = False Then
                If objEAnalysisMst.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1, , , blnOnlyCommitted) = False Then
                    'S.SANDEEP |26-AUG-2019| -- END
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
                                                                "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), Me, "wPg_AssessorEvaluationList.aspx")
                    Exit Sub
                End If
            End If

            'S.SANDEEP |09-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : PA CHANGES
            'Dim objPrd As New clscommom_period_Tran
            'objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'If dtpAssessdate.GetDate <= objPrd._Start_Date.Date Then
            '    Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 36, "Sorry, assessment date should be greater than") & " ( " & objPrd._Start_Date.Date.ToShortDateString & " )."
            '    DisplayMessage.DisplayMessage(strMsg, Me)
            '    dtpAssessdate.Focus()
            '    objPrd = Nothing
            '    Exit Sub
            'End If
            'objPrd = Nothing
            'S.SANDEEP |09-JUL-2019| -- END

            If CBool(Session("AllowAssessor_Before_Emp")) = False Then
                Dim strMsg As String = String.Empty
                If menAssess <> enAssessmentMode.SELF_ASSESSMENT Then
                    'S.SANDEEP |09-JUL-2019| -- START
                    'ISSUE/ENHANCEMENT : PA CHANGES
                    'strMsg = objEAnalysisMst.IsValidAssessmentDate(enAssessmentMode.APPRAISER_ASSESSMENT, dtpAssessdate.GetDate, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
                    'If strMsg <> "" Then
                    '    DisplayMessage.DisplayMessage(strMsg, Me)
                    '    Exit Sub
                    'End If
                    If dtpAssessdate.IsNull = False Then
                    strMsg = objEAnalysisMst.IsValidAssessmentDate(enAssessmentMode.APPRAISER_ASSESSMENT, dtpAssessdate.GetDate, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
                    If strMsg <> "" Then
                        DisplayMessage.DisplayMessage(strMsg, Me)
                        Exit Sub
                    End If
                End If
                    'S.SANDEEP |09-JUL-2019| -- END
                End If
            End If
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                'S.SANDEEP |18-JAN-2019| -- START
                'objbtnNext.Enabled = True : iHeaderId = -1 : objbtnNext_Click(sender, e)
                objbtnNext.Enabled = True : iHeaderId = -1
                'S.SANDEEP |18-JAN-2019| -- END
                objbtnBack.Enabled = True
            End If

            'S.SANDEEP |20-SEP-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#0004155}
            lnkCopyScore.Enabled = True
            'S.SANDEEP |20-SEP-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub objbtnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnNext.Click
        Try
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                If iHeaderId < dsHeaders.Tables(0).Rows.Count - 1 Then
                    iHeaderId += 1
                    objbtnBack.Enabled = True
                End If
                If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
                    objbtnNext.Enabled = False
                End If
                Call PanelVisibility()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub objbtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnBack.Click
        Try
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                iHeaderId = iHeaderId - 1
                If iHeaderId <= -1 Then
                    objbtnBack.Enabled = False
                    objbtnNext.Enabled = True
                End If
                If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
                    objbtnNext.Enabled = False
                Else
                    objbtnNext.Enabled = True
                End If
                Call PanelVisibility()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Protected Sub btnIClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIClose.Click
        Try
            mblnItemAddEdit = False
            popup_CItemAddEdit.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub btnIAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIAdd.Click
        Try
            If Session("dtCItems") IsNot Nothing Then
                dtCItems = Session("dtCItems")
            End If
            Dim dgv As DataGridItem = Nothing
            For Each xRow As DataRow In dtCItems.Rows
                For i As Integer = 0 To dgv_Citems.Items.Count - 1
                    dgv = dgv_Citems.Items(i)
                    Select Case xRow("itemtypeid")
                        Case clsassess_custom_items.enCustomType.FREE_TEXT
                            If dgv.FindControl("txt" & xRow("customitemunkid")) IsNot Nothing Then
                                Dim xtxt As TextBox = CType(dgv.FindControl("txt" & xRow("customitemunkid")), TextBox)
                                xRow.Item("custom_value") = xtxt.Text
                                Exit For
                            End If
                        Case clsassess_custom_items.enCustomType.SELECTION
                            If dgv.FindControl("cbo" & xRow("customitemunkid")) IsNot Nothing Then
                                Dim xCbo As DropDownList = CType(dgv.FindControl("cbo" & xRow("customitemunkid")), DropDownList)
                                If xCbo.SelectedValue > 0 Then
                                    xRow.Item("custom_value") = xCbo.SelectedValue
                                    xRow.Item("selectedid") = xCbo.SelectedValue
                                    Exit For
                                End If
                            End If
                        Case clsassess_custom_items.enCustomType.DATE_SELECTION
                            If dgv.FindControl("dtp" & xRow("customitemunkid")) IsNot Nothing Then
                                If CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).IsNull = False Then
                                    xRow.Item("custom_value") = CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate
                                    xRow.Item("ddate") = CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate.Date
                                    Exit For
                                End If
                            End If
                        Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                            If dgv.FindControl("txtnum" & xRow("customitemunkid")) IsNot Nothing Then
                                Dim xtxt As TextBox = CType(dgv.FindControl("txtnum" & xRow("customitemunkid").ToString()), TextBox)
                                xRow.Item("custom_value") = xtxt.Text
                                Exit For
                            End If
                    End Select
                Next
            Next
            Dim dtmp() As DataRow = Nothing
            If dtCItems IsNot Nothing Then
                dtCItems.AcceptChanges()
                dtmp = dtCItems.Select("custom_value = '' AND rOnly = FALSE ")
                Dim strMsg As String = ""

                If dtmp.Length > 0 Then
                    For Each dRow As DataRow In dtmp
                        strMsg &= " \n * " & dRow.Item("custom_item").ToString
                    Next
                End If
                If strMsg.Trim.Length > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmAddCustomValue", 3, "Sorry, Please enter all mandatory information mentioned below in order to save.") & strMsg, Me)
                    Exit Sub
                End If
                'Shani(24-Feb-2016) -- End
                dtmp = dtCItems.Select("")
            End If
            If dtmp.Length > 0 Then
                If CInt(Me.ViewState("RowIndex")) <= -1 Then
                    Dim iGUID As String = ""
                    iGUID = Guid.NewGuid.ToString
                    For iR As Integer = 0 To dtmp.Length - 1
                        Dim dRow As DataRow = mdtCustomEvaluation.NewRow
                        dRow.Item("customanalysistranguid") = iGUID
                        dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
                        dRow.Item("customitemunkid") = dtmp(iR).Item("customitemunkid")
                        dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                        Select Case CInt(dtmp(iR).Item("itemtypeid"))
                            Case clsassess_custom_items.enCustomType.FREE_TEXT
                                dRow.Item("custom_value") = dtmp(iR).Item("custom_value")
                            Case clsassess_custom_items.enCustomType.SELECTION
                                dRow.Item("custom_value") = dtmp(iR).Item("selectedid")
                            Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                If dtmp(iR).Item("ddate").ToString.Trim.Length > 0 Then
                                    dRow.Item("custom_value") = eZeeDate.convertDate(dtmp(iR).Item("ddate"))
                                Else
                                    dRow.Item("custom_value") = ""
                                End If
                            Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                dRow.Item("custom_value") = dtmp(iR).Item("custom_value")
                        End Select
                        dRow.Item("custom_header") = Me.ViewState("Header")
                        dRow.Item("custom_item") = dtmp(iR).Item("custom_item")
                        dRow.Item("dispaly_value") = dtmp(iR).Item("custom_value")
                        dRow.Item("AUD") = "A"
                        dRow.Item("itemtypeid") = dtmp(iR).Item("itemtypeid")
                        dRow.Item("selectionmodeid") = dtmp(iR).Item("selectionmodeid")
                        'S.SANDEEP |25-MAR-2019| -- START
                        dRow.Item("Header_Id") = dtmp(iR).Item("customheaderunkid")
                        'S.SANDEEP |25-MAR-2019| -- END
                        mdtCustomEvaluation.Rows.Add(dRow)
                    Next
                Else
                    If dtmp.Length > 0 Then
                        For iEdit As Integer = 0 To dtmp.Length - 1
                            Dim xRow() As DataRow = mdtCustomEvaluation.Select("customitemunkid = '" & dtmp(iEdit).Item("customitemunkid") & "' AND customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D'")
                            If xRow.Length > 0 Then
                                xRow(0).Item("customanalysistranguid") = xRow(0).Item("customanalysistranguid")
                                xRow(0).Item("analysisunkid") = xRow(0).Item("analysisunkid")
                                xRow(0).Item("customitemunkid") = dtmp(iEdit).Item("customitemunkid")
                                xRow(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
                                    Case clsassess_custom_items.enCustomType.FREE_TEXT
                                        xRow(0).Item("custom_value") = dtmp(iEdit).Item("custom_value")
                                    Case clsassess_custom_items.enCustomType.SELECTION
                                        xRow(0).Item("custom_value") = dtmp(iEdit).Item("selectedid")
                                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
                                        If dtmp(iEdit).Item("ddate").ToString.Trim.Length > 0 Then
                                            xRow(0).Item("custom_value") = eZeeDate.convertDate(dtmp(iEdit).Item("ddate"))
                                        Else
                                            xRow(0).Item("custom_value") = ""
                                        End If
                                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
                                        xRow(0).Item("custom_value") = dtmp(iEdit).Item("custom_value")
                                End Select
                                xRow(0).Item("custom_header") = Me.ViewState("Header")
                                xRow(0).Item("custom_item") = dtmp(iEdit).Item("custom_item")
                                xRow(0).Item("dispaly_value") = dtmp(iEdit).Item("custom_value")
                                If xRow(0).Item("analysisunkid") <= 0 Then
                                    xRow(0).Item("AUD") = "A"
                                Else
                                    xRow(0).Item("AUD") = "U"
                                End If
                                xRow(0).Item("itemtypeid") = dtmp(iEdit).Item("itemtypeid")
                                xRow(0).Item("selectionmodeid") = dtmp(iEdit).Item("selectionmodeid")
                                'S.SANDEEP |25-MAR-2019| -- START
                                xRow(0).Item("Header_Id") = dtmp(iEdit).Item("customheaderunkid")
                                'S.SANDEEP |25-MAR-2019| -- END
                            End If
                        Next
                    End If
                End If
            End If
            mblnItemAddEdit = False
            popup_CItemAddEdit.Hide()
            Call Fill_Custom_Evaluation_Data()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Try
            If txtMessage.Text.Trim.Length > 0 Then
                If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
                    Dim xRow() As DataRow = Nothing
                    xRow = mdtCustomEvaluation.Select("customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D' AND isdefaultentry = False")
                    If xRow.Length > 0 Then
                        For x As Integer = 0 To xRow.Length - 1
                            xRow(x).Item("isvoid") = True
                            xRow(x).Item("voiduserunkid") = Session("UserId")
                            xRow(x).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                            xRow(x).Item("voidreason") = txtMessage.Text
                            xRow(x).Item("AUD") = "D"
                        Next
                        Call Fill_Custom_Evaluation_Data()
                    End If
                End If
            Else
                popup_CItemReason.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub btnSaveCommit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCommit.Click
        Try
            'S.SANDEEP |20-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : ASSESSMENT DATE SET TO FUTURE 
            If dtpAssessdate.GetDate.Date > Now.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 58, "Sorry, you cannot commit assessment for future date. Please set the assessment date as today date."), Me)
                Exit Sub
            End If
            'S.SANDEEP |20-NOV-2019| -- END

            If dsHeaders.Tables.Count <= 0 Then Exit Sub
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                If Validation() = False Then Exit Sub

                'S.SANDEEP |14-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : {#0003481|ARUTI-567}
                If CBool(Session("MakeAsrAssessCommentsMandatory")) Then
                    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                        If mdtBSC_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 51, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for Objective/Targets. Please provide remark to continue. "), Me)
                            Exit Sub
                        End If
                    End If
                    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                        If mdtGE_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue. "), enMsgBoxStyle.Information)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue. "), Me)
                            'Sohail (23 Mar 2019) -- End

                            Exit Sub
                        End If
                    End If
                End If
                'S.SANDEEP |14-MAR-2019| -- END

                Dim dtmp() As DataRow = Nothing

                'Shani(23-FEB-2017) -- Start
                'Enhancement - Add new custom item saving setting requested by (aga khan)
                'If mdtCustomEvaluation IsNot Nothing Then
                '    If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables(0).Rows.Count > 0 Then
                '        dtmp = dsHeaders.Tables(0).Select("Id > 0")
                '        Dim dCEval As DataView = New DataView(mdtCustomEvaluation, "periodunkid = '" & CInt(cboPeriod.SelectedValue) & "'", "", DataViewRowState.CurrentRows)
                '        If dtmp.Length <> dCEval.ToTable(True, "custom_header").Rows.Count Then
                '            lblConfirmMessage.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 38, "You still have some assessment items not evaluated. Do you want to save now and complete them later?")
                '            btnCnfYes.CommandName = btnSaveCommit.ID
                '            popup_CnfYesNo.Show()
                '            Exit Sub
                '        End If
                '    End If
                'End If
                'Shani(23-FEB-2017) -- End
                btnSaveCommit.CommandName = btnSaveCommit.ID
                Call btnCnfYes_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If dsHeaders.Tables.Count <= 0 Then Exit Sub
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                If Validation() = False Then Exit Sub
                'S.SANDEEP |14-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : {#0003481|ARUTI-567}
                If CBool(Session("MakeAsrAssessCommentsMandatory")) Then
                    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                        If mdtBSC_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 51, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for Objective/Targets. Please provide remark to continue. "), Me)
                            Exit Sub
                        End If
                    End If
                    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                        If mdtGE_Evaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D" And x.Field(Of String)("remark") = "").Count > 0 Then
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue. "), enMsgBoxStyle.Information)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 52, "Sorry, As per the setting set on configuration, Remark are mandatory information for each score you set for competencies. Please provide remark to continue. "), Me)
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                    End If
                End If
                'S.SANDEEP |14-MAR-2019| -- END
                Dim dtmp() As DataRow = Nothing
                If mdtCustomEvaluation IsNot Nothing Then
                    If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables(0).Rows.Count > 0 Then
                        dtmp = dsHeaders.Tables(0).Select("Id > 0")
                        Dim dCEval As DataView = New DataView(mdtCustomEvaluation, "periodunkid = '" & CInt(cboPeriod.SelectedValue) & "'", "", DataViewRowState.CurrentRows)
                        If dtmp.Length <> dCEval.ToTable(True, "custom_header").Rows.Count Then
                            lblConfirmMessage.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 38, "You still have some assessment items not evaluated. Do you want to save now and complete them later?")
                            btnCnfYes.CommandName = ""
                            popup_CnfYesNo.Show()
                            Exit Sub
                        End If
                    End If
                End If
                btnCnfYes.CommandName = ""
                Call btnCnfYes_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub btnCnfNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCnfNo.Click
        Try
            objbtnNext_Click(sender, e)
            Exit Sub
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub btnCnfYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCnfYes.Click
        Dim blnFlag As Boolean = False
        Dim blnIsBSC_Set As Boolean = False
        Dim blnIsGE_Set As Boolean = False
        Try
            If dsHeaders.Tables.Count <= 0 Then Exit Sub
            If dsHeaders.Tables(0).Rows.Count > 0 Then
                Dim dtmp() As DataRow = Nothing
                Dim blnIsOneAssessed As Boolean = False
                If mdtBSC_Evaluation IsNot Nothing Then
                    dtmp = dsHeaders.Tables(0).Select("Id=-3")
                    If dtmp.Length > 0 Then
                        blnIsBSC_Set = True
                        dtmp = mdtBSC_Evaluation.Select("AUD <> 'D'")
                        If dtmp.Length > 0 Then
                            blnIsOneAssessed = True
                        End If
                    End If
                End If
                If mdtGE_Evaluation IsNot Nothing Then
                    dtmp = dsHeaders.Tables(0).Select("Id=-2")
                    If dtmp.Length > 0 Then
                        blnIsGE_Set = True
                        dtmp = mdtGE_Evaluation.Select("AUD <> 'D'")
                        If dtmp.Length > 0 Then
                            blnIsOneAssessed = True
                        End If
                    End If
                End If

                If blnIsOneAssessed = False Then
                    'S.SANDEEP [01-OCT-2018] -- START
                    'ISSUE/ENHANCEMENT : {Ref#2586|ARUTI-}
                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 35, "Sorry, Please assess atleast one item from Balance Score Card Or Competencies Evaluation in order to save."), Me)
                    If blnIsGE_Set = True AndAlso blnIsBSC_Set = True Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 35, "Sorry, Please assess atleast one item from Balance Score Card Or Competencies Evaluation in order to save."), Me)
                    ElseIf blnIsBSC_Set = True Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 46, "Sorry, Please assess atleast one item from Balance Score Card Evaluation in order to save."), Me)
                    ElseIf blnIsGE_Set = True Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 47, "Sorry, Please assess atleast one item from Competencies Evaluation in order to save."), Me)
                    End If
                    'S.SANDEEP [01-OCT-2018] -- END
                    Exit Sub
                End If
                Call SetValue()
                Select Case CType(sender, Button).CommandName.ToUpper
                    Case btnSaveCommit.ID.ToUpper
                        If Session("IsAllowFinalSave") = False Then
                            If blnIsBSC_Set = True Then If isAll_Assessed_BSC() = False Then Exit Sub
                            If blnIsGE_Set = True Then If isAll_Assessed_GE() = False Then Exit Sub
                            'Call CnfSaveCommit_buttonYes_Click(New Object, New EventArgs)
                            'Exit Sub
                        ElseIf Session("IsAllowFinalSave") = True Then
                            'S.SANDEEP [11-OCT-2018] -- START
                            'If isAll_Assessed_BSC(False) = False Then 'Shani(30-MAR-2016) 
                            '    CnfSaveCommit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                            '    CnfSaveCommit.Show()
                            '    Exit Sub
                            'End If
                            'If isAll_Assessed_GE(False) = False Then
                            '    CnfSaveCommit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                            '    CnfSaveCommit.Show()
                            '    Exit Sub
                            'End If
                            If blnIsBSC_Set = True Then
                                If isAll_Assessed_BSC(False) = False Then
                                CnfSaveCommit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                                CnfSaveCommit.Show()
                                Exit Sub
                            End If
                            End If
                            If blnIsGE_Set = True Then
                            If isAll_Assessed_GE(False) = False Then
                                CnfSaveCommit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
                                CnfSaveCommit.Show()
                                Exit Sub
                            End If
                            End If
                            'S.SANDEEP [11-OCT-2018] -- END

                            'Call CnfSaveCommit_buttonYes_Click(New Object, New EventArgs)
                            'Exit Sub
                        End If
                        'Shani(23-FEB-2017) -- Start
                        'Enhancement - Add new custom item saving setting requested by (aga khan)
                        If Session("IsAllowCustomItemFinalSave") = False Then
                            If isAll_Assessed_CustomItem() = False Then Exit Sub
                        ElseIf Session("IsAllowFinalSave") = True Then
                            If isAll_Assessed_CustomItem(False) = False Then
                                CnfSaveCommit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 38, "You still have some assessment items not evaluated. Do you want to save now and complete them later?")
                                CnfSaveCommit.Show()
                                Exit Sub
                            End If
                        End If
                            Call CnfSaveCommit_buttonYes_Click(New Object, New EventArgs)
                            Exit Sub
                        'Shani(23-FEB-2017) -- End
                End Select

                'S.SANDEEP [27-APR-2017] -- START
                'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
                'If menAction = enAction.EDIT_ONE Then
                '    blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
                'Else
                '    blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
                'End If

                If menAction = enAction.EDIT_ONE Then
                    blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation, Session("ScoringOptionId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("IsUseAgreedScore"), Session("Self_Assign_Competencies"))
                Else
                    'S.SANDEEP |18-JAN-2020| -- START
                    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                    If Is_Already_Assessed() = False Then
                        Exit Sub
                    End If
                    'S.SANDEEP |18-JAN-2020| -- END
                    blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation, Session("ScoringOptionId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("IsUseAgreedScore"), Session("Self_Assign_Competencies"))
                End If
                'S.SANDEEP [27-APR-2017] -- END

                If blnFlag = False And objEAnalysisMst._Message <> "" Then
                    DisplayMessage.DisplayMessage(objEAnalysisMst._Message, Me)
                    Exit Sub
                End If

                If blnFlag = True Then
                    Call ClearForm_Values()
                    If Request.QueryString.Count > 0 Then
                        Response.Redirect("~/Index.aspx",False)
                    Else
                        'S.SANDEEP |25-MAR-2019| -- START
                        'Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_AssessorEvaluationList.aspx", False)

                        'S.SANDEEP |30-MAR-2019| -- START
                        'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 54, "Assessment has been submitted successfully."), Me, "wPg_AssessorEvaluationList.aspx")
                        Select Case CType(sender, Button).CommandName.ToUpper
                            Case btnSaveCommit.ID.ToUpper
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 54, "Assessment has been submitted successfully."), Me, "wPg_AssessorEvaluationList.aspx")
                            Case Else
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 56, "Assessment has been saved successfully."), Me, "wPg_AssessorEvaluationList.aspx")
                        End Select
                        'S.SANDEEP |30-MAR-2019| -- END

                        'S.SANDEEP |25-MAR-2019| -- END
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub


    Protected Sub CnfSaveCommit_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CnfSaveCommit.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Call SetValue()
            objEAnalysisMst._Iscommitted = True
            objEAnalysisMst._Committeddatetime = ConfigParameter._Object._CurrentDateAndTime

            'S.SANDEEP [27-APR-2017] -- START
            'ISSUE/ENHANCEMENT : OPTIMIZING PERFORMANCE MODULE
            'If menAction = enAction.EDIT_ONE Then
            '    blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
            'Else
            '    blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
            'End If

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation, Session("ScoringOptionId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("IsUseAgreedScore"), Session("Self_Assign_Competencies"))
            Else
                'S.SANDEEP |18-JAN-2020| -- START
                'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                If Is_Already_Assessed() = False Then
                    Exit Sub
                End If
                'S.SANDEEP |18-JAN-2020| -- END
                blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation, Session("ScoringOptionId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("IsUseAgreedScore"), Session("Self_Assign_Competencies"))
            End If
            'S.SANDEEP [27-APR-2017] -- END

            If blnFlag = False And objEAnalysisMst._Message <> "" Then
                DisplayMessage.DisplayMessage(objEAnalysisMst._Message, Me)
                Exit Sub
            End If

            If blnFlag = True Then
                Call ClearForm_Values()
                objEAnalysisMst.Email_Notification(enAssessmentMode.APPRAISER_ASSESSMENT, _
                                                   CInt(cboEmployee.SelectedValue), _
                                                   CInt(cboPeriod.SelectedValue), _
                                                   Session("IsCompanyNeedReviewer"), _
                                                   Session("Database_Name"), _
                                                   Session("CompanyUnkId"), _
                                                   Session("ArutiSelfServiceURL"), _
                                                   CStr(cboAssessor.SelectedItem.Text), _
                                                   enLogin_Mode.MGR_SELF_SERVICE, 0)
                If Request.QueryString.Count > 0 Then
                    Response.Redirect("~/Index.aspx", False)
                Else
                    'S.SANDEEP |25-MAR-2019| -- START
                    'Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_AssessorEvaluationList.aspx", False)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 54, "Assessment has been submitted successfully."), Me, "wPg_AssessorEvaluationList.aspx")
                    'S.SANDEEP |25-MAR-2019| -- END
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Call ClearForm_Values()
        Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_AssessorEvaluationList.aspx", False)
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            txtInstruction.Text = Session("Assessment_Instructions")
            objlblCaption.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Intructions")
            objpnlInstruction.Visible = True
            txtInstruction.Height = Unit.Pixel(430)

            objbtnNext.Enabled = False : objbtnBack.Enabled = False
            objlblValue1.Visible = False : objlblValue2.Visible = False
            objlblValue3.Visible = False : objlblValue4.Visible = False

            Call FillCombo()
            If menAction = enAction.EDIT_ONE Then
                objEAnalysisMst._Analysisunkid = mintAssessAnalysisUnkid
                cboEmployee.Enabled = False
                cboAssessor.Enabled = False
                cboPeriod.Enabled = False
                radOption.Enabled = False
                cboReviewer.Enabled = False
                BtnSearch.Enabled = False
                BtnReset.Enabled = False
            End If
            Call GetValue()

            objGoalsTran._AnalysisUnkid = mintAssessAnalysisUnkid
            mdtBSC_Evaluation = objGoalsTran._DataTable

            objCAssessTran._ConsiderWeightAsNumber = CBool(Session("ConsiderItemWeightAsNumber"))
            objCAssessTran._AnalysisUnkid = mintAssessAnalysisUnkid
            mdtGE_Evaluation = objCAssessTran._DataTable

            objCCustomTran._AnalysisUnkid = mintAssessAnalysisUnkid
            objCCustomTran._EmployeeId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue))
            objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))

            mdtCustomEvaluation = objCCustomTran._DataTable

            If menAction = enAction.EDIT_ONE Then
                Call Fill_BSC_Evaluation()
                Call Fill_GE_Evaluation()
                Call Fill_Custom_Grid()
                Call BtnSearch_Click(sender, e)
            Else
                objpnlBSC.Visible = False
                objpnlGE.Visible = False
                objpnlCItems.Visible = False
            End If
            dtpAssessdate.SetDate = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub
#End Region

#Region "GridView Event"

    Protected Sub dgvGE_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvGE.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim intCount As Integer = 1
                If CBool(DataBinder.Eval(e.Item.DataItem, "IsPGrp")) = True Then
                    For i = 1 To dgvGE.Columns.Count - 1
                        If dgvGE.Columns(i).Visible = True Then
                            intCount += 1
                            e.Item.Cells(i).Visible = False
                        End If
                    Next
                    e.Item.Cells(disGEColumn("dgcolheval_itemGE")).ColumnSpan = intCount
                    e.Item.Cells(disGEColumn("dgcolheval_itemGE")).CssClass = "MainGroupHeaderStyle"
                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then
                    intCount = 1
                    For i = 1 To dgvGE.Columns.Count - 2
                        If dgvGE.Columns(i).Visible = True Then
                            intCount += 1
                            e.Item.Cells(i).Visible = False
                        End If
                    Next
                    e.Item.Cells(disGEColumn("dgcolheval_itemGE")).ColumnSpan = intCount
                    e.Item.Cells(disGEColumn("dgcolheval_itemGE")).CssClass = "GroupHeaderStylecomp"
                    e.Item.Cells(disGEColumn("objdgcolhInformation")).CssClass = "GroupHeaderStylecomp"
                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then
                    e.Item.Cells(disGEColumn("dgcolheval_itemGE")).Text = "&nbsp;" & e.Item.Cells(disGEColumn("dgcolheval_itemGE")).Text
                End If

                If CInt(e.Item.Cells(disGEColumn("objdgcolhcompetenciesunkidGE")).Text) > 0 Then
                    'S.SANDEEP |18-JAN-2020| -- START
                    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                    'If mdtGE_Evaluation.Rows.Count > 0 Then
                    '    Dim dtmp() As DataRow = Nothing
                    '    dtmp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(e.Item.Cells(disGEColumn("objdgcolhassessgroupunkidGE")).Text) & "' AND competenciesunkid = '" & CInt(e.Item.Cells(disGEColumn("objdgcolhcompetenciesunkidGE")).Text) & "' AND AUD <> 'D' ")
                    '    If dtmp.Length > 0 Then
                    '        CType(e.Item.Cells(disGEColumn("dgcolhaselfGE")).Controls(1), TextBox).Text = CInt(dtmp(0).Item("result"))
                    '        CType(e.Item.Cells(disGEColumn("dgcolharemarkGE")).Controls(1), TextBox).Text = dtmp(0).Item("remark")
                    '        CType(e.Item.Cells(disGEColumn("dgcolhaAgreedScoreGE")).FindControl("dgcolhaAgreedScoreGE"), TextBox).Text = dtmp(0).Item("agreed_score")

                    '        'If CBool(Session("ConsiderItemWeightAsNumber")) = True Then
                    '        '    Dim iWgt As Decimal = 0 : Decimal.TryParse(e.Item.Cells(disGEolumn("dgcolhGEWeight")).Text, iWgt)
                    '        '    If iWgt <= 0 Then iWgt = 1
                    '        '    e.Item.Cells(disGEolumn("objdgcolhadisplayGE")).Text = iWgt * CInt(dtmp(0).Item("result"))
                    '        'Else
                    '        '    e.Item.Cells(disGEolumn("objdgcolhadisplayGE")).Text = 1 * CInt(dtmp(0).Item("result"))
                    '        '    dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                    '        'End If
                    '    End If
                    'End If
                    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
                        Dim dtmp() As DataRow = Nothing
                        dtmp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(e.Item.Cells(disGEColumn("objdgcolhassessgroupunkidGE")).Text) & "' AND competenciesunkid = '" & CInt(e.Item.Cells(disGEColumn("objdgcolhcompetenciesunkidGE")).Text) & "' AND AUD <> 'D' ")
                        If dtmp IsNot Nothing AndAlso dtmp.Length > 0 Then
                            CType(e.Item.Cells(disGEColumn("dgcolhaselfGE")).Controls(1), TextBox).Text = CInt(dtmp(0).Item("result"))
                            CType(e.Item.Cells(disGEColumn("dgcolharemarkGE")).Controls(1), TextBox).Text = dtmp(0).Item("remark")
                            CType(e.Item.Cells(disGEColumn("dgcolhaAgreedScoreGE")).FindControl("dgcolhaAgreedScoreGE"), TextBox).Text = dtmp(0).Item("agreed_score")
                        End If
                    End If
                    'S.SANDEEP |18-JAN-2020| -- END
                End If
                If IsNumeric(e.Item.Cells(disGEColumn("dgcolhGEWeight")).Text) Then
                    iWeightTotal = iWeightTotal + CDec(e.Item.Cells(disGEColumn("dgcolhGEWeight")).Text)
                End If

                'S.SANDEEP |20-SEP-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#0004155}
                Dim hf As HiddenField = CType(e.Item.Cells(2).FindControl("dgcolhGEScoreId"), HiddenField)
                If hf.Value > 0 Then
                    Dim lk As LinkButton = CType(e.Item.Cells(2).FindControl("dgcolhGEScore"), LinkButton)
                    Dim dsScore_Guide As New DataSet
                    Dim objScaleMaster As New clsAssessment_Scale
                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), hf.Value)
                    objScaleMaster = Nothing
                    Dim strValue As String = String.Join(vbCrLf, dsScore_Guide.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of String)("scale").ToString() & " - " & x.Field(Of String)("description")).ToArray())
                    lk.ToolTip = strValue
                End If
                'S.SANDEEP |20-SEP-2019| -- END

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)

        Finally
        End Try
    End Sub

    Protected Sub dgvBSC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvBSC.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Call SetDateFormat()
                If CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then
                    For i = xVal To dgvBSC.Columns.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next
                    e.Item.Cells(xVal - 1).ColumnSpan = dgvBSC.Columns.Count - 1
                    e.Item.Cells(xVal - 1).CssClass = "GroupHeaderStyleBorderLeft"
                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then
                    e.Item.Cells(xVal - 1).Text = "&nbsp;" & e.Item.Cells(0).Text

                    If e.Item.Cells(disBSCColumn("dgcolhSDate")).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(disBSCColumn("dgcolhSDate")).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(disBSCColumn("dgcolhSDate")).Text = CDate(e.Item.Cells(disBSCColumn("dgcolhSDate")).Text).Date.ToShortDateString
                    End If

                    If e.Item.Cells(disBSCColumn("dgcolhEDate")).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(disBSCColumn("dgcolhEDate")).Text.Trim <> "&nbsp;" Then
                        e.Item.Cells(disBSCColumn("dgcolhEDate")).Text = CDate(e.Item.Cells(disBSCColumn("dgcolhEDate")).Text).Date.ToShortDateString
                    End If
                End If
                If CStr(DataBinder.Eval(e.Item.DataItem, "Weight")) = "" Then
                    CType(e.Item.Cells(disBSCColumn("dgcolhaselfBSC")).FindControl("dgcolhaselfBSC"), TextBox).Visible = False
                    CType(e.Item.Cells(disBSCColumn("dgcolharemarkBSC")).FindControl("dgcolharemarkBSC"), TextBox).Visible = False
                    CType(e.Item.Cells(disBSCColumn("dgcolharemarkBSC")).FindControl("dgcolharemarkBSC"), TextBox).Visible = False
                ElseIf CStr(DataBinder.Eval(e.Item.DataItem, "Weight")) <> "" Then
                    'S.SANDEEP |18-JAN-2020| -- START
                    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                    'If mdtBSC_Evaluation.Rows.Count > 0 Then
                    '    Dim dtmp() As DataRow = Nothing
                    '    Select Case iExOrdr
                    '        Case enWeight_Types.WEIGHT_FIELD1
                    '            dtmp = mdtBSC_Evaluation.Select("empfield1unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield1unkid").ToString & "' AND AUD <> 'D' ")
                    '        Case enWeight_Types.WEIGHT_FIELD2
                    '            dtmp = mdtBSC_Evaluation.Select("empfield2unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield2unkid").ToString & "' AND AUD <> 'D' ")
                    '        Case enWeight_Types.WEIGHT_FIELD3
                    '            dtmp = mdtBSC_Evaluation.Select("empfield3unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield3unkid").ToString & "' AND AUD <> 'D' ")
                    '        Case enWeight_Types.WEIGHT_FIELD4
                    '            dtmp = mdtBSC_Evaluation.Select("empfield4unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield4unkid").ToString & "' AND AUD <> 'D' ")
                    '        Case enWeight_Types.WEIGHT_FIELD5
                    '            dtmp = mdtBSC_Evaluation.Select("empfield5unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield5unkid").ToString & "' AND AUD <> 'D' ")
                    '    End Select

                    '    If dtmp.Length > 0 Then
                    '        CType(e.Item.Cells(disBSCColumn("dgcolhaselfBSC")).FindControl("dgcolhaselfBSC"), TextBox).Text = CDec(dtmp(0).Item("result"))
                    '        CType(e.Item.Cells(disBSCColumn("dgcolharemarkBSC")).FindControl("dgcolharemarkBSC"), TextBox).Text = dtmp(0).Item("remark")
                    '        CType(e.Item.Cells(disBSCColumn("dgcolhAgreedScoreBSC")).FindControl("dgcolhAgreedScoreBSC"), TextBox).Text = dtmp(0).Item("agreed_score")
                    '    End If
                    'End If
                    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
                        Dim dtmp() As DataRow = Nothing
                        Select Case iExOrdr
                            Case enWeight_Types.WEIGHT_FIELD1
                                dtmp = mdtBSC_Evaluation.Select("empfield1unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield1unkid").ToString & "' AND AUD <> 'D' ")
                            Case enWeight_Types.WEIGHT_FIELD2
                                dtmp = mdtBSC_Evaluation.Select("empfield2unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield2unkid").ToString & "' AND AUD <> 'D' ")
                            Case enWeight_Types.WEIGHT_FIELD3
                                dtmp = mdtBSC_Evaluation.Select("empfield3unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield3unkid").ToString & "' AND AUD <> 'D' ")
                            Case enWeight_Types.WEIGHT_FIELD4
                                dtmp = mdtBSC_Evaluation.Select("empfield4unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield4unkid").ToString & "' AND AUD <> 'D' ")
                            Case enWeight_Types.WEIGHT_FIELD5
                                dtmp = mdtBSC_Evaluation.Select("empfield5unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield5unkid").ToString & "' AND AUD <> 'D' ")
                        End Select

                        If dtmp IsNot Nothing AndAlso dtmp.Length > 0 Then
                            CType(e.Item.Cells(disBSCColumn("dgcolhaselfBSC")).FindControl("dgcolhaselfBSC"), TextBox).Text = CDec(dtmp(0).Item("result"))
                            CType(e.Item.Cells(disBSCColumn("dgcolharemarkBSC")).FindControl("dgcolharemarkBSC"), TextBox).Text = dtmp(0).Item("remark")
                            CType(e.Item.Cells(disBSCColumn("dgcolhAgreedScoreBSC")).FindControl("dgcolhAgreedScoreBSC"), TextBox).Text = dtmp(0).Item("agreed_score")
                        End If
                    End If
                    'S.SANDEEP |18-JAN-2020| -- END
                End If
                If IsNumeric(e.Item.Cells(disBSCColumn("dgcolhBSCWeight")).Text) Then
                    iWeightTotal = iWeightTotal + CDec(e.Item.Cells(disBSCColumn("dgcolhBSCWeight")).Text)
                End If
                If CBool(Session("DontAllowToEditScoreGenbySys")) Then
                    CType(e.Item.Cells(disBSCColumn("dgcolhaselfBSC")).FindControl("dgcolhaselfBSC"), TextBox).ReadOnly = True
                    CType(e.Item.Cells(disBSCColumn("dgcolhAgreedScoreBSC")).FindControl("dgcolhAgreedScoreBSC"), TextBox).ReadOnly = True
                End If
                'S.SANDEEP |20-SEP-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#0004155}
                Dim hf As HiddenField = CType(e.Item.Cells(14).FindControl("dgcolhBSCScoreId"), HiddenField)
                If hf.Value > 0 Then
                    Dim lk As LinkButton = CType(e.Item.Cells(14).FindControl("dgcolhBSCScore"), LinkButton)
                    Dim dsScore_Guide As New DataSet
                    Dim objScaleMaster As New clsAssessment_Scale
                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), hf.Value)
                    objScaleMaster = Nothing
                    Dim strValue As String = String.Join(vbCrLf, dsScore_Guide.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of String)("scale").ToString() & " - " & x.Field(Of String)("description")).ToArray())
                    lk.ToolTip = strValue
                End If
                'S.SANDEEP |20-SEP-2019| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub dgvGE_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvGE.ItemCommand
        Try
            If menAction = enAction.EDIT_ONE Then
                Dim dtmp() As DataRow = Nothing
                dtmp = mdtGE_Evaluation.Select("assessgroupunkid = '" & dtGE_TabularGrid.Rows(e.Item.ItemIndex).Item("assessgroupunkid").ToString & "' AND competenciesunkid = '" & dtGE_TabularGrid.Rows(e.Item.ItemIndex).Item("competenciesunkid").ToString & "' AND AUD <> 'D' ")
                If dtmp.Length > 0 Then
                    If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then


                        e.Item.Cells(disGEColumn("dgcolharemarkGE")).Text = 1 * CInt(e.Item.Cells(disGEColumn("objdgcolhedisplayGE")).Text)
                        dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))

                        e.Item.Cells(disGEColumn("dgcolharemarkGE")).Text = 1 * CInt(dtmp(0).Item("result"))
                        dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
                    End If
                End If
            ElseIf menAction <> enAction.EDIT_ONE Then
                If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
                    e.Item.Cells(disGEColumn("dgcolhaselfGE")).Text = 0
                    e.Item.Cells(disGEColumn("dgcolharemarkGE")).Text = ""
                    e.Item.Cells(disGEColumn("dgcolhaAgreedScoreGE")).Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub dgvItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvItems.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim imgAdd As New ImageButton()
                imgAdd.ID = "imgAdd"
                imgAdd.Attributes.Add("Class", "objAddBtn")
                imgAdd.CommandName = "objAdd"
                imgAdd.ImageUrl = "~/images/add_16.png"
                imgAdd.ToolTip = "New"
                AddHandler imgAdd.Click, AddressOf imgAdd_Click
                e.Row.Cells(0).Controls.Add(imgAdd)

                Dim imgEdit As New ImageButton()
                imgEdit.ID = "imgEdit"
                imgEdit.Attributes.Add("Class", "objAddBtn")
                imgEdit.CommandName = "objEdit"
                imgEdit.ImageUrl = "~/images/Edit.png"
                imgEdit.ToolTip = "Edit"
                AddHandler imgEdit.Click, AddressOf imgEdit_Click
                e.Row.Cells(1).Controls.Add(imgEdit)


                Dim imgDelete As New ImageButton()
                imgDelete.ID = "imgDelete"
                imgDelete.Attributes.Add("Class", "objAddBtn")
                imgDelete.CommandName = "objDelete"
                imgDelete.ImageUrl = "~/images/remove.png"
                imgDelete.ToolTip = "Delete"
                AddHandler imgDelete.Click, AddressOf imgDelete_Click
                e.Row.Cells(2).Controls.Add(imgDelete)
                'S.SANDEEP |18-JAN-2020| -- START
                'ISSUE/ENHANCEMENT : PA-OPTIMIZATION

                'If dtCustomTabularGrid.Rows(e.Row.RowIndex)("analysisunkid") <= 0 AndAlso mintAssessAnalysisUnkid <= 0 Then
                'ElseIf dtCustomTabularGrid.Rows(e.Row.RowIndex)("analysisunkid") <> mintAssessAnalysisUnkid Then
                '    e.Row.Cells(2).Controls.Remove(imgDelete)
                '    e.Row.Cells(1).Controls.Remove(imgEdit)
                '    'S.SANDEEP |25-MAR-2019| -- START
                '    'Dim objAnalysis As New clsevaluation_analysis_master
                '    'objAnalysis._Analysisunkid = dtCustomTabularGrid.Rows(e.Row.RowIndex)("analysisunkid")
                '    'If menAssess < objAnalysis._Assessmodeid Then
                '    '    e.Row.Cells(1).Controls.Remove(imgEdit)
                '    'End If
                '    'objAnalysis = Nothing
                '    'S.SANDEEP |25-MAR-2019| -- END
                'End If
                ''S.SANDEEP |25-MAR-2019| -- START

                If dtCustomTabularGrid IsNot Nothing AndAlso dtCustomTabularGrid.Rows(e.Row.RowIndex)("analysisunkid") <= 0 AndAlso mintAssessAnalysisUnkid <= 0 Then
                ElseIf dtCustomTabularGrid IsNot Nothing AndAlso dtCustomTabularGrid.Rows(e.Row.RowIndex)("analysisunkid") <> mintAssessAnalysisUnkid Then
                    e.Row.Cells(2).Controls.Remove(imgDelete)
                        e.Row.Cells(1).Controls.Remove(imgEdit)
                    End If
                'S.SANDEEP |18-JAN-2020| -- END

                If Session("CompanyGroupName") = "NMB PLC" Then
                    If dtCustomTabularGrid IsNot Nothing AndAlso dtCustomTabularGrid.Rows(e.Row.RowIndex)("viewmodeid") <> 2 Then
                        e.Row.Cells(0).Controls.Remove(imgAdd)
                        e.Row.Cells(1).Controls.Remove(imgEdit)
                        e.Row.Cells(2).Controls.Remove(imgDelete)
                End If
            End If
                'S.SANDEEP |25-MAR-2019| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Protected Sub link_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            txtData.Text = ""
            Dim lnkWhatIsThis As LinkButton = CType(sender, LinkButton)
            Dim xRow As DataGridItem = CType(lnkWhatIsThis.NamingContainer, DataGridItem)
            If CBool(xRow.Cells(disGEColumn("objdgcolhIsGrpGE")).Text) = True Then
                Dim objCOMaster As New clsCommon_Master
                objCOMaster._Masterunkid = CInt(xRow.Cells(disGEColumn("objdgcolhGrpIdGE")).Text)
                If objCOMaster._Description <> "" Then
                    txtData.Text = objCOMaster._Description
                    popup_ComInfo.Show()
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
                End If
                objCOMaster = Nothing
            ElseIf CBool(xRow.Cells(disGEColumn("objdgcolhIsGrpGE")).Text) = False Then
                Dim objCPMsater As New clsassess_competencies_master
                objCPMsater._Competenciesunkid = CInt(xRow.Cells(disGEColumn("objdgcolhcompetenciesunkidGE")).Text)
                If objCPMsater._Description <> "" Then
                    txtData.Text = objCPMsater._Description
                    popup_ComInfo.Show()
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
                End If
                objCPMsater = Nothing
            End If
            dgvGE.DataSource = dtGE_TabularGrid
            dgvGE.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

#End Region

#Region "Control Event"

    Protected Sub radOption_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOption.SelectedIndexChanged
        Try
            If radOption.SelectedValue = "2" Then
                cboAssessor.DataSource = Nothing
                Dim objExtAssessor As New clsexternal_assessor_master
                Dim dsCombos As DataSet = objExtAssessor.GetDisplayNameComboList("Assessor", False)

                With cboAssessor
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .DataBind()
                    .SelectedIndex = 0
                End With
                Call cboAssessor_SelectedIndexChanged(cboAssessor, Nothing)
            ElseIf radOption.SelectedValue = "1" Then
                cboAssessor.DataSource = Nothing
                Dim dsCombos As DataSet = objEAnalysisMst.getAssessorComboList(Session("Database_Name"), _
                                                                               Session("UserId"), _
                                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                                               Session("IsIncludeInactiveEmp"), _
                                                                              "Assessor", clsAssessor.enARVisibilityTypeId.VISIBLE, False)
                'Shani (12-Jan-2017) -- [clsAssessor.enARVisibilityTypeId.VISIBLE]
                With cboAssessor
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsCombos.Tables("Assessor")
                    .DataBind()
                    .SelectedIndex = 0
                End With
                Call cboAssessor_SelectedIndexChanged(cboAssessor, Nothing)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboAssessor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAssessor.SelectedIndexChanged
        Try
            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
            If CInt(cboAssessor.SelectedValue) > 0 Then
                Dim dsList As New DataSet
                If radOption.SelectedValue = "1" Then
                    dsList = objEAnalysisMst.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                                      Session("UserId"), _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                                      Session("IsIncludeInactiveEmp"), _
                                                                      CInt(cboAssessor.SelectedValue), "AEmp", True)

                ElseIf radOption.SelectedValue = "2" Then
                    Dim objExtAssessor As New clsexternal_assessor_master
                    dsList = objExtAssessor.GetEmpBasedOnExtAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)
                End If
                With cboEmployee
                    .DataValueField = "Id"
                    .DataTextField = "Name"
                    .DataSource = dsList.Tables("AEmp")
                    .DataBind()
                    .SelectedValue = 0
                End With
            Else
                cboEmployee.DataSource = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
        Try
            Dim objPrd As New clscommom_period_Tran
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            mintYearUnkid = objPrd._Yearunkid
            objPrd = Nothing
            Dim objMapping As New clsAssess_Field_Mapping
            iLinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            iMappingUnkid = objMapping.Get_MappingUnkId(CInt(cboPeriod.SelectedValue))
            objMapping._Mappingunkid = iMappingUnkid
            xTotAssignedWeight = objMapping._Weight
            objMapping = Nothing

            Dim objFMaster As New clsAssess_Field_Master
            iExOrdr = objFMaster.Get_Field_ExOrder(iLinkedFieldId, True)
            objFMaster = Nothing

            objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)
            objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
            mdtCustomEvaluation = objCCustomTran._DataTable
            If CInt(cboPeriod.SelectedValue) > 0 Then
                If CStr(Session("Perf_EvaluationOrder")).Trim.Length > 0 Then
                    Dim objCHeader As New clsassess_custom_header
                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                    dsHeaders.Tables(0).Rows.Clear()
                    Dim xRow As DataRow = Nothing
                    Dim iOrdr() As String = CStr(Session("Perf_EvaluationOrder")).Split("|")
                    If iOrdr.Length > 0 Then
                        Select Case CInt(iOrdr(0))
                            Case enEvaluationOrder.PE_BSC_SECTION
                                xRow = dsHeaders.Tables(0).NewRow
                                xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                                xRow = dsHeaders.Tables(0).NewRow
                                xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                            Case enEvaluationOrder.PE_CUSTOM_SECTION
                                dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                        End Select
                        Select Case CInt(iOrdr(1))
                            Case enEvaluationOrder.PE_BSC_SECTION
                                xRow = dsHeaders.Tables(0).NewRow
                                xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                                xRow = dsHeaders.Tables(0).NewRow
                                xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_CUSTOM_SECTION
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    Dim dsList As New DataSet
                                    dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                    dsHeaders.Tables(0).Merge(dsList.Tables(0), True)
                                Else
                                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                End If
                        End Select
                        Select Case CInt(iOrdr(2))
                            Case enEvaluationOrder.PE_BSC_SECTION
                                xRow = dsHeaders.Tables(0).NewRow
                                xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
                                xRow = dsHeaders.Tables(0).NewRow
                                xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
                                Else
                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
                                End If
                            Case enEvaluationOrder.PE_CUSTOM_SECTION
                                If dsHeaders.Tables(0).Rows.Count > 0 Then
                                    Dim dsList As New DataSet
                                    dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                    dsHeaders.Tables(0).Merge(dsList.Tables(0), True)
                                Else
                                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
                                End If
                        End Select
                    End If
                    objCHeader = Nothing
                End If
            Else
                If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables.Count > 0 Then
                    dsHeaders.Tables(0).Rows.Clear()
                End If
            End If

            'S.SANDEEP |12-FEB-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            BtnSearch_Click(New Object(), New EventArgs())
            'S.SANDEEP |12-FEB-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Protected Sub dgv_Citems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgv_Citems.ItemDataBound
        Try
            dtCItems = Me.Session("dtCItems")
            If e.Item.ItemIndex > -1 Then
                If CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.FREE_TEXT Then
                    Dim txt As New TextBox
                    txt.ID = "txt" & e.Item.Cells(4).Text
                    txt.TextMode = TextBoxMode.MultiLine
                    'S.SANDEEP [25-JAN-2017] -- START
                    'ISSUE/ENHANCEMENT : ISSUE RELATED TO HEIGHT OF TEXTBOX {AKF Self-services In PA Custom Items}
                    txt.Rows = 7
                    'S.SANDEEP [25-JAN-2017] -- END
                    txt.Style.Add("resize", "none")
                    txt.Width = Unit.Percentage(100)
                    txt.CssClass = "removeTextcss"
                    If CBool(e.Item.Cells(3).Text) Then
                        txt.ReadOnly = True
                    End If
                    If mblnIsMatchCompetencyStructure Then
                        txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
                    ElseIf CInt(Me.ViewState("RowIndex")) > -1 Then
                        txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
                    End If
                    If CBool(e.Item.Cells(6).Text) Then
                        txt.ReadOnly = True
                    End If

                    e.Item.Cells(1).Controls.Add(txt)
                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                    Dim dtp As Control
                    dtp = LoadControl("~/Controls/DateCtrl.ascx")
                    dtp.ID = "dtp" & e.Item.Cells(4).Text
                    CType(dtp, Controls_DateCtrl).AutoPostBack = True
                    If CBool(e.Item.Cells(3).Text) Then
                        CType(dtp, Controls_DateCtrl).Enabled = False
                    End If
                    If CInt(Me.ViewState("RowIndex")) > -1 Then
                        If dtCItems.Rows(e.Item.ItemIndex)("custom_value").ToString().Trim.Length > 0 Then
                            CType(dtp, Controls_DateCtrl).SetDate = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
                        End If

                    End If
                    AddHandler CType(dtp, Controls_DateCtrl).TextChanged, AddressOf dtpCustomItem_TextChanged
                    e.Item.Cells(1).Controls.Add(dtp)
                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.SELECTION Then
                    Dim cbo As New DropDownList
                    cbo.ID = "cbo" & e.Item.Cells(4).Text
                    cbo.Width = Unit.Pixel(250)
                    If CBool(e.Item.Cells(3).Text) Then
                        cbo.Enabled = False
                    End If
                    Select Case CInt(e.Item.Cells(5).Text)
                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
                            'S.SANDEEP |08-JAN-2019| -- START
                            'Dim objCMaster As New clsCommon_Master
                            'Dim dsList As New DataSet
                            'dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                            'With cbo
                            '    .DataValueField = "masterunkid"
                            '    .DataTextField = "name"
                            '    .DataSource = dsList.Tables(0)
                            '    .ToolTip = "name"
                            '    .SelectedValue = 0
                            '    .DataBind()
                            'End With
                            'objCMaster = Nothing

                            Dim dsList As New DataSet
                            If CBool(IIf(e.Item.Cells(7).Text = "&nbsp;", 0, e.Item.Cells(7).Text)) = False Then
                                'If CBool(e.Item.Cells(7).Text) = False Then
                            Dim objCMaster As New clsCommon_Master
                            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                objCMaster = Nothing
                            Else
                                Dim objEvalCItem As New clsevaluation_analysis_master
                                dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(CInt(cboEmployee.SelectedValue))
                                objEvalCItem = Nothing
                            End If

                            With cbo
                                .DataValueField = "masterunkid"
                                .DataTextField = "name"
                                .DataSource = dsList.Tables(0)
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With
                            'S.SANDEEP |08-JAN-2019| -- END


                            'S.SANDEEP [06-NOV-2017] -- START
                        Case clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES

                            'S.SANDEEP |08-JAN-2019| -- START
                            'Dim dtab As DataTable = Nothing
                            'Dim objCMaster As New clsCommon_Master
                            'Dim dsList As New DataSet
                            'dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")

                            'If CInt(e.Item.Cells(5).Text) = clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES Then
                            '    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,1)", "", DataViewRowState.CurrentRows).ToTable
                            'ElseIf CInt(e.Item.Cells(5).Text) = clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                            '    dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,2)", "", DataViewRowState.CurrentRows).ToTable
                            'End If
                            'With cbo
                            '    .DataValueField = "masterunkid"
                            '    .DataTextField = "name"
                            '    .DataSource = dtab
                            '    .ToolTip = "name"
                            '    .SelectedValue = 0
                            '    .DataBind()
                            'End With
                            'objCMaster = Nothing

                            Dim dtab As DataTable = Nothing
                            Dim dsList As New DataSet
                            If CBool(IIf(e.Item.Cells(7).Text = "&nbsp;", 0, e.Item.Cells(7).Text)) = False Then
                                'If CBool(e.Item.Cells(7).Text) = False Then
                            Dim objCMaster As New clsCommon_Master
                            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
                                objCMaster = Nothing
                            Else
                                Dim objEvalCItem As New clsevaluation_analysis_master
                                dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(CInt(cboEmployee.SelectedValue))
                                objEvalCItem = Nothing
                            End If

                            If CInt(e.Item.Cells(5).Text) = clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES Then
                                dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,1)", "", DataViewRowState.CurrentRows).ToTable
                            ElseIf CInt(e.Item.Cells(5).Text) = clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                                dtab = New DataView(dsList.Tables(0), "coursetypeid IN (99,2)", "", DataViewRowState.CurrentRows).ToTable
                            End If
                            With cbo
                                .DataValueField = "masterunkid"
                                .DataTextField = "name"
                                .DataSource = dtab
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With
                            'S.SANDEEP |08-JAN-2019| -- END

                            'S.SANDEEP [06-NOV-2017] -- END

                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
                            Dim objCompetency As New clsassess_competencies_master
                            Dim dsList As New DataSet
                            dsList = objCompetency.getAssigned_Competencies_List(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")), True)
                            With cbo
                                .DataValueField = "Id"
                                .DataTextField = "Name"
                                .DataSource = dsList.Tables(0)
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With
                            objCompetency = Nothing
                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
                            Dim objEmpField1 As New clsassess_empfield1_master
                            Dim dsList As New DataSet
                            dsList = objEmpField1.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", True, True)
                            With cbo
                                .DataValueField = "Id"
                                .DataTextField = "Name"
                                .DataSource = dsList.Tables(0)
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With
                            objEmpField1 = Nothing


                            'S.SANDEEP |16-AUG-2019| -- START
                            'ISSUE/ENHANCEMENT : {ARUTI-877|Ref#0003997}
                        Case clsassess_custom_items.enSelectionMode.PERFORMANCE_CUSTOM_ITEM
                            Dim dsList As New DataSet
                            If CBool(IIf(e.Item.Cells(7).Text = "&nbsp;", 0, e.Item.Cells(7).Text)) = False Then
                                Dim objCMaster As New clsCommon_Master
                                dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.PERFORMANCE_CUSTOM_ITEM, True, "List")
                                objCMaster = Nothing
                            Else
                                Dim objEvalCItem As New clsevaluation_analysis_master
                                dsList = objEvalCItem.GetCompletedTrainingListForCustomItem(CInt(cboEmployee.SelectedValue))
                                objEvalCItem = Nothing
                            End If

                            With cbo
                                .DataValueField = "masterunkid"
                                .DataTextField = "name"
                                .DataSource = dsList.Tables(0)
                                .ToolTip = "name"
                                .SelectedValue = 0
                                .DataBind()
                            End With
                            'S.SANDEEP |16-AUG-2019| -- END

                    End Select
                    If CInt(Me.ViewState("RowIndex")) > -1 Then
                        cbo.SelectedValue = IIf(dtCItems.Rows(e.Item.ItemIndex)("custom_value") = "", 0, dtCItems.Rows(e.Item.ItemIndex)("custom_value"))
                    End If
                    e.Item.Cells(1).Controls.Add(cbo)
                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.NUMERIC_DATA Then
                    Dim txt As New TextBox
                    txt.ID = "txtnum" & e.Item.Cells(4).Text
                    If CBool(e.Item.Cells(3).Text) Then
                        txt.ReadOnly = True
                    End If
                    txt.Style.Add("text-align", "right")
                    txt.Attributes.Add("onKeypress", "return onlyNumbers(this, event);")
                    txt.Width = Unit.Percentage(100)
                    txt.CssClass = "removeTextcss"
                    If CInt(Me.ViewState("RowIndex")) > -1 Then
                        txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
                    End If
                    e.Item.Cells(1).Controls.Add(txt)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub TxtValRemarkGE_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim bln As Boolean = False
        Try
            Dim xRow As DataGridItem = CType(CType(sender, TextBox).NamingContainer, DataGridItem)

            Dim iDecWgt As Decimal = 0
            Decimal.TryParse(xRow.Cells(disGEColumn("dgcolhGEWeight")).Text, iDecWgt)
            mdecItemWeight = iDecWgt
            Me.ViewState("mdecItemWeight") = mdecItemWeight

            Dim txt As TextBox = CType(sender, TextBox)
            If txt.ID = "dgcolharemarkGE" Then
                Dim iResult As Decimal = 0 : Dim iAgreedScore As Decimal = 0
                Decimal.TryParse(CType(xRow.Cells(disGEColumn("dgcolhaselfGE")).Controls(1), TextBox).Text, iResult)
                Decimal.TryParse(CType(xRow.Cells(disGEColumn("dgcolhaAgreedScoreGE")).FindControl("dgcolhaAgreedScoreGE"), TextBox).Text, iAgreedScore)
                Call Evaluated_Data_GE(iResult, txt.Text, xRow.ItemIndex, xRow, iAgreedScore, True)
            ElseIf txt.ID = "dgcolhaselfGE" OrElse txt.ID = "dgcolhaAgreedScoreGE" Then
                Dim iDecVal As Decimal
                Decimal.TryParse(txt.Text, iDecVal)
                If IsNumeric(iDecVal) Then
                    Select Case CInt(Session("ScoringOptionId"))
                        Case enScoringOption.SC_WEIGHTED_BASED
                            If CDec(iDecVal) > iDecWgt Then
                                'S.SANDEEP |08-JAN-2019| -- START
                                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), Me)
                                'bln = True
                                'txt.Text = "" : Exit Sub
                                If CBool(Session("EnableBSCAutomaticRating")) = False Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), Me)
                                bln = True
                                txt.Text = "" : Exit Sub
                                End If
                                'S.SANDEEP |08-JAN-2019| -- END
                            Else
                                GoTo iValid
                            End If
                        Case enScoringOption.SC_SCALE_BASED
                            Decimal.TryParse(txt.Text, iDecVal)
                            If IsNumeric(iDecVal) Then
                                If CInt(xRow.Cells(disGEColumn("objdgcolhscalemasterunkidGE")).Text) > 0 Then    'SCALE MASTER ID
                                    Dim dsScore_Guide As New DataSet
                                    Dim objScaleMaster As New clsAssessment_Scale
                                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(xRow.Cells(disGEColumn("objdgcolhscalemasterunkidGE")).Text))
                                    objScaleMaster = Nothing

                                    mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
                                    Me.ViewState("mdecMaxScale") = mdecMaxScale
                                    Dim dtmp() As DataRow = dsScore_Guide.Tables(0).Select("scale = " & CDec(iDecVal))

                                    If dtmp.Length <= 0 Then
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Sorry, particular scale is not defined. Please refer score guide for the list of score(s) defined."), Me)
                                        Dim dTemp() As DataRow = Nothing
                                        dTemp = GetOldValue_GE(xRow)
                                        If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
                                            txt.Text = dTemp(0).Item("result")
                                        Else
                                            txt.Text = "" : txt.Focus()
                                        End If
                                        bln = True
                                        Exit Sub
                                    Else
                                        GoTo iValid
                                    End If
                                Else
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Sorry, no scale is not defined. Please define scale."), Me)
                                    Dim dTemp() As DataRow = Nothing
                                    dTemp = GetOldValue_GE(xRow)
                                    If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
                                        txt.Text = dTemp(0).Item("result")
                                    Else
                                        txt.Text = "" : txt.Focus()
                                    End If
                                    bln = True
                                    Exit Sub
                                End If
                            Else
                                Exit Sub
                            End If
                    End Select
iValid:             If Validation() = False Then bln = True : Exit Sub
                    If Is_Already_Assessed() = False Then
                        bln = True
                        Exit Sub
                    End If
                    If txt.ID = "dgcolhaAgreedScoreGE" Then
                        Dim idecScore As Decimal = 0 : Decimal.TryParse(CType(xRow.Cells(disGEColumn("dgcolhaselfGE")).Controls(1), TextBox).Text, idecScore)
                        Call Evaluated_Data_GE(idecScore, CType(xRow.Cells(disGEColumn("dgcolharemarkGE")).Controls(1), TextBox).Text, xRow.ItemIndex, xRow, iDecVal)
                    Else
                        Dim idecScore As Decimal = 0 : Decimal.TryParse(CType(xRow.Cells(disGEColumn("dgcolhaAgreedScoreGE")).FindControl("dgcolhaAgreedScoreGE"), TextBox).Text, idecScore)
                        Call Evaluated_Data_GE(iDecVal, CType(xRow.Cells(disGEColumn("dgcolharemarkGE")).Controls(1), TextBox).Text, xRow.ItemIndex, xRow, idecScore)
                    End If
                Else
                    txt.Text = ""
                End If
            End If
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & objpnlGE.ClientID & ");", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Protected Sub TxtValRemarkBSC_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim bln As Boolean = False
        Try
            Dim xRow As DataGridItem = CType(CType(sender, TextBox).NamingContainer, DataGridItem)

            Dim iDecWgt As Decimal = 0
            Decimal.TryParse(xRow.Cells(disBSCColumn("dgcolhBSCWeight")).Text, iDecWgt)
            mdecItemWeight = iDecWgt
            Me.ViewState("mdecItemWeight") = mdecItemWeight
            Dim txt As TextBox = CType(sender, TextBox)

            If txt.ID = "dgcolharemarkBSC" Then
                Dim iResult As Decimal = 0 : Dim iAgreedScore As Decimal = 0
                Decimal.TryParse(CType(xRow.Cells(disBSCColumn("dgcolhaselfBSC")).Controls(1), TextBox).Text, iResult)
                Decimal.TryParse(CType(xRow.Cells(disBSCColumn("dgcolhAgreedScoreBSC")).FindControl("dgcolhAgreedScoreBSC"), TextBox).Text, iAgreedScore)

                Call Evaluated_Data_BSC(iResult, txt.Text, xRow.ItemIndex, iAgreedScore, True)
            ElseIf txt.ID = "dgcolhaselfBSC" OrElse txt.ID = "dgcolhAgreedScoreBSC" Then
                Dim iDecVal As Decimal
                Decimal.TryParse(txt.Text, iDecVal)
                If IsNumeric(iDecVal) Then
                    Select Case CInt(Session("ScoringOptionId"))
                        Case enScoringOption.SC_WEIGHTED_BASED
                            If CDec(iDecVal) > iDecWgt Then
                                'S.SANDEEP |08-JAN-2019| -- START
                                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), Me)
                                'bln = True
                                'txt.Text = "" : Exit Sub
                                If CBool(Session("EnableBSCAutomaticRating")) = False Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), Me)
                                bln = True
                                txt.Text = "" : Exit Sub
                                End If
                                'S.SANDEEP |08-JAN-2019| -- END
                            Else
                                GoTo iValid
                            End If
                        Case enScoringOption.SC_SCALE_BASED
                            Decimal.TryParse(txt.Text, iDecVal)
                            If IsNumeric(iDecVal) Then
                                If CInt(xRow.Cells(disBSCColumn("objdgcolhScaleMasterId")).Text) > 0 Then
                                    Dim dsScore_Guide As New DataSet
                                    Dim objScaleMaster As New clsAssessment_Scale
                                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(xRow.Cells(disBSCColumn("objdgcolhScaleMasterId")).Text))
                                    objScaleMaster = Nothing
                                    mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
                                    Me.ViewState("mdecMaxScale") = mdecMaxScale
                                    Dim dtmp() As DataRow = dsScore_Guide.Tables(0).Select("scale = " & CDec(iDecVal))
                                    If dtmp.Length <= 0 Then
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Sorry, particular scale is not defined. Please refer score guide for the list of score(s) defined."), Me)
                                        Dim dTemp() As DataRow = Nothing
                                        dTemp = GetOldValue_BSC(xRow.ItemIndex)
                                        If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
                                            If txt.ID = "dgcolhAgreedScoreBSC" Then
                                                txt.Text = dTemp(0).Item("agreed_score")
                                            Else
                                                txt.Text = dTemp(0).Item("result")
                                            End If

                                        Else
                                            txt.Text = "" : txt.Focus()
                                        End If
                                        bln = True
                                        Exit Sub
                                    Else
                                        GoTo iValid
                                    End If
                                Else
                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Sorry, no scale is not defined. Please define scale."), Me)
                                    Dim dTemp() As DataRow = Nothing
                                    dTemp = GetOldValue_BSC(xRow.ItemIndex)
                                    If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
                                        txt.Text = dTemp(0).Item("result")
                                    Else
                                        txt.Text = "" : txt.Focus()
                                    End If
                                    bln = True
                                    Exit Sub
                                End If
                            Else
                                Exit Sub
                            End If
                    End Select
iValid:             If Validation() = False Then bln = True : Exit Sub 'SHANI [21 Mar 2015]-bln = True 
                    If Is_Already_Assessed() = False Then
                        bln = True
                        txt.Text = "" : Exit Sub
                    End If
                    If txt.ID = "dgcolhAgreedScoreBSC" Then
                        Dim iRScore As Decimal = 0 : Decimal.TryParse(CType(xRow.Cells(disBSCColumn("dgcolhaselfBSC")).Controls(1), TextBox).Text, iRScore)
                        Call Evaluated_Data_BSC(iRScore, CType(xRow.Cells(disBSCColumn("dgcolharemarkBSC")).Controls(1), TextBox).Text, xRow.ItemIndex, iDecVal)
                    Else
                        Dim iAgreedScore As Decimal = 0 : Decimal.TryParse(CType(xRow.Cells(disBSCColumn("dgcolhAgreedScoreBSC")).FindControl("dgcolhAgreedScoreBSC"), TextBox).Text, iAgreedScore)
                        Call Evaluated_Data_BSC(iDecVal, CType(xRow.Cells(disBSCColumn("dgcolharemarkBSC")).Controls(1), TextBox).Text, xRow.ItemIndex, iAgreedScore)
                    End If

                Else
                    txt.Text = ""
                End If

            End If
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & objpnlBSC.ClientID & ");", True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Protected Sub dgcolhGEScore_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If CInt(CType(sender, LinkButton).CommandArgument) > 0 Then 'SCALE MASTER ID
                Dim dsScore_Guide As New DataSet
                Dim objScaleMaster As New clsAssessment_Scale
                dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(CType(sender, LinkButton).CommandArgument))
                objScaleMaster = Nothing
                dgvGEScoreGuide.DataSource = dsScore_Guide.Tables(0)
                dgvGEScoreGuide.DataBind()
                popup_ViewGuideGE.Show()
                iWeightTotal = 0
                dgvGE.DataSource = dtGE_TabularGrid
                dgvGE.DataBind()
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 26, "Sorry no score gude defined for the selected item."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Protected Sub dgcolhBSCScore_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If CInt(CType(sender, LinkButton).CommandArgument) > 0 Then 'SCALE MASTER ID
                Dim dsScore_Guide As New DataSet
                Dim objScaleMaster As New clsAssessment_Scale
                dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(CType(sender, LinkButton).CommandArgument))
                objScaleMaster = Nothing
                dgvBSCScoreGuide.DataSource = dsScore_Guide.Tables(0)
                dgvBSCScoreGuide.DataBind()
                popup_ViewGuideBSC.Show()
                iWeightTotal = 0
                dgvBSC.DataSource = dtBSC_TabularGrid
                dgvBSC.DataBind()
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 26, "Sorry no score gude defined for the selected item."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        Finally
        End Try
    End Sub

    Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim xRow As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)

            If mblnIsMatchCompetencyStructure Then
                Me.ViewState("RowIndex") = -1
                mstriEditingGUID = dtCustomTabularGrid.Rows(xRow.RowIndex).Item("GUID")
            Else
                Me.ViewState("RowIndex") = -1
                mstriEditingGUID = ""
            End If

            'S.SANDEEP [25-JAN-2017] -- START
            'ISSUE/ENHANCEMENT : ISSUE RELATED TO HEIGHT OF TEXTBOX {AKF Self-services In PA Custom Items}
            btnIAdd.Text = "Add"
            'S.SANDEEP [25-JAN-2017] -- END

            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
                'S.SANDEEP |25-MAR-2019| -- START
                'Dim xtmp() As DataRow = mdtCustomEvaluation.Select("custom_header = '" & dtCustomTabularGrid.Rows(xRow.RowIndex).Item("Header_Name") & "' AND AUD <> 'D'")
                Dim xtmp() As DataRow = mdtCustomEvaluation.Select("Header_Id = '" & dtCustomTabularGrid.Rows(xRow.RowIndex).Item("Header_Id") & "' AND AUD <> 'D'")
                'S.SANDEEP |25-MAR-2019| -- END
                If xtmp.Length > 0 AndAlso CBool(dtCustomTabularGrid.Rows(xRow.RowIndex).Item("Is_Allow_Multiple")) = False Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 118, "Sorry, this particular custom header is not set for allow multiple entries when defined."), Me)
                    Exit Sub
                End If
            End If
            Call Generate_Popup_Data(dtCustomTabularGrid.Rows(xRow.RowIndex).Item("Header_Id"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub imgEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
            Me.ViewState("RowIndex") = row.RowIndex

            'S.SANDEEP [25-JAN-2017] -- START
            'ISSUE/ENHANCEMENT : ISSUE RELATED TO HEIGHT OF TEXTBOX {AKF Self-services In PA Custom Items}
            btnIAdd.Text = "Save"
            'S.SANDEEP [25-JAN-2017] -- END

            mstriEditingGUID = dtCustomTabularGrid.Rows(row.RowIndex).Item("GUID")

            'S.SANDEEP |30-MAR-2019| -- START
            If mstriEditingGUID.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 57, "Sorry, No comment(s) has been added in order to edit."), Me)
                Exit Sub
            End If
            'S.SANDEEP |30-MAR-2019| -- END

            Call Generate_Popup_Data(dtCustomTabularGrid.Rows(row.RowIndex).Item("Header_Id"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
            Me.ViewState("RowIndex") = row.RowIndex
            mstriEditingGUID = dtCustomTabularGrid.Rows(row.RowIndex).Item("GUID")
            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
                Dim xRow() As DataRow = Nothing
                xRow = mdtCustomEvaluation.Select("customanalysistranguid = '" & dtCustomTabularGrid.Rows(row.RowIndex).Item("GUID") & "' AND AUD <> 'D'")
                If xRow.Length > 0 Then
                    lblTitle.Text = "Aruti"
                    lblMessage.Text = "Please enter vaild reason to void following entry."
                    txtMessage.Text = ""
                    popup_CItemReason.Show()
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    Protected Sub dtpCustomItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If CDate(dtpAssessdate.GetDate) >= CDate(CType(CType(sender, TextBox).NamingContainer, Controls_DateCtrl).GetDate) Then
                CType(CType(sender, TextBox).NamingContainer, Controls_DateCtrl).SetDate = Nothing
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmAddCustomValue", 2, "Sorry, Selected date should be greter than the assessment date selected."), Me)
                CType(sender, Controls_DateCtrl).Focus()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub

    'S.SANDEEP |20-SEP-2019| -- START
    'ISSUE/ENHANCEMENT : {Ref#0004155}
    Protected Sub lnkCopyScore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCopyScore.Click
        Try
            '################################## COPY SCORE BSC DATA ##################################> START
            If objpnlBSC.Visible = True Then
                For Each gvItem As DataGridItem In dgvBSC.Items
                    dgvBSC.SelectedIndex = gvItem.ItemIndex
                    If CBool(gvItem.Cells(disBSCColumn("objdgcolhIsGrpBSC")).Text) = False Then
                        If gvItem.Cells(disBSCColumn("dgcolhaselfBSC")).Text.Trim.ToUpper = "&NBSP;" Then Continue For
                        CType(gvItem.Cells(disBSCColumn("dgcolhAgreedScoreBSC")).Controls(1), TextBox).Text = CType(gvItem.Cells(disBSCColumn("dgcolhaselfBSC")).Controls(1), TextBox).Text
                        Dim iDecWgt As Decimal = 0 : Decimal.TryParse(gvItem.Cells(disBSCColumn("dgcolhBSCWeight")).Text, iDecWgt) : Me.ViewState("mdecItemWeight") = iDecWgt
                        If CInt(gvItem.Cells(disBSCColumn("objdgcolhScaleMasterId")).Text) > 0 Then
                            Dim dsScore_Guide As New DataSet
                            Dim objScaleMaster As New clsAssessment_Scale
                            dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(gvItem.Cells(disBSCColumn("objdgcolhScaleMasterId")).Text))
                            objScaleMaster = Nothing
                            mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
                            Me.ViewState("mdecMaxScale") = mdecMaxScale
                        End If
                        If CType(gvItem.Cells(disBSCColumn("dgcolhaselfBSC")).Controls(1), TextBox).Text.Trim.Length > 0 Then
                            Dim iVal As Decimal = 0
                            Decimal.TryParse(CType(gvItem.Cells(disBSCColumn("dgcolhaselfBSC")).Controls(1), TextBox).Text, iVal)
                            If iVal >= 0 Then Call Evaluated_Data_BSC(CDec(CType(gvItem.Cells(disBSCColumn("dgcolhaselfBSC")).Controls(1), TextBox).Text), "", gvItem.ItemIndex, CDec(CType(gvItem.Cells(disBSCColumn("dgcolhaselfBSC")).Controls(1), TextBox).Text))
                        End If
                    End If
                Next
            End If
            '################################## COPY SCORE BSC DATA ##################################> END

            '################################## COPY SCORE COMPETENCIES DATA ##################################> START
            If objpnlGE.Visible = True Then
                For Each gvItem As DataGridItem In dgvGE.Items
                    dgvGE.SelectedIndex = gvItem.ItemIndex
                    If CBool(gvItem.Cells(disGEColumn("objdgcolhIsGrpGE")).Text) = False AndAlso CBool(gvItem.Cells(disGEColumn("objdgcolhIsPGrpGE")).Text) = False Then
                        If gvItem.Cells(disGEColumn("dgcolhaselfGE")).Text.Trim.ToUpper = "&NBSP;" Then Continue For
                        CType(gvItem.Cells(disGEColumn("dgcolhaAgreedScoreGE")).Controls(1), TextBox).Text = CType(gvItem.Cells(disGEColumn("dgcolhaselfGE")).Controls(1), TextBox).Text
                        Dim iDecWgt As Decimal = 0
                        Decimal.TryParse(gvItem.Cells(disGEColumn("dgcolhGEWeight")).Text, iDecWgt)
                        Me.ViewState("mdecItemWeight") = iDecWgt
                        If CInt(gvItem.Cells(disGEColumn("objdgcolhscalemasterunkidGE")).Text) > 0 Then
                            Dim dsScore_Guide As New DataSet
                            Dim objScaleMaster As New clsAssessment_Scale
                            dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(gvItem.Cells(disGEColumn("objdgcolhscalemasterunkidGE")).Text))
                            objScaleMaster = Nothing
                            mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
                            Me.ViewState("mdecMaxScale") = mdecMaxScale
                        End If
                        If CType(gvItem.Cells(disGEColumn("dgcolhaselfGE")).Controls(1), TextBox).Text.Trim.Length > 0 Then
                            Dim iVal As Decimal = 0
                            Decimal.TryParse(CType(gvItem.Cells(disBSCColumn("dgcolhaselfGE")).Controls(1), TextBox).Text, iVal)
                            If iVal >= 0 Then Call Evaluated_Data_GE(CType(gvItem.Cells(disGEColumn("dgcolhaselfGE")).Controls(1), TextBox).Text, "", gvItem.ItemIndex, gvItem, CType(gvItem.Cells(disGEColumn("dgcolhaselfGE")).Controls(1), TextBox).Text)
                        End If
                    End If
                Next
            End If
            '################################## COPY SCORE COMPETENCIES DATA ##################################> END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub
    'S.SANDEEP |20-SEP-2019| -- END
    

#End Region

    'S.SANDEEP |25-MAR-2019| -- START
    Private Sub SetLanguage()
        Try
            ''Language.setLanguage(mstrModuleName)
            'Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)
            'Me.lblAssessDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblAssessDate.ID, Me.lblAssessDate.Text)
            'Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblEmployee.ID, Me.lblEmployee.Text)
            'Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),lblPeriod.ID, Me.lblPeriod.Text)
            'Me.btnSaveCommit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),btnSaveCommit.ID, Me.btnSaveCommit.Text).Replace("&", "")
            'Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),btnSave.ID, Me.btnSave.Text).Replace("&", "")
            'Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),btnClose.ID, Me.btnClose.Text).Replace("&", "")

            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"dgcolhSDate", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhEDate", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"dgcolhEDate", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhCompleted", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"dgcolhCompleted", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhGoalType", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"dgcolhGoalType", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgoalvalue", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"dgoalvalue", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhStatus", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"dgcolhStatus", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhBSCScore", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"dgcolhBSCScore", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhBSCWeight", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"dgcolhBSCWeight", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolheselfBSC", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"dgcolheselfBSC", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolhSDate", False, True)).HeaderText)
            'dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "dgcolheremarkBSC", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"dgcolheremarkBSC", dgvBSC.Columns(getColumnId_Datagrid(dgvBSC, "", False, True)).HeaderText)

            'Language.setLanguage("frmAddCustomValue")
            dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhItems", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmAddCustomValue",CInt(HttpContext.Current.Session("LangId")),"dgcolhItems", dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhItems", False, True)).HeaderText)
            dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhValue", False, True)).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmAddCustomValue",CInt(HttpContext.Current.Session("LangId")),"dgcolhValue", dgv_Citems.Columns(getColumnId_Datagrid(dgv_Citems, "dgcolhValue", False, True)).HeaderText)

        Catch Ex As Exception
            DisplayMessage.DisplayError(ex,Me)
        End Try
    End Sub
    'S.SANDEEP |25-MAR-2019| -- END

End Class


'Partial Class wPgAssessorEvaluation
'    Inherits Basepage

'#Region "Private Variables"


'    Private DisplayMessage As New CommonCodes
'    Private mstrModuleName As String = "frmPerformanceEvaluation"
'    Private objEAnalysisMst As New clsevaluation_analysis_master
'    Private objGoalsTran As New clsgoal_analysis_tran
'    Private objCAssessTran As New clscompetency_analysis_tran
'    Private objCCustomTran As New clscompeteny_customitem_tran
'    Private menAction As enAction = enAction.ADD_ONE
'    Private mintAssessAnalysisUnkid As Integer = -1
'    'Private mintEmplId As Integer = 0
'    'Private mintPeriodId As Integer = 0
'    Private mintYearUnkid As Integer = 0
'    'Private mintAssessorId As Integer = 0
'    Private menAssess As enAssessmentMode = enAssessmentMode.APPRAISER_ASSESSMENT
'    Private mdtBSC_Evaluation As DataTable
'    Private mdtGE_Evaluation As DataTable
'    Private mdtCustomEvaluation As DataTable
'    Private dtBSC_TabularGrid As New DataTable
'    Private dtGE_TabularGrid As New DataTable
'    Private dtCustomTabularGrid As New DataTable
'    Private iWeightTotal As Decimal = 0
'    Private dsHeaders As New DataSet
'    Private iHeaderId As Integer = 0
'    Private iExOrdr As Integer = 0
'    Private iLinkedFieldId As Integer
'    Private iMappingUnkid As Integer
'    Private xVal As Integer = 1
'    Private xTotAssignedWeight As Decimal = 0

'    Private dtCItems As New DataTable
'    Private mblnItemAddEdit As Boolean = False
'    Private mstriEditingGUID As String = String.Empty
'    Private objCONN As SqlConnection

'    'S.SANDEEP [21 JAN 2015] -- START
'    Private mdecItemWeight As Decimal = 0
'    Private mdecMaxScale As Decimal = 0
'    'S.SANDEEP [21 JAN 2015] -- END

'    'S.SANDEEP [12 OCT 2016] -- START
'    Private mblnIsMatchCompetencyStructure As Boolean = False
'    'S.SANDEEP [12 OCT 2016] -- END

'    'Shani (23-Nov-2016) -- Start
'    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'    Dim disBSColumn As Dictionary(Of String, Integer) = Nothing
'    Dim disGEolumn As Dictionary(Of String, Integer) = Nothing
'    'Shani (23-Nov123-2016-2016) -- End


'#End Region

'#Region "Page Event"
'    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
'        Try
'            'Shani (23-Nov-2016) -- Start
'            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'            disBSColumn = dgvBSC.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvBSC.Columns.IndexOf(x))
'            disGEolumn = dgvGE.Columns.Cast(Of DataGridColumn).ToDictionary(Function(x) x.FooterText, Function(x) dgvGE.Columns.IndexOf(x))
'            'Shani (23-Nov123-2016-2016) -- End

'            'S.SANDEEP [ 17 DEC 2014 ] -- START
'            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
'                objCONN = Nothing
'                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
'                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
'                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
'                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
'                    objCONN = New SqlConnection
'                    objCONN.ConnectionString = constr
'                    objCONN.Open()
'                    HttpContext.Current.Session("gConn") = objCONN
'                End If
'                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")
'                If arr.Length = 5 Then
'                    Try
'                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
'                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
'                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
'                        Else
'                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
'                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
'                        End If

'                    Catch ex As Exception
'                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
'                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
'                    End Try
'                    Blank_ModuleName()
'                    clsCommonATLog._WebFormName = "frmPerformanceEvaluation"
'                    StrModuleName2 = "mnuAssessment"
'                    StrModuleName3 = "mnuPerformaceEvaluation"
'                    clsCommonATLog._WebClientIP = Session("IP_ADD")
'                    clsCommonATLog._WebHostName = Session("HOST_NAME")
'                    Me.ViewState.Add("IsDirect", True)

'                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
'                    HttpContext.Current.Session("UserId") = CInt(arr(1))
'                    Me.ViewState.Add("employeeunkid", CInt(arr(2)))
'                    Me.ViewState.Add("assessormasterunkid", CInt(arr(3)))
'                    Me.ViewState.Add("periodid", CInt(arr(4)))

'                    'Sohail (30 Mar 2015) -- Start
'                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
'                    'Dim objCommon As New CommonCodes
'                    'objCommon.GetCompanyYearInfo(CInt(Session("CompanyUnkId")))
'                    Dim strError As String = ""
'                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
'                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
'                        Exit Sub
'                    End If
'                    'Sohail (30 Mar 2015) -- End
'                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
'                    gobjConfigOptions = New clsConfigOptions
'                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
'                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
'                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
'                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

'                    ArtLic._Object = New ArutiLic(False)
'                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
'                        Dim objGroupMaster As New clsGroup_Master
'                        objGroupMaster._Groupunkid = 1
'                        ArtLic._Object.HotelName = objGroupMaster._Groupname
'                    End If

'                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
'                        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
'                        Exit Sub
'                    End If

'                    If ConfigParameter._Object._IsArutiDemo Then
'                        If ConfigParameter._Object._IsExpire Then
'                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
'                            Exit Try
'                        Else
'                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
'                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
'                                Exit Try
'                            End If
'                        End If
'                    End If

'                    Dim clsConfig As New clsConfigOptions
'                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
'                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
'                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
'                    Else
'                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
'                    End If

'                    Session("IsAllowFinalSave") = clsConfig._IsAllowFinalSave
'                    Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp
'                    Session("EmployeeAsOnDate") = eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime)
'                    Session("IsBSC_ByEmployee") = clsConfig._IsBSC_ByEmployee
'                    Session("IsCompanyNeedReviewer") = clsConfig._IsCompanyNeedReviewer
'                    Session("Assessment_Instructions") = clsConfig._Assessment_Instructions
'                    Session("ViewTitles_InPlanning") = clsConfig._ViewTitles_InPlanning
'                    Session("FollowEmployeeHierarchy") = clsConfig._FollowEmployeeHierarchy
'                    Session("CascadingTypeId") = clsConfig._CascadingTypeId
'                    Session("ScoringOptionId") = clsConfig._ScoringOptionId
'                    Session("Perf_EvaluationOrder") = clsConfig._Perf_EvaluationOrder
'                    Session("ViewTitles_InEvaluation") = clsConfig._ViewTitles_InEvaluation
'                    Session("ConsiderItemWeightAsNumber") = clsConfig._ConsiderItemWeightAsNumber
'                    'S.SANDEEP [09 OCT 2015] -- START
'                    Session("Self_Assign_Competencies") = clsConfig._Self_Assign_Competencies
'                    'S.SANDEEP [09 OCT 2015] -- END

'                    'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
'                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
'                    Session("DateFormat") = clsConfig._CompanyDateFormat
'                    Session("DateSeparator") = clsConfig._CompanyDateSeparator
'                    SetDateFormat()
'                    'Pinkal (16-Apr-2016) -- End


'                    Dim objUser As New clsUserAddEdit
'                    objUser._Userunkid = CInt(Session("UserId"))
'                    'Sohail (21 Mar 2015) -- Start
'                    'Enhancement - New UI Notification Link Changes.
'                    'Dim clsuser As New User(objUser._Username, objUser._Password, Global.User.en_loginby.User, Session("mdbname"))
'                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
'                    Call GetDatabaseVersion()
'                    Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname"))
'                    'Sohail (21 Mar 2015) -- End
'                    HttpContext.Current.Session("clsuser") = clsuser
'                    HttpContext.Current.Session("UserName") = clsuser.UserName
'                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
'                    HttpContext.Current.Session("Surname") = clsuser.Surname
'                    HttpContext.Current.Session("MemberName") = clsuser.MemberName
'                    'Sohail (30 Mar 2015) -- Start
'                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
'                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
'                    HttpContext.Current.Session("UserId") = clsuser.UserID
'                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
'                    HttpContext.Current.Session("Password") = clsuser.password
'                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
'                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

'                    strError = ""
'                    If SetUserSessions(strError) = False Then
'                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
'                        Exit Sub
'                    End If

'                    strError = ""
'                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
'                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
'                        Exit Sub
'                    End If
'                    'Sohail (30 Mar 2015) -- End

'                    Dim objUserPrivilege As New clsUserPrivilege
'                    objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))

'                    'S.SANDEEP [28 MAY 2015] -- START
'                    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
'                    'Session("AllowToAddAssessorBSCAssessment") = objUserPrivilege._AllowToAddAssessorBSCAssessment
'                    Session("AllowtoAddAssessorEvaluation") = objUserPrivilege._AllowtoAddAssessorEvaluation
'                    'S.SANDEEP [28 MAY 2015] -- END



'                    txtInstruction.Text = Session("Assessment_Instructions")
'                    objlblCaption.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Instructions")
'                    objpnlInstruction.Visible = True
'                    txtInstruction.Height = Unit.Pixel(430)

'                    objbtnNext.Enabled = False : objbtnBack.Enabled = False
'                    objlblValue1.Visible = False : objlblValue2.Visible = False
'                    objlblValue3.Visible = False : objlblValue4.Visible = False

'                    Call FillCombo()

'                    Call GetValue()

'                    radOption.SelectedValue = 1 : radOption.Enabled = True
'                    Call radOption_SelectedIndexChanged(New Object, New EventArgs)

'                    cboAssessor.SelectedValue = CInt(Me.ViewState("assessormasterunkid"))
'                    Call cboAssessor_SelectedIndexChanged(New Object, New EventArgs)
'                    cboAssessor.Enabled = False

'                    cboEmployee.SelectedValue = CInt(Me.ViewState("employeeunkid"))
'                    cboEmployee.Enabled = False

'                    cboPeriod.SelectedValue = CInt(Me.ViewState("periodid"))
'                    Call cboPeriod_SelectedIndexChanged(New Object, New EventArgs)
'                    cboPeriod.Enabled = False

'                    Me.ViewState.Add("AssessAnalysisUnkid", -1)

'                    If objEAnalysisMst.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Me.ViewState("assessormasterunkid"), , Me.ViewState("AssessAnalysisUnkid")) = True Then
'                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry, You have already assessed some items. Please open in edit mode and add new items."), Me, "../../Index.aspx")
'                    End If

'                    objGoalsTran._AnalysisUnkid = mintAssessAnalysisUnkid
'                    mdtBSC_Evaluation = objGoalsTran._DataTable

'                    objCAssessTran._ConsiderWeightAsNumber = CBool(Session("ConsiderItemWeightAsNumber"))
'                    'S.SANDEEP [09 OCT 2015] -- START
'                    objCAssessTran._SelfAssignCompetencies = CBool(Session("Self_Assign_Competencies"))
'                    'S.SANDEEP [09 OCT 2015] -- END
'                    objCAssessTran._AnalysisUnkid = mintAssessAnalysisUnkid
'                    mdtGE_Evaluation = objCAssessTran._DataTable

'                    objCCustomTran._AnalysisUnkid = mintAssessAnalysisUnkid
'                    objCCustomTran._EmployeeId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue))
'                    'S.SANDEEP [29 DEC 2015] -- START

'                    'Shani (26-Sep-2016) -- Start
'                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                    'objCCustomTran._AllowCustomItemInPlanning = CBool(Session("IncludeCustomItemInPlanning"))
'                    'Shani (26-Sep-2016) -- End


'                    objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
'                    'S.SANDEEP [29 DEC 2015] -- END
'                    mdtCustomEvaluation = objCCustomTran._DataTable

'                    BtnReset.Enabled = False

'                    'Sohail (30 Mar 2015) -- Start
'                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
'                    'SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, Session("UserId"), , Session("mdbname"), Session("CompanyUnkId"), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
'                    'Sohail (30 Mar 2015) -- End
'                    HttpContext.Current.Session("Login") = True
'                    GoTo Link
'                End If
'            End If
'            'S.SANDEEP [ 17 DEC 2014 ] -- END

'            Blank_ModuleName()
'            clsCommonATLog._WebFormName = "frmPerformanceEvaluation"
'            StrModuleName2 = "mnuAssessment"
'            StrModuleName3 = "mnuPerformaceEvaluation"
'            clsCommonATLog._WebClientIP = Session("IP_ADD")
'            clsCommonATLog._WebHostName = Session("HOST_NAME")

'            If IsPostBack = False Then
'                If Session("Action") IsNot Nothing AndAlso Session("Unkid") IsNot Nothing Then
'                    Call ClearForm_Values()
'                    mintAssessAnalysisUnkid = Session("Unkid")
'                    ''Session("Action") 1:ADD_CONTINUE 2: ADD_ONE 3 :EDIT_ONE
'                    If CInt(Session("Action")) = 0 Then
'                        menAction = enAction.ADD_ONE
'                    ElseIf CInt(Session("Action")) = 1 Then
'                        menAction = enAction.EDIT_ONE
'                    ElseIf CInt(Session("Action")) = 2 Then
'                        menAction = enAction.ADD_CONTINUE
'                    End If

'                    txtInstruction.Text = Session("Assessment_Instructions")
'                    objlblCaption.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Instructions")
'                    objpnlInstruction.Visible = True
'                    txtInstruction.Height = Unit.Pixel(430)

'                    objbtnNext.Enabled = False : objbtnBack.Enabled = False
'                    objlblValue1.Visible = False : objlblValue2.Visible = False
'                    objlblValue3.Visible = False : objlblValue4.Visible = False

'                    Call FillCombo()
'                    If menAction = enAction.EDIT_ONE Then
'                        objEAnalysisMst._Analysisunkid = mintAssessAnalysisUnkid
'                        cboEmployee.Enabled = False
'                        cboAssessor.Enabled = False
'                        cboPeriod.Enabled = False
'                        radOption.Enabled = False
'                        cboReviewer.Enabled = False
'                        BtnSearch.Enabled = False
'                        BtnReset.Enabled = False
'                    End If
'                    Call GetValue()

'                    objGoalsTran._AnalysisUnkid = mintAssessAnalysisUnkid
'                    mdtBSC_Evaluation = objGoalsTran._DataTable
'                    'S.SANDEEP [09 OCT 2015] -- START
'                    objCAssessTran._SelfAssignCompetencies = CBool(Session("Self_Assign_Competencies"))
'                    'S.SANDEEP [09 OCT 2015] -- END
'                    objCAssessTran._ConsiderWeightAsNumber = CBool(Session("ConsiderItemWeightAsNumber"))
'                    objCAssessTran._AnalysisUnkid = mintAssessAnalysisUnkid
'                    mdtGE_Evaluation = objCAssessTran._DataTable

'                    objCCustomTran._AnalysisUnkid = mintAssessAnalysisUnkid
'                    objCCustomTran._EmployeeId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue))
'                    'S.SANDEEP [29 DEC 2015] -- START

'                    'Shani (26-Sep-2016) -- Start
'                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                    'objCCustomTran._AllowCustomItemInPlanning = CBool(Session("IncludeCustomItemInPlanning"))
'                    'Shani (26-Sep-2016) -- End


'                    objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
'                    'S.SANDEEP [29 DEC 2015] -- END
'                    mdtCustomEvaluation = objCCustomTran._DataTable

'                    If menAction = enAction.EDIT_ONE Then
'                        Call Fill_BSC_Evaluation()
'                        Call Fill_GE_Evaluation()
'                        Call Fill_Custom_Grid()
'                        Call BtnSearch_Click(sender, e)
'                    End If

'                    'SHANI [09 Mar 2015]-START
'                    'Enhancement - REDESIGN SELF SERVICE.
'                    If menAction <> enAction.EDIT_ONE Then

'                        'S.SANDEEP [18 DEC 2015] -- START
'                        'If Session("Assessor_Filter") IsNot Nothing Then
'                        '    cboAssessor.SelectedValue = CStr(Session("Assessor_Filter")).Split("-")(0).Trim
'                        '    Call cboAssessor_SelectedIndexChanged(cboAssessor, Nothing)
'                        '    cboEmployee.SelectedValue = CStr(Session("Assessor_Filter")).Split("-")(1).Trim
'                        'End If

'                        If Session("PaAssessPeriodUnkid") IsNot Nothing AndAlso Session("PaAssessEmpUnkid") IsNot Nothing AndAlso Session("PaAssessMstUnkid") IsNot Nothing Then
'                            cboAssessor.SelectedValue = Session("PaAssessMstUnkid")
'                            Call cboAssessor_SelectedIndexChanged(cboAssessor, Nothing)
'                            cboEmployee.SelectedValue = Session("PaAssessEmpUnkid")
'                            cboPeriod.SelectedValue = Session("PaAssessPeriodUnkid")
'                            Call cboPeriod_SelectedIndexChanged(cboPeriod, Nothing)
'                            dtpAssessdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
'                            cboAssessor.Enabled = False
'                            cboEmployee.Enabled = False
'                            cboPeriod.Enabled = False

'                            Call BtnSearch_Click(BtnSearch, Nothing)
'                        End If

'                        'S.SANDEEP [18 DEC 2015] -- END

'                    End If
'                    'SHANI [09 Mar 2015]--END 

'                Else
'                    ''Session is nothing here code
'                End If
'            End If

'Link:




'            If Me.ViewState("Action") IsNot Nothing Then
'                menAction = Me.ViewState("Action")
'            End If

'            If Me.ViewState("AssessAnalysisUnkid") IsNot Nothing Then
'                mintAssessAnalysisUnkid = Me.ViewState("AssessAnalysisUnkid")
'            End If
'            If Me.ViewState("Assess") IsNot Nothing Then
'                menAssess = enAssessmentMode.APPRAISER_ASSESSMENT
'            End If

'            If Me.Session("BSC_Evaluation") IsNot Nothing Then
'                mdtBSC_Evaluation = Me.Session("BSC_Evaluation")
'            End If

'            If Me.Session("GE_Evaluation") IsNot Nothing Then
'                mdtGE_Evaluation = Me.Session("GE_Evaluation")
'            End If

'            If Me.Session("CustomEvaluation") IsNot Nothing Then
'                mdtCustomEvaluation = Me.Session("CustomEvaluation")
'            End If

'            If Me.Session("BSC_TabularGrid") IsNot Nothing Then
'                dtBSC_TabularGrid = Me.Session("BSC_TabularGrid")
'            End If

'            If Me.Session("GE_TabularGrid") IsNot Nothing Then
'                dtGE_TabularGrid = Me.Session("GE_TabularGrid")
'            End If

'            If Me.Session("CustomTabularGrid") IsNot Nothing Then
'                dtCustomTabularGrid = Me.Session("CustomTabularGrid")
'            End If

'            If Me.ViewState("iWeightTotal") IsNot Nothing Then
'                iWeightTotal = Me.ViewState("iWeightTotal")
'            End If

'            If Me.ViewState("Headers") IsNot Nothing Then
'                dsHeaders = Me.ViewState("Headers")
'            End If

'            If Me.ViewState("iHeaderId") IsNot Nothing Then
'                iHeaderId = Me.ViewState("iHeaderId")
'            End If

'            If Me.ViewState("iExOrdr") IsNot Nothing Then
'                iExOrdr = Me.ViewState("iExOrdr")
'            End If

'            If Me.ViewState("iLinkedFieldId") IsNot Nothing Then
'                iLinkedFieldId = Me.ViewState("iLinkedFieldId")
'            End If

'            If Me.ViewState("YearUnkid") IsNot Nothing Then
'                mintYearUnkid = Me.ViewState("YearUnkid")
'            End If

'            If Me.ViewState("iMappingUnkid") IsNot Nothing Then
'                iMappingUnkid = Me.ViewState("iMappingUnkid")
'            End If

'            If Me.ViewState("xTotAssignedWeight") IsNot Nothing Then
'                xTotAssignedWeight = Me.ViewState("xTotAssignedWeight")
'            End If

'            If Me.ViewState("ColIndex") IsNot Nothing Then
'                xVal = Me.ViewState("ColIndex")
'            End If

'            If Me.Session("dtCItems") IsNot Nothing Then
'                dtCItems = Me.Session("dtCItems")
'            End If

'            If Me.ViewState("ItemAddEdit") IsNot Nothing Then
'                mblnItemAddEdit = Me.ViewState("ItemAddEdit")
'                If mblnItemAddEdit Then
'                    popup_CItemAddEdit.Show()
'                End If
'            End If

'            'S.SANDEEP [12 OCT 2016] -- START
'            mblnIsMatchCompetencyStructure = Me.ViewState("mblnIsMatchCompetencyStructure")
'            'S.SANDEEP [12 OCT 2016] -- END

'            If objpnlCItems.Visible = True Then
'                dgvItems.DataSource = dtCustomTabularGrid
'                dgvItems.DataBind()
'            End If
'            dtCItems = Session("dtCItems")
'            If pnl_CItemAddEdit.Visible Then
'                If dtCItems IsNot Nothing Then
'                    If dtCItems.Rows.Count > 0 Then
'                        dgv_Citems.DataSource = dtCItems
'                        dgv_Citems.DataBind()
'                    End If
'                End If
'            End If

'            If Me.ViewState("iEditingGUID") IsNot Nothing Then
'                mstriEditingGUID = Me.ViewState("iEditingGUID")
'            End If

'            'S.SANDEEP [21 JAN 2015] -- START
'            Me.ViewState("mdecItemWeight") = 0
'            Me.ViewState("mdecMaxScale") = 0
'            'S.SANDEEP [21 JAN 2015] -- END

'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
'        If Request.QueryString.Count <= 0 Then
'            Me.IsLoginRequired = True
'        End If
'    End Sub

'    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
'        Try
'            If Me.ViewState("Action") Is Nothing Then
'                Me.ViewState.Add("Action", menAction)
'            Else
'                Me.ViewState("Action") = menAction
'            End If

'            If Me.ViewState("AssessAnalysisUnkid") Is Nothing Then
'                Me.ViewState.Add("AssessAnalysisUnkid", mintAssessAnalysisUnkid)
'            Else
'                Me.ViewState("AssessAnalysisUnkid") = mintAssessAnalysisUnkid
'            End If

'            If Me.ViewState("YearUnkid") Is Nothing Then
'                Me.ViewState.Add("YearUnkid", mintYearUnkid)
'            Else
'                Me.ViewState("YearUnkid") = mintYearUnkid
'            End If

'            If Me.ViewState("Assess") Is Nothing Then
'                Me.ViewState.Add("Assess", menAssess)
'            Else
'                Me.ViewState("Assess") = menAssess
'            End If

'            If Me.Session("BSC_Evaluation") Is Nothing Then
'                Me.Session.Add("BSC_Evaluation", mdtBSC_Evaluation)
'            Else
'                Me.Session("BSC_Evaluation") = mdtBSC_Evaluation
'            End If

'            If Me.Session("GE_Evaluation") Is Nothing Then
'                Me.Session.Add("GE_Evaluation", mdtGE_Evaluation)
'            Else
'                Me.Session("GE_Evaluation") = mdtGE_Evaluation
'            End If

'            If Me.Session("CustomEvaluation") Is Nothing Then
'                Me.Session.Add("CustomEvaluation", mdtCustomEvaluation)
'            Else
'                Me.Session("CustomEvaluation") = mdtCustomEvaluation
'            End If

'            If Me.Session("BSC_TabularGrid") Is Nothing Then
'                Me.Session.Add("BSC_TabularGrid", dtBSC_TabularGrid)
'            Else
'                Me.Session("BSC_TabularGrid") = dtBSC_TabularGrid
'            End If

'            If Me.Session("GE_TabularGrid") Is Nothing Then
'                Me.Session.Add("GE_TabularGrid", dtGE_TabularGrid)
'            Else
'                Me.Session("GE_TabularGrid") = dtGE_TabularGrid
'            End If

'            If Me.Session("CustomTabularGrid") Is Nothing Then
'                Me.Session.Add("CustomTabularGrid", dtCustomTabularGrid)
'            Else
'                Me.Session("CustomTabularGrid") = dtCustomTabularGrid
'            End If

'            If Me.ViewState("iWeightTotal") Is Nothing Then
'                Me.ViewState.Add("iWeightTotal", iWeightTotal)
'            Else
'                Me.ViewState("iWeightTotal") = iWeightTotal
'            End If

'            If Me.ViewState("Headers") Is Nothing Then
'                Me.ViewState.Add("Headers", dsHeaders)
'            Else
'                Me.ViewState("Headers") = dsHeaders
'            End If

'            If Me.ViewState("iHeaderId") Is Nothing Then
'                Me.ViewState.Add("iHeaderId", iHeaderId)
'            Else
'                Me.ViewState("iHeaderId") = iHeaderId
'            End If

'            If Me.ViewState("iExOrdr") Is Nothing Then
'                Me.ViewState.Add("iExOrdr", iExOrdr)
'            Else
'                Me.ViewState("iExOrdr") = iExOrdr
'            End If

'            If Me.ViewState("iLinkedFieldId") Is Nothing Then
'                Me.ViewState.Add("iLinkedFieldId", iLinkedFieldId)
'            Else
'                Me.ViewState("iLinkedFieldId") = iLinkedFieldId
'            End If

'            If Me.ViewState("iMappingUnkid") Is Nothing Then
'                Me.ViewState.Add("iMappingUnkid", iMappingUnkid)
'            Else
'                Me.ViewState("iMappingUnkid") = iMappingUnkid
'            End If

'            If Me.ViewState("xTotAssignedWeight") Is Nothing Then
'                Me.ViewState.Add("xTotAssignedWeight", xTotAssignedWeight)
'            Else
'                Me.ViewState("xTotAssignedWeight") = xTotAssignedWeight
'            End If

'            If Me.ViewState("ColIndex") Is Nothing Then
'                Me.ViewState.Add("ColIndex", xVal)
'            Else
'                Me.ViewState("ColIndex") = xVal
'            End If

'            If Me.ViewState("ItemAddEdit") Is Nothing Then
'                Me.ViewState.Add("ItemAddEdit", mblnItemAddEdit)
'            Else
'                Me.ViewState("ItemAddEdit") = mblnItemAddEdit
'            End If

'            If Me.Session("dtCItems") Is Nothing Then
'                Me.Session.Add("dtCItems", dtCItems)
'            End If

'            If Me.ViewState("iEditingGUID") Is Nothing Then
'                Me.ViewState.Add("iEditingGUID", mstriEditingGUID)
'            Else
'                Me.ViewState("iEditingGUID") = mstriEditingGUID
'            End If

'            'S.SANDEEP [12 OCT 2016] -- START
'            Me.ViewState("mblnIsMatchCompetencyStructure") = mblnIsMatchCompetencyStructure
'            'S.SANDEEP [12 OCT 2016] -- END

'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
'        Try
'            If Session("Unkid") IsNot Nothing Then
'                Session.Remove("Unkid")
'            End If
'            If Session("Action") IsNot Nothing Then
'                Session.Remove("Action")
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub
'#End Region

'#Region "Private Methods"


'    Private Sub FillCombo()
'        Dim dsCombos As New DataSet
'        Dim objEmp As New clsEmployee_Master
'        Dim objPeriod As New clscommom_period_Tran
'        Try

'            'S.SANDEEP [17 NOV 2015] -- START
'            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "APeriod", True, 1)

'            'Shani(20-Nov-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "APeriod", True, 1, , , Session("Database_Name"))

'            'S.SANDEEP [10 DEC 2015] -- START
'            'dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "APeriod", True, 1)
'            dsCombos = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "APeriod", True, 1)
'            'S.SANDEEP [10 DEC 2015] -- END

'            'Shani(20-Nov-2015) -- End

'            'S.SANDEEP [17 NOV 2015] -- END

'            'Shani (09-May-2016) -- Start
'            Dim intCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
'            'Shani (09-May-2016) -- End


'            With cboPeriod
'                .DataValueField = "periodunkid"
'                .DataTextField = "name"
'                .DataSource = dsCombos.Tables("APeriod")
'                .DataBind()
'                .SelectedValue = 0
'            End With
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Private Sub SetValue()
'        Try
'            objEAnalysisMst._Analysisunkid = mintAssessAnalysisUnkid
'            objEAnalysisMst._Periodunkid = CInt(cboPeriod.SelectedValue)
'            objEAnalysisMst._Selfemployeeunkid = -1
'            objEAnalysisMst._Assessedemployeeunkid = CInt(cboEmployee.SelectedValue)
'            If radOption.SelectedValue = "1" Then
'                objEAnalysisMst._Assessormasterunkid = CInt(cboAssessor.SelectedValue)
'                Dim intEmployeeId As Integer = -1
'                intEmployeeId = objEAnalysisMst.GetAssessorEmpId(CInt(cboAssessor.SelectedValue))
'                objEAnalysisMst._Assessoremployeeunkid = intEmployeeId
'            ElseIf radOption.SelectedValue = "1" Then
'                objEAnalysisMst._Ext_Assessorunkid = CInt(cboAssessor.SelectedValue)
'                objEAnalysisMst._Assessormasterunkid = -1
'                objEAnalysisMst._Assessoremployeeunkid = -1
'            End If
'            objEAnalysisMst._Reviewerunkid = -1
'            objEAnalysisMst._Assessmodeid = enAssessmentMode.APPRAISER_ASSESSMENT
'            objEAnalysisMst._Assessmentdate = dtpAssessdate.GetDate
'            objEAnalysisMst._Userunkid = Session("UserId")
'            If objEAnalysisMst._Committeddatetime <> Nothing Then
'                objEAnalysisMst._Committeddatetime = objEAnalysisMst._Committeddatetime
'            Else
'                objEAnalysisMst._Committeddatetime = Nothing
'            End If
'            objEAnalysisMst._Isvoid = False
'            objEAnalysisMst._Voiduserunkid = -1
'            objEAnalysisMst._Voiddatetime = Nothing
'            objEAnalysisMst._Voidreason = ""
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Private Sub GetValue()
'        Try
'            If objEAnalysisMst._Ext_Assessorunkid > 0 Then
'                radOption.SelectedValue = "2"
'                Call radOption_SelectedIndexChanged(radOption, Nothing)
'                'Shani(14-Sep-2015) -- Start
'                'Issue: TRA Training Comments & Changes Requested By Dennis
'                'cboAssessor.SelectedValue = objEAnalysisMst._Ext_Assessorunkid
'                If objEAnalysisMst._Ext_Assessorunkid > 0 Then
'                    cboAssessor.SelectedValue = objEAnalysisMst._Ext_Assessorunkid
'                End If
'                'Shani(14-Sep-2015) -- End
'            Else
'                radOption.SelectedValue = "1"
'                Call radOption_SelectedIndexChanged(radOption, Nothing)
'                'Shani(14-Sep-2015) -- Start
'                'Issue: TRA Training Comments & Changes Requested By Dennis
'                If objEAnalysisMst._Assessormasterunkid > 0 Then
'                    'Shani(14-Sep-2015) -- End
'                    cboAssessor.SelectedValue = objEAnalysisMst._Assessormasterunkid
'                    If cboAssessor.SelectedValue > 0 Then
'                        Call cboAssessor_SelectedIndexChanged(cboAssessor, Nothing)
'                    End If
'                    'Shani(14-Sep-2015) -- Start
'                    'Issue: TRA Training Comments & Changes Requested By Dennis
'                End If
'                'Shani(14-Sep-2015) -- End
'            End If
'            cboEmployee.SelectedValue = objEAnalysisMst._Assessedemployeeunkid
'            cboPeriod.SelectedValue = objEAnalysisMst._Periodunkid
'            If menAction = enAction.EDIT_ONE Then
'                Call cboPeriod_SelectedIndexChanged(cboPeriod, Nothing)
'            End If
'            If objEAnalysisMst._Assessmentdate <> Nothing Then
'                dtpAssessdate.SetDate = objEAnalysisMst._Assessmentdate
'                'Shani(11-NOV-2015) -- Start
'                'ENHANCEMENT : 
'            Else
'                dtpAssessdate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
'                'Shani(11-NOV-2015) -- End
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    'Shani (26-Sep-2016) -- Start
'    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)



'    'Private Sub SetTotals(ByVal iDG As DataGrid)
'    '    Try
'    '        Select Case iDG.ID.ToUpper
'    '            Case dgvBSC.ID.ToUpper
'    '                objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
'    '                objlblValue1.Visible = True : objlblValue2.Visible = True

'    '                'S.SANDEEP [21 JAN 2015] -- START
'    '                'If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
'    '                '    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
'    '                '        objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D'")).ToString
'    '                '    End If
'    '                '    objlblValue3.Visible = False : objlblValue4.Visible = False

'    '                'ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'    '                '    'S.SANDEEP [ 15 DEC 2014 ] -- START
'    '                '    'objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(escore)", "")).ToString
'    '                '    If dtBSC_TabularGrid.Rows.Count > 0 Then
'    '                '        objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(escore)", "")).ToString
'    '                '    End If
'    '                '    'S.SANDEEP [ 15 DEC 2014 ] -- END
'    '                '    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
'    '                '        objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D'")).ToString
'    '                '    End If
'    '                '    objlblValue3.Visible = True : objlblValue4.Visible = False
'    '                'ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
'    '                '    'S.SANDEEP [ 15 DEC 2014 ] -- START
'    '                '    'objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(escore)", "")).ToString
'    '                '    'objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(ascore)", "")).ToString
'    '                '    If dtBSC_TabularGrid.Rows.Count > 0 Then
'    '                '        objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(escore)", "")).ToString
'    '                '        objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & CDbl(dtBSC_TabularGrid.Compute("SUM(ascore)", "")).ToString
'    '                '    End If
'    '                '    'S.SANDEEP [ 15 DEC 2014 ] -- END
'    '                '    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
'    '                '        objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D'")).ToString
'    '                '    End If
'    '                '    objlblValue3.Visible = True : objlblValue4.Visible = True
'    '                'End If

'    '                If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
'    '                    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then

'    '                        'Shani(20-Nov-2015) -- Start
'    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'    '                        'objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'    '                        'objEAnalysisMst.Compute_Score(menAssess, _
'    '                        '                              True, _
'    '                        '                              Session("ScoringOptionId"), _
'    '                        '                              enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
'    '                        '                              cboEmployee.SelectedValue, _
'    '                        '                              cboPeriod.SelectedValue, , mdtBSC_Evaluation)
'    '                        objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'    '                        objEAnalysisMst.Compute_Score(menAssess, _
'    '                                                      True, _
'    '                                                      Session("ScoringOptionId"), _
'    '                                                      enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
'    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                                      cboEmployee.SelectedValue, _
'    '                                                      cboPeriod.SelectedValue, , mdtBSC_Evaluation, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'    '                        'Shani(20-Nov-2015) -- End
'    '                    End If
'    '                    objlblValue3.Visible = False : objlblValue4.Visible = False
'    '                ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'    '                    'Shani(20-Nov-2015) -- Start
'    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

'    '                    'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'    '                    'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'    '                    '                              True, _
'    '                    '                              Session("ScoringOptionId"), _
'    '                    '                              enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
'    '                    '                              cboEmployee.SelectedValue, _
'    '                    '                              cboPeriod.SelectedValue, , dtBSC_TabularGrid)

'    '                    If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'    '                    objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'    '                                                  True, _
'    '                                                  Session("ScoringOptionId"), _
'    '                                                  enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
'    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                                  cboEmployee.SelectedValue, _
'    '                                                  cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

'    '                    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
'    '                        'Shani(20-Nov-2015) -- Start
'    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'    '                        'objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'    '                        'objEAnalysisMst.Compute_Score(menAssess, _
'    '                        '                              True, _
'    '                        '                              Session("ScoringOptionId"), _
'    '                        '                              enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
'    '                        '                              cboEmployee.SelectedValue, cboPeriod.SelectedValue, , _
'    '                        '                              mdtBSC_Evaluation, cboAssessor.SelectedValue)
'    '                        objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'    '                        objEAnalysisMst.Compute_Score(menAssess, _
'    '                                                      True, _
'    '                                                      Session("ScoringOptionId"), _
'    '                                                      enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
'    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                                      cboEmployee.SelectedValue, cboPeriod.SelectedValue, , _
'    '                                                      mdtBSC_Evaluation, cboAssessor.SelectedValue, , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'    '                        'Shani(20-Nov-2015) -- End

'    '                    End If
'    '                    objlblValue3.Visible = True : objlblValue4.Visible = False
'    '                ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
'    '                    'Shani(20-Nov-2015) -- Start
'    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'    '                    'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'    '                    'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'    '                    '                              True, _
'    '                    '                              Session("ScoringOptionId"), enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
'    '                    '                              cboEmployee.SelectedValue, _
'    '                    '                              cboPeriod.SelectedValue, , dtBSC_TabularGrid)

'    '                    'If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'    '                    'objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
'    '                    '                              True, _
'    '                    '                              Session("ScoringOptionId"), _
'    '                    '                              enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
'    '                    '                              cboEmployee.SelectedValue, _
'    '                    '                              cboPeriod.SelectedValue, , dtBSC_TabularGrid)

'    '                    If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'    '                    objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'    '                                                  True, _
'    '                                                  Session("ScoringOptionId"), enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
'    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                                  cboEmployee.SelectedValue, _
'    '                                                  cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

'    '                    If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'    '                    objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
'    '                                                  True, _
'    '                                                  Session("ScoringOptionId"), _
'    '                                                  enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
'    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                                  cboEmployee.SelectedValue, _
'    '                                                  cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

'    '                    'Shani(20-Nov-2015) -- End


'    '                    If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
'    '                        'Shani(20-Nov-2015) -- Start
'    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'    '                        'objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & _
'    '                        'objEAnalysisMst.Compute_Score(menAssess, _
'    '                        '                              True, _
'    '                        '                              Session("ScoringOptionId"), _
'    '                        '                              enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
'    '                        '                              cboEmployee.SelectedValue, _
'    '                        '                              cboPeriod.SelectedValue, , _
'    '                        '                              mdtBSC_Evaluation, cboReviewer.SelectedValue)
'    '                        objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & _
'    '                        objEAnalysisMst.Compute_Score(menAssess, _
'    '                                                      True, _
'    '                                                      Session("ScoringOptionId"), _
'    '                                                      enAssess_Computation_Formulas.BSC_REV_TOTAL_SCORE, _
'    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                                      cboEmployee.SelectedValue, _
'    '                                                      cboPeriod.SelectedValue, , _
'    '                                                      mdtBSC_Evaluation, cboReviewer.SelectedValue, , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'    '                        'Shani(20-Nov-2015) -- End

'    '                    End If
'    '                    objlblValue3.Visible = True : objlblValue4.Visible = True
'    '                End If
'    '                'S.SANDEEP [21 JAN 2015] -- END



'    '            Case dgvGE.ID.ToUpper

'    '                objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
'    '                objlblValue1.Visible = True : objlblValue2.Visible = True

'    '                'S.SANDEEP [21 JAN 2015] -- START
'    '                'If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
'    '                '    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
'    '                '        objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & CDbl(mdtGE_Evaluation.Compute("SUM(dresult)", "AUD <> 'D'")).ToString
'    '                '    End If
'    '                '    objlblValue3.Visible = False : objlblValue4.Visible = False
'    '                'ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'    '                '    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(escore)", "")).ToString
'    '                '    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
'    '                '        objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & CDbl(mdtGE_Evaluation.Compute("SUM(dresult)", "AUD <> 'D'")).ToString
'    '                '    End If
'    '                '    objlblValue3.Visible = True : objlblValue4.Visible = False
'    '                'ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
'    '                '    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(escore)", "")).ToString
'    '                '    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & CDbl(dtGE_TabularGrid.Compute("SUM(ascore)", "")).ToString

'    '                '    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
'    '                '        objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & CDbl(mdtGE_Evaluation.Compute("SUM(dresult)", "AUD <> 'D'")).ToString
'    '                '    End If
'    '                '    objlblValue3.Visible = True : objlblValue4.Visible = True
'    '                'End If

'    '                If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
'    '                    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
'    '                        'Shani(20-Nov-2015) -- Start
'    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

'    '                        'objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'    '                        'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'    '                        '                              False, _
'    '                        '                              Session("ScoringOptionId"), _
'    '                        '                              enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
'    '                        '                              cboEmployee.SelectedValue, _
'    '                        '                              cboPeriod.SelectedValue, , mdtGE_Evaluation)
'    '                        objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'    '                        objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'    '                                                      False, _
'    '                                                      Session("ScoringOptionId"), _
'    '                                                      enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
'    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                                      cboEmployee.SelectedValue, _
'    '                                                      cboPeriod.SelectedValue, , mdtGE_Evaluation, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'    '                        'Shani(20-Nov-2015) -- End

'    '                    End If
'    '                    objlblValue3.Visible = False : objlblValue4.Visible = False
'    '                ElseIf menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'    '                    'Shani(20-Nov-2015) -- Start
'    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

'    '                    'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'    '                    'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'    '                    '                              False, _
'    '                    '                              Session("ScoringOptionId"), _
'    '                    '                              enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
'    '                    '                              cboEmployee.SelectedValue, _
'    '                    '                              cboPeriod.SelectedValue, , dtGE_TabularGrid)
'    '                    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'    '                   objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'    '                                                 False, _
'    '                                                 Session("ScoringOptionId"), _
'    '                                                 enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
'    '                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                                 cboEmployee.SelectedValue, _
'    '                                                 cboPeriod.SelectedValue, , dtGE_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'    '                    'Shani(20-Nov-2015) -- End


'    '                    objlblValue3.Text = ""
'    '                    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then

'    '                        'Shani(20-Nov-2015) -- Start
'    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'    '                        'objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'    '                        'objEAnalysisMst.Compute_Score(menAssess, _
'    '                        '                              False, _
'    '                        '                              Session("ScoringOptionId"), _
'    '                        '                              enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
'    '                        '                              cboEmployee.SelectedValue, cboPeriod.SelectedValue, , _
'    '                        '                              mdtGE_Evaluation, cboAssessor.SelectedValue)
'    '                        objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'    '                        objEAnalysisMst.Compute_Score(menAssess, _
'    '                                                      False, _
'    '                                                      Session("ScoringOptionId"), _
'    '                                                      enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
'    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                                      cboEmployee.SelectedValue, cboPeriod.SelectedValue, , _
'    '                                                      mdtGE_Evaluation, cboAssessor.SelectedValue, , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'    '                        'Shani(20-Nov-2015) -- End

'    '                    End If
'    '                    objlblValue3.Visible = True : objlblValue4.Visible = False
'    '                ElseIf menAssess = enAssessmentMode.REVIEWER_ASSESSMENT Then
'    '                    'Shani(20-Nov-2015) -- Start
'    '                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'    '                    'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'    '                    'objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'    '                    '                              False, _
'    '                    '                              Session("ScoringOptionId"), _
'    '                    '                              enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
'    '                    '                              cboEmployee.SelectedValue, _
'    '                    '                              cboPeriod.SelectedValue, , dtGE_TabularGrid)

'    '                    'If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'    '                    'objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
'    '                    '                              False, _
'    '                    '                              Session("ScoringOptionId"), _
'    '                    '                              enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
'    '                    '                              cboEmployee.SelectedValue, _
'    '                    '                              cboPeriod.SelectedValue, , dtGE_TabularGrid)
'    '                    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'    '                    objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'    '                                                  False, _
'    '                                                  Session("ScoringOptionId"), _
'    '                                                  enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
'    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                                  cboEmployee.SelectedValue, _
'    '                                                  cboPeriod.SelectedValue, , dtGE_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

'    '                    If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'    '                    objEAnalysisMst.Compute_Score(enAssessmentMode.APPRAISER_ASSESSMENT, _
'    '                                                  False, _
'    '                                                  Session("ScoringOptionId"), _
'    '                                                  enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
'    '                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                                  cboEmployee.SelectedValue, _
'    '                                                  cboPeriod.SelectedValue, , dtGE_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

'    '                    'Shani(20-Nov-2015) -- End


'    '                    objlblValue4.Text = ""
'    '                    If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
'    '                        'Shani(20-Nov-2015) -- Start
'    '                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'    '                        'objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & _
'    '                        'objEAnalysisMst.Compute_Score(menAssess, _
'    '                        '                              False, _
'    '                        '                              Session("ScoringOptionId"), _
'    '                        '                              enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
'    '                        '                              cboEmployee.SelectedValue, _
'    '                        '                              cboPeriod.SelectedValue, , _
'    '                        '                              mdtGE_Evaluation, cboReviewer.SelectedValue)
'    '                        objlblValue4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Reviewer Score :") & " " & _
'    '                        objEAnalysisMst.Compute_Score(menAssess, _
'    '                                                      False, _
'    '                                                      Session("ScoringOptionId"), _
'    '                                                      enAssess_Computation_Formulas.CMP_REV_TOTAL_SCORE, _
'    '                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'    '                                                      cboEmployee.SelectedValue, _
'    '                                                      cboPeriod.SelectedValue, , _
'    '                                                      mdtGE_Evaluation, cboReviewer.SelectedValue, , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'    '                        'Shani(20-Nov-2015) -- End
'    '                    End If
'    '                    objlblValue3.Visible = True : objlblValue4.Visible = True
'    '                End If
'    '                'S.SANDEEP [21 JAN 2015] -- END

'    '        End Select
'    '    Catch ex As Exception
'    '        DisplayMessage.DisplayError(ex,Me)
'    '    Finally
'    '    End Try
'    'End Sub
'    Private Sub SetTotals(ByVal iDG As DataGrid)
'        Try

'            'Shani (23-Nov-2016) -- Start
'            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'            'Select Case iDG.ID.ToUpper
'            '    Case dgvBSC.ID.ToUpper
'            '        objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
'            '        objlblValue1.Visible = True : objlblValue2.Visible = True
'            '        If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'            '            If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'            '            objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'            '                                          True, _
'            '                                          Session("ScoringOptionId"), _
'            '                                          enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
'            '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'            '                                          cboEmployee.SelectedValue, _
'            '                                          cboPeriod.SelectedValue, , dtBSC_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

'            '            If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
'            '                objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'            '                objEAnalysisMst.Compute_Score(menAssess, _
'            '                                              True, _
'            '                                              Session("ScoringOptionId"), _
'            '                                              enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
'            '                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'            '                                              cboEmployee.SelectedValue, cboPeriod.SelectedValue, , _
'            '                                              mdtBSC_Evaluation, cboAssessor.SelectedValue, , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'            '            End If
'            '            objlblValue3.Visible = True : objlblValue4.Visible = False
'            '        End If

'            '    Case dgvGE.ID.ToUpper
'            '        objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
'            '        objlblValue1.Visible = True : objlblValue2.Visible = True

'            '        If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'            '            If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'            '            objEAnalysisMst.Compute_Score(enAssessmentMode.SELF_ASSESSMENT, _
'            '                                          False, _
'            '                                          Session("ScoringOptionId"), _
'            '                                          enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
'            '                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'            '                                          cboEmployee.SelectedValue, _
'            '                                          cboPeriod.SelectedValue, , dtGE_TabularGrid, , , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END
'            '            objlblValue3.Text = ""
'            '            If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then

'            '                objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'            '                objEAnalysisMst.Compute_Score(menAssess, _
'            '                                              False, _
'            '                                              Session("ScoringOptionId"), _
'            '                                              enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
'            '                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'            '                                              cboEmployee.SelectedValue, cboPeriod.SelectedValue, , _
'            '                                              mdtGE_Evaluation, cboAssessor.SelectedValue, , Session("Self_Assign_Competencies")) 'S.SANDEEP [08 Jan 2016] -- START {Session("Self_Assign_Competencies")} -- END

'            '            End If
'            '            objlblValue3.Visible = True : objlblValue4.Visible = False
'            '        End If
'            'End Select

'            Select Case iDG.ID.ToUpper
'                Case dgvBSC.ID.ToUpper
'                    objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
'                    objlblValue1.Visible = True : objlblValue2.Visible = True
'                    If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'                        If dtBSC_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'                        objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
'                                                      IsBalanceScoreCard:=True, _
'                                                      xScoreOptId:=Session("ScoringOptionId"), _
'                                                      xCompute_Formula:=enAssess_Computation_Formulas.BSC_EMP_TOTAL_SCORE, _
'                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                      xEmployeeId:=cboEmployee.SelectedValue, _
'                                                      xPeriodId:=cboPeriod.SelectedValue, _
'                                                      xUsedAgreedScore:=Session("IsUseAgreedScore"), _
'                                                      xDataTable:=dtBSC_TabularGrid, _
'                                                      xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

'                        If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
'                            objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'                            objEAnalysisMst.Compute_Score(xAssessMode:=menAssess, _
'                                                          IsBalanceScoreCard:=True, _
'                                                          xScoreOptId:=Session("ScoringOptionId"), _
'                                                          xCompute_Formula:=enAssess_Computation_Formulas.BSC_ASR_TOTAL_SCORE, _
'                                                          xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                          xEmployeeId:=cboEmployee.SelectedValue, _
'                                                          xPeriodId:=cboPeriod.SelectedValue, _
'                                                          xUsedAgreedScore:=Session("IsUseAgreedScore"), _
'                                                          xDataTable:=mdtBSC_Evaluation, _
'                                                          xAssessorReviewerId:=cboAssessor.SelectedValue, _
'                                                          xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
'                        End If
'                        objlblValue3.Visible = True : objlblValue4.Visible = False
'                    End If

'                Case dgvGE.ID.ToUpper
'                    objlblValue1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Total Weight :") & " " & CDbl(iWeightTotal).ToString
'                    objlblValue1.Visible = True : objlblValue2.Visible = True

'                    If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'                        If dtGE_TabularGrid.Rows.Count > 0 Then objlblValue2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Self Score :") & " " & _
'                        objEAnalysisMst.Compute_Score(xAssessMode:=enAssessmentMode.SELF_ASSESSMENT, _
'                                                      IsBalanceScoreCard:=False, _
'                                                      xScoreOptId:=Session("ScoringOptionId"), _
'                                                      xCompute_Formula:=enAssess_Computation_Formulas.CMP_EMP_TOTAL_SCORE, _
'                                                      xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                      xEmployeeId:=cboEmployee.SelectedValue, _
'                                                      xPeriodId:=cboPeriod.SelectedValue, _
'                                                      xUsedAgreedScore:=Session("IsUseAgreedScore"), _
'                                                      xDataTable:=dtGE_TabularGrid, _
'                                                      xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))
'                        objlblValue3.Text = ""
'                        If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then

'                            objlblValue3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Assessor Score :") & " " & _
'                            objEAnalysisMst.Compute_Score(xAssessMode:=menAssess, _
'                                                          IsBalanceScoreCard:=False, _
'                                                          xScoreOptId:=Session("ScoringOptionId"), _
'                                                          xCompute_Formula:=enAssess_Computation_Formulas.CMP_ASR_TOTAL_SCORE, _
'                                                          xEmployeeAsOnDate:=eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                          xEmployeeId:=cboEmployee.SelectedValue, _
'                                                          xPeriodId:=cboPeriod.SelectedValue, _
'                                                          xUsedAgreedScore:=Session("IsUseAgreedScore"), _
'                                                          xDataTable:=mdtGE_Evaluation, _
'                                                          xAssessGrpId:=cboAssessor.SelectedValue, _
'                                                          xSelfAssignedCompetencies:=Session("Self_Assign_Competencies"))

'                        End If
'                        objlblValue3.Visible = True : objlblValue4.Visible = False
'                    End If
'            End Select
'            'Shani (23-Nov123-2016-2016) -- End
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub
'    'Shani (26-Sep-2016) -- End

'    Private Sub PanelVisibility()
'        Try
'            Dim xVal As Integer = -1
'            If iHeaderId >= 0 Then
'                objlblCaption.Text = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString
'                xVal = dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")
'            Else
'                objlblCaption.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Instructions")
'                xVal = -1
'            End If
'            Select Case xVal
'                Case -1 'Instruction
'                    objpnlBSC.Visible = False
'                    objpnlGE.Visible = False
'                    objpnlCItems.Visible = False

'                    objpnlInstruction.Visible = True
'                    objpnlInstruction.Height = Unit.Pixel(500)
'                    objpnlInstruction.Width = Unit.Percentage(100)

'                Case -3  'Balance Score Card
'                    objpnlInstruction.Visible = False
'                    objpnlGE.Visible = False
'                    objpnlCItems.Visible = False

'                    'If mdtBSC_Evaluation.Rows.Count <= 0 Or menAction = enAction.EDIT_ONE Then
'                    Call Fill_BSC_Evaluation()
'                    'End If

'                    objpnlBSC.Visible = True
'                    'objpnlBSC.Height = Unit.Percentage(100)
'                    'objpnlBSC.Width = Unit.Percentage(100)
'                Case -2  'Competencies
'                    objpnlInstruction.Visible = False
'                    objpnlBSC.Visible = False
'                    objpnlCItems.Visible = False

'                    'If mdtGE_Evaluation.Rows.Count <= 0 Or menAction = enAction.EDIT_ONE Then
'                    Call Fill_GE_Evaluation()
'                    'End If
'                    objpnlGE.Visible = True
'                    objpnlGE.Height = Unit.Percentage(100)
'                    objpnlGE.Width = Unit.Percentage(100)

'                Case Else  'Dynamic Custom Headers
'                    objpnlInstruction.Visible = False
'                    objpnlBSC.Visible = False
'                    objpnlGE.Visible = False
'                    Call Fill_Custom_Grid()
'                    Call Fill_Custom_Evaluation_Data()
'                    objpnlCItems.Visible = True
'                    objpnlCItems.Height = Unit.Percentage(100)
'                    objpnlCItems.Width = Unit.Percentage(100)
'            End Select
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Private Function Validation() As Boolean
'        Try
'            If CInt(cboEmployee.SelectedValue) <= 0 Then
'                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 16, "Emplolyee is compulsory information.Please Select Emplolyee."), Me)
'                cboEmployee.Focus()
'                Return False
'            ElseIf CInt(cboPeriod.SelectedValue) <= 0 Then
'                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 17, "Period is compulsory information.Please Select Period."), Me)
'                cboPeriod.Focus()
'                Return False
'            End If

'            'SHANI [09 APR 2015] -- START
'            'Dim dsYr As New DataSet : Dim oCompany As New clsCompany_Master
'            'dsYr = oCompany.GetFinancialYearList(Session("CompanyUnkId"), Session("UserId"), "List", Session("Fin_year"))
'            'If dsYr.Tables("List").Rows.Count > 0 Then
'            '    If dtpAssessdate.GetDate.Date > eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date Or _
'            '       dtpAssessdate.GetDate.Date < eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date Then
'            '        Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 19, "Assessment date should be in between ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("start_date").ToString).Date & _
'            '                               Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 20, " And ") & eZeeDate.convertDate(dsYr.Tables("List").Rows(0).Item("end_date").ToString).Date
'            '        DisplayMessage.DisplayMessage(strMsg, Me)
'            '        dtpAssessdate.Focus()
'            '        Return False
'            '    End If
'            'Else
'            '    If dtpAssessdate.GetDate.Date > Session("fin_enddate") Or _
'            '       dtpAssessdate.GetDate.Date < Session("fin_startdate") Then
'            '        Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 19, "Assessment date should be in between ") & CDate(Session("fin_startdate")) & _
'            '                               Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 20, " And ") & CDate(Session("fin_enddate"))
'            '        DisplayMessage.DisplayMessage(strMsg, Me)
'            '        dtpAssessdate.Focus()
'            '        Return False
'            '    End If
'            'End If
'            Dim objPrd As New clscommom_period_Tran

'            'Shani(20-Nov-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
'            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
'            'Shani(20-Nov-2015) -- End

'            If dtpAssessdate.GetDate <= objPrd._Start_Date.Date Then
'                Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 36, "Sorry, assessment date should be greater than") & " ( " & objPrd._Start_Date.Date.ToShortDateString & " )."
'                DisplayMessage.DisplayMessage(strMsg, Me)
'                dtpAssessdate.Focus()
'                objPrd = Nothing
'                Return False
'            End If
'            objPrd = Nothing
'            'SHANI [09 APR 2015] -- END

'            Return True
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Function

'    Private Function Is_Already_Assessed() As Boolean
'        Try
'            If objEAnalysisMst.isExist(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAssessor.SelectedValue), , Me.ViewState("AssessAnalysisUnkid")) = True Then
'                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry, You have already assessed some items of particular perspective. Please open in edit mode and add new items."), Me, "wPg_AssessorEvaluationList.aspx") 'Shani [23 MAR 2015]-wPg_AssessorEvaluationList.aspx
'                Return False
'            End If

'            Return True
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Function

'    Private Sub ClearForm_Values()
'        Try
'            Me.ViewState("Action") = Nothing
'            Me.ViewState("AssessAnalysisUnkid") = Nothing
'            Me.ViewState("YearUnkid") = Nothing
'            Me.ViewState("Assess") = Nothing
'            Me.Session.Remove("BSC_Evaluation")
'            Me.Session.Remove("GE_Evaluation")
'            Me.Session.Remove("CustomEvaluation")
'            Me.Session.Remove("BSC_TabularGrid")
'            Me.Session.Remove("GE_TabularGrid")
'            Me.Session.Remove("CustomTabularGrid")
'            Me.ViewState("iWeightTotal") = Nothing
'            Me.ViewState("Headers") = Nothing
'            Me.ViewState("iHeaderId") = Nothing
'            Me.ViewState("iExOrdr") = Nothing
'            Me.ViewState("iLinkedFieldId") = Nothing
'            Me.ViewState("iMappingUnkid") = Nothing
'            Me.ViewState("xTotAssignedWeight") = Nothing
'            Me.ViewState("ColIndex") = Nothing
'            Me.ViewState("ItemAddEdit") = Nothing
'            Me.Session.Remove("dtCItems")
'            Me.ViewState("iEditingGUID") = Nothing
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    ''''''''''''''''''''''''''BSC EVOLUTION

'    Private Sub SetBSC_GridCols_Tags()
'        Try
'            Dim objFMst As New clsAssess_Field_Master
'            Dim dFld As New DataSet : dFld.Tables.Add(objFMst.GetFieldsForViewSetting())
'            Dim xcolWidth As Integer = 0
'            If dFld.Tables(0).Rows.Count > 0 Then
'                Dim xCol As DataGridColumn = Nothing
'                Dim iExOrder As Integer = -1
'                For Each xRow As DataRow In dFld.Tables(0).Rows
'                    iExOrder = -1
'                    iExOrder = objFMst.Get_Field_ExOrder(xRow.Item("Id"))
'                    xCol = Nothing
'                    Select Case iExOrder
'                        Case 1

'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'dgvBSC.Columns(0).FooterText = xRow.Item("Id")
'                            'xCol = dgvBSC.Columns(0)
'                            xCol = dgvBSC.Columns(disBSColumn("objdgcolhBSCField1"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                        Case 2
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'dgvBSC.Columns(1).FooterText = xRow.Item("Id")
'                            'xCol = dgvBSC.Columns(1)
'                            xCol = dgvBSC.Columns(disBSColumn("objdgcolhBSCField2"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                        Case 3
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'dgvBSC.Columns(2).FooterText = xRow.Item("Id")
'                            'xCol = dgvBSC.Columns(2)
'                            xCol = dgvBSC.Columns(disBSColumn("objdgcolhBSCField3"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                        Case 4
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'dgvBSC.Columns(3).FooterText = xRow.Item("Id")
'                            'xCol = dgvBSC.Columns(3)
'                            xCol = dgvBSC.Columns(disBSColumn("objdgcolhBSCField4"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                        Case 5
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'dgvBSC.Columns(4).FooterText = xRow.Item("Id")
'                            'xCol = dgvBSC.Columns(4)
'                            xCol = dgvBSC.Columns(disBSColumn("objdgcolhBSCField5"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                        Case 6
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'dgvBSC.Columns(5).FooterText = xRow.Item("Id")
'                            'xCol = dgvBSC.Columns(5)
'                            xCol = dgvBSC.Columns(disBSColumn("objdgcolhBSCField6"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                        Case 7
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'dgvBSC.Columns(6).FooterText = xRow.Item("Id")
'                            'xCol = dgvBSC.Columns(6)
'                            xCol = dgvBSC.Columns(disBSColumn("objdgcolhBSCField7"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                        Case 8
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'dgvBSC.Columns(7).FooterText = xRow.Item("Id")
'                            'xCol = dgvBSC.Columns(7)
'                            xCol = dgvBSC.Columns(disBSColumn("objdgcolhBSCField8"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                    End Select
'                    Select Case xRow.Item("Id")
'                        Case clsAssess_Field_Master.enOtherInfoField.ST_DATE
'                            'dgvBSC.Columns(8).FooterText = xRow.Item("Id")

'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'xCol = dgvBSC.Columns(8)
'                            xCol = dgvBSC.Columns(disBSColumn("dgcolhSDate"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                        Case clsAssess_Field_Master.enOtherInfoField.ED_DATE
'                            'dgvBSC.Columns(9).FooterText = xRow.Item("Id")
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'xCol = dgvBSC.Columns(9)
'                            xCol = dgvBSC.Columns(disBSColumn("dgcolhEDate"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                        Case clsAssess_Field_Master.enOtherInfoField.PCT_COMPLETE
'                            'dgvBSC.Columns(10).FooterText = xRow.Item("Id")

'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'xCol = dgvBSC.Columns(10)
'                            xCol = dgvBSC.Columns(disBSColumn("dgcolhCompleted"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                        Case clsAssess_Field_Master.enOtherInfoField.STATUS
'                            'dgvBSC.Columns(11).FooterText = xRow.Item("Id")
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'xCol = dgvBSC.Columns(11)
'                            xCol = dgvBSC.Columns(disBSColumn("dgcolhStatus"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                            'S.SANDEEP [04 MAR 2015] -- START
'                        Case clsAssess_Field_Master.enOtherInfoField.WEIGHT
'                            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
'                                'Shani (23-Nov-2016) -- Start
'                                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                                'xCol = dgvBSC.Columns(12)
'                                xCol = dgvBSC.Columns(disBSColumn("dgcolhBSCScore"))
'                                'Shani (23-Nov123-2016-2016) -- End
'                            Else
'                                'dgvBSC.Columns(13).FooterText = xRow.Item("Id")
'                                'Shani (23-Nov-2016) -- Start
'                                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                                'xCol = dgvBSC.Columns(13)
'                                xCol = dgvBSC.Columns(disBSColumn("dgcolhBSCWeight"))
'                                'Shani (23-Nov123-2016-2016) -- End
'                            End If
'                        Case clsAssess_Field_Master.enOtherInfoField.SCORE
'                            'dgvBSC.Columns(14).FooterText = xRow.Item("Id")
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'xCol = dgvBSC.Columns(14)
'                            xCol = dgvBSC.Columns(disBSColumn("dgcolheselfBSC"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                            'S.SANDEEP [04 MAR 2015] -- END
'                    End Select
'                    If xCol IsNot Nothing Then
'                        xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
'                        xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
'                        xcolWidth = xcolWidth + objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"))
'                        'S.SANDEEP [04 MAR 2015] -- START
'                        If xRow.Item("Id") = clsAssess_Field_Master.enOtherInfoField.SCORE Then
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'dgvBSC.Columns(16).FooterText = xRow.Item("Id")
'                            'xCol = dgvBSC.Columns(16)
'                            xCol = dgvBSC.Columns(disBSColumn("dgcolhaselfBSC"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                            xCol.HeaderStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
'                            xCol.ItemStyle.Width = Unit.Pixel(objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id")))
'                            xcolWidth = xcolWidth + objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, xRow.Item("Id"))
'                        End If
'                        'S.SANDEEP [04 MAR 2015] -- END
'                    End If
'                Next
'                'S.SANDEEP [04 MAR 2015] -- START
'                'dgvBSC.Columns(16).ItemStyle.Width = Unit.Pixel(105)
'                'S.SANDEEP [04 MAR 2015] -- END
'                dgvBSC.Width = xcolWidth
'            End If
'            objFMst = Nothing
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Private Sub Fill_BSC_Evaluation()
'        Try

'            'Shani (26-Sep-2016) -- Start
'            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'            'dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)), CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), -1)
'            dtBSC_TabularGrid = objEAnalysisMst.Get_BSC_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), Session("CascadingTypeId"), CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)), CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), -1, CBool(Session("EnableBSCAutomaticRating")), CInt(Session("ScoringOptionId")))
'            'Shani (26-Sep-2016) -- End


'            Call SetBSC_GridCols_Tags()

'            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_SCALE_BASED Then
'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                'dgvBSC.Columns(13).Visible = False  'Weight
'                'dgvBSC.Columns(12).Visible = True   'Score
'                'Else
'                dgvBSC.Columns(disBSColumn("dgcolhBSCWeight")).Visible = False  'Weight
'                dgvBSC.Columns(disBSColumn("dgcolhBSCScore")).Visible = True   'Score
'            Else
'                dgvBSC.Columns(disBSColumn("dgcolhBSCScore")).Visible = False  'Score
'                dgvBSC.Columns(disBSColumn("dgcolhBSCWeight")).Visible = True   'Weight
'                'Shani (23-Nov123-2016-2016) -- End
'            End If

'            'Shani (23-Nov-2016) -- Start
'            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'            'If dtBSC_TabularGrid.Columns.Contains("Field1") Then
'            '    dgvBSC.Columns(0).HeaderText = dtBSC_TabularGrid.Columns("Field1").Caption
'            '    dgvBSC.Columns(0).Visible = True
'            'Else
'            '    dtBSC_TabularGrid.Columns.Add("Field1", System.Type.GetType("System.String"))
'            '    dgvBSC.Columns(0).Visible = False
'            'End If

'            'If dtBSC_TabularGrid.Columns.Contains("Field2") Then
'            '    dgvBSC.Columns(1).HeaderText = dtBSC_TabularGrid.Columns("Field2").Caption
'            '    dgvBSC.Columns(1).Visible = True
'            'Else
'            '    dtBSC_TabularGrid.Columns.Add("Field2", System.Type.GetType("System.String"))
'            '    dgvBSC.Columns(1).Visible = False
'            'End If

'            'If dtBSC_TabularGrid.Columns.Contains("Field3") Then
'            '    dgvBSC.Columns(2).HeaderText = dtBSC_TabularGrid.Columns("Field3").Caption
'            '    dgvBSC.Columns(2).Visible = True
'            'Else
'            '    dtBSC_TabularGrid.Columns.Add("Field3", System.Type.GetType("System.String"))
'            '    dgvBSC.Columns(2).Visible = False
'            'End If

'            'If dtBSC_TabularGrid.Columns.Contains("Field4") Then
'            '    dgvBSC.Columns(3).HeaderText = dtBSC_TabularGrid.Columns("Field4").Caption
'            '    dgvBSC.Columns(3).Visible = True
'            'Else
'            '    dtBSC_TabularGrid.Columns.Add("Field4", System.Type.GetType("System.String"))
'            '    dgvBSC.Columns(3).Visible = False
'            'End If

'            'If dtBSC_TabularGrid.Columns.Contains("Field5") Then
'            '    dgvBSC.Columns(4).HeaderText = dtBSC_TabularGrid.Columns("Field5").Caption
'            '    dgvBSC.Columns(4).Visible = True
'            'Else
'            '    dtBSC_TabularGrid.Columns.Add("Field5", System.Type.GetType("System.String"))
'            '    dgvBSC.Columns(4).Visible = False
'            'End If

'            'If dtBSC_TabularGrid.Columns.Contains("Field6") Then
'            '    dgvBSC.Columns(5).HeaderText = dtBSC_TabularGrid.Columns("Field6").Caption
'            '    dgvBSC.Columns(5).Visible = True
'            'Else
'            '    dtBSC_TabularGrid.Columns.Add("Field6", System.Type.GetType("System.String"))
'            '    dgvBSC.Columns(5).Visible = False
'            'End If

'            'If dtBSC_TabularGrid.Columns.Contains("Field7") Then
'            '    dgvBSC.Columns(6).HeaderText = dtBSC_TabularGrid.Columns("Field7").Caption
'            '    dgvBSC.Columns(6).Visible = True
'            'Else
'            '    dtBSC_TabularGrid.Columns.Add("Field7", System.Type.GetType("System.String"))
'            '    dgvBSC.Columns(6).Visible = False
'            'End If

'            'If dtBSC_TabularGrid.Columns.Contains("Field8") Then
'            '    dgvBSC.Columns(7).HeaderText = dtBSC_TabularGrid.Columns("Field8").Caption
'            '    dgvBSC.Columns(7).Visible = True
'            'Else
'            '    dtBSC_TabularGrid.Columns.Add("Field8", System.Type.GetType("System.String"))
'            '    dgvBSC.Columns(7).Visible = False
'            'End If

'            'dgvBSC.Columns(14).HeaderText = dtBSC_TabularGrid.Columns("eself").Caption
'            'dgvBSC.Columns(15).HeaderText = dtBSC_TabularGrid.Columns("eremark").Caption

'            'If dtBSC_TabularGrid.Columns.Contains("aself") Then
'            '    dgvBSC.Columns(16).HeaderText = dtBSC_TabularGrid.Columns("aself").Caption
'            'End If
'            'If dtBSC_TabularGrid.Columns.Contains("aremark") Then
'            '    dgvBSC.Columns(17).HeaderText = dtBSC_TabularGrid.Columns("aremark").Caption
'            'End If


'            ''S.SANDEEP [06 Jan 2016] -- START
'            'If dtBSC_TabularGrid.Columns.Contains("Score") Then
'            '    If dgvBSC.Columns(12).Visible Then
'            '        dgvBSC.Columns(12).HeaderText = dtBSC_TabularGrid.Columns("Score").Caption
'            '    End If
'            'End If
'            ''S.SANDEEP [06 Jan 2016] -- END

'            If dtBSC_TabularGrid.Columns.Contains("Field1") Then
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField1")).HeaderText = dtBSC_TabularGrid.Columns("Field1").Caption
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField1")).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field1", System.Type.GetType("System.String"))
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField1")).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field2") Then
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField2")).HeaderText = dtBSC_TabularGrid.Columns("Field2").Caption
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField2")).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field2", System.Type.GetType("System.String"))
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField2")).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field3") Then
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField3")).HeaderText = dtBSC_TabularGrid.Columns("Field3").Caption
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField3")).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field3", System.Type.GetType("System.String"))
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField3")).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field4") Then
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField4")).HeaderText = dtBSC_TabularGrid.Columns("Field4").Caption
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField4")).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field4", System.Type.GetType("System.String"))
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField4")).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field5") Then
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField5")).HeaderText = dtBSC_TabularGrid.Columns("Field5").Caption
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField5")).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field5", System.Type.GetType("System.String"))
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField5")).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field6") Then
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField6")).HeaderText = dtBSC_TabularGrid.Columns("Field6").Caption
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField6")).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field6", System.Type.GetType("System.String"))
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField6")).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field7") Then
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField7")).HeaderText = dtBSC_TabularGrid.Columns("Field7").Caption
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField7")).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field7", System.Type.GetType("System.String"))
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField7")).Visible = False
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Field8") Then
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField8")).HeaderText = dtBSC_TabularGrid.Columns("Field8").Caption
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField8")).Visible = True
'            Else
'                dtBSC_TabularGrid.Columns.Add("Field8", System.Type.GetType("System.String"))
'                dgvBSC.Columns(disBSColumn("objdgcolhBSCField8")).Visible = False
'            End If

'            dgvBSC.Columns(disBSColumn("dgcolheselfBSC")).HeaderText = dtBSC_TabularGrid.Columns("eself").Caption
'            dgvBSC.Columns(disBSColumn("dgcolheremarkBSC")).HeaderText = dtBSC_TabularGrid.Columns("eremark").Caption

'            If dtBSC_TabularGrid.Columns.Contains("aself") Then
'                dgvBSC.Columns(disBSColumn("dgcolhaselfBSC")).HeaderText = dtBSC_TabularGrid.Columns("aself").Caption
'            End If
'            If dtBSC_TabularGrid.Columns.Contains("aremark") Then
'                dgvBSC.Columns(disBSColumn("dgcolharemarkBSC")).HeaderText = dtBSC_TabularGrid.Columns("aremark").Caption
'            End If

'            If dtBSC_TabularGrid.Columns.Contains("Score") Then
'                If dgvBSC.Columns(disBSColumn("dgcolhBSCScore")).Visible Then
'                    dgvBSC.Columns(disBSColumn("dgcolhBSCScore")).HeaderText = dtBSC_TabularGrid.Columns("Score").Caption
'                End If
'            End If

'            'Shani (23-Nov123-2016-2016) -- End

'            If CStr(Session("ViewTitles_InEvaluation")).Trim.Length > 0 Then
'                Dim iEval() As String = CStr(Session("ViewTitles_InEvaluation")).Split("|")
'                If iEval IsNot Nothing Then
'                    For Each xCol As DataGridColumn In dgvBSC.Columns
'                        If xCol.FooterText IsNot Nothing Then
'                            If IsNumeric(xCol.FooterText) Then
'                                If Array.IndexOf(iEval, xCol.FooterText.ToString) < 0 Then
'                                    xCol.Visible = False
'                                End If
'                            End If
'                        End If
'                    Next
'                End If
'            End If

'            'Shani (23-Nov-2016) -- Start
'            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'            'If dgvBSC.Columns(0).Visible = True Then
'            '    xVal = 1
'            'ElseIf dgvBSC.Columns(1).Visible = True Then
'            '    xVal = 2
'            'ElseIf dgvBSC.Columns(2).Visible = True Then
'            '    xVal = 3
'            'ElseIf dgvBSC.Columns(3).Visible = True Then
'            '    xVal = 4
'            'ElseIf dgvBSC.Columns(4).Visible = True Then
'            '    xVal = 5
'            'ElseIf dgvBSC.Columns(5).Visible = True Then
'            '    xVal = 6
'            'ElseIf dgvBSC.Columns(6).Visible = True Then
'            '    xVal = 7
'            'ElseIf dgvBSC.Columns(7).Visible = True Then
'            '    xVal = 8
'            'End If
'            If dgvBSC.Columns(disBSColumn("objdgcolhBSCField1")).Visible = True Then
'                xVal = 1
'            ElseIf dgvBSC.Columns(disBSColumn("objdgcolhBSCField2")).Visible = True Then
'                xVal = 2
'            ElseIf dgvBSC.Columns(disBSColumn("objdgcolhBSCField3")).Visible = True Then
'                xVal = 3
'            ElseIf dgvBSC.Columns(disBSColumn("objdgcolhBSCField4")).Visible = True Then
'                xVal = 4
'            ElseIf dgvBSC.Columns(disBSColumn("objdgcolhBSCField5")).Visible = True Then
'                xVal = 5
'            ElseIf dgvBSC.Columns(disBSColumn("objdgcolhBSCField6")).Visible = True Then
'                xVal = 6
'            ElseIf dgvBSC.Columns(disBSColumn("objdgcolhBSCField7")).Visible = True Then
'                xVal = 7
'            ElseIf dgvBSC.Columns(disBSColumn("objdgcolhBSCField8")).Visible = True Then
'                xVal = 8
'            End If
'            'Shani (23-Nov123-2016-2016) -- End

'            'S.SANDEEP [07 FEB 2015] -- START
'            Dim xWidth As Integer = 0
'            Dim objFMst As New clsAssess_Field_Master

'            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK)
'            'S.SANDEEP [04 MAR 2015] -- START
'            'If xWidth > 0 Then dgvBSC.Columns(15).ItemStyle.Width = xWidth
'            If xWidth > 0 Then
'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                'dgvBSC.Columns(15).HeaderStyle.Width = Unit.Pixel(xWidth)
'                'dgvBSC.Columns(15).ItemStyle.Width = Unit.Pixel(xWidth)
'                dgvBSC.Columns(disBSColumn("dgcolheremarkBSC")).HeaderStyle.Width = Unit.Pixel(xWidth)
'                dgvBSC.Columns(disBSColumn("dgcolheremarkBSC")).ItemStyle.Width = Unit.Pixel(xWidth)
'                'Shani (23-Nov123-2016-2016) -- End
'            End If
'            'S.SANDEEP [04 MAR 2015] -- END

'            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK)

'            'S.SANDEEP [04 MAR 2015] -- START
'            'If xWidth > 0 Then dgvBSC.Columns(17).ItemStyle.Width = xWidth
'            If xWidth > 0 Then
'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                'dgvBSC.Columns(17).HeaderStyle.Width = Unit.Pixel(xWidth)
'                'dgvBSC.Columns(17).ItemStyle.Width = Unit.Pixel(xWidth)
'                dgvBSC.Columns(disBSColumn("dgcolharemarkBSC")).HeaderStyle.Width = Unit.Pixel(xWidth)
'                dgvBSC.Columns(disBSColumn("dgcolharemarkBSC")).ItemStyle.Width = Unit.Pixel(xWidth)
'                'Shani (23-Nov123-2016-2016) -- End
'            End If
'            'S.SANDEEP [04 MAR 2015] -- END



'            objFMst = Nothing
'            'S.SANDEEP [07 FEB 2015] -- END

'            'SHANI [09 Mar 2015]-START
'            'Enhancement - REDESIGN SELF SERVICE.
'            Dim intTotalWidth As Integer = 0
'            For Each xCol As DataGridColumn In dgvBSC.Columns
'                If xCol.Visible Then
'                    intTotalWidth += xCol.HeaderStyle.Width.Value
'                End If
'            Next
'            For Each xCol As DataGridColumn In dgvBSC.Columns
'                If xCol.Visible AndAlso xCol.HeaderStyle.Width.Value > 0 Then
'                    Dim decColumnWidth As Decimal = xCol.HeaderStyle.Width.Value * 100 / intTotalWidth
'                    xCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
'                    xCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
'                End If
'            Next
'            'Shani (26-Sep-2016) -- Start
'            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'            If menAction <> enAction.EDIT_ONE AndAlso CBool(Session("EnableBSCAutomaticRating")) Then
'                If mdtBSC_Evaluation.Select("AUD = 'A' AND iEmployeeId = '" & cboEmployee.SelectedValue & "'").Count <= 0 Then
'                    Dim dRow As DataRow
'                    For Each dtRow As DataRow In dtBSC_TabularGrid.Select("IsGrp = 'False'")
'                        dRow = mdtBSC_Evaluation.NewRow
'                        dRow.Item("analysistranunkid") = -1
'                        dRow.Item("analysisunkid") = -1
'                        dRow.Item("empfield1unkid") = CInt(dtRow.Item("empfield1unkid"))
'                        dRow.Item("result") = dtRow.Item("aself")
'                        dRow.Item("remark") = ""
'                        dRow.Item("AUD") = "A"
'                        dRow.Item("GUID") = Guid.NewGuid.ToString
'                        dRow.Item("isvoid") = False
'                        dRow.Item("voiduserunkid") = -1
'                        dRow.Item("voiddatetime") = DBNull.Value
'                        dRow.Item("voidreason") = ""
'                        dRow.Item("empfield2unkid") = CInt(dtRow.Item("empfield2unkid"))
'                        dRow.Item("empfield3unkid") = CInt(dtRow.Item("empfield3unkid"))
'                        dRow.Item("empfield4unkid") = CInt(dtRow.Item("empfield4unkid"))
'                        dRow.Item("empfield5unkid") = CInt(dtRow.Item("empfield5unkid"))
'                        dRow.Item("perspectiveunkid") = CInt(dtRow.Item("perspectiveunkid"))
'                        dRow.Item("item_weight") = dtRow.Item("aitem_weight")
'                        dRow.Item("max_scale") = dtRow.Item("amax_scale")
'                        dRow.Item("iPeriodId") = cboPeriod.SelectedValue

'                        Select Case iExOrdr
'                            Case enWeight_Types.WEIGHT_FIELD1
'                                dRow.Item("iItemUnkid") = dRow.Item("empfield1unkid")
'                            Case enWeight_Types.WEIGHT_FIELD2
'                                dRow.Item("iItemUnkid") = dRow.Item("empfield2unkid")
'                            Case enWeight_Types.WEIGHT_FIELD3
'                                dRow.Item("iItemUnkid") = dRow.Item("empfield3unkid")
'                            Case enWeight_Types.WEIGHT_FIELD4
'                                dRow.Item("iItemUnkid") = dRow.Item("empfield4unkid")
'                            Case enWeight_Types.WEIGHT_FIELD5
'                                dRow.Item("iItemUnkid") = dRow.Item("empfield5unkid")
'                        End Select
'                        dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
'                        dRow.Item("iScore") = dtRow.Item("aself")

'                        mdtBSC_Evaluation.Rows.Add(dRow)
'                        mdecMaxScale = 0 : mdecMaxScale = 0
'                    Next
'                End If
'            End If
'            'Shani (26-Sep-2016) -- End



'            dgvBSC.Width = Unit.Percentage(99.5)
'            'SHANI [09 Mar 2015]--END 

'            dgvBSC.DataSource = dtBSC_TabularGrid
'            'S.SANDEEP [01 OCT 2015] -- START
'            iWeightTotal = 0
'            'S.SANDEEP [01 OCT 2015] -- END
'            dgvBSC.DataBind()
'            Call SetTotals(dgvBSC)
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    '''''''''''''''''''''''''' COMPETENCIES/GENERAL EVALUATION METHODS
'    Private Sub Fill_GE_Evaluation()
'        Try
'            'S.SANDEEP [29 JAN 2015] -- START
'            'dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)), CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), Session("ConsiderItemWeightAsNumber"), -1)

'            'Shani(20-Nov-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)), CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), Session("ConsiderItemWeightAsNumber"), -1, Session("Self_Assign_Competencies"))
'            dtGE_TabularGrid = objEAnalysisMst.Get_GE_Evaluation_Data(menAssess, CInt(cboEmployee.SelectedValue), _
'                                                                      CInt(cboPeriod.SelectedValue), _
'                                                                      CInt(IIf(cboAssessor.SelectedValue = "", 0, cboAssessor.SelectedValue)), _
'                                                                      CInt(IIf(cboReviewer.SelectedValue = "", 0, cboReviewer.SelectedValue)), _
'                                                                      Session("ConsiderItemWeightAsNumber"), -1, _
'                                                                      Session("Self_Assign_Competencies"), _
'                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")))
'            'Shani(20-Nov-2015) -- End

'            'S.SANDEEP [29 JAN 2015] -- END
'            dgvGE.AutoGenerateColumns = False
'            If Session("ScoringOptionId") = enScoringOption.SC_SCALE_BASED Then
'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                'dgvGE.Columns(1).Visible = False    'WEIGHT
'                'dgvGE.Columns(2).Visible = True     'SCORE GUIDE
'                dgvGE.Columns(disGEolumn("dgcolhGEWeight")).Visible = False    'WEIGHT
'                dgvGE.Columns(disGEolumn("dgcolhGEScore")).Visible = True     'SCORE GUIDE
'                'Shani (23-Nov123-2016-2016) -- End
'            Else
'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                'dgvGE.Columns(1).Visible = True     'WEIGHT
'                'dgvGE.Columns(2).Visible = False    'SCORE GUIDE
'                dgvGE.Columns(disGEolumn("dgcolhGEWeight")).Visible = True     'WEIGHT
'                dgvGE.Columns(disGEolumn("dgcolhGEScore")).Visible = False    'SCORE GUIDE
'                'Shani (23-Nov123-2016-2016) -- End
'            End If

'            'Shani (23-Nov-2016) -- Start
'            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'            'dgvGE.Columns(4).Visible = CBool(Session("ConsiderItemWeightAsNumber"))
'            'dgvGE.Columns(7).Visible = CBool(Session("ConsiderItemWeightAsNumber"))

'            'dgvGE.Columns(3).HeaderText = dtGE_TabularGrid.Columns("eself").Caption
'            'dgvGE.Columns(5).HeaderText = dtGE_TabularGrid.Columns("eremark").Caption

'            dgvGE.Columns(disGEolumn("objdgcolhedisplayGE")).Visible = CBool(Session("ConsiderItemWeightAsNumber"))
'            dgvGE.Columns(disGEolumn("objdgcolhadisplayGE")).Visible = CBool(Session("ConsiderItemWeightAsNumber"))

'            dgvGE.Columns(disGEolumn("dgcolheselfGE")).HeaderText = dtGE_TabularGrid.Columns("eself").Caption
'            dgvGE.Columns(disGEolumn("dgcolheremarkGE")).HeaderText = dtGE_TabularGrid.Columns("eremark").Caption
'            'Shani (23-Nov123-2016-2016) -- End

'            If dtGE_TabularGrid.Columns.Contains("aself") Then
'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                'dgvGE.Columns(6).HeaderText = dtGE_TabularGrid.Columns("aself").Caption
'                dgvGE.Columns(disGEolumn("dgcolhaselfGE")).HeaderText = dtGE_TabularGrid.Columns("aself").Caption
'                'Shani (23-Nov123-2016-2016) -- End
'            End If
'            If dtGE_TabularGrid.Columns.Contains("aremark") Then
'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                'dgvGE.Columns(8).HeaderText = dtGE_TabularGrid.Columns("aremark").Caption
'                dgvGE.Columns(disGEolumn("dgcolharemarkGE")).HeaderText = dtGE_TabularGrid.Columns("aremark").Caption
'                'Shani (23-Nov123-2016-2016) -- End
'            End If

'            'S.SANDEEP [06 Jan 2016] -- START
'            If dtGE_TabularGrid.Columns.Contains("Score") Then
'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                'If dgvGE.Columns(2).Visible Then
'                '    dgvGE.Columns(2).HeaderText = dtGE_TabularGrid.Columns("Score").Caption
'                'End If
'                If dgvGE.Columns(disGEolumn("dgcolhGEScore")).Visible Then
'                    dgvGE.Columns(disGEolumn("dgcolhGEScore")).HeaderText = dtGE_TabularGrid.Columns("Score").Caption
'                End If
'                'Shani (23-Nov123-2016-2016) -- End
'            End If
'            'S.SANDEEP [06 Jan 2016] -- END


'            'S.SANDEEP [07 FEB 2015] -- START
'            Dim xWidth As Integer = 0
'            Dim objFMst As New clsAssess_Field_Master

'            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.EMP_REMARK)
'            'Shani (23-Nov-2016) -- Start
'            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'            'If xWidth > 0 Then dgvBSC.Columns(8).ItemStyle.Width = xWidth
'            'If xWidth > 0 Then dgvGE.Columns(5).ItemStyle.Width = xWidth
'            If xWidth > 0 Then dgvGE.Columns(disGEolumn("dgcolheremarkGE")).ItemStyle.Width = xWidth
'            'Shani (23-Nov123-2016-2016) -- End

'            xWidth = objFMst.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_EVAL, clsAssess_Field_Master.enOtherInfoField.ASR_REMARK)
'            'Shani (23-Nov-2016) -- Start
'            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'            'If xWidth > 0 Then dgvBSC.Columns(8).ItemStyle.Width = xWidth
'            If xWidth > 0 Then dgvGE.Columns(disGEolumn("dgcolharemarkGE")).ItemStyle.Width = xWidth
'            'Shani (23-Nov123-2016-2016) -- End


'            'S.SANDEEP [07 FEB 2015] -- END

'            dgvGE.DataSource = dtGE_TabularGrid
'            iWeightTotal = 0
'            dgvGE.DataBind()
'            Call SetTotals(dgvGE)
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub


'    '''''''''''''''''''''''''' CUSTOM ITEMS EVALUATION METHODS
'    Private Sub Fill_Custom_Grid(Optional ByVal iFromAddEdit As Boolean = False)
'        Try
'            If iHeaderId >= 0 Then

'                'S.SANDEEP [12 OCT 2016] -- START
'                Dim objCHdr As New clsassess_custom_header
'                objCHdr._Customheaderunkid = dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")
'                mblnIsMatchCompetencyStructure = objCHdr._IsMatch_With_Competency
'                objCHdr = Nothing
'                'S.SANDEEP [12 OCT 2016] -- END

'                If iFromAddEdit = False Then
'                    dtCustomTabularGrid = objEAnalysisMst.Get_Custom_Items_List(CInt(cboPeriod.SelectedValue), CInt(dsHeaders.Tables(0).Rows(iHeaderId).Item("Id")), mintAssessAnalysisUnkid, menAssess, CInt(cboEmployee.SelectedValue), menAction)
'                Else
'                    Dim iRow() As DataRow = dtCustomTabularGrid.Select("periodunkid <= 0")
'                    For i As Integer = 0 To iRow.Length - 1
'                        dtCustomTabularGrid.Rows.Remove(iRow(i))
'                    Next
'                End If
'                If dtCustomTabularGrid.Rows.Count > 0 Then
'                    dgvItems.Columns.Clear()
'                End If
'                Call Add_GridColumns()
'                dgvItems.DataSource = Nothing
'                dgvItems.AutoGenerateColumns = False
'                Dim iColName As String = String.Empty
'                For Each dCol As DataColumn In dtCustomTabularGrid.Columns
'                    iColName = "" : iColName = "obj" & dCol.ColumnName
'                    Dim dgvCol As New BoundField()
'                    dgvCol.FooterText = iColName
'                    dgvCol.ReadOnly = True
'                    dgvCol.DataField = dCol.ColumnName
'                    dgvCol.HeaderText = dCol.Caption
'                    If dgvItems.Columns.Contains(dgvCol) = True Then Continue For
'                    If dCol.Caption.Length <= 0 Then
'                        dgvCol.Visible = False
'                    End If
'                    dgvItems.Columns.Add(dgvCol)
'                    If dgvCol.FooterText = "objHeader_Name" Then dgvCol.Visible = False
'                Next

'                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
'                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
'                Call SetDateFormat()
'                'Pinkal (16-Apr-2016) -- End
'                dgvItems.DataSource = dtCustomTabularGrid
'                dgvItems.DataBind()
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Private Sub Fill_Custom_Evaluation_Data()
'        Try
'            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
'                'S.SANDEEP [12 OCT 2016] -- START
'                'Dim dtEval As DataTable
'                ''S.SANDEEP [10 DEC 2015] -- START
'                ''dtEval = New DataView(mdtCustomEvaluation, "custom_header = '" & dsHeaders.Tables(0).Rows(iHeaderId).Item("Name") & "' AND AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable
'                'dtEval = New DataView(mdtCustomEvaluation, "custom_header = '" & dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString.Replace("'", "''") & "' AND AUD <> 'D'", "", DataViewRowState.CurrentRows).ToTable
'                ''S.SANDEEP [10 DEC 2015] -- END

'                'dtCustomTabularGrid.Rows.Clear()
'                'Dim dFRow As DataRow = Nothing
'                'Dim iCustomId, iNewCustomId As Integer : iCustomId = 0 : iNewCustomId = 0
'                'For Each dRow As DataRow In dtEval.Rows
'                '    If dRow.Item("AUD") = "D" Then Continue For
'                '    If iCustomId <= 0 Then iCustomId = dRow.Item("customitemunkid")
'                '    iNewCustomId = dRow.Item("customitemunkid")
'                '    If iCustomId = iNewCustomId Then
'                '        dFRow = dtCustomTabularGrid.NewRow
'                '        dtCustomTabularGrid.Rows.Add(dFRow)
'                '    End If
'                '    Select Case CInt(dRow.Item("itemtypeid"))
'                '        Case clsassess_custom_items.enCustomType.FREE_TEXT
'                '            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
'                '        Case clsassess_custom_items.enCustomType.SELECTION
'                '            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'                '                Select Case CInt(dRow.Item("selectionmodeid"))
'                '                    Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
'                '                        Dim objCMaster As New clsCommon_Master
'                '                        objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
'                '                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
'                '                        objCMaster = Nothing
'                '                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
'                '                        Dim objCMaster As New clsassess_competencies_master
'                '                        objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
'                '                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
'                '                        objCMaster = Nothing
'                '                    Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
'                '                        Dim objEmpField1 As New clsassess_empfield1_master
'                '                        objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
'                '                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
'                '                        objEmpField1 = Nothing
'                '                End Select
'                '            End If
'                '        Case clsassess_custom_items.enCustomType.DATE_SELECTION
'                '            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'                '                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
'                '            End If
'                '        Case clsassess_custom_items.enCustomType.NUMERIC_DATA
'                '            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'                '                If IsNumeric(dRow.Item("custom_value")) Then
'                '                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
'                '                End If
'                '            End If
'                '    End Select
'                '    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
'                '    dFRow.Item("periodunkid") = dRow.Item("periodunkid")
'                '    dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
'                '    dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
'                '    dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
'                '    dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
'                'Next
'                'If dtCustomTabularGrid.Rows.Count <= 0 Then
'                '    dtCustomTabularGrid.Rows.Add(dtCustomTabularGrid.NewRow)
'                '    dtCustomTabularGrid.Rows(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                'End If
'                'Call Fill_Custom_Grid(True)
'                dtCustomTabularGrid.Rows.Clear()
'                Dim strGUIDArray() As String = Nothing

'                Dim dR = mdtCustomEvaluation.AsEnumerable().Where(Function(x) x.Field(Of String)("custom_header") = dsHeaders.Tables(0).Rows(iHeaderId).Item("Name").ToString And x.Field(Of String)("AUD") <> "D")
'                If dR.Count > 0 Then
'                    strGUIDArray = dR.Cast(Of DataRow).Select(Function(x) x.Field(Of String)("customanalysistranguid")).Distinct().ToArray()
'                End If
'                If strGUIDArray IsNot Nothing AndAlso strGUIDArray.Length > 0 Then
'                    Dim dFRow As DataRow = Nothing
'                    For Each Str As String In strGUIDArray
'                        dFRow = dtCustomTabularGrid.NewRow
'                        dtCustomTabularGrid.Rows.Add(dFRow)
'                        For Each dRow As DataRow In mdtCustomEvaluation.Select("customanalysistranguid = '" & Str & "' AND AUD <> 'D' ")
'                            Select Case CInt(dRow.Item("itemtypeid"))
'                                Case clsassess_custom_items.enCustomType.FREE_TEXT
'                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
'                                Case clsassess_custom_items.enCustomType.SELECTION
'                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'                                        Select Case CInt(dRow.Item("selectionmodeid"))
'                                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE, clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES, clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES
'                                                Dim objCMaster As New clsCommon_Master
'                                                objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
'                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
'                                                objCMaster = Nothing
'                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
'                                                Dim objCMaster As New clsassess_competencies_master
'                                                objCMaster._Competenciesunkid = CInt(dRow.Item("custom_value"))
'                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
'                                                objCMaster = Nothing
'                                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
'                                                Dim objEmpField1 As New clsassess_empfield1_master
'                                                objEmpField1._Empfield1unkid = CInt(dRow.Item("custom_value"))
'                                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objEmpField1._Field_Data
'                                                objEmpField1 = Nothing
'                                        End Select
'                                    End If
'                                Case clsassess_custom_items.enCustomType.DATE_SELECTION
'                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
'                                    End If
'                                Case clsassess_custom_items.enCustomType.NUMERIC_DATA
'                                    If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
'                                        If IsNumeric(dRow.Item("custom_value")) Then
'                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
'                                        End If
'                                    End If
'                            End Select
'                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
'                            dFRow.Item("periodunkid") = dRow.Item("periodunkid")
'                            dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
'                            dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
'                            dFRow.Item("GUID") = dRow.Item("customanalysistranguid")
'                            dFRow.Item("analysisunkid") = dRow.Item("analysisunkid")
'                            dFRow.Item("ismanual") = dRow.Item("ismanual")
'                        Next
'                    Next
'                End If
'                mdtCustomEvaluation.AcceptChanges()
'                If dtCustomTabularGrid.Rows.Count <= 0 Then
'                    dtCustomTabularGrid.Rows.Add(dtCustomTabularGrid.NewRow)
'                    dtCustomTabularGrid.Rows(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                End If
'                Call Fill_Custom_Grid(True)
'                'S.SANDEEP [12 OCT 2016] -- END
'                'Shani (02-Nov-2016) -- Start
'                objlblValue1.Text = "&nbsp"
'                objlblValue2.Text = "&nbsp"
'                objlblValue3.Text = "&nbsp"
'                objlblValue4.Text = "&nbsp"
'                'Shani (02-Nov-2016) -- End
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Private Sub Add_GridColumns()
'        Try
'            '************* ADD
'            Dim iTempField As New TemplateField()
'            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
'            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
'            iTempField.FooterText = "ObjAdd"
'            iTempField.HeaderText = "Add"
'            iTempField.ItemStyle.Width = Unit.Pixel(40)
'            dgvItems.Columns.Add(iTempField)

'            '************* EDIT
'            iTempField = New TemplateField()
'            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
'            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
'            iTempField.FooterText = "objEdit"
'            iTempField.HeaderText = "Edit"
'            iTempField.ItemStyle.Width = Unit.Pixel(40)
'            dgvItems.Columns.Add(iTempField)

'            '************* DELETE
'            iTempField = New TemplateField()
'            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
'            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
'            iTempField.FooterText = "objDelete"
'            iTempField.HeaderText = "Delete"
'            iTempField.ItemStyle.Width = Unit.Pixel(40)
'            dgvItems.Columns.Add(iTempField)

'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Private Sub Generate_Popup_Data(ByVal xHeaderUnkid As Integer)
'        Try

'            'S.SANDEEP [12 OCT 2016] -- START
'            'Dim dtCItems As DataTable
'            'dtCItems = objEAnalysisMst.Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid, menAssess)

'            'If CInt(Me.ViewState("RowIndex")) > -1 AndAlso mstriEditingGUID <> "" Then 'mdtCustomEvaluation
'            '    Dim dtmp() As DataRow = mdtCustomEvaluation.Select("customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D'")
'            '    If dtmp.Length > 0 Then
'            '        For iEdit As Integer = 0 To dtmp.Length - 1
'            '            Dim xRow() As DataRow = dtCItems.Select("customitemunkid = '" & dtmp(iEdit).Item("customitemunkid") & "'")
'            '            If xRow.Length > 0 Then
'            '                Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
'            '                    Case clsassess_custom_items.enCustomType.SELECTION
'            '                        Select Case CInt(dtmp(iEdit).Item("selectionmodeid"))
'            '                            Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
'            '                                xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
'            '                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
'            '                                xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
'            '                            Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
'            '                                xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
'            '                        End Select
'            '                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
'            '                        If dtmp(iEdit).Item("custom_value").ToString.Trim.Length > 0 Then
'            '                            xRow(0).Item("ddate") = eZeeDate.convertDate(dtmp(iEdit).Item("custom_value"))
'            '                        End If
'            '                End Select
'            '                xRow(0).Item("custom_value") = dtmp(iEdit).Item("dispaly_value")
'            '            End If
'            '        Next
'            '    End If
'            'End If
'            Dim dtCItems As DataTable
'            dtCItems = objEAnalysisMst.Get_CItems_ForAddEdit(CInt(cboPeriod.SelectedValue), xHeaderUnkid, menAssess)
'            dtCItems = dtCItems.Select("", "isdefaultentry DESC").CopyToDataTable
'            If mstriEditingGUID <> "" Then
'                Dim strFilter As String = ""
'                If CInt(Me.ViewState("RowIndex")) > -1 Then
'                    strFilter = "customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D'"
'                Else
'                    strFilter = "customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D' AND isdefaultentry = True"
'                End If
'                Dim dtmp() As DataRow = mdtCustomEvaluation.Select(strFilter)
'                If dtmp.Length > 0 Then
'                    For iEdit As Integer = 0 To dtmp.Length - 1
'                        Dim xRow() As DataRow = dtCItems.Select("customitemunkid = '" & dtmp(iEdit).Item("customitemunkid") & "'")
'                        If xRow.Length > 0 Then
'                            Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
'                                Case clsassess_custom_items.enCustomType.SELECTION
'                                    Select Case CInt(dtmp(iEdit).Item("selectionmodeid"))
'                                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
'                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
'                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
'                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
'                                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
'                                            xRow(0).Item("selectedid") = dtmp(iEdit).Item("custom_value")
'                                    End Select
'                                Case clsassess_custom_items.enCustomType.DATE_SELECTION
'                                    If dtmp(iEdit).Item("custom_value").ToString.Trim.Length > 0 Then
'                                        xRow(0).Item("ddate") = eZeeDate.convertDate(dtmp(iEdit).Item("custom_value"))
'                                    End If
'                            End Select
'                            xRow(0).Item("custom_value") = dtmp(iEdit).Item("dispaly_value")
'                        End If
'                    Next
'                End If
'            End If
'            'S.SANDEEP [12 OCT 2016] -- END




'            Me.Session("dtCItems") = dtCItems
'            dgv_Citems.AutoGenerateColumns = False
'            Dim objCHeader As New clsassess_custom_header
'            objCHeader._Customheaderunkid = xHeaderUnkid
'            If Me.ViewState("Header") IsNot Nothing Then
'                Me.ViewState("Header") = objCHeader._Name
'            Else
'                Me.ViewState.Add("Header", objCHeader._Name)
'            End If
'            objCHeader = Nothing
'            dgv_Citems.DataSource = dtCItems
'            dgv_Citems.DataBind()
'            mblnItemAddEdit = True
'            popup_CItemAddEdit.Show()
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    ''''''''''''''''''''''''''''''''''' ADD EDIT DELETE ROWS IN TABLES
'    Private Sub Evaluated_Data_BSC(ByVal iResult As Decimal, ByVal iRemark As String, ByVal iRowIdx As Integer, Optional ByVal isRemarkChanged As Boolean = False) 'S.SANDEEP [21 JAN 2015] -- START {isRemarkChanged} -- END
'        'Private Sub Evaluated_Data_BSC(ByVal iResult As Decimal, ByVal iRemark As String, ByVal iRowIdx As Integer)
'        Try
'            'If Session("BSC_Evaluation") IsNot Nothing Then
'            '    mdtBSC_Evaluation = Session("BSC_Evaluation")
'            'End If
'            'If Session("BSC_TabularGrid") IsNot Nothing Then
'            '    dtBSC_TabularGrid = Session("BSC_TabularGrid")
'            'End If

'            Dim dRow As DataRow = Nothing : Dim dTemp() As DataRow = Nothing

'            'Select Case CInt(Me.ViewState("iExOrdr"))
'            '    Case enWeight_Types.WEIGHT_FIELD1
'            '        dTemp = mdtBSC_Evaluation.Select("empfield1unkid = '" & dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield1unkid").ToString & "' AND AUD <> 'D' ")
'            '    Case enWeight_Types.WEIGHT_FIELD2
'            '        dTemp = mdtBSC_Evaluation.Select("empfield2unkid = '" & dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield2unkid").ToString & "' AND AUD <> 'D' ")
'            '    Case enWeight_Types.WEIGHT_FIELD3
'            '        dTemp = mdtBSC_Evaluation.Select("empfield3unkid = '" & dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield3unkid").ToString & "' AND AUD <> 'D' ")
'            '    Case enWeight_Types.WEIGHT_FIELD4
'            '        dTemp = mdtBSC_Evaluation.Select("empfield4unkid = '" & dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield4unkid").ToString & "' AND AUD <> 'D' ")
'            '    Case enWeight_Types.WEIGHT_FIELD5
'            '        dTemp = mdtBSC_Evaluation.Select("empfield5unkid = '" & dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield5unkid").ToString & "' AND AUD <> 'D' ")
'            'End Select
'            dTemp = GetOldValue_BSC(iRowIdx)

'            'S.SANDEEP [ 17 DEC 2014 ] -- START
'            'If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
'            '    If CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D' ")) > xTotAssignedWeight Then
'            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry you cannot go beyond the total weight assigned."), Me)
'            '        Exit Sub
'            '    End If
'            'End If
'            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_WEIGHTED_BASED Then
'                If mdtBSC_Evaluation IsNot Nothing AndAlso mdtBSC_Evaluation.Rows.Count > 0 Then
'                    If CDbl(mdtBSC_Evaluation.Compute("SUM(result)", "AUD <> 'D' ")) > xTotAssignedWeight Then
'                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry you cannot go beyond the total weight assigned."), Me)
'                        Exit Sub
'                    End If
'                End If
'            End If
'            'S.SANDEEP [ 17 DEC 2014 ] -- END

'            If dTemp IsNot Nothing AndAlso dTemp.Length <= 0 Then
'                dRow = mdtBSC_Evaluation.NewRow
'                dRow.Item("analysistranunkid") = -1
'                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
'                dRow.Item("empfield1unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield1unkid"))
'                If iResult <= 0 Then
'                    dRow.Item("result") = dRow.Item("result")
'                Else
'                    dRow.Item("result") = iResult
'                End If
'                dRow.Item("remark") = iRemark
'                dRow.Item("AUD") = "A"
'                dRow.Item("GUID") = Guid.NewGuid.ToString
'                dRow.Item("isvoid") = False
'                dRow.Item("voiduserunkid") = -1
'                dRow.Item("voiddatetime") = DBNull.Value
'                dRow.Item("voidreason") = ""
'                dRow.Item("empfield2unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield2unkid"))
'                dRow.Item("empfield3unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield3unkid"))
'                dRow.Item("empfield4unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield4unkid"))
'                dRow.Item("empfield5unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield5unkid"))
'                dRow.Item("perspectiveunkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("perspectiveunkid"))
'                'S.SANDEEP [21 JAN 2015] -- START
'                Select Case iExOrdr
'                    Case enWeight_Types.WEIGHT_FIELD1
'                        If dRow.Item("empfield1unkid") <= 0 Then Exit Sub
'                    Case enWeight_Types.WEIGHT_FIELD2
'                        If dRow.Item("empfield2unkid") <= 0 Then Exit Sub
'                    Case enWeight_Types.WEIGHT_FIELD3
'                        If dRow.Item("empfield3unkid") <= 0 Then Exit Sub
'                    Case enWeight_Types.WEIGHT_FIELD4
'                        If dRow.Item("empfield4unkid") <= 0 Then Exit Sub
'                    Case enWeight_Types.WEIGHT_FIELD5
'                        If dRow.Item("empfield5unkid") <= 0 Then Exit Sub
'                End Select
'                If isRemarkChanged = False Then
'                    dRow.Item("item_weight") = Me.ViewState("mdecItemWeight")
'                    dRow.Item("max_scale") = Me.ViewState("mdecMaxScale")
'                End If
'                dRow.Item("iPeriodId") = cboPeriod.SelectedValue
'                Select Case iExOrdr
'                    Case enWeight_Types.WEIGHT_FIELD1
'                        dRow.Item("iItemUnkid") = dRow.Item("empfield1unkid")
'                    Case enWeight_Types.WEIGHT_FIELD2
'                        dRow.Item("iItemUnkid") = dRow.Item("empfield2unkid")
'                    Case enWeight_Types.WEIGHT_FIELD3
'                        dRow.Item("iItemUnkid") = dRow.Item("empfield3unkid")
'                    Case enWeight_Types.WEIGHT_FIELD4
'                        dRow.Item("iItemUnkid") = dRow.Item("empfield4unkid")
'                    Case enWeight_Types.WEIGHT_FIELD5
'                        dRow.Item("iItemUnkid") = dRow.Item("empfield5unkid")
'                End Select
'                dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
'                dRow.Item("iScore") = iResult
'                'S.SANDEEP [21 JAN 2015] -- END
'                mdtBSC_Evaluation.Rows.Add(dRow)
'                'S.SANDEEP [21 JAN 2015] -- START
'                Me.ViewState("mdecItemWeight") = 0 : Me.ViewState("mdecMaxScale") = 0
'                'S.SANDEEP [21 JAN 2015] -- END
'            Else
'                If menAction <> enAction.EDIT_ONE Then
'                    dTemp(0).Item("analysistranunkid") = -1
'                Else
'                    dTemp(0).Item("analysistranunkid") = dTemp(0).Item("analysistranunkid")
'                End If
'                dTemp(0).Item("analysisunkid") = mintAssessAnalysisUnkid
'                dTemp(0).Item("empfield1unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield1unkid"))
'                If iResult <= 0 Then
'                    dTemp(0).Item("result") = dTemp(0).Item("result")
'                Else
'                    dTemp(0).Item("result") = iResult
'                End If
'                dTemp(0).Item("remark") = iRemark
'                If IsDBNull(dTemp(0).Item("AUD")) Or CStr(dTemp(0).Item("AUD")).ToString.Trim = "" Then
'                    dTemp(0).Item("AUD") = "U"
'                End If
'                dTemp(0).Item("GUID") = Guid.NewGuid.ToString
'                dTemp(0).Item("isvoid") = False
'                dTemp(0).Item("voiduserunkid") = -1
'                dTemp(0).Item("voiddatetime") = DBNull.Value
'                dTemp(0).Item("voidreason") = ""
'                dTemp(0).Item("empfield2unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield2unkid"))
'                dTemp(0).Item("empfield3unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield3unkid"))
'                dTemp(0).Item("empfield4unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield4unkid"))
'                dTemp(0).Item("empfield5unkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("empfield5unkid"))
'                dTemp(0).Item("perspectiveunkid") = CInt(dtBSC_TabularGrid.Rows(iRowIdx).Item("perspectiveunkid"))

'                'S.SANDEEP [21 JAN 2015] -- START
'                If isRemarkChanged = False Then
'                    dTemp(0).Item("item_weight") = Me.ViewState("mdecItemWeight")
'                    dTemp(0).Item("max_scale") = Me.ViewState("mdecMaxScale")
'                End If
'                dTemp(0).Item("iPeriodId") = cboPeriod.SelectedValue
'                Select Case iExOrdr
'                    Case enWeight_Types.WEIGHT_FIELD1
'                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield1unkid")
'                    Case enWeight_Types.WEIGHT_FIELD2
'                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield2unkid")
'                    Case enWeight_Types.WEIGHT_FIELD3
'                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield3unkid")
'                    Case enWeight_Types.WEIGHT_FIELD4
'                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield4unkid")
'                    Case enWeight_Types.WEIGHT_FIELD5
'                        dTemp(0).Item("iItemUnkid") = dTemp(0).Item("empfield5unkid")
'                End Select
'                dTemp(0).Item("iEmployeeId") = cboEmployee.SelectedValue
'                dTemp(0).Item("iScore") = iResult
'                'S.SANDEEP [21 JAN 2015] -- END

'                mdtBSC_Evaluation.AcceptChanges()

'                'S.SANDEEP [21 JAN 2015] -- START
'                Me.ViewState("mdecItemWeight") = 0 : Me.ViewState("mdecMaxScale") = 0
'                'S.SANDEEP [21 JAN 2015] -- END
'            End If

'            Call SetTotals(dgvBSC)
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    'Shani (23-Nov-2016) -- Start
'    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'    'Private Sub Evaluated_Data_GE(ByVal iResult As Decimal, ByVal iRemark As String, ByVal iRowIdx As Integer, ByVal xGrdItem As DataGridItem, Optional ByVal isRemarkChanged As Boolean = False) 'S.SANDEEP [21 JAN 2015] -- START {isRemarkChanged} -- END
'    Private Sub Evaluated_Data_GE(ByVal iResult As Decimal, ByVal iRemark As String, ByVal iRowIdx As Integer, ByVal xGrdItem As DataGridItem, ByVal iAgreedScore As Decimal, Optional ByVal isRemarkChanged As Boolean = False)
'        'Shani (23-Nov123-2016-2016) -- End
'        'Private Sub Evaluated_Data_GE(ByVal iResult As Decimal, ByVal iRemark As String, ByVal iRowIdx As Integer, ByVal xGrdItem As DataGridItem)
'        Dim dRow As DataRow = Nothing
'        Dim dTemp() As DataRow = Nothing
'        Try
'            Dim dtTemp() As DataRow = Nothing
'            Dim iDecWeight As Decimal = 0
'            'Shani (23-Nov-2016) -- Start
'            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'            'Decimal.TryParse(CStr(xGrdItem.Cells(1).Text), iDecWeight)  'WEIGHT
'            Decimal.TryParse(CStr(xGrdItem.Cells(disGEolumn("dgcolhGEWeight")).Text), iDecWeight)  'WEIGHT
'            'Shani (23-Nov123-2016-2016) -- End

'            If iDecWeight <= 0 Then iDecWeight = 1
'            If Session("ConsiderItemWeightAsNumber") = False Then iDecWeight = 1

'            'dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdItem.Cells(11).Text) & "' AND competenciesunkid = '" & _
'            '                                                  CInt(xGrdItem.Cells(10).Text) & "' AND AUD <> 'D' ") '10-> competenciesunkid, 11-> assessgroupunkid
'            dtTemp = GetOldValue_GE(xGrdItem)

'            If dtTemp IsNot Nothing AndAlso dtTemp.Length <= 0 Then
'                dRow = mdtGE_Evaluation.NewRow
'                dRow.Item("analysistranunkid") = -1
'                dRow.Item("analysisunkid") = mintAssessAnalysisUnkid

'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                'dRow.Item("assessgroupunkid") = CInt(xGrdItem.Cells(11).Text)    'assessgroupunkid
'                'dRow.Item("competenciesunkid") = CInt(xGrdItem.Cells(10).Text)   'competenciesunkid
'                dRow.Item("assessgroupunkid") = CInt(xGrdItem.Cells(disGEolumn("objdgcolhassessgroupunkidGE")).Text)    'assessgroupunkid
'                dRow.Item("competenciesunkid") = CInt(xGrdItem.Cells(disGEolumn("objdgcolhcompetenciesunkidGE")).Text)   'competenciesunkid
'                'Shani (23-Nov123-2016-2016) -- End

'                dRow.Item("result") = iResult
'                dRow.Item("remark") = iRemark
'                dRow.Item("AUD") = "A"
'                dRow.Item("GUID") = Guid.NewGuid.ToString
'                dRow.Item("isvoid") = False
'                dRow.Item("voiduserunkid") = -1
'                dRow.Item("voiddatetime") = DBNull.Value
'                dRow.Item("voidreason") = ""
'                dRow.Item("dresult") = iResult * iDecWeight
'                'S.SANDEEP [21 JAN 2015] -- START
'                If isRemarkChanged = False Then
'                    dRow.Item("item_weight") = Me.ViewState("mdecItemWeight")
'                    dRow.Item("max_scale") = Me.ViewState("mdecMaxScale")
'                End If
'                dRow.Item("iPeriodId") = cboPeriod.SelectedValue
'                dRow.Item("iEmployeeId") = cboEmployee.SelectedValue
'                dRow.Item("iScore") = iResult
'                dRow.Item("iItemUnkid") = dRow.Item("competenciesunkid")
'                'S.SANDEEP [21 JAN 2015] -- END

'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                dRow.Item("agreed_score") = iAgreedScore
'                'Shani (23-Nov-2016) -- End

'                mdtGE_Evaluation.Rows.Add(dRow)
'                'S.SANDEEP [21 JAN 2015] -- START
'                Me.ViewState("mdecItemWeight") = 0 : Me.ViewState("mdecMaxScale") = 0
'                'S.SANDEEP [21 JAN 2015] -- END
'            Else
'                dtTemp(0).Item("analysistranunkid") = dtTemp(0).Item("analysistranunkid")
'                dtTemp(0).Item("analysisunkid") = mintAssessAnalysisUnkid

'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                'dtTemp(0).Item("assessgroupunkid") = CInt(xGrdItem.Cells(11).Text)    'assessgroupunkid
'                'dtTemp(0).Item("competenciesunkid") = CInt(xGrdItem.Cells(10).Text)   'competenciesunkid
'                dtTemp(0).Item("assessgroupunkid") = CInt(xGrdItem.Cells(disGEolumn("objdgcolhassessgroupunkidGE")).Text)    'assessgroupunkid
'                dtTemp(0).Item("competenciesunkid") = CInt(xGrdItem.Cells(disGEolumn("objdgcolhcompetenciesunkidGE")).Text)   'competenciesunkid
'                'Shani (23-Nov123-2016-2016) -- End

'                dtTemp(0).Item("result") = iResult
'                dtTemp(0).Item("remark") = iRemark
'                If IsDBNull(dtTemp(0).Item("AUD")) Or CStr(dtTemp(0).Item("AUD")).ToString.Trim = "" Then
'                    dtTemp(0).Item("AUD") = "U"
'                End If
'                dtTemp(0).Item("GUID") = Guid.NewGuid.ToString
'                dtTemp(0).Item("isvoid") = False
'                dtTemp(0).Item("voiduserunkid") = -1
'                dtTemp(0).Item("voiddatetime") = DBNull.Value
'                dtTemp(0).Item("voidreason") = ""
'                dtTemp(0).Item("dresult") = iResult * iDecWeight

'                'S.SANDEEP [21 JAN 2015] -- START
'                If isRemarkChanged = False Then
'                    dtTemp(0).Item("item_weight") = Me.ViewState("mdecItemWeight")
'                    dtTemp(0).Item("max_scale") = Me.ViewState("mdecMaxScale")
'                End If
'                dtTemp(0).Item("iPeriodId") = cboPeriod.SelectedValue
'                dtTemp(0).Item("iEmployeeId") = cboEmployee.SelectedValue
'                dtTemp(0).Item("iScore") = iResult
'                dtTemp(0).Item("iItemUnkid") = dtTemp(0).Item("competenciesunkid")
'                'S.SANDEEP [21 JAN 2015] -- END

'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                dtTemp(0).Item("agreed_score") = iAgreedScore
'                'Shani (23-Nov-2016) -- End

'                dtTemp(0).AcceptChanges()

'                'S.SANDEEP [21 JAN 2015] -- START
'                Me.ViewState("mdecItemWeight") = 0 : Me.ViewState("mdecMaxScale") = 0
'                'S.SANDEEP [21 JAN 2015] -- END
'            End If

'            'S.SANDEEP [ 17 DEC 2014 ] -- START
'            'If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
'            '    Dim objAGroup As New clsassess_group_master
'            '    objAGroup._Assessgroupunkid = CInt(xGrdItem.Cells(11).Text)
'            '    If CBool(Session("ConsiderItemWeightAsNumber")) = True Then
'            '        If CDbl(mdtGE_Evaluation.Compute("SUM(dresult)", "assessgroupunkid = '" & CInt(xGrdItem.Cells(11).Text) & "' AND AUD <> 'D' ")) > objAGroup._Weight Then
'            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 27, "Sorry you cannot go beyond the total weight assigned for the particular assessment group weight."), Me)

'            '            dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdItem.Cells(11).Text) & "' AND competenciesunkid = '" & _
'            '                                                      CInt(xGrdItem.Cells(10).Text) & "' AND AUD <> 'D' ") '10-> competenciesunkid, 11-> assessgroupunkid

'            '            If dtTemp.Length > 0 Then
'            '                dtTemp(0).Item("result") = 0 : CType(xGrdItem.Cells(3).Controls(1), TextBox).Text = 0
'            '                dTemp(0).Item("dresult") = 0
'            '            End If
'            '            Exit Sub
'            '        End If
'            '    Else
'            '        If CDbl(mdtGE_Evaluation.Compute("SUM(result)", "assessgroupunkid = '" & CInt(xGrdItem.Cells(11).Text) & "' AND AUD <> 'D' ")) > objAGroup._Weight Then
'            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 27, "Sorry you cannot go beyond the total weight assigned for the particular assessment group weight."), Me)

'            '            dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdItem.Cells(11).Text) & "' AND competenciesunkid = '" & _
'            '                                                      CInt(xGrdItem.Cells(10).Text) & "' AND AUD <> 'D' ") '10-> competenciesunkid, 11-> assessgroupunkid

'            '            If dtTemp.Length > 0 Then
'            '                dtTemp(0).Item("result") = 0 : CType(xGrdItem.Cells(3).Controls(1), TextBox).Text = 0
'            '                dTemp(0).Item("dresult") = 0
'            '            End If
'            '            Exit Sub
'            '        End If
'            '    End If
'            '    objAGroup = Nothing
'            'End If
'            If CInt(Session("ScoringOptionId")) = enScoringOption.SC_WEIGHTED_BASED Then
'                If mdtGE_Evaluation IsNot Nothing AndAlso mdtGE_Evaluation.Rows.Count > 0 Then
'                    Dim objAGroup As New clsassess_group_master

'                    'Shani (23-Nov-2016) -- Start
'                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    'objAGroup._Assessgroupunkid = CInt(xGrdItem.Cells(11).Text)
'                    objAGroup._Assessgroupunkid = CInt(xGrdItem.Cells(disGEolumn("objdgcolhassessgroupunkidGE")).Text)
'                    'Shani (23-Nov123-2016-2016) -- End

'                    'Shani (23-Nov-2016) -- Start
'                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    'If CBool(Session("ConsiderItemWeightAsNumber")) = True Then
'                    '    'Shani (23-Nov-2016) -- Start
'                    '    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    '    'If CDbl(mdtGE_Evaluation.Compute("SUM(dresult)", "assessgroupunkid = '" & CInt(xGrdItem.Cells(11).Text) & "' AND AUD <> 'D' ")) > objAGroup._Weight Then
'                    '    If CDbl(mdtGE_Evaluation.Compute("SUM(dresult)", "assessgroupunkid = '" & CInt(xGrdItem.Cells(disGEolumn("objdgcolhassessgroupunkidGE")).Text) & "' AND AUD <> 'D' ")) > objAGroup._Weight Then
'                    '        'Shani (23-Nov123-2016-2016) -- End

'                    '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 27, "Sorry you cannot go beyond the total weight assigned for the particular assessment group weight."), Me)
'                    '        'Shani (23-Nov-2016) -- Start
'                    '        'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    '        'dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdItem.Cells(11).Text) & "' AND competenciesunkid = '" & _
'                    '        'CInt(xGrdItem.Cells(10).Text) & "' AND AUD <> 'D' ")
'                    '        dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdItem.Cells(disGEolumn("objdgcolhassessgroupunkidGE")).Text) & "' AND " & _
'                    '                                         "competenciesunkid = '" & CInt(xGrdItem.Cells(disGEolumn("objdgcolhcompetenciesunkidGE")).Text) & "' AND AUD <> 'D' ")
'                    '        'Shani (23-Nov123-2016-2016) -- End
'                    '        '10-> competenciesunkid, 
'                    '        '11-> assessgroupunkid

'                    '        If dtTemp.Length > 0 Then
'                    '            'Shani (23-Nov-2016) -- Start
'                    '            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    '            'dtTemp(0).Item("result") = 0 : CType(xGrdItem.Cells(3).Controls(1), TextBox).Text = 0
'                    '            dtTemp(0).Item("result") = 0 : CType(xGrdItem.Cells(3).Controls(1), TextBox).Text = 0
'                    '            'Shani (23-Nov123-2016-2016) -- End
'                    '            dTemp(0).Item("dresult") = 0
'                    '        End If
'                    '        Exit Sub
'                    '    End If
'                    'Else
'                    '    If CDbl(mdtGE_Evaluation.Compute("SUM(result)", "assessgroupunkid = '" & CInt(xGrdItem.Cells(11).Text) & "' AND AUD <> 'D' ")) > objAGroup._Weight Then
'                    '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 27, "Sorry you cannot go beyond the total weight assigned for the particular assessment group weight."), Me)

'                    '        dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdItem.Cells(11).Text) & "' AND competenciesunkid = '" & _
'                    '                                                  CInt(xGrdItem.Cells(10).Text) & "' AND AUD <> 'D' ") '10-> competenciesunkid, 11-> assessgroupunkid

'                    '        If dtTemp.Length > 0 Then
'                    '            dtTemp(0).Item("result") = 0 : CType(xGrdItem.Cells(3).Controls(1), TextBox).Text = 0
'                    '            dTemp(0).Item("dresult") = 0
'                    '        End If
'                    '        Exit Sub
'                    '    End If
'                    'End If

'                    If CDbl(mdtGE_Evaluation.Compute("SUM(result)", "assessgroupunkid = '" & CInt(xGrdItem.Cells(disGEolumn("objdgcolhassessgroupunkidGE")).Text) & "' AND AUD <> 'D' ")) > objAGroup._Weight Then
'                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 27, "Sorry you cannot go beyond the total weight assigned for the particular assessment group weight."), Me)

'                        dtTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdItem.Cells(disGEolumn("objdgcolhassessgroupunkidGE")).Text) & "' AND competenciesunkid = '" & _
'                                                                  CInt(xGrdItem.Cells(disGEolumn("objdgcolhcompetenciesunkidGE")).Text) & "' AND AUD <> 'D' ") '10-> competenciesunkid, 11-> assessgroupunkid

'                        If dtTemp.Length > 0 Then
'                            dtTemp(0).Item("result") = 0 : CType(xGrdItem.Cells(disGEolumn("dgcolhaselfGE")).Controls(1), TextBox).Text = 0
'                            dTemp(0).Item("dresult") = 0
'                        End If
'                        Exit Sub
'                    End If
'                    'Shani (23-Nov123-2016-2016) -- End
'                    objAGroup = Nothing
'                End If
'            End If
'            'S.SANDEEP [ 17 DEC 2014 ] -- END


'            'Shani (23-Nov-2016) -- Start
'            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'            'If CBool(Session("ConsiderItemWeightAsNumber")) = True Then
'            '    If iDecWeight > 0 Then
'            '        xGrdItem.Cells(7).Text = iDecWeight * iResult
'            '    End If
'            'End If
'            'Shani (23-Nov123-2016-2016) -- End

'            Call SetTotals(dgvGE)
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Private Function isAll_Assessed_BSC(Optional ByVal blnFlag As Boolean = True) As Boolean
'        Try
'            If mdtBSC_Evaluation.Rows.Count <= 0 Then
'                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 22, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), Me)
'                Return False
'            End If
'            Dim dtmp1() As DataRow = Nothing
'            Dim dtmp2() As DataRow = Nothing

'            Dim dView As DataView = dtBSC_TabularGrid.DefaultView

'            Select Case iExOrdr
'                Case enWeight_Types.WEIGHT_FIELD1
'                    'S.SANDEEP [ 17 DEC 2014 ] -- START
'                    'dtmp1 = dView.ToTable(True, "empfield1unkid").Select("empfield1unkid > 0")
'                    'dtmp2 = mdtBSC_Evaluation.Select("empfield1unkid > 0")

'                    dtmp1 = dView.ToTable(True, "empfield1unkid", "Weight").Select("empfield1unkid > 0 AND Weight <> '' ")
'                    dtmp2 = mdtBSC_Evaluation.Select("empfield1unkid > 0")
'                    'S.SANDEEP [ 17 DEC 2014 ] -- END

'                Case enWeight_Types.WEIGHT_FIELD2
'                    'S.SANDEEP [ 17 DEC 2014 ] -- START
'                    'dtmp1 = dView.ToTable(True, "empfield2unkid").Select("empfield2unkid > 0")
'                    'dtmp2 = mdtBSC_Evaluation.Select("empfield2unkid > 0")

'                    dtmp1 = dView.ToTable(True, "empfield2unkid", "Weight").Select("empfield2unkid > 0 AND Weight <> '' ")
'                    dtmp2 = mdtBSC_Evaluation.Select("empfield2unkid > 0")
'                    'S.SANDEEP [ 17 DEC 2014 ] -- END

'                Case enWeight_Types.WEIGHT_FIELD3
'                    'S.SANDEEP [ 17 DEC 2014 ] -- START
'                    'dtmp1 = dView.ToTable(True, "empfield3unkid").Select("empfield3unkid > 0")
'                    'dtmp2 = mdtBSC_Evaluation.Select("empfield3unkid > 0")

'                    dtmp1 = dView.ToTable(True, "empfield3unkid", "Weight").Select("empfield3unkid > 0 AND Weight <> '' ")
'                    dtmp2 = mdtBSC_Evaluation.Select("empfield3unkid > 0")
'                    'S.SANDEEP [ 17 DEC 2014 ] -- END

'                Case enWeight_Types.WEIGHT_FIELD4
'                    'S.SANDEEP [ 17 DEC 2014 ] -- START
'                    'dtmp1 = dView.ToTable(True, "empfield4unkid").Select("empfield4unkid > 0")
'                    'dtmp2 = mdtBSC_Evaluation.Select("empfield4unkid > 0")

'                    dtmp1 = dView.ToTable(True, "empfield4unkid", "Weight").Select("empfield4unkid > 0 AND Weight <> '' ")
'                    dtmp2 = mdtBSC_Evaluation.Select("empfield4unkid > 0")
'                    'S.SANDEEP [ 17 DEC 2014 ] -- END

'                Case enWeight_Types.WEIGHT_FIELD5
'                    'S.SANDEEP [ 17 DEC 2014 ] -- START
'                    'dtmp1 = dView.ToTable(True, "empfield5unkid").Select("empfield5unkid > 0")
'                    'dtmp2 = mdtBSC_Evaluation.Select("empfield5unkid > 0")

'                    dtmp1 = dView.ToTable(True, "empfield5unkid", "Weight").Select("empfield5unkid > 0 AND Weight <> '' ")
'                    dtmp2 = mdtBSC_Evaluation.Select("empfield5unkid > 0")
'                    'S.SANDEEP [ 17 DEC 2014 ] -- END

'            End Select

'            If dtmp1.Length <> dtmp2.Length Then
'                If blnFlag = True Then
'                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 22, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some objective."), Me)
'                    Return False
'                    'Shani(30-MAR-2016) -- Start
'                Else
'                    Return False
'                    'Shani(30-MAR-2016) -- End
'                End If
'            End If
'            Return True
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Function

'    Private Function isAll_Assessed_GE(Optional ByVal blnFlag As Boolean = True) As Boolean
'        Try
'            If mdtGE_Evaluation.Rows.Count <= 0 Then
'                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 23, "Sorry, you cannot do Save Commit Operation as you have not assessed all item(s) in some assessment group."), Me)
'                Return False
'            End If
'            Dim dtmp1() As DataRow = Nothing
'            Dim dtmp2() As DataRow = Nothing
'            Dim dView As DataView = dtGE_TabularGrid.DefaultView
'            Dim dMView As DataView = mdtGE_Evaluation.DefaultView
'            dtmp1 = dView.ToTable(True, "assessgroupunkid").Select("assessgroupunkid > 0")
'            dtmp2 = dMView.ToTable(True, "assessgroupunkid").Select("assessgroupunkid > 0")
'            If dtmp1.Length <> dtmp2.Length Then
'                If blnFlag = True Then
'                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 24, "Sorry, you cannot do Save Commit Operation as you have not assessed all assessment group."), Me)
'                    Return False
'                End If
'            End If
'            dtmp1 = dView.ToTable.Select("assessgroupunkid > 0 AND IsGrp = False AND IsPGrp = False")
'            'dtmp2 = dMView.ToTable.Select("assessgroupunkid > 0 AND result <> 0 ")
'            dtmp2 = dMView.ToTable.Select("assessgroupunkid > 0")
'            If dtmp1.Length <> dtmp2.Length Then
'                If blnFlag = True Then
'                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 25, "Sorry, you cannot do Save Commit Operation as you have not assessed all item in assessment group."), Me)
'                    Return False
'                    'Shani(30-MAR-2016) -- Start
'                Else
'                    Return False
'                    'Shani(30-MAR-2016) -- End
'                End If
'            End If
'            Return True
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Function

'    Private Function GetOldValue_BSC(ByVal xRowIndex As Integer) As DataRow()
'        Dim xTemp() As DataRow = Nothing
'        Try
'            If mdtBSC_Evaluation IsNot Nothing Then
'                Select Case CInt(Me.ViewState("iExOrdr"))
'                    Case enWeight_Types.WEIGHT_FIELD1
'                        xTemp = mdtBSC_Evaluation.Select("empfield1unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield1unkid").ToString & "' AND AUD <> 'D' ")
'                    Case enWeight_Types.WEIGHT_FIELD2
'                        xTemp = mdtBSC_Evaluation.Select("empfield2unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield2unkid").ToString & "' AND AUD <> 'D' ")
'                    Case enWeight_Types.WEIGHT_FIELD3
'                        xTemp = mdtBSC_Evaluation.Select("empfield3unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield3unkid").ToString & "' AND AUD <> 'D' ")
'                    Case enWeight_Types.WEIGHT_FIELD4
'                        xTemp = mdtBSC_Evaluation.Select("empfield4unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield4unkid").ToString & "' AND AUD <> 'D' ")
'                    Case enWeight_Types.WEIGHT_FIELD5
'                        xTemp = mdtBSC_Evaluation.Select("empfield5unkid = '" & dtBSC_TabularGrid.Rows(xRowIndex).Item("empfield5unkid").ToString & "' AND AUD <> 'D' ")
'                End Select
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'        Return xTemp
'    End Function

'    Private Function GetOldValue_GE(ByVal xGrdRow As DataGridItem) As DataRow()
'        Dim xTemp() As DataRow = Nothing
'        Try
'            If mdtGE_Evaluation IsNot Nothing Then
'                xTemp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(xGrdRow.Cells(11).Text) & "' AND competenciesunkid = '" & _
'                                                              CInt(xGrdRow.Cells(10).Text) & "' AND AUD <> 'D' ")
'            End If
'        Catch ex As Exception
'            Throw ex
'        End Try
'        Return xTemp
'    End Function
'#End Region

'#Region "Button Event"
'    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
'        Try
'            'S.SANDEEP [03 OCT 2015] -- START
'            'If CInt(cboEmployee.SelectedValue) <= 0 Or _
'            '   CInt(cboPeriod.SelectedValue) <= 0 Then
'            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or _
'              CInt(cboPeriod.SelectedValue) <= 0 Then
'                'S.SANDEEP [03 OCT 2015] -- END
'                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Please set following information [Employee,Period] to view assessment."), Me)
'                Exit Sub
'            End If

'            'S.SANDEEP [04 JUN 2015] -- START
'            If CBool(Session("AllowAssessor_Before_Emp")) = False Then
'                If objEAnalysisMst.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , -1) = False Then
'                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Sorry, You cannot do assessment for the particular employee for the selected period." & vbCrLf & _
'                                                                "Reason: As per the setting set in configuration,You are not allowed to do assessment unless employee do his self assessment for the selected period."), Me, "wPg_AssessorEvaluationList.aspx")
'                    Exit Sub
'                End If
'            End If

'            Dim objPrd As New clscommom_period_Tran

'            'Shani(20-Nov-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
'            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
'            'Shani(20-Nov-2015) -- End

'            If dtpAssessdate.GetDate <= objPrd._Start_Date.Date Then
'                Dim strMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 36, "Sorry, assessment date should be greater than") & " ( " & objPrd._Start_Date.Date.ToShortDateString & " )."
'                DisplayMessage.DisplayMessage(strMsg, Me)
'                dtpAssessdate.Focus()
'                objPrd = Nothing
'                Exit Sub
'            End If
'            objPrd = Nothing

'            If CBool(Session("AllowAssessor_Before_Emp")) = False Then
'                Dim strMsg As String = String.Empty
'                If menAssess <> enAssessmentMode.SELF_ASSESSMENT Then
'                    strMsg = objEAnalysisMst.IsValidAssessmentDate(enAssessmentMode.APPRAISER_ASSESSMENT, dtpAssessdate.GetDate, CInt(cboPeriod.SelectedValue), CInt(cboEmployee.SelectedValue))
'                    If strMsg <> "" Then
'                        DisplayMessage.DisplayMessage(strMsg, Me)
'                        Exit Sub
'                    End If
'                End If
'            End If
'            'S.SANDEEP [04 JUN 2015] -- END

'            If dsHeaders.Tables(0).Rows.Count > 0 Then
'                objbtnNext.Enabled = True : iHeaderId = -1 : objbtnNext_Click(sender, e)
'                objbtnBack.Enabled = True
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub objbtnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnNext.Click
'        Try
'            If dsHeaders.Tables(0).Rows.Count > 0 Then
'                If iHeaderId < dsHeaders.Tables(0).Rows.Count - 1 Then
'                    iHeaderId += 1
'                    objbtnBack.Enabled = True
'                End If
'                If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
'                    objbtnNext.Enabled = False
'                End If
'                Call PanelVisibility()
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub objbtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnBack.Click
'        Try
'            If dsHeaders.Tables(0).Rows.Count > 0 Then
'                iHeaderId = iHeaderId - 1
'                If iHeaderId <= -1 Then
'                    objbtnBack.Enabled = False
'                    objbtnNext.Enabled = True
'                End If
'                If iHeaderId >= dsHeaders.Tables(0).Rows.Count - 1 Then
'                    objbtnNext.Enabled = False
'                Else
'                    objbtnNext.Enabled = True
'                End If
'                Call PanelVisibility()
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Protected Sub btnIClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIClose.Click
'        Try
'            mblnItemAddEdit = False
'            popup_CItemAddEdit.Hide()
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub btnIAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIAdd.Click
'        Try
'            If Session("dtCItems") IsNot Nothing Then
'                dtCItems = Session("dtCItems")
'            End If
'            Dim dgv As DataGridItem = Nothing
'            For Each xRow As DataRow In dtCItems.Rows
'                For i As Integer = 0 To dgv_Citems.Items.Count - 1
'                    dgv = dgv_Citems.Items(i)
'                    Select Case xRow("itemtypeid")
'                        Case clsassess_custom_items.enCustomType.FREE_TEXT
'                            If dgv.FindControl("txt" & xRow("customitemunkid")) IsNot Nothing Then
'                                Dim xtxt As TextBox = CType(dgv.FindControl("txt" & xRow("customitemunkid")), TextBox)
'                                xRow.Item("custom_value") = xtxt.Text
'                                Exit For
'                            End If
'                        Case clsassess_custom_items.enCustomType.SELECTION
'                            If dgv.FindControl("cbo" & xRow("customitemunkid")) IsNot Nothing Then
'                                Dim xCbo As DropDownList = CType(dgv.FindControl("cbo" & xRow("customitemunkid")), DropDownList)
'                                If xCbo.SelectedValue > 0 Then
'                                    xRow.Item("custom_value") = xCbo.SelectedValue
'                                    xRow.Item("selectedid") = xCbo.SelectedValue
'                                    Exit For
'                                End If
'                            End If
'                        Case clsassess_custom_items.enCustomType.DATE_SELECTION
'                            If dgv.FindControl("dtp" & xRow("customitemunkid")) IsNot Nothing Then
'                                If CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).IsNull = False Then
'                                    xRow.Item("custom_value") = CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate
'                                    xRow.Item("ddate") = CType(dgv.FindControl("dtp" & xRow("customitemunkid")), Controls_DateCtrl).GetDate.Date
'                                    Exit For
'                                End If
'                            End If
'                        Case clsassess_custom_items.enCustomType.NUMERIC_DATA
'                            If dgv.FindControl("txtnum" & xRow("customitemunkid")) IsNot Nothing Then
'                                Dim xtxt As TextBox = CType(dgv.FindControl("txtnum" & xRow("customitemunkid").ToString()), TextBox)
'                                xRow.Item("custom_value") = xtxt.Text
'                                Exit For
'                            End If
'                    End Select
'                Next
'            Next
'            Dim dtmp() As DataRow = Nothing
'            If dtCItems IsNot Nothing Then
'                dtCItems.AcceptChanges()

'                'Shani(24-Feb-2016) -- Start
'                'dtmp = dtCItems.Select("custom_value <> ''")
'                'If dtmp.Length <= 0 Then
'                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please enter value for atleast one custom item in order to add."), Me)
'                '    Exit Sub
'                'End If

'                dtmp = dtCItems.Select("custom_value = '' AND rOnly = FALSE ")
'                Dim strMsg As String = ""

'                If dtmp.Length > 0 Then
'                    For Each dRow As DataRow In dtmp
'                        strMsg &= " \n * " & dRow.Item("custom_item").ToString
'                    Next
'                End If

'                If strMsg.Trim.Length > 0 Then
'                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmAddCustomValue", 3, "Sorry, Please enter all mandatory information mentioned below in order to save.") & strMsg, Me)
'                    Exit Sub
'                End If
'                'Shani(24-Feb-2016) -- End

'                dtmp = dtCItems.Select("")
'            End If
'            If dtmp.Length > 0 Then
'                If CInt(Me.ViewState("RowIndex")) <= -1 Then
'                    Dim iGUID As String = ""
'                    iGUID = Guid.NewGuid.ToString
'                    For iR As Integer = 0 To dtmp.Length - 1
'                        Dim dRow As DataRow = mdtCustomEvaluation.NewRow
'                        dRow.Item("customanalysistranguid") = iGUID
'                        dRow.Item("analysisunkid") = mintAssessAnalysisUnkid
'                        dRow.Item("customitemunkid") = dtmp(iR).Item("customitemunkid")
'                        dRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                        Select Case CInt(dtmp(iR).Item("itemtypeid"))
'                            Case clsassess_custom_items.enCustomType.FREE_TEXT
'                                dRow.Item("custom_value") = dtmp(iR).Item("custom_value")
'                            Case clsassess_custom_items.enCustomType.SELECTION
'                                dRow.Item("custom_value") = dtmp(iR).Item("selectedid")
'                            Case clsassess_custom_items.enCustomType.DATE_SELECTION
'                                If dtmp(iR).Item("ddate").ToString.Trim.Length > 0 Then
'                                    dRow.Item("custom_value") = eZeeDate.convertDate(dtmp(iR).Item("ddate"))
'                                Else
'                                    dRow.Item("custom_value") = ""
'                                End If
'                            Case clsassess_custom_items.enCustomType.NUMERIC_DATA
'                                dRow.Item("custom_value") = dtmp(iR).Item("custom_value")
'                        End Select
'                        dRow.Item("custom_header") = Me.ViewState("Header")
'                        dRow.Item("custom_item") = dtmp(iR).Item("custom_item")
'                        dRow.Item("dispaly_value") = dtmp(iR).Item("custom_value")
'                        dRow.Item("AUD") = "A"
'                        dRow.Item("itemtypeid") = dtmp(iR).Item("itemtypeid")
'                        dRow.Item("selectionmodeid") = dtmp(iR).Item("selectionmodeid")

'                        mdtCustomEvaluation.Rows.Add(dRow)
'                    Next
'                Else
'                    If dtmp.Length > 0 Then
'                        For iEdit As Integer = 0 To dtmp.Length - 1
'                            Dim xRow() As DataRow = mdtCustomEvaluation.Select("customitemunkid = '" & dtmp(iEdit).Item("customitemunkid") & "' AND customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D'")
'                            If xRow.Length > 0 Then
'                                xRow(0).Item("customanalysistranguid") = xRow(0).Item("customanalysistranguid")
'                                xRow(0).Item("analysisunkid") = xRow(0).Item("analysisunkid")
'                                xRow(0).Item("customitemunkid") = dtmp(iEdit).Item("customitemunkid")
'                                xRow(0).Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                Select Case CInt(dtmp(iEdit).Item("itemtypeid"))
'                                    Case clsassess_custom_items.enCustomType.FREE_TEXT
'                                        xRow(0).Item("custom_value") = dtmp(iEdit).Item("custom_value")
'                                    Case clsassess_custom_items.enCustomType.SELECTION
'                                        xRow(0).Item("custom_value") = dtmp(iEdit).Item("selectedid")
'                                    Case clsassess_custom_items.enCustomType.DATE_SELECTION
'                                        If dtmp(iEdit).Item("ddate").ToString.Trim.Length > 0 Then
'                                            xRow(0).Item("custom_value") = eZeeDate.convertDate(dtmp(iEdit).Item("ddate"))
'                                        Else
'                                            xRow(0).Item("custom_value") = ""
'                                        End If
'                                    Case clsassess_custom_items.enCustomType.NUMERIC_DATA
'                                        xRow(0).Item("custom_value") = dtmp(iEdit).Item("custom_value")
'                                End Select
'                                xRow(0).Item("custom_header") = Me.ViewState("Header")
'                                xRow(0).Item("custom_item") = dtmp(iEdit).Item("custom_item")
'                                xRow(0).Item("dispaly_value") = dtmp(iEdit).Item("custom_value")
'                                'S.SANDEEP [31 DEC 2015] -- START
'                                'xRow(0).Item("AUD") = "U"
'                                If xRow(0).Item("analysisunkid") <= 0 Then
'                                    xRow(0).Item("AUD") = "A"
'                                Else
'                                    xRow(0).Item("AUD") = "U"
'                                End If
'                                'S.SANDEEP [31 DEC 2015] -- END
'                                xRow(0).Item("itemtypeid") = dtmp(iEdit).Item("itemtypeid")
'                                xRow(0).Item("selectionmodeid") = dtmp(iEdit).Item("selectionmodeid")
'                            End If
'                        Next
'                    End If
'                End If
'            End If
'            mblnItemAddEdit = False
'            popup_CItemAddEdit.Hide()
'            Call Fill_Custom_Evaluation_Data()
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
'        Try
'            If txtMessage.Text.Trim.Length > 0 Then
'                If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
'                    Dim xRow() As DataRow = Nothing
'                    'S.SANDEEP [12 OCT 2016] -- START
'                    'ENHANCEMENT : ACB REPORT CHANGES
'                    'xRow = mdtCustomEvaluation.Select("customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D'")
'                    xRow = mdtCustomEvaluation.Select("customanalysistranguid = '" & mstriEditingGUID & "' AND AUD <> 'D' AND isdefaultentry = False")
'                    'S.SANDEEP [12 OCT 2016] -- END
'                    If xRow.Length > 0 Then
'                        For x As Integer = 0 To xRow.Length - 1
'                            xRow(x).Item("isvoid") = True
'                            xRow(x).Item("voiduserunkid") = Session("UserId")
'                            xRow(x).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
'                            xRow(x).Item("voidreason") = txtMessage.Text
'                            xRow(x).Item("AUD") = "D"
'                        Next
'                        Call Fill_Custom_Evaluation_Data()
'                    End If
'                End If
'            Else
'                popup_CItemReason.Show()
'            End If

'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub btnSaveCommit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCommit.Click
'        Try
'            If dsHeaders.Tables.Count <= 0 Then Exit Sub
'            If dsHeaders.Tables(0).Rows.Count > 0 Then
'                If Validation() = False Then Exit Sub
'                Dim dtmp() As DataRow = Nothing
'                If mdtCustomEvaluation IsNot Nothing Then
'                    If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables(0).Rows.Count > 0 Then
'                        dtmp = dsHeaders.Tables(0).Select("Id > 0")
'                        'S.SANDEEP [ 18 DEC 2014 ] -- START
'                        'Dim dCEval As DataView = mdtCustomEvaluation.DefaultView
'                        Dim dCEval As DataView = New DataView(mdtCustomEvaluation, "periodunkid = '" & CInt(cboPeriod.SelectedValue) & "'", "", DataViewRowState.CurrentRows)
'                        'S.SANDEEP [ 18 DEC 2014 ] -- END
'                        If dtmp.Length <> dCEval.ToTable(True, "custom_header").Rows.Count Then
'                            'SHANI (09 APR 2015)-START
'                            'lblConfirmMessage.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 31, "You have some custom items along with evaluation items, which you have not assessed. Would you like to assess then before saving this evaluation?")

'                            'Shani (26-Sep-2016) -- Start
'                            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                            'lblConfirmMessage.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 31, "You have some custom items along with evaluation items, which you have not assessed. Would you like to assess them before saving this evaluation?")
'                            lblConfirmMessage.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 38, "You still have some assessment items not evaluated. Do you want to save now and complete them later?")
'                            'Shani (26-Sep-2016) -- End



'                            'SHANI (09 APR 2015)--END 

'                            'S.SANDEEP [09 OCT 2015] -- START
'                            'btnCnfNo.CommandName = btnSaveCommit.ID
'                            btnCnfYes.CommandName = btnSaveCommit.ID
'                            'S.SANDEEP [09 OCT 2015] -- END

'                            popup_CnfYesNo.Show()
'                            Exit Sub
'                        End If
'                    End If
'                End If
'                btnSaveCommit.CommandName = btnSaveCommit.ID

'                'Shani(15-Oct-2015) -- Start
'                'ENHANCEMENT : Doing Changes as per Andrew's Discussion for Assessing employee for user ease
'                'Call btnCnfNo_Click(sender, e)
'                Call btnCnfYes_Click(sender, e)
'                'Shani(15-Oct-2015) -- End

'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Try
'            If dsHeaders.Tables.Count <= 0 Then Exit Sub
'            If dsHeaders.Tables(0).Rows.Count > 0 Then
'                If Validation() = False Then Exit Sub
'                Dim dtmp() As DataRow = Nothing
'                If mdtCustomEvaluation IsNot Nothing Then
'                    If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables(0).Rows.Count > 0 Then
'                        dtmp = dsHeaders.Tables(0).Select("Id > 0")
'                        'S.SANDEEP [ 18 DEC 2014 ] -- START
'                        'Dim dCEval As DataView = mdtCustomEvaluation.DefaultView
'                        Dim dCEval As DataView = New DataView(mdtCustomEvaluation, "periodunkid = '" & CInt(cboPeriod.SelectedValue) & "'", "", DataViewRowState.CurrentRows)
'                        'S.SANDEEP [ 18 DEC 2014 ] -- END
'                        If dtmp.Length <> dCEval.ToTable(True, "custom_header").Rows.Count Then
'                            'SHANI (09 APR 2015)-START
'                            'Enhancement - Allow more than one employee to be replaced in staff requisition.
'                            'lblConfirmMessage.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 31, "You have some custom items along with evaluation items, which you have not assessed. Would you like to assess then before saving this evaluation?")

'                            'S.SANDEEP [09 OCT 2015] -- START
'                            'lblConfirmMessage.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 31, "You have some custom items along with evaluation items, which you have not assessed. Would you like to assess them before saving this evaluation?")

'                            'Shani (26-Sep-2016) -- Start
'                            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                            'lblConfirmMessage.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 31, "You have some custom items along with evaluation items, which you have not assessed. Do you want to save without assessing them?")
'                            lblConfirmMessage.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 38, "You still have some assessment items not evaluated. Do you want to save now and complete them later?")
'                            'Shani (26-Sep-2016) -- End



'                            'S.SANDEEP [09 OCT 2015] -- END

'                            'Shani [ 09 APR  2015 ] -- END

'                            'S.SANDEEP [09 OCT 2015] -- START
'                            'btnCnfNo.CommandName = ""
'                            'popup_CnfYesNo.Show()
'                            'Exit Sub

'                            btnCnfYes.CommandName = ""
'                            popup_CnfYesNo.Show()
'                            Exit Sub
'                            'S.SANDEEP [09 OCT 2015] -- END
'                        End If
'                    End If
'                End If
'                'S.SANDEEP [09 OCT 2015] -- START
'                'btnCnfNo.CommandName = ""
'                'Call btnCnfNo_Click(sender, e)

'                btnCnfYes.CommandName = ""
'                Call btnCnfYes_Click(sender, e)
'                'S.SANDEEP [09 OCT 2015] -- END
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    'S.SANDEEP [09 OCT 2015] -- START
'    'Protected Sub btnCnfYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCnfYes.Click
'    '    Try
'    '        objbtnNext_Click(sender, e)
'    '        Exit Sub
'    '    Catch ex As Exception
'    '        DisplayMessage.DisplayError(ex,Me)
'    '    End Try
'    'End Sub

'    'Protected Sub btnCnfNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCnfNo.Click
'    '    Dim blnFlag As Boolean = False
'    '    Dim blnIsBSC_Set As Boolean = False
'    '    Dim blnIsGE_Set As Boolean = False
'    '    Try
'    '        If dsHeaders.Tables.Count <= 0 Then Exit Sub
'    '        If dsHeaders.Tables(0).Rows.Count > 0 Then
'    '            Dim dtmp() As DataRow = Nothing
'    '            'SHANI [21 Mar 2015]-START
'    '            'Issue : Fixing Issues Sent By Andrew in PA Testing.
'    '            'If mdtBSC_Evaluation IsNot Nothing Then
'    '            '    dtmp = dsHeaders.Tables(0).Select("Id=-3")
'    '            '    If dtmp.Length > 0 Then
'    '            '        blnIsBSC_Set = True
'    '            '        dtmp = mdtBSC_Evaluation.Select("AUD <> 'D'")
'    '            '        If dtmp.Length <= 0 Then
'    '            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 30, "Sorry, Please assess atleast one item from Balance Score Card Evaluation in order to save."), Me)
'    '            '            Exit Sub
'    '            '        End If
'    '            '    End If
'    '            'End If
'    '            'If mdtGE_Evaluation IsNot Nothing Then
'    '            '    dtmp = dsHeaders.Tables(0).Select("Id=-2")
'    '            '    If dtmp.Length > 0 Then
'    '            '        blnIsGE_Set = True
'    '            '        dtmp = mdtGE_Evaluation.Select("AUD <> 'D'")
'    '            '        If dtmp.Length <= 0 Then
'    '            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 34, "Sorry, Please assess atleast one item from Competencies Evaluation in order to save."), Me)
'    '            '            Exit Sub
'    '            '        End If
'    '            '    End If
'    '            'End If

'    '            Dim blnIsOneAssessed As Boolean = False
'    '            If mdtBSC_Evaluation IsNot Nothing Then
'    '                dtmp = dsHeaders.Tables(0).Select("Id=-3")
'    '                If dtmp.Length > 0 Then
'    '                    blnIsBSC_Set = True
'    '                    dtmp = mdtBSC_Evaluation.Select("AUD <> 'D'")
'    '                    If dtmp.Length > 0 Then
'    '                        blnIsOneAssessed = True
'    '                    End If
'    '                End If
'    '            End If

'    '            If mdtGE_Evaluation IsNot Nothing Then
'    '                dtmp = dsHeaders.Tables(0).Select("Id=-2")
'    '                If dtmp.Length > 0 Then
'    '                    'S.SANDEEP [04 JUN 2015] -- START
'    '                    'blnIsBSC_Set = True
'    '                    blnIsGE_Set = True
'    '                    'S.SANDEEP [04 JUN 2015] -- END
'    '                    dtmp = mdtGE_Evaluation.Select("AUD <> 'D'")
'    '                    If dtmp.Length > 0 Then
'    '                        blnIsOneAssessed = True
'    '                    End If
'    '                End If
'    '            End If

'    '            If blnIsOneAssessed = False Then
'    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 35, "Sorry, Please assess atleast one item from Balance Score Card Or Competencies Evaluation in order to save."), Me)
'    '                Exit Sub
'    '            End If
'    '            'SHANI [21 Mar 2015]--END 
'    '            Call SetValue()
'    '            Select Case CType(sender, Button).CommandName.ToUpper
'    '                Case btnSaveCommit.ID.ToUpper
'    '                    If Session("IsAllowFinalSave") = False Then
'    '                        If blnIsBSC_Set = True Then If isAll_Assessed_BSC() = False Then Exit Sub
'    '                        If blnIsGE_Set = True Then If isAll_Assessed_GE() = False Then Exit Sub
'    '                        Call CnfSaveCommit_buttonYes_Click(New Object, New EventArgs)
'    '                        Exit Sub
'    '                    ElseIf Session("IsAllowFinalSave") = True Then
'    '                        'S.SANDEEP [01 OCT 2015] -- START
'    '                        'If isAll_Assessed_BSC(False) = False Then
'    '                        '    CnfSaveCommit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
'    '                        '    CnfSaveCommit.Show()
'    '                        '    Exit Sub
'    '                        'End If
'    '                        'If isAll_Assessed_GE(False) = False Then
'    '                        '    CnfSaveCommit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
'    '                        '    CnfSaveCommit.Show()
'    '                        '    Exit Sub
'    '                        'End If
'    '                        If isAll_Assessed_BSC(False) = True Then
'    '                            CnfSaveCommit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
'    '                            CnfSaveCommit.Show()
'    '                            Exit Sub
'    '                        End If
'    '                        If isAll_Assessed_GE(False) = False Then
'    '                            CnfSaveCommit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
'    '                            CnfSaveCommit.Show()
'    '                            Exit Sub
'    '                        End If
'    '                        'S.SANDEEP [01 OCT 2015] -- END
'    '                    End If
'    '            End Select

'    '            If menAction = enAction.EDIT_ONE Then
'    '                blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
'    '            Else
'    '                blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
'    '            End If

'    '            If blnFlag = False And objEAnalysisMst._Message <> "" Then
'    '                DisplayMessage.DisplayMessage(objEAnalysisMst._Message, Me)
'    '                Exit Sub
'    '            End If

'    '            If blnFlag = True Then
'    '                Call ClearForm_Values()
'    '                If Request.QueryString.Count > 0 Then
'    '                    Response.Redirect("~/Index.aspx")
'    '                Else
'    '                    Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_AssessorEvaluationList.aspx", False)
'    '                End If
'    '            End If
'    '        End If
'    '    Catch ex As Exception
'    '        DisplayMessage.DisplayError(ex,Me)
'    '    Finally
'    '    End Try
'    'End Sub

'    Protected Sub btnCnfNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCnfNo.Click
'        Try
'            objbtnNext_Click(sender, e)
'            Exit Sub
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub btnCnfYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCnfYes.Click
'        Dim blnFlag As Boolean = False
'        Dim blnIsBSC_Set As Boolean = False
'        Dim blnIsGE_Set As Boolean = False
'        Try
'            If dsHeaders.Tables.Count <= 0 Then Exit Sub
'            If dsHeaders.Tables(0).Rows.Count > 0 Then
'                Dim dtmp() As DataRow = Nothing
'                'SHANI [21 Mar 2015]-START
'                'Issue : Fixing Issues Sent By Andrew in PA Testing.
'                'If mdtBSC_Evaluation IsNot Nothing Then
'                '    dtmp = dsHeaders.Tables(0).Select("Id=-3")
'                '    If dtmp.Length > 0 Then
'                '        blnIsBSC_Set = True
'                '        dtmp = mdtBSC_Evaluation.Select("AUD <> 'D'")
'                '        If dtmp.Length <= 0 Then
'                '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 30, "Sorry, Please assess atleast one item from Balance Score Card Evaluation in order to save."), Me)
'                '            Exit Sub
'                '        End If
'                '    End If
'                'End If
'                'If mdtGE_Evaluation IsNot Nothing Then
'                '    dtmp = dsHeaders.Tables(0).Select("Id=-2")
'                '    If dtmp.Length > 0 Then
'                '        blnIsGE_Set = True
'                '        dtmp = mdtGE_Evaluation.Select("AUD <> 'D'")
'                '        If dtmp.Length <= 0 Then
'                '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 34, "Sorry, Please assess atleast one item from Competencies Evaluation in order to save."), Me)
'                '            Exit Sub
'                '        End If
'                '    End If
'                'End If

'                Dim blnIsOneAssessed As Boolean = False
'                If mdtBSC_Evaluation IsNot Nothing Then
'                    dtmp = dsHeaders.Tables(0).Select("Id=-3")
'                    If dtmp.Length > 0 Then
'                        blnIsBSC_Set = True
'                        dtmp = mdtBSC_Evaluation.Select("AUD <> 'D'")
'                        If dtmp.Length > 0 Then
'                            blnIsOneAssessed = True
'                        End If
'                    End If
'                End If

'                If mdtGE_Evaluation IsNot Nothing Then
'                    dtmp = dsHeaders.Tables(0).Select("Id=-2")
'                    If dtmp.Length > 0 Then
'                        'S.SANDEEP [04 JUN 2015] -- START
'                        'blnIsBSC_Set = True
'                        blnIsGE_Set = True
'                        'S.SANDEEP [04 JUN 2015] -- END
'                        dtmp = mdtGE_Evaluation.Select("AUD <> 'D'")
'                        If dtmp.Length > 0 Then
'                            blnIsOneAssessed = True
'                        End If
'                    End If
'                End If

'                If blnIsOneAssessed = False Then
'                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 35, "Sorry, Please assess atleast one item from Balance Score Card Or Competencies Evaluation in order to save."), Me)
'                    Exit Sub
'                End If
'                'SHANI [21 Mar 2015]--END 
'                Call SetValue()
'                Select Case CType(sender, Button).CommandName.ToUpper
'                    Case btnSaveCommit.ID.ToUpper
'                        If Session("IsAllowFinalSave") = False Then
'                            If blnIsBSC_Set = True Then If isAll_Assessed_BSC() = False Then Exit Sub
'                            If blnIsGE_Set = True Then If isAll_Assessed_GE() = False Then Exit Sub
'                            Call CnfSaveCommit_buttonYes_Click(New Object, New EventArgs)
'                            Exit Sub
'                        ElseIf Session("IsAllowFinalSave") = True Then
'                            'S.SANDEEP [01 OCT 2015] -- START
'                            'If isAll_Assessed_BSC(False) = False Then
'                            '    CnfSaveCommit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
'                            '    CnfSaveCommit.Show()
'                            '    Exit Sub
'                            'End If
'                            'If isAll_Assessed_GE(False) = False Then
'                            '    CnfSaveCommit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
'                            '    CnfSaveCommit.Show()
'                            '    Exit Sub
'                            'End If
'                            If isAll_Assessed_BSC(False) = False Then 'Shani(30-MAR-2016) 
'                                CnfSaveCommit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
'                                CnfSaveCommit.Show()
'                                Exit Sub
'                            End If
'                            If isAll_Assessed_GE(False) = False Then
'                                CnfSaveCommit.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "There are some Assessement Items which are still not Assessed for particular Group.If you Press Yes,you will Commit this group.Do you want to continue?")
'                                CnfSaveCommit.Show()
'                                Exit Sub
'                            End If
'                            'Shani(30-MAR-2016) -- Start
'                            Call CnfSaveCommit_buttonYes_Click(New Object, New EventArgs)
'                            Exit Sub
'                            'Shani(30-MAR-2016) -- End 

'                            'S.SANDEEP [01 OCT 2015] -- END
'                        End If
'                End Select

'                If menAction = enAction.EDIT_ONE Then
'                    blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
'                Else
'                    blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
'                End If

'                If blnFlag = False And objEAnalysisMst._Message <> "" Then
'                    DisplayMessage.DisplayMessage(objEAnalysisMst._Message, Me)
'                    Exit Sub
'                End If

'                If blnFlag = True Then
'                    Call ClearForm_Values()
'                    If Request.QueryString.Count > 0 Then
'                        Response.Redirect("~/Index.aspx")
'                    Else
'                        Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_AssessorEvaluationList.aspx", False)
'                    End If
'                End If
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub
'    'S.SANDEEP [09 OCT 2015] -- END

'    Protected Sub CnfSaveCommit_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CnfSaveCommit.buttonYes_Click
'        Dim blnFlag As Boolean = False
'        Try
'            'S.SANDEEP [03 OCT 2015] -- START
'            Call SetValue()
'            'S.SANDEEP [03 OCT 2015] -- END

'            objEAnalysisMst._Iscommitted = True
'            objEAnalysisMst._Committeddatetime = ConfigParameter._Object._CurrentDateAndTime

'            If menAction = enAction.EDIT_ONE Then
'                blnFlag = objEAnalysisMst.Update(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
'            Else
'                blnFlag = objEAnalysisMst.Insert(mdtBSC_Evaluation, mdtGE_Evaluation, mdtCustomEvaluation)
'            End If

'            If blnFlag = False And objEAnalysisMst._Message <> "" Then
'                DisplayMessage.DisplayMessage(objEAnalysisMst._Message, Me)
'                Exit Sub
'            End If

'            If blnFlag = True Then
'                Call ClearForm_Values()

'                'Shani(20-Nov-2015) -- Start
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                'objEAnalysisMst.Email_Notification(enAssessmentMode.SELF_ASSESSMENT, _
'                '                                   CInt(cboEmployee.SelectedValue), _
'                '                                   CInt(cboPeriod.SelectedValue), _
'                '                                   Session("IsCompanyNeedReviewer"), , , , _
'                '                                   enLogin_Mode.DESKTOP, 0)
'                objEAnalysisMst.Email_Notification(enAssessmentMode.APPRAISER_ASSESSMENT, _
'                                                   CInt(cboEmployee.SelectedValue), _
'                                                   CInt(cboPeriod.SelectedValue), _
'                                                   Session("IsCompanyNeedReviewer"), _
'                                                   Session("Database_Name"), _
'                                                   Session("CompanyUnkId"), _
'                                                   Session("ArutiSelfServiceURL"), _
'                                                   CStr(cboAssessor.SelectedItem.Text), _
'                                                   enLogin_Mode.MGR_SELF_SERVICE, 0)
'                'Shani(20-Nov-2015) -- End


'                If Request.QueryString.Count > 0 Then
'                    Response.Redirect("~/Index.aspx")
'                Else
'                    Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_AssessorEvaluationList.aspx", False)
'                End If
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
'        Call ClearForm_Values()
'        Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_AssessorEvaluationList.aspx", False)
'    End Sub


'    'SHANI [01 FEB 2015]-START
'    'Enhancement - REDESIGN SELF SERVICE.
'    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
'    '    Call ClearForm_Values()
'    '    Response.Redirect(Session("servername") & "~/Assessment New/Performance Evaluation/wPg_AssessorEvaluationList.aspx", False)
'    'End Sub
'    'SHANI [01 FEB 2015]--END

'    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
'        Try
'            txtInstruction.Text = Session("Assessment_Instructions")
'            objlblCaption.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Intructions")
'            objpnlInstruction.Visible = True
'            txtInstruction.Height = Unit.Pixel(430)

'            objbtnNext.Enabled = False : objbtnBack.Enabled = False
'            objlblValue1.Visible = False : objlblValue2.Visible = False
'            objlblValue3.Visible = False : objlblValue4.Visible = False

'            Call FillCombo()
'            If menAction = enAction.EDIT_ONE Then
'                objEAnalysisMst._Analysisunkid = mintAssessAnalysisUnkid
'                cboEmployee.Enabled = False
'                cboAssessor.Enabled = False
'                cboPeriod.Enabled = False
'                radOption.Enabled = False
'                cboReviewer.Enabled = False
'                BtnSearch.Enabled = False
'                BtnReset.Enabled = False
'            End If
'            Call GetValue()

'            objGoalsTran._AnalysisUnkid = mintAssessAnalysisUnkid
'            mdtBSC_Evaluation = objGoalsTran._DataTable

'            objCAssessTran._ConsiderWeightAsNumber = CBool(Session("ConsiderItemWeightAsNumber"))
'            objCAssessTran._AnalysisUnkid = mintAssessAnalysisUnkid
'            mdtGE_Evaluation = objCAssessTran._DataTable

'            objCCustomTran._AnalysisUnkid = mintAssessAnalysisUnkid
'            objCCustomTran._EmployeeId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue))
'            'S.SANDEEP [29 DEC 2015] -- START

'            'Shani (26-Sep-2016) -- Start
'            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'            'objCCustomTran._AllowCustomItemInPlanning = CBool(Session("IncludeCustomItemInPlanning"))
'            'Shani (26-Sep-2016) -- End


'            objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
'            'S.SANDEEP [29 DEC 2015] -- END
'            mdtCustomEvaluation = objCCustomTran._DataTable

'            If menAction = enAction.EDIT_ONE Then
'                Call Fill_BSC_Evaluation()
'                Call Fill_GE_Evaluation()
'                Call Fill_Custom_Grid()
'                Call BtnSearch_Click(sender, e)
'            Else
'                objpnlBSC.Visible = False
'                objpnlGE.Visible = False
'                objpnlCItems.Visible = False
'            End If
'            dtpAssessdate.SetDate = Nothing
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub
'#End Region

'#Region "GridView Event"

'    Protected Sub dgvGE_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvGE.ItemDataBound
'        Try
'            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
'                Dim intCount As Integer = 1
'                If CBool(DataBinder.Eval(e.Item.DataItem, "IsPGrp")) = True Then
'                    For i = 1 To dgvGE.Columns.Count - 1
'                        If dgvGE.Columns(i).Visible = True Then
'                            intCount += 1
'                            e.Item.Cells(i).Visible = False
'                        End If
'                    Next

'                    'Shani (23-Nov-2016) -- Start
'                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    'e.Item.Cells(0).ColumnSpan = intCount
'                    'e.Item.Cells(0).CssClass = "MainGroupHeaderStyle"
'                    e.Item.Cells(disGEolumn("dgcolheval_itemGE")).ColumnSpan = intCount
'                    e.Item.Cells(disGEolumn("dgcolheval_itemGE")).CssClass = "MainGroupHeaderStyle"
'                    'Shani (23-Nov123-2016-2016) -- End
'                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then
'                    intCount = 1
'                    For i = 1 To dgvGE.Columns.Count - 2
'                        If dgvGE.Columns(i).Visible = True Then
'                            intCount += 1
'                            e.Item.Cells(i).Visible = False
'                        End If
'                    Next

'                    'Shani (23-Nov-2016) -- Start
'                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    'e.Item.Cells(0).ColumnSpan = intCount
'                    'e.Item.Cells(0).CssClass = "GroupHeaderStylecomp"
'                    'e.Item.Cells(15).CssClass = "GroupHeaderStylecomp"
'                    e.Item.Cells(disGEolumn("dgcolheval_itemGE")).ColumnSpan = intCount
'                    e.Item.Cells(disGEolumn("dgcolheval_itemGE")).CssClass = "GroupHeaderStylecomp"
'                    e.Item.Cells(disGEolumn("objdgcolhInformation")).CssClass = "GroupHeaderStylecomp"
'                    'Shani (23-Nov123-2016-2016) -- End
'                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then
'                    'Shani (23-Nov-2016) -- Start
'                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    'e.Item.Cells(0).Text = "&nbsp;" & e.Item.Cells(0).Text
'                    e.Item.Cells(disGEolumn("dgcolheval_itemGE")).Text = "&nbsp;" & e.Item.Cells(disGEolumn("dgcolheval_itemGE")).Text
'                    'Shani (23-Nov123-2016-2016) -- End
'                End If

'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                'If CInt(e.Item.Cells(10).Text) > 0 Then
'                '    If mdtGE_Evaluation.Rows.Count > 0 Then
'                '        Dim dtmp() As DataRow = Nothing
'                '        dtmp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(e.Item.Cells(11).Text) & "' AND competenciesunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D' ")
'                '        If dtmp.Length > 0 Then
'                '            CType(e.Item.Cells(6).Controls(1), TextBox).Text = CInt(dtmp(0).Item("result"))
'                '            CType(e.Item.Cells(8).Controls(1), TextBox).Text = dtmp(0).Item("remark")
'                '            If CBool(Session("ConsiderItemWeightAsNumber")) = True Then
'                '                Dim iWgt As Decimal = 0 : Decimal.TryParse(e.Item.Cells(1).Text, iWgt)
'                '                If iWgt <= 0 Then iWgt = 1
'                '                e.Item.Cells(7).Text = iWgt * CInt(dtmp(0).Item("result"))
'                '            Else
'                '                e.Item.Cells(7).Text = 1 * CInt(dtmp(0).Item("result"))
'                '                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
'                '            End If
'                '        End If
'                '    End If
'                'End If
'                'If IsNumeric(e.Item.Cells(1).Text) Then
'                '    iWeightTotal = iWeightTotal + CDec(e.Item.Cells(1).Text)
'                'End If
'                If CInt(e.Item.Cells(disGEolumn("objdgcolhcompetenciesunkidGE")).Text) > 0 Then
'                    If mdtGE_Evaluation.Rows.Count > 0 Then
'                        Dim dtmp() As DataRow = Nothing
'                        dtmp = mdtGE_Evaluation.Select("assessgroupunkid = '" & CInt(e.Item.Cells(disGEolumn("objdgcolhassessgroupunkidGE")).Text) & "' AND competenciesunkid = '" & CInt(e.Item.Cells(disGEolumn("objdgcolhcompetenciesunkidGE")).Text) & "' AND AUD <> 'D' ")
'                        If dtmp.Length > 0 Then
'                            CType(e.Item.Cells(disGEolumn("dgcolhaselfGE")).Controls(1), TextBox).Text = CInt(dtmp(0).Item("result"))
'                            CType(e.Item.Cells(disGEolumn("dgcolharemarkGE")).Controls(1), TextBox).Text = dtmp(0).Item("remark")
'                            If CBool(Session("ConsiderItemWeightAsNumber")) = True Then
'                                Dim iWgt As Decimal = 0 : Decimal.TryParse(e.Item.Cells(disGEolumn("dgcolhGEWeight")).Text, iWgt)
'                                If iWgt <= 0 Then iWgt = 1
'                                e.Item.Cells(disGEolumn("objdgcolhadisplayGE")).Text = iWgt * CInt(dtmp(0).Item("result"))
'                            Else
'                                e.Item.Cells(disGEolumn("objdgcolhadisplayGE")).Text = 1 * CInt(dtmp(0).Item("result"))
'                                dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
'                            End If
'                        End If
'                    End If
'                End If
'                If IsNumeric(e.Item.Cells(disGEolumn("dgcolhGEWeight")).Text) Then
'                    iWeightTotal = iWeightTotal + CDec(e.Item.Cells(disGEolumn("dgcolhGEWeight")).Text)
'                End If
'                'Shani (23-Nov123-2016-2016) -- End
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)

'        Finally
'        End Try
'    End Sub

'    Protected Sub dgvBSC_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvBSC.ItemDataBound
'        Try
'            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
'                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
'                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
'                Call SetDateFormat()
'                'Pinkal (16-Apr-2016) -- End
'                If CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = True Then
'                    For i = xVal To dgvBSC.Columns.Count - 1
'                        e.Item.Cells(i).Visible = False
'                    Next
'                    e.Item.Cells(xVal - 1).ColumnSpan = dgvBSC.Columns.Count - 1
'                    e.Item.Cells(xVal - 1).CssClass = "GroupHeaderStyleBorderLeft"
'                ElseIf CBool(DataBinder.Eval(e.Item.DataItem, "IsGrp")) = False Then
'                    e.Item.Cells(xVal - 1).Text = "&nbsp;" & e.Item.Cells(0).Text

'                    'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
'                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
'                    'CType(e.Item.Cells(16).Controls(1), TextBox).Attributes.Add("onKeypress", "return onlyNumbers(this,event);")
'                    ' using New jquery .decimal class in master page

'                    'Shani (23-Nov-2016) -- Start
'                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    'If e.Item.Cells(8).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(8).Text.Trim <> "&nbsp;" Then
'                    '    e.Item.Cells(8).Text = CDate(e.Item.Cells(8).Text).Date.ToShortDateString
'                    'End If

'                    'If e.Item.Cells(9).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(9).Text.Trim <> "&nbsp;" Then
'                    '    e.Item.Cells(9).Text = CDate(e.Item.Cells(9).Text).Date.ToShortDateString
'                    'End If
'                    If e.Item.Cells(disBSColumn("dgcolhSDate")).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(disBSColumn("dgcolhSDate")).Text.Trim <> "&nbsp;" Then
'                        e.Item.Cells(disBSColumn("dgcolhSDate")).Text = CDate(e.Item.Cells(disBSColumn("dgcolhSDate")).Text).Date.ToShortDateString
'                    End If

'                    If e.Item.Cells(disBSColumn("dgcolhEDate")).Text.ToString().Trim <> "" <> Nothing AndAlso e.Item.Cells(disBSColumn("dgcolhEDate")).Text.Trim <> "&nbsp;" Then
'                        e.Item.Cells(disBSColumn("dgcolhEDate")).Text = CDate(e.Item.Cells(disBSColumn("dgcolhEDate")).Text).Date.ToShortDateString
'                    End If
'                    'Shani (23-Nov123-2016-2016) -- End
'                    'Pinkal (16-Apr-2016) -- End
'                End If
'                If CStr(DataBinder.Eval(e.Item.DataItem, "Weight")) = "" Then
'                    'S.SANDEEP [ 17 DEC 2014 ] -- START
'                    'e.Item.Cells(16).Controls(0).Visible = False : e.Item.Cells(17).Controls(0).Visible = False

'                    'Shani (23-Nov-2016) -- Start
'                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    'e.Item.Cells(16).Controls(1).Visible = False : e.Item.Cells(17).Controls(1).Visible = False
'                    CType(e.Item.Cells(disBSColumn("dgcolhaselfBSC")).FindControl("dgcolhaselfBSC"), TextBox).Visible = False
'                    CType(e.Item.Cells(disBSColumn("dgcolharemarkBSC")).FindControl("dgcolharemarkBSC"), TextBox).Visible = False
'                    'Shani (23-Nov123-2016-2016) -- End


'                    'S.SANDEEP [ 17 DEC 2014 ] -- END
'                ElseIf CStr(DataBinder.Eval(e.Item.DataItem, "Weight")) <> "" Then
'                    If mdtBSC_Evaluation.Rows.Count > 0 Then
'                        Dim dtmp() As DataRow = Nothing
'                        Select Case iExOrdr
'                            Case enWeight_Types.WEIGHT_FIELD1
'                                dtmp = mdtBSC_Evaluation.Select("empfield1unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield1unkid").ToString & "' AND AUD <> 'D' ")
'                            Case enWeight_Types.WEIGHT_FIELD2
'                                dtmp = mdtBSC_Evaluation.Select("empfield2unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield2unkid").ToString & "' AND AUD <> 'D' ")
'                            Case enWeight_Types.WEIGHT_FIELD3
'                                dtmp = mdtBSC_Evaluation.Select("empfield3unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield3unkid").ToString & "' AND AUD <> 'D' ")
'                            Case enWeight_Types.WEIGHT_FIELD4
'                                dtmp = mdtBSC_Evaluation.Select("empfield4unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield4unkid").ToString & "' AND AUD <> 'D' ")
'                            Case enWeight_Types.WEIGHT_FIELD5
'                                dtmp = mdtBSC_Evaluation.Select("empfield5unkid = '" & dtBSC_TabularGrid.Rows(e.Item.ItemIndex).Item("empfield5unkid").ToString & "' AND AUD <> 'D' ")
'                        End Select

'                        If dtmp.Length > 0 Then
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'CType(e.Item.Cells(16).Controls(1), TextBox).Text = CDec(dtmp(0).Item("result"))
'                            'CType(e.Item.Cells(17).Controls(1), TextBox).Text = dtmp(0).Item("remark")
'                            CType(e.Item.Cells(disBSColumn("dgcolhaselfBSC")).FindControl("dgcolhaselfBSC"), TextBox).Text = CDec(dtmp(0).Item("result"))
'                            CType(e.Item.Cells(disBSColumn("dgcolharemarkBSC")).FindControl("dgcolharemarkBSC"), TextBox).Text = dtmp(0).Item("remark")
'                            'Shani (23-Nov123-2016-2016) -- End
'                        End If
'                        ' ''
'                        'If menAction <> enAction.ADD_CONTINUE Then
'                        '    If menAssess = enAssessmentMode.SELF_ASSESSMENT Then
'                        '        e.Item.Cells(14).Controls(0).Visible = True : e.Item.Cells(15).Controls(0).Visible = True
'                        '    End If
'                        'End If
'                    End If
'                End If

'                'Shani (23-Nov-2016) -- Start
'                'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                'If IsNumeric(e.Item.Cells(13).Text) Then
'                '    iWeightTotal = iWeightTotal + CDec(e.Item.Cells(13).Text)
'                'End If
'                If IsNumeric(e.Item.Cells(disBSColumn("dgcolhBSCWeight")).Text) Then
'                    iWeightTotal = iWeightTotal + CDec(e.Item.Cells(disBSColumn("dgcolhBSCWeight")).Text)
'                End If
'                'Shani (23-Nov123-2016-2016) -- End

'                'Shani (26-Sep-2016) -- Start
'                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                If CBool(Session("DontAllowToEditScoreGenbySys")) Then
'                    'Shani (23-Nov-2016) -- Start
'                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    'CType(e.Item.Cells(16).Controls(1), TextBox).ReadOnly = True
'                    CType(e.Item.Cells(disBSColumn("dgcolhaselfBSC")).FindControl("dgcolhaselfBSC"), TextBox).ReadOnly = True
'                    'Shani (23-Nov123-2016-2016) -- End
'                End If
'                'Shani (26-Sep-2016) -- End
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub dgvGE_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvGE.ItemCommand
'        Try
'            If menAction = enAction.EDIT_ONE Then
'                Dim dtmp() As DataRow = Nothing
'                dtmp = mdtGE_Evaluation.Select("assessgroupunkid = '" & dtGE_TabularGrid.Rows(e.Item.ItemIndex).Item("assessgroupunkid").ToString & "' AND competenciesunkid = '" & dtGE_TabularGrid.Rows(e.Item.ItemIndex).Item("competenciesunkid").ToString & "' AND AUD <> 'D' ")
'                If dtmp.Length > 0 Then
'                    If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'                        If Session("ConsiderItemWeightAsNumber") Then
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'e.Item.Cells(5).Text = CDbl(e.Item.Cells(2).Text) * CInt(e.Item.Cells(4).Text)
'                            'e.Item.Cells(8).Text = CDbl(e.Item.Cells(2).Text) * CInt(e.Item.Cells(4).Text)
'                            e.Item.Cells(disGEolumn("dgcolharemarkGE")).Text = CDbl(e.Item.Cells(disGEolumn("dgcolhGEScore")).Text) * CInt(e.Item.Cells(disGEolumn("objdgcolhedisplayGE")).Text)
'                            'Shani (23-Nov123-2016-2016) -- End
'                        Else
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'e.Item.Cells(5).Text = 1 * CInt(e.Item.Cells(4).Text)
'                            'e.Item.Cells(8).Text = 1 * CInt(e.Item.Cells(4).Text)
'                            e.Item.Cells(disGEolumn("dgcolharemarkGE")).Text = 1 * CInt(e.Item.Cells(disGEolumn("objdgcolhedisplayGE")).Text)
'                            'Shani (23-Nov123-2016-2016) -- End
'                            dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
'                        End If

'                        If Session("ConsiderItemWeightAsNumber") Then
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'e.Item.Cells(8).Text = CDbl(e.Item.Cells(2).Text) * CInt(dtmp(0).Item("result"))
'                            e.Item.Cells(disGEolumn("dgcolharemarkGE")).Text = CDbl(e.Item.Cells(disGEolumn("dgcolhGEScore")).Text) * CInt(dtmp(0).Item("result"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                        Else
'                            'Shani (23-Nov-2016) -- Start
'                            'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                            'e.Item.Cells(8).Text = 1 * CInt(dtmp(0).Item("result"))
'                            e.Item.Cells(disGEolumn("dgcolharemarkGE")).Text = 1 * CInt(dtmp(0).Item("result"))
'                            'Shani (23-Nov123-2016-2016) -- End
'                            dtmp(0).Item("dresult") = CInt(dtmp(0).Item("result"))
'                        End If
'                    End If
'                End If
'            ElseIf menAction <> enAction.EDIT_ONE Then
'                If menAssess = enAssessmentMode.APPRAISER_ASSESSMENT Then
'                    'Shani (23-Nov-2016) -- Start
'                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    'If Session("ConsiderItemWeightAsNumber") Then
'                    '    e.Item.Cells(5).Text = CDbl(IIf(e.Item.Cells(2).Text = "", 1, e.Item.Cells(2).Text)) * CInt(IIf(e.Item.Cells(4).Text = "", 1, e.Item.Cells(4).Text))
'                    'Else
'                    '    e.Item.Cells(5).Text = 1 * CInt(IIf(e.Item.Cells(4).Text = "", 1, e.Item.Cells(4).Text))
'                    'End If
'                    'e.Item.Cells(7).Text = 0 : e.Item.Cells(9).Text = ""
'                    If Session("ConsiderItemWeightAsNumber") Then
'                        e.Item.Cells(disGEolumn("dgcolharemarkGE")).Text = CDbl(IIf(e.Item.Cells(disGEolumn("dgcolhGEWeight")).Text = "", 1, e.Item.Cells(disGEolumn("dgcolhGEWeight")).Text)) * CInt(IIf(e.Item.Cells(disGEolumn("objdgcolhedisplayGE")).Text = "", 1, e.Item.Cells(disGEolumn("objdgcolhedisplayGE")).Text))
'                    Else
'                        e.Item.Cells(disGEolumn("dgcolharemarkGE")).Text = 1 * CInt(IIf(e.Item.Cells(disGEolumn("objdgcolhedisplayGE")).Text = "", 1, e.Item.Cells(disGEolumn("objdgcolhedisplayGE")).Text))
'                    End If
'                    e.Item.Cells(disGEolumn("dgcolhaselfGE")).Text = 0 : e.Item.Cells(disGEolumn("dgcolharemarkGE")).Text = ""
'                    'Shani (23-Nov123-2016-2016) -- End
'                End If
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub dgvItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvItems.RowDataBound
'        Try
'            If e.Row.RowType = DataControlRowType.DataRow Then
'                Dim imgAdd As New ImageButton()
'                imgAdd.ID = "imgAdd"
'                imgAdd.Attributes.Add("Class", "objAddBtn")
'                imgAdd.CommandName = "objAdd"
'                imgAdd.ImageUrl = "~/images/add_16.png"
'                imgAdd.ToolTip = "New"
'                AddHandler imgAdd.Click, AddressOf imgAdd_Click
'                e.Row.Cells(0).Controls.Add(imgAdd)

'                Dim imgEdit As New ImageButton()
'                imgEdit.ID = "imgEdit"
'                imgEdit.Attributes.Add("Class", "objAddBtn")
'                imgEdit.CommandName = "objEdit"
'                imgEdit.ImageUrl = "~/images/Edit.png"
'                imgEdit.ToolTip = "Edit"
'                AddHandler imgEdit.Click, AddressOf imgEdit_Click
'                e.Row.Cells(1).Controls.Add(imgEdit)


'                Dim imgDelete As New ImageButton()
'                imgDelete.ID = "imgDelete"
'                imgDelete.Attributes.Add("Class", "objAddBtn")
'                imgDelete.CommandName = "objDelete"
'                imgDelete.ImageUrl = "~/images/remove.png"
'                imgDelete.ToolTip = "Delete"
'                AddHandler imgDelete.Click, AddressOf imgDelete_Click
'                e.Row.Cells(2).Controls.Add(imgDelete)
'                If dtCustomTabularGrid.Rows(e.Row.RowIndex)("analysisunkid") <= 0 AndAlso mintAssessAnalysisUnkid <= 0 Then
'                ElseIf dtCustomTabularGrid.Rows(e.Row.RowIndex)("analysisunkid") <> mintAssessAnalysisUnkid Then
'                    e.Row.Cells(2).Controls.Remove(imgDelete)
'                    Dim objAnalysis As New clsevaluation_analysis_master
'                    objAnalysis._Analysisunkid = dtCustomTabularGrid.Rows(e.Row.RowIndex)("analysisunkid")
'                    If menAssess < objAnalysis._Assessmodeid Then
'                        e.Row.Cells(1).Controls.Remove(imgEdit)
'                    End If
'                    objAnalysis = Nothing
'                End If
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub


'    'Shani (31-Aug-2016) -- Start
'    'Enhancement - Change Competencies List Design Given by Andrew
'    Protected Sub link_Click(ByVal sender As Object, ByVal e As System.EventArgs)
'        Try
'            txtData.Text = ""
'            Dim lnkWhatIsThis As LinkButton = CType(sender, LinkButton)
'            Dim xRow As DataGridItem = CType(lnkWhatIsThis.NamingContainer, DataGridItem)
'            If CBool(xRow.Cells(12).Text) = True Then
'                Dim objCOMaster As New clsCommon_Master
'                objCOMaster._Masterunkid = CInt(xRow.Cells(14).Text)
'                If objCOMaster._Description <> "" Then
'                    txtData.Text = objCOMaster._Description
'                    popup_ComInfo.Show()
'                Else
'                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
'                End If
'                objCOMaster = Nothing
'            ElseIf CBool(xRow.Cells(12).Text) = False Then
'                Dim objCPMsater As New clsassess_competencies_master
'                objCPMsater._Competenciesunkid = CInt(xRow.Cells(10).Text)
'                If objCPMsater._Description <> "" Then
'                    txtData.Text = objCPMsater._Description
'                    popup_ComInfo.Show()
'                Else
'                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "Sorry, No description set for the selected information."), Me)
'                End If
'                objCPMsater = Nothing
'            End If
'            dgvGE.DataSource = dtGE_TabularGrid
'            dgvGE.DataBind()
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "link_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub
'    'Shani (31-Aug-2016) -- End
'#End Region

'#Region "Control Event"

'    Protected Sub radOption_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOption.SelectedIndexChanged
'        Try
'            If radOption.SelectedValue = "2" Then
'                cboAssessor.DataSource = Nothing
'                Dim objExtAssessor As New clsexternal_assessor_master

'                'Shani(14-Sep-2015) -- Start
'                'Issue: TRA Training Comments & Changes Requested By Dennis
'                'Dim dsCombos As DataSet = objExtAssessor.GetDisplayNameComboList("Assessor", True)
'                Dim dsCombos As DataSet = objExtAssessor.GetDisplayNameComboList("Assessor", False)
'                'Shani(14-Sep-2015) -- End
'                With cboAssessor
'                    .DataValueField = "Id"
'                    .DataTextField = "Name"
'                    .DataSource = dsCombos.Tables("Assessor")
'                    .DataBind()
'                    'Shani(14-Sep-2015) -- Start
'                    'Issue: TRA Training Comments & Changes Requested By Dennis
'                    '.SelectedValue = 0
'                    .SelectedIndex = 0
'                    'Shani(14-Sep-2015) -- End
'                End With
'                Call cboAssessor_SelectedIndexChanged(cboAssessor, Nothing)
'            ElseIf radOption.SelectedValue = "1" Then
'                cboAssessor.DataSource = Nothing

'                'Shani(14-Sep-2015) -- Start
'                'Issue: TRA Training Comments & Changes Requested By Dennis
'                'Dim dsCombos As DataSet = objEAnalysisMst.getAssessorComboList("Assessor", True, , Session("UserId"), Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"))

'                'Shani(20-Nov-2015) -- Start
'                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                'Dim dsCombos As DataSet = objEAnalysisMst.getAssessorComboList("Assessor", False, , Session("UserId"), Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"))
'                Dim dsCombos As DataSet = objEAnalysisMst.getAssessorComboList(Session("Database_Name"), _
'                                                                               Session("UserId"), _
'                                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
'                                                                               Session("IsIncludeInactiveEmp"), _
'                                                                               "Assessor", False)
'                'Shani(20-Nov-2015) -- End

'                'Shani(14-Sep-2015) -- End

'                With cboAssessor
'                    .DataValueField = "Id"
'                    .DataTextField = "Name"
'                    .DataSource = dsCombos.Tables("Assessor")
'                    .DataBind()
'                    'Shani(14-Sep-2015) -- Start
'                    'Issue: TRA Training Comments & Changes Requested By Dennis
'                    '.SelectedValue = 0
'                    .SelectedIndex = 0
'                    'Shani(14-Sep-2015) -- End
'                End With
'                Call cboAssessor_SelectedIndexChanged(cboAssessor, Nothing)
'            End If

'        Catch ex As Exception

'        End Try
'    End Sub

'    Protected Sub cboAssessor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAssessor.SelectedIndexChanged
'        Try
'            If menAssess = enAssessmentMode.SELF_ASSESSMENT Then Exit Sub
'            If CInt(cboAssessor.SelectedValue) > 0 Then
'                Dim dsList As New DataSet
'                If radOption.SelectedValue = "1" Then

'                    'Shani(20-Nov-2015) -- Start
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    'dsList = objEAnalysisMst.getEmployeeBasedAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True, Session("IsIncludeInactiveEmp"), Session("EmployeeAsOnDate"), Session("UserId"))
'                    dsList = objEAnalysisMst.getEmployeeBasedAssessor(Session("Database_Name"), _
'                                                                      Session("UserId"), _
'                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
'                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
'                                                                      Session("IsIncludeInactiveEmp"), _
'                                                                      CInt(cboAssessor.SelectedValue), "AEmp", True)
'                    'Shani(20-Nov-2015) -- End

'                ElseIf radOption.SelectedValue = "2" Then
'                    Dim objExtAssessor As New clsexternal_assessor_master
'                    dsList = objExtAssessor.GetEmpBasedOnExtAssessor(CInt(cboAssessor.SelectedValue), "AEmp", True)
'                End If
'                With cboEmployee
'                    .DataValueField = "Id"
'                    .DataTextField = "Name"
'                    .DataSource = dsList.Tables("AEmp")
'                    .DataBind()
'                    .SelectedValue = 0
'                End With
'            Else
'                cboEmployee.DataSource = Nothing
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboEmployee.SelectedIndexChanged
'        Try
'            Dim objPrd As New clscommom_period_Tran

'            'Shani(20-Nov-2015) -- Start
'            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
'            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
'            'Shani(20-Nov-2015) -- End

'            mintYearUnkid = objPrd._Yearunkid
'            objPrd = Nothing

'            Dim objMapping As New clsAssess_Field_Mapping
'            iLinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
'            iMappingUnkid = objMapping.Get_MappingUnkId(CInt(cboPeriod.SelectedValue))
'            objMapping._Mappingunkid = iMappingUnkid
'            xTotAssignedWeight = objMapping._Weight
'            objMapping = Nothing

'            Dim objFMaster As New clsAssess_Field_Master
'            iExOrdr = objFMaster.Get_Field_ExOrder(iLinkedFieldId, True)
'            objFMaster = Nothing

'            objCCustomTran._EmployeeId = CInt(cboEmployee.SelectedValue)
'            'S.SANDEEP [29 DEC 2015] -- START

'            'Shani (26-Sep-2016) -- Start
'            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'            'objCCustomTran._AllowCustomItemInPlanning = CBool(Session("IncludeCustomItemInPlanning"))
'            'Shani (26-Sep-2016) -- End


'            objCCustomTran._PeriodId = CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue))
'            'S.SANDEEP [29 DEC 2015] -- END
'            mdtCustomEvaluation = objCCustomTran._DataTable
'            'If menAction <> enAction.EDIT_ONE Then
'            '    mdtCustomEvaluation.Rows.Clear()
'            'End If

'            If CInt(cboPeriod.SelectedValue) > 0 Then
'                If CStr(Session("Perf_EvaluationOrder")).Trim.Length > 0 Then
'                    Dim objCHeader As New clsassess_custom_header
'                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
'                    dsHeaders.Tables(0).Rows.Clear()
'                    Dim xRow As DataRow = Nothing
'                    Dim iOrdr() As String = CStr(Session("Perf_EvaluationOrder")).Split("|")
'                    If iOrdr.Length > 0 Then
'                        Select Case CInt(iOrdr(0))
'                            Case enEvaluationOrder.PE_BSC_SECTION
'                                xRow = dsHeaders.Tables(0).NewRow

'                                'Shani (26-Sep-2016) -- Start
'                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                                'xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Balance Score Card Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                'Shani (26-Sep-2016) -- End


'                                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
'                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
'                                xRow = dsHeaders.Tables(0).NewRow
'                                xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
'                            Case enEvaluationOrder.PE_CUSTOM_SECTION
'                                dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
'                        End Select
'                        Select Case CInt(iOrdr(1))
'                            Case enEvaluationOrder.PE_BSC_SECTION
'                                xRow = dsHeaders.Tables(0).NewRow

'                                'Shani (26-Sep-2016) -- Start
'                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                                'xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Balance Score Card Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                'Shani (26-Sep-2016) -- End


'                                If dsHeaders.Tables(0).Rows.Count > 0 Then
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
'                                Else
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
'                                End If
'                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
'                                xRow = dsHeaders.Tables(0).NewRow
'                                xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                If dsHeaders.Tables(0).Rows.Count > 0 Then
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
'                                Else
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
'                                End If
'                            Case enEvaluationOrder.PE_CUSTOM_SECTION
'                                If dsHeaders.Tables(0).Rows.Count > 0 Then
'                                    Dim dsList As New DataSet
'                                    dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
'                                    dsHeaders.Tables(0).Merge(dsList.Tables(0), True)
'                                Else
'                                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
'                                End If
'                        End Select
'                        Select Case CInt(iOrdr(2))
'                            Case enEvaluationOrder.PE_BSC_SECTION
'                                xRow = dsHeaders.Tables(0).NewRow

'                                'Shani (26-Sep-2016) -- Start
'                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                                'xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Balance Score Card Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                xRow.Item("Id") = -3 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 40, "Objectives/Goals/Targets") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                'Shani (26-Sep-2016) -- End


'                                If dsHeaders.Tables(0).Rows.Count > 0 Then
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
'                                Else
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
'                                End If
'                            Case enEvaluationOrder.PE_COMPETENCY_SECTION
'                                xRow = dsHeaders.Tables(0).NewRow
'                                xRow.Item("Id") = -2 : xRow.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Competencies Evaluation") : xRow.Item("periodunkid") = CInt(cboPeriod.SelectedValue)
'                                If dsHeaders.Tables(0).Rows.Count > 0 Then
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, dsHeaders.Tables(0).Rows.Count)
'                                Else
'                                    dsHeaders.Tables(0).Rows.InsertAt(xRow, 0)
'                                End If
'                            Case enEvaluationOrder.PE_CUSTOM_SECTION
'                                If dsHeaders.Tables(0).Rows.Count > 0 Then
'                                    Dim dsList As New DataSet
'                                    dsList = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
'                                    dsHeaders.Tables(0).Merge(dsList.Tables(0), True)
'                                Else
'                                    dsHeaders = objCHeader.getComboList(CInt(cboPeriod.SelectedValue), False, "List", True)
'                                End If
'                        End Select
'                    End If
'                    objCHeader = Nothing
'                End If
'            Else
'                If dsHeaders IsNot Nothing AndAlso dsHeaders.Tables.Count > 0 Then
'                    dsHeaders.Tables(0).Rows.Clear()
'                End If
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Protected Sub dgv_Citems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgv_Citems.ItemDataBound
'        Try
'            dtCItems = Me.Session("dtCItems")
'            If e.Item.ItemIndex > -1 Then
'                If CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.FREE_TEXT Then
'                    Dim txt As New TextBox
'                    txt.ID = "txt" & e.Item.Cells(4).Text
'                    txt.TextMode = TextBoxMode.MultiLine
'                    txt.Style.Add("resize", "none")
'                    'SHANI [21 Mar 2015]-START
'                    'Issue : Fixing Issues Sent By Andrew in PA Testing.
'                    'txt.Width = Unit.Percentage(97)
'                    txt.Width = Unit.Percentage(100)
'                    txt.CssClass = "removeTextcss"
'                    'SHANI [21 Mar 2015]--END 

'                    If CBool(e.Item.Cells(3).Text) Then
'                        txt.ReadOnly = True
'                    End If
'                    'S.SANDEEP [12 OCT 2016] -- START
'                    'If CInt(Me.ViewState("RowIndex")) > -1 Then
'                    '    txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
'                    'End If
'                    If mblnIsMatchCompetencyStructure Then
'                        txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
'                    ElseIf CInt(Me.ViewState("RowIndex")) > -1 Then
'                        txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
'                    End If
'                    If CBool(e.Item.Cells(6).Text) Then
'                        txt.ReadOnly = True
'                    End If
'                    'S.SANDEEP [12 OCT 2016] -- END
'                    e.Item.Cells(1).Controls.Add(txt)
'                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.DATE_SELECTION Then
'                    Dim dtp As Control
'                    dtp = LoadControl("~/Controls/DateCtrl.ascx")
'                    dtp.ID = "dtp" & e.Item.Cells(4).Text

'                    'SHANI [21 Mar 2015]-START
'                    'Issue : Fixing Issues Sent By Andrew in PA Testing.
'                    'CType(dtp, Controls_DateCtrl).AutoPostBack = False
'                    CType(dtp, Controls_DateCtrl).AutoPostBack = True
'                    'SHANI [21 Mar 2015]--END 

'                    If CBool(e.Item.Cells(3).Text) Then
'                        CType(dtp, Controls_DateCtrl).Enabled = False
'                    End If
'                    If CInt(Me.ViewState("RowIndex")) > -1 Then
'                        If dtCItems.Rows(e.Item.ItemIndex)("custom_value").ToString().Trim.Length > 0 Then
'                            CType(dtp, Controls_DateCtrl).SetDate = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
'                        End If

'                    End If
'                    'SHANI [21 Mar 2015]-START
'                    'Issue : Fixing Issues Sent By Andrew in PA Testing.
'                    AddHandler CType(dtp, Controls_DateCtrl).TextChanged, AddressOf dtpCustomItem_TextChanged
'                    'SHANI [21 Mar 2015]--END 

'                    e.Item.Cells(1).Controls.Add(dtp)
'                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.SELECTION Then
'                    Dim cbo As New DropDownList
'                    cbo.ID = "cbo" & e.Item.Cells(4).Text

'                    'SHANI [01 FEB 2015]-START
'                    'Enhancement - REDESIGN SELF SERVICE.
'                    cbo.Width = Unit.Pixel(250)
'                    'cbo.Height = Unit.Pixel(20)
'                    'SHANI [01 FEB 2015]--END
'                    If CBool(e.Item.Cells(3).Text) Then
'                        cbo.Enabled = False
'                    End If
'                    Select Case CInt(e.Item.Cells(5).Text)
'                        Case clsassess_custom_items.enSelectionMode.TRAINING_OBJECTIVE
'                            Dim objCMaster As New clsCommon_Master
'                            Dim dsList As New DataSet
'                            dsList = objCMaster.getComboList(clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER, True, "List")
'                            With cbo
'                                .DataValueField = "masterunkid"
'                                .DataTextField = "name"
'                                .DataSource = dsList.Tables(0)
'                                .ToolTip = "name"
'                                .SelectedValue = 0
'                                .DataBind()
'                            End With
'                            objCMaster = Nothing
'                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_COMPETENCIES
'                            Dim objCompetency As New clsassess_competencies_master
'                            Dim dsList As New DataSet

'                            'Shani(20-Nov-2015) -- Start
'                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                            'dsList = objCompetency.getAssigned_Competencies_List(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), True)
'                            dsList = objCompetency.getAssigned_Competencies_List(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), eZeeDate.convertDate(Session("EmployeeAsOnDate")), True)
'                            'Shani(20-Nov-2015) -- End

'                            With cbo
'                                .DataValueField = "Id"
'                                .DataTextField = "Name"
'                                .DataSource = dsList.Tables(0)
'                                .ToolTip = "name"
'                                .SelectedValue = 0
'                                .DataBind()
'                            End With
'                            objCompetency = Nothing
'                        Case clsassess_custom_items.enSelectionMode.EMPLOYEE_GOALS
'                            Dim objEmpField1 As New clsassess_empfield1_master
'                            Dim dsList As New DataSet
'                            dsList = objEmpField1.getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", True, True)
'                            With cbo
'                                .DataValueField = "Id"
'                                .DataTextField = "Name"
'                                .DataSource = dsList.Tables(0)
'                                .ToolTip = "name"
'                                .SelectedValue = 0
'                                .DataBind()
'                            End With
'                            objEmpField1 = Nothing
'                    End Select
'                    If CInt(Me.ViewState("RowIndex")) > -1 Then
'                        cbo.SelectedValue = IIf(dtCItems.Rows(e.Item.ItemIndex)("custom_value") = "", 0, dtCItems.Rows(e.Item.ItemIndex)("custom_value"))
'                    End If
'                    e.Item.Cells(1).Controls.Add(cbo)
'                ElseIf CInt(e.Item.Cells(2).Text) = clsassess_custom_items.enCustomType.NUMERIC_DATA Then
'                    Dim txt As New TextBox
'                    txt.ID = "txtnum" & e.Item.Cells(4).Text
'                    If CBool(e.Item.Cells(3).Text) Then
'                        txt.ReadOnly = True
'                    End If
'                    txt.Style.Add("text-align", "right")
'                    txt.Attributes.Add("onKeypress", "return onlyNumbers(this, event);")
'                    'SHANI [21 Mar 2015]-START
'                    'Issue : Fixing Issues Sent By Andrew in PA Testing.
'                    'txt.Width = Unit.Pixel(245)
'                    txt.Width = Unit.Percentage(100)
'                    txt.CssClass = "removeTextcss"
'                    'SHANI [21 Mar 2015]--END 
'                    If CInt(Me.ViewState("RowIndex")) > -1 Then
'                        txt.Text = dtCItems.Rows(e.Item.ItemIndex)("custom_value")
'                    End If
'                    e.Item.Cells(1).Controls.Add(txt)
'                End If
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    'Shani (23-Nov-2016) -- Start
'    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'    '    Protected Sub TxtValRemarkGE_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
'    '        'SHANI [21 Mar 2015]-START
'    '        'Issue : Fixing Issues Sent By Andrew in PA Testing.
'    '        Dim bln As Boolean = False
'    '        'SHANI [21 Mar 2015]--END
'    '        Try
'    '            Dim xRow As DataGridItem = CType(CType(sender, TextBox).NamingContainer, DataGridItem)
'    '            'S.SANDEEP [21 JAN 2015] -- START
'    '            Dim iDecWgt As Decimal = 0
'    '            Decimal.TryParse(xRow.Cells(1).Text, iDecWgt)
'    '            mdecItemWeight = iDecWgt
'    '            Me.ViewState("mdecItemWeight") = mdecItemWeight
'    '            'S.SANDEEP [21 JAN 2015] -- END

'    '            Dim txt As TextBox = CType(sender, TextBox)
'    '            If CType(sender, TextBox).ID = "dgcolharemarkGE" Then
'    '                Dim iResult As Decimal = 0
'    '                Decimal.TryParse(CType(xRow.Cells(6).Controls(1), TextBox).Text, iResult)
'    '                'S.SANDEEP [21 JAN 2015] -- START
'    '                'Call Evaluated_Data_GE(iResult, txt.Text, xRow.ItemIndex, xRow)
'    '                Call Evaluated_Data_GE(iResult, txt.Text, xRow.ItemIndex, xRow, True)
'    '                'S.SANDEEP [21 JAN 2015] -- END
'    '            ElseIf CType(sender, TextBox).ID = "dgcolhaselfGE" Then
'    '                Dim iDecVal As Decimal
'    '                Decimal.TryParse(txt.Text, iDecVal)
'    '                If IsNumeric(iDecVal) Then
'    '                    Select Case CInt(Session("ScoringOptionId"))
'    '                        Case enScoringOption.SC_WEIGHTED_BASED
'    '                            'S.SANDEEP [21 JAN 2015] -- START
'    '                            'Dim iDecWgt As Decimal = 0
'    '                            'Decimal.TryParse(xRow.Cells(1).Text, iDecWgt)
'    '                            'S.SANDEEP [21 JAN 2015] -- END
'    '                            If CDec(iDecVal) > iDecWgt Then
'    '                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), Me)
'    '                                'SHANI [21 Mar 2015]-START
'    '                                'Issue : Fixing Issues Sent By Andrew in PA Testing.
'    '                                bln = True
'    '                                'SHANI [21 Mar 2015]--END 
'    '                                txt.Text = "" : Exit Sub
'    '                            Else
'    '                                GoTo iValid
'    '                            End If
'    '                        Case enScoringOption.SC_SCALE_BASED
'    '                            Decimal.TryParse(txt.Text, iDecVal)
'    '                            If IsNumeric(iDecVal) Then
'    '                                If CInt(xRow.Cells(9).Text) > 0 Then    'SCALE MASTER ID
'    '                                    Dim dsScore_Guide As New DataSet
'    '                                    Dim objScaleMaster As New clsAssessment_Scale
'    '                                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(xRow.Cells(9).Text))
'    '                                    objScaleMaster = Nothing

'    '                                    'S.SANDEEP [21 JAN 2015] -- START
'    '                                    mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
'    '                                    Me.ViewState("mdecMaxScale") = mdecMaxScale
'    '                                    'S.SANDEEP [21 JAN 2015] -- END

'    '                                    Dim dtmp() As DataRow = dsScore_Guide.Tables(0).Select("scale = " & CDec(iDecVal))

'    '                                    If dtmp.Length <= 0 Then
'    '                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Sorry, particular scale is not defined. Please refer score guide for the list of score(s) defined."), Me)
'    '                                        Dim dTemp() As DataRow = Nothing
'    '                                        dTemp = GetOldValue_GE(xRow)
'    '                                        If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
'    '                                            txt.Text = dTemp(0).Item("result")
'    '                                        Else
'    '                                            txt.Text = "" : txt.Focus()
'    '                                        End If
'    '                                        'SHANI [21 Mar 2015]-START
'    '                                        'Issue : Fixing Issues Sent By Andrew in PA Testing.
'    '                                        bln = True
'    '                                        'SHANI [21 Mar 2015]--END 
'    '                                        Exit Sub
'    '                                    Else
'    '                                        GoTo iValid
'    '                                    End If
'    '                                Else
'    '                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Sorry, no scale is not defined. Please define scale."), Me)
'    '                                    Dim dTemp() As DataRow = Nothing
'    '                                    dTemp = GetOldValue_GE(xRow)
'    '                                    If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
'    '                                        txt.Text = dTemp(0).Item("result")
'    '                                    Else
'    '                                        txt.Text = "" : txt.Focus()
'    '                                    End If
'    '                                    'SHANI [21 Mar 2015]-START
'    '                                    'Issue : Fixing Issues Sent By Andrew in PA Testing.
'    '                                    bln = True
'    '                                    'SHANI [21 Mar 2015]--END 
'    '                                    Exit Sub
'    '                                End If
'    '                            Else
'    '                                Exit Sub
'    '                            End If
'    '                    End Select
'    'iValid:             If Validation() = False Then bln = True : Exit Sub 'SHANI [21 Mar 2015]-bln = True : 
'    '                    If Is_Already_Assessed() = False Then
'    '                        'SHANI [21 Mar 2015]-START
'    '                        'Issue : Fixing Issues Sent By Andrew in PA Testing.
'    '                        bln = True
'    '                        'SHANI [21 Mar 2015]--END 
'    '                        Exit Sub
'    '                    End If
'    '                    Call Evaluated_Data_GE(iDecVal, CType(xRow.Cells(8).Controls(1), TextBox).Text, xRow.ItemIndex, xRow)
'    '                Else
'    '                    txt.Text = ""
'    '                End If
'    '            End If
'    '            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & objpnlGE.ClientID & ");", True)
'    '        Catch ex As Exception
'    '            DisplayMessage.DisplayError(ex,Me)
'    '        Finally
'    '            'SHANI [21 Mar 2015]-START
'    '            'Issue : Fixing Issues Sent By Andrew in PA Testing.

'    '            'SHANI [21 Mar 2015]-START
'    '            'Issue : Fixing Issues Sent By Andrew in PA Testing.
'    '            'If bln Then
'    '            '    CType(sender, TextBox).Focus()
'    '            'Else
'    '            '    Dim dtRow As DataGridItem = CType(CType(sender, TextBox).NamingContainer, DataGridItem)
'    '            '    If CType(sender, TextBox).ID = "dgcolhaselfGE" Then
'    '            '        CType(dtRow.FindControl("dgcolharemarkGE"), TextBox).Focus()
'    '            '    ElseIf CType(sender, TextBox).ID = "dgcolharemarkGE" Then
'    '            '        If dgvGE.Items.Count > (dtRow.ItemIndex + 1) Then
'    '            '            For i As Integer = dtRow.ItemIndex + 1 To dgvGE.Items.Count - 1
'    '            '                If CBool(dgvGE.Items(i).Cells(12).Text) = False AndAlso CBool(dgvGE.Items(i).Cells(13).Text) = False Then
'    '            '                    CType(dgvGE.Items(i).FindControl("dgcolhaselfGE"), TextBox).Focus()
'    '            '                    Exit For
'    '            '                End If
'    '            '            Next
'    '            '        Else
'    '            '            CType(sender, TextBox).Focus()
'    '            '        End If
'    '            '    End If
'    '            'End If
'    '            'SHANI [21 Mar 2015]--END

'    '            'SHANI [21 Mar 2015]--END 
'    '        End Try
'    '    End Sub
'    Protected Sub TxtValRemarkGE_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
'        Dim bln As Boolean = False
'        Try
'            Dim xRow As DataGridItem = CType(CType(sender, TextBox).NamingContainer, DataGridItem)

'            Dim iDecWgt As Decimal = 0
'            Decimal.TryParse(xRow.Cells(disGEolumn("dgcolhGEWeight")).Text, iDecWgt)
'            mdecItemWeight = iDecWgt
'            Me.ViewState("mdecItemWeight") = mdecItemWeight

'            Dim txt As TextBox = CType(sender, TextBox)
'            If txt.ID = "dgcolharemarkGE" Then
'                Dim iResult As Decimal = 0 : Dim iAgreedScore As Decimal = 0
'                Decimal.TryParse(CType(xRow.Cells(disGEolumn("dgcolhaselfGE")).Controls(1), TextBox).Text, iResult)
'                Decimal.TryParse(CType(xRow.Cells(disGEolumn("dgcolhaAgreedScoreGE")).FindControl("dgcolhaAgreedScoreGE"), TextBox).Text, iAgreedScore)
'                Call Evaluated_Data_GE(iResult, txt.Text, xRow.ItemIndex, xRow, iAgreedScore, True)
'            ElseIf txt.ID = "dgcolhaselfGE" OrElse txt.ID = "dgcolhaAgreedScoreGE" Then
'                Dim iDecVal As Decimal
'                Decimal.TryParse(txt.Text, iDecVal)
'                If IsNumeric(iDecVal) Then
'                    Select Case CInt(Session("ScoringOptionId"))
'                        Case enScoringOption.SC_WEIGHTED_BASED
'                            If CDec(iDecVal) > iDecWgt Then
'                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), Me)
'                                bln = True
'                                txt.Text = "" : Exit Sub
'                            Else
'                                GoTo iValid
'                            End If
'                        Case enScoringOption.SC_SCALE_BASED
'                            Decimal.TryParse(txt.Text, iDecVal)
'                            If IsNumeric(iDecVal) Then
'                                If CInt(xRow.Cells(disGEolumn("objdgcolhscalemasterunkidGE")).Text) > 0 Then    'SCALE MASTER ID
'                                    Dim dsScore_Guide As New DataSet
'                                    Dim objScaleMaster As New clsAssessment_Scale
'                                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(xRow.Cells(disGEolumn("objdgcolhscalemasterunkidGE")).Text))
'                                    objScaleMaster = Nothing

'                                    mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
'                                    Me.ViewState("mdecMaxScale") = mdecMaxScale
'                                    Dim dtmp() As DataRow = dsScore_Guide.Tables(0).Select("scale = " & CDec(iDecVal))

'                                    If dtmp.Length <= 0 Then
'                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Sorry, particular scale is not defined. Please refer score guide for the list of score(s) defined."), Me)
'                                        Dim dTemp() As DataRow = Nothing
'                                        dTemp = GetOldValue_GE(xRow)
'                                        If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
'                                            txt.Text = dTemp(0).Item("result")
'                                        Else
'                                            txt.Text = "" : txt.Focus()
'                                        End If
'                                        bln = True
'                                        Exit Sub
'                                    Else
'                                        GoTo iValid
'                                    End If
'                                Else
'                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Sorry, no scale is not defined. Please define scale."), Me)
'                                    Dim dTemp() As DataRow = Nothing
'                                    dTemp = GetOldValue_GE(xRow)
'                                    If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
'                                        txt.Text = dTemp(0).Item("result")
'                                    Else
'                                        txt.Text = "" : txt.Focus()
'                                    End If
'                                    bln = True
'                                    Exit Sub
'                                End If
'                            Else
'                                Exit Sub
'                            End If
'                    End Select
'iValid:             If Validation() = False Then bln = True : Exit Sub
'                    If Is_Already_Assessed() = False Then
'                        bln = True
'                        Exit Sub
'                    End If

'                    'Shani (23-Nov-2016) -- Start
'                    'Enhancement - Add New PA Enhancement (Agreed Score,exc) Given by Akiba,KBC,TNP exc...
'                    'Call Evaluated_Data_GE(iDecVal, CType(xRow.Cells(disGEolumn("dgcolharemarkGE")).Controls(1), TextBox).Text, xRow.ItemIndex, xRow)
'                    If txt.ID = "dgcolhaAgreedScoreGE" Then
'                        Dim idecScore As Decimal = 0 : Decimal.TryParse(CType(xRow.Cells(disGEolumn("dgcolhaselfGE")).Controls(1), TextBox).Text, idecScore)
'                        Call Evaluated_Data_GE(idecScore, CType(xRow.Cells(disGEolumn("dgcolharemarkGE")).Controls(1), TextBox).Text, xRow.ItemIndex, xRow, iDecVal)
'                    Else
'                        Dim idecScore As Decimal = 0 : Decimal.TryParse(CType(xRow.Cells(disGEolumn("dgcolhaAgreedScoreGE")).FindControl("dgcolhaAgreedScoreGE"), TextBox).Text, idecScore)
'                        Call Evaluated_Data_GE(iDecVal, CType(xRow.Cells(disGEolumn("dgcolharemarkGE")).Controls(1), TextBox).Text, xRow.ItemIndex, xRow, idecScore)
'                    End If

'                    'Shani (23-Nov123-2016-2016) -- End
'                Else
'                    txt.Text = ""
'                End If
'            End If
'            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & objpnlGE.ClientID & ");", True)
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub
'    'Shani (23-Nov123-2016-2016) -- End

'    Protected Sub TxtValRemarkBSC_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
'        'SHANI [21 Mar 2015]-START
'        'Issue : Fixing Issues Sent By Andrew in PA Testing.
'        Dim bln As Boolean = False
'        'SHANI [21 Mar 2015]--END
'        Try
'            Dim xRow As DataGridItem = CType(CType(sender, TextBox).NamingContainer, DataGridItem)
'            'S.SANDEEP [21 JAN 2015] -- START
'            Dim iDecWgt As Decimal = 0
'            Decimal.TryParse(xRow.Cells(13).Text, iDecWgt)
'            mdecItemWeight = iDecWgt
'            Me.ViewState("mdecItemWeight") = mdecItemWeight
'            'S.SANDEEP [21 JAN 2015] -- END

'            Dim txt As TextBox = CType(sender, TextBox)
'            If CType(sender, TextBox).ID = "dgcolharemarkBSC" Then
'                Dim iResult As Decimal = 0
'                Decimal.TryParse(CType(xRow.Cells(16).Controls(1), TextBox).Text, iResult)
'                'S.SANDEEP [21 JAN 2015] -- START
'                'Call Evaluated_Data_BSC(iResult, txt.Text, xRow.ItemIndex)
'                Call Evaluated_Data_BSC(iResult, txt.Text, xRow.ItemIndex, True)
'                'S.SANDEEP [21 JAN 2015] -- END
'            ElseIf CType(sender, TextBox).ID = "dgcolhaselfBSC" Then
'                'If txt.Text.Trim.Length > 0 Then
'                Dim iDecVal As Decimal
'                Decimal.TryParse(txt.Text, iDecVal)
'                If IsNumeric(iDecVal) Then
'                    Select Case CInt(Session("ScoringOptionId"))
'                        Case enScoringOption.SC_WEIGHTED_BASED
'                            'S.SANDEEP [21 JAN 2015] -- START
'                            'Dim iDecWgt As Decimal = 0
'                            'Decimal.TryParse(xRow.Cells(13).Text, iDecWgt)
'                            'S.SANDEEP [21 JAN 2015] -- END
'                            If CDec(iDecVal) > iDecWgt Then
'                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, you cannot set more result value than assigned weight."), Me)
'                                'SHANI [21 Mar 2015]-START
'                                'Issue : Fixing Issues Sent By Andrew in PA Testing.
'                                bln = True
'                                'SHANI [21 Mar 2015]--END 
'                                txt.Text = "" : Exit Sub
'                            Else
'                                GoTo iValid
'                            End If
'                        Case enScoringOption.SC_SCALE_BASED
'                            Decimal.TryParse(txt.Text, iDecVal)
'                            If IsNumeric(iDecVal) Then
'                                If CInt(xRow.Cells(20).Text) > 0 Then
'                                    Dim dsScore_Guide As New DataSet
'                                    Dim objScaleMaster As New clsAssessment_Scale
'                                    dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(xRow.Cells(20).Text))
'                                    objScaleMaster = Nothing

'                                    'S.SANDEEP [21 JAN 2015] -- START
'                                    mdecMaxScale = dsScore_Guide.Tables(0).Compute("MAX(scale)", "")
'                                    Me.ViewState("mdecMaxScale") = mdecMaxScale
'                                    'S.SANDEEP [21 JAN 2015] -- END

'                                    Dim dtmp() As DataRow = dsScore_Guide.Tables(0).Select("scale = " & CDec(iDecVal))
'                                    If dtmp.Length <= 0 Then
'                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Sorry, particular scale is not defined. Please refer score guide for the list of score(s) defined."), Me)
'                                        Dim dTemp() As DataRow = Nothing
'                                        dTemp = GetOldValue_BSC(xRow.ItemIndex)
'                                        If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
'                                            txt.Text = dTemp(0).Item("result")
'                                        Else
'                                            txt.Text = "" : txt.Focus()
'                                        End If
'                                        'SHANI [21 Mar 2015]-START
'                                        'Issue : Fixing Issues Sent By Andrew in PA Testing.
'                                        bln = True
'                                        'SHANI [21 Mar 2015]--END 
'                                        Exit Sub
'                                    Else
'                                        GoTo iValid
'                                    End If
'                                Else
'                                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Sorry, no scale is not defined. Please define scale."), Me)
'                                    Dim dTemp() As DataRow = Nothing
'                                    dTemp = GetOldValue_BSC(xRow.ItemIndex)
'                                    If dTemp IsNot Nothing AndAlso dTemp.Length > 0 Then
'                                        txt.Text = dTemp(0).Item("result")
'                                    Else
'                                        txt.Text = "" : txt.Focus()
'                                    End If
'                                    'SHANI [21 Mar 2015]-START
'                                    'Issue : Fixing Issues Sent By Andrew in PA Testing.
'                                    bln = True
'                                    'SHANI [21 Mar 2015]--END 
'                                    Exit Sub
'                                End If
'                            Else
'                                Exit Sub
'                            End If
'                    End Select
'iValid:             If Validation() = False Then bln = True : Exit Sub 'SHANI [21 Mar 2015]-bln = True 
'                    If Is_Already_Assessed() = False Then
'                        'SHANI [21 Mar 2015]-START
'                        'Issue : Fixing Issues Sent By Andrew in PA Testing.
'                        bln = True
'                        'SHANI [21 Mar 2015]--END 
'                        txt.Text = "" : Exit Sub
'                    End If
'                    Call Evaluated_Data_BSC(iDecVal, CType(xRow.Cells(17).Controls(1), TextBox).Text, xRow.ItemIndex)
'                Else
'                    txt.Text = ""
'                End If
'                'End If
'            End If
'            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & objpnlBSC.ClientID & ");", True)
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'            'SHANI [21 Mar 2015]-START
'            'Issue : Fixing Issues Sent By Andrew in PA Testing.

'            'SHANI [21 Mar 2015]-START
'            'Issue : Fixing Issues Sent By Andrew in PA Testing.
'            'If bln Then
'            '    CType(sender, TextBox).Focus()
'            'Else
'            '    Dim dtRow As DataGridItem = CType(CType(sender, TextBox).NamingContainer, DataGridItem)
'            '    If CType(sender, TextBox).ID = "dgcolhaselfBSC" Then
'            '        CType(dtRow.FindControl("dgcolharemarkBSC"), TextBox).Focus()
'            '    ElseIf CType(sender, TextBox).ID = "dgcolharemarkBSC" Then
'            '        If dgvBSC.Items.Count > (dtRow.ItemIndex + 1) Then
'            '            If CBool(dgvBSC.Items(dtRow.ItemIndex + 1).Cells(18).Text) Then
'            '                'For i As Integer = dtRow.ItemIndex + 1 To dgvBSC.Items.Count - 1
'            '                '    If CBool(dgvBSC.Items(i).Cells(18).Text) = False Then
'            '                '        CType(dgvBSC.Items(i).FindControl("dgcolhaselfBSC"), TextBox).Focus()
'            '                '        Exit For
'            '                '    End If
'            '                'Next
'            '                CType(dgvBSC.Items(dtRow.ItemIndex + 2).FindControl("dgcolhaselfBSC"), TextBox).Focus()
'            '            Else
'            '                CType(dgvBSC.Items(dtRow.ItemIndex + 1).FindControl("dgcolhaselfBSC"), TextBox).Focus()
'            '            End If
'            '        Else
'            '            CType(sender, TextBox).Focus()
'            '        End If
'            '    End If
'            '    End If
'            'SHANI [21 Mar 2015]--END

'            'SHANI [21 Mar 2015]--END 
'        End Try
'    End Sub

'    Protected Sub dgcolhGEScore_Click(ByVal sender As Object, ByVal e As System.EventArgs)
'        Try
'            If CInt(CType(sender, LinkButton).CommandArgument) > 0 Then 'SCALE MASTER ID
'                Dim dsScore_Guide As New DataSet
'                Dim objScaleMaster As New clsAssessment_Scale
'                dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(CType(sender, LinkButton).CommandArgument))
'                objScaleMaster = Nothing
'                dgvGEScoreGuide.DataSource = dsScore_Guide.Tables(0)
'                dgvGEScoreGuide.DataBind()
'                popup_ViewGuideGE.Show()
'                'S.SANDEEP [ 15 DEC 2014 ] -- START
'                iWeightTotal = 0 'S.SANDEEP [ 17 DEC 2014 ] -- START -- END
'                dgvGE.DataSource = dtGE_TabularGrid
'                dgvGE.DataBind()
'                'S.SANDEEP [ 15 DEC 2014 ] -- END
'            Else
'                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 26, "Sorry no score gude defined for the selected item."), Me)
'            End If
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "dgcolhGEScore_Click", mstrModuleName)
'        Finally
'        End Try
'    End Sub

'    Protected Sub dgcolhBSCScore_Click(ByVal sender As Object, ByVal e As System.EventArgs)
'        Try
'            If CInt(CType(sender, LinkButton).CommandArgument) > 0 Then 'SCALE MASTER ID
'                Dim dsScore_Guide As New DataSet
'                Dim objScaleMaster As New clsAssessment_Scale
'                dsScore_Guide = objScaleMaster.GetList("List", CInt(cboPeriod.SelectedValue), CInt(CType(sender, LinkButton).CommandArgument))
'                objScaleMaster = Nothing
'                dgvBSCScoreGuide.DataSource = dsScore_Guide.Tables(0)
'                dgvBSCScoreGuide.DataBind()
'                popup_ViewGuideBSC.Show()
'                'S.SANDEEP [ 15 DEC 2014 ] -- START
'                iWeightTotal = 0 'S.SANDEEP [ 17 DEC 2014 ] -- START -- END
'                dgvBSC.DataSource = dtBSC_TabularGrid
'                dgvBSC.DataBind()
'                'S.SANDEEP [ 15 DEC 2014 ] -- END
'            Else
'                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 26, "Sorry no score gude defined for the selected item."), Me)
'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        Finally
'        End Try
'    End Sub

'    Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
'        Try
'            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
'            Dim xRow As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
'            'S.SANDEEP [12 OCT 2016] -- START
'            'Me.ViewState("RowIndex") = -1
'            'mstriEditingGUID = ""
'            If mblnIsMatchCompetencyStructure Then
'                Me.ViewState("RowIndex") = -1
'                mstriEditingGUID = dtCustomTabularGrid.Rows(xRow.RowIndex).Item("GUID")
'            Else
'                Me.ViewState("RowIndex") = -1
'                mstriEditingGUID = ""
'            End If
'            'S.SANDEEP [12 OCT 2016] -- END
'            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
'                Dim xtmp() As DataRow = mdtCustomEvaluation.Select("custom_header = '" & dtCustomTabularGrid.Rows(xRow.RowIndex).Item("Header_Name") & "' AND AUD <> 'D'")
'                If xtmp.Length > 0 AndAlso CBool(dtCustomTabularGrid.Rows(xRow.RowIndex).Item("Is_Allow_Multiple")) = False Then
'                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 118, "Sorry, this particular custom header is not set for allow multiple entries when defined."), Me)
'                    Exit Sub
'                End If
'            End If
'            Call Generate_Popup_Data(dtCustomTabularGrid.Rows(xRow.RowIndex).Item("Header_Id"))
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub imgEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
'        Try
'            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
'            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
'            Me.ViewState("RowIndex") = row.RowIndex
'            mstriEditingGUID = dtCustomTabularGrid.Rows(row.RowIndex).Item("GUID")
'            Call Generate_Popup_Data(dtCustomTabularGrid.Rows(row.RowIndex).Item("Header_Id"))
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub

'    Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
'        Try
'            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
'            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
'            Me.ViewState("RowIndex") = row.RowIndex
'            mstriEditingGUID = dtCustomTabularGrid.Rows(row.RowIndex).Item("GUID")
'            If mdtCustomEvaluation IsNot Nothing AndAlso mdtCustomEvaluation.Rows.Count > 0 Then
'                Dim xRow() As DataRow = Nothing
'                xRow = mdtCustomEvaluation.Select("customanalysistranguid = '" & dtCustomTabularGrid.Rows(row.RowIndex).Item("GUID") & "' AND AUD <> 'D'")
'                If xRow.Length > 0 Then
'                    lblTitle.Text = "Aruti"
'                    lblMessage.Text = "Please enter vaild reason to void following entry."
'                    txtMessage.Text = ""
'                    popup_CItemReason.Show()
'                End If

'            End If
'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub


'    'SHANI [21 Mar 2015]-START
'    'Issue : Fixing Issues Sent By Andrew in PA Testing.
'    Protected Sub dtpCustomItem_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
'        Try
'            If CDate(dtpAssessdate.GetDate) >= CDate(CType(CType(sender, TextBox).NamingContainer, Controls_DateCtrl).GetDate) Then
'                CType(CType(sender, TextBox).NamingContainer, Controls_DateCtrl).SetDate = Nothing
'                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmAddCustomValue", 2, "Sorry, Selected date should be greter than the assessment date selected."), Me)
'                CType(sender, Controls_DateCtrl).Focus()
'            End If

'        Catch ex As Exception
'            DisplayMessage.DisplayError(ex,Me)
'        End Try
'    End Sub
'    'SHANI [21 Mar 2015]--END 

'#End Region

'End Class
