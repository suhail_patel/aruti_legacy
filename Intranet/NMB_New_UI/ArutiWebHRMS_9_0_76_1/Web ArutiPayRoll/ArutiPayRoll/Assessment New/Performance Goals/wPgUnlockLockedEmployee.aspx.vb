﻿#Region " Import "

Imports System.Data
Imports Aruti.Data
Imports System.Drawing
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Data.SqlClient
Imports System.Globalization

#End Region

Partial Class Assessment_New_Performance_Goals_wPgUnlockLockedEmployee
    Inherits Basepage

#Region " Private Variables "

    Private objUnlock As New clsassess_plan_eval_lockunlock
    Private mstrModuleName_unlockemployee As String = "frmUnlockEmployee"
    Private mdtUnlockEmployeePrdEndDate As Date = Nothing
    Private mdtUnlockEmployee As DataTable = Nothing
    Private DisplayMessage As New CommonCodes
    Private mintAssessorMasterId As Integer = 0
    Private mintAssessorEmployeeId As Integer = 0

#End Region

#Region " Page Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Call FillCombo()
            Else
                mintAssessorMasterId = Me.ViewState("mintAssessorMasterId")
                mintAssessorEmployeeId = Me.ViewState("mintAssessorEmployeeId")
                mdtUnlockEmployeePrdEndDate = Me.ViewState("mdtUnlockEmployeePrdEndDate")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mintAssessorMasterId") = mintAssessorMasterId
            Me.ViewState("mintAssessorEmployeeId") = mintAssessorEmployeeId
            Me.ViewState("mdtUnlockEmployeePrdEndDate") = mdtUnlockEmployeePrdEndDate
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            'S.SANDEEP |02-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1, False, False, True)
            'S.SANDEEP |02-MAR-2021| -- END
            'Dim mintCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            With cboUnlockPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy
                .DataBind()
            End With

            dsList = objUnlock.GetLockTypeComboList("List", True)
            Dim dTable As DataTable = Nothing : Dim strIds As String = String.Empty
            If Session("AllowToUnlockEmployeeInPlanning") = False Then
                strIds &= "," & CInt(clsassess_plan_eval_lockunlock.enAssementLockType.PLANNING).ToString()
            End If
            If Session("AllowToUnlockEmployeeInAssessment") = False Then
                strIds &= "," & CInt(clsassess_plan_eval_lockunlock.enAssementLockType.ASSESSMENT).ToString()
            End If
            If strIds.Trim.Length > 0 Then
                strIds = Mid(strIds, 2)
                dTable = New DataView(dsList.Tables(0), "Id NOT IN (" & strIds & ")", "Id", DataViewRowState.CurrentRows).ToTable().Copy()
                If dTable Is Nothing Then
                    dTable = dsList.Tables(0)
                End If
            Else
                dTable = dsList.Tables(0)
            End If
            With cboLockType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dTable
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillUnlockEmpGrid()
        Dim dsList As New DataSet
        Dim strFilter As String = String.Empty
        Try
            If CInt(cboUnlockPeriod.SelectedValue) > 0 Then
                strFilter &= "AND periodunkid = " & CInt(cboUnlockPeriod.SelectedValue)
            End If
            If CInt(cboLockType.SelectedValue) > 0 Then
                strFilter &= "AND locktypeid = " & CInt(cboLockType.SelectedValue)
            End If

            Dim csvIds As String = String.Empty
            Dim dsMapEmp As New DataSet
            Dim objEval As New clsevaluation_analysis_master
            dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
                                                    Session("UserId"), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                    True, Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)

            If dsMapEmp.Tables("List").Rows.Count > 0 Then

                mintAssessorMasterId = CInt(dsMapEmp.Tables("List").Rows(0)("Id"))
                mintAssessorEmployeeId = CInt(dsMapEmp.Tables("List").Rows(0)("EmpId"))

                dsList = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                          Session("UserId"), _
                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                          Session("IsIncludeInactiveEmp"), _
                                                          CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)


                strFilter = "AND hremployee_master.employeeunkid IN "
                csvIds = String.Join(",", dsList.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
                If csvIds.Trim.Length > 0 Then
                    strFilter &= "(" & csvIds & ")"
                Else
                    strFilter &= "(0)"
                End If
            Else
                strFilter = "AND hremployee_master.employeeunkid IN (0) "
            End If

            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)

            dsList = objUnlock.GetList(Session("Database_Name"), _
                                       Session("UserId"), _
                                       Session("Fin_year"), _
                                       Session("CompanyUnkId"), _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                       eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                       Session("UserAccessModeSetting"), True, _
                                       Session("IsIncludeInactiveEmp"), "List", strFilter, False)

            Dim dCol As New DataColumn
            With dCol
                .DataType = GetType(Boolean)
                .ColumnName = "ischeck"
                .DefaultValue = False
            End With
            dsList.Tables(0).Columns.Add(dCol)
            dCol = New DataColumn
            With dCol
                .DataType = GetType(String)
                .ColumnName = "message"
                .DefaultValue = ""
            End With
            dsList.Tables(0).Columns.Add(dCol)
            Dim blnFlag As Boolean = False
            mdtUnlockEmployee = dsList.Tables(0).Copy()
            If mdtUnlockEmployee.Rows.Count <= 0 Then
                mdtUnlockEmployee.Rows.Add(mdtUnlockEmployee.NewRow())
                blnFlag = True
            End If
            dgvUnlockEmp.DataSource = mdtUnlockEmployee
            dgvUnlockEmp.DataBind()
            If blnFlag Then dgvUnlockEmp.Rows(0).Visible = False
            btnUProcess.Enabled = Not blnFlag
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function UpdateRowValue(ByVal dr As DataRow, ByVal intValue As Integer) As Boolean
        Try
            Dim mdtdate As DateTime = CDate(dr("lockunlockdatetime")).AddDays(intValue)
            If mdtUnlockEmployeePrdEndDate <> Nothing Then
                If mdtdate.Date > mdtUnlockEmployeePrdEndDate.Date Then
                    dr("message") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 5, "Sorry, Next Lock date exceed the period end date.")
                Else
                    dr("udays") = intValue
                    dr("nextlockdatetime") = mdtdate
                    dr("message") = ""
                    dr("unlockdays") = intValue
                End If
            Else
                dr("udays") = intValue
                dr("nextlockdatetime") = mdtdate
                dr("message") = ""
                dr("unlockdays") = intValue
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

#End Region

#Region " Button Event "

    Protected Sub btnUSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUSearch.Click
        Try
            If CInt(cboUnlockPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 1, "Period is compulsory information.Please select period."), Me)
                cboUnlockPeriod.Focus()
                Exit Sub
            End If

            If CInt(cboLockType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 2, "Lock type is compulsory information.Please select Lock type."), Me)
                cboLockType.Focus()
                Exit Sub
            End If
            Call FillUnlockEmpGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUReset.Click
        Try
            cboUnlockPeriod.SelectedValue = 0
            cboLockType.SelectedIndex = 0
            dgvUnlockEmp.DataSource = Nothing
            dgvUnlockEmp.DataBind()
            objlblPeriodDates.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUpdateEmp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateEmp.Click
        Try
            If dgvUnlockEmp.Rows.Count <= 0 Then Exit Sub

            If Convert.ToInt32(txtNextDays.Text) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 3, "Sorry, Next Lock days are mandatory information. Please set the next lock days."), Me)
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing

            If radApplyToAll.Checked = False AndAlso radApplyTochecked.Checked = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 4, "Sorry, Please select atleast one setting to apply changes for the selected or all employee(s)."), Me)
                Exit Sub
            End If

            If radApplyTochecked.Checked = True Then
                If dgvUnlockEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.Cells(0).FindControl("chkbox1"), CheckBox).Checked = True).Count <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 13, "Sorry, Please select atleast one employee from the list to apply changes."), Me)
                    Exit Sub
                End If
            End If

            If dgvUnlockEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.Cells(0).FindControl("chkbox1"), CheckBox).Checked = True).Count > 0 Then
                gRow = dgvUnlockEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.Cells(0).FindControl("chkbox1"), CheckBox).Checked = True)
            Else
                gRow = dgvUnlockEmp.Rows.Cast(Of GridViewRow).AsEnumerable()
            End If

            For Each row In gRow
                Dim mdtdate As DateTime = Nothing
                If mdtUnlockEmployeePrdEndDate <> Nothing Then
                    If row.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhlockunlockdatetime", False, True)).Text <> "&nbsp;" Then
                        mdtdate = CDate(row.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhlockunlockdatetime", False, True)).Text).AddDays(txtNextDays.Text)
                    End If
                    If mdtdate.Date > mdtUnlockEmployeePrdEndDate.Date Then
                        row.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhmessage", False, True)).Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 5, "Sorry, Next Lock date exceed the period end date.")
                    Else
                        row.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhudays", False, True)).Text = txtNextDays.Text
                        row.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhnextlockdatetime", False, True)).Text = mdtdate
                        row.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhmessage", False, True)).Text = "&nbsp;"

                        'row.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhunlockdays", False, True)).Text = txtNextDays.Text
                    End If
                Else
                    row.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhudays", False, True)).Text = txtNextDays.Text
                    row.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhnextlockdatetime", False, True)).Text = mdtdate
                    row.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhmessage", False, True)).Text = "&nbsp;"

                    'row.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhunlockdays", False, True)).Text = txtNextDays.Text
                End If
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnUProcess_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUProcess.Click
        Try
            If dgvUnlockEmp.Rows.Count <= 0 Then Exit Sub

            If dgvUnlockEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) x.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhudays", False, True)).Text <> "&nbsp;").Count <= 0 Then
                cnfUnlockDays.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 6, "You have not set any next locking days for") & " " & _
                                        cboLockType.SelectedItem.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 7, ", due to this employee will not be locked for planning once it's unlocked for the selected assessment period.") & vbCrLf & _
                                        Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 8, "Do you wish to continue?")
                cnfUnlockDays.Show()
            Else
                If dgvUnlockEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.Cells(0).FindControl("chkbox1"), CheckBox).Checked = True And x.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhmessage", False, True)).Text = "&nbsp;").Count <= 0 Then
                    cnfUnlockCheck.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 9, "You have not checked any employee from below list") & " " & _
                                         Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 10, ", due to this all employee will be unlocked for the selected assessment period.") & vbCrLf & _
                                         Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 8, "Do you wish to continue?")
                    cnfUnlockCheck.Show()
                Else
                    Call cnfUnlockCheck_buttonYes_Click(New Object(), New EventArgs())
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfUnlockDays_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfUnlockDays.buttonYes_Click
        Try
            If dgvUnlockEmp.Rows.Count <= 0 Then Exit Sub
            If dgvUnlockEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.Cells(0).FindControl("chkbox1"), CheckBox).Checked = True And x.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhmessage", False, True)).Text = "&nbsp;").Count <= 0 Then
                cnfUnlockCheck.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 9, "You have not checked any employee from below list") & " " & _
                                         Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 10, ", due to this all employee will be unlocked for the selected assessment period.") & vbCrLf & _
                                         Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 8, "Do you wish to continue?")
                cnfUnlockCheck.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfUnlockCheck_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfUnlockCheck.buttonYes_Click
        Try
            If dgvUnlockEmp.Rows.Count <= 0 Then Exit Sub
            'mdtUnlockEmployee = Me.ViewState("mdtUnlockEmployee")
            'Dim drRow() As DataRow = Nothing
            'If mdtUnlockEmployee.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True).Count() > 0 Then
            '    drRow = mdtUnlockEmployee.Select("ischeck = True And message = '' ")
            'Else
            '    drRow = mdtUnlockEmployee.Select("message = '' ")
            'End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            If dgvUnlockEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.Cells(0).FindControl("chkbox1"), CheckBox).Checked = True).Count > 0 Then
                gRow = dgvUnlockEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.Cells(0).FindControl("chkbox1"), CheckBox).Checked = True And x.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhmessage", False, True)).Text = "&nbsp;")
            Else
                gRow = dgvUnlockEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) x.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhmessage", False, True)).Text = "&nbsp;")
            End If

            Dim iMsgText As String = ""
            For Each dr In gRow
                iMsgText = ""
                objUnlock._Lockunkid = dgvUnlockEmp.DataKeys(dr.RowIndex)("lockunkid")
                objUnlock._Lockunlockdatetime = ConfigParameter._Object._CurrentDateAndTime
                objUnlock._Auditdatetime = ConfigParameter._Object._CurrentDateAndTime
                objUnlock._Audittype = enAuditType.EDIT
                objUnlock._Audituserunkid = Session("UserId")
                objUnlock._Form_Name = mstrModuleName_unlockemployee
                objUnlock._Hostname = Session("HOST_NAME")
                objUnlock._Ip = Session("IP_ADD")
                objUnlock._Islock = False
                objUnlock._Isunlocktenure = True
                objUnlock._Isweb = True
                If dr.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhnextlockdatetime", False, True)).Text <> "&nbsp;" Then
                    objUnlock._Nextlockdatetime = CDate(dr.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhnextlockdatetime", False, True)).Text)
                Else
                    objUnlock._Nextlockdatetime = Nothing
                End If
                objUnlock._Unlockdays = CInt(dr.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhudays", False, True)).Text)
                objUnlock._Unlockreason = txtUnlockEmpRemark.Text
                objUnlock._Unlockuserunkid = Session("UserId")
                If objUnlock.Update() = False Then
                    dr.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhmessage", False, True)).Text = objUnlock._Message
                    Continue For
                Else
                    objUnlock.SendUnlockNotification(dgvUnlockEmp.DataKeys(dr.RowIndex)("oName").ToString(), _
                                                     dgvUnlockEmp.DataKeys(dr.RowIndex)("emp_email").ToString(), _
                                                     cboUnlockPeriod.SelectedItem.Text, _
                                                     CType(cboLockType.SelectedValue, clsassess_plan_eval_lockunlock.enAssementLockType), _
                                                     Session("CompanyUnkId"), _
                                                     enLogin_Mode.MGR_SELF_SERVICE, _
                                                     IIf(Session("Firstname") & "" & Session("Surname") = "", Session("UserName"), Session("Firstname") & " " & Session("Surname")).ToString(), _
                                                     CInt(dr.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhudays", False, True)).Text), _
                                                     objUnlock._Nextlockdatetime, _
                                                     mstrModuleName_unlockemployee, _
                                                     Session("IP_ADD"), _
                                                     Session("UserId"), _
                                                     ConfigParameter._Object._CurrentDateAndTime, True, 0, Session("HOST_NAME"), _
                                                     Session("ArutiSelfServiceURL"))
                    dr.Cells(getColumnID_Griview(dgvUnlockEmp, "dgcolhmessage", False, True)).Text = cboLockType.SelectedItem.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_unlockemployee, 12, "Succesfully.")
                    Continue For
                End If
            Next
            txtUnlockEmpRemark.Text = ""
            txtNextDays.Text = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUClose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Grid Event(s) "

    Protected Sub dgvUnlockEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvUnlockEmp.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Call SetDateFormat()
                Dim index As Integer = 0

                index = GetColumnIndex.getColumnID_Griview(dgvUnlockEmp, "dgcolhlockunlockdatetime", False, True)
                If e.Row.Cells(index).Text.Length > 0 AndAlso e.Row.Cells(index).Text <> "&nbsp;" Then
                    e.Row.Cells(index).Text = CDate(e.Row.Cells(index).Text).Date.ToShortDateString()
                End If

                index = GetColumnIndex.getColumnID_Griview(dgvUnlockEmp, "dgcolhnextlockdatetime", False, True)
                If e.Row.Cells(index).Text.Length > 0 AndAlso e.Row.Cells(index).Text <> "&nbsp;" Then
                    e.Row.Cells(index).Text = CDate(e.Row.Cells(index).Text).Date.ToShortDateString()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Protected Sub cboUnlockPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboUnlockPeriod.SelectedIndexChanged
        Try
            If CInt(cboUnlockPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(Session("Database_Name").ToString()) = CInt(cboUnlockPeriod.SelectedValue)
                txtNextDays.Max = objPeriod._Constant_Days
                mdtUnlockEmployeePrdEndDate = objPeriod._End_Date.Date
                objlblPeriodDates.Text = objPeriod._Start_Date.Date.ToShortDateString() & " - " & objPeriod._End_Date.Date.ToShortDateString()
                objPeriod = Nothing
            Else
                objlblPeriodDates.Text = ""
                mdtUnlockEmployeePrdEndDate = Nothing
                txtNextDays.Max = 100
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboLockType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLockType.SelectedIndexChanged
        Try
            dgvUnlockEmp.DataSource = Nothing
            dgvUnlockEmp.DataBind()
            btnUProcess.Enabled = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_unlockemployee, 1, "Period is compulsory information.Please select period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_unlockemployee, 2, "Lock type is compulsory information.Please select Lock type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_unlockemployee, 3, "Sorry, Next Lock days are mandatory information. Please set the next lock days.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_unlockemployee, 4, "Sorry, Please select atleast one setting to apply changes for the selected or all employee(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_unlockemployee, 5, "Sorry, Next Lock date exceed the period end date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_unlockemployee, 6, "You have not set any next locking days for")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_unlockemployee, 7, ", due to this employee will not be locked for planning once it's unlocked for the selected assessment period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_unlockemployee, 8, "Do you wish to continue?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_unlockemployee, 9, "You have not checked any employee from below list")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_unlockemployee, 10, ", due to this all employee will be unlocked for the selected assessment period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_unlockemployee, 12, "Succesfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_unlockemployee, 13, "Sorry, Please select atleast one employee from the list to apply changes.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

End Class
