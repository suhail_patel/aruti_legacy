﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgUnlockLockedEmployee.aspx.vb" Inherits="Assessment_New_Performance_Goals_wPgUnlockLockedEmployee" %>

<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%@ Register Src="~/Controls/NumericTextBox.ascx" TagName="Numeric" TagPrefix="nud" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>
--%>

    <script type="text/javascript">
        //        $("[id*=chkHeder1]").live("click", function() {
        //            var chkHeader = $(this);
        //            var grid = $(this).closest("table");
        //            $("input[type=checkbox]", grid).each(function() {
        //                if (chkHeader.is(":checked")) {
        //                    //debugger;
        //                    $(this).attr("checked", "checked");

        //                } else {
        //                    $(this).removeAttr("checked");
        //                }
        //            });
        //        });
        //        $("[id*=chkbox1]").live("click", function() {
        //            var grid = $(this).closest("table");
        //            var chkHeader = $("[id*=chkHeader]", grid);
        //            var row = $(this).closest("tr")[0];
        //            //debugger;
        //            if (!$(this).is(":checked")) {
        //                var row = $(this).closest("tr")[0];
        //                chkHeader.removeAttr("checked");

        //            } else {

        //                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
        //                    chkHeader.attr("checked", "checked");
        //                }
        //            }

        //        });
        $("body").on("click", "[id*=chkHeder1]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkbox1]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=chkbox1]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeder1]", grid);
            debugger;
            if ($("[id*=chkbox1]", grid).length == $("[id*=chkbox1]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server" EnableViewState="true">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Unlock Employee(s)"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblUnlockFC" runat="server" Text="Filter Criteria"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblUnlockPeriod" runat="server" Width="100%" Text="Period" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboUnlockPeriod" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblLockType" runat="server" Text="Select Lock Type" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboLockType" runat="server" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:LinkButton ID="objlblPeriodDates" runat="server" Text="" CssClass="pull-left"
                                                            Font-Underline="false" Enabled="false" Font-Bold="true"></asp:LinkButton>
                                                        <asp:Button ID="btnUSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                                        <asp:Button ID="btnUReset" runat="server" Text="Search" CssClass="btn btn-default" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="card inner-card">
                                                    <div class="header">
                                                        <h2>
                                                            <asp:Label ID="lblUSetInfo" runat="server" Text="Set Information"></asp:Label>
                                                        </h2>
                                                    </div>
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblSetDays" runat="server" Text="Set Next Lock Days" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <nud:Numeric ID="txtNextDays" runat="server" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <asp:RadioButton ID="radApplyTochecked" runat="server" GroupName="setinfo" Text="Apply To Checked" />
                                                                    <asp:RadioButton ID="radApplyToAll" runat="server" GroupName="setinfo" Text="Apply To All" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="footer">
                                                        <asp:Button ID="btnUpdateEmp" runat="server" Text="Update" CssClass="btn btn-primary" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 150px">
                                            <asp:GridView ID="dgvUnlockEmp" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                Width="99%" AllowPaging="false" DataKeyNames="lockunkid,employeeunkid,unlockdays,oName,emp_email">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkHeder1" CssClass="filled-in" Text=" " runat="server" Enabled="true" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkbox1" runat="server" CssClass="filled-in" Text=" " Enabled="true"
                                                                CommandArgument='<%# Container.DataItemIndex %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Employee" HeaderText="Emplyoee" FooterText="dgcolhEmployee" />
                                                    <asp:BoundField DataField="lockunlockdatetime" HeaderText="Lock Date" FooterText="dgcolhlockunlockdatetime" />
                                                    <asp:BoundField DataField="udays" HeaderText="Unlock Day(s)" FooterText="dgcolhudays" />
                                                    <asp:BoundField DataField="nextlockdatetime" HeaderText="Next Lock Date" FooterText="dgcolhnextlockdatetime" />
                                                    <asp:BoundField DataField="message" HeaderText="Message" FooterText="dgcolhmessage" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblUnlockEmpRemark" runat="server" Width="100%" Text="Remark" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtUnlockEmpRemark" runat="server" TextMode="MultiLine" Rows="3"
                                                    class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnUProcess" runat="server" Text="Process" CssClass="btn btn-primary" />
                                <asp:Button ID="btnUClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                <asp:HiddenField ID="hdf_UnlockEmp" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                <ucCfnYesno:Confirmation ID="cnfUnlockDays" runat="server" Title="Aruti" />
                <ucCfnYesno:Confirmation ID="cnfUnlockCheck" runat="server" Title="Aruti" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
