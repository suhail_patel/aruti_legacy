﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
Imports System.Drawing
#End Region

Partial Class Assessment_New_Performance_Goals_wPgEmpGlobalAssignGoals
    Inherits Basepage

#Region "Private Variables"
    Private DisplayMessage As New CommonCodes
    Private objEmpOwner As New clsassess_empowner_tran
    Private ReadOnly mstrModuleName As String = "frmGlobalAssignEmpGoals"
    Private strLinkedFld As String = ""
    Private dtFinalTab As DataTable = Nothing
#End Region

#Region "Page Event"
    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                Dim mintOwrFldTypeId As Integer = -1
                Me.ViewState.Add("mintSelectedPeriodId", 0)
                FillCombo()
                Dim mdtOwner As New DataTable
                Dim dtOwner As New DataTable
                mdtOwner = objEmpOwner.Get_Data(-1, mintOwrFldTypeId)

                Me.ViewState.Add("mdtOwner", mdtOwner)
                Me.ViewState.Add("dtOwner", dtOwner)
                Me.ViewState.Add("dtFinalTab", dtOwner)

                Me.ViewState.Add("mintExOrder", 0)
                Me.ViewState.Add("AdvanceFilter", "")
                Me.ViewState.Add("OwrFldTypeId", mintOwrFldTypeId)
              
            Else
                dtFinalTab = Me.ViewState("dtFinalTab")
                'If dtFinalTab IsNot Nothing Then
                '    gvItems.DataSource = dtFinalTab
                '    gvItems.DataBind()
                'End If
            End If
            If Me.ViewState("strLinkedFld") IsNot Nothing Then
                strLinkedFld = Me.ViewState("strLinkedFld")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.ViewState("strLinkedFld") Is Nothing Then
            Me.ViewState.Add("strLinkedFld", strLinkedFld)

        Else
            Me.ViewState("strLinkedFld") = strLinkedFld
        End If
        Me.ViewState("dtFinalTab") = dtFinalTab
    End Sub
#End Region

#Region "Private Methods"
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Shani [ 24 DEC 2014 ] -- END


    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Dim dsList As DataSet
        Try
            'S.SANDEEP [09 NOV 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, Session("Fin_year"), Session("Database_Name"), Session("fin_startdate"), "List", True, 1)

            'S.SANDEEP |02-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1, False, False, True)
            'S.SANDEEP |02-MAR-2021| -- END

            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [09 NOV 2015] -- END

            'Shani (09-May-2016) -- Start
            Dim intCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                Try
                .SelectedValue = intCurrentPeriodId
                Catch ex As Exception
                    .SelectedValue = 0
                End Try
            End With
            'Shani (09-May-2016) -- [.SelectedValue = intCurrentPeriodId]

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("List", True, True, , , , , , , , , , , , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , , Session("AccessLevelFilterString"))

            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                           Session("UserId"), _
                                           Session("Fin_year"), _
                                           Session("CompanyUnkId"), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                           Session("UserAccessModeSetting"), True, _
                                           Session("IsIncludeInactiveEmp"), "List", True)



            With cboOwner
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Set_Caption()
        Dim mintLinkedFld As Integer
        Dim mintExOrder As Integer
        Dim mintOwrFldTypeId As Integer
        Try
            Dim objFMapping As New clsAssess_Field_Mapping
            strLinkedFld = objFMapping.Get_Map_FieldName(cboPeriod.SelectedValue)
            mintLinkedFld = objFMapping.Get_Map_FieldId(cboPeriod.SelectedValue)
            Dim objFMaster As New clsAssess_Field_Master
            mintExOrder = objFMaster.Get_Field_ExOrder(mintLinkedFld)
            Select Case mintExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD1
                Case enWeight_Types.WEIGHT_FIELD2
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD2
                Case enWeight_Types.WEIGHT_FIELD3
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD3
                Case enWeight_Types.WEIGHT_FIELD4
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD4
                Case enWeight_Types.WEIGHT_FIELD5
                    mintOwrFldTypeId = enWeight_Types.WEIGHT_FIELD5
            End Select
            objFMaster = Nothing : objFMapping = Nothing
            objgbOwnerItems.Text = strLinkedFld & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Information")
            Me.ViewState("mintExOrder") = mintExOrder
            Me.ViewState("OwrFldTypeId") = mintOwrFldTypeId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data()
        Dim dtOwnerView As DataView
        Dim dtOwner As DataTable = CType(Me.ViewState("dtOwner"), DataTable)
        Dim mstrAdvanceFilter As String = CType(Me.ViewState("AdvanceFilter"), String)
        Try
            Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [27 Jan 2016] -- START
            'DUPLICATION OF EMPLOYEE {If Employee Reporting To & Assessor/Reviewer is Same Employee}
            'dList = objEmployee.GetEmployee_Access(CInt(cboOwner.SelectedValue), 0, CInt(cboPeriod.SelectedValue), mstrAdvanceFilter)

            dList = objEmployee.GetEmployee_Access(CInt(cboOwner.SelectedValue), 0, Session("Database_Name"), CInt(cboPeriod.SelectedValue), mstrAdvanceFilter)

            Dim dView As DataView = dList.Tables(0).DefaultView.ToTable(True, "ischeck", "employeecode", "employeename", "employeeunkid").DefaultView
            dList.Tables(0).Rows.Clear()
            dList.Tables.RemoveAt(0)
            dList.Tables.Add(dView.ToTable)
            'S.SANDEEP [27 Jan 2016] -- END

            dtOwner = dList.Tables(0)
            dtOwnerView = dtOwner.DefaultView
            Dim strSearch As String = ""
            If txtSearchEmp.Text.Trim.Length > 0 Then
                strSearch = "employeecode LIKE '%" & txtSearchEmp.Text & "%' OR " & _
                            "employeename LIKE '%" & txtSearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If
            dgvOwner.AutoGenerateColumns = False
            dgvOwner.DataSource = dtOwnerView
            dgvOwner.DataBind()
            Me.ViewState("dtOwner") = dtOwner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Items_Grid()
        Dim dtFinalTabView As DataView
        Try

            Dim objEmpFld1 As New clsassess_empfield1_master
            dtFinalTab = objEmpFld1.GetDisplayList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), "List")
            dtFinalTab.Columns.Add("fcheck", System.Type.GetType("System.Boolean")).DefaultValue = False
            'dgvItems.AutoGenerateColumns = False
            Select Case CInt(Me.ViewState("mintExOrder"))
                Case enWeight_Types.WEIGHT_FIELD1
                    dtFinalTab.Columns("Field1").ColumnName = "EmpField"
                Case enWeight_Types.WEIGHT_FIELD2
                    dtFinalTab.Columns("Field2").ColumnName = "EmpField"
                Case enWeight_Types.WEIGHT_FIELD3
                    dtFinalTab.Columns("Field3").ColumnName = "EmpField"
                Case enWeight_Types.WEIGHT_FIELD4
                    dtFinalTab.Columns("Field4").ColumnName = "EmpField"
                Case enWeight_Types.WEIGHT_FIELD5
                    dtFinalTab.Columns("Field5").ColumnName = "EmpField"
            End Select
            Dim xRow = From iRow In dtFinalTab.AsEnumerable() Where iRow.IsNull("fcheck") = True Select iRow
            For Each mRow As DataRow In xRow
                mRow.Item("fcheck") = False
            Next
            dtFinalTabView = dtFinalTab.DefaultView
            Dim strSearch As String = ""

            'SHANI (11 May 2015) -- Start
            'Enhancement - Payroll AT Log
            'If txtSearchItems.Text.Trim.Length > 0 Then
            '    strSearch = "EmpField LIKE '%" & txtSearchItems.Text & "%' OR " & _
            '                "Ed_Date LIKE '%" & txtSearchItems.Text & "%' OR " & _
            '                "CStatus LIKE '%" & txtSearchItems.Text & "%' OR " & _
            '                "St_Date LIKE '%" & txtSearchItems.Text & "%' OR " & _
            '                "Weight LIKE '%" & txtSearchItems.Text & "%'"
            '    dtFinalTabView.RowFilter = strSearch
            'End If

            gvItems.AutoGenerateColumns = False
            Dim iColName As String = String.Empty

            Dim iPlan() As String = Nothing
            If CStr(Session("ViewTitles_InPlanning")).Trim.Length > 0 Then
                iPlan = CStr(Session("ViewTitles_InPlanning")).Split("|")
            End If

            'If gvItems.Columns.Count > 0 Then gvItems.Columns.Clear()
            'Call AddColumn()

            For Each dCol As DataColumn In dtFinalTab.Columns
                If dCol.ColumnName = "fcheck" Then Continue For
                iColName = "" : iColName = "obj" & dCol.ColumnName
                Dim dgvCol As New BoundField()
                dgvCol.FooterText = iColName
                dgvCol.ReadOnly = True
                dgvCol.DataField = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                dgvCol.HtmlEncode = False
                If gvItems.Columns.Contains(dgvCol) = True Then Continue For

                If CInt(Session("CascadingTypeId")) = enPACascading.NEITHER_CASCADING_NOR_ALIGNMENT Then
                    If dgvCol.DataField.StartsWith("Owr") Then
                        dgvCol.Visible = False
                    End If

                End If

                If dCol.Caption.Length <= 0 Or dCol.ColumnName = "Emp" Or dCol.ColumnName = "OPeriod" Then
                    dgvCol.Visible = False
                Else
                    If dtFinalTab.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
                        Dim decColumnWidth As Decimal = 0
                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, dtFinalTab.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                            End If
                        End If
                    End If
                End If

                'S.SANDEEP [01 JUL 2015] -- START
                If dCol.ColumnName = "vuRemark" Or dCol.ColumnName = "vuProgress" Then
                    dgvCol.Visible = False
                End If
                'S.SANDEEP [01 JUL 2015] -- END

                gvItems.Columns.Add(dgvCol)

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                dtFinalTab.Columns(dCol.ColumnName).ExtendedProperties.Add("index", gvItems.Columns.IndexOf(dgvCol))
                'Pinkal (16-Apr-2016) -- End
            Next

            'SHANI (11 May 2015) -- End

            gvItems.DataSource = dtFinalTabView
            gvItems.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function IsValidOwner(ByVal dtFinalTab As DataTable, ByVal dtOwner As DataTable) As Boolean
        Try
            If CInt(cboOwner.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, Owner is mandatory information. Please select owner to continue."), Me)
                Return False
            End If

            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, Period is mandatory information. Please select Period to continue."), Me)
                Return False
            End If

            dtFinalTab.AcceptChanges() : dtOwner.AcceptChanges()
            Dim xRow() As DataRow = Nothing
            xRow = dtFinalTab.Select("fcheck = true")
            If xRow.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, please check atleast one of the") & " " & strLinkedFld & " " & _
                                Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "goal to assign owner."), Me)
                Return False
            End If

            xRow = dtOwner.Select("ischeck = true")
            If xRow.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, "Sorry, please check atleast one of the owners to assign them goals."), Me)
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

#End Region

#Region "Button Event"
    Protected Sub btnSeach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSeach.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, Please select Period and Owner to do global assign operation."), Me)
                Exit Sub
            End If
            cboOwner.Focus()
            Call Fill_Data()
            Call Fill_Items_Grid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim dtOwner As DataTable = CType(Me.ViewState("dtOwner"), DataTable)
        Dim mdtOwner As DataTable = CType(Me.ViewState("mdtOwner"), DataTable)
        Try
            If dtFinalTab IsNot Nothing AndAlso dtOwner IsNot Nothing Then
                Dim iFlag As Boolean = False
                If IsValidOwner(dtFinalTab, dtOwner) = False Then Exit Sub
                Dim dGoals(), dOwner() As DataRow
                dGoals = dtFinalTab.Select("fcheck = true")
                If dGoals.Length > 0 Then
                    dOwner = dtOwner.Select("ischeck = true")
                    If dOwner.Length > 0 Then
                        Dim iOwnerFldId As Integer = 0
                        For mGoalIdx As Integer = 0 To dGoals.Length - 1
                            For mEmpIdx As Integer = 0 To dOwner.Length - 1
                                Dim dRow As DataRow = mdtOwner.NewRow
                                dRow.Item("ownertranunkid") = -1
                                Select Case CInt(Me.ViewState("mintExOrder"))
                                    Case enWeight_Types.WEIGHT_FIELD1
                                        dRow.Item("empfieldunkid") = dGoals(mGoalIdx).Item("empfield1unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("empfield1unkid"))
                                    Case enWeight_Types.WEIGHT_FIELD2
                                        dRow.Item("empfieldunkid") = dGoals(mGoalIdx).Item("empfield2unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("empfield2unkid"))
                                    Case enWeight_Types.WEIGHT_FIELD3
                                        dRow.Item("empfieldunkid") = dGoals(mGoalIdx).Item("empfield3unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("empfield3unkid"))
                                    Case enWeight_Types.WEIGHT_FIELD4
                                        dRow.Item("empfieldunkid") = dGoals(mGoalIdx).Item("empfield4unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("empfield4unkid"))
                                    Case enWeight_Types.WEIGHT_FIELD5
                                        dRow.Item("empfieldunkid") = dGoals(mGoalIdx).Item("empfield5unkid")
                                        iOwnerFldId = CInt(dGoals(mGoalIdx).Item("empfield5unkid"))
                                End Select
                                dRow.Item("employeeunkid") = dOwner(mEmpIdx).Item("employeeunkid")
                                dRow.Item("empfieldtypeid") = Me.ViewState("OwrFldTypeId")
                                dRow.Item("AUD") = "A"
                                dRow.Item("GUID") = Guid.NewGuid.ToString
                                mdtOwner.Rows.Add(dRow)

                                If mdtOwner IsNot Nothing Then
                                    objEmpOwner = New clsassess_empowner_tran
                                    objEmpOwner._DatTable = mdtOwner.Copy
                                    If objEmpOwner.GlobalAssign_Owner(Session("UserId"), iOwnerFldId, Me.ViewState("OwrFldTypeId")) = False Then
                                        objEmpOwner = Nothing : iFlag = False
                                        Exit Sub
                                    Else
                                        iFlag = True
                                    End If
                                    objEmpOwner = Nothing
                                End If
                            Next
                        Next
                    End If
                    If iFlag = True Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Goals assigned successfully."), Me)
                        Dim xRowItem = From iRow In dtFinalTab.AsEnumerable() Where iRow.IsNull("fcheck") = True Select iRow
                        For Each mRow As DataRow In xRowItem
                            mRow.Item("fcheck") = False
                        Next

                        Dim xRowEmp = From iRow In dtOwner.AsEnumerable() Where iRow.IsNull("ischeck") = True Select iRow
                        For Each mRow As DataRow In xRowEmp
                            mRow.Item("ischeck") = False
                        Next

                        txtSearchEmp.Text = ""

                        'SHANI (11 May 2015) -- Start
                        '
                        'txtSearchItems.Text = ""
                        'SHANI (11 May 2015) -- End
                        Call Fill_Data()
                        Call Fill_Items_Grid()
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgEmployeeLvlGoalsList.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Control Event(S)"
    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged, cboOwner.SelectedIndexChanged

        Dim dtOwner As DataTable = CType(Me.ViewState("dtOwner"), DataTable)
        Dim mdtOwner As DataTable = CType(Me.ViewState("mdtOwner"), DataTable)
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Then
                gvItems.DataSource = Nothing : gvItems.DataBind()
                dgvOwner.DataSource = Nothing : dgvOwner.DataBind()
                If dtFinalTab IsNot Nothing Then dtFinalTab.Rows.Clear()
                If dtOwner IsNot Nothing Then dtOwner.Rows.Clear()
                If mdtOwner IsNot Nothing Then mdtOwner.Rows.Clear()
            ElseIf CInt(cboPeriod.SelectedValue) > 0 Then
                Call Set_Caption()
            End If
            Me.ViewState("dtOwner") = dtOwner
            Me.ViewState("mdtOwner") = mdtOwner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub imgbtnReset_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgbtnReset.Click
        Try
            Me.ViewState("AdvanceFilter") = ""
            Call Fill_Data()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master."
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Dim strAdvanceSearch As String
        Try
            strAdvanceSearch = popupAdvanceFilter._GetFilterString
            Me.ViewState("AdvanceFilter") = strAdvanceSearch
            Call Fill_Data()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkAllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtOwner As DataTable = CType(Me.ViewState("dtOwner"), DataTable)
        Try
            Dim chkAll As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvOwner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkSelect"), CheckBox)
                chk.Checked = chkAll.Checked
                dtOwner.Rows(iRow.ItemIndex)("ischeck") = chkAll.Checked
            Next
            dtOwner.AcceptChanges()
            Me.ViewState("dtOwner") = dtOwner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkItemAllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkAll As CheckBox = CType(sender, CheckBox)
            For Each iRow As GridViewRow In gvItems.Rows
                Dim chk As CheckBox = CType(iRow.FindControl("chkItemSelect"), CheckBox)
                chk.Checked = chkAll.Checked
                dtFinalTab.Rows(iRow.RowIndex)("fcheck") = chkAll.Checked
            Next
            dtFinalTab.AcceptChanges()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dtOwner As DataTable = CType(Me.ViewState("dtOwner"), DataTable)
        Try
            Dim chk As CheckBox = CType(sender, CheckBox)
            Dim xRow As DataGridItem = CType(chk.NamingContainer, DataGridItem)
            If xRow.ItemIndex >= 0 Then
                dtOwner.Rows(xRow.ItemIndex)("ischeck") = chk.Checked
            End If
            dtOwner.AcceptChanges()
            Me.ViewState("dtOwner") = dtOwner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkItemSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkItem As CheckBox = CType(sender, CheckBox)
            Dim xRow As GridViewRow = CType(chkItem.NamingContainer, GridViewRow)
            If xRow.RowIndex >= 0 Then
                dtFinalTab.Rows(xRow.RowIndex)("fcheck") = chkItem.Checked
            End If
            dtFinalTab.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtSearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchEmp.TextChanged
        Try
            Call Fill_Data()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI (11 May 2015) -- Start
    'Private Sub AddColumn()
    '    Try
    '        Dim colItemTemp As New TemplateField
    '        colItemTemp.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
    '        colItemTemp.ItemStyle.HorizontalAlign = HorizontalAlign.Center
    '        gvItems.Columns.Add(colItemTemp)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI (11 May 2015) -- End

    'SHANI (11 May 2015) -- Start
    'Enhancement - Payroll AT Log
    'Protected Sub txtSearchItems_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchItems.TextChanged
    '    Try
    '        Call Fill_Items_Grid()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI (11 May 2015) -- End
#End Region

#Region "GridView Event(S)"

    Protected Sub gvItems_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItems.RowDataBound
        'Dim chk As CheckBox
        Try

            'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim intIndex As Integer = 0
                intIndex = dtFinalTab.Columns("St_Date").ExtendedProperties("index")

                If e.Row.Cells(intIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(intIndex).Text.Trim <> "&nbsp;" Then
                    e.Row.Cells(intIndex).Text = CDate(e.Row.Cells(intIndex).Text).Date.ToShortDateString
                End If

                intIndex = dtFinalTab.Columns("Ed_Date").ExtendedProperties("index")
                If e.Row.Cells(intIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(intIndex).Text.Trim <> "&nbsp;" Then
                    e.Row.Cells(intIndex).Text = CDate(e.Row.Cells(intIndex).Text).Date.ToShortDateString
                End If
            End If
            'Pinkal (16-Apr-2016) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region
       
End Class
