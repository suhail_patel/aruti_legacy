﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
Imports System.Drawing
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Data.SqlClient
Imports System.Globalization
Imports System.IO

#End Region

Partial Class wPgESSUpdateGoalProgress
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmEmpFieldsList"
    Private ReadOnly mstrModuleName_upd_percent As String = "frmUpdateFieldValue"
    Private ReadOnly mstrModuleName_AppRejGoals As String = "frmAppRejGoalAccomplishmentList"
    'Private objEmpField1 As New clsassess_empfield1_master
    Private DisplayMessage As New CommonCodes
    'Private objEUpdateProgress As New clsassess_empupdate_tran
    Private mintUpdateProgressIndex As Integer = -1
    Private mintFieldUnkid As Integer = 0
    Private mintLinkedFld As Integer = 0
    Private mintFieldTypeId As Integer = 0
    Private mdtPeriodStartDate As Date = Nothing
    Private mstrDefinedGoal As String = ""
    Private mintTableFieldTranUnkId As Integer = 0
    Private mblnUpdateProgress As Boolean = False
    Private mintEmpUpdateTranunkid As Integer = 0
    Private mintAssessorMasterId As Integer = 0
    Private mintAssessorEmployeeId As Integer = 0
    Private mblnpopup_Accomplishment As Boolean = False
    Private mdtAccomplishmentData As DataTable = Nothing
    Private objCONN As SqlConnection
    Private mintEmpUnkId As Integer = -1
    Private mintPeriodUnkid As Integer = -1
    Private mdtAttachement As DataTable = Nothing
    Private mblnAttachmentPopup As Boolean = False
    Private mintDeleteIndex As Integer = 0
    Private mintGoalTypeId As Integer = 0
    Private mdblGoalValue As Double = 0
    Private mdtUnlockEmployeePrdEndDate As Date = Nothing
    Private mdtUnlockEmployee As DataTable = Nothing
    Private strPerspectiveColName As String = String.Empty
    Private mintPerspectiveColIndex As Integer = -1
    Private strGoalValueColName As String = String.Empty
    Private mintGoalValueColIndex As Integer = -1
    Private mstrGroupName As String = String.Empty
    Private mintUoMColIndex As Integer = -1
    Private mstrUoMColName As String = String.Empty
    Private mdblPercentage As Double = 0
    Private mdtFinal As DataTable
    Private mdtGoalAccomplishment As DataTable
    Private mdecTotalProgress As Decimal = 0

#End Region

#Region " Page Event "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''S.SANDEEP |05-MAR-2019| -- START
            ''ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            'If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0 AndAlso Request.QueryString.AllKeys(0) Is Nothing) AndAlso IsPostBack = False Then
            '    'Sohail (02 Apr 2019) -- Start
            '    'NMB Issue - 74.1 - Error "On Load Event !! Bad Data" on clicking any page with link after session get expired.
            '    If Request.QueryString.Count <= 0 Then Exit Sub
            '    'Sohail (02 Apr 2019) -- End

            '    'S.SANDEEP |17-MAR-2020| -- START
            '    'ISSUE/ENHANCEMENT : PM ERROR
            '    KillIdleSQLSessions()
            '    'S.SANDEEP |17-MAR-2020| -- END
            '    objCONN = Nothing
            '    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
            '        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
            '        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
            '        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
            '        objCONN = New SqlConnection
            '        objCONN.ConnectionString = constr
            '        objCONN.Open()
            '        HttpContext.Current.Session("gConn") = objCONN
            '    End If
            '    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")
            '    If arr.Length = 4 Then
            '        Try
            '            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
            '                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
            '                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
            '            Else
            '                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
            '                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
            '            End If

            '        Catch ex As Exception
            '            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
            '            HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
            '        End Try
            '        Blank_ModuleName()
            '        clsCommonATLog._WebFormName = "frmAppRejGoalAccomplishmentList"
            '        StrModuleName2 = "mnuAssessment"
            '        StrModuleName3 = "mnuSetups"
            '        clsCommonATLog._WebClientIP = Session("IP_ADD")
            '        clsCommonATLog._WebHostName = Session("HOST_NAME")

            '        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
            '        HttpContext.Current.Session("UserId") = CInt(arr(1))
            '        mintEmpUnkId = CInt(arr(2))
            '        mintPeriodUnkid = CInt(arr(3))

            '        Dim strError As String = ""
            '        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
            '            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
            '            Exit Sub
            '        End If

            '        HttpContext.Current.Session("mdbname") = Session("Database_Name")
            '        gobjConfigOptions = New clsConfigOptions
            '        gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
            '        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
            '        Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
            '        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

            '        ArtLic._Object = New ArutiLic(False)
            '        If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
            '            Dim objGroupMaster As New clsGroup_Master
            '            objGroupMaster._Groupunkid = 1
            '            ArtLic._Object.HotelName = objGroupMaster._Groupname
            '        End If

            '        If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
            '            DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '            Exit Sub
            '        End If

            '        If ConfigParameter._Object._IsArutiDemo Then
            '            If ConfigParameter._Object._IsExpire Then
            '                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
            '                Exit Try
            '            Else
            '                If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
            '                    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
            '                    Exit Try
            '                End If
            '            End If
            '        End If

            '        Dim clsConfig As New clsConfigOptions
            '        clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
            '        If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
            '            Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
            '        Else
            '            Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
            '        End If

            '        Session("DateFormat") = clsConfig._CompanyDateFormat
            '        Session("DateSeparator") = clsConfig._CompanyDateSeparator
            '        Session("GoalsAccomplishedRequiresApproval") = clsConfig._GoalsAccomplishedRequiresApproval
            '        Session("Ntf_FinalAcknowledgementUserIds") = clsConfig._Ntf_FinalAcknowledgementUserIds
            '        Session("Ntf_GoalsUnlockUserIds") = clsConfig._Ntf_GoalsUnlockUserIds
            '        SetDateFormat()
            '        Dim objUser As New clsUserAddEdit
            '        objUser._Userunkid = CInt(Session("UserId"))
            '        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
            '        Call GetDatabaseVersion()
            '        Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname"))
            '        HttpContext.Current.Session("clsuser") = clsuser
            '        HttpContext.Current.Session("UserName") = clsuser.UserName
            '        HttpContext.Current.Session("Firstname") = clsuser.Firstname
            '        HttpContext.Current.Session("Surname") = clsuser.Surname
            '        HttpContext.Current.Session("MemberName") = clsuser.MemberName
            '        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
            '        HttpContext.Current.Session("UserId") = clsuser.UserID
            '        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
            '        HttpContext.Current.Session("Password") = clsuser.password
            '        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
            '        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

            '        strError = ""
            '        If SetUserSessions(strError) = False Then
            '            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
            '            Exit Sub
            '        End If

            '        strError = ""
            '        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
            '            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
            '            Exit Sub
            '        End If
            '        Dim objUserPrivilege As New clsUserPrivilege
            '        objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))

            '        HttpContext.Current.Session("Login") = True

            '        Dim dsList As DataSet = objEmpField1.GetAccomplishemtn_DisplayList(mintEmpUnkId, mintPeriodUnkid, clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending, , "List")
            '        If dsList Is Nothing OrElse dsList.Tables(0).Rows.Count <= 0 Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 14, "Sorry, Goal detail is already updated with the selected link."), Me.Page, Session("rootpath") & "Index.aspx")
            '            Exit Sub
            '        End If
            '        mblnpopup_Accomplishment = True
            '    End If
            'End If
            ''S.SANDEEP |05-MAR-2019| -- END

            If IsPostBack = False Then
                Call FillCombo()
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) > 0 AndAlso CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) > 0 Then
                        'Call BtnSearch_Click(New Object, New EventArgs)

                        If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or _
                           CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                            'Pinkal (15-Mar-2019) -- Start
                            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 36, "Period is mandatory information. Please select Period to continue."), Me)  '--ADDED NEW LANAGUAGE.
                            Else
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Employee and Period are mandatory information. Please select Employee and Period to continue."), Me)
                            End If
                            Exit Sub
                        End If


                        'S.SANDEEP [10 DEC 2015] -- START
                        If Session("SkipApprovalFlowInPlanning") = True Then
                            Dim intU_EmpId As Integer = 0
                            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                                Dim objUserAddEdit As New clsUserAddEdit
                                objUserAddEdit._Userunkid = Session("UserId")
                                intU_EmpId = objUserAddEdit._EmployeeUnkid
                                objUserAddEdit = Nothing
                            End If

                            If intU_EmpId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) Then
                                DisplayMessage.DisplayMessage("You cannot access your own goals from MSS, Switch to ESS if you want to access your goals or Remove the Skip the Approval Setting", Me)
                                Exit Sub
                            End If
                        End If
                        'S.SANDEEP [10 DEC 2015] -- END


                        Call Fill_Grid()

                    End If
                End If

                'S.SANDEEP |05-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                'S.SANDEEP |18-JAN-2020| -- START
                'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                'If Request.QueryString.Count > 0 Then
                If Request.QueryString.Count > 0 AndAlso Request.QueryString.Keys(0) Is Nothing Then
                    'S.SANDEEP |18-JAN-2020| -- END
                    cboEmployee.SelectedValue = mintEmpUnkId
                    cboPeriod.SelectedValue = mintPeriodUnkid
                    'btnApproveReject_Click(New Object(), New EventArgs())
                    If CInt(cboEmployee.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                        If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 14, "Period is mandatory information. Please select Period to continue."), Me)  '--ADDED NEW LANAGUAGE.
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 1, "Employee and Period are mandatory information. Please select Employee and Period to continue."), Me)
                        End If
                        Exit Sub
                    End If

                    'Call Fill_Accomplishment_Grid()

                    'If mblnpopup_Accomplishment = True Then
                    '    popup_GoalAccomplishment.Show()
                    'End If
                End If
                'S.SANDEEP |05-MAR-2019| -- END
            Else
                mintUpdateProgressIndex = Me.ViewState("mintUpdateProgressIndex")
                mintFieldUnkid = Me.ViewState("mintFieldUnkid")
                mintLinkedFld = Me.ViewState("mintLinkedFld")
                mintFieldTypeId = Me.ViewState("mintFieldTypeId")
                mdtPeriodStartDate = Me.ViewState("mdtPeriodStartDate")
                mstrDefinedGoal = Me.ViewState("mstrDefinedGoal")
                mintTableFieldTranUnkId = Me.ViewState("mintTableFieldTranUnkId")
                mblnUpdateProgress = Me.ViewState("mblnUpdateProgress")
                mintEmpUpdateTranunkid = Me.ViewState("mintEmpUpdateTranunkid")
                mblnpopup_Accomplishment = Me.ViewState("mblnpopup_Accomplishment")
                mintGoalTypeId = Me.ViewState("mintGoalTypeId")
                mdblGoalValue = Me.ViewState("mdblGoalValue")
                mdblPercentage = Me.ViewState("mdblPercentage")
                'S.SANDEEP |18-JAN-2020| -- START
                'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                'mdtAccomplishmentData = Me.ViewState("mdtAccomplishmentData")
                'mdtFinal = Me.ViewState("mdtFinal")
                'mdtGoalAccomplishment = Me.ViewState("mdtGoalAccomplishment")
                'mdtAttachement = CType(Me.ViewState("FieldAttachement"), DataTable)
                'S.SANDEEP |18-JAN-2020| -- END


                'S.SANDEEP |18-JAN-2020| -- START
                'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                Call AddLinkCol()
                'If mdtFinal IsNot Nothing Then
                '    dgvData.DataSource = mdtFinal
                '    dgvData.DataBind()
                'Else
                '    dgvData.DataSource = Nothing : dgvData.DataBind()
                'End If
                'S.SANDEEP |18-JAN-2020| -- END

                If mblnUpdateProgress Then
                    popup_UpdateProgress.Show()
                End If

                mintAssessorMasterId = Me.ViewState("AssessorMasterId")
                mintAssessorEmployeeId = Me.ViewState("AssessorEmployeeId")
                mblnAttachmentPopup = Convert.ToBoolean(Me.ViewState("mblnCancelAttachmentPopup"))
                mintDeleteIndex = CInt(Me.ViewState("mintDeleteIndex"))
                mdtUnlockEmployeePrdEndDate = Me.ViewState("mdtUnlockEmployeePrdEndDate")
                mdtUnlockEmployee = Me.ViewState("mdtUnlockEmployee")
                strPerspectiveColName = Me.ViewState("strPerspectiveColName")
                mintPerspectiveColIndex = Me.ViewState("mintPerspectiveColIndex")
                strGoalValueColName = Me.ViewState("strGoalValueColName")
                mintGoalValueColIndex = Me.ViewState("mintGoalValueColIndex")
                mstrGroupName = Me.ViewState("mstrGroupName")
                mintUoMColIndex = Me.ViewState("mintUoMColIndex")
                mstrUoMColName = Me.ViewState("mstrUoMColName")

                mdecTotalProgress = Me.ViewState("mdecTotalProgress")
            End If

            If mblnAttachmentPopup Then
                popup_ScanAttchment.Show()
            End If
            If mblnpopup_Accomplishment Then
                'S.SANDEEP |25-MAR-2019| -- START
                'If Me.ViewState("mdtAccomplishmentData") IsNot Nothing Then
                '    mdtAccomplishmentData = Me.ViewState("mdtAccomplishmentData")
                '    dgv_Accomplishment_data.DataSource = mdtAccomplishmentData
                '    dgv_Accomplishment_data.DataBind()
                'End If
                'S.SANDEEP |25-MAR-2019| -- END
                'popup_GoalAccomplishment.Show()
            End If

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If

            'S.SANDEEP |05-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}            
            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                'btnApproveReject.Visible = False
                btnSubmit.Visible = True
            Else
                btnSubmit.Visible = False
                'btnApproveReject.Visible = True 'Implement Privilege
            End If
            'S.SANDEEP |05-MAR-2019| -- END

            Call SetLanguage()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            'S.SANDEEP |11-APR-2019| -- START
            If Request.QueryString.Count <= 0 Then
                Me.IsLoginRequired = True
            End If
            'Me.IsLoginRequired = True
            'S.SANDEEP |11-APR-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            'Me.ViewState("mdtFinal") = mdtFinal
            'Me.ViewState("mdtGoalAccomplishment") = mdtGoalAccomplishment
            'Me.ViewState("mdtAccomplishmentData") = mdtAccomplishmentData
            'Me.ViewState("FieldAttachement") = mdtAttachement
            'S.SANDEEP |18-JAN-2020| -- END
            Me.ViewState("mintUpdateProgressIndex") = mintUpdateProgressIndex
            Me.ViewState("mintFieldUnkid") = mintFieldUnkid
            Me.ViewState("mintLinkedFld") = mintLinkedFld
            Me.ViewState("mintFieldTypeId") = mintFieldTypeId
            Me.ViewState("mdtPeriodStartDate") = mdtPeriodStartDate
            Me.ViewState("mstrDefinedGoal") = mstrDefinedGoal
            Me.ViewState("mintTableFieldTranUnkId") = mintTableFieldTranUnkId
            Me.ViewState("mintEmpUpdateTranunkid") = mintEmpUpdateTranunkid
            Me.ViewState("mblnUpdateProgress") = mblnUpdateProgress

            Me.ViewState("mintGoalTypeId") = mintGoalTypeId
            Me.ViewState("mdblGoalValue") = mdblGoalValue
            Me.ViewState("mdblPercentage") = mdblPercentage

            Me.ViewState("AssessorMasterId") = mintAssessorMasterId
            Me.ViewState("AssessorEmployeeId") = mintAssessorEmployeeId
            Me.ViewState("mblnpopup_Accomplishment") = mblnpopup_Accomplishment

            Me.ViewState("mblnCancelAttachmentPopup") = mblnAttachmentPopup
            Me.ViewState("mintDeleteIndex") = mintDeleteIndex

            If Me.ViewState("strPerspectiveColName") IsNot Nothing Then
                If CStr(Me.ViewState("strPerspectiveColName")).Trim.Length <= 0 Then Me.ViewState("strPerspectiveColName") = strPerspectiveColName
            Else
                Me.ViewState("strPerspectiveColName") = strPerspectiveColName
            End If
            If Me.ViewState("mintPerspectiveColIndex") IsNot Nothing Then
                If CInt(Me.ViewState("mintPerspectiveColIndex")) <= 0 Then Me.ViewState("mintPerspectiveColIndex") = mintPerspectiveColIndex
            Else
                Me.ViewState("mintPerspectiveColIndex") = mintPerspectiveColIndex
            End If
            If Me.ViewState("strGoalValueColName") IsNot Nothing Then
                If CStr(Me.ViewState("strGoalValueColName")).Trim.Length <= 0 Then Me.ViewState("strGoalValueColName") = strGoalValueColName
            Else
                Me.ViewState("strGoalValueColName") = strGoalValueColName
            End If
            If Me.ViewState("mintGoalValueColIndex") IsNot Nothing Then
                If CInt(Me.ViewState("mintGoalValueColIndex")) < 0 Then Me.ViewState("mintGoalValueColIndex") = mintGoalValueColIndex
            Else
                Me.ViewState("mintGoalValueColIndex") = mintGoalValueColIndex
            End If

            If Me.ViewState("mstrUoMColName") IsNot Nothing Then
                If Me.ViewState("mstrUoMColName").ToString().Trim.Length <= 0 Then Me.ViewState("mstrUoMColName") = mstrUoMColName
            Else
                Me.ViewState("mstrUoMColName") = mstrUoMColName
            End If
            If Me.ViewState("mstrGroupName") IsNot Nothing Then
                If Me.ViewState("mstrGroupName").ToString().Trim.Length <= 0 Then Me.ViewState("mstrGroupName") = mstrGroupName
            Else
                Me.ViewState("mstrGroupName") = mstrGroupName
            End If
            If Me.ViewState("mintUoMColIndex") IsNot Nothing Then
                If CInt(Me.ViewState("mintUoMColIndex")) < 0 Then Me.ViewState("mintUoMColIndex") = mintUoMColIndex
            Else
                Me.ViewState("mintUoMColIndex") = mintUoMColIndex
            End If

            Me.ViewState("mdecTotalProgress") = mdecTotalProgress

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clsEmployee_Master
        Dim objEUpdateProgress As New clsassess_empupdate_tran
        Try

            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            Dim strFilterQry As String = String.Empty
            Dim blnApplyFilter As Boolean = False

            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            Else
                Dim csvIds As String = String.Empty
                Dim dsMapEmp As New DataSet
                Dim objEval As New clsevaluation_analysis_master
                dsMapEmp = objEval.getAssessorComboList(Session("Database_Name"), _
                                                        Session("UserId"), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                        True, Session("IsIncludeInactiveEmp"), "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)

                If dsMapEmp.Tables("List").Rows.Count > 0 Then
                    mintAssessorMasterId = CInt(dsMapEmp.Tables("List").Rows(0)("Id"))
                    mintAssessorEmployeeId = CInt(dsMapEmp.Tables("List").Rows(0)("EmpId"))
                    dsList = objEval.getEmployeeBasedAssessor(Session("Database_Name"), _
                                                              Session("UserId"), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), True, _
                                                              Session("IsIncludeInactiveEmp"), _
                                                              CInt(dsMapEmp.Tables("List").Rows(0)("Id")), "AEmp", True)

                    strFilterQry = " hremployee_master.employeeunkid IN "
                    csvIds = String.Join(",", dsList.Tables("AEmp").AsEnumerable().Select(Function(x) x.Field(Of Integer)("Id").ToString()).ToArray())
                    If csvIds.Trim.Length > 0 Then
                        strFilterQry &= "(" & csvIds & ")"
                    Else
                        strFilterQry &= "(0)"
                    End If
                Else
                    strFilterQry = " hremployee_master.employeeunkid IN (0) "
                End If
                objEval = Nothing
            End If

            dsList = objEmp.GetEmployeeList(Session("Database_Name"), _
                                            Session("UserId"), _
                                            Session("Fin_year"), _
                                            Session("CompanyUnkId"), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                            Session("UserAccessModeSetting"), True, _
                                            Session("IsIncludeInactiveEmp"), "List", blnSelect, intEmpId, , , , , , , , , , , , , , strFilterQry, , blnApplyFilter)

            With cboEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsList.Tables(0)
                .DataBind()
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    .SelectedValue = 0
                ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    .SelectedValue = Session("Employeeunkid")
                End If
            End With


            'S.SANDEEP |02-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1, False, False, True)
            'S.SANDEEP |02-MAR-2021| -- END

            Dim mintCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                Try
                .SelectedValue = mintCurrentPeriodId
                Catch ex As Exception
                    .SelectedValue = 0
                End Try
            End With

            Call cboEmployee_SelectedIndexChanged(cboEmployee, Nothing)

            dsList = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
            End With

            'Dim item As System.Web.UI.WebControls.ListItem
            'With cboChangeBy
            '    .Items.Clear()
            '    item = New System.Web.UI.WebControls.ListItem
            '    item.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 100, "Select")
            '    item.Value = 0
            '    item.Attributes.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 100, "Select"), 0)
            '    .Items.Add(item)

            '    item = New System.Web.UI.WebControls.ListItem
            '    item.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 101, "Percentage")
            '    item.Value = 1
            '    item.Attributes.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 101, "Percentage"), 1)
            '    .Items.Add(item)

            '    item = New System.Web.UI.WebControls.ListItem
            '    item.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 102, "Value")
            '    item.Value = 2
            '    item.Attributes.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 102, "Value"), 2)
            '    .Items.Add(item)

            '    .DataBind()
            '    .SelectedIndex = 0
            'End With

            With cboChangeBy
                .Items.Clear()
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 100, "Select"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 101, "Percentage"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 102, "Value"))
                .DataBind()
                .SelectedIndex = 0
            End With

            'dsList = (New clsMasterData).Get_Goal_Accomplishement_Status("List", True)
            'With cboAccomplishmentStatus
            '    .DataValueField = "Id"
            '    .DataTextField = "Name"
            '    .DataSource = dsList.Tables("List")
            '    .DataBind()
            '    .SelectedValue = 0
            'End With

            ''S.SANDEEP |29-JUL-2019| -- START
            ''ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
            'dsList = (New clsassess_perspective_master).getComboList("List", True)
            'With cboPerspective
            '    .DataValueField = "Id"
            '    .DataTextField = "Name"
            '    .DataSource = dsList.Tables("List")
            '    .DataBind()
            '    .SelectedValue = 0
            'End With
            ''S.SANDEEP |29-JUL-2019| -- END

            'S.SANDEEP |24-APR-2020| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
            dsList = objEUpdateProgress.GetCalculationMode("List", False)
            With cboCalculationType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = clsassess_empupdate_tran.enCalcMode.Increasing
            End With
            'S.SANDEEP |24-APR-2020| -- END

            Call UpdateFillCombo()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objMData = Nothing
            dsList.Dispose()
            objPeriod = Nothing
            objEmp = Nothing
            objEUpdateProgress = Nothing
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dtFinal As DataTable
        Dim iSearch As String = String.Empty
        Dim objFMaster = New clsAssess_Field_Master(True)
        Dim intGridWidth As Integer = 0
        Dim objEmpField1 As New clsassess_empfield1_master
        Try

            If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "Sorry, No data defined for asseessment caption settings screen."), Me)
                Exit Sub
            End If

            Dim objMap As New clsAssess_Field_Mapping
            If objMap.isExist(CInt(cboPeriod.SelectedValue)) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry no field is mapped with the selected period."), Me)
                Exit Sub
            End If
            objMap = Nothing

            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                If CBool(Session("AllowtoViewEmployeeGoalsList")) = False Then Exit Sub
            End If

            dtFinal = objEmpField1.GetDisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", Session("CascadingTypeId"))

            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            If dtFinal.Columns.Contains("Field1") = False Then
                dtFinal.Columns.Add("Field1", GetType(System.String)).DefaultValue = ""
            End If
            If dtFinal.Columns.Contains("Field2") = False Then
                dtFinal.Columns.Add("Field2", GetType(System.String)).DefaultValue = ""
            End If
            If dtFinal.Columns.Contains("Field3") = False Then
                dtFinal.Columns.Add("Field3", GetType(System.String)).DefaultValue = ""
            End If
            If dtFinal.Columns.Contains("Field4") = False Then
                dtFinal.Columns.Add("Field4", GetType(System.String)).DefaultValue = ""
            End If
            If dtFinal.Columns.Contains("Field5") = False Then
                dtFinal.Columns.Add("Field5", GetType(System.String)).DefaultValue = ""
            End If
            'S.SANDEEP |18-JAN-2020| -- END

            mdtFinal = New DataView(dtFinal, "", "perspectiveunkid ASC,empfield1unkid ASC, Field1 ASC", DataViewRowState.CurrentRows).ToTable

            If mdtFinal.Rows.Count <= 0 Then
                Dim iRow As DataRow = mdtFinal.NewRow
                mdtFinal.Rows.Add(iRow)
            End If

            dgvData.AutoGenerateColumns = False
            Dim iColName As String = String.Empty

            Dim iPlan() As String = Nothing
            If CStr(Session("ViewTitles_InPlanning")).Trim.Length > 0 Then
                iPlan = CStr(Session("ViewTitles_InPlanning")).Split("|")
            End If

            Dim intTotalWidth As Integer = 0
            If iPlan IsNot Nothing Then
                For index As Integer = 0 To iPlan.Length - 1
                    intTotalWidth += objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, iPlan(index), Session("CompnayUnkId"))
                Next
            End If

            If dgvData.Columns.Count > 0 Then dgvData.Columns.Clear()

            intGridWidth = 75
            strPerspectiveColName = mdtFinal.Columns.Cast(Of DataColumn)().Where(Function(x) CInt(x.ExtendedProperties(x.ColumnName)) = CInt(clsAssess_Field_Master.enOtherInfoField.PERSPECTIVE)).Select(Function(x) x.ColumnName).First()
            strGoalValueColName = mdtFinal.Columns.Cast(Of DataColumn)().Where(Function(x) CInt(x.ExtendedProperties(x.ColumnName)) = CInt(clsAssess_Field_Master.enOtherInfoField.GOAL_VALUE)).Select(Function(x) x.ColumnName).First()
            mstrUoMColName = mdtFinal.Columns.Cast(Of DataColumn)().Where(Function(x) x.ColumnName = "UoMType").Select(Function(x) x.ColumnName).First()

            mstrGroupName = "" : objlblCurrentStatus.Text = ""
            If mdtFinal.Rows.Count > 0 Then
                'S.SANDEEP |07-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#0004244}
                'mstrGroupName = mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of String)("STypeName") <> "").Select(Function(x) x.Field(Of String)("STypeName")).FirstOrDefault()
                mstrGroupName = mdtFinal.AsEnumerable().Where(Function(x) x.Field(Of String)("STypeName") <> "").Select(Function(x) x.Field(Of String)("STypeName")).DefaultIfEmpty().First()
                If mstrGroupName Is Nothing Then mstrGroupName = ""
                'S.SANDEEP |07-NOV-2019| -- END

                mdtFinal.Columns.Remove("STypeName")
                If mstrGroupName Is Nothing Then mstrGroupName = ""
            End If
            If mstrGroupName.Trim.Length > 0 Then objlblCurrentStatus.Text = "Current Status : " & mstrGroupName

            For Each dCol As DataColumn In mdtFinal.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                Dim dgvCol As New BoundField()

                dgvCol.FooterText = iColName
                dgvCol.ReadOnly = True
                dgvCol.DataField = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                dgvCol.HtmlEncode = False
                If dgvData.Columns.Contains(dgvCol) = True Then Continue For

                'S.SANDEEP |18-JAN-2020| -- START
                'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                If dgvCol.DataField.StartsWith("Field") Then
                    If mdtFinal.Columns(dCol.ColumnName).ExtendedProperties.Count <= 0 Then dgvCol.Visible = False
                End If
                'S.SANDEEP |18-JAN-2020| -- END

                If dgvCol.DataField.StartsWith("Owr") Then
                    dgvCol.Visible = False
                End If



                'S.SANDEEP |21-AUG-2019| -- START
                'ISSUE/ENHANCEMENT : DECIMAL PLACES ISSUES
                'If dCol.Caption.Length <= 0 Or dCol.ColumnName = "Emp" Or dCol.ColumnName = "OPeriod" _
                '        Or dCol.ColumnName = "vuGoalAccomplishmentStatus" Then
                If dCol.Caption.Length <= 0 Or dCol.ColumnName = "Emp" Or dCol.ColumnName = "OPeriod" Then
                    'S.SANDEEP |21-AUG-2019| -- END
                    dgvCol.Visible = False
                Else
                    If mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then
                        Dim decColumnWidth As Decimal = 0
                        'S.SANDEEP |15-MAR-2019| -- START
                        'If intTotalWidth > 0 Then
                        '    decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName)) * 95 / intTotalWidth
                        '    dgvCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
                        '    dgvCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
                        'Else
                        '    decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName))
                        '    dgvCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
                        '    dgvCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
                        'End If
                        decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), Session("CompanyUnkid"))
                        dgvCol.HeaderStyle.Width = Unit.Pixel(decColumnWidth)
                        dgvCol.ItemStyle.Width = Unit.Pixel(decColumnWidth)
                        'S.SANDEEP |15-MAR-2019| -- END

                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                            End If
                        End If
                    End If
                End If

                If dCol.ColumnName.ToUpper = "VUPROGRESS" Then
                    dgvCol.Visible = False
                    Call Add_UpdateProgressFiled(dCol)
                End If
                dgvData.Columns.Add(dgvCol)
                mdtFinal.Columns(dCol.ColumnName).ExtendedProperties.Add("index", dgvData.Columns.IndexOf(dgvCol))
                If dCol.ColumnName = "OPeriod" Then
                    dgvCol.Visible = False
                End If
                If dgvCol.Visible = True Then
                    If strPerspectiveColName = dCol.ColumnName Then
                        mintPerspectiveColIndex = dgvData.Columns.IndexOf(dgvCol)
                    End If
                End If
                If dgvCol.Visible = True Then
                    If strGoalValueColName = dCol.ColumnName Then
                        mintGoalValueColIndex = dgvData.Columns.IndexOf(dgvCol)
                    End If
                End If
                If mstrUoMColName = dCol.ColumnName Then
                    mintUoMColIndex = dgvData.Columns.IndexOf(dgvCol)
                End If
            Next

            If Me.ViewState("mintUpdateProgressIndex") IsNot Nothing Then
                'S.SANDEEP |05-MAR-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                'If (Session("LoginBy") = Global.User.en_loginby.User) Then
                '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = CBool(Session("AllowToViewPercentCompletedEmployeeGoals"))
                '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = CBool(Session("AllowToViewPercentCompletedEmployeeGoals"))
                'Else
                '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
                '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
                'End If
                If CBool(mdtFinal.Rows(0)("isfinal")) = False Then
                    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
                    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
                Else
                    If (Session("LoginBy") = Global.User.en_loginby.User) Then
                        dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
                        dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
                    Else
                        dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = True
                        dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = True
                    End If
                End If
                'S.SANDEEP |05-MAR-2019| -- END
            End If
            Dim blnIsStyleAdded As Boolean = False
            For Each dgvRow As DataRow In mdtFinal.Rows
                If CBool(dgvRow("isfinal")) = True Then
                    'dgvData.Style.Add("color", "blue")
                    If blnIsStyleAdded = False Then
                        If Session("BSC_StatusColors").Keys.Count > 0 Then
                            If Session("BSC_StatusColors").ContainsKey(CInt(enObjective_Status.FINAL_SAVE)) Then
                                dgvData.Style.Add("color", ColorTranslator.ToHtml(Color.FromArgb(CInt(Session("BSC_StatusColors")(CInt(enObjective_Status.FINAL_SAVE))))))
                                objlblCurrentStatus.ForeColor = Color.FromArgb(CInt(Session("BSC_StatusColors")(CInt(enObjective_Status.FINAL_SAVE))))
                            Else
                    dgvData.Style.Add("color", "blue")
                                objlblCurrentStatus.ForeColor = Color.Blue
                            End If
                        Else
                    dgvData.Style.Add("color", "blue")
                            objlblCurrentStatus.ForeColor = Color.Blue
                        End If
                        blnIsStyleAdded = True
                    End If
                    If Me.ViewState("mintUpdateProgressIndex") IsNot Nothing Then
                        'S.SANDEEP |05-MAR-2019| -- START
                        'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                        'If (Session("LoginBy") = Global.User.en_loginby.User) Then
                        '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = CBool(Session("AllowToViewPercentCompletedEmployeeGoals"))
                        '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = CBool(Session("AllowToViewPercentCompletedEmployeeGoals"))
                        'Else
                        '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
                        '    dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
                        'End If
                        If CBool(mdtFinal.Rows(0)("isfinal")) = False Then
                            dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
                            dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
                        Else
                            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                                dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
                                dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
                            Else
                                dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = True
                                dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = True
                            End If
                        End If
                        'S.SANDEEP |05-MAR-2019| -- END
                    End If
                ElseIf CInt(dgvRow("opstatusid")) = enObjective_Status.SUBMIT_APPROVAL Then
                    'dgvData.Style.Add("color", "Green")
                    If Session("BSC_StatusColors").Keys.Count > 0 Then
                        If Session("BSC_StatusColors").ContainsKey(CInt(enObjective_Status.SUBMIT_APPROVAL)) Then
                            dgvData.Style.Add("color", ColorTranslator.ToHtml(Color.FromArgb(CInt(Session("BSC_StatusColors")(CInt(enObjective_Status.SUBMIT_APPROVAL))))))
                            objlblCurrentStatus.ForeColor = Color.FromArgb(CInt(Session("BSC_StatusColors")(CInt(enObjective_Status.SUBMIT_APPROVAL))))
                        Else
                            dgvData.Style.Add("color", "Green")
                            objlblCurrentStatus.ForeColor = Color.Green
                        End If
                    Else
                    dgvData.Style.Add("color", "Green")
                        objlblCurrentStatus.ForeColor = Color.Green
                    End If
                    Exit For
                Else
                    'dgvData.Style.Add("color", "black")
                    Dim strHex As String = ""
                    Select Case CInt(dgvRow("opstatusid"))
                        Case enObjective_Status.OPEN_CHANGES
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.OPEN_CHANGES), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.OPEN_CHANGES))), ColorTranslator.ToHtml(Color.Black))
                        Case enObjective_Status.NOT_SUBMIT
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.NOT_SUBMIT), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.NOT_SUBMIT))), ColorTranslator.ToHtml(Color.Red))
                        Case enObjective_Status.NOT_COMMITTED
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.NOT_COMMITTED), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.NOT_COMMITTED))), ColorTranslator.ToHtml(Color.Purple))
                        Case enObjective_Status.FINAL_COMMITTED
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.FINAL_COMMITTED), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.FINAL_COMMITTED))), ColorTranslator.ToHtml(Color.Blue))
                        Case enObjective_Status.PERIODIC_REVIEW
                            strHex = IIf(Session("BSC_StatusColors").ContainsKey(enObjective_Status.PERIODIC_REVIEW), ColorTranslator.ToHtml(Color.FromArgb(Session("BSC_StatusColors")(enObjective_Status.PERIODIC_REVIEW))), ColorTranslator.ToHtml(Color.Brown))
                    End Select
                    dgvData.Style.Add("color", strHex)
                    objlblCurrentStatus.ForeColor = ColorTranslator.FromHtml(strHex)
                    Exit For
                End If
            Next
            'S.SANDEEP |15-MAR-2019| -- START
            dgvData.Width = Unit.Percentage(99.5)
            'dgvData.Width = Unit.Pixel(intTotalWidth)
            'S.SANDEEP |15-MAR-2019| -- END
            dgvData.DataSource = mdtFinal
            dgvData.DataBind()

            Dim objFMapping As New clsAssess_Field_Mapping
            Dim xTotalWeight As Double = 0
            xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_EMPLOYEE_LEVEL, CInt(cboPeriod.SelectedValue), 0, CInt(cboEmployee.SelectedValue))
            If xTotalWeight > 0 Then
                objlblTotalWeight.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Total Weight Assigned :") & " " & xTotalWeight.ToString
            Else
                objlblTotalWeight.Text = ""
            End If
            objFMapping = Nothing

            'S.SANDEEP |05-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            If CBool(mdtFinal.Rows(0)("isfinal")) = False Then
                'pnlOperation.Visible = False
                btnSubmit.Visible = False
            ElseIf CBool(mdtFinal.Rows(0)("isfinal")) = True Then
                'pnlOperation.Visible = True
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    'btnApproveReject.Visible = False
                    btnSubmit.Visible = True
                Else
                    btnSubmit.Visible = False
                    'btnApproveReject.Visible = True 'Implement Privilege
                End If
            End If
            'If pnlOperation.Visible = True Then
            
            'End If
            'S.SANDEEP |05-MAR-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmpField1 = Nothing
        End Try
    End Sub

    Private Sub Add_UpdateProgressFiled(ByVal dcol As DataColumn)
        Try
            Dim iTempField As New TemplateField()
            iTempField.FooterText = dcol.ColumnName
            iTempField.HeaderText = dcol.Caption
            iTempField.HeaderStyle.Width = Unit.Pixel(70)
            iTempField.ItemStyle.Width = Unit.Pixel(70)
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            dgvData.Columns.Add(iTempField)
            mintUpdateProgressIndex = dgvData.Columns.IndexOf(iTempField)
            'S.SANDEEP [01 JUL 2015] -- START
            Me.ViewState("mintUpdateProgressIndex") = mintUpdateProgressIndex
            'S.SANDEEP [01 JUL 2015] -- END

            'S.SANDEEP [16 JUN 2015] -- START
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                'S.SANDEEP |12-FEB-2019| -- START
                'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                'dgvData.Columns(mintUpdateProgressIndex).Visible = CBool(Session("AllowtoUpdatePercentCompletedEmployeeGoals"))
                dgvData.Columns(mintUpdateProgressIndex).Visible = CBool(Session("AllowToViewPercentCompletedEmployeeGoals"))
                btnUpdateSave.Visible = CBool(Session("AllowtoUpdatePercentCompletedEmployeeGoals"))
                'S.SANDEEP |12-FEB-2019| -- END
            End If
            'S.SANDEEP [16 JUN 2015] -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            If mintEmpUpdateTranunkid > 0 Then
                Dim objScanAttachment As New clsScan_Attach_Documents
                'S.SANDEEP |04-SEP-2021| -- START
                'ISSUE : TAKING CARE FROM SLOWNESS QUERY
                'objScanAttachment.GetList(Session("Document_Path"), "List", "", CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "scanattachrefid = '" & enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & CInt(mintEmpUpdateTranunkid) & "' AND form_name = '" & mstrModuleName_upd_percent & "'")
                objScanAttachment.GetList(Session("Document_Path"), _
                                          "List", "", _
                                          CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, _
                                          "scanattachrefid = '" & enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & CInt(mintEmpUpdateTranunkid) & "' AND form_name = '" & mstrModuleName_upd_percent & "'", _
                                          CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
                'S.SANDEEP |04-SEP-2021| -- END
                Dim xRow As DataRow() = objScanAttachment._Datatable.Select()
                If xRow IsNot Nothing AndAlso xRow.Count > 0 Then
                    mdtAttachement = xRow.CopyToDataTable
                Else
                    mdtAttachement = objScanAttachment._Datatable.Clone
                End If
                Dim dtRow() As DataRow = mdtAttachement.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
                If dtRow.Length <= 0 Then
                    dRow = mdtAttachement.NewRow
                    dRow("scanattachtranunkid") = -1
                    dRow("employeeunkid") = CInt(cboEmployee.SelectedValue)
                    dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
                    dRow("modulerefid") = enImg_Email_RefId.Appraisal_Module
                    dRow("scanattachrefid") = enScanAttactRefId.ASSESSMENT
                    dRow("transactionunkid") = mintEmpUpdateTranunkid
                    dRow("filepath") = ""
                    dRow("filename") = f.Name
                    dRow("filesize") = f.Length / 1024
                    dRow("attached_date") = Today.Date
                    dRow("orgfilepath") = strfullpath
                    dRow("GUID") = Guid.NewGuid
                    dRow("AUD") = "A"
                    dRow("form_name") = mstrModuleName_upd_percent
                    dRow("userunkid") = CInt(Session("userid"))
                    Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                    dRow("file_data") = xDocumentData
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 6, "Selected information is already present for particular employee."), Me)
                    Exit Sub
                End If
                mdtAttachement.Rows.Add(dRow)
            End If

            Dim objDocument As New clsScan_Attach_Documents
            Dim mdsDoc As DataSet
            Dim mstrFolderName As String = ""
            mdsDoc = objDocument.GetDocFolderName("Docs")
            Dim strFileName As String = ""
            For Each dtRow As DataRow In mdtAttachement.Rows
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dtRow("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault
                If dtRow("AUD").ToString = "A" AndAlso dtRow("orgfilepath").ToString <> "" Then
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & System.IO.Path.GetExtension(CStr(dtRow("orgfilepath")))
                    If System.IO.File.Exists(CStr(dtRow("orgfilepath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                        If System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If
                        System.IO.File.Move(CStr(dtRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dtRow("fileuniquename") = strFileName
                        dtRow("filepath") = strPath
                        dtRow.AcceptChanges()
                    Else
                        DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                        Exit Sub
                    End If
                ElseIf dtRow("AUD").ToString = "U" AndAlso dtRow("fileuniquename").ToString <> "" Then
                    strFileName = dtRow("fileuniquename").ToString()
                    Dim strServerPath As String = dtRow("filepath").ToString()
                    Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                    If strServerPath.Contains(strPath) Then
                        strServerPath = strServerPath.Replace(strPath, "")
                        If Strings.Left(strServerPath, 1) <> "/" Then
                            strServerPath = "~/" & strServerPath
                        Else
                            strServerPath = "~" & strServerPath

                        End If
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                        If System.IO.File.Exists(Server.MapPath(strServerPath)) Then
                            If System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                            End If
                            System.IO.File.Move(Server.MapPath(strServerPath), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                            dtRow("filepath") = strPath
                            dtRow.AcceptChanges()
                        Else
                            DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                            Exit Sub
                        End If
                    Else
                        DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                        Exit Sub
                    End If
                ElseIf dtRow("AUD").ToString = "D" AndAlso dtRow("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dtRow("filepath").ToString()
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If System.IO.File.Exists(Server.MapPath(strFilepath)) Then
                            System.IO.File.Delete(Server.MapPath(strFilepath))
                        Else
                            DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                            Exit Sub
                        End If
                    Else
                        DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                        Exit Sub
                    End If
                End If
            Next
            objDocument._Datatable = mdtAttachement
            objDocument.InsertUpdateDelete_Documents()
            If objDocument._Message <> "" Then
                DisplayMessage.DisplayMessage(objDocument._Message, Me)
                Exit Sub
            End If
            'Call FillAttachment()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            Dim objScanAttachment As New clsScan_Attach_Documents
            'S.SANDEEP |04-SEP-2021| -- START
            'ISSUE : TAKING CARE FROM SLOWNESS QUERY
            'objScanAttachment.GetList(Session("Document_Path"), "List", "", CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "scanattachrefid = '" & _
            '                                           enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & _
            '                                           CInt(mintEmpUpdateTranunkid) & "' AND form_name = '" & _
            '                                           mstrModuleName_upd_percent & "'")

            objScanAttachment.GetList(Session("Document_Path"), "List", _
                                      "", CInt(cboEmployee.SelectedValue), _
                                      False, Nothing, Nothing, _
                                      "scanattachrefid = '" & enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & _
                                      CInt(mintEmpUpdateTranunkid) & "' AND form_name = '" & mstrModuleName_upd_percent & "'", _
                                              CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
            'S.SANDEEP |04-SEP-2021| -- END

            Dim xrow() As DataRow = objScanAttachment._Datatable.Select()
            If xrow IsNot Nothing AndAlso xrow.Count > 0 Then
                mdtAttachement = xrow.CopyToDataTable
            Else
                mdtAttachement = objScanAttachment._Datatable.Clone
            End If
            dtView = New DataView(mdtAttachement, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgv_Attchment.AutoGenerateColumns = False
            dgv_Attchment.DataSource = dtView
            dgv_Attchment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub MergeRows(ByVal gridView As GridView)
        Try
        If gridView.Rows.Count > 1 AndAlso mintPerspectiveColIndex > 0 Then
            For rowIndex As Integer = gridView.Rows.Count - 2 To 0 Step -1
                Dim row As GridViewRow = gridView.Rows(rowIndex)
                Dim previousRow As GridViewRow = gridView.Rows(rowIndex + 1)
                If row.Cells(mintPerspectiveColIndex).Text = previousRow.Cells(mintPerspectiveColIndex).Text Then
                    row.Cells(mintPerspectiveColIndex).RowSpan = If(previousRow.Cells(mintPerspectiveColIndex).RowSpan < 2, 2, previousRow.Cells(mintPerspectiveColIndex).RowSpan + 1)
                    previousRow.Cells(mintPerspectiveColIndex).Visible = False
                End If
            Next
        End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub SetTagValue(ByVal dtTab As DataTable, ByVal blnIsEdit As Boolean)
        Try
            If dtTab.Rows.Count > 0 Then
                Dim iMaxTranId As Integer = 0

                If IsDBNull(dtTab.Compute("SUM(finalvalue)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved))) = False Then
                    mdecTotalProgress = dtTab.Compute("SUM(finalvalue)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved))
                End If

                If IsDBNull(dtTab.Compute("MAX(empupdatetranunkid)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved))) = False Then
                    iMaxTranId = Convert.ToInt32(dtTab.Compute("MAX(empupdatetranunkid)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)))
                End If
                If blnIsEdit Then
                    If IsDBNull(dtTab.Compute("SUM(finalvalue)", "approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved))) = False Then
                        If IsDBNull(dtTab.Compute("SUM(finalvalue)", "empupdatetranunkid <> '" & iMaxTranId & "' AND approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved))) = False Then
                            mdecTotalProgress = dtTab.Compute("SUM(finalvalue)", "empupdatetranunkid <> '" & iMaxTranId & "' AND approvalstatusunkid =" & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved))
                        End If
                    End If
                    Dim iSecondLastTranId As Integer = 0
                    Dim iList As List(Of Integer) = dtTab.AsEnumerable().Select(Function(x) x.Field(Of Integer)("empupdatetranunkid")).ToList()
                    If iList.Count > 0 Then
                        Dim dr() As DataRow = Nothing
                        If iList.Min() = iMaxTranId Then
                            dr = dtTab.Select("empupdatetranunkid = " & iMaxTranId)
                            If dr.Length > 0 Then
                                cboChangeBy.SelectedIndex = CInt(dr(0)("changebyid"))
                                cboChangeBy.Enabled = False
                                cboChangeBy_SelectedIndexChanged(cboChangeBy, New EventArgs())

                                'S.SANDEEP |24-APR-2020| -- START
                                'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
                                cboCalculationType.SelectedValue = CInt(dr(0)("calcmodeid"))
                                cboCalculationType.Enabled = False
                                'S.SANDEEP |24-APR-2020| -- END

                                'S.SANDEEP |13-JUL-2019| -- START
                                'ISSUE/ENHANCEMENT : PA CHANGES
                                'Select Case cboChangeBy.SelectedIndex
                                '    Case 1
                                '        txtNewPercentage.Attributes("lstval") = 0
                                '    Case 2
                                '        txtNewValue.Attributes("lstval") = 0
                                'End Select
                                Select Case cboChangeBy.SelectedIndex
                                    Case 1
                                        txtNewPercentage.Attributes("lstval") = CDec(dr(0)("pct_complete"))
                                    Case 2
                                        txtNewValue.Attributes("lstval") = CDec(dr(0)("finalvalue"))
                                End Select
                                'S.SANDEEP |13-JUL-2019| -- END
                            End If
                        Else
                            Try
                                iSecondLastTranId = iList.Item(iList.IndexOf(iMaxTranId) + 1)
                            Catch ex As Exception
                                iSecondLastTranId = iMaxTranId
                            End Try
                            dr = dtTab.Select("empupdatetranunkid = " & iSecondLastTranId)
                            If dr.Length > 0 Then
                                cboChangeBy.SelectedIndex = CInt(dr(0)("changebyid"))
                                cboChangeBy.Enabled = False
                                cboChangeBy_SelectedIndexChanged(cboChangeBy, New EventArgs())

                                'S.SANDEEP |24-APR-2020| -- START
                                'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
                                cboCalculationType.SelectedValue = CInt(dr(0)("calcmodeid"))
                                cboCalculationType.Enabled = False
                                'S.SANDEEP |24-APR-2020| -- END

                                'S.SANDEEP |22-MAR-2019| -- START
                                'ISSUE/ENHANCEMENT : ISSUE ON PA (TC0017)
                                'Select Case cboChangeBy.SelectedIndex
                                '    Case 1
                                '        txtNewPercentage.Attributes("lstval") = CDec(dr(0)("pct_complete"))
                                '    Case 2
                                '        txtNewValue.Attributes("lstval") = CDec(dr(0)("finalvalue"))
                                'End Select
                                If iMaxTranId > 0 Then
                                    Select Case cboChangeBy.SelectedIndex
                                        Case 1
                                            txtNewPercentage.Attributes("lstval") = CDec(dr(0)("pct_complete"))
                                        Case 2
                                            txtNewValue.Attributes("lstval") = CDec(dr(0)("finalvalue"))
                                    End Select
                                Else
                                    Select Case cboChangeBy.SelectedIndex
                                        Case 1
                                            txtNewPercentage.Attributes("lstval") = 0
                                        Case 2
                                            txtNewValue.Attributes("lstval") = CDec(dr(0)("finalvalue"))
                                    End Select
                                End If
                                'S.SANDEEP |22-MAR-2019| -- END
                            End If
                        End If
                    Else
                        txtNewPercentage.Attributes("lstval") = 0
                        txtNewValue.Attributes("lstval") = 0
                    End If
                Else
                    If iMaxTranId > 0 Then
                        Dim dr() As DataRow = dtTab.Select("empupdatetranunkid = " & iMaxTranId)
                        If dr.Length > 0 Then
                            cboChangeBy.SelectedIndex = CInt(dr(0)("changebyid"))
                            cboChangeBy.Enabled = False
                            cboChangeBy_SelectedIndexChanged(cboChangeBy, New EventArgs())
                            Select Case cboChangeBy.SelectedIndex
                                Case 1
                                    txtNewPercentage.Attributes("lstval") = CDec(dr(0)("pct_complete"))
                                Case 2
                                    txtNewValue.Attributes("lstval") = CDec(dr(0)("finalvalue"))
                            End Select

                            'S.SANDEEP |24-APR-2020| -- START
                            'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
                            cboCalculationType.SelectedValue = CInt(dr(0)("calcmodeid"))
                            cboCalculationType.Enabled = False
                            'S.SANDEEP |24-APR-2020| -- END

                        Else
                            txtNewPercentage.Attributes("lstval") = 0
                            txtNewValue.Attributes("lstval") = 0
                        End If
                    Else
                        txtNewPercentage.Attributes("lstval") = 0
                        txtNewValue.Attributes("lstval") = 0
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Private Sub Fill_Accomplishment_Grid()
    '    Dim iSearch As String = String.Empty
    '    Dim objFMaster As New clsAssess_Field_Master
    '    Dim intGridWidth As Integer = 0
    '    Dim strFiledName As String = "" : Dim dsFinal As DataSet
    '    Dim objEmpField1 As New clsassess_empfield1_master
    '    Try
    '        'mdtAccomplishmentData = Nothing
    '        If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 4, "Sorry, No data defined for asseessment caption settings screen."), Me)
    '            Exit Sub
    '        End If
    '        If (New clsAssess_Field_Mapping).isExist(CInt(cboPeriod.SelectedValue)) = False Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 5, "Sorry no field is mapped with the selected period."), Me)
    '            Exit Sub
    '        End If
    '        strFiledName = (New clsAssess_Field_Mapping).Get_Map_FieldName(cboPeriod.SelectedValue)
    '        'S.SANDEEP |24-JUL-2019| -- START
    '        'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        'dsFinal = objEmpField1.GetAccomplishemtn_DisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAccomplishmentStatus.SelectedValue), , "List")
    '        'If dsFinal Is Nothing Then Exit Sub
    '        'mdtAccomplishmentData = New DataView(dsFinal.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
    '        'dgv_Accomplishment_data.AutoGenerateColumns = False
    '        ''S.SANDEEP |25-MAR-2019| -- START
    '        ''dgv_Accomplishment_data.Columns(2).HeaderText = strFiledName
    '        'dgv_Accomplishment_data.Columns(3).HeaderText = strFiledName
    '        ''S.SANDEEP |25-MAR-2019| -- END
    '        'dgv_Accomplishment_data.Width = Unit.Percentage(99.5)
    '        'dgv_Accomplishment_data.DataSource = mdtAccomplishmentData
    '        'dgv_Accomplishment_data.DataBind()

    '        'S.SANDEEP |29-JUL-2019| -- START
    '        'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        'dsFinal = objEmpField1.GetNewAccomplishemtn_DisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAccomplishmentStatus.SelectedValue), Session("CascadingTypeId"), , "List", Session("fmtCurrency"))
    '        dsFinal = objEmpField1.GetNewAccomplishemtn_DisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboAccomplishmentStatus.SelectedValue), Session("CascadingTypeId"), , "List", Session("fmtCurrency"), CInt(IIf(cboPerspective.SelectedValue = "", 0, cboPerspective.SelectedValue)), CInt(IIf(cboLinkedField.SelectedValue = "", 0, cboLinkedField.SelectedValue)))
    '        'S.SANDEEP |29-JUL-2019| -- END

    '        'If dsFinal Is Nothing Then Exit Sub
    '        'If dsFinal.Tables(0).Columns.Contains("IsGrp") = False Then
    '        '    Dim dCol As New DataColumn
    '        '    With dCol
    '        '        .DataType = GetType(System.Boolean)
    '        '        .DefaultValue = False
    '        '        .ColumnName = "IsGrp"
    '        '    End With
    '        '    dsFinal.Tables(0).Columns.Add(dCol)
    '        'End If
    '        mdtAccomplishmentData = New DataView(dsFinal.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
    '        'S.SANDEEP |29-JUL-2019| -- START
    '        'ISSUE/ENHANCEMENT : There is no Row at position 0
    '        'dgv_AccomplishmentData.AutoGenerateColumns = False
    '        'dgv_AccomplishmentData.Columns(1).HeaderText = mdtAccomplishmentData.Rows(0)("pCaption")
    '        'dgv_AccomplishmentData.Columns(2).HeaderText = strFiledName
    '        'If mdtAccomplishmentData.Rows(0)("cFCaption").ToString.Trim.Length > 0 Then
    '        '    dgv_AccomplishmentData.Columns(3).HeaderText = mdtAccomplishmentData.Rows(0)("cFCaption")
    '        '    dgv_AccomplishmentData.Columns(3).Visible = True
    '        'Else
    '        '    dgv_AccomplishmentData.Columns(3).Visible = False
    '        'End If
    '        'If mdtAccomplishmentData.Rows(0)("dFCaption").ToString.Trim.Length > 0 Then
    '        '    dgv_AccomplishmentData.Columns(4).HeaderText = mdtAccomplishmentData.Rows(0)("dFCaption")
    '        '    dgv_AccomplishmentData.Columns(4).Visible = True
    '        'Else
    '        '    dgv_AccomplishmentData.Columns(4).Visible = False
    '        'End If
    '        'If mdtAccomplishmentData.Rows(0)("eFCaption").ToString.Trim.Length > 0 Then
    '        '    dgv_AccomplishmentData.Columns(5).HeaderText = mdtAccomplishmentData.Rows(0)("eFCaption")
    '        '    dgv_AccomplishmentData.Columns(5).Visible = True
    '        'Else
    '        '    dgv_AccomplishmentData.Columns(5).Visible = False
    '        'End If
    '        If mdtAccomplishmentData.Rows.Count > 0 Then
    '            dgv_AccomplishmentData.AutoGenerateColumns = False

    '            dgv_AccomplishmentData.Columns(2).HeaderText = strFiledName
    '            dgv_AccomplishmentData.Columns(1).HeaderText = mdtAccomplishmentData.Rows(0)("pCaption")
    '            If mdtAccomplishmentData.Rows(0)("cFCaption").ToString.Trim.Length > 0 Then
    '                dgv_AccomplishmentData.Columns(3).HeaderText = mdtAccomplishmentData.Rows(0)("cFCaption")
    '                dgv_AccomplishmentData.Columns(3).Visible = True
    '            Else
    '                dgv_AccomplishmentData.Columns(3).Visible = False
    '            End If
    '            If mdtAccomplishmentData.Rows(0)("dFCaption").ToString.Trim.Length > 0 Then
    '                dgv_AccomplishmentData.Columns(4).HeaderText = mdtAccomplishmentData.Rows(0)("dFCaption")
    '                dgv_AccomplishmentData.Columns(4).Visible = True
    '            Else
    '                dgv_AccomplishmentData.Columns(4).Visible = False
    '            End If
    '            If mdtAccomplishmentData.Rows(0)("eFCaption").ToString.Trim.Length > 0 Then
    '                dgv_AccomplishmentData.Columns(5).HeaderText = mdtAccomplishmentData.Rows(0)("eFCaption")
    '                dgv_AccomplishmentData.Columns(5).Visible = True
    '            Else
    '                dgv_AccomplishmentData.Columns(5).Visible = False
    '            End If
    '        End If
    '        'S.SANDEEP |29-JUL-2019| -- END


    '        'dgv_AccomplishmentData.Width = Unit.Percentage(99.5)
    '        dgv_AccomplishmentData.DataSource = mdtAccomplishmentData
    '        dgv_AccomplishmentData.DataBind()
    '        'S.SANDEEP |24-JUL-2019| -- END
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objFMaster = Nothing
    '        objEmpField1 = Nothing
    '    End Try
    'End Sub

    Private Function Get_Assessor(ByRef iAssessorEmpId As Integer, ByRef sAssessorName As String) As Integer
        Dim iAssessor As Integer = 0
        Try
            Dim dLst As New DataSet
            Dim objBSC As New clsevaluation_analysis_master
            dLst = objBSC.getAssessorComboList(CStr(Session("Database_Name")), _
                                               CInt(Session("Userid")), _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                               True, True, "List", clsAssessor.enARVisibilityTypeId.VISIBLE, False, False)
            If dLst.Tables(0).Rows.Count > 0 Then
                iAssessor = CInt(dLst.Tables(0).Rows(0).Item("Id"))
                iAssessorEmpId = objBSC.GetAssessorEmpId(iAssessor)
                sAssessorName = CStr(dLst.Tables(0).Rows(0).Item("Name"))
            End If
            objBSC = Nothing
            Return iAssessor
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    'S.SANDEEP |29-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    'Private Sub FillLinkedField(ByVal intPerspectiveId As Integer)
    '    Try
    '        lblLinkedField.Text = (New clsAssess_Field_Mapping).Get_Map_FieldName(cboPeriod.SelectedValue)
    '        Dim intLinkedId As Integer = 0
    '        intLinkedId = (New clsAssess_Field_Mapping).Get_Map_FieldId(cboPeriod.SelectedValue)
    '        Dim dsList As New DataSet
    '        cboLinkedField.DataSource = Nothing
    '        Select Case intLinkedId
    '            Case enWeight_Types.WEIGHT_FIELD1
    '                dsList = (New clsassess_empfield1_master).getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), "List", True, True, CInt(IIf(cboPerspective.SelectedValue = "", 0, cboPerspective.SelectedValue)))
    '            Case enWeight_Types.WEIGHT_FIELD2
    '                dsList = (New clsassess_empfield2_master).getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, "List", True)
    '            Case enWeight_Types.WEIGHT_FIELD3
    '                dsList = (New clsassess_empfield3_master).getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, "List", True)
    '            Case enWeight_Types.WEIGHT_FIELD4
    '                dsList = (New clsassess_empfield4_master).getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, "List", True)
    '            Case enWeight_Types.WEIGHT_FIELD5
    '                dsList = (New clsassess_empfield5_master).getComboList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, "List", True)
    '        End Select
    '        Dim dtTable As DataTable = Nothing
    '        dtTable = dsList.Tables(0).DefaultView.ToTable("List", True, "Id", "Name")
    '        With cboLinkedField
    '            .DataValueField = "Id"
    '            .DataTextField = "Name"
    '            .DataSource = dtTable
    '            .DataBind()
    '            .SelectedValue = 0
    '        End With

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'S.SANDEEP |29-JUL-2019| -- END

    'S.SANDEEP |18-JAN-2020| -- START
    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    Private Sub AddLinkCol()
        Try
            For Each gRow As GridViewRow In dgvData.Rows
                If gRow.Cells(mintUpdateProgressIndex).FindControl("lnkUpdateProgress") Is Nothing Then
                    If CBool(dgvData.DataKeys(gRow.RowIndex)("isfinal")) = True Then
                        If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                            Dim lnk As New LinkButton
                            lnk.ID = "lnkUpdateProgress"
                            lnk.CommandName = "UpdateProgress"
                            'S.SANDEEP |20-JAN-2022| -- START
                            'lnk.Text = ""
                            'lnk.CssClass = "updatedata123"
                            'lnk.Style.Add("color", "red")
                            'lnk.Style.Add("font-size", "20px")

                            lnk.Text = "≡"
                            'lnk.CssClass = "updatedata123"
                            lnk.Style.Add("color", "red")
                            lnk.Style.Add("font-size", "40px")
                            'S.SANDEEP |20-JAN-2022| -- END
                            gRow.Cells(mintUpdateProgressIndex).Controls.Add(lnk)
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP |18-JAN-2020| -- END

#End Region

#Region " Button Event "

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or _
                   CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                'Pinkal (15-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 36, "Period is mandatory information. Please select Period to continue."), Me)  '--ADDED NEW LANAGUAGE.
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Employee and Period are mandatory information. Please select Employee and Period to continue."), Me)
                End If
                Exit Sub
            End If


            'S.SANDEEP [10 DEC 2015] -- START
            If Session("SkipApprovalFlowInPlanning") = True Then
                Dim intU_EmpId As Integer = 0
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    Dim objUserAddEdit As New clsUserAddEdit
                    objUserAddEdit._Userunkid = Session("UserId")
                    intU_EmpId = objUserAddEdit._EmployeeUnkid
                    objUserAddEdit = Nothing
                End If

                If intU_EmpId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) Then
                    DisplayMessage.DisplayMessage("You cannot access your own goals from MSS, Switch to ESS if you want to access your goals or Remove the Skip the Approval Setting", Me)
                    Exit Sub
                End If
            End If
            'S.SANDEEP [10 DEC 2015] -- END


            Call Fill_Grid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            Me.ViewState("mdtFinal") = Nothing
            dgvData.DataSource = Nothing
            dgvData.DataBind()
            Call cboEmployee_SelectedIndexChanged(cboEmployee, Nothing)
            objlblTotalWeight.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUpdateSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateSave.Click
        Try
            If UpdateIsValidInfo() = False Then Exit Sub

            If txtUpdateRemark.Text.Trim.Length <= 0 Then
                lblUpdateMessages.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmUpdateFieldValue", 6, "Sorry, you have not entered the remark. Would you like to save the changes without remark?")
                hdf_UpdateYesNo.Value = "TXTREMARK"
                popup_UpdateYesNO.Show()
                Exit Sub
            End If
            Call Save_UpdateProgress()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUpdateClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateClose.Click
        Try
            popup_UpdateProgress.Hide()
            mblnUpdateProgress = False
            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            'Call Fill_Grid()
            If Session("GoalsAccomplishedRequiresApproval") = False Then
                Call Fill_Grid()
            End If
            'S.SANDEEP |18-JAN-2020| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnupdateYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdateYes.Click
        Try
            If hdf_UpdateYesNo.Value.ToUpper = "TXTREMARK" Then
                Call Save_UpdateProgress()
            ElseIf hdf_UpdateYesNo.Value.ToUpper = "DELETE" Then
                popup_UpdateProgressDeleteReason.Title = "Delete Reason:"
                popup_UpdateProgressDeleteReason.Show()
            End If
            hdf_UpdateYesNo.Value = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnupdateNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdateNo.Click
        Try
            If hdf_UpdateYesNo.Value.ToUpper = "TXTREMARK" Then
            ElseIf hdf_UpdateYesNo.Value.ToUpper = "DELETE" Then
                mintEmpUpdateTranunkid = 0
            End If
            hdf_UpdateYesNo.Value = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_UpdateProgressDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UpdateProgressDeleteReason.buttonDelReasonYes_Click
        Dim objEUpdateProgress As New clsassess_empupdate_tran
        Try
            objEUpdateProgress._Isvoid = True
            objEUpdateProgress._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objEUpdateProgress._Voidreason = popup_UpdateProgressDeleteReason.Reason
            objEUpdateProgress._Voiduserunkid = CInt(Session("UserId"))

            If objEUpdateProgress.Delete(mintEmpUpdateTranunkid) = True Then
                Call Fill_History()
                popup_UpdateProgressDeleteReason.Reason = ""
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEUpdateProgress = Nothing
        End Try
    End Sub

    Protected Sub popup_UpdateProgressDeleteReason_buttonDelReasonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UpdateProgressDeleteReason.buttonDelReasonNo_Click
        Try
            mintEmpUpdateTranunkid = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)
                'mdtAttachement = CType(Me.ViewState("FieldAttachement"), DataTable)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    popup_ScanAttchment.Show()
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                AddDocumentAttachment(f, f.FullName)
                Call FillAttachment()
                popup_ScanAttchment.Show()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_AttachementYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonNo_Click
        Try
            If mintDeleteIndex > 0 Then mintDeleteIndex = 0
            popup_ScanAttchment.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_AttachementYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonYes_Click
        Try
            If mintDeleteIndex >= 0 Then
                Dim objScanAttachment As New clsScan_Attach_Documents
                'S.SANDEEP |04-SEP-2021| -- START
                'ISSUE : TAKING CARE FROM SLOWNESS QUERY
                'objScanAttachment.GetList(Session("Document_Path"), "List", "", _
                '                          CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "scanattachrefid = '" & _
                '                          enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & _
                '                          CInt(mintEmpUpdateTranunkid) & "' AND form_name = '" & _
                '                          mstrModuleName_upd_percent & "' AND scanattachtranunkid = " & _
                '                          mintDeleteIndex)
                objScanAttachment.GetList(Session("Document_Path"), "List", "", _
                                          CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "scanattachrefid = '" & _
                                          enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & _
                                          CInt(mintEmpUpdateTranunkid) & "' AND form_name = '" & _
                                          mstrModuleName_upd_percent & "' AND scanattachtranunkid = " & _
                                          mintDeleteIndex, _
                                          CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
                'S.SANDEEP |04-SEP-2021| -- END
                Dim xrow() As DataRow = objScanAttachment._Datatable.Select()
                If xrow IsNot Nothing AndAlso xrow.Count > 0 Then
                    mdtAttachement = xrow.CopyToDataTable
                Else
                    mdtAttachement = objScanAttachment._Datatable.Clone
                End If
                mdtAttachement.Rows(0)("AUD") = "D"
                mdtAttachement.AcceptChanges()
                mintDeleteIndex = 0
                Dim strFilepath As String = mdtAttachement.Rows(0)("filepath").ToString()
                Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                If strFilepath.Contains(strArutiSelfService) Then
                    strFilepath = strFilepath.Replace(strArutiSelfService, "")
                    If Strings.Left(strFilepath, 1) <> "/" Then
                        strFilepath = "~/" & strFilepath
                    Else
                        strFilepath = "~" & strFilepath
                    End If

                    If System.IO.File.Exists(Server.MapPath(strFilepath)) Then
                        System.IO.File.Delete(Server.MapPath(strFilepath))
                    Else
                        DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                        Exit Sub
                    End If
                Else
                    DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                    Exit Sub
                End If
                objScanAttachment._Datatable = mdtAttachement
                objScanAttachment.InsertUpdateDelete_Documents()
                If objScanAttachment._Message <> "" Then
                    DisplayMessage.DisplayMessage(objScanAttachment._Message, Me)
                    Exit Sub
                End If
                Call FillAttachment()
                popup_ScanAttchment.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnScanClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanClose.Click
        Try
            mblnAttachmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnScanSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanSave.Click
        Try
            'Dim objDocument As New clsScan_Attach_Documents
            'Dim mdsDoc As DataSet
            'Dim mstrFolderName As String = ""
            'mdsDoc = objDocument.GetDocFolderName("Docs")
            'Dim strFileName As String = ""
            'For Each dRow As DataRow In mdtAttachement.Rows
            '    mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRow("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault
            '    If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then

            '        'Pinkal (20-Nov-2018) -- Start
            '        'Enhancement - Working on P2P Integration for NMB.
            '        'strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & System.IO.Path.GetExtension(CStr(dRow("orgfilepath")))
            '        strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & System.IO.Path.GetExtension(CStr(dRow("orgfilepath")))
            '        'Pinkal (20-Nov-2018) -- End
            '        If System.IO.File.Exists(CStr(dRow("orgfilepath"))) Then
            '            Dim strPath As String = Session("ArutiSelfServiceURL").ToString
            '            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
            '                strPath += "/"
            '            End If
            '            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

            '            If System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
            '                System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
            '            End If

            '            System.IO.File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
            '            dRow("fileuniquename") = strFileName
            '            dRow("filepath") = strPath
            '            dRow.AcceptChanges()
            '        Else
            '            'Sohail (23 Mar 2019) -- Start
            '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            '            'DisplayMessage.DisplayError(ex,Me)
            '            DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
            '            'Sohail (23 Mar 2019) -- End
            '            Exit Sub
            '        End If
            '    ElseIf dRow("AUD").ToString = "U" AndAlso dRow("fileuniquename").ToString <> "" Then
            '        strFileName = dRow("fileuniquename").ToString()
            '        Dim strServerPath As String = dRow("filepath").ToString()
            '        Dim strPath As String = Session("ArutiSelfServiceURL").ToString

            '        If strServerPath.Contains(strPath) Then
            '            strServerPath = strServerPath.Replace(strPath, "")
            '            If Strings.Left(strServerPath, 1) <> "/" Then
            '                strServerPath = "~/" & strServerPath
            '            Else
            '                strServerPath = "~" & strServerPath

            '            End If

            '            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
            '                strPath += "/"
            '            End If
            '            strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

            '            If System.IO.File.Exists(Server.MapPath(strServerPath)) Then

            '                If System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
            '                    System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
            '                End If

            '                System.IO.File.Move(Server.MapPath(strServerPath), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))

            '                dRow("filepath") = strPath
            '                dRow.AcceptChanges()
            '            Else
            '                'Sohail (23 Mar 2019) -- Start
            '                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            '                'DisplayMessage.DisplayError(ex,Me)
            '                DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
            '                'Sohail (23 Mar 2019) -- End
            '                Exit Sub
            '            End If
            '        Else
            '            'Sohail (23 Mar 2019) -- Start
            '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            '            'DisplayMessage.DisplayError(ex,Me)
            '            DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
            '            'Sohail (23 Mar 2019) -- End
            '            Exit Sub
            '        End If

            '    ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
            '        Dim strFilepath As String = dRow("filepath").ToString()
            '        Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
            '        If strFilepath.Contains(strArutiSelfService) Then
            '            strFilepath = strFilepath.Replace(strArutiSelfService, "")
            '            If Strings.Left(strFilepath, 1) <> "/" Then
            '                strFilepath = "~/" & strFilepath
            '            Else
            '                strFilepath = "~" & strFilepath
            '            End If

            '            If System.IO.File.Exists(Server.MapPath(strFilepath)) Then
            '                System.IO.File.Delete(Server.MapPath(strFilepath))
            '            Else
            '                'Sohail (23 Mar 2019) -- Start
            '                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            '                'DisplayMessage.DisplayError(ex,Me)
            '                DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
            '                'Sohail (23 Mar 2019) -- End
            '                Exit Sub
            '            End If
            '        Else
            '            'Sohail (23 Mar 2019) -- Start
            '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            '            'DisplayMessage.DisplayError(ex,Me)
            '            DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
            '            'Sohail (23 Mar 2019) -- End
            '            Exit Sub
            '        End If

            '    End If
            'Next

            'objDocument._Datatable = mdtAttachement
            'objDocument.InsertUpdateDelete_Documents()

            'If objDocument._Message <> "" Then
            '    DisplayMessage.DisplayMessage(objDocument._Message, Me)
            '    Exit Sub
            'End If

            'mblnAttachmentPopup = False
            'popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 15, "Sorry, Employee and Period are mandatory information to performe Add Operation."), Me)
                Exit Sub
            End If
            Dim dsFinal As DataSet = (New clsassess_empfield1_master).GetAccomplishemtn_DisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, , "List")
            If dsFinal Is Nothing OrElse dsFinal.Tables(0).Select("Accomplished_statusId = " & clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending & " AND IsGrp = FALSE").Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "Sorry, Pending data not found in system."), Me)
                Exit Sub
            End If
            Dim objEUpdateProgress As New clsassess_empupdate_tran
            Dim enLogin As enLogin_Mode = enLogin_Mode.MGR_SELF_SERVICE
            Dim intloginEmpid As Integer = 0
            Dim intloginUserid As Integer = 0
            If Session("LoginBy") = Global.User.en_loginby.Employee Then
                enLogin = enLogin_Mode.EMP_SELF_SERVICE
                intloginEmpid = Session("Employeeunkid")
            ElseIf Session("LoginBy") = Global.User.en_loginby.User Then
                enLogin = enLogin_Mode.MGR_SELF_SERVICE
                intloginUserid = Session("UserId")
            End If
            objEUpdateProgress.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), _
                                                          CInt(cboPeriod.SelectedValue), _
                                                          Session("Fin_year"), _
                                                          Session("FinancialYear_Name"), _
                                                          Session("Database_Name"), objEUpdateProgress._Empupdatetranunkid, _
                                                          CInt(Session("CompanyUnkId")), _
                                                          Session("ArutiSelfServiceURL"), _
                                                          enLogin, intloginEmpid, intloginUserid)
            'S.SANDEEP |14-MAR-2019| -- START

            'S.SANDEEP |25-MAR-2019| -- START
            'DisplayMessage.DisplayMessage("Update of goals progress has been submitted successfully for approval.", Me)
            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 36, "Progress Update have been submitted for approval."), Me)
            'S.SANDEEP |25-MAR-2019| -- END

            btnSubmit.Enabled = False
            'S.SANDEEP |14-MAR-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub btnApproveReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproveReject.Click
    '    Try
    '        If CInt(cboEmployee.SelectedValue) <= 0 Or _
    '               CInt(cboPeriod.SelectedValue) <= 0 Then
    '            'Pinkal (15-Mar-2019) -- Start
    '            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    '            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 14, "Period is mandatory information. Please select Period to continue."), Me)  '--ADDED NEW LANAGUAGE.
    '            Else
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 1, "Employee and Period are mandatory information. Please select Employee and Period to continue."), Me)
    '            End If
    '            Exit Sub
    '        End If
    '        'Dim dsList As DataSet = (New clsMasterData).Get_Goal_Accomplishement_Status("List", True)
    '        'With cboAccomplishmentStatus
    '        '    .DataValueField = "Id"
    '        '    .DataTextField = "Name"
    '        '    .DataSource = dsList.Tables("List")
    '        '    .DataBind()
    '        '    .SelectedValue = 0
    '        'End With

    '        'S.SANDEEP |29-JUL-2019| -- START
    '        'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        Call FillLinkedField(CInt(IIf(cboPerspective.SelectedValue = "", 0, cboPerspective.SelectedValue)))
    '        cboPerspective.SelectedValue = 0
    '        cboLinkedField.SelectedValue = 0
    '        'S.SANDEEP |29-JUL-2019| -- END

    '        'S.SANDEEP |07-SEP-2019| -- START
    '        'ISSUE/ENHANCEMENT : Default Status
    '        cboAccomplishmentStatus.SelectedValue = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending)
    '        'S.SANDEEP |07-SEP-2019| -- END

    '        Call Fill_Accomplishment_Grid()
    '        mblnpopup_Accomplishment = True
    '        popup_GoalAccomplishment.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAccomplishmentClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishmentClose.Click
    '    Try
    '        mblnpopup_Accomplishment = False
    '        popup_GoalAccomplishment.Hide()
    '        If Request.QueryString.Count > 0 Then
    '            Response.Redirect("~/Index.aspx", False)
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAccomplishmentReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishmentReset.Click
    '    Try
    '        'S.SANDEEP |29-JUL-2019| -- START
    '        'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        cboPerspective.SelectedValue = 0
    '        cboLinkedField.SelectedValue = 0
    '        'S.SANDEEP |29-JUL-2019| -- END

    '        'S.SANDEEP |07-SEP-2019| -- START
    '        'ISSUE/ENHANCEMENT : Default Status
    '        'cboAccomplishmentStatus.SelectedValue = 0
    '        cboAccomplishmentStatus.SelectedValue = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending)
    '        'S.SANDEEP |07-SEP-2019| -- END

    '        Call Fill_Accomplishment_Grid()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAccomplishmentSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishmentSearch.Click
    '    Try
    '        Call Fill_Accomplishment_Grid()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAccomplishment_yes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishment_yes.Click
    '    Dim strMessage As String = ""
    '    Dim objEUpdateProgress As New clsassess_empupdate_tran
    '    Try
    '        If hdfAccomplishment.Value.Trim.Length <= 0 Then Exit Sub


    '        Dim iAssessorMasterId, iAssessorEmpId As Integer
    '        Dim sAssessorName As String = ""
    '        iAssessorMasterId = 0 : iAssessorEmpId = 0
    '        iAssessorMasterId = Get_Assessor(iAssessorEmpId, sAssessorName)

    '        If iAssessorMasterId <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 13, "Sorry, You cannot perform this operation there is no Assessor mapped with your account."), Me)
    '            Exit Sub
    '        End If

    '        'S.SANDEEP |24-JUL-2019| -- START
    '        'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        'For Each dgRow As GridViewRow In dgv_Accomplishment_data.Rows
    '        '    If CType(dgRow.Cells(2).FindControl("chkSelect"), CheckBox).Checked = True Then
    '        '        Dim dRow As DataRow = mdtAccomplishmentData.Rows(dgRow.RowIndex)
    '        '        If CInt(dRow.Item("empupdatetranunkid")) <= 0 Then Exit Sub
    '        '        Dim mintEmpUpdateTranunkid As Integer = CInt(dRow.Item("empupdatetranunkid"))
    '        '        If CInt(dRow.Item("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
    '        '            dgRow.Style.Add("background", "red")
    '        '            strMessage &= " * " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 6, "Sorry you can not approved this goal,reason goal alrady approved status") & "<br />"
    '        '        End If
    '        '        If CInt(dRow.Item("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
    '        '            dgRow.Style.Add("background", "red")
    '        '            strMessage &= " * " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 7, "Sorry you can not approved this goal,reason goal alrady rejected status") & "<br />"
    '        '        End If
    '        '        objEUpdateProgress._Empupdatetranunkid = mintEmpUpdateTranunkid
    '        '        objEUpdateProgress._Pct_Completed = CDec(CType(dgRow.Cells(5).FindControl("txtPerComp"), TextBox).Text)

    '        '        If hdfAccomplishment.Value.ToString = "A" Then
    '        '            objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)
    '        '        Else
    '        '            objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected)
    '        '        End If
    '        '        objEUpdateProgress._GoalAccomplishmentRemark = txtAccomplishmentRemark.Text
    '        '        If objEUpdateProgress.Update() = False Then
    '        '            dgRow.Style.Add("background", "red")
    '        '            strMessage &= " * " & objEUpdateProgress._Message & "<br />"
    '        '        End If
    '        '    End If
    '        'Next

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgv_AccomplishmentData.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.Cells(2).FindControl("chkSelect"), CheckBox).Visible = True And CType(x.Cells(2).FindControl("chkSelect"), CheckBox).Checked = True)
    '        If gRow IsNot Nothing AndAlso gRow.Count > 0 Then
    '            For Each dgRow As GridViewRow In gRow
    '                objEUpdateProgress._Empupdatetranunkid = CInt(dgv_AccomplishmentData.DataKeys(dgRow.RowIndex)("empupdatetranunkid").ToString())
    '                objEUpdateProgress._Pct_Completed = CDec(CType(dgRow.Cells(9).FindControl("txtPerComp"), TextBox).Text)
    '                If hdfAccomplishment.Value.ToString = "A" Then
    '                    objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)
    '                Else
    '                    objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected)
    '                End If
    '                objEUpdateProgress._GoalAccomplishmentRemark = txtAccomplishmentRemark.Text

    '                If objEUpdateProgress.Update() = False Then
    '                    dgRow.Style.Add("background", "red")
    '                    strMessage &= " * " & objEUpdateProgress._Message & "<br />"
    '                End If
    '            Next
            '        End If

    '        'Dim xTable As DataTable = New DataView(mdtAccomplishmentData, "ischeck=true", "", DataViewRowState.CurrentRows).ToTable()
    '        'If hdfAccomplishment.Value.ToString = "A" Then
    '        '    objEUpdateProgress.Update(xTable, txtAccomplishmentRemark.Text, clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved, Session("UserId"), mstrModuleName_AppRejGoals, Session("HOST_NAME"), True, Session("IP_ADD"))
    '        'Else
    '        '    objEUpdateProgress.Update(xTable, txtAccomplishmentRemark.Text, clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved, Session("UserId"), mstrModuleName_AppRejGoals, Session("HOST_NAME"), True, Session("IP_ADD"))
    '        'End If

    '        'For Each dgRow As GridViewRow In dgv_AccomplishmentData.Rows
    '        '    If CType(dgRow.Cells(2).FindControl("chkSelect"), CheckBox).Checked = True Then
    '        '        If CType(dgRow.Cells(2).FindControl("chkSelect"), CheckBox).Visible = False Then Continue For
    '        '        Dim dRow As DataRow = mdtAccomplishmentData.Rows(dgRow.RowIndex)
    '        '        If CInt(dRow.Item("empupdatetranunkid")) <= 0 Then Exit Sub
    '        '        Dim mintEmpUpdateTranunkid As Integer = CInt(dRow.Item("empupdatetranunkid"))
    '        '        If CInt(dRow.Item("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
    '        '            dgRow.Style.Add("background", "red")
    '        '            strMessage &= " * " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 6, "Sorry you can not approved this goal,reason goal alrady approved status") & "<br />"
    '        '        End If
    '        '        If CInt(dRow.Item("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
    '        '            dgRow.Style.Add("background", "red")
    '        '            strMessage &= " * " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 7, "Sorry you can not approved this goal,reason goal alrady rejected status") & "<br />"
    '        '        End If
    '        '        objEUpdateProgress._Empupdatetranunkid = mintEmpUpdateTranunkid
    '        '        objEUpdateProgress._Pct_Completed = CDec(CType(dgRow.Cells(9).FindControl("txtPerComp"), TextBox).Text)

    '        '        If hdfAccomplishment.Value.ToString = "A" Then
    '        '            objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)
    '        '        Else
    '        '            objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected)
    '        '        End If
    '        '        objEUpdateProgress._GoalAccomplishmentRemark = txtAccomplishmentRemark.Text
    '        '        If objEUpdateProgress.Update() = False Then
    '        '            dgRow.Style.Add("background", "red")
    '        '            strMessage &= " * " & objEUpdateProgress._Message & "<br />"
    '        '        End If
    '        '    End If
    '        'Next
    '        'S.SANDEEP |24-JUL-2019| -- END


    '        If strMessage.Trim.Length > 0 Then
    '            DisplayMessage.DisplayMessage(strMessage, Me)
    '            Exit Sub
            '        Else
    '            objEUpdateProgress._WebFrmName = mstrModuleName_AppRejGoals
    '            objEUpdateProgress.Send_Notification_Employee(CInt(cboEmployee.SelectedValue), _
    '                                                          sAssessorName, txtAccomplishmentRemark.Text, _
    '                                                          IIf(hdfAccomplishment.Value.ToString = "A", True, False), _
    '                                                          cboPeriod.SelectedItem.Text, CInt(Session("CompanyUnkId")), _
    '                                                          enLogin_Mode.MGR_SELF_SERVICE, 0, Session("UserId"))
    '            Call Fill_Accomplishment_Grid()
            '        End If
    '        hdfAccomplishment.Value = ""
    '        'S.SANDEEP |25-MAR-2019| -- START
    '        txtAccomplishmentRemark.Text = ""
    '        'S.SANDEEP |25-MAR-2019| -- END
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '        objEUpdateProgress = Nothing
    '    End Try
    'End Sub

    'Protected Sub btnAccomplishmentApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishmentApprove.Click
    '    Dim strMessage As String = ""
    '    Try
    '        'S.SANDEEP |18-JAN-2020| -- START
    '        'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    '        'If mdtAccomplishmentData Is Nothing Then Exit Sub
    '        If dgv_AccomplishmentData.Rows.Count <= 0 Then Exit Sub
    '        'S.SANDEEP |18-JAN-2020| -- END

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgv_AccomplishmentData.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.Cells(2).FindControl("chkSelect"), CheckBox).Visible = True And CType(x.Cells(2).FindControl("chkSelect"), CheckBox).Checked = True)
    '        If gRow Is Nothing Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 15, "Please select") & " " & dgv_AccomplishmentData.Columns(2).HeaderText & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 16, "to proceed."), Me)
    '        ElseIf gRow IsNot Nothing AndAlso gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 15, "Please select") & " " & dgv_AccomplishmentData.Columns(2).HeaderText & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 16, "to proceed."), Me)
    '        End If

    '        ''S.SANDEEP |30-MAR-2019| -- START
    '        'mdtAccomplishmentData.AcceptChanges()
    '        'If mdtAccomplishmentData.AsEnumerable().Where(Function(x) x.Field(Of Integer)("Accomplished_statusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending).Count <= 0 Then
    '        '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 17, "No pending progress update to approve."), Me)
    '        '    For Each dRow As DataRow In mdtAccomplishmentData.Select("IsGrp = TRUE")
    '        '        'S.SANDEEP |24-JUL-2019| -- START
    '        '        'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        '        'CType(dgv_Accomplishment_data.Rows(mdtAccomplishmentData.Rows.IndexOf(dRow)).Cells(5).FindControl("txtPerComp"), TextBox).Visible = False
    '        '        CType(dgv_AccomplishmentData.Rows(mdtAccomplishmentData.Rows.IndexOf(dRow)).Cells(9).FindControl("txtPerComp"), TextBox).Visible = False
    '        '        'S.SANDEEP |24-JUL-2019| -- END
    '        '    Next
    '        '    Exit Sub
    '        'End If
    '        ''S.SANDEEP |24-JUL-2019| -- START
    '        ''ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        ''If mdtAccomplishmentData.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True And x.Field(Of Integer)("IsGrp") = 0).Count <= 0 Then
    '        'If mdtAccomplishmentData.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True And x.Field(Of Boolean)("IsGrp") = False).Count <= 0 Then
    '        '    'S.SANDEEP |24-JUL-2019| -- END
    '        '    'S.SANDEEP |24-JUL-2019| -- START
    '        '    'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        '    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 15, "Please select") & " " & dgv_Accomplishment_data.Columns(3).HeaderText & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 16, "to proceed."), Me)
    '        '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 15, "Please select") & " " & dgv_AccomplishmentData.Columns(2).HeaderText & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 16, "to proceed."), Me)
    '        '    'S.SANDEEP |24-JUL-2019| -- END
    '        '    For Each dRow As DataRow In mdtAccomplishmentData.Select("IsGrp = TRUE")
    '        '        'S.SANDEEP |24-JUL-2019| -- START
    '        '        'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        '        'CType(dgv_Accomplishment_data.Rows(mdtAccomplishmentData.Rows.IndexOf(dRow)).Cells(5).FindControl("txtPerComp"), TextBox).Visible = False
    '        '        CType(dgv_AccomplishmentData.Rows(mdtAccomplishmentData.Rows.IndexOf(dRow)).Cells(9).FindControl("txtPerComp"), TextBox).Visible = False
    '        '        'S.SANDEEP |24-JUL-2019| -- END
    '        '    Next
    '        '    Exit Sub
    '        'End If
    '        ''S.SANDEEP |30-MAR-2019| -- END

    '        If txtAccomplishmentRemark.Text.Trim.Length <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 27, "Remark is mandatory inforamation,please enter remark first."), Me)
    '            Exit Sub
    '        End If
    '        hdfAccomplishment.Value = "A"
    '        'S.SANDEEP |25-MAR-2019| -- START
    '        'lblAccomplishment_Message.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 2, "Are you sure, You want to Appoved this goal accomplishment?")
    '        lblAccomplishment_Message.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 2, "Are you sure you want to approve this progress update?")
    '        'S.SANDEEP |25-MAR-2019| -- END
    '        popup_Accomplishment_YesNo.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub btnAccomplishmentReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAccomplishmentReject.Click
    '    Try
    '        'S.SANDEEP |18-JAN-2020| -- START
    '        'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    '        'If mdtAccomplishmentData Is Nothing Then Exit Sub
    '        If dgv_AccomplishmentData.Rows.Count <= 0 Then Exit Sub
    '        'S.SANDEEP |18-JAN-2020| -- END

    '        'S.SANDEEP |30-MAR-2019| -- START
    '        'mdtAccomplishmentData.AcceptChanges()
    '        'If mdtAccomplishmentData.AsEnumerable().Where(Function(x) x.Field(Of Integer)("Accomplished_statusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending).Count <= 0 Then
    '        '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 18, "No pending progress update to reject."), Me)
    '        '    For Each dRow As DataRow In mdtAccomplishmentData.Select("IsGrp = TRUE")
    '        '        'S.SANDEEP |24-JUL-2019| -- START
    '        '        'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        '        'CType(dgv_Accomplishment_data.Rows(mdtAccomplishmentData.Rows.IndexOf(dRow)).Cells(5).FindControl("txtPerComp"), TextBox).Visible = False
    '        '        CType(dgv_AccomplishmentData.Rows(mdtAccomplishmentData.Rows.IndexOf(dRow)).Cells(9).FindControl("txtPerComp"), TextBox).Visible = False
    '        '        'S.SANDEEP |24-JUL-2019| -- END
    '        '    Next
    '        '    Exit Sub
    '        'End If
    '        ''S.SANDEEP |24-JUL-2019| -- START
    '        ''ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        ''If mdtAccomplishmentData.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True And x.Field(Of Integer)("IsGrp") = 0).Count <= 0 Then
    '        'If mdtAccomplishmentData.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ischeck") = True And x.Field(Of Boolean)("IsGrp") = False).Count <= 0 Then
    '        '    'S.SANDEEP |24-JUL-2019| -- END
    '        '    'S.SANDEEP |24-JUL-2019| -- START
    '        '    'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        '    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 15, "Please select") & " " & dgv_Accomplishment_data.Columns(3).HeaderText & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 16, "to proceed."), Me)
    '        '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 15, "Please select") & " " & dgv_AccomplishmentData.Columns(2).HeaderText & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 16, "to proceed."), Me)
    '        '    'S.SANDEEP |24-JUL-2019| -- END

    '        '    For Each dRow As DataRow In mdtAccomplishmentData.Select("IsGrp = TRUE")
    '        '        'S.SANDEEP |24-JUL-2019| -- START
    '        '        'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        '        'CType(dgv_Accomplishment_data.Rows(mdtAccomplishmentData.Rows.IndexOf(dRow)).Cells(5).FindControl("txtPerComp"), TextBox).Visible = False
    '        '        CType(dgv_AccomplishmentData.Rows(mdtAccomplishmentData.Rows.IndexOf(dRow)).Cells(9).FindControl("txtPerComp"), TextBox).Visible = False
    '        '        'S.SANDEEP |24-JUL-2019| -- END
    '        '    Next
    '        '    Exit Sub
    '        'End If
    '        ''S.SANDEEP |30-MAR-2019| -- END

    '        Dim gRow As IEnumerable(Of GridViewRow) = Nothing
    '        gRow = dgv_AccomplishmentData.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.Cells(2).FindControl("chkSelect"), CheckBox).Visible = True And CType(x.Cells(2).FindControl("chkSelect"), CheckBox).Checked = True)
    '        If gRow Is Nothing Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 15, "Please select") & " " & dgv_AccomplishmentData.Columns(2).HeaderText & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 16, "to proceed."), Me)
    '        ElseIf gRow IsNot Nothing AndAlso gRow.Count <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 15, "Please select") & " " & dgv_AccomplishmentData.Columns(2).HeaderText & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 16, "to proceed."), Me)
    '        End If

    '        If txtAccomplishmentRemark.Text.Trim.Length <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 27, "Remark is mandatory inforamation,please enter remark first."), Me)
    '            Exit Sub
    '        End If
    '        hdfAccomplishment.Value = "R"
    '        'S.SANDEEP |25-MAR-2019| -- START
    '        'lblAccomplishment_Message.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 2, "Are you sure, You want to Reject this goal accomplishment?")
    '        lblAccomplishment_Message.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 3, "Are you sure you want to reject this progress update?")
    '        'S.SANDEEP |25-MAR-2019| -- END
    '        popup_Accomplishment_YesNo.Show()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgv_Attchment.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage("No Files to download.", Me)
                Exit Sub
            End If

            Dim objScanAttachment As New clsScan_Attach_Documents
            'S.SANDEEP |04-SEP-2021| -- START
            'ISSUE : TAKING CARE FROM SLOWNESS QUERY
            'objScanAttachment.GetList(Session("Document_Path"), "List", "", CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "scanattachrefid = '" & _
            '                                           enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & _
            '                                           CInt(mintEmpUpdateTranunkid) & "' AND form_name = '" & _
            '                                           mstrModuleName_upd_percent & "'")

            objScanAttachment.GetList(Session("Document_Path"), "List", "", CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "scanattachrefid = '" & _
                                                       enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & _
                                                       CInt(mintEmpUpdateTranunkid) & "' AND form_name = '" & _
                                      mstrModuleName_upd_percent & "'", _
                                      CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
            'S.SANDEEP |04-SEP-2021| -- END
            Dim xrow() As DataRow = objScanAttachment._Datatable.Select()
            If xrow IsNot Nothing AndAlso xrow.Count > 0 Then
                mdtAttachement = xrow.CopyToDataTable
            Else
                mdtAttachement = objScanAttachment._Datatable.Clone
            End If
            strMsg = DownloadAllDocument("file" & txtUpdateEmployeeName.Text.Replace(" ", "") + ".zip", mdtAttachement, Session("ArutiSelfServiceURL"), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

    'S.SANDEEP |04-DEC-2019| -- START
    'ISSUE/ENHANCEMENT : REPORT TEMPLATE 17 -- NMB
    Protected Sub btnViewUpdateProgressReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewUpdateProgressReport.Click
        Dim objEmpField1 As New clsassess_empfield1_master
        Try
            If dgvData.Rows.Count > 0 Then

                If objEmpField1 Is Nothing Then objEmpField1 = New clsassess_empfield1_master
                Dim dsFinal As DataSet = objEmpField1.GetNewAccomplishemtn_DisplayList(CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), 0, Session("CascadingTypeId"), True, "List", Session("fmtCurrency"), 0, 0)
                If dsFinal IsNot Nothing AndAlso dsFinal.Tables.Count > 0 AndAlso dsFinal.Tables(0).Rows.Count > 0 Then
                    Dim dtftab As DataTable = dsFinal.Tables(0).DefaultView.ToTable(False, "pName", "field_data", "cfieldunkid", "cFCaption", "cData", "dfieldunkid", "dFCaption", "dData", "efieldunkid", "eFCaption", "eData", "Goal_status", "per_comp", "Goal_Remark", "accomplished_status", "fieldcaption", "ddate", "dfinalvalue", "dgoalvalue", "pCaption", "empfieldunkid", "finalvalue", "empupdatetranunkid", "empfieldtypeid", "approval_remark", "perspectiveunkid")
                    Dim dtemp As DataTable = dtftab.DefaultView.ToTable(True, "pCaption", "fieldcaption", "cfieldunkid", "cFCaption", "dfieldunkid", "dFCaption", "efieldunkid", "eFCaption", "empfieldtypeid")
                    Dim xTable As New DataTable
                    Dim dCol As DataColumn = Nothing

                    For Each iRow As DataRow In dtemp.Rows
                        For Each iCol As DataColumn In dtemp.Columns
                            Select Case iCol.ColumnName.ToUpper
                                Case "CFIELDUNKID", "DFIELDUNKID", "EFIELDUNKID"
                                    If iRow(iCol.ColumnName) > 0 Then
                                        dCol = New DataColumn
                                        If xTable.Columns.Contains("Col_" & iRow(iCol.ColumnName)) = False Then
                                            dCol.ColumnName = "Col_" & iRow(iCol.ColumnName)
                                            Select Case CInt(iRow(iCol.ColumnName))
                                                Case 6 : dCol.Caption = iRow("cFCaption")
                                                Case 7 : dCol.Caption = iRow("dFCaption")
                                                Case 8 : dCol.Caption = iRow("eFCaption")
                                            End Select
                                            dCol.DataType = GetType(System.String)
                                            dCol.DefaultValue = ""
                                            xTable.Columns.Add(dCol)
                                        End If
                                    End If
                                Case "PCAPTION"
                                    If xTable.Columns.Contains("Col_Perspective") = False Then
                                        dCol = New DataColumn
                                        dCol.ColumnName = "Col_Perspective"
                                        dCol.Caption = iRow(iCol.ColumnName)
                                        dCol.DataType = iCol.DataType
                                        dCol.DefaultValue = ""
                                        xTable.Columns.Add(dCol)
                                    End If

                                Case "FIELDCAPTION"
                                    If xTable.Columns.Contains("Col_" & iRow("empfieldtypeid")) = False Then
                                        dCol = New DataColumn
                                        dCol.ColumnName = "Col_" & iRow("empfieldtypeid")
                                        dCol.Caption = iRow(iCol.ColumnName)
                                        dCol.DataType = iCol.DataType
                                        dCol.DefaultValue = ""
                                        xTable.Columns.Add(dCol)
                                    End If
                            End Select
                        Next
                    Next

                    dCol = New DataColumn
                    dCol.ColumnName = "goalvalue"
                    dCol.Caption = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 3004, "Target")
                    dCol.DataType = GetType(System.String)
                    dCol.DefaultValue = ""
                    xTable.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "updatedate"
                    dCol.Caption = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 3005, "Update Date")
                    dCol.DataType = GetType(System.String)
                    dCol.DefaultValue = ""
                    xTable.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "value"
                    dCol.Caption = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 3006, "Value")
                    dCol.DataType = GetType(System.String)
                    dCol.DefaultValue = ""
                    xTable.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "updateValue"
                    dCol.Caption = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 3007, "Updated Value")
                    dCol.DataType = GetType(System.String)
                    dCol.DefaultValue = ""
                    xTable.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "pct_cmplt"
                    dCol.Caption = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 3008, "% Completed")
                    dCol.DataType = GetType(System.String)
                    dCol.DefaultValue = ""
                    xTable.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "e_comments"
                    dCol.Caption = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 3009, "Employee Comments")
                    dCol.DataType = GetType(System.String)
                    dCol.DefaultValue = ""
                    xTable.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "a_comments"
                    dCol.Caption = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 3010, "Approval Comments")
                    dCol.DataType = GetType(System.String)
                    dCol.DefaultValue = ""
                    xTable.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "astatus"
                    dCol.Caption = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 3011, "Status")
                    dCol.DataType = GetType(System.String)
                    dCol.DefaultValue = ""
                    xTable.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "empupdatetranunkid"
                    dCol.Caption = ""
                    dCol.DataType = GetType(System.Int32)
                    dCol.DefaultValue = 0
                    xTable.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "empfieldunkid"
                    dCol.Caption = ""
                    dCol.DataType = GetType(System.Int32)
                    dCol.DefaultValue = 0
                    xTable.Columns.Add(dCol)

                    dCol = New DataColumn
                    dCol.ColumnName = "perspectiveunkid"
                    dCol.Caption = ""
                    dCol.DataType = GetType(System.Int32)
                    dCol.DefaultValue = 0
                    xTable.Columns.Add(dCol)

                    Dim iList As List(Of Integer) = dtftab.AsEnumerable().Select(Function(x) x.Field(Of Integer)("empfieldunkid")).Distinct().ToList()

                    If iList IsNot Nothing AndAlso iList.Count > 0 Then
                        For Each ival In iList
                            dtemp = New DataView(dtftab, "empfieldunkid='" & ival & "'", "empupdatetranunkid", DataViewRowState.CurrentRows).ToTable()
                            Dim iRowTotal As Decimal = 0
                            If dtemp IsNot Nothing AndAlso dtemp.Rows.Count > 0 Then
                                For Each iRw As DataRow In dtemp.Rows
                                    If IsDBNull(iRw("finalvalue")) = False Then
                                        iRowTotal += CDec(iRw("finalvalue"))
                                    End If
                                    Dim xR As DataRow = xTable.NewRow
                                    xR("Col_Perspective") = iRw("pName")
                                    xR("Col_" & iRw("empfieldtypeid")) = iRw("field_data")
                                    If xTable.Columns.Contains("Col_" & iRw("cfieldunkid")) Then
                                        xR("Col_" & iRw("cfieldunkid")) = iRw("cData")
                                    End If
                                    If xTable.Columns.Contains("Col_" & iRw("dfieldunkid")) Then
                                        xR("Col_" & iRw("dfieldunkid")) = iRw("dData")
                                    End If
                                    If xTable.Columns.Contains("Col_" & iRw("efieldunkid")) Then
                                        xR("Col_" & iRw("efieldunkid")) = iRw("eData")
                                    End If
                                    xR("Col_" & iRw("empfieldtypeid")) = iRw("field_data")
                                    xR("goalvalue") = iRw("dgoalvalue")
                                    xR("updatedate") = iRw("ddate")
                                    If IsDBNull(iRw("finalvalue")) = False Then
                                        xR("value") = Format(CDec(iRw("finalvalue")), Session("fmtCurrency"))
                                    Else
                                        xR("value") = Format(CDec(0), Session("fmtCurrency"))
                                    End If
                                    xR("updateValue") = Format(iRowTotal, Session("fmtCurrency"))
                                    If IsDBNull(iRw("per_comp")) = False Then
                                        xR("pct_cmplt") = Format(CDec(iRw("per_comp")), "#############0.#0")
                                    Else
                                        xR("pct_cmplt") = Format(CDec(0), "#############0.#0")
                                    End If
                                    xR("e_comments") = iRw("Goal_Remark")
                                    xR("a_comments") = iRw("approval_remark")
                                    xR("astatus") = iRw("accomplished_status")
                                    xR("empupdatetranunkid") = iRw("empupdatetranunkid")
                                    xR("perspectiveunkid") = iRw("perspectiveunkid")
                                    xTable.Rows.Add(xR)
                                Next
                            End If
                        Next
                    End If

                    xTable = New DataView(xTable, "", "perspectiveunkid, empfieldunkid, empupdatetranunkid DESC", DataViewRowState.CurrentRows).ToTable()

                    xTable.Columns.Remove("empfieldunkid")
                    xTable.Columns.Remove("empupdatetranunkid")
                    xTable.Columns.Remove("perspectiveunkid")

                    Dim strBuilder As New StringBuilder

                    Dim objEmp As New clsEmployee_Master
                    Dim objPrd As New clscommom_period_Tran
                    Dim objDep As New clsDepartment
                    Dim objJob As New clsJobs
                    Dim objCls As New clsClass

                    objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
                    objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate")), Nothing) = CInt(cboEmployee.SelectedValue)
                    objDep._Departmentunkid = objEmp._Departmentunkid
                    objCls._Classesunkid = objEmp._Classunkid
                    objJob._Jobunkid = objEmp._Jobunkid

                    Dim objEvalMaster As New clsevaluation_analysis_master
                    Dim dsList As DataSet = objEvalMaster.GetEmployee_AssessorReviewerDetails(CInt(cboPeriod.SelectedValue), Session("Database_Name").ToString(), Nothing, CInt(cboEmployee.SelectedValue))
                    objEvalMaster = Nothing
                    Dim iEAssessorName As String = String.Empty
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        Dim dtmp() As DataRow = Nothing
                        dtmp = dsList.Tables(0).Select("isreviewer = 0 AND isfound = 1")
                        If dtmp.Length > 0 Then
                            iEAssessorName = dtmp(0).Item("arName")
                        Else
                            dtmp = dsList.Tables(0).Select("isreviewer = 0 AND isfound = 0 AND visibletypeid = 1")
                            If dtmp.Length > 0 Then
                                iEAssessorName = dtmp(0).Item("arName")
                            End If
                        End If
                    End If

                    strBuilder.Append("<HTML>" & vbCrLf)
                    strBuilder.Append("<HEAD>" & vbCrLf)
                    strBuilder.Append("<STYLE TYPE='text/css'>" & vbCrLf)
                    strBuilder.Append("u.dotted" & vbCrLf)
                    strBuilder.Append("{" & vbCrLf)
                    strBuilder.Append("border-bottom: 1px dotted #000;" & vbCrLf)
                    strBuilder.Append("text-decoration: none;" & vbCrLf)
                    strBuilder.Append("}" & vbCrLf)
                    strBuilder.Append("</STYLE>" & vbCrLf)
                    strBuilder.Append("</HEAD>" & vbCrLf)
                    strBuilder.Append("<BODY style='width: 100%'>" & vbCrLf)
                    strBuilder.Append("<TABLE width = '600px' align='center'>" & vbCrLf)
                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                    strBuilder.Append("    <td>" & vbCrLf)
                    strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                    strBuilder.Append("            <tr style='text-align: Center; width: 100%'>" & vbCrLf)
                    strBuilder.Append("                <td style='text-align: Center; width: 100%'>" & vbCrLf)
                    Dim objCompany As New clsCompany_Master
                    objCompany._Companyunkid = CInt(Session("CompanyUnkId"))
                    Dim strPath As String = String.Empty
                    Dim base64String As String = ""
                    If objCompany._Image IsNot Nothing Then
                        strPath = My.Computer.FileSystem.SpecialDirectories.Temp & "\clogo.png"
                        objCompany._Image.Save(strPath)
                        If strPath.Trim.Length > 0 Then
                            Try
                                base64String = Convert.ToBase64String(IO.File.ReadAllBytes(strPath))
                            Catch ex1 As ArgumentException
                                CommonCodes.LogErrorOnly(ex1)
                            Catch ex2 As FormatException
                                CommonCodes.LogErrorOnly(ex2)
                            Catch ex As Exception
                                CommonCodes.LogErrorOnly(ex)
                            End Try
                        End If
                        'Using image As Image = image.FromFile(strPath)
                        '    Using m As New MemoryStream()
                        '        image.Save(m, image.RawFormat)
                        '        Dim imageBytes() As Byte = m.ToArray()
                        '        Try
                        '            If imageBytes IsNot Nothing AndAlso imageBytes.Length > 0 Then
                        '                base64String = Convert.ToBase64String(imageBytes)
                        '            End If
                        '        Catch ex1 As ArgumentException
                        '            CommonCodes.LogErrorOnly(ex1)
                        '        Catch ex As FormatException
                        '            CommonCodes.LogErrorOnly(ex)
                        '        End Try
                        '    End Using
                        'End Using
                        strBuilder.Append(" <img src = ""data:image/png;base64, " & base64String & """ HEIGHT = '100px' WIDTH = '150px' />" & vbCrLf)
                    Else
                        strBuilder.Append(" <img src = '' HEIGHT = '100px' WIDTH = '150px' />" & vbCrLf)
                    End If
                    strBuilder.Append("                </td>" & vbCrLf)
                    strBuilder.Append("            </tr>" & vbCrLf)
                    strBuilder.Append("        </table>" & vbCrLf)
                    strBuilder.Append("    </td>" & vbCrLf)
                    strBuilder.Append("</tr>" & vbCrLf)
                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                    strBuilder.Append("    <td style='text-align: Center;'><B><FONT SIZE=3 FONT COLOR = 'Blue'>" & vbCrLf)
                    strBuilder.Append("                " & objCompany._Name & vbCrLf)
                    strBuilder.Append("    </td>" & vbCrLf)
                    strBuilder.Append("</tr>" & vbCrLf)
                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                    strBuilder.Append("    <td>" & vbCrLf)
                    strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                    strBuilder.Append("            <tr style='text-align: Center; width: 100%'>" & vbCrLf)
                    strBuilder.Append("                <td style='text-align: Center;'><FONT SIZE=2>" & vbCrLf)
                    strBuilder.Append("                    <B>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 3012, "Progress Update Report") & "</B></FONT>" & vbCrLf)
                    strBuilder.Append("                </td>" & vbCrLf)
                    strBuilder.Append("            </tr>" & vbCrLf)
                    strBuilder.Append("        </table>" & vbCrLf)
                    strBuilder.Append("    </td>" & vbCrLf)
                    strBuilder.Append("</tr>" & vbCrLf)

                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                    strBuilder.Append("    <td>" & vbCrLf)
                    strBuilder.Append("        <table border='0' style='width: 100%;'>" & vbCrLf)
                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 74, "PERIOD UNDER REVIEW") & "</b></td>" & vbCrLf)
                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 75, "FROM") & "</b></td>" & vbCrLf)
                    strBuilder.Append("                <td><u class='dotted'><b>" & objPrd._Start_Date.Date & "</b></u></td>" & vbCrLf)
                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 76, "TO") & "</b></td>" & vbCrLf)
                    strBuilder.Append("                <td><u class='dotted'><b>" & objPrd._End_Date.Date & "</b></u></td>" & vbCrLf)
                    strBuilder.Append("            </tr>" & vbCrLf)
                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 77, "NAME OF STAFF") & "</b></td>" & vbCrLf)
                    strBuilder.Append("                <td><u class='dotted'><b>" & objEmp._Firstname & " " & objEmp._Surname & "</b></u></td>" & vbCrLf)
                    strBuilder.Append("            </tr>" & vbCrLf)
                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 78, "POSITION TITLE") & "</b></td>" & vbCrLf)
                    strBuilder.Append("                <td><u class='dotted'><b>" & objJob._Job_Name & "</b></u></td>" & vbCrLf)
                    strBuilder.Append("            </tr>" & vbCrLf)
                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 79, "DEPARTMENT/BRANCH") & "</b></td>" & vbCrLf)
                    strBuilder.Append("                <td><u class='dotted'><b>" & objDep._Name & "/" & objCls._Name & "</b></u></td>" & vbCrLf)
                    strBuilder.Append("            </tr>" & vbCrLf)
                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 80, "DATE OF APPOINTMENT") & "</b></td>" & vbCrLf)
                    strBuilder.Append("                <td><u class='dotted'><b>" & objEmp._Appointeddate.Date & "</b></u></td>" & vbCrLf)
                    strBuilder.Append("                <td><b>" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsAssessmentListReport", 81, "NAME OF SUPERVISOR") & "</b></td>" & vbCrLf)
                    strBuilder.Append("                <td><u class='dotted'><b>" & iEAssessorName & "</b></u></td>" & vbCrLf)
                    strBuilder.Append("            </tr>" & vbCrLf)
                    strBuilder.Append("        </table>" & vbCrLf)
                    strBuilder.Append("    </td>" & vbCrLf)
                    strBuilder.Append("</tr>" & vbCrLf)
                    strBuilder.Append("<tr style='width: 100%'>" & vbCrLf)
                    strBuilder.Append("    <td>" & vbCrLf)
                    strBuilder.Append("        <table border='1' style='width: 100%;'>" & vbCrLf)
                    strBuilder.Append("            <tr style='width: 100%'>" & vbCrLf)
                    For Each dtCol As DataColumn In xTable.Columns
                        strBuilder.Append("         <TD bgcolor='#2e6da4'>" & vbCrLf)
                        strBuilder.Append("             <b><Font Color = 'White'>" & dtCol.Caption & "</Font></b>" & vbCrLf)
                        strBuilder.Append("         </TD>" & vbCrLf)
                    Next
                    strBuilder.Append("            </tr>" & vbCrLf)
                    Dim mDicPerspective As New Dictionary(Of String, Integer)
                    mDicPerspective = xTable.AsEnumerable().GroupBy(Function(x) x.Field(Of String)("Col_Perspective")) _
                                     .[Select](Function(x) New With {Key .barid = x.Key, Key .barCount = x.[Select](Function(z) z.Field(Of String)("Col_Perspective")) _
                                     .Count()}).ToDictionary(Function(x) x.barid.ToString(), Function(y) y.barCount)
                    Dim blnIsSamePerspective As Boolean = False
                    Dim strOldPerspective As String = String.Empty

                    For Each dtRow As DataRow In xTable.Rows
                        strBuilder.Append("<TR style='width: 100%'>" & vbCrLf)
                        If strOldPerspective <> dtRow("Col_Perspective") Then
                            strOldPerspective = dtRow("Col_Perspective")
                            blnIsSamePerspective = True
                        End If
                        For Each dtCol As DataColumn In xTable.Columns
                            If blnIsSamePerspective Then
                                strBuilder.Append("<TD rowspan = '" & mDicPerspective(strOldPerspective) & "'>" & vbCrLf)
                                strBuilder.Append("<b>" & dtRow(dtCol) & "</b>" & vbCrLf)
                                strBuilder.Append("</TD>" & vbCrLf)
                                blnIsSamePerspective = False
                            Else
                                If dtCol.ColumnName.ToString().ToUpper() <> "COL_PERSPECTIVE" Then
                                    strBuilder.Append("<TD>" & vbCrLf)
                                    strBuilder.Append(dtRow(dtCol) & vbCrLf)
                                    strBuilder.Append("</TD>" & vbCrLf)
                                End If
                            End If
                        Next
                        strBuilder.Append("         </TR>" & vbCrLf)
                    Next

                    strBuilder.Append("        </table>" & vbCrLf)
                    strBuilder.Append("    </td>" & vbCrLf)
                    strBuilder.Append("</tr>" & vbCrLf)
                    strBuilder.Append("</TABLE>" & vbCrLf)
                    strBuilder.Append("</BODY>" & vbCrLf)
                    strBuilder.Append("</HEAD>" & vbCrLf)
                    strBuilder.Append("</HTML>")
                    divhtml.InnerHtml = strBuilder.ToString
                    popupEmpAssessForm.Show()
                    objEmp = Nothing : objPrd = Nothing : objDep = Nothing : objCls = Nothing : objJob = Nothing
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmpField1 = Nothing
        End Try
    End Sub
    'S.SANDEEP |04-DEC-2019| -- END

#End Region

#Region " GridView Event "

    Protected Sub dgvData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvData.RowCommand
        Try
            If e.CommandName = "UpdateProgress" Then

                'S.SANDEEP [04 Jan 2016] -- START
                If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) <= 0 Or _
                   CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                    'Pinkal (15-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 36, "Period is mandatory information. Please select Period to continue."), Me)  '--ADDED NEW LANAGUAGE.
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Employee and Period are mandatory information. Please select Employee and Period to continue."), Me)
                    End If
                    'Pinkal (15-Mar-2019) -- End
                    Exit Sub
                End If

                Dim objEval As New clsevaluation_analysis_master
                'S.SANDEEP |27-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                'If objEval.isExist(enAssessmentMode.SELF_ASSESSMENT, cboEmployee.SelectedValue, cboPeriod.SelectedValue) = True Then
                '    DisplayMessage.DisplayMessage("Sorry, you cannot do update progress. Reason assessment is already for the employee for selected period.", Me)
                '    objEval = Nothing
                '    Exit Sub
                'End If
                'If objEval.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, cboEmployee.SelectedValue, cboPeriod.SelectedValue) = True Then
                '    DisplayMessage.DisplayMessage("Sorry, you cannot do update progress. Reason assessment is already for the employee for selected period.", Me)
                '    objEval = Nothing
                '    Exit Sub
                'End If
                'If objEval.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, cboEmployee.SelectedValue, cboPeriod.SelectedValue) = True Then
                '    DisplayMessage.DisplayMessage("Sorry, you cannot do update progress. Reason assessment is already for the employee for selected period.", Me)
                '    objEval = Nothing
                '    Exit Sub
                'End If
                If objEval.isExist(enAssessmentMode.SELF_ASSESSMENT, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , , , , , False) = True Then
                    DisplayMessage.DisplayMessage("Sorry, you cannot do update progress. Reason assessment is already for the employee for selected period.", Me)
                    objEval = Nothing
                    Exit Sub
                End If
                If objEval.isExist(enAssessmentMode.APPRAISER_ASSESSMENT, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , , , , , False) = True Then
                    DisplayMessage.DisplayMessage("Sorry, you cannot do update progress. Reason assessment is already for the employee for selected period.", Me)
                    objEval = Nothing
                    Exit Sub
                End If
                If objEval.isExist(enAssessmentMode.REVIEWER_ASSESSMENT, cboEmployee.SelectedValue, cboPeriod.SelectedValue, , , , , , False) = True Then
                    DisplayMessage.DisplayMessage("Sorry, you cannot do update progress. Reason assessment is already for the employee for selected period.", Me)
                    objEval = Nothing
                    Exit Sub
                End If
                'S.SANDEEP |27-JUL-2019| -- END

                If objEval IsNot Nothing Then objEval = Nothing
                'S.SANDEEP [04 Jan 2016] -- END

                Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Call UpdateProgressLoad(gridRow)
                popup_UpdateProgress.Show()
                mblnUpdateProgress = True

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Call SetDateFormat()

                Dim lnk As New LinkButton

                

                lnk.ID = "lnkUpdateProgress"
                lnk.CommandName = "UpdateProgress"
                'S.SANDEEP |20-JAN-2022| -- START
                'lnk.Text = ""
                'lnk.CssClass = "updatedata123"
                'lnk.Style.Add("color", "red")
                'lnk.Style.Add("font-size", "20px")

                lnk.Text = "≡"
                'lnk.CssClass = "updatedata123"
                lnk.Style.Add("color", "red")
                lnk.Style.Add("font-size", "40px")
                'S.SANDEEP |20-JAN-2022| -- END
                e.Row.Cells(mintUpdateProgressIndex).Controls.Add(lnk)

                'S.SANDEEP |18-JAN-2020| -- START
                'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                'Dim intIndex As Integer = 0
                'intIndex = mdtFinal.Columns("St_Date").ExtendedProperties("index")

                'If e.Row.Cells(intIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(intIndex).Text.Trim <> "&nbsp;" Then
                '    e.Row.Cells(intIndex).Text = CDate(e.Row.Cells(intIndex).Text).Date.ToShortDateString
                'End If

                'intIndex = mdtFinal.Columns("Ed_Date").ExtendedProperties("index")
                'If e.Row.Cells(intIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(intIndex).Text.Trim <> "&nbsp;" Then
                '    e.Row.Cells(intIndex).Text = CDate(e.Row.Cells(intIndex).Text).Date.ToShortDateString
                'End If

                'If mintGoalValueColIndex < 0 Then mintGoalValueColIndex = Me.ViewState("mintGoalValueColIndex")
                ''S.SANDEEP |05-MAR-2019| -- START
                ''ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                'If mintUoMColIndex < 0 Then mintUoMColIndex = Me.ViewState("mintUoMColIndex")
                'If mstrUoMColName.Trim.Length <= 0 Then mstrUoMColName = Me.ViewState("mstrUoMColName")
                ''S.SANDEEP |05-MAR-2019| -- END
                'If mintGoalValueColIndex <> -1 Then
                '    If e.Row.Cells(mintGoalValueColIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(mintGoalValueColIndex).Text.Trim <> "&nbsp;" Then
                '        If mintUoMColIndex < 0 Then
                '            e.Row.Cells(mintGoalValueColIndex).Text = Format(CDbl(e.Row.Cells(mintGoalValueColIndex).Text), Session("fmtCurrency"))
                '        Else
                '            Dim iUoM As String = ""
                '            If mdtFinal.Rows.Count > 0 Then
                '                iUoM = mdtFinal.Rows(e.Row.RowIndex)(mstrUoMColName).ToString()
                '            End If
                '            e.Row.Cells(mintGoalValueColIndex).Text = Format(CDbl(e.Row.Cells(mintGoalValueColIndex).Text), Session("fmtCurrency")) & " " & iUoM
                '            dgvData.DataKeyNames(e.Row.RowIndex)("UoMType").ToString()
                '        End If
                '    End If
                'End If
                If mintUoMColIndex < 0 Then mintUoMColIndex = Me.ViewState("mintUoMColIndex")
                If mintGoalValueColIndex <> -1 Then
                    If e.Row.Cells(mintGoalValueColIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(mintGoalValueColIndex).Text.Trim <> "&nbsp;" Then
                        If mintUoMColIndex < 0 Then
                            If mintGoalValueColIndex <> -1 Then e.Row.Cells(mintGoalValueColIndex).Text = Format(CDbl(e.Row.Cells(mintGoalValueColIndex).Text), Session("fmtCurrency"))
                        Else
                            If mintGoalValueColIndex <> -1 Then e.Row.Cells(mintGoalValueColIndex).Text = Format(CDbl(e.Row.Cells(mintGoalValueColIndex).Text), Session("fmtCurrency")) & " " & dgvData.DataKeys(e.Row.RowIndex)("UoMType").ToString()
                        End If
                    End If
                End If
                'S.SANDEEP |18-JAN-2020| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvHistory_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvHistory.ItemCommand
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                If e.Item.ItemIndex < 0 Then Exit Sub

                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    CType(e.Item.Cells(0).FindControl("imgEdit"), ImageButton).Visible = CBool(Session("AllowtoUpdatePercentCompletedEmployeeGoals"))
                    CType(e.Item.Cells(1).FindControl("imgDelete"), ImageButton).Visible = CBool(Session("AllowToDeletePercentCompletedEmployeeGoals"))
                End If

                'S.SANDEEP |18-JAN-2020| -- START
                'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                'If e.CommandName = "objEdit" Then
                '    If CBool(Session("GoalsAccomplishedRequiresApproval")) Then
                '        If CInt(e.Item.Cells(9).Text) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
                '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 10, "Sorry, You can not edit this recoed.reason: this record is approved "), Me)
                '            Exit Sub
                '        End If
                '        If CInt(e.Item.Cells(9).Text) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
                '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 11, "Sorry, You can not edit this recoed.reason: this record is Rejected "), Me)
                '            Exit Sub
                '        End If
                '    End If

                '    If mdtGoalAccomplishment IsNot Nothing Then
                '        If mdtGoalAccomplishment.Select("empupdatetranunkid > " & CInt(e.Item.Cells(7).Text)).Length > 0 Then
                '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 204, "Sorry, you cannot edit this information. Reason there is another information present which is greater than selected information."), Me)
                '            Exit Sub
                '        End If
                '    End If

                '    mintEmpUpdateTranunkid = CInt(e.Item.Cells(7).Text)
                '    objEUpdateProgress._Empupdatetranunkid = mintEmpUpdateTranunkid
                '    Call SetTagValue(mdtGoalAccomplishment, True)
                '    Call GetUpdateValue()
                'Else
                'S.SANDEEP |18-JAN-2020| -- END
                If e.CommandName = "objDelete" Then

                    If CBool(Session("GoalsAccomplishedRequiresApproval")) Then
                        If CInt(e.Item.Cells(9).Text) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 10, "Sorry, You can not edit this recoed.reason: this record is approved "), Me)
                            Exit Sub
                        End If
                        If CInt(e.Item.Cells(9).Text) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 11, "Sorry, You can not edit this recoed.reason: this record is Rejected "), Me)
                            Exit Sub
                        End If
                    End If

                    'S.SANDEEP |18-JAN-2020| -- START
                    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                    'If mdtGoalAccomplishment IsNot Nothing Then
                    '    If mdtGoalAccomplishment.Select("empupdatetranunkid > " & CInt(e.Item.Cells(7).Text)).Length > 0 Then
                    '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 205, "Sorry, you cannot delete this information. Reason there is another information present which is greater than selected information."), Me)
                    '        Exit Sub
                    '    End If
                    'End If
                    Dim iTranId As Integer = dgvHistory.Items.Cast(Of DataGridItem).AsEnumerable().Select(Function(x) CInt(x.Cells(7).Text)).Max()
                    If iTranId > CInt(e.Item.Cells(7).Text) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 205, "Sorry, you cannot delete this information. Reason there is another information present which is greater than selected information."), Me)
                        Exit Sub
                    End If
                    'S.SANDEEP |18-JAN-2020| -- END

                    mintEmpUpdateTranunkid = CInt(e.Item.Cells(7).Text)
                    lblUpdateMessages.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 7, "Are you sure you want to delete this progress for the selected employee?")
                    hdf_UpdateYesNo.Value = "DELETE"
                    popup_UpdateYesNO.Show()

                ElseIf e.CommandName = "objAttach" Then
                    mintEmpUpdateTranunkid = CInt(e.Item.Cells(7).Text)
                    Dim objScanAttachment As New clsScan_Attach_Documents

                    'S.SANDEEP |04-SEP-2021| -- START
                    'ISSUE : TAKING CARE FROM SLOWNESS QUERY
                    'objScanAttachment.GetList(Session("Document_Path"), "List", "", CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "scanattachrefid = '" & _
                    '                                   enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & _
                    '                                   CInt(mintEmpUpdateTranunkid) & "' AND form_name = '" & _
                    '                                   mstrModuleName_upd_percent & "'")

                    objScanAttachment.GetList(Session("Document_Path"), "List", "", CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "scanattachrefid = '" & _
                                                       enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & _
                                                       CInt(mintEmpUpdateTranunkid) & "' AND form_name = '" & _
                                                       mstrModuleName_upd_percent & "'", _
                                                       CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
                    'S.SANDEEP |04-SEP-2021| -- END

                    Dim xRow As DataRow() = objScanAttachment._Datatable.Select()
                    If xRow IsNot Nothing AndAlso xRow.Count > 0 Then
                        mdtAttachement = xRow.CopyToDataTable
                    Else
                        mdtAttachement = objScanAttachment._Datatable.Clone
                    End If
                    'S.SANDEEP |18-JAN-2020| -- START
                    'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
                    'mblnAttachmentPopup = True
                    'Call FillAttachment()
                    'popup_ScanAttchment.Show()
                    dgv_Attchment.AutoGenerateColumns = False
                    dgv_Attchment.DataSource = mdtAttachement
                    dgv_Attchment.DataBind()
                    mblnAttachmentPopup = True
                    popup_ScanAttchment.Show()
                    'S.SANDEEP |18-JAN-2020| -- END
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgv_Attchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgv_Attchment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow = Nothing
                'mdtAttachement = CType(Me.ViewState("FieldAttachement"), DataTable)

                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                'If CInt(e.Item.Cells(3).Text) > 0 Then
                '    xrow = mdtAttachement.Select("scanattachtranunkid = " & CInt(e.Item.Cells(3).Text) & "")
                'Else
                '    xrow = mdtAttachement.Select("GUID = '" & e.Item.Cells(2).Text & "'")
                'End If
                'If e.CommandName = "Delete" Then
                '    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                '        popup_AttachementYesNo.Message = "Are you sure you want to delete this attachment?"
                '        mintDeleteIndex = mdtAttachement.Rows.IndexOf(xrow(0))
                '        'popup_ScanAttchment.Show()
                '        popup_AttachementYesNo.Show()
                '    End If
                'End If

                If e.CommandName = "Delete" Then
                    'If xrow IsNot Nothing AndAlso xrow.Length > 0 Then                        
                    'End If
                    popup_AttachementYesNo.Message = "Are you sure you want to delete this attachment?"
                    mintDeleteIndex = CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text)
                    popup_AttachementYesNo.Show()
                ElseIf e.CommandName = "Download" Then

                    Dim objScanAttachment As New clsScan_Attach_Documents
                    'S.SANDEEP |04-SEP-2021| -- START
                    'ISSUE : TAKING CARE FROM SLOWNESS QUERY
                    'objScanAttachment.GetList(Session("Document_Path"), "List", "", _
                    '                          CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "scanattachrefid = '" & _
                    '                          enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & _
                    '                          CInt(mintEmpUpdateTranunkid) & "' AND form_name = '" & _
                    '                          mstrModuleName_upd_percent & "' AND scanattachtranunkid = " & _
                    '                          CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text))

                    objScanAttachment.GetList(Session("Document_Path"), "List", "", _
                                              CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "scanattachrefid = '" & _
                                              enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & _
                                              CInt(mintEmpUpdateTranunkid) & "' AND form_name = '" & _
                                              mstrModuleName_upd_percent & "' AND scanattachtranunkid = " & _
                                              CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text), _
                                              CBool(IIf(CInt(cboEmployee.SelectedValue) <= 0, True, False)))
                    'S.SANDEEP |04-SEP-2021| -- END

                    xrow = objScanAttachment._Datatable.Select()
                    If xrow IsNot Nothing AndAlso xrow.Count > 0 Then
                        mdtAttachement = xrow.CopyToDataTable
                    Else
                        mdtAttachement = objScanAttachment._Datatable.Clone
                    End If

                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        Dim xPath As String = ""

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("orgfilepath").ToString
                        End If

                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                DisplayMessage.DisplayMessage("File does not Exist...", Me)
                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & xrow(0)("filename").ToString() & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
                'S.SANDEEP |16-MAY-2019| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvData_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvData.PreRender
        Try
            MergeRows(dgvData)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    ''S.SANDEEP |24-JUL-2019| -- START
    ''ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    'Protected Sub dgv_Accomplishment_data_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgv_AccomplishmentData.RowCommand
    '    'Protected Sub dgv_Accomplishment_data_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgv_Accomplishment_data.RowCommand
    '    'S.SANDEEP |24-JUL-2019| -- END

    '    Dim intUpdateTranUnkid As Integer = 0
    '    Try
    '        If e.CommandName = "Approved" Then
    '            Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
    '            intUpdateTranUnkid = CInt(dgv_AccomplishmentData.DataKeys(gridRow.RowIndex)("empupdatetranunkid")) 'mdtAccomplishmentData.Rows(gridRow.RowIndex)("empupdatetranunkid")
    '            If intUpdateTranUnkid <= 0 Then Exit Sub

    '            'S.SANDEEP |18-JAN-2020| -- START
    '            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    '            'If mdtAccomplishmentData Is Nothing Then Exit Sub

    '            'Dim drow() As DataRow = mdtAccomplishmentData.Select("empupdatetranunkid ='" & intUpdateTranUnkid & "'")
    '            'If drow.Length <= 0 Then Exit Sub

    '            'If drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
    '            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 6, "Sorry you can not approved this goal,reason goal already approved status"), Me)
    '            '    Exit Sub
    '            'End If

    '            'If drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
    '            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 7, "Sorry you can not approved this goal,reason goal already rejected status"), Me)
    '            '    Exit Sub
    '            'End If

    '            If CInt(dgv_AccomplishmentData.DataKeys(gridRow.RowIndex)("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 6, "Sorry you can not approved this goal,reason goal already approved status"), Me)
    '                Exit Sub
    '            End If

    '            If CInt(dgv_AccomplishmentData.DataKeys(gridRow.RowIndex)("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 7, "Sorry you can not approved this goal,reason goal already rejected status"), Me)
    '                Exit Sub
    '            End If
    '            'S.SANDEEP |18-JAN-2020| -- END

    '            hdfAccomplishment.Value = CStr(intUpdateTranUnkid) & ",A"

    '            lblAccomplishment_Message.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "are you sure appoved this goal accomplishment?")
    '            popup_Accomplishment_YesNo.Show()
    '        ElseIf e.CommandName = "Rejected" Then
    '            Dim gridRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
    '            'S.SANDEEP |18-JAN-2020| -- START
    '            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
    '            'intUpdateTranUnkid = mdtAccomplishmentData.Rows(gridRow.RowIndex)("empupdatetranunkid")
    '            'If intUpdateTranUnkid <= 0 Then Exit Sub

    '            'If mdtAccomplishmentData Is Nothing Then Exit Sub

    '            'Dim drow() As DataRow = mdtAccomplishmentData.Select("empupdatetranunkid ='" & intUpdateTranUnkid & "'")
    '            'If drow.Length <= 0 Then Exit Sub

    '            'If drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
    '            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 8, "Sorry you can not reject this goal,reason goal already approved status"), Me)
    '            '    Exit Sub
    '            'End If

    '            'If drow(0).Item("vuGoalAccomplishmentStatusId") = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
    '            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 9, "Sorry you can not reject this goal,reason goal already rejected status"), Me)
    '            '    Exit Sub
    '            'End If
    '            intUpdateTranUnkid = CInt(dgv_AccomplishmentData.DataKeys(gridRow.RowIndex)("empupdatetranunkid"))
    '            If intUpdateTranUnkid <= 0 Then Exit Sub

    '            If CInt(dgv_AccomplishmentData.DataKeys(gridRow.RowIndex)("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 8, "Sorry you can not reject this goal,reason goal already approved status"), Me)
    '                Exit Sub
    '            End If

    '            If CInt(dgv_AccomplishmentData.DataKeys(gridRow.RowIndex)("Accomplished_statusId")) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 9, "Sorry you can not reject this goal,reason goal already rejected status"), Me)
    '                Exit Sub
    '            End If
    '            'S.SANDEEP |18-JAN-2020| -- END

    '            hdfAccomplishment.Value = CStr(intUpdateTranUnkid) & ",R"

    '            lblAccomplishment_Message.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 3, "are you sure rejected this goal accomplishment?")
    '            popup_Accomplishment_YesNo.Show()
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    ''S.SANDEEP |24-JUL-2019| -- START
    ''ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    'Protected Sub dgv_AccomplishmentData_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv_AccomplishmentData.PreRender
    '    Try
    '        MergeRows(dgv_AccomplishmentData)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    ''Protected Sub dgv_Accomplishment_data_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgv_Accomplishment_data.RowDataBound
    ''    Try
    ''        If e.Row.RowType = DataControlRowType.DataRow Then
    ''            If mdtAccomplishmentData Is Nothing Then Exit Sub
    ''            Dim drow As DataRow = mdtAccomplishmentData.Rows(e.Row.RowIndex)
    ''            If CBool(drow.Item("IsGrp")) = True Then
    ''                CType(e.Row.Cells(2).FindControl("chkSelect"), CheckBox).Visible = False
    ''                'S.SANDEEP |24-JUL-2019| -- START
    ''                'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    ''                'CType(e.Row.Cells(5).FindControl("txtPerComp"), TextBox).Visible = False
    ''                CType(e.Row.Cells(6).FindControl("txtPerComp"), TextBox).Visible = False
    ''                'S.SANDEEP |24-JUL-2019| -- END
    ''                e.Row.Cells(2).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"

    ''                'S.SANDEEP |18-JUL-2019| -- START
    ''                'ISSUE/ENHANCEMENT : PROGRESS UPDATE COL. SPAN
    ''                'e.Row.Cells(3).CssClass = "MainGroupHeaderStyleLeftAlign"
    ''                'e.Row.Cells(4).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
    ''                'e.Row.Cells(5).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
    ''                'e.Row.Cells(6).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
    ''                'e.Row.Cells(7).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"

    ''                'S.SANDEEP |24-JUL-2019| -- START
    ''                'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    ''                'e.Row.Cells(3).CssClass = "MainGroupHeaderStyleLeftAlign" : e.Row.Cells(3).ColumnSpan = 5
    ''                'e.Row.Cells(4).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(4).Visible = False
    ''                'e.Row.Cells(5).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(5).Visible = False
    ''                'e.Row.Cells(6).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(6).Visible = False
    ''                'e.Row.Cells(7).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(7).Visible = False
    ''                'e.Row.Cells(8).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
    ''                'CType(e.Row.Cells(8).FindControl("lnkDownload"), LinkButton).Visible = False
    ''                'S.SANDEEP |18-JUL-2019| -- END
    ''                'e.Row.Cells(5).Text = ""
    ''                'e.Row.Cells(6).Text = ""

    ''                e.Row.Cells(3).CssClass = "MainGroupHeaderStyleLeftAlign" : e.Row.Cells(3).ColumnSpan = 7
    ''                e.Row.Cells(4).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(4).Visible = False
    ''                e.Row.Cells(5).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(5).Visible = False
    ''                e.Row.Cells(6).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(6).Visible = False
    ''                e.Row.Cells(7).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(7).Visible = False
    ''                e.Row.Cells(8).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(8).Visible = False
    ''                e.Row.Cells(9).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(9).Visible = False
    ''                e.Row.Cells(10).CssClass = "MainGroupHeaderStyleLeftAlign no-padding" : e.Row.Cells(10).Visible = False
    ''                e.Row.Cells(11).CssClass = "MainGroupHeaderStyleLeftAlign no-padding"
    ''                CType(e.Row.Cells(11).FindControl("lnkDownload"), LinkButton).Visible = False

    ''                e.Row.Cells(6).Text = ""
    ''                e.Row.Cells(7).Text = ""
    ''                'S.SANDEEP |24-JUL-2019| -- END
    ''            Else
    ''                If CInt(drow.Item("Accomplished_statusId")) <> clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending Then
    ''                    CType(e.Row.Cells(2).FindControl("chkSelect"), CheckBox).Visible = False
    ''                End If
    ''                'S.SANDEEP |24-JUL-2019| -- START
    ''                'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    ''                'e.Row.Cells(4).Text = drow.Item("Goal_status")
    ''                e.Row.Cells(5).Text = drow.Item("Goal_status")
    ''                'S.SANDEEP |24-JUL-2019| -- END
    ''            End If
    ''        End If
    ''    Catch ex As Exception
    ''        DisplayMessage.DisplayError(ex,Me)
    ''    End Try
    ''End Sub
    'Protected Sub dgv_Accomplishment_data_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgv_AccomplishmentData.RowDataBound
    '    Try
    '        If e.Row.RowType = DataControlRowType.DataRow Then
    '            'If mdtAccomplishmentData Is Nothing Then Exit Sub
    '            'Dim drow As DataRow = mdtAccomplishmentData.Rows(e.Row.RowIndex)
    '            'If CInt(drow.Item("Accomplished_statusId")) <> clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending Then
    '            '    CType(e.Row.Cells(0).FindControl("chkSelect"), CheckBox).Visible = False
    '            '    If e.Row.Cells(9).FindControl("txtPerComp") IsNot Nothing Then
    '            '        e.Row.Cells(9).Text = CType(e.Row.Cells(9).FindControl("txtPerComp"), TextBox).Text
    '            '    Else
    '            '        e.Row.Cells(9).Text = ""
    '            '    End If
    '            'End If
    '            'If IsDBNull(drow.Item("Goal_status")) = False Then
    '            '    e.Row.Cells(7).Text = drow.Item("Goal_status").ToString()
    '            'Else
    '            '    e.Row.Cells(7).Text = ""
    '            'End If

    '            If CInt(dgv_AccomplishmentData.DataKeys(e.Row.RowIndex)("Accomplished_statusId")) <> clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending Then
    '                CType(e.Row.Cells(0).FindControl("chkSelect"), CheckBox).Visible = False
    '                CType(e.Row.Cells(9).FindControl("txtPerComp"), TextBox).Enabled = False
    '            End If
    '            If IsDBNull(dgv_AccomplishmentData.DataKeys(e.Row.RowIndex)("Goal_status")) = False Then
    '                e.Row.Cells(7).Text = dgv_AccomplishmentData.DataKeys(e.Row.RowIndex)("Goal_status")
    '            Else
    '                e.Row.Cells(7).Text = ""
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'S.SANDEEP |24-JUL-2019| -- END


#End Region

#Region " ComboBox Event(S) "

    'S.SANDEEP |29-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    'Protected Sub cboPerspective_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPerspective.SelectedIndexChanged
    '    Try
    '        Call FillLinkedField(CInt(IIf(cboPerspective.SelectedValue = "", 0, cboPerspective.SelectedValue)))
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'S.SANDEEP |29-JUL-2019| -- END

    Protected Sub cboEmployee_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged, cboPeriod.SelectedIndexChanged
        Try
            'Pinkal (25-Jan-2022) -- Start
            'Enhancement NMB  - Language Change in PM Module.	
            'If CInt(cboEmployee.SelectedValue) > 0 AndAlso CInt(cboPeriod.SelectedValue) > 0 Then
            If CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) > 0 AndAlso CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) > 0 Then
                'Pinkal (25-Jan-2022) -- End
                If Session("SkipApprovalFlowInPlanning") = True Then
                    Dim intU_EmpId As Integer = 0
                    If (Session("LoginBy") = Global.User.en_loginby.User) Then
                        Dim objUserAddEdit As New clsUserAddEdit
                        objUserAddEdit._Userunkid = Session("UserId")
                        intU_EmpId = objUserAddEdit._EmployeeUnkid
                        objUserAddEdit = Nothing
                    End If

                    If intU_EmpId = CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) Then
                        DisplayMessage.DisplayMessage("You cannot access your own goals from MSS, Switch to ESS if you want to access your goals or Remove the Skip the Approval Setting", Me)
                        Exit Sub
                    End If
                End If
                Fill_Grid()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboChangeBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            'Dim iList As List(Of ListItem) = cboChangeBy.Items.Cast(Of ListItem)().ToList()

            Select Case cboChangeBy.SelectedIndex
                Case 0  'SELECT
                    txtNewPercentage.Enabled = False
                    txtNewValue.Enabled = False
                    txtTotalPercentage.Enabled = False
                    txtTotalValue.Enabled = False
                Case 1  'PERCENTAGE
                    txtNewPercentage.Enabled = True
                    txtNewValue.Enabled = False
                    txtTotalPercentage.Enabled = True
                    txtTotalValue.Enabled = False
                Case 2 'VALUE
                    txtNewPercentage.Enabled = False
                    txtNewValue.Enabled = True
                    txtTotalPercentage.Enabled = False
                    txtTotalValue.Enabled = True
            End Select

            'Select Case cboChangeBy.SelectedIndex
            '    Case 0
            '        txtChangeBy.Enabled = False
            '        objUpdateCaption1.Text = ""
            '        objUpdateCaption2.Text = ""
            '        'S.SANDEEP |18-JAN-2019| -- START
            '        objlblCaption3.Text = ""
            '        objlblCaption4.Text = ""
            '        'S.SANDEEP |18-JAN-2019| -- END
            '    Case 1
            '        txtChangeBy.Enabled = True
            '        'S.SANDEEP |18-JAN-2019| -- START
            '        'objUpdateCaption1.Text = cboChangeBy.SelectedItem.Text
            '        'objUpdateCaption2.Text = iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) + 1).Text
            '        objUpdateCaption1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 202, "Current") & " " & cboChangeBy.SelectedItem.Text
            '        objUpdateCaption2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 203, "Final") & " " & iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) + 1).Text
            '        objlblCaption3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 201, "Last") & " " & iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) + 1).Text
            '        objlblCaption4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 202, "Current") & " " & iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) + 1).Text
            '        'S.SANDEEP |18-JAN-2019| -- END
            '    Case 2
            '        txtChangeBy.Enabled = True
            '        'S.SANDEEP |18-JAN-2019| -- START
            '        'objUpdateCaption1.Text = cboChangeBy.SelectedItem.Text
            '        'objUpdateCaption2.Text = iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) - 1).Text
            '        objUpdateCaption1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 202, "Current") & " " & cboChangeBy.SelectedItem.Text
            '        objUpdateCaption2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 203, "Final") & " " & iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) - 1).Text
            '        objlblCaption3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 201, "Last") & " " & iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) - 1).Text
            '        objlblCaption4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 202, "Current") & " " & iList.Item(iList.IndexOf(cboChangeBy.SelectedItem) - 1).Text
            '        'S.SANDEEP |18-JAN-2019| -- END
            'End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " CheckBox Event(S) "

    'S.SANDEEP |24-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    'Protected Sub chkselectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim chkAll As CheckBox = CType(sender, CheckBox)
    '        For Each iRow As GridViewRow In dgv_AccomplishmentData.Rows
    '            Dim chk As CheckBox = CType(iRow.FindControl("chkSelect"), CheckBox)
    '            chk.Checked = chkAll.Checked
    '            If chk.Checked Then
    '                CType(iRow.Cells(9).FindControl("txtPerComp"), TextBox).ReadOnly = False
    '            Else
    '                Dim dtRow As DataRow = mdtAccomplishmentData.Rows(iRow.RowIndex)
    '                CType(iRow.Cells(9).FindControl("txtPerComp"), TextBox).Text = dtRow.Item("org_per_comp")
    '                CType(iRow.Cells(9).FindControl("txtPerComp"), TextBox).ReadOnly = True
    '            End If
    '            mdtAccomplishmentData.Rows(iRow.RowIndex)("ischeck") = chkAll.Checked
    '        Next
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub
    'S.SANDEEP |24-JUL-2019| -- END

    'Protected Sub chkselect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '       
    '        Dim chk As CheckBox = CType(sender, CheckBox)
    '        Dim row As GridViewRow = TryCast(chk.NamingContainer, GridViewRow)
    '        Dim dtRow As DataRow = mdtAccomplishmentData.Rows(row.RowIndex)

    '        'S.SANDEEP |24-JUL-2019| -- START
    '        'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '        'If chk.Checked Then
    '        '    CType(row.Cells(5).FindControl("txtPerComp"), TextBox).ReadOnly = False
    '        'Else
    '        '    CType(row.Cells(5).FindControl("txtPerComp"), TextBox).Text = dtRow.Item("org_per_comp")
    '        '    CType(row.Cells(5).FindControl("txtPerComp"), TextBox).ReadOnly = True
    '        'End If

    'If chk.Checked Then
    '            CType(row.Cells(9).FindControl("txtPerComp"), TextBox).ReadOnly = False
    'Else
    '            CType(row.Cells(9).FindControl("txtPerComp"), TextBox).Text = dtRow.Item("org_per_comp")
    '            CType(row.Cells(9).FindControl("txtPerComp"), TextBox).ReadOnly = True
    'End If
    '        'S.SANDEEP |24-JUL-2019| -- END

    '        'S.SANDEEP |30-MAR-2019| -- START
    '        mdtAccomplishmentData.Rows(row.RowIndex).Item("ischeck") = chk.Checked
    '        mdtAccomplishmentData.AcceptChanges()
    '        'S.SANDEEP |30-MAR-2019| -- END

    '        For Each dRow As DataRow In mdtAccomplishmentData.Select("IsGrp = TRUE")
    '            'S.SANDEEP |24-JUL-2019| -- START
    '            'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
    '            'CType(dgv_Accomplishment_data.Rows(mdtAccomplishmentData.Rows.IndexOf(dRow)).Cells(5).FindControl("txtPerComp"), TextBox).Visible = False
    '            CType(dgv_AccomplishmentData.Rows(mdtAccomplishmentData.Rows.IndexOf(dRow)).Cells(9).FindControl("txtPerComp"), TextBox).Visible = False
    '            'S.SANDEEP |24-JUL-2019| -- END
    '        Next
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex,Me)
    '    End Try
    'End Sub

#End Region

#Region " Textbox Event(s) "

    'Protected Sub txtAccomplishmentPerComp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim txt As TextBox = CType(sender, TextBox)
    '        Dim gvRow As GridViewRow = CType(txt.NamingContainer, GridViewRow)
    '        If gvRow.Style("background") = "red" Then
    '            gvRow.Style.Remove("background")
    '        End If

    '        Dim dblMaxValue As Double = CDec(dgv_AccomplishmentData.DataKeys(gvRow.RowIndex)("org_per_comp")) 'mdtAccomplishmentData.Rows(gvRow.RowIndex)("org_per_comp")
    '        If CDec(txt.Text) > dblMaxValue Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, 28, "Sorry, % completed cannot be greater than " & dblMaxValue & "."), Me)
    '            txt.Text = dblMaxValue
    '        End If
    '        If CDec(txt.Text) <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
    '            txt.Text = dblMaxValue
    '        End If
    '        'mdtAccomplishmentData.Rows(gvRow.RowIndex)("per_comp") = CDec(txt.Text)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

#End Region

#Region " Update Progress Popup "

    Private Sub UpdateProgressLoad(ByVal dgvRow As GridViewRow)
        Dim objFMapping As New clsAssess_Field_Mapping
        Dim iColName As String = ""
        Dim iExOrder As Integer = 0
        mintFieldUnkid = 0
        mintLinkedFld = 0
        mintFieldTypeId = 0
        mdtPeriodStartDate = Nothing
        mstrDefinedGoal = ""
        mintTableFieldTranUnkId = 0
        mintGoalTypeId = 0
        mdblGoalValue = 0
        mdblPercentage = 0
        'mdtGoalAccomplishment = Nothing
        'S.SANDEEP |29-JUL-2019| -- START
        'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
        mdecTotalProgress = 0
        'S.SANDEEP |29-JUL-2019| -- END
        Try
            Call Update_Clear_Controls()
            mintFieldUnkid = objFMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            If mintFieldUnkid <= 0 Then Exit Sub
            Dim objFMaster As New clsAssess_Field_Master
            iExOrder = objFMaster.Get_Field_ExOrder(mintFieldUnkid)

            txtUpdateGoalsCaption.Text = objFMapping.Get_Map_FieldName(CInt(cboPeriod.SelectedValue))

            'S.SANDEEP |18-JAN-2020| -- START
            'ISSUE/ENHANCEMENT : PA-OPTIMIZATION
            'Select Case iExOrder
            '    Case 1
            '        iColName = "empfield1unkid"
            '    Case 2
            '        iColName = "empfield2unkid"
            '    Case 3
            '        iColName = "empfield3unkid"
            '    Case 4
            '        iColName = "empfield4unkid"
            '    Case 5
            '        iColName = "empfield5unkid"
            'End Select
            'mintTableFieldTranUnkId = mdtFinal.Rows(dgvRow.RowIndex)(iColName)
            'mstrDefinedGoal = mdtFinal.Rows(dgvRow.RowIndex)("Field" & mintFieldUnkid.ToString)

            ''S.SANDEEP [01-OCT-2018] -- START
            ''ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            'mintGoalTypeId = mdtFinal.Rows(dgvRow.RowIndex)("goaltypeid")
            'mdblGoalValue = mdtFinal.Rows(dgvRow.RowIndex)("goalvalue")

            ''S.SANDEEP |12-FEB-2019| -- START
            ''ISSUE/ENHANCEMENT : {Performance Assessment Changes}
            ''If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
            ''    objlblGoalTypeInfo.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 202, "Goal Type : Qualitative | Value : 100")
            ''ElseIf mintGoalTypeId = enGoalType.GT_QUANTITATIVE Then
            ''    objlblGoalTypeInfo.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 203, "Goal Type : Quantitative | Value : " & CDbl(mdblGoalValue))
            ''End If
            'Dim mstrUoMTypeName As String = mdtFinal.Rows(dgvRow.RowIndex)(mstrUoMColName)

            ''S.SANDEEP |08-JUL-2019| -- START
            ''ISSUE/ENHANCEMENT : PA CHANGES
            ''If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
            ''    objlblGoalTypeInfo.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 202, "Goal Type : Qualitative | Value : 100") & " " & mstrUoMTypeName
            ''ElseIf mintGoalTypeId = enGoalType.GT_QUANTITATIVE Then
            ''    objlblGoalTypeInfo.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 203, "Goal Type : Quantitative | Value : ") & Format(CDbl(mdblGoalValue), Session("fmtCurrency")) & " " & mstrUoMTypeName
            ''End If
            'If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
            '    objlblGoalTypeInfo.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 843, "Qualitative") & " | " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 102, "Value") & " : 100 " & mstrUoMTypeName
            'ElseIf mintGoalTypeId = enGoalType.GT_QUANTITATIVE Then
            '    objlblGoalTypeInfo.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 844, "Quantitative") & " | " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 102, "Value") & " : " & Format(CDbl(mdblGoalValue), Session("fmtCurrency")) & " " & mstrUoMTypeName
            'End If
            ''S.SANDEEP |08-JUL-2019| -- END
            Select Case iExOrder
                Case 1
                    mintTableFieldTranUnkId = dgvData.DataKeys(dgvRow.RowIndex)("empfield1unkid")
                    mstrDefinedGoal = dgvData.DataKeys(dgvRow.RowIndex)("Field1")
                Case 2
                    mintTableFieldTranUnkId = dgvData.DataKeys(dgvRow.RowIndex)("empfield2unkid")
                    mstrDefinedGoal = dgvData.DataKeys(dgvRow.RowIndex)("Field2")
                Case 3
                    mintTableFieldTranUnkId = dgvData.DataKeys(dgvRow.RowIndex)("empfield3unkid")
                    mstrDefinedGoal = dgvData.DataKeys(dgvRow.RowIndex)("Field3")
                Case 4
                    mintTableFieldTranUnkId = dgvData.DataKeys(dgvRow.RowIndex)("empfield4unkid")
                    mstrDefinedGoal = dgvData.DataKeys(dgvRow.RowIndex)("Field4")
                Case 5
                    mintTableFieldTranUnkId = dgvData.DataKeys(dgvRow.RowIndex)("empfield5unkid")
                    mstrDefinedGoal = dgvData.DataKeys(dgvRow.RowIndex)("Field5")
            End Select
            mintGoalTypeId = dgvData.DataKeys(dgvRow.RowIndex)("goaltypeid")
            mdblGoalValue = dgvData.DataKeys(dgvRow.RowIndex)("goalvalue")

            If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
                objlblGoalTypeInfo.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 843, "Qualitative") & " | " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 102, "Value") & " : 100 " & dgvData.DataKeys(dgvRow.RowIndex)("UoMType")
            ElseIf mintGoalTypeId = enGoalType.GT_QUANTITATIVE Then
                objlblGoalTypeInfo.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsMasterData", 844, "Quantitative") & " | " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 102, "Value") & " : " & Format(CDbl(mdblGoalValue), Session("fmtCurrency")) & " " & dgvData.DataKeys(dgvRow.RowIndex)("UoMType")
            End If

            txtNewPercentage.Attributes.Add("lstval", 0) : txtNewValue.Attributes.Add("lstval", 0)
            txtTotalPercentage.Attributes.Add("lstval", 0) : txtTotalValue.Attributes.Add("lstval", 0)

            If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
                cboChangeBy.SelectedIndex = 1
                cboChangeBy.Enabled = False
                pnlbypct.Visible = True
                pnlbyval.Visible = False
                lblCaption3.Visible = True : txtNewPercentage.Visible = True
                lblCaption4.Visible = True : txtTotalPercentage.Visible = True
                lblCaption1.Visible = False : txtNewValue.Visible = False
                lblCaption2.Visible = False : txtTotalValue.Visible = False
            Else
                cboChangeBy.SelectedIndex = 2
                cboChangeBy.Enabled = False
                pnlbypct.Visible = False
                pnlbyval.Visible = True
                lblCaption3.Visible = False : txtNewPercentage.Visible = False
                lblCaption4.Visible = False : txtTotalPercentage.Visible = False
                lblCaption1.Visible = True : txtNewValue.Visible = True
                lblCaption2.Visible = True : txtTotalValue.Visible = True
            End If
            Call cboChangeBy_SelectedIndexChanged(New Object(), New EventArgs())

            txtUpdatePeriod.Text = cboPeriod.SelectedItem.Text
            txtUpdateGoals.Text = mstrDefinedGoal
            txtUpdateEmployeeName.Text = cboEmployee.SelectedItem.Text

            Dim strLinkedFld As String = objFMapping.Get_Map_FieldName(CInt(cboPeriod.SelectedValue))
            mintLinkedFld = objFMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))

            Dim mintExOrder As Integer = objFMaster.Get_Field_ExOrder(mintLinkedFld)

            Select Case mintExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD1
                Case enWeight_Types.WEIGHT_FIELD2
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD2
                Case enWeight_Types.WEIGHT_FIELD3
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD3
                Case enWeight_Types.WEIGHT_FIELD4
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD4
                Case enWeight_Types.WEIGHT_FIELD5
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD5
            End Select
            objFMapping = Nothing : objFMaster = Nothing

            Dim objEvaluation As New clsevaluation_analysis_master
            If objEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                dgvHistory.Columns(0).Visible = False
                dgvHistory.Columns(1).Visible = False
                dgvHistory.Columns(2).Visible = False
                btnUpdateSave.Enabled = False
            End If
            objEvaluation = Nothing
            Dim objPrd As New clscommom_period_Tran
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            mdtPeriodStartDate = objPrd._Start_Date
            objPrd = Nothing
            Call Fill_History()
            If CBool(Session("GoalsAccomplishedRequiresApproval")) = False Then
                dgvHistory.Columns(8).Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub UpdateFillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Try
            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboUpdateStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList.Dispose() : objMData = Nothing
        End Try
    End Sub

    Private Sub Fill_History()
        Dim objEUpdateProgress As New clsassess_empupdate_tran
        Try
            Call SetDateFormat()

            mdtGoalAccomplishment = objEUpdateProgress.GetList("List", CInt(cboEmployee.SelectedValue), CInt(cboPeriod.SelectedValue), mintTableFieldTranUnkId, Session("fmtCurrency")).Tables(0)


            'S.SANDEEP |23-DEC-2020| -- START
            'ISSUE/ENHANCEMENT : DON'T SHOW DISAPPROVED DATA BY DEFAULT {UPDATE PROGRESS}
            If chkIncludeDisapproved.Checked = False Then
                Dim dt As DataTable = New DataView(mdtGoalAccomplishment, "approvalstatusunkid <> " & CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Rejected), "", DataViewRowState.CurrentRows).ToTable()
                mdtGoalAccomplishment = dt
            End If
            'S.SANDEEP |23-DEC-2020| -- END

            mdtGoalAccomplishment.AsEnumerable().Select(Function(x) x.Field(Of Integer)("empupdatetranunkid")).ToList()


            'S.SANDEEP |24-APR-2020| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
            cboCalculationType.Enabled = True
            'S.SANDEEP |24-APR-2020| -- END

            Call SetTagValue(mdtGoalAccomplishment, False)

            If mdtGoalAccomplishment IsNot Nothing Then
                dgvHistory.DataSource = mdtGoalAccomplishment
                dgvHistory.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEUpdateProgress = Nothing
        End Try
    End Sub

    'Private Sub GetUpdateValue()
    '    Try
    '        'txtUpdatePercent.Text = objEUpdateProgress._Pct_Completed
    '        txtUpdateRemark.Text = objEUpdateProgress._Remark
    '        cboUpdateStatus.SelectedValue = objEUpdateProgress._Statusunkid
    '        dtpUpdateChangeDate.SetDate = objEUpdateProgress._Updatedate
    '        'S.SANDEEP [01-OCT-2018] -- START
    '        'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    '        cboChangeBy.SelectedIndex = objEUpdateProgress._ChangeById
    '        'Select Case objEUpdateProgress._ChangeById
    '        '    Case 1  'PERCENTAGE
    '        '        txtChangeBy.Text = objEUpdateProgress._Pct_Completed
    '        '        txtFinalValue.Text = objEUpdateProgress._FinalValue
    '        '    Case 2  'VALUE
    '        '        txtChangeBy.Text = objEUpdateProgress._FinalValue
    '        '        txtFinalValue.Text = objEUpdateProgress._Pct_Completed
    '        'End Select
    '        'If txtChangeBy.Enabled = False Then txtChangeBy.Enabled = True
    '        'If txtFinalValue.Enabled = False Then txtFinalValue.Enabled = True
    '        'S.SANDEEP [01-OCT-2018] -- END
    '        Select Case objEUpdateProgress._ChangeById
    '            Case 1  'PERCENTAGE
    '                txtTotalPercentage.Text = Format(objEUpdateProgress._FinalValue, Session("fmtCurrency")).ToString
    '                Call txtTotalPercentage_TextChanged(txtTotalPercentage, New EventArgs())
    '            Case 2  'VALUE
    '                'S.SANDEEP |13-JUL-2019| -- START
    '                'ISSUE/ENHANCEMENT : PA CHANGES
    '                'txtTotalValue.Text = Format(objEUpdateProgress._FinalValue, Session("fmtCurrency")).ToString
    '                txtNewValue.Text = Format(CDec(objEUpdateProgress._FinalValue), Session("fmtCurrency"))
    '                txtTotalValue.Text = Format(CDec(mdecTotalProgress) + objEUpdateProgress._FinalValue, Session("fmtCurrency")).ToString
    '                'S.SANDEEP |13-JUL-2019| -- END
    '                Call txtNewValue_TextChanged(txtNewValue, New EventArgs())
    '        End Select
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    'End Sub

    Private Sub SeUpdateValue(ByRef objEUpdateProgress As clsassess_empupdate_tran)
        Try
            objEUpdateProgress._Empupdatetranunkid = mintEmpUpdateTranunkid
            objEUpdateProgress._Empfieldtypeid = mintFieldTypeId
            objEUpdateProgress._Empfieldunkid = mintTableFieldTranUnkId
            objEUpdateProgress._Employeeunkid = CInt(cboEmployee.SelectedValue)
            objEUpdateProgress._Fieldunkid = mintFieldUnkid
            objEUpdateProgress._Isvoid = False
            'objEUpdateProgress._Pct_Completed = txtUpdatePercent.Text
            objEUpdateProgress._Periodunkid = CInt(cboPeriod.SelectedValue)
            objEUpdateProgress._Remark = txtUpdateRemark.Text
            objEUpdateProgress._Statusunkid = CInt(cboUpdateStatus.SelectedValue)
            objEUpdateProgress._Updatedate = dtpUpdateChangeDate.GetDate
            objEUpdateProgress._Userunkid = CInt(Session("UserId"))
            objEUpdateProgress._Voiddatetime = Nothing
            objEUpdateProgress._Voidreason = ""
            objEUpdateProgress._Voiduserunkid = -1
            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            If CBool(Session("GoalsAccomplishedRequiresApproval")) Then
                objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending)
            Else
                objEUpdateProgress._GoalAccomplishmentStatusid = CInt(clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Approved)
            End If
            objEUpdateProgress._GoalAccomplishmentRemark = ""
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objEUpdateProgress._ChangeById = cboChangeBy.SelectedIndex

            'S.SANDEEP |18-JAN-2019| -- START
            'If cboChangeBy.SelectedIndex = 1 Then
            '    objEUpdateProgress._Pct_Completed = txtChangeBy.Text
            '    objEUpdateProgress._FinalValue = txtFinalValue.Text
            'ElseIf cboChangeBy.SelectedIndex = 2 Then
            '    objEUpdateProgress._Pct_Completed = txtFinalValue.Text
            '    objEUpdateProgress._FinalValue = txtChangeBy.Text
            'End If
            'If cboChangeBy.SelectedIndex = 1 Then
            '    objEUpdateProgress._Pct_Completed = txtFinalValue.Text
            '    objEUpdateProgress._FinalValue = txtFinalValue.Text
            'ElseIf cboChangeBy.SelectedIndex = 2 Then
            '    objEUpdateProgress._Pct_Completed = txtFinalValue.Text
            '    objEUpdateProgress._FinalValue = txtChangeBy.Text
            'End If
            'S.SANDEEP |18-JAN-2019| -- END

            'S.SANDEEP [01-OCT-2018] -- END

            If cboChangeBy.SelectedIndex = 1 Then
                objEUpdateProgress._Pct_Completed = CDec(mdblPercentage)
                'S.SANDEEP |13-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : PA CHANGES
                'objEUpdateProgress._FinalValue = txtTotalPercentage.Text
                objEUpdateProgress._FinalValue = txtNewPercentage.Text
                'S.SANDEEP |13-JUL-2019| -- END
            ElseIf cboChangeBy.SelectedIndex = 2 Then
                objEUpdateProgress._Pct_Completed = CDec(mdblPercentage)
                'S.SANDEEP |13-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : PA CHANGES
                'objEUpdateProgress._FinalValue = txtTotalValue.Text
                objEUpdateProgress._FinalValue = txtNewValue.Text
                'S.SANDEEP |13-JUL-2019| -- END
            End If

            'S.SANDEEP |24-APR-2020| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
            objEUpdateProgress._CalcModeId = CInt(cboCalculationType.SelectedValue)
            'S.SANDEEP |24-APR-2020| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Update_Clear_Controls()
        Try
            dtpUpdateChangeDate.SetDate = Now.Date
            mintEmpUpdateTranunkid = 0
            txtUpdateRemark.Text = ""
            'cboChangeBy.SelectedIndex = 0
            'Call cboChangeBy_SelectedIndexChanged(cboChangeBy, New EventArgs())
            txtNewPercentage.Text = ""
            txtNewValue.Text = ""
            txtTotalPercentage.Text = ""
            txtTotalValue.Text = ""
            'S.SANDEEP |08-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : PA CHANGES
            cboUpdateStatus.SelectedValue = 0
            'S.SANDEEP |08-JUL-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Private Function UpdateIsValidInfo() As Boolean
    '    Try
    '        If dtpUpdateChangeDate.IsNull = True Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 1, "Sorry, Date is mandatory information. Please set date to continue."), Me)
    '            dtpUpdateChangeDate.Focus()
    '            Return False
    '        End If

    '        If dtpUpdateChangeDate.GetDate.Date < mdtPeriodStartDate.Date Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 2, "Sorry, Date cannot be less than period start date."), Me)
    '            dtpUpdateChangeDate.Focus()
    '            Return False
    '        End If

    '        If dtpUpdateChangeDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 102, "Sorry, Date cannot be greater than today date."), Me)
    '            dtpUpdateChangeDate.Focus()
    '            Return False
    '        End If

    '        If CInt(cboUpdateStatus.SelectedValue) <= 0 Then
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 3, "Sorry, Change Status is mandatory information. Please select change status to continue."), Me)
    '            cboUpdateStatus.Focus()
    '            Return False
    '        End If

    '        'Shani (05-Sep-2016) -- Start
    '        'If CDec(txtUpdatePercent.Text) <= 0 Then
    '        '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
    '        '    txtUpdatePercent.Focus()
    '        '    Return False
    '        'End If
    '        'Shani (05-Sep-2016) -- End

    '        'If CDec(txtUpdatePercent.Text) > 100 Then
    '        '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Sorry, Percentage Completed cannot be greater than 100."), Me)
    '        '    txtUpdatePercent.Focus()
    '        '    Return False
    '        'End If

    '        'S.SANDEEP [01-OCT-2018] -- START
    '        'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    '        'If cboChangeBy.Enabled = True Then
    '        '    If cboChangeBy.SelectedIndex = 0 Then
    '        '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 200, "Sorry, change by is mandatory information. Please select change by to continue."), Me)
    '        '        cboChangeBy.Focus()
    '        '        Return False
    '        '    ElseIf cboChangeBy.SelectedIndex = 1 AndAlso txtChangeBy.Text <= 0 Then
    '        '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 200, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
    '        '        txtChangeBy.Focus()
    '        '        Return False
    '        '    ElseIf cboChangeBy.SelectedIndex = 2 AndAlso txtFinalValue.Text <= 0 Then
    '        '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 200, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
    '        '        txtChangeBy.Focus()
    '        '        Return False
    '        '    End If
    '        'End If
    '        'If txtChangeBy.Text <= 0 Then
    '        '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 200, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
    '        '    txtChangeBy.Focus()
    '        '    Return False
    '        'End If
    '        'Dim dblMaxValue As Double = 0
    '        'If txtChangeBy.HasAttributes() Then
    '        '    Try
    '        '        dblMaxValue = txtChangeBy.Attributes(CInt(enGoalType.GT_QUALITATIVE).ToString)
    '        '        If dblMaxValue > 0 AndAlso (CDec(txtChangeBy.Text) > dblMaxValue) Then
    '        '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 8, "Sorry, Percentage Completed cannot be greater than 100."), Me)
    '        '            txtChangeBy.Focus()
    '        '    Return False
    '        'End If
    '        '    Catch ex As Exception

    '        '    End Try
    '        'End If

    '        If CInt(cboChangeBy.SelectedIndex) = 1 Then
    '            'S.SANDEEP |22-MAR-2019| -- START
    '            'ISSUE/ENHANCEMENT : ISSUE ON PA (TC0016)
    '            'Dim dblMaxValue As Double = 0
    '            'dblMaxValue = 100
    '            'If dblMaxValue > 0 AndAlso (CDec(txtNewPercentage.Text) > dblMaxValue) Then
    '            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 8, "Sorry, Percentage Completed cannot be greater than 100."), Me)
    '            '    txtNewPercentage.Focus()
    '            '    Return False
    '            'End If
    '            'If dblMaxValue > 0 AndAlso (CDec(txtTotalPercentage.Text) > dblMaxValue) Then
    '            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 8, "Sorry, Percentage Completed cannot be greater than 100."), Me)
    '            '    txtNewPercentage.Focus()
    '            '    Return False
    '            'End If
    '            If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
    '                Dim dblMaxValue As Double = 0
    '                dblMaxValue = 100
    '                If dblMaxValue > 0 AndAlso (CDec(txtNewPercentage.Text) > dblMaxValue) Then
    '                    'S.SANDEEP |13-JUL-2019| -- START
    '                    'ISSUE/ENHANCEMENT : PA CHANGES
    '                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 8, "Sorry, Percentage Completed cannot be greater than 100."), Me)
    '                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 28, "Sorry, Percentage Completed cannot be greater than 100."), Me)
    '                    'S.SANDEEP |13-JUL-2019| -- END
    '                    txtNewPercentage.Focus()
    '                    Return False
    '                End If
    '                If dblMaxValue > 0 AndAlso (CDec(txtTotalPercentage.Text) > dblMaxValue) Then
    '                    'S.SANDEEP |13-JUL-2019| -- START
    '                    'ISSUE/ENHANCEMENT : PA CHANGES
    '                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 8, "Sorry, Percentage Completed cannot be greater than 100."), Me)
    '                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 28, "Sorry, Percentage Completed cannot be greater than 100."), Me)
    '                    'S.SANDEEP |13-JUL-2019| -- END
    '                    txtNewPercentage.Focus()
    '                    Return False
    '                End If
    '            End If
    '            'S.SANDEEP |22-MAR-2019| -- END
    '        End If

    '        'If cboChangeBy.Enabled = True Then
    '        If cboChangeBy.SelectedIndex = 0 Then
    '            'Sohail (23 Mar 2019) -- Start
    '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '            'eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 200, "Sorry, change by is mandatory information. Please select change by to continue."), enMsgBoxStyle.Information)
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 200, "Sorry, change by is mandatory information. Please select change by to continue."), Me)
    '            'Sohail (23 Mar 2019) -- End
    '            cboChangeBy.Focus()
    '            Return False
    '        ElseIf cboChangeBy.SelectedIndex = 1 AndAlso txtTotalPercentage.Text < 0 Then
    '            'Sohail (23 Mar 2019) -- Start
    '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '            'eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 201, "Sorry, % completed is mandatory information. Please enter % completed to continue."), enMsgBoxStyle.Information)
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 201, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
    '            'Sohail (23 Mar 2019) -- End
    '            txtNewValue.Focus()
    '            Return False
    '        ElseIf cboChangeBy.SelectedIndex = 2 AndAlso txtTotalValue.Text.Trim().Length <= 0 Then
    '            'Sohail (23 Mar 2019) -- Start
    '            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '            'eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 202, "Sorry, total value is mandatory information. Please total value to continue."), enMsgBoxStyle.Information)
    '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 202, "Sorry, total value is mandatory information. Please total value to continue."), Me)
    '            'Sohail (23 Mar 2019) -- End
    '            txtNewValue.Focus()
    '            Return False
    '        End If
    '        'End If
    '        'S.SANDEEP [01-OCT-2018] -- END

    '        'Shani (26-Sep-2016) -- Start
    '        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '        If mintEmpUpdateTranunkid <= 0 Then 'INSERT
    '            If CBool(Session("GoalsAccomplishedRequiresApproval")) Then
    '                'S.SANDEEP [29-NOV-2017] -- START
    '                'ISSUE/ENHANCEMENT : REF-ID # 136
    '                'Dim drow = dgvHistory.Items.Cast(Of DataGridItem).Where(Function(x) CInt(x.Cells(8).Text) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending)
    '                Dim drow = dgvHistory.Items.Cast(Of DataGridItem).Where(Function(x) CInt(x.Cells(9).Text) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending)
    '                'S.SANDEEP [29-NOV-2017] -- END
    '                If drow.Count > 0 Then
    '                    'S.SANDEEP [15-Feb-2018] -- START
    '                    'ISSUE/ENHANCEMENT : {#32}
    '                    'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, there goals are can't be inserted reason: privous updated progress record is pedding "), Me)
    '                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 9, "Sorry, there goals are can't be inserted reason: privous updated progress record is pedding "), Me)
    '                    'S.SANDEEP [15-Feb-2018] -- END
    '                    Return False
    '                End If
    '            End If
    '        End If
    '        'Shani (26-Sep-2016) -- End


    '        'S.SANDEEP |30-JAN-2019| -- START
    '        ' REMOVED : mstrModuleName
    '        ' ADDED : mstrModuleName_upd_percent
    '        'S.SANDEEP |30-JAN-2019| -- END

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    Finally
    '    End Try
    '    Return True
    'End Function

    Private Function UpdateIsValidInfo() As Boolean
        Try
            If dtpUpdateChangeDate.IsNull = True Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 1, "Sorry, Date is mandatory information. Please set date to continue."), Me)
                dtpUpdateChangeDate.Focus()
                Return False
            End If

            If dtpUpdateChangeDate.GetDate.Date < mdtPeriodStartDate.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 2, "Sorry, Date cannot be less than period start date."), Me)
                dtpUpdateChangeDate.Focus()
                Return False
            End If

            If dtpUpdateChangeDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 102, "Sorry, Date cannot be greater than today date."), Me)
                dtpUpdateChangeDate.Focus()
                Return False
            End If

            If CInt(cboUpdateStatus.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 3, "Sorry, Change Status is mandatory information. Please select change status to continue."), Me)
                cboUpdateStatus.Focus()
                Return False
            End If

            If CInt(cboChangeBy.SelectedIndex) = 1 Then
                If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
                    Dim dblMaxValue As Double = 0
                    dblMaxValue = 100
                    If dblMaxValue > 0 AndAlso (CDec(txtNewPercentage.Text) > dblMaxValue) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 28, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                        txtNewPercentage.Focus()
                        Return False
                    End If
                    If dblMaxValue > 0 AndAlso (CDec(txtTotalPercentage.Text) > dblMaxValue) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 28, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                        txtNewPercentage.Focus()
                        Return False
                    End If
                End If
            End If

                If cboChangeBy.SelectedIndex = 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 200, "Sorry, change by is mandatory information. Please select change by to continue."), Me)
                    cboChangeBy.Focus()
                    Return False
            ElseIf cboChangeBy.SelectedIndex = 1 AndAlso txtTotalPercentage.Text < 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 201, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
                txtNewPercentage.Focus()
                Return False
            ElseIf cboChangeBy.SelectedIndex = 2 AndAlso txtTotalValue.Text.Trim().Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 202, "Sorry, total value is mandatory information. Please total value to continue."), Me)
                    txtNewValue.Focus()
                    Return False
            ElseIf cboChangeBy.SelectedIndex = 2 AndAlso txtTotalValue.Text.Trim().Length > 0 Then
                If CBool(Session("ConsiderZeroAsNumUpdateProgress")) = False Then
                    If txtTotalValue.Text.Trim().Length > 0 AndAlso CDec(txtTotalValue.Text) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 202, "Sorry, total value is mandatory information. Please total value to continue."), Me)
                    txtNewValue.Focus()
                    Return False
                End If
            End If
            End If

            If mintEmpUpdateTranunkid <= 0 Then 'INSERT
                If CBool(Session("GoalsAccomplishedRequiresApproval")) Then
                    Dim drow = dgvHistory.Items.Cast(Of DataGridItem).Where(Function(x) CInt(x.Cells(9).Text) = clsassess_empupdate_tran.enGoalAccomplished_Status.GA_Pending)
                    If drow.Count > 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 9, "Sorry, there goals are can't be inserted reason: privous updated progress record is pedding "), Me)
                        Return False
                    End If
                End If
            End If
            
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
        Return True
    End Function

    Private Sub Save_UpdateProgress()
        Dim blnFlag As Boolean = False
        Dim objEUpdateProgress As New clsassess_empupdate_tran
        Try
            Call SeUpdateValue(objEUpdateProgress)

            If mintEmpUpdateTranunkid <= 0 Then 'INSERT
                blnFlag = objEUpdateProgress.Insert()
            ElseIf mintEmpUpdateTranunkid > 0 Then  'UPDATE
                blnFlag = objEUpdateProgress.Update()
            End If

            If blnFlag = True Then
                If CBool(Session("GoalsAccomplishedRequiresApproval")) Then
                    If mintEmpUpdateTranunkid <= 0 Then 'INSERT
                        objEUpdateProgress._WebFrmName = mstrModuleName_upd_percent
                        Dim iLoginType As enLogin_Mode = enLogin_Mode.MGR_SELF_SERVICE
                        Dim intEmpUnkid As Integer = 0

                        If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                            iLoginType = enLogin_Mode.EMP_SELF_SERVICE
                            intEmpUnkid = CInt(Session("Employeeunkid"))
                        End If
                        'S.SANDEEP |05-MAR-2019| -- START
                        'ISSUE/ENHANCEMENT : {Performance Assessment Changes}
                        'objEUpdateProgress.Send_Notification_Assessor(CInt(cboEmployee.SelectedValue), _
                        '                                              CInt(cboPeriod.SelectedValue), _
                        '                                              Session("Fin_year"), _
                        '                                              Session("FinancialYear_Name"), _
                        '                                              CStr(Session("Database_Name")), _
                        '                                              objEUpdateProgress._Empupdatetranunkid, _
                        '                                              CInt(Session("CompanyUnkId")), _
                        '                                              Session("ArutiSelfServiceURL"), _
                        '                                              iLoginType, intEmpUnkid, Session("UserId"))
                        'S.SANDEEP |05-MAR-2019| -- END
                    End If
                End If
                Call Update_Clear_Controls()
                Call Fill_History()
            Else
                If objEUpdateProgress._Message <> "" Then
                    DisplayMessage.DisplayMessage(objEUpdateProgress._Message, Me)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            Throw ex
        Finally
            objEUpdateProgress = Nothing
        End Try
    End Sub

    Private Sub txtTotalPercentage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotalPercentage.TextChanged
        Try
            RemoveHandler txtNewPercentage.TextChanged, AddressOf txtNewPercentage_Pct_TextChanged

            'S.SANDEEP |22-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : ISSUE ON PA (TC0007)
            If txtTotalPercentage.Text.Trim.Length <= 0 Then Exit Sub
            'S.SANDEEP |22-MAR-2019| -- END

            'S.SANDEEP |25-MAR-2019| -- START
            Dim mdecVal As Decimal = 0
            Decimal.TryParse(txtTotalPercentage.Text, mdecVal)
            'S.SANDEEP |25-MAR-2019| -- END

            If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
                If CDbl(mdecVal) > CDbl(100) Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), enMsgBoxStyle.Information)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), Me)
                    'Sohail (23 Mar 2019) -- End

                    Exit Sub
                End If
            End If
            'S.SANDEEP |13-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : PA CHANGES
            'txtNewPercentage.Text = Math.Abs(mdecVal) - CDec(txtNewPercentage.Attributes("lstval"))
            'mdblPercentage = txtTotalPercentage.Text

            txtNewPercentage.Text = Format(CDec(Math.Abs(mdecVal) - CDec(txtNewPercentage.Attributes("lstval"))), Session("fmtCurrency"))
            mdblPercentage = Format(CDec(txtTotalPercentage.Text), Session("fmtCurrency"))
            'S.SANDEEP |13-JUL-2019| -- END
            AddHandler txtNewPercentage.TextChanged, AddressOf txtNewPercentage_Pct_TextChanged
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "txtTotalPercentage_TextChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub txtNewPercentage_Pct_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewPercentage.TextChanged
        Try
            RemoveHandler txtTotalPercentage.TextChanged, AddressOf txtTotalPercentage_TextChanged
            'S.SANDEEP |22-MAR-2019| -- START
            'ISSUE/ENHANCEMENT : ISSUE ON PA (TC0007)
            If txtNewPercentage.Text.Trim.Length <= 0 Then Exit Sub
            'S.SANDEEP |22-MAR-2019| -- END

            'S.SANDEEP |25-MAR-2019| -- START
            Dim mdecVal As Decimal = 0
            Decimal.TryParse(txtNewPercentage.Text, mdecVal)
            'S.SANDEEP |25-MAR-2019| -- END

            If mintGoalTypeId = enGoalType.GT_QUALITATIVE Then
                If CDbl(mdecVal) > CDbl(100) Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), enMsgBoxStyle.Information)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Sorry, % completed cannot be greater than 100. Please enter proper % completed to continue."), Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
            End If
            'S.SANDEEP |13-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : PA CHANGES
            'txtTotalPercentage.Text = mdecVal + CDec(txtNewPercentage.Attributes("lstval"))
            'mdblPercentage = txtTotalPercentage.Text

            txtTotalPercentage.Text = Format(CDec(mdecVal + CDec(txtNewPercentage.Attributes("lstval"))), Session("fmtCurrency"))
            mdblPercentage = Format(CDec(txtTotalPercentage.Text), Session("fmtCurrency"))
            'S.SANDEEP |13-JUL-2019| -- END
            AddHandler txtTotalPercentage.TextChanged, AddressOf txtTotalPercentage_TextChanged
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "txtNewPercentage_Pct_TextChanged", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    'Private Sub txtTotalValue_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotalValue.TextChanged
    '    Try
    '        RemoveHandler txtNewValue.TextChanged, AddressOf txtNewValue_TextChanged
    '        'S.SANDEEP |22-MAR-2019| -- START
    '        'ISSUE/ENHANCEMENT : ISSUE ON PA (TC0007)
    '        If txtTotalValue.Text.Trim.Length <= 0 Then Exit Sub
    '        'S.SANDEEP |22-MAR-2019| -- END

    '        'S.SANDEEP |25-MAR-2019| -- START
    '        Dim mdecVal As Decimal = 0
    '        Decimal.TryParse(txtTotalValue.Text, mdecVal)
    '        'S.SANDEEP |25-MAR-2019| -- END

    '        'S.SANDEEP |13-JUL-2019| -- START
    '        'ISSUE/ENHANCEMENT : PA CHANGES
    '        'txtNewValue.Text = Math.Abs(mdecVal) - CDec(txtNewValue.Attributes("lstval"))
    '        'mdblPercentage = CDbl((mdecVal * 100) / CDec(IIf(mdblGoalValue <= 0, 1, mdblGoalValue)))

    '        txtNewValue.Text = Format(CDec(Math.Abs(mdecVal) - CDec(txtNewValue.Attributes("lstval"))), Session("fmtCurrency"))

    '        'S.SANDEEP |24-APR-2020| -- START
    '        'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
    '        'mdblPercentage = Format(CDbl((mdecVal * 100) / CDec(IIf(mdblGoalValue <= 0, 1, mdblGoalValue))), Session("fmtCurrency"))
    '        If CInt(cboCalculationType.SelectedValue) = clsassess_empupdate_tran.enCalcMode.Increasing Then
    '            mdblPercentage = Format(CDbl((mdecVal * 100) / CDec(IIf(mdblGoalValue <= 0, 1, mdblGoalValue))), Session("fmtCurrency"))
    '        Else
    '            mdblPercentage = Format(CDbl((mdblGoalValue * 100) / CDec(IIf(txtTotalValue.Text <= 0, 1, txtTotalValue.Text))), Session("fmtCurrency"))
    '        End If
    '        'S.SANDEEP |24-APR-2020| -- END

    '        'S.SANDEEP |13-JUL-2019| -- END

    '        AddHandler txtNewValue.TextChanged, AddressOf txtNewValue_TextChanged
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayError.Show("-1", ex.Message, "txtTotalValue_TextChanged", mstrModuleName)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    Finally
    '    End Try
    'End Sub

    Private Sub txtTotalValue_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTotalValue.TextChanged
        Try
            RemoveHandler txtNewValue.TextChanged, AddressOf txtNewValue_TextChanged
            If txtTotalValue.Text.Trim.Length <= 0 Then Exit Sub

            Dim mdecVal As Decimal = 0
            Decimal.TryParse(txtTotalValue.Text, mdecVal)

            Dim IsZeroAsNo As Boolean = CBool(IIf(Session("ConsiderZeroAsNumUpdateProgress") Is Nothing, False, Session("ConsiderZeroAsNumUpdateProgress")))

            If IsZeroAsNo Then
                txtNewValue.Text = Format(CDec(Math.Abs(mdecVal)), Session("fmtCurrency"))
            Else
            txtNewValue.Text = Format(CDec(Math.Abs(mdecVal) - CDec(txtNewValue.Attributes("lstval"))), Session("fmtCurrency"))
            End If

            If CInt(cboCalculationType.SelectedValue) = clsassess_empupdate_tran.enCalcMode.Increasing Then
                mdblPercentage = Format(CDbl((mdecVal * 100) / CDec(IIf(mdblGoalValue <= 0, 1, mdblGoalValue))), Session("fmtCurrency"))
            Else
                If IsZeroAsNo Then
                    mdecTotalProgress = CDec(txtNewValue.Text) + CDec(txtNewValue.Attributes("lstval"))
                    mdblPercentage = Format(CDbl((mdblGoalValue * 100) / CDec(IIf(Math.Abs(mdecTotalProgress) <= 0, mdblGoalValue, mdecTotalProgress))), Session("fmtCurrency"))
                Else
                mdblPercentage = Format(CDbl((mdblGoalValue * 100) / CDec(IIf(txtTotalValue.Text <= 0, 1, txtTotalValue.Text))), Session("fmtCurrency"))
            End If
            End If

            AddHandler txtNewValue.TextChanged, AddressOf txtNewValue_TextChanged
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Private Sub txtNewValue_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewValue.TextChanged
    '    Try
    '        RemoveHandler txtTotalValue.TextChanged, AddressOf txtTotalValue_TextChanged
    '        'S.SANDEEP |22-MAR-2019| -- START
    '        'ISSUE/ENHANCEMENT : ISSUE ON PA (TC0007)
    '        If txtNewValue.Text.Trim.Length <= 0 Then Exit Sub
    '        'S.SANDEEP |22-MAR-2019| -- END

    '        'S.SANDEEP |25-MAR-2019| -- START
    '        Dim mdecVal As Decimal = 0
    '        Decimal.TryParse(txtNewValue.Text, mdecVal)
    '        'S.SANDEEP |25-MAR-2019| -- END


    '        'S.SANDEEP |13-JUL-2019| -- START
    '        'ISSUE/ENHANCEMENT : PA CHANGES
    '        'txtTotalValue.Text = mdecVal + CDec(txtNewValue.Attributes("lstval"))
    '        'mdblPercentage = CDbl((mdecVal * 100) / CDec(IIf(mdblGoalValue <= 0, 1, mdblGoalValue)))
    '        'txtTotalValue.Text = Format(CDec(mdecVal + CDec(mdecTotalProgress)), Session("fmtCurrency"))
    '        'S.SANDEEP |24-APR-2020| -- START
    '        'ISSUE/ENHANCEMENT : UPDATE PROGRESS IN DECREASING ORDER 
    '        'mdblPercentage = Format(CDbl(((mdecVal + CDec(mdecTotalProgress)) * 100) / CDec(IIf(mdblGoalValue <= 0, 1, mdblGoalValue))), Session("fmtCurrency"))
    '        If CInt(cboCalculationType.SelectedValue) = clsassess_empupdate_tran.enCalcMode.Increasing Then
    '            mdblPercentage = Format(CDbl(((mdecVal + CDec(mdecTotalProgress)) * 100) / CDec(IIf(mdblGoalValue <= 0, 1, mdblGoalValue))), Session("fmtCurrency"))
    '        Else
    '            If mdecVal + CDec(mdecTotalProgress) <= 0 Then
    '                mdblPercentage = Format(CDbl(0), Session("fmtCurrency"))
    '            Else
    '                mdecVal = Math.Abs(mdecVal + CDec(mdecTotalProgress))
    '                mdblPercentage = Format(CDbl((mdblGoalValue * 100) / CDec(IIf(mdecVal <= 0, 1, mdecVal))), Session("fmtCurrency"))
    '            End If
    '        End If
    '        'S.SANDEEP |24-APR-2020| -- END
    '        txtTotalValue.Text = Format(CDec(mdecTotalProgress), Session("fmtCurrency"))

    '        'S.SANDEEP |13-JUL-2019| -- END

    '        AddHandler txtTotalValue.TextChanged, AddressOf txtTotalValue_TextChanged
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayError.Show("-1", ex.Message, "txtNewValue_TextChanged", mstrModuleName)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    Finally
    '    End Try
    'End Sub

    Private Sub txtNewValue_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNewValue.TextChanged
        Try
            RemoveHandler txtTotalValue.TextChanged, AddressOf txtTotalValue_TextChanged

            If txtNewValue.Text.Trim.Length <= 0 Then Exit Sub

            Dim mdecVal As Decimal = 0
            Decimal.TryParse(txtNewValue.Text, mdecVal)

            If CInt(cboCalculationType.SelectedValue) = clsassess_empupdate_tran.enCalcMode.Increasing Then
                mdblPercentage = Format(CDbl(((mdecVal + CDec(mdecTotalProgress)) * 100) / CDec(IIf(mdblGoalValue <= 0, 1, mdblGoalValue))), Session("fmtCurrency"))
            Else
                Dim IsZeroAsNo As Boolean = CBool(IIf(Session("ConsiderZeroAsNumUpdateProgress") Is Nothing, False, Session("ConsiderZeroAsNumUpdateProgress")))
                If IsZeroAsNo Then
                    If mdecTotalProgress <= 0 Then
                        mdecTotalProgress = mdblGoalValue - mdecVal
                    Else
                        mdecTotalProgress = (mdblGoalValue - (mdecVal + mdecTotalProgress))
                    End If
                    mdblPercentage = Format(CDbl((mdblGoalValue * 100) / CDec(IIf(Math.Abs(mdecTotalProgress) <= 0, mdblGoalValue, mdecTotalProgress))), Session("fmtCurrency"))
                Else
                    If mdecVal + CDec(mdecTotalProgress) = 0 Then
                    mdblPercentage = Format(CDbl(0), Session("fmtCurrency"))
                Else
                    mdecVal = Math.Abs(mdecVal + CDec(mdecTotalProgress))
                    mdblPercentage = Format(CDbl((mdblGoalValue * 100) / CDec(IIf(mdecVal <= 0, 1, mdecVal))), Session("fmtCurrency"))
                End If
            End If
            End If
            txtTotalValue.Text = Format(CDec(mdecTotalProgress), Session("fmtCurrency"))
            AddHandler txtTotalValue.TextChanged, AddressOf txtTotalValue_TextChanged
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboCalculationType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCalculationType.SelectedIndexChanged
        Try
            txtNewPercentage.Text = ""
            txtNewValue.Text = ""
            txtTotalPercentage.Text = ""
            txtTotalValue.Text = ""
            mdblPercentage = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'S.SANDEEP |23-DEC-2020| -- START
    'ISSUE/ENHANCEMENT : DON'T SHOW DISAPPROVED DATA BY DEFAULT {UPDATE PROGRESS}
    Protected Sub chkIncludeDisapproved_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeDisapproved.CheckedChanged
        Try
            Call Fill_History()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |23-DEC-2020| -- END

#End Region


    'S.SANDEEP |18-JUL-2019| -- START
    'ISSUE/ENHANCEMENT : PROGRESS UPDATE DOWNLOAD DOCUMENT
    'Protected Sub lnkdownloadAttachment_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Try
    '        Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
    '        Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
    '        Dim objScanAttachment As New clsScan_Attach_Documents
    '        objScanAttachment.GetList(Session("Document_Path"), "List", "", CInt(cboEmployee.SelectedValue), False, Nothing, Nothing, "scanattachrefid = '" & enScanAttactRefId.ASSESSMENT & "' AND transactionunkid = '" & CInt(dgv_AccomplishmentData.DataKeys(row.RowIndex)("empupdatetranunkid")) & "' AND form_name = '" & mstrModuleName_upd_percent & "'")
    '        Dim dtTranTable As DataTable = Nothing
    '        Dim xRow As DataRow() = objScanAttachment._Datatable.Select()
    '        If xRow.Length <= 0 Then
    '            DisplayMessage.DisplayMessage("Sorry, No document attached with particular update progress.", Me)
    '            Exit Sub
    '        End If
    '        If xRow.Length > 0 Then
    '            dtTranTable = xRow.CopyToDataTable()
    '        Else
    '            dtTranTable = objScanAttachment._Datatable.Clone
    '        End If
    '        Dim xPath As String = ""
    '        Dim fileToAdd As New List(Of String)
    '        If dtTranTable.Columns.Contains("temppath") = False Then dtTranTable.Columns.Add("temppath", Type.GetType("System.String"))
    '        If dtTranTable.Columns.Contains("error") = False Then dtTranTable.Columns.Add("error", Type.GetType("System.String"))

    '        For Each xr As DataRow In xRow
    '            If IsDBNull(xr("file_data")) = False Then
    '                xPath = IO.Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xr("fileuniquename").ToString
    '                Dim ms As New IO.MemoryStream(CType(xr("file_data"), Byte()))
    '                Dim fs As New IO.FileStream(xPath, IO.FileMode.Create)
    '                ms.WriteTo(fs)
    '                ms.Close()
    '                fs.Close()
    '                fs.Dispose()
    '                If xPath <> "" Then
    '                    fileToAdd.Add(xPath)
    '                End If
    '            ElseIf IsDBNull(xr("file_data")) = True Then
    '                If xr("temppath").ToString.Trim <> "" Then
    '                    If System.IO.File.Exists(xr("temppath").ToString.Trim) Then
    '                        xPath = xr("temppath").ToString.Trim
    '                    End If
    '                Else
    '                    xPath = xr("filepath").ToString
    '                    xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
    '                    If Strings.Left(xPath, 1) <> "/" Then
    '                        xPath = "~/" & xPath
    '                    Else
    '                        xPath = "~" & xPath
    '                    End If
    '                    xPath = Server.MapPath(xPath)
    '                    If xPath <> "" Then
    '                        If IO.File.Exists(xPath) Then
    '                            fileToAdd.Add(xPath)
    '                        End If
    '                    End If
    '                End If
    '            End If
    '        Next
    '        Dim zipFilename As String = ""
    '        zipFilename = "file_" + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
    '        Using ms As New IO.MemoryStream()
    '            Using pkg As IO.Packaging.Package = IO.Packaging.Package.Open(ms, IO.FileMode.Create)
    '                For Each item As String In fileToAdd
    '                    Dim destFilename As String = "/" & IO.Path.GetFileName(item)
    '                    Dim part As IO.Packaging.PackagePart = pkg.CreatePart(New Uri(destFilename, UriKind.Relative), "", IO.Packaging.CompressionOption.Normal)
    '                    Dim byt As Byte() = IO.File.ReadAllBytes(item)

    '                    Using fileStream As IO.FileStream = New IO.FileStream(item, IO.FileMode.Open)
    '                        CopyStream(CType(fileStream, IO.Stream), part.GetStream())
    '                    End Using
    '                Next
    '                pkg.Close()
    '            End Using
    '            Response.Clear()
    '            Response.ContentType = "application/zip"
    '            Response.AddHeader("content-disposition", "attachment; filename=" + zipFilename + "")
    '            Response.OutputStream.Write(ms.GetBuffer(), 0, CInt(ms.Length))
    '            Response.Flush()
    '            Response.End()
    '        End Using
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Private Sub CopyStream(ByVal source As IO.Stream, ByVal target As IO.Stream)
    '    Try
    '        Const bufSize As Integer = (5859 * 1024)
    '        Dim buf(bufSize - 1) As Byte
    '        Dim bytesRead As Integer = 0

    '        bytesRead = source.Read(buf, 0, bufSize)
    '        Do While bytesRead > 0
    '            target.Write(buf, 0, bytesRead)
    '            bytesRead = source.Read(buf, 0, bufSize)
    '        Loop
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'S.SANDEEP |18-JUL-2019| -- END


    Private Sub SetLanguage()
        Try
            '====== Employee Goal List (frmEmpFieldsList) Start =====
            'Language.setLanguage(mstrModuleName_upd_percent)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_upd_percent,CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, Me.lblPageHeader.Text)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_upd_percent,CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_upd_percent,CInt(HttpContext.Current.Session("LangId")),"gbFilterCriteria", Me.lblDetialHeader.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_upd_percent,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_upd_percent,CInt(HttpContext.Current.Session("LangId")),Me.lblPeriod.ID, Me.lblPeriod.Text)
            '====== Employee Goal List (frmEmpFieldsList) End =====


            '====== Update Progress (frmUpdateFieldValue) Start =====
            'Language.setLanguage("frmUpdateFieldValue")
            Me.lblUpdateProgressPageTitle.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"frmUpdateFieldValue", Me.lblUpdateProgressPageTitle.Text)
            Me.lblUpdateProgressPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"frmUpdateFieldValue", Me.lblUpdateProgressPageTitle.Text)
            Me.lblUpdatePeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"lblPeriod", Me.lblUpdatePeriod.Text)
            'Me.lblUpdateGoals.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"lblGoals", Me.lblUpdateGoals.Text)
            Me.lblUpdateEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"lblEmployee", Me.lblUpdateEmployee.Text)
            Me.lblUpdateDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"lblDate", Me.lblUpdateDate.Text)
            'Me.lblUpdatePercentage.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"lblPercentage", Me.lblUpdatePercentage.Text)
            Me.lblUpdateStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"lblStatus", Me.lblUpdateStatus.Text)
            Me.lblUpdateRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"lblRemark", Me.lblUpdateRemark.Text)
            Me.btnUpdateSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"btnSave", Me.btnUpdateSave.Text).Replace("&", "")
            Me.btnUpdateClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"btnClose", Me.btnUpdateClose.Text).Replace("&", "")
            Me.dgvHistory.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"dgcolhDate", Me.dgvHistory.Columns(3).HeaderText)
            Me.dgvHistory.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"dgcolhPercent", Me.dgvHistory.Columns(4).HeaderText)
            Me.dgvHistory.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"dgcolhStatus", Me.dgvHistory.Columns(5).HeaderText)
            Me.dgvHistory.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"dgcolhRemark", Me.dgvHistory.Columns(6).HeaderText)
            Me.dgvHistory.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"dgcolhAccomplishedStatus", Me.dgvHistory.Columns(8).HeaderText)
            'S.SANDEEP |08-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : PA CHANGES
            Me.dgvHistory.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"dgcolhLastValue", Me.dgvHistory.Columns(10).HeaderText)
            lblChangeBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"lblChangeBy", Me.lblChangeBy.Text)
            lblCaption3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"lblCaption3", Me.lblCaption3.Text)
            lblCaption4.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"lblCaption4", Me.lblCaption4.Text)
            lblCaption1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"lblCaption1", Me.lblCaption1.Text)
            lblCaption2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),"frmUpdateFieldValue",CInt(HttpContext.Current.Session("LangId")),"lblCaption2", Me.lblCaption2.Text)
            'S.SANDEEP |08-JUL-2019| -- END
            '====== Update Progress (frmUpdateFieldValue) End =====

            'S.SANDEEP |24-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : UPDATE PROGRESS APPROVAL SCREEN CHANGES
            '====== Approve/Reject Goals Accomplishment (frmAppRejGoalAccomplishmentList) Start =====
            ''Language.setLanguage(mstrModuleName_AppRejGoals)
            'Me.lblAccomplishment_Header.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_AppRejGoals,CInt(HttpContext.Current.Session("LangId")),mstrModuleName_AppRejGoals, Me.lblAccomplishment_Header.Text)
            'Me.lblAccomplishment_gbFilterCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_AppRejGoals,CInt(HttpContext.Current.Session("LangId")),"gbFilterCriteria", Me.lblAccomplishment_gbFilterCriteria.Text)
            'Me.lblAccomplishmentStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_AppRejGoals,CInt(HttpContext.Current.Session("LangId")),"lblStatus", Me.lblAccomplishmentStatus.Text)
            'Me.btnAccomplishmentClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_AppRejGoals,CInt(HttpContext.Current.Session("LangId")),"btnClose", Me.btnAccomplishmentClose.Text).Replace("&", "")

            'Me.dgv_AccomplishmentData.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_AppRejGoals,CInt(HttpContext.Current.Session("LangId")),"dgcolhDate", Me.dgvHistory.Columns(3).HeaderText)
            'Me.dgv_AccomplishmentData.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_AppRejGoals,CInt(HttpContext.Current.Session("LangId")),"dgcolhStatus", Me.dgvHistory.Columns(5).HeaderText)
            'Me.dgv_AccomplishmentData.Columns(8).HeaderText = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName_upd_percent, 102, "Value")
            'Me.dgv_AccomplishmentData.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_AppRejGoals,CInt(HttpContext.Current.Session("LangId")),"dgcolhPercent", Me.dgvHistory.Columns(4).HeaderText)
            'Me.dgv_AccomplishmentData.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_AppRejGoals,CInt(HttpContext.Current.Session("LangId")),"dgcolhRemark", Me.dgvHistory.Columns(6).HeaderText)
            'Me.dgv_AccomplishmentData.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_AppRejGoals,CInt(HttpContext.Current.Session("LangId")),"dgcolhAccomplishedStatus", Me.dgvHistory.Columns(8).HeaderText)
            'Me.dgv_AccomplishmentData.Columns(13).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName_AppRejGoals,CInt(HttpContext.Current.Session("LangId")),"dgcolhLastValue", Me.dgvHistory.Columns(10).HeaderText)
            '====== Approve/Reject Goals Accomplishment (frmAppRejGoalAccomplishmentList) End =====
            'S.SANDEEP |24-JUL-2019| -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


End Class
