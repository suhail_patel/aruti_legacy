﻿<%@ Page Title="Update Progress" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="~/Assessment New/Performance Goals/wPgESSUpdateGoalProgress.aspx.vb"
    Inherits="wPgESSUpdateGoalProgress" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ApproveCnf" TagPrefix="apprCnf" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>--%>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.uploadfile.min.js"></script>

    <%--<script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;
            //'S.SANDEEP |25-MAR-2019| -- START
            if (cval.length > 0)
                if (charCode == 45)
                if (cval.indexOf("-") > -1)
                return false;
            //'S.SANDEEP |25-MAR-2019| -- END

            if (charCode == 13)
                return false;

            //'S.SANDEEP |25-MAR-2019| -- START
            //            if (charCode > 31 && (charCode < 46 || charCode > 57))
            if (charCode > 31 && (charCode < 45 || charCode > 57))
            //'S.SANDEEP |25-MAR-2019| -- END
                return false;
            return true;
        }


        //        $("[id*=chkAllSelect]").live("click", function() {
        //            var chkHeader = $(this);
        //            var grid = $(this).closest("table");
        //            $("input[type=checkbox]", grid).each(function() {
        //                if (chkHeader.is(":checked")) {
        //                    debugger;
        //                    $(this).attr("checked", "checked");

        //                } else {
        //                    $(this).removeAttr("checked");
        //                }
        //            });
        //        });
        //        $("[id*=chkSelect]").live("click", function() {
        //            var grid = $(this).closest("table");
        //            var chkHeader = $("[id*=chkHeader]", grid);
        //            var row = $(this).closest("tr")[0];

        //            debugger;
        //            if (!$(this).is(":checked")) {
        //                var row = $(this).closest("tr")[0];
        //                chkHeader.removeAttr("checked");

        //            } else {

        //                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
        //                    chkHeader.attr("checked", "checked");
        //                }
        //            }

        //        });

        $("body").on("click", "[id*=chkAllSelect]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkSelect]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=chkSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkAllSelect]", grid);
            debugger;
            if ($("[id*=chkSelect]", grid).length == $("[id*=chkSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });    
    </script>

    <%--<script type="text/javascript">
        function setscrollPosition(sender) {
            sender.scrollLeft = document.getElementById("<%=hdf_leftposition.ClientID%>").value;
            sender.scrollTop = document.getElementById("<%=hdf_topposition.ClientID%>").value;
        }
        function getscrollPosition() {
            document.getElementById("<%=hdf_topposition.ClientID%>").value = document.getElementById("<%=pnl_dgvData.ClientID%>").scrollTop;
            document.getElementById("<%=hdf_leftposition.ClientID%>").value = document.getElementById("<%=pnl_dgvData.ClientID%>").scrollLeft;
        }
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        //        var scroll = {
        //            Y: '#<%= hfScrollPosition.ClientID %>'
        //        };
        //        var scroll1 = {
        //            Y: '#<%= hfScrollPosition1.ClientID %>'
        //        };
        //        prm = Sys.WebForms.PageRequestManager.getInstance();
        //        prm.add_beginRequest(beginRequestHandler);
        //        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            //            var cend = $("#endreq").val();
            //            if (cend == "1") {
            //                $("#endreq").val("0");
            //                var nbodyY = $("#bodyy").val();
            //                $(window).scrollTop(nbodyY);
            //            }

            $(".updatedata123").html("<i class='fa fa-tasks'></i>");
        });

        //'S.SANDEEP |20-JAN-2022| -- START
        //var intervalId = window.setInterval(function() {
        //$(".updatedata123").html("<i class='fa fa-tasks'></i>");
        //}, 999);
        //'S.SANDEEP |20-JAN-2022| -- END
        
        function beginRequestHandler(sender, args) {
        //            $("#endreq").val("0");
        //            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            //            $("#endreq").val("1");
            //            SetGeidScrolls();
            //            if (args.get_error() == undefined) {
            //                $("#scrollable-container").scrollTop($(scroll.Y).val());
            //                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            //            }
            $(".updatedata123").html("<i class='fa fa-tasks'></i>");
        }
        $(document).ready(function() {
            $(".updatedata123").html("<i class='fa fa-tasks'></i>");
        });
    </script>

    <%--<script type="text/javascript">
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    </script>--%>

    <script>
        function IsValidAttach() {
            if (parseInt($('.cboScanDcoumentType').val()) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }
        }    
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= popup_AttachementYesNo.ClientID %>_Panel1").css("z-index", "100002");
        }
        
    </script>

    <script type="text/javascript">
        function Print() {
            var printWin = window.open('', '', 'left=0,top=0,width=1000,height=600,status=0');
            printWin.document.write(document.getElementById("<%=divhtml.ClientID %>").innerHTML);
            printWin.document.close();
            printWin.focus();
            printWin.print();
            printWin.close();
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server" EnableViewState="true">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Update Progress"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblHeader" runat="server" Text="Mandatory Filters" />
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <asp:Label ID="lblEmployee" runat="server" Width="20%" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <asp:Label ID="lblPeriod" runat="server" Width="20%" Text="Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Label ID="objlblCurrentStatus" runat="server" Font-Bold="true" Font-Size="Small"
                                    ForeColor="Red" CssClass="pull-left" Visible="false"></asp:Label>
                                <asp:Label ID="objlblTotalWeight" runat="server" Text="" Font-Bold="true"></asp:Label>
                                <asp:Button ID="BtnSearch" runat="server" CssClass="btn btn-primary" Text="Search" />
                                <asp:Button ID="BtnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 300px">
                                            <asp:GridView ID="dgvData" runat="server" AutoGenerateColumns="false" Width="99%"
                                                AllowPaging="false" CssClass="table table-hover table-bordered" DataKeyNames="isfinal,UoMType,empfield1unkid,empfield2unkid,empfield3unkid,empfield4unkid,empfield5unkid,goaltypeid,goalvalue,Field1,Field2,Field3,Field4,Field5">
                                                <Columns>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit for Approval" CssClass="btn btn-primary pull-left" />
                                <asp:Button ID="btnViewUpdateProgressReport" runat="server" Text="Update Progress Report"
                                    CssClass="btn btn-primary pull-left" />
                                <asp:Button ID="btnclose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hdf_locationx" runat="server" />
                <asp:HiddenField ID="hdf_locationy" runat="server" />
                <asp:HiddenField ID="hdf_topposition" runat="server" />
                <asp:HiddenField ID="hdf_leftposition" runat="server" />
                <cc1:ModalPopupExtender ID="popup_UpdateProgress" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="hdf_UpdateProgress" PopupControlID="pnl_UpdateProgress" TargetControlID="hdf_UpdateProgress">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_UpdateProgress" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblUpdateProgressPageTitle" runat="server" Text="Update Progress"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card inner-card">
                                    <div class="header">
                                        <h2>
                                            <asp:Label ID="lblUpdateProgressPageHeader" runat="server" Text="Update Progress" />
                                        </h2>
                                    </div>
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblUpdatePeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtUpdatePeriod" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblUpdateEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtUpdateEmployeeName" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblUpdateDate" runat="server" Text="Change Date" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <uc2:DateCtrl ID="dtpUpdateChangeDate" runat="server" Width="100" AutoPostBack="false" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblChangeBy" runat="server" Text="Change by" Visible="false" CssClass="form-label"></asp:Label>
                                                        <asp:Label ID="lblCalcType" runat="server" Text="Calc. Mode" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboChangeBy" runat="server" Width="310px" AutoPostBack="true"
                                                                OnSelectedIndexChanged="cboChangeBy_SelectedIndexChanged" Visible="false">
                                                            </asp:DropDownList>
                                                            <asp:DropDownList ID="cboCalculationType" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <asp:Panel ID="pnlbypct" runat="server">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                            <asp:Label ID="lblCaption3" runat="server" Text="New Percentage" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtNewPercentage" runat="server" onKeypress="return onlyNumbers(this,event);"
                                                                        Style="text-align: right;" Text="0" AutoPostBack="true" class="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                            <asp:Label ID="lblCaption4" runat="server" Text="Total Percentage" CssClass="form-label"></asp:Label>
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtTotalPercentage" runat="server" onKeypress="return onlyNumbers(this,event);"
                                                                        Style="text-align: right;" Text="0" AutoPostBack="true" class="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel ID="pnlbyval" runat="server">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <asp:Label ID="lblCaption1" runat="server" Text="New Value" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtNewValue" runat="server" onKeypress="return onlyNumbers(this,event);"
                                                                    Style="text-align: right;" Text="0" AutoPostBack="true" class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <asp:Label ID="lblCaption2" runat="server" Text="Total Value" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtTotalValue" runat="server" onKeypress="return onlyNumbers(this,event);"
                                                                    Style="text-align: right;" Text="0" AutoPostBack="true" class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                </asp:Panel>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblUpdateStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboUpdateStatus" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblUpdateGoals" runat="server" Text="Goal" Visible="false" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtUpdateGoalsCaption" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtUpdateGoals" Rows="6" TextMode="MultiLine" runat="server" ReadOnly="true"
                                                                    class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblUpdateRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtUpdateRemark" runat="server" TextMode="MultiLine" Rows="2" class="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Label ID="objlblGoalTypeInfo" runat="server" Text="" Font-Bold="true" Font-Size="Small"
                                            ForeColor="Red" Font-Italic="true" CssClass="form-label pull-left"></asp:Label>
                                        <asp:Button ID="btnUpdateSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                        <asp:Button ID="btnUpdateClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                        <asp:HiddenField ID="hdf_UpdateProgress" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--'S.SANDEEP |23-DEC-2020| -- START--%>
                        <%--'ISSUE/ENHANCEMENT : DON'T SHOW DISAPPROVED DATA BY DEFAULT {UPDATE PROGRESS}--%>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <asp:CheckBox ID="chkIncludeDisapproved" runat="server" AutoPostBack="true" Text="Include Disapproved data in the below list" />
                                </div>
                            </div>
                        </div>
                        <%--'S.SANDEEP |23-DEC-2020| -- END--%>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 200px">
                                    <asp:DataGrid ID="dgvHistory" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                Visible="false">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgEdit" runat="server" CommandName="objEdit" ImageUrl="~/images/edit.png"
                                                        ToolTip="Edit" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <%--<asp:ImageButton ID="imgDelete" runat="server" CommandName="objDelete" ImageUrl="~/images/remove.png"
                                                        ToolTip="Delete" />--%>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="imgDelete" runat="server" CommandName="objDelete" ToolTip="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <%--<asp:ImageButton ID="imgAttach" runat="server" CommandName="objAttach" ImageUrl="~/images/add_16.png"
                                                        ToolTip="Add Attachment" />--%>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="imgAttach" runat="server" CommandName="objAttach" ToolTip="Add Attachment"><i class='fas fa-plus'></i></asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn DataField="ddate" HeaderText="Date" FooterText="dgcolhDate" />
                                            <asp:BoundColumn DataField="pct_completed" HeaderText="% Completed" FooterText="dgcolhPercent" />
                                            <asp:BoundColumn DataField="dstatus" HeaderText="Status" FooterText="dgcolhStatus" />
                                            <asp:BoundColumn DataField="remark" HeaderText="Remark" FooterText="dgcolhRemark" />
                                            <asp:BoundColumn DataField="empupdatetranunkid" FooterText="objdgcolhUnkid" Visible="false" />
                                            <asp:BoundColumn DataField="GoalAccomplishmentStatus" HeaderText="Approval Status"
                                                FooterText="dgcolhAccomplishedStatus" />
                                            <asp:BoundColumn DataField="approvalstatusunkid" Visible="false" FooterText="objcolhAccomplishedStatusId" />
                                            <asp:BoundColumn DataField="dfinalvalue" FooterText="dgcolhLastValue" HeaderText="Last Value">
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="tp" FooterText="dgcolhTotalProgress" HeaderText="Total Progress">
                                            </asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_UpdateYesNO" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="hdf_UpdateYesNo" PopupControlID="pnl_UpdateYesNO" TargetControlID="hdf_UpdateYesNo">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_UpdateYesNO" runat="server" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="Label1" runat="server" Text="Aruti"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblUpdateMessages" runat="server" Text="Message :" CssClass="form-label" />
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnupdateYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnupdateNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_UpdateYesNo" runat="server" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="modal-backdrop"
                    TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" CancelControlID="hdf_ScanAttchment">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                <div id="fileuploader">
                                    <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" value="Browse" />
                                </div>
                                <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                    Text="Browse" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                <div class="table-responsive" style="height: 250px">
                                    <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" Width="99%"
                                        AllowPaging="false" CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                            ToolTip="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="50px" ItemStyle-Width="50px"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download"><i class="fa fa-download"></i></asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                            <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                            <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary pull-left" />
                        <asp:Button ID="btnScanSave" runat="server" Text="Save" CssClass="btn btn-primary"
                            Visible="false" />
                        <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                    </div>
                </asp:Panel>
                <ucDel:DeleteReason ID="popup_UpdateProgressDeleteReason" runat="server" Title="Aruti"
                    CancelControlName="hdf_UpdateProgress" />
                <apprCnf:ApproveCnf ID="cnfApprove" runat="server" Title="Aruti" />
                <cc1:ModalPopupExtender ID="popupEmpAssessForm" runat="server" CancelControlID="btnEmpReportingClose"
                    PopupControlID="pnlEmpReporting" TargetControlID="HiddenField2" PopupDragHandleControlID="pnlEmpReporting">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlEmpReporting" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="height: 600px" DefaultButton="btnEmpReportingClose" ScrollBars="Auto">
                    <div class="header">
                        <h2>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div id="divhtml" runat="server" style="width: 89%; left: 76px; top: 13px">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnPlanningPring" runat="server" Text="Print" CssClass="btn btn-primary"
                            OnClientClick="javascript:Print()" />
                        <asp:Button ID="btnEmpReportingClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                    </div>
                </asp:Panel>
                <ucCfnYesno:Confirmation ID="popup_AttachementYesNo" runat="server" Message="" Title="Confirmation" />
                <%--OLD--%>
                <%--<div class="panel-primary">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default">
                                <div class="row2">
                                    <div class="ibwm" style="width: 100%">
                                        <div style="margin-bottom: 15px;">
                                            <h2 style="margin: 0 !important; color: #666; font-weight: normal; font-size: 14px;
                                                text-align: left">
                                            </h2>
                                        </div>
                                        <table style="width: 100%;">
                                            <tr style="width: 100%">
                                                <td style="width: 50%;" align="left">
                                                </td>
                                                <td style="width: 50%;" align="left">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="btn-default">
                                    <div style="float: left;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-default">
                            <div class="panel-body-default">
                                <div id="scrollable-container" onscroll="$(scroll.Y).val(this.scrollTop);" style="width: 99%;
                                    overflow: auto" class="gridscroll">
                                    <asp:Panel ID="pnl_dgvData" runat="server" Width="100%" Style="text-align: center">
                                    </asp:Panel>
                                </div>
                                <div class="btn-default" style="height: 31px">
                                    <asp:Panel ID="pnlOperation" runat="server">
                                        <div id="btnLeft" runat="server" style="float: left">
                                        </div>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <div id="div_PopupControl" style="width: 100%;">
                            <div id="hiddenFiled">
                            </div>
                            <div id="HiddenFieldSetScroll">
                            </div>
                            <div id="UpdateProgress">
                                <%--Drag="true"
                PopupDragHandleControlID="pnl_UpdateProgress"--%>
                </div>
                <div id="UpdateYesNO">
                </div>
                <div id="GoalAccomplishment">
                    <%--<cc1:ModalPopupExtender ID="popup_GoalAccomplishment" runat="server" TargetControlID="hdf_GoalAccomplishment"
                                    CancelControlID="hdf_GoalAccomplishment" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_GoalAccomplishment">
                                </cc1:ModalPopupExtender>--%>
                    <%--<asp:Panel ID="pnl_GoalAccomplishment" runat="server" CssClass="newpopup" Style="width: 950px;
                                    display: none">
                                    <div class="panel-primary" style="margin-bottom: 0px;">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblAccomplishment_Header" runat="server" Text="Approve/Reject
                Goals Accomplishment List"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div class="panel-default">
                                                <div id="Div34" class="panel-heading-default">
                                                    <div style="float: left;">
                                                        <asp:Label ID="lblAccomplishment_gbFilterCriteria" runat="server" Text="Filter Criteria" />
                                                    </div>
                                                </div>
                                                <div class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 5%">
                                                                <asp:Label ID="lblAccomplishmentStatus" runat="server" Text="Status" />
                                                            </td>
                                                            <td style="width: 16%">
                                                                <asp:DropDownList ID="cboAccomplishmentStatus" runat="server" Width="265px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 10%">
                                                                <asp:Label ID="lblPerspective" runat="server" Text="Perspective" />
                                                            </td>
                                                            <td style="width: 16%">
                                                                <asp:DropDownList ID="cboPerspective" runat="server" Width="265px" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 5%">
                                                                <asp:Label ID="lblLinkedField" runat="server" Text="" />
                                                            </td>
                                                            <td style="width: 16%">
                                                                <asp:DropDownList ID="cboLinkedField" runat="server" Width="754px">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="btn-default">
                                                        <asp:Button ID="btnAccomplishmentSearch" runat="server" Text="Search" CssClass="btnDefault" />
                                                        <asp:Button ID="btnAccomplishmentReset" runat="server" Text="Reset" CssClass="btnDefault" />
                                                        <asp:HiddenField ID="hdf_GoalAccomplishment" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel-default">
                                                <div class="panel-body-default">
                                                    <asp:Panel ID="pnlAccomplishmentData" runat="server" Style="width: 99%; max-height: 250px"
                                                        ScrollBars="Auto">
                                                        
                                                        <asp:GridView ID="dgv_AccomplishmentData" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                            RowStyle-CssClass="griviewitem" Style="margin: 0px" AllowPaging="false" HeaderStyle-Font-Bold="false"
                                                            DataKeyNames="empupdatetranunkid,Accomplished_statusId,Goal_status,org_per_comp"
                                                            Width="250%">
                                                            <Columns>
                                                                <asp:TemplateField HeaderStyle-Width="1%" ItemStyle-Width="1%" HeaderStyle-HorizontalAlign="Center"
                                                                    ItemStyle-HorizontalAlign="Center">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="chkAllSelect" runat="server" />
                                                                        <%--AutoPostBack="true" OnCheckedChanged="chkselectAll_CheckedChanged"
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                                        <%--AutoPostBack="true" OnCheckedChanged="chkselect_CheckedChanged"
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="pName" HeaderText="" HeaderStyle-Width="3%" />
                                                                <asp:BoundField DataField="field_data" HeaderText="" FooterText="objdgcolhfield_data"
                                                                    HeaderStyle-Width="10%" />
                                                                <asp:BoundField DataField="cData" HeaderText="" Visible="false" HeaderStyle-Width="10%" />
                                                                <asp:BoundField DataField="dData" HeaderText="" Visible="false" HeaderStyle-Width="10%" />
                                                                <asp:BoundField DataField="eData" HeaderText="" Visible="false" HeaderStyle-Width="10%" />
                                                                <asp:BoundField DataField="ddate" HeaderText="Date" FooterText="dgbcolhDate" HeaderStyle-Width="3%" />
                                                                <asp:BoundField DataField="Goal_status" HeaderText="Goals
                Status" HeaderStyle-Width="1%" />
                                                                <asp:BoundField DataField="dgoalvalue" HeaderText="Goal
                Value" FooterText="dgoalvalue" HeaderStyle-Width="1%" />
                                                                <asp:TemplateField HeaderStyle-Width="1%" ItemStyle-Width="1%" HeaderStyle-HorizontalAlign="Center"
                                                                    ItemStyle-HorizontalAlign="Center" HeaderText="% Completed">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtPerComp" runat="server" CssClass="decimal" AutoPostBack="true"
                                                                            ReadOnly="true" Text='<%# Eval("per_comp")
                %>' OnTextChanged="txtAccomplishmentPerComp_TextChanged" Style="text-align: right"></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Goal_Remark" HeaderText="Remark" HeaderStyle-Width="7%" />
                                                                <asp:BoundField DataField="accomplished_status" HeaderText="Approve/Disapprove
                Status" HeaderStyle-Width="1%" />
                                                                <asp:BoundField DataField="org_per_comp" Visible="false" />
                                                                <asp:BoundField DataField="dfinalvalue" FooterText="dgbcolhLastValue" HeaderText="Last
                Value" HeaderStyle-Width="1%" />
                                                                <asp:TemplateField HeaderStyle-Width="1%" ItemStyle-Width="1%" HeaderStyle-HorizontalAlign="Center"
                                                                    ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkDownload" runat="server" ToolTip="Download Document" Font-Underline="false"
                                                                            CommandName="download" OnClick="lnkdownloadAttachment_Click"> <i class="fa fa-download"></i>
                                                                        </asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                        <%--<asp:GridView ID="dgv_Accomplishment_data" runat="server" CssClass="gridview"
                HeaderStyle-CssClass="griviewheader" RowStyle-CssClass="griviewitem" Style="margin:
                0px" AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="empupdatetranunkid">
                <Columns> <asp:TemplateField HeaderStyle-Width="25px" ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center"
                ItemStyle-HorizontalAlign="Center" Visible="false"> <ItemTemplate> <asp:LinkButton
                ID="lnlApproved" runat="server" ToolTip="Approve" Font-Underline="false" CommandName="Approved">
                <i class="fa fa-check-circle" aria-hidden="true" style="font-size:20px;color:Green"></i>
                </asp:LinkButton> </ItemTemplate> </asp:TemplateField> <asp:TemplateField HeaderStyle-Width="25px"
                ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                Visible="false"> <ItemTemplate> <asp:LinkButton ID="lnkRejected" runat="server"
                ToolTip="Reject" Font-Underline="false" CommandName="Rejected"> <i class="fa fa-times-circle"
                aria-hidden="true" style="font-size:20px;color:Red"></i> </asp:LinkButton> </ItemTemplate>
                </asp:TemplateField> <asp:TemplateField HeaderStyle-Width="25px" ItemStyle-Width="25px"
                HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"> <HeaderTemplate>
                <asp:CheckBox ID="chkAllSelect" runat="server" AutoPostBack="true" Visible="false"
                /> </HeaderTemplate> <ItemTemplate> <asp:CheckBox ID="chkSelect" runat="server"
                AutoPostBack="true" OnCheckedChanged="chkselect_CheckedChanged" /> </ItemTemplate>
                </asp:TemplateField> <asp:BoundField DataField="field_data" HeaderText="" FooterText="objdgcolhfield_data"
                /> <asp:BoundField DataField="ddate" HeaderText="Date" FooterText="dgbcolhDate"
                HeaderStyle-Width="91px" /> <asp:BoundField DataField="Goal_status" HeaderText="Goals
                Status" HeaderStyle-Width="100px" /> <asp:TemplateField HeaderStyle-Width="25px"
                ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                HeaderText="% Complate"> <ItemTemplate> <asp:TextBox ID="txtPerComp" runat="server"
                CssClass="decimal" AutoPostBack="true" ReadOnly="true" Text='<%# Eval("per_comp")
                %>' OnTextChanged="txtAccomplishmentPerComp_TextChanged"></asp:TextBox> </ItemTemplate>
                </asp:TemplateField> <asp:BoundField DataField="Goal_Remark" HeaderText="Remark"
                /> <asp:BoundField DataField="accomplished_status" HeaderText="Approve/Disapprove
                Status" /> <asp:BoundField DataField="org_per_comp" Visible="false" /> <asp:BoundField
                DataField="dfinalvalue" FooterText="dgbcolhLastValue" HeaderText="Last Value" />
                <asp:TemplateField HeaderStyle-Width="25px" ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center"
                ItemStyle-HorizontalAlign="Center"> <ItemTemplate> <asp:LinkButton ID="lnkDownload"
                runat="server" ToolTip="Download Document" Font-Underline="false" CommandName="download"
                OnClick="lnkdownloadAttachment_Click"> <i class="fa fa-download"></i> </asp:LinkButton>
                </ItemTemplate> </asp:TemplateField> </Columns> </asp:GridView>
                                                    </asp:Panel>
                                                    <div style="width: 100%">
                                                        <table style="width: 100%">
                                                            <tr style="width: 100%">
                                                                <td style="width: 25%">
                                                                    <asp:Label ID="lblAccomplishmentRemark" runat="server" Text="Remark"></asp:Label>
                                                                </td>
                                                                <td style="width: 75%">
                                                                    <asp:TextBox ID="txtAccomplishmentRemark" runat="server" Text="" TextMode="MultiLine"
                                                                        Rows="3"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="btn-default">
                                                        <asp:Button ID="btnAccomplishmentApprove" runat="server" Text="Approve" CssClass="btnDefault" />
                                                        <asp:Button ID="btnAccomplishmentReject" runat="server" Text="Reject" CssClass="btnDefault" />
                                                        <asp:Button ID="btnAccomplishmentClose" runat="server" Text="Close" CssClass="btnDefault" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <%--<div id="Accomplishment_YesNo">
                                <cc1:ModalPopupExtender ID="popup_Accomplishment_YesNo" runat="server" BackgroundCssClass="modal-backdrop"
                                    CancelControlID="hdfAccomplishment" PopupControlID="pnl_Accomplishment_YesNo"
                                    TargetControlID="hdfAccomplishment">
                                </cc1:ModalPopupExtender>
                                <asp:Panel ID="pnl_Accomplishment_YesNo" runat="server" CssClass="newpopup" Style="display: none;
                                    width: 450px">
                                    <div class="panel-primary" style="margin-bottom: 0px;">
                                        <div class="panel-heading">
                                            <asp:Label ID="lblAccompllishmentHeader" runat="server" Text="Aruti"></asp:Label>
                                        </div>
                                        <div class="panel-body">
                                            <div class="panel-default">
                                                <div class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 100%">
                                                                <asp:Label ID="lblAccomplishment_Message" runat="server" Text="Message :" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="btn-default">
                                                        <asp:Button ID="btnAccomplishment_yes" runat="server" Text="Yes" CssClass="btnDefault" />
                                                        <asp:Button ID="btnAccomplishment_No" runat="server" Text="No" CssClass="btnDefault" />
                                                        <asp:HiddenField ID="hdfAccomplishment" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div id="ScanAttachment">
                            </div>
                        </div>
                    </div>
                </div>--%>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgv_Attchment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
                <%--<asp:PostBackTrigger ControlID="dgv_AccomplishmentData" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script type="text/javascript">
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPgUpdateGoalProgress.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }
        $("body").on("click", "[type=file]", function() {
            //$('input[type=file]').live("click", function() {
            return IsValidAttach();
        });
        
    </script>

</asp:Content>
