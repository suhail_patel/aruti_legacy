<%@ Page Title="Owners Goals List" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPgAllocationLvlGoalsList.aspx.vb" Inherits="wPgAllocationLvlGoalsList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc8" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <link href="../../App_Themes/PA_Style.css" type="text/css" />

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>

    <script type="text/javascript">

        //        $(".objAddBtn").live("mousedown", function(e) {
        //            $("#<%= hdf_locationx.ClientID %>").val(e.clientX);
        //            $("#<%= hdf_locationy.ClientID %>").val(e.clientY);

        $("body").on("mousedown", ".lnAdd", function(e) {
            $("#<%= hdf_locationx.ClientID %>").val(e.clientX);
            $("#<%= hdf_locationy.ClientID %>").val(e.clientY);
        });
        $("body").on("mousedown", ".lnEdit", function(e) {
            $("#<%= hdf_locationx.ClientID %>").val(e.clientX);
            $("#<%= hdf_locationy.ClientID %>").val(e.clientY);
        });
        $("body").on("mousedown", ".lnDelt", function(e) {
            $("#<%= hdf_locationx.ClientID %>").val(e.clientX);
            $("#<%= hdf_locationy.ClientID %>").val(e.clientY);
        });
       
    </script>

    <script type="text/javascript">
        //$(".popMenu").live("click", function(e) {
        $("body").on("click", '.popMenu', function(e) {
            var id = jQuery(this).attr('id').replace('_backgroundElement', '');
            $find(id).hide();
        });
    </script>

    <script type="text/javascript">
        function ClosePopup(ctrl) {
            $find(id).hide();
        }

        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 46 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <script type="text/javascript">
        function setscrollPosition(sender) {
            sender.scrollLeft = document.getElementById("<%=hdf_leftposition.ClientID%>").value;
            sender.scrollTop = document.getElementById("<%=hdf_topposition.ClientID%>").value;
        }
        function getscrollPosition() {

            document.getElementById("<%=hdf_topposition.ClientID%>").value = document.getElementById("pnl_dgvData").scrollTop;
            document.getElementById("<%=hdf_leftposition.ClientID%>").value = document.getElementById("pnl_dgvData").scrollLeft;
        }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            SetGeidScrolls();
            if (args.get_error() == undefined) {
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            }
            $(".updatedata123").html("<i class='fa fa-tasks'></i>");
            $(".lnAdd").html("<i class='fas fa-plus'></i>");
            $(".lnEdit").html("<i class='fas fa-pencil-alt text-primary'></i>");
            $(".lnDelt").html("<i class='fas fa-trash text-danger'></i>");
        }
        $(document).ready(function() {
            $(".updatedata123").html("<i class='fa fa-tasks'></i>");
            $(".lnAdd").html("<i class='fas fa-plus'></i>");
            $(".lnEdit").html("<i class='fas fa-pencil-alt text-primary'></i>");
            $(".lnDelt").html("<i class='fas fa-trash text-danger'></i>");
        });

    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".desc").hover(function() {
                $(".abc").show();
            });

        });
        function SetGeidScrolls() {
            var arrPnl = $('.gridscroll');
            for (j = 0; j < arrPnl.length; j++) {
                var trtag = $(arrPnl[j]).find('.gridview').children('tbody').children();
                if (trtag.length > 52) {
                    var trheight = 0;
                    for (i = 0; i < 52; i++) {
                        trheight = trheight + $(trtag[i]).height();
                    }
                    $(arrPnl[j]).css("overflow", "auto");
                    $(arrPnl[j]).css("height", trheight + "px");
                }
                else {
                    $(arrPnl[j]).css("overflow", "auto");
                    $(arrPnl[j]).css("height", "100%");
                }
            }
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Owners Goals List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="objlblField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboFieldValue1" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="objlblField4" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboFieldValue4" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblOwnerType" runat="server" Text="Owner Type" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboOwnerType" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="objlblField2" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboFieldValue2" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="objlblField5" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboFieldValue5" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblOwner" runat="server" Text="Owner" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboOwner" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="objlblField3" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboFieldValue3" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objlblTotalWeight" runat="server" Text="" Font-Bold="true" CssClass="form-label pull-right"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Panel ID="pnl_Button" runat="server">
                                    <asp:Button ID="btnCommit" runat="server" CssClass="btn btn-primary pull-left" Text="Commit" />
                                    <asp:Button ID="btnUnlock" runat="server" CssClass="btn btn-primary pull-left" Text="Unlock Committed" />
                                    <asp:Button ID="btnGlobalAssign" runat="server" CssClass="btn btn-primary pull-left"
                                        Text="Global Assign" />
                                    <asp:Button ID="btnUpdatePercentage" runat="server" CssClass="btn btn-primary pull-left"
                                        Text="Update % Completed" Visible="false" />
                                </asp:Panel>
                                <asp:Button ID="BtnSearch" runat="server" CssClass="btn btn-primary" Text="Search" />
                                <asp:Button ID="BtnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div id="pnl_dgvData" class="table-responsive" onscroll="$(scroll1.Y).val(this.scrollTop);"
                                            style="height: 350px">
                                            <asp:GridView ID="dgvData" runat="server" AutoGenerateColumns="false" Width="99%"
                                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField ID="hdfv" runat="server" />
                <cc1:ModalPopupExtender ID="popAddButton" BehaviorID="MPE" runat="server" TargetControlID="hdfv"
                    CancelControlID="btnAddCancel" PopupControlID="pnlImageAdd" BackgroundCssClass="modal-backdrop">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlImageAdd" runat="server" CssClass="card">
                    <div class="header">
                        <ul class="header-dropdown m-r-10 p-l-0">
                            <asp:LinkButton ID="lnkaclose" runat="server" OnClientClick="ClosePopup('popAddButton');return false;"><i class="fas fa-window-close"></i></asp:LinkButton>
                        </ul>
                    </div>
                    <div class="body" style="height: auto">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:DataList ID="dlmnuAdd" runat="server" ItemStyle-Wrap="true">
                                    <AlternatingItemStyle BackColor="Transparent" />
                                    <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                    <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID='objadd' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                            OnClick="lnkAdd_Click" Font-Underline="false" CssClass="form-label" Font-Bold="true" />
                                    </ItemTemplate>
                                </asp:DataList>
                                <asp:HiddenField ID="btnAddCancel" runat="server" />
                                <asp:HiddenField ID="btndlmnuAdd" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popEditButton" runat="server" TargetControlID="btndlmnuEdit"
                    CancelControlID="btnEditCancel" BackgroundCssClass="modal-backdrop" PopupControlID="pnlImageEdit">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlImageEdit" runat="server" CssClass="card">
                    <div class="header">
                        <ul class="header-dropdown m-r-10 p-l-0">
                            <asp:LinkButton ID="lnkeclose" runat="server" OnClientClick="ClosePopup('popEditButton');return false;"><i class="fas fa-window-close"></i></asp:LinkButton>
                        </ul>
                    </div>
                    <div class="body" style="height: auto">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:DataList ID="dlmnuEdit" runat="server">
                                    <AlternatingItemStyle BackColor="Transparent" />
                                    <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                    <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID='objEdit' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                            Font-Underline="false" CssClass="form-label" Font-Bold="true" OnClick="lnkEdit_Click" />
                                        <asp:HiddenField ID="objEditOrignalName" runat="server" Value='<%# Eval("lnkOriginal") %>' />
                                    </ItemTemplate>
                                </asp:DataList>
                                <asp:HiddenField ID="btnEditCancel" runat="server" />
                                <asp:HiddenField ID="btndlmnuEdit" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popDeleteButton" runat="server" TargetControlID="btndlmnuDelete"
                    CancelControlID="btnDeleteCancel" BackgroundCssClass="modal-backdrop" PopupControlID="pnlImageDelete">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlImageDelete" runat="server" CssClass="card">
                    <div class="header">
                        <ul class="header-dropdown m-r-10 p-l-0">
                            <asp:LinkButton ID="lnkdclose" runat="server" OnClientClick="ClosePopup('popDeleteButton');return false;"><i class="fas fa-window-close"></i></asp:LinkButton>
                        </ul>
                    </div>
                    <div class="body" style="height: auto">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:DataList ID="dlmnuDelete" runat="server">
                                    <AlternatingItemStyle BackColor="Transparent" />
                                    <ItemStyle BackColor="Transparent" ForeColor="Black" />
                                    <SelectedItemStyle BackColor="#DDD" Font-Bold="True" ForeColor="White" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID='objDelete' runat="server" Text='<%# Eval("lnkName") %>' CommandArgument='<%# Eval("lnkTag") %>'
                                            Font-Underline="false" CssClass="form-label" Font-Bold="true" OnClick="lnkDelete_Click" />
                                        <asp:HiddenField ID="objDeleteOrignalName" runat="server" Value='<%# Eval("lnkOriginal") %>' />
                                    </ItemTemplate>
                                </asp:DataList>
                                <asp:HiddenField ID="btnDeleteCancel" runat="server" />
                                <asp:HiddenField ID="btndlmnuDelete" runat="server" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="pop_objfrmAddEditOwrField1" runat="server" TargetControlID="hdf_btnOwrField1Save"
                    CancelControlID="hdf_btnOwrField1Save" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_objfrmAddEditOwrField1"
                    Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_objfrmAddEditOwrField1" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblheaderOwrField1" runat="server" Text="AddEditOwrField1"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOwrField1Period" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtOwrField1Period" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOwrField1GoalOwner" runat="server" Text="Owner Category" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboOwrField1Allocations" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOwrField1Owner" runat="server" Text="Owner" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboOwrField1Owner" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField1lblField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboOwrField1FieldValue1" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField1lblOwrField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objOwrField1txtOwrField1" runat="server" TextMode="MultiLine" Rows="5"
                                                    class="form-control" Style="resize: none;"></asp:TextBox>
                                                <asp:HiddenField ID="objOwrField1txtOwrField1Tag" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="pnl_RightOwrField1" runat="server" Style="height: auto">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblOwrField1StartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpOwrField1StartDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblOwrField1EndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpOwrField1EndDate" runat="server" AutoPostBack="false" />
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblOwrField1Weight" runat="server" Text="Weight" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtOwrField1Weight" runat="server" Style="text-align: right" Text="0.00"
                                                        onKeypress="return onlyNumbers(this, event);" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div style="display: none">
                                                <asp:Label ID="lblOwrField1Percentage" runat="server" Text="% Completed" Visible="false"></asp:Label>
                                                <asp:TextBox ID="txtOwrField1Percent" runat="server" Enabled="false" Style="text-align: right"
                                                    Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);" Visible="false"></asp:TextBox>
                                                <asp:Label ID="lblOwrField1Status" runat="server" Text="Status" Visible="false"></asp:Label>
                                                <asp:DropDownList ID="cboOwrField1Status" runat="server" Enabled="false" Visible="false">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Panel ID="objOwrField1tabcRemarks" runat="server">
                                                <%--Tab-Headers--%>
                                                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                    <li id="objOwrField1tabpRemark1" runat="server" role="presentation" class="active"><a
                                                        href="#InfFld6" data-toggle="tab">
                                                        <asp:Label ID="lblOwrField1Remark1" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="hdfOwrField1Tag1" runat="server" />
                                                    </a></li>
                                                    <li id="objOwrField1tabpRemark2" runat="server" role="presentation"><a href="#InfFld7"
                                                        data-toggle="tab">
                                                        <asp:Label ID="lblOwrField1Remark2" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="hdfOwrField1Tag2" runat="server" />
                                                    </a></li>
                                                    <li id="objOwrField1tabpRemark3" runat="server" role="presentation"><a href="#InfFld8"
                                                        data-toggle="tab">
                                                        <asp:Label ID="lblOwrField1Remark3" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="hdfOwrField1Tag3" runat="server" />
                                                    </a></li>
                                                </ul>
                                                <%--Tab-Panes--%>
                                                <div class="tab-content p-b-0">
                                                    <div role="tabpanel" class="tab-pane fade in active" id="InfFld6">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField1Remark1" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                            Rows="2" Style="resize: none;"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="InfFld7">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField1Remark2" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                            Rows="2" Style="resize: none;"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="InfFld8">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField1Remark3" runat="server" CssClass="form-control" TextMode="MultiLine"
                                                                            Rows="2" Style="resize: none;"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtOwrField1SearchEmp" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="height: 160px">
                                                <asp:Panel ID="pnl_OwrField1dgvower" runat="server">
                                                    <asp:DataGrid ID="dgvOwrField1Owner" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkOwrField1allSelect" runat="server" OnCheckedChanged="chkOwrField1AllSelect_CheckedChanged"
                                                                        CssClass="filled-in" Text=" " AutoPostBack="true" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkOwrField1select" runat="server" Checked='<%# Eval("ischeck") %>'
                                                                        AutoPostBack="true" CssClass="filled-in" Text=" " OnCheckedChanged="chkOwrField1select_CheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="name" HeaderText="Employee" FooterText="dgcolhEName"
                                                                HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                            </asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_btnOwrField1Save" runat="server" />
                        <asp:Button ID="btnOwrField1Save" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnOwrField1Cancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="pop_objfrmAddEditOwrField2" runat="server" TargetControlID="hdf_btnOwrField2Save"
                    CancelControlID="hdf_btnOwrField2Save" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_objfrmAddEditOwrField2">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_objfrmAddEditOwrField2" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblHeaderOwrField2" runat="server" Text="AddEditOwrField2"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOwrField2Period" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtOwrField2Period" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField2lblOwrField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboOwrField2OwrFieldValue1" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hdf_cboOwrField2OwrFieldValue1" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField2lblOwrField2" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objOwrField2txtOwrField2" class="form-control" runat="server" TextMode="MultiLine"
                                                    Rows="6" Style="resize: none"></asp:TextBox>
                                                <asp:HiddenField ID="hdf_objOwrField2txtOwrField2" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <asp:Panel ID="pnl_RightOwrField2" runat="server" Style="height: auto">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblStartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpOwrField2StartDate" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblEndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpOwrField2EndDate" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblOwrField2Weight" runat="server" Text="Weight" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtOwrField2Weight" runat="server" Style="text-align: right" Text="0.00"
                                                        onKeypress="return onlyNumbers(this, event);" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: none">
                                            <asp:Label ID="lblOwrField2Status" runat="server" Text="Status"></asp:Label>
                                            <asp:DropDownList ID="cboOwrField2Status" runat="server" Enabled="false" Height="25px"
                                                Width="200px">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblOwrField2Percentage" runat="server" Text="% Completed"></asp:Label>
                                            <asp:TextBox ID="txtOwrField2Percent" runat="server" Enabled="false" Style="text-align: right"
                                                Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Panel ID="objOwrField2tabcRemarks" runat="server">
                                                <%--Tab-Headers--%>
                                                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                    <li id="objOwrField2tabpRemark1" runat="server" role="presentation" class="active"><a
                                                        href="#Inf2Fld6" data-toggle="tab">
                                                        <asp:Label ID="lblOwrField2Remark1" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="hdfOwrField2Tag1" runat="server" />
                                                    </a></li>
                                                    <li id="objOwrField2tabpRemark2" runat="server" role="presentation"><a href="#Inf2Fld7"
                                                        data-toggle="tab">
                                                        <asp:Label ID="lblOwrField2Remark2" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="hdfOwrField2Tag2" runat="server" />
                                                    </a></li>
                                                    <li id="objOwrField2tabpRemark3" runat="server" role="presentation"><a href="#Inf2Fld8"
                                                        data-toggle="tab">
                                                        <asp:Label ID="lblOwrField2Remark3" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="hdfOwrField2Tag3" runat="server" />
                                                    </a></li>
                                                </ul>
                                                <%--Tab-Panes--%>
                                                <div class="tab-content p-b-0">
                                                    <div role="tabpanel" class="tab-pane fade in active" id="Inf2Fld6">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField2Remark1" runat="server" TextMode="MultiLine" Rows="2"
                                                                            Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="Inf2Fld7">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField2Remark2" runat="server" TextMode="MultiLine" Rows="2"
                                                                            Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="Inf2Fld8">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField2Remark3" runat="server" TextMode="MultiLine" Rows="2"
                                                                            Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtOwrField2SearchEmp" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="height: 160px">
                                                <asp:Panel ID="pnl_OwrField2dgvower" runat="server">
                                                    <asp:DataGrid ID="dgvOwrField2Owner" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkOwrField2allSelect" runat="server" CssClass="filled-in" Text=" "
                                                                        OnCheckedChanged="chkOwrField2AllSelect_CheckedChanged" AutoPostBack="true" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkOwrField2select" runat="server" Checked='<%# Eval("ischeck") %>'
                                                                        AutoPostBack="true" CssClass="filled-in" Text=" " OnCheckedChanged="chkOwrField2select_CheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="name" HeaderText="Employee" FooterText="dgcolhEName"
                                                                HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                            </asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_btnOwrField2Save" runat="server" />
                        <asp:Button ID="btnOwrField2Save" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnOwrField2Cancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="pop_objfrmAddEditOwrField3" runat="server" TargetControlID="hdf_btnOwrField3Save"
                    CancelControlID="hdf_btnOwrField3Save" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_objfrmAddEditOwrField3"
                    Drag="True">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_objfrmAddEditOwrField3" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblHeaderOwrField3" runat="server" Text="AddEditOwrField3"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOwrField3Period" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtOwrField3Period" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField3lblOwrField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objOwrField3txtOwrField1" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField3lblOwrField2" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboOwrField3OwrFieldValue2" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hdf_cboOwrField3OwrFieldValue2" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField3lblOwrField3" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objOwrField3txtOwrField3" runat="server" TextMode="MultiLine" Rows="7"
                                                    Style="resize: none" class="form-control"></asp:TextBox>
                                                <asp:HiddenField ID="hdf_objOwrField3txtOwrField3" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <asp:Panel ID="pnl_RightOwrField3" runat="server" Style="height: auto">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblOwrField3StartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpOwrField3StartDate" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblOwrField3EndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpOwrField3EndDate" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblOwrField3Weight" runat="server" Text="Weight" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtOwrField3Weight" runat="server" Style="text-align: right" Text="0.00"
                                                        onKeypress="return onlyNumbers(this, event);" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: none">
                                            <asp:Label ID="lblOwrField3Status" runat="server" Text="Status"></asp:Label>
                                            <asp:DropDownList ID="cboOwrField3Status" runat="server" Enabled="false" Height="25px"
                                                Width="200px">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblOwrField3Percentage" runat="server" Text="% Completed"></asp:Label>
                                            <asp:TextBox ID="txtOwrField3Percent" runat="server" Enabled="false" Style="text-align: right"
                                                Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Panel ID="objOwrField3tabcRemarks" runat="server">
                                                <%--Tab-Headers--%>
                                                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                    <li id="objOwrField3tabpRemark1" runat="server" role="presentation" class="active"><a
                                                        href="#Inf3Fld6" data-toggle="tab">
                                                        <asp:Label ID="lblOwrField3Remark1" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="hdf_lblOwrField3Remark1" runat="server" />
                                                    </a></li>
                                                    <li id="objOwrField3tabpRemark2" runat="server" role="presentation"><a href="#Inf3Fld7"
                                                        data-toggle="tab">
                                                        <asp:Label ID="lblOwrField3Remark2" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="hdf_lblOwrField3Remark2" runat="server" />
                                                    </a></li>
                                                    <li id="objOwrField3tabpRemark3" runat="server" role="presentation"><a href="#Inf3Fld8"
                                                        data-toggle="tab">
                                                        <asp:Label ID="lblOwrField3Remark3" runat="server"></asp:Label>
                                                        <asp:HiddenField ID="hdf_lblOwrField3Remark3" runat="server" />
                                                    </a></li>
                                                </ul>
                                                <%--Tab-Panes--%>
                                                <div class="tab-content p-b-0">
                                                    <div role="tabpanel" class="tab-pane fade in active" id="Inf3Fld6">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField3Remark1" runat="server" TextMode="MultiLine" Rows="2"
                                                                            Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="Inf3Fld7">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField3Remark2" runat="server" TextMode="MultiLine" Rows="2"
                                                                            Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="Inf3Fld6Inf3Fld68">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField3Remark3" runat="server" TextMode="MultiLine" Rows="2"
                                                                            Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtOwrField3SearchEmp" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="height: 160px">
                                                <asp:Panel ID="pnl_OwrField3dgvOwner" runat="server">
                                                    <asp:DataGrid ID="dgvOwrField3Owner" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkOwrField3allSelect" runat="server" CssClass="filled-in" Text=" "
                                                                        OnCheckedChanged="chkOwrField3AllSelect_CheckedChanged" AutoPostBack="true" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkOwrField3select" runat="server" Checked='<%# Eval("ischeck") %>'
                                                                        AutoPostBack="true" CssClass="filled-in" Text=" " OnCheckedChanged="chkOwrField3select_CheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="name" HeaderText="Employee" FooterText="dgcolhEName"
                                                                HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                            </asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_btnOwrField3Save" runat="server" />
                        <asp:Button ID="btnOwrField3Save" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnOwrField3Cancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="pop_objfrmAddEditOwrField4" runat="server" TargetControlID="hdf_btnOwrField4Save"
                    CancelControlID="hdf_btnOwrField4Save" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_objfrmAddEditOwrField4">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_objfrmAddEditOwrField4" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblHeaderOwrField4" runat="server" Text="AddEditOwrField3"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOwrField4Period" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtOwrField4Period" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField4lblOwrField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objOwrField4txtOwrField1" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField4lblOwrField2" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objOwrField4txtOwrField2" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField4lblOwrField3" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboOwrField4OwrFieldValue3" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hdf_cboOwrField4OwrFieldValue3" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField4lblOwrField4" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objOwrField4txtOwrField4" runat="server" TextMode="MultiLine" Rows="4"
                                                    Style="resize: none" class="form-control"></asp:TextBox>
                                                <asp:HiddenField ID="hdf_objOwrField4txtOwrField4" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <asp:Panel ID="pnl_RightOwrField4" runat="server" Style="height: auto">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblOwrField4StartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpOwrField4StartDate" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblOwrField4EndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpOwrField4EndDate" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblOwrField4Weight" runat="server" Text="Weight" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtOwrField4Weight" runat="server" Style="text-align: right" Text="0.00"
                                                        onKeypress="return onlyNumbers(this, event);" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: none">
                                            <asp:Label ID="lblOwrField4Status" runat="server" Text="Status"></asp:Label>
                                            <asp:DropDownList ID="cboOwrField4Status" runat="server" Enabled="false" Height="25px"
                                                Width="200px">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblOwrField4Percentage" runat="server" Text="% Completed"></asp:Label>
                                            <asp:TextBox ID="txtOwrField4Percent" runat="server" Enabled="false" Style="text-align: right"
                                                Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Panel ID="objOwrField4tabcRemarks" runat="server">
                                                <%--Tab-Headers--%>
                                                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                    <li id="objOwrField4tabpRemark1" runat="server" role="presentation" class="active"><a
                                                        href="#Inf4Fld6" data-toggle="tab">
                                                        <asp:Label ID="lblOwrField4Remark1" runat="server"></asp:Label>
                                                    </a></li>
                                                    <li id="objOwrField4tabpRemark2" runat="server" role="presentation"><a href="#Inf4Fld7"
                                                        data-toggle="tab">
                                                        <asp:Label ID="lblOwrField4Remark2" runat="server"></asp:Label>
                                                    </a></li>
                                                    <li id="objOwrField4tabpRemark3" runat="server" role="presentation"><a href="#Inf4Fld8"
                                                        data-toggle="tab">
                                                        <asp:Label ID="lblOwrField4Remark3" runat="server"></asp:Label>
                                                    </a></li>
                                                </ul>
                                                <%--Tab-Panes--%>
                                                <div class="tab-content p-b-0">
                                                    <div role="tabpanel" class="tab-pane fade in active" id="Inf4Fld6">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField4Remark1" runat="server" TextMode="MultiLine" Rows="2"
                                                                            Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdf_txtOwrField4Remark1" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="Inf4Fld7">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField4Remark2" runat="server" TextMode="MultiLine" Rows="2"
                                                                            Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdf_txtOwrField4Remark2" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="Inf4Fld8">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField4Remark3" runat="server" TextMode="MultiLine" Rows="2"
                                                                            Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdf_txtOwrField4Remark3" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtOwrField4SearchEmp" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="height: 160px">
                                                <asp:Panel ID="pnl_OwrField4dgvOwner" runat="server">
                                                    <asp:DataGrid ID="dgvOwrField4Owner" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkOwrField4allSelect" runat="server" CssClass="filled-in" Text=" "
                                                                        OnCheckedChanged="chkOwrField4AllSelect_CheckedChanged" AutoPostBack="true" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkOwrField4select" runat="server" Checked='<%# Eval("ischeck") %>'
                                                                        AutoPostBack="true" CssClass="filled-in" Text=" " OnCheckedChanged="chkOwrField4select_CheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="name" HeaderText="Employee" FooterText="dgcolhEName"
                                                                HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                            </asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_btnOwrField4Save" runat="server" />
                        <asp:Button ID="btnOwrField4Save" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnOwrField4Cancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="pop_objfrmAddEditOwrField5" runat="server" TargetControlID="hdf_btnOwrField5Save"
                    CancelControlID="hdf_btnOwrField5Save" BackgroundCssClass="modal-backdrop" PopupControlID="pnl_objfrmAddEditOwrField5">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_objfrmAddEditOwrField5" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblHeaderOwrField5" runat="server" Text="AddEditOwrField5"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblOwrField5Period" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtOwrField5Period" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField5lblOwrField1" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objOwrField5txtOwrField1" runat="server" ReadOnly="true" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField5lblOwrField2" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objOwrField5txtOwrField2" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField5lblOwrField3" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objOwrField5txtOwrField3" runat="server" class="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField5lblOwrField4" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboOwrField5OwrFieldValue4" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <asp:HiddenField ID="hdf_cboOwrField5OwrFieldValue4" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="objOwrField5lblOwrField5" runat="server" Text="#Caption" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="objOwrField5txtOwrField5" runat="server" TextMode="MultiLine" Rows="4"
                                                    Style="resize: none" class="form-control"></asp:TextBox>
                                                <asp:HiddenField ID="hdf_objOwrField5txtOwrField5" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <asp:Panel ID="pnl_RightOwrField5" runat="server" Style="height: auto">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblOwrField5StartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpOwrField5StartDate" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblOwrField5EndDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <uc2:DateCtrl ID="dtpOwrField5EndDate" runat="server" />
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:Label ID="lblOwrField5Weight" runat="server" Text="Weight" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtOwrField5Weight" runat="server" Style="text-align: right" Text="0.00"
                                                        onKeypress="return onlyNumbers(this, event);" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display: none">
                                            <asp:Label ID="lblOwrField5Status" runat="server" Text="Status"></asp:Label>
                                            <asp:DropDownList ID="cboOwrField5Status" runat="server" Enabled="false" Height="25px"
                                                Width="200px">
                                            </asp:DropDownList>
                                            <asp:Label ID="lblOwrField5Percentage" runat="server" Text="% Completed"></asp:Label>
                                            <asp:TextBox ID="txtOwrField5Percent" runat="server" Enabled="false" Style="text-align: right"
                                                Text="0.00" Width="100px" onKeypress="return onlyNumbers(this, event);"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Panel ID="objOwrField5tabcRemarks" runat="server">
                                                <%--Tab-Headers--%>
                                                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                    <li id="objOwrField5tabpRemark1" runat="server" role="presentation" class="active"><a
                                                        href="#Inf5Fld6" data-toggle="tab">
                                                        <asp:Label ID="lblOwrField5Remark1" runat="server"></asp:Label>
                                                    </a></li>
                                                    <li id="objOwrField5tabpRemark2" runat="server" role="presentation"><a href="#Inf5Fld7"
                                                        data-toggle="tab">
                                                        <asp:Label ID="lblOwrField5Remark2" runat="server"></asp:Label>
                                                    </a></li>
                                                    <li id="objOwrField5tabpRemark3" runat="server" role="presentation"><a href="#Inf5Fld8"
                                                        data-toggle="tab">
                                                        <asp:Label ID="lblOwrField5Remark3" runat="server"></asp:Label>
                                                    </a></li>
                                                </ul>
                                                <%--Tab-Panes--%>
                                                <div class="tab-content p-b-0">
                                                    <div role="tabpanel" class="tab-pane fade in active" id="Inf5Fld6">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField5Remark1" runat="server" TextMode="MultiLine" Rows="2"
                                                                            Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdf_txtOwrField5Remark1" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="Inf5Fld7">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField5Remark2" runat="server" TextMode="MultiLine" Rows="2"
                                                                            Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdf_txtOwrField5Remark2" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div role="tabpanel" class="tab-pane fade" id="Inf5Fld8">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtOwrField5Remark3" runat="server" TextMode="MultiLine" Rows="2"
                                                                            Style="resize: none;" CssClass="form-control"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdf_txtOwrField5Remark3" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtOwrField5SearchEmp" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="table-responsive" style="height: 160px">
                                                <asp:Panel ID="pnl_OwrField5dgvOwner" runat="server">
                                                    <asp:DataGrid ID="dgvOwrField5Owner" runat="server" AutoGenerateColumns="false" CssClass="table table-hover table-bordered">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="chkOwrField5allSelect" runat="server" CssClass="filled-in" Text=" "
                                                                        OnCheckedChanged="chkOwrField5AllSelect_CheckedChanged" AutoPostBack="true" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkOwrField5select" runat="server" Checked='<%# Eval("ischeck") %>'
                                                                        AutoPostBack="true" CssClass="filled-in" Text=" " OnCheckedChanged="chkOwrField5select_CheckedChanged" />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="employeecode" HeaderText="Code" FooterText="dgcolhEcode"
                                                                HeaderStyle-Width="120px" ItemStyle-Width="120px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="name" HeaderText="Employee" FooterText="dgcolhEName"
                                                                HeaderStyle-Width="250px" ItemStyle-Width="250px"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="employeeunkid" FooterText="objdgcolhEmpId" Visible="false">
                                                            </asp:BoundColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdf_btnOwrField5Save" runat="server" />
                        <asp:Button ID="btnOwrField5Save" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnOwrField5Cancel" runat="server" Text="Cancel" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_objfrmUpdateFieldValue" runat="server" BackgroundCssClass="modal-backdrop"
                    PopupControlID="pnl_objfrmUpdateFieldValue" TargetControlID="hdf_btnUpdateFieldvalue"
                    CancelControlID="hdf_btnUpdateFieldvalue" PopupDragHandleControlID="pnl_objfrmUpdateFieldValue">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_objfrmUpdateFieldValue" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblpopupHeader" runat="server" Text="Update Progress"></asp:Label>
                            <asp:Label ID="lblDetailUpdateHeader" runat="server" Text="Update Progress" Visible="false"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblUpdatePeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtUpdatePeriod" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblUpdateOwner" runat="server" Text="Owner" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtUpdateOwner" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblUpdateDate" runat="server" Text="Change Date" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <uc2:DateCtrl ID="dtpUpdateChangeDate" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblUpdatePercentage" runat="server" Text="% Completed" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtUpdatePercent" runat="server" Text="0.00" onKeypress="return onlyNumbers(this, event);"
                                                    Style="text-align: right" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblUpdateStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboUpdateStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblUpdateGoals" runat="server" Text="Goal" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtUpdateGoals" runat="server" class="form-control" TextMode="MultiLine"
                                                    Rows="4" Style="resize: none"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblUpdateRemark" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtUpdateRemark" runat="server" class="form-control" TextMode="MultiLine"
                                                    Rows="3" Style="resize: none"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="footer">
                                    <asp:Button ID="btnUpdateSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnUpdateClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                    <asp:HiddenField ID="hdf_btnUpdateFieldvalue" runat="server" />
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                            <div class="table-responsive" style="height: 250px">
                                                <asp:DataGrid ID="dgvHistory" runat="server" AutoGenerateColumns="false" Width="99%"
                                                    AllowPaging="false" CssClass="table table-hover table-bordered">
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center" FooterText="brnEdit">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit"><i class="fas fa-pencil-alt text-primary" ></i></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-HorizontalAlign="Center" FooterText="btnDelete">
                                                            <ItemStyle HorizontalAlign="Center" />
                                                            <ItemTemplate>
                                                                <span class="gridiconbc">
                                                                    <asp:LinkButton ID="ImgDelete" Style="text-align: center;" runat="server" ToolTip="Delete"
                                                                        CommandName="Delete"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:BoundColumn DataField="ddate" HeaderText="Date" FooterText="dgcolhDate"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="pct_completed" HeaderText="% Completed" HeaderStyle-HorizontalAlign="Right"
                                                            ItemStyle-HorizontalAlign="Right" FooterText="dgcolhPercent"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="dstatus" HeaderText="Status" FooterText="dgcolhStatus">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="remark" HeaderText="Remark" FooterText="dgcolhRemark">
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="owrupdatetranunkid" HeaderText="objdgcolhUnkid" Visible="false"
                                                            FooterText="objdgcolhUnkid"></asp:BoundColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popup_UpdateYesNO" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="hdf_UpdateYesNo" PopupControlID="pnl_UpdateYesNO" TargetControlID="hdf_UpdateYesNo">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_UpdateYesNO" runat="server" runat="server" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="Label1" runat="server" Text="Aruti"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblUpdateMessages" runat="server" Text="Message:" CssClass="form-label" />
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnupdateYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnupdateNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_UpdateYesNo" runat="server" />
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hdf_locationx" runat="server" />
                <asp:HiddenField ID="hdf_locationy" runat="server" />
                <cc1:ModalPopupExtender ID="popup_YesNo" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnNo" PopupControlID="pnl_YesNo" TargetControlID="hdf_popupYesNo">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_YesNo" runat="server" runat="server" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblTitle" runat="server" Text="Title"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblMessage" runat="server" Text="Message:" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" Rows="3" Style="resize: none;"
                                            class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_popupYesNo" runat="server" />
                    </div>
                </asp:Panel>
                <ucCfnYesno:Confirmation ID="popup_Commit" runat="server" Message="" Title="Confirmation" />
                <cc1:ModalPopupExtender ID="popup_UnCommit" runat="server" BackgroundCssClass="modal-backdrop"
                    CancelControlID="btnUnCmtNo" PopupControlID="pnl_UnCommit" TargetControlID="hdf_UnCmtYesNo">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_UnCommit" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblUnCmtTitle" runat="server" Text="Title"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblUnCmtMessage" runat="server" Text="Message :" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtUnCmtMessage" runat="server" TextMode="MultiLine" Rows="3" Style="resize: none;"
                                            class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnUnCmtYes" runat="server" Text="Yes" CssClass="btn btn-primary" />
                        <asp:Button ID="btnUnCmtNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_UnCmtYesNo" runat="server" />
                    </div>
                </asp:Panel>
                <asp:HiddenField ID="hdf_topposition" runat="server" />
                <asp:HiddenField ID="hdf_leftposition" runat="server" />
                <ucDel:DeleteReason ID="popup_UpdateProgressDeleteReason" runat="server" Title="Aruti"
                    CancelControlName="hdf_btnUpdateFieldvalue" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
