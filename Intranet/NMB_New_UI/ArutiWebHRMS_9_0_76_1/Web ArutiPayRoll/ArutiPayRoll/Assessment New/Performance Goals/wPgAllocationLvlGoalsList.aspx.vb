﻿#Region " Import "
Imports System.Data
Imports Aruti.Data
Imports System.Drawing

#End Region

Partial Class wPgAllocationLvlGoalsList
    Inherits Basepage

#Region "Private Variables"
    Private ReadOnly mstrModuleName As String = "frmCoyFieldsList"
    Private objOwrField1 As New clsassess_owrfield1_master
    Private objOwrField2 As New clsassess_owrfield2_master
    Private objOwrField3 As New clsassess_owrfield3_master
    Private objOwrField4 As New clsassess_owrfield4_master
    Private objOwrField5 As New clsassess_owrfield5_master
    Private DisplayMessage As New CommonCodes
    Private mblnpopupShow_OwrField1 As Boolean = False
    Private mblnpopupShow_OwrField2 As Boolean = False
    Private mblnpopupShow_OwrField3 As Boolean = False
    Private mblnpopupShow_OwrField4 As Boolean = False
    Private mblnpopupShow_OwrField5 As Boolean = False

    'Nilay (19-Jun-2015) -- Start
    Private mintUpdateProgressIndex As Integer = -1
    Private eType As clsAssess_Field_Mapping.enWeightCheckType
    Private objOUpdateProgress As New clsassess_owrupdate_tran
    Private mintFieldUnkid As Integer = 0
    Private mintLinkedFld As Integer = 0
    Private mintFieldTypeId As Integer = 0
    Private mdtPeriodStartDate As Date = Nothing
    Private mstrDefinedGoal As String = ""
    Private mintTableFieldTranUnkId As Integer = 0
    Private mblnUpdateProgress As Boolean = False
    Private mintOwnerUpdateTranunkid As Integer = 0
    'Nilay (19-Jun-2015) -- End

#End Region

#Region "Page Event"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then

                Call SetFormViewSetting()
                Call FillCombo()
                'S.SANDEEP [28 MAY 2015] -- START
                'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
                Call SetVisibility()
                'S.SANDEEP [28 MAY 2015] -- END
                Me.ViewState.Add("iOwnerRefId", 0)
                Me.ViewState.Add("mintLinkedFieldId", 0)
                Me.ViewState.Add("mdtFinal", Nothing)
                Me.ViewState.Add("AddRowIndex", 0)
                Me.ViewState.Add("EditRowIndex", 0)
                Me.ViewState.Add("DeleteRowIndex", 0)
                Me.ViewState.Add("AddFieldUnkid", "")
                Me.ViewState.Add("DeleteScreenId", "")
                Me.ViewState.Add("DeleteColumnName", "")
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    pnl_Button.Visible = False
                End If
            Else

                'Nilay (19-Jun-2015) -- Start
                mintUpdateProgressIndex = Me.ViewState("mintUpdateProgressIndex")
                
                mintFieldUnkid = Me.ViewState("FieldUnkid")
                mintLinkedFld = Me.ViewState("LinkedFld")
                mintFieldTypeId = Me.ViewState("OwnerFieldTypeId")
                mdtPeriodStartDate = Me.ViewState("PeriodStartDate")
                mstrDefinedGoal = Me.ViewState("DefinedGoal")
                mintTableFieldTranUnkId = Me.ViewState("OwnerFieldUnkid")
                mblnUpdateProgress = Me.ViewState("UpdateProgress")
                mintOwnerUpdateTranunkid = Me.ViewState("OwnerUpdateTranunkid")
                'Nilay (19-Jun-2015) -- End

                If Me.ViewState("mdtFinal") IsNot Nothing Then
                    dgvData.DataSource = CType(Me.ViewState("mdtFinal"), DataTable)
                    dgvData.DataBind()
                End If

                mblnpopupShow_OwrField1 = Me.ViewState("popupShow_OwrField1")
                If mblnpopupShow_OwrField1 Then
                    pop_objfrmAddEditOwrField1.Show()
                End If

                mblnpopupShow_OwrField2 = Me.ViewState("popupShow_OwrField2")
                If mblnpopupShow_OwrField2 Then
                    pop_objfrmAddEditOwrField2.Show()
                End If

                mblnpopupShow_OwrField3 = Me.ViewState("popupShow_OwrField3")
                If mblnpopupShow_OwrField3 Then
                    pop_objfrmAddEditOwrField3.Show()
                End If

                mblnpopupShow_OwrField4 = Me.ViewState("popupShow_OwrField4")
                If mblnpopupShow_OwrField4 Then
                    pop_objfrmAddEditOwrField4.Show()
                End If

                mblnpopupShow_OwrField5 = Me.ViewState("popupShow_OwrField5")
                If mblnpopupShow_OwrField5 Then
                    pop_objfrmAddEditOwrField5.Show()
                End If


                'Nilay (19-Jun-2015) -- Start
                If mblnUpdateProgress Then
                    popup_objfrmUpdateFieldValue.Show()
                End If
                'Nilay (19-Jun-2015) -- End

            End If

            'SHANI (18 JUN 2015) -- Start
            '
            btnUpdatePercentage.Visible = False
            'SHANI (18 JUN 2015) -- End 

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.ViewState("popupShow_OwrField1") Is Nothing Then
            Me.ViewState.Add("popupShow_OwrField1", mblnpopupShow_OwrField1)
        Else
            Me.ViewState("popupShow_OwrField1") = mblnpopupShow_OwrField1
        End If

        If Me.ViewState("popupShow_OwrField2") Is Nothing Then
            Me.ViewState.Add("popupShow_OwrField2", mblnpopupShow_OwrField2)
        Else
            Me.ViewState("popupShow_OwrField2") = mblnpopupShow_OwrField2
        End If

        If Me.ViewState("popupShow_OwrField3") Is Nothing Then
            Me.ViewState.Add("popupShow_OwrField3", mblnpopupShow_OwrField3)
        Else
            Me.ViewState("popupShow_OwrField3") = mblnpopupShow_OwrField3
        End If

        If Me.ViewState("popupShow_OwrField4") Is Nothing Then
            Me.ViewState.Add("popupShow_OwrField4", mblnpopupShow_OwrField4)
        Else
            Me.ViewState("popupShow_OwrField4") = mblnpopupShow_OwrField4
        End If

        If Me.ViewState("popupShow_OwrField5") Is Nothing Then
            Me.ViewState.Add("popupShow_OwrField5", mblnpopupShow_OwrField5)
        Else
            Me.ViewState("popupShow_OwrField5") = mblnpopupShow_OwrField5
        End If

        'Nilay (19-Jun-2015) -- Start
        Me.ViewState("mintUpdateProgressIndex") = mintUpdateProgressIndex
        Me.ViewState.Add("FieldUnkid", mintFieldUnkid)
        Me.ViewState.Add("LinkedFld", mintLinkedFld)
        Me.ViewState.Add("OwnerFieldTypeId", mintFieldTypeId)
        Me.ViewState.Add("PeriodStartDate", mdtPeriodStartDate)
        Me.ViewState.Add("DefinedGoal", mstrDefinedGoal)
        Me.ViewState.Add("OwnerFieldUnkid", mintTableFieldTranUnkId)
        Me.ViewState.Add("UpdateProgress", mblnUpdateProgress)
        Me.ViewState.Add("OwnerUpdateTranunkid", mintOwnerUpdateTranunkid)
        'Nilay (19-Jun-2015) -- End

        'SHANI (16 APR 2015)-START
        '
        lblOwrField1Remark1.ToolTip = lblOwrField1Remark1.Text
        lblOwrField1Remark2.ToolTip = lblOwrField1Remark2.Text
        lblOwrField1Remark3.ToolTip = lblOwrField1Remark3.Text

        lblOwrField2Remark1.ToolTip = lblOwrField2Remark1.Text
        lblOwrField2Remark2.ToolTip = lblOwrField2Remark2.Text
        lblOwrField2Remark3.ToolTip = lblOwrField2Remark3.Text

        lblOwrField3Remark1.ToolTip = lblOwrField3Remark1.Text
        lblOwrField3Remark2.ToolTip = lblOwrField3Remark2.Text
        lblOwrField3Remark3.ToolTip = lblOwrField3Remark3.Text

        lblOwrField4Remark1.ToolTip = lblOwrField4Remark1.Text
        lblOwrField4Remark2.ToolTip = lblOwrField4Remark2.Text
        lblOwrField4Remark3.ToolTip = lblOwrField4Remark3.Text

        lblOwrField5Remark1.ToolTip = lblOwrField5Remark1.Text
        lblOwrField5Remark2.ToolTip = lblOwrField5Remark2.Text
        lblOwrField5Remark3.ToolTip = lblOwrField5Remark3.Text
        'SHANI (16 APR 2015)--END 

    End Sub
#End Region

#Region "Private Methods"

    Private Sub SetFormViewSetting()
        Dim objFMaster = New clsAssess_Field_Master(True)
        Try
            objlblField1.Text = objFMaster._Field1_Caption
            If objlblField1.Text = "" Then
                cboFieldValue1.Enabled = False
            End If
            objlblField2.Text = objFMaster._Field2_Caption
            If objlblField2.Text = "" Then
                cboFieldValue2.Enabled = False
            End If
            objlblField3.Text = objFMaster._Field3_Caption
            If objlblField3.Text = "" Then
                cboFieldValue3.Enabled = False
            End If
            objlblField4.Text = objFMaster._Field4_Caption
            If objlblField4.Text = "" Then
                cboFieldValue4.Enabled = False
            End If
            objlblField5.Text = objFMaster._Field5_Caption
            If objlblField5.Text = "" Then
                cboFieldValue5.Enabled = False
            End If
            'S.SANDEEP [09 FEB 2015] -- START
            btnGlobalAssign.Visible = Session("FollowEmployeeHierarchy")
            'S.SANDEEP [09 FEB 2015] -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Try
            'S.SANDEEP [09 NOV 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, "List", True, 1, , , Session("Database_Name"))

            'S.SANDEEP [10 DEC 2015] -- START
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, _
            '                                   Session("Fin_year"), _
            '                                   Session("Database_Name"), _
            '                                   Session("fin_startdate"), "List", _
            '                                   True, 1)

            'S.SANDEEP |02-MAR-2021| -- START
            'ISSUE/ENHANCEMENT : Competencies Period Changes
            'dsList = objPeriod.getListForCombo(enModuleReference.Assessment, _
            '                                   0, _
            '                                   Session("Database_Name"), _
            '                                   Session("fin_startdate"), "List", _
            '                                   True, 1)

            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, _
                                               0, _
                                               Session("Database_Name"), _
                                               Session("fin_startdate"), "List", _
                                               True, 1, False, False, True)
            'S.SANDEEP |02-MAR-2021| -- END
            
            'S.SANDEEP [10 DEC 2015] -- END

            'Shani(20-Nov-2015) -- End

            'S.SANDEEP [09 NOV 2015] -- END

            'Shani (09-May-2016) -- Start
            Dim intCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            'Shani (09-May-2016) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                Try
                .SelectedValue = intCurrentPeriodId
                Catch ex As Exception
                    .SelectedValue = 0
                End Try
            End With
            'Shani (12-Jan-2017) -- Start
            Call cboPeriod_SelectedIndexChanged(cboPeriod, Nothing)
            'Shani (12-Jan-2017) -- End

            'Shani (09-May-2016) -- Add[.SelectedValue = intCurrentPeriodId]

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Grid()
        Dim dtFinal As DataTable
        Dim mdtFinal As DataTable = Nothing
        Dim iSearch As String = String.Empty
        Dim objFMaster = New clsAssess_Field_Master(True)

        'SHANI [09 Mar 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        Dim intGridWidth As Integer = 0
        'SHANI [09 Mar 2015]--END 

        Try
            If objFMaster.IsPresent(clsAssess_Field_Master.enFieldCheckType.FIELD_DATA) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "Sorry, No data defined for asseessment caption settings screen."), Me)
                Exit Sub
            End If

            Dim objMap As New clsAssess_Field_Mapping
            If objMap.isExist(CInt(cboPeriod.SelectedValue)) = False Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry no field is mapped with the selected period."), Me)
                Exit Sub
            End If
            objMap = Nothing


            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                If CBool(Session("AllowtoViewAllocationGoalsList")) = False Then Exit Sub
            End If
            'S.SANDEEP [28 MAY 2015] -- END


            dtFinal = objOwrField1.GetDisplayList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), "List")

            If dtFinal.Rows.Count <= 0 Then
                Dim iRow As DataRow = dtFinal.NewRow
                dtFinal.Rows.Add(iRow)
            Else
                If CInt(cboOwner.SelectedValue) > 0 Then
                    iSearch &= "AND ownerunkid = '" & CInt(cboOwner.SelectedValue) & "' "
                End If

                If CInt(cboPeriod.SelectedValue) > 0 Then
                    iSearch &= "AND periodunkid = '" & CInt(cboPeriod.SelectedValue) & "' "
                End If

                If cboFieldValue1.Enabled = True AndAlso CInt(cboFieldValue1.SelectedValue) > 0 Then
                    iSearch &= "AND owrfield1unkid = '" & CInt(cboFieldValue1.SelectedValue) & "' "
                End If

                If cboFieldValue2.Enabled = True AndAlso CInt(cboFieldValue2.SelectedValue) > 0 Then
                    iSearch &= "AND owrfield2unkid = '" & CInt(cboFieldValue2.SelectedValue) & "' "
                End If

                If cboFieldValue3.Enabled = True AndAlso CInt(cboFieldValue3.SelectedValue) > 0 Then
                    iSearch &= "AND owrfield3unkid = '" & CInt(cboFieldValue3.SelectedValue) & "' "
                End If

                If cboFieldValue4.Enabled = True AndAlso CInt(cboFieldValue4.SelectedValue) > 0 Then
                    iSearch &= "AND owrfield4unkid = '" & CInt(cboFieldValue4.SelectedValue) & "' "
                End If

                If cboFieldValue5.Enabled = True AndAlso CInt(cboFieldValue5.SelectedValue) > 0 Then
                    iSearch &= "AND owrfield5unkid = '" & CInt(cboFieldValue5.SelectedValue) & "' "
                End If

                If CInt(cboStatus.SelectedValue) > 0 Then
                    iSearch &= "AND CStatusId = '" & CInt(cboStatus.SelectedValue) & "' "
                End If
            End If

            If iSearch.Trim.Length > 0 Then
                iSearch = iSearch.Substring(3)
                mdtFinal = New DataView(dtFinal, iSearch, "Field1Id DESC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
            Else
                mdtFinal = New DataView(dtFinal, "", "Field1Id DESC, Field1 ASC", DataViewRowState.CurrentRows).ToTable
            End If

            dgvData.AutoGenerateColumns = False
            Dim iColName As String = String.Empty

            Dim iPlan() As String = Nothing
            If CStr(Session("ViewTitles_InPlanning")).Trim.Length > 0 Then
                iPlan = CStr(Session("ViewTitles_InPlanning")).Split("|")
            End If


            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            Dim intTotalWidth As Integer = 0
            If iPlan IsNot Nothing Then
                For index As Integer = 0 To iPlan.Length - 1
                    intTotalWidth += objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, iPlan(index), Session("CompnayUnkId"))
                Next
            End If
            'SHANI [09 Mar 2015]--END 


            If dgvData.Columns.Count > 0 Then dgvData.Columns.Clear()
            Call Add_GridColumns()

            For Each dCol As DataColumn In mdtFinal.Columns
                iColName = "" : iColName = "obj" & dCol.ColumnName
                Dim dgvCol As New BoundField()
                dgvCol.FooterText = iColName
                dgvCol.ReadOnly = True
                dgvCol.DataField = dCol.ColumnName
                dgvCol.HeaderText = dCol.Caption
                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                dgvCol.HtmlEncode = False
                'SHANI [01 FEB 2015]--END
                If dgvData.Columns.Contains(dgvCol) = True Then Continue For
                If dCol.Caption.Length <= 0 Then
                    dgvCol.Visible = False
                Else
                    If mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName) IsNot Nothing Then

                        'SHANI [09 Mar 2015]-START
                        'Enhancement - REDESIGN SELF SERVICE.
                        'dgvCol.HeaderStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), Session("CompnayUnkId")))
                        'dgvCol.ItemStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), Session("CompnayUnkId")))
                        'dgvCol.FooterStyle.Width = Unit.Pixel(objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), Session("CompnayUnkId")))

                        'SHANI [21 Mar 2015]-START
                        'Issue : Fixing Issues Sent By Aandrew in PA Testing.
                        'Dim decColumnWidth As Decimal = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), Session("CompnayUnkId")) * 95 / intTotalWidth
                        'dgvCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
                        'dgvCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
                        'dgvCol.FooterStyle.Width = Unit.Percentage(decColumnWidth)
                        Dim decColumnWidth As Decimal = 0
                        If intTotalWidth > 0 Then
                            decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), Session("CompnayUnkId")) * 95 / intTotalWidth
                            dgvCol.HeaderStyle.Width = Unit.Percentage(decColumnWidth)
                            dgvCol.ItemStyle.Width = Unit.Percentage(decColumnWidth)
                            dgvCol.FooterStyle.Width = Unit.Percentage(decColumnWidth)
                        Else
                            decColumnWidth = objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName), Session("CompnayUnkId"))
                            dgvCol.HeaderStyle.Width = Unit.Pixel(decColumnWidth)
                            dgvCol.ItemStyle.Width = Unit.Pixel(decColumnWidth)
                            dgvCol.FooterStyle.Width = Unit.Pixel(decColumnWidth)
                        End If

                        'SHANI [21 Mar 2015]--END 

                        'SHANI [09 Mar 2015]--END
                        If iPlan IsNot Nothing Then
                            If Array.IndexOf(iPlan, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName).ToString) < 0 Then
                                dgvCol.Visible = False
                                ''SHANI [09 Mar 2015]-START
                                ''Enhancement - REDESIGN SELF SERVICE.
                                'intGridWidth += objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName))
                                ''SHANI [09 Mar 2015]--END 
                            End If

                            '    'SHANI [09 Mar 2015]-START
                            '    'Enhancement - REDESIGN SELF SERVICE.
                            'Else
                            '    intGridWidth += objFMaster.GetColumnWidth(clsAssess_Field_Master.enColWidthType.COL_PLAN, mdtFinal.Columns(dCol.ColumnName).ExtendedProperties(dCol.ColumnName))
                            '    'SHANI [09 Mar 2015]--END 

                        End If
                    End If
                End If


                'S.SANDEEP [02 JUL 2015] -- START
                If dCol.ColumnName.StartsWith("CoyField") Then
                    dgvCol.Visible = False
                End If
                'S.SANDEEP [02 JUL 2015] -- END


                'Nilay (19-Jun-2015) -- Start
                If dCol.ColumnName.ToUpper = "VUPROGRESS" Then
                    dgvCol.Visible = False
                    Call Add_UpdateProgressFiled(dCol)
                End If
                'Nilay (19-Jun-2015) -- End

                dgvData.Columns.Add(dgvCol)

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                mdtFinal.Columns(dCol.ColumnName).ExtendedProperties.Add("index", dgvData.Columns.IndexOf(dgvCol))
                'Pinkal (16-Apr-2016) -- End


            Next

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Dim idgvWidth As Integer = 0
            'For i As Integer = 0 To dgvData.Columns.Count - 1
            '    idgvWidth = idgvWidth + CInt(dgvData.Columns(i).ItemStyle.Width.Value)
            'Next
            'SHANI [09 Mar 2015]--END

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'pnl_dgvData.Height = Unit.Pixel(400)
            'SHANI [01 FEB 2015]--END

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'dgvData.Width = Unit.Pixel(idgvWidth)

            'SHANI [09 Mar 2015]--END

            'S.SANDEEP [01 JUL 2015] -- START
            If Me.ViewState("mintUpdateProgressIndex") IsNot Nothing Then
                dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = False
                dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = False
            End If
            'S.SANDEEP [01 JUL 2015] -- END

            If (Session("LoginBy") = Global.User.en_loginby.User) Then
                For Each dgvRow As DataRow In mdtFinal.Rows
                    If CInt(dgvRow("opstatusid")) = enObjective_Status.FINAL_COMMITTED Then
                        dgvData.Columns(0).Visible = False
                        dgvData.Columns(1).Visible = False
                        dgvData.Columns(2).Visible = False

                        'S.SANDEEP [01 JUL 2015] -- START
                        If Me.ViewState("mintUpdateProgressIndex") IsNot Nothing Then
                            dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex"))).Visible = True
                            dgvData.Columns(CInt(Me.ViewState("mintUpdateProgressIndex")) - 1).Visible = True
                        End If
                        'S.SANDEEP [01 JUL 2015] -- END
                    Else
                        'S.SANDEEP [16 JUN 2015] -- START
                        'dgvData.Columns(0).Visible = True
                        'dgvData.Columns(1).Visible = True
                        'dgvData.Columns(2).Visible = True

                        dgvData.Columns(0).Visible = CBool(Session("AllowtoAddAllocationGoals"))
                        dgvData.Columns(1).Visible = CBool(Session("AllowtoEditAllocationGoals"))
                        dgvData.Columns(2).Visible = CBool(Session("AllowtoDeleteAllocationGoals"))
                        'S.SANDEEP [16 JUN 2015] -- END



                        'SHANI [09 Mar 2015]-START
                        'Enhancement - REDESIGN SELF SERVICE.
                        'intGridWidth += 75
                        'SHANI [09 Mar 2015]--END
                    End If
                Next
                pnl_Button.Visible = True
            ElseIf (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                dgvData.Columns(0).Visible = False
                dgvData.Columns(1).Visible = False
                dgvData.Columns(2).Visible = False
            End If

            'SHANI (16 APR 2015)-START
            If CInt(Session("CascadingTypeId")) = enPACascading.STRICT_CASCADING Then
                dgvData.Columns(0).Visible = False
                dgvData.Columns(2).Visible = False
            End If
            'SHANI (16 APR 2015)--END 

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'dgvData.Width = Unit.Pixel(intGridWidth)
            dgvData.Width = Unit.Percentage(99)
            'SHANI [09 Mar 2015]--END 

            'Nilay (19-Jun-2015) -- Start
            Me.ViewState("mdtFinal") = mdtFinal
            'Nilay (19-Jun-2015) -- End

            dgvData.DataSource = mdtFinal
            dgvData.DataBind()

            'Nilay (19-Jun-2015) -- Start
            'Me.ViewState("mdtFinal") = mdtFinal
            'Nilay (19-Jun-2015) -- End

            Dim objFMapping As New clsAssess_Field_Mapping
            Dim xTotalWeight As Double = 0
            xTotalWeight = objFMapping.GetTotalWeight(clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL, CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue), 0)
            If xTotalWeight > 0 Then
                objlblTotalWeight.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "Total Weight Assigned :") & " " & xTotalWeight.ToString
            Else
                objlblTotalWeight.Text = ""
            End If
            objFMapping = Nothing

            'S.SANDEEP [28 MAY 2015] -- START
            'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
            Call SetVisibility()
            'S.SANDEEP [28 MAY 2015] -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Add_GridColumns()
        Try
            '************* ADD
            Dim iTempField As New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            'Shani [ 10 DEC 2014 ] -- START
            '

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'iTempField.ItemStyle.Width = Unit.Pixel(25)
            'SHANI [09 Mar 2015]--END

            'Shani [ 10 DEC 2014 ] -- END
            iTempField.FooterText = "ObjAdd"

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'iTempField.HeaderText = "Add"
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            iTempField.HeaderStyle.Width = Unit.Pixel(50)
            iTempField.ItemStyle.Width = Unit.Pixel(50)
            'SHANI [09 Mar 2015]--END

            'SHANI [01 FEB 2015]--END 
            dgvData.Columns.Add(iTempField)

            '************* EDIT
            iTempField = New TemplateField()
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            'Shani [ 10 DEC 2014 ] -- START
            '

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'iTempField.ItemStyle.Width = Unit.Pixel(25)
            'SHANI [09 Mar 2015]--END

            'Shani [ 10 DEC 2014 ] -- END
            iTempField.FooterText = "objEdit"

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'iTempField.HeaderText = "Edit"
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            iTempField.HeaderStyle.Width = Unit.Pixel(50)
            iTempField.ItemStyle.Width = Unit.Pixel(50)
            'SHANI [09 Mar 2015]--END

            'SHANI [01 FEB 2015]--END 
            dgvData.Columns.Add(iTempField)

            '************* DELETE
            iTempField = New TemplateField()

            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            'Shani [ 10 DEC 2014 ] -- START
            '

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'iTempField.ItemStyle.Width = Unit.Pixel(25)
            'SHANI [09 Mar 2015]--END

            'Shani [ 10 DEC 2014 ] -- END
            iTempField.FooterText = "objDelete"

            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'iTempField.HeaderText = "Delete"
            'SHANI [09 Mar 2015]--END

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            iTempField.HeaderStyle.Width = Unit.Pixel(50)
            iTempField.ItemStyle.Width = Unit.Pixel(50)
            'SHANI [01 FEB 2015]--END 

            dgvData.Columns.Add(iTempField)

            For Each gRow As GridViewRow In dgvData.Rows

                If gRow.Cells(0).FindControl("imgAdd") Is Nothing Then
                    Dim imgAdd As New LinkButton
                    imgAdd.ID = "imgAdd"
                    imgAdd.CommandName = "objAdd"
                    imgAdd.ToolTip = "New"
                    imgAdd.CssClass = "lnAdd"
                    gRow.Cells(0).Controls.Add(imgAdd)
                End If

                If gRow.Cells(1).FindControl("imgEdit") Is Nothing Then
                    Dim imgEdit As New LinkButton
                    imgEdit.ID = "imgEdit"
                    imgEdit.CommandName = "objEdit"
                    imgEdit.ToolTip = "Edit"
                    imgEdit.CssClass = "lnEdit"
                    gRow.Cells(1).Controls.Add(imgEdit)
                End If

                If gRow.Cells(2).FindControl("imgDelete") Is Nothing Then
                    Dim imgDelete As New LinkButton
                    imgDelete.ID = "imgDelete"
                    imgDelete.CommandName = "objDelete"
                    imgDelete.ToolTip = "Delete"
                    imgDelete.CssClass = "lnDelt"
                    gRow.Cells(2).Controls.Add(imgDelete)
                End If
            Next

            dgvData.Columns(0).Visible = CBool(Session("AllowtoAddAllocationGoals"))
            dgvData.Columns(1).Visible = CBool(Session("AllowtoEditAllocationGoals"))
            dgvData.Columns(2).Visible = CBool(Session("AllowtoDeleteAllocationGoals"))

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Add_UpdateProgressFiled(ByVal dcol As DataColumn)
        Try
            Dim iTempField As New TemplateField()
            iTempField.FooterText = dcol.ColumnName
            iTempField.HeaderText = dcol.Caption
            iTempField.HeaderStyle.Width = Unit.Pixel(70)
            iTempField.ItemStyle.Width = Unit.Pixel(70)
            'SHANI (23 JUN 2015) -- Start
            iTempField.HeaderStyle.HorizontalAlign = HorizontalAlign.Center
            iTempField.ItemStyle.HorizontalAlign = HorizontalAlign.Center
            'SHANI (23 JUN 2015) -- End 
            dgvData.Columns.Add(iTempField)
            mintUpdateProgressIndex = dgvData.Columns.IndexOf(iTempField)

            'S.SANDEEP [01 JUL 2015] -- START
            Me.ViewState("mintUpdateProgressIndex") = mintUpdateProgressIndex
            'S.SANDEEP [01 JUL 2015] -- END

            'S.SANDEEP [16 JUN 2015] -- START
            dgvData.Columns(mintUpdateProgressIndex).Visible = CBool(Session("AllowtoUpdatePercentCompletedAllocationGoals"))
            'S.SANDEEP [16 JUN 2015] -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Genrate_AddmanuItem()
        Try
            Dim dsList As New DataSet
            Dim objFMaster As New clsAssess_Field_Master
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            Dim iclmid As New DataColumn("lnkId", Type.GetType("System.String"))
            Dim iclmname As New DataColumn("lnkName", Type.GetType("System.String"))
            Dim iclmtag As New DataColumn("lnkTag", Type.GetType("System.String"))
            dsList.Tables(0).Columns.Add(iclmid)
            dsList.Tables(0).Columns.Add(iclmname)
            dsList.Tables(0).Columns.Add(iclmtag)
            If dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    dsList.Tables(0).Rows(i)("lnkId") = "objbtnAdd" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    dsList.Tables(0).Rows(i)("lnkName") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Add") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    dsList.Tables(0).Rows(i)("lnkTag") = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                Next
            End If
            dlmnuAdd.DataSource = dsList
            dlmnuAdd.DataBind()

            objFMaster = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Genrate_EditmanuItem()
        Try
            Dim dsList As New DataSet
            Dim objFMaster As New clsAssess_Field_Master
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            Dim iclmid As New DataColumn("lnkId", Type.GetType("System.String"))
            Dim iclmname As New DataColumn("lnkName", Type.GetType("System.String"))
            Dim iclmtag As New DataColumn("lnkTag", Type.GetType("System.String"))
            Dim iclmOrgname As New DataColumn("lnkOriginal", Type.GetType("System.String"))
            dsList.Tables(0).Columns.Add(iclmid)
            dsList.Tables(0).Columns.Add(iclmname)
            dsList.Tables(0).Columns.Add(iclmtag)
            dsList.Tables(0).Columns.Add(iclmOrgname)
            If dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    dsList.Tables(0).Rows(i)("lnkId") = "objbtnEdt" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    dsList.Tables(0).Rows(i)("lnkName") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 30, "Edit") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    dsList.Tables(0).Rows(i)("lnkTag") = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    dsList.Tables(0).Rows(i)("lnkOriginal") = dsList.Tables(0).Rows(i)("fieldcaption").ToString
                Next
            End If
            dlmnuEdit.DataSource = dsList
            dlmnuEdit.DataBind()
            objFMaster = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Genrate_DeletemanuItem()
        Try
            Dim dsList As New DataSet
            Dim objFMaster As New clsAssess_Field_Master
            dsList = objFMaster.Get_Field_Mapping("List", , True)
            Dim iclmid As New DataColumn("lnkId", Type.GetType("System.String"))
            Dim iclmname As New DataColumn("lnkName", Type.GetType("System.String"))
            Dim iclmtag As New DataColumn("lnkTag", Type.GetType("System.String"))
            Dim iclmOrgname As New DataColumn("lnkOriginal", Type.GetType("System.String"))
            dsList.Tables(0).Columns.Add(iclmid)
            dsList.Tables(0).Columns.Add(iclmname)
            dsList.Tables(0).Columns.Add(iclmtag)
            dsList.Tables(0).Columns.Add(iclmOrgname)
            If dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    dsList.Tables(0).Rows(i)("lnkId") = "objbtnDel" & dsList.Tables(0).Rows(i).Item("fieldunkid").ToString
                    dsList.Tables(0).Rows(i)("lnkName") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 31, "Delete") & " " & dsList.Tables(0).Rows(i)("fieldcaption").ToString
                    dsList.Tables(0).Rows(i)("lnkTag") = dsList.Tables(0).Rows(i)("fieldunkid").ToString & "|" & dsList.Tables(0).Rows(i)("ExOrder").ToString
                    dsList.Tables(0).Rows(i)("lnkOriginal") = dsList.Tables(0).Rows(i)("fieldcaption").ToString
                Next
            End If
            dlmnuDelete.DataSource = dsList
            dlmnuDelete.DataBind()

            objFMaster = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub OwrField1_load(ByVal p_mintOwrField1Unkid As Integer, ByVal p_enOwrField1Action As enAction)
        Dim objOwrOwner As New clsassess_owrowner_tran
        Dim mdtOwrField1Owner As New DataTable
        Dim mintOwrField1Unkid As Integer = p_mintOwrField1Unkid
        Dim mdicOwrField1FieldData As New Dictionary(Of Integer, String)
        Dim mintOwrField1FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0))
        Dim mintOwrField1LinkedFieldId As Integer = 0
        Try
            mblnpopupShow_OwrField1 = True
            mdtOwrField1Owner = objOwrOwner.Get_Data(mintOwrField1Unkid, enWeight_Types.WEIGHT_FIELD1)
            Me.ViewState.Add("mdtOwrField1Owner", mdtOwrField1Owner)
            Me.ViewState.Add("mintOwrField1Unkid", mintOwrField1Unkid)
            Me.ViewState.Add("mdicOwrField1FieldData", mdicOwrField1FieldData)
            Me.ViewState.Add("mintOwrField1FieldUnkid", mintOwrField1FieldUnkid)
            Me.ViewState.Add("enOwrField1Action", p_enOwrField1Action)
            Dim objMapping As New clsAssess_Field_Mapping
            mintOwrField1LinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            objMapping = Nothing

            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            txtOwrField1Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If mintOwrField1LinkedFieldId <> mintOwrField1FieldUnkid Then
                objOwrField1tabcRemarks.Enabled = False : pnl_RightOwrField1.Enabled = False

                dtpOwrField1StartDate.Enabled = False
                dtpOwrField1EndDate.Enabled = False
                objOwrField1tabpRemark1.Visible = False
                objOwrField1tabpRemark2.Visible = False
                objOwrField1tabpRemark3.Visible = False

            End If

            Me.ViewState.Add("mintOwrField1LinkedFieldId", mintOwrField1LinkedFieldId)

            If p_enOwrField1Action = enAction.EDIT_ONE Then
                objOwrField1._Owrfield1unkid = mintOwrField1Unkid
                cboOwrField1Allocations.Enabled = False
            End If

            Call Set_OwrField1Form_Information()
            Call FillCombo_OwrField1()
            Call Fill_Data_OwrField1()
            Call GetOwerField1Value()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub OwrField2_load(ByVal p_mintOwrField2Unkid As Integer, ByVal p_enOwrField2Action As enAction, Optional ByVal p_mintOwrField2ParentId As Integer = 0)
        Dim objOwrOwner As New clsassess_owrowner_tran
        Dim mintOwrField2Unkid As Integer = p_mintOwrField2Unkid 'Owner Field2 unkId (Identity Column)
        Dim menOwrField2Action As enAction = p_enOwrField2Action 'Owner Field2 Actions
        Dim mdicOwrField2FieldData As New Dictionary(Of Integer, String) 'Owner Field2 Dictionary
        Dim mintOwrField2ParentId As Integer = p_mintOwrField2ParentId 'Owner Field2 ParentID
        Dim mdtOwrField2Owner As DataTable 'Owner Field2 Table
        Dim mintOwrField2FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Owner Field2 FieldunkID (Assement Caption FieldID)
        Dim mintOwrField2LinkedFieldId As Integer = -1  'Owner Field2 LinkedFieldID
        Try
            mblnpopupShow_OwrField2 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintOwrField2LinkedFieldId = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            objMapping = Nothing

            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            txtOwrField2Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If mintOwrField2LinkedFieldId <> mintOwrField2FieldUnkid Then
                objOwrField2tabcRemarks.Enabled = False : pnl_RightOwrField2.Enabled = False
                dtpOwrField2StartDate.Enabled = False
                dtpOwrField2EndDate.Enabled = False
                objOwrField2tabpRemark1.Visible = False
                objOwrField2tabpRemark2.Visible = False
                objOwrField2tabpRemark3.Visible = False
            End If

            If menOwrField2Action = enAction.EDIT_ONE Then
                objOwrField2._Owrfield2unkid = mintOwrField2Unkid
            End If
            mdtOwrField2Owner = objOwrOwner.Get_Data(mintOwrField2Unkid, enWeight_Types.WEIGHT_FIELD2)


            Me.ViewState.Add("mdtOwrField2Owner", mdtOwrField2Owner)
            Me.ViewState.Add("mintOwrField2Unkid", mintOwrField2Unkid)
            Me.ViewState.Add("menOwrField2Action", menOwrField2Action)
            Me.ViewState.Add("mdicOwrField2FieldData", mdicOwrField2FieldData)
            Me.ViewState.Add("mintOwrField2FieldUnkid", mintOwrField2FieldUnkid)
            Me.ViewState.Add("mintOwrField2LinkedFieldId", mintOwrField2LinkedFieldId)
            Me.ViewState.Add("mintOwrField2ParentId", mintOwrField2ParentId)


            Call Set_OwrField2Form_Information()
            Call Fill_Data_OwrField2()
            Call FillCombo_OwrField2()
            Call GetOwrField2Value()
            If mintOwrField2ParentId > 0 Then
                cboOwrField2OwrFieldValue1.SelectedValue = mintOwrField2ParentId
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub OwrField3_load(ByVal p_mintOwrField3Unkid As Integer, ByVal p_enOwrField3Action As enAction, Optional ByVal p_mintOwrField3ParentId As Integer = 0, Optional ByVal p_mintOwrField3MainParentId As Integer = 0)
        Dim objOwrOwner As New clsassess_owrowner_tran
        Dim mintOwrField3Unkid As Integer = p_mintOwrField3Unkid 'Owner Field3 unkId (Identity Column)
        Dim menOwrField3Action As enAction = p_enOwrField3Action 'Owner Field3 Actions
        Dim mdicOwrField3FieldData As New Dictionary(Of Integer, String) 'Owner Field3 Dictionary
        Dim mintOwrField3ParentId As Integer = p_mintOwrField3ParentId 'Owner Field3 ParentID
        Dim mintOwrField3MainParentId As Integer = p_mintOwrField3MainParentId 'Owner Field3 Main ParentID
        Dim mdtOwrField3Owner As New DataTable 'Owner Field3 Table
        Dim mintOwrField3FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Owner Field3 FieldunkID (Assement Caption FieldID)
        Dim mintOwrField3LinkedFieldId As Integer = -1  'Owner Field3 LinkedFieldID
        Try
            mblnpopupShow_OwrField3 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintOwrField3LinkedFieldId = objMapping.Get_Map_FieldId(cboPeriod.SelectedValue)
            objMapping = Nothing

            If mintOwrField3LinkedFieldId <> mintOwrField3FieldUnkid Then
                objOwrField3tabcRemarks.Enabled = False : pnl_RightOwrField3.Enabled = False

                dtpOwrField3StartDate.Enabled = False
                dtpOwrField3EndDate.Enabled = False
                objOwrField3tabpRemark1.Visible = False
                objOwrField3tabpRemark2.Visible = False
                objOwrField3tabpRemark3.Visible = False

            End If

            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            txtOwrField3Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If menOwrField3Action = enAction.EDIT_ONE Then
                objOwrField3._Owrfield3unkid = mintOwrField3Unkid
            End If
            mdtOwrField3Owner = objOwrOwner.Get_Data(mintOwrField3Unkid, enWeight_Types.WEIGHT_FIELD3)

            Me.ViewState.Add("mdtOwrField3Owner", mdtOwrField3Owner)
            Me.ViewState.Add("mintOwrField3Unkid", mintOwrField3Unkid)
            Me.ViewState.Add("menOwrField3Action", menOwrField3Action)
            Me.ViewState.Add("mdicOwrField3FieldData", mdicOwrField3FieldData)
            Me.ViewState.Add("mintOwrField3FieldUnkid", mintOwrField3FieldUnkid)
            Me.ViewState.Add("mintOwrField3LinkedFieldId", mintOwrField3LinkedFieldId)
            Me.ViewState.Add("mintOwrField3ParentId", mintOwrField3ParentId)
            Me.ViewState.Add("mintOwrField3MainParentId", mintOwrField3MainParentId)

            Call Set_OwrField3Form_Information()
            Call Fill_Data_OwrField3()
            Call FillCombo_OwerField3()
            Call GetOwrField3Value()
            If mintOwrField3ParentId > 0 Then
                cboOwrField3OwrFieldValue2.SelectedValue = mintOwrField3ParentId
                Call cboOwrField3OwrFieldValue2_SelectedIndexChanged(cboOwrField3OwrFieldValue2, Nothing)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub OwrField4_load(ByVal p_mintOwrField4Unkid As Integer, ByVal p_enOwrField4Action As enAction, Optional ByVal p_mintOwrField4ParentId As Integer = 0, Optional ByVal p_mintOwrField4MainParentId As Integer = 0)
        Dim objOwrOwner As New clsassess_owrowner_tran
        Dim mintOwrField4Unkid As Integer = p_mintOwrField4Unkid 'Owner Field4 unkId (Identity Column)
        Dim menOwrField4Action As enAction = p_enOwrField4Action 'Owner Field4 Actions
        Dim mdicOwrField4FieldData As New Dictionary(Of Integer, String) 'Owner Field4 Dictionary
        Dim mintOwrField4ParentId As Integer = p_mintOwrField4ParentId 'Owner Field4 ParentID
        Dim mintOwrField4MainParentId As Integer = p_mintOwrField4MainParentId 'Owner Field4 Main ParentID
        Dim mdtOwrField4Owner As New DataTable 'Owner Field4 Table
        Dim mintOwrField4FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Owner Field4 FieldunkID (Assement Caption FieldID)
        Dim mintOwrField4LinkedFieldId As Integer = -1  'Owner Field4 LinkedFieldID
        Try
            mblnpopupShow_OwrField4 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintOwrField4LinkedFieldId = objMapping.Get_Map_FieldId(cboPeriod.SelectedValue)
            objMapping = Nothing

            If mintOwrField4FieldUnkid <> mintOwrField4LinkedFieldId Then
                objOwrField4tabcRemarks.Enabled = False : pnl_RightOwrField4.Enabled = False

                dtpOwrField4StartDate.Enabled = False
                dtpOwrField4EndDate.Enabled = False
                objOwrField4tabpRemark1.Visible = False
                objOwrField4tabpRemark2.Visible = False
                objOwrField4tabpRemark3.Visible = False

            End If

            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            txtOwrField4Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If menOwrField4Action = enAction.EDIT_ONE Then
                objOwrField4._Owrfield4unkid = mintOwrField4Unkid
            End If
            mdtOwrField4Owner = objOwrOwner.Get_Data(mintOwrField4Unkid, enWeight_Types.WEIGHT_FIELD4)

            Me.ViewState.Add("mdtOwrField4Owner", mdtOwrField4Owner)
            Me.ViewState.Add("mintOwrField4Unkid", mintOwrField4Unkid)
            Me.ViewState.Add("menOwrField4Action", menOwrField4Action)
            Me.ViewState.Add("mdicOwrField4FieldData", mdicOwrField4FieldData)
            Me.ViewState.Add("mintOwrField4FieldUnkid", mintOwrField4FieldUnkid)
            Me.ViewState.Add("mintOwrField4LinkedFieldId", mintOwrField4LinkedFieldId)
            Me.ViewState.Add("mintOwrField4ParentId", mintOwrField4ParentId)
            Me.ViewState.Add("mintOwrField4MainParentId", mintOwrField4MainParentId)

            Call Set_OwrField4Form_Information()
            Call Fill_Data_OwrField4()
            Call FillCombo_OwrField4()
            Call GetOwrField4Value()
            If mintOwrField4ParentId > 0 Then
                cboOwrField4OwrFieldValue3.SelectedValue = mintOwrField4ParentId
                Call cboOwrField4OwrFieldValue3_SelectedIndexChanged(cboOwrField4OwrFieldValue3, Nothing)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub OwrField5_load(ByVal p_mintOwrField5Unkid As Integer, ByVal p_enOwrField5Action As enAction, Optional ByVal p_mintOwrField5ParentId As Integer = 0, Optional ByVal p_mintOwrField5MainParentId As Integer = 0)
        Dim objOwrOwner As New clsassess_owrowner_tran
        Dim mintOwrField5Unkid As Integer = p_mintOwrField5Unkid 'Owner Field5 unkId (Identity Column)
        Dim menOwrField5Action As enAction = p_enOwrField5Action 'Owner Field5 Actions
        Dim mdicOwrField5FieldData As New Dictionary(Of Integer, String) 'Owner Field5 Dictionary
        Dim mintOwrField5ParentId As Integer = p_mintOwrField5ParentId 'Owner Field5 ParentID
        Dim mintOwrField5MainParentId As Integer = p_mintOwrField5MainParentId 'Owner Field5 Main ParentID
        Dim mdtOwrField5Owner As New DataTable 'Owner Field5 Table
        Dim mintOwrField5FieldUnkid As Integer = CInt(Me.ViewState("AddFieldUnkid").ToString().Split("|")(0)) 'Owner Field5 FieldunkID (Assement Caption FieldID)
        Dim mintOwrField5LinkedFieldId As Integer = -1  'Owner Field5 LinkedFieldID
        Try
            mblnpopupShow_OwrField5 = True
            Dim objMapping As New clsAssess_Field_Mapping
            mintOwrField5LinkedFieldId = objMapping.Get_Map_FieldId(cboPeriod.SelectedValue)
            objMapping = Nothing

            If mintOwrField5LinkedFieldId <> mintOwrField5FieldUnkid Then
                objOwrField5tabcRemarks.Enabled = False : pnl_RightOwrField5.Enabled = False

                dtpOwrField5StartDate.Enabled = False
                dtpOwrField5EndDate.Enabled = False
                objOwrField5tabpRemark1.Visible = False
                objOwrField5tabpRemark2.Visible = False
                objOwrField5tabpRemark3.Visible = False


            End If

            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            txtOwrField5Period.Text = objPrd._Period_Name
            objPrd = Nothing

            If menOwrField5Action = enAction.EDIT_ONE Then
                objOwrField5._Owrfield5unkid = mintOwrField5Unkid
            End If
            mdtOwrField5Owner = objOwrOwner.Get_Data(mintOwrField5Unkid, enWeight_Types.WEIGHT_FIELD5)

            Me.ViewState.Add("mdtOwrField5Owner", mdtOwrField5Owner)
            Me.ViewState.Add("mintOwrField5Unkid", mintOwrField5Unkid)
            Me.ViewState.Add("menOwrField5Action", menOwrField5Action)
            Me.ViewState.Add("mdicOwrField5FieldData", mdicOwrField5FieldData)
            Me.ViewState.Add("mintOwrField5FieldUnkid", mintOwrField5FieldUnkid)
            Me.ViewState.Add("mintOwrField5LinkedFieldId", mintOwrField5LinkedFieldId)
            Me.ViewState.Add("mintOwrField5ParentId", mintOwrField5ParentId)
            Me.ViewState.Add("mintOwrField5MainParentId", mintOwrField5MainParentId)

            Call Set_OwrField5Form_Information()
            Call Fill_Data_OwrField5()
            Call FillCombo_OwrField5()
            Call GetOwrField5Value()

            If mintOwrField5ParentId > 0 Then
                cboOwrField5OwrFieldValue4.SelectedValue = mintOwrField5ParentId
                Call cboOwrField5OwrFieldValue4_SelectedIndexChanged(cboOwrField5OwrFieldValue4, Nothing)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCombo_OwrField1()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objCoyField1 As New clsassess_coyfield1_master
        Try
            dsList = objMData.GetEAllocation_Notification("List")
            Dim dtTable As DataTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
            With cboOwrField1Allocations
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
                .Enabled = False
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboOwrField1Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objCoyField1.getComboList(CInt(cboPeriod.SelectedValue), "List", True, True)
            With cboOwrField1FieldValue1
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCombo_OwrField2()
        Dim objMData As New clsMasterData
        Dim objOwrField1 As New clsassess_owrfield1_master
        Dim dsList As New DataSet
        Try

            'S.SANDEEP |05-APR-2019| -- START
            'dsList = objOwrField1.getComboList("List", True, CInt(cboPeriod.SelectedValue), , CInt(cboOwner.SelectedValue))
            dsList = objOwrField1.getComboList(eZeeDate.convertDate(Session("EmployeeAsOnDate")), "List", True, CInt(cboPeriod.SelectedValue), , CInt(cboOwner.SelectedValue))
            'S.SANDEEP |05-APR-2019| -- END
            With cboOwrField2OwrFieldValue1
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboOwrField2Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo_OwerField3()
        Dim objMData As New clsMasterData
        Dim objOwrField2 As New clsassess_owrfield2_master
        Dim dsList As New DataSet
        Try

            dsList = objOwrField2.getComboList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintOwrField3MainParentId")), "List", True)
            With cboOwrField3OwrFieldValue2
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboOwrField3Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo_OwrField4()
        Dim objMData As New clsMasterData
        Dim objOwrField3 As New clsassess_owrfield3_master
        Dim dsList As New DataSet
        Try
            dsList = objOwrField3.getComboList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintOwrField4MainParentId")), "List", True)
            With cboOwrField4OwrFieldValue3
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboOwrField4Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillCombo_OwrField5()
        Dim objMData As New clsMasterData
        Dim objField4 As New clsassess_owrfield4_master
        Dim dsList As New DataSet
        Try
            dsList = objField4.getComboList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintOwrField5MainParentId")), "List", True)
            With cboOwrField5OwrFieldValue4
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboOwrField5Status
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = enCompGoalStatus.ST_PENDING
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_OwrField1()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim strSearch As String = ""
        Try
            Dim mintOwnerId As Integer = CInt(cboOwner.SelectedValue)
            Dim mdtOwrField1Owner As DataTable = CType(Me.ViewState("mdtOwrField1Owner"), DataTable)
            Dim iFilter As String = "hremployee_master.isapproved = 1"
            Select Case CInt(cboOwnerType.SelectedValue)
                Case enAllocation.BRANCH
                    iFilter &= " AND hremployee_master.stationunkid = '" & mintOwnerId & "' "
                Case enAllocation.DEPARTMENT_GROUP
                    iFilter &= " AND hremployee_master.deptgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.DEPARTMENT
                    iFilter &= " AND hremployee_master.departmentunkid = '" & mintOwnerId & "' "
                Case enAllocation.SECTION_GROUP
                    iFilter &= " AND hremployee_master.sectiongroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.SECTION
                    iFilter &= " AND hremployee_master.sectionunkid = '" & mintOwnerId & "' "
                Case enAllocation.UNIT_GROUP
                    iFilter &= " AND hremployee_master.unitgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.UNIT
                    iFilter &= " AND hremployee_master.unitunkid = '" & mintOwnerId & "' "
                Case enAllocation.TEAM
                    iFilter &= " AND hremployee_master.teamunkid = '" & mintOwnerId & "' "
                Case enAllocation.JOB_GROUP
                    iFilter &= " AND hremployee_master.jobgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.JOBS
                    iFilter &= " AND hremployee_master.jobunkid = '" & mintOwnerId & "' "
                Case enAllocation.CLASS_GROUP
                    iFilter &= " AND hremployee_master.classgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.CLASSES
                    iFilter &= " AND hremployee_master.classunkid = '" & mintOwnerId & "' "
            End Select

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dList = objEmployee.GetList("iList", False, True, eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , Session("AccessLevelFilterString"), , iFilter)
            dList = objEmployee.GetList(Session("Database_Name"), _
                                        Session("UserId"), _
                                        Session("Fin_year"), _
                                        Session("CompanyUnkId"), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                        Session("UserAccessModeSetting"), _
                                        True, False, "iList", _
                                        Session("ShowFirstAppointmentDate"), , , _
                                        iFilter)
            'Shani(24-Aug-2015) -- End
            If dList.Tables(0).Rows.Count > 0 Then
                dList.Tables(0).Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    dtRow.Item("ischeck") = False
                    If mdtOwrField1Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtOwrField1Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,name"

            If txtOwrField1SearchEmp.Text.Trim.Length > 0 Then
                strSearch = "employeecode LIKE '%" & txtOwrField1SearchEmp.Text & "%' OR " & _
                            "name LIKE '%" & txtOwrField1SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If

            dgvOwrField1Owner.DataSource = dtOwnerView
            dgvOwrField1Owner.DataBind()
            Me.ViewState("mdtOwrField1Owner") = mdtOwrField1Owner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_OwrField2()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim strSearch As String = ""
        Try
            Dim mintOwnerId As Integer = CInt(cboOwner.SelectedValue)
            Dim mdtOwrField2Owner As DataTable = CType(Me.ViewState("mdtOwrField2Owner"), DataTable)
            Dim iFilter As String = "hremployee_master.isapproved = 1"
            Select Case CInt(cboOwnerType.SelectedValue)
                Case enAllocation.BRANCH
                    iFilter &= " AND hremployee_master.stationunkid = '" & mintOwnerId & "' "
                Case enAllocation.DEPARTMENT_GROUP
                    iFilter &= " AND hremployee_master.deptgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.DEPARTMENT
                    iFilter &= " AND hremployee_master.departmentunkid = '" & mintOwnerId & "' "
                Case enAllocation.SECTION_GROUP
                    iFilter &= " AND hremployee_master.sectiongroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.SECTION
                    iFilter &= " AND hremployee_master.sectionunkid = '" & mintOwnerId & "' "
                Case enAllocation.UNIT_GROUP
                    iFilter &= " AND hremployee_master.unitgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.UNIT
                    iFilter &= " AND hremployee_master.unitunkid = '" & mintOwnerId & "' "
                Case enAllocation.TEAM
                    iFilter &= " AND hremployee_master.teamunkid = '" & mintOwnerId & "' "
                Case enAllocation.JOB_GROUP
                    iFilter &= " AND hremployee_master.jobgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.JOBS
                    iFilter &= " AND hremployee_master.jobunkid = '" & mintOwnerId & "' "
                Case enAllocation.CLASS_GROUP
                    iFilter &= " AND hremployee_master.classgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.CLASSES
                    iFilter &= " AND hremployee_master.classunkid = '" & mintOwnerId & "' "
            End Select

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dList = objEmployee.GetList("iList", False, True, eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , Session("AccessLevelFilterString"), , iFilter)
            dList = objEmployee.GetList(Session("Database_Name"), _
                                        Session("UserId"), _
                                        Session("Fin_year"), _
                                        Session("CompanyUnkId"), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                        Session("UserAccessModeSetting"), _
                                        True, False, "iList", _
                                        Session("ShowFirstAppointmentDate"), , , _
                                        iFilter)
            'Shani(24-Aug-2015) -- End
            If dList.Tables(0).Rows.Count > 0 Then
                dList.Tables(0).Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    dtRow.Item("ischeck") = False
                    If mdtOwrField2Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtOwrField2Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,name"

            If txtOwrField2SearchEmp.Text.Trim.Length > 0 Then
                strSearch = "employeecode LIKE '%" & txtOwrField2SearchEmp.Text & "%' OR " & _
                            "name LIKE '%" & txtOwrField2SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If

            dgvOwrField2Owner.DataSource = dtOwnerView
            dgvOwrField2Owner.DataBind()
            Me.ViewState("mdtOwrField2Owner") = mdtOwrField2Owner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_OwrField3()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim strSearch As String = ""
        Try
            Dim mintOwnerId As Integer = CInt(cboOwner.SelectedValue)
            Dim mdtOwrField3Owner As DataTable = CType(Me.ViewState("mdtOwrField3Owner"), DataTable)
            Dim iFilter As String = "hremployee_master.isapproved = 1"
            Select Case CInt(cboOwnerType.SelectedValue)
                Case enAllocation.BRANCH
                    iFilter &= " AND hremployee_master.stationunkid = '" & mintOwnerId & "' "
                Case enAllocation.DEPARTMENT_GROUP
                    iFilter &= " AND hremployee_master.deptgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.DEPARTMENT
                    iFilter &= " AND hremployee_master.departmentunkid = '" & mintOwnerId & "' "
                Case enAllocation.SECTION_GROUP
                    iFilter &= " AND hremployee_master.sectiongroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.SECTION
                    iFilter &= " AND hremployee_master.sectionunkid = '" & mintOwnerId & "' "
                Case enAllocation.UNIT_GROUP
                    iFilter &= " AND hremployee_master.unitgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.UNIT
                    iFilter &= " AND hremployee_master.unitunkid = '" & mintOwnerId & "' "
                Case enAllocation.TEAM
                    iFilter &= " AND hremployee_master.teamunkid = '" & mintOwnerId & "' "
                Case enAllocation.JOB_GROUP
                    iFilter &= " AND hremployee_master.jobgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.JOBS
                    iFilter &= " AND hremployee_master.jobunkid = '" & mintOwnerId & "' "
                Case enAllocation.CLASS_GROUP
                    iFilter &= " AND hremployee_master.classgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.CLASSES
                    iFilter &= " AND hremployee_master.classunkid = '" & mintOwnerId & "' "
            End Select

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dList = objEmployee.GetList("iList", False, True, eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , Session("AccessLevelFilterString"), , iFilter)
            dList = objEmployee.GetList(Session("Database_Name"), _
                                        Session("UserId"), _
                                        Session("Fin_year"), _
                                        Session("CompanyUnkId"), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                        Session("UserAccessModeSetting"), _
                                        True, False, "iList", _
                                        Session("ShowFirstAppointmentDate"), , , _
                                        iFilter)
            'Shani(24-Aug-2015) -- End
            If dList.Tables(0).Rows.Count > 0 Then
                dList.Tables(0).Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    dtRow.Item("ischeck") = False
                    If mdtOwrField3Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtOwrField3Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,name"

            If txtOwrField3SearchEmp.Text.Trim.Length > 0 Then
                strSearch = "employeecode LIKE '%" & txtOwrField3SearchEmp.Text & "%' OR " & _
                            "name LIKE '%" & txtOwrField3SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If

            dgvOwrField3Owner.DataSource = dtOwnerView
            dgvOwrField3Owner.DataBind()
            Me.ViewState("mdtOwrField3Owner") = mdtOwrField3Owner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_OwrField4()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim strSearch As String = ""
        Try
            Dim mintOwnerId As Integer = CInt(cboOwner.SelectedValue)
            Dim mdtOwrField4Owner As DataTable = CType(Me.ViewState("mdtOwrField4Owner"), DataTable)
            Dim iFilter As String = "hremployee_master.isapproved = 1"
            Select Case CInt(cboOwnerType.SelectedValue)
                Case enAllocation.BRANCH
                    iFilter &= " AND hremployee_master.stationunkid = '" & mintOwnerId & "' "
                Case enAllocation.DEPARTMENT_GROUP
                    iFilter &= " AND hremployee_master.deptgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.DEPARTMENT
                    iFilter &= " AND hremployee_master.departmentunkid = '" & mintOwnerId & "' "
                Case enAllocation.SECTION_GROUP
                    iFilter &= " AND hremployee_master.sectiongroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.SECTION
                    iFilter &= " AND hremployee_master.sectionunkid = '" & mintOwnerId & "' "
                Case enAllocation.UNIT_GROUP
                    iFilter &= " AND hremployee_master.unitgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.UNIT
                    iFilter &= " AND hremployee_master.unitunkid = '" & mintOwnerId & "' "
                Case enAllocation.TEAM
                    iFilter &= " AND hremployee_master.teamunkid = '" & mintOwnerId & "' "
                Case enAllocation.JOB_GROUP
                    iFilter &= " AND hremployee_master.jobgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.JOBS
                    iFilter &= " AND hremployee_master.jobunkid = '" & mintOwnerId & "' "
                Case enAllocation.CLASS_GROUP
                    iFilter &= " AND hremployee_master.classgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.CLASSES
                    iFilter &= " AND hremployee_master.classunkid = '" & mintOwnerId & "' "
            End Select

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dList = objEmployee.GetList("iList", False, True, eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , Session("AccessLevelFilterString"), , iFilter)
            dList = objEmployee.GetList(Session("Database_Name"), _
                                        Session("UserId"), _
                                        Session("Fin_year"), _
                                        Session("CompanyUnkId"), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                        Session("UserAccessModeSetting"), _
                                        True, False, "iList", _
                                        Session("ShowFirstAppointmentDate"), , , _
                                        iFilter)
            'Shani(24-Aug-2015) -- End
            If dList.Tables(0).Rows.Count > 0 Then
                dList.Tables(0).Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    dtRow.Item("ischeck") = False
                    If mdtOwrField4Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtOwrField4Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,name"

            If txtOwrField4SearchEmp.Text.Trim.Length > 0 Then
                strSearch = "employeecode LIKE '%" & txtOwrField4SearchEmp.Text & "%' OR " & _
                            "name LIKE '%" & txtOwrField4SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If

            dgvOwrField4Owner.DataSource = dtOwnerView
            dgvOwrField4Owner.DataBind()
            Me.ViewState("mdtOwrField4Owner") = mdtOwrField4Owner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Fill_Data_OwrField5()
        Dim dList As New DataSet : Dim objEmployee As New clsEmployee_Master
        Dim iTable As DataTable = Nothing
        Dim dtOwnerView As DataView
        Dim strSearch As String = ""
        Try
            Dim mintOwnerId As Integer = CInt(cboOwner.SelectedValue)
            Dim mdtOwrField5Owner As DataTable = CType(Me.ViewState("mdtOwrField5Owner"), DataTable)
            Dim iFilter As String = "hremployee_master.isapproved = 1"
            Select Case CInt(cboOwnerType.SelectedValue)
                Case enAllocation.BRANCH
                    iFilter &= " AND hremployee_master.stationunkid = '" & mintOwnerId & "' "
                Case enAllocation.DEPARTMENT_GROUP
                    iFilter &= " AND hremployee_master.deptgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.DEPARTMENT
                    iFilter &= " AND hremployee_master.departmentunkid = '" & mintOwnerId & "' "
                Case enAllocation.SECTION_GROUP
                    iFilter &= " AND hremployee_master.sectiongroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.SECTION
                    iFilter &= " AND hremployee_master.sectionunkid = '" & mintOwnerId & "' "
                Case enAllocation.UNIT_GROUP
                    iFilter &= " AND hremployee_master.unitgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.UNIT
                    iFilter &= " AND hremployee_master.unitunkid = '" & mintOwnerId & "' "
                Case enAllocation.TEAM
                    iFilter &= " AND hremployee_master.teamunkid = '" & mintOwnerId & "' "
                Case enAllocation.JOB_GROUP
                    iFilter &= " AND hremployee_master.jobgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.JOBS
                    iFilter &= " AND hremployee_master.jobunkid = '" & mintOwnerId & "' "
                Case enAllocation.CLASS_GROUP
                    iFilter &= " AND hremployee_master.classgroupunkid = '" & mintOwnerId & "' "
                Case enAllocation.CLASSES
                    iFilter &= " AND hremployee_master.classunkid = '" & mintOwnerId & "' "
            End Select

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dList = objEmployee.GetList("iList", False, True, eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), , Session("AccessLevelFilterString"), , iFilter)
            dList = objEmployee.GetList(Session("Database_Name"), _
                                        Session("UserId"), _
                                        Session("Fin_year"), _
                                         Session("CompanyUnkId"), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                        Session("UserAccessModeSetting"), _
                                        True, False, "iList", _
                                        Session("ShowFirstAppointmentDate"), , , _
                                        iFilter)
            'Shani(24-Aug-2015) -- End
            If dList.Tables(0).Rows.Count > 0 Then
                dList.Tables(0).Columns.Add("ischeck", System.Type.GetType("System.Boolean")).DefaultValue = False
                For Each dtRow As DataRow In dList.Tables(0).Rows
                    dtRow.Item("ischeck") = False
                    If mdtOwrField5Owner.Rows.Count > 0 Then
                        Dim dRow As DataRow() = mdtOwrField5Owner.Select("employeeunkid = '" & CInt(dtRow.Item("employeeunkid")) & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dtRow.Item("ischeck") = True
                        End If
                    End If
                Next
            End If
            dtOwnerView = dList.Tables(0).DefaultView
            dtOwnerView.Sort = "ischeck DESC,name"

            If txtOwrField5SearchEmp.Text.Trim.Length > 0 Then
                strSearch = "employeecode LIKE '%" & txtOwrField5SearchEmp.Text & "%' OR " & _
                            "name LIKE '%" & txtOwrField5SearchEmp.Text & "%'"
                dtOwnerView.RowFilter = strSearch
            End If

            dgvOwrField5Owner.DataSource = dtOwnerView
            dgvOwrField5Owner.DataBind()
            Me.ViewState("mdtOwrField5Owner") = mdtOwrField5Owner
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GoalOwrField1Operation(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            Dim mdtOwrField1Owner As DataTable = CType(Me.ViewState("mdtOwrField1Owner"), DataTable)
            If mdtOwrField1Owner IsNot Nothing Then
                Dim dtmp() As DataRow = mdtOwrField1Owner.Select("employeeunkid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtOwrField1Owner.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("owrfieldunkid") = CInt(Me.ViewState("mintOwrField1Unkid"))
                        dRow.Item("employeeunkid") = iTagUnkid
                        dRow.Item("owrfieldtypeid") = enWeight_Types.WEIGHT_FIELD1
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtOwrField1Owner.Rows.Add(dRow)
                    End If
                    mdtOwrField1Owner.AcceptChanges()
                    Me.ViewState("mdtOwrField1Owner") = mdtOwrField1Owner
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GoalOwrField2Operation(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            Dim mdtOwrField2Owner As DataTable = CType(Me.ViewState("mdtOwrField2Owner"), DataTable)
            If mdtOwrField2Owner IsNot Nothing Then
                Dim dtmp() As DataRow = mdtOwrField2Owner.Select("employeeunkid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtOwrField2Owner.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("owrfieldunkid") = CInt(Me.ViewState("mintOwrField2Unkid"))
                        dRow.Item("employeeunkid") = iTagUnkid
                        dRow.Item("owrfieldtypeid") = enWeight_Types.WEIGHT_FIELD1
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtOwrField2Owner.Rows.Add(dRow)
                    End If
                    mdtOwrField2Owner.AcceptChanges()
                    Me.ViewState("mdtOwrField2Owner") = mdtOwrField2Owner
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GoalOwrField3Operation(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            Dim mdtOwrField3Owner As DataTable = CType(Me.ViewState("mdtOwrField3Owner"), DataTable)
            If mdtOwrField3Owner IsNot Nothing Then
                Dim dtmp() As DataRow = mdtOwrField3Owner.Select("employeeunkid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtOwrField3Owner.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("owrfieldunkid") = CInt(Me.ViewState("mintOwrField3Unkid"))
                        dRow.Item("employeeunkid") = iTagUnkid
                        dRow.Item("owrfieldtypeid") = enWeight_Types.WEIGHT_FIELD1
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtOwrField3Owner.Rows.Add(dRow)
                    End If
                    mdtOwrField3Owner.AcceptChanges()
                    Me.ViewState("mdtOwrField3Owner") = mdtOwrField3Owner
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GoalOwrField4Operation(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            Dim mdtOwrField4Owner As DataTable = CType(Me.ViewState("mdtOwrField4Owner"), DataTable)
            If mdtOwrField4Owner IsNot Nothing Then
                Dim dtmp() As DataRow = mdtOwrField4Owner.Select("employeeunkid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtOwrField4Owner.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("owrfieldunkid") = CInt(Me.ViewState("mintOwrField4Unkid"))
                        dRow.Item("employeeunkid") = iTagUnkid
                        dRow.Item("owrfieldtypeid") = enWeight_Types.WEIGHT_FIELD1
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtOwrField4Owner.Rows.Add(dRow)
                    End If
                    mdtOwrField4Owner.AcceptChanges()
                    Me.ViewState("mdtOwrField4Owner") = mdtOwrField4Owner
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GoalOwrField5Operation(ByVal iTagUnkid As Integer, ByVal iFlag As Boolean)
        Try
            Dim mdtOwrField5Owner As DataTable = CType(Me.ViewState("mdtOwrField5Owner"), DataTable)
            If mdtOwrField5Owner IsNot Nothing Then
                Dim dtmp() As DataRow = mdtOwrField5Owner.Select("employeeunkid = '" & iTagUnkid & "'")
                If dtmp.Length > 0 Then
                    If iFlag = False Then
                        dtmp(0).Item("AUD") = "D"
                    End If
                Else
                    If iFlag = True Then
                        Dim dRow As DataRow = mdtOwrField5Owner.NewRow
                        dRow.Item("ownertranunkid") = -1
                        dRow.Item("owrfieldunkid") = CInt(Me.ViewState("mintOwrField5Unkid"))
                        dRow.Item("employeeunkid") = iTagUnkid
                        dRow.Item("owrfieldtypeid") = enWeight_Types.WEIGHT_FIELD1
                        dRow.Item("AUD") = "A"
                        dRow.Item("GUID") = Guid.NewGuid.ToString
                        mdtOwrField5Owner.Rows.Add(dRow)
                    End If
                    mdtOwrField5Owner.AcceptChanges()
                    Me.ViewState("mdtOwrField5Owner") = mdtOwrField5Owner
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Set_OwrField1Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicOwrField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField1FieldData"), Dictionary(Of Integer, String))
        Try
            lblheaderOwrField1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Add/Edit Goal Owner") & " " & objFieldMaster._Field1_Caption & " " & _
                      Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Information")

            objOwrField1lblField1.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 16, "Company") & " " & objFieldMaster._Field1_Caption

            objOwrField1lblOwrField1.Text = objFieldMaster._Field1_Caption
            objOwrField1txtOwrField1Tag.Value = objFieldMaster._Field1Unkid


            If CInt(Me.ViewState("mintOwrField1FieldUnkid")) = CInt(Me.ViewState("mintOwrField1LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField1tabpRemark1.Visible = False
                    objOwrField1tabpRemark1.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                    txtOwrField1Remark1.Enabled = False
                Else
                    'objOwrField1tabpRemark1.HeaderText = objFieldMaster._Field6_Caption
                    lblOwrField1Remark1.Text = objFieldMaster._Field6_Caption
                    hdfOwrField1Tag1.Value = objFieldMaster._Field6Unkid
                    If mdicOwrField1FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicOwrField1FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField1tabpRemark2.Visible = False
                    objOwrField1tabpRemark2.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                    txtOwrField1Remark2.Enabled = False
                Else
                    'objOwrField1tabpRemark2.HeaderText = objFieldMaster._Field7_Caption
                    hdfOwrField1Tag2.Value = objFieldMaster._Field7Unkid
                    lblOwrField1Remark2.Text = objFieldMaster._Field7_Caption
                    If mdicOwrField1FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicOwrField1FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField1tabpRemark3.Visible = False
                    objOwrField1tabpRemark3.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                    txtOwrField1Remark3.Enabled = False
                Else
                    'objOwrField1tabpRemark3.HeaderText = objFieldMaster._Field8_Caption
                    hdfOwrField1Tag3.Value = objFieldMaster._Field8Unkid
                    lblOwrField1Remark3.Text = objFieldMaster._Field8_Caption
                    If mdicOwrField1FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicOwrField1FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicOwrField1FieldData.Keys.Count > 0 Then objOwrField1tabcRemarks.Enabled = True
                pnl_OwrField1dgvower.Enabled = Session("FollowEmployeeHierarchy")
                txtOwrField1SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objOwrField1tabcRemarks.Enabled = False : pnl_RightOwrField1.Enabled = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Set_OwrField2Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicOwrField2FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField2FieldData"), Dictionary(Of Integer, String))
        Try
            lblHeaderOwrField2.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Add/Edit Goal Owner") & " " & objFieldMaster._Field2_Caption & " " & _
                      Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Information")

            objOwrField2lblOwrField1.Text = objFieldMaster._Field1_Caption
            hdf_cboOwrField2OwrFieldValue1.Value = objFieldMaster._Field1Unkid

            objOwrField2lblOwrField2.Text = objFieldMaster._Field2_Caption
            hdf_objOwrField2txtOwrField2.Value = objFieldMaster._Field2Unkid

            If CInt(Me.ViewState("mintOwrField2FieldUnkid")) = CInt(Me.ViewState("mintOwrField2LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField2tabpRemark1.Visible = False
                    objOwrField2tabpRemark1.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                    txtOwrField2Remark1.Enabled = False
                Else
                    'objOwrField2tabpRemark1.HeaderText = objFieldMaster._Field6_Caption
                    lblOwrField2Remark1.Text = objFieldMaster._Field6_Caption
                    hdfOwrField2Tag1.Value = objFieldMaster._Field6Unkid
                    If mdicOwrField2FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicOwrField2FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField2tabpRemark2.Visible = False
                    objOwrField2tabpRemark2.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                    txtOwrField2Remark2.Enabled = False
                Else
                    'objOwrField2tabpRemark2.HeaderText = objFieldMaster._Field7_Caption
                    lblOwrField2Remark2.Text = objFieldMaster._Field7_Caption
                    hdfOwrField2Tag2.Value = objFieldMaster._Field7Unkid
                    If mdicOwrField2FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicOwrField2FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField2tabpRemark3.Visible = False
                    objOwrField2tabpRemark3.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                    txtOwrField2Remark3.Enabled = False
                Else
                    'objOwrField2tabpRemark3.HeaderText = objFieldMaster._Field8_Caption
                    lblOwrField2Remark3.Text = objFieldMaster._Field7_Caption
                    hdfOwrField2Tag3.Value = objFieldMaster._Field8Unkid
                    If mdicOwrField2FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicOwrField2FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicOwrField2FieldData.Keys.Count > 0 Then objOwrField2tabcRemarks.Enabled = True
                pnl_OwrField2dgvower.Enabled = Session("FollowEmployeeHierarchy")
                txtOwrField2SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objOwrField2tabcRemarks.Enabled = False : pnl_RightOwrField2.Enabled = False : objOwrField2tabcRemarks.Enabled = False
            End If
            Me.ViewState("mdicOwrField2FieldData") = mdicOwrField2FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Set_OwrField3Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicOwrField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField3FieldData"), Dictionary(Of Integer, String))
        Try
            lblHeaderOwrField3.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Add/Edit Goal Owner") & " " & objFieldMaster._Field3_Caption & " " & _
                      Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Information")

            objOwrField3lblOwrField1.Text = objFieldMaster._Field1_Caption

            objOwrField3lblOwrField2.Text = objFieldMaster._Field2_Caption
            hdf_cboOwrField3OwrFieldValue2.Value = objFieldMaster._Field2Unkid

            objOwrField3lblOwrField3.Text = objFieldMaster._Field3_Caption
            hdf_objOwrField3txtOwrField3.Value = objFieldMaster._Field3Unkid

            If CInt(Me.ViewState("mintOwrField3FieldUnkid")) = CInt(Me.ViewState("mintOwrField3LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField3tabpRemark1.Visible = False
                    objOwrField3tabpRemark1.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else

                    lblOwrField3Remark1.Text = objFieldMaster._Field6_Caption
                    hdf_lblOwrField3Remark1.Value = objFieldMaster._Field6Unkid
                    If mdicOwrField3FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicOwrField3FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then

                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField3tabpRemark2.Visible = False
                    objOwrField3tabpRemark2.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else

                    lblOwrField3Remark2.Text = objFieldMaster._Field7_Caption
                    hdf_lblOwrField3Remark2.Value = objFieldMaster._Field7Unkid
                    If mdicOwrField3FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicOwrField3FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField3tabpRemark3.Visible = False
                    objOwrField3tabpRemark3.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else

                    lblOwrField3Remark3.Text = objFieldMaster._Field8_Caption
                    hdf_lblOwrField3Remark3.Value = objFieldMaster._Field8Unkid
                    If mdicOwrField3FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicOwrField3FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicOwrField3FieldData.Keys.Count > 0 Then objOwrField3tabcRemarks.Enabled = True
                pnl_OwrField3dgvOwner.Enabled = Session("FollowEmployeeHierarchy")
                txtOwrField3SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objOwrField3tabcRemarks.Enabled = False : pnl_RightOwrField3.Enabled = False : objOwrField3tabcRemarks.Enabled = False
            End If
            Me.ViewState("mdicOwrField3FieldData") = mdicOwrField3FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Set_OwrField4Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicOwrField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField4FieldData"), Dictionary(Of Integer, String))
        Try
            lblHeaderOwrField4.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Add/Edit Goal Owner") & " " & objFieldMaster._Field4_Caption & " " & _
                      Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Information")

            objOwrField4lblOwrField1.Text = objFieldMaster._Field1_Caption
            objOwrField4lblOwrField2.Text = objFieldMaster._Field2_Caption
            objOwrField4lblOwrField3.Text = objFieldMaster._Field3_Caption
            hdf_cboOwrField4OwrFieldValue3.Value = objFieldMaster._Field3Unkid

            objOwrField4lblOwrField4.Text = objFieldMaster._Field4_Caption
            hdf_objOwrField4txtOwrField4.Value = objFieldMaster._Field4Unkid

            If CInt(Me.ViewState("mintOwrField4FieldUnkid")) = CInt(Me.ViewState("mintOwrField4LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField4tabpRemark1.Visible = False
                    objOwrField4tabpRemark1.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else

                    lblOwrField4Remark1.Text = objFieldMaster._Field6_Caption
                    hdf_txtOwrField4Remark1.Value = objFieldMaster._Field6Unkid
                    If mdicOwrField4FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicOwrField4FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField4tabpRemark2.Visible = False
                    objOwrField4tabpRemark2.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else

                    lblOwrField4Remark2.Text = objFieldMaster._Field7_Caption
                    hdf_txtOwrField4Remark2.Value = objFieldMaster._Field7Unkid
                    If mdicOwrField4FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicOwrField4FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField4tabpRemark3.Visible = False
                    objOwrField4tabpRemark3.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else

                    lblOwrField4Remark3.Text = objFieldMaster._Field8_Caption
                    hdf_txtOwrField4Remark3.Value = objFieldMaster._Field8Unkid
                    If mdicOwrField4FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicOwrField4FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicOwrField4FieldData.Keys.Count > 0 Then objOwrField4tabcRemarks.Enabled = True
                pnl_OwrField4dgvOwner.Enabled = Session("FollowEmployeeHierarchy")
                txtOwrField4SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objOwrField4tabcRemarks.Enabled = False : pnl_RightOwrField4.Enabled = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub Set_OwrField5Form_Information()
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim mdicOwrField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField5FieldData"), Dictionary(Of Integer, String))
        Try
            lblHeaderOwrField5.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Add/Edit Goal Owner") & " " & objFieldMaster._Field5_Caption & " " & _
                      Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Information")

            objOwrField5lblOwrField1.Text = objFieldMaster._Field1_Caption
            objOwrField5lblOwrField2.Text = objFieldMaster._Field2_Caption
            objOwrField5lblOwrField3.Text = objFieldMaster._Field3_Caption
            objOwrField5lblOwrField4.Text = objFieldMaster._Field4_Caption
            hdf_cboOwrField5OwrFieldValue4.Value = objFieldMaster._Field4Unkid

            objOwrField5lblOwrField5.Text = objFieldMaster._Field5_Caption
            hdf_objOwrField5txtOwrField5.Value = objFieldMaster._Field5Unkid

            If CInt(Me.ViewState("mintOwrField5FieldUnkid")) = CInt(Me.ViewState("mintOwrField5LinkedFieldId")) Then
                If objFieldMaster._Field6_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField5tabpRemark1.Visible = False
                    objOwrField5tabpRemark1.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else

                    lblOwrField5Remark1.Text = objFieldMaster._Field6_Caption
                    hdf_txtOwrField5Remark1.Value = objFieldMaster._Field6Unkid
                    If mdicOwrField5FieldData.ContainsKey(objFieldMaster._Field6Unkid) = False Then
                        mdicOwrField5FieldData.Add(objFieldMaster._Field6Unkid, "")
                    End If
                End If

                If objFieldMaster._Field7_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField5tabpRemark2.Visible = False
                    objOwrField5tabpRemark2.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else

                    lblOwrField5Remark2.Text = objFieldMaster._Field7_Caption
                    hdf_txtOwrField5Remark2.Value = objFieldMaster._Field7Unkid
                    If mdicOwrField5FieldData.ContainsKey(objFieldMaster._Field7Unkid) = False Then
                        mdicOwrField5FieldData.Add(objFieldMaster._Field7Unkid, "")
                    End If
                End If

                If objFieldMaster._Field8_Caption = "" Then
                    'Shani [ 10 DEC 2014 ] -- START
                    '
                    'objOwrField5tabpRemark3.Visible = False
                    objOwrField5tabpRemark3.Visible = False
                    'Shani [ 10 DEC 2014 ] -- END
                Else

                    lblOwrField5Remark3.Text = objFieldMaster._Field8_Caption
                    hdf_txtOwrField5Remark3.Value = objFieldMaster._Field8Unkid
                    If mdicOwrField5FieldData.ContainsKey(objFieldMaster._Field8Unkid) = False Then
                        mdicOwrField5FieldData.Add(objFieldMaster._Field8Unkid, "")
                    End If
                End If
                If mdicOwrField5FieldData.Keys.Count > 0 Then objOwrField5tabcRemarks.Enabled = True
                pnl_OwrField5dgvOwner.Enabled = Session("FollowEmployeeHierarchy")
                txtOwrField5SearchEmp.Enabled = Session("FollowEmployeeHierarchy")
            Else
                objOwrField5tabcRemarks.Enabled = False : pnl_RightOwrField5.Enabled = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GetOwerField1Value()
        Dim enOwrField1Action As enAction = CType(Me.ViewState("enOwrField1Action"), enAction)
        Dim mdicOwrField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField1FieldData"), Dictionary(Of Integer, String))
        Try
            If objOwrField1._Enddate <> Nothing Then
                dtpOwrField1EndDate.SetDate = objOwrField1._Enddate
            End If
            If objOwrField1._Startdate <> Nothing Then
                dtpOwrField1StartDate.SetDate = objOwrField1._Startdate
            End If
            objOwrField1txtOwrField1.Text = objOwrField1._Field_Data
            cboOwrField1Allocations.SelectedValue = IIf(objOwrField1._Owenerrefid <= 0, CInt(cboOwnerType.SelectedValue), objOwrField1._Owenerrefid)
            Try
                cboOwrField1FieldValue1.SelectedValue = objOwrField1._Coyfield1unkid
            Catch ex As Exception

            End Try
            Call cboOwrField1Allocations_SelectedIndexChanged(cboOwrField1Allocations, Nothing)
            cboOwner.SelectedValue = IIf(objOwrField1._Ownerunkid <= 0, CInt(cboOwner.SelectedValue), objOwrField1._Ownerunkid)
            cboOwrField1Status.SelectedValue = IIf(objOwrField1._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objOwrField1._Statusunkid)
            txtOwrField1Weight.Text = CDec(objOwrField1._Weight)
            txtOwrField1Percent.Text = objOwrField1._Pct_Completed

            If enOwrField1Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_owrinfofield_tran
                mdicOwrField1FieldData = objInfoField.Get_Data(CInt(Me.ViewState("mintOwrField1Unkid")), enWeight_Types.WEIGHT_FIELD1)
                If mdicOwrField1FieldData.Keys.Count > 0 Then
                    If mdicOwrField1FieldData.ContainsKey(CInt(IIf(hdfOwrField1Tag1.Value = "", 0, hdfOwrField1Tag1.Value))) Then
                        txtOwrField1Remark1.Text = mdicOwrField1FieldData(CInt(hdfOwrField1Tag1.Value))
                    End If
                    If mdicOwrField1FieldData.ContainsKey(CInt(IIf(hdfOwrField1Tag2.Value = "", 0, hdfOwrField1Tag2.Value))) Then
                        txtOwrField1Remark2.Text = mdicOwrField1FieldData(CInt(hdfOwrField1Tag2.Value))
                    End If
                    If mdicOwrField1FieldData.ContainsKey(CInt(IIf(hdfOwrField1Tag3.Value = "", 0, hdfOwrField1Tag3.Value))) Then
                        txtOwrField1Remark3.Text = mdicOwrField1FieldData(CInt(hdfOwrField1Tag3.Value))
                    End If
                End If
                Me.ViewState("mdicOwrField1FieldData") = mdicOwrField1FieldData
                objInfoField = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GetOwrField2Value()
        Dim menOwrField2Action As enAction = CType(Me.ViewState("menOwrField2Action"), enAction)
        Dim mdicOwrField2FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField2FieldData"), Dictionary(Of Integer, String))
        Try
            txtOwrField2Weight.Text = CDec(objOwrField2._Weight)
            objOwrField2txtOwrField2.Text = objOwrField2._Field_Data
            If objOwrField2._Enddate <> Nothing Then
                dtpOwrField2EndDate.SetDate = objOwrField2._Enddate
            End If
            cboOwrField2OwrFieldValue1.SelectedValue = objOwrField2._Owrfield1unkid
            cboOwrField2Status.SelectedValue = IIf(objOwrField2._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objOwrField2._Statusunkid)
            If objOwrField2._Startdate <> Nothing Then
                dtpOwrField2StartDate.SetDate = objOwrField2._Startdate
            End If
            txtOwrField2Percent.Text = objOwrField2._Pct_Completed
            If menOwrField2Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_owrinfofield_tran
                mdicOwrField2FieldData = objInfoField.Get_Data(CInt(Me.ViewState("mintOwrField2Unkid")), enWeight_Types.WEIGHT_FIELD2)
                If mdicOwrField2FieldData.Keys.Count > 0 Then
                    If mdicOwrField2FieldData.ContainsKey(CInt(hdfOwrField2Tag1.Value)) Then
                        txtOwrField2Remark1.Text = mdicOwrField2FieldData(CInt(hdfOwrField2Tag1.Value))
                    End If
                    If mdicOwrField2FieldData.ContainsKey(CInt(hdfOwrField2Tag2.Value)) Then
                        txtOwrField2Remark2.Text = mdicOwrField2FieldData(CInt(hdfOwrField2Tag2.Value))
                    End If
                    If mdicOwrField2FieldData.ContainsKey(CInt(hdfOwrField2Tag3.Value)) Then
                        txtOwrField2Remark3.Text = mdicOwrField2FieldData(CInt(hdfOwrField2Tag3.Value))
                    End If
                End If
                Me.ViewState("mdicOwrField2FieldData") = mdicOwrField2FieldData
                objInfoField = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GetOwrField3Value()
        Dim menOwrField3Action As enAction = CType(Me.ViewState("menOwrField3Action"), enAction)
        Dim mdicOwrField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField3FieldData"), Dictionary(Of Integer, String))
        Try

            txtOwrField3Weight.Text = CDec(objOwrField3._Weight)
            objOwrField3txtOwrField3.Text = objOwrField3._Field_Data
            If objOwrField3._Enddate <> Nothing Then
                dtpOwrField3EndDate.SetDate = objOwrField3._Enddate
            End If
            cboOwrField3OwrFieldValue2.SelectedValue = objOwrField3._Owrfield2unkid

            cboOwrField3Status.SelectedValue = IIf(objOwrField3._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objOwrField3._Statusunkid)
            If objOwrField3._Startdate <> Nothing Then
                dtpOwrField3StartDate.SetDate = objOwrField3._Startdate
            End If
            txtOwrField3Percent.Text = objOwrField3._Pct_Completed
            If menOwrField3Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_owrinfofield_tran
                mdicOwrField3FieldData = objInfoField.Get_Data(CInt(Me.ViewState("mintOwrField3Unkid")), enWeight_Types.WEIGHT_FIELD3)
                If mdicOwrField3FieldData.Keys.Count > 0 Then
                    If mdicOwrField3FieldData.ContainsKey(CInt(IIf(hdf_lblOwrField3Remark1.Value = "", 0, hdf_lblOwrField3Remark1.Value))) Then
                        txtOwrField3Remark1.Text = mdicOwrField3FieldData(CInt(hdf_lblOwrField3Remark1.Value))
                    End If
                    If mdicOwrField3FieldData.ContainsKey(CInt(IIf(hdf_lblOwrField3Remark2.Value = "", 0, hdf_lblOwrField3Remark2.Value))) Then
                        txtOwrField3Remark2.Text = mdicOwrField3FieldData(CInt(hdf_lblOwrField3Remark2.Value))
                    End If
                    If mdicOwrField3FieldData.ContainsKey(CInt(IIf(hdf_lblOwrField3Remark3.Value = "", 0, hdf_lblOwrField3Remark3.Value))) Then
                        txtOwrField3Remark3.Text = mdicOwrField3FieldData(CInt(hdf_lblOwrField3Remark3.Value))
                    End If
                End If
                objInfoField = Nothing
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GetOwrField4Value()
        Dim menOwrField4Action As enAction = CType(Me.ViewState("menOwrField4Action"), enAction)
        Dim mdicOwrField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField4FieldData"), Dictionary(Of Integer, String))
        Try
            If objOwrField4._Enddate <> Nothing Then
                dtpOwrField4EndDate.SetDate = objOwrField4._Enddate
            End If

            objOwrField4txtOwrField4.Text = objOwrField4._Field_Data
            cboOwrField4OwrFieldValue3.SelectedValue = objOwrField4._Owrfield3unkid
            If objOwrField4._Startdate <> Nothing Then
                dtpOwrField4StartDate.SetDate = objOwrField4._Startdate
            End If

            cboOwrField4Status.SelectedValue = IIf(objOwrField4._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objOwrField4._Statusunkid)
            txtOwrField4Weight.Text = CDec(objOwrField4._Weight)
            txtOwrField4Percent.Text = objOwrField4._Pct_Completed
            If menOwrField4Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_owrinfofield_tran
                mdicOwrField4FieldData = objInfoField.Get_Data(CInt(Me.ViewState("mintOwrField4Unkid")), enWeight_Types.WEIGHT_FIELD4)
                If mdicOwrField4FieldData.Keys.Count > 0 Then
                    If mdicOwrField4FieldData.ContainsKey(CInt(IIf(hdf_txtOwrField4Remark1.Value = "", 0, hdf_txtOwrField4Remark1.Value))) Then
                        txtOwrField4Remark1.Text = mdicOwrField4FieldData(CInt(hdf_txtOwrField4Remark1.Value))
                    End If
                    If mdicOwrField4FieldData.ContainsKey(CInt(IIf(hdf_txtOwrField4Remark2.Value = "", 0, hdf_txtOwrField4Remark2.Value))) Then
                        txtOwrField4Remark2.Text = mdicOwrField4FieldData(CInt(hdf_txtOwrField4Remark2.Value))
                    End If
                    If mdicOwrField4FieldData.ContainsKey(CInt(IIf(hdf_txtOwrField4Remark3.Value = "", 0, hdf_txtOwrField4Remark3.Value))) Then
                        txtOwrField4Remark3.Text = mdicOwrField4FieldData(CInt(hdf_txtOwrField4Remark1.Value))
                    End If
                End If
                objInfoField = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub GetOwrField5Value()
        Dim menOwrField5Action As enAction = CType(Me.ViewState("menOwrField5Action"), enAction)
        Dim mdicOwrField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField5FieldData"), Dictionary(Of Integer, String))
        Try
            If objOwrField5._Enddate <> Nothing Then
                dtpOwrField5EndDate.SetDate = objOwrField5._Enddate
            End If
            objOwrField5txtOwrField5.Text = objOwrField5._Field_Data
            cboOwrField5OwrFieldValue4.SelectedValue = objOwrField5._Owrfield4unkid
            cboOwrField5Status.SelectedValue = IIf(objOwrField5._Statusunkid <= 0, enCompGoalStatus.ST_PENDING, objOwrField5._Statusunkid)
            If objOwrField5._Startdate <> Nothing Then
                dtpOwrField5StartDate.SetDate = objOwrField5._Startdate
            End If

            txtOwrField5Weight.Text = CDec(objOwrField5._Weight)
            txtOwrField5Percent.Text = objOwrField5._Pct_Completed
            If menOwrField5Action = enAction.EDIT_ONE Then
                Dim objInfoField As New clsassess_owrinfofield_tran
                mdicOwrField5FieldData = objInfoField.Get_Data(CInt(Me.ViewState("mintOwrField5Unkid")), enWeight_Types.WEIGHT_FIELD5)
                If mdicOwrField5FieldData.Keys.Count > 0 Then
                    If mdicOwrField5FieldData.ContainsKey(CInt(IIf(hdf_txtOwrField5Remark1.Value = "", 0, hdf_txtOwrField5Remark1.Value))) Then
                        txtOwrField5Remark1.Text = mdicOwrField5FieldData(CInt(hdf_txtOwrField5Remark1.Value))
                    End If
                    If mdicOwrField5FieldData.ContainsKey(CInt(IIf(hdf_txtOwrField5Remark2.Value = "", 0, hdf_txtOwrField5Remark2.Value))) Then
                        txtOwrField5Remark2.Text = mdicOwrField5FieldData(CInt(hdf_txtOwrField5Remark3.Value))
                    End If
                    If mdicOwrField5FieldData.ContainsKey(CInt(IIf(hdf_txtOwrField5Remark3.Value = "", 0, hdf_txtOwrField5Remark3.Value))) Then
                        txtOwrField5Remark3.Text = mdicOwrField5FieldData(CInt(hdf_txtOwrField5Remark3.Value))
                    End If
                End If
                objInfoField = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Function IsOwrField1ValidData() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim enOwrField1Action As enAction = CType(Me.ViewState("enOwrField1Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            If CInt(cboOwrField1Owner.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Owner is mandatory information. Please select Owner to continue."), Me)
                Return False
            End If

            Select Case Session("CascadingTypeId")
                Case enPACascading.STRICT_CASCADING, enPACascading.STRICT_GOAL_ALIGNMENT
                    'S.SANDEEP [27 Jan 2016] -- START
                    If CInt(cboOwrField1FieldValue1.SelectedValue) <= 0 Then
                        'If CInt(cboFieldValue1.SelectedValue) <= 0 Then
                        'S.SANDEEP [27 Jan 2016] -- END

                        iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Sorry Company, ") & objFieldMaster._Field1_Caption & _
                               Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, " is mandatory information. Please provide ") & objFieldMaster._Field1_Caption & _
                               Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, " to continue.")
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        Return False
                    End If
            End Select

            If objOwrField1txtOwrField1.Text.Trim.Length <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, ") & objFieldMaster._Field1_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, " is mandatory information. Please provide ") & objFieldMaster._Field1_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                Return False
            End If

            If dtpOwrField1StartDate.IsNull = False AndAlso dtpOwrField1EndDate.IsNull = False Then
                If dtpOwrField1EndDate.GetDate.Date < dtpOwrField1StartDate.GetDate.Date Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 19, "Sorry, End Date cannot be less then Start Date."), Me)
                    Return False
                End If
            End If

            If CInt(Me.ViewState("mintOwrField1LinkedFieldId")) = CInt(Me.ViewState("mintOwrField1FieldUnkid")) Then
                Dim mDecWeight As Decimal = 0
                Decimal.TryParse(txtOwrField1Weight.Text, mDecWeight)
                If mDecWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    Return False
                End If

                Dim objMapping As New clsAssess_Field_Mapping
                If mDecWeight > 0 Then
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL, enWeight_Types.WEIGHT_FIELD1, mDecWeight, CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintOwrField1LinkedFieldId")), CInt(cboOwner.SelectedValue), 0, enOwrField1Action, CInt(Me.ViewState("mintOwrField1Unkid")))
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        Return False
                    End If
                End If

                objMapping = Nothing
                mDecWeight = 0 : Decimal.TryParse(txtOwrField1Percent.Text, mDecWeight)
                If mDecWeight > 100 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 20, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                    Return False
                End If

                If CInt(cboOwrField1Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Function IsOwrField2ValidData() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim menOwrField2Action As enAction = CType(Me.ViewState("menOwrField2Action"), enAction)
        Try
            Dim iMsg As String = String.Empty

            If CInt(cboOwrField2OwrFieldValue1.SelectedValue) <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, ") & objFieldMaster._Field1_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, " is mandatory information. Please select ") & objFieldMaster._Field1_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, " to continue.")
                DisplayMessage.DisplayMessage(iMsg, Me)
                Return False
            End If

            If objOwrField2txtOwrField2.Text.Trim.Length <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, ") & objFieldMaster._Field2_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, " is mandatory information. Please provide ") & objFieldMaster._Field2_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                Return False
            End If

            If dtpOwrField2StartDate.IsNull = False AndAlso dtpOwrField2EndDate.IsNull = False Then
                If dtpOwrField2EndDate.GetDate.Date < dtpOwrField2StartDate.GetDate.Date Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Sorry, End Date cannot be less then Start Date."), Me)
                    Return False
                End If
            End If

            If CInt(Me.ViewState("mintOwrField2FieldUnkid")) = CInt(Me.ViewState("mintOwrField2LinkedFieldId")) Then
                Dim objMapping As New clsAssess_Field_Mapping
                Dim mDecWeight As Decimal = 0
                'SHANI (14 May 2015) -- Start
                Decimal.TryParse(txtOwrField2Weight.Text, mDecWeight)
                'SHANI (14 May 2015) -- End 
                If mDecWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    Return False
                End If


                If mDecWeight > 0 Then
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL, enWeight_Types.WEIGHT_FIELD2, mDecWeight, CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintOwrField2LinkedFieldId")), CInt(cboOwner.SelectedValue), 0, menOwrField2Action, CInt(Me.ViewState("mintOwrField2Unkid")))
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        Return False
                    End If
                End If
                objMapping = Nothing
                mDecWeight = 0 : Decimal.TryParse(txtOwrField2Percent.Text, mDecWeight)
                If mDecWeight > 100 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 14, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                    Return False
                End If

                If CInt(cboOwrField2Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Function IsOwrField3ValidData() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim menOwrField3Action As enAction = CType(Me.ViewState("menOwrField3Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            If CInt(cboOwrField3OwrFieldValue2.SelectedValue) <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, ") & objFieldMaster._Field2_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, " is mandatory information. Please select ") & objFieldMaster._Field2_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                cboOwrField3OwrFieldValue2.Focus()
                Return False
            End If

            If objOwrField3txtOwrField3.Text.Trim.Length <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, ") & objFieldMaster._Field3_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, " is mandatory information. Please provide ") & objFieldMaster._Field3_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                objOwrField3txtOwrField3.Focus()
                Return False
            End If

            If dtpOwrField3StartDate.IsNull = False AndAlso dtpOwrField3EndDate.IsNull = False Then
                If dtpOwrField3EndDate.GetDate.Date < dtpOwrField3StartDate.GetDate.Date Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Sorry, End Date cannot be less then Start Date."), Me)
                    Return False
                End If
            End If

            If CInt(Me.ViewState("mintOwrField3FieldUnkid")) = CInt(Me.ViewState("mintOwrField3LinkedFieldId")) Then
                Dim mDecWeight As Decimal = 0
                Decimal.TryParse(txtOwrField3Weight.Text, mDecWeight)
                If mDecWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                    txtOwrField3Weight.Focus()
                    Return False
                End If

                Dim objMapping As New clsAssess_Field_Mapping
                If mDecWeight > 0 Then
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL, enWeight_Types.WEIGHT_FIELD3, mDecWeight, CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintOwrField3LinkedFieldId")), CInt(cboOwner.SelectedValue), 0, menOwrField3Action, CInt(Me.ViewState("mintOwrField3Unkid")))
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        txtOwrField3Weight.Focus()
                        Return False
                    End If
                End If

                objMapping = Nothing
                mDecWeight = 0 : Decimal.TryParse(txtOwrField3Percent.Text, mDecWeight)
                If mDecWeight > 100 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 14, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                    txtOwrField3Percent.Focus()
                    Return False
                End If

                If CInt(cboOwrField3Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    cboOwrField3Status.Focus()
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Function IsOwrField4ValidData() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim menOwrField4Action As enAction = CType(Me.ViewState("menOwrField4Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            If CInt(cboOwrField4OwrFieldValue3.SelectedValue) <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, ") & objFieldMaster._Field3_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, " is mandatory information. Please select ") & objFieldMaster._Field3_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                cboOwrField4OwrFieldValue3.Focus()
                Return False
            End If

            If objOwrField4txtOwrField4.Text.Trim.Length <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, ") & objFieldMaster._Field4_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, " is mandatory information. Please provide ") & objFieldMaster._Field4_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                objOwrField4txtOwrField4.Focus()
                Return False
            End If

            If dtpOwrField4StartDate.IsNull = False AndAlso dtpOwrField4EndDate.IsNull = False Then
                If dtpOwrField4EndDate.GetDate.Date < dtpOwrField4StartDate.GetDate.Date Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Sorry, End Date cannot be less then Start Date."), Me)
                    Return False
                End If
            End If

            If CInt(Me.ViewState("mintOwrField4FieldUnkid")) = CInt(Me.ViewState("mintOwrField4LinkedFieldId")) Then
                Dim mDecWeight As Decimal = 0
                Decimal.TryParse(txtOwrField4Weight.Text, mDecWeight)
                If mDecWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    txtOwrField4Weight.Focus()
                    Return False
                End If

                Dim objMapping As New clsAssess_Field_Mapping
                If mDecWeight > 0 Then
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL, enWeight_Types.WEIGHT_FIELD4, mDecWeight, CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintOwrField4LinkedFieldId")), CInt(cboOwner.SelectedValue), 0, menOwrField4Action, CInt(Me.ViewState("mintOwrField4Unkid")))
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        txtOwrField4Weight.Focus()
                        Return False
                    End If
                End If

                objMapping = Nothing
                mDecWeight = 0 : Decimal.TryParse(txtOwrField4Percent.Text, mDecWeight)
                If mDecWeight > 100 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 14, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                    txtOwrField4Percent.Focus()
                    Return False
                End If

                If CInt(cboOwrField4Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    cboOwrField4Status.Focus()
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Function IsOwrField5ValidData() As Boolean
        Dim objFieldMaster As New clsAssess_Field_Master(True)
        Dim menOwrField5Action As enAction = CType(Me.ViewState("menOwrField5Action"), enAction)
        Try
            Dim iMsg As String = String.Empty
            If CInt(cboOwrField5OwrFieldValue4.SelectedValue) <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, ") & objFieldMaster._Field4_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, " is mandatory information. Please select ") & objFieldMaster._Field4_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, " to continue.")
                DisplayMessage.DisplayMessage(iMsg, Me)
                cboOwrField5OwrFieldValue4.Focus()
                Return False
            End If

            If objOwrField5txtOwrField5.Text.Trim.Length <= 0 Then
                iMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, ") & objFieldMaster._Field5_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, " is mandatory information. Please provide ") & objFieldMaster._Field5_Caption & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 5, " to continue.")

                DisplayMessage.DisplayMessage(iMsg, Me)
                objOwrField5txtOwrField5.Focus()
                Return False
            End If

            If dtpOwrField5StartDate.IsNull = False AndAlso dtpOwrField5EndDate.IsNull = False Then
                If dtpOwrField5EndDate.GetDate.Date < dtpOwrField5StartDate.GetDate.Date Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Sorry, End Date cannot be less then Start Date."), Me)
                    Return False
                End If
            End If

            If CInt(Me.ViewState("mintOwrField5FieldUnkid")) = CInt(Me.ViewState("mintOwrField5LinkedFieldId")) Then
                Dim mDecWeight As Decimal = 0
                Decimal.TryParse(txtOwrField5Weight.Text, mDecWeight)
                If mDecWeight <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Sorry, Weight is mandatory information. Please provide Weight to continue."), Me)
                    txtOwrField5Weight.Focus()
                    Return False
                End If

                Dim objMapping As New clsAssess_Field_Mapping
                If mDecWeight > 0 Then
                    iMsg = objMapping.Is_Valid_Weight(clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL, enWeight_Types.WEIGHT_FIELD5, mDecWeight, CInt(cboPeriod.SelectedValue), CInt(Me.ViewState("mintOwrField5LinkedFieldId")), CInt(cboOwner.SelectedValue), 0, menOwrField5Action, CInt(Me.ViewState("mintOwrField5Unkid")))
                    If iMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(iMsg, Me)
                        txtOwrField5Weight.Focus()
                        Return False
                    End If
                End If

                objMapping = Nothing
                mDecWeight = 0 : Decimal.TryParse(txtOwrField5Percent.Text, mDecWeight)
                If mDecWeight > 100 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 14, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                    txtOwrField5Percent.Focus()
                    Return False
                End If

                If CInt(cboOwrField5Status.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, Status is mandatory information. Please select Status to continue."), Me)
                    cboOwrField5Status.Focus()
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Function

    Private Sub SetOwrField1Value()
        Try
            objOwrField1._Owrfield1unkid = CInt(Me.ViewState("mintOwrField1Unkid"))
            'S.SANDEEP [27 Jan 2016] -- START
            'objOwrField1._Coyfield1unkid = CInt(cboFieldValue1.SelectedValue)
            objOwrField1._Coyfield1unkid = CInt(cboOwrField1FieldValue1.SelectedValue)
            'S.SANDEEP [27 Jan 2016] -- END
            If dtpOwrField1EndDate.IsNull = False Then
                objOwrField1._Enddate = dtpOwrField1EndDate.GetDate.Date
            Else
                objOwrField1._Enddate = Nothing
            End If
            objOwrField1._Field_Data = objOwrField1txtOwrField1.Text
            objOwrField1._Fieldunkid = CInt(Me.ViewState("mintOwrField1FieldUnkid"))
            objOwrField1._Isvoid = False
            objOwrField1._Owenerrefid = CInt(cboOwrField1Allocations.SelectedValue)
            objOwrField1._Ownerunkid = CInt(cboOwrField1Owner.SelectedValue)
            objOwrField1._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD1
            objOwrField1._Periodunkid = CInt(cboPeriod.SelectedValue)
            If dtpOwrField1StartDate.IsNull = False Then
                objOwrField1._Startdate = dtpOwrField1StartDate.GetDate.Date
            Else
                objOwrField1._Startdate = Nothing
            End If
            objOwrField1._Statusunkid = CInt(cboOwrField1Status.SelectedValue)
            objOwrField1._Userunkid = Session("UserId")
            objOwrField1._Voiddatetime = Nothing
            objOwrField1._Voiduserunkid = 0
            objOwrField1._Voidreason = ""
            objOwrField1._Weight = txtOwrField1Weight.Text
            objOwrField1._Yearunkid = 0
            objOwrField1._Pct_Completed = txtOwrField1Percent.Text
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetOwrField2Value()
        Try
            objOwrField2._Owrfield2unkid = CInt(Me.ViewState("mintOwrField2Unkid"))
            If dtpOwrField2EndDate.IsNull = False Then
                objOwrField2._Enddate = dtpOwrField2EndDate.GetDate.Date
            Else
                objOwrField2._Enddate = Nothing
            End If
            objOwrField2._Field_Data = objOwrField2txtOwrField2.Text
            objOwrField2._Fieldunkid = CInt(Me.ViewState("mintOwrField2FieldUnkid"))
            objOwrField2._Isvoid = False
            objOwrField2._Owrfield1unkid = CInt(cboOwrField2OwrFieldValue1.SelectedValue)
            If dtpOwrField2StartDate.IsNull = False Then
                objOwrField2._Startdate = dtpOwrField2StartDate.GetDate.Date
            Else
                objOwrField2._Startdate = Nothing
            End If
            objOwrField2._Statusunkid = CInt(cboOwrField2Status.SelectedValue)
            objOwrField2._Userunkid = CInt(Session("UserId"))
            objOwrField2._Voiddatetime = Nothing
            objOwrField2._Voidreason = ""
            objOwrField2._Voiduserunkid = -1
            objOwrField2._Weight = CDec(txtOwrField2Weight.Text)
            objOwrField2._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD2
            objOwrField2._Pct_Completed = CDec(txtOwrField2Percent.Text)
            objOwrField2._Ownerunkid = cboOwner.SelectedValue
            objOwrField2._Periodunkid = cboPeriod.SelectedValue
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetOwrField3Value()
        Try
            objOwrField3._Owrfield3unkid = CInt(Me.ViewState("mintOwrField3Unkid"))
            If dtpOwrField3EndDate.IsNull = False Then
                objOwrField3._Enddate = dtpOwrField3EndDate.GetDate
            Else
                objOwrField3._Enddate = Nothing
            End If
            objOwrField3._Field_Data = objOwrField3txtOwrField3.Text
            objOwrField3._Fieldunkid = CInt(Me.ViewState("mintOwrField3FieldUnkid"))
            objOwrField3._Isvoid = False
            objOwrField3._Owrfield2unkid = CInt(cboOwrField3OwrFieldValue2.SelectedValue)
            objOwrField3._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD3
            If dtpOwrField3StartDate.IsNull = False Then
                objOwrField3._Startdate = dtpOwrField3StartDate.GetDate
            Else
                objOwrField3._Startdate = Nothing
            End If
            objOwrField3._Statusunkid = CInt(cboOwrField3Status.SelectedValue)
            objOwrField3._Userunkid = Session("UserId")
            objOwrField3._Voiddatetime = Nothing
            objOwrField3._Voidreason = ""
            objOwrField3._Voiduserunkid = -1
            objOwrField3._Weight = txtOwrField3Weight.Text
            objOwrField3._Pct_Completed = txtOwrField3Percent.Text
            objOwrField3._Ownerunkid = CInt(cboOwner.SelectedValue)
            objOwrField3._Periodunkid = CInt(cboPeriod.SelectedValue)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetOwrField4Value()
        Try
            objOwrField4._Owrfield4unkid = CInt(Me.ViewState("mintOwrField4Unkid"))
            If dtpOwrField4EndDate.IsNull = False Then
                objOwrField4._Enddate = dtpOwrField4EndDate.GetDate
            End If
            objOwrField4._Field_Data = objOwrField4txtOwrField4.Text
            objOwrField4._Fieldunkid = CInt(Me.ViewState("mintOwrField4FieldUnkid"))
            objOwrField4._Isvoid = False
            objOwrField4._Owrfield3unkid = CInt(cboOwrField4OwrFieldValue3.SelectedValue)
            If dtpOwrField4StartDate.IsNull = False Then
                objOwrField4._Startdate = dtpOwrField4StartDate.GetDate
            Else
                objOwrField4._Startdate = Nothing
            End If
            objOwrField4._Statusunkid = CInt(cboOwrField4Status.SelectedValue)
            objOwrField4._Userunkid = Session("UserId")
            objOwrField4._Voiddatetime = Nothing
            objOwrField4._Voidreason = ""
            objOwrField4._Voiduserunkid = -1
            objOwrField4._Weight = txtOwrField4Weight.Text
            objOwrField4._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD4
            objOwrField4._Pct_Completed = txtOwrField4Percent.Text
            objOwrField4._Ownerunkid = CInt(cboOwner.SelectedValue)
            objOwrField4._Periodunkid = CInt(cboPeriod.SelectedValue)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetOwrField5Value()
        Try
            objOwrField5._Owrfield5unkid = CInt(Me.ViewState("mintOwrField5Unkid"))
            If dtpOwrField5EndDate.IsNull = False Then
                objOwrField5._Enddate = dtpOwrField5EndDate.GetDate
            End If
            objOwrField5._Field_Data = objOwrField5txtOwrField5.Text
            objOwrField5._Fieldunkid = CInt(Me.ViewState("mintOwrField5FieldUnkid"))
            objOwrField5._Isvoid = False
            objOwrField5._Owrfield4unkid = CInt(cboOwrField5OwrFieldValue4.SelectedValue)
            If dtpOwrField5StartDate.IsNull = False Then
                objOwrField5._Startdate = dtpOwrField5StartDate.GetDate
            End If
            objOwrField5._Statusunkid = CInt(cboOwrField5Status.SelectedValue)
            objOwrField5._Userunkid = Session("UserId")
            objOwrField5._Voiddatetime = Nothing
            objOwrField5._Voidreason = ""
            objOwrField5._Voiduserunkid = -1
            objOwrField5._Weight = txtOwrField5Weight.Text
            objOwrField5._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD5
            objOwrField5._Pct_Completed = txtOwrField5Percent.Text
            objOwrField5._Ownerunkid = CInt(cboOwner.SelectedValue)
            objOwrField5._Periodunkid = CInt(cboPeriod.SelectedValue)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'S.SANDEEP [28 MAY 2015] -- START
    'ENHANCEMENT : NEW PRIVILEGE (Performance Assessment)
    Private Sub SetVisibility()
        Try
            btnCommit.Visible = CBool(Session("AllowtoCommitAllocationGoals"))
            btnUnlock.Visible = CBool(Session("AllowtoUnlockcommittedAllocationGoals"))
            'S.SANDEEP [16 JUN 2015] -- START
            'btnUpdatePercentage.Visible = CBool(Session("AllowtoUpdatePercentCompletedAllocationGoals"))
            'S.SANDEEP [16 JUN 2015] -- END


            'S.SANDEEP [01 DEC 2015] -- START
            'btnGlobalAssign.Visible = CBool(Session("AllowtoPerformGlobalAssignAllocationGoals"))
            If CBool(Session("FollowEmployeeHierarchy")) = True Then
                btnGlobalAssign.Visible = CBool(Session("AllowtoPerformGlobalAssignAllocationGoals"))
            End If
            'S.SANDEEP [01 DEC 2015] -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP [28 MAY 2015] -- END

#End Region

#Region "Button Event"
    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~/UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Shani [ 24 DEC 2014 ] -- END

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Owner Type, Owner and Period as mandatory information. Please select Owner Type, Owner and Period to continue."), Me)
                Exit Sub
            Else
                If CInt(cboOwnerType.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Owner Type, Owner and Period as mandatory information. Please select Owner Type, Owner and Period to continue."), Me)
                    Exit Sub
                End If
            End If
            Call Fill_Grid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnReset.Click
        Try
            cboPeriod.SelectedValue = 0
            cboStatus.SelectedValue = 0
            cboOwner.SelectedValue = 0
            cboFieldValue1.SelectedValue = 0
            cboFieldValue2.SelectedValue = 0
            cboFieldValue3.SelectedValue = 0
            cboFieldValue4.SelectedValue = 0
            cboFieldValue5.SelectedValue = 0
            dgvData.DataSource = Nothing
            dgvData.DataBind()

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'pnl_dgvData.Height = 0
            'SHANI [01 FEB 2015]--END
            Call cboPeriod_SelectedIndexChanged(cboPeriod, Nothing)
            objlblTotalWeight.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnOwrField1Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOwrField1Save.Click
        Dim iblnFlag As Boolean = False
        Dim menAction As enAction = CType(Me.ViewState("enOwrField1Action"), enAction)
        Dim mdtOwrField1Owner As DataTable = CType(Me.ViewState("mdtOwrField1Owner"), DataTable)
        Dim mdicOwrField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField1FieldData"), Dictionary(Of Integer, String))
        Try
            If IsOwrField1ValidData() = False Then Exit Sub
            Call SetOwrField1Value()
            If menAction = enAction.EDIT_ONE Then
                iblnFlag = objOwrField1.Update(mdicOwrField1FieldData, mdtOwrField1Owner)
            Else
                iblnFlag = objOwrField1.Insert(mdicOwrField1FieldData, mdtOwrField1Owner)
            End If
            If iblnFlag = False Then
                If objOwrField1._Message <> "" Then
                    DisplayMessage.DisplayMessage(objOwrField1._Message, Me)
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Sorry, problem in saving Owner Goals."), Me)
                End If
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Owner Goals are saved successfully."), Me)
                If menAction = enAction.ADD_CONTINUE Then
                    objOwrField1 = New clsassess_owrfield1_master
                    Call GetOwerField1Value()
                    txtOwrField1Remark1.Text = "" : txtOwrField1Remark2.Text = "" : txtOwrField1Remark3.Text = ""
                    Call Fill_Grid()
                Else
                    Call Fill_Grid()
                    Call btnOwrField1Cancel_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnOwrField1Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOwrField1Cancel.Click
        Try
            mblnpopupShow_OwrField1 = False
            pop_objfrmAddEditOwrField1.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnOwrField2Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOwrField2Save.Click
        Dim iblnFlag As Boolean = False
        Dim menOwrField2Action As enAction = CType(Me.ViewState("menOwrField2Action"), enAction)
        Dim mdtOwrField2Owner As DataTable = CType(Me.ViewState("mdtOwrField2Owner"), DataTable)
        Dim mdicOwrField2FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField2FieldData"), Dictionary(Of Integer, String))
        Try
            If IsOwrField2ValidData() = False Then Exit Sub
            Call SetOwrField2Value()
            If menOwrField2Action = enAction.EDIT_ONE Then
                iblnFlag = objOwrField2.Update(mdicOwrField2FieldData, mdtOwrField2Owner)
            Else
                iblnFlag = objOwrField2.Insert(mdicOwrField2FieldData, mdtOwrField2Owner)
            End If
            If iblnFlag = False Then
                If objOwrField2._Message <> "" Then
                    DisplayMessage.DisplayMessage(objOwrField2._Message, Me)
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, problem in saving Owner Goals."), Me)
                End If
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Owner Goals are saved successfully."), Me)
                If menOwrField2Action = enAction.ADD_CONTINUE Then
                    objOwrField2 = New clsassess_owrfield2_master
                    Call GetOwrField2Value()
                    txtOwrField2Remark1.Text = "" : txtOwrField2Remark2.Text = "" : txtOwrField2Remark3.Text = ""
                    If CInt(Me.ViewState("mintOwrField2ParentId")) > 0 Then
                        cboOwrField2OwrFieldValue1.SelectedValue = CInt(Me.ViewState("mintOwrField2ParentId"))
                    End If
                    Call Fill_Grid()
                Else
                    Call Fill_Grid()
                    Call btnOwrField2Cancel_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnOwrField2Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOwrField2Cancel.Click
        Try
            mblnpopupShow_OwrField2 = False
            pop_objfrmAddEditOwrField2.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnOwrField3Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOwrField3Save.Click
        Dim iblnFlag As Boolean = False
        Dim menOwrField3Action As enAction = CType(Me.ViewState("menOwrField3Action"), enAction)
        Dim mdtOwrField3Owner As DataTable = CType(Me.ViewState("mdtOwrField3Owner"), DataTable)
        Dim mdicOwrField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField3FieldData"), Dictionary(Of Integer, String))
        Try
            If menOwrField3Action = enAction.EDIT_ONE Then
                iblnFlag = objOwrField3.Update(mdicOwrField3FieldData, mdtOwrField3Owner)
            Else
                iblnFlag = objOwrField3.Insert(mdicOwrField3FieldData, mdtOwrField3Owner)
            End If
            If iblnFlag = False Then
                If objOwrField3._Message <> "" Then
                    DisplayMessage.DisplayMessage(objOwrField3._Message, Me)
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, problem in saving Owner Goals."), Me)
                End If
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Owner Goals are saved successfully."), Me)
                If menOwrField3Action = enAction.ADD_CONTINUE Then
                    objOwrField3 = New clsassess_owrfield3_master
                    Call GetOwrField3Value()
                    txtOwrField3Remark1.Text = "" : txtOwrField3Remark2.Text = "" : txtOwrField3Remark3.Text = ""
                    If CInt(Me.ViewState("mintOwrField3ParentId")) > 0 Then
                        cboOwrField3OwrFieldValue2.SelectedValue = CInt(Me.ViewState("mintOwrField3ParentId"))
                        Call cboOwrField3OwrFieldValue2_SelectedIndexChanged(cboOwrField3OwrFieldValue2, Nothing)
                    End If
                    Call Fill_Grid()
                Else
                    Call Fill_Grid()
                    Call btnOwrField3Cancel_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnOwrField3Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOwrField3Cancel.Click
        Try
            mblnpopupShow_OwrField3 = False
            pop_objfrmAddEditOwrField3.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnOwrField4Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOwrField4Save.Click
        Dim iblnFlag As Boolean = False
        Dim menOwrField4Action As enAction = CType(Me.ViewState("menOwrField4Action"), enAction)
        Dim mdtOwrField4Owner As DataTable = CType(Me.ViewState("mdtOwrField4Owner"), DataTable)
        Dim mdicOwrField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField4FieldData"), Dictionary(Of Integer, String))
        Try
            If IsOwrField4ValidData() = False Then Exit Sub
            Call SetOwrField4Value()
            If menOwrField4Action = enAction.EDIT_ONE Then
                iblnFlag = objOwrField4.Update(mdicOwrField4FieldData, mdtOwrField4Owner)
            Else
                iblnFlag = objOwrField4.Insert(mdicOwrField4FieldData, mdtOwrField4Owner)
            End If
            If iblnFlag = False Then
                If objOwrField4._Message <> "" Then
                    DisplayMessage.DisplayMessage(objOwrField4._Message, Me)
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, problem in saving Owner Goals."), Me)
                End If
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Owner Goals are saved successfully."), Me)
                If menOwrField4Action = enAction.ADD_CONTINUE Then
                    objOwrField4 = New clsassess_owrfield4_master
                    Call GetOwrField4Value()
                    txtOwrField4Remark1.Text = "" : txtOwrField4Remark2.Text = "" : txtOwrField4Remark3.Text = ""
                    If CInt(Me.ViewState("mintOwrField4ParentId")) > 0 Then
                        cboOwrField4OwrFieldValue3.SelectedValue = CInt(Me.ViewState("mintOwrField4ParentId"))
                        Call cboOwrField4OwrFieldValue3_SelectedIndexChanged(cboOwrField4OwrFieldValue3, Nothing)
                    End If
                    Call Fill_Grid()
                Else
                    Call Fill_Grid()
                    Call btnOwrField4Cancel_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnOwrField4Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOwrField4Cancel.Click
        Try
            mblnpopupShow_OwrField4 = False
            pop_objfrmAddEditOwrField4.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnOwrField5Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOwrField5Save.Click
        Dim iblnFlag As Boolean = False
        Dim menOwrField5Action As enAction = CType(Me.ViewState("menOwrField5Action"), enAction)
        Dim mdtOwrField5Owner As DataTable = CType(Me.ViewState("mdtOwrField5Owner"), DataTable)
        Dim mdicOwrField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField5FieldData"), Dictionary(Of Integer, String))
        Try
            If IsOwrField5ValidData() = False Then Exit Sub
            Call SetOwrField5Value()
            If menOwrField5Action = enAction.EDIT_ONE Then
                iblnFlag = objOwrField5.Update(mdicOwrField5FieldData, mdtOwrField5Owner)
            Else
                iblnFlag = objOwrField5.Insert(mdicOwrField5FieldData, mdtOwrField5Owner)
            End If
            If iblnFlag = False Then
                If objOwrField5._Message <> "" Then
                    DisplayMessage.DisplayMessage(objOwrField5._Message, Me)
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Sorry, problem in saving Owner Goals."), Me)
                End If
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Owner Goals are saved successfully."), Me)
                If menOwrField5Action = enAction.ADD_CONTINUE Then
                    objOwrField5 = New clsassess_owrfield5_master
                    Call GetOwrField5Value()
                    txtOwrField5Remark1.Text = "" : txtOwrField5Remark2.Text = "" : txtOwrField5Remark3.Text = ""
                    If CInt(Me.ViewState("mintOwrField5ParentId")) > 0 Then
                        cboOwrField5OwrFieldValue4.SelectedValue = CInt(Me.ViewState("mintOwrField5ParentId"))
                    End If
                    Call Fill_Grid()
                Else
                    Call Fill_Grid()
                    Call btnOwrField5Cancel_Click(sender, e)
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnOwrField5Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOwrField5Cancel.Click
        Try
            mblnpopupShow_OwrField5 = False
            pop_objfrmAddEditOwrField5.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Dim mdtFinal As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
        Dim mstrScreenId As String = Me.ViewState("DeleteScreenId")
        Dim mstrScreeName As String = Me.ViewState("DeleteColumnName")
        Try

            If txtMessage.Text.Trim.Length > 0 Then

                Select Case mstrScreenId.Split("|")(1)
                    Case 1
                        If CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("owrfield1unkid")) > 0 Then
                            objOwrField1._Isvoid = True
                            objOwrField1._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objOwrField1._Voidreason = txtMessage.Text
                            objOwrField1._Voiduserunkid = Session("UserId")
                            If objOwrField1.Delete(CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("owrfield1unkid"))) = True Then
                                Call Fill_Grid()
                                popup_YesNo.Hide()
                            Else
                                If objOwrField1._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objOwrField1._Message, Me)
                                    Exit Sub
                                End If
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                             " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                             " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                            Exit Sub
                        End If
                    Case 2
                        If CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("owrfield2unkid")) > 0 Then
                            objOwrField2._Isvoid = True
                            objOwrField2._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objOwrField2._Voidreason = txtMessage.Text
                            objOwrField2._Voiduserunkid = Session("UserId")
                            If objOwrField2.Delete(CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("owrfield2unkid"))) = True Then
                                Call Fill_Grid()
                                popup_YesNo.Hide()
                            Else
                                If objOwrField2._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objOwrField2._Message, Me)
                                    Exit Sub
                                End If
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                             " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                             " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                            Exit Sub
                        End If
                    Case 3
                        If CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("owrfield3unkid")) > 0 Then
                            objOwrField3._Isvoid = True
                            objOwrField3._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objOwrField3._Voidreason = txtMessage.Text
                            objOwrField3._Voiduserunkid = Session("UserId")
                            If objOwrField3.Delete(CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("owrfield3unkid"))) = True Then
                                Call Fill_Grid()
                                popup_YesNo.Hide()
                            Else
                                If objOwrField3._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objOwrField3._Message, Me)
                                    Exit Sub
                                End If
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                            " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                            " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                            Exit Sub
                        End If
                    Case 4
                        If CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("owrfield4unkid")) > 0 Then
                            objOwrField4._Isvoid = True
                            objOwrField4._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objOwrField4._Voidreason = txtMessage.Text
                            objOwrField4._Voiduserunkid = Session("UserId")
                            If objOwrField4.Delete(CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("owrfield4unkid"))) = True Then
                                Call Fill_Grid()
                                popup_YesNo.Hide()
                            Else
                                If objOwrField4._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objOwrField4._Message, Me)
                                    Exit Sub
                                End If
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                            " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                            " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                            Exit Sub
                        End If
                    Case 5
                        If CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("owrfield5unkid")) > 0 Then
                            objOwrField5._Isvoid = True
                            objOwrField5._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                            objOwrField5._Voidreason = txtMessage.Text
                            objOwrField5._Voiduserunkid = Session("UserId")
                            If objOwrField5.Delete(CInt(mdtFinal.Rows(CInt(Me.ViewState("DeleteRowIndex")))("owrfield5unkid"))) = True Then
                                Call Fill_Grid()
                                popup_YesNo.Hide()
                            Else
                                If objOwrField5._Message <> "" Then
                                    DisplayMessage.DisplayMessage(objOwrField5._Message, Me)
                                    Exit Sub
                                End If
                            End If
                        Else
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                            " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                            " " & mstrScreeName & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                            Exit Sub
                        End If
                End Select
            Else
                popup_YesNo.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCommit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCommit.Click
        Try
            If dgvData.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Sorry, there is no goal defined in order to commit."), Me)
                Exit Sub
            End If

            If CInt(cboOwnerType.SelectedValue) <= 0 Or _
               CInt(cboOwner.SelectedValue) <= 0 Or _
               CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 12, "Sorry in order to do commit operation please select following things [Owner Type, Owner and Period]."), Me)
                Exit Sub
            End If
            Dim iLast_StatusId As Integer = 0 : Dim iMsgText As String = String.Empty
            iLast_StatusId = objOwrField1.GetLastStatus(CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue))

            If iLast_StatusId = enObjective_Status.FINAL_COMMITTED Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Sorry, you cannot set the same operational status again. Reason : Owner goals are already committed."), Me)
                Exit Sub
            End If

            Dim objFMapping As New clsAssess_Field_Mapping
            iMsgText = objFMapping.Is_Weight_Set_For_Commiting(clsAssess_Field_Mapping.enWeightCheckType.CKT_ALLOCATION_LEVEL, CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue), 0)
            If iMsgText.Trim.Length > 0 Then
                DisplayMessage.DisplayMessage(iMsgText, Me)
                objFMapping = Nothing
                Exit Sub
            End If
            If objFMapping IsNot Nothing Then objFMapping = Nothing
            Select Case CInt(Session("CascadingTypeId"))
                Case enPACascading.STRICT_CASCADING, enPACascading.LOOSE_CASCADING
                    iMsgText = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 14, "You are about to commit the owner goals for the selected period. This will tranfer all goals to employee goals.") & vbCrLf & _
                               Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 15, "Due to this you will not be allowed to do any operation(s) on owner goals.") & vbCrLf & _
                               Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 16, "Do you wish to continue?")

                    'SHANI [01 FEB 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    'lblCmtTitle.Text = "Aruti"
                    'lblCmtMessage.Text = iMsgText
                    popup_Commit.Title = "Aruti"
                    popup_Commit.Message = iMsgText
                    'SHANI [01 FEB 2015]--END 
                    popup_Commit.Show()
                Case Else
                    iMsgText = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 24, "You are about to commit the owner goals for the selected period.") & vbCrLf & _
                               Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 15, "Due to this you will not be allowed to do any operation(s) on owner goals.") & vbCrLf & _
                               Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 16, "Do you wish to continue?")

                    'SHANI [01 FEB 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    'lblCmtTitle.Text = "Aruti"
                    'lblCmtMessage.Text = iMsgText
                    popup_Commit.Title = "Aruti"
                    popup_Commit.Message = iMsgText
                    'SHANI [01 FEB 2015]--END
                    popup_Commit.Show()
                    If objOwrField1.Update_Status(enObjective_Status.FINAL_COMMITTED, "", CInt(Session("UserId")), CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue)) Then
                        Call Fill_Grid()
                    End If
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnCmtYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCmtYes.Click
    Protected Sub btnCmtYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_Commit.buttonYes_Click
        'SHANI [01 FEB 2015]--END
        Try
            Select Case CInt(Session("CascadingTypeId"))
                Case enPACascading.STRICT_CASCADING, enPACascading.LOOSE_CASCADING

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If objOwrField1.Transfer_Goals_ToEmployee(Session("Fin_year"), Session("Database_Name"), CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue)) Then
                    If objOwrField1.Transfer_Goals_ToEmployee(Session("Fin_year"), Session("Database_Name"), CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), Session("UserAccessModeSetting"), Session("CompanyUnkId"), Session("UserId"), True) Then
                        'Shani(20-Nov-2015) -- End
                        If objOwrField1.Update_Status(enObjective_Status.FINAL_COMMITTED, "", CInt(Session("UserId")), CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue)) Then
                            Call Fill_Grid()
                        End If
                    End If
                Case Else
                    If objOwrField1.Update_Status(enObjective_Status.FINAL_COMMITTED, "", CInt(Session("UserId")), CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue)) Then
                        Call Fill_Grid()
                    End If
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUnlock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnlock.Click
        Try
            If dgvData.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 17, "Sorry, there is no goal defined in order to unlock."), Me)
                Exit Sub
            End If

            If CInt(cboOwnerType.SelectedValue) <= 0 Or _
               CInt(cboOwner.SelectedValue) <= 0 Or _
               CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 18, "Sorry in order to do unlock operation please select following things [Owner Type, Owner and Period]."), Me)
                Exit Sub
            End If
            Dim iLast_StatusId As Integer = 0 : Dim iMsgText As String = String.Empty
            iLast_StatusId = objOwrField1.GetLastStatus(CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue))

            If iLast_StatusId <> enObjective_Status.FINAL_COMMITTED Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 19, "Sorry you cannot unlock company goals. Reson : Owner goals are not committed."), Me)
                Exit Sub
            End If

            If iLast_StatusId = enObjective_Status.OPEN_CHANGES Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 20, "Sorry, you cannot set the same operational status again. Reason : Owner goals are already uncommitted."), Me)
                Exit Sub
            End If

            iMsgText = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 21, "You are about to unlock the owner goals. This will result in opening of all operation on owner goals.") & vbCrLf & _
                       Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 16, "Do you wish to continue?")

            lblUnCmtTitle.Text = "Aruti"
            lblUnCmtMessage.Text = iMsgText
            txtUnCmtMessage.Text = ""
            popup_UnCommit.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUnCmtYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnCmtYes.Click
        Try
            If txtUnCmtMessage.Text.Trim.Length > 0 Then
                If objOwrField1.Update_Status(enObjective_Status.OPEN_CHANGES, txtUnCmtMessage.Text.Trim, CInt(Session("UserId")), CInt(cboPeriod.SelectedValue), CInt(cboOwner.SelectedValue)) Then
                    Call Fill_Grid()
                End If
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 23, "Sorry, Unlock reason is mandaroty information."), Me)
                Exit Sub
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnGlobalAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGlobalAssign.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Sorry, Period is mandatory information. Please select Period to continue.", Me)
                'Sohail (23 Mar 2019) -- End
                Exit Sub
            End If

            If CInt(cboOwnerType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 33, "Sorry, Owner Type is mandatory information. Please select Owner Type to continue."), Me)
                Exit Sub
            End If
            Session.Add("iOwnerTypeId", CInt(cboOwnerType.SelectedValue))
            Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgGlobalAssignGoals.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnUpdatePercentage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdatePercentage.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Sorry, Period is mandatory information. Please select Period to continue.", Me)
                'Sohail (23 Mar 2019) -- End
                Exit Sub
            End If

            If CInt(cboOwnerType.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 33, "Sorry, Owner Type is mandatory information. Please select Owner Type to continue."), Me)
                Exit Sub
            End If
            Session.Add("iOwnerTypeId", CInt(cboOwnerType.SelectedValue))
            Response.Redirect(Session("servername") & "~/Assessment New/Performance Goals/wPgOwrUpdatePercentage.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Nilay (19-Jun-2015) -- Start
    'Update Progress Button Save 
    Protected Sub btnUpdateSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateSave.Click
        Try
            If IsValidUpdateInfo() = False Then Exit Sub

            If txtUpdateRemark.Text.Trim.Length <= 0 Then
                'Language.setLanguage(mstrModuleName)
                lblUpdateMessages.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Sorry, you have not entered the remark. Would you like to save the changes without remark?")
                hdf_UpdateYesNo.Value = "TXTREMARK"
                popup_UpdateYesNO.Show()
                Exit Sub
            End If
            Call Save_UpdateProgress()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnUpdateSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnUpdateClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateClose.Click
        Try
            popup_objfrmUpdateFieldValue.Hide()
            mblnUpdateProgress = False
            Call Fill_Grid()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnUpdateClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnupdateYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdateYes.Click
        Try
            If hdf_UpdateYesNo.Value.ToUpper = "TXTREMARK" Then
                Call Save_UpdateProgress()
            ElseIf hdf_UpdateYesNo.Value.ToUpper = "DELETE" Then
                popup_UpdateProgressDeleteReason.Title = "Delete Reason:"
                popup_UpdateProgressDeleteReason.Show()
            End If
            hdf_UpdateYesNo.Value = ""

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnupdateYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnupdateNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnupdateNo.Click
        Try
            If hdf_UpdateYesNo.Value.ToUpper = "TXTREMARK" Then
            ElseIf hdf_UpdateYesNo.Value.ToUpper = "DELETE" Then
                mintOwnerUpdateTranunkid = 0
            End If
            hdf_UpdateYesNo.Value = ""
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnupdateNo_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popup_UpdateProgressDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UpdateProgressDeleteReason.buttonDelReasonYes_Click
        Try
            objOUpdateProgress._Isvoid = True
            objOUpdateProgress._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objOUpdateProgress._Voidreason = popup_UpdateProgressDeleteReason.Reason
            objOUpdateProgress._Voiduserunkid = CInt(Session("UserId"))

            If objOUpdateProgress.Delete(mintOwnerUpdateTranunkid) = True Then
                Call FillUpdateHistory()
                popup_UpdateProgressDeleteReason.Reason = ""
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_UpdateProgressDeleteReason_buttonDelReasonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UpdateProgressDeleteReason.buttonDelReasonNo_Click
        Try
            mintOwnerUpdateTranunkid = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Nilay (19-Jun-2015) -- End


#End Region

#Region "GridView Event"
    'Nilay (19-Jun-2015) -- Start
    Protected Sub dgvData_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles dgvData.RowCommand
        Try
            Dim xRow As GridViewRow = CType(CType(e.CommandSource, LinkButton).NamingContainer, GridViewRow)

            Select Case e.CommandName.ToUpper()
                Case "UPDATEPROGRESS"
                    UpdateProcessLoad(xRow)
                popup_objfrmUpdateFieldValue.Show()
                mblnUpdateProgress = True

                Case "OBJADD"
                    Me.ViewState("AddRowIndex") = xRow.RowIndex
                    Genrate_AddmanuItem()
                    popAddButton.X = CInt(hdf_locationx.Value)
                    popAddButton.Y = CInt(hdf_locationy.Value)
                    popAddButton.Show()

                Case "OBJEDIT"
                    Me.ViewState("EditRowIndex") = xRow.RowIndex
                    Genrate_EditmanuItem()
                    popEditButton.X = CInt(hdf_locationx.Value)
                    popEditButton.Y = CInt(hdf_locationy.Value)
                    popEditButton.Show()

                Case "OBJDELETE"
                    Me.ViewState("DeleteRowIndex") = xRow.RowIndex
                    Genrate_DeletemanuItem()
                    popDeleteButton.X = CInt(hdf_locationx.Value)
                    popDeleteButton.Y = CInt(hdf_locationy.Value)
                    popDeleteButton.Show()
            End Select


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Nilay (19-Jun-2015) -- End

    Protected Sub dgvData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvData.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Call SetDateFormat()
                'Pinkal (16-Apr-2016) -- End


                'Dim imgAdd As New ImageButton()
                'imgAdd.ID = "imgAdd"
                'imgAdd.Attributes.Add("Class", "objAddBtn")
                ''Shani [ 10 DEC 2014 ] -- START
                ''
                'imgAdd.Attributes.Add("onclick", "getscrollPosition()")
                ''Shani [ 10 DEC 2014 ] -- END
                'imgAdd.CommandName = "objAdd"
                'imgAdd.ImageUrl = "~/images/add_16.png"
                'imgAdd.ToolTip = "New"
                'AddHandler imgAdd.Click, AddressOf imgAdd_Click
                'e.Row.Cells(0).Controls.Add(imgAdd)

                Dim imgAdd As New LinkButton
                imgAdd.ID = "imgAdd"
                imgAdd.CommandName = "objAdd"
                imgAdd.ToolTip = "New"
                imgAdd.CssClass = "lnAdd"
                e.Row.Cells(0).Controls.Add(imgAdd)


                'Dim imgEdit As New ImageButton()
                'imgEdit.ID = "imgEdit"
                'imgEdit.Attributes.Add("Class", "objAddBtn")
                ''Shani [ 10 DEC 2014 ] -- START
                ''
                'imgEdit.Attributes.Add("onclick", "getscrollPosition()")
                ''Shani [ 10 DEC 2014 ] -- END
                'imgEdit.CommandName = "objEdit"
                'imgEdit.ImageUrl = "~/images/Edit.png"
                'imgEdit.ToolTip = "Edit"
                'AddHandler imgEdit.Click, AddressOf imgEdit_Click
                'e.Row.Cells(1).Controls.Add(imgEdit)

                Dim imgEdit As New LinkButton
                imgEdit.ID = "imgEdit"
                imgEdit.CommandName = "objEdit"
                imgEdit.ToolTip = "Edit"
                imgEdit.CssClass = "lnEdit"
                e.Row.Cells(1).Controls.Add(imgEdit)

                'Dim imgDelete As New ImageButton()
                'imgDelete.ID = "imgDelete"
                'imgDelete.Attributes.Add("Class", "objAddBtn")
                ''Shani [ 10 DEC 2014 ] -- START
                ''
                'imgDelete.Attributes.Add("onclick", "getscrollPosition()")
                ''Shani [ 10 DEC 2014 ] -- END
                'imgDelete.CommandName = "objDelete"
                'imgDelete.ImageUrl = "~/images/remove.png"
                'imgDelete.ToolTip = "Delete"
                'AddHandler imgDelete.Click, AddressOf imgDelete_Click
                'e.Row.Cells(2).Controls.Add(imgDelete)

                Dim imgDelete As New LinkButton
                imgDelete.ID = "imgDelete"
                imgDelete.CommandName = "objDelete"
                imgDelete.ToolTip = "Delete"
                imgDelete.CssClass = "lnDelt"
                e.Row.Cells(2).Controls.Add(imgDelete)

                'Nilay (19-Jun-2015) -- Start
                Dim lnk As New LinkButton
                lnk.ID = "lnkUpdateProgress"
                lnk.CommandName = "UpdateProgress"

                'SHANI (20 JUN 2015) -- Start
                'lnk.Text = CType(Me.ViewState("mdtFinal"), DataTable).Rows(e.Row.RowIndex)("vuProgress")
                lnk.Text = ""
                lnk.CssClass = "updatedata123"
                lnk.Style.Add("color", "red")
                lnk.Style.Add("font-size", "20px")
                'SHANI (20 JUN 2015) -- End
                e.Row.Cells(mintUpdateProgressIndex).Controls.Add(lnk)
                'Nilay (19-Jun-2015) -- End

                'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Dim intIndex As Integer = 0
                intIndex = CType(Me.ViewState("mdtFinal"), DataTable).Columns("St_Date").ExtendedProperties("index")
                If e.Row.Cells(intIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(intIndex).Text.Trim <> "&nbsp;" Then
                    e.Row.Cells(intIndex).Text = CDate(e.Row.Cells(intIndex).Text).Date.ToShortDateString
                End If

                intIndex = CType(Me.ViewState("mdtFinal"), DataTable).Columns("Ed_Date").ExtendedProperties("index")
                If e.Row.Cells(intIndex).Text.ToString().Trim <> "" <> Nothing AndAlso e.Row.Cells(intIndex).Text.Trim <> "&nbsp;" Then
                    e.Row.Cells(intIndex).Text = CDate(e.Row.Cells(intIndex).Text).Date.ToShortDateString
                End If
                'Pinkal (16-Apr-2016) -- End


            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Nilay (19-Jun-2015) -- Start
    'Update Progress DataGrid
    Protected Sub dgvHistory_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvHistory.ItemCommand
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then
                If e.Item.ItemIndex < 0 Then Exit Sub
                If e.CommandName.ToUpper = "EDIT" Then
                    mintOwnerUpdateTranunkid = CInt(e.Item.Cells(6).Text)
                    objOUpdateProgress._Owrupdatetranunkid = mintOwnerUpdateTranunkid
                    Call GetUpdateValue()
                ElseIf e.CommandName.ToUpper = "DELETE" Then
                    mintOwnerUpdateTranunkid = CInt(e.Item.Cells(6).Text)
                    lblUpdateMessages.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 7, "Are you sure you want to delete this progress for the selected employee?")
                    hdf_UpdateYesNo.Value = "DELETE"
                    popup_UpdateYesNO.Show()
                End If

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvHistory_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (19-Jun-2015) -- End


#End Region

#Region "ComboBOx Event"

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objFMaster = New clsAssess_Field_Master(True)
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim iExecOrder As Integer = 0
                Dim objMapping As New clsAssess_Field_Mapping
                Me.ViewState("mintLinkedFieldId") = objMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
                objMapping = Nothing
                Dim dsFields As New DataSet
                dsFields = objFMaster.Get_Field_Mapping("List", , False)
                If dsFields.Tables(0).Rows.Count > 0 Then
                    Dim dtmp() As DataRow = dsFields.Tables(0).Select("fieldunkid = '" & Me.ViewState("mintLinkedFieldId") & "'")
                    If dtmp.Length > 0 Then
                        Select Case dtmp(0).Item("ExOrder")
                            Case 1
                                Dim objCoyField1 As New clsassess_coyfield1_master
                                Me.ViewState("iOwnerRefId") = objCoyField1.GetOwnerRefId
                                objCoyField1 = Nothing
                            Case 2
                                Dim objCoyField2 As New clsassess_coyfield2_master
                                Me.ViewState("iOwnerRefId") = objCoyField2.GetOwnerRefId
                                objCoyField2 = Nothing
                            Case 3
                                Dim objCoyField3 As New clsassess_coyfield3_master
                                Me.ViewState("iOwnerRefId") = objCoyField3.GetOwnerRefId
                                objCoyField3 = Nothing
                            Case 4
                                Dim objCoyField4 As New clsassess_coyfield4_master
                                Me.ViewState("iOwnerRefId") = objCoyField4.GetOwnerRefId
                                objCoyField4 = Nothing
                            Case 5
                                Dim objCoyField5 As New clsassess_coyfield5_master
                                Me.ViewState("iOwnerRefId") = objCoyField5.GetOwnerRefId
                                objCoyField5 = Nothing
                        End Select
                    End If
                    Dim dsList As New DataSet : Dim objMData As New clsMasterData
                    dsList = objMData.GetEAllocation_Notification("List")
                    Dim dtTable As DataTable = Nothing
                    If CInt(Me.ViewState("iOwnerRefId")) > 0 Then
                        dtTable = New DataView(dsList.Tables(0), "Id IN(0," & Me.ViewState("iOwnerRefId") & ")", "", DataViewRowState.CurrentRows).ToTable
                    Else
                        dtTable = New DataView(dsList.Tables(0), "Id NOT IN(" & enAllocation.JOB_GROUP & "," & enAllocation.JOBS & "," & enAllocation.COST_CENTER & ")", "", DataViewRowState.CurrentRows).ToTable
                    End If
                    With cboOwnerType
                        .DataValueField = "Id"
                        .DataTextField = "Name"
                        .DataSource = dtTable
                        .DataBind()
                        .SelectedValue = IIf(CInt(Me.ViewState("iOwnerRefId")) <= 0, enAllocation.BRANCH, Me.ViewState("iOwnerRefId"))
                    End With
                    objMData = Nothing
                End If
            Else
                cboOwnerType.DataSource = Nothing
            End If

            Call cboOwnerType_SelectedIndexChanged(cboOwnerType, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboOwnerType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOwnerType.SelectedIndexChanged
        Try
            Dim dList As New DataSet
            cboOwner.DataSource = Nothing
            Select Case CInt(cboOwnerType.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "stationunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0).Copy
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "deptgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "departmentunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "sectiongroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "sectionunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "unitgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "unitunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "teamunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "jobgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "jobunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "classgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.getComboList("List", True)
                    With cboOwner
                        .DataValueField = "classesunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = 0
                    End With
            End Select
            Call cboOwner_SelectedIndexChanged(cboOwner, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboOwner_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOwner.SelectedIndexChanged
        Try
            Dim dsList As New DataSet


            'Shani(14-APR-2016) -- Start
            'dsList = objOwrField1.getComboList( "List", True, CInt(cboPeriod.SelectedValue), , CInt(cboOwner.SelectedValue))
            dsList = objOwrField1.getComboList(eZeeDate.convertDate(Session("EmployeeAsOnDate")), "List", True, CInt(cboPeriod.SelectedValue), , CInt(cboOwner.SelectedValue))
            'Shani(14-APR-2016) -- End 

            With cboFieldValue1
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
            Call cboFieldValue1_SelectedIndexChanged(cboFieldValue1, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboFieldValue1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue1.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objOwrField2.getComboList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue1.SelectedValue), "List", True)
            With cboFieldValue2
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
            Call cboFieldValue2_SelectedIndexChanged(cboFieldValue2, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboFieldValue2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue2.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objOwrField3.getComboList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue2.SelectedValue), "List", True)
            With cboFieldValue3
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With

            Call cboFieldValue3_SelectedIndexChanged(cboFieldValue3, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboFieldValue3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue3.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objOwrField4.getComboList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue3.SelectedValue), "List", True)
            With cboFieldValue4
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
            Call cboFieldValue4_SelectedIndexChanged(cboFieldValue4, Nothing)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboFieldValue4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFieldValue4.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objOwrField5.getComboList(CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), CInt(cboFieldValue4.SelectedValue), "List", True)
            With cboFieldValue5
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboOwrField1Allocations_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOwrField1Allocations.SelectedIndexChanged
        Try
            Dim dList As New DataSet
            cboOwner.DataSource = Nothing
            Select Case CInt(cboOwrField1Allocations.SelectedValue)
                Case enAllocation.BRANCH
                    Dim objBranch As New clsStation
                    dList = objBranch.getComboList("List", True)
                    With cboOwrField1Owner
                        .DataValueField = "stationunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0).Copy
                        .DataBind()
                        .SelectedValue = CInt(cboOwner.SelectedValue)
                        .Enabled = False
                    End With
                Case enAllocation.DEPARTMENT_GROUP
                    Dim objDeptGrp As New clsDepartmentGroup
                    dList = objDeptGrp.getComboList("List", True)
                    With cboOwrField1Owner
                        .DataValueField = "deptgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = CInt(cboOwner.SelectedValue)
                        .Enabled = False
                    End With
                Case enAllocation.DEPARTMENT
                    Dim objDept As New clsDepartment
                    dList = objDept.getComboList("List", True)
                    With cboOwrField1Owner
                        .DataValueField = "departmentunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = CInt(cboOwner.SelectedValue)
                        .Enabled = False
                    End With
                Case enAllocation.SECTION_GROUP
                    Dim objSecGrp As New clsSectionGroup
                    dList = objSecGrp.getComboList("List", True)
                    With cboOwrField1Owner
                        .DataValueField = "sectiongroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = CInt(cboOwner.SelectedValue)
                        .Enabled = False
                    End With
                Case enAllocation.SECTION
                    Dim objSec As New clsSections
                    dList = objSec.getComboList("List", True)
                    With cboOwrField1Owner
                        .DataValueField = "sectionunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = CInt(cboOwner.SelectedValue)
                        .Enabled = False
                    End With
                Case enAllocation.UNIT_GROUP
                    Dim objUnitGrp As New clsUnitGroup
                    dList = objUnitGrp.getComboList("List", True)
                    With cboOwrField1Owner
                        .DataValueField = "unitgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = CInt(cboOwner.SelectedValue)
                        .Enabled = False
                    End With
                Case enAllocation.UNIT
                    Dim objUnit As New clsUnits
                    dList = objUnit.getComboList("List", True)
                    With cboOwrField1Owner
                        .DataValueField = "unitunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = CInt(cboOwner.SelectedValue)
                        .Enabled = False
                    End With
                Case enAllocation.TEAM
                    Dim objTeam As New clsTeams
                    dList = objTeam.getComboList("List", True)
                    With cboOwrField1Owner
                        .DataValueField = "teamunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = CInt(cboOwner.SelectedValue)
                        .Enabled = False
                    End With
                Case enAllocation.JOB_GROUP
                    Dim objJobGrp As New clsJobGroup
                    dList = objJobGrp.getComboList("List", True)
                    With cboOwrField1Owner
                        .DataValueField = "jobgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = CInt(cboOwner.SelectedValue)
                        .Enabled = False
                    End With
                Case enAllocation.JOBS
                    Dim objJob As New clsJobs
                    dList = objJob.getComboList("List", True)
                    With cboOwrField1Owner
                        .DataValueField = "jobunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = CInt(cboOwner.SelectedValue)
                        .Enabled = False
                    End With
                Case enAllocation.CLASS_GROUP
                    Dim objClsGrp As New clsClassGroup
                    dList = objClsGrp.getComboList("List", True)
                    With cboOwrField1Owner
                        .DataValueField = "classgroupunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = CInt(cboOwner.SelectedValue)
                        .Enabled = False
                    End With
                Case enAllocation.CLASSES
                    Dim objCls As New clsClass
                    dList = objCls.getComboList("List", True)
                    With cboOwrField1Owner
                        .DataValueField = "classesunkid"
                        .DataTextField = "name"
                        .DataSource = dList.Tables(0)
                        .DataBind()
                        .SelectedValue = CInt(cboOwner.SelectedValue)
                        .Enabled = False
                    End With
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboOwrField2OwrFieldValue1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOwrField2OwrFieldValue1.SelectedIndexChanged
        Dim mintOwrField2ParentId As Integer = 0
        Try
            mintOwrField2ParentId = CInt(cboOwrField2OwrFieldValue1.SelectedValue)
            If CInt(cboOwrField2OwrFieldValue1.SelectedValue) > 0 Then
                Dim objOwrField1 As New clsassess_owrfield1_master
                objOwrField1._Owrfield1unkid = CInt(cboOwrField2OwrFieldValue1.SelectedValue)
                txtOwrField2Period.Text = objOwrField1._PeriodName
                objOwrField1 = Nothing
            Else
                txtOwrField2Period.Text = ""
            End If
            Me.ViewState("mintOwrField2ParentId") = mintOwrField2ParentId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboOwrField3OwrFieldValue2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOwrField3OwrFieldValue2.SelectedIndexChanged
        Dim mintOwrField3ParentId As Integer
        Try
            mintOwrField3ParentId = CInt(cboOwrField3OwrFieldValue2.SelectedValue)
            If CInt(cboOwrField3OwrFieldValue2.SelectedValue) > 0 Then
                Dim objOwrField2 As New clsassess_owrfield2_master
                Dim objOwrField1 As New clsassess_owrfield1_master
                objOwrField2._Owrfield2unkid = CInt(cboOwrField3OwrFieldValue2.SelectedValue)
                objOwrField1._Owrfield1unkid = objOwrField2._Owrfield1unkid
                txtOwrField3Period.Text = objOwrField1._PeriodName
                objOwrField3txtOwrField1.Text = objOwrField1._Field_Data
                objOwrField1 = Nothing : objOwrField2 = Nothing
            Else
                txtOwrField3Period.Text = "" : objOwrField3txtOwrField1.Text = ""
            End If
            Me.ViewState("mintOwrField3ParentId") = mintOwrField3ParentId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub cboOwrField4OwrFieldValue3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOwrField4OwrFieldValue3.SelectedIndexChanged
        Dim mintOwrField4ParentId As Integer
        Try
            mintOwrField4ParentId = CInt(cboOwrField4OwrFieldValue3.SelectedValue)
            If CInt(cboOwrField4OwrFieldValue3.SelectedValue) > 0 Then
                Dim objField3 As New clsassess_owrfield3_master
                Dim objField2 As New clsassess_owrfield2_master
                Dim objField1 As New clsassess_owrfield1_master
                objField3._Owrfield3unkid = CInt(cboOwrField4OwrFieldValue3.SelectedValue)
                objField2._Owrfield2unkid = objField3._Owrfield2unkid
                objField1._Owrfield1unkid = objField2._Owrfield1unkid
                objOwrField4txtOwrField2.Text = objField2._Field_Data
                objOwrField4txtOwrField1.Text = objField1._Field_Data
                txtOwrField4Period.Text = objField1._PeriodName
                objField3 = Nothing : objField2 = Nothing : objField1 = Nothing
            Else
                txtOwrField4Period.Text = "" : objOwrField4txtOwrField1.Text = ""
                objOwrField4txtOwrField2.Text = ""
            End If
            Me.ViewState("mintOwrField4ParentId") = mintOwrField4ParentId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboOwrField5OwrFieldValue4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboOwrField5OwrFieldValue4.SelectedIndexChanged
        Dim mintOwrField5ParentId As Integer
        Try
            mintOwrField5ParentId = CInt(cboOwrField5OwrFieldValue4.SelectedValue)
            If CInt(cboOwrField5OwrFieldValue4.SelectedValue) > 0 Then
                Dim objField4 As New clsassess_owrfield4_master
                Dim objField3 As New clsassess_owrfield3_master
                Dim objField2 As New clsassess_owrfield2_master
                Dim objField1 As New clsassess_owrfield1_master
                objField4._Owrfield4unkid = CInt(cboOwrField5OwrFieldValue4.SelectedValue)
                objField3._Owrfield3unkid = objField4._Owrfield3unkid
                objField2._Owrfield2unkid = objField3._Owrfield2unkid
                objField1._Owrfield1unkid = objField2._Owrfield1unkid

                objOwrField5txtOwrField1.Text = objField1._Field_Data
                txtOwrField5Period.Text = objField1._PeriodName
                objOwrField5txtOwrField2.Text = objField2._Field_Data
                objOwrField5txtOwrField3.Text = objField3._Field_Data
                objField3 = Nothing : objField2 = Nothing : objField1 = Nothing
            Else
                objOwrField5txtOwrField1.Text = "" : objOwrField5txtOwrField2.Text = "" : objOwrField5txtOwrField3.Text = ""
                txtOwrField5Period.Text = ""
            End If
            Me.ViewState("mintOwrField5ParentId") = mintOwrField5ParentId
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Controls Event(S) "
    Protected Sub imgAdd_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
            Me.ViewState("AddRowIndex") = row.RowIndex
            Genrate_AddmanuItem()
            ''Shani [ 10 DEC 2014 ] -- START
            ''
            'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & imgAdd.ClientID & ");", True)
            ''Shani [ 10 DEC 2014 ] -- END
            popAddButton.X = CInt(hdf_locationx.Value)
            popAddButton.Y = CInt(hdf_locationy.Value)
            popAddButton.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub imgEdit_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
            Me.ViewState("EditRowIndex") = row.RowIndex
            Genrate_EditmanuItem()
            'Shani [ 10 DEC 2014 ] -- START
            '
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & imgAdd.ClientID & ");", True)
            'Shani [ 10 DEC 2014 ] -- END
            popEditButton.X = CInt(hdf_locationx.Value)
            popEditButton.Y = CInt(hdf_locationy.Value)
            popEditButton.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub imgDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        Try
            Dim imgAdd As ImageButton = TryCast(sender, ImageButton)
            Dim row As GridViewRow = TryCast(imgAdd.NamingContainer, GridViewRow)
            Me.ViewState("DeleteRowIndex") = row.RowIndex
            Genrate_DeletemanuItem()
            'Shani [ 10 DEC 2014 ] -- START
            '
            'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & pnl_dgvData.ClientID & ");", True)
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "JsStatus", "setscrollPosition(" & imgAdd.ClientID & ");", True)

            'Shani [ 10 DEC 2014 ] -- END
            popDeleteButton.X = CInt(hdf_locationx.Value)
            popDeleteButton.Y = CInt(hdf_locationy.Value)
            popDeleteButton.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ilnk As LinkButton = CType(sender, LinkButton)
            Dim mdtFinal As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)
            If CInt(cboOwnerType.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Owner Type, Owner and Period as mandatory information. Please select Owner Type, Owner and Period to continue."), Me)
                Exit Sub
            End If

            Select Case CInt(ilnk.CommandArgument.ToString.Split("|")(1))
                Case 1
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call OwrField1_load(-1, enAction.ADD_CONTINUE)
                    pop_objfrmAddEditOwrField1.Show()
                Case 2
                    Dim mintOwrField2ParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("owrfield1unkid"))
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call OwrField2_load(-1, enAction.ADD_CONTINUE, mintOwrField2ParentId)
                    pop_objfrmAddEditOwrField2.Show()
                Case 3
                    Dim mintOwrField3ParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("owrfield2unkid"))
                    Dim mintOwrField3MainParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("owrfield1unkid"))
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call OwrField3_load(-1, enAction.ADD_CONTINUE, mintOwrField3ParentId, mintOwrField3MainParentId)
                    pop_objfrmAddEditOwrField3.Show()
                Case 4
                    Dim mintOwrField4ParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("owrfield3unkid"))
                    Dim mintOwrField4MainParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("owrfield2unkid"))
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call OwrField4_load(-1, enAction.ADD_CONTINUE, mintOwrField4ParentId, mintOwrField4MainParentId)
                    pop_objfrmAddEditOwrField4.Show()
                Case 5
                    Dim mintOwrField5ParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("owrfield4unkid"))
                    Dim mintOwrField5MainParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("AddRowIndex")))("owrfield3unkid"))
                    Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                    Call OwrField5_load(-1, enAction.ADD_CONTINUE, mintOwrField5ParentId, mintOwrField5MainParentId)
                    pop_objfrmAddEditOwrField5.Show()
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ilnk As LinkButton = CType(sender, LinkButton)
            Dim irow As DataListItem = CType(ilnk.NamingContainer, DataListItem)
            Dim hdf As HiddenField = CType(dlmnuEdit.Items(irow.ItemIndex).FindControl("objEditOrignalName"), HiddenField)
            Dim mdtFinal As DataTable = CType(Me.ViewState("mdtFinal"), DataTable)

            If CInt(cboOwnerType.SelectedValue) <= 0 Or CInt(cboOwner.SelectedValue) <= 0 Or CInt(cboPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Owner Type, Owner and Period as mandatory information. Please select Owner Type, Owner and Period to continue."), Me)
                Exit Sub
            End If

            Select Case CInt(ilnk.CommandArgument.ToString.Split("|")(1))
                Case 1
                    If CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield1unkid")) > 0 Then
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call OwrField1_load(CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield1unkid")), enAction.EDIT_ONE)
                        pop_objfrmAddEditOwrField1.Show()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                         " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
                Case 2
                    If CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield2unkid")) > 0 Then
                        Dim mintOwrField2ParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield1unkid"))
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call OwrField2_load(CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield2unkid")), enAction.EDIT_ONE, mintOwrField2ParentId)
                        pop_objfrmAddEditOwrField2.Show()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                         " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                         " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
                Case 3
                    If CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield3unkid")) > 0 Then
                        Dim mintOwrField3ParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield2unkid"))
                        Dim mintOwrField3MainParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield1unkid"))
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call OwrField3_load(CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield3unkid")), enAction.EDIT_ONE, mintOwrField3ParentId, mintOwrField3MainParentId)
                        pop_objfrmAddEditOwrField3.Show()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                        " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                        " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
                Case 4
                    If CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield4unkid")) > 0 Then
                        Dim mintOwrField4ParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield3unkid"))
                        Dim mintOwrField4MainParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield2unkid"))
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call OwrField4_load(CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield4unkid")), enAction.EDIT_ONE, mintOwrField4ParentId, mintOwrField4MainParentId)
                        pop_objfrmAddEditOwrField4.Show()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                        " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                        " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
                Case 5
                    'SHANI (24 APR 2015)-START
                    'ISSUE REPORTED BY ANDREW ON NWB TRAINING ON PA
                    'If CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield5unkid")) > 0 > 0 Then
                    If CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield5unkid")) > 0 Then
                        'SHANI (24 APR 2015)--END
                        Dim mintOwrField5ParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield4unkid"))
                        Dim mintOwrField5MainParentId As Integer = CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield3unkid"))
                        Me.ViewState("AddFieldUnkid") = ilnk.CommandArgument
                        Call OwrField5_load(CInt(mdtFinal.Rows(CInt(Me.ViewState("EditRowIndex")))("owrfield5unkid")), enAction.EDIT_ONE, mintOwrField5ParentId, mintOwrField5MainParentId)
                        pop_objfrmAddEditOwrField5.Show()
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, there is no") & _
                                        " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "defined. Please define") & _
                                        " " & hdf.Value & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "in order to perform edit operation."), Me)
                        Exit Sub
                    End If
            End Select
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ilnk As LinkButton = CType(sender, LinkButton)
            Dim irow As DataListItem = CType(ilnk.NamingContainer, DataListItem)
            Dim hdf As HiddenField = CType(dlmnuDelete.Items(irow.ItemIndex).FindControl("objDeleteOrignalName"), HiddenField)
            Me.ViewState("DeleteScreenId") = ilnk.CommandArgument.ToString().Trim
            Me.ViewState("DeleteColumnName") = hdf.Value
            lblTitle.Text = "Aruti"
            Dim iMsg As String = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 26, "You are about to remove the information from owner level, this will delete all child linked to this parent") & vbCrLf & _
                                 Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 27, "Are you sure you want to delete?")
            lblMessage.Text = iMsg
            txtMessage.Text = ""
            popup_YesNo.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField1Remark1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField1Remark1.TextChanged
        Try
            Dim mdicOwrField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField1FieldData"), Dictionary(Of Integer, String))
            mdicOwrField1FieldData(CInt(hdfOwrField1Tag1.Value)) = txtOwrField1Remark1.Text
            Me.ViewState("mdicOwrField1FieldData") = mdicOwrField1FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField1Remark2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField1Remark2.TextChanged
        Try
            Dim mdicOwrField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField1FieldData"), Dictionary(Of Integer, String))
            mdicOwrField1FieldData(CInt(hdfOwrField1Tag2.Value)) = txtOwrField1Remark2.Text
            Me.ViewState("mdicOwrField1FieldData") = mdicOwrField1FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField1Remark3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField1Remark3.TextChanged
        Try
            Dim mdicOwrField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField1FieldData"), Dictionary(Of Integer, String))
            mdicOwrField1FieldData(CInt(hdfOwrField1Tag3.Value)) = txtOwrField1Remark3.Text
            Me.ViewState("mdicOwrField1FieldData") = mdicOwrField1FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField2Remark1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField2Remark1.TextChanged
        Try
            Dim mdicOwrField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField2FieldData"), Dictionary(Of Integer, String))
            mdicOwrField1FieldData(CInt(hdfOwrField2Tag1.Value)) = txtOwrField2Remark1.Text
            Me.ViewState("mdicOwrField2FieldData") = mdicOwrField1FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField2Remark2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField2Remark2.TextChanged
        Try
            Dim mdicOwrField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField2FieldData"), Dictionary(Of Integer, String))
            mdicOwrField1FieldData(CInt(hdfOwrField2Tag2.Value)) = txtOwrField2Remark2.Text
            Me.ViewState("mdicOwrField2FieldData") = mdicOwrField1FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField2Remark3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField2Remark3.TextChanged
        Try
            Dim mdicOwrField1FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField2FieldData"), Dictionary(Of Integer, String))
            mdicOwrField1FieldData(CInt(hdfOwrField2Tag3.Value)) = txtOwrField2Remark3.Text
            Me.ViewState("mdicOwrField1FieldData") = mdicOwrField1FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField3Remark1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField3Remark1.TextChanged
        Try
            Dim mdicOwrField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField3FieldData"), Dictionary(Of Integer, String))
            mdicOwrField3FieldData(CInt(hdf_lblOwrField3Remark1.Value)) = txtOwrField3Remark1.Text
            Me.ViewState("mdicOwrField3FieldData") = mdicOwrField3FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField3Remark2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField3Remark2.TextChanged
        Try
            Dim mdicOwrField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField3FieldData"), Dictionary(Of Integer, String))
            mdicOwrField3FieldData(CInt(hdf_lblOwrField3Remark2.Value)) = txtOwrField3Remark2.Text
            Me.ViewState("mdicOwrField3FieldData") = mdicOwrField3FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField3Remark3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField3Remark3.TextChanged
        Try
            Dim mdicOwrField3FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField3FieldData"), Dictionary(Of Integer, String))
            mdicOwrField3FieldData(CInt(hdf_lblOwrField3Remark3.Value)) = txtOwrField3Remark3.Text
            Me.ViewState("mdicOwrField3FieldData") = mdicOwrField3FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField4Remark1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField4Remark1.TextChanged
        Try
            Dim mdicOwrField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField4FieldData"), Dictionary(Of Integer, String))
            mdicOwrField4FieldData(CInt(hdf_txtOwrField4Remark1.Value)) = txtOwrField4Remark1.Text
            Me.ViewState("mdicOwrField4FieldData") = mdicOwrField4FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField4Remark2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField4Remark2.TextChanged
        Try
            Dim mdicOwrField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField4FieldData"), Dictionary(Of Integer, String))
            mdicOwrField4FieldData(CInt(hdf_txtOwrField4Remark2.Value)) = txtOwrField4Remark2.Text
            Me.ViewState("mdicOwrField4FieldData") = mdicOwrField4FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField4Remark3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField4Remark3.TextChanged
        Try
            Dim mdicOwrField4FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField4FieldData"), Dictionary(Of Integer, String))
            mdicOwrField4FieldData(CInt(hdf_txtOwrField4Remark3.Value)) = txtOwrField4Remark3.Text
            Me.ViewState("mdicOwrField4FieldData") = mdicOwrField4FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField5Remark1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField5Remark1.TextChanged
        Try
            Dim mdicOwrField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField5FieldData"), Dictionary(Of Integer, String))
            mdicOwrField5FieldData(CInt(hdf_txtOwrField5Remark1.Value)) = txtOwrField5Remark1.Text
            Me.ViewState("mdicOwrField5FieldData") = mdicOwrField5FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField5Remark2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField5Remark2.TextChanged
        Try
            Dim mdicOwrField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField5FieldData"), Dictionary(Of Integer, String))
            mdicOwrField5FieldData(CInt(hdf_txtOwrField5Remark2.Value)) = txtOwrField5Remark2.Text
            Me.ViewState("mdicOwrField5FieldData") = mdicOwrField5FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField5Remark3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField5Remark3.TextChanged
        Try
            Dim mdicOwrField5FieldData As Dictionary(Of Integer, String) = CType(Me.ViewState("mdicOwrField5FieldData"), Dictionary(Of Integer, String))
            mdicOwrField5FieldData(CInt(hdf_txtOwrField5Remark3.Value)) = txtOwrField5Remark3.Text
            Me.ViewState("mdicOwrField5FieldData") = mdicOwrField5FieldData
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField1SearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField1SearchEmp.TextChanged
        Try
            Call Fill_Data_OwrField1()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField2SearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField2SearchEmp.TextChanged
        Try
            Call Fill_Data_OwrField2()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField3SearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField3SearchEmp.TextChanged
        Try
            Call Fill_Data_OwrField3()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField4SearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField4SearchEmp.TextChanged
        Try
            Call Fill_Data_OwrField4()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtOwrField5SearchEmp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwrField5SearchEmp.TextChanged
        Try
            Call Fill_Data_OwrField5()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " CheckBox Event(S) "

    Protected Sub chkOwrField1AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField1All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvOwrField1Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkOwrField1select"), CheckBox)
                chk.Checked = chkOwrField1All.Checked
                Call GoalOwrField1Operation(iRow.Cells(3).Text, CBool(chkOwrField1All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkOwrField1select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField1 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField1.NamingContainer, DataGridItem)
            If chkOwrField1 IsNot Nothing Then
                Call GoalOwrField1Operation(dgvOwrField1Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField1.Checked))
            End If
            Call Fill_Data_OwrField1()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkOwrField2AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField2All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvOwrField2Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkOwrField2select"), CheckBox)
                chk.Checked = chkOwrField2All.Checked
                Call GoalOwrField2Operation(iRow.Cells(3).Text, CBool(chkOwrField2All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkOwrField2select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField2 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField2.NamingContainer, DataGridItem)
            If chkOwrField2 IsNot Nothing Then
                Call GoalOwrField2Operation(dgvOwrField2Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField2.Checked))
            End If
            Call Fill_Data_OwrField2()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkOwrField3AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField3All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvOwrField3Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkOwrField3select"), CheckBox)
                chk.Checked = chkOwrField3All.Checked
                Call GoalOwrField3Operation(iRow.Cells(3).Text, CBool(chkOwrField3All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkOwrField3select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField3 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField3.NamingContainer, DataGridItem)
            If chkOwrField3 IsNot Nothing Then
                Call GoalOwrField3Operation(dgvOwrField3Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField3.Checked))
            End If
            Call Fill_Data_OwrField3()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkOwrField4AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField4All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvOwrField4Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkOwrField4select"), CheckBox)
                chk.Checked = chkOwrField4All.Checked
                Call GoalOwrField4Operation(iRow.Cells(3).Text, CBool(chkOwrField4All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkOwrField4select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField4 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField4.NamingContainer, DataGridItem)
            If chkOwrField4 IsNot Nothing Then
                Call GoalOwrField4Operation(dgvOwrField4Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField4.Checked))
            End If
            Call Fill_Data_OwrField4()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkOwrField5AllSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField5All As CheckBox = CType(sender, CheckBox)
            For Each iRow As DataGridItem In dgvOwrField5Owner.Items
                Dim chk As CheckBox = CType(iRow.FindControl("chkOwrField5select"), CheckBox)
                chk.Checked = chkOwrField5All.Checked
                Call GoalOwrField5Operation(iRow.Cells(3).Text, CBool(chkOwrField5All.Checked))
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkOwrField5select_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkOwrField5 As CheckBox = CType(sender, CheckBox)
            Dim row As DataGridItem = TryCast(chkOwrField5.NamingContainer, DataGridItem)
            If chkOwrField5 IsNot Nothing Then
                Call GoalOwrField5Operation(dgvOwrField5Owner.Items(row.ItemIndex).Cells(3).Text, CBool(chkOwrField5.Checked))
            End If
            Call Fill_Data_OwrField5()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Nilay (19-Jun-2015) -- Start
#Region "Update Process Popup"

#Region "Private Methods"

    Private Sub UpdateProcessLoad(ByVal dgvRow As GridViewRow)

        Dim objFMapping As New clsAssess_Field_Mapping
        Dim iColName As String = ""
        Dim iExOrder As Integer = 0
        mintFieldUnkid = 0
        mintLinkedFld = 0
        mintFieldTypeId = 0
        mdtPeriodStartDate = Nothing
        mstrDefinedGoal = ""
        mintTableFieldTranUnkId = 0

        Try
            mintFieldUnkid = objFMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))
            If mintFieldUnkid < 0 Then Exit Sub

            Dim objFMaster As New clsAssess_Field_Master
            iExOrder = objFMaster.Get_Field_ExOrder(mintFieldUnkid)
            Select Case iExOrder
                Case 1
                    iColName = "owrfield1unkid"
                Case 2
                    iColName = "owrfield2unkid"
                Case 3
                    iColName = "owrfield3unkid"
                Case 4
                    iColName = "owrfield4unkid"
                Case 5
                    iColName = "owrfield5unkid"
            End Select

            mintTableFieldTranUnkId = CType(Me.ViewState("mdtFinal"), DataTable).Rows(dgvRow.RowIndex)(iColName)
            mstrDefinedGoal = CType(Me.ViewState("mdtFinal"), DataTable).Rows(dgvRow.RowIndex)("Field" & mintFieldUnkid.ToString)

            txtUpdatePeriod.Text = cboPeriod.SelectedItem.Text
            txtUpdateGoals.Text = mstrDefinedGoal
            txtUpdateOwner.Text = cboOwner.SelectedItem.Text

            Dim strLinkedFld As String = objFMapping.Get_Map_FieldName(CInt(cboPeriod.SelectedValue))
            mintLinkedFld = objFMapping.Get_Map_FieldId(CInt(cboPeriod.SelectedValue))

            Dim mintExOrder As Integer = objFMaster.Get_Field_ExOrder(mintLinkedFld)
            Select Case mintExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD1
                Case enWeight_Types.WEIGHT_FIELD2
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD2
                Case enWeight_Types.WEIGHT_FIELD3
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD3
                Case enWeight_Types.WEIGHT_FIELD4
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD4
                Case enWeight_Types.WEIGHT_FIELD5
                    mintFieldTypeId = enWeight_Types.WEIGHT_FIELD5
            End Select
            objFMapping = Nothing
            objFMaster = Nothing

            Dim objEvaluation As New clsevaluation_analysis_master
            'S.SANDEEP |26-AUG-2019| -- START
            'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            'If objEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue)) = True Then
            If objEvaluation.isExist(enAssessmentMode.SELF_ASSESSMENT, CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), , , , , , False) = True Then
                'S.SANDEEP |26-AUG-2019| -- END
                dgvHistory.Columns(0).Visible = False
                dgvHistory.Columns(1).Visible = False
                btnUpdateSave.Enabled = False
            End If
            objEvaluation = Nothing

            Dim objPrd As New clscommom_period_Tran

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objPrd._Periodunkid = CInt(cboPeriod.SelectedValue)
            objPrd._Periodunkid(Session("Database_Name")) = CInt(cboPeriod.SelectedValue)
            'Shani(20-Nov-2015) -- End

            mdtPeriodStartDate = objPrd._Start_Date
            objPrd = Nothing

            Call FillUpdateCombo()
            Call FillUpdateHistory()
            Call ClearUpdateControls()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("UpdateProcessLoad:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillUpdateCombo()
        Try
            Dim dsList As New DataSet
            Dim objMData As New clsMasterData

            dsList = objMData.Get_CompanyGoal_Status("List", True)
            With cboUpdateStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillUpdateCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillUpdateHistory()
        Try

            'Pinkal (16-Apr-2016) -- Start [Changes Done By Shani]
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            Dim dtViewTable As New DataTable
            dtViewTable = objOUpdateProgress.GetList("List", CInt(cboOwner.SelectedValue), CInt(cboPeriod.SelectedValue), mintTableFieldTranUnkId).Tables(0)

            If dtViewTable IsNot Nothing Then
                dgvHistory.DataSource = dtViewTable
                dgvHistory.DataBind()
            End If


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillUpdateGrid:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub GetUpdateValue()
        Try
            txtUpdatePercent.Text = objOUpdateProgress._Pct_Completed
            txtUpdateRemark.Text = objOUpdateProgress._Remark
            dtpUpdateChangeDate.SetDate = objOUpdateProgress._Updatedate
            cboUpdateStatus.SelectedValue = objOUpdateProgress._Statusunkid
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetUpdateValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetUpdateValue()
        Try
            objOUpdateProgress._Owrupdatetranunkid = mintOwnerUpdateTranunkid
            objOUpdateProgress._Owrfieldtypeid = mintFieldTypeId
            objOUpdateProgress._Owrfieldunkid = mintTableFieldTranUnkId
            objOUpdateProgress._Fieldunkid = mintFieldUnkid
            objOUpdateProgress._Ownerunkid = CInt(cboOwner.SelectedValue)
            objOUpdateProgress._Isvoid = False
            objOUpdateProgress._Pct_Completed = CDec(txtUpdatePercent.Text)
            objOUpdateProgress._Periodunkid = CInt(cboPeriod.SelectedValue)
            objOUpdateProgress._Remark = txtUpdateRemark.Text
            objOUpdateProgress._Statusunkid = CInt(cboUpdateStatus.SelectedValue)
            objOUpdateProgress._Updatedate = dtpUpdateChangeDate.GetDate.Date
            objOUpdateProgress._Userunkid = CInt(Session("UserId"))
            objOUpdateProgress._Voiddatetime = Nothing
            objOUpdateProgress._Voidreason = ""
            objOUpdateProgress._Voiduserunkid = -1

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetUpdateValue:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub ClearUpdateControls()
        Try
            dtpUpdateChangeDate.SetDate = Nothing
            mintOwnerUpdateTranunkid = 0
            cboStatus.SelectedValue = 0
            txtUpdatePercent.Text = 0
            txtUpdateRemark.Text = ""

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ClearUpdateControls:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function IsValidUpdateInfo() As Boolean
        Try
            If dtpUpdateChangeDate.IsNull = True Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, Date is mandatory information. Please set date to continue."), Me)
                dtpUpdateChangeDate.Focus()
                Return False
            End If

            If dtpUpdateChangeDate.GetDate.Date < mdtPeriodStartDate.Date Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Sorry, Date cannot be less than period start date."), Me)
                dtpUpdateChangeDate.Focus()
                Return False
            End If

            If dtpUpdateChangeDate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 102, "Sorry, Date cannot be greater than today date."), Me)
                dtpUpdateChangeDate.Focus()
                Return False
            End If

            If CInt(cboUpdateStatus.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Sorry, Change Status is mandatory information. Please select change status to continue."), Me)
                cboUpdateStatus.Focus()
                Return False
            End If

            'Shani (05-Sep-2016) -- Start
            'If CDec(txtUpdatePercent.Text) <= 0 Then
            '    'Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Sorry, % completed is mandatory information. Please enter % completed to continue."), Me)
            '    txtUpdatePercent.Focus()
            '    Return False
            'End If
            'Shani (05-Sep-2016) -- End
            If CDec(txtUpdatePercent.Text) > 100 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 8, "Sorry, Percentage Completed cannot be greater than 100."), Me)
                txtUpdatePercent.Focus()
                Return False
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidUpdateInfo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return True
    End Function

    Private Sub Save_UpdateProgress()
        Dim blnFlag As Boolean = False
        Try
            Call SetUpdateValue()

            If mintOwnerUpdateTranunkid <= 0 Then 'INSERT
                blnFlag = objOUpdateProgress.Insert()
            ElseIf mintOwnerUpdateTranunkid > 0 Then  'UPDATE
                blnFlag = objOUpdateProgress.Update()
            End If

            If blnFlag = True Then
                Call ClearUpdateControls()
                Call FillUpdateHistory()
            Else
                If objOUpdateProgress._Message <> "" Then
                    DisplayMessage.DisplayMessage(objOUpdateProgress._Message, Me)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Save_UpdateProgress:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#End Region
    'Nilay (19-Jun-2015) -- End

End Class
