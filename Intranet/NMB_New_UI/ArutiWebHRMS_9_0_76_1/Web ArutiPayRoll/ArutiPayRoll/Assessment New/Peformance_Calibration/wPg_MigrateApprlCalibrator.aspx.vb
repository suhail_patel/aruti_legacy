﻿#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data
Imports System.Web.Services

#End Region

Partial Class Assessment_New_Peformance_Calibration_wPg_MigrateApprlCalibrator
    Inherits Basepage

#Region " Private Variables "

    Private clsuser As New User
    Private DisplayMessage As New CommonCodes
    Private dtNewEmp As DataTable
    Private dtAssignedEmp As DataTable
    Private mstrModuleName As String = "frmMigrateApprlCalibrator"
    Private objCAMstr As New clscalibrate_approver_master
    Private objCATran As New clscalibrate_approver_tran

#End Region

#Region " Page Events "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            btnTransfer.Visible = Session("AllowtoPerformAssessorReviewerMigration")

            If IsPostBack = False Then
                Call radMode_SelectedIndexChanged(New Object, New EventArgs)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Function & Procedures "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Try
            Dim blnCalibrator As Boolean = False
            If radMode.SelectedValue = "1" Then
                blnCalibrator = False
            ElseIf radMode.SelectedValue = "2" Then
                blnCalibrator = True
            End If
            dsList = objCAMstr.GetList("List", True, "", 0, Nothing, True, blnCalibrator)
            With cboFromValue
                .DataValueField = "mapuserunkid"
                .DataTextField = "approver"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With
            With cboToValue
                .DataValueField = "mapuserunkid"
                .DataTextField = "approver"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLevel(ByVal intUserId As Integer, ByVal txt As TextBox)
        Try
            If radMode.SelectedValue = "1" Then
                Dim dsList As New DataSet
                Dim blnCalibrator As Boolean = False
                If radMode.SelectedValue = "1" Then
                    blnCalibrator = False
                ElseIf radMode.SelectedValue = "2" Then
                    blnCalibrator = True
                End If
                dsList = objCAMstr.GetList("List", True, "mapuserunkid = '" & intUserId & "'", 0, Nothing, False, blnCalibrator)
                If dsList.Tables("List").Rows.Count > 0 Then
                    txt.Text = dsList.Tables(0).Rows(0)("Level").ToString()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillFromEmployee()
        Dim dsfemp As New DataSet
        Try
            Dim blnCalibrator As Boolean = False
            If radMode.SelectedValue = "1" Then
                blnCalibrator = False
            ElseIf radMode.SelectedValue = "2" Then
                blnCalibrator = True
            End If
            dsfemp = objCATran.GetMappedEmployeecombolist(Session("Database_Name"), CInt(cboFromValue.SelectedValue), blnCalibrator, Session("EmployeeAsOnDate"), "List", False, Nothing)
            dgvFrmEmp.AutoGenerateColumns = False
            dgvFrmEmp.DataSource = dsfemp.Tables(0)
            dgvFrmEmp.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillToEmployee()
        Dim dsfemp As New DataSet
        Try
            Dim blnCalibrator As Boolean = False
            If radMode.SelectedValue = "1" Then
                blnCalibrator = False
            ElseIf radMode.SelectedValue = "2" Then
                blnCalibrator = True
            End If
            dsfemp = objCATran.GetMappedEmployeecombolist(Session("Database_Name"), CInt(cboToValue.SelectedValue), blnCalibrator, Session("EmployeeAsOnDate"), "List", False, Nothing)
            dgvToEmp.AutoGenerateColumns = False
            dgvToEmp.DataSource = dsfemp.Tables(0)
            dgvToEmp.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    <WebMethod()> _
    Public Shared Function TransferEmployee(ByVal intFromUserId As Integer, _
                                            ByVal intToUserId As Integer, _
                                            ByVal strEmpIds As String(), _
                                            ByVal blnIsCalibrator As Boolean, _
                                            ByVal intUserId As Integer, _
                                            ByVal strIPAddr As String, _
                                            ByVal strHostName As String, _
                                            ByVal strScreenName As String, _
                                            ByVal intOperationType As Integer, _
                                            ByVal strUserName As String, _
                                            ByVal strSenderAddress As String, _
                                            ByVal intCompanyId As Integer) As String
        Try
            Dim blnFlag As Boolean = False
            Dim objApprTran As New clscalibrate_approver_tran
            blnFlag = objApprTran.MigrateEmployee(intFromUserId, _
                                                  intToUserId, _
                                                  strEmpIds, _
                                                  blnIsCalibrator, _
                                                  intUserId, _
                                                  strIPAddr, _
                                                  strHostName, _
                                                  strScreenName, True, intOperationType, _
                                                  strUserName, _
                                                  strSenderAddress, _
                                                  intCompanyId, Nothing)
            objApprTran = Nothing

            If blnFlag Then
                Return "1"
            Else
                Return "0"
            End If            
        Catch ex As Exception
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                'S.SANDEEP |04-MAR-2020| -- START
                '._Error_Message = clsCrypto.Encrypt(ex.Message)
                'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
                Dim S_dispmsg As String = ex.Message & "; " & ex.StackTrace.ToString
                If ex.InnerException IsNot Nothing Then
                    S_dispmsg &= "; " & ex.InnerException.Message
                End If
                'Sohail (01 Feb 2020) -- End
                S_dispmsg = S_dispmsg.Replace("'", "")
                S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                ._Error_Message = S_dispmsg
                'S.SANDEEP |04-MAR-2020| -- END
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("UserId")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With
            Dim strDBName As String = ""
            If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    strDBName = "hrmsConfiguration"
                End If
            End If
            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            End If
            Throw New Exception("Something went wrong, Please contact administrator!")
        End Try
    End Function

#End Region

#Region " Button's Events "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect(Session("rootpath") & "Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Radio Event(s) "

    Protected Sub radMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radMode.SelectedIndexChanged
        Try
            objlblFromValue.Text = "From" & " " & radMode.SelectedItem.Text
            objlblToValue.Text = "To" & " " & radMode.SelectedItem.Text
            txtFrmLevel.Text = "" : txtToLevel.Text = ""
            lblCaption.Text = radMode.SelectedItem.Text
            Select Case radMode.SelectedValue
                Case "1"
                    pnllvel.Visible = True
                    Panel1.Visible = True
                Case "2"
                    pnllvel.Visible = False
                    Panel1.Visible = False
            End Select
            FillCombo()
            dgvFrmEmp.DataSource = Nothing : dgvFrmEmp.DataBind()
            dgvToEmp.DataSource = Nothing : dgvToEmp.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combox Event(s) "

    Protected Sub cboFromValue_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboFromValue.SelectedIndexChanged
        Try
            FillFromEmployee()
            SetLevel(CInt(cboFromValue.SelectedValue), txtFrmLevel)
            If dgvFrmEmp IsNot Nothing And dgvFrmEmp.Rows.Count <= 0 Then
                btnTransfer.Enabled = False            
            Else
                btnTransfer.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboToValue_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboToValue.SelectedIndexChanged
        Try
            FillToEmployee()
            SetLevel(CInt(cboToValue.SelectedValue), txtToLevel)
            If dgvToEmp IsNot Nothing And dgvToEmp.Rows.Count <= 0 Then
                btnTransfer.Enabled = False
            Else
                btnTransfer.Enabled = True
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

End Class
