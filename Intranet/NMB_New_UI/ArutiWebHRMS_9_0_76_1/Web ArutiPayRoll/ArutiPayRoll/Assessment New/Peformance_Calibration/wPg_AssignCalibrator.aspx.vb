﻿Imports Aruti.Data
Imports System.Data
Imports System.Drawing

Partial Class Assessment_New_Peformance_Calibration_wPg_AssignCalibrator
    Inherits Basepage

#Region "Private Variable"

    Private DisplayMessage As New CommonCodes
    Private objApproverMaster As New clscalibrate_approver_master
    Private objApproverTran As New clscalibrate_approver_tran
    Private mstrAdvanceFilter As String = String.Empty

    Private Shared ReadOnly mstrModuleName As String = "frmAssignCalibratorList"
    Private Shared ReadOnly mstrModuleName1 As String = "frmCalibratorAddedit"
    Private blnpopupApproverUseraccess As Boolean = False
    Private mintMappingUnkid As Integer
    Private currentId As String = ""
    Private Index As Integer
    Private mblActiveDeactiveApprStatus As Boolean
    Private mblnIsEditMode As Boolean = False

#End Region

#Region " Form Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                Call SetControlCaptions()
                'S.SANDEEP |27-JUL-2019| -- START
                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                Call SetMessages()
                'S.SANDEEP |27-JUL-2019| -- END
                Call SetLanguage()
'Call Language._Object.SaveValue()
                ListFillCombo()
                If CBool(Session("AllowToViewCalibratorList")) Then
                    FillList(False)
                Else
                    FillList(True)
                End If
                SetVisibility()
            Else

                'Gajanan [15-April-2020] -- Start
                'Enhancement:Worked On Calibration Approver Change
                mstrAdvanceFilter = Me.ViewState("mstrAdvanceFilter")
                mblnIsEditMode = CBool(ViewState("mblnIsEditMode"))
                'Gajanan [15-April-2020] -- End

                mintMappingUnkid = CInt(ViewState("mintMappingUnkid"))
                mblActiveDeactiveApprStatus = CBool(ViewState("mblActiveDeactiveApprStatus"))
                blnpopupApproverUseraccess = ViewState("blnpopupApproverUseraccess")

                'If ViewState("mdtApproverList") IsNot Nothing Then
                'mdtApproverList = CType(ViewState("mdtApproverList"), DataTable)
                '    If mdtApproverList.Rows.Count > 0 AndAlso CInt(mdtApproverList.Rows(0)("mappingunkid").ToString) > 0 Then
                '        gvCalibratorList.DataSource = mdtApproverList
                '        gvCalibratorList.DataBind()
                '    End If
                'End If


                If blnpopupApproverUseraccess Then
                    popupCalibratorUseraccess.Show()
                End If

                setPopupvisibility()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            ViewState("blnpopupApproverUseraccess") = blnpopupApproverUseraccess
            ViewState("mintMappingUnkid") = mintMappingUnkid
            ViewState("mblActiveDeactiveApprStatus") = mblActiveDeactiveApprStatus

            'Gajanan [15-April-2020] -- Start
            'Enhancement:Worked On Calibration Approver Change
            ViewState("mstrAdvanceFilter") = mstrAdvanceFilter
            ViewState("mblnIsEditMode") = mblnIsEditMode
            'Gajanan [15-April-2020] -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " List Methods "

    Private Sub FillList(ByVal isblank As Boolean)
        Dim dsApproverList As New DataSet
        Dim dtApprover As DataTable
        Dim strSearching As String = ""
        Try
            strSearching &= "AND TAM.iscalibrator = 1 "

            If CInt(drpCalibrator.SelectedValue) > 0 Then
                strSearching &= "AND TAM.mappingunkid = " & CInt(drpCalibrator.SelectedValue) & " "
            End If

            If CInt(drpStatus.SelectedValue) = 1 Then
                strSearching &= "AND TAM.isactive  = 1 "
            ElseIf CInt(drpStatus.SelectedValue) = 2 Then
                strSearching &= "AND TAM.isactive  = 0 "
            End If

            If isblank Or CBool(Session("AllowToViewCalibratorList")) = False Then
                strSearching = "AND 1 = 2 "
            End If

            If strSearching.Trim.Length > 0 Then strSearching = strSearching.Substring(3)

            dsApproverList = objApproverMaster.GetList("List", True, strSearching, 0, Nothing, False, True)

            If dsApproverList.Tables(0).Rows.Count <= 0 Then
                dsApproverList = objApproverMaster.GetList("List", True, "1 = 2", 0, Nothing, True)
                isblank = True
            End If

            If dsApproverList IsNot Nothing Then
                dtApprover = New DataView(dsApproverList.Tables("List"), "", "Level", DataViewRowState.CurrentRows).ToTable()
                Dim strLeaveName As String = ""
                Dim dtTable As DataTable = dtApprover.Clone
                dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))
                Dim dtRow As DataRow = Nothing
                For Each drow As DataRow In dtApprover.Rows
                    If CStr(drow("Level")).Trim <> strLeaveName.Trim Then
                        dtRow = dtTable.NewRow
                        dtRow("IsGrp") = True
                        dtRow("mappingunkid") = drow("mappingunkid")
                        dtRow("Level") = drow("Level")
                        strLeaveName = drow("Level").ToString()
                        dtTable.Rows.Add(dtRow)
                    End If
                    dtRow = dtTable.NewRow
                    For Each dtcol As DataColumn In dtApprover.Columns
                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next
                dtTable.AcceptChanges()
                gvCalibratorList.DataSource = dtTable
                gvCalibratorList.DataBind()

                If isblank = True Then
                    gvCalibratorList.Rows(0).Visible = False
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnNew.Visible = CBool(Session("AllowToAddCalibrator"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub AddGroup(ByVal rw As GridViewRow, ByVal title As String, ByVal gd As GridView)
        Try
            rw.Visible = False
            Dim row As GridViewRow = New GridViewRow(0, 0, DataControlRowType.DataRow, DataControlRowState.Normal)
            row.BackColor = ColorTranslator.FromHtml("#F9F9F9")
            Dim cell As TableCell = New TableCell()
            cell.Text = title
            cell.ColumnSpan = gd.Columns.Count
            row.Cells.Add(cell)
            gd.Controls(0).Controls.Add(row)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub ListFillCombo()
        Try
            Dim objMaster As New clsMasterData
            Dim dsCombo As DataSet = objMaster.getComboListTranHeadActiveInActive("List", False)
            With drpStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "1"
            End With

            Dim objApprover As New clscalibrate_approver_master
            Dim dsApprList As DataSet

            dsApprList = objApprover.GetList("List", True, "", 0, Nothing, True, True)

            drpCalibrator.DataTextField = "approver"
            drpCalibrator.DataValueField = "mappingunkid"
            drpCalibrator.DataSource = dsApprList.Tables("List")
            drpCalibrator.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub SetValueToPopup()
        Try
            drpCalibratorUseraccess_user.SelectedValue = objApproverMaster._Mapuserunkid
            'Gajanan [15-April-2020] -- Start
            'Enhancement:Worked On Calibration Approver Change
            If mintMappingUnkid > 0 Then
                setPopupvisibility()
                FillEmployee()
                FillApproverEmployeeList()
            End If
            'Gajanan [15-April-2020] -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#Region " Button Event "

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            FillAddEditCombo()
            Dim lnkedit As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkedit).NamingContainer, GridViewRow)
            objApproverMaster._Mappingunkid = lnkedit.CommandArgument.ToString()
            'Gajanan [15-April-2020] -- Start
            'Enhancement:Worked On Calibration Approver Change
            mintMappingUnkid = objApproverMaster._Mappingunkid
            'Gajanan [15-April-2020] -- End
            SetValueToPopup()
            mblnIsEditMode = True
            popupCalibratorUseraccess.Show()
            blnpopupApproverUseraccess = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkdelete As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkdelete).NamingContainer, GridViewRow)

            objApproverMaster._Mappingunkid = CInt(lnkdelete.CommandArgument.ToString())
            mintMappingUnkid = CInt(lnkdelete.CommandArgument.ToString())

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            If objApproverMaster.isUsed(CInt(lnkdelete.CommandArgument.ToString())) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Sorry, You cannot delete this Approver. Reason: This Approver is in use."), Me)
                Exit Sub
            End If
            'S.SANDEEP |27-JUL-2019| -- END

            confirmapproverdelete.Show()
            confirmapproverdelete.Title = "Confirmation"
            confirmapproverdelete.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 5, "Are You Sure You Want To Delete This Approver?")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkActive_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkActive As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkActive).NamingContainer, GridViewRow)
            Dim blnFlag As Boolean = False

            mintMappingUnkid = CInt(lnkActive.CommandArgument)
            mblActiveDeactiveApprStatus = True

            objApproverMaster._Mappingunkid = CInt(lnkActive.CommandArgument.ToString())
            popupconfirmActiveCalibrator.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkDeActive_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnkDeActive As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast((lnkDeActive).NamingContainer, GridViewRow)

            mintMappingUnkid = CInt(lnkDeActive.CommandArgument)
            mblActiveDeactiveApprStatus = False

            objApproverMaster._Mappingunkid = CInt(lnkDeActive.CommandArgument)

            'If objApproverMaster.isApproverInGrievanceUsed(objApproverMaster._Mappingunkid) Then
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Sorry, You cannot delete this Approver . Reason: This Approver is in use."), Me)
            '    Exit Sub
            'End If

            popupconfirmDeactiveCalibrator.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            FillAddEditCombo()
            blnpopupApproverUseraccess = True
            popupCalibratorUseraccess.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            FillList(False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupconfirmActiveCalibrator_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmActiveCalibrator.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False
            SetAtValue(mstrModuleName)
            blnFlag = objApproverMaster.InActiveApprover(mintMappingUnkid, CInt(Session("CompanyUnkId")), True)

            If blnFlag = False And objApproverMaster._Message <> "" Then
                DisplayMessage.DisplayMessage("popupconfirmActiveAppr_buttonYes_Click :- " & objApproverMaster._Message, Me)
            Else
                FillList(False)
                mintMappingUnkid = 0
                mblActiveDeactiveApprStatus = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupconfirmDeactiveCalibrator_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupconfirmDeactiveCalibrator.buttonYes_Click
        Try
            Dim blnFlag As Boolean = False
            SetAtValue(mstrModuleName)
            blnFlag = objApproverMaster.InActiveApprover(mintMappingUnkid, CInt(Session("CompanyUnkId")), False)
            If blnFlag = False And objApproverMaster._Message <> "" Then
                DisplayMessage.DisplayMessage("popupconfirmDeactiveAppr_buttonYes_Click :- " & objApproverMaster._Message, Me)
            Else
                FillList(False)
                mintMappingUnkid = 0
                mblActiveDeactiveApprStatus = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub DeleteCalibratorReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles DeleteCalibratorReason.buttonDelReasonYes_Click
        Try
            SetAtValue(mstrModuleName)
            objApproverMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
            objApproverMaster._Voidreason = DeleteCalibratorReason.Reason
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objApproverMaster._Voiduserunkid = CInt(Session("UserId"))
            End If

            If objApproverMaster.Delete(CInt(mintMappingUnkid)) = False Then
                DisplayMessage.DisplayMessage(objApproverMaster._Message, Me)
                Exit Sub
            End If
            'Gajanan [15-April-2020] -- Start
            'Enhancement:Worked On Calibration Approver Change
            mintMappingUnkid = -1
            'Gajanan [15-April-2020] -- End

            FillList(False)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region " AddEdit Methods "

#Region " Private Function "

    Dim dsList As DataSet
    Private Sub FillAddEditCombo()
        Try
            Dim objUser As New clsUserAddEdit
            dsList = objUser.getNewComboList("User", , True, CInt(Session("CompanyUnkId")), CInt(enUserPriviledge.AllowtoCalibrateProvisionalScore).ToString, CInt(Session("Fin_year")), True)
            drpCalibratorUseraccess_user.DataSource = dsList.Tables("User")
            drpCalibratorUseraccess_user.DataTextField = "name"
            drpCalibratorUseraccess_user.DataValueField = "userunkid"
            drpCalibratorUseraccess_user.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try

            If drpCalibratorUseraccess_user.SelectedValue = 0 Then
                DisplayMessage.DisplayMessage("Approver User cannot be blank. Approver User is required information.", Me)
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Private Sub SetValue()
        Try
            If mintMappingUnkid > 0 Then
                objApproverMaster._Mappingunkid = mintMappingUnkid
            End If
            objApproverMaster._Levelunkid = -1
            objApproverMaster._Mapuserunkid = Convert.ToInt32(drpCalibratorUseraccess_user.SelectedValue)
            objApproverMaster._Isactive = True

            objApproverMaster._VisibilityType = CInt(clscalibrate_approver_tran.enCalibrationApproverVisibilityType.VISIBLE)
            objApproverMaster._IsCalibrator = True

            Call SetAtValue(mstrModuleName1)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetAtValue(ByVal strFormName As String)
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objApproverMaster._AuditUserId = CInt(Session("UserId"))
                objApproverTran._AuditUserid = CInt(Session("UserId"))
            End If
            objApproverMaster._AuditDatetime = ConfigParameter._Object._CurrentDateAndTime
            objApproverMaster._ClientIP = CStr(Session("IP_ADD"))
            objApproverMaster._HostName = CStr(Session("HOST_NAME"))
            objApproverMaster._FormName = strFormName
            objApproverMaster._IsFromWeb = True
            'Gajanan [15-April-2020] -- Start
            'Enhancement:Worked On Calibration Approver Change

            objApproverTran._AuditDate = ConfigParameter._Object._CurrentDateAndTime
            objApproverTran._ClientIp = CStr(Session("IP_ADD"))
            objApproverTran._HostName = CStr(Session("HOST_NAME"))
            objApproverTran._FormName = strFormName
            objApproverTran._IsFromWeb = True
            'Gajanan [15-April-2020] -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub ResetAddedit()
        Try
            drpCalibratorUseraccess_user.SelectedIndex = 0
            popupCalibratorUseraccess.Hide()
            blnpopupApproverUseraccess = False
            FillList(False)
            mintMappingUnkid = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub FillUserAccess(ByVal dt As DataTable)
        Try
            'TvApproverUseraccess.DataSource = dt
            'TvApproverUseraccess.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Gajanan [15-April-2020] -- Start
    'Enhancement:Worked On Calibration Approver Change


    Private Sub FillEmployee()
        Try
            Dim objEmployee As New clsEmployee_Master
            Dim dsEmployee As DataSet = Nothing
            Dim strSearch As String = String.Empty


            Dim strIds As String = ""
            strIds = objApproverTran.GetAssignedEmpIds(True)

            Dim blnInActiveEmp As Boolean = False
            If (Session("ApproverId") Is Nothing OrElse CInt(Session("ApproverId")) <= 0) Then
                blnInActiveEmp = False
            Else
                blnInActiveEmp = True
            End If
            Dim intSelectedUserEmpId As Integer = 0
            If CInt(drpCalibratorUseraccess_user.SelectedValue) > 0 Then
                Dim objUsr As New clsUserAddEdit
                objUsr._Userunkid = CInt(drpCalibratorUseraccess_user.SelectedValue)
                If objUsr._EmployeeUnkid > 0 AndAlso objUsr._EmployeeCompanyUnkid = CInt(Session("CompanyUnkId")) Then
                    intSelectedUserEmpId = objUsr._EmployeeUnkid
                End If
                objUsr = Nothing
            End If

            If intSelectedUserEmpId > 0 Then
                If strIds.Trim.Length > 0 Then
                    strIds &= "," & intSelectedUserEmpId.ToString()
                Else
                    strIds = intSelectedUserEmpId.ToString()
                End If
            End If

            'S.SANDEEP |04-FEB-2022| -- START
            'ISSUE : CODE OPTIMIZATION (CALIBRATION)
            'If strIds.Trim.Length > 0 Then
            '    strSearch &= "AND hremployee_master.employeeunkid NOT IN (" & strIds & ") "
            'End If
            'S.SANDEEP |04-FEB-2022| -- END
            

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strSearch &= "AND " & mstrAdvanceFilter
            End If

            If strSearch.Length > 0 Then
                strSearch = strSearch.Substring(3)
            End If

            dsEmployee = objEmployee.GetEmployeeList(Session("Database_Name").ToString(), CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                            blnInActiveEmp, _
                                            "Employee", _
                                            False, , , , , , , , , , , , , , , strSearch, True)

            If dsEmployee IsNot Nothing Then
                dsEmployee.Tables(0).Columns.Add("Select", Type.GetType("System.Boolean"))
                dsEmployee.Tables(0).Columns("Select").DefaultValue = False
            End If

            
            Dim dtEmployee As DataTable = Nothing
            'S.SANDEEP |04-FEB-2022| -- START
            'ISSUE : CODE OPTIMIZATION (CALIBRATION)
            'dtEmployee = dsEmployee.Tables("Employee")
            If strIds.Trim.Length > 0 Then
                dtEmployee = New DataView(dsEmployee.Tables("Employee"), "employeeunkid NOT IN (" & strIds & ")", "", DataViewRowState.CurrentRows).ToTable().Copy
            Else
            dtEmployee = dsEmployee.Tables("Employee")
            End If
            'S.SANDEEP |04-FEB-2022| -- END

            dgvEmp.DataSource = dtEmployee
            dgvEmp.DataBind()

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                dgvEmp.PageIndex = 0
                dgvEmp.DataBind()
            Else
                'Throw ex
                DisplayMessage.DisplayError(ex, Me)
            End If
        End Try
    End Sub


    'Gajanan [15-April-2020] -- Start
    'Enhancement:Worked On Calibration Approver Change
    Private Sub setPopupvisibility()
        Try
        If mintMappingUnkid > 0 Then
            drpCalibratorUseraccess_user.Enabled = False
        Else
            drpCalibratorUseraccess_user.Enabled = True
        End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [15-April-2020] -- End

    Private Sub FillApproverEmployeeList()
        Try
            If mintMappingUnkid > 0 Then
            Dim dsemployee As DataTable = objApproverTran.GetApproverTran(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, mintMappingUnkid, Nothing)
            dgvApproverEmp.DataSource = dsemployee
            dgvApproverEmp.DataBind()
        End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Gajanan [15-April-2020] -- End
#End Region

#Region " Dropdown Event "

    Protected Sub drpCalibratorUseraccess_user_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpCalibratorUseraccess_user.SelectedIndexChanged
        'Dim objUser As New clsUserAddEdit
        Try
            'Dim dtTable As DataTable = Nothing
            'If CInt(drpCalibratorUseraccess_user.SelectedValue) > 0 Then
            '    dtTable = objUser.GetUserAccessList(Convert.ToString(Session("UserAccessModeSetting")), CInt(drpCalibratorUseraccess_user.SelectedValue), Convert.ToInt32(Session("CompanyUnkId")))
            '    dtTable = New DataView(dtTable, "isAssign = 1 OR isGrp = 1", "", DataViewRowState.CurrentRows).ToTable()
            'End If
            'FillUserAccess(dtTable)
            'dtTable = Nothing
            If CInt(drpCalibratorUseraccess_user.SelectedValue) > 0 Then
                FillEmployee()
                setPopupvisibility()
            Else
                dgvEmp.DataSource = Nothing : dgvEmp.DataBind()
                dgvApproverEmp.DataSource = Nothing : dgvApproverEmp.DataBind()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'objUser = Nothing
        End Try
    End Sub

#End Region

#Region " Button Event "

    Protected Sub btnApproverUseraccessClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproverUseraccessClose.Click
        Try
            blnpopupApproverUseraccess = False
            popupCalibratorUseraccess.Hide()
            ResetAddedit()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Protected Sub btnApproverUseraccessSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApproverUseraccessSave.Click
    '    Dim blnFlag As Boolean = False
    '    Try
    '        If Validation() = False Then Exit Sub

    '        SetValue()

    '        blnFlag = objApproverMaster.Insert()

    '        If blnFlag = False And objApproverMaster._Message <> "" Then
    '            DisplayMessage.DisplayMessage(objApproverMaster._Message, Me)
    '        End If

    '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 6, "Approver Saved Successfully"), Me)
    '        ResetAddedit()

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try

    'End Sub

    Protected Sub confirmapproverdelete_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles confirmapproverdelete.buttonYes_Click
        Try
            DeleteCalibratorReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            drpCalibrator.SelectedIndex = 0
            drpStatus.SelectedIndex = 0
            Call FillList(True)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub
    'Gajanan [15-April-2020] -- Start
    'Enhancement:Worked On Calibration Approver Change
    Protected Sub btnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Dim blnFlag As Boolean = False
        Try
            If Validation() = False Then Exit Sub

            'If mdtTran.Rows.Count = 0 Or count = 0 Or GvSelectedEmployee.Rows.Count <= 0 Then
            '    'Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Employee is compulsory information.Please Check atleast One Employee."), Me)
            '    Exit Sub
            'End If

            'Dim dtemployee As DataTable = objApproverTran._DataList
            'For Each row As GridViewRow In dgvEmp.Rows
            '    Dim chk As CheckBox = CType(row.FindControl("ChkSelectedEmp"), CheckBox)
            '    Dim hfemployee As HiddenField = CType(row.FindControl("hfemployeeunkid"), HiddenField)

            '    If chk.Checked Then
            '        Dim drow As DataRow = dtemployee.NewRow
            '        drow("employeeunkid") = hfemployee.Value
            '        dtemployee.Rows.Add(drow)
            '        chk.Checked = False
            '    End If
            'Next

            Dim dtemployee As DataTable = objApproverTran._DataList

            Dim iRows As IEnumerable(Of GridViewRow) = dgvEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) x.FindControl("ChkSelectedEmp") IsNot Nothing AndAlso CType(x.FindControl("ChkSelectedEmp"), CheckBox).Checked = True)
            If iRows Is Nothing Then Exit Sub

            For Each row As GridViewRow In iRows
                Dim chk As CheckBox = CType(row.FindControl("ChkSelectedEmp"), CheckBox)
                Dim hfemployee As HiddenField = CType(row.FindControl("hfemployeeunkid"), HiddenField)
                If dtemployee.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(hfemployee.Value)).Count <= 0 Then
                    Dim drow As DataRow = dtemployee.NewRow
                    drow("employeeunkid") = hfemployee.Value
                    dtemployee.Rows.Add(drow)
                End If
                    chk.Checked = False
            Next

            SetValue()



            If mintMappingUnkid > 0 Then
                objApproverTran._DataList = dtemployee
                objApproverTran._Mappingunkid = mintMappingUnkid
                objApproverTran.Insert_CalibarteApproverData(Nothing)
            Else

                If objApproverMaster.isExist(objApproverMaster._Mapuserunkid, True) Then
                    If mblnIsEditMode Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 2, "This calibrator is already map.please select new user to map."), Me)
                    Else
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 1, "This calibrator is alredy exist to update data please edit from list."), Me)
                    End If
                    Exit Sub
                End If

                If objApproverMaster.Insert(dtemployee, True) = False Then

                    If objApproverMaster._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage(objApproverMaster._Message, Me)
                        Exit Sub
                    End If
                End If
                mintMappingUnkid = objApproverMaster._Mappingunkid
            End If

            If mblnIsEditMode Then
                popupCalibratorUseraccess.Hide()
            Else
                mintMappingUnkid = -1
                drpCalibratorUseraccess_user.SelectedValue = 0
                Call drpCalibratorUseraccess_user_SelectedIndexChanged(New Object(), New EventArgs)
            End If

            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 6, "Employee assigned successfully to selected calibrator."), Me)

            'FillEmployee()
            'FillApproverEmployeeList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnAddeditDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddeditDelete.Click
        Dim blnFlag As Boolean = False
        Try

            'If mdtTran.Rows.Count = 0 Or count = 0 Or GvSelectedEmployee.Rows.Count <= 0 Then
            '    'Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Employee is compulsory information.Please Check atleast One Employee."), Me)
            '    Exit Sub
            'End If
            Dim mdicEmployee As New Dictionary(Of Integer, Integer)


            Dim iRows As IEnumerable(Of GridViewRow) = dgvApproverEmp.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) x.FindControl("ChkISelectedEmp") IsNot Nothing AndAlso CType(x.FindControl("ChkISelectedEmp"), CheckBox).Checked = True)

            'For Each row As GridViewRow In dgvApproverEmp.Rows
            '    Dim chk As CheckBox = CType(row.FindControl("ChkSelectedEmp"), CheckBox)

            '    Dim empid As Integer = dgvApproverEmp.DataKeys(row.RowIndex)("employeeunkid").ToString()
            '    Dim mappingtranunkid As Integer = dgvApproverEmp.DataKeys(row.RowIndex)("mappingtranunkid").ToString()


            '    If chk.Checked Then
            '        mdicEmployee.Add(mappingtranunkid, empid)
            '    End If
            'Next

            If iRows IsNot Nothing AndAlso iRows.Count > 0 Then
                For Each row As GridViewRow In iRows
                Dim empid As Integer = dgvApproverEmp.DataKeys(row.RowIndex)("employeeunkid").ToString()
                Dim mappingtranunkid As Integer = dgvApproverEmp.DataKeys(row.RowIndex)("mappingtranunkid").ToString()
                    mdicEmployee.Add(mappingtranunkid, empid)
            Next

            SetValue()

            objApproverTran._Mappingunkid = mintMappingUnkid
            objApproverTran.Delete_CalibarteApproverData(mdicEmployee, Nothing)
            FillApproverEmployeeList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub AdvanceFilter1_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles AdvanceFilter1.buttonApply_Click
        Try
            mstrAdvanceFilter = AdvanceFilter1._GetFilterString
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Gajanan [15-April-2020] -- End
#End Region

#Region " Link Event(s) "

    Protected Sub lnkAdvanceFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAdvanceFilter.Click
        Try
            AdvanceFilter1._Hr_EmployeeTable_Alias = "hremployee_master"
            AdvanceFilter1.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Call FillEmployee()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            mstrAdvanceFilter = ""
            Call FillEmployee()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Gridview Event "
    Protected Sub gvCalibratorList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCalibratorList.RowDataBound
        Dim oldid As String
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dt As DataTable = (TryCast(e.Row.DataItem, DataRowView)).DataView.Table
                If CInt(gvCalibratorList.DataKeys(e.Row.RowIndex)("mappingunkid").ToString) > 0 Then
                    oldid = dt.Rows(e.Row.RowIndex)("Level").ToString()
                    If dt.Rows(e.Row.RowIndex)("IsGrp").ToString() = True AndAlso oldid <> currentId Then
                        Me.AddGroup(e.Row, dt.Rows(e.Row.RowIndex)("Level").ToString(), gvCalibratorList)
                        currentId = oldid
                    Else
                        Dim lnkactive As LinkButton = TryCast(e.Row.FindControl("lnkactive"), LinkButton)
                        Dim lnkDeactive As LinkButton = TryCast(e.Row.FindControl("lnkDeactive"), LinkButton)
                        Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                        Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)

                        If CBool(Session("AllowToEditCalibrator")) Then
                            lnkedit.Visible = True
                        Else
                            lnkedit.Visible = False
                        End If

                        If CBool(Session("AllowToDeleteCalibrator")) Then
                            lnkdelete.Visible = True
                        Else
                            lnkdelete.Visible = False
                        End If

                        If CBool(Session("AllowToMakeCalibratorActive")) Then
                            lnkactive.Visible = True
                        Else
                            lnkactive.Visible = False
                        End If

                        If CBool(Session("AllowToMakeCalibratorInactive")) Then
                            lnkDeactive.Visible = True
                        Else
                            lnkDeactive.Visible = False
                        End If

                        If dt.Rows(e.Row.RowIndex)("isactive").ToString() <> "" Then
                            If dt.Rows(e.Row.RowIndex)("isactive").ToString() = True Then
                                lnkactive.Visible = False
                                lnkDeactive.Visible = True
                            Else
                                lnkactive.Visible = True
                                lnkDeactive.Visible = False
                            End If
                        End If

                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            currentId = ""
            oldid = ""
        End Try
    End Sub
#End Region

#End Region

#Region " Language "

    Private Sub SetControlCaptions()
        Try
            'Language.setLanguage(mstrModuleName)
            If Me.Title.Trim.Length > 0 Then
                Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,mstrModuleName, Me.Title)
            End If

            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,mstrModuleName, Me.lblCaption.Text)

            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblCaption.ID, Me.lblCaption.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblcalibrator.ID, Me.lblcalibrator.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblStatus.ID, Me.lblStatus.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnNew.ID, Me.btnNew.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnSearch.ID, Me.btnSearch.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnReset.ID, Me.btnReset.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibratorList.Columns(2).FooterText, gvCalibratorList.Columns(2).HeaderText)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibratorList.Columns(3).FooterText, gvCalibratorList.Columns(3).HeaderText)

            'Language.setLanguage(mstrModuleName1)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,mstrModuleName1, Me.lblHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblHeader.ID, Me.lblHeader.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblCalibratorInfo.ID, Me.lblCalibratorInfo.Text)
            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblCalibratorUseraccess_user.ID, Me.lblCalibratorUseraccess_user.Text)

            Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnApproverUseraccessClose.ID, Me.btnApproverUseraccessClose.Text)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)

            'Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblCaption.ID, Me.lblCaption.Text)
            Me.lblcalibrator.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblcalibrator.ID, Me.lblcalibrator.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblStatus.ID, Me.lblStatus.Text)
            'Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblCaption.ID, Me.lblCaption.Text)
            'Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblCaption.ID, Me.lblCaption.Text)
            'Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblCaption.ID, Me.lblCaption.Text)

            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")
            Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSearch.ID, Me.btnSearch.Text).Replace("&", "")
            Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnReset.ID, Me.btnReset.Text).Replace("&", "")

            gvCalibratorList.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibratorList.Columns(2).FooterText, gvCalibratorList.Columns(2).HeaderText)
            gvCalibratorList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibratorList.Columns(3).FooterText, gvCalibratorList.Columns(3).HeaderText)

            'Language.setLanguage(mstrModuleName1)
            Me.lblHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblHeader.ID, Me.lblHeader.Text).Replace("&", "")
            Me.lblCalibratorInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblCalibratorInfo.ID, Me.lblCalibratorInfo.Text).Replace("&", "")
            Me.lblCalibratorUseraccess_user.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblCalibratorUseraccess_user.ID, Me.lblCalibratorUseraccess_user.Text).Replace("&", "")

            Me.btnApproverUseraccessClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnApproverUseraccessClose.ID, Me.btnApproverUseraccessClose.Text).Replace("&", "")

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region




    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Sorry, You cannot delete this Approver. Reason: This Approver is in use.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 5, "Are You Sure You Want To Delete This Approver?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 6, "Employee assigned successfully to selected calibrator.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
