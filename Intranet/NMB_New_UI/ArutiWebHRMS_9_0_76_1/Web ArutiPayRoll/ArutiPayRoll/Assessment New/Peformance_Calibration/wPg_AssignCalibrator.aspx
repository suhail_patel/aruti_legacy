﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_AssignCalibrator.aspx.vb"
    Inherits="Assessment_New_Peformance_Calibration_wPg_AssignCalibrator" Title="Assign Calibrator" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
        </script>--%>

    <script type="text/javascript">
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching() {
            if ($('#txtEmployeeSearch').val().length > 0) {
                $('#<%=dgvEmp.ClientID %> tbody tr').hide();
                $('#<%=dgvEmp.ClientID %> tbody tr:first').show();
                $('#<%=dgvEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtEmployeeSearch').val() + '\')').parent().show();
            }

            else if ($('#txtSelectedEmployee').val().length > 0) {
                $('#<%=dgvApproverEmp.ClientID %> tbody tr').hide();
                $('#<%=dgvApproverEmp.ClientID %> tbody tr:first').show();
                $('#<%=dgvApproverEmp.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSelectedEmployee').val() + '\')').parent().show();
            }


            else if ($('#dgvEmp').val().length == 0) {
                resetFromSearchValue();
            }
            else if ($('#dgvApproverEmp').val().length == 0) {
                resetFromSearchValue();
            }

            if ($('#<%=dgvEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if ($('#<%=dgvApproverEmp.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }

        function resetFromSearchValue() {
            $('#dgvEmp').val('');
            $('#<%= dgvEmp.ClientID %> tr').show();
            $('.norecords').remove();
            $('#dgvEmp').focus();
        }


        $("body").on("click", "[id*=ChkAllSelectedEmp]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkSelectedEmp]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=ChkSelectedEmp]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAllSelectedEmp]", grid);
            debugger;
            if ($("[id*=ChkSelectedEmp]", grid).length == $("[id*=ChkSelectedEmp]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

        $("body").on("click", "[id*=ChkAllISelectedEmp]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkISelectedEmp]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=ChkISelectedEmp]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAllISelectedEmp]", grid);
            debugger;
            if ($("[id*=ChkISelectedEmp]", grid).length == $("[id*=ChkISelectedEmp]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

        
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Assign Calibrator"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblcalibrator" runat="server" Text="Calibrator" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpCalibrator" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label" Visible="false" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpStatus" runat="server" Visible="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:GridView ID="gvCalibratorList" DataKeyNames="mappingunkid" runat="server" AutoGenerateColumns="false"
                                                Width="99%" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" CommandArgument='<%#Eval("mappingunkid") %>'
                                                                ToolTip="Delete"><i class="fas fa-trash text-danger"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkEdit" runat="server" OnClick="lnkedit_Click" CommandArgument='<%#Eval("mappingunkid") %>'
                                                                ToolTip="Edit"><i class="fas fa-pencil-alt text-primary" ></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="dgcolhActiveInactive" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkactive" runat="server" ToolTip="Make Calibrator Active" OnClick="lnkActive_Click"
                                                                CommandArgument='<%#Eval("mappingunkid") %>'>
                                                    <i class="fa fa-user-plus" style="font-size:18px;color:Green"></i>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="lnkDeactive" runat="server" ToolTip="Make Calibrator Inactive"
                                                                OnClick="lnkDeActive_Click" CommandArgument='<%#Eval("mappingunkid") %>'>
                                                    <i class="fa fa-user-times" style="font-size:18px;color:red" ></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField HeaderText="Calibrator Name" DataField="approver" FooterText="dgcolhCalibratorName" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupCalibratorUseraccess" DropShadow="false" BackgroundCssClass="modal-backdrop"
                    TargetControlID="lblCalibratorInfo" runat="server" PopupControlID="PanelCalibratorUseraccess"
                    CancelControlID="lblHeader" />
                <asp:Panel ID="PanelCalibratorUseraccess" runat="server" CssClass="card modal-dialog modal-lg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCalibratorInfo" runat="server" Text="Add / Edit Calibrator"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="card inner-card">
                                    <div class="header">
                                        <h2>
                                            <asp:Label ID="lblHeader" runat="server" Text="Calibrators Info" />
                                        </h2>
                                        <ul class="header-dropdown m-r-50 p-l-0">
                                            <asp:LinkButton ID="lnkAdvanceFilter" Font-Underline="false" runat="server" ToolTip="Advance Filter">
                                                                          <i class="fas fa-sliders-h"></i>
                                            </asp:LinkButton>
                                        </ul>
                                        <ul class="header-dropdown m-r-25  p-l-0">
                                            <asp:LinkButton ID="lnkSearch" runat="server" ToolTip="Search" OnClick="lnkSearch_Click"><i class="fa fa-search"></i></asp:LinkButton>
                                        </ul>
                                        <ul class="header-dropdown m-r--14  p-l-0">
                                            <asp:LinkButton ID="lnkReset" runat="server" ToolTip="Reset" OnClick="lnkReset_Click">
                                                                               <i class="fas fa-sync-alt"></i>
                                            </asp:LinkButton>
                                        </ul>
                                    </div>
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblCalibratorUseraccess_user" runat="server" Text="User" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="drpCalibratorUseraccess_user" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" id="txtEmployeeSearch" name="txtSearch" placeholder="type search text"
                                                            maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="height: 215px">
                                                    <asp:GridView ID="dgvEmp" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                        Width="99%" AllowPaging="false">
                                                        <Columns>
                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="ChkAllSelectedEmp" runat="server" CssClass="filled-in" Text=" " />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="ChkSelectedEmp" runat="server" CssClass="filled-in" Text=" " />
                                                                    <asp:HiddenField ID="hfemployeeunkid" runat="server" Value='<%#eval("employeeunkid") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="ColhEmployeecode" />
                                                            <asp:BoundField DataField="employeename" HeaderText="Employee" ReadOnly="true" FooterText="colhEmp" />
                                                            <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                Visible="false" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Button ID="btnAssign" runat="server" Text="Assign" CssClass="btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="card inner-card">
                                    <div class="header">
                                        <h2>
                                            <asp:Label ID="LblGreAddEditLeaveApprover" runat="server" Text="Selected Employee" />
                                        </h2>
                                    </div>
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <input type="text" id="txtSelectedEmployee" name="txtSearch" placeholder="type search text"
                                                            maxlength="50" class="form-control" style="height: 25px; font: 100" onkeyup="FromSearching();" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="height: 294px">
                                                    <asp:GridView ID="dgvApproverEmp" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                        Width="99%" AllowPaging="false" DataKeyNames="employeeunkid,mappingtranunkid">
                                                        <Columns>
                                                            <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox ID="ChkAllISelectedEmp" runat="server" CssClass="filled-in" Text=" " />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="ChkISelectedEmp" runat="server" CssClass="filled-in" Text=" " />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField HeaderText="Employee" DataField="name" FooterText="dgcolhEmpName" />
                                                            <asp:BoundField HeaderText="Department" DataField="departmentname" FooterText="dgcolhEDept" />
                                                            <asp:BoundField HeaderText="Job" DataField="jobname" FooterText="dgcolhEJob" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Button ID="btnAddeditDelete" runat="server" Text="Delete" CssClass="btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnApproverUseraccessClose" runat="server" Text="Close" CssClass="btn btn-primary" />
                    </div>
                </asp:Panel>
                <%--OLD--%>
                <%--<div class="panel-primary">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body">
                        <div id="FilterCriteria" class="panel-default">
                            <div id="FilterCriteriaTitle" class="panel-heading-default">
                                <div style="float: left;">
                                    <asp:Label ID="lblCaption" runat="server" Text="Calibrator(s) List"></asp:Label>
                                </div>
                            </div>
                            <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                <table style="width: 100%;">
                                    <tr style="width: 100%">
                                        <td style="width: 8%">
                                        </td>
                                        <td style="width: 25%">
                                        </td>
                                        <td style="width: 8%">
                                        </td>
                                        <td style="width: 25%">
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                </div>
                            </div>
                        </div>
                        <div id="Div1" class="panel-default">
                            <div id="Div2" class="panel-body-default" style="position: relative">
                                <div class="row2">
                                    <asp:Panel ID="pnlGrid" runat="server" ScrollBars="Auto" Height="250px">
                                    </asp:Panel>
                                </div>
                                <div id="btnfixedbottom" class="btn-default">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <uc9:ConfirmYesNo ID="confirmapproverdelete" runat="server" />
                <ucDel:DeleteReason ID="DeleteCalibratorReason" runat="server" Title="Are You Sure You Want Delete Calibrator ?" />
                <uc9:ConfirmYesNo ID="popupconfirmDeactiveCalibrator" runat="server" Title="Confirmation"
                    Message="Are You Sure You Want To Deactive  This Calibrator?" />
                <uc9:ConfirmYesNo ID="popupconfirmActiveCalibrator" runat="server" Title="Confirmation"
                    Message="Are You Sure You Want To Active This Calibrator?" />
                <uc7:AdvanceFilter ID="AdvanceFilter1" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
