﻿#Region " Imports "

Imports System.Data
Imports Aruti.Data
Imports eZeeCommonLib
Imports System.Drawing
Imports System.IO
Imports System.IO.Packaging
Imports System.Data.SqlClient
Imports System.Web.Services
Imports System.Net.Dns
Imports System.Web.UI.DataVisualization.Charting

#End Region

Partial Class Assessment_New_Peformance_Calibration_wPg_CalibrateList
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmScoreCalibrationList"
    Private ReadOnly mstrModuleName1 As String = "frmScoreCalibrationAddEdit"
    Private blnShowApplyCalibrationpopup As Boolean = False
    Private objScoreCalibrateApproval As New clsScoreCalibrationApproval
    Private mintCalibrationUnkid As Integer = 0
    Private mintEmployeeUnkid As Integer = 0
    Private mintPeriodUnkid As Integer = 0
    Private mblnIsGrp As Boolean = False
    Private mintgrpId As Integer = 0
    Private mblnIsExpand As Boolean = False
    Private mblnShowMyReport As Boolean = False
    Private mblnShowHPOCurve As Boolean = False
    Private objCONN As SqlConnection
    Private mlbnIsSubmitted As Boolean = False
    Private mstrAdvanceFilter As String = ""
#End Region

#Region " Form's Event "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                KillIdleSQLSessions()
                objCONN = Nothing
                If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                    Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                    Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                    constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                    objCONN = New SqlConnection
                    objCONN.ConnectionString = constr
                    objCONN.Open()
                    HttpContext.Current.Session("gConn") = objCONN
                End If
                Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split("|")
                If arr.Length = 3 Then
                    Try
                        If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        Else
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                            HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                        End If

                    Catch ex As Exception
                        HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                        HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                    End Try

                    Blank_ModuleName()
                    clsCommonATLog._WebFormName = "frmScoreCalibrationAddEdit"
                    StrModuleName2 = "mnuAssessment"
                    StrModuleName3 = "mnuSetups"
                    clsCommonATLog._WebClientIP = Session("IP_ADD")
                    clsCommonATLog._WebHostName = Session("HOST_NAME")

                    HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                    HttpContext.Current.Session("UserId") = CInt(arr(1))
                    mintPeriodUnkid = CInt(arr(2))

                    Dim strError As String = ""
                    If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    HttpContext.Current.Session("mdbname") = Session("Database_Name")
                    gobjConfigOptions = New clsConfigOptions
                    gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                    ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                    CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString)

                    ArtLic._Object = New ArutiLic(False)
                    If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                        Dim objGroupMaster As New clsGroup_Master
                        objGroupMaster._Groupunkid = 1
                        ArtLic._Object.HotelName = objGroupMaster._Groupname
                    End If

                    If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Employee_Performance_Appraisal_Management)) = False Then
                        DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                        Exit Sub
                    End If

                    If ConfigParameter._Object._IsArutiDemo Then
                        If ConfigParameter._Object._IsExpire Then
                            DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                            Exit Try
                        Else
                            If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            End If
                        End If
                    End If

                    Dim clsConfig As New clsConfigOptions
                    clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                    If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        Me.ViewState.Add("ArutiSelfServiceURL", Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath)
                    Else
                        Me.ViewState.Add("ArutiSelfServiceURL", clsConfig._ArutiSelfServiceURL)
                    End If

                    Session("DateFormat") = clsConfig._CompanyDateFormat
                    Session("DateSeparator") = clsConfig._CompanyDateSeparator
                    SetDateFormat()
                    Dim objUser As New clsUserAddEdit
                    objUser._Userunkid = CInt(Session("UserId"))
                    Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                    Call GetDatabaseVersion()
                    Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname"))
                    HttpContext.Current.Session("clsuser") = clsuser
                    HttpContext.Current.Session("UserName") = clsuser.UserName
                    HttpContext.Current.Session("Firstname") = clsuser.Firstname
                    HttpContext.Current.Session("Surname") = clsuser.Surname
                    HttpContext.Current.Session("MemberName") = clsuser.MemberName
                    HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                    HttpContext.Current.Session("UserId") = clsuser.UserID
                    HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                    HttpContext.Current.Session("Password") = clsuser.password
                    HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                    HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid

                    strError = ""
                    If SetUserSessions(strError) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    strError = ""
                    If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                        DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath") & "Index.aspx")
                        Exit Sub
                    End If

                    Call SetControlCaptions()
                    Call SetMessages()
'Call Language._Object.SaveValue()
                    Call SetLanguage()

                    Call FillCombo()
                    If mintPeriodUnkid > 0 Then
                        cboSPeriod.SelectedValue = mintPeriodUnkid
                    End If

                    blnShowApplyCalibrationpopup = True
                    cboSPeriod.Enabled = False : btnSReset.Enabled = False : btnISearch.Enabled = False : lnkAllocation.Visible = False
                    Call btnISearch_Click(New Object(), New EventArgs)
                    HttpContext.Current.Session("Login") = True
                    gvCalibrateList.Columns(0).Visible = False
                    gvCalibrateList.Columns(1).Visible = False
                    btnNew.Visible = False
                    GoTo lnk
                End If
            End If
            If IsPostBack = False Then
                Call SetControlCaptions()
                Call SetMessages()
'Call Language._Object.SaveValue()
                Call SetLanguage()
                Call FillCombo()
            Else
                blnShowApplyCalibrationpopup = Me.ViewState("blnShowApplyCalibrationpopup")
                mlbnIsSubmitted = Me.ViewState("mlbnIsSubmitted")

                mintCalibrationUnkid = Me.ViewState("mintCalibrationUnkid")
                mintEmployeeUnkid = Me.ViewState("mintEmployeeUnkid")
                mintPeriodUnkid = Me.ViewState("mintPeriodUnkid")
                mblnIsGrp = Me.ViewState("mblnIsGrp")
                mblnShowMyReport = Me.ViewState("mblnShowMyReport")
                mblnShowHPOCurve = Me.ViewState("mblnShowHPOCurve")
            End If
lnk:        If blnShowApplyCalibrationpopup Then
                popupApproverUseraccess.Show()
            End If
            If mblnShowMyReport Then
                popupMyReport.Show()
            End If
            If mblnShowHPOCurve Then
                popupHPOChart.Show()
            End If
            If CInt(Session("ScoreCalibrationFormNotype")) = 1 Then
                txtICalibrationNo.Enabled = False
            Else
                txtICalibrationNo.Enabled = True
            End If

            SetTitle()
            'lnkCopyData.Attributes.Add("onclick", "return false")
            'btnIApply.Attributes.Add("onclick", "return false")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        If Request.QueryString.Count <= 0 Then
            Me.IsLoginRequired = True
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("blnShowApplyCalibrationpopup") = blnShowApplyCalibrationpopup
            Me.ViewState("mlbnIsSubmitted") = mlbnIsSubmitted
            Me.ViewState("mintCalibrationUnkid") = mintCalibrationUnkid
            Me.ViewState("mintEmployeeUnkid") = mintEmployeeUnkid
            Me.ViewState("mintPeriodUnkid") = mintPeriodUnkid
            Me.ViewState("mblnIsGrp") = mblnIsGrp
            Me.ViewState("mblnShowMyReport") = mblnShowMyReport
            Me.ViewState("mblnShowHPOCurve") = mblnShowHPOCurve
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Common Part "

    Private Sub FillCombo()
        Dim objMData As New clsMasterData
        Dim dsList As New DataSet
        Dim objPeriod As New clscommom_period_Tran
        Dim objEmp As New clscalibrate_approver_tran
        Dim objRating As New clsAppraisal_Rating
        Try
            dsList = objEmp.GetMappedEmployeecombolist(Session("Database_Name"), _
                                            Session("UserId"), _
                                            True, _
                                            Session("EmployeeAsOnDate"), "List", True)
            With cboEmployee
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
                .DataBind()
            End With

            With cboRptEmployee
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List").Copy
                .SelectedValue = 0
                .DataBind()
            End With

            dsList = objPeriod.getListForCombo(enModuleReference.Assessment, 0, Session("Database_Name"), Session("fin_startdate"), "List", True, 1)
            Dim mintCurrentPeriodId As Integer = (New clsMasterData).getCurrentPeriodID(enModuleReference.Assessment, ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")), 1, , False)
            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With
            With cboSPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With
            With cboRptPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With
            Call cboRptPeriod_SelectedIndexChanged(New Object, New EventArgs)

            With cboChPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0).Copy()
                .DataBind()
                .SelectedValue = mintCurrentPeriodId
            End With

            dsList = objScoreCalibrateApproval.GetStatusList("List", True)
            With cboStatus
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            dsList = objRating.getComboList("List", True)
            With cbocRating
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With

            Dim objCScoreMaster As New clsComputeScore_master
            dsList = objCScoreMaster.GetDisplayScoreOption("List", False, Nothing)
            With cbochDisplay
                .DataValueField = "Id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE
                .DataBind()
            End With
            objCScoreMaster = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_FRating()
        Try
            Dim objRating As New clsAppraisal_Rating
            Dim dsList As New DataSet
            dsList = objRating.getComboList("List", False)
            gvFilterRating.AutoGenerateColumns = False
            gvFilterRating.DataSource = dsList
            gvFilterRating.DataBind()
            objRating = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<WebMethod()> _
    'Public Shared Function GetCalibrationNo(ByVal intPrdId As Integer, ByVal intUsrId As Integer) As List(Of ListItem)
    '    Try
    '        Dim ds As New DataSet
    '        Dim cnum As New List(Of ListItem)()
    '        Dim objNum As New clsScoreCalibrationApproval
    '        ds = objNum.GetComboListCalibrationNo("List", intPrdId, intUsrId, True)
    '        For Each r As DataRow In ds.Tables(0).Rows
    '            cnum.Add(New ListItem() With { _
    '                  .Value = r("Id").ToString(), _
    '                  .Text = r("Name").ToString() _
    '                })
    '        Next
    '        objNum = Nothing
    '        Return cnum
    '    Catch ex As Exception
    '        Dim objErrorLog As New clsErrorlog_Tran
    '        With objErrorLog
    '            'S.SANDEEP |04-MAR-2020| -- START
    '            '._Error_Message = clsCrypto.Encrypt(ex.Message)
    '            'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
    '            Dim S_dispmsg As String = ex.Message & "; " & ex.StackTrace.ToString
    '            If ex.InnerException IsNot Nothing Then
    '                S_dispmsg &= "; " & ex.InnerException.Message
    '            End If
    '            'Sohail (01 Feb 2020) -- End
    '            S_dispmsg = S_dispmsg.Replace("'", "")
    '            S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
    '            ._Error_Message = S_dispmsg
    '            'S.SANDEEP |04-MAR-2020| -- END
    '            ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
    '            ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
    '            ._Companyunkid = HttpContext.Current.Session("Companyunkid")
    '            If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
    '                ._Database_Version = ""
    '            Else
    '                ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
    '            End If
    '            If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
    '                ._Userunkid = HttpContext.Current.Session("UserId")
    '                ._Loginemployeeunkid = 0
    '            Else
    '                ._Userunkid = 0
    '                ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
    '            End If
    '            ._Isemailsent = False
    '            ._Isweb = True
    '            ._Isvoid = False
    '            ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
    '            ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
    '        End With
    '        Dim strDBName As String = ""
    '        If HttpContext.Current.Session("mdbname") IsNot Nothing Then
    '            If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
    '                strDBName = "hrmsConfiguration"
    '            End If
    '        End If
    '        If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

    '        End If
    '        Throw New Exception("Something went wrong, Please contact administrator!")
    '    End Try
    'End Function
#End Region

#Region " List Methods "

#Region " Button's Event(s) "

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            mstrAdvanceFilter = ""
            txtICalibrationNo.Text = ""
            txtIRemark.Text = ""
            cboSPeriod.Enabled = True : txtICalibrationNo.Enabled = True
            lnkAllocation.Enabled = True : btnISearch.Enabled = True : btnSReset.Enabled = True
            cbocRating.SelectedValue = 0
            gvApplyCalibration.DataSource = Nothing
            gvApplyCalibration.DataBind()
            blnShowApplyCalibrationpopup = True
            popupApproverUseraccess.Show()
            Call Fill_FRating()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            cboPeriod.SelectedValue = 0
            cboStatus.SelectedValue = 0
            txtBatchNo.Text = ""
            dtDateFrom.SetDate = Nothing
            dtDateTo.SetDate = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(IIf(cboPeriod.SelectedValue = "", 0, cboPeriod.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 1, "Sorry, Period is mandatory information, Please select period to continue"), Me)
                Exit Sub
            End If
            Call FillGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            If Request.QueryString.Count <= 0 Then
                Response.Redirect(Session("rootpath") & "UserHome.aspx", False)
            Else
                Session.Abandon()
                Response.Redirect("~/Index.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub delReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delReason.buttonDelReasonYes_Click
        Try
            If objScoreCalibrateApproval.Void_Calibration(mblnIsGrp, _
                                                          mintCalibrationUnkid, _
                                                          mintEmployeeUnkid, _
                                                          mintPeriodUnkid, _
                                                          Session("Database_Name"), _
                                                          Session("CompanyUnkId"), _
                                                          Session("Fin_year"), _
                                                          ConfigParameter._Object._CurrentDateAndTime, _
                                                          enAuditType.DELETE, _
                                                          Session("UserId"), _
                                                          Session("IP_ADD"), _
                                                          Session("HOST_NAME"), mstrModuleName, delReason.Reason, True) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Information deleted Successfully."), Me)
                FillGrid()
                mblnIsGrp = False : mintCalibrationUnkid = 0 : mintEmployeeUnkid = 0 : mintPeriodUnkid = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfDelete_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfDelete.buttonYes_Click
        Try
            delReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Enter reason to void data.")
            delReason.Reason = ""
            delReason.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnShowAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAll.Click
        Try
        mblnShowMyReport = True
        btnRptReset_Click(sender, e)
        popupMyReport.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillGrid()
        Dim strFilter As String = String.Empty
        Dim mdtListDataTable As DataTable
        Try
            If txtBatchNo.Text.Trim.Length > 0 Then
                strFilter &= "AND calibration_no = '" & txtBatchNo.Text & "' "
            End If

            If CInt(IIf(cboStatus.SelectedValue = "", 0, cboStatus.SelectedValue)) > 0 Then
                strFilter &= "AND hrassess_computescore_approval_tran.statusunkid = '" & cboStatus.SelectedValue & "' "
            End If

            If dtDateFrom.IsNull = False Then
                strFilter &= "AND ISNULL(CONVERT(NVARCHAR(8), pScore.submit_date, 112), '') >= '" & eZeeDate.convertDate(dtDateFrom.GetDate()).ToString() & "' "
            End If

            If dtDateTo.IsNull = False Then
                strFilter &= "AND ISNULL(CONVERT(NVARCHAR(8), pScore.submit_date, 112), '') <= '" & eZeeDate.convertDate(dtDateTo.GetDate()).ToString() & "' "
            End If

            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)

            mdtListDataTable = objScoreCalibrateApproval.GetList(CInt(cboPeriod.SelectedValue), _
                                                                 Session("Database_Name"), _
                                                                 Session("UserId"), _
                                                                 Session("Fin_year"), _
                                                                 Session("CompanyUnkId"), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                 Session("UserAccessModeSetting"), True, False, "List", strFilter, Nothing, True, _
                                                                 IIf(CInt(IIf(cboEmployee.SelectedValue = "", 0, cboEmployee.SelectedValue)) > 0, " AND hrassess_computescore_approval_tran.employeeunkid = '" & cboEmployee.SelectedValue & "' ", ""))


            gvCalibrateList.AutoGenerateColumns = False
            gvCalibrateList.DataSource = mdtListDataTable
            gvCalibrateList.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Links Event(s) "

    Protected Sub lnkdelete_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            If CBool(gvCalibrateList.DataKeys(row.RowIndex)("isgrp").ToString) Then
                mintCalibrationUnkid = gvCalibrateList.DataKeys(row.RowIndex)("grpid")
                mintEmployeeUnkid = 0 : mintPeriodUnkid = 0
                mblnIsGrp = True
            Else
                mintCalibrationUnkid = gvCalibrateList.DataKeys(row.RowIndex)("grpid")
                mintEmployeeUnkid = gvCalibrateList.DataKeys(row.RowIndex)("employeeunkid")
                mintPeriodUnkid = gvCalibrateList.DataKeys(row.RowIndex)("periodunkid")
                mblnIsGrp = False
            End If
            cnfDelete.Title = "Aruti"
            cnfDelete.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Are you sure you wanted to delete information.")
            cnfDelete.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkedit_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim lnk As LinkButton = TryCast(sender, LinkButton)
            Dim row As GridViewRow = TryCast(lnk.NamingContainer, GridViewRow)
            If CBool(gvCalibrateList.DataKeys(row.RowIndex)("isgrp").ToString) Then
                cboSPeriod.SelectedValue = gvCalibrateList.DataKeys(row.RowIndex)("periodunkid")
                Dim objScoreCompute As New clsComputeScore_master
                cbocRating.SelectedValue = 0
                txtIRemark.Text = ""
                txtICalibrationNo.Text = row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text
                cboSPeriod.Enabled = False : txtICalibrationNo.Enabled = False
                lnkAllocation.Enabled = False : btnISearch.Enabled = False : btnSReset.Enabled = False
                Call FillApplyCalibratedList("ISNULL(CSM.calibratnounkid,0) = '" & gvCalibrateList.DataKeys(row.RowIndex)("grpid") & "'")
                blnShowApplyCalibrationpopup = True
                'Call btnISearch_Click(New Object(), New EventArgs)
                Call Fill_FRating()
                popupApproverUseraccess.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Grid Event(s) "

    Protected Sub gvFilterRating_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvFilterRating.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(getColumnID_Griview(gvFilterRating, "dgcolhFRating", False, True)).ToolTip = gvFilterRating.DataKeys(e.Row.RowIndex)("scrf").ToString() & " - " & gvFilterRating.DataKeys(e.Row.RowIndex)("scrt").ToString()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub gvCalibrateList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCalibrateList.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim intCount As Integer = 4 : Dim strCaption As String = String.Empty
                If CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("ispgrp").ToString) = True Then
                    For i = 4 To gvCalibrateList.Columns.Count - 1
                        If gvCalibrateList.Columns(i).Visible Then
                            e.Row.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Row.Cells(3).ColumnSpan = intCount
                    e.Row.Cells(0).CssClass = "main-group-header"
                    e.Row.Cells(1).CssClass = "main-group-header"
                    e.Row.Cells(2).CssClass = "main-group-header"
                    e.Row.Cells(3).CssClass = "main-group-header"
                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton) : lnkedit.Visible = False
                    Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton) : lnkdelete.Visible = False

                ElseIf CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("isgrp").ToString) = True Then
                    For i = 4 To gvCalibrateList.Columns.Count - 1
                        If gvCalibrateList.Columns(i).Visible Then
                            e.Row.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Row.Cells(3).ColumnSpan = intCount
                    e.Row.Cells(0).CssClass = "group-header"
                    e.Row.Cells(1).CssClass = "group-header"
                    e.Row.Cells(2).CssClass = "group-header"
                    e.Row.Cells(3).CssClass = "group-header"

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpovision", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgpovision", False, True)).Text = ""
                    End If
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgcalibratescore", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgcalibratescore", False, True)).Text = ""
                    End If

                    If gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString().Trim <> "" Then
                        strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text
                        Dim oDate As Date = eZeeDate.convertDate(gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString()).Date
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = strCaption & " - " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 10, "Date") & " [" & oDate & "]"
                    End If

                ElseIf CBool(gvCalibrateList.DataKeys(e.Row.RowIndex)("isgrp").ToString) = False Then
                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                    Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                    lnkedit.Visible = False : lnkdelete.Visible = True

                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvCalibrateList, "dgEmployee", False, True)).Text.ToString.Replace("/r/n", "")
                    End If


                End If
                If gvCalibrateList.DataKeys(e.Row.RowIndex)("submitdate").ToString().Trim <> "" Then
                    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                    Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                    lnkedit.Visible = False : lnkdelete.Visible = False
                End If
                'If CInt(gvCalibrateList.DataKeys(e.Row.RowIndex)("statusunkid").ToString) <> clsScoreCalibrationApproval.enCalibrationStatus.NotSubmitted Then
                '    Dim lnkedit As LinkButton = TryCast(e.Row.FindControl("lnkedit"), LinkButton)
                '    Dim lnkdelete As LinkButton = TryCast(e.Row.FindControl("lnkdelete"), LinkButton)
                '    lnkedit.Visible = False : lnkdelete.Visible = False
                'End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region " AddEdit Methdos "

#Region " Button's Event(s) "

    Protected Sub btnISearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnISearch.Click
        Try
            If CInt(IIf(cboSPeriod.SelectedValue = "", 0, cboSPeriod.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 1, "Sorry, Period is mandatory information, Please select period to continue"), Me)
                Exit Sub
            End If

            Dim dLst As New DataSet
            Dim xMsg As String = ""

            'dLst = objScoreCalibrateApproval.IsValidToStartCalibration(CInt(cboSPeriod.SelectedValue), Session("Database_Name").ToString(), Session("UserId"), Session("Fin_year"), Session("CompanyUnkId"), eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), Session("UserAccessModeSetting"), True, False, "List", Session("IsCompanyNeedReviewer"), xMsg)

            'If xMsg.Trim.Length > 0 Then
            '    If dLst.Tables(0).Rows.Count <= 0 Then
            '        DisplayMessage.DisplayMessage(xMsg, Me)
            '    Else
            '        valid.Message = xMsg
            '        valid.Data_Table = New DataView(dLst.Tables(0), "ascnt <= 0", "empname", DataViewRowState.CurrentRows).ToTable.DefaultView.ToTable(True, "empname")
            '        valid.Data_Table.Columns(0).ColumnName = "Employee"
            '        valid.Show()
            '        Exit Sub
            '    End If
            'End If

            Call FillApplyCalibratedList()
            Call Fill_FRating()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnIApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIApply.Click
        Try
            If CInt(IIf(cbocRating.SelectedValue = "", 0, cbocRating.SelectedValue)) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 4, "Sorry, Rating is mandatory information. Please select rating to continue."), Me)
                Exit Sub
            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing

            gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable().Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)

            Dim intFirstValue As Integer = 0
            'For Each iRow In gRow
            '    Dim mRow As DataRow() = Nothing
            '    mRow = mdtApplyNew.Select("Code = '" & iRow.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSCode", False, True)).Text & "'")
            '    If mRow.Length > 0 Then
            '        Dim dr() As DataRow = mdtRatings.Select("id = '" & cbocRating.SelectedValue & "'")
            '        Dim rnd As New Random : Dim random As Integer = 0
            '        If intFirstValue <= 0 Then
            '            random = rnd.Next(dr(0)("scrf"), dr(0)("scrt"))
            '        Else
            '            random = rnd.Next(intFirstValue, dr(0)("scrt"))
            '            If intFirstValue = random Then
            '                random = rnd.Next(intFirstValue, dr(0)("scrt"))
            '            End If
            '        End If
            '        intFirstValue = random
            '        mRow(0)("calibrated_score") = IIf(random <= 0, CDec(dr(0)("scrt")), random)
            '        mRow(0)("isChanged") = True
            '        mRow(0)("calibration_remark") = txtIRemark.Text
            '        'S.SANDEEP |26-AUG-2019| -- START
            '        'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
            '        'mRow(0)("calibRating") = cbocRating.SelectedItem.Text
            '        If CInt(cbocRating.SelectedValue) > 0 Then
            '            mRow(0)("calibRating") = cbocRating.SelectedItem.Text
            '        Else
            '            mRow(0)("calibRating") = ""
            '        End If
            '        'S.SANDEEP |26-AUG-2019| -- END
            '    End If
            'Next
            'mdtApplyNew.AcceptChanges()
            'gvApplyCalibration.AutoGenerateColumns = False
            'gvApplyCalibration.DataSource = mdtApplyNew
            'gvApplyCalibration.DataBind()
            'txtListSearch.Focus()
            'ClearData()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cnfRemark_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cnfRemark.buttonYes_Click
        Try
            'Call SaveData(mlbnIsSubmitted)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnISubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnISubmit.Click
        Try
            'If IsValidData() = False Then Exit Sub
            'mlbnIsSubmitted = True
            'If mdtApplyNew IsNot Nothing AndAlso gvApplyCalibration.Rows.Count > 0 Then

            '    'If mintCalibrationUnkid > 0 Then
            '    Dim iTotalRow, iChangedRow As Integer : iTotalRow = 0 : iChangedRow = 0
            '    iTotalRow = mdtApplyNew.Rows.Count() : iChangedRow = mdtApplyNew.AsEnumerable().Where(Function(x) x.Field(Of Decimal)("calibrated_score") > 0).Count()
            '    If iTotalRow <> iChangedRow Then
            '        DisplayMessage.DisplayMessage("Sorry, you cannot do submit operation as you have not provided score to some of the employee(s), Please assign socre to those employee(s) which are in red.", Me)
            '        Dim gvRow As IEnumerable(Of GridViewRow) = gvApplyCalibration.Rows.Cast(Of GridViewRow)()
            '        Dim oRow As IEnumerable(Of DataRow) = mdtApplyNew.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isChanged") = False)
            '        For Each iRow In oRow
            '            Dim strCode As String = iRow("Code").ToString()
            '            Dim gr = gvRow.Where(Function(x) x.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhSCode", False, True)).Text = strCode).Select(Function(X) X).First()
            '            gr.ForeColor = Color.Red
            '        Next
            '        Exit Sub
            '    End If
            '    'End If

            '    Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            '    gRow = gvApplyCalibration.Rows.Cast(Of GridViewRow).AsEnumerable()
            '    If gRow.AsEnumerable().Where(Function(x) x.Cells(GetColumnIndex.getColumnID_Griview(gvApplyCalibration, "dgcolhcalibrationremark", False, True)).Text = "&nbsp;").Count > 0 Then
            '        cnfRemark.Title = "Aruti"
            '        cnfRemark.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 7, "Are you sure you want to submit without remark?")
            '        cnfRemark.Show()
            '        Exit Sub
            '    End If

            '    Call SaveData(True)
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnIClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIClose.Click
        Try
            blnShowApplyCalibrationpopup = False
            popupApproverUseraccess.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
    End Sub

    Protected Sub btnSReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSReset.Click
        Try
            mstrAdvanceFilter = "" : If txtListSearch.Text.Trim.Length > 0 Then txtListSearch.Text = ""
            Call FillApplyCalibratedList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Method(s) "

    Private Sub FillApplyCalibratedList(Optional ByVal strExtraFilter As String = "")
        Dim strFilter As String = String.Empty
        Dim mdtFullDataTable As DataTable
        Try
            If mstrAdvanceFilter.Trim.Length > 0 Then
                strFilter &= "AND " & mstrAdvanceFilter
            End If
            If strExtraFilter.Trim.Length > 0 Then
                strFilter &= "AND " & strExtraFilter
            End If

            If cboSPeriod.Enabled = True Then
                'strFilter &= "AND ISNULL(CSM.calibrated_score,0) <= 0 "
            End If

            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)

            mdtFullDataTable = objScoreCalibrateApproval.CalibrateData(CInt(cboSPeriod.SelectedValue), _
                                                                            Session("Database_Name"), _
                                                                            Session("UserId"), _
                                                                            Session("Fin_year"), _
                                                                            Session("CompanyUnkId"), _
                                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                                            HttpContext.Current.Session("IP_ADD").ToString(), _
                                                                            HttpContext.Current.Session("HOST_NAME").ToString(), _
                                                                            mstrModuleName1, True, CBool(Session("IsCompanyNeedReviewer")), strFilter)



            'mdtFullDataTable = objScoreCalibrateApproval.GetListForSubmit(CInt(cboSPeriod.SelectedValue), _
            '                                                              Session("Database_Name"), _
            '                                                              Session("UserId"), _
            '                                                              Session("Fin_year"), _
            '                                                              Session("CompanyUnkId"), _
            '                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                              eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                              Session("UserAccessModeSetting"), True, False, "List", _
            '                                                              strFilter, Nothing, CBool(Session("IsCompanyNeedReviewer")))

            If mdtFullDataTable.Rows.Count > 0 Then
                If mdtFullDataTable.AsEnumerable().Select(Function(x) x.Field(Of String)("calibration_no")).Distinct.Count() > 1 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 101, "Sorry, you have already calibrated for the selected period. Please open it in edit mode from the list to continue."), Me)
                    Exit Sub
                End If
                txtICalibrationNo.Text = mdtFullDataTable.Rows(0)("calibration_no")
                txtIRemark.Text = mdtFullDataTable.Rows(0)("calibration_remark")
            End If
            gvApplyCalibration.DataSource = mdtFullDataTable
            gvApplyCalibration.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub ClearData()
        Try
            cbocRating.SelectedValue = 0
            txtIRemark.Text = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetTitle()
        Dim dsList As New DataSet
        Dim objRating As New clsAppraisal_Rating
        Try
            dsList = objRating.getComboList("List", True)
            For Each item As ListItem In cbocRating.Items
                Dim irow As DataRow() = Nothing
                irow = dsList.Tables(0).Select("name = '" & item.Text & "' ")
                If IsNothing(irow) = False AndAlso irow.Length > 0 Then
                    item.Attributes.Add("title", irow(0)("scrf").ToString() & " - " & irow(0)("scrt").ToString())
                End If
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList = Nothing : objRating = Nothing
        End Try
    End Sub

    <WebMethod()> _
    Public Shared Function IsNumberExists(ByVal strNum As String) As String
        Try
            Dim objComputeScore As New clsComputeScore_master
            If objComputeScore.IsCalibrationNoExist(strNum) Then
                Return "1"
            Else
                Return "0"
            End If
        Catch ex As Exception
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                'S.SANDEEP |04-MAR-2020| -- START
                '._Error_Message = clsCrypto.Encrypt(ex.Message)
                'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
                Dim S_dispmsg As String = ex.Message & "; " & ex.StackTrace.ToString
                If ex.InnerException IsNot Nothing Then
                    S_dispmsg &= "; " & ex.InnerException.Message
                End If
                'Sohail (01 Feb 2020) -- End
                S_dispmsg = S_dispmsg.Replace("'", "")
                S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                ._Error_Message = S_dispmsg
                'S.SANDEEP |04-MAR-2020| -- END
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("UserId")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With
            Dim strDBName As String = ""
            If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    strDBName = "hrmsConfiguration"
                End If
            End If
            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            End If
            Throw New Exception("Something went wrong, Please contact administrator!")
        End Try
    End Function

    <WebMethod()> _
    Public Shared Function CalibrationSubmission(ByVal intCompanyId As Integer, _
                                              ByVal intPeriodId As Integer, _
                                              ByVal intCalibratorId As Integer, _
                                              ByVal strGuids() As String, _
                                              ByVal strCalibrationNo As String, _
                                              ByVal strPeriodName As String, _
                                              ByVal strOverallRemark As String, _
                                              ByVal strFormName As String, _
                                              ByVal strSenderAddress As String, _
                                              ByVal strIPAddress As String, _
                                              ByVal strHostName As String) As String

        Try
            Dim blnFlag As Boolean = False
            Dim objApprlTran As New clsScoreCalibrationApproval
            blnFlag = objApprlTran.SubmitCalibration(intCompanyId, _
                                                     intPeriodId, _
                                                     intCalibratorId, _
                                                     strGuids, _
                                                     strCalibrationNo, _
                                                     strPeriodName, _
                                                     strOverallRemark, _
                                                     strFormName, _
                                                     strSenderAddress, _
                                                     strIPAddress, _
                                                     strHostName)
            If blnFlag Then
                Return "1"
            Else
                Return "0"
            End If
        Catch ex As Exception
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                'S.SANDEEP |04-MAR-2020| -- START
                '._Error_Message = clsCrypto.Encrypt(ex.Message)
                'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
                Dim S_dispmsg As String = ex.Message & "; " & ex.StackTrace.ToString
                If ex.InnerException IsNot Nothing Then
                    S_dispmsg &= "; " & ex.InnerException.Message
                End If
                'Sohail (01 Feb 2020) -- End
                S_dispmsg = S_dispmsg.Replace("'", "")
                S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                ._Error_Message = S_dispmsg
                'S.SANDEEP |04-MAR-2020| -- END
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("UserId")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With
            Dim strDBName As String = ""
            If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    strDBName = "hrmsConfiguration"
                End If
            End If
            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            End If
            Throw New Exception("Something went wrong, Please contact administrator!")
        Finally
        End Try


    End Function

    <WebMethod()> _
    Public Shared Function RatingUpdate(ByVal intPeriodId As Integer, ByVal strGuids() As String, ByVal strRemark As String, ByVal intRatingId As Integer) As String
        Try
            Dim blnFlag As Boolean = False
            Dim objApprlTran As New clsScoreCalibrationApproval
            blnFlag = objApprlTran.UpdateCalibrateRating(intPeriodId, strGuids, strRemark, intRatingId)
            If blnFlag Then
                Return "1"
            Else
                Return "0"
            End If
        Catch ex As Exception
            Dim objErrorLog As New clsErrorlog_Tran
            With objErrorLog
                'S.SANDEEP |04-MAR-2020| -- START
                '._Error_Message = clsCrypto.Encrypt(ex.Message)
                'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
                Dim S_dispmsg As String = ex.Message & "; " & ex.StackTrace.ToString
                If ex.InnerException IsNot Nothing Then
                    S_dispmsg &= "; " & ex.InnerException.Message
                End If
                'Sohail (01 Feb 2020) -- End
                S_dispmsg = S_dispmsg.Replace("'", "")
                S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
                ._Error_Message = S_dispmsg
                'S.SANDEEP |04-MAR-2020| -- END
                ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
                ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
                ._Companyunkid = HttpContext.Current.Session("Companyunkid")
                If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
                    ._Database_Version = ""
                Else
                    ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
                End If
                If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
                    ._Userunkid = HttpContext.Current.Session("UserId")
                    ._Loginemployeeunkid = 0
                Else
                    ._Userunkid = 0
                    ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
                End If
                ._Isemailsent = False
                ._Isweb = True
                ._Isvoid = False
                ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
                ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
            End With
            Dim strDBName As String = ""
            If HttpContext.Current.Session("mdbname") IsNot Nothing Then
                If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
                    strDBName = "hrmsConfiguration"
                End If
            End If
            If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

            End If
            Throw New Exception("Something went wrong, Please contact administrator!")
        End Try
    End Function

    '<WebMethod()> _
    'Public Shared Function UpdatedRating(ByVal strGUID As String, ByVal decScore As Decimal, ByVal strRemark As String, ByVal intRatingId As Integer) As String
    '    Try
    '        Dim blnFlag As Boolean = False
    '        Dim objApprlTran As New clsScoreCalibrationApproval
    '        blnFlag = objApprlTran.UpdateCalibrateRating(strGUID, decScore, strRemark, intRatingId)
    '        Return intRatingId.ToString
    '    Catch ex As Exception
    '        Dim objErrorLog As New clsErrorlog_Tran
    '        With objErrorLog
    '            'S.SANDEEP |04-MAR-2020| -- START
    '            '._Error_Message = clsCrypto.Encrypt(ex.Message)
    '            'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
    '            Dim S_dispmsg As String = ex.Message & "; " & ex.StackTrace.ToString
    '            If ex.InnerException IsNot Nothing Then
    '                S_dispmsg &= "; " & ex.InnerException.Message
    '            End If
    '            'Sohail (01 Feb 2020) -- End
    '            S_dispmsg = S_dispmsg.Replace("'", "")
    '            S_dispmsg = S_dispmsg.Replace(vbCrLf, "")
    '            ._Error_Message = S_dispmsg
    '            'S.SANDEEP |04-MAR-2020| -- END
    '            ._Error_Location = HttpContext.Current.Request.Url.AbsoluteUri
    '            ._Error_DateString = Format(DateAndTime.Now, "yyyy-MM-dd HH:mm:ss")
    '            ._Companyunkid = HttpContext.Current.Session("Companyunkid")
    '            If HttpContext.Current.Session("DatabaseVersion") Is Nothing Then
    '                ._Database_Version = ""
    '            Else
    '                ._Database_Version = HttpContext.Current.Session("DatabaseVersion")
    '            End If
    '            If CType(HttpContext.Current.Session("LoginBy"), Global.User.en_loginby) = Global.User.en_loginby.User Then
    '                ._Userunkid = HttpContext.Current.Session("UserId")
    '                ._Loginemployeeunkid = 0
    '            Else
    '                ._Userunkid = 0
    '                ._Loginemployeeunkid = HttpContext.Current.Session("Employeeunkid")
    '            End If
    '            ._Isemailsent = False
    '            ._Isweb = True
    '            ._Isvoid = False
    '            ._Ip = If(HttpContext.Current.Session("IP_ADD") Is Nothing, "", HttpContext.Current.Session("IP_ADD")).ToString
    '            ._Host = If(HttpContext.Current.Session("HOST_NAME") Is Nothing, "", HttpContext.Current.Session("HOST_NAME")).ToString
    '        End With
    '        Dim strDBName As String = ""
    '        If HttpContext.Current.Session("mdbname") IsNot Nothing Then
    '            If HttpContext.Current.Session("mdbname").ToString.Trim.ToLower.StartsWith("tran") = False Then
    '                strDBName = "hrmsConfiguration"
    '            End If
    '        End If
    '        If objErrorLog.Insert(Nothing, objErrorLog, strDBName) = False Then

    '        End If
    '        Throw New Exception("Something went wrong, Please contact administrator!")
    '    End Try
    'End Function

#End Region

#Region " Link's Event(s) "

    'S.SANDEEP |26-AUG-2019| -- START
    'ISSUE/ENHANCEMENT : CalibrationReviewCommentsV3
    'Protected Sub lnkFilterData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFilterData.Click
    '    Try
    '        Dim oView As DataView = mdtApplyNew.DefaultView
    '        Dim StrSearch As String = String.Empty
    '        If txtSearch.Text.Trim.Length > 0 Then
    '            StrSearch = "Code LIKE '%" & txtSearch.Text & "%' OR " & _
    '                        "EmployeeName LIKE '%" & txtSearch.Text & "%' OR " & _
    '                        "JobTitle LIKE '%" & txtSearch.Text & "%' "
    '        End If

    '        Dim mDecFromScore, mDecToScore As Decimal : mDecFromScore = 0 : mDecToScore = 0

    '        If cboPRatingFrm.SelectedIndex > 0 AndAlso cboPRatingTo.SelectedIndex > 0 Then
    '            If cboPRatingTo.SelectedIndex < cboPRatingFrm.SelectedIndex Then
    '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 11, "Sorry, To rating cannot be less than From rating. Please select higher rating to continue."), Me)
    '                Exit Sub
    '            Else
    '                Dim dr() As DataRow = mdtRatings.Select("id = '" & cboPRatingFrm.SelectedValue & "'")
    '                If dr.Length > 0 Then mDecFromScore = dr(0)("scrf")
    '                dr = mdtRatings.Select("id = '" & cboPRatingTo.SelectedValue & "'")
    '                If dr.Length > 0 Then mDecToScore = dr(0)("scrt")
    '    If StrSearch.Trim.Length > 0 Then
    '        StrSearch &= " AND (Provision_Score >= " & mDecFromScore & " AND Provision_Score <= " & mDecToScore & ") "
    '    Else
    '        StrSearch &= " (Provision_Score >= " & mDecFromScore & " AND Provision_Score <= " & mDecToScore & ") "
    '    End If
    '            End If
    '        ElseIf cboPRatingFrm.SelectedIndex > 0 Then
    '            Dim dr() As DataRow = mdtRatings.Select("id = '" & cboPRatingFrm.SelectedValue & "'")
    '            If dr.Length > 0 Then mDecFromScore = dr(0)("scrf")
    '    If StrSearch.Trim.Length > 0 Then
    '        StrSearch &= " AND (Provision_Score >= " & mDecFromScore & ") "
    '    Else
    '                StrSearch &= " (Provision_Score >= " & mDecFromScore & ") "
    '    End If
    '        ElseIf cboPRatingTo.SelectedIndex > 0 Then
    '            Dim dr() As DataRow = mdtRatings.Select("id = '" & cboPRatingTo.SelectedValue & "'")
    '            If dr.Length > 0 Then mDecToScore = dr(0)("scrt")
    '    If StrSearch.Trim.Length > 0 Then
    '        StrSearch &= " AND (Provision_Score <= " & mDecToScore & ") "
    '    Else
    '        StrSearch &= " (Provision_Score <= " & mDecToScore & ") "
    '    End If
    '        End If


    '        'If txtScoreFrom.Text.Trim.Length > 0 Then Decimal.TryParse(txtScoreFrom.Text, mDecFromScore)
    '        'If txtScoreTo.Text.Trim.Length > 0 Then Decimal.TryParse(txtScoreTo.Text, mDecToScore)

    '        'If txtScoreFrom.Text.Trim.Length > 0 AndAlso txtScoreTo.Text.Trim.Length > 0 Then
    '        '    If StrSearch.Trim.Length > 0 Then
    '        '        StrSearch &= " AND (Provision_Score >= " & mDecFromScore & " AND Provision_Score <= " & mDecToScore & ") "
    '        '    Else
    '        '        StrSearch &= " (Provision_Score >= " & mDecFromScore & " AND Provision_Score <= " & mDecToScore & ") "
    '        '    End If
    '        'ElseIf txtScoreFrom.Text.Trim.Length > 0 Then
    '        '    If StrSearch.Trim.Length > 0 Then
    '        '        StrSearch &= " AND (Provision_Score >= " & mDecFromScore & ") "
    '        '    Else
    '        '        StrSearch &= "(Provision_Score >= " & mDecFromScore & ") "
    '        '    End If
    '        'ElseIf txtScoreTo.Text.Trim.Length > 0 Then
    '        '    If StrSearch.Trim.Length > 0 Then
    '        '        StrSearch &= " AND (Provision_Score <= " & mDecToScore & ") "
    '        '    Else
    '        '        StrSearch &= " (Provision_Score <= " & mDecToScore & ") "
    '        '    End If
    '        'End If

    '        oView.RowFilter = StrSearch
    '        gvApplyCalibration.AutoGenerateColumns = False
    '        gvApplyCalibration.DataSource = oView
    '        gvApplyCalibration.DataBind()
    '        'mView = oView
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'S.SANDEEP |26-AUG-2019| -- END

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            popupAdvanceFilter.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#Region " Textbox Event(s) "

#End Region

#End Region

#Region " Grid Event(s) "

    Protected Sub gvApplyCalibration_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvApplyCalibration.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "dgcolhcalibRating", False, True)).Text <> "&nbsp;" Then
                    e.Row.ForeColor = Color.Blue
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            If e.Row.RowType = DataControlRowType.DataRow Or e.Row.RowType = DataControlRowType.Header Then
                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "objdgcolhtranguid", False, True)).Attributes.Add("style", "display:none")
                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "objMsgId", False, True)).Attributes.Add("style", "display:none")

                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "dgcolhSProvision", False, True)).Attributes.Add("Id", "pScore")
                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "objdgcolhtranguid", False, True)).Attributes.Add("Id", "trgid")

                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "dgcolhprovRating", False, True)).Attributes.Add("Id", "pRate")
                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "dgcolhcalibRating", False, True)).Attributes.Add("Id", "cRate")
                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "dgcolhcalibrationremark", False, True)).Attributes.Add("Id", "cRem")
                e.Row.Cells(getColumnID_Griview(gvApplyCalibration, "objMsgId", False, True)).Attributes.Add("ID", "mCode")
            End If
        End Try
    End Sub

#End Region

#End Region

#Region " Report Methods "

#Region " Button's Events "

    Protected Sub btnRptShow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRptShow.Click
        Try
            Dim strFilter As String = String.Empty
            If CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) > 0 Then
                strFilter &= "AND iData.calibratnounkid = '" & CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) & "'"
            End If
            If CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) > 0 Then
                strFilter &= "AND iData.employeeunkid = '" & CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) & "'"
            End If
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            Dim mdtDataTable As DataTable
            mdtDataTable = objScoreCalibrateApproval.GetEmployeeApprovalDataNew(CInt(cboRptPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), False, True, Nothing, 0, True, strFilter, "", False, False, "")
            gvMyReport.AutoGenerateColumns = False
            gvMyReport.DataSource = mdtDataTable
            gvMyReport.DataBind()
            popupMyReport.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnRptReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRptReset.Click
        Try
            cboRptCalibNo.SelectedValue = 0
            cboRptEmployee.SelectedValue = 0
            gvMyReport.AutoGenerateColumns = False
            gvMyReport.DataSource = Nothing
            gvMyReport.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnExportMyReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportMyReport.Click
        Try
            If gvMyReport.Rows.Count <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 25, "Sorry, there is no data in order to export."), Me)
                Exit Sub
            End If

            Dim strFilter As String = String.Empty
            If CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) > 0 Then
                strFilter &= "AND iData.calibratnounkid = '" & CInt(IIf(cboRptCalibNo.SelectedValue = "", 0, cboRptCalibNo.SelectedValue)) & "'"
            End If
            If CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) > 0 Then
                strFilter &= "AND iData.employeeunkid = '" & CInt(IIf(cboRptEmployee.SelectedValue = "", 0, cboRptEmployee.SelectedValue)) & "'"
            End If
            If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
            Dim mdtDataTable As DataTable
            mdtDataTable = objScoreCalibrateApproval.GetEmployeeApprovalDataNew(CInt(cboRptPeriod.SelectedValue), Session("Database_Name"), Session("CompanyUnkId"), Session("Fin_year"), Session("UserAccessModeSetting"), enUserPriviledge.AllowToApproveRejectCalibratedScore, Session("EmployeeAsOnDate"), Session("UserId"), False, True, Nothing, 0, True, strFilter, "", False, False, "")
            Dim objRpt As New ArutiReports.clsPerformanceEvaluationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
            objRpt.Export_Calibration_Report(Session("UserId"), _
                                             Session("CompanyUnkId"), _
                                             mdtDataTable, strFilter, _
                                             Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 24, "Approver Wise Status"), _
                                             IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), False)

            If objRpt._FileNameAfterExported.Trim <> "" Then
                Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
                response.ClearContent()
                response.Clear()
                response.ContentType = "text/plain"
                response.AddHeader("Content-Disposition", "attachment; filename=" + objRpt._FileNameAfterExported + ";")
                response.TransmitFile(IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objRpt._FileNameAfterExported)
                response.Flush()
                response.Buffer = False
                response.End()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCloseMyReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCloseMyReport.Click
        Try
            mblnShowMyReport = False
            popupMyReport.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    Protected Sub cboRptPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRptPeriod.SelectedIndexChanged
        Try
            Dim dsList As New DataSet
            dsList = objScoreCalibrateApproval.GetComboListCalibrationNo("List", CInt(cboRptPeriod.SelectedValue), CInt(Session("UserId")), True)
            With cboRptCalibNo
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " GridViewEvent "

    Protected Sub gvMyReport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvMyReport.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If CBool(gvMyReport.DataKeys(e.Row.RowIndex)("isgrp").ToString) = True Then
                    e.Row.BackColor = Color.Silver
                    e.Row.ForeColor = Color.Black
                    e.Row.Font.Bold = True

                    Dim intCount As Integer = 1
                    For i = 1 To gvCalibrateList.Columns.Count - 1
                        If gvCalibrateList.Columns(i).Visible Then
                            e.Row.Cells(i).Visible = False
                            intCount += 1
                        End If
                    Next
                    e.Row.Cells(0).ColumnSpan = intCount

                    Dim strCaption As String = ""
                    Dim strUser As String = gvMyReport.DataKeys(e.Row.RowIndex)("calibuser").ToString()
                    Dim strallocation As String = gvMyReport.DataKeys(e.Row.RowIndex)("allocation").ToString()

                    strCaption = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text & " - (" & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 13, "Total") & " : " & _
                                 gvMyReport.DataKeys(e.Row.RowIndex)("total").ToString() & ") "

                    If strallocation.Trim <> "" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = strCaption & " - " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 19, "By") & " " & strUser & " (" & strallocation & ") "
                    Else
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = strCaption & " - " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, 19, "By") & " " & strUser
                    End If
                    'S.SANDEEP |04-OCT-2019| -- END

                Else
                    If e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text <> "&nbsp;" Then
                        e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text = e.Row.Cells(GetColumnIndex.getColumnID_Griview(gvMyReport, "dgcolhMEmployee", False, True)).Text.ToString.Replace("/r/n", "&nbsp;")
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region " Chart Methods "

    Protected Sub btnChReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChReset.Click
        Try        
            cboChPeriod.SelectedIndex = 0
            cbochDisplay.SelectedIndex = 0
            dgvCalibData.DataSource = Nothing
            dgvCalibData.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnDisplayCurve_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisplayCurve.Click
        Try
            cboChPeriod.SelectedIndex = 0
            cbochDisplay.SelectedIndex = 0
            dgvCalibData.DataSource = Nothing
            dgvCalibData.DataBind()
            mblnShowHPOCurve = True
            popupHPOChart.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnChGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChGenerate.Click
        Try
            If cboChPeriod.SelectedValue <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmPerformanceEvaluationReport", 1, "Period is mandatory information. Please select Period to continue."), Me)
                cboChPeriod.Focus()
                Exit Sub
            End If

            Dim dsDataSet As New DataSet
            Dim objPerformance As New ArutiReports.clsPerformanceEvaluationReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            dsDataSet = objPerformance.GenerateChartData(CInt(cboChPeriod.SelectedValue), _
                                                            Session("Database_Name").ToString, _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            CStr(Session("UserAccessModeSetting")), True, _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                                                            False, 1, "List", , , , , IIf(cbochDisplay.SelectedValue = "", clsComputeScore_master.enScoreMode.PROVISIONAL_SCORE, cbochDisplay.SelectedValue))


            If dsDataSet.Tables.Count > 0 Then
                dgvCalibData.DataSource = dsDataSet.Tables(0)
                dgvCalibData.DataBind()

                If dgvCalibData.Items.Count > 0 Then
                    Dim gvr As DataGridItem = Nothing

                    gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(6).Text = "N").FirstOrDefault()
                    If gvr IsNot Nothing Then
                        gvr.ForeColor = Color.Blue
                    End If
                    gvr = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Where(Function(x) x.Cells(6).Text = "A").FirstOrDefault()
                    If gvr IsNot Nothing Then
                        gvr.ForeColor = Color.Tomato
                    End If
                    Dim xItems = dgvCalibData.Items.Cast(Of DataGridItem).AsEnumerable().Select(Function(x) x.Cells(6))
                    If xItems IsNot Nothing AndAlso xItems.Count > 0 Then
                        For Each item In xItems
                            item.Visible = False
                        Next
                    End If
                End If

                chHpoCurve.ChartAreas(0).AxisX.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 100, "Ratings")
                chHpoCurve.ChartAreas(0).AxisY.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 101, "%")
                chHpoCurve.ChartAreas(0).AxisX.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chHpoCurve.ChartAreas(0).AxisY.TitleFont = New Font("Tahoma", 9, FontStyle.Bold)
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.Format = "{0.##}%"

                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.Interval = 10
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.IntervalOffset = 10
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.IntervalOffsetType = DateTimeIntervalType.Number
                chHpoCurve.ChartAreas(0).AxisY.LabelStyle.IntervalType = DateTimeIntervalType.Number
                chHpoCurve.ChartAreas(0).AxisY.MajorGrid.Enabled = False
                chHpoCurve.ChartAreas(0).AxisY.Maximum = 105
                chHpoCurve.ChartAreas(0).AxisY.Minimum = 0
                chHpoCurve.ChartAreas(0).AxisY.ScaleBreakStyle.MaxNumberOfBreaks = 1
                chHpoCurve.ChartAreas(0).AxisY.ScaleBreakStyle.Spacing = 0

                chHpoCurve.Series(0).Points.DataBindXY(dsDataSet.Tables("Chart").DefaultView, "erate", dsDataSet.Tables("Chart").DefaultView, "np")
                chHpoCurve.Series(1).Points.DataBindXY(dsDataSet.Tables("Chart").DefaultView, "erate", dsDataSet.Tables("Chart").DefaultView, "ap")

                chHpoCurve.Titles(0).Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 200, "Performance HPO Curve") & vbCrLf & _
                                      cboChPeriod.SelectedItem.Text

                chHpoCurve.Titles(0).Font = New Font(chHpoCurve.Font.Name, 11, FontStyle.Bold)

                chHpoCurve.Series(0).LabelFormat = "{0.##}%"
                chHpoCurve.Series(1).LabelFormat = "{0.##}%"

                chHpoCurve.Series(0).IsVisibleInLegend = False
                chHpoCurve.Series(1).IsVisibleInLegend = False

                chHpoCurve.Series(0).Color = Color.Blue
                chHpoCurve.Series(1).Color = Color.Tomato

                Dim legendItem1 As New LegendItem()
                legendItem1.Name = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 102, "Normal Distribution")
                legendItem1.ImageStyle = LegendImageStyle.Rectangle
                legendItem1.ShadowOffset = 2
                legendItem1.Color = chHpoCurve.Series(0).Color
                chHpoCurve.Legends(0).CustomItems.Add(legendItem1)

                Dim legendItem2 As New LegendItem()
                If dsDataSet.Tables("Chart").Rows.Count > 0 Then
                    legendItem2.Name = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 103, "Actual Performance") & dsDataSet.Tables("Chart").Rows(0)("litem").ToString()
                Else
                    legendItem2.Name = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 103, "Actual Performance")
                End If
                legendItem2.ImageStyle = LegendImageStyle.Rectangle
                legendItem2.ShadowOffset = 2
                legendItem2.Color = chHpoCurve.Series(1).Color
                chHpoCurve.Legends(0).CustomItems.Add(legendItem2)

                chHpoCurve.Series(0).ToolTip = chHpoCurve.Legends(0).CustomItems(0).Name & " : is #VAL "
                chHpoCurve.Series(1).ToolTip = chHpoCurve.Legends(0).CustomItems(1).Name & " : is #VAL "

                For i As Integer = 0 To chHpoCurve.Series.Count - 1
                    For Each dp As DataPoint In chHpoCurve.Series(i).Points
                        If dp.YValues(0) = 0 Then
                            dp.Label = " "
                        End If
                    Next
                Next
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmPerformanceEvaluationReport", 4, "Sorry, No performace evaluation data present for the selected period."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnChClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChClose.Click
        Try
            mblnShowHPOCurve = False
            popupHPOChart.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetControlCaptions()
        Try
            ''S.SANDEEP |27-JUL-2019| -- START
            ''ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            ''Language.setLanguage(mstrModuleName)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,mstrModuleName, Me.Title)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblCaption.ID, Me.lblCaption.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblBatchNo.ID, Me.lblBatchNo.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblPeriod.ID, Me.lblPeriod.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblEmployee.ID, Me.lblEmployee.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblStatus.ID, Me.lblStatus.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblSubmitDateFrom.ID, Me.lblSubmitDateFrom.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.lblSubmitDateTo.ID, Me.lblSubmitDateTo.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnShowAll.ID, Me.btnShowAll.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnNew.ID, Me.btnNew.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnSearch.ID, Me.btnSearch.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnReset.ID, Me.btnReset.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,Me.btnClose.ID, Me.btnClose.Text)




            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibrateList.Columns(3).FooterText, gvCalibrateList.Columns(3).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibrateList.Columns(4).FooterText, gvCalibrateList.Columns(4).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibrateList.Columns(5).FooterText, gvCalibrateList.Columns(5).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibrateList.Columns(6).FooterText, gvCalibrateList.Columns(6).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibrateList.Columns(7).FooterText, gvCalibrateList.Columns(7).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,gvCalibrateList.Columns(8).FooterText, gvCalibrateList.Columns(8).HeaderText)



            ''Language.setLanguage(mstrModuleName1)

            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblCalibrateHeading.ID, Me.lblCalibrateHeading.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblFilter.ID, Me.lblFilter.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblSPeriod.ID, Me.lblSPeriod.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnISearch.ID, Me.btnISearch.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnSReset.ID, Me.btnSReset.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblCalibrationNo.ID, Me.lblCalibrationNo.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblCRating.ID, Me.lblCRating.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lnkCopyData.ID, Me.lnkCopyData.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblIRemark.ID, Me.lblIRemark.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnIApply.ID, Me.btnIApply.Text)


            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnISubmit.ID, Me.btnISubmit.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.btnIClose.ID, Me.btnIClose.Text)


            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,Me.lblFilterData.ID, Me.lblFilterData.Text)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvFilterRating.Columns(1).FooterText, gvFilterRating.Columns(1).HeaderText)

            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(1).FooterText, gvApplyCalibration.Columns(1).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(2).FooterText, gvApplyCalibration.Columns(2).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(3).FooterText, gvApplyCalibration.Columns(3).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(4).FooterText, gvApplyCalibration.Columns(4).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(5).FooterText, gvApplyCalibration.Columns(5).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(6).FooterText, gvApplyCalibration.Columns(6).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(7).FooterText, gvApplyCalibration.Columns(7).HeaderText)
            'Basepage.SetWebCaption(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,gvApplyCalibration.Columns(8).FooterText, gvApplyCalibration.Columns(8).HeaderText)

            'S.SANDEEP |27-JUL-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub SetLanguage()
        Try
            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            ''Language.setLanguage(mstrModuleName)
            'Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            'Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPageHeader.ID, Me.lblPageHeader.Text)

            'Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblCaption.ID, Me.lblCaption.Text)
            'Me.lblBatchNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblBatchNo.ID, Me.lblBatchNo.Text)
            'Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblPeriod.ID, Me.lblPeriod.Text)
            'Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
            'Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblStatus.ID, Me.lblStatus.Text)
            'Me.lblSubmitDateFrom.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblSubmitDateFrom.ID, Me.lblSubmitDateFrom.Text)
            'Me.lblSubmitDateTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblSubmitDateTo.ID, Me.lblSubmitDateTo.Text)
            'Me.btnShowAll.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnShowAll.ID, Me.btnShowAll.Text)
            'Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnNew.ID, Me.btnNew.Text)
            'Me.btnSearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSearch.ID, Me.btnSearch.Text)
            'Me.btnReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnReset.ID, Me.btnReset.Text)
            'Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnClose.ID, Me.btnClose.Text)


            'gvCalibrateList.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibrateList.Columns(3).FooterText, gvCalibrateList.Columns(3).HeaderText)
            'gvCalibrateList.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibrateList.Columns(4).FooterText, gvCalibrateList.Columns(4).HeaderText)
            'gvCalibrateList.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibrateList.Columns(5).FooterText, gvCalibrateList.Columns(5).HeaderText)
            'gvCalibrateList.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibrateList.Columns(6).FooterText, gvCalibrateList.Columns(6).HeaderText)
            'gvCalibrateList.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibrateList.Columns(7).FooterText, gvCalibrateList.Columns(7).HeaderText)
            'gvCalibrateList.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),gvCalibrateList.Columns(8).FooterText, gvCalibrateList.Columns(8).HeaderText)

            ''Language.setLanguage(mstrModuleName1)

            'Me.lblCalibrateHeading.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblCalibrateHeading.ID, Me.lblCalibrateHeading.Text)
            'Me.lblFilter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblFilter.ID, Me.lblFilter.Text)
            'Me.lnkAllocation.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lnkAllocation.ID, Me.lnkAllocation.Text)
            'Me.lblSPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblSPeriod.ID, Me.lblSPeriod.Text)
            'Me.btnISearch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnISearch.ID, Me.btnISearch.Text)
            'Me.btnSReset.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnSReset.ID, Me.btnSReset.Text)
            'Me.lblCalibrationNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblCalibrationNo.ID, Me.lblCalibrationNo.Text)
            'Me.lblCRating.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblCRating.ID, Me.lblCRating.Text)
            'Me.lnkCopyData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lnkCopyData.ID, Me.lnkCopyData.Text)
            'Me.lblIRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblIRemark.ID, Me.lblIRemark.Text)
            'Me.btnIApply.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnIApply.ID, Me.btnIApply.Text)
            ''Me.lblpRatingFrm.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblpRatingFrm.ID, Me.lblpRatingFrm.Text)
            ''Me.lblpRatingTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lblpRatingTo.ID, Me.lblpRatingTo.Text)
            ''Me.lnkFilterData.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.lnkFilterData.ID, Me.lnkFilterData.Text)

            'Me.btnISubmit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnISubmit.ID, Me.btnISubmit.Text)
            'Me.btnIClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),Me.btnIClose.ID, Me.btnIClose.Text)


            'gvApplyCalibration.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),gvApplyCalibration.Columns(1).FooterText, gvApplyCalibration.Columns(1).HeaderText)
            'gvApplyCalibration.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),gvApplyCalibration.Columns(2).FooterText, gvApplyCalibration.Columns(2).HeaderText)
            'gvApplyCalibration.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),gvApplyCalibration.Columns(3).FooterText, gvApplyCalibration.Columns(3).HeaderText)
            'gvApplyCalibration.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),gvApplyCalibration.Columns(4).FooterText, gvApplyCalibration.Columns(4).HeaderText)
            'gvApplyCalibration.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),gvApplyCalibration.Columns(5).FooterText, gvApplyCalibration.Columns(5).HeaderText)
            'gvApplyCalibration.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),gvApplyCalibration.Columns(6).FooterText, gvApplyCalibration.Columns(6).HeaderText)
            'gvApplyCalibration.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),gvApplyCalibration.Columns(7).FooterText, gvApplyCalibration.Columns(7).HeaderText)
            'gvApplyCalibration.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),gvApplyCalibration.Columns(8).FooterText, gvApplyCalibration.Columns(8).HeaderText)



            'S.SANDEEP |27-JUL-2019| -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Language.setMessage("frmPerformanceEvaluationReport", 1, "Period is mandatory information. Please select Period to continue.")
            Language.setMessage("frmPerformanceEvaluationReport", 4, "Sorry, No performace evaluation data present for the selected period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Are you sure you wanted to delete information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 2, "Information deleted Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 3, "Enter reason to void data.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 20, "Period :")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 21, "Calibration No :")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 22, "Employee :")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 23, "Approver :")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 24, "Approver Wise Status")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 25, "Sorry, there is no data in order to export.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 100, "Ratings")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 101, "%")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 102, "Normal Distribution")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 103, "Actual Performance")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 200, "Performance HPO Curve")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 1, "Sorry, Period is mandatory information, Please select period to continue")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 3, "Sorry, Please check atleast one item from the list in order to assign calibrated score.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 4, "Sorry, Rating is mandatory information. Please select rating to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 5, "Sorry, Calibration number is mandatory information. Please provide calibration number to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 7, "Are you sure you want to submit without remark?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 8, "Sorry, Calibration number is mandatory information. Please provide calibration number to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 9, "Information Submitted Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 10, "Date")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 13, "Total")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 15, "Information Saved Successfully.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 16, "Are you sure you want to save without calibration rating(s)?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1, 19, "By")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>





End Class
