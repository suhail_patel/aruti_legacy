﻿<%@ Page Title="" Language="VB" MasterPageFile="~/home.master" AutoEventWireup="false"
    CodeFile="wPg_ImportCalibrateScore.aspx.vb" Inherits="Assessment_New_Peformance_Calibration_wPg_ImportCalibrateScore" %>

<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="cnf" %>
<%@ Register Src="~/Controls/OperationButton.ascx" TagName="ExportMenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>

    <script type="text/javascript">
        function IsValidFile() {
            var yourFileName = $('#<%=FileUpload1.ClientID %>').val();
            var extension = yourFileName.substring(yourFileName.lastIndexOf('.') + 1).toLowerCase();
            switch (extension) {
                case "xlsx":
                    $("#<%= btnUpload.ClientID %>").click();
                    break;
                default:
                    alert("File with 'xlsx' extension is allowed. Please select valid file in order to import data.");
                    break;
            }
        }        
    </script>

    <center>
        <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
            <asp:UpdatePanel ID="uppnl_mian" runat="server" EnableViewState="true">
                <ContentTemplate>
                    <div class="panel-primary">
                        <div class="panel-heading">
                            <asp:Label ID="lblPageHeader" runat="server" Text="Import Calibration"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        <asp:Label ID="lblDetialHeader" runat="server" Text="Import/Mapping Parameters"></asp:Label>
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <div class="row2">
                                        <asp:Label ID="lblSelectFile" runat="server" Width="15%" Text="Select Excel File to Import"></asp:Label>
                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                        <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btndefault" OnClientClick="IsValidFile()"
                                            OnClick="btnUpload_Click" />
                                        <asp:Button ID="btnGetFileFormat" runat="server" Text="Get File Format" CssClass="btndefault" />
                                    </div>
                                    <hr />
                                    <div class="row2">
                                        <div class="ib" style="width: 12%">
                                            <span style="color: Red; font-size: small">*</span>
                                            <asp:Label ID="lblCalibratorName" runat="server" Text="Calibrate Username"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 16%">
                                            <asp:DropDownList ID="cboCalibratorName" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 12%">
                                            <span style="color: Red; font-size: small">*</span>
                                            <asp:Label ID="lblEmployeeCode" runat="server" Text="Employee Code"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 16%">
                                            <asp:DropDownList ID="cboEmployeeCode" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 12%">
                                            <span style="color: Red; font-size: small">*</span>
                                            <asp:Label ID="lblEmployeeName" runat="server" Text="Employee Name"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 16%">
                                            <asp:DropDownList ID="cboEmployeeName" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 12%">
                                            <span style="color: Red; font-size: small">*</span>
                                            <asp:Label ID="lblPeriodName" runat="server" Text="Period Name"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 16%">
                                            <asp:DropDownList ID="cboPeriodName" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 12%">
                                            <span style="color: Red; font-size: small">*</span>
                                            <asp:Label ID="lblCalibratorRating" runat="server" Text="Calibration Rating"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 16%">
                                            <asp:DropDownList ID="cboCalibrationRating" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 12%">
                                            <span style="color: Red; font-size: small">*</span>
                                            <asp:Label ID="lblCalibrationRemark" runat="server" Text="Calibration Remark"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 16%">
                                            <asp:DropDownList ID="cboCalibrationRemark" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row2">
                                        <div class="ib" style="width: 12%">
                                            <span style="color: Red; font-size: small">*</span>
                                            <asp:Label ID="lblCalibrationNo" runat="server" Text="Calibration Number"></asp:Label>
                                        </div>
                                        <div class="ib" style="width: 16%">
                                            <asp:DropDownList ID="cboCalibrationNo" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="ib" style="width: 20%">
                                            <asp:CheckBox ID="chkSubmitStatus" runat="server" Text="Import With Submit Status"
                                                Font-Bold="true" />
                                        </div>
                                        <div class="ib" style="width: 16%">
                                            <asp:LinkButton ID="lnkImportData" runat="server" Text="Import Data" Font-Underline="false"
                                                Font-Bold="true"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="row2">
                                    </div>
                                    <hr />
                                    <div class="row2">
                                        <asp:Panel ID="pnlAccomplishmentData" runat="server" Style="width: 99%; max-height: 250px"
                                            ScrollBars="Auto">
                                            <asp:GridView ID="gvData" runat="server" CssClass="gridview" HeaderStyle-CssClass="griviewheader"
                                                AutoGenerateColumns="false" RowStyle-CssClass="griviewitem" Style="margin: 0px"
                                                AllowPaging="false" HeaderStyle-Font-Bold="false" Width="100%">
                                            </asp:GridView>
                                        </asp:Panel>
                                    </div>
                                </div>
                                <div class="btn-default" style="height: 31px">
                                    <div style="float: left">
                                        <uc1:ExportMenu ID="btnExport" runat="server" Text="Export" TragateControlId="data"
                                            CssClass="btndefault" />
                                        <div id="data" style="display: none">
                                            <asp:LinkButton ID="mnuExportEE" Style="min-width: 200px" runat="server" Text="Export Error"></asp:LinkButton>
                                            <asp:LinkButton ID="mnuExportSF" runat="server" Style="min-width: 200px" Text="Export Successful"></asp:LinkButton>
                                            <asp:LinkButton ID="mnuExportEA" runat="server" Style="min-width: 200px" Text="Export All"></asp:LinkButton>
                                        </div>
                                    </div>
                                    <asp:Button ID="btnclose" runat="server" CssClass="btndefault" Text="Close" />
                                </div>
                            </div>
                        </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUpload" />
                    <asp:PostBackTrigger ControlID="btnGetFileFormat" />
                    <asp:PostBackTrigger ControlID="mnuExportEE" />
                    <asp:PostBackTrigger ControlID="mnuExportSF" />
                    <asp:PostBackTrigger ControlID="mnuExportEA" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
</asp:Content>
