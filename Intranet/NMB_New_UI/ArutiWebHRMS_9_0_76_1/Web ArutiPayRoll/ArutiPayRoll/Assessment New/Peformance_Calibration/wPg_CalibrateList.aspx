<%@ Page Title="" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPg_CalibrateList.aspx.vb" Inherits="Assessment_New_Peformance_Calibration_wPg_CalibrateList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc9" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="ConfirmYesNo" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/CommonValidationList.ascx" TagName="CV" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

        function pageLoad(sender, args) {
            $("select").searchable();
        }
    </script>--%>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (cval.length > 0)
                if (charCode == 45)
                if (cval.indexOf("-") > -1)
                return false;

            if (charCode == 13)
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <%--<style>
        .ib
        {
            display: inline-block;
            margin-right: 10px;
        }
    </style>--%>
    <style>
        .vl
        {
            border-left: 1px solid black;
            height: 15px;
        }
        .swal-wide
        {
            width: 600px !important;
        }
    </style>

    <script language="javascript" type="text/javascript">
        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };


        function ListSeaching() {
            if ($('#<%= txtClistSearch.ClientID %>').val().length > 0) {
                $('#<%= gvCalibrateList.ClientID %> tbody tr').hide();
                $('#<%= gvCalibrateList.ClientID %> tbody tr:first').show();
                $('#<%= gvCalibrateList.ClientID %> tbody tr td:containsNoCase(\'' + $('#<%= txtClistSearch.ClientID %>').val() + '\')').parent().show();
                $('#<%= gvCalibrateList.ClientID %> tbody tr td.MainGroupHeaderStyleLeft').parent().show();
                $('#<%= gvCalibrateList.ClientID %> tbody tr td.GroupHeaderStylecompLeft').parent().show();
            }
            else if ($('#<%= txtClistSearch.ClientID %>').val().length == 0) {
                resetListSeaching();
            }
            if ($('#<%= gvCalibrateList.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetListSeaching();
            }
        }
        function resetListSeaching() {
            $('#<%= txtClistSearch.ClientID %>').val('');
            $('#<%= gvCalibrateList.ClientID %> tr').show();
            $('.norecords').remove();
            $('#<%= txtClistSearch.ClientID %>').focus();
        }


        function FromSearching() {
            if ($('#<%= txtListSearch.ClientID %>').val().length > 0) {
                $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
                $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
                $('#<%= gvApplyCalibration.ClientID %> tbody tr td:containsNoCase(\'' + $('#<%= txtListSearch.ClientID %>').val() + '\')').parent().show();
            }
            else if ($('#<%= txtListSearch.ClientID %>').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= gvApplyCalibration.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }

        function resetFromSearchValue() {
            $('#<%= txtListSearch.ClientID %>').val('');
            $('#<%= gvApplyCalibration.ClientID %> tr').show();
            $('.norecords').remove();
            $('#<%= txtListSearch.ClientID %>').focus();
        }
        //        $("[id*=chkHeder1]").live("click", function() {
        //            var chkHeader = $(this);
        //            var grid = $(this).closest("table");
        //            $("input[type=checkbox]", grid).each(function() {
        //                if (chkHeader.is(":checked")) {

        //                    if ($(this).is(":visible")) {
        //                        $(this).attr("checked", "checked");
        //                    }
        //                } else {
        //                    $(this).removeAttr("checked");
        //                }
        //            });
        //        });

        //        $("[id*=chkbox1]").live("click", function() {
        //            var grid = $(this).closest("table");
        //            var chkHeader = $("[id*=chkHeader]", grid);
        //            var row = $(this).closest("tr")[0];
        //            if (!$(this).is(":checked")) {
        //                var row = $(this).closest("tr")[0];
        //                chkHeader.removeAttr("checked");

        //            } else {

        //                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
        //                    chkHeader.attr("checked", "checked");
        //                }
        //            }

        //        });

        $("body").on("click", "[id*=chkHeder1]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=chkbox1]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=chkbox1]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=chkHeder1]", grid);
            debugger;
            if ($("[id*=chkbox1]", grid).length == $("[id*=chkbox1]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });

        function iSubmitOpr() {
            var txtvalue = document.getElementById("<%=txtICalibrationNo.ClientID%>");
            if (txtvalue.disabled == false) {
                if (txtvalue.value == '') {
                    //alert('Sorry, Calibration number is mandatory information. Please provide calibration number to continue.');                   
                    swal({ title: '', text: "Sorry, Calibration number is mandatory information. Please provide calibration number to continue." });
                    return;
                }
                if (txtvalue.value != '') {
                    PageMethods.IsNumberExists(txtvalue.value, onSuccess, onFailure);
                    function onSuccess(str) {
                        if (str == "1") {
                            //alert('Sorry, This calibration number is already exists. Please provide another calibration number to continue.');                            
                            swal({ title: '', text: "Sorry, This calibration number is already exists. Please provide another calibration number to continue." });
                            return;
                        }
                    }

                    function onFailure(err) {
                        //alert(err.get_message());
                        swal({ title: '', text: err.get_message() });
                        return;
                    }
                }
            }
            var grid = document.getElementById("<%=gvApplyCalibration.ClientID%>");
            if (grid == null) {
                //alert('Sorry, No records found in order to perform selected operation.');
                swal({ title: '', text: "Sorry, No records found in order to perform selected operation." });
                return;
            }

            var msrv = '<%= Session("Mailserverip") %>';
            if (msrv == "") {
                swal({ title: "", type: "info", text: "Sorry, you have not configured, Email account setup under company registration. Please go to aruti configuration in oder to configure email setup." });
                return;
            }

            var flg = false;
            var table = document.getElementById("<%=gvApplyCalibration.ClientID%>");

            for (var i = 1, row; row = table.rows[i]; i++) {
                for (var j = 1, col; col = row.cells[j]; j++) {
                    if ($(col).attr('Id') != undefined && $(col).attr('Id') == "cRate") {
                        if ($(col).html() == '&nbsp;') {
                            flg = true;
                            break;
                        }
                    }
                }
                if (flg == true) { break; }
            }
            if (flg == true) {
                //alert('Sorry, you have not calibrate some of the employee(s). Please calibrated them in order to perform submit operation?');
                swal({ title: '', text: "Sorry, you have not calibrate some of the employee(s). Please calibrated them in order to perform submit operation." });
                return;
            }

            var iMsg = '';
            if (flg == false) {
                var icode = 0;
                iMsg = '<div style="height:200px; width:100%;overflow-y: scroll;">';
                for (var i = 1, row; row = table.rows[i]; i++) {
                    iMsg += '<div class="row2">';
                    for (var j = 1, col; col = row.cells[j]; j++) {
                        if ($(col).attr('Id') != undefined && $(col).attr('Id') == "mCode") {
                            if (parseInt($(col).html()) != 0) {
                                flg = true;
                                iMsg += '<div style="width: 46%" class="ib">' + row.cells[1].innerHTML + ' - ' + row.cells[2].innerHTML + '</div>';
                                switch (parseInt($(col).html())) {
                                    case 1:
                                        iMsg += '<div style="width: 50%" class="ib">Approver Not Assigned</div>';
                                        break;
                                    case 2:
                                        iMsg += '<div style="width: 50%" class="ib">Email not Assigned to Approver</div>';
                                        break;
                                }
                            }
                        }
                    }
                    iMsg += '</div>';
                }
            }
            if (flg == true) {
                swal({ html: true, type: "error", title: 'Information Missing', text: iMsg, customClass: 'swal-wide' });
                return;
            }

            if (flg == false) {
                for (var i = 1, row; row = table.rows[i]; i++) {
                    for (var j = 1, col; col = row.cells[j]; j++) {
                        if ($(col).attr('Id') != undefined && $(col).attr('Id') == "cRem") {
                            if ($(col).html() == '&nbsp;') {
                                flg = true;
                                break;
                            }
                        }
                    }
                    if (flg == true) { break; }
                }
                if (flg == true) {
                    //                    var yes = confirm('You have not set calibration remark for some of employee. Do you wish to continue with submit operation.');
                    //                    if (yes == true) {

                    //                    }
                    //                    else {
                    //                        return;
                    //                    }

                    swal({
                        title: '',
                        text: "You have not set calibration remark for some of employee. Do you wish to continue with submit operation?",
                        //type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#3085d6",
                        confirmButtonText: "Yes",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }, function(isConfirm) {
                        if (isConfirm) {
                            swal({
                                title: '',
                                text: "Enter Submission Remark",
                                type: "input",
                                showCancelButton: false,
                                closeOnConfirm: false,
                                animation: "slide-from-top",
                                inputPlaceholder: "Submit Remark"
                            }, function(inputValue) {
                                if (inputValue === false) return false;

                                if (inputValue === "") {
                                    swal.showInputError("Please provide remark in order to submit.");
                                    return false;
                                }
                                var remark = inputValue;
                                var arr = [];
                                var pid = document.getElementById("<%=cboSPeriod.ClientID%>");
                                $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                                    var guid = $(this).closest("tr").find("td[Id='trgid']").text();
                                    arr.push(guid);
                                });
                                var coyid = '<%= Session("CompanyUnkId") %>';
                                var ipadd = '<%= Session("IP_ADD") %>';
                                var hstname = '<%= Session("HOST_NAME") %>';
                                var calusrid = '<%= Session("UserId") %>';
                                var sndradd = '<%= Session("Senderaddress") %>';

                                swal({ title: '', type: "info", text: "Performing selected operation and sending notification(s), Please wait for a while. This dailog will close in some time.", showCancelButton: false, showConfirmButton: false });

                                PageMethods.CalibrationSubmission(coyid, $(pid).val(), calusrid, arr, txtvalue.value, pid.options[pid.selectedIndex].text, remark, "frmScoreCalibrationAddEdit", sndradd, ipadd, hstname, onSuccess, onFailure);

                                function onSuccess(str) {
                                    if (str == "1") {
                                        $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
                                        $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
                                        swal({ title: '', type: "success", text: "Information submitted successfully." });
                                    }
                                    else if (str == "0") {
                                        swal({ title: '', type: "warning", text: "Problem in submitting calibration." });
                                    }
                                }

                                function onFailure(err) {

                                    swal({ title: '', text: err.get_message() });
                                    return;
                                }
                                //swal("Nice!", "You wrote: " + inputValue, "success");
                            });
                        }
                    });
                    return;
                }
                else {
                    swal({
                        title: '',
                        text: "Enter Submission Remark",
                        type: "input",
                        showCancelButton: false,
                        closeOnConfirm: false,
                        animation: "slide-from-top",
                        inputPlaceholder: "Submit Remark"
                    }, function(inputValue) {
                        if (inputValue === false) return false;

                        if (inputValue === "") {
                            swal.showInputError("Please provide remark in order to submit.");
                            return false;
                        }
                        var remark = inputValue;
                        var arr = [];
                        var pid = document.getElementById("<%=cboSPeriod.ClientID%>");
                        $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                            var guid = $(this).closest("tr").find("td[Id='trgid']").text();
                            arr.push(guid);
                        });
                        var coyid = '<%= Session("CompanyUnkId") %>';
                        var ipadd = '<%= Session("IP_ADD") %>';
                        var hstname = '<%= Session("HOST_NAME") %>';
                        var calusrid = '<%= Session("UserId") %>';
                        var sndradd = '<%= Session("Senderaddress") %>';

                        swal({ title: '', type: "info", text: "Sending Notification(s), Please wait for a while. This dailog will close in some time.", showCancelButton: false, showConfirmButton: false });

                        PageMethods.CalibrationSubmission(coyid, $(pid).val(), calusrid, arr, txtvalue.value, pid.options[pid.selectedIndex].text, remark, "frmScoreCalibrationAddEdit", sndradd, ipadd, hstname, onSuccess, onFailure);
                        function onSuccess(str) {
                            if (str == "1") {
                                $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
                                $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
                                swal({ title: '', type: "success", text: "Information submitted successfully." });
                            }
                            else if (str == "0") {
                                swal({ title: '', type: "warning", text: "Problem in submitting calibration." });
                            }
                        }

                        function onFailure(err) {

                            swal({ title: '', text: err.get_message() });
                            return;
                        }
                        //swal("Nice!", "You wrote: " + inputValue, "success");
                    });
                }
            }
        }

        function RatingOperation(ival) {
            var arr = [];
            var ratingid = 0;
            var pid = document.getElementById("<%=cboSPeriod.ClientID%>");
            var rid = document.getElementById("<%=cbocRating.ClientID %>");
            if (ival == 1) {
                var rid = document.getElementById("<%=cbocRating.ClientID%>");
                ratingid = parseInt($(rid).val());
                if (ratingid <= 0) {
                    //alert('Sorry, Please select rating in order to perform calibrate operation.');
                    swal({ title: '', text: "Sorry, Please select rating in order to perform calibrate operation." });
                    return;
                }
            }
            var grid = document.getElementById("<%=gvApplyCalibration.ClientID%>");
            if (grid == null) {
                //alert('Sorry, No records found in order to perform selected operation.');
                swal({ title: '', text: "Sorry, No records found in order to perform selected operation." });
                return;
            }
            var txt = document.getElementById("<%=txtIRemark.ClientID%>").value;
            $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                if ($(this).is(':checked')) {
                    var guid = $(this).closest("tr").find("td[Id='trgid']").text();
                    arr.push(guid);
                }
            });
            if (arr == null) {
                //alert('Sorry, Please check atleast one information in order to calibrate score.');
                swal({ title: '', text: "Sorry, Please check atleast one information in order to calibrate score." });
                return;
            }
            if (arr != null && arr.length <= 0) {
                //alert('Sorry, Please check atleast one information in order to calibrate score.');
                swal({ title: '', text: "Sorry, Please check atleast one information in order to calibrate score." });
                return;
            }
            if (arr != null && arr.length > 0) {

                PageMethods.RatingUpdate(parseInt($(pid).val()), arr, txt, ratingid, onSuccess, onFailure);

                function onSuccess(str) {
                    if (str == "1") {
                        for (var k = 0; k < arr.length; k++) {
                            var table = document.getElementById("<%=gvApplyCalibration.ClientID%>");
                            for (var i = 1, row; row = table.rows[i]; i++) {
                                for (var j = 1, col; col = row.cells[j]; j++) {
                                    if ($(col).attr('Id') != undefined && $(col).attr('Id') == "trgid") {
                                        if ($(col).html() == arr[k]) {
                                            $(row).closest("tr").find("td[Id='cRem']").html(txt);
                                            if (ival == 1) { $(row).closest("tr").find("td[Id='cRate']").html(rid.options[rid.selectedIndex].innerHTML); }
                                            else { $(row).closest("tr").find("td[Id='cRate']").html($(row).closest("tr.griviewitem").find("td[Id='pRate']").text()); }
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                            if ($(this).is(':checked')) {
                                var grd = document.getElementById("<%=gvApplyCalibration.ClientID%>");
                                var tbod = grd.rows[0].parentNode;
                                var row = $(this).closest("tr");
                                var newRow = row[0].cloneNode(true);
                                tbod.append(newRow);
                                $(newRow).closest("tr")[0].style.color = "#0000FF"
                                $(this).closest("tr").remove();
                            }
                        });
                        $("#<%=gvApplyCalibration.ClientID%> input[id*='chkbox1']:checkbox").each(function(index) {
                            if ($(this).is(':checked')) {
                                $(this).removeAttr("checked");
                            }
                        });
                        //alert('Selected Operation performed successfully.');
                        swal({ title: '', text: "Selected Operation performed successfully." });
                    }
                }

                function onFailure(err) {
                    //alert(err.get_message());
                    swal({ title: '', text: err.get_message() });
                    return;
                }

            }
        }

        function GetSelected() {
            var ischkboxfound = false;
            var grid = document.getElementById("<%=gvFilterRating.ClientID%>");
            var checkBoxes = grid.getElementsByTagName("INPUT");
            $('#<%= gvApplyCalibration.ClientID %> tbody tr').hide();
            $('#<%= gvApplyCalibration.ClientID %> tbody tr:first').show();
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i] != undefined && checkBoxes[i].checked) {
                    var row = $(checkBoxes[i]).closest("tr").find("td:eq(1)").attr('title');
                    ischkboxfound = true;
                    var res = row.split(" - ");
                    RangeSearch(parseFloat(res[0]), parseFloat(res[1]));
                }
            }
            if (ischkboxfound == false) {
                $('#<%= gvApplyCalibration.ClientID %> tr').show();
            }
        }

        function RangeSearch(val1, val2) {
            var table = document.getElementById("<%=gvApplyCalibration.ClientID%>");
            for (var i = 1, row; row = table.rows[i]; i++) {
                for (var j = 1, col; col = row.cells[j]; j++) {
                    if ($(col).attr('Id') != undefined && $(col).attr('Id') == "pScore") {
                        if (parseFloat($(col).html()) >= val1 && parseFloat($(col).html()) <= val2) {
                            $(row).show();
                        }
                    }
                }
            }
        }

        //        function FillCalibNumn(drp) {
        //            debugger;
        //            var myindex = drp.selectedIndex;
        //            var SelValue = drp.options[myindex].value;
        //            cbno = document.getElementById("<%=cboRptCalibNo.ClientID %>");
        //            //cbno.empty();
        //            var calusrid = '<%= Session("UserId") %>';
        //            PageMethods.GetCalibrationNo(SelValue, calusrid, onSuccess);
        //            function onSuccess(response) {
        //                for (var i in response) {
        //                    if (response[i].Value == 0) {
        //                        cbno.append('<option selected="selected" value="' + response[i].Value + '">' + response[i].Text + '</option>');
        //                        cbno.nextSibling.append('<option title="' + response[i].Text + '" lang="' + i.toString() + '" selected="selected" value="' + response[i].Value + '">' + response[i].Text + '</option>');
        //                    }
        //                    else {
        //                        cbno.append('<option value="' + response[i].Value + '">' + response[i].Text + '</option>');
        //                        cbno.nextSibling.append('<option title="' + response[i].Text + '" lang="' + i.toString() + '" value="' + response[i].Value + '">' + response[i].Text + '</option>');
        //                    }
        //                    //cbno.append('<Option value=' + response[i].Value + '>' + response[i].Text + '</Option>')
        //                    //                    var option = document.createElement('<option value="' + response[i].Value + '">');
        //                    //                    countries.options.add(option);
        //                    //                    option.innerText = response[i].Text;
        //                }
        //            }

        //        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Score Calibration List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblBatchNo" runat="server" Text="Calibration No" CssClass="form-label" />
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtBatchNo" runat="server" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Period" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboPeriod" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblSubmitDateFrom" runat="server" Text="Submit From" CssClass="form-label" />
                                        <div class="form-group">
                                            <uc2:DateCtrl ID="dtDateFrom" runat="server" AutoPostBack="false" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <asp:Label ID="lblSubmitDateTo" runat="server" Text="To" CssClass="form-label" />
                                        <div class="form-group">
                                            <uc2:DateCtrl ID="dtDateTo" runat="server" AutoPostBack="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnShowAll" runat="server" Text="View Report" CssClass="btn btn-primary pull-left" />
                                <asp:Button ID="btnDisplayCurve" runat="server" Text="Display HPO Curve" CssClass="btn btn-primary pull-left" />
                                <asp:Button ID="btnImportCalibScore" runat="server" Text="Import Calib. Score" CssClass="btn btn-primary pull-left"
                                    Visible="false" />
                                <asp:Button ID="btnNew" runat="server" Text="Calibrate" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtClistSearch" runat="server" AutoPostBack="false" class="form-control"
                                                    onkeyup="ListSeaching();"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 250px">
                                            <asp:GridView ID="gvCalibrateList" DataKeyNames="grpid,employeeunkid,periodunkid,statusunkid,isgrp,submitdate,calibratescore,calibuser,allocation,ispgrp,pgrpid"
                                                runat="server" AutoGenerateColumns="false" Width="99%" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkedit" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                                                ToolTip="Edit" OnClick="lnkedit_Click"><i class="fas fa-pencil-alt text-primary" ></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%# Container.DataItemIndex %>'
                                                                ToolTip="Delete" OnClick="lnkdelete_Click"><i class="fas fa-trash text-danger"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgEmployee" />
                                                    <asp:BoundField DataField="povisionscore" HeaderText="Provision Score (%)" ItemStyle-HorizontalAlign="Right"
                                                        FooterText="dgpovision" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="100px" />
                                                    <asp:BoundField DataField="pRating" HeaderText="Prov. Rating" ItemStyle-HorizontalAlign="Right"
                                                        FooterText="dgpRating" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="100px" />
                                                    <asp:BoundField DataField="calibratescore" HeaderText="Calibrated Score" ItemStyle-HorizontalAlign="Right"
                                                        FooterText="dgcalibratescore" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="100px"
                                                        Visible="false" />
                                                    <asp:BoundField DataField="cRating" HeaderText="Calib. Rating" ItemStyle-HorizontalAlign="Right"
                                                        FooterText="dgcRating" HeaderStyle-HorizontalAlign="Right" HeaderStyle-Width="100px" />
                                                    <asp:BoundField DataField="c_remark" HeaderText="Calibration Remark" FooterText="dgcRemark" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupApproverUseraccess" BackgroundCssClass="modalBackground"
                    TargetControlID="lblCalibrateHeading" runat="server" PopupControlID="PanelApproverUseraccess"
                    CancelControlID="lblFilter" />
                <asp:Panel ID="PanelApproverUseraccess" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblCalibrateHeading" runat="server" Text="BSC Calibration"></asp:Label>
                        </h2>
                    </div>
                    <div class="body" style="height: 500px">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card inner-card">
                                    <div class="header">
                                        <h2>
                                            <asp:Label ID="lblFilter" runat="server" Text="Search Criteria" />
                                        </h2>
                                        <ul class="header-dropdown m-r-50 p-l-0" style="display: none">
                                            <asp:LinkButton ID="lnkAllocation" runat="server" Text="Allocation">
                                                                          <i class="fas fa-sliders-h"></i>
                                            </asp:LinkButton>
                                        </ul>
                                    </div>
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblSPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboSPeriod" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 m-t-25">
                                                <asp:Button ID="btnISearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnSReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblCalibrationNo" runat="server" Text="Calibration No." CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtICalibrationNo" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblCRating" runat="server" Text="Calibration Rating" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cbocRating" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 m-t-25">
                                                <asp:LinkButton ID="lnkCopyData" runat="server" Font-Underline="false" Font-Bold="true"
                                                    Text="Copy Rating" OnClientClick="RatingOperation(0); return false;" CssClass="form-label"></asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblIRemark" runat="server" Text="Calibration Remark" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtIRemark" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 m-t-25">
                                                <asp:Button ID="btnIApply" runat="server" Text="Apply" OnClientClick="RatingOperation(1); return false;"
                                                    CssClass="btn btn-primary" />
                                            </div>
                                        </div>
                                        <hr />
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblFilterData" runat="server" Text="Search" CssClass="form-label"
                                                    Visible="false"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtListSearch" runat="server" Width="99%" onkeyup="FromSearching();"
                                                            class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 170px">
                                                            <asp:GridView ID="gvFilterRating" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                Width="99%" AllowPaging="false" DataKeyNames="scrf,scrt,id">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                        FooterText="dgcolhchkbox">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkFRating" runat="server" Enabled="true" CssClass="filled-in"
                                                                                Text=" " CommandArgument='<%# Container.DataItemIndex %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="name" HeaderText="Filter By Prov. Rating" FooterText="dgcolhFRating" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Button ID="btnFilter" OnClientClick="GetSelected(); return false;" runat="server"
                                                            Text="Filter" CssClass="btn btn-primary" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 200px">
                                                            <asp:GridView ID="gvApplyCalibration" DataKeyNames="employeeunkid,periodunkid,calibratnounkid,calibrated_score,tranguid"
                                                                runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                                Width="99%" AllowPaging="false">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkHeder1" runat="server" Enabled="true" CssClass="filled-in" Text=" " />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkbox1" runat="server" Enabled="true" CommandArgument='<%# Container.DataItemIndex %>'
                                                                                CssClass="filled-in" Text=" " />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Code" HeaderText="Code" FooterText="dgcolhSCode" />
                                                                    <asp:BoundField DataField="EmployeeName" HeaderText="Employee" FooterText="dgcolhSEmployee" />
                                                                    <asp:BoundField DataField="JobTitle" HeaderText="Job Title" FooterText="dgcolhSJob" />
                                                                    <asp:BoundField DataField="Provision_Score" HeaderText="Provision Score (%)" ItemStyle-HorizontalAlign="Right"
                                                                        FooterText="dgcolhSProvision" />
                                                                    <asp:BoundField DataField="provRating" HeaderText="Prov. Rating" FooterText="dgcolhprovRating" />
                                                                    <asp:BoundField DataField="calibrated_score" HeaderText="Calibrated Score" ItemStyle-HorizontalAlign="Right"
                                                                        FooterText="dgcolhScalibratescore" Visible="false" />
                                                                    <asp:BoundField DataField="calibRating" HeaderText="Calib. Rating" FooterText="dgcolhcalibRating" />
                                                                    <asp:BoundField DataField="calibration_remark" HeaderText="Calibration Remark" FooterText="dgcolhcalibrationremark"
                                                                        ItemStyle-Wrap="true" />
                                                                    <asp:BoundField DataField="tranguid" HeaderText="" FooterText="objdgcolhtranguid" />
                                                                    <asp:BoundField DataField="MsgId" HeaderText="" FooterText="objMsgId" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnISubmit" runat="server" Text="Submit" CssClass="btn btn-primary"
                            OnClientClick="iSubmitOpr();return false;" />
                        <asp:Button ID="btnIClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <uc3:CV ID="valid" runat="server" ShowYesNo="false" />
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <uc9:ConfirmYesNo ID="cnfRemark" runat="server" />
                <uc2:ConfirmYesNo ID="cnfDelete" runat="server" />
                <ucDel:DeleteReason ID="delReason" runat="server" />
                <cc1:ModalPopupExtender ID="popupMyReport" BackgroundCssClass="modalBackground" TargetControlID="lblMyReport"
                    runat="server" PopupControlID="pnlMyReport" CancelControlID="lblMyReport" />
                <asp:Panel ID="pnlMyReport" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblMyReport" runat="server" Text="My Report"></asp:Label>
                        </h2>
                        <ul class="header-dropdown m-r-50 p-l-0">
                            <asp:LinkButton ID="lnkRptAdvFilter" Font-Underline="false" runat="server" ToolTip="Advance Filter"
                                Visible="false"><i class="fas fa-sliders-h"></i></asp:LinkButton>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <asp:Label ID="lblRptPeriod" runat="server" Text="Period" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboRptPeriod" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <asp:Label ID="lblRptCalibNo" runat="server" Text="Calibration No" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboRptCalibNo" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <asp:Label ID="lblRptEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboRptEmployee" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnRptShow" runat="server" Text="Show" CssClass="btn btn-primary" />
                        <asp:Button ID="btnRptReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                    </div>
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 325px">
                                            <asp:GridView ID="gvMyReport" DataKeyNames="grpid,employeeunkid,periodunkid,statusunkid,isgrp,userunkid,priority,submitdate,iStatusId,calibration_no,allocation,calibuser,total"
                                                runat="server" AutoGenerateColumns="false" Width="99%" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:BoundField DataField="Employee" HeaderText="Employee" FooterText="dgcolhMEmployee" />
                                                    <asp:BoundField DataField="lstpRating" HeaderText="Prov. Rating" FooterText="dgcolhMprovRating" />
                                                    <asp:BoundField DataField="acrating" HeaderText="Applied Rating" FooterText="dgcolhsacrating" />
                                                    <asp:BoundField DataField="acremark" HeaderText="Applied Remark" FooterText="dgcolhsacremark" />
                                                    <asp:BoundField DataField="lstapRating" HeaderText="Approver Calib. Rating" FooterText="dgcolhMcalibRating" />
                                                    <asp:BoundField DataField="apcalibremark" HeaderText="Calib. Remark" FooterText="dgcolhMcalibrationremark"
                                                        ItemStyle-Wrap="true" />
                                                    <asp:BoundField DataField="approvalremark" HeaderText="Approval Remark" FooterText="dgcolhsApprovalRemark" />
                                                    <asp:BoundField DataField="iStatus" HeaderText="Status" FooterText="dgcolhMiStatus" />
                                                    <asp:BoundField DataField="username" HeaderText="Approver" FooterText="dgcolhMusername" />
                                                    <asp:BoundField DataField="levelname" HeaderText="Level" FooterText="dgcolhMlevelname" />
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnExportMyReport" runat="server" Text="Export" CssClass="btn btn-primary pull-left" />
                                <asp:Button ID="btnCloseMyReport" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupHPOChart" BackgroundCssClass="modalBackground" TargetControlID="lblHPOCurver"
                    runat="server" PopupControlID="pnlHPOChart" CancelControlID="lblchPeriod" />
                <asp:Panel ID="pnlHPOChart" runat="server" CssClass="card modal-dialog modal-xlg"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblHPOCurver" runat="server" Text="HPO Chart"></asp:Label>
                        </h2>
                        <ul class="header-dropdown m-r-50 p-l-0">
                            <asp:LinkButton ID="lnkChAllocation" runat="server" Text="Allocation" Font-Underline="false"
                                Visible="false"><i class="fas fa-sliders-h"></i></asp:LinkButton>
                        </ul>
                        <asp:Label ID="lblChGenerate" runat="server" Text="Generate Criteria" Visible="false" />
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:Label ID="lblchPeriod" runat="server" Text="Period" CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="cboChPeriod" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:Label ID="lblChDisplayType" runat="server" Text="Display Type" CssClass="form-label" />
                                <div class="form-group">
                                    <asp:DropDownList ID="cbochDisplay" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnChGenerate" runat="server" Text="Generate" CssClass="btn btn-primary" />
                        <asp:Button ID="btnChReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                    </div>
                    <div class="row clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                            <div class="body" style="height: 330px">
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <div class="table-responsive" style="height: 165px">
                                            <asp:DataGrid ID="dgvCalibData" runat="server" Width="99%" AllowPaging="false" ShowHeader="false"
                                                CssClass="table table-hover table-bordered">
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-xs-12">
                                        <asp:Chart ID="chHpoCurve" runat="server" BorderlineColor="255, 128, 0" BorderlineDashStyle="Solid"
                                            BorderlineWidth="2" Width="917px" Style="margin-left: 7px" Height="300px" AntiAliasing="All">
                                            <Titles>
                                                <asp:Title Name="Title1">
                                                </asp:Title>
                                            </Titles>
                                            <Series>
                                                <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                                    MarkerStyle="Square" Name="Series1" BorderWidth="3">
                                                </asp:Series>
                                                <asp:Series ChartType="Spline" CustomProperties="LabelStyle=Top" MarkerSize="10"
                                                    MarkerStyle="Circle" Name="Series2" BorderWidth="3">
                                                </asp:Series>
                                            </Series>
                                            <ChartAreas>
                                                <asp:ChartArea Name="ChartArea1">
                                                </asp:ChartArea>
                                            </ChartAreas>
                                            <Legends>
                                                <asp:Legend Name="Legend1" Docking="Bottom" TableStyle="Tall" LegendStyle="Row">
                                                </asp:Legend>
                                            </Legends>
                                        </asp:Chart>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnChClose" runat="server" CssClass="btn btn-primary" Text="Close" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnExportMyReport" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
