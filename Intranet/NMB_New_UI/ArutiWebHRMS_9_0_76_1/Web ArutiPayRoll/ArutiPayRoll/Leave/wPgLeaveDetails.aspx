﻿<%@ Page Title="Leave Details" Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false"
    CodeFile="wPgLeaveDetails.aspx.vb" Inherits="wPgLeaveDetails" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Employee Leave Details"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="True" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblLeaveInfo" runat="server" Text="Leave Info"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:GridView ID="GvLeaveDetails" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:BoundField DataField="Leave" HeaderText="Leave Type" FooterText="dgcolhLvDetailLvType">
                                                        <HeaderStyle HorizontalAlign="Left" Width="150px" />
                                                        <ItemStyle HorizontalAlign="Left" Width="150px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Yearly Accrue Entitled" HeaderText="Yearly Accrue Entitled"
                                                        FooterText="dgcolhLvDetailLvAccrueEntitled">
                                                        <HeaderStyle HorizontalAlign="Right" Width="120px" />
                                                        <ItemStyle HorizontalAlign="Right" Width="120px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="StDate" HeaderText="Start Date" FooterText="dgcolhLvDetailLvStartDate">
                                                        <HeaderStyle HorizontalAlign="Left" Width="120px" />
                                                        <ItemStyle HorizontalAlign="Left" Width="120px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="EdDate" HeaderText="End Date" FooterText="dgcolhLvDetailLvEndDate">
                                                        <HeaderStyle HorizontalAlign="Left" Width="120px" />
                                                        <ItemStyle HorizontalAlign="Left" Width="120px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="LeaveAmountBF" HeaderText="Leave B/F" FooterText="dgcolhLvDetailLvLeaveBF">
                                                        <HeaderStyle HorizontalAlign="Right" Width="80px" />
                                                        <ItemStyle HorizontalAlign="Right" Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Acc_Amt" HeaderText="As On Accrued Days" FooterText="dgcolhLvDetailLvAsonAccrueDays">
                                                        <HeaderStyle HorizontalAlign="Right" Width="150px" />
                                                        <ItemStyle HorizontalAlign="Right" Width="150px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Iss_Amt" HeaderText="Issued Days" FooterText="dgcolhLvDetailLvIssueDays">
                                                        <HeaderStyle HorizontalAlign="Right" Width="90px" />
                                                        <ItemStyle HorizontalAlign="Right" Width="90px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="AdjustmentLeave" HeaderText="Leave Adjustment" FooterText="dgcolhLvDetailLvLeaveAdjustment">
                                                        <HeaderStyle HorizontalAlign="Right" Width="120px" />
                                                        <ItemStyle HorizontalAlign="Right" Width="120px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="balanceasondate" HeaderText="Balance as on date" FooterText="dgcolhLvDetailLvBalAsonDate">
                                                        <HeaderStyle HorizontalAlign="Right" Width="140px" />
                                                        <ItemStyle HorizontalAlign="Right" Width="140px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Balance" HeaderText="Total Balance" FooterText="dgcolhLvDetailLvTotalBalance">
                                                        <HeaderStyle HorizontalAlign="Right" Width="110px" />
                                                        <ItemStyle HorizontalAlign="Right" Width="110px" />
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
