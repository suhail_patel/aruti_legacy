﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_LeaveFormAddEdit.aspx.vb"
    Inherits="Leave_wPg_LeaveFormAddEdit" Title="Leave Form Add/Edit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="uc7" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="Confirmation" TagPrefix="ucCfnYesno" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());

            }
        }
    </script>

    <script type="text/javascript">
        function IsValidAttach() {
            var cboEmployee = $('#<%= cboEmployee.ClientID %>');

            if (parseInt($('select.cboScanDcoumentType')[0].value) <= 0) {
                alert('Please Select Document Type.');
                $('.cboScanDcoumentType').focus();
                return false;
            }

            if (parseInt(cboEmployee.val()) <= 0) {
                alert('Employee Name is compulsory information. Please select Employeee to continue.');
                cboEmployee.focus();
                return false;
            }
        }    
        
    </script>

    <script type="text/javascript">
        function onlyNumbers(txtBox, e) {
            if (window.event)
                var charCode = window.event.keyCode;       // IE
            else
                var charCode = e.which;

            var cval = txtBox.value;

            if (cval.length > 0)
                if (charCode == 46)
                if (cval.indexOf(".") > -1)
                return false;

            if (charCode == 45)
                return false;

            if (charCode == 13)
                return false;

            if (charCode == 47) // '/' Not Allow Sign 
                return false;

            if (charCode > 31 && (charCode < 45 || charCode > 57))
                return false;
            return true;
        }    
    </script>

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(endRequestHandler);
        function endRequestHandler(sender, evemt) {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
            $("#<%= popup_YesNo.ClientID %>_Panel1").css("z-index", "100005");
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Add / Edit Leave Form"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Add / Edit Leave Form"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" CssClass="mandatory_field form-label"
                                            Text="Employee"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLeaveType" runat="server" CssClass="mandatory_field form-label"
                                            Text="Leave Type"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLeaveType" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStartDate" runat="server" CssClass="mandatory_field form-label"
                                            Text="Start Date:"></asp:Label>
                                        <uc2:DateCtrl ID="dtpStartDate" runat="server" AutoPostBack="True" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFormNo" runat="server" Text="Application No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtFormNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApplyDate" runat="server" Text="Apply Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpApplyDate" runat="server" AutoPostBack="True" />
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblReturnDate" runat="server" Text="End Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpEndDate" runat="server" AutoPostBack="True" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblReliever" runat="server" CssClass="mandatory_field form-label"
                                            Text="Reliever"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboReliever" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                            <asp:Label ID="lblDays" runat="server" Text="Days to Apply" CssClass="form-label"></asp:Label>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                            <asp:Label ID="objNoofDays" runat="server" Text="0.00" CssClass="form-label"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                            <asp:Label ID="lblLeaveAccrue" runat="server" Text="Total Leave Accrue" CssClass="form-label"></asp:Label>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                            <asp:Label ID="objLeaveAccrue" runat="server" Text="0.00" CssClass="form-label"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                            <asp:Label ID="LblAsonDateAccrue" runat="server" Text="As On Date Leave Accrue" CssClass="form-label"></asp:Label>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                            <asp:Label ID="objAsonDateAccrue" runat="server" Text="0.00" CssClass="form-label"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                            <asp:Label ID="lblBalance" runat="server" Text="Total Leave Balance" CssClass="form-label"></asp:Label>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                            <asp:Label ID="objBalance" runat="server" Text="0.00" CssClass="form-label"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                            <asp:Label ID="lblAsonDateBalance" runat="server" Text="As On Date Leave Balance"
                                                CssClass="form-label"></asp:Label>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                            <asp:Label ID="objAsonDateBalance" runat="server" Text="0.00" CssClass="form-label"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <asp:Label ID="LblELCStart" runat="server" CssClass="form-label"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:LinkButton ID="lnkScanDocuements" runat="server" Visible="false" Font-Underline="false"
                                                Text="Browse" Style="padding-right: 20px;" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:LinkButton ID="lnkLeaveDayCount" runat="server" Font-Underline="false" Text="Leave Day Fraction"
                                                Style="padding-right: 20px;" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <asp:LinkButton ID="lvLeaveExpense" runat="server" Font-Underline="false" Text="Leave Expense" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLeaveAddress" runat="server" Text="Address and Telephone Number While On leave"
                                            CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtLeaveAddress" runat="server" Rows="2" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblRemarks" runat="server" Text="Reason" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtLeaveReason" runat="server" Rows="2" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />
                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                    <uc7:Confirmation ID="ExpenseConfirmation" Title="Confirmation" runat="server" />
                    <uc7:Confirmation ID="LeaveFormConfirmation" runat="server" Title="Confirmation" />
                    <uc7:Confirmation ID="FormDatesConfirmation" Title="Confirmation" runat="server" />
                </div>
                <cc1:ModalPopupExtender ID="popupFraction" BackgroundCssClass="modal-backdrop" TargetControlID="LblFraction"
                    runat="server" PopupControlID="pnlFractionPopup" CancelControlID="btnFractionCancel">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlFractionPopup" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblFraction" Text="Leave Day Count" runat="server"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 375px;">
                                    <asp:GridView ID="dgFraction" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:CommandField ShowEditButton="true" ItemStyle-HorizontalAlign="Center" ControlStyle-Font-Underline="false" ItemStyle-Width = "100px"  />
                                            <asp:BoundField DataField="leavedate" HeaderText="Date" ReadOnly="True" FooterText="dgColhDate" />
                                            <asp:BoundField DataField="dayfraction" HeaderText="Fractions" ItemStyle-HorizontalAlign="Right"
                                                HeaderStyle-HorizontalAlign="Right" FooterText="dgcolhFraction" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnFractionOK" runat="server" CssClass="btn btn-primary" Text="Ok" />
                        <asp:Button ID="btnFractionCancel" runat="server" CssClass="btn btn-default" Text="Cancel" />
                    </div>
                </asp:Panel>
                
                <cc1:ModalPopupExtender ID="popupExpense" runat="server" BackgroundCssClass="modal-backdrop"
                    TargetControlID="hdf_Claim" PopupControlID="pnlExpensePopup" CancelControlID="hdf_Claim">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlExpensePopup" runat="server" Style="display: none;" CssClass="card modal-dialog modal-lg">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblpopupHeader" runat="server" Text="Claim & Request"></asp:Label>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <asp:LinkButton ID="lnkViewDependants" runat="server" ToolTip="View Depedents List">
                                              <i class="fas fa-restroom"></i>
                            </asp:LinkButton>
                        </ul>
                    </div>
                    <div class="body" style="height: 525px;">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card inner-card">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblExpCategory" runat="server" Text="Exp.Cat." CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboExpCategory" runat="server" AutoPostBack="true" />
                                                                </div>
                                                                <asp:Label ID="lblPeriod" runat="server" Visible="false" Text="Period" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboPeriod" runat="server" Visible="false" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblClaimNo" runat="server" Text="Claim No" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtClaimNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                                                <uc2:DateCtrl ID="dtpDate" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblExpEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboExpEmployee" runat="server" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblExpLeaveType" runat="server" Text="Leave Type" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboExpLeaveType" runat="server" AutoPostBack="true" Enabled="False" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="objlblValue" runat="server" Text="Leave Form" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboReference" runat="server" AutoPostBack="false" Enabled="False" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3"
                                                                            ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                <div class="card inner-card">
                                                    <div class="body">
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblExpense" runat="server" Text="Expense" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblUoM" runat="server" Text="UoM Type" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtUoMType" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblSector" runat="server" Text="Sector/Route" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblQty" runat="server" Text="Qty" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtQty" runat="server" CssClass="form-control" Text="0" AutoPostBack="true"
                                                                            Style="text-align: right" onKeypress="return onlyNumbers(this,event)"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboCostCenter" runat="server" AutoPostBack="true" />
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price" CssClass="form-label"></asp:Label>
                                                                <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                        <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="form-control" Text="1.00"
                                                                            Style="text-align: right" onKeypress="return onlyNumbers(this,event)"></asp:TextBox>
                                                                        <asp:TextBox ID="txtCosting" runat="server" CssClass="form-control" Text="0.00" Enabled="false"
                                                                            Visible="false"></asp:TextBox>
                                                                        <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                                <asp:Panel ID="pnlBalAsonDate" runat="server">
                                                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 m-l--15">
                                                                        <asp:Label ID="lblBalanceasondate" runat="server" Text="Balance As on Date" CssClass="form-label"></asp:Label>
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtBalanceAsOnDate" runat="server" CssClass="form-control" Enabled="false"
                                                                                    Style="text-align: right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 m-l--15">
                                                                        <asp:Label ID="lblExpBalance" runat="server" Text="Balance" CssClass="form-label"></asp:Label>
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtBalance" runat="server" CssClass="form-control" Enabled="false"
                                                                                    Style="text-align: right"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                                <asp:Label ID="LblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                                                <div class="form-group">
                                                                    <asp:DropDownList ID="cboCurrency" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row clearfix">
                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                                    <li role="presentation" class="active"><a href="#tbpExpenseRemark" data-toggle="tab">
                                                                        <asp:Label ID="tbExpenseRemark" runat="server" Text="Expense Remark" CssClass="form-label" />
                                                                    </a></li>
                                                                    <li role="presentation"><a href="#tbpClaimRemark" data-toggle="tab">
                                                                        <asp:Label ID="tbClaimRemark" runat="server" Text="Claim Remark" CssClass="form-label" />
                                                                    </a></li>
                                                                </ul>
                                                                <!-- Tab panes -->
                                                                <div class="tab-content">
                                                                    <div role="tabpanel" class="tab-pane fade in active" id="tbpExpenseRemark">
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtExpRemark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div role="tabpanel" class="tab-pane fade" id="tbpClaimRemark">
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                <asp:TextBox ID="txtClaimRemark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                                        <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="btn btn-primary" Visible="false" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="card inner-card">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                                <div class="table-responsive" style="height: 170px">
                                                    <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                        AllowPaging="false" CssClass="table table-hover table-bordered">
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="brnEdit">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="imgEdit" runat="server" ToolTip="Edit" CommandName="Edit">
                                                                                          <i class="fas fa-pencil-alt text-primary"></i>
                                                                        </asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                                FooterText="btnDelete">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <span class="gridiconbc">
                                                                        <asp:LinkButton ID="ImgDelete" runat="server" ToolTip="Delete" CommandName="Delete">
                                                                                         <i class="fas fa-trash text-danger"></i>
                                                                        </asp:LinkButton>
                                                                    </span>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn FooterText="objcolhAttachment" HeaderStyle-HorizontalAlign="Center"
                                                                HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="imgView" runat="server" CommandName="attachment" ToolTip="Attachment">
                                                                                <i class="fa fa-paperclip" aria-hidden="true" style="font-size:20px;font-weight:bold"></i>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:BoundColumn DataField="expense" HeaderText="Claim/Expense Desc" ReadOnly="true"
                                                                FooterText="dgcolhExpense" />
                                                            <asp:BoundColumn DataField="sector" HeaderText="Sector / Route" ReadOnly="true" FooterText="dgcolhSectorRoute" />
                                                            <asp:BoundColumn DataField="uom" HeaderText="UoM" ReadOnly="true" FooterText="dgcolhUoM" />
                                                            <asp:BoundColumn DataField="quantity" HeaderText="Quantity" ReadOnly="true" FooterText="dgcolhQty"
                                                                ItemStyle-CssClass="txttextalignright" />
                                                            <asp:BoundColumn DataField="unitprice" HeaderText="Unit Price" ReadOnly="true" FooterText="dgcolhUnitPrice"
                                                                ItemStyle-CssClass="txttextalignright" />
                                                            <asp:BoundColumn DataField="amount" HeaderText="Amount" ReadOnly="true" FooterText="dgcolhAmount"
                                                                ItemStyle-CssClass="txttextalignright" />
                                                            <asp:BoundColumn DataField="expense_remark" HeaderText="Expense Remark" ReadOnly="true"
                                                                Visible="true" FooterText="dgcolhExpenseRemark" />
                                                            <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" ReadOnly="true"
                                                                Visible="false" />
                                                            <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" ReadOnly="true" Visible="false" />
                                                        </Columns>
                                                        <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                            Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                                    </asp:DataGrid>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row clearfix">
                                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total" CssClass="form-label"></asp:Label>
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtGrandTotal" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <asp:HiddenField ID="hdf_Claim" runat="server" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <asp:Button ID="btnScanAttchment" runat="server" Visible="false" Text="Scan/Attchment"
                                                    CssClass="btndefault" />
                                                <asp:Button ID="btnSaveAddEdit" runat="server" Text="Save" CssClass="btn btn-primary"
                                                    ValidationGroup="Step" />
                                                <asp:Button ID="btnCloseAddEdit" runat="server" Text="Close" CssClass="btn btn-default"
                                                    CausesValidation="false" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <uc7:Confirmation ID="popup_UnitPriceYesNo" runat="server" Title="Confirmation" />
                        <uc7:Confirmation ID="popup_ExpRemarkYesNo" runat="server" Title="Confirmation" />
                </asp:Panel>
                
                <cc1:ModalPopupExtender ID="popupEmpDepedents" runat="server" BackgroundCssClass="modal-backdrop"
                    TargetControlID="btnEmppnlEmpDepedentsClose" PopupControlID="pnlEmpDepedents"
                    CancelControlID="btnEmppnlEmpDepedentsClose">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlEmpDepedents" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblEmpDependentsList" runat="server" Text="Dependents List" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 250px;">
                                    <asp:DataGrid ID="dgDepedent" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:BoundColumn DataField="dependants" HeaderText="Name" ReadOnly="true" FooterText="dgcolhName" />
                                            <asp:BoundColumn DataField="gender" HeaderText="Gender" ReadOnly="true" FooterText="dgcolhGender" />
                                            <asp:BoundColumn DataField="age" HeaderText="Age" ReadOnly="true" FooterText="dgcolhAge" />
                                            <asp:BoundColumn DataField="Months" HeaderText="Month" ReadOnly="true" FooterText="dgcolhMonth" />
                                            <asp:BoundColumn DataField="relation" HeaderText="Relation" ReadOnly="true" FooterText="dgcolhRelation" />
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnEmppnlEmpDepedentsClose" runat="server" Text="Cancel" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hdnEmpDepedents" runat="server" />
                    </div>
                </asp:Panel>
                
                <cc1:ModalPopupExtender ID="popupClaim" runat="server" BackgroundCssClass="modal-backdrop"
                    TargetControlID="hdf_ClaimDelete" PopupControlID="pnl_popupClaimDelete" CancelControlID="btnDelReasonNo">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_popupClaimDelete" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblClaimDelete" runat="server" Text="Aruti"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblClaimTitle" runat="server" Text="Are you sure you want to delete selected transaction?"
                                    CssClass="form-label"></asp:Label>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <asp:TextBox ID="txtClaimDelReason" runat="server" TextMode="MultiLine" CssClass="form-control" />
                                    </div>
                                </div>
                                <asp:RequiredFieldValidator ID="rqfv_txtClaimDelReason" runat="server" Display="None"
                                    ControlToValidate="txtClaimDelReason" ErrorMessage="Delete Reason can not be blank. "
                                    CssClass="ErrorControl" ForeColor="White" Style="" SetFocusOnError="True" ValidationGroup="ClaimDelete"></asp:RequiredFieldValidator>
                                <cc1:ValidatorCalloutExtender runat="Server" ID="PNReqE" TargetControlID="rqfv_txtClaimDelReason" />
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnDelReasonYes" runat="server" Text="Yes" CssClass="btn btn-primary"
                            ValidationGroup="ClaimDelete" />
                        <asp:Button ID="btnDelReasonNo" runat="server" Text="No" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_ClaimDelete" runat="server" />
                    </div>
                </asp:Panel>
                
                <cc1:ModalPopupExtender ID="popup_ScanAttchment" runat="server" BackgroundCssClass="modal-backdrop"
                    TargetControlID="hdf_ScanAttchment" PopupControlID="pnl_ScanAttchment" CancelControlID="hdf_ScanAttchment">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnl_ScanAttchment" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="lblScanHeader" runat="server" Text="Scan/Attchment"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <asp:Label ID="lblScanDocumentType" runat="server" Text="Document Type" CssClass="form-label"></asp:Label>
                                <div class="form-group">
                                    <asp:DropDownList ID="cboScanDcoumentType" CssClass="cboScanDcoumentType" runat="server" />
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4  m-t-25">
                                <div id="fileuploader">
                                    <input type="button" id="btnAddFile" runat="server" class="btn btn-primary" value="Add" />
                                </div>
                                <asp:Button ID="btnSaveAttachment" runat="server" Style="display: none" OnClick="btnSaveAttachment_Click"
                                    Text="Browse" CssClass="btn btn-primary" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 150px;">
                                    <asp:DataGrid ID="dgv_Attchment" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:TemplateColumn FooterText="objcohDelete" HeaderStyle-Width="23px">
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="DeleteImg" runat="server" CssClass="griddelete" CommandName="Delete"
                                                            ToolTip="Delete">
                                                                    <i class="fas fa-trash text-danger"></i>    
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn FooterText="objcolhDownload" HeaderStyle-Width="23px">
                                                <ItemTemplate>
                                                    <span class="gridiconbc">
                                                        <asp:LinkButton ID="colhDownload" runat="server" CommandName="Download" ToolTip="Download">
                                                                        <i class="fa fa-download"></i>
                                                        </asp:LinkButton>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn HeaderText="File Name" DataField="filename" FooterText="colhName" />
                                            <asp:BoundColumn DataField="GUID" FooterText="objcolhGUID" Visible="false" />
                                            <asp:BoundColumn DataField="scanattachtranunkid" FooterText="objcolhScanUnkId" Visible="false" />
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnDownloadAll" runat="server" Text="Download All" CssClass="btn btn-primary" />
                        <asp:Button ID="btnScanSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnScanClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        <asp:HiddenField ID="hdf_ScanAttchment" runat="server" />
                    </div>
                </asp:Panel>
                
                <ucCfnYesno:Confirmation ID="popup_YesNo" runat="server" Message="" Title="Confirmation" />
                <uc7:Confirmation ID="P2PLvFormDeleteConfirmation" Title="Confirmation" runat="server"
                    IsFireButtonNoClick="true" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="dgv_Attchment" />
                <asp:PostBackTrigger ControlID="btnDownloadAll" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
        $(document).ready(function() {
            ImageLoad();
            $(".ajax-upload-dragdrop").css("width", "auto");
        });
        function ImageLoad() {
            if ($(".ajax-upload-dragdrop").length <= 0) {
                $("#fileuploader").uploadFile({
                    url: "wPg_LeaveFormAddEdit.aspx?uploadimage=mSEfU19VPc4=",
                    multiple: false,
                    method: "POST",
                    dragDropStr: "",
                    maxFileSize: 1024 * 1024,
                    showStatusAfterSuccess: false,
                    showAbort: false,
                    sizeErrorStr: "sorry,you can not attch file more than 1MB.",
                    showDone: false,
                    fileName: "myfile",
                    onSuccess: function(path, data, xhr) {
                        $("#<%= btnSaveAttachment.ClientID %>").click();
                    },
                    onError: function(files, status, errMsg) {
                        alert(errMsg);
                    }
                });
            }
        }
        $("body").on("click", "input[type=file]", function() {
            return IsValidAttach();
        });
    </script>

</asp:Content>
