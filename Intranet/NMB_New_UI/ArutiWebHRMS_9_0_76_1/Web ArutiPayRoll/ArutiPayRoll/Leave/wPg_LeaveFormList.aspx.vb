﻿Option Strict On

Imports Aruti.Data
Imports System.Data
Imports System.Globalization
Imports System.IO


Partial Class Leave_wPg_LeaveFormList
    Inherits Basepage

#Region "Private Variable"
    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleName As String = "frmLeaveFormList"
    Private Shared ReadOnly mstrModuleName4 As String = "frmCancel_leaveform"


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objapprover As New clsleaveapprover_master
    'Private objEmployee As New clsEmployee_Master
    'Private clsleavetype As New clsleavetype_master
    'Private clsstatus As New clsMasterData
    'Private clsLeaveForm As New clsleaveform
    'Private objClaimRequestMst As New clsclaim_request_master
    'Private objClaimTran As New clsclaim_request_tran
    'Private objExpApproverTran As New clsclaim_request_approval_tran
    'Pinkal (11-Sep-2020) -- End

    Private mintFormUnkId As Integer = -1
    Private mintTotalIssuedDays As Integer = 0
    Private mintTotalCanceldays As Integer = 0
    Private mintCancelFormId As Integer = 0
    Private mintCancelEmpUnkId As Integer = 0
    Private mintCancelLeaveTypeUnkId As Integer = 0
    Private mintClaimMstUnkId As Integer = 0
    Private mdtCanceExpTable As DataTable = Nothing
    Private mstrCancelClmRemark As String = ""


    'Pinkal (28-Nov-2017) -- Start
    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
    Dim mdtAttachement As DataTable = Nothing
    Dim mblnCancelLvFormPopup As Boolean = False
    Dim mblnCancelAttachmentPopup As Boolean = False
    Dim mintDeleteIndex As Integer = 0
    Dim mintLeaveTypeID As Integer = 0
    Dim mintLeaveApplicationEmpID As Integer = 0
    'Pinkal (28-Nov-2017) -- End

    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mblnLvTypeSkipApproverFlow As Boolean = False
    'Pinkal (01-Oct-2018) -- End

#End Region

#Region "Page Event(S)"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If



            If IsPostBack = False Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (11-Sep-2020) -- End

                SetLanguage()

                Call FillCombo()

                If CInt(Session("Employeeunkid")) > 0 Then

                    'Pinkal (03-May-2019) -- Start
                    'Enhancement - Working on Leave UAT Changes for NMB.
                    'dgView.Columns(1).Visible = True    'FOR DELETE LEAVE FORM
                    dgView.Columns(0).Visible = CBool(Session("AllowToEditLeaveApplication"))    'FOR EDIT LEAVE FORM
                    dgView.Columns(1).Visible = CBool(Session("AllowToDeleteLeaveApplication"))    'FOR DELETE LEAVE FORM
                    'Pinkal (03-May-2019) -- End

                    dgView.Columns(2).Visible = False   'FOR CANCEL LEAVE FORM
                    dgView.Columns(3).Visible = False   'FOR PREVIEW LEAVE FORM
                    dgView.Columns(4).Visible = False   'FOR EMPLOYEE CODE
                    dgView.Columns(5).Visible = False   'FOR EMPLOYEE NAME
                    ''Varsha Rana (17-Oct-2017) -- Start
                    ''Enhancement - Give user privileges.
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    BtnNew.Visible = CBool(Session("AddLeaveForm"))
                    ''Varsha Rana (17-Oct-2017) -- End
                End If
            Else
                mintFormUnkId = CInt(Me.ViewState("Formunkid"))
                mintTotalIssuedDays = CInt(Me.ViewState("TotalIssueDays"))
                mintTotalCanceldays = CInt(Me.ViewState("TotalCancelDays"))
                mintCancelFormId = CInt(Me.ViewState("CancelFormId"))
                mintCancelEmpUnkId = CInt(Me.ViewState("CancelEmpUnkId"))
                mintCancelLeaveTypeUnkId = CInt(Me.ViewState("CancelLeaveTypeUnkId"))
                mintClaimMstUnkId = CInt(Me.ViewState("ClaimMstUnkId"))
                mdtCanceExpTable = CType(Me.ViewState("CanceExpTable"), DataTable)
                mstrCancelClmRemark = CStr(Me.ViewState("CancelClmRemark"))

                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                mblnLvTypeSkipApproverFlow = CBool(Me.ViewState("LvTypeSkipApproverFlow"))
                'Pinkal (01-Oct-2018) -- End

                'Pinkal (28-Nov-2017) -- Start
                'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                mdtAttachement = CType(Me.ViewState("CancelAttachement"), DataTable)
                mblnCancelAttachmentPopup = Convert.ToBoolean(Me.ViewState("mblnCancelAttachmentPopup"))
                mblnCancelLvFormPopup = Convert.ToBoolean(Me.ViewState("mblnCancelLvFormPopup"))
                mintDeleteIndex = CInt(Me.ViewState("mintDeleteIndex"))
                mintLeaveTypeID = CInt(Me.ViewState("mintLeaveTypeID"))
                mintLeaveApplicationEmpID = CInt(Me.ViewState("mintLeaveApplicationEmpID"))


                If mblnCancelLvFormPopup = True Then
                    popupCancel.Show()
                End If

                'Pinkal (28-Nov-2017) -- End

            End If


            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .

            If mblnCancelAttachmentPopup Then
                popup_ScanAttchment.Show()
            End If

            If Request.QueryString.Count > 0 Then
                If Request.QueryString("uploadimage") IsNot Nothing Then
                    If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                        Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                        postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                        Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                    End If
                End If
                Exit Sub
            End If

            'Pinkal (28-Nov-2017) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("Formunkid") = mintFormUnkId
            Me.ViewState("TotalIssueDays") = mintTotalIssuedDays
            Me.ViewState("TotalCancelDays") = mintTotalCanceldays
            Me.ViewState("CancelFormId") = mintCancelFormId
            Me.ViewState("CancelEmpUnkId") = mintCancelEmpUnkId
            Me.ViewState("CancelLeaveTypeUnkId") = mintCancelLeaveTypeUnkId
            Me.ViewState("ClaimMstUnkId") = mintClaimMstUnkId
            Me.ViewState("CanceExpTable") = mdtCanceExpTable
            Me.ViewState("CancelClmRemark") = mstrCancelClmRemark

            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            Me.ViewState("CancelAttachement") = mdtAttachement
            Me.ViewState("mblnCancelLvFormPopup") = mblnCancelLvFormPopup
            Me.ViewState("mblnCancelAttachmentPopup") = mblnCancelAttachmentPopup
            Me.ViewState("mintDeleteIndex") = mintDeleteIndex
            Me.ViewState("mintLeaveTypeID") = mintLeaveTypeID
            Me.ViewState("mintLeaveApplicationEmpID") = mintLeaveApplicationEmpID
            'Pinkal (28-Nov-2017) -- End

            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            Me.ViewState("LvTypeSkipApproverFlow") = mblnLvTypeSkipApproverFlow
            'Pinkal (01-Oct-2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "Private Funcation(S)"

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objEmployee As New clsEmployee_Master
        Dim clsleavetype As New clsleavetype_master
        Dim clsstatus As New clsMasterData
        'Pinkal (11-Sep-2020) -- End
        Try

            Dim blnApplyFilter As Boolean = True
            Dim blnSelect As Boolean = True
            Dim intEmpId As Integer = 0
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnSelect = False
                intEmpId = CInt(Session("Employeeunkid"))
                blnApplyFilter = False
            End If

            dsCombos = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                        CInt(Session("UserId")), _
                                        CInt(Session("Fin_year")), _
                                        CInt(Session("CompanyUnkId")), _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                        CStr(Session("UserAccessModeSetting")), True, _
                                        CBool(Session("IsIncludeInactiveEmp")), "Employee", blnSelect, intEmpId, , , , , , , , , , , , , , , , blnApplyFilter)

            With ddlFilEmpList
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombos.Tables(0)
                .DataBind()
                .SelectedIndex = 0
            End With


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                dsCombos = clsleavetype.getListForCombo("LeaveType", True, , Session("Database_Name").ToString, "", True)
            Else
                dsCombos = clsleavetype.getListForCombo("LeaveType", True, , Session("Database_Name").ToString, "", False)
            End If
            'Pinkal (25-May-2019) -- End
            With ddlfilLeave
                .DataSource = dsCombos.Tables(0)
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataBind()
                .SelectedIndex = 0
            End With


            'Pinkal (03-Jan-2020) -- Start
            'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
            'dsCombos = clsstatus.getLeaveStatusList("Cfcommon_master", True)
            dsCombos = clsstatus.getLeaveStatusList("Cfcommon_master", Session("ApplicableLeaveStatus").ToString(), True)
            'Pinkal (03-Jan-2020) -- End


            With status1
                .DataSource = dsCombos.Tables(0)
                .DataTextField = "name"
                .DataValueField = "statusunkid"
                .DataBind()
            End With


            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            dsCombos = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombos.Tables("List")
                .DataBind()
            End With
            'Pinkal (28-Nov-2017) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombos IsNot Nothing Then dsCombos.Clear()
            dsCombos = Nothing
            objEmployee = Nothing
            clsstatus = Nothing
            clsleavetype = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Public Sub FillList()
        Dim strSearching As String = ""
        Dim dtLeaveForm As DataTable = Nothing
        Dim dsLeaveForm As DataSet = Nothing
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim clsLeaveForm As New clsleaveform
        'Pinkal (11-Sep-2020) -- End
        Try

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                If CBool(Session("ViewLeaveFormList")) = False Then Exit Sub
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

            'dsLeaveForm = clsLeaveForm.GetList("LeaveForm", _
            '                                    CStr(Session("Database_Name")), _
            '                                    CInt(Session("UserId")), _
            '                                    CInt(Session("Fin_year")), _
            '                                    CInt(Session("CompanyUnkId")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                    CStr(Session("UserAccessModeSetting")), True, _
            '                                    CBool(Session("IsIncludeInactiveEmp")), True, -1, , CInt(Session("Employeeunkid")))

            If CInt(ddlFilEmpList.SelectedValue) > 0 Then
                strSearching += "AND lvleaveform.employeeunkid =" & CInt(ddlFilEmpList.SelectedValue) & " "
            End If

            If CInt(ddlfilLeave.SelectedValue) > 0 Then
                strSearching += "AND lvleaveform.leavetypeunkid =" & CInt(ddlfilLeave.SelectedValue) & " "
            End If

            If txtdrpformNo.Text.Trim.Length > 0 Then
                strSearching += "AND lvleaveform.FormNo like '%" & txtdrpformNo.Text.Trim & "%' "
            End If

            If dtfilApplyDate.IsNull = False Then
                strSearching += "AND lvleaveform.applydate='" & eZeeDate.convertDate(dtfilApplyDate.GetDate) & "' "
            End If

            If dtfilStartDate.IsNull = False Then
                strSearching += "AND lvleaveform.startdate >='" & eZeeDate.convertDate(dtfilStartDate.GetDate) & "' "
            End If

            If dtfilReturnDate.IsNull = False Then
                strSearching += "AND lvleaveform.returndate <='" & eZeeDate.convertDate(dtfilReturnDate.GetDate) & "' "
            End If


            If CInt(status1.SelectedValue) > 0 Then
                strSearching &= "AND lvleaveform.statusunkid = " & CInt(status1.SelectedValue)
            End If


            'If strSearching.Length > 0 Then
            '    strSearching = strSearching.Substring(3)
            '    dtLeaveForm = New DataView(dsLeaveForm.Tables("LeaveForm"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            'Else
            '    dtLeaveForm = dsLeaveForm.Tables("LeaveForm")
            'End If

            dsLeaveForm = clsLeaveForm.GetList("LeaveForm", _
                                              CStr(Session("Database_Name")), _
                                              CInt(Session("UserId")), _
                                              CInt(Session("Fin_year")), _
                                              CInt(Session("CompanyUnkId")), _
                                              eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                              CStr(Session("UserAccessModeSetting")), True, _
                                              CBool(Session("IsIncludeInactiveEmp")), True, -1, , CInt(Session("Employeeunkid")), strSearching)


            dtLeaveForm = dsLeaveForm.Tables("LeaveForm")

            'Pinkal (11-Sep-2020) -- End


            If dtLeaveForm IsNot Nothing Then
                dgView.DataSource = dtLeaveForm
                dgView.DataKeyField = "formunkid"
                dgView.DataBind()
            Else
                DisplayMessage.DisplayMessage("Error In Binddata method  ", Me)
                Exit Sub
            End If

            If (CBool(Session("AllowToCancelLeave")) = True Or CBool(Session("AllowToCancelPreviousDateLeave")) = True) Then
                dgView.Columns(2).Visible = True
            Else
                dgView.Columns(2).Visible = False
            End If


            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dgView.Columns(0).Visible = CBool(Session("EditLeaveForm"))
                dgView.Columns(1).Visible = CBool(Session("DeleteLeaveForm"))
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            If dtLeaveForm IsNot Nothing Then dtLeaveForm.Clear()
            dtLeaveForm = Nothing
            If dsLeaveForm IsNot Nothing Then dsLeaveForm.Clear()
            dsLeaveForm = Nothing
            clsLeaveForm = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try

    End Sub

    Public Sub delete()
        Try

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#Region "Leave Cancel"

    Private Sub GetLeaveDate(ByVal dsList As DataSet, ByVal intFormunkid As Integer)
        Try
            chkListLeavedate.Items.Clear()

            Dim objCancelForm As New clsCancel_Leaveform
            If dsList IsNot Nothing And dsList.Tables(0).Rows.Count > 0 Then
                mintTotalIssuedDays = dsList.Tables(0).Rows.Count
                Dim dsCancelList As DataSet = objCancelForm.GetList("List", intFormunkid)
                SetDateFormat()

                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1

                    Dim drRow As DataRow() = dsCancelList.Tables(0).Select("cancel_leavedate = '" & dsList.Tables(0).Rows(i)("leavedate").ToString() & "'")
                    If drRow.Length = 0 Then
                        Dim objPeriod As New clscommom_period_Tran
                        Dim objMaster As New clsMasterData

                        'Pinkal (30-May-2020) -- Start
                        'Enhancement NMB - Leave Cancellation Option put on Configuration and implementing on Leave Form List screen.

                        If (CBool(Session("IsArutiDemo")) = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management)) AndAlso CBool(Session("AllowToCancelLeaveForClosedPeriod")) = False Then
                            Dim intstPerioId As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, eZeeDate.convertDate(dsList.Tables(0).Rows(i)("leavedate").ToString()).Date, CInt(Session("Fin_year")), 0)
                            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = intstPerioId

                            If objPeriod._Statusid = 2 Then
                                'Pinkal (11-Sep-2020) -- Start
                                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                objPeriod = Nothing
                                objMaster = Nothing
                                'Pinkal (11-Sep-2020) -- End
                                Continue For
                            End If
                        End If


                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objPeriod = Nothing
                        objMaster = Nothing
                        'Pinkal (11-Sep-2020) -- End


                        'Pinkal (30-May-2020) -- End

                        If CBool(Session("AllowToCancelPreviousDateLeave")) = True AndAlso CBool(Session("AllowToCancelLeave")) = False Then
                            If eZeeDate.convertDate(dsList.Tables(0).Rows(i)("leavedate").ToString()).Date <= ConfigParameter._Object._CurrentDateAndTime.Date Then
                                chkListLeavedate.Items.Add(eZeeDate.convertDate(dsList.Tables(0).Rows(i)("leavedate").ToString()).Date.ToShortDateString())
                            End If
                        ElseIf CBool(Session("AllowToCancelPreviousDateLeave")) = False AndAlso CBool(Session("AllowToCancelLeave")) = True Then
                            If eZeeDate.convertDate(dsList.Tables(0).Rows(i)("leavedate").ToString()).Date >= ConfigParameter._Object._CurrentDateAndTime.Date Then
                                chkListLeavedate.Items.Add(eZeeDate.convertDate(dsList.Tables(0).Rows(i)("leavedate").ToString()).Date.ToShortDateString())
                            End If
                        ElseIf CBool(Session("AllowToCancelPreviousDateLeave")) = True AndAlso CBool(Session("AllowToCancelLeave")) = True Then
                            chkListLeavedate.Items.Add(eZeeDate.convertDate(dsList.Tables(0).Rows(i)("leavedate").ToString()).Date.ToShortDateString())
                        End If
                    Else
                        mintTotalCanceldays += 1
                    End If

                Next

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'dsCancelList.Dispose()
                If dsCancelList IsNot Nothing Then dsCancelList.Clear()
                dsCancelList = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objCancelForm = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            dsList.Dispose()
        End Try
    End Sub

    Private Sub Fill_CancelCombo()
        Try
            Dim dsCombo As New DataSet
            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List")
            With cboCancelExpCategory
                .DataSource = dsCombo
                .DataValueField = "id"
                .DataTextField = "name"
                .DataBind()
            End With

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_CancelCombo :" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    '  Private Sub GetCancelValue()
    Private Sub GetCancelValue(ByRef objClaimRequestMst As clsclaim_request_master)
        'Pinkal (11-Sep-2020) -- End
        Try
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objClaimRequestMst._Employeeunkid
            txtCanelExpEmployee.Text = objEmployee._Employeecode & " - " & objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
            txtCancelExpEmployeeid.Value = objClaimRequestMst._Employeeunkid.ToString
            txtCancelExpClaimNo.Text = objClaimRequestMst._Claimrequestno
            cboCancelExpCategory.SelectedValue = objClaimRequestMst._Expensetypeid.ToString
            dtpCancelExpDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            objClaimRequestMst._Transactiondate = dtpCancelExpDate.GetDate

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objEmployee = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetCancelValue : " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub Fill_CancelExpense()
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdt As DataTable = mdtCanceExpTable
            Dim mdt As DataTable = mdtCanceExpTable.Copy()
            'Pinkal (11-Sep-2020) -- End

            If mdt IsNot Nothing AndAlso mdt.Rows.Count > 0 Then
                If mdt.Columns.Contains("IsPosted") = False Then
                    mdt.Columns.Add("IsPosted", Type.GetType("System.Boolean"))
                    mdt.Columns("IsPosted").DefaultValue = False
                End If

                Dim mstrApprovalTranunkid As String = ""
                Dim objClaimProces As New clsclaim_process_Tran
                Dim dsList As DataSet = objClaimProces.GetList("List", True, , "cmclaim_process_tran.crmasterunkid = " & mintClaimMstUnkId)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    Dim objPeriod As New clscommom_period_Tran
                    For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                        If CBool(dsList.Tables(0).Rows(i)("IsPosted")) Then

                            objPeriod._Periodunkid(CStr(Session("Database_Name"))) = CInt(dsList.Tables(0).Rows(i)("periodunkid"))

                            Dim drRow() As DataRow = mdt.Select("crapprovaltranunkid = " & CInt(dsList.Tables(0).Rows(i)("crapprovaltranunkid")))
                            If objPeriod._Statusid = 2 Then 'enStatusType.Close
                                If drRow.Length > 0 Then
                                    drRow(0).Delete()
                                End If
                            Else
                                If drRow.Length > 0 Then
                                    drRow(0)("IsPosted") = CBool(dsList.Tables(0).Rows(i)("isposted"))
                                End If
                            End If
                        Else
                            Dim drRow() As DataRow = mdt.Select("crapprovaltranunkid = " & CInt(dsList.Tables(0).Rows(i)("crapprovaltranunkid")))
                            If drRow.Length > 0 Then
                                drRow(0)("IsPosted") = CBool(dsList.Tables(0).Rows(i)("isposted"))
                            End If
                        End If
                        mstrApprovalTranunkid &= CInt(dsList.Tables(0).Rows(i)("crapprovaltranunkid")) & ","
                    Next

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objPeriod = Nothing
                    'Pinkal (11-Sep-2020) -- End


                    If mstrApprovalTranunkid.Trim.Length > 0 Then mstrApprovalTranunkid = mstrApprovalTranunkid.Trim.Substring(0, mstrApprovalTranunkid.Trim.Length - 1)

                    Dim dRows() As DataRow = mdt.Select("crapprovaltranunkid Not in ( " & mstrApprovalTranunkid & ")")
                    If dRows.Length > 0 Then
                        For j As Integer = 0 To dRows.Length - 1
                            dRows(j)("IsPosted") = False
                        Next
                    End If
                Else
                    For Each dRow As DataRow In mdt.Rows
                        dRow("IsPosted") = False
                    Next
                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsList IsNot Nothing Then dsList.Clear()
                dsList = Nothing
                objClaimProces = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If


            mdt.AcceptChanges()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'mdtCanceExpTable = mdt
            mdtCanceExpTable = mdt.Copy()
            'Pinkal (11-Sep-2020) -- End


            Dim mdtTran As DataTable = New DataView(mdt, "iscancel = 0", "", DataViewRowState.CurrentRows).ToTable
            dgvCancelExpData.DataSource = mdtTran
            dgvCancelExpData.DataBind()
            If mdt.Rows.Count > 0 Then
                txtCancelExpGrandTotal.Text = Format(CDec(mdtTran.Compute("SUM(amount)", "AUD<>'D' AND iscancel = 0")), Session("fmtCurrency").ToString)
            Else
                txtCancelExpGrandTotal.Text = "0.00"
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Me.ViewState("mdtTran") = mdtTran
            Me.ViewState("mdtTran") = mdtTran.Copy()

            If mdt IsNot Nothing Then mdt.Clear()
            mdt = Nothing

            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing

            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_CancelExpense : " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Function Is_Valid_CancelExp() As Boolean
        Try

            If txtCancelExpRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Cancel Remark cannot be blank.Cancel Remark is required information."), Me)
                txtCancelExpRemark.Focus()
                Return False
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdtTran As DataTable = mdtCanceExpTable
            Dim mdtTran As DataTable = mdtCanceExpTable.Copy()
            'Pinkal (11-Sep-2020) -- End

            Dim drRow() As DataRow = mdtTran.Select("Ischeck = True")
            If drRow.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Please Select atleast One Expense to cancel."), Me)
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                drRow = Nothing
                If mdtTran IsNot Nothing Then mdtTran.Clear()
                mdtTran = Nothing
                'Pinkal (11-Sep-2020) -- End
                Return False
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            drRow = Nothing
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (11-Sep-2020) -- End


            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "Is_Valid", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function

    Private Function CancelLeaveFormValidation() As Boolean
        Try

            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            'If txtCancelFormNo.Text.Trim.Length = 0 Then
            If CInt(Session("LeaveCancelFormNotype")) = 0 AndAlso txtCancelFormNo.Text.Trim.Length <= 0 Then
                'Pinkal (03-May-2019) -- End
                'Language.setLanguage(mstrModuleName4)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 1, "Cancel Form No cannot be blank. Cancel Form No is required information."), Me)
                popupCancel.Show()
                txtCancelFormNo.Focus()
                Return False
            ElseIf chkListLeavedate.SelectedItem Is Nothing Then
                'Language.setLanguage(mstrModuleName4)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 2, "Leave Date is compulsory information.Please Select atleast One Leave Date."), Me)
                popupCancel.Show()
                chkListLeavedate.Focus()
                Return False
            ElseIf txtCancelReason.Text.Trim.Length = 0 Then
                'Language.setLanguage(mstrModuleName4)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 3, "Reason cannot be blank. Reason is required information."), Me)
                popupCancel.Show()
                txtCancelReason.Focus()
                Return False
                'Pinkal (28-Nov-2017) -- Start
                'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            Else
                Dim objLvType As New clsleavetype_master
                objLvType._Leavetypeunkid = mintCancelLeaveTypeUnkId
                If objLvType._CancelLvFormDocMandatory Then
                    Dim mstrInfoMsg As String = ""
                    If mdtAttachement IsNot Nothing Then
                        Dim drRow() As DataRow = mdtAttachement.Select("AUD <> 'D' AND AUD <> ''")
                        If drRow.Length <= 0 Then
                            mstrInfoMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Cancel Leave Form has mandatory document attachment. please attach document.")
                        End If
                    ElseIf mdtAttachement Is Nothing Then
                        mstrInfoMsg = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "Cancel Leave Form has mandatory document attachment. please attach document.")
                    End If
                    If mstrInfoMsg.Trim.Length > 0 Then
                        DisplayMessage.DisplayMessage(mstrInfoMsg, Me)
                        popupCancel.Show()
                        Return False
                    End If
                End If
                objLvType = Nothing
                'Pinkal (28-Nov-2017) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function

    Private Sub FinalCancelSave()
        Try

            'Pinkal (16-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            Dim objLvType As New clsleavetype_master
            objLvType._Leavetypeunkid = mintCancelLeaveTypeUnkId
            If objLvType._IsBlockLeave AndAlso objLvType._LVDaysMinLimit > 0 Then
                Dim mdecCancelDays As Decimal = 0
                Dim mdecTotalIssueDays As Decimal = 0
                Dim objDayFraction As New clsleaveday_fraction

                For i As Integer = 0 To chkListLeavedate.Items.Count - 1
                    If (chkListLeavedate.Items(i).Selected) Then
                        mdecCancelDays += objDayFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(DateTime.Parse(chkListLeavedate.Items(i).Value.ToString(), CultureInfo.InvariantCulture).Date), mintCancelFormId _
                                                                      , mintCancelEmpUnkId, CBool(Session("LeaveApproverForLeaveType")).ToString(), -1, Nothing)
                    End If
                Next

                objDayFraction = Nothing

                Dim objBlockIssueLeave As New clsleaveissue_master
                Dim dsIssueDays As DataSet = objBlockIssueLeave.GetEmployeeIssueData(mintCancelEmpUnkId, objLvType._Leavetypeunkid)
                objBlockIssueLeave = Nothing

                If dsIssueDays IsNot Nothing AndAlso dsIssueDays.Tables(0).Rows.Count > 0 Then
                    mdecTotalIssueDays = dsIssueDays.Tables(0).AsEnumerable.Sum(Function(x) x.Field(Of Decimal)("dayfraction"))
                End If


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsIssueDays IsNot Nothing Then dsIssueDays.Clear()
                dsIssueDays = Nothing
                'Pinkal (11-Sep-2020) -- End


                If (mdecTotalIssueDays - mdecCancelDays) > 0 AndAlso (mdecTotalIssueDays - mdecCancelDays) < objLvType._LVDaysMinLimit Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 15, "Sorry, you cannot cancel leave days as it is going below as compared to") & " " & objLvType._LVDaysMinLimit & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 16, "days configured on Leave Application Leave days limit setting for this leave type."), Me)
                    Exit Sub
                End If

            End If

            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if all issued days are cancelled then system will void employee exemption transaction for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            Dim intSelectedCnt As Integer = (From p In chkListLeavedate.Items.Cast(Of ListItem)() Where (p.Selected = True)).Count
            If objLvType._IsPaid = False AndAlso chkListLeavedate.Items.Count = intSelectedCnt Then
                If CBool(Session("SkipEmployeeMovementApprovalFlow")) = False Then
                    Dim objADate As New clsDates_Approval_Tran
                    Dim intID As Integer = objADate.GetUnkIdByLeaveIssueUnkId(Nothing, 0, "")
                    Dim Flag As Boolean = objADate.isExist(Nothing, Nothing, Nothing, enEmp_Dates_Transaction.DT_EXEMPTION, CInt(mintCancelEmpUnkId), clsEmployeeMovmentApproval.enOperationType.EDITED, "", Nothing, True, intID)

                    If Flag = False Then
                        Flag = objADate.isExist(Nothing, Nothing, Nothing, enEmp_Dates_Transaction.DT_EXEMPTION, CInt(mintCancelEmpUnkId), clsEmployeeMovmentApproval.enOperationType.ADDED, "", Nothing, True, intID)

                        If Flag = False Then
                            Flag = objADate.isExist(Nothing, Nothing, Nothing, enEmp_Dates_Transaction.DT_EXEMPTION, CInt(mintCancelEmpUnkId), clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True, intID)
                        End If
                    End If

                    If Flag Then
                        'Pinkal (30-May-2020) -- Start
                        'Enhancement NMB - Leave Cancellation Option put on Configuration and implementing on Leave Form List screen.
                        'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 17, "Sorry, You can not perform delete opration, Reason:This Entry Is Already Present In Approval Process."), Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 17, "Sorry, You can not perform delete opration, Reason:This Entry Is Already Present In Approval Process."), Me)
                        'Pinkal (30-May-2020) -- End
                        Exit Sub
                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objADate = Nothing
                    'Pinkal (11-Sep-2020) -- End

                End If
            End If
            'Sohail (21 Oct 2019) -- End

            objLvType = Nothing
            'Pinkal (16-May-2019) -- End


            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmCancel_leaveform"
            StrModuleName2 = "mnuLeaveInformation"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If


            Dim objCancelForm As New clsCancel_Leaveform
            objCancelForm._Formunkid = mintCancelFormId
            objCancelForm._Cancelformno = txtCancelFormNo.Text.Trim
            objCancelForm._Cancel_Reason = txtCancelReason.Text.Trim
            objCancelForm._Cancel_Trandate = ConfigParameter._Object._CurrentDateAndTime
            objCancelForm._Canceluserunkid = CInt(Session("UserId"))


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objCancelForm._SkipForApproverFlow = mblnLvTypeSkipApproverFlow
            objCancelForm._WebFrmName = "frmCancel_leaveform"
            objCancelForm._WebClientIP = Session("IP_ADD").ToString
            objCancelForm._WebHostName = Session("HOST_NAME").ToString
            'Pinkal (01-Oct-2018) -- End


            Dim mstrLeavedates As String = ""
            For i As Integer = 0 To chkListLeavedate.Items.Count - 1
                If (chkListLeavedate.Items(i).Selected) Then
                    mstrLeavedates &= eZeeDate.convertDate(DateTime.Parse(chkListLeavedate.Items(i).Value.ToString(), CultureInfo.InvariantCulture).Date) & ","
                End If
            Next

            If mstrLeavedates.Trim.Length > 0 Then
                mstrLeavedates = mstrLeavedates.Trim.Substring(0, mstrLeavedates.Trim.Length - 1)
            End If

            If mintClaimMstUnkId > 0 Then
                objCancelForm._CancelExpenseRemark = mstrCancelClmRemark
                objCancelForm._dtCancelExpense = mdtCanceExpTable
                objCancelForm._ClaimMstId = mintClaimMstUnkId
                objCancelForm._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
            End If


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            Dim mintRelieverEmployeeId As Integer = 0
            'Pinkal (25-May-2019) -- End


            Dim mintApproverID As Integer = -1
            Dim objPending As New clspendingleave_Tran
            Dim dsApprover As DataSet = objPending.GetEmployeeApproverListWithPriority(mintCancelEmpUnkId, mintCancelFormId)
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objPending = Nothing
            'Pinkal (11-Sep-2020) -- End
            If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then
                Dim drRow() As DataRow = dsApprover.Tables(0).Select("priority = Max(priority)")
                If drRow.Length > 0 Then
                    Dim objLeaveDayFraction As New clsleaveday_fraction
                    For i As Integer = 0 To drRow.Length - 1
                        Dim dsDayFraction As DataSet = objLeaveDayFraction.GetList("List", mintCancelFormId, True, mintCancelEmpUnkId, CInt(drRow(i)("leaveapproverunkid")), "")
                        If dsDayFraction IsNot Nothing AndAlso dsDayFraction.Tables(0).Rows.Count > 0 Then
                            mintApproverID = CInt(dsDayFraction.Tables(0).Rows(0)("approverunkid"))
                            Exit For
                        End If
                    Next
                End If

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                If CBool(Session("SetRelieverAsMandatoryForApproval")) Then
                    Dim mintMaxPriority As Integer = dsApprover.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("statusunkid") = 7).Select(Function(x) x.Field(Of Integer)("priority")).Max()
                    Dim drReliever = dsApprover.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("statusunkid") = 7 And x.Field(Of Integer)("priority") = mintMaxPriority)
                    If drReliever.Count > 0 Then
                        mintRelieverEmployeeId = CInt(drReliever(0)("relieverempunkid"))
                    End If
                End If
                'Pinkal (25-May-2019) -- End

            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsApprover IsNot Nothing Then dsApprover.Clear()
            dsApprover = Nothing
            'Pinkal (11-Sep-2020) -- End


            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .

            objCancelForm._dtAttachment = mdtAttachement

            Dim mdsDoc As DataSet
            Dim mstrFolderName As String = ""
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
            Dim strFileName As String = ""
            For Each dRow As DataRow In mdtAttachement.Rows
                mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRow("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault
                If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                    'Pinkal (20-Nov-2018) -- Start
                    'Enhancement - Working on P2P Integration for NMB.
                    'strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & System.IO.Path.GetExtension(CStr(dRow("orgfilepath")))
                    strFileName = Session("CompanyCode").ToString() & "_" & Guid.NewGuid.ToString & System.IO.Path.GetExtension(CStr(dRow("orgfilepath")))
                    'Pinkal (20-Nov-2018) -- End
                    If System.IO.File.Exists(CStr(dRow("orgfilepath"))) Then
                        Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                            System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                        End If

                        System.IO.File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                        dRow("fileuniquename") = strFileName
                        dRow("filepath") = strPath
                        dRow.AcceptChanges()
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                ElseIf dRow("AUD").ToString = "U" AndAlso dRow("fileuniquename").ToString <> "" Then
                    strFileName = dRow("fileuniquename").ToString()
                    Dim strServerPath As String = dRow("filepath").ToString()
                    Dim strPath As String = Session("ArutiSelfServiceURL").ToString

                    If strServerPath.Contains(strPath) Then
                        strServerPath = strServerPath.Replace(strPath, "")
                        If Strings.Left(strServerPath, 1) <> "/" Then
                            strServerPath = "~/" & strServerPath
                        Else
                            strServerPath = "~" & strServerPath

                        End If

                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                        If System.IO.File.Exists(Server.MapPath(strServerPath)) Then

                            If System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                            End If

                            System.IO.File.Move(Server.MapPath(strServerPath), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))

                            dRow("filepath") = strPath
                            dRow.AcceptChanges()
                        Else
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'DisplayMessage.DisplayError(ex, Me)
                            DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If

                ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                    Dim strFilepath As String = dRow("filepath").ToString()
                    Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                    If strFilepath.Contains(strArutiSelfService) Then
                        strFilepath = strFilepath.Replace(strArutiSelfService, "")
                        If Strings.Left(strFilepath, 1) <> "/" Then
                            strFilepath = "~/" & strFilepath
                        Else
                            strFilepath = "~" & strFilepath
                        End If

                        If System.IO.File.Exists(Server.MapPath(strFilepath)) Then
                            System.IO.File.Delete(Server.MapPath(strFilepath))
                        Else
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'DisplayMessage.DisplayError(ex, Me)
                            DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                    Else
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If

                End If
            Next

            'Pinkal (28-Nov-2017) -- End


            'Pinkal (13-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.

            'Dim blnFlag As Boolean = objCancelForm.Insert(mstrLeavedates, _
            '                                               mintCancelEmpUnkId, _
            '                                               mintCancelLeaveTypeUnkId, _
            '                                               mintTotalIssuedDays, _
            '                                               CInt(Me.ViewState("CancelDays")), ConfigParameter._Object._CurrentDateAndTime, _
            '                                               CInt(Session("Fin_year")), _
            '                                               Session("LeaveApproverForLeaveType").ToString(), _
            '                                               CInt(Session("LeaveBalanceSetting")), mintApproverID)


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.

            'Dim blnFlag As Boolean = objCancelForm.Insert(mstrLeavedates, _
            '                                              mintCancelEmpUnkId, _
            '                                              mintCancelLeaveTypeUnkId, _
            '                                              mintTotalIssuedDays, _
            '                                           mintTotalCanceldays, ConfigParameter._Object._CurrentDateAndTime, _
            '                                              CInt(Session("Fin_year")), _
            '                                              Session("LeaveApproverForLeaveType").ToString(), _
            '                                              CInt(Session("LeaveBalanceSetting")), mintApproverID)

            Dim blnFlag As Boolean = objCancelForm.Insert(CInt(Session("CompanyUnkId")), mstrLeavedates _
                                                         , mintCancelEmpUnkId, mintCancelLeaveTypeUnkId _
                                                         , mintTotalIssuedDays, mintTotalCanceldays _
                                                         , ConfigParameter._Object._CurrentDateAndTime, CInt(Session("Fin_year")) _
                                                         , Session("LeaveApproverForLeaveType").ToString(), CInt(Session("LeaveBalanceSetting")), mintApproverID _
                                                         , CBool(Session("SkipEmployeeMovementApprovalFlow")) _
                                                         , CBool(Session("CreateADUserFromEmpMst")) _
                                                         , Session("Database_Name").ToString _
                                                         , Session("UserAccessModeSetting").ToString _
                                                         , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                         , enLogin_Mode.MGR_SELF_SERVICE _
                                                         , Session("UserName").ToString _
                                                         )
            'Sohail (21 Oct 2019) -- [blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst, xDatabaseName, xUserAccessModeSetting, dtEmployeeAsOnDate, eMode, xUsername]
            'Pinkal (03-May-2019) -- End

            'Pinkal (13-Oct-2018) -- End

            If blnFlag = False And objCancelForm._Message <> "" Then
                DisplayMessage.DisplayMessage(objCancelForm._Message, Me)
                popupCancel.Show()
            Else
                SetDateFormat()

                Dim intStatusID As Integer = 0
                Dim arStr As String() = mstrLeavedates.Split(CChar(","))


                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                'If arStr.Length = chkListLeavedate.Items.Count Then
                '    intStatusID = 6
                'Else
                '    intStatusID = 7
                'End If
                If arStr.Length > 0 Then
                    intStatusID = 6
                End If
                'Pinkal (03-May-2019) -- End


                Dim objLeaveForm As New clsleaveform
                objLeaveForm._Formunkid = objCancelForm._Formunkid

                Dim objEmp As New clsEmployee_Master

                objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = objLeaveForm._Employeeunkid
                objLeaveForm._EmployeeCode = objEmp._Employeecode
                objLeaveForm._EmployeeFirstName = objEmp._Firstname
                objLeaveForm._EmployeeMiddleName = objEmp._Othername
                objLeaveForm._EmployeeSurName = objEmp._Surname
                objLeaveForm._EmpMail = objEmp._Email
                Blank_ModuleName()
                objLeaveForm._WebFrmName = "frmCancel_leaveform"
                objLeaveForm._WebClientIP = Session("IP_ADD").ToString
                objLeaveForm._WebHostName = Session("HOST_NAME").ToString
                StrModuleName2 = "mnuLeaveInformation"

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then

                    'Pinkal (01-Apr-2019) -- Start
                    'Enhancement - Working on Leave Changes for NMB.
                    'objLeaveForm.SendMailToEmployee(CInt(Me.ViewState("Employeeunkid")), txtCancelLeaveType.Text.Trim, objLeaveForm._Startdate.Date, objLeaveForm._Returndate.Date, intStatusID, CInt(Session("CompanyUnkId")), mstrLeavedates, "", "", enLogin_Mode.EMP_SELF_SERVICE, CInt(Session("Employeeunkid")), 0)
                    objLeaveForm.SendMailToEmployee(CInt(Me.ViewState("Employeeunkid")), txtCancelLeaveType.Text.Trim, objLeaveForm._Startdate.Date _
                                                                       , objLeaveForm._Returndate.Date, intStatusID, CInt(Session("CompanyUnkId")), mstrLeavedates, "", "" _
                                                                       , enLogin_Mode.EMP_SELF_SERVICE, CInt(Session("Employeeunkid")), 0, objLeaveForm._Formno)
                    'Pinkal (01-Apr-2019) -- End


                ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then

                    'Pinkal (01-Apr-2019) -- Start
                    'Enhancement - Working on Leave Changes for NMB.
                    'objLeaveForm.SendMailToEmployee(CInt(Me.ViewState("Employeeunkid")), txtCancelLeaveType.Text.Trim, objLeaveForm._Startdate.Date, objLeaveForm._Returndate.Date, intStatusID, CInt(Session("CompanyUnkId")), mstrLeavedates, "", "", enLogin_Mode.EMP_SELF_SERVICE, 0, CInt(Session("UserId")))
                    objLeaveForm.SendMailToEmployee(CInt(Me.ViewState("Employeeunkid")), txtCancelLeaveType.Text.Trim, objLeaveForm._Startdate.Date _
                                                                      , objLeaveForm._Returndate.Date, intStatusID, CInt(Session("CompanyUnkId")), mstrLeavedates, "", "" _
                                                                      , enLogin_Mode.EMP_SELF_SERVICE, 0, CInt(Session("UserId")), objLeaveForm._Formno)
                    'Pinkal (01-Apr-2019) -- End

                End If


                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                If CBool(Session("SetRelieverAsMandatoryForApproval")) AndAlso mintRelieverEmployeeId > 0 Then
                    objLeaveForm.SendMailToReliever(CInt(Session("CompanyUnkId")), mintRelieverEmployeeId, objLeaveForm._Leavetypeunkid, objLeaveForm._Formunkid _
                                                                    , objLeaveForm._Formno, objEmp._Firstname & " " & objEmp._Surname, objLeaveForm._Startdate.Date, objLeaveForm._Returndate.Date _
                                                                    , CInt(Session("UserId")), 6, mstrLeavedates, enLogin_Mode.MGR_SELF_SERVICE)
                End If
                'Pinkal (25-May-2019) -- End


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objEmp = Nothing
                objLvType = Nothing
                objCancelForm = Nothing
                'Pinkal (11-Sep-2020) -- End



                mdtAttachement = Nothing
                mintCancelFormId = 0
                mintCancelEmpUnkId = 0
                mintCancelLeaveTypeUnkId = 0
                mintTotalCanceldays = 0
                mintTotalIssuedDays = 0

                popupCancel.Dispose()
                Call FillList()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FinalCancelSave :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (28-Nov-2017) -- Start
    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .

    Private Sub FillAttachment()
        Dim dtView As DataView
        Try
            If mdtAttachement Is Nothing Then Exit Sub
            dtView = New DataView(mdtAttachement, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgv_Attchment.AutoGenerateColumns = False
            dgv_Attchment.DataSource = dtView
            dgv_Attchment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtAttachement.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtAttachement.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = mintCancelEmpUnkId
                dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Leave_Module
                dRow("scanattachrefid") = enScanAttactRefId.LEAVEFORMS
                dRow("transactionunkid") = -1
                dRow("filepath") = ""
                dRow("filename") = f.Name
                dRow("filesize") = f.Length / 1024
                dRow("attached_date") = Today.Date
                dRow("orgfilepath") = strfullpath
                dRow("GUID") = Guid.NewGuid
                dRow("AUD") = "A"
                dRow("form_name") = mstrModuleName4
                dRow("userunkid") = CInt(Session("userid"))
                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
                'S.SANDEEP |16-MAY-2019| -- END
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 13, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtAttachement.Rows.Add(dRow)
            Call FillAttachment()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (28-Nov-2017) -- End

    'Pinkal (10-Jun-2020) -- Start
    'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.

    Private Sub GetBalanceInfo()
        Try
            Dim objLeaveAccrue As New clsleavebalance_tran
            Dim dsBalance As DataSet = Nothing
            Dim mintDeductedTypeId As String = ""

            Dim objLvType As New clsleavetype_master
            objLvType._Leavetypeunkid = mintCancelLeaveTypeUnkId
            If objLvType._DeductFromLeaveTypeunkid > 0 Then
                mintDeductedTypeId = mintCancelLeaveTypeUnkId.ToString() & "," & objLvType._DeductFromLeaveTypeunkid.ToString()
            Else
                mintDeductedTypeId = mintCancelLeaveTypeUnkId.ToString()
            End If
            objLvType = Nothing

            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                dsBalance = objLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, _
                                                                                            mintDeductedTypeId.ToString(), _
                                                                                            mintCancelEmpUnkId.ToString(), _
                                                                                            CInt(Session("LeaveAccrueTenureSetting")), _
                                                                                            CInt(Session("LeaveAccrueDaysAfterEachMonth")), _
                                                                                            CDate(Session("fin_startdate")).Date, _
                                                                                            CDate(Session("fin_enddate")).Date, , , _
                                                                                            CInt(Session("LeaveBalanceSetting")), , _
                                                                                            CInt(Session("Fin_year")))

            ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                objLeaveAccrue._DBStartdate = CDate(Session("fin_startdate")).Date
                objLeaveAccrue._DBEnddate = CDate(Session("fin_enddate")).Date

                dsBalance = objLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, _
                                                                                         mintDeductedTypeId.ToString(), _
                                                                                         mintCancelEmpUnkId.ToString(), _
                                                                                         CInt(Session("LeaveAccrueTenureSetting")), _
                                                                                         CInt(Session("LeaveAccrueDaysAfterEachMonth")), _
                                                                                         Nothing, Nothing, True, True, _
                                                                                         CInt(Session("LeaveBalanceSetting")), , _
                                                                                         CInt(Session("Fin_year")))
            End If

            If Not dsBalance.Tables(0).Rows.Count = 0 Then
                If dsBalance.Tables("Balanceinfo").Rows.Count > 0 Then
                    objlblLeaveBFvalue.Text = CDec(dsBalance.Tables("Balanceinfo").Rows(0)("LeaveBF")).ToString("#0.00")
                    objlblAccruedToDateValue.Text = CDec(dsBalance.Tables("Balanceinfo").Rows(0)("Accrue_amount")).ToString("#0.00")
                    objlblIssuedToDateValue.Text = CDec(CDec(dsBalance.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsBalance.Tables("Balanceinfo").Rows(0)("LeaveEncashment"))).ToString("#0.00")
                    objlblTotalAdjustment.Text = CDec(dsBalance.Tables("Balanceinfo").Rows(0)("LeaveAdjustment")).ToString("#0.00")
                    objlblBalanceAsonDateValue.Text = CDec(CDec(CDec(dsBalance.Tables("Balanceinfo").Rows(0)("LeaveBF")) + CDec(dsBalance.Tables("Balanceinfo").Rows(0)("Accrue_amount"))) - CDec(CDec(dsBalance.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsBalance.Tables("Balanceinfo").Rows(0)("LeaveEncashment")) + CDec(dsBalance.Tables("Balanceinfo").Rows(0)("LeaveAdjustment")))).ToString("#0.00")
                    objlblBalanceValue.Text = CDec(dsBalance.Tables("Balanceinfo").Rows(0)("Balance")).ToString("#0.00")
                End If
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsBalance IsNot Nothing Then dsBalance.Clear()
            dsBalance = Nothing
            'Pinkal (11-Sep-2020) -- End

            objLeaveAccrue = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (10-Jun-2020) -- End

#End Region

#End Region

#Region "Button Event(S)"

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click
        Try
            If CInt(ddlFilEmpList.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If

            Call FillList()

            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            Me.ViewState("CancelAttachement") = Nothing
            Me.ViewState("mblnCancelLvFormPopup") = False
            Me.ViewState("mblnCancelAttachmentPopup") = False
            'Pinkal (28-Nov-2017) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_DeleteForm_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_DeleteForm.buttonDelReasonYes_Click
        Dim mintClaimMstID As Integer = -1
        Dim intLoginEmpUnkID As Integer = 0
        Try
            If (popup_DeleteForm.Reason.Trim = "") Then
                DisplayMessage.DisplayMessage(" Please enter delete reason.", Me)
                popup_DeleteForm.Show()
                Exit Sub
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim clsleaveform As New clsleaveform
            Dim objClaimRequestMst As New clsclaim_request_master
            'Pinkal (11-Sep-2020) -- End

            With clsleaveform
                ._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popup_DeleteForm.Reason
                ._Voiduserunkid = CInt(Session("UserId"))
                ._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))
                Blank_ModuleName()
                clsCommonATLog._WebFormName = "frmLeaveFormList"
                StrModuleName2 = "mnuLeaveInformation"
                clsCommonATLog._WebClientIP = Session("IP_ADD").ToString
                clsCommonATLog._WebHostName = Session("HOST_NAME").ToString


                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                Else
                    clsCommonATLog._LoginEmployeeUnkid = -1
                End If

                mintClaimMstID = objClaimRequestMst.GetClaimRequestMstIDFromLeaveForm(mintFormUnkId)
                objClaimRequestMst._Crmasterunkid = mintClaimMstID
                ._ObjClaimRequestMst = objClaimRequestMst


                'Pinkal (28-Nov-2017) -- Start
                'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                Dim objClaimTran As New clsclaim_request_tran
                Dim mdtAttachement As DataTable = Nothing
                objClaimTran._ClaimRequestMasterId = mintClaimMstID

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Dim dtExpense As DataTable = objClaimTran._DataTable
                Dim dtExpense As DataTable = objClaimTran._DataTable.Copy()
                'Pinkal (11-Sep-2020) -- End

                objClaimTran = Nothing

                Dim objAttchement As New clsScan_Attach_Documents
                'S.SANDEEP |04-SEP-2021| -- START
                'ISSUE : TAKING CARE FROM SLOWNESS QUERY
                'Call objAttchement.GetList(Session("Document_Path").ToString(), "List", "", mintLeaveApplicationEmpID)
                Call objAttchement.GetList(Session("Document_Path").ToString(), "List", "", mintLeaveApplicationEmpID, False, Nothing, Nothing, "", CBool(IIf(mintLeaveApplicationEmpID <= 0, True, False)))
                'S.SANDEEP |04-SEP-2021| -- END



                Dim strTranIds As String = String.Join(",", dtExpense.AsEnumerable().Select(Function(x) x.Field(Of Integer)("crtranunkid").ToString).ToArray)
                If strTranIds.Trim.Length <= 0 Then strTranIds = "0"

                mdtAttachement = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable

                For Each dRow As DataRow In mdtAttachement.Rows
                    dRow.Item("AUD") = "D"
                Next
                mdtAttachement.AcceptChanges()


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtExpense IsNot Nothing Then dtExpense.Clear()
                dtExpense = Nothing
                'Pinkal (11-Sep-2020) -- End


                Dim objlvType As New clsleavetype_master
                objlvType._Leavetypeunkid = mintLeaveTypeID
                If objlvType._IsCheckDocOnLeaveForm Then
                    Dim dtTable As DataTable = objAttchement.GetAttachmentTranunkIds(mintLeaveApplicationEmpID _
                                                                                , enScanAttactRefId.LEAVEFORMS, enImg_Email_RefId.Leave_Module _
                                                                                , mintFormUnkId)

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'clsleaveform._dtAttachDocument = dtTable
                    'dtTable = Nothing
                    clsleaveform._dtAttachDocument = dtTable.Copy()
                    If dtTable IsNot Nothing Then dtTable.Clear()
                    dtTable = Nothing
                    'Pinkal (11-Sep-2020) -- End
                Else
                    clsleaveform._dtAttachDocument = Nothing
                End If
                objlvType = Nothing
                objAttchement = Nothing

                'If .Delete(mintFormUnkId, _
                '           CStr(Session("Database_Name")), _
                '           CInt(Session("UserId")), _
                '           CInt(Session("Fin_year")), _
                '           CInt(Session("CompanyUnkId")), _
                '           CStr(Session("EmployeeAsOnDate")), _
                '           CStr(Session("UserAccessModeSetting")), True, _
                '           CBool(Session("IsIncludeInactiveEmp")), (New DataTable), True) Then

                If .Delete(mintFormUnkId, _
                           CStr(Session("Database_Name")), _
                           CInt(Session("UserId")), _
                           CInt(Session("Fin_year")), _
                           CInt(Session("CompanyUnkId")), _
                           CStr(Session("EmployeeAsOnDate")), _
                           CStr(Session("UserAccessModeSetting")), True, _
                        CBool(Session("IsIncludeInactiveEmp")), mdtAttachement, True) Then

                    'Pinkal (28-Nov-2017) -- End

                    clsleaveform._Formunkid = mintFormUnkId
                    clsleaveform._WebFrmName = "frmLeaveFormList"
                    StrModuleName2 = "mnuLeaveInformation"

                    If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                        intLoginEmpUnkID = CInt(Session("Employeeunkid"))
                    End If


                    'Pinkal (01-Apr-2019) -- Start
                    'Enhancement - Working on Leave Changes for NMB.

                    'clsLeaveForm.SendMailToApprover(CStr(Session("Database_Name")), _
                    '                                    CInt(Session("UserId")), _
                    '                                    CInt(Session("Fin_year")), _
                    '                                    CInt(Session("CompanyUnkId")), _
                    '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                    '                                    CStr(Session("UserAccessModeSetting")), True, _
                    '                                    CBool(Session("IsIncludeInactiveEmp")), _
                    '                                    clsLeaveForm._Employeeunkid, _
                    '                                    clsLeaveForm._Leavetypeunkid, _
                    '                                    clsLeaveForm._Formunkid, _
                    '                                    clsLeaveForm._Formno, True, , , _
                    '                                    Aruti.Data.enLogin_Mode.MGR_SELF_SERVICE, intLoginEmpUnkID)


                    Dim mstrDeleterName As String = ""

                    If Session("Firstname") IsNot Nothing AndAlso Session("Surname") IsNot Nothing Then
                        mstrDeleterName = Session("Firstname").ToString().Trim() & " " & Session("Surname").ToString().Trim()
                    End If

                    If mstrDeleterName.Trim.Length <= 0 Then
                        mstrDeleterName = Session("UserName").ToString().Trim()
                    End If

                    clsleaveform.SendMailToApprover(CStr(Session("Database_Name")), _
                                                        CInt(Session("UserId")), _
                                                        CInt(Session("Fin_year")), _
                                                        CInt(Session("CompanyUnkId")), _
                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                        CStr(Session("UserAccessModeSetting")), True, _
                                                        CBool(Session("IsIncludeInactiveEmp")), _
                                                        clsleaveform._Employeeunkid, _
                                                        clsleaveform._Leavetypeunkid, _
                                                        clsleaveform._Formunkid, _
                                                        clsleaveform._Formno, True, , , _
                                                       Aruti.Data.enLogin_Mode.MGR_SELF_SERVICE, intLoginEmpUnkID, False, mstrDeleterName)

                    'Pinkal (01-Apr-2019) -- End



                    'Pinkal (28-Nov-2017) -- Start
                    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .

                    If mdtAttachement IsNot Nothing Then
                        Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                        Dim mdsDoc As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                        Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.CLAIM_REQUEST) Select (p.Item("Name").ToString)).FirstOrDefault

                        For Each dRow As DataRow In mdtAttachement.Rows
                            Dim strError As String = ""
                            If dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                                Dim strFileName As String = dRow("fileuniquename").ToString

                                If blnIsIISInstalled Then
                                    If clsFileUploadDownload.DeleteFile(dRow("filepath").ToString, strFileName, mstrFolderName, strError, Session("ArutiSelfServiceURL").ToString(), CInt(Session("CompanyUnkId"))) = False Then
                                        DisplayMessage.DisplayMessage(strError, Me)
                                        Exit Sub
                                    End If
                                Else
                                    If System.IO.Directory.Exists(Session("Document_Path").ToString()) Then
                                        Dim strDocLocalPath As String = Session("Document_Path").ToString() & "\" & mstrFolderName & "\" & strFileName
                                        If System.IO.File.Exists(strDocLocalPath) Then
                                            System.IO.File.Delete(strDocLocalPath)
                                        End If
                                    Else
                                        'Sohail (23 Mar 2019) -- Start
                                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                        'DisplayMessage.DisplayError(ex, Me)
                                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "Configuration Path does not Exist."), Me)
                                        'Sohail (23 Mar 2019) -- End
                                        Exit Sub
                                    End If
                                End If
                            End If
                        Next
                    End If
                    'Pinkal (28-Nov-2017) -- End
                End If

                If (._Message.Length > 0) Then
                    DisplayMessage.DisplayMessage("You Cannot Delete this Entry." & ._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Successfully deleted." & ._Message, Me)
                    Me.ViewState("Formunkid") = Nothing
                    Call FillList()
                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If mdtAttachement IsNot Nothing Then mdtAttachement.Clear()
                mdtAttachement = Nothing
                'Pinkal (11-Sep-2020) -- End


            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnCancelExpYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Dim mdtTran As New DataTable
        Dim dtApprover As DataTable = Nothing

        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objClaimRequestMst As New clsclaim_request_master
        Dim objClaimTran As New clsclaim_request_tran
        Dim objExpApproverTran As New clsclaim_request_approval_tran
        'Pinkal (11-Sep-2020) -- End

        Try
            popupCancel.Show()

            Call Fill_CancelCombo()
            txtCancelExpClaimNo.Enabled = False
            cboCancelExpCategory.Enabled = False
            dtpCancelExpDate.Enabled = False
            objClaimRequestMst._Crmasterunkid = mintClaimMstUnkId
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Call GetCancelValue()
            Call GetCancelValue(objClaimRequestMst)
            'Pinkal (11-Sep-2020) -- End

            objClaimTran._ClaimRequestMasterId = mintClaimMstUnkId

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            'dtApprover = objExpApproverTran.GetMaxApproverForClaimForm(objClaimRequestMst._Employeeunkid, _
            '                                                           objClaimRequestMst._Expensetypeid, _
            '                                                           objClaimRequestMst._Crmasterunkid, _
            '                                                           CBool(Session("PaymentApprovalwithLeaveApproval")), _
            '                                                           objClaimRequestMst._Statusunkid)

            dtApprover = objExpApproverTran.GetMaxApproverForClaimForm(objClaimRequestMst._Employeeunkid, _
                                                                       objClaimRequestMst._Expensetypeid, _
                                                                       objClaimRequestMst._Crmasterunkid, _
                                                                       CBool(Session("PaymentApprovalwithLeaveApproval")), _
                                                                       objClaimRequestMst._Statusunkid, _
                                                                       objClaimRequestMst._Referenceunkid)

            'Pinkal (04-Feb-2019) -- End

            If dtApprover.Rows.Count > 0 Then
                Me.ViewState.Add("crapproverunkid", CInt(dtApprover.Rows(0)("crapproverunkid")))

                mdtTran = objExpApproverTran.GetApproverExpesneList("List", False, _
                                                                    CBool(Session("PaymentApprovalwithLeaveApproval")), _
                                                                    CStr(Session("Database_Name")), _
                                                                    CInt(Session("UserId")), _
                                                                    Session("EmployeeAsOnDate").ToString, _
                                                                    CInt(cboCancelExpCategory.SelectedValue), False, True, _
                                                                    CInt(dtApprover.Rows(0)("crapproverunkid")), "", mintClaimMstUnkId).Tables(0)


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'mdtCanceExpTable = mdtTran
                mdtCanceExpTable = mdtTran.Copy()
                'Pinkal (11-Sep-2020) -- End

                Call Fill_CancelExpense()
                txtCancelExpRemark.Text = ""
                popup_CancelExp.Show()
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 6, "There is no Expense Data to Cancel the Expense Form."), Me)
                Exit Sub
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnCancelExpYes_Click :" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            If dtApprover IsNot Nothing Then dtApprover.Clear()
            dtApprover = Nothing
            objExpApproverTran = Nothing
            objClaimTran = Nothing
            objClaimRequestMst = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnCancelExpSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelExpSave.Click
        Try
            If Is_Valid_CancelExp() = False Then popupCancel.Show() : popup_CancelExp.Show() : Exit Sub
            mstrCancelClmRemark = txtCancelExpRemark.Text
            popup_CancelExp.Hide()
            popupCancel.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnCancelExpSave_Click :" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnCancelSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelSave.Click
        Try

            If CancelLeaveFormValidation() Then
                If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                    Dim mdtTnAStartdate As Date = Nothing
                    Dim objLvForm As New clsleaveform
                    objLvForm._Formunkid = mintCancelFormId
                    Dim objPaymentTran As New clsPayment_tran
                    Dim objPeriod As New clsMasterData
                    Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, objLvForm._Startdate.Date)

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objPeriod = Nothing
                    'Pinkal (11-Sep-2020) -- End


                    If intPeriodId > 0 Then
                        Dim objTnAPeriod As New clscommom_period_Tran
                        mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objTnAPeriod = Nothing
                        'Pinkal (11-Sep-2020) -- End
                    End If

                    If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, objLvForm._Employeeunkid, mdtTnAStartdate, intPeriodId, objLvForm._Startdate.Date) > 0 Then
                        CancelFormConfirmation.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 9, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to cancel this leave ?")
                        CancelFormConfirmation.Show()
                        Exit Sub
                    End If
                    objLvForm = Nothing

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objPaymentTran = Nothing
                    'Pinkal (11-Sep-2020) -- End

                End If
                Call FinalCancelSave()
                'Pinkal (28-Nov-2017) -- Start
                'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                mblnCancelLvFormPopup = False
                'Pinkal (28-Nov-2017) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnCancelSave_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnCancelClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelClose.Click
        Try

            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            mblnCancelLvFormPopup = False
            'Pinkal (28-Nov-2017) -- End

            mintTotalIssuedDays = 0
            mintTotalCanceldays = 0
            popupCancel.Dispose()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnCancelClose_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub BtnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNew.Click
        Try
            Session("formunkid") = Nothing
            Response.Redirect(Session("rootpath").ToString & "Leave/wPg_LeaveFormAddEdit.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnclose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (28-Nov-2017) -- Start
    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .

    Protected Sub btnCancelScanAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelScanAttachment.Click
        Try
            'Session("ScanAttchLableCaption") = "Select Employee"
            'Session("ScanAttchRefModuleID") = enImg_Email_RefId.Leave_Module
            'Session("ScanAttchenAction") = enAction.ADD_ONE
            'Session("ScanAttchToEditIDs") = ""
            'Session("LVForm_EmployeeID") = mintCancelEmpUnkId
            'Session("LVForm_LeaveTypeID") = mintCancelLeaveTypeUnkId
            'Session("ScanAttachData") = mdtAttachement
            'Response.Redirect(Session("rootpath").ToString & "Others_Forms/wPg_ScanOrAttachmentInfo.aspx?" & Server.UrlEncode(clsCrypto.Encrypt(mintCancelFormId & "|" & enScanAttactRefId.LEAVEFORMS)), False)
            mblnCancelAttachmentPopup = True
            FillAttachment()
            popup_ScanAttchment.Show()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), "frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                mdtAttachement = CType(Me.ViewState("CancelAttachement"), DataTable)
                AddDocumentAttachment(f, f.FullName)
                Call FillAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnScanClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanClose.Click
        Try
            mblnCancelAttachmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnScanSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanSave.Click
        Try
            mblnCancelAttachmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub CancelFormConfirmation_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelFormConfirmation.buttonYes_Click
        Try
            FinalCancelSave()
            mblnCancelLvFormPopup = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_AttachementYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonNo_Click
        Try
            If mintDeleteIndex > 0 Then mintDeleteIndex = 0
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_AttachementYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_AttachementYesNo.buttonYes_Click
        Try
            If mintDeleteIndex >= 0 Then
                mdtAttachement.Rows(mintDeleteIndex)("AUD") = "D"
                mdtAttachement.AcceptChanges()
                mintDeleteIndex = 0
                Call FillAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (28-Nov-2017) -- End

    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgv_Attchment.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage("No Files to download.", Me)
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & txtLeaveFormNo.Text.Replace(" ", "") + ".zip", mdtAttachement, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END

#End Region

#Region "GridView Event(S)"

    Protected Sub dgView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.DeleteCommand
        Try
            mintFormUnkId = CInt(dgView.DataKeys(e.Item.ItemIndex))
            If CInt(e.Item.Cells(15).Text) = 2 Then
                Dim objPending As New clspendingleave_Tran

                Dim strFilter As String = " lvpendingleave_tran.formunkid = " & mintFormUnkId & _
                                          " AND lvpendingleave_tran.employeeunkid = " & CInt(e.Item.Cells(16).Text) & _
                                          " AND lvpendingleave_tran.statusunkid <> 2 "
                Dim dsPendingList As DataSet = objPending.GetList("Pending", _
                                                                  CStr(Session("Database_Name")), _
                                                                  CInt(Session("UserId")), _
                                                                  CInt(Session("Fin_year")), _
                                                                  CInt(Session("CompanyUnkId")), _
                                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), True, _
                                                                  CBool(Session("IsIncludeInactiveEmp")), True, strFilter)


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objPending = Nothing
                'Pinkal (11-Sep-2020) -- End


                If dsPendingList IsNot Nothing AndAlso dsPendingList.Tables(0).Rows.Count > 0 Then
                    DisplayMessage.DisplayMessage("You can not Delete this form.Reason:It Is already approved/Rejected/Rescheduled by some of the approver(s).", Me)
                    Exit Sub
                End If


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsPendingList IsNot Nothing Then dsPendingList.Clear()
                dsPendingList = Nothing
                'Pinkal (11-Sep-2020) -- End


                'Pinkal (28-Nov-2017) -- Start
                'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                mintLeaveApplicationEmpID = CInt(e.Item.Cells(16).Text)  'Employee Id
                mintLeaveTypeID = CInt(e.Item.Cells(17).Text) 'Leave Type ID
                'Pinkal (28-Nov-2017) -- End

            End If

            'Pinkal (20-Nov-2018) -- Start
            'Enhancement - Working on P2P Integration for NMB.
            '/* START FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
            If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim().Length > 0 Then
                Dim objClaimRequestMst As New clsclaim_request_master
                Dim mintClaimMstID As Integer = objClaimRequestMst.GetClaimRequestMstIDFromLeaveForm(mintFormUnkId)
                objClaimRequestMst._Crmasterunkid = mintClaimMstID
                objClaimRequestMst = Nothing

                Dim objClaim As New clsclaim_request_tran
                objClaim._ClaimRequestMasterId = mintClaimMstID

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Dim dtTable As DataTable = objClaim._DataTable
                Dim dtTable As DataTable = objClaim._DataTable.Copy()
                'Pinkal (11-Sep-2020) -- End
                objClaim = Nothing

                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    Dim xBudgetMandatoryCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True).Count
                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'If xBudgetMandatoryCount > 0 Then
                    Dim xNonHRExpenseCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False).Count
                    If xBudgetMandatoryCount > 0 AndAlso xNonHRExpenseCount > 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Sorry, you cannot delete this leave application.Reason:This leave application is attached with expense application.Please contact your administrator to do futher operation on it."), Me)
                        Exit Sub
                    End If
                    'Pinkal (04-Feb-2019) --  End
                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtTable IsNot Nothing Then dtTable.Clear()
                dtTable = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If
            '/* END FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
            'Pinkal (20-Nov-2018) -- End


            If (CInt(e.Item.Cells(15).Text) <> 2) Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("You can not Delete Entry.", Me)
                'Sohail (23 Mar 2019) -- End
            Else

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Dim clsleaveform As New clsleaveform
                'Pinkal (11-Sep-2020) -- End
                clsleaveform._Formunkid = mintFormUnkId
                If clsleaveform._Statusunkid <> 2 Then
                    DisplayMessage.DisplayMessage("You Cannot Delete this Entry.Reason : This form status is not Pending.", Me)
                    Exit Sub

                    'Pinkal (03-Nov-2017) -- Start
                    'Enhancement - Ref id 90  Working on Allow to delete /edit leave form if is on past date, both user and employee should be able to delete this form for past dates if not a single approver has approved..
                    'ElseIf clsLeaveForm._Statusunkid = 2 AndAlso clsLeaveForm._Startdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                    '    DisplayMessage.DisplayMessage("You Cannot Delete this Entry.Reason : This form Start Date or End Date is already over.", Me)
                    '    Exit Sub
                    'Pinkal (03-Nov-2017) -- End

                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                clsleaveform = Nothing
                'Pinkal (11-Sep-2020) -- End

                popup_DeleteForm.Reason = ""
                popup_DeleteForm.Show()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.ItemCommand
        Dim ds As DataSet = Nothing
        Try
            If e.CommandName.ToUpper = "SELECT" Then
                Dim clsLeaveForm As New clsleaveform
                Dim dtLeaveForm As DataTable = Nothing

                Blank_ModuleName()

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.

                'ds = clsLeaveForm.GetList("Leaveform", _
                '                         Session("Database_Name").ToString, _
                '                         CInt(Session("UserId")), _
                '                         CInt(Session("Fin_year")), _
                '                         CInt(Session("CompanyUnkId")), _
                '                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                '                         Session("UserAccessModeSetting").ToString, True, _
                '                         CBool(Session("IsIncludeInactiveEmp")), True)

                Dim mstrSearch = "AND lvleaveform.formunkid = " & dgView.DataKeys(e.Item.ItemIndex).ToString

                ds = clsLeaveForm.GetList("Leaveform", _
                                          Session("Database_Name").ToString, _
                                          CInt(Session("UserId")), _
                                          CInt(Session("Fin_year")), _
                                          CInt(Session("CompanyUnkId")), _
                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                          Session("UserAccessModeSetting").ToString, True, _
                                         CBool(Session("IsIncludeInactiveEmp")), True, -1, True, -1, mstrSearch)

                'Pinkal (11-Sep-2020) -- End

                Dim drLeaveForm() As DataRow = ds.Tables(0).Select("formunkid = " & dgView.DataKeys(e.Item.ItemIndex).ToString)

                Dim mdtStartdate As DateTime = Nothing
                If drLeaveForm.Length <= 0 Then
                    mdtStartdate = CDate(e.Item.Cells(8).Text)
                Else
                    mdtStartdate = eZeeDate.convertDate(drLeaveForm(0)("startdate").ToString()).Date
                End If


                If CInt(e.Item.Cells(15).Text) = 1 Then
                    DisplayMessage.DisplayMessage("This Form cannot be Edited.", Me)
                    Exit Sub


                    'Pinkal (03-Nov-2017) -- Start
                    'Enhancement - Ref id 90  Working on Allow to delete /edit leave form if is on past date, both user and employee should be able to delete this form for past dates if not a single approver has approved..
                    'ElseIf CInt(e.Item.Cells(15).Text) = 2 And mdtStartdate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                    '    DisplayMessage.DisplayMessage("This Form cannot be Edited.", Me)
                    '    Exit Sub
                    'Pinkal (03-Nov-2017) -- End

                ElseIf CInt(e.Item.Cells(15).Text) = 3 Or CInt(e.Item.Cells(15).Text) = 4 Or CInt(e.Item.Cells(15).Text) = 6 Or CInt(e.Item.Cells(15).Text) = 7 Then
                    DisplayMessage.DisplayMessage("This Form cannot be Edited.", Me)
                    Exit Sub
                End If


                If CInt(e.Item.Cells(15).Text) = 2 Then
                    Dim objPending As New clspendingleave_Tran

                    Dim strFilter As String = "lvpendingleave_tran.formunkid = " & dgView.DataKeys(e.Item.ItemIndex).ToString & _
                                                                      " AND lvpendingleave_tran.employeeunkid = " & CInt(e.Item.Cells(16).Text) & _
                                                                      " AND lvpendingleave_tran.statusunkid <> 2 "

                    Dim dsPendingList As DataSet = objPending.GetList("Pending", _
                                                                      Session("Database_Name").ToString, _
                                                                      CInt(Session("UserId")), _
                                                                      CInt(Session("Fin_year")), _
                                                                      CInt(Session("CompanyUnkId")), _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), True, _
                                                                      CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                       strFilter)
                    If dsPendingList IsNot Nothing AndAlso dsPendingList.Tables(0).Rows.Count > 0 Then
                        DisplayMessage.DisplayMessage("This Form cannot be Edited.Reason:It Is already approved/Rejected/Rescheduled by some of the approver(s).", Me)
                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        If dsPendingList IsNot Nothing Then dsPendingList.Clear()
                        dsPendingList = Nothing
                        objPending = Nothing
                        'Pinkal (11-Sep-2020) -- End
                        Exit Sub
                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsPendingList IsNot Nothing Then dsPendingList.Clear()
                    dsPendingList = Nothing
                    objPending = Nothing
                    'Pinkal (11-Sep-2020) -- End

                End If


                Session("formunkid") = dgView.DataKeys(e.Item.ItemIndex)
                Response.Redirect(Session("rootpath").ToString & "Leave/wPg_LeaveFormAddEdit.aspx", False)

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtLeaveForm IsNot Nothing Then dtLeaveForm.Clear()
                dtLeaveForm = Nothing
                If ds IsNot Nothing Then ds.Clear()
                ds = Nothing
                clsLeaveForm = Nothing
                'Pinkal (11-Sep-2020) -- End


            ElseIf e.CommandName.ToUpper = "CANCEL" Then
                Dim blnClose As Boolean = False
                Dim objPeriod As New clscommom_period_Tran
                Dim objMaster As New clsMasterData


                'Pinkal (28-Nov-2017) -- Start
                'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .

                'If CInt(e.Item.Cells(15).Text) <> 7 Then
                '    DisplayMessage.DisplayMessage("You can not Cancel this Leave form.Reason: This Leave form has not been in Issued status.", Me)
                '    Exit Sub
                'End If

                If CInt(e.Item.Cells(15).Text) <> 7 AndAlso CInt(e.Item.Cells(15).Text) <> 6 Then
                    DisplayMessage.DisplayMessage("You can not Cancel this Leave form.Reason: This Leave form has not been in Issued status.", Me)
                    Exit Sub
                ElseIf CInt(e.Item.Cells(15).Text) = 6 Then
                    DisplayMessage.DisplayMessage("You can not Cancel this Leave form.Reason: This Leave form has already cancelled.", Me)
                    Exit Sub
                End If

                'Pinkal (28-Nov-2017) -- End

                'Pinkal (20-Nov-2018) -- Start
                'Enhancement - Working on P2P Integration for NMB.

                '/* START FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim().Length > 0 Then
                    Dim objClaimRequestMst As New clsclaim_request_master
                    Dim mintClaimMstID As Integer = objClaimRequestMst.GetClaimRequestMstIDFromLeaveForm(CInt(dgView.DataKeys(e.Item.ItemIndex)))
                    objClaimRequestMst._Crmasterunkid = mintClaimMstID
                    objClaimRequestMst = Nothing

                    Dim objClaim As New clsclaim_request_tran
                    objClaim._ClaimRequestMasterId = mintClaimMstID

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'Dim dtTable As DataTable = objClaim._DataTable
                    Dim dtTable As DataTable = objClaim._DataTable.Copy()
                    'Pinkal (11-Sep-2020) -- End
                    objClaim = Nothing

                    If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                        Dim xBudgetMandatoryCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isbudgetmandatory") = True).Count
                        'Pinkal (04-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        'If xBudgetMandatoryCount > 0 Then
                        Dim xNonHRExpenseCount As Integer = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Boolean)("ishrexpense") = False).Count
                        If xBudgetMandatoryCount > 0 AndAlso xNonHRExpenseCount > 0 Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Sorry, you cannot cancel this leave application.Reason:This leave application is attached with expense application.Please contact your administrator to do futher operation on it."), Me)
                            Exit Sub
                        End If
                        'Pinkal (04-Feb-2019) -- End
                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtTable IsNot Nothing Then dtTable.Clear()
                    dtTable = Nothing
                    'Pinkal (11-Sep-2020) -- End

                End If
                '/* END FOR CHECK CLAIM FOR HAVING IS BUDGET MANDATORY OPTION IF YES THEN DISABLED EDIT,DELETE AND CANCEL EXPENSE OPTION AS PER RUTTA COMMENT.


                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                'Pinkal (13-Jun-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.[Matthew, 10:45 We discussed this yesterday. Remove the validation on cancelling leave. Only validate during application Yes, i was with Rutta and NMB guy]
                'Dim objLvType As New clsleavetype_master
                'objLvType._Leavetypeunkid = CInt(e.Item.Cells(17).Text) 'LeaveTypeId
                'If objLvType._DeductFromLeaveTypeunkid > 0 Then
                '    objLvType._Leavetypeunkid = objLvType._DeductFromLeaveTypeunkid
                '    If clsLeaveForm.IsLeaveForm_Exists(CInt(e.Item.Cells(16).Text), objLvType._Leavetypeunkid, True) Then
                '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 15, "You cannot cancel this leave form for this leave type.Reason : This leave type is configured as deducted from") & " " & _
                '                                                   objLvType._Leavename & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 16, "which has been already in Pending/approved/issued status."), Me)
                '        Exit Sub
                '    End If
                'End If
                'objLvType = Nothing
                'Pinkal (13-Jun-2019) -- End

                'Pinkal (26-Feb-2019) -- End


                'Pinkal (30-May-2020) -- Start
                'Enhancement NMB - Leave Cancellation Option put on Configuration and implementing on Leave Form List screen.

                If (CBool(Session("IsArutiDemo")) = True OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management)) AndAlso CBool(Session("AllowToCancelLeaveForClosedPeriod")) = False Then
                    Dim mblnStartDateClose As Boolean = False
                    Dim mblnEndDateClose As Boolean = False

                    Dim intstPerioId As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Parse(e.Item.Cells(8).Text, CultureInfo.InvariantCulture).Date, CInt(Session("Fin_year")), 0)
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = intstPerioId
                    If objPeriod._Statusid = 2 Then mblnStartDateClose = True


                    Dim intEdPerioId As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, DateTime.Parse(e.Item.Cells(9).Text, CultureInfo.InvariantCulture).Date, CInt(Session("Fin_year")), 0)
                    objPeriod._Periodunkid(CStr(Session("Database_Name"))) = intEdPerioId
                    If objPeriod._Statusid = 2 Then mblnEndDateClose = True

                    If mblnStartDateClose = True OrElse mblnEndDateClose = True Then
                        'DisplayMessage.DisplayMessage("This Period is already closed.You can not cancel this leave.", Me) '?1
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "You can not cancel this leave application.Reason: Leave Date(s) are fall in period which is already closed."), Me)
                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objMaster = Nothing
                        objPeriod = Nothing
                        'Pinkal (11-Sep-2020) -- End
                        Exit Sub
                    End If

                End If
                'Pinkal (30-May-2020) -- End


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objMaster = Nothing
                objPeriod = Nothing
                'Pinkal (11-Sep-2020) -- End


                Dim objIssue As New clsleaveissue_Tran
                Dim dsList As DataSet = objIssue.GetList("List", False, CInt(dgView.DataKeys(e.Item.ItemIndex)))

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objIssue = Nothing
                'Pinkal (11-Sep-2020) -- End


                Dim drRow As DataRow() = Nothing
                If CBool(Session("AllowToCancelPreviousDateLeave")) = False AndAlso CBool(Session("AllowToCancelLeave")) = True Then
                    drRow = dsList.Tables(0).Select("leavedate >= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) & "' AND isvoid = 0")
                ElseIf CBool(Session("AllowToCancelPreviousDateLeave")) = True AndAlso CBool(Session("AllowToCancelLeave")) = False Then
                    drRow = dsList.Tables(0).Select("leavedate <= '" & eZeeDate.convertDate(ConfigParameter._Object._CurrentDateAndTime.Date) & "' AND isvoid = 0")
                ElseIf CBool(Session("AllowToCancelPreviousDateLeave")) = True AndAlso CBool(Session("AllowToCancelLeave")) = True Then
                    drRow = dsList.Tables(0).Select("formunkid = " & CInt(dgView.DataKeys(e.Item.ItemIndex)) & " AND isvoid = 0")
                End If

                If drRow.Length > 0 Then
                    txtLeaveFormNo.Text = ""
                    txtCancelEmployeeCode.Text = ""
                    txtCancelEmployee.Text = ""
                    txtCancelLeaveType.Text = ""
                    txtCancelFormNo.Text = ""
                    txtCancelReason.Text = ""
                    chkSelectAll.Checked = False

                    'Pinkal (03-May-2019) -- Start
                    'Enhancement - Working on Leave UAT Changes for NMB.
                    If CInt(Session("LeaveCancelFormNotype")) = 1 Then
                        txtCancelFormNo.Enabled = False
                    Else
                        txtCancelFormNo.Enabled = True
                    End If
                    'Pinkal (03-May-2019) -- End

                    txtLeaveFormNo.Text = e.Item.Cells(4).Text
                    txtCancelEmployeeCode.Text = e.Item.Cells(5).Text
                    txtCancelEmployee.Text = e.Item.Cells(6).Text
                    txtCancelLeaveType.Text = e.Item.Cells(7).Text

                    GetLeaveDate(dsList, CInt(dgView.DataKeys(e.Item.ItemIndex)))

                    mintCancelFormId = CInt(dgView.DataKeys(e.Item.ItemIndex))  'FOR LEAVE FORM UNKID
                    mintCancelEmpUnkId = CInt(e.Item.Cells(16).Text)   'FOR EMPLOYEE
                    mintCancelLeaveTypeUnkId = CInt(e.Item.Cells(17).Text)   'FOR LEAVE TYPE


                    'Pinkal (01-Oct-2018) -- Start
                    'Enhancement - Leave Enhancement for NMB.
                    mblnLvTypeSkipApproverFlow = CBool(e.Item.Cells(18).Text) 'FOR SKIP APPROVER FLOW
                    'Pinkal (01-Oct-2018) -- End


                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    Dim objClaimRequestMst As New clsclaim_request_master
                    'Pinkal (11-Sep-2020) -- End
                    Dim mintClaimstId As Integer = objClaimRequestMst.GetClaimRequestMstIDFromLeaveForm(mintCancelFormId)

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objClaimRequestMst = Nothing
                    'Pinkal (11-Sep-2020) -- End

                    mintClaimMstUnkId = mintClaimstId
                    If mintClaimstId > 0 Then
                        lnkCancelExpense.Visible = True
                    Else
                        lnkCancelExpense.Visible = False
                    End If



                    'Pinkal (30-Mar-2020) -- Start
                    'Enhancement - Changes Related to Retain Leave Expense when leave is cancel.
                    If CBool(Session("AllowToCancelLeaveButRetainExpense")) Then
                        lnkCancelExpense.Visible = False
                    End If
                    'Pinkal (30-Mar-2020) -- End


                    ''Pinkal (28-Nov-2017) -- Start
                    ''Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .

                    If mdtAttachement Is Nothing Then
                        Dim objAttchement As New clsScan_Attach_Documents
                        'S.SANDEEP |04-SEP-2021| -- START
                        'ISSUE : TAKING CARE FROM SLOWNESS QUERY
                        'Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", mintCancelEmpUnkId)
                        Call objAttchement.GetList(Session("Document_Path").ToString(), "List", "", mintCancelEmpUnkId, False, Nothing, Nothing, "", CBool(IIf(mintCancelEmpUnkId <= 0, True, False)))
                        'S.SANDEEP |04-SEP-2021| -- END

                        Dim mdtTran As DataTable = clsleaveform.GetLeaveForms(mintCancelEmpUnkId, False, mintCancelFormId).Tables(0)
                        Dim strTranIds As String = String.Join(",", mdtTran.AsEnumerable().Select(Function(x) x.Field(Of String)("cancelunkid").ToString()).ToArray())
                        If strTranIds.Trim.Length <= 0 Then strTranIds = "0"
                        mdtAttachement = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.LEAVEFORMS & " AND transactionunkid IN (" & strTranIds & ")  AND form_name = '" & mstrModuleName4 & "'", "", DataViewRowState.CurrentRows).ToTable
                        objAttchement = Nothing
                    End If

                    mblnCancelLvFormPopup = True

                    ''Pinkal (28-Nov-2017) -- End


                    'Pinkal (10-Jun-2020) -- Start
                    'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
                    GetBalanceInfo()
                    'Pinkal (10-Jun-2020) -- End

                    popupCancel.Show()
                Else
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "There is no Leave data to cancel the leave form."), Me)
                    Exit Sub
                End If

            ElseIf e.CommandName.ToUpper = "PRINT" Then

                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                SetDateFormat()
                'Pinkal (16-Apr-2016) -- End

                Dim objLeaveFormRpt As New ArutiReports.clsEmployeeLeaveForm(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))
                objLeaveFormRpt._LeaveFormId = CInt(dgView.DataKeys(e.Item.ItemIndex))
                objLeaveFormRpt._LeaveFormNo = e.Item.Cells(4).Text
                objLeaveFormRpt._EmployeeId = CInt(e.Item.Cells(16).Text)
                objLeaveFormRpt._LeaveTypeId = CInt(e.Item.Cells(17).Text)
                objLeaveFormRpt._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
                objLeaveFormRpt._YearId = CInt(Session("Fin_year"))
                objLeaveFormRpt._CompanyUnkId = CInt(Session("CompanyUnkId"))
                objLeaveFormRpt._UserUnkId = CInt(Session("UserId"))
                GUI.fmtCurrency = Session("fmtCurrency").ToString
                objLeaveFormRpt.generateReportNew(CStr(Session("Database_Name")), _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("Fin_year")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                  eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                  Session("UserAccessModeSetting").ToString, True, _
                                                  Session("ExportReportPath").ToString, _
                                                  CBool(Session("OpenAfterExport")), _
                                                  0, enPrintAction.None, enExportAction.None, _
                                                  CInt(Session("Base_CurrencyId")))
                Session("objRpt") = objLeaveFormRpt._Rpt
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_Report", "ShowReportNewTab();", True)

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objLeaveFormRpt = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgView.ItemDataBound
        Try
            SetDateFormat()

            If e.Item.ItemIndex >= 0 Then

                e.Item.Cells(8).Text = eZeeDate.convertDate(e.Item.Cells(8).Text).Date.ToShortDateString

                If e.Item.Cells(9).Text.ToString().Trim.Length > 0 AndAlso e.Item.Cells(9).Text.ToString().Trim <> "&nbsp;" Then
                    e.Item.Cells(9).Text = eZeeDate.convertDate(e.Item.Cells(9).Text).Date.ToShortDateString
                End If

                If e.Item.Cells(11).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(11).Text.Trim <> "" Then
                    e.Item.Cells(11).Text = eZeeDate.convertDate(e.Item.Cells(11).Text).Date.ToShortDateString
                End If

                If e.Item.Cells(12).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(12).Text.Trim <> "" Then
                    e.Item.Cells(12).Text = eZeeDate.convertDate(e.Item.Cells(12).Text).Date.ToShortDateString
                End If

                If e.Item.Cells(13).Text.Trim <> "&nbsp;" AndAlso e.Item.Cells(13).Text.Trim <> "" Then
                    e.Item.Cells(13).Text = Math.Round(CDec(e.Item.Cells(13).Text), 2).ToString
                Else
                    e.Item.Cells(13).Text = "0.00"
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvCancelExpData_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvCancelExpData.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(4).Text.Trim.Length > 0 Then
                    e.Row.Cells(4).Text = Format(CDec(e.Row.Cells(4).Text), Session("fmtCurrency").ToString)
                End If
                If e.Row.Cells(5).Text.Trim.Length > 0 Then
                    e.Row.Cells(5).Text = Format(CDec(e.Row.Cells(5).Text), Session("fmtCurrency").ToString)
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvCancelExpData_RowDataBound :" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (28-Nov-2017) -- Start
    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .

    Protected Sub dgv_Attchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgv_Attchment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow

                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                'If CInt(e.Item.Cells(3).Text > 0 Then
                '    xrow = mdtAttachement.Select("scanattachtranunkid = " & CInt(e.Item.Cells(3).Text) & "")
                'Else
                '    xrow = mdtAttachement.Select("GUID = '" & e.Item.Cells(2).Text.Trim & "'")
                'End If

                'If e.CommandName = "Delete" Then
                '    If xrow.Length > 0 Then
                '        popup_AttachementYesNo.Message = "Are you sure you want to delete this attachment?"
                '        mintDeleteIndex = mdtAttachement.Rows.IndexOf(xrow(0))
                '        popup_AttachementYesNo.Show()
                '    End If
                'End If

                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtAttachement.Select("scanattachtranunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtAttachement.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhGUID", False, True)).Text.Trim & "'")
                End If

                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_AttachementYesNo.Message = "Are you sure you want to delete this attachment?"
                        mintDeleteIndex = mdtAttachement.Rows.IndexOf(xrow(0))
                        popup_AttachementYesNo.Show()
                    End If
                ElseIf e.CommandName = "Download" Then
                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        Dim xPath As String = ""

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("orgfilepath").ToString
                        End If

                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                DisplayMessage.DisplayMessage("File does not Exist...", Me)
                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & xrow(0)("filename").ToString() & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
                'S.SANDEEP |16-MAY-2019| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'Pinkal (28-Nov-2017) -- End


#End Region

#Region "Link Event(S)"

    Protected Sub lnkCancelExpense_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCancelExpense.Click
        Try
            popup_YesNo.Title = "Aruti"
            popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, 7, "You are about to cancel leave expense form, are you sure you want to cancel leave expense form ?")

            popupCancel.Show()
            popup_YesNo.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Event(S)"

    Protected Sub chkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            For i As Integer = 0 To chkListLeavedate.Items.Count - 1
                chkListLeavedate.Items(i).Selected = chkSelectAll.Checked
            Next
            popupCancel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkCancelExpAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = CType(sender, CheckBox)
            If dgvCancelExpData.Rows.Count <= 0 Then Exit Sub

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdtTran As DataTable = mdtCanceExpTable
            Dim mdtTran As DataTable = mdtCanceExpTable.Copy()
            'Pinkal (11-Sep-2020) -- End

            If mdtTran.Rows.Count <= 0 Then Exit Sub
            For Each xRow As GridViewRow In dgvCancelExpData.Rows
                If CBool(mdtTran.Rows(xRow.RowIndex)("IsPosted")) = True Then
                    CType(xRow.FindControl("ChkCancelExpSelect"), CheckBox).Checked = False
                Else
                    mdtTran.Rows(xRow.RowIndex)("ischeck") = CBool(cb.Checked)
                    CType(xRow.FindControl("ChkCancelExpSelect"), CheckBox).Checked = cb.Checked
                End If
            Next
            mdtTran.AcceptChanges()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'mdtCanceExpTable = mdtTran
            mdtCanceExpTable = mdtTran.Copy()

            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (11-Sep-2020) -- End


            popupCancel.Show()
            popup_CancelExp.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkAll_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub ChkCancelExpSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chk As CheckBox = CType(sender, CheckBox)
            Dim xRow As GridViewRow = CType(chk.NamingContainer, GridViewRow)

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdtTran As DataTable = mdtCanceExpTable
            Dim mdtTran As DataTable = mdtCanceExpTable.Copy()
            'Pinkal (11-Sep-2020) -- End

            If mdtTran.Rows.Count <= 0 Then Exit Sub
            If CBool(mdtTran.Rows(xRow.RowIndex)("IsPosted")) = True Then
                DisplayMessage.DisplayMessage("You cannot cancel this claim transaction.Reason:This claim transaction is already posted to current payroll period.", Me)
                chk.Checked = False
            Else
                mdtTran.Rows(xRow.RowIndex)("ischeck") = CBool(chk.Checked)
                mdtTran.AcceptChanges()
            End If

            Dim drRow() As DataRow = mdtTran.Select("ischeck = True")

            If drRow.Length > 0 Then
                If drRow.Length = dgvCancelExpData.Rows.Count Then
                    CType(dgvCancelExpData.HeaderRow.FindControl("chkCancelExpAll"), CheckBox).Checked = True
                Else
                    CType(dgvCancelExpData.HeaderRow.FindControl("chkCancelExpAll"), CheckBox).Checked = False
                End If
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'mdtCanceExpTable = mdtTran
            mdtCanceExpTable = mdtTran.Copy()

            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (11-Sep-2020) -- End


            popupCancel.Show()
            popup_CancelExp.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("chkAll_CheckedChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "CheckListBox Event"

    Protected Sub chkListLeavedate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkListLeavedate.TextChanged
        Try
            Dim listitems As New ListItemCollection
            For i As Integer = 0 To chkListLeavedate.Items.Count - 1
                If (chkListLeavedate.Items(i).Selected) Then
                    listitems.Add(chkListLeavedate.Items(i).Value)
                End If
            Next
            If listitems.Count = chkListLeavedate.Items.Count Then
                chkSelectAll.Checked = True
            Else
                chkSelectAll.Checked = False
            End If
            popupCancel.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, "Leave Form List")
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, "Leave Form List")

            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblFormNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFormNo.ID, Me.lblFormNo.Text)
            Me.lblApplyDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApplyDate.ID, Me.lblApplyDate.Text)
            Me.lblReturnDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblReturnDate.ID, Me.lblReturnDate.Text)
            Me.lblStartDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStartDate.ID, Me.lblStartDate.Text)
            Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblStatus.ID, Me.lblStatus.Text)
            Me.lblLeaveType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLeaveType.ID, Me.lblLeaveType.Text)



            Me.BtnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.BtnNew.ID, Me.BtnNew.Text).Replace("&", "")
            Me.Btnclose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.Btnclose.ID, Me.Btnclose.Text).Replace("&", "")


            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'dgView.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgView.Columns(0).FooterText, dgView.Columns(0).HeaderText).Replace("&", "")
            'dgView.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgView.Columns(1).FooterText, dgView.Columns(1).HeaderText).Replace("&", "")
            'dgView.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgView.Columns(2).FooterText, dgView.Columns(2).HeaderText).Replace("&", "")
            'dgView.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),dgView.Columns(3).FooterText, dgView.Columns(3).HeaderText).Replace("&", "")
            'Gajanan [17-Sep-2020] -- End

            dgView.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(4).FooterText, dgView.Columns(4).HeaderText).Replace("&", "")
            dgView.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(5).FooterText, dgView.Columns(5).HeaderText).Replace("&", "")
            dgView.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(6).FooterText, dgView.Columns(6).HeaderText).Replace("&", "")
            dgView.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(7).FooterText, dgView.Columns(7).HeaderText).Replace("&", "")
            dgView.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(8).FooterText, dgView.Columns(8).HeaderText).Replace("&", "")
            dgView.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(9).FooterText, dgView.Columns(9).HeaderText).Replace("&", "")
            dgView.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(10).FooterText, dgView.Columns(10).HeaderText).Replace("&", "")
            dgView.Columns(11).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(11).FooterText, dgView.Columns(11).HeaderText).Replace("&", "")
            dgView.Columns(12).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(12).FooterText, dgView.Columns(12).HeaderText).Replace("&", "")
            dgView.Columns(13).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(13).FooterText, dgView.Columns(13).HeaderText).Replace("&", "")
            dgView.Columns(14).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(14).FooterText, dgView.Columns(14).HeaderText).Replace("&", "")

            'Language.setLanguage(mstrModuleName4)


            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lblCancelText.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName4,CInt(HttpContext.Current.Session("LangId")),mstrModuleName4, Me.lblCancelText.Text)
            'Gajanan [17-Sep-2020] -- End

            Me.lblCancelText1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), mstrModuleName4, Me.lblCancelText1.Text)

            Me.lblCancelEmployeeCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblCancelEmployeeCode.ID, Me.lblCancelEmployeeCode.Text)
            Me.lblCancelFormNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblCancelFormNo.ID, Me.lblCancelFormNo.Text)
            Me.lblCancelEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), "lblEmployee", Me.lblCancelEmployee.Text)
            Me.lblCancelLeaveType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), "lblLeaveType", Me.lblCancelLeaveType.Text)
            Me.lblCancelReason.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), "lblReason", Me.lblCancelReason.Text)

            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            'Me.lblCancelFormNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName4,CInt(HttpContext.Current.Session("LangId")),"lblFormNo", Me.lblCancelFormNo.Text)
            Me.lblLeaveFormNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), "lblFormNo", Me.lblCancelFormNo.Text)
            'Pinkal (03-May-2019) -- End

            Me.lblLeaveFormDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblLeaveFormDate.ID, Me.lblLeaveFormDate.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            Me.chkSelectAll.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.chkSelectAll.ID, Me.chkSelectAll.Text)
            If Me.chkSelectAll.Text.Trim.Length <= 0 Then
                Me.chkSelectAll.Text = " "
            End If
            'Gajanan [17-Sep-2020] -- End

            Me.lnkCancelExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lnkCancelExpense.ID, Me.lnkCancelExpense.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.TabPanel1.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName4,CInt(HttpContext.Current.Session("LangId")),Me.TabPanel1.ID, Me.TabPanel1.HeaderText)
            'Gajanan [17-Sep-2020] -- End


            Me.btnCancelSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), "btnSave", Me.btnCancelSave.Text).Replace("&", "")
            Me.btnCancelClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), "Btnclose", Me.btnCancelClose.Text).Replace("&", "")

            'Hemant (30 Nov 2018) -- Start
            'Enhancement : Including Language Settings For Scan/Attachment Button
            Me.btnCancelScanAttachment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), "btnAttachDocument", Me.btnCancelScanAttachment.Text).Replace("&", "")
            'Hemant (30 Nov 2018) -- End



            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            Me.lblDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblDescription.ID, Me.lblDescription.Text)
            Me.lblAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblAmount.ID, Me.lblAmount.Text)
            Me.LblLeaveBF.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.LblLeaveBF.ID, Me.LblLeaveBF.Text)
            Me.lblToDateAccrued.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblToDateAccrued.ID, Me.lblToDateAccrued.Text)
            Me.lblTotalIssuedToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblTotalIssuedToDate.ID, Me.lblTotalIssuedToDate.Text)
            Me.LblTotalAdjustment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.LblTotalAdjustment.ID, Me.LblTotalAdjustment.Text)
            Me.LblBalanceAsonDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.LblBalanceAsonDate.ID, Me.LblBalanceAsonDate.Text)
            Me.lblBalance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, CInt(HttpContext.Current.Session("LangId")), Me.lblBalance.ID, Me.lblBalance.Text)
            'Pinkal (10-Jun-2020) -- End


        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Cancel Remark cannot be blank.Cancel Remark is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Please Select atleast One Expense to cancel.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "There is no Leave data to cancel the leave form.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "Cancel Leave Form has mandatory document attachment. please attach document.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "Configuration Path does not Exist.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "Sorry, you cannot cancel this leave application.Reason:This leave application is attached with expense application.Please contact your administrator to do futher operation on it.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Sorry, you cannot delete this leave application.Reason:This leave application is attached with expense application.Please contact your administrator to do futher operation on it.")

            'Pinkal (30-May-2020) -- Start
            'Enhancement NMB - Leave Cancellation Option put on Configuration and implementing on Leave Form List screen.
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "You can not cancel this leave application.Reason: Leave Date(s) are fall in period which is already closed.")
            'Pinkal (30-May-2020) -- End

            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 1, "Cancel Form No cannot be blank. Cancel Form No is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 2, "Leave Date is compulsory information.Please Select atleast One Leave Date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 3, "Reason cannot be blank. Reason is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 6, "There is no Expense Data to Cancel the Expense Form.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 7, "You are about to cancel leave expense form, are you sure you want to cancel leave expense form ?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 9, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to cancel this leave ?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 13, "Selected information is already present for particular employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 15, "Sorry, you cannot cancel leave days as it is going below as compared to")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 16, "days configured on Leave Application Leave days limit setting for this leave type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName4, 17, "Sorry, You can not perform delete opration, Reason:This Entry Is Already Present In Approval Process.")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
