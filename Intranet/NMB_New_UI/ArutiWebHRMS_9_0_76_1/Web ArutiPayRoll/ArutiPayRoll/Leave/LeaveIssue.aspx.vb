﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data

#End Region

Partial Class LeaveIssue
    Inherits Basepage

#Region " Private Variables "

    Private mintL_FormId As Integer
    Private clsuser As New User
    Private DisplayMessage As New CommonCodes
    Private objLForm As New clsleaveform
    Private objLIssue As New clsleaveissue_master
    Private objLIssueTran As New clsleaveissue_Tran
    Dim objDataOperation As clsDataOperation
    Private mdic_ArryDays As New Dictionary(Of String, String)
    Dim objLeaveFraction As New clsleaveday_fraction
    Dim mStrYear As String = ""

    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private ReadOnly mstrModuleName As String = "frmLeaveIssue_AddEdit"
    'Pinkal (06-May-2014) -- End

    'Pinkal (01-Mar-2016) -- Start
    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
    Private mblnIsExternalApprover As Boolean = False
    'Pinkal (01-Mar-2016) -- End

    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mblnIsLvTypePaid As Boolean = False
    Private mintLvformMinDays As Integer = 0
    Private mintLvformMaxDays As Integer = 0
    'Pinkal (08-Oct-2018) -- End

    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
    Private mblnConsiderLeaveOnTnAPeriod As Boolean = False
    'Pinkal (01-Jan-2019) -- End

#End Region

#Region " Page Events "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If


            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            objDataOperation = New clsDataOperation()

            If IsPostBack = False Then
                SetLanguage()
                Call FillCombo()
                mblnIsExternalApprover = CBool(Session("IsExternalApprover"))
            Else
                mblnIsExternalApprover = CBool(Me.ViewState("IsExternalApprover"))

                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                mblnIsLvTypePaid = CBool(Me.ViewState("LvTypeIsPaid"))
                mintLvformMinDays = CInt(Me.ViewState("LVDaysMinLimit"))
                mintLvformMaxDays = CInt(Me.ViewState("LVDaysMaxLimit"))
                'Pinkal (08-Oct-2018) -- End

                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                mblnConsiderLeaveOnTnAPeriod = CBool(Me.ViewState("ConsiderLeaveOnTnAPeriod"))
                'Pinkal (01-Jan-2019) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
        Try
            If Not IsPostBack Then
                IsFrom_Process_List()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("IsExternalApprover") = mblnIsExternalApprover

            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            Me.ViewState("LvTypeIsPaid") = mblnIsLvTypePaid
            Me.ViewState("LVDaysMinLimit") = mintLvformMinDays
            Me.ViewState("LVDaysMaxLimit") = mintLvformMaxDays
            'Pinkal (08-Oct-2018) -- End


            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            Me.ViewState("ConsiderLeaveOnTnAPeriod") = mblnConsiderLeaveOnTnAPeriod
            'Pinkal (01-Jan-2019) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#End Region

#Region " Buttons Events "

    Protected Sub Btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..
            'Response.Redirect("~/Leave/ProcessPendingLeave.aspx")
            Response.Redirect(Session("rootpath").ToString & "Leave/wPg_ProcessLeaveList.aspx", False)
            'Pinkal (16-Dec-2016) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Dim mdblLeaveAmout As Decimal = 0
        Try

            If Me.ViewState("L_FormId") IsNot Nothing AndAlso CInt(Me.ViewState("L_FormId")) > 0 Then
                objLForm._Formunkid = CInt(Me.ViewState("L_FormId"))
                If objLForm._Statusunkid = 7 Then
                    DisplayMessage.DisplayMessage("This Leave Form is already issued.You can't do any operation on it.", Me, "ProcessPendingLeave.aspx")
                    Exit Sub
                End If
            End If


            If txtFormNo.Text.Trim.Length <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Form No is compulsory information.Please Select Form No."), Me)
                Exit Sub
            End If
            If txtLeaveType.Text.Trim.Length <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "Leave Type is compulsory information.Please Select Leave Type."), Me)
                Exit Sub
            End If

            If Me.ViewState("L_Days") Is Nothing Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Leave Date is compulsory information.Please Select Leave Date between given Leave Form Period."), Me)
                Exit Sub
            End If

            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If mblnIsLvTypePaid Then
                If mintLvformMinDays <> 0 And mintLvformMinDays > CDec(lblDays.Text) Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 26, "Sorry, you cannot approve/Issue for this leave type as it has been configured with setting for minimum") & " " & mintLvformMinDays & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "day(s) to be applied for this leave type."), Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
                If mintLvformMaxDays <> 0 And mintLvformMaxDays < CDec(lblDays.Text) Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 27, "Sorry, you cannot approve/Issue for this leave type as it has been configured with setting for maximum") & " " & mintLvformMaxDays & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 28, "day(s) to be applied for this leave type."), Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
            End If
            'Pinkal (08-Oct-2018) -- End


            Dim objLeaveType As New clsleavetype_master
            objLeaveType._Leavetypeunkid = CInt(Me.ViewState("L_TypeId"))
            Dim objAccruetran As New clsleavebalance_tran

            If objLeaveType._IsPaid And objLeaveType._IsAccrueAmount Then
                Dim drAccrue() As DataRow = Nothing



                If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                    'drAccrue = objAccruetran.GetList("AccrueTran", _
                    '                                 Session("Database_Name").ToString(), _
                    '                                 CInt(Session("UserId")), _
                    '                                 CInt(Session("Fin_year")), _
                    '                                 CInt(Session("CompanyUnkId")), _
                    '                                 Session("EmployeeAsOnDate").ToString(), _
                    '                                 Session("UserAccessModeSetting").ToString(), True, _
                    '                              True, True, True, False, CInt(Me.ViewState("L_EmpId")) _
                    '                              , False, False, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(Me.ViewState("L_TypeId")) & " AND yearunkid = " & CInt(Session("Fin_year")))


                    drAccrue = objAccruetran.GetList("AccrueTran", _
                                                     Session("Database_Name").ToString(), _
                                                     CInt(Session("UserId")), _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     Session("EmployeeAsOnDate").ToString(), _
                                                     Session("UserAccessModeSetting").ToString(), True, _
                                                  True, True, False, False, CInt(Me.ViewState("L_EmpId")) _
                                                  , False, False, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(Me.ViewState("L_TypeId")) & " AND yearunkid = " & CInt(Session("Fin_year")))

                    'Pinkal (26-Feb-2019) -- End

                ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then


                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                    'drAccrue = objAccruetran.GetList("AccrueTran", _
                    '                                 Session("Database_Name").ToString(), _
                    '                                 CInt(Session("UserId")), _
                    '                                 CInt(Session("Fin_year")), _
                    '                                 CInt(Session("CompanyUnkId")), _
                    '                                Session("EmployeeAsOnDate").ToString(), _
                    '                                 Session("UserAccessModeSetting").ToString(), True, _
                    '                              True, True, True, False, CInt(Me.ViewState("L_EmpId")) _
                    '                              , True, True, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(Me.ViewState("L_TypeId")) & " AND yearunkid = " & CInt(Session("Fin_year")))

                    drAccrue = objAccruetran.GetList("AccrueTran", _
                                                     Session("Database_Name").ToString(), _
                                                     CInt(Session("UserId")), _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                    Session("EmployeeAsOnDate").ToString(), _
                                                     Session("UserAccessModeSetting").ToString(), True, _
                                               True, True, False, False, CInt(Me.ViewState("L_EmpId")) _
                                                  , True, True, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(Me.ViewState("L_TypeId")) & " AND yearunkid = " & CInt(Session("Fin_year")))

                    'Pinkal (26-Feb-2019) -- End

                End If


                If drAccrue.Length = 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "Please Enter Accrue amount for this paid leave."), Me)
                    Exit Sub
                End If

            ElseIf objLeaveType._IsPaid And objLeaveType._IsAccrueAmount = False And objLeaveType._DeductFromLeaveTypeunkid > 0 Then

                Dim drAccrue() As DataRow = Nothing


                'Pinkal (02-Jan-2018) -- Start
                'Enhancement - Ref 125 For leave types whose days are deducted from leave types of accrue nature, don't allow employee to apply more than the maximum set on the Add/Edit Leave type screen. 
                'As at now, employee can apply more than the set maximum; approver is only given a warning if he still wishes to issue. If he still wants to approve even with the exceeding leave days he can approve.
                'Control to be put on application/not just issuing. employee to be warned and stopped.
                Dim dsBalance As DataSet = Nothing
                If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                    'drAccrue = objAccruetran.GetList("AccrueTran", _
                    '                                 Session("Database_Name").ToString(), _
                    '                                 CInt(Session("UserId")), _
                    '                                 CInt(Session("Fin_year")), _
                    '                                 CInt(Session("CompanyUnkId")), _
                    '                                 Session("EmployeeAsOnDate").ToString(), _
                    '                                 Session("UserAccessModeSetting").ToString(), True, _
                    '                                True, True, True, False, CInt(Me.ViewState("L_EmpId")) _
                    '                                , False, False, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid) & " AND yearunkid = " & CInt(Session("Fin_year")))


                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                    'dsBalance = objAccruetran.GetList("AccrueTran", _
                    '                                 Session("Database_Name").ToString(), _
                    '                                 CInt(Session("UserId")), _
                    '                                 CInt(Session("Fin_year")), _
                    '                                 CInt(Session("CompanyUnkId")), _
                    '                                 Session("EmployeeAsOnDate").ToString(), _
                    '                                 Session("UserAccessModeSetting").ToString(), True, _
                    '                                True, True, True, False, CInt(Me.ViewState("L_EmpId")) _
                    '                             , False, False, False, "", Nothing, mblnIsExternalApprover)

                    dsBalance = objAccruetran.GetList("AccrueTran", _
                                                     Session("Database_Name").ToString(), _
                                                     CInt(Session("UserId")), _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     Session("EmployeeAsOnDate").ToString(), _
                                                     Session("UserAccessModeSetting").ToString(), True, _
                                                  True, True, False, False, CInt(Me.ViewState("L_EmpId")) _
                                                 , False, False, False, "", Nothing, mblnIsExternalApprover)

                    'Pinkal (26-Feb-2019) -- End

                ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then


                    'drAccrue = objAccruetran.GetList("AccrueTran", _
                    '                                 Session("Database_Name").ToString(), _
                    '                                 CInt(Session("UserId")), _
                    '                                 CInt(Session("Fin_year")), _
                    '                                 CInt(Session("CompanyUnkId")), _
                    '                                 Session("EmployeeAsOnDate").ToString(), _
                    '                                 Session("UserAccessModeSetting").ToString(), True, _
                    '                                 True, True, True, False, CInt(Me.ViewState("L_EmpId")), True, True _
                    '                                 , False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._DeductFromLeaveTypeunkid) & " AND yearunkid = " & CInt(Session("Fin_year")))


                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                    'dsBalance = objAccruetran.GetList("AccrueTran", _
                    '                                 Session("Database_Name").ToString(), _
                    '                                 CInt(Session("UserId")), _
                    '                                 CInt(Session("Fin_year")), _
                    '                                 CInt(Session("CompanyUnkId")), _
                    '                                 Session("EmployeeAsOnDate").ToString(), _
                    '                                 Session("UserAccessModeSetting").ToString(), True, _
                    '                                 True, True, True, False, CInt(Me.ViewState("L_EmpId")), True, True _
                    '                               , False, "", Nothing, mblnIsExternalApprover)

                    dsBalance = objAccruetran.GetList("AccrueTran", _
                                                     Session("Database_Name").ToString(), _
                                                     CInt(Session("UserId")), _
                                                     CInt(Session("Fin_year")), _
                                                     CInt(Session("CompanyUnkId")), _
                                                     Session("EmployeeAsOnDate").ToString(), _
                                                     Session("UserAccessModeSetting").ToString(), True, _
                                                  True, True, False, False, CInt(Me.ViewState("L_EmpId")), True, True _
                                                   , False, "", Nothing, mblnIsExternalApprover)

                    'Pinkal (26-Feb-2019) -- End

                End If

                drAccrue = dsBalance.Tables(0).Select("leavetypeunkid =" & CInt(objLeaveType._Leavetypeunkid) & " AND yearunkid = " & CInt(Session("Fin_year")))

                If drAccrue.Length = 0 Then
                    'Language.setLanguage(mstrModuleName)
                    objLeaveType._Leavetypeunkid = objLeaveType._DeductFromLeaveTypeunkid
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "This particular leave is mapped with ") & objLeaveType._Leavename _
                                                  & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, ", which does not have the accrue amount. Please add accrue amount for ") & objLeaveType._Leavename _
                                                  & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, " inorder to issue leave."), Me)
                    Exit Sub
                Else

                    Dim mdblIssue As Decimal = 0
                    Dim mdtStartDate As Date = Nothing
                    Dim mdtEndDate As Date = Nothing
                    Dim objLeaveIssueTran As New clsleaveissue_Tran

                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC AndAlso drAccrue.Length > 0 Then
                        mdtStartDate = CDate(drAccrue(0)("startdate")).Date
                        mdtEndDate = CDate(drAccrue(0)("enddate")).Date
                    End If

                    If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                        mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(Me.ViewState("L_EmpId")), CInt(objLeaveType._Leavetypeunkid), CInt(Session("Fin_year")), Nothing, mdtStartDate, mdtEndDate)
                    Else
                        mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(CInt(Me.ViewState("L_EmpId")), CInt(objLeaveType._Leavetypeunkid), CInt(Session("Fin_year")))
                    End If

                    If objLeaveType._MaxAmount < (mdblIssue + CDec(lblDays.Text)) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), Me)
                        Exit Sub
                    End If
                    'Pinkal (02-Jan-2018) -- End
                End If

            ElseIf objLeaveType._IsPaid And objLeaveType._IsShortLeave Then

                Dim drShortLeave() As DataRow = Nothing

                If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                    'drShortLeave = objAccruetran.GetList("AccrueTran", _
                    '                                     Session("Database_Name").ToString(), _
                    '                                     CInt(Session("UserId")), _
                    '                                     CInt(Session("Fin_year")), _
                    '                                     CInt(Session("CompanyUnkId")), _
                    '                                     Session("EmployeeAsOnDate").ToString(), _
                    '                                     Session("UserAccessModeSetting").ToString, True, _
                    '                                   True, True, True, False, CInt(Me.ViewState("L_EmpId")) _
                    '                                   , False, False, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(Me.ViewState("L_TypeId")) & " AND isshortleave = 1 " & " AND yearunkid = " & CInt(Session("Fin_year")))

                    drShortLeave = objAccruetran.GetList("AccrueTran", _
                                                         Session("Database_Name").ToString(), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         Session("EmployeeAsOnDate").ToString(), _
                                                         Session("UserAccessModeSetting").ToString, True, _
                                                        True, True, False, False, CInt(Me.ViewState("L_EmpId")) _
                                                       , False, False, False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(Me.ViewState("L_TypeId")) & " AND isshortleave = 1 " & " AND yearunkid = " & CInt(Session("Fin_year")))


                    'Pinkal (26-Feb-2019) -- End


                ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                    'drShortLeave = objAccruetran.GetList("AccrueTran", _
                    '                                     Session("Database_Name").ToString(), _
                    '                                     CInt(Session("UserId")), _
                    '                                     CInt(Session("Fin_year")), _
                    '                                     CInt(Session("CompanyUnkId")), _
                    '                                     Session("EmployeeAsOnDate").ToString(), _
                    '                                     Session("UserAccessModeSetting").ToString(), True, _
                    '                                     True, True, True, False, CInt(Me.ViewState("L_EmpId")), True, True _
                    '                                     , False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(Me.ViewState("L_TypeId")) & " AND isshortleave = 1 " & " AND yearunkid = " & CInt(Session("Fin_year")))

                    drShortLeave = objAccruetran.GetList("AccrueTran", _
                                                         Session("Database_Name").ToString(), _
                                                         CInt(Session("UserId")), _
                                                         CInt(Session("Fin_year")), _
                                                         CInt(Session("CompanyUnkId")), _
                                                         Session("EmployeeAsOnDate").ToString(), _
                                                         Session("UserAccessModeSetting").ToString(), True, _
                                                       True, True, False, False, CInt(Me.ViewState("L_EmpId")), True, True _
                                                         , False, "", Nothing, mblnIsExternalApprover).Tables(0).Select("leavetypeunkid =" & CInt(Me.ViewState("L_TypeId")) & " AND isshortleave = 1 " & " AND yearunkid = " & CInt(Session("Fin_year")))

                    'Pinkal (26-Feb-2019) -- End


                End If

                If drShortLeave.Length = 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "Sorry, there is no leave frequency accrue amount set for the selected short leave, please enter accrue amount for this leave from, Leave Information -> Leave Frequency."), Me)
                    Exit Sub
                End If


            End If


            Dim dsList As New DataSet
            If objLeaveType._IsPaid Then
                Dim objAcccure As New clsleavebalance_tran

                If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                    'dsList = objAcccure.GetList("List", _
                    '                            Session("Database_Name").ToString(), _
                    '                            CInt(Session("UserId")), _
                    '                            CInt(Session("Fin_year")), _
                    '                            CInt(Session("CompanyUnkId")), _
                    '                            Session("EmployeeAsOnDate").ToString(), _
                    '                            Session("UserAccessModeSetting").ToString(), True, _
                    '                        True, True, True, False, CInt(Me.ViewState("L_EmpId")), False, False, False, "", Nothing, mblnIsExternalApprover)

                    dsList = objAcccure.GetList("List", _
                                                Session("Database_Name").ToString(), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                Session("EmployeeAsOnDate").ToString(), _
                                                Session("UserAccessModeSetting").ToString(), True, _
                                        True, True, False, False, CInt(Me.ViewState("L_EmpId")), False, False, False, "", Nothing, mblnIsExternalApprover)

                    'Pinkal (26-Feb-2019) -- End


                ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                    dsList = objAcccure.GetList("List", _
                                                Session("Database_Name").ToString(), _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                Session("EmployeeAsOnDate").ToString(), _
                                                Session("UserAccessModeSetting").ToString(), True, _
                                                True, True, False, False, CInt(Me.ViewState("L_EmpId")), True, True, False, "", Nothing, mblnIsExternalApprover)


                    'Pinkal (26-Feb-2019) -- End

                End If

                Dim dtList As DataTable = New DataView(dsList.Tables("List"), "yearunkid = " & CInt(cboYear.SelectedValue) & " AND leavetypeunkid = " & CInt(Me.ViewState("L_TypeId")), "", DataViewRowState.CurrentRows).ToTable
                If dtList IsNot Nothing Then
                    If dtList.Rows.Count > 0 Then
                        mdblLeaveAmout = CDec(dtList.Rows(0)("accrue_amount"))
                    End If
                End If
            Else
                mdblLeaveAmout = objLeaveType._MaxAmount
            End If

            mdic_ArryDays = CType(Me.ViewState("mdic_ArryDays"), Dictionary(Of String, String))
            Dim arrDay() As String : ReDim arrDay(mdic_ArryDays.Keys.Count - 1)
            Dim i As Integer = 0
            For Each sKey As String In mdic_ArryDays.Keys
                arrDay(i) = eZeeDate.convertDate(mdic_ArryDays(sKey)).ToString
                i += 1
            Next

            If arrDay.Length <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Leave Date is compulsory information.Please Select Leave Date between given Leave Form Period."), Me)
                Exit Sub
            End If

            Me.ViewState("L_Days") = arrDay
            Me.ViewState.Add("IsPaid", objLeaveType._IsPaid)
            Me.ViewState.Add("LeaveAmout", mdblLeaveAmout)
            Me.ViewState.Add("mdblIssueAmount", CDec(lblDays.Text))

            If objLeaveType._DeductFromLeaveTypeunkid > 0 Then
                Dim mdblIssue As Decimal = objLIssueTran.GetEmployeeIssueDaysCount(CInt(Me.ViewState("L_EmpId")), CInt(Me.ViewState("L_TypeId")), CInt(Session("Fin_year")))
                If objLeaveType._MaxAmount < (mdblIssue + CDec(Me.ViewState("mdblIssueAmount"))) Then
                    radYesNo.Show()
                    Exit Sub
                End If

            ElseIf objLeaveType._DeductFromLeaveTypeunkid <= 0 AndAlso objLeaveType._IsPaid = False Then
                Dim mdblIssue As Decimal = objLIssueTran.GetEmployeeIssueDaysCount(CInt(Me.ViewState("L_EmpId")), CInt(Me.ViewState("L_TypeId")), CInt(Session("Fin_year")))

                If objLeaveType._MaxAmount < (mdblIssue + CDec(Me.ViewState("mdblIssueAmount"))) Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), Me)
                    Exit Sub
                End If

            End If

            Dim objbalance As New clsleavebalance_tran


            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then


                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                'dsList = objbalance.GetList("List", _
                '                            Session("Database_Name").ToString(), _
                '                            CInt(Session("UserId")), _
                '                            CInt(Session("Fin_year")), _
                '                            CInt(Session("CompanyUnkId")), _
                '                           Session("EmployeeAsOnDate").ToString(), _
                '                            Session("UserAccessModeSetting").ToString(), True, _
                '                           True, True, True, False, CInt(Me.ViewState("L_EmpId")), False, False, False, "", Nothing, mblnIsExternalApprover)

                dsList = objbalance.GetList("List", _
                                            Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                           Session("EmployeeAsOnDate").ToString(), _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                           True, True, False, False, CInt(Me.ViewState("L_EmpId")), False, False, False, "", Nothing, mblnIsExternalApprover)

                'Pinkal (26-Feb-2019) -- End


            ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then


                dsList = objbalance.GetList("List", _
                                            Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            Session("EmployeeAsOnDate").ToString(), _
                                            Session("UserAccessModeSetting").ToString(), True, _
                                           True, True, False, False, CInt(Me.ViewState("L_EmpId")), True, True _
                                           , False, "", Nothing, mblnIsExternalApprover)

            End If


            Dim drRow As DataRow() = dsList.Tables("List").Select("leavetypeunkid=" & CInt(Me.ViewState("L_TypeId")))

            If drRow.Length > 0 Then

                If CInt(drRow(0)("accruesetting")) <> enAccrueSetting.Donot_Issue_On_Exceeding_Balance AndAlso CInt(drRow(0)("accruesetting")) <> enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then
                    If drRow.Length > 0 Then
                        If CDec(drRow(0)("Accrue_Amount")) < (CDec(drRow(0)("Issue_amount")) + CDec(Me.ViewState("mdblIssueAmount"))) Then
                            radYesNo.Show()
                            Exit Sub
                        End If
                    End If

                ElseIf CInt(drRow(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance Then

                    If CDec(drRow(0)("Accrue_Amount")) < (CDec(drRow(0)("Issue_amount")) + CDec(Me.ViewState("mdblIssueAmount"))) Then
                        'Language.setLanguage(mstrModuleName)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "You can't issue this leave form.Reason : Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type."), Me)
                        Exit Sub
                    End If

                ElseIf CInt(drRow(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then

                    Dim mdecAsonDateAccrue As Decimal = 0
                    Dim mdecAsonDateBalance As Decimal = 0

                    'Pinkal (06-Apr-2018) -- Start
                    'bug - 0002164  Employee unable to apply for leave. message indicating that leave has exceeded max limit [CCK].

                    'objbalance.GetAsonDateAccrue(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date _
                    '                                       , CInt(Session("LeaveBalanceSetting")), CInt(Me.ViewState("L_EmpId")), CInt(Me.ViewState("L_TypeId")) _
                    '                                       , objLForm._Returndate.Date, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                    '                                       , mdecAsonDateAccrue, mdecAsonDateBalance)

                    objbalance.GetAsonDateAccrue(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date _
                                                                 , CInt(Session("LeaveBalanceSetting")), CInt(Me.ViewState("L_EmpId")), CInt(Me.ViewState("L_TypeId")) _
                                                                 , objLForm._Returndate.Date, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                                                              , mdecAsonDateAccrue, mdecAsonDateBalance, CInt(Session("Fin_year")))

                    'Pinkal (06-Apr-2018) -- End


                    If mdecAsonDateBalance < CDec(Me.ViewState("mdblIssueAmount")) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 20, "You cannot issue this leave form.Reason : This Leave Limit has exceeded the Maximum Leave balance as on date Limit for this leave type."), Me)
                        Exit Sub
                    End If

                End If

            End If


            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                Dim objLvForm As New clsleaveform
                objLvForm._Formunkid = CInt(Me.ViewState("L_FormId"))
                Dim objPaymentTran As New clsPayment_tran
                Dim objPeriod As New clsMasterData

                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                mblnConsiderLeaveOnTnAPeriod = objLvForm._ConsiderLeaveOnTnAPeriod
                'Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, objLvForm._Startdate.Date)

                Dim intPeriodId As Integer = 0
                If mblnConsiderLeaveOnTnAPeriod Then
                    intPeriodId = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, objLvForm._Startdate.Date)
                Else
                    intPeriodId = objPeriod.getCurrentPeriodID(enModuleReference.Payroll, objLvForm._Startdate.Date, CInt(Session("Fin_year")), 0, False, True, Nothing, True)
                End If

                'Dim mdtTnAStartdate As Date = Nothing
                'If intPeriodId > 0 Then
                '    Dim objTnAPeriod As New clscommom_period_Tran
                '    mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                'End If

                Dim mdtTnAStartdate As Date = Nothing
                If intPeriodId > 0 Then
                    Dim objTnAPeriod As New clscommom_period_Tran
                    If mblnConsiderLeaveOnTnAPeriod Then
                        mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                    Else
                        objTnAPeriod._Periodunkid(Session("Database_Name").ToString()) = intPeriodId
                        mdtTnAStartdate = objTnAPeriod._Start_Date
                    End If
                    objTnAPeriod = Nothing
                End If

                'If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(Me.ViewState("L_EmpId")), mdtTnAStartdate, intPeriodId, objLvForm._Startdate.Date) > 0 Then
                If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, CInt(Me.ViewState("L_EmpId")), mdtTnAStartdate, intPeriodId, objLvForm._Startdate.Date, "", Nothing, mblnConsiderLeaveOnTnAPeriod) > 0 Then
                    'Pinkal (01-Jan-2019) -- End
                    LeaveIssueConfirmation.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 24, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to Issue this leave ?  ")
                    LeaveIssueConfirmation.Show()
                    Exit Sub
                End If
                objLvForm = Nothing
            End If
            'Pinkal (13-Jan-2015) -- End



            radYesNo_buttonYes_Click(sender, e)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Protected Sub radYesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radYesNo.buttonNo_Click
        Try
            Me.ViewState.Add("sValue", False)
            radYesNo.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub radYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radYesNo.buttonYes_Click
        Dim blnFlag As Boolean = False
        Try
            Call SetValue()

            Blank_ModuleName()
            objLIssue._WebFormName = "frmLeaveIssue_AddEdit"
            StrModuleName2 = "mnuLeaveInformation"
            objLIssue._WebClientIP = Session("IP_ADD").ToString
            objLIssue._WebHostName = Session("HOST_NAME").ToString()
            objLIssue._Userunkid = CInt(Session("UserId"))

            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            Dim objLeaveType As New clsleavetype_master
            objLeaveType._Leavetypeunkid = CInt(Me.ViewState("L_TypeId"))
            'Sohail (21 Oct 2019) -- End

            'Pinkal (29-Mar-2019) -- Start
            'Bug - Worked on Leave Issue User Access Issue for all companies.

            'blnFlag = objLIssue.Insert(Session("Database_Name").ToString(), _
            '                           CInt(Session("UserId")), _
            '                           CInt(Session("Fin_year")), _
            '                           CInt(Session("CompanyUnkId")), _
            '                           Session("EmployeeAsOnDate").ToString(), _
            '                           Session("UserAccessModeSetting").ToString(), True, _
            '                           True, _
            '                           CType(Me.ViewState("L_Days"), String()), _
            '                           CBool(Me.ViewState("IsPaid")), _
            '                           CDbl(Me.ViewState("LeaveAmout")), _
            '                           CInt(Session("LeaveBalanceSetting")), _
            '                           Session("LeaveApproverForLeaveType").ToString(), _
            '                           CInt(Session("EmpMaxapproverunkid")), Nothing, True, mblnIsExternalApprover)


            blnFlag = objLIssue.Insert(Session("Database_Name").ToString(), _
                                       CInt(Session("UserId")), _
                                       CInt(Session("Fin_year")), _
                                       CInt(Session("CompanyUnkId")), _
                                       Session("EmployeeAsOnDate").ToString(), _
                                       Session("UserAccessModeSetting").ToString(), True, _
                                       True, _
                                       CType(Me.ViewState("L_Days"), String()), _
                                       CBool(Me.ViewState("IsPaid")), _
                                       CDbl(Me.ViewState("LeaveAmout")), _
                                       CInt(Session("LeaveBalanceSetting")), _
                                       Session("LeaveApproverForLeaveType").ToString(), _
                                   CInt(Session("EmpMaxapproverunkid")), Nothing, False, mblnIsExternalApprover, objLeaveType._Isexempt_from_payroll, CBool(Session("SkipEmployeeMovementApprovalFlow")), CBool(Session("CreateADUserFromEmpMst")))
            'Sohail (21 Oct 2019) - [blnExemptFromPayroll, blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]
            'Pinkal (29-Mar-2019) -- End


            If blnFlag = False AndAlso objLIssue._Message <> "" Then
                DisplayMessage.DisplayMessage(objLIssue._Message, Me)
            Else
                radYesNo.Hide()
                Dim objPending As New clspendingleave_Tran
                If objLIssue._Leaveissueunkid > 0 Then
                    Blank_ModuleName()
                    clsCommonATLog._WebFormName = "frmProcessLeaveList"
                    StrModuleName2 = "mnuLeaveInformation"
                    clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
                    clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

                    objPending.UpdatePendingFormStatus(Session("Database_Name").ToString(), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       Session("EmployeeAsOnDate").ToString(), _
                                                       Session("UserAccessModeSetting").ToString(), True, _
                                                       True, CInt(Me.ViewState("L_FormId")), _
                                                       CInt(Me.ViewState("L_EmpId")), _
                                                       CInt(Session("approverunkid")), _
                                                       CInt(Session("leaveapproverunkid")), _
                                                       CInt(Session("UserId")))


                    'Pinkal (03-May-2019) -- Start
                    'Enhancement - Working on Leave UAT Changes for NMB.

                    '*************** START TO SEND RELIEVER EMAIL NOTIFICATION************************

                    Dim objLeaveForm As New clsleaveform
                    objLeaveForm._Formunkid = objLIssue._Formunkid

                    objLeaveForm._WebFrmName = "frmProcessLeaveList"
                    objLeaveForm._WebClientIP = Session("IP_ADD").ToString()
                    objLeaveForm._WebHostName = Session("HOST_NAME").ToString()

                    objPending._Pendingleavetranunkid = CInt(Session("pendingleavetranunkid"))

                    If CBool(Session("SetRelieverAsMandatoryForApproval")) AndAlso objLeaveForm._Statusunkid = 7 AndAlso objPending._RelieverEmpId > 0 Then
                        'Pinkal (03-Jul-2019) -- Start
                        'Bug - Solved Problem for Blank Email Notification to Leave Approver for Reliever.
                        'objLeaveForm.SendMailToReliever(CInt(Session("CompanyUnkId")), objPending._RelieverEmpId, objLeaveForm._Leavetypeunkid _
                        '                                                , objLeaveForm._Formunkid, objLeaveForm._Formno, txtEmployee.Text.Trim(), objPending._Startdate, objPending._Enddate _
                        '                                                , CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE)
                        objLeaveForm.SendMailToReliever(CInt(Session("CompanyUnkId")), objPending._RelieverEmpId, objLeaveForm._Leavetypeunkid _
                                                                        , objLeaveForm._Formunkid, objLeaveForm._Formno, txtEmployee.Text.Trim(), objPending._Startdate, objPending._Enddate _
                                                                       , CInt(Session("UserId")), objLeaveForm._Statusunkid, "", enLogin_Mode.MGR_SELF_SERVICE)
                        'Pinkal (03-Jul-2019) -- End
                    End If

                    objLeaveForm = Nothing
                    Session("pendingleavetranunkid") = Nothing
                    objPending = Nothing

                    '*************** END TO SEND RELIEVER EMAIL NOTIFICATION************************

                    'Pinkal (03-May-2019) -- End



                    If CStr(Session("Notify_IssuedLeave_Users")).Trim.Length > 0 Then

                        Dim objMaster As New clsMasterData
                        gobjEmailList = New List(Of clsEmailCollection)
                        Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                        Dim objEmployee As New clsEmployee_Master
                        objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = CInt(Me.ViewState("L_EmpId"))
                        For Each sId As String In ConfigParameter._Object._Notify_IssuedLeave_Users.Trim.Split(CChar(","))
                            If sId.Trim = "" Then Continue For
                            Dim dtUserAccess As DataTable = objUsr.GetUserAccessFromUser(CInt(sId), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")))
                            If dtUserAccess IsNot Nothing AndAlso dtUserAccess.Rows.Count > 0 Then
                                Dim mblnFlag As Boolean = False
                                For Each AID As String In CStr(Session("UserAccessModeSetting")).Trim.Split(CChar(","))
                                    Dim drRow() As DataRow = Nothing
                                    Select Case CInt(AID)

                                        Case enAllocation.DEPARTMENT
                                            drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Departmentunkid & " AND referenceunkid = " & enAllocation.DEPARTMENT)
                                            If drRow.Length > 0 Then
                                                mblnFlag = True
                                            Else
                                                mblnFlag = False
                                                Exit For
                                            End If

                                        Case enAllocation.JOBS
                                            drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Jobunkid & " AND referenceunkid = " & enAllocation.JOBS)
                                            If drRow.Length > 0 Then
                                                mblnFlag = True
                                            Else
                                                mblnFlag = False
                                                Exit For
                                            End If

                                        Case enAllocation.CLASS_GROUP
                                            drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Classgroupunkid & " AND referenceunkid = " & enAllocation.CLASS_GROUP)
                                            If drRow.Length > 0 Then
                                                mblnFlag = True
                                            Else
                                                mblnFlag = False
                                                Exit For
                                            End If

                                        Case enAllocation.CLASSES
                                            drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Classunkid & " AND referenceunkid = " & enAllocation.CLASSES)
                                            If drRow.Length > 0 Then
                                                mblnFlag = True
                                            Else
                                                mblnFlag = False
                                                Exit For
                                            End If

                                        Case Else
                                            mblnFlag = False
                                    End Select

                                Next

                                If mblnFlag Then
                                    objUsr._Userunkid = CInt(sId)
                                    StrMessage = objLIssue.SetNotificationForIssuedUser(objUsr._Firstname & " " & objUsr._Lastname, txtEmployee.Text, txtFormNo.Text.Trim, txtLeaveType.Text.Trim, objLIssue._StartDate.Date, objLIssue._EndDate.Date)
                                    gobjEmailList.Add(New clsEmailCollection(objUsr._Email, Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "Issued Leave Application form notification"), StrMessage, mstrModuleName, 0, Session("IP_ADD").ToString, Session("HOST_NAME").ToString, CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE, clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT, objUsr._Email))
                                End If

                            End If
                        Next
                        Send_Notification()

                    End If

                    Session("approverunkid") = Nothing
                End If

                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.
                'Response.Redirect("~/Leave/ProcessPendingLeave.aspx?Id=" & b64encode(Me.ViewState("L_EmpId").ToString()))
                Response.Redirect("~/Leave/wPg_ProcessLeaveList.aspx?Id=" & b64encode(Me.ViewState("L_EmpId").ToString()), False)
                'Pinkal (06-Jan-2016) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub LeaveIssueConfirmation_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LeaveIssueConfirmation.buttonYes_Click
        Try
            radYesNo_buttonYes_Click(sender, e)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Function & Procedures "

    Private Sub SetValue()
        Try

            objLIssue._Leaveyearunkid = CInt(Session("Fin_year"))

            objLIssue._Formunkid = CInt(Me.ViewState("L_FormId"))
            objLIssue._Leavetypeunkid = CInt(Me.ViewState("L_TypeId"))
            objLIssue._Employeeunkid = CInt(Me.ViewState("L_EmpId"))
            objLIssue._Userunkid = CInt(Session("UserId"))

            Dim objfrm As New clsleaveform
            objfrm._Formunkid = objLIssue._Formunkid
            objLIssue._StartDate = objfrm._Startdate
            objLIssue._EndDate = objfrm._Returndate
            objLIssue._TotalIssue = CDec(lblDays.Text)


            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            mblnConsiderLeaveOnTnAPeriod = objfrm._ConsiderLeaveOnTnAPeriod
            objLIssue._ConsiderLeaveOnTnAPeriod = mblnConsiderLeaveOnTnAPeriod
            'Pinkal (01-Jan-2019) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dList As New DataSet
        Dim objYear As New clsMasterData
        Try
            dList = objYear.getComboListPAYYEAR(CInt(Session("Fin_year")), Session("FinancialYear_Name").ToString(), CInt(Session("CompanyUnkId")), "Year", True)
            With cboYear
                .DataTextField = "name"
                .DataValueField = "Id"
                .DataSource = dList.Tables(0)
                .SelectedValue = Session("Fin_year").ToString()
                .DataBind()
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function b64decode(ByVal StrDecode As String) As String
        Dim decodedString As String = ""
        Try
            decodedString = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(StrDecode))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return decodedString
    End Function

    Private Sub IsFrom_Process_List()
        Try
            If (Request.QueryString("ProcessId") <> "") Then
                mintL_FormId = CInt(b64decode(Request.QueryString("ProcessId")))
                Me.ViewState.Add("L_FormId", mintL_FormId)
                objLForm._Formunkid = mintL_FormId
            Else
                Me.ViewState.Add("PendingUnkid", -1)
            End If
            Call Fill_Info()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Fill_Info()
        Dim objEmp As New clsEmployee_Master
        Dim objLeave As New clsleavetype_master
        Try
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = objLForm._Employeeunkid
            objLeave._Leavetypeunkid = objLForm._Leavetypeunkid

            Me.ViewState.Add("L_EmpId", objLForm._Employeeunkid)
            Me.ViewState.Add("L_TypeId", objLForm._Leavetypeunkid)
            Me.ViewState.Add("E_ShiftId", objEmp._Shiftunkid)
            Me.ViewState.Add("Issue_Holiday", objLeave._Isissueonholiday)
            Me.ViewState.Add("Issue_Weekend", objLeave._Isissueonweekend)

            txtEmployee.Text = objEmp._Firstname & " " & " " & IIf(objEmp._Othername <> "", objEmp._Othername, "").ToString() & " " & objEmp._Surname


            If Session("DateFormat").ToString() <> Nothing Then
                txtAsOnDate.Text = ConfigParameter._Object._CurrentDateAndTime.Date.ToString(Session("DateFormat").ToString())
            Else
                txtAsOnDate.Text = ConfigParameter._Object._CurrentDateAndTime.ToShortDateString
            End If

            txtFormNo.Text = objLForm._Formno
            txtLeaveType.Text = objLeave._Leavename

            If Session("DateFormat").ToString() <> Nothing Then
                lblFDateVal.Text = objLForm._Startdate.Date.ToString(Session("DateFormat").ToString()) & " To " & objLForm._Returndate.Date.ToString(Session("DateFormat").ToString())
            Else
                lblFDateVal.Text = objLForm._Startdate.ToShortDateString & " To " & objLForm._Returndate.ToShortDateString
            End If

            txtColor.BackColor = Color.FromArgb(objLeave._Color)

            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            mblnIsLvTypePaid = objLeave._IsPaid
            mintLvformMinDays = objLeave._LVDaysMinLimit
            mintLvformMaxDays = objLeave._LVDaysMaxLimit
            Me.ViewState("LvTypeIsPaid") = mblnIsLvTypePaid
            Me.ViewState("LVDaysMinLimit") = mintLvformMinDays
            Me.ViewState("LVDaysMaxLimit") = mintLvformMaxDays
            'Pinkal (08-Oct-2018) -- End

            Call GetBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, CInt(objLeave._Leavetypeunkid), objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date))
            Call Create_Grid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objEmp = Nothing : objLeave = Nothing
        End Try
    End Sub

    Private Sub GetBalanceInfo(ByVal mdtDate As DateTime, ByVal mintLeaveTypeunkid As Integer, ByVal mintEmpId As Integer)
        Try
            Dim objLeaveAccrue As New clsleavebalance_tran

            Dim dsList As DataSet = Nothing

            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                dsList = objLeaveAccrue.GetLeaveBalanceInfo(mdtDate, CStr(mintLeaveTypeunkid), mintEmpId.ToString _
                                                                                  , CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                                                                                  , CDate(Session("fin_startdate")), CDate(Session("fin_enddate")), , , CInt(Session("LeaveBalanceSetting")), -1, CInt(Session("Fin_year")))

            ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                objLeaveAccrue._DBStartdate = CDate(Session("fin_startdate")).Date
                objLeaveAccrue._DBEnddate = CDate(Session("fin_enddate")).Date

                dsList = objLeaveAccrue.GetLeaveBalanceInfo(mdtDate, CStr(mintLeaveTypeunkid), mintEmpId.ToString _
                                                                                  , CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                                                                                  , Nothing, Nothing, True, True, CInt(Session("LeaveBalanceSetting")), -1, CInt(Session("Fin_year")))
            End If


            If dsList.Tables("Balanceinfo").Rows.Count > 0 Then
                lblCaption1Value.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LstyearAccrue_amount")).ToString("#0.00")
                lblCaption3Value.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LstyearIssue_amount")).ToString("#0.00")
                lblCaption2Value.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Accrue_amount")).ToString("#0.00")
                lblCaption4Value.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")).ToString("#0.00")
                lblCaption5Value.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("Balance")).ToString("#0.00")
                lblCaption4Value.Text = CDec(CDec(dsList.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveEncashment"))).ToString("#0.00")
                objlblLeaveBFvalue.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveBF")).ToString("#0.00")
                objlblTotalAdjustment.Text = CDec(dsList.Tables("Balanceinfo").Rows(0)("LeaveAdjustment")).ToString("#0.00")
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Create_Grid()
        Try
            Me.ViewState.Add("decLAmt", 0)
            Dim objLType As New clsleavetype_master
            Dim ds As New DataSet : Dim StrIds As String = "" : Dim dsList As New DataSet

            Dim objShiftTran As New clsshift_tran
            objShiftTran.GetShiftTran(CInt(Me.ViewState("E_ShiftId")))
            Me.ViewState.Add("Shift_Day", objShiftTran._dtShiftday)

            Dim TblName As String = "Issue_Leave_" & Me.ViewState("L_FormId").ToString & "_" & Me.ViewState("L_EmpId").ToString
            Dim MSQL As String = "EXECUTE CreateMonthCalendar  '" & TblName & "' , '" & Session("L_StartDate").ToString & "','" & Session("L_EndDate").ToString & "'"
            objDataOperation.ExecNonQuery(MSQL)

            Dim flag As Boolean = False
            Dim totday As Integer

            MSQL = "SELECT * FROM " & TblName & ""

            ds = objDataOperation.WExecQuery(MSQL, "Tblname")

            Dim objEmpHoliday As New Aruti.Data.clsemployee_holiday
            Dim mDicArrayDays As New Dictionary(Of Integer, String)

            Dim iDiff As Integer = CInt(DateDiff(DateInterval.Day, eZeeDate.convertDate(Session("L_StartDate").ToString).Date, eZeeDate.convertDate(Session("L_EndDate").ToString()).Date)) + 1

            Me.ViewState("statdate") = eZeeDate.convertDate(Session("L_StartDate").ToString).ToString("MMdd")
            Me.ViewState("enddate") = eZeeDate.convertDate(Session("L_EndDate").ToString).ToString("MMdd")

            Dim dtDate As Date
            If dtDate = Nothing Then dtDate = eZeeDate.convertDate(Session("L_StartDate").ToString).Date
            objLType._Leavetypeunkid = CInt(Me.ViewState("L_TypeId"))
            Dim arDays() As String : ReDim arDays(iDiff - 1)

            For k As Integer = 0 To iDiff - 1
                If objLeaveFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(dtDate), CInt(Me.ViewState("L_FormId")), CInt(Me.ViewState("L_EmpId")), Session("LeaveApproverForLeaveType").ToString(), CInt(Session("EmpMaxapproverunkid"))) > 0 Then
                    mDicArrayDays.Add(k, eZeeDate.convertDate(dtDate))
                    arDays(k) = dtDate.ToString
                End If
                dtDate = dtDate.AddDays(1)
            Next

            Me.ViewState.Add("L_Days", arDays)

            For i = 0 To ds.Tables(0).Rows.Count - 1 'month loop
                Dim mth As String = ds.Tables(0).Rows(i)("Monthyear").ToString
                Dim startday As String = ds.Tables(0).Rows(i)("startday").ToString
                totday = CInt(ds.Tables(0).Rows(i)("MonthDays"))
                Dim d As Integer = ds.Tables(0).Columns(startday).Ordinal
                For j As Integer = 1 To totday
                    Dim mCol As String = ds.Tables(0).Columns(d).ToString
                    Dim mdtdate As New Date(CInt(ds.Tables(0).Rows(i)("YearId")), CInt(ds.Tables(0).Rows(i)("MonthId")), j)
                    Dim dtHoliday As DataTable = objEmpHoliday.GetEmployeeHoliday(CInt(Me.ViewState("L_EmpId")), mdtdate)
                    flag = False
                    For Each sKey As Integer In mDicArrayDays.Keys
                        Dim blnHoliday As Boolean = False
                        Dim dDate As Date = eZeeDate.convertDate(mDicArrayDays(sKey)).Date

                        If dtHoliday IsNot Nothing And dtHoliday.Rows.Count > 0 Then
                            Dim decLAmt As Decimal = objLeaveFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(mdtdate), CInt(Me.ViewState("L_FormId")), CInt(Me.ViewState("L_EmpId")), Session("LeaveApproverForLeaveType").ToString(), CInt(Session("EmpMaxapproverunkid")))
                            Dim mVal As String = ""
                            If (objLType._Isissueonweekend = True OrElse objLType._Isissueonholiday = True) AndAlso decLAmt > 0 Then
                                mVal = j & "~" & objLType._Color.ToString()
                            Else
                                mVal = j & "~" & dtHoliday.Rows(0)("color").ToString
                            End If

                            If decLAmt > 0 Then
                                MSQL = "UPDATE " & TblName & " SET " & mCol & " = '" & mVal & " / " & decLAmt.ToString & "' WHERE MonthYear = '" & ds.Tables(0).Rows(i)("monthyear").ToString & "' "


                                If j = dDate.Day Then
                                    If mdic_ArryDays.ContainsKey(eZeeDate.convertDate(dDate)) = False Then
                                        mdic_ArryDays.Add(eZeeDate.convertDate(dDate), eZeeDate.convertDate(mdtdate))
                                        Me.ViewState("decLAmt") = CDec(Me.ViewState("decLAmt")) + decLAmt
                                    End If
                                End If

                            Else
                                MSQL = "UPDATE " & TblName & " SET " & mCol & " = '" & mVal & "' WHERE MonthYear = '" & ds.Tables(0).Rows(i)("monthyear").ToString & "' "
                            End If
                            objDataOperation.WExecQuery(MSQL, "TblName")
                            StrIds &= "," & ds.Tables(0).Rows(i)("MonthId").ToString()
                            mStrYear &= "," & ds.Tables(0).Rows(i)("YearId").ToString()
                            blnHoliday = True
                            flag = True
                        End If

                        If dDate.Month = CInt(ds.Tables(0).Rows(i)("MonthId")) And dDate.Year = CInt(ds.Tables(0).Rows(i)("YearId")) And dDate.Day = j AndAlso blnHoliday = False Then
                            Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(dDate.DayOfWeek.ToString()) & " AND isweekend = 1")
                            If drShift.Length <= 0 Then
                                mdic_ArryDays.Add(eZeeDate.convertDate(dDate), eZeeDate.convertDate(mdtdate))
                                Dim decLAmt As Decimal = objLeaveFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(mdtdate), CInt(Me.ViewState("L_FormId")), CInt(Me.ViewState("L_EmpId")), Session("LeaveApproverForLeaveType").ToString(), CInt(Session("EmpMaxapproverunkid")))

                                Me.ViewState("decLAmt") = CDec(Me.ViewState("decLAmt")) + decLAmt
                                Dim mVal As String = j & "~" & objLType._Color.ToString
                                MSQL = "UPDATE " & TblName & " SET " & mCol & " = '" & mVal & " / " & decLAmt.ToString & "' WHERE MonthYear = '" & ds.Tables(0).Rows(i)("monthyear").ToString & "' "
                                objDataOperation.WExecQuery(MSQL, "TblName")
                                StrIds &= "," & ds.Tables(0).Rows(i)("MonthId").ToString()
                                mStrYear &= "," & ds.Tables(0).Rows(i)("YearId").ToString()
                                Exit For
                            Else
                                If flag = False Then

                                    Dim decLAmt As String = objLeaveFraction.GetEmployeeLeaveDay_Fraction(eZeeDate.convertDate(mdtdate).ToString(), CInt(Me.ViewState("L_FormId")), CInt(Me.ViewState("L_EmpId")), Session("LeaveApproverForLeaveType").ToString(), CInt(Session("EmpMaxapproverunkid"))).ToString()
                                    If CDbl(decLAmt) > 0 Then
                                        mdic_ArryDays.Add(eZeeDate.convertDate(dDate), eZeeDate.convertDate(mdtdate))
                                        Me.ViewState("decLAmt") = CDec(Me.ViewState("decLAmt")) + CDec(decLAmt)
                                        Dim mVal As String = j & "~" & objLType._Color.ToString & " / " & decLAmt.ToString
                                        MSQL = "UPDATE " & TblName & " SET " & mCol & " = '" & mVal & "' where MonthYear = '" & ds.Tables(0).Rows(i)("monthyear").ToString & "' "
                                    Else
                                        MSQL = "UPDATE " & TblName & " SET " & mCol & " = " & j & " where MonthYear = '" & ds.Tables(0).Rows(i)("monthyear").ToString & "' "
                                    End If
                                    objDataOperation.WExecQuery(MSQL, "TblName")
                                    flag = True
                                End If
                            End If
                        Else
                            If flag = False Then
                                MSQL = "UPDATE " & TblName & " SET " & mCol & " = " & j & " WHERE MonthYear = '" & ds.Tables(0).Rows(i)("monthyear").ToString & "' "
                                objDataOperation.WExecQuery(MSQL, "TblName")
                            End If
                        End If
                    Next
                    d = d + 1
                Next
            Next
            If StrIds.Length > 0 Then
                StrIds = Mid(StrIds, 2)
            End If

            If mStrYear.Length > 0 Then
                mStrYear = Mid(mStrYear, 2)
            End If

            Call Bind_Grid(StrIds, mStrYear)
            MSQL = "DROP TABLE " & TblName & ""
            lblDays.Text = Me.ViewState("decLAmt").ToString
            Me.ViewState.Add("mdic_ArryDays", mdic_ArryDays)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub Bind_Grid(ByVal StrIds As String, ByVal StrYearId As String)
        Dim MSQL As String = "" : Dim ds As New DataSet
        Try
            MSQL = "SELECT CalendarTableID,MonthYear,startday,MonthDays,MonthId,YearId,sun_1 ,Mon_1,Tue_1,Wed_1 ,Thu_1 ,Fri_1 ,Sat_1 " & vbCrLf
            MSQL &= ",sun_2 ,Mon_2 ,Tue_2 ,Wed_2 ,Thu_2 ,Fri_2 ,Sat_2 " & vbCrLf
            MSQL &= ",sun_3 ,Mon_3 ,Tue_3 ,Wed_3 ,Thu_3 ,Fri_3 ,Sat_3 " & vbCrLf
            MSQL &= ",sun_4 ,Mon_4 ,Tue_4 ,Wed_4 ,Thu_4 ,Fri_4 ,Sat_4 " & vbCrLf
            MSQL &= ",sun_5 ,Mon_5 ,Tue_5 ,Wed_5 ,Thu_5 ,Fri_5 ,Sat_5" & vbCrLf
            MSQL &= ",sun_6 ,Mon_6 ,Tue_6 ,Wed_6 ,Thu_6 ,Fri_6 ,Sat_6 " & vbCrLf
            MSQL &= " from  Issue_Leave_" & Me.ViewState("L_FormId").ToString & "_" & Me.ViewState("L_EmpId").ToString

            If StrIds.Length > 0 Then
                MSQL &= " WHERE MonthId IN (" & StrIds & ")"
            End If

            If StrYearId.Length > 0 Then
                MSQL &= " AND YearId IN (" & StrYearId & ")"
            End If

            ds = objDataOperation.WExecQuery(MSQL, "calendartable")

            dgViewLeave.DataSource = ds.Tables(0)
            dgViewLeave.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Shared Function b64encode(ByVal StrEncode As String) As String
        Try
            Dim encodedString As String
            encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
            Return (encodedString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = mstrModuleName
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.MGR_SELF_SERVICE
                    objSendMail._UserUnkid = CInt(Session("UserId"))
                    objSendMail._SenderAddress = Company._Object._Senderaddress
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception
                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub

#End Region

#Region " Datagrid Events "

    Protected Sub dgViewLeave_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgViewLeave.ItemCommand
        Try
            If e.CommandName.ToString = "ColumnClick" Then
                Dim intCellIdx As Integer = Convert.ToInt32(Request.Form("__EVENTARGUMENT").ToString)
                Dim StrDate As String = ""
                If e.Item.Cells(intCellIdx).BackColor.ToArgb = txtColor.BackColor.ToArgb Then

                    If e.Item.Cells(intCellIdx).Attributes("id") IsNot Nothing Then
                        If e.Item.Cells(intCellIdx).Attributes("H_Clor") IsNot Nothing Then
                            'RETAIN BACK THE HOLIDAY COLOR ONCE IT'S UNTICKED
                            e.Item.Cells(intCellIdx).BackColor = Color.FromArgb(CInt(e.Item.Cells(intCellIdx).Attributes("H_Clor")))
                            e.Item.Cells(intCellIdx).ForeColor = Color.White
                        Else
                            e.Item.Cells(intCellIdx).BackColor = Color.Empty
                            e.Item.Cells(intCellIdx).ForeColor = Color.Black
                        End If

                        'Pinkal (01-Feb-2014) -- Start
                        'Enhancement : TRA Changes 

                        ''ASKING CONFIRMATION FOR ISSUING LEAVE ON HOLIDAY OR WEEKEND ON PERTICULAR CELL WHEN THAT CELL IS CLICKED
                        'If e.Item.Cells(intCellIdx).BackColor.ToArgb = txtColor.BackColor.ToArgb Then
                        '    e.Item.Cells(intCellIdx).Attributes("onclick") = "if (!confirm('Are you sure you want to Issue Leave on this Holiday?')) return; " & e.Item.Cells(intCellIdx).Attributes("onclick").Substring(e.Item.Cells(intCellIdx).Attributes("onclick").IndexOf("javascript"))
                        'Else
                        '    e.Item.Cells(intCellIdx).Attributes("onclick") = "if (!confirm('This is Weekend holiday.Are you sure you want to Issue Leave on this day?')) return; " & e.Item.Cells(intCellIdx).Attributes("onclick").Substring(e.Item.Cells(intCellIdx).Attributes("onclick").IndexOf("javascript"))
                        'End If

                        'Pinkal (01-Feb-2014) -- End

                    Else
                        e.Item.Cells(intCellIdx).BackColor = Color.Empty
                        e.Item.Cells(intCellIdx).ForeColor = Color.Black
                    End If
                    'REMOVING DATES FROM LEAVE ISSUED AND MAINTAIN IT DICTIONARY FOR FINAL SAVE INFO
                    StrDate = CInt(e.Item.Cells(5).Text).ToString("#000") & CInt(e.Item.Cells(4).Text).ToString("#00") & CInt(e.Item.Cells(intCellIdx).Text.Split(CChar("/"))(0).Trim).ToString("#00")
                    mdic_ArryDays = CType(Me.ViewState("mdic_ArryDays"), Dictionary(Of String, String))
                    If mdic_ArryDays.Keys.Count > 0 Then
                        If mdic_ArryDays.ContainsKey(StrDate) Then
                            mdic_ArryDays.Remove(StrDate)

                            'Pinkal (15-Jul-2013) -- Start
                            'Enhancement : TRA Changes
                            'lblDays.Text = CDec(lblDays.Text) - objLeaveFraction.GetEmployeeLeaveDay_Fraction(StrDate, Me.ViewState("L_FormId"), Me.ViewState("L_EmpId"))
                            lblDays.Text = CStr(CDec(lblDays.Text) - objLeaveFraction.GetEmployeeLeaveDay_Fraction(StrDate, CInt(Me.ViewState("L_FormId")), CInt(Me.ViewState("L_EmpId")), "", CInt(Session("EmpMaxapproverunkid"))))
                            'Pinkal (15-Jul-2013) -- End

                        End If
                    End If
                Else
                    'ADDING ATTRIBUTE ONCLICK ON PARTICULAR CELL
                    If e.Item.Cells(intCellIdx).Attributes("id") IsNot Nothing Then
                        e.Item.Cells(intCellIdx).Attributes("onclick") = e.Item.Cells(intCellIdx).Attributes("onclick").Substring(e.Item.Cells(intCellIdx).Attributes("onclick").IndexOf("javascript"))
                    End If

                    StrDate = CInt(e.Item.Cells(5).Text).ToString("#000") & CInt(e.Item.Cells(4).Text).ToString("#00") & CInt(e.Item.Cells(intCellIdx).Text.Split(CChar("/"))(0).Trim).ToString("#00")
                    e.Item.Cells(intCellIdx).BackColor = txtColor.BackColor ' (Color.FromArgb(e.Item.Cells(intCellIdx).Attributes("cellbackcolor")))
                    e.Item.Cells(intCellIdx).ForeColor = Color.White
                    'ADDING DATES TO DICTIONARY FOR FINAL SAVE INFO, IF PARTICULAR DAY IS CLICKED
                    mdic_ArryDays = CType(Me.ViewState("mdic_ArryDays"), Dictionary(Of String, String))
                    If mdic_ArryDays.Keys.Count > 0 Then
20:                     If mdic_ArryDays.ContainsKey(StrDate) = False Then
                            mdic_ArryDays.Add(StrDate, StrDate)
                            Me.ViewState("mdic_ArryDays") = mdic_ArryDays

                            'Pinkal (15-Jul-2013) -- Start
                            'Enhancement : TRA Changes
                            'lblDays.Text = CDec(lblDays.Text) + objLeaveFraction.GetEmployeeLeaveDay_Fraction(StrDate, Me.ViewState("L_FormId"), Me.ViewState("L_EmpId"))
                            lblDays.Text = CStr(CDec(lblDays.Text) + objLeaveFraction.GetEmployeeLeaveDay_Fraction(StrDate, CInt(Me.ViewState("L_FormId")), CInt(Me.ViewState("L_EmpId")), "", CInt(Session("EmpMaxapproverunkid"))))
                            'Pinkal (15-Jul-2013) -- End


                        End If
                    Else
                        GoTo 20
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgViewLeave_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgViewLeave.ItemDataBound
        Try
            Dim objLvType As New clsleavetype_master
            objLvType._Leavetypeunkid = CInt(Me.ViewState("L_TypeId"))
            Dim arr() As String
            Dim cellidx As String = "0"
            For Each cell As TableCell In e.Item.Cells
                cell.HorizontalAlign = HorizontalAlign.Center
                e.Item.Cells(1).HorizontalAlign = HorizontalAlign.Left
                e.Item.Cells(1).Wrap = False
                arr = Split(cell.Text, "~")
                If IsNumeric(arr(0)) Then
                    If (CInt(e.Item.Cells(4).Text).ToString("#00") & CInt(arr(0)).ToString("#00")) >= (Me.ViewState("statdate")).ToString And (CInt(e.Item.Cells(4).Text).ToString("#00") & CInt(arr(0)).ToString("#00")) <= (Me.ViewState("enddate")).ToString Then
                        If Val(arr(0)) <> 0 Then
                            If arr.Count > 1 Then
                                Dim str As String() = arr(1).Split(CChar("/"))


                                'Pinkal (01-Feb-2014) -- Start
                                'Enhancement : TRA Changes

                                If str.Length > 1 Then
                                    cell.Text = arr(0) & " / " & str(1)
                                    cell.BackColor = (Color.FromArgb(CInt(str(0))))
                                Else
                                    cell.Text = arr(0)
                                    cell.BackColor = (Color.FromArgb(CInt(arr(1))))
                                End If

                                'Pinkal (01-Feb-2014) -- End

                                cell.ForeColor = Color.White
                            End If


                            Dim btn As LinkButton = CType(e.Item.Cells(dgViewLeave.Columns.Count - 1).Controls(0), LinkButton)
                            Dim strJsClick As String = ClientScript.GetPostBackClientHyperlink(btn, "")

                            '*** Add event to each date cell.
                            Dim strJs As String = strJsClick.Insert(strJsClick.Length - 2, cellidx)

                            ''*** Add this javascript to the onclick Attribute of the cell
                            'e.Item.Cells(cellidx).Attributes("onclick") = strJs

                            '*** Add this javascript to the onclick Attribute of the cell
                            If arr.Count > 1 Then
                                If CDbl(arr(1).Split(CChar("/"))(0)) = txtColor.BackColor.ToArgb Then
                                    e.Item.Cells(CInt(cellidx)).Attributes("onclick") = strJs
                                    e.Item.Cells(CInt(cellidx)).Attributes.Add("id", e.Item.ItemIndex.ToString & "_" & cellidx.ToString)
                                    Dim objEmpHoliday As New clsemployee_holiday
                                    Dim dtEmpHL As DataTable = objEmpHoliday.GetEmployeeHoliday(CInt(Me.ViewState("L_EmpId")), eZeeDate.convertDate((CInt(e.Item.Cells(5).Text).ToString("#000") & CInt(e.Item.Cells(4).Text).ToString("#00") & CInt(e.Item.Cells(CInt(cellidx)).Text.Split(CChar("/"))(0).Trim).ToString("#00"))))
                                    If dtEmpHL IsNot Nothing AndAlso dtEmpHL.Rows.Count > 0 Then
                                        e.Item.Cells(CInt(cellidx)).Attributes("H_Clor") = CStr(CInt(dtEmpHL.Rows(0)("color")))
                                    End If

                                Else
                                    If e.Item.Cells(CInt(cellidx)).Attributes("H_Clor") Is Nothing Then
                                        e.Item.Cells(CInt(cellidx)).Attributes.Add("H_Clor", arr(1).Split(CChar("/"))(0))
                                    Else
                                        e.Item.Cells(CInt(cellidx)).Attributes("H_Clor") = arr(1).Split(CChar("/"))(0)
                                    End If

                                    'Pinkal (01-Feb-2014) -- Start
                                    'Enhancement : TRA Changes

                                    'If Me.ViewState("Issue_Holiday") = True Then
                                    '    e.Item.Cells(cellidx).Attributes("onclick") = "if (!confirm('Are you sure you want to Issue Leave on this Holiday?')) return; " & strJs
                                    '    e.Item.Cells(cellidx).Attributes.Add("id", e.Item.ItemIndex.ToString & "_" & cellidx.ToString)
                                    'Else
                                    '    e.Item.Cells(cellidx).Attributes("onclick") = "if (!alert('You cannot issue leave for this holiday.')) return; " & strJs
                                    'End If

                                    'Pinkal (01-Feb-2014) -- End

                                End If
                            Else

                                'Pinkal (01-Feb-2014) -- Start
                                'Enhancement : TRA Changes

                                'If Me.ViewState("Issue_Holiday") = True Then
                                '    e.Item.Cells(cellidx).Attributes("onclick") = "if (!confirm('This is Weekend holiday.Are you sure you want to Issue Leave on this day?')) return; " & strJs
                                '    e.Item.Cells(cellidx).Attributes.Add("id", e.Item.ItemIndex.ToString & "_" & cellidx.ToString)
                                'Else
                                '    e.Item.Cells(cellidx).Attributes("onclick") = "if (!alert('This is Weekend holiday.You cannot issue leave for this day.')) return; " & strJs
                                '    e.Item.Cells(cellidx).Attributes.Add("id", e.Item.ItemIndex.ToString & "_" & cellidx.ToString)
                                'End If

                                'Pinkal (01-Feb-2014) -- End

                            End If

                            '*** Add a cursor style to the cells
                            e.Item.Cells(CInt(cellidx)).Attributes("style") += "cursor:pointer;cursor:hand;"

                        End If
                    Else
                        cell.Text = arr(0)
                    End If
                Else
                    'THIS IS FOR WEEKEND WHERE NO COLOR IS PRESENT IN THE CELL, AND ADDING EVENTS TO THAT CELL
                    If arr.Count >= 1 Then
                        Dim str As String() = arr(0).Split(CChar("/"))
                        If IsNumeric(str(0)) Then
                            cell.Text = arr(0)
                            Dim btn As LinkButton = CType(e.Item.Cells(dgViewLeave.Columns.Count - 1).Controls(0), LinkButton)
                            Dim strJsClick As String = ClientScript.GetPostBackClientHyperlink(btn, "")
                            Dim strJs As String = strJsClick.Insert(strJsClick.Length - 2, cellidx)
                            e.Item.Cells(CInt(cellidx)).Attributes("style") += "cursor:pointer;cursor:hand;"
                            e.Item.Cells(CInt(cellidx)).Attributes("onclick") = "if (!confirm('This is Weekend holiday.Are you sure you want to Issue Leave on this day?')) return; " & strJs
                            e.Item.Cells(CInt(cellidx)).Attributes.Add("id", e.Item.ItemIndex.ToString & "_" & cellidx.ToString)
                        End If
                    End If
                End If
                cellidx = CStr(CDbl(cellidx) + 1)
            Next

            e.Item.Cells(0).Visible = False
            e.Item.Cells(2).Visible = False
            e.Item.Cells(3).Visible = False
            e.Item.Cells(4).Visible = False
            e.Item.Cells(5).Visible = False

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region


    Private Sub SetLanguage()
        Try

            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

            Me.gbLeaveIssue.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbLeaveIssue.ID, Me.gbLeaveIssue.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblFormNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFormNo.ID, Me.lblFormNo.Text)
            Me.lblLeaveType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLeaveType.ID, Me.lblLeaveType.Text)
            Me.lblLeaveEndDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLeaveEndDate.ID, Me.lblLeaveEndDate.Text)
            Me.lblDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblDescription.ID, Me.lblDescription.Text)
            Me.lblLastYearAccrued.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLastYearAccrued.ID, Me.lblLastYearAccrued.Text)
            Me.lblToDateAccrued.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToDateAccrued.ID, Me.lblToDateAccrued.Text)
            Me.lblLastYearIssued.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLastYearIssued.ID, Me.lblLastYearIssued.Text)
            Me.lblTotalIssuedToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTotalIssuedToDate.ID, Me.lblTotalIssuedToDate.Text)
            Me.lblBalance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBalance.ID, Me.lblBalance.Text)
            Me.lblTotalLeaveDays.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblTotalLeaveDays.ID, Me.lblTotalLeaveDays.Text)
            Me.lblAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAmount.ID, Me.lblAmount.Text)
            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.LblLeaveBF.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblLeaveBF.ID, Me.LblLeaveBF.Text)
            Me.LblTotalAdjustment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.LblTotalAdjustment.ID, Me.LblTotalAdjustment.Text)

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Form No is compulsory information.Please Select Form No.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Leave Date is compulsory information.Please Select Leave Date between given Leave Form Period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "Please Enter Accrue amount for this paid leave.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "Leave Type is compulsory information.Please Select Leave Type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "This particular leave is mapped with")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, ", which does not have the accrue amount. Please add accrue amount for")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, " inorder to issue leave.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "You can't issue this leave form.Reason : Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 20, "You cannot issue this leave form.Reason : This Leave Limit has exceeded the Maximum Leave balance as on date Limit for this leave type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 21, "Sorry, there is no leave frequency accrue amount set for the selected short leave, please enter accrue amount for this leave from, Leave Information -> Leave Frequency.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 22, "Issued Leave Application form notification")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 24, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to Issue this leave ?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 26, "Sorry, you cannot approve/Issue for this leave type as it has been configured with setting for minimum")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 27, "Sorry, you cannot approve/Issue for this leave type as it has been configured with setting for maximum")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 28, "day(s) to be applied for this leave type.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
