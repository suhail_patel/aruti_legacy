﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_LeaveApproverList.aspx.vb"
    Inherits="Leave_wPg_LeaveApproverList" Title="Leave Approver List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<%@ Register Src="~/Controls/ConfirmYesNo.ascx" TagName="confirmyesno" TagPrefix="cnfpopup" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll.Y).val());

            }
        }


//        $("[id*=ChkAll]").live("click", function() {
//            var chkHeader = $(this);
//            var grid = $(this).closest("table");
//            $("input[type=checkbox]", grid).each(function() {
//                if (chkHeader.is(":checked")) {
//                    debugger;
//                    if ($(this).is(":visible")) {
//                        $(this).attr("checked", "checked");
//                    }

//                } else {
//                    $(this).removeAttr("checked");
//                }
//            });
//        });

//        $("[id*=ChkgvSelect]").live("click", function() {
//            var grid = $(this).closest("table");
//            var chkHeader = $("[id*=chkHeader]", grid);
//            var row = $(this).closest("tr")[0];

//            debugger;
//            if (!$(this).is(":checked")) {
//                var row = $(this).closest("tr")[0];
//                chkHeader.removeAttr("checked");
//            } else {

//                if ($("[id*=chkRow]", grid).length == $("[id*=chkRow]:checked", grid).length) {
//                    chkHeader.attr("checked", "checked");
//                }
//            }
        //        });


        $("body").on("click", "[id*=ChkAll]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkgvSelect]").prop("checked", $(chkHeader).prop("checked"));
        });


        $("body").on("click", "[id*=ChkgvSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAll]", grid);
            debugger;
            if ($("[id*=ChkgvSelect]", grid).length == $("[id*=ChkgvSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });


    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Leave Approver List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text="Leave Approver List"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLevel" runat="server" Text="Approver Level" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpLevel" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" AutoPostBack="true" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label" />
                                        <div class="form-group">
                                            <asp:DropDownList ID="drpApprover" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                     <cnfpopup:confirmyesno ID="popupActive" runat="server" />
                     <cnfpopup:confirmyesno ID="popupInactive" runat="server" />
                      <ucDel:DeleteReason ID="popup1" runat="server" Title="Are you Sure You Want To delete?:" />
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 350px">
                                            <asp:DataGrid ID="dgView" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderText="Edit"
                                                        HeaderStyle-HorizontalAlign="Center" FooterText="btnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgSelect" runat="server" CommandName="Edit" ToolTip="Edit">
                                                                    <i class="fas fa-pencil-alt text-primary"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" ToolTip="Delete">
                                                                <i class="fas fa-trash text-danger"></i>       
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkMapUser" runat="server" Text="Map User" Font-Underline="false"
                                                                CommandName="MapUser">
                                                        
                                                            </asp:LinkButton>l
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px"
                                                        ItemStyle-Width="30px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkMapLeaveType" runat="server" ToolTip="Map Leave Type" Font-Underline="false"
                                                                CommandName="MapLeaveType">
                                                           <i class="fas fa-user-cog"></i>  
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkActiveApprover" runat="server" TooTip="Active" Font-Underline="false"
                                                                CommandName="Active">
                                                                 <i class="fa fa-user-plus" style="font-size:18px;color:Green"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkInactiveApprover" runat="server" TooTip="Inactive" Font-Underline="false"
                                                                CommandName="Inactive">
                                                                    <i class="fa fa-user-times" style="font-size:18px;color:red"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="levelname" HeaderText="Level" ReadOnly="True" FooterText="colhLevel" />
                                                    <asp:BoundColumn DataField="name" HeaderText="Approver Name" ReadOnly="True" FooterText="colhApproverName" />
                                                    <asp:BoundColumn DataField="departmentname" HeaderText="Department" ReadOnly="True"
                                                        FooterText="colhDepartment" />
                                                    <asp:BoundColumn DataField="sectionname" HeaderText="Section" ReadOnly="True" FooterText="colhSection" />
                                                    <asp:BoundColumn DataField="jobname" HeaderText="Job" ReadOnly="True" FooterText="colhJob" />
                                                    <asp:BoundColumn DataField="approverunkid" HeaderText="ApproverId" ReadOnly="True"
                                                        Visible="false" />
                                                    <asp:BoundColumn DataField="IsGrp" Visible="false" />
                                                    <asp:BoundColumn DataField="ExAppr" HeaderText="External Approver" ReadOnly="true"
                                                        FooterText="colhIsExternalApprover"></asp:BoundColumn>
                                                </Columns>
                                                <PagerStyle Mode="NumericPages" Font-Bold="False" Font-Italic="False" Font-Overline="False"
                                                    Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" />
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="popupUserMapping" BackgroundCssClass="modal-backdrop"
                    TargetControlID="LblUserMapping" runat="server" PopupControlID="pnlUserMapPopup"
                    CancelControlID="btnMapClose" />
                <asp:Panel ID="pnlUserMapPopup" runat="server" CssClass="card modal-dialog" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblUserMapping" runat="server" Text="User Mapping" Font-Bold="true" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:Label ID="LblApproverMap" runat="server" Text="Approver" CssClass="form-label" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:Label ID="LblApproverVal" runat="server" Text="" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:Label ID="LblLevelMap" runat="server" Text="Approver Level" CssClass="form-label" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:Label ID="LblLevelVal" runat="server" Text="" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <asp:Label ID="LblUser" runat="server" Text="User" CssClass="form-label" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <asp:DropDownList ID="drpUser" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnMapSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                        <asp:Button ID="btnMapDelete" runat="server" Text="Delete" CssClass="btn btn-primary" />
                        <asp:Button ID="btnMapClose" runat="server" Text="Close" CssClass="btn btn-default" />
                    </div>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupLeaveMapping" BackgroundCssClass="modal-backdrop"
                    TargetControlID="LblLeaveMapping" runat="server" PopupControlID="pnlLeaveMappingPopup"
                    CancelControlID="btnLeaveMapClose" />
                <asp:Panel ID="pnlLeaveMappingPopup" runat="server" CssClass="card modal-dialog"
                    Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblLeaveMapping" runat="server" Text="Leave Type Mapping" Font-Bold="true" />
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <asp:Label ID="LblApproverLeaveMap" runat="server" Text="Approver" CssClass="form-label" />
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <asp:Label ID="LblApproverLeaveMapVal" runat="server" Text="" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                <asp:Label ID="LblLevelLeaveMap" runat="server" Text="Approver Level" CssClass="form-label" />
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <asp:Label ID="LblLevelLeaveMapVal" runat="server" Text="" CssClass="form-label" />
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 350px">
                                    <asp:GridView ID="GvLeaveTypeMapping" runat="server" AutoGenerateColumns="false"
                                        AllowPaging="false" DataKeyNames="leavetypeunkid,leavetypecode" CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="30px" ItemStyle-Width="30px">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="ChkAll" runat="server" CssClass="filled-in" Text=" " />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkgvSelect" runat="server" CssClass="filled-in" Text=" " />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="leavetypecode" HeaderText="Leave Code" ReadOnly="True"
                                                FooterText="dgcolhLeaveCode" />
                                            <asp:BoundField DataField="leavename" HeaderText="Leave Type" ReadOnly="True" FooterText="dgcolhLeaveType" />
                                            <asp:BoundField DataField="ispaid" HeaderText="IsPaid" ReadOnly="True" FooterText="dgcolhPaidLeave" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Button ID="btnLeaveMapSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                            <asp:Button ID="btnLeaveMapClose" runat="server" Text="Close" CssClass="btn btn-default" />
                        </div>
                </asp:Panel>
               
               
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
