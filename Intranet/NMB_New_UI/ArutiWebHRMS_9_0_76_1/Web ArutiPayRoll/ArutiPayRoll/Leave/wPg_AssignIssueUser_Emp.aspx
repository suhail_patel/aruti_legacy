﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_AssignIssueUser_Emp.aspx.vb"
    Inherits="Leave_wPg_AssignIssueUser_Emp" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
                $("#scrollable-container1").scrollTop($(scroll1.Y).val());
            }
        }

        $("body").on("click", "[id*=ChkAll]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkgvSelect]").prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=ChkgvSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAll]", grid);
            debugger;
            if ($("[id*=ChkgvSelect]", grid).length == $("[id*=ChkgvSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });


        $("body").on("click", "[id*=ChkSelectedAllEmp]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkSelectedEmp]").prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=ChkSelectedEmp]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkSelectedAllEmp]", grid);
            debugger;
            if ($("[id*=ChkSelectedEmp]", grid).length == $("[id*=ChkSelectedEmp]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });


        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching() {
            if ($('#txtSearch').val().length > 0) {
                $('#<%= GvEmployee.ClientID %> tbody tr').hide();
                $('#<%= GvEmployee.ClientID %> tbody tr:first').show();
                $('#<%= GvEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearch').val() + '\')').parent().show();
            }
            else if ($('#txtSearch').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= GvEmployee.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtSearch').val('');
            $('#<%= GvEmployee.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtSearch').focus();
        }
        
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Assign Issue User to Employee"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblCaption" runat="server" Text="Assign Issue User to Employee"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblIssueUser" runat="server" Text="Issue User" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="drpIssueUser" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="LblToIssueUser" runat="server" Text="To Issue User" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="drpToIssueUser" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblFilterDepartment" runat="server" Text="Department" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="drpDepartment" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblFilterjob" runat="server" Text="Job" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="drpJob" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblFilterSection" runat="server" Text="Section" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="drpSection" runat="server" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                               <%-- <asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>--%>
                                                               <input type="text" id="txtSearch" name="txtSearch" placeholder = "Type To Search Text"  maxlength="50"  onkeyup="FromSearching();" class="form-control" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 350px">
                                                            <asp:GridView ID="GvEmployee" runat="server" AutoGenerateColumns="False" ShowFooter="False" DataKeyNames="employeeunkid,Code,Employee Name,Department,Section,Job"
                                                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                                        HeaderStyle-Width="50px" ItemStyle-Width="50px">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="ChkAll" runat="server" CssClass="filled-in" Text=" " /> <%-- AutoPostBack="true" OnCheckedChanged="chkAll_CheckedChanged"--%>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ChkgvSelect" runat="server" CssClass="filled-in" Text=" " /> <%--AutoPostBack="true" OnCheckedChanged="ChkgvSelect_CheckedChanged"--%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Code" HeaderText="Code" ReadOnly="true" FooterText="ColhEmployeecode" />
                                                                    <asp:BoundField DataField="Employee Name" HeaderText="Employee" ReadOnly="true" FooterText="ColhEmp" />
                                                                    <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                        Visible="false" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="height: 405px">
                                                            <asp:GridView ID="GvSelectedEmployee" runat="server" AutoGenerateColumns="False" DataKeyNames="issueusertranunkid,GUID,AUD"
                                                                ShowFooter="False" AllowPaging="false" CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" ItemStyle-Width="50px">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="ChkSelectedAllEmp" runat="server" CssClass="filled-in" Text=" " /> <%--AutoPostBack="true" OnCheckedChanged="ChkSelectedEmpAll_CheckedChanged"--%>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="ChkSelectedEmp" runat="server" CssClass="filled-in" Text=" " /><%-- AutoPostBack="true" OnCheckedChanged="ChkgvSelectedEmp_CheckedChanged"--%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="issueuser" HeaderText="Issue User" ReadOnly="true" FooterText="colhIssueUser" />
                                                                    <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="true" />
                                                                    <asp:BoundField DataField="employee" HeaderText="Employee" ReadOnly="true" FooterText="colhEmployee" />
                                                                    <asp:BoundField DataField="department" HeaderText="Department" ReadOnly="true" FooterText="colhDept" />
                                                                    <asp:BoundField DataField="section" HeaderText="Section" ReadOnly="true" FooterText="colhSection" />
                                                                    <asp:BoundField DataField="job" HeaderText="Job" ReadOnly="true" FooterText="colhJob" />
                                                                    <asp:BoundField DataField="employeeunkid" HeaderText="EmployeeId" ReadOnly="true"
                                                                        Visible="false" />
                                                                    <asp:BoundField DataField="departmentunkid" HeaderText="Departmentunkid" ReadOnly="true"
                                                                        Visible="false" />
                                                                    <asp:BoundField DataField="sectionunkid" HeaderText="Sectionunkid" ReadOnly="true"
                                                                        Visible="false" />
                                                                    <asp:BoundField DataField="jobunkid" HeaderText="Jobunkid" ReadOnly="true" Visible="false" />
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="footer">
                                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-primary" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
