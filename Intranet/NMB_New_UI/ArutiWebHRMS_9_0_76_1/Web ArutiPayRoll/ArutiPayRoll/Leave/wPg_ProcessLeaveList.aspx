﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_ProcessLeaveList.aspx.vb"
    Inherits="Leave_wPg_ProcessLeaveList" Title="Leave Approval List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/PreviewAttachment.ascx" TagName="PreviewAttachment"
    TagPrefix="PrviewAttchmet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />
    <asp:HiddenField ID="hfScrollPosition1" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };
        var scroll1 = {
            Y: '#<%= hfScrollPosition1.ClientID %>'
        };
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());

        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");
            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());

            }

        }
        //            $('input[type="image"]').live("click",function(){
        //                alert('hiiii');
        //                $(".cal_Theme1").children("div").css("z-index","99999");
        //            });
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Leave Approval List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboEmployee" runat="server" Visible="True" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4  col-md-4  col-sm-12 col-xs-12">
                                        <asp:Label ID="lblFormNo" runat="server" Text="Application No" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtFormNo" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4  col-md-4  col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStartDate" runat="server" Text="Start Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpStartDate" AutoPostBack="false" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApprover" runat="server" Text="Approver" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <asp:TextBox ID="txtApprover" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12">
                                        <asp:Label ID="lblApplyDate" runat="server" Text="Apply Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpApplyDate" AutoPostBack="false" runat="server" />
                                    </div>
                                    <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEndDate" runat="server" Text="Return Date" CssClass="form-label"></asp:Label>
                                        <uc2:DateCtrl ID="dtpEndDate" AutoPostBack="false" runat="server" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12">
                                        <asp:Label ID="lblLeaveType" runat="server" Text="Leave" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboLeaveType" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12">
                                        <asp:Label ID="lblStatus" runat="server" Text="Status" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboStatus" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4  col-sm-12 col-xs-12 m-t-35">
                                        <div class="col-lg-6 col-md-6  col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkMyApprovals" runat="server" Text="My Approvals" Checked="true" />
                                        </div>
                                        <div class="col-lg-6 col-md-6  col-sm-12 col-xs-12">
                                            <asp:CheckBox ID="chkIncludeClosedFYTransactions" runat="server" Text="Include Closed FY Transactions" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="BtnSearch" CssClass="btn btn-primary" runat="server" Text="Search" />
                                <asp:Button ID="Btnclose" runat="server" CssClass=" btn btn-default" Text="Close" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-xs-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:DataGrid ID="dgView" runat="server" Width="150%" AutoGenerateColumns="False"
                                                AllowPaging="false" CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-HorizontalAlign="Left"
                                                        FooterText="btnEdit">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                                <asp:LinkButton ID="Imgchange" runat="server" CommandName="Select" ToolTip="Change Approval Status"> 
                                                                            <img src="../images/change_status.png" height="20px" width="20px" alt="Change Approval Status" />
                                                                </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px"
                                                        ItemStyle-Width="50px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkLeaveExpense" runat="server" CommandName="Expense" Font-Underline="false"
                                                                ToolTip="Leave Expense">
                                                                     <i class="fas fa-file-invoice-dollar"  style="font-size:20px"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--1--%>
                                                    <asp:TemplateColumn  HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="50px" ItemStyle-Width="50px">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkIssueLeave" runat="server" CommandName="Issue" Font-Underline="false"
                                                                ToolTip="Issue Leave">
                                                                    <i class="fas fa-calendar-check" style="font-size:20px"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <%--2--%>
                                                    <asp:BoundColumn DataField="formno" HeaderText="Application No" FooterText="colhFormNo" />
                                                    <%--3--%>
                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Employee Code" FooterText="colhEmployeeCode" />
                                                    <%--4--%>
                                                    <asp:BoundColumn DataField="employeename" HeaderText="Employee Name" FooterText="colhEmployee"
                                                        ItemStyle-Width="200px" />
                                                    <%--5--%>
                                                    <asp:BoundColumn DataField="approvername" HeaderText="Approver" FooterText="colhApprover"
                                                        ItemStyle-Width="200px" />
                                                    <%--6--%>
                                                    <asp:BoundColumn DataField="levelname" HeaderText="Approver Level" FooterText=""
                                                        HeaderStyle-Wrap="false" />
                                                    <%--7--%>
                                                    <asp:BoundColumn DataField="leavename" HeaderText="Leave" FooterText="colLeaveType"
                                                        ItemStyle-Width="200px" />
                                                    <%--8--%>
                                                    <asp:BoundColumn DataField="startdate" HeaderText="Start Date" FooterText="colhStartDate"
                                                        HeaderStyle-Wrap="false" ItemStyle-Width="150px" />
                                                    <%--9--%>
                                                    <asp:BoundColumn DataField="enddate" HeaderText="End Date" FooterText="colhEndDate"
                                                        HeaderStyle-Wrap="false" ItemStyle-Width="150px" />
                                                    <%--10--%>
                                                    <asp:BoundColumn DataField="days" HeaderText="Days" FooterText="colhLeaveDays" HeaderStyle-Width="65px"
                                                        ItemStyle-Width="65px" />
                                                    <%--11--%>
                                                    <asp:BoundColumn DataField="status" HeaderText="Status" FooterText="colhStatus"  ItemStyle-Width="120px" />
                                                    <%--12--%>
                                                    <asp:BoundColumn DataField="formunkid" HeaderText="Formunkid" Visible="False" />
                                                    <%--13--%>
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="Employeeunkid" Visible="False" />
                                                    <%--14--%>
                                                    <asp:BoundColumn DataField="approverunkid" Visible="False" />
                                                    <%--15--%>
                                                    <asp:BoundColumn DataField="statusunkid" Visible="False" />
                                                    <%--16--%>
                                                    <asp:BoundColumn DataField="mapuserunkid" Visible="False" />
                                                    <%--17--%>
                                                    <asp:BoundColumn DataField="mapapproverunkid" Visible="False" />
                                                    <%--18--%>
                                                    <asp:BoundColumn DataField="priority" Visible="False" />
                                                    <%--19--%>
                                                    <asp:BoundColumn DataField="remark" HeaderText="Remark" FooterText="gbRemark" HeaderStyle-Wrap="false" />
                                                    <%--20--%>
                                                    <asp:BoundColumn DataField="iscancelform" Visible="false" />
                                                    <%--21--%>
                                                    <asp:BoundColumn DataField="leaveissueunkid" Visible="false" />
                                                    <%--22--%>
                                                    <asp:BoundColumn DataField="leaveapproverunkid" Visible="false" />
                                                    <%--23--%>
                                                    <asp:BoundColumn DataField="IsGrp" Visible="false" />
                                                    <%--24--%>
                                                    <asp:BoundColumn DataField="isexternalapprover" Visible="false" />
                                                    <%--25--%>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <cc1:ModalPopupExtender ID="popupClaimRequest" runat="server" BackgroundCssClass="modal-backdrop"
                    TargetControlID="hdf_ClaimRequest" PopupControlID="pnl_ClaimRequest" 
                    CancelControlID="hdf_ClaimRequest" />
                     
                <asp:Panel ID="pnl_ClaimRequest" runat="server" CssClass="card modal-dialog modal-lg" Style="display: none;">
                    <div class="header">
                        <h2>
                             <asp:Label ID="lblpopupHeader" runat="server" Text="Claim & Request"></asp:Label>
                        </h2>
                        <ul class="header-dropdown m-r--5">
                                    <asp:LinkButton ID="lnkClaimDepedents" runat="server" ToolTip="View Depedents List">
                                              <i class="fas fa-restroom"></i>
                                    </asp:LinkButton>
                         </ul>
                    </div>
                    <div class="body" style="height: 500px;">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="card inner-card">
                                    <div class="body">
                                        <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblExpCategory" runat="server" Text="Exp.Cat." CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                             <asp:DropDownList ID="cboExpCategory" runat="server" AutoPostBack="true"  />
                                                    </div>
                                                     <asp:Label ID="lblPeriod" runat="server" Visible="false" Text="Period" CssClass="form-label"></asp:Label>
                                                     <div class="form-group">
                                                            <asp:DropDownList ID="cboPeriod" runat="server" Visible="false" AutoPostBack="true" />
                                                     </div>
                                            </div>
                                       </div>
                                       <div class="row clearfix">
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                     <asp:Label ID="lblClaimNo" runat="server" Text="Claim No" CssClass="form-label"></asp:Label>
                                                     <div class="form-group">
                                                            <div class="form-line">
                                                                    <asp:TextBox ID="txtClaimNo" runat="server" CssClass="form-control"></asp:TextBox>     
                                                            </div>
                                                      </div>      
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="form-label"></asp:Label>
                                                    <uc2:DateCtrl ID="dtpDate" runat="server" />
                                            </div>
                                       </div>
                                       <div class="row clearfix">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblEmpAddEdit" runat="server" Text="Employee" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                              <asp:DropDownList ID="cboClaimEmployee" runat="server" AutoPostBack="true" />
                                                    </div>
                                            </div>
                                       </div>
                                        <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                         <asp:Label ID="lblLeaveTypeAddEdit" runat="server" Text="Leave Type" CssClass="form-label"></asp:Label>
                                                         <div class="form-group">
                                                                <asp:DropDownList ID="cboClaimLeaveType" runat="server" AutoPostBack="true" Enabled="False" />
                                                         </div>
                                                </div>
                                        </div>
                                         <div class="row clearfix">
                                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="objlblValue" runat="server" Text="Leave Form" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                                 <asp:DropDownList ID="cboReference" runat="server" AutoPostBack="false" Enabled="False" />
                                                        </div>
                                                 </div>
                                         </div>
                                         <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                         <asp:Label ID="LblDomicileAdd" runat="server" Text="Domicile Address" CssClass="form-label"></asp:Label>
                                                          <div class="form-group">
                                                                <div class="form-line">
                                                                        <asp:TextBox ID="txtDomicileAddress" runat="server" TextMode="MultiLine" Rows="3" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                          </div>      
                                                </div>
                                         </div>
                                   </div>
                              </div>
                          </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                              <div class="card inner-card">
                                    <div class="body">
                                          <div class="row clearfix">
                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblExpense" runat="server" Text="Expense" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                                   <asp:DropDownList ID="cboExpense" runat="server" AutoPostBack="true" />
                                                        </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblUoM" runat="server" Text="UoM Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                             <div class="form-line">
                                                                    <asp:TextBox ID="txtUoMType" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                              </div>
                                                         </div>     
                                                </div>
                                        </div>
                                          <div class="row clearfix">
                                             <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                     <asp:Label ID="lblSector" runat="server" Text="Sector/Route" CssClass="form-label"></asp:Label>
                                                      <div class="form-group">
                                                            <asp:DropDownList ID="cboSectorRoute" runat="server" AutoPostBack="true"/>
                                                      </div>
                                             </div>
                                             <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                    <asp:Label ID="lblQty" runat="server" Text="Qty" CssClass="form-label"></asp:Label>
                                                    <div class="form-group">
                                                             <div class="form-line">
                                                                        <asp:TextBox ID="txtCalimQty" runat="server" style="text-align:right" Text="0"  AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                                             </div>
                                                      </div>       
                                             </div>
                                        </div>
                                          <div class="row clearfix">
                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCostCenter" runat="server" Text="Cost Center" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                                <asp:DropDownList ID="cboCostCenter" runat="server" AutoPostBack="true" />
                                                        </div>
                                                </div>
                                                 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblUnitPrice" runat="server" Text="Unit Price" CssClass="form-label"></asp:Label>
                                                        <asp:Label ID="lblCosting" runat="server" Text="Costing" Visible="false" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                             <div class="form-line">
                                                                      <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="form-control" Text="0"  AutoPostBack="true" ></asp:TextBox>
                                                                      <asp:TextBox ID="txtCosting" runat="server" CssClass="form-control" Text="0.00"  Enabled="false" Visible="false"></asp:TextBox>
                                                                      <asp:HiddenField ID="txtCostingTag" runat="server" />
                                                             </div>
                                                        </div>     
                                                 </div>
                                         </div>
                                          <div class="row clearfix">
                                                 <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                         <asp:Panel ID="pnlBalAsonDate" runat="server">
                                                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 m-l--15">
                                                                        <asp:Label ID="lblBalanceasondate" runat="server" Text="Balance As on Date" CssClass="form-label"></asp:Label>
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                      <asp:TextBox ID="txtBalanceAsOnDate" runat="server" CssClass="form-control"  Enabled="false" Style="text-align: right"></asp:TextBox>
                                                                            </div>
                                                                        </div>    
                                                                 </div>
                                                                 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 m-l--15">
                                                                        <asp:Label ID="lblBalanceAddEdit" runat="server" Text="Balance" CssClass="form-label"></asp:Label>
                                                                        <div class="form-group">
                                                                            <div class="form-line">
                                                                                     <asp:TextBox ID="txtClaimBalance" runat="server" CssClass="form-control" Enabled="false" Style="text-align: right"></asp:TextBox>
                                                                            </div>
                                                                        </div>    
                                                                 </div>
                                                         </asp:Panel>
                                                 </div>
                                                 <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                         <asp:Label ID="LblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                                         <div class="form-group">
                                                                  <asp:DropDownList ID="CboCurrency" runat="server" />
                                                         </div>
                                                 </div>
                                          </div>
                                          <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                                                <li role="presentation" class="active"><a href="#tbpExpenseRemark" data-toggle="tab">
                                                                    <asp:Label ID="tbExpenseRemark" runat="server" Text="Expense Remark" CssClass="form-label" />
                                                                </a></li>
                                                                <li role="presentation"><a href="#tbpClaimRemark" data-toggle="tab">
                                                                    <asp:Label ID="tbClaimRemark" runat="server" Text="Claim Remark" CssClass="form-label" />
                                                                </a></li>
                                                        </ul>
                                                            <!-- Tab panes -->
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane fade in active" id="tbpExpenseRemark">
                                                                <div class="form-group">
                                                                    <div class="form-line">
                                                                              <asp:TextBox ID="txtExpRemark" runat="server" TextMode="MultiLine"  CssClass="form-control"></asp:TextBox>
                                                                    </div>
                                                                </div>    
                                                            </div>
                                                             <div role="tabpanel" class="tab-pane fade" id="tbpClaimRemark">
                                                                  <div class="form-group">
                                                                        <div class="form-line">
                                                                                 <asp:TextBox ID="txtClaimRemark" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                                        </div>
                                                                   </div>     
                                                            </div>  
                                                         </div>
                                                          <asp:HiddenField ID="hdf_ClaimRequest" runat="server" />   
                                                    </div>
                                           </div>         
                                     </div>
                               </div>          
                          </div>
                        </div>
                        <div class="row clearfix">
                             <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                    <div class="card inner-card">
                                        <div class="body">
                                            <div class="row clearfix">
                                                <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                                                    <div class="table-responsive" style="height: 300px">
                                                        <asp:DataGrid ID="dgvData" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                                AllowPaging="false"  CssClass="table table-hover table-bordered">
                                                                <Columns>
                                                                    <asp:TemplateColumn HeaderStyle-Width="25px" ItemStyle-Width="25px" HeaderStyle-HorizontalAlign="Center"
                                                                        ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkPreview" runat="server" ToolTip="Preview" Font-Underline="false"
                                                                                CommandName="Preview">
                                                                                        <i class="fa fa-eye" aria-hidden="true" style="font-size:20px;"></i>                                                                                              
                                                                            </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <%--0--%>
                                                                    <asp:BoundColumn DataField="expense" HeaderText="Claim/Expense Desc" FooterText="dgcolhExpense" />
                                                                    <%--1--%>
                                                                    <asp:BoundColumn DataField="sector" HeaderText="Sector / Route" FooterText="dgcolhSectorRoute" />
                                                                    <%--2--%>
                                                                    <asp:BoundColumn DataField="uom" HeaderText="UoM" FooterText="dgcolhUoM" />
                                                                    <%--3--%>
                                                                    <asp:BoundColumn DataField="quantity" HeaderText="Quantity" FooterText="dgcolhQty"
                                                                        ItemStyle-CssClass="txttextalignright" />
                                                                    <%--4--%>
                                                                    <asp:BoundColumn DataField="unitprice" HeaderText="Unit Price" FooterText="dgcolhUnitPrice"
                                                                        ItemStyle-CssClass="txttextalignright" />
                                                                    <%--5--%>
                                                                    <asp:BoundColumn DataField="amount" HeaderText="Amount" FooterText="dgcolhAmount"
                                                                        ItemStyle-CssClass="txttextalignright" />
                                                                    <%--6--%>
                                                                    <asp:BoundColumn DataField="expense_remark" HeaderText="Expense Remark" FooterText="dgcolhExpenseRemark" />
                                                                    <%--7--%>
                                                                    <asp:BoundColumn DataField="crtranunkid" HeaderText="objdgcolhTranId" Visible="false" />
                                                                    <%--8--%>
                                                                    <asp:BoundColumn DataField="crmasterunkid" HeaderText="objdgcolhMasterId" Visible="false" />
                                                                    <%--9--%>
                                                                    <asp:BoundColumn DataField="GUID" HeaderText="objdgcolhGUID" Visible="false" />
                                                                    <%--10--%>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                    </div>
                                               </div>
                                            </div>
                                            <div class="row clearfix">
                                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                    </div>
                                                    <div class="col-lg-3 col-md-3  col-sm-3 col-xs-3">
                                                            <asp:Label ID="lblGrandTotal" runat="server" Text="Grand Total" CssClass="form-label"></asp:Label>
                                                             <div class="form-group">
                                                                    <div class="form-line">
                                                                            <asp:TextBox ID="txtGrandTotal" runat="server" Enabled="False" CssClass="form-control"></asp:TextBox>
                                                                            <asp:HiddenField ID="hdf_Claim" runat="server" />
                                                                    </div>
                                                            </div>        
                                                    </div>
                                             </div>       
                                       </div>
                                    </div>
                                </div>                                                        
                          </div>
                        <div class="footer">
                             <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div style="position: absolute; text-align: left;">
                                                <asp:Button ID="btnViewScanAttchment" runat="server" Text="View Scan/Attchment" Visible="false"
                                                    CssClass="btn btn-primary" />
                                            </div>
                                            <asp:Button ID="btnClaimClose" runat="server" Text="Close" CssClass="btn btn-primary"
                                                CausesValidation="false" />
                                        </ContentTemplate>
                              </asp:UpdatePanel>
                        </div>
                  </div>                           
                                            
                    
                    <%--<div class="panel-primary" style="margin-bottom: 0px;">
                        <div class="panel-heading">
                           
                        </div>
                        <div class="panel-body">
                            <div id="Div1" class="panel-default">
                                <div id="Div2" class="panel-heading-default">
                                    <div style="float: left;">
                                      
                                    </div>
                                    <div style="text-align: right">
                                        
                                    </div>
                                </div>
                                <div id="Div6" class="panel-body-default">
                                    <table style="width: 100%;">
                                        <tr style="width: 100%">
                                            <td style="border-right: solid 1px #DDD; width: 40%;">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            
                                                        </td>
                                                        <td style="width: 75%" colspan="3">
                                                           
                                                          
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                           
                                                        </td>
                                                        <td style="width: 30%">
                                                           
                                                        </td>
                                                        <td style="width: 10%">
                                                            
                                                        </td>
                                                        <td style="width: 35%">
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            
                                                        </td>
                                                        <td colspan="3" style="width: 75%">
                                                          
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                           
                                                        </td>
                                                        <td colspan="3" style="width: 75%">
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                            
                                                        </td>
                                                        <td colspan="3" style="width: 75%">
                                                           
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 25%">
                                                           
                                                        </td>
                                                        <td colspan="3" style="width: 75%">
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td style="width: 60">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%">
                                                            
                                                        </td>
                                                        <td colspan="3" style="width: 90%">
                                                         
                                                        </td>
                                                        <td style="width: 10%">
                                                            
                                                        </td>
                                                        <td style="width: 20%; padding: 0" colspan="1">
                                                           
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%">
                                                           
                                                        </td>
                                                        <td style="width: 50%" colspan="3">
                                                            
                                                        </td>
                                                        <td style="width: 10%">
                                                            
                                                        </td>
                                                        <td style="width: 20%; padding: 0" colspan="1">
                                                        
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 10%">
                                                            
                                                        </td>
                                                        <td style="width: 50%" colspan="3">
                                                          
                                                        </td>
                                                        <td style="width: 13%">
                                                            
                                                            
                                                        </td>
                                                        <td style="width: 20%">
                                                           
                                                          
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td style="width: 14%">
                                                            
                                                        </td>
                                                        <td style="width: 20%">
                                                          
                                                        </td>
                                                        <td style="width: 11%">
                                                            
                                                        </td>
                                                        <td style="width: 15%">
                                                           
                                                        </td>
                                                        <td style="width: 13%">
                                                           
                                                        </td>
                                                        <td style="width: 20%">
                                                          
                                                        </td>
                                                    </tr>
                                                    <tr style="width: 100%">
                                                        <td colspan="6">
                                                            <cc1:TabContainer ID="tabmain" runat="server" ActiveTabIndex="0">
                                                                <cc1:TabPanel ID="tbExpenseRemark" runat="server" HeaderText="Expense Remark">
                                                                    <ContentTemplate>
                                                                      
                                                                    </ContentTemplate>
                                                                </cc1:TabPanel>
                                                                <cc1:TabPanel ID="tbClaimRemark" runat="server" HeaderText="Claim Remark">
                                                                    <ContentTemplate>
                                                                       
                                                                    </ContentTemplate>
                                                                </cc1:TabPanel>
                                                            </cc1:TabContainer>
                                                           
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div id="Div7" class="panel-default">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 100%">
                                            <asp:Panel ID="pnl_dgvdata" runat="server" Height="170px" ScrollBars="Auto">
                                                
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="text-align: right">
                                            
                                            
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                    
                                </div>
                            </div>
                           
                        </div>
                    </div>--%>
                </asp:Panel>
                <cc1:ModalPopupExtender ID="popupEmpDepedents" runat="server" BackgroundCssClass="modal-backdrop bd-l2"
                    TargetControlID="hdf_claimDeletePopup" PopupControlID="pnlEmpDepedents" CancelControlID="btnEmppnlEmpDepedentsClose">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="pnlEmpDepedents" runat="server" CssClass="card modal-dialog modal-l2" Style="display: none;">
                    <div class="header">
                        <h2>
                            <asp:Label ID="LblEmpDependentsList" runat="server" Text="Dependents List"></asp:Label>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table-responsive" style="height: 250px;">
                                    <asp:DataGrid ID="dgDepedent" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                        CssClass="table table-hover table-bordered">
                                        <Columns>
                                            <asp:BoundColumn DataField="dependants" HeaderText="Name" ReadOnly="true" FooterText="dgcolhName" />
                                            <asp:BoundColumn DataField="gender" HeaderText="Gender" ReadOnly="true" FooterText="dgcolhGender" />
                                            <asp:BoundColumn DataField="age" HeaderText="Age" ReadOnly="true" FooterText="dgcolhAge" />
                                            <asp:BoundColumn DataField="Months" HeaderText="Month" ReadOnly="true" FooterText="dgcolhMonth" />
                                            <asp:BoundColumn DataField="relation" HeaderText="Relation" ReadOnly="true" FooterText="dgcolhRelation" />
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnEmppnlEmpDepedentsClose" runat="server" Text="Cancel" CssClass="btn btn-primary" />
                        <asp:HiddenField ID="hdf_claimDeletePopup" runat="server" />
                    </div>
                </asp:Panel>
                
                 <PrviewAttchmet:PreviewAttachment ID="popup_ShowAttchment" runat="server" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="popup_ShowAttchment" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
