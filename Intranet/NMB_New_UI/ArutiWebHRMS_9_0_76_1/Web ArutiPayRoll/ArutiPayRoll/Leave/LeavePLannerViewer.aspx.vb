﻿#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data

#End Region

Partial Class LeavePLannerViewer
    Inherits Basepage

#Region " Private Variables "

    Dim msql As String
    Dim clsDataOpr As eZeeCommonLib.clsDataOperation
    Dim objGlobalAccess As New GlobalAccess
    Dim ds As DataSet
    Dim DisplayMessage As New CommonCodes
    Dim clsuser As New User
    Private WithEvents hplink As New System.Web.UI.WebControls.Button
    Private _prUserunkid As Int16
    Public Property UserId() As Int16
        Get
            Return _prUserunkid
        End Get
        Set(ByVal value As Int16)
            _prUserunkid = value
        End Set
    End Property


    'Pinkal (06-May-2014) -- Start
    'Enhancement : Language Changes 
    Private ReadOnly mstrModuleName As String = "frmLeaveSchedular"
    'Pinkal (06-May-2014) -- End


#End Region

#Region " Private Functions & Procedure "

    Public Sub Fetchlaststatus()
        Try
            Dim str() As String
            str = Split(Session("Filstate").ToString, "~")
            Calendar1.TodaysDate = CDate(str(0))
            Calendar1.SelectedDate = Calendar1.TodaysDate.Date
            drpdepartment.SelectedValue = str(1)
            drpjob.SelectedValue = str(2)
            drpsection.SelectedValue = str(3)
            RbtlistView.SelectedValue = str(4)

            Session("RidirectFromViewer") = False


            'Pinkal (02-May-2017) -- Start
            'Enhancement - Working on Leave Enhancement for HJF.

            'generateCalendar("Cal_" & Me.ViewState("Empunkid") & "", Calendar1.SelectedDate, IIf(RbtlistView.SelectedValue = 0, (Calendar1.SelectedDate).AddDays(6), (Calendar1.SelectedDate).AddMonths(1).AddDays(-1)), drpdepartment.SelectedValue, drpsection.SelectedValue, drpjob.SelectedValue)
            generateCalendar("Cal_" & Me.ViewState("Empunkid") & "", Calendar1.SelectedDate, IIf(RbtlistView.SelectedValue = 0, (Calendar1.SelectedDate).AddDays(6), (Calendar1.SelectedDate).AddMonths(1).AddDays(-1)))
            'Pinkal (02-May-2017) -- End
            BindGrid()
        Catch ex As Exception
            DisplayMessage.DisplayMessage("Error in Fetchlaststatus function " & ex.Message, Me)
        End Try
    End Sub

    Public Sub SaveFilterStatus()
        Try
            Session("Filstate") = Calendar1.SelectedDate & "~" & drpdepartment.SelectedValue & "~" & drpjob.SelectedValue & "~" & drpsection.SelectedValue & "~" & RbtlistView.SelectedValue
        Catch ex As Exception
            DisplayMessage.DisplayMessage("Error inSaveFilterStatus function " & ex.Message, Me)
        End Try
    End Sub


    'Pinkal (02-May-2017) -- Start
    'Enhancement - Working on Leave Enhancement for HJF.

    'Public Sub generateCalendar(ByVal tblnm As String, ByVal stdt As DateTime, ByVal enddt As DateTime, ByVal depid As Int16, ByVal secid As Int16, ByVal jobid As Int16)
    '    Try
    '        Dim flag As Boolean = True
    '        Dim inflag As Boolean

    '        CreateCalendarTable(tblnm, stdt, enddt, depid, secid, jobid, 112)
    '        msql = "   select p.leaveplannerunkid, l.leavename as leavetypename, p.employeeunkid  as EmpId, convert(varchar(8),p.startdate,112) as StartDT,  datediff(day,p.startdate,p.stopdate) as applieddays  , l.leavetypeunkid   , l.Color as color ,p.leaveplannerunkid as PlannerId  "
    '        msql += "  from lvleaveplanner p" & vbCrLf
    '        msql += "  left outer Join lvleavetype_master l on l.leavetypeunkid = p.leavetypeunkid" & vbCrLf
    '        msql += "  where 1=1 AND p.isvoid = 0 " & vbCrLf
    '        If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '            msql += " and p.userunkid IN(-1," & Session("UserId") & ")" & vbCrLf
    '        Else
    '            msql += " and p.employeeunkid =" & Session("Employeeunkid") & "" & vbCrLf
    '        End If
    '        msql += "  order by PlannerId,EmpId,StartDT"

    '        ds = clsDataOpr.WExecQuery(msql, "lvleaveplanner")
    '        Dim mFldName As String

    '        For i As Int16 = 0 To ds.Tables(0).Rows.Count - 1
    '            msql = ""
    '            msql = "update  " & tblnm & "   set "
    '            mFldName = ds.Tables(0).Rows(i)("StartDT")
    '            inflag = True
    '            For j = 0 To ds.Tables(0).Rows(i)("applieddays")
    '                If (eZeeDate.convertDate(mFldName.ToString).Date >= CDate(stdt)) Then ' for -> 
    '                    If (eZeeDate.convertDate(mFldName.ToString).Date <= CDate(enddt)) Then ' calender table ends here, can not update Columns which does not exist.
    '                        If inflag = True Then
    '                            msql += "[" & mFldName & "]  = '" & ds.Tables(0).Rows(i)("color") & "~" & ds.Tables(0).Rows(i)("leaveplannerunkid") & "~" & ds.Tables(0).Rows(i)("leavetypename") & "'"
    '                        Else
    '                            msql += ",[" & mFldName & "]  = '" & ds.Tables(0).Rows(i)("color") & "~" & ds.Tables(0).Rows(i)("leaveplannerunkid") & "~" & ds.Tables(0).Rows(i)("leavetypename") & "'"
    '                        End If
    '                        inflag = False
    '                    End If
    '                End If
    '                mFldName = eZeeDate.convertDate(mFldName.ToString).Date.AddDays(1)
    '                mFldName = eZeeDate.convertDate(CDate(mFldName))
    '            Next

    '            If (inflag = False) Then 'flag = False And
    '                msql += " where employeeunkid=" & ds.Tables(0).Rows(i)("EmpId") & "  "
    '                Dim ret As Int16 = clsDataOpr.ExecNonQuery(msql)
    '                flag = True
    '            End If
    '            msql = ""
    '        Next
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub


    Public Sub generateCalendar(ByVal tblnm As String, ByVal stdt As DateTime, ByVal enddt As DateTime)
        Try
            Dim flag As Boolean = True
            Dim inflag As Boolean


            'Pinkal (02-May-2017) -- Start
            'Enhancement - Working on Leave Enhancement for HJF.

            CreateCalendarTable(tblnm, stdt, enddt, 112)

            'msql = "   select p.leaveplannerunkid, l.leavename as leavetypename, p.employeeunkid  as EmpId, convert(varchar(8),p.startdate,112) as StartDT,  datediff(day,p.startdate,p.stopdate) as applieddays  , l.leavetypeunkid   , l.Color as color ,p.leaveplannerunkid as PlannerId  "
            'msql += "  from lvleaveplanner p" & vbCrLf
            'msql += "  left outer Join lvleavetype_master l on l.leavetypeunkid = p.leavetypeunkid" & vbCrLf
            'msql += "  where 1=1 AND p.isvoid = 0 " & vbCrLf
            'If (Session("LoginBy") = Global.User.en_loginby.User) Then
            '    msql += " and p.userunkid IN(-1," & Session("UserId") & ")" & vbCrLf
            'Else
            '    msql += " and p.employeeunkid =" & Session("Employeeunkid") & "" & vbCrLf
            'End If
            'msql += "  order by PlannerId,EmpId,StartDT"

            msql = "   SELECT lvleaveplanner.leaveplannerunkid, lvleavetype_master.leavename as leavetypename, lvleaveplanner.employeeunkid  as EmpId, CONVERT(NVARCHAR(8),lvleaveplanner.startdate,112) as StartDT " & _
                       ",  datediff(day,lvleaveplanner.startdate,lvleaveplanner.stopdate) as applieddays  , lvleavetype_master.leavetypeunkid   , lvleavetype_master.Color ,lvleaveplanner.leaveplannerunkid as PlannerId  " & _
                       "  FROM lvleaveplanner " & _
                       "  LEFT JOIN lvleavetype_master  on lvleavetype_master.leavetypeunkid = lvleaveplanner.leavetypeunkid " & _
                       "  WHERE  lvleaveplanner.isvoid = 0 "

            If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                msql &= " and lvleaveplanner.employeeunkid =" & Session("Employeeunkid") & " "
            End If

            If RbtlistView.SelectedValue = 0 Then 'WEEKLY VIEW
                msql &= " AND ((CONVERT(Char(8),lvleaveplanner.startdate,112)  BETWEEN '" & eZeeDate.convertDate(Calendar1.SelectedDate.Date) & "' AND '" & eZeeDate.convertDate(Calendar1.SelectedDate.AddDays(6)) & "' " & _
                             " OR CONVERT(Char(8),lvleaveplanner.stopdate,112) BETWEEN '" & eZeeDate.convertDate(Calendar1.SelectedDate.Date) & "' AND '" & eZeeDate.convertDate(Calendar1.SelectedDate.AddDays(6)) & " ')  " & _
                             " OR ('" & eZeeDate.convertDate(Calendar1.SelectedDate.Date) & "' BETWEEN lvleaveplanner.startdate AND lvleaveplanner.stopdate  AND '" & eZeeDate.convertDate(Calendar1.SelectedDate.AddDays(0)) & "' BETWEEN lvleaveplanner.startdate AND lvleaveplanner.stopdate)) "

            ElseIf RbtlistView.SelectedValue = 1 Then 'MONTHLY VIEW

                msql &= " AND ((CONVERT(Char(8),lvleaveplanner.startdate,112)  BETWEEN '" & eZeeDate.convertDate(Calendar1.SelectedDate.Date) & "' AND '" & eZeeDate.convertDate(Calendar1.SelectedDate.AddMonths(1)) & "' " & _
                             " OR CONVERT(Char(8),lvleaveplanner.stopdate,112) BETWEEN '" & eZeeDate.convertDate(Calendar1.SelectedDate.Date) & "' AND '" & eZeeDate.convertDate(Calendar1.SelectedDate.AddMonths(1)) & " ')  " & _
                             " OR ('" & eZeeDate.convertDate(Calendar1.SelectedDate.Date) & "' BETWEEN lvleaveplanner.startdate AND lvleaveplanner.stopdate  AND '" & eZeeDate.convertDate(Calendar1.SelectedDate.AddMonths(0)) & "' BETWEEN lvleaveplanner.startdate AND lvleaveplanner.stopdate)) "

            End If

            msql &= "  ORDER BY lvleaveplanner.leaveplannerunkid,lvleaveplanner.employeeunkid, CONVERT(NVARCHAR(8),lvleaveplanner.startdate,112)"

            'Pinkal (02-May-2017) -- End

            ds = clsDataOpr.WExecQuery(msql, "lvleaveplanner")
            Dim mFldName As String

            For i As Int16 = 0 To ds.Tables(0).Rows.Count - 1
                msql = ""
                msql = "update  " & tblnm & "   set "
                mFldName = ds.Tables(0).Rows(i)("StartDT")
                inflag = True
                For j = 0 To ds.Tables(0).Rows(i)("applieddays")
                    If (eZeeDate.convertDate(mFldName.ToString).Date >= CDate(stdt)) Then ' for -> 
                        If (eZeeDate.convertDate(mFldName.ToString).Date <= CDate(enddt)) Then ' calender table ends here, can not update Columns which does not exist.
                            If inflag = True Then
                                msql += "[" & mFldName & "]  = '" & ds.Tables(0).Rows(i)("color") & "~" & ds.Tables(0).Rows(i)("leaveplannerunkid") & "~" & ds.Tables(0).Rows(i)("leavetypename") & "'"
                            Else
                                msql += ",[" & mFldName & "]  = '" & ds.Tables(0).Rows(i)("color") & "~" & ds.Tables(0).Rows(i)("leaveplannerunkid") & "~" & ds.Tables(0).Rows(i)("leavetypename") & "'"
                            End If
                            inflag = False
                        End If
                    End If
                    mFldName = eZeeDate.convertDate(mFldName.ToString).Date.AddDays(1)
                    mFldName = eZeeDate.convertDate(CDate(mFldName))
                Next

                If (inflag = False) Then 'flag = False And
                    msql += " where employeeunkid=" & ds.Tables(0).Rows(i)("EmpId") & "  "
                    Dim ret As Int16 = clsDataOpr.ExecNonQuery(msql)
                    flag = True
                End If
                msql = ""
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    Public Sub BindDepartment(Optional ByVal xDepartmentID As Integer = 0)
        Try
            Dim objDept As New Aruti.Data.clsDepartment
            ds = objDept.getComboList("Department", True)
            Dim dtTable As DataTable = Nothing
            If xDepartmentID >= 0 Then
                dtTable = New DataView(ds.Tables(0), "departmentunkid = " & xDepartmentID, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(ds.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If
            drpdepartment.DataSource = dtTable
            drpdepartment.DataValueField = "departmentunkid"
            drpdepartment.DataTextField = "name"
            drpdepartment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub Bindjob(Optional ByVal xJobID As Integer = 0)
        Try
            Dim objJob As New Aruti.Data.clsJobs
            ds = objJob.getComboList("Job", True)

            Dim dtTable As DataTable = Nothing
            If xJobID >= 0 Then
                dtTable = New DataView(ds.Tables(0), "jobunkid = " & xJobID, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(ds.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If

            drpjob.DataSource = dtTable
            drpjob.DataTextField = "name"
            drpjob.DataValueField = "jobunkid"
            drpjob.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub bindSection(Optional ByVal xSectionID As Integer = 0)
        Try
            Dim objSection As New Aruti.Data.clsSections
            ds = objSection.getComboList("Section", True)
            Dim dtTable As DataTable = Nothing
            If xSectionID >= 0 Then
                dtTable = New DataView(ds.Tables(0), "sectionunkid = " & xSectionID, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtTable = New DataView(ds.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
            End If
            drpsection.DataSource = dtTable
            drpsection.DataTextField = "name"
            drpsection.DataValueField = "sectionunkid"
            drpsection.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (02-May-2017) -- End

    Public Sub BindGrid()
        Try


            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End


            msql = "select * from  Cal_" & Me.ViewState("Empunkid") & " "
            ds = clsDataOpr.WExecQuery(msql, "calendartable")

            For i As Integer = 4 To ds.Tables(0).Columns.Count - 1
                If ds.Tables(0).Columns(i).ColumnName.ToUpper = "DISPLAYNAME" Then
                    ds.Tables(0).Columns(i).ColumnName = "EMPLOYEE"
                ElseIf ds.Tables(0).Columns(i).ColumnName.ToUpper = "ECODE" Then
                    ds.Tables(0).Columns(i).ColumnName = "CODE"
                ElseIf i > 5 Then
                    ds.Tables(0).Columns(i).ColumnName = eZeeDate.convertDate(ds.Tables(0).Columns(i).ColumnName)
                End If
            Next
            XGridView.DataSource = ds.Tables(0)
            XGridView.DataBind()

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." And XGridView.PageIndex > 0 Then
                XGridView.PageIndex = XGridView.PageIndex - 1
                XGridView.DataBind()
            Else
                Throw ex
                DisplayMessage.DisplayError(ex, Me)
            End If
        Finally
            'Pinkal (02-May-2017) -- Start
            'Enhancement - Working on Leave Enhancement for HJF.
            msql = "IF EXISTS ( SELECT  name FROM sys.tables WHERE name = '" & "Cal_" & Me.ViewState("Empunkid") & "" & "' ) BEGIN DROP TABLE " & "Cal_" & Me.ViewState("Empunkid") & "" & " END "
            clsDataOpr.WExecQuery(msql, "Table")
            'Pinkal (02-May-2017) -- End
        End Try
    End Sub


    'Pinkal (02-May-2017) -- Start
    'Enhancement - Working on Leave Enhancement for HJF.
    'Public Sub CreateCalendarTable(ByVal tblnm As String, ByVal stdt As DateTime, ByVal enddt As DateTime, ByVal depid As Integer, ByVal sectid As Integer, ByVal jobid As Integer, ByVal dtfmtno As Integer)
    '    Try
    '        If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '            msql = "exec Createcalendar  '" & tblnm & "' , '" & Format(CDate(stdt), "yyyyMMdd") & "','" & Format(CDate(enddt), "yyyyMMdd") & "'," & depid & "," & sectid & "," & jobid & "," & Session("UserId") & "," & -1 & ", " & dtfmtno 
    '        Else
    '            msql = "exec Createcalendar  '" & tblnm & "' , '" & Format(CDate(stdt), "yyyyMMdd") & "','" & Format(CDate(enddt), "yyyyMMdd") & "'," & depid & "," & sectid & "," & jobid & "," & Session("UserId") & "," & Session("Employeeunkid") & ", " & dtfmtno 
    '        End If

    '        clsDataOpr.WExecQuery(msql, "Table")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Public Sub CreateCalendarTable(ByVal tblnm As String, ByVal stdt As DateTime, ByVal enddt As DateTime, ByVal dtfmtno As Integer)
        Try
            Dim mstrDeptIDs As String = ""
            Dim mstrClassGrpIDs As String = ""
            Dim mstrClassIDs As String = ""
            Dim mstrJobIDs As String = ""

            Dim mstrUserID As String = ""

            If (Session("LoginBy") = Global.User.en_loginby.User) Then

                'Pinkal (02-May-2017) -- Start
                'Enhancement - Working on Leave Enhancement for HJF.

                Dim objUser As New clsUserAddEdit
                Dim arAllocation() As String = Session("UserAccessModeSetting").ToString().Split(",")

                If arAllocation.Length > 0 Then

                    For i As Integer = 0 To arAllocation.Length - 1

                        If CInt(arAllocation(i)) = enAllocation.DEPARTMENT Then
                            mstrDeptIDs = objUser.GetUserAccessIDsAllocationWise(CInt(Session("UserId")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(arAllocation(i)))
                        End If

                        If CInt(arAllocation(i)) = enAllocation.CLASS_GROUP Then
                            mstrClassGrpIDs = objUser.GetUserAccessIDsAllocationWise(CInt(Session("UserId")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(arAllocation(i)))
                        End If

                        If CInt(arAllocation(i)) = enAllocation.CLASSES Then
                            mstrClassIDs = objUser.GetUserAccessIDsAllocationWise(CInt(Session("UserId")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(arAllocation(i)))
                        End If

                        If CInt(arAllocation(i)) = enAllocation.JOBS Then
                            mstrJobIDs = objUser.GetUserAccessIDsAllocationWise(CInt(Session("UserId")), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CInt(arAllocation(i)))
                        End If

                    Next

                End If

                objUser = Nothing

                mstrUserID = Session("UserId").ToString()

                'Pinkal (02-May-2017) -- End

                msql = "exec Createcalendar  '" & tblnm & "' , '" & Format(CDate(stdt), "yyyyMMdd") & "','" & Format(CDate(enddt), "yyyyMMdd") & "'," & CInt(drpdepartment.SelectedValue) & "," & CInt(drpsection.SelectedValue) & "," & CInt(drpjob.SelectedValue) & ",'" & mstrDeptIDs & "','" & mstrJobIDs & "','" & mstrClassGrpIDs & "','" & mstrClassIDs & "','" & mstrUserID & "'," & -1 & ", " & dtfmtno & ",'" & Session("EmployeeAsOnDate").ToString() & "'"
            Else
                msql = "exec Createcalendar  '" & tblnm & "' , '" & Format(CDate(stdt), "yyyyMMdd") & "','" & Format(CDate(enddt), "yyyyMMdd") & "'," & CInt(drpdepartment.SelectedValue) & "," & CInt(drpsection.SelectedValue) & "," & CInt(drpjob.SelectedValue) & ", '" & mstrDeptIDs & "','" & mstrJobIDs & "','" & mstrClassGrpIDs & "','" & mstrClassIDs & "','" & mstrUserID & "'," & Session("Employeeunkid") & ", " & dtfmtno & ",'" & Session("EmployeeAsOnDate").ToString() & "'"
            End If

            clsDataOpr.WExecQuery(msql, "Table")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (02-May-2017) -- End

    Public Shared Function b64encode(ByVal StrEncode As String) As String
        Try
            Dim encodedString As String
            encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
            Return (encodedString)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function GetLeavenameFromColour(ByVal col As String) As String
        Try
            msql = "select leavename  from lvleavetype_master  where color = '" & col & " '"
            ds = clsDataOpr.WExecQuery(msql, "LeaveName")
            If Not ds Is Nothing Then
                Return ds.Tables(0).Rows(0)("leavename").ToString
            Else
                Return Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return Nothing
        End Try
    End Function

#End Region

#Region " Page Events "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            clsDataOpr = New clsDataOperation()
            Me.Title = ""
            Me.lblPageHeader.Text = ""



            If (Page.IsPostBack = False) Then
                SetLanguage()
                'Pinkal (02-May-2017) -- Start
                'Enhancement - Working on Leave Enhancement for HJF.
                'BindDepartment()
                'Bindjob()
                'bindSection()
                'Pinkal (02-May-2017) -- End

                'eZeeCommonLib.eZeeDatabase.change_database(Session("mdbname"))
                RbtlistView.SelectedValue = 0
                clsuser = CType(Session("clsuser"), User)
                Aruti.Data.User._Object._Userunkid = Session("UserId")
                Calendar1.TodaysDate = CDate(Session("fin_startdate"))
                If (Session("LoginBy") = Global.User.en_loginby.User) Then
                    Me.ViewState("Empunkid") = Session("UserId")
                Else
                    Me.ViewState("Empunkid") = Session("Employeeunkid")
                End If


                'Pinkal (02-May-2017) -- Start
                'Enhancement - Working on Leave Enhancement for HJF.
                Dim mintDeparmentID As Integer = -1
                Dim mintJobID As Integer = -1
                Dim mintSectionID As Integer = -1
                If (Session("LoginBy") = Global.User.en_loginby.Employee) Then
                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString())) = CInt(Session("Employeeunkid"))
                    mintDeparmentID = objEmployee._Departmentunkid
                    mintJobID = objEmployee._Jobunkid
                    mintSectionID = objEmployee._Sectionunkid
                End If

                BindDepartment(mintDeparmentID)
                Bindjob(mintJobID)
                bindSection(mintSectionID)

                'Pinkal (02-May-2017) -- End



                Calendar1.TodaysDate = Aruti.Data.ConfigParameter._Object._CurrentDateAndTime.Date
                Calendar1.SelectedDate = Calendar1.TodaysDate.Date
                If (Session("RedirectFromPlanner") = True) Then
                    Fetchlaststatus()
                    'Pinkal (02-May-2017) -- Start
                    'Enhancement - Working on Leave Enhancement for HJF.
                    'BindGrid()
                    'Pinkal (02-May-2017) -- End
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState.Add("Empunkid", Me.ViewState("Empunkid"))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Buttons Events "

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try

            If Session("LoginBy") = Global.User.en_loginby.User Then
                If CBool(Session("ViewPlannedLeaveViewer")) = False Then
                    Exit Sub
                End If
            End If

            Dim TblName As String
            TblName = "Cal_" & Me.ViewState("Empunkid") & ""
            Dim mDateFmt As String = CultureInfo.CurrentCulture.DateTimeFormat.ToString
            Dim mDateFmtNo As Integer = 112
            If Not (RbtlistView.SelectedItem) Is Nothing Then
                If (RbtlistView.SelectedValue > 0) Then
                    Dim Firstdate As New DateTime(Calendar1.SelectedDate.Year, Calendar1.SelectedDate.Month, 1)
                    Dim LastDate As New DateTime(Calendar1.SelectedDate.Year, Calendar1.SelectedDate.Month, Date.DaysInMonth(Calendar1.SelectedDate.Year, Calendar1.SelectedDate.Month))
                    Calendar1.SelectedDate = Firstdate
                End If
            End If

            'Pinkal (02-May-2017) -- Start
            'Enhancement - Working on Leave Enhancement for HJF.
            'CreateCalendarTable(TblName, Calendar1.SelectedDate, IIf(RbtlistView.SelectedValue = 0, Calendar1.SelectedDate.AddDays(6), Calendar1.SelectedDate.AddMonths(1).AddDays(-1)), drpdepartment.SelectedValue, drpsection.SelectedValue, drpjob.SelectedValue, mDateFmtNo)
            'generateCalendar(TblName, Calendar1.SelectedDate, IIf(RbtlistView.SelectedValue = 0, Calendar1.SelectedDate.AddDays(6), Calendar1.SelectedDate.AddMonths(1).AddDays(-1)), drpdepartment.SelectedValue, drpsection.SelectedValue, drpjob.SelectedValue)

            generateCalendar(TblName, Calendar1.SelectedDate, IIf(RbtlistView.SelectedValue = 0, Calendar1.SelectedDate.AddDays(6), Calendar1.SelectedDate.AddMonths(1).AddDays(-1)))

            'Pinkal (02-May-2017) -- End
            BindGrid()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Calender's Events "

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        Try
            If Session("LoginBy") = Global.User.en_loginby.User Then
                If CBool(Session("ViewPlannedLeaveViewer")) = False Then
                    Exit Sub
                End If
            End If

            Dim TblName As String
            TblName = "Cal_" & Me.ViewState("Empunkid") & ""


            If Not (RbtlistView.SelectedItem) Is Nothing Then
                If (RbtlistView.SelectedValue = 0) Then

                    'Pinkal (02-May-2017) -- Start
                    'Enhancement - Working on Leave Enhancement for HJF.
                    'generateCalendar(TblName, Calendar1.SelectedDate, (Calendar1.SelectedDate).AddDays(6), drpdepartment.SelectedValue, drpsection.SelectedValue, drpjob.SelectedValue)
                    generateCalendar(TblName, Calendar1.SelectedDate, (Calendar1.SelectedDate).AddDays(6))
                    'Pinkal (02-May-2017) -- End
                    BindGrid()
                Else
                    Dim Firstdate As New DateTime(Calendar1.SelectedDate.Year, Calendar1.SelectedDate.Month, 1)
                    Dim LastDate As New DateTime(Calendar1.SelectedDate.Year, Calendar1.SelectedDate.Month, Date.DaysInMonth(Calendar1.SelectedDate.Year, Calendar1.SelectedDate.Month))
                    Calendar1.SelectedDate = Firstdate

                    'Pinkal (02-May-2017) -- Start
                    'Enhancement - Working on Leave Enhancement for HJF.
                    'generateCalendar(TblName, Calendar1.SelectedDate, (Calendar1.SelectedDate).AddMonths(1).AddDays(-1), drpdepartment.SelectedValue, drpsection.SelectedValue, drpjob.SelectedValue)
                    generateCalendar(TblName, Calendar1.SelectedDate, (Calendar1.SelectedDate).AddMonths(1).AddDays(-1))
                    'Pinkal (02-May-2017) -- End
                    BindGrid()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " DataGrid Events "

    Protected Sub XGridView_CellClicked(ByVal sender As Object, ByVal e As CustomControls.GridViewCellClickedEventArgs) Handles XGridView.CellClicked
        Try
            If e.ColumnIndex <= 0 Then Exit Sub
            If (e.Cell.TabIndex.ToString <> "0") Then
                Session("RidirectFromViewer") = True
                SaveFilterStatus()
                Response.Redirect("Leaveplanner.aspx?Planner_Id=" & b64encode(CInt(e.Cell.TabIndex)), False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub XGridView_PageIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles XGridView.PageIndexChanged

    End Sub

    Protected Sub XGridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles XGridView.RowDataBound
        Try
            Dim ar() As String
            Dim hplink As New System.Web.UI.WebControls.Button
            Dim count As Int16 = 0
            Dim planner As String = ""
            If e.Row.RowIndex > -1 Then
                For i = 0 To e.Row.Cells.Count - 1
                    ar = Split(e.Row.Cells(i).Text, "~")
                    If (ar.Length > 1) Then
                        e.Row.Cells(i).ToolTip = ar(2)
                        e.Row.Cells(i).TabIndex = ar(1)
                        e.Row.Cells(i).BackColor = (Color.FromArgb(ar(0)))
                        If (ar(1) <> planner) Then
                            e.Row.Cells(i).Text = ar(2)
                        Else
                            e.Row.Cells(i).Text = ""
                        End If
                        planner = ar(1)
                    End If
                Next
            End If
            e.Row.Cells(0).Visible = False
            e.Row.Cells(1).Visible = False
            e.Row.Cells(2).Visible = False
            e.Row.Cells(3).Visible = False
            e.Row.Cells(4).CssClass = "griviewheader" 'SHANI [01 FEB 2015]-START 'Enhancement - REDESIGN SELF SERVICE.
            e.Row.Cells(5).CssClass = "griviewheader" 'SHANI [01 FEB 2015]-START 'Enhancement - REDESIGN SELF SERVICE.
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " HypeLnk Events "

    Protected Sub hplink_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles hplink.Command
        Try
            Response.Redirect("Leaveplanner.aspx?Planner_Id=" & b64encode(Val(e.CommandArgument)), False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub hplink_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hplink.Click

    End Sub

#End Region


    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, "Leave Planner Viewer")
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, "Leave Planner Viewer")
            Me.lbljob.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lbljob.ID, Me.lbljob.Text)
            Me.lblSection.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSection.ID, Me.lblSection.Text)
            Me.lbldepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lbldepartment.ID, Me.lbldepartment.Text)
            Me.RbtlistView.Items(0).Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "btnWeek", Me.RbtlistView.Items(0).Text)
            Me.RbtlistView.Items(1).Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "btnMonthly", Me.RbtlistView.Items(1).Text)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

End Class

'Dim Flag As String
'Private _prUserunkid As Int16
'Public Property UserId() As Int16
'    Get
'        Return _prUserunkid
'    End Get
'    Set(ByVal value As Int16)
'        _prUserunkid = value
'    End Set
'End Property
'CreateCalendarTable(tblnm, stdt, enddt, depid, secid, jobid)
'msql = "   select p.leaveplannerunkid, p.employeeunkid  as EmpId, convert(varchar(8),p.startdate,112) as StartDT,  datediff(day,p.startdate,p.stopdate) as applieddays  , l.leavetypeunkid   , l.Color as color ,p.leaveplannerunkid as PlannerId  "
'msql += "  from lvleaveplanner p" & vbCrLf
'msql += "  left outer Join lvleavetype_master l on l.leavetypeunkid = p.leavetypeunkid" & vbCrLf
'msql += "  where 1=1" & vbCrLf
'If (Session("LoginBy") = Global.User.en_loginby.User) Then
'    msql += " and p.userunkid =" & Session("UserId") & "" & vbCrLf
'Else
'    msql += " and p.employeeunkid =" & Session("Employeeunkid") & "" & vbCrLf
'End If

'msql += "  order by PlannerId,EmpId,StartDT"
'For i As Int16 = 0 To ds.Tables(0).Rows.Count - 1
'    msql = ""
'    msql = "update  " & tblnm & "   set "
'    'mFldName = CDate(ds.Tables(0).Rows(i)("StartDT"))
'    mFldName = ds.Tables(0).Rows(i)("StartDT")
'    inflag = True
'    For j = 0 To ds.Tables(0).Rows(i)("applieddays")
'        If (CDate(mFldName) >= stdt) Then ' for -> 
'            If (mFldName <= CDate(enddt)) Then ' calender table ends here, can not update Columns which does not exist.
'                If inflag = True Then
'                    msql += "[" & mFldName & "]  = " & ds.Tables(0).Rows(i)("color") & ""
'                Else
'                    msql += ",[" & mFldName & "]  = " & ds.Tables(0).Rows(i)("color") & ""
'                End If
'                inflag = False
'            End If
'        End If
'        'Dim temp = FormatDateTime(CDate(mFldName), DateFormat.ShortDate)
'        'Dim temp1 = CDate(temp).AddDays(1)
'        mFldName = CDate(mFldName).AddDays(1)
'    Next
'Dim temp = FormatDateTime(CDate(mFldName), DateFormat.ShortDate)
'Dim temp1 = CDate(temp).AddDays(1)
'Public Sub BindGrid()
'    Try
'        msql = "select * from  Cal_" & Me.ViewState("Empunkid") & " "
'        ds = clsDataOpr.WExecQuery(msql, "calendartable")
'        dgView.DataSource = ds.Tables(0)
'        dgView.DataBind()

'    Catch ex As Exception

'        If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." And dgView.CurrentPageIndex > 0 Then
'            dgView.CurrentPageIndex = dgView.CurrentPageIndex - 1
'            dgView.DataBind()
'        Else
'            Throw ex
'            DisplayMessage.DisplayError(ex, Me)
'        End If
'    End Try
'End Sub
'Protected Sub btnView_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnView.Click
'    generateCalendar(, )
'End Sub
'Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
'    'Dim TblName As String
'    'TblName = "Cal_" & Me.ViewState("Empunkid") & ""
'    'Dim objdataOperation = New clsDataOperation
'    'msql = "drop table " & TblName & ""
'    'objdataOperation.ExecNonQuery(msql)
'End Sub
'Protected Sub dgView_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgView.ItemCreated
'    For Each cell As TableCell In e.Item.Cells
'        If (cell.Text = "-8372160") Then
'            cell.Text = "Maternity Leave"
'            cell.ColumnSpan = e.Item.DataSetIndex
'            cell.BackColor = Drawing.Color.SkyBlue
'        ElseIf (cell.Text = "-16744320") Then
'            cell.Text = "Annual Leave"
'            cell.BackColor = Drawing.Color.Lime
'        ElseIf (cell.Text = "-8355648") Then
'            cell.Text = "Personal Leave"
'            cell.BackColor = Drawing.Color.Wheat
'        End If
'    Next
'    e.Item.Cells(0).Visible = False
'    e.Item.Cells(1).Visible = False
'End Sub
'If (cell.Text = "-8372160") Then
'    If mat = True Then
'        cell.Text = "Maternity Leave"
'        mat = False
'    Else
'        cell.Text = ""
'    End If
'    cell.BackColor = (Color.FromArgb(-8372160))
'    '  cell.BackColor = Drawing.Color.SkyBlue
'ElseIf (cell.Text = "-16744320") Then
'    If ann = True Then
'        cell.Text = "Annual Leave"
'        ann = False
'    Else
'        cell.Text = ""
'    End If
'    cell.BackColor = (Color.FromArgb(-16744320))
'    'cell.BackColor = Drawing.Color.Lime

'ElseIf (cell.Text = "-8355648") Then
'    If per = True Then
'        cell.Text = "Personal Leave"
'        per = False
'    Else
'        cell.Text = ""
'    End If
'    cell.BackColor = (Color.FromArgb(-8355648))
'    cell.BackColor = Drawing.Color.Wheat
'End If
'' End If
'Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.ItemCommand
'    Try
'        If (e.CommandName = "View") Then
'            Session("Redirectedfromviewer") = True
'            Response.Redirect("Leaveplanner.aspx?Planer_Id=" & b64encode(e.Item.Cells(2).Text) & "")
'        End If
'    Catch ex As Exception
'        DisplayMessage.DisplayError(ex, Me)
'    End Try
'End Sub

'Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgView.ItemDataBound
'    Try
'        Dim mat As Boolean = True
'        Dim ann As Boolean = True
'        Dim per As Boolean = True
'        For Each cell As TableCell In e.Item.Cells

'            If (e.Item.ItemIndex = -1) Then
'                e.Item.Cells(4).CssClass = "GridHeader"
'                cell.CssClass = "GridHeader"
'            End If

'            Dim ar() As String
'            ar = Split(cell.Text, "-")

'            If (ar.Length > 1) Then

'                If (cell.Text <> "") Then

'                    cell.BackColor = (Color.FromArgb(cell.Text))
'                    If mat = True Then
'                        cell.Text = GetLeavenameFromColour(cell.Text)
'                        mat = False
'                    Else
'                        cell.Text = ""
'                    End If
'                End If
'            End If

'        Next

'        e.Item.Cells(0).Visible = True
'        e.Item.Cells(1).Visible = False
'        e.Item.Cells(2).Visible = False
'        e.Item.Cells(3).Visible = False
'        e.Item.Cells(4).CssClass = "Emp_Grid"
'    Catch ex As Exception
'        DisplayMessage.DisplayError(ex, Me)
'    End Try
'End Sub

'Protected Sub dgView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgView.PageIndexChanged
'    Try
'        dgView.CurrentPageIndex = e.NewPageIndex
'        BindGrid()
'    Catch ex As Exception
'        DisplayMessage.DisplayError(ex, Me)
'    End Try
'End Sub