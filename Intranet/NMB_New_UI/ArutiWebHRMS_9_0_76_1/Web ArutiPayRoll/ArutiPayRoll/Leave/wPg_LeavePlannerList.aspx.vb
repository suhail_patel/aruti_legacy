﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports ArutiReports
Imports System.Data
Imports System.IO
#End Region

Partial Class Leave_wPg_LeavePlannerList
    Inherits Basepage

#Region "Private Variables"
    Private Shared ReadOnly mstrModuleName As String = "frmleaveplannerList"
    Dim DisplayMessage As New CommonCodes

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objLeavePlanner As clsleaveplanner
    'Pinkal (11-Sep-2020) -- End

    Private mstrAdvanceFilter As String = String.Empty

#End Region

#Region "Page's Events"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            If IsPostBack = False Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (11-Sep-2020) -- End

                dtpStartDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpEndDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date

                dgvLeavePlannerList.DataSource = New List(Of String)
                dgvLeavePlannerList.DataBind()

                Call FillCombo()

                dgvLeavePlannerList.Columns(2).Visible = False

                If CInt(Session("Employeeunkid")) > 0 Then
                    lnkAllocation.Enabled = False
                    Call FillList()
                    ''Varsha Rana (17-Oct-2017) -- Start
                    ''Enhancement - Give user privileges.
                ElseIf CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                    btnNew.Visible = CBool(Session("AddPlanLeave"))
                    ''Varsha Rana (17-Oct-2017) -- End
                End If
            Else
                mstrAdvanceFilter = Me.ViewState("AdvanceFilter").ToString
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("AdvanceFilter") = mstrAdvanceFilter
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            Dim dsList As DataSet = Nothing
            Dim objEmployee As New clsEmployee_Master
            Dim objLeaveType As New clsleavetype_master

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsList = objEmployee.GetEmployeeList(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                     CStr(Session("UserAccessModeSetting")), _
                                                     True, CBool(Session("IsIncludeInactiveEmp")), _
                                                     "Employee", True)
                With cboEmployee
                    .DataValueField = "employeeunkid"
                    .DataTextField = "EmpCodeName"
                    .DataSource = dsList.Tables("Employee")
                    .DataBind()
                    .SelectedValue = "0"
                End With

            Else
                Dim objglobalassess = New GlobalAccess
                objglobalassess = CType(Session("objGlobalAccess"), GlobalAccess)
                cboEmployee.DataSource = objglobalassess.ListOfEmployee
                cboEmployee.DataTextField = "loginname"
                cboEmployee.DataValueField = "employeeunkid"
                cboEmployee.DataBind()
            End If
            dsList = Nothing


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                dsList = objLeaveType.getListForCombo("LeaveType", True, , Session("Database_Name").ToString, "", True)
            Else
                dsList = objLeaveType.getListForCombo("LeaveType", True, , Session("Database_Name").ToString, "", False)
            End If
            'Pinkal (25-May-2019) -- End

            With cboLeaveType
                .DataValueField = "leavetypeunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("LeaveType")
                .DataBind()
                .SelectedValue = "0"
            End With


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objEmployee = Nothing
            objLeaveType = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo: - " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Try
            Dim dsList As DataSet = Nothing
            Dim strSearching As String = ""

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objLeavePlanner = New clsleaveplanner
            Dim objLeavePlanner As New clsleaveplanner
            'Pinkal (11-Sep-2020) -- End


            Dim blnFlag As Boolean = False
            Dim blnApplyAccessFilter As Boolean

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                blnFlag = CBool(Session("ViewLeavePlannerList").ToString)
                blnApplyAccessFilter = True
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnFlag = True
                blnApplyAccessFilter = False
            End If

            If blnFlag = True Then

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    strSearching = "AND hremployee_master.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboLeaveType.SelectedValue) > 0 Then
                    strSearching &= "AND lvleaveplanner.leavetypeunkid = " & CInt(cboLeaveType.SelectedValue) & " "
                End If

                If dtpStartDate.IsNull = False Then
                    strSearching &= "AND startdate >= '" & eZeeDate.convertDate(dtpStartDate.GetDate.Date) & "'" & " "
                End If

                If dtpEndDate.IsNull = False Then
                    strSearching &= "AND stopdate <= '" & eZeeDate.convertDate(dtpEndDate.GetDate.Date) & "'" & " "
                End If

                'If CInt(Session("Employeeunkid")) > 0 Then
                '    strSearching &= "AND hremployee_master.employeeunkid = " & CInt(Session("Employeeunkid")) & " "
                'End If

                If mstrAdvanceFilter.Trim.Length > 0 Then
                    strSearching &= "AND " & mstrAdvanceFilter.Trim
                End If

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                End If

                dsList = objLeavePlanner.GetList("LeavePlanner", Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), Session("UserAccessModeSetting").ToString, True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), blnApplyAccessFilter, True, strSearching)

                If dsList IsNot Nothing Then
                    Dim dtTable As DataTable = dsList.Tables(0).Clone
                    Dim dtRow As DataRow = Nothing
                    Dim strform As String = ""
                    dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))

                    For Each dRow As DataRow In dsList.Tables(0).Rows
                        If strform.Trim <> dRow("employeeunkid").ToString.Trim Then
                            dtRow = dtTable.NewRow
                            strform = dRow("employeeunkid").ToString.Trim
                            dtRow("employeeunkid") = strform
                            dtRow("leavename") = dRow("EName").ToString()
                            dtRow("IsGrp") = True
                            dtTable.Rows.Add(dtRow)
                        End If
                        dtRow = dtTable.NewRow
                        For Each dcol As DataColumn In dsList.Tables(0).Columns
                            dtRow(dcol.ColumnName) = dRow(dcol.ColumnName)
                        Next
                        dtRow("EName") = ""
                        dtRow("IsGrp") = False
                        dtTable.Rows.Add(dtRow)
                    Next
                    dtTable.AcceptChanges()

                    dgvLeavePlannerList.AutoGenerateColumns = False
                    dgvLeavePlannerList.DataSource = dtTable
                    dgvLeavePlannerList.DataBind()


                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtTable IsNot Nothing Then dtTable.Clear()
                    dtTable = Nothing
                    'Pinkal (11-Sep-2020) -- End

                End If

            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objLeavePlanner = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillList:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Session("LeavePlannerUnkid") = Nothing
            Response.Redirect(Session("rootpath").ToString & "Leave/wPg_LeavePlanner_AddEdit.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnNew_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnSearch_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try

            'Pinkal (30-Jun-2016) -- Start
            'Enhancement - Working on SL changes on Leave Module.
            If CInt(Session("employeeunkid")) <= 0 Then cboEmployee.SelectedValue = CStr(0)
            'Pinkal (30-Jun-2016) -- End

            cboLeaveType.SelectedValue = CStr(0)

            If CDate(Session("fin_enddate").ToString).Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                dtpStartDate.SetDate = CDate(Session("fin_enddate").ToString).Date
                dtpEndDate.SetDate = CDate(Session("fin_enddate").ToString).Date
            ElseIf CDate(Session("fin_enddate").ToString).Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                dtpStartDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtpEndDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If

            mstrAdvanceFilter = ""

            dgvLeavePlannerList.DataSource = New List(Of String)
            dgvLeavePlannerList.DataBind()
            'Call FillList()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~/Userhome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try
            popupAdvanceFilter._Hr_EmployeeTable_Alias = "hremployee_master"
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popupAdvanceFilter_buttonApply_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popDeleteReason_buttonDelReasonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popDeleteReason.buttonDelReasonNo_Click
        Try
            Session("LeavePlannerUnkid") = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popDeleteReason_buttonDelReasonNo_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub popDeleteReason_buttonDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popDeleteReason.buttonDelReasonYes_Click
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objLeavePlanner = New clsleaveplanner
            Dim objLeavePlanner As New clsleaveplanner
            'Pinkal (11-Sep-2020) -- End

            objLeavePlanner._Voidreason = popDeleteReason.Reason
            objLeavePlanner._Isvoid = True

            If CInt(Session("Employeeunkid")) > 0 Then
                objLeavePlanner._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))
            Else
                objLeavePlanner._Voiduserunkid = CInt(Session("UserId"))
            End If

            objLeavePlanner._WebFormName = "frmleaveplannerList"
            objLeavePlanner._WebClientIP = CStr(Session("IP_ADD"))
            objLeavePlanner._WebHostName = CStr(Session("HOST_NAME"))
            StrModuleName2 = "mnuLeaveInformation"


            If objLeavePlanner.Delete(CInt(Session("LeavePlannerUnkid"))) = False Then
                DisplayMessage.DisplayMessage(objLeavePlanner._Message, Me)
            Else
                Call FillList()
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeavePlanner = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("popDeleteReason_buttonDelReasonYes_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " LinkButton's Events "

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try

            popupAdvanceFilter.Show()

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAllocation_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " DataGrid's Events "

    Protected Sub dgvLeavePlannerList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvLeavePlannerList.ItemCommand
        Try
            If e.CommandName.ToUpper = "EDIT" Then


                'Pinkal (30-Jun-2016) -- Start
                'Enhancement - Working on SL changes on Leave Module.
                'If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then 'FOR MSS
                '    Session("LeavePlannerUnkid") = CInt(e.Item.Cells(6).Text)
                'Else
                '    Session("LeavePlannerUnkid") = Nothing 'FOR ESS
                'End If
                Session("LeavePlannerUnkid") = CInt(e.Item.Cells(7).Text)
                'Pinkal (30-Jun-2016) -- End

                Response.Redirect(Session("rootpath").ToString & "Leave/wPg_LeavePlanner_AddEdit.aspx", False)

            ElseIf e.CommandName.ToUpper = "DELETE" Then

                'Pinkal (30-Jun-2016) -- Start
                'Enhancement - Working on SL changes on Leave Module.
                'Session("LeavePlannerUnkid") = CInt(e.Item.Cells(6).Text)
                Session("LeavePlannerUnkid") = CInt(e.Item.Cells(7).Text)
                'Pinkal (30-Jun-2016) -- End


                'Language.setLanguage(mstrModuleName)
                popDeleteReason.Title = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Are you sure you want to Delete this Leave Planner record ?")
                popDeleteReason.Reason = ""
                popDeleteReason.Show()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvLeavePlannerList_ItemCommand:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvLeavePlannerList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvLeavePlannerList.ItemDataBound
        Try
            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If e.Item.ItemType = ListItemType.AlternatingItem OrElse e.Item.ItemType = ListItemType.Item Then


                'Pinkal (30-Jun-2016) -- Start
                'Enhancement - Working on SL changes on Leave Module.
                'If CBool(e.Item.Cells(7).Text) = True Then
                If CBool(e.Item.Cells(8).Text) = True Then
                    'Pinkal (30-Jun-2016) -- End
                    CType(e.Item.Cells(0).FindControl("imgEdit"), LinkButton).Visible = False
                    CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False

                    'If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                    'Else
                    e.Item.Cells(3).ColumnSpan = e.Item.Cells.Count - 1
                    e.Item.Cells(3).Style.Add("text-align", "left")


                    'Gajanan [17-Sep-2020] -- Start
                    'New UI Change
                    'e.Item.Cells(3).CssClass = "GroupHeaderStyleBorderLeft"
                    e.Item.Cells(3).CssClass = "group-header"
                    'Gajanan [17-Sep-2020] -- End

                    e.Item.Cells(0).Visible = False
                    e.Item.Cells(1).Visible = False
                    e.Item.Cells(2).Visible = False
                    'End If
                    For i = 4 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next
                Else

                    If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                        'e.Item.Cells(0).Attributes.Add("EName", e.Item.Cells(2).Text.Trim)
                        'e.Item.Cells(0).ColumnSpan = 2
                        'e.Item.Cells(2).Visible = False
                    Else
                        e.Item.Cells(0).Attributes.Add("EName", e.Item.Cells(2).Text.Trim)
                        e.Item.Cells(2).Text = e.Item.Cells(3).Text.Trim
                        e.Item.Cells(2).ColumnSpan = 1
                        e.Item.Cells(2).Visible = False
                    End If


                    'Pinkal (16-Apr-2016) -- Start
                    'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                    'e.Item.Cells(4).Text = (eZeeDate.convertDate(e.Item.Cells(4).Text).Date.ToString(Session("DateFormat").ToString()))
                    'e.Item.Cells(5).Text = (eZeeDate.convertDate(e.Item.Cells(5).Text).Date.ToString(Session("DateFormat").ToString()))
                    e.Item.Cells(4).Text = eZeeDate.convertDate(e.Item.Cells(4).Text).ToShortDateString()
                    e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).ToShortDateString()
                    'Pinkal (16-Apr-2016) -- End


                End If

            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvLeavePlannerList_ItemDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

End Class
