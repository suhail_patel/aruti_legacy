﻿<%@ Page Title="Leave Planner Add/Edit" Language="VB" MasterPageFile="~/Home1.master"
    AutoEventWireup="false" CodeFile="wPg_LeavePlanner_AddEdit.aspx.vb" Inherits="Leave_wPg_LeavePlanner_AddEdit" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
        prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_beginRequest(beginRequestHandler);
        prm.add_endRequest(endRequestHandler);

        $(window).scroll(function() {
            var cend = $("#endreq").val();
            if (cend == "1") {
                $("#endreq").val("0");
                var nbodyY = $("#bodyy").val();
                $(window).scrollTop(nbodyY);
            }
        });

        var scroll = {
            Y: '#<%= hfScrollPosition.ClientID %>'
        };

        function beginRequestHandler(sender, args) {
            $("#endreq").val("0");
            $("#bodyy").val($(window).scrollTop());
        }

        function endRequestHandler(sender, args) {
            $("#endreq").val("1");

            if (args.get_error() == undefined) {
                $("#scrollable-container").scrollTop($(scroll.Y).val());
            }
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            //called when key is pressed in textbox
            $(".OnlyNumbers").keypress(function(e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
            });
        });


        $("body").on("click", "[id*=ChkAll]", function() {
            var chkHeader = $(this);
            debugger;
            var grid = $(this).closest("table");
            $("[id*=ChkgvSelect]").prop("checked", $(chkHeader).prop("checked"));
        });

        $("body").on("click", "[id*=ChkgvSelect]", function() {
            var grid = $(this).closest("table");
            var chkHeader = $("[id*=ChkAll]", grid);
            debugger;
            if ($("[id*=ChkgvSelect]", grid).length == $("[id*=ChkgvSelect]:checked", grid).length) {
                chkHeader.prop("checked", true);
            }
            else {
                chkHeader.prop("checked", false);
            }
        });


        $.expr[":"].containsNoCase = function(el, i, m) {
            var search = m[3];
            if (!search) return false;
            return eval("/" + search + "/i").test($(el).text());
        };

        function FromSearching() {
            if ($('#txtSearch').val().length > 0) {
                $('#<%= dgvEmployee.ClientID %> tbody tr').hide();
                $('#<%= dgvEmployee.ClientID %> tbody tr:first').show();
                $('#<%= dgvEmployee.ClientID %> tbody tr td:containsNoCase(\'' + $('#txtSearch').val() + '\')').parent().show();
            }
            else if ($('#txtSearch').val().length == 0) {
                resetFromSearchValue();
            }
            if ($('#<%= dgvEmployee.ClientID %> tr:visible').length == 1) {
                $('.norecords').remove();
            }

            if (event.keyCode == 27) {
                resetFromSearchValue();
            }
        }
        function resetFromSearchValue() {
            $('#txtSearch').val('');
            $('#<%= dgvEmployee.ClientID %> tr').show();
            $('.norecords').remove();
            $('#txtSearch').focus();
        }
        
    </script>

    <script>
        function numbersLimit(input) {
            if (input.value < 0) input.value = 0;
            if (input.value > 12) {
                alert("Months cannot be greater than 12");
                input.value = 12;
            }
        }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Leave Planner Add/Edit"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <asp:Panel ID="pnl_Filter" runat="server">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria"></asp:Label>
                                </h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <asp:LinkButton ID="lnkAllocation" runat="server" ToolTip="Allocations">
                                                   <i class="fas fa-sliders-h"></i>
                                        </asp:LinkButton>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:RadioButton ID="radExpYear" runat="server" Text="Year of Experience" AutoPostBack="true"
                                            GroupName="Group" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:RadioButton ID="radAppointedDate" runat="server" Text="Appointment Date" AutoPostBack="true"
                                            GroupName="Group" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:RadioButton ID="radProbationDate" runat="server" Text="Probation Date" AutoPostBack="true"
                                            GroupName="Group" />
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <asp:RadioButton ID="radConfirmationDate" runat="server" Text="Confirmation Date"
                                            AutoPostBack="true" GroupName="Group" />
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnl_FromYear" runat="server">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblFromYear" runat="server" Text="From Year" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtFromYear" runat="server" Style="text-align: right" Text="0" AutoPostBack="true"
                                                        CssClass="OnlyNumbers form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblFromMonth" runat="server" Text="From Month" CssClass="form-label"></asp:Label>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtFromMonth" runat="server" Style="text-align: right" Text="0"
                                                            onchange="numbersLimit(this);" CssClass="OnlyNumbers form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboFromcondition" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel ID="pnl_Date" runat="server">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblFromDate" runat="server" Text="From Date" CssClass="form-label"></asp:Label>
                                            <uc1:DateCtrl ID="dtpFromDate" runat="server" />
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblToDate" runat="server" Text="To Date" CssClass="form-label"></asp:Label>
                                            <uc1:DateCtrl ID="dtpToDate" runat="server" />
                                        </div>
                                    </asp:Panel>
                                </div>
                                <div class="row clearfix">
                                    <asp:Panel ID="pnl_ToYear" runat="server">
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblToYear" runat="server" Text="To Year" CssClass="form-label"></asp:Label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <asp:TextBox ID="txtToYear" runat="server" Style="text-align: right" Text="0" AutoPostBack="true"
                                                        CssClass="OnlyNumbers form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <asp:Label ID="lblToMonth" runat="server" Text="To Month" CssClass="form-label"></asp:Label>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <div class="form-line">
                                                        <asp:TextBox ID="txtToMonth" runat="server" Style="text-align: right" Text="0" onchange="numbersLimit(this);"
                                                            CssClass="OnlyNumbers form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboTocondition" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblGender" runat="server" Text="Gender" CssClass="form-label"></asp:Label>
                                        <div class="form-group">
                                            <asp:DropDownList ID="cboGender" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                   </asp:Panel> 
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <%--<asp:TextBox ID="txtSearch" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>--%>
                                                <input type="text" id="txtSearch" name="txtSearch" placeholder = "Type To Search Text"  maxlength="50"  onkeyup="FromSearching();" class="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 m-t-15">
                                        <asp:Label ID="lblFrom" runat="server" Text="From:"  CssClass="form-label"></asp:Label>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 m-t-15">
                                        <asp:Label ID="lblValue" runat="server" Style="font-weight: bold" Text="#Value"  CssClass="form-label"></asp:Label>
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 m-t-15">
                                        <asp:Label ID="lblEmployeeCount" runat="server" Style="font-weight: bold;" Text="#Count"
                                             CssClass="form-label"></asp:Label>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:DataGrid ID="dgvEmployee" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px" HeaderStyle-CssClass="headerstyle"
                                                        ItemStyle-CssClass="itemstyle" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="ChkAll" runat="server" CssClass="filled-in" Text=" " />  <%--AutoPostBack="true" OnCheckedChanged="ChkAll_CheckedChanged"--%>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="ChkgvSelect" runat="server" CssClass="filled-in" Text=" " /> <%--AutoPostBack="true" OnCheckedChanged="ChkgvSelect_CheckedChanged"--%>
                                                        </ItemTemplate>
                                                        <HeaderStyle CssClass="headerstyle" HorizontalAlign="Center" Width="50px" />
                                                        <ItemStyle CssClass="itemstyle" HorizontalAlign="Center" Width="50px" />
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="employeecode" HeaderText="Code" ReadOnly="true" FooterText="dgColhEmpCode" />
                                                    <asp:BoundColumn DataField="name" HeaderText="Employee" ReadOnly="true" FooterText="dgColhEmployee" />
                                                    <asp:BoundColumn DataField="appointeddate" HeaderText="Appointment Date" ReadOnly="true"
                                                        FooterText="dgcolhAppointedDate" />
                                                    <asp:BoundColumn DataField="confirmation_date" HeaderText="Confirmation Date" ReadOnly="true"
                                                        FooterText="dgcolhConfirmationDate" />
                                                    <asp:BoundColumn DataField="probation_from_date" HeaderText="Probation From Date"
                                                        ReadOnly="true" FooterText="dgcolhProbationFromDate" />
                                                    <asp:BoundColumn DataField="probation_to_date" HeaderText="Probation To Date" ReadOnly="true"
                                                        FooterText="dgcolhProbationToDate" />
                                                    <asp:BoundColumn DataField="job_name" HeaderText="Job Title" ReadOnly="true" FooterText="dgcolhjob">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="ischecked" HeaderText="" Visible="false" ReadOnly="true"
                                                        FooterText="objSelect" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="" Visible="false" ReadOnly="true" FooterText = "objdgcolhEmployeeId" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblLeaveInfo" runat="server" Text="Leave Information"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblLeaveType" runat="server" Text="Leave Type" CssClass="form-label"> </asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboLeaveType" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblStartDate" runat="server" Style="margin-left: 20px" Text="Start Date"
                                                            CssClass="form-label"></asp:Label>
                                                         <uc1:DateCtrl ID="dtpStartDate" runat="server"  AutoPostBack="false"/>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblStopDate" runat="server" Text="Stop Date" CssClass="form-label"></asp:Label>
                                                        <uc1:DateCtrl ID="dtpStopDate" runat="server" AutoPostBack="false" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRemarks" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="2" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" />
                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--  <div class="panel-primary">
                    <div class="panel-heading">
                        
                    </div>
                    <div class="panel-body">
                      
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                      
                                    </div>
                                    <div style="text-align: right;">
                                      
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default">
                                    <table style="width: 100%">
                                        <tr style="width: 100%">
                                            <td style="width: 18%">
                                               
                                            </td>
                                            <td style="width: 18%">
                                              
                                            </td>
                                            <td style="width: 18%">
                                             
                                            </td>
                                            <td style="width: 18%">
                                                
                                            </td>
                                            <td style="width: 28%">
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="width: 100%; margin-top: 10px">
                                        <tr style="width: 100%">
                                            <td style="width: 50%">
                                              
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                             
                                                            </td>
                                                            <td style="width: 16%">
                                                               
                                                            </td>
                                                            <td style="width: 28%">
                                                                
                                                            </td>
                                                            <td style="width: 16%">
                                                                
                                                            </td>
                                                            <td style="width: 20%">
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                            <td style="width: 50%">
                                               
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 16%">
                                                            </td>
                                                            <td style="width: 17%">
                                                                
                                                            </td>
                                                            <td style="width: 27%">
                                                               
                                                            </td>
                                                            <td style="width: 13%">
                                                               
                                                            </td>
                                                            <td style="width: 27%">
                                                               
                                                            </td>
                                                        </tr>
                                                    </table>
                                               
                                            </td>
                                        </tr>
                                        <tr style="width: 100%">
                                            <td style="width: 50%">
                                               
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 20%">
                                                               
                                                            </td>
                                                            <td style="width: 16%">
                                                               
                                                            </td>
                                                            <td style="width: 28%">
                                                                
                                                            </td>
                                                            <td style="width: 16%">
                                                              
                                                            </td>
                                                            <td style="width: 20%">
                                                               
                                                            </td>
                                                        </tr>
                                                    </table>
                                            </td>
                                            <td style="width: 50%">
                                                <table style="width: 100%">
                                                    <tr style="width: 100%">
                                                        <td style="width: 16%">
                                                        </td>
                                                        <td style="width: 17%">
                                                            
                                                        </td>
                                                        <td style="width: 27%">
                                                           
                                                        </td>
                                                        <td style="width: 13%">
                                                        </td>
                                                        <td style="width: 27%">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                      
                                    </div>
                                </div>
                            </div>

                        <div id="Div1" class="panel-default">
                            <div id="Div3" class="panel-body-default">
                                <table style="width: 100%">
                                    <tr style="width: 100%">
                                        <td style="width: 35%; padding-left: 8px">
                                           
                                        </td>
                                        <td style="width: 3%">
                                        </td>
                                        <td style="width: 7%">
                                           
                                        </td>
                                        <td style="width: 30%">
                                            
                                        </td>
                                        <td style="width: 25%; text-align: right">
                                           
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 100%" colspan="5">
                                            <div id="scrollable-container" style="overflow: auto; vertical-align: top; max-height: 400px;
                                                margin-bottom: 10px" onscroll="$(scroll1.Y).val(this.scrollTop);">
                                               
                                            </div>
                                        </td>
                                    </tr>
                                    <tr style="width: 100%">
                                        <td style="width: 100%" colspan="5">
                                            <div id="Div2" class="panel-default">
                                                <div id="Div4" class="panel-heading-default">
                                                    <div style="float: left;">
                                                      
                                                    </div>
                                                </div>
                                                <div id="Div5" class="panel-body-default">
                                                    <table style="width: 100%">
                                                        <tr style="width: 100%">
                                                            <td style="width: 10%">
                                                                
                                                            </td>
                                                            <td style="width: 40%">
                                                               
                                                            </td>
                                                            <td style="width: 12%">
                                                               
                                                            </td>
                                                            <td style="width: 15%">
                                                                
                                                            </td>
                                                            <td style="width: 8%">
                                                                
                                                            </td>
                                                            <td style="width: 15%">
                                                                
                                                            </td>
                                                        </tr>
                                                        <tr style="width: 100%">
                                                            <td style="width: 10%; vertical-align: top">
                                                                
                                                            </td>
                                                            <td style="width: 100%" colspan="5">
                                                              
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div class="btn-default">
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <uc2:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
