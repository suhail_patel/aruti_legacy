﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data

#End Region

Partial Class Leave_Approver_Migration
    Inherits Basepage

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTransfer_Approver"
    Private clsuser As New User
    Private DisplayMessage As New CommonCodes
    Private dtFromEmp As DataTable
    Private dtAssignedEmp As DataTable
    Private dtMigratedEmp As DataTable
    'Nilay (18-Mar-2015) -- Start
    'Enhancement - ADD ALLOCATION LINK IN LEAVE APPROVER MIGRATION AS PER FINCA TRANSANIA REQUIREMENT
    Private dvFromEmp As DataView
    Private mstrAdvanceFilter As String = String.Empty
    'Nilay (18-Mar-2015) -- End

    'S.SANDEEP [30 JAN 2016] -- START
    Private dtComboFromApprover As DataTable
    Private dtComboToApprover As DataTable
    'S.SANDEEP [30 JAN 2016] -- END

#End Region

#Region " Page Events "

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            GC.Collect()
            'Pinkal (11-Sep-2020) -- End




            If IsPostBack = False Then
                SetLanguage()
                Call FillCombo()
                'btnBack.Visible = False
            Else
                mstrAdvanceFilter = IIf(Me.ViewState("EmpAdvanceSearch") Is Nothing, "", Me.ViewState("EmpAdvanceSearch")).ToString()
            End If

            If Me.ViewState("dtComboFromApprover") IsNot Nothing Then
                dtComboFromApprover = CType(Me.ViewState("dtComboFromApprover"), DataTable)
            End If

            If Me.ViewState("dtComboToApprover") IsNot Nothing Then
                dtComboToApprover = CType(Me.ViewState("dtComboToApprover"), DataTable)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If Me.ViewState("dtComboFromApprover") Is Nothing Then
                Me.ViewState("dtComboFromApprover") = dtComboFromApprover
            End If

            If Me.ViewState("dtComboToApprover") Is Nothing Then
                Me.ViewState("dtComboToApprover") = dtComboToApprover
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Buttons "

    'Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
    '    Try
    '        If mltiview.ActiveViewIndex > 0 Then
    '            mltiview.ActiveViewIndex -= 1
    '        End If
    '        btnNext.Visible = True : btnBack.Visible = False

    '        'Pinkal (14-May-2020) -- Start
    '        'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
    '        ' txtToSearch.Text = ""
    '        'Pinkal (14-May-2020) -- End

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
    '    Try
    '        If mltiview.ActiveViewIndex < mltiview.Views.Count Then
    '            mltiview.ActiveViewIndex += 1
    '        End If
    '        btnNext.Visible = False : btnBack.Visible = True
    '        'Pinkal (14-May-2020) -- Start
    '        'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
    '        ' txtToSearch.Text = ""
    '        'Pinkal (14-May-2020) -- End
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Protected Sub objbtnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnAssign.Click
        Try
            If Me.ViewState("dtFromEmp") Is Nothing Then
                DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
                Exit Sub
            End If

            If CInt(cboToApprover.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select To Approver to migrate employee(s).", Me)
                Exit Sub
            End If

            If CInt(cboToLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Level to migrate employee(s).", Me)
                Exit Sub
            End If


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.

            '            Dim drRow() As DataRow = CType(Me.ViewState("dtFromEmp"), DataTable).Select("select = true")

            '            If drRow.Length <= 0 Then
            'warning:        DisplayMessage.DisplayMessage("Please check atleast one employee to migrate.", Me)
            '                Exit Sub
            '            End If


            'Dim dtGetExistData As DataTable = Nothing
            'For i As Integer = 0 To drRow.Length - 1

            '    If CType(Me.ViewState("dtAssignedEmp"), DataTable) IsNot Nothing AndAlso CType(Me.ViewState("dtAssignedEmp"), DataTable).Rows.Count > 0 Then
            '        Dim dRow() As DataRow = CType(Me.ViewState("dtAssignedEmp"), DataTable).Select("employeeunkid=" & drRow(i)("employeeunkid").ToString())
            '        If dRow.Length > 0 Then
            '            blnFlag = True
            '            If dtGetExistData Is Nothing Then
            '                dtGetExistData = CType(Me.ViewState("dtAssignedEmp"), DataTable).Clone
            '            End If
            '            dtGetExistData.ImportRow(drRow(i))
            '            Continue For
            '        End If
            '    End If

            '    drRow(i)("select") = False

            '    If CInt(drRow(i)("employeeunkid")) = CInt(cboToApprover.SelectedValue) Then Continue For

            '    If dtMigratedEmp Is Nothing Then
            '        dtMigratedEmp = CType(Me.ViewState("dtFromEmp"), DataTable).Clone
            '        dtMigratedEmp.ImportRow(drRow(i))
            '    Else
            '        Dim dr As DataRow() = dtMigratedEmp.Select("employeeunkid = " & CInt(drRow(i)("employeeunkid")))
            '        If dr.Length <= 0 Then
            '            dtMigratedEmp.ImportRow(drRow(i))
            '        End If
            '    End If

            '    Me.ViewState("dtMigratedEmp") = dtMigratedEmp

            '    CType(Me.ViewState("dtFromEmp"), DataTable).Rows.Remove(drRow(i))
            '    CType(Me.ViewState("dtFromEmp"), DataTable).AcceptChanges()

            '    'objLeaveForm = Nothing
            'Next


            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvFrmEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkbox1"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
                Exit Sub
            End If


            Dim rblnPendingFlag As Boolean = False
            Dim blnFlag As Boolean = False
            If Me.ViewState("dtMigratedEmp") Is Nothing Then
                Me.ViewState.Add("dtMigratedEmp", dtMigratedEmp)
            Else
                dtMigratedEmp = CType(Me.ViewState("dtMigratedEmp"), DataTable)
            End If

            Dim xCount As Integer = -1
            For i As Integer = 0 To gRow.Count - 1
                xCount = i
                Dim dRow() As DataRow = Nothing
                If CType(Me.ViewState("dtAssignedEmp"), DataTable) IsNot Nothing AndAlso CType(Me.ViewState("dtAssignedEmp"), DataTable).Rows.Count > 0 Then
                    dRow = CType(Me.ViewState("dtAssignedEmp"), DataTable).Select("employeeunkid=" & CInt(dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")))
                    If dRow.Length > 0 Then
                        blnFlag = True
                        Continue For
                    End If
                End If


                If CInt(dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")) = CInt(cboToApprover.SelectedValue) Then Continue For

                If dtMigratedEmp Is Nothing Then
                    dtMigratedEmp = CType(Me.ViewState("dtFromEmp"), DataTable).Clone
                    Dim drMigratedEmp As DataRow = dtMigratedEmp.NewRow()
                    drMigratedEmp("select") = False
                    drMigratedEmp("approverunkid") = CInt(dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("approverunkid"))
                    drMigratedEmp("leaveapprovertranunkid") = CInt(dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("leaveapprovertranunkid"))
                    drMigratedEmp("leaveapproverunkid") = CInt(dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("leaveapproverunkid"))
                    drMigratedEmp("levelunkid") = 0
                    drMigratedEmp("employeeunkid") = CInt(dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid"))
                    drMigratedEmp("employeecode") = dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("employeecode").ToString()
                    drMigratedEmp("employeename") = dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("employeename").ToString()
                    dtMigratedEmp.Rows.Add(drMigratedEmp)
                Else
                    Dim dr As DataRow() = dtMigratedEmp.Select("employeeunkid = " & CInt(dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")))
                    If dr.Length <= 0 Then
                        Dim drMigratedEmp As DataRow = dtMigratedEmp.NewRow()
                        drMigratedEmp("select") = False
                        drMigratedEmp("approverunkid") = CInt(dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("approverunkid"))
                        drMigratedEmp("leaveapprovertranunkid") = CInt(dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("leaveapprovertranunkid"))
                        drMigratedEmp("leaveapproverunkid") = CInt(dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("leaveapproverunkid"))
                        drMigratedEmp("levelunkid") = 0
                        drMigratedEmp("employeeunkid") = CInt(dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid"))
                        drMigratedEmp("employeecode") = dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("employeecode").ToString()
                        drMigratedEmp("employeename") = dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("employeename").ToString()
                        dtMigratedEmp.Rows.Add(drMigratedEmp)
                    End If
                End If

                Me.ViewState("dtMigratedEmp") = dtMigratedEmp

                If CType(Me.ViewState("dtFromEmp"), DataTable) IsNot Nothing Then
                    Dim drEmp = CType(Me.ViewState("dtFromEmp"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of String)("employeecode") <> "None" And x.Field(Of Integer)("employeeunkid") = CInt(dgvFrmEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")))
                    If drEmp.Count > 0 Then
                        CType(Me.ViewState("dtFromEmp"), DataTable).Rows.Remove(drEmp(0))
                CType(Me.ViewState("dtFromEmp"), DataTable).AcceptChanges()
                    End If
                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    drEmp = Nothing
                    'Pinkal (11-Sep-2020) -- End
                End If
            Next

            'Pinkal (14-May-2020) -- End

            If blnFlag Then
                DisplayMessage.DisplayMessage("Some of the checked employee is already binded with the selected approver.", Me)
                Me.ViewState("dtMigratedEmp") = dtMigratedEmp
            End If

            Call FillMigratedEmployee()
            Call FillOldApproverEmployeeList()


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            'txtFrmSearch.Text = ""
            'txtFrmSearch.Focus()
            'Pinkal (14-May-2020) -- End
        End Try
    End Sub

    Protected Sub objbtnUnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles objbtnUnAssign.Click
        Try

            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.

            If Me.ViewState("dtMigratedEmp") Is Nothing Then
                'GoTo warning
                DisplayMessage.DisplayMessage("Please check atleast one employee to unassign.", Me)
                Exit Sub
            End If

            '            Dim drRow() As DataRow = CType(Me.ViewState("dtMigratedEmp"), DataTable).Select("select = true AND employeecode <> 'None'")
            '            If drRow.Length <= 0 Then
            'warning:        DisplayMessage.DisplayMessage("Please check atleast one employee to unassign.", Me)
            '                Exit Sub
            '            End If

            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvToEmp.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("chkbox2"), CheckBox).Checked = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Employee is compulsory information.Please Check atleast One Employee.", Me)
                Exit Sub
            End If

            'lnkReset_Click(lnkReset, Nothing)
            'txtFrmSearch.Text = ""
            mstrAdvanceFilter = ""

            'Pinkal (14-May-2020) -- End


            If Me.ViewState("dtFromEmp") Is Nothing Then
                Me.ViewState.Add("dtFromEmp", dtFromEmp)
            Else
                dtFromEmp = CType(Me.ViewState("dtFromEmp"), DataTable)
            End If


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.

            'For i As Integer = 0 To drRow.Length - 1
            '    drRow(i)("select") = False
            '    If dtFromEmp Is Nothing Then
            '        dtFromEmp = CType(Me.ViewState("dtMigratedEmp"), DataTable).Clone
            '        dtFromEmp.ImportRow(drRow(i))
            '    Else
            '        dtFromEmp.ImportRow(drRow(i))
            '    End If
            '    Me.ViewState("dtFromEmp") = dtFromEmp
            '    CType(Me.ViewState("dtMigratedEmp"), DataTable).Rows.Remove(drRow(i))
            'Next

            Dim xCount As Integer = -1
            For i As Integer = 0 To gRow.Count - 1
                xCount = i
                If dtFromEmp Is Nothing Then
                    dtFromEmp = CType(Me.ViewState("dtMigratedEmp"), DataTable).Clone
                    Dim drFromEmp As DataRow = dtFromEmp.NewRow()
                    drFromEmp("select") = False
                    drFromEmp("approverunkid") = CInt(dgvToEmp.DataKeys(gRow(xCount).DataItemIndex)("approverunkid"))
                    drFromEmp("leaveapprovertranunkid") = CInt(dgvToEmp.DataKeys(gRow(xCount).DataItemIndex)("leaveapprovertranunkid"))
                    drFromEmp("leaveapproverunkid") = CInt(dgvToEmp.DataKeys(gRow(xCount).DataItemIndex)("leaveapproverunkid"))
                    drFromEmp("levelunkid") = 0
                    drFromEmp("employeeunkid") = CInt(dgvToEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid"))
                    drFromEmp("employeecode") = dgvToEmp.DataKeys(gRow(xCount).DataItemIndex)("employeecode").ToString()
                    drFromEmp("employeename") = dgvToEmp.DataKeys(gRow(xCount).DataItemIndex)("employeename").ToString()
                    dtFromEmp.Rows.Add(drFromEmp)
                Else
                    Dim drFromEmp As DataRow = dtFromEmp.NewRow()
                    drFromEmp("select") = False
                    drFromEmp("approverunkid") = CInt(dgvToEmp.DataKeys(gRow(xCount).DataItemIndex)("approverunkid"))
                    drFromEmp("leaveapprovertranunkid") = CInt(dgvToEmp.DataKeys(gRow(xCount).DataItemIndex)("leaveapprovertranunkid"))
                    drFromEmp("leaveapproverunkid") = CInt(dgvToEmp.DataKeys(gRow(xCount).DataItemIndex)("leaveapproverunkid"))
                    drFromEmp("levelunkid") = 0
                    drFromEmp("employeeunkid") = CInt(dgvToEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid"))
                    drFromEmp("employeecode") = dgvToEmp.DataKeys(gRow(xCount).DataItemIndex)("employeecode").ToString()
                    drFromEmp("employeename") = dgvToEmp.DataKeys(gRow(xCount).DataItemIndex)("employeename").ToString()
                    dtFromEmp.Rows.Add(drFromEmp)
                End If
                Me.ViewState("dtFromEmp") = dtFromEmp


                If CType(Me.ViewState("dtMigratedEmp"), DataTable) IsNot Nothing Then
                    Dim drEmp = CType(Me.ViewState("dtMigratedEmp"), DataTable).AsEnumerable().Where(Function(x) x.Field(Of String)("employeecode") <> "None" And x.Field(Of Integer)("employeeunkid") = CInt(dgvToEmp.DataKeys(gRow(xCount).DataItemIndex)("employeeunkid")))
                    If drEmp.Count > 0 Then
                        CType(Me.ViewState("dtMigratedEmp"), DataTable).Rows.Remove(drEmp(0))
                        CType(Me.ViewState("dtMigratedEmp"), DataTable).AcceptChanges()
                    End If
                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    drEmp = Nothing
                    'Pinkal (11-Sep-2020) -- End

                End If

            Next

            'Pinkal (14-May-2020) -- End

            Call FillOldApproverEmployeeList()
            Call FillMigratedEmployee()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try

            'Pinkal (11-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	
            'Response.Redirect("~/Userhome.aspx")
            Response.Redirect("~/Userhome.aspx", False)
            'Pinkal (11-Feb-2022) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Procedure btnClose_Click : " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect("~/Userhome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("Procedure Closebotton1_CloseButton_click : " & ex.Message, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CInt(Me.ViewState("cboToApprover")) <= 0 Then
                DisplayMessage.DisplayMessage("Approver not found. Please reselect To Approver.", Me)
                Exit Sub
            End If

            If CInt(cboFrmApprover.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select From Approver to migrate employee(s).", Me)
                Exit Sub
            ElseIf CInt(cboFrmLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Level to migrate employee(s).", Me)
                Exit Sub
            ElseIf CInt(cboToApprover.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select To Approver to migrate employee(s).", Me)
                Exit Sub
            ElseIf CInt(cboToLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Level to migrate employee(s).", Me)
                Exit Sub
            ElseIf dgvToEmp.Rows.Count = 0 Then
                DisplayMessage.DisplayMessage("There is no employee(s) for migration.", Me)
                Exit Sub
            End If



            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            Dim gRow As IEnumerable(Of GridViewRow) = Nothing
            gRow = dgvToEmp.Rows.Cast(Of GridViewRow).Where(Function(x) x.Visible = True)

            If gRow Is Nothing OrElse gRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("There is no employee(s) for migration.", Me)
                Exit Sub
            End If

            'Pinkal (14-May-2020) -- End


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'Pinkal (11-Sep-2020) -- End


            dtMigratedEmp = CType(Me.ViewState("dtMigratedEmp"), DataTable)
            dtAssignedEmp = CType(Me.ViewState("dtAssignedEmp"), DataTable)

            Dim dtRow() As DataRow = dtMigratedEmp.Select("employeecode = 'None'")
            If dtRow.Length > 0 Then
                dtMigratedEmp.Rows.Remove(dtRow(0))
                dtMigratedEmp.AcceptChanges()
            End If

            Dim objApproverTran As New clsleaveapprover_Tran

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmLeaveApproversList"
            StrModuleName2 = "mnuLeaveInformation"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()


            'Pinkal (12-Oct-2020) -- Start
            'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
            'If objApproverTran.Migration_Insert(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, dtMigratedEmp, CInt(Me.ViewState("cboToApprover")), CInt(cboToApprover.SelectedValue), CBool(Session("PaymentApprovalwithLeaveApproval")), CInt(Session("UserId"))) = False Then
            If objApproverTran.Migration_Insert(CStr(Session("Database_Name")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, dtMigratedEmp, CInt(Me.ViewState("cboToApprover")) _
                                                              , CInt(cboToApprover.SelectedValue), CBool(Session("PaymentApprovalwithLeaveApproval")), chkShowInActiveEmployees.Checked _
                                                              , CInt(Session("UserId"))) = False Then
                'Pinkal (12-Oct-2020) -- End
                DisplayMessage.DisplayMessage("Problem in Approver Migration.", Me)
            Else
                DisplayMessage.DisplayMessage("Approver Migration done successfully.", Me)
                Me.ViewState("dvFromEmp") = Nothing
                Me.ViewState("EmpAdvanceSearch") = Nothing
                Me.ViewState("dtFromEmp") = Nothing
                Me.ViewState("dtMigratedEmp") = Nothing
                Me.ViewState("dtAssignedEmp") = Nothing
                Me.ViewState("gv1_FirstRecordNo") = Nothing
                Me.ViewState("gv1_LastRecordNo") = Nothing
                Me.ViewState("gv2_FirstRecordNo") = Nothing
                Me.ViewState("gv2_LastRecordNo") = Nothing
                cboFrmLevel.SelectedValue = "0"
                cboToLevel.SelectedValue = "0"
                cboToApprover.SelectedValue = "0"
                cboFrmApprover.SelectedValue = "0"

                dgvAssignedEmp.DataSource = Nothing
                dgvAssignedEmp.DataBind()

                dgvFrmEmp.DataSource = Nothing
                dgvFrmEmp.DataBind()

                dgvToEmp.DataSource = Nothing
                dgvToEmp.DataBind()

                'Pinkal (14-May-2020) -- Start
                'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
                chkShowInactiveApprovers.Checked = False
                chkShowInActiveEmployees.Checked = False
                chkShowInActiveEmployees.Enabled = True
                FillCombo()
                'Pinkal (14-May-2020) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Nilay (18-Mar-2015) -- Start
    'Enhancement - ADD ALLOCATION LINK IN LEAVE APPROVER MIGRATION AS PER FINCA TRANSANIA REQUIREMENT

    Protected Sub popupAdvanceFilter_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonApply_Click
        Try

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            'mstrAdvanceFilter = popupAdvanceFilter._GetFilterString
            mstrAdvanceFilter = popupAdvanceFilter._GetFilterString.Replace("ADF.", "")
            'Pinkal (06-Jan-2016) -- End

            Me.ViewState.Add("EmpAdvanceSearch", mstrAdvanceFilter)
            Call FillOldApproverEmployeeList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAdvanceFilter_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAdvanceFilter.buttonClose_Click
        Try
            mstrAdvanceFilter = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Nilay (18-Mar-2015) -- End

#End Region

#Region " Private Function & Procedures "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objApprover As New clsleaveapprover_master
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim dtTable As DataTable = Nothing
        'Pinkal (11-Sep-2020) -- End
        Try



            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.

            'dsList = objApprover.GetList("List", _
            '                           Session("Database_Name").ToString(), _
            '                           CInt(Session("UserId")), _
            '                           CInt(Session("Fin_year")), _
            '                           CInt(Session("CompanyUnkId")), _
            '                           Session("EmployeeAsOnDate").ToString(), _
            '                           Session("UserAccessModeSetting").ToString(), True, _
            '                           True, True, True, -1, Nothing, "", "")


            dsList = objApprover.GetList("List", _
                                       Session("Database_Name").ToString(), _
                                       CInt(Session("UserId")), _
                                       CInt(Session("Fin_year")), _
                                       CInt(Session("CompanyUnkId")), _
                                       Session("EmployeeAsOnDate").ToString(), _
                                       Session("UserAccessModeSetting").ToString(), True, _
                                      chkShowInactiveApprovers.Checked, True, True, -1, Nothing, "", "")

            dtComboFromApprover = dsList.Tables(0)

            dtTable = dsList.Tables(0).DefaultView.ToTable(True, "leaveapproverunkid", "name", "isexternalapprover")

            Dim dr As DataRow = dtTable.NewRow
            dr("leaveapproverunkid") = 0
            dr("name") = "Select"
            dr("isexternalapprover") = False
            dtTable.Rows.InsertAt(dr, 0)

            cboFrmApprover.DataTextField = "name"
            cboFrmApprover.DataValueField = "leaveapproverunkid"
            cboFrmApprover.DataSource = dtTable
            cboFrmApprover.DataBind()

            'dtTable = Nothing
            'dtTable = objApprover.GetList("List", _
            '                           Session("Database_Name").ToString(), _
            '                           CInt(Session("UserId")), _
            '                           CInt(Session("Fin_year")), _
            '                           CInt(Session("CompanyUnkId")), _
            '                           Session("EmployeeAsOnDate").ToString(), _
            '                           Session("UserAccessModeSetting").ToString(), True, _
            '                           True, True, True, -1, Nothing, "", "").Tables(0)

            'dtComboFromApprover = dtTable

            'Pinkal (14-May-2020) -- End


            cboFrmApprover_SelectedIndexChanged(New Object(), New EventArgs())
            cboToApprover_SelectedIndexChanged(New Object(), New EventArgs())

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            objApprover = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    'S.SANDEEP [30 JAN 2016] -- START

    'Pinkal (13-Jul-2017) -- Start
    'Enhancement - Bug Solved in Leave Approver Migration .
    'Private Sub GetLeaveApproverLevel(ByVal sender As String, ByVal intApproverID As Integer, ByVal blnExternalApprover As Boolean)
    Private Sub GetLeaveApproverLevel(ByVal sender As String, ByVal intApproverEmpID As Integer, ByVal blnExternalApprover As Boolean)
        'Pinkal (13-Jul-2017) -- End
        'Private Sub GetLeaveApproverLevel(ByVal sender As String, ByVal intApproverID As Integer)
        'S.SANDEEP [30 JAN 2016] -- END

        Try
            Dim objLeaveApprover As New clsleaveapprover_master

            'S.SANDEEP [30 JAN 2016] -- START
            'Dim dsList As DataSet = objLeaveApprover.GetLevelFromLeaveApprover(True, intApproverID)
            Dim dsList As DataSet = objLeaveApprover.GetLevelFromLeaveApprover(intApproverEmpID, blnExternalApprover, True)
            'S.SANDEEP [30 JAN 2016] -- END


            If sender.ToUpper = "CBOTOAPPROVER" Then
                cboToLevel.DataTextField = "levelname"
                cboToLevel.DataValueField = "levelunkid"
                cboToLevel.DataSource = dsList.Tables(0)
                cboToLevel.DataBind()
            ElseIf sender.ToUpper = "CBOFRMAPPROVER" Then
                cboFrmLevel.DataTextField = "levelname"
                cboFrmLevel.DataValueField = "levelunkid"
                cboFrmLevel.DataSource = dsList.Tables(0)
                cboFrmLevel.DataBind()
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objLeaveApprover = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillNewApproverAssignEmployeeList()
        Try
            If dtAssignedEmp Is Nothing Then
                dtAssignedEmp = CType(Me.ViewState("dtAssignedEmp"), DataTable)
            End If

            dgvAssignedEmp.AutoGenerateColumns = False
            dgvAssignedEmp.DataSource = dtAssignedEmp
            dgvAssignedEmp.DataBind()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillOldApproverEmployeeList()
        Try
            If dtFromEmp Is Nothing Then
                dtFromEmp = CType(Me.ViewState("dtFromEmp"), DataTable)
            End If

            'Nilay (18-Mar-2015) -- Start
            'Enhancement - ADD ALLOCATION LINK IN LEAVE APPROVER MIGRATION AS PER FINCA TRANSANIA REQUIREMENT
            dvFromEmp = dtFromEmp.DefaultView
            If ViewState("EmpAdvanceSearch") IsNot Nothing Then
                dvFromEmp.RowFilter = CType(Me.ViewState("EmpAdvanceSearch"), String)
                'dvFromEmp.RowFilter = mstrAdvanceFilter
                dvFromEmp.Table.AcceptChanges()
                dtFromEmp = dvFromEmp.Table
                Me.ViewState("dtFromEmp") = dtFromEmp
            End If
            'Nilay (18-Mar-2015) -- End

            dgvFrmEmp.AutoGenerateColumns = False
            dgvFrmEmp.DataSource = dtFromEmp
            dgvFrmEmp.DataBind()
            If Me.ViewState("gv1_FirstRecordNo") Is Nothing Then
                Me.ViewState.Add("gv1_FirstRecordNo", 0)
            End If
            If Me.ViewState("gv1_LastRecordNo") Is Nothing Then
                Me.ViewState.Add("gv1_LastRecordNo", dgvFrmEmp.PageSize)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

        End Try
    End Sub

    Private Sub FillMigratedEmployee()
        Try
            If dtMigratedEmp Is Nothing Then
                dtMigratedEmp = CType(Me.ViewState("dtMigratedEmp"), DataTable)
            End If

            dgvToEmp.AutoGenerateColumns = False
            dgvToEmp.DataSource = dtMigratedEmp
            dgvToEmp.DataBind()

            If Me.ViewState("gv2_FirstRecordNo") Is Nothing Then
                Me.ViewState.Add("gv2_FirstRecordNo", 0)
            End If
            If Me.ViewState("gv2_LastRecordNo") Is Nothing Then
                Me.ViewState.Add("gv2_LastRecordNo", dgvToEmp.PageSize)
            End If


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            Dim drRow = dtMigratedEmp.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") > 0)
            If drRow.Count > 0 Then
                chkShowInActiveEmployees.Enabled = False
            Else
                chkShowInActiveEmployees.Enabled = True
            End If
            'Pinkal (14-May-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboFrmApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFrmApprover.SelectedIndexChanged
        Try
            Dim objLeaveApprover As New clsleaveapprover_master


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.

            'Dim dsList As DataSet = objLeaveApprover.GetList("List", _
            '                                             Session("Database_Name").ToString(), _
            '                                             CInt(Session("UserId")), _
            '                                             CInt(Session("Fin_year")), _
            '                                             CInt(Session("CompanyUnkId")), _
            '                                             Session("EmployeeAsOnDate").ToString(), _
            '                                             Session("UserAccessModeSetting").ToString(), True, _
            '                                             True, True, False, -1, Nothing, "", "")

            Dim dsList As DataSet = objLeaveApprover.GetList("List", _
                                                           Session("Database_Name").ToString(), _
                                                           CInt(Session("UserId")), _
                                                           CInt(Session("Fin_year")), _
                                                           CInt(Session("CompanyUnkId")), _
                                                           Session("EmployeeAsOnDate").ToString(), _
                                                           Session("UserAccessModeSetting").ToString(), True, _
                                                         chkShowInactiveApprovers.Checked, True, False, -1, Nothing, "", "")
            'Pinkal (14-May-2020) -- End


            Dim dtmp() As DataRow = dtComboFromApprover.Select("leaveapproverunkid = '" & CInt(cboFrmApprover.SelectedValue) & "'")

            Dim dtTable As DataTable = Nothing

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If CInt(cboFrmApprover.SelectedValue) <= 0 Then
            '    dtTable = dsList.Tables(0)
            'Else
            '    dtTable = New DataView(dsList.Tables(0), "leaveapproverunkid NOT IN(" & CInt(cboFrmApprover.SelectedValue) & ")", "", DataViewRowState.CurrentRows).ToTable()
            'End If
            If CInt(cboFrmApprover.SelectedValue) <= 0 Then
                dtTable = dsList.Tables(0).Copy()
            Else
                dtTable = New DataView(dsList.Tables(0), "leaveapproverunkid NOT IN(" & CInt(cboFrmApprover.SelectedValue) & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
            End If
            'Pinkal (11-Sep-2020) -- End

            dtTable = dtTable.DefaultView.ToTable(True, "leaveapproverunkid", "name", "isexternalapprover")

            Dim dr As DataRow = dtTable.NewRow

            dr("leaveapproverunkid") = 0
            dr("name") = "Select"
            dr("isexternalapprover") = False
            dtTable.Rows.InsertAt(dr, 0)

            cboToApprover.DataTextField = "name"
            cboToApprover.DataValueField = "leaveapproverunkid"
            cboToApprover.DataSource = dtTable
            cboToApprover.DataBind()


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'dtComboToApprover = dtTable
            dtComboToApprover = dtTable.Copy()
            'Pinkal (11-Sep-2020) -- End



            If dtmp.Length > 0 Then
                GetLeaveApproverLevel("cboFrmApprover", CInt(cboFrmApprover.SelectedValue), CBool(dtmp(0).Item("isexternalapprover")))
            Else
                GetLeaveApproverLevel("cboFrmApprover", CInt(cboFrmApprover.SelectedValue), False)
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objLeaveApprover = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboToApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboToApprover.SelectedIndexChanged
        Try
            'Pinkal (08-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	
            If dtComboToApprover Is Nothing Then Exit Sub
            'Pinkal (08-Feb-2022) -- End

            Dim dtmp() As DataRow = dtComboToApprover.Select("leaveapproverunkid = '" & CInt(cboToApprover.SelectedValue) & "'")
            If dtmp.Length > 0 Then
                GetLeaveApproverLevel("cboToApprover", CInt(cboToApprover.SelectedValue), CBool(dtmp(0).Item("isexternalapprover")))
            Else
                GetLeaveApproverLevel("cboToApprover", CInt(cboToApprover.SelectedValue), False)
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            dtmp = Nothing
            'Pinkal (11-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboFrmLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFrmLevel.SelectedIndexChanged
        Try
            Dim objLeaveApprover As New clsleaveapprover_master

            Dim dtmp() As DataRow = dtComboFromApprover.Select("leaveapproverunkid = '" & CInt(cboFrmApprover.SelectedValue) & "'")


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            'Dim dsOldApproverEmp As DataSet = objLeaveApprover.GetEmployeeFromApprover(CInt(cboFrmApprover.SelectedValue), CInt(cboFrmLevel.SelectedValue) _
            '                                                                                                                           , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, CBool(dtmp(0).Item("isexternalapprover")))
            'Pinkal (28-May-2020) -- Start
            'BUG NMB:  problem solved on TnA OT Requisition whenever employee apply for OT on holiday. 
            Dim dsOldApproverEmp As DataSet = Nothing
            If dtmp.Length > 0 Then
                dsOldApproverEmp = objLeaveApprover.GetEmployeeFromApprover(Session("Database_Name").ToString(), CInt(cboFrmApprover.SelectedValue), CInt(cboFrmLevel.SelectedValue) _
                                                                                                                                       , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, CBool(dtmp(0).Item("isexternalapprover")) _
                                                                                                                                       , chkShowInActiveEmployees.Checked)
            Else
                dsOldApproverEmp = objLeaveApprover.GetEmployeeFromApprover(Session("Database_Name").ToString(), CInt(cboFrmApprover.SelectedValue), CInt(cboFrmLevel.SelectedValue) _
                                                                                                                                          , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, False _
                                                                                                                                          , chkShowInActiveEmployees.Checked)
            End If
            'Pinkal (28-May-2020) -- End

            'Pinkal (14-May-2020) -- End


            dtFromEmp = dsOldApproverEmp.Tables(0).Copy
            dvFromEmp = dsOldApproverEmp.Tables(0).DefaultView
            Me.ViewState.Add("dvFromEmp", dtFromEmp)
            Me.ViewState.Add("dtFromEmp", dtFromEmp)
            FillOldApproverEmployeeList()

            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            'If dsOldApproverEmp IsNot Nothing AndAlso dsOldApproverEmp.Tables(0).Rows.Count > 0 Then
            '    Me.ViewState.Add("cboFrmApprover", CInt(dsOldApproverEmp.Tables(0).Rows(0)("approverunkid")))
            'Else
            '    Me.ViewState.Add("cboFrmApprover", 0)
            'End If

            If dsOldApproverEmp IsNot Nothing Then
                If dsOldApproverEmp.Tables(0).Rows.Count > 0 Then
                Me.ViewState.Add("cboFrmApprover", CInt(dsOldApproverEmp.Tables(0).Rows(0)("approverunkid")))
            Else
                    Me.ViewState.Add("cboFrmApprover", CInt(objLeaveApprover.GetApproverID(CInt(cboFrmApprover.SelectedValue), CInt(cboFrmLevel.SelectedValue))))
                End If
            Else
                Me.ViewState.Add("cboFrmApprover", 0)
            End If
            'Pinkal (14-May-2020) -- End


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsOldApproverEmp IsNot Nothing Then dsOldApproverEmp.Clear()
            dsOldApproverEmp = Nothing
            objLeaveApprover = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub cboToLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboToLevel.SelectedIndexChanged
        Try


            'Pinkal (08-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	
            If dtComboToApprover Is Nothing Then Exit Sub
            'Pinkal (08-Feb-2022) -- End


            Dim objLeaveApprover As New clsleaveapprover_master



            Dim dtmp() As DataRow = dtComboToApprover.Select("leaveapproverunkid = '" & CInt(cboToApprover.SelectedValue) & "'")

            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            'Dim dsNewApproverEmp As DataSet = objLeaveApprover.GetEmployeeFromApprover(CInt(cboToApprover.SelectedValue), CInt(cboToLevel.SelectedValue) _
            '                                                                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, CBool(dtmp(0).Item("isexternalapprover")))

            'Pinkal (28-May-2020) -- Start
            'BUG NMB:  problem solved on TnA OT Requisition whenever employee apply for OT on holiday. 
            Dim dsNewApproverEmp As DataSet = Nothing
            If dtmp.Length > 0 Then
                dsNewApproverEmp = objLeaveApprover.GetEmployeeFromApprover(Session("Database_Name").ToString(), CInt(cboToApprover.SelectedValue), CInt(cboToLevel.SelectedValue) _
                                                                                                                                        , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                                                                        , CBool(dtmp(0).Item("isexternalapprover")), chkShowInActiveEmployees.Checked)
            Else
                dsNewApproverEmp = objLeaveApprover.GetEmployeeFromApprover(Session("Database_Name").ToString(), CInt(cboToApprover.SelectedValue), CInt(cboToLevel.SelectedValue) _
                                                                                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date _
                                                                                                                                            , False, chkShowInActiveEmployees.Checked)
            End If
            'Pinkal (28-May-2020) -- End

            'Pinkal (14-May-2020) -- End




            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'dtAssignedEmp = dsNewApproverEmp.Tables(0)
            dtAssignedEmp = dsNewApproverEmp.Tables(0).Copy()
            'Pinkal (11-Sep-2020) -- End


            Me.ViewState.Add("dtAssignedEmp", dtAssignedEmp)
            FillNewApproverAssignEmployeeList()

            'Pinkal (28-May-2020) -- Start
            'BUG NMB:  problem solved on TnA OT Requisition whenever employee apply for OT on holiday. 
            If dtMigratedEmp Is Nothing Then
                dtMigratedEmp = CType(Me.ViewState("dtMigratedEmp"), DataTable)
            End If
            'Pinkal (28-May-2020) -- End

            If CInt(cboToApprover.SelectedValue) <= 0 Then
                If dtMigratedEmp IsNot Nothing Then
                    'Pinkal (28-May-2020) -- Start
                    'BUG NMB:  problem solved on TnA OT Requisition whenever employee apply for OT on holiday. 
                    dtMigratedEmp.Rows.Clear()
                    dgvToEmp.AutoGenerateColumns = False
                    dgvToEmp.DataSource = dtMigratedEmp
                    dgvToEmp.DataBind()
                    'Pinkal (28-May-2020) -- End
                End If

            Else
                dtMigratedEmp = dtAssignedEmp.Copy
                dtMigratedEmp.Rows.Clear()
                If dtMigratedEmp.Rows.Count <= 0 Then
                    Dim r As DataRow = dtMigratedEmp.NewRow

                    'Pinkal (14-May-2020) -- Start
                    'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
                    'r.Item(5) = "None" ' To Hide the row and display only Row Header
                    r.Item("select") = False
                    r.Item("approverunkid") = 0
                    r.Item("leaveapprovertranunkid") = 0
                    r.Item("leaveapproverunkid") = 0
                    r.Item("levelunkid") = 0
                    r.Item("employeeunkid") = 0
                    r.Item("employeecode") = "None" ' To Hide the row and display only Row Header
                    'Pinkal (14-May-2020) -- End


                    dtMigratedEmp.Rows.Add(r)
                    dgvToEmp.AutoGenerateColumns = False
                    dgvToEmp.DataSource = dtMigratedEmp
                    dgvToEmp.DataBind()
                End If
                Me.ViewState.Add("dtMigratedEmp", dtMigratedEmp)
            End If


            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            'If dsNewApproverEmp IsNot Nothing AndAlso dsNewApproverEmp.Tables(0).Rows.Count > 0 Then
            '    Me.ViewState.Add("cboToApprover", CInt(dsNewApproverEmp.Tables(0).Rows(0)("approverunkid")))
            'Else
            '    Me.ViewState.Add("cboToApprover", 0)
            'End If


            If dsNewApproverEmp IsNot Nothing Then
                If dsNewApproverEmp.Tables(0).Rows.Count > 0 Then
                Me.ViewState.Add("cboToApprover", CInt(dsNewApproverEmp.Tables(0).Rows(0)("approverunkid")))
                Else
                    Me.ViewState.Add("cboToApprover", CInt(objLeaveApprover.GetApproverID(CInt(cboToApprover.SelectedValue), CInt(cboToLevel.SelectedValue))))
                End If
            Else
                Me.ViewState.Add("cboToApprover", 0)
            End If
            'Pinkal (14-May-2020) -- End


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsNewApproverEmp IsNot Nothing Then dsNewApproverEmp.Clear()
            dsNewApproverEmp = Nothing
            objLeaveApprover = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Grid View Events "

    Protected Sub dgvFrmEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgvFrmEmp.PageIndexChanging
        Try
            dgvFrmEmp.PageIndex = e.NewPageIndex

            If dgvFrmEmp.PageIndex > 0 Then
                Me.ViewState("gv1_FirstRecordNo") = (((dgvFrmEmp.PageIndex + 1) * dgvFrmEmp.Rows.Count) - dgvFrmEmp.Rows.Count)
            Else
                If dgvFrmEmp.Rows.Count < dgvFrmEmp.PageSize Then
                    Me.ViewState("gv1_FirstRecordNo") = ((1 * dgvFrmEmp.Rows.Count) - dgvFrmEmp.Rows.Count)
                Else
                    Me.ViewState("gv1_FirstRecordNo") = ((1 * dgvFrmEmp.PageSize) - dgvFrmEmp.Rows.Count)
                End If
            End If
            Me.ViewState("gv1_LastRecordNo") = ((dgvFrmEmp.PageIndex + 1) * dgvFrmEmp.Rows.Count)

            FillOldApproverEmployeeList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    'Protected Sub dgvFrmEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvFrmEmp.RowDataBound
    '    Try
    '        If e.Row.Cells.Count > 1 Then
    '            If e.Row.RowIndex > -1 Then
    '                If dtFromEmp IsNot Nothing AndAlso dtFromEmp.Rows.Count > 0 Then
    '                    'Dim dRow() As DataRow = CType(Me.ViewState("dtFromEmp"), DataTable).Select("employeecode = '" & e.Row.Cells(1).Text & "'")
    '                    Dim dRow() As DataRow = dtFromEmp.Select("employeecode = '" & e.Row.Cells(1).Text & "'")
    '                    If dRow.Length > 0 Then
    '                        If CBool(dRow(0).Item("select")) = True Then
    '                            Dim gvRow As GridViewRow = e.Row
    '                            CType(gvRow.FindControl("chkbox1"), CheckBox).Checked = CBool(dRow(0).Item("select"))
    '                        End If
    '                    End If
    '                End If

    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    Protected Sub dgvToEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgvToEmp.PageIndexChanging
        Try
            dgvToEmp.PageIndex = e.NewPageIndex

            If dgvToEmp.PageIndex > 0 Then
                Me.ViewState("gv2_FirstRecordNo") = (((dgvToEmp.PageIndex + 1) * dgvToEmp.Rows.Count) - dgvToEmp.Rows.Count)
            Else
                If dgvToEmp.Rows.Count < dgvToEmp.PageSize Then
                    Me.ViewState("gv2_FirstRecordNo") = ((1 * dgvToEmp.Rows.Count) - dgvToEmp.Rows.Count)
                Else
                    Me.ViewState("gv2_FirstRecordNo") = ((1 * dgvToEmp.PageSize) - dgvToEmp.Rows.Count)
                End If
            End If
            Me.ViewState("gv2_LastRecordNo") = ((dgvToEmp.PageIndex + 1) * dgvToEmp.Rows.Count)

            Call FillMigratedEmployee()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub dgvToEmp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgvToEmp.RowDataBound
        Try
            If e.Row.Cells.Count > 1 Then
                If e.Row.Cells(1).Text = "None" Then
                    e.Row.Visible = False
                End If
                If e.Row.RowIndex > -1 Then
                    If Me.ViewState("dtMigratedEmp") IsNot Nothing Then
                        Dim dRow() As DataRow = CType(Me.ViewState("dtMigratedEmp"), DataTable).Select("employeecode = '" & e.Row.Cells(1).Text & "' AND employeecode <> 'None'")
                        If dRow.Length > 0 Then
                            If CBool(dRow(0).Item("select")) = True Then
                                Dim gvRow As GridViewRow = e.Row
                                CType(gvRow.FindControl("chkbox2"), CheckBox).Checked = CBool(dRow(0).Item("select"))
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgvAssignedEmp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgvAssignedEmp.PageIndexChanging
        Try
            dgvAssignedEmp.PageIndex = e.NewPageIndex
            FillNewApproverAssignEmployeeList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

#End Region

#Region " Textbox Events "

    'Pinkal (14-May-2020) -- Start
    'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.

    'Protected Sub txtFrmSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFrmSearch.TextChanged
    '    Try
    '        'Nilay (21-Apr-2015) -- Start
    '        'Enhancement- ADD Allocation Link and Reset Link
    '        'Dim dView As DataView = CType(Me.ViewState("dtFromEmp"), DataTable).DefaultView
    '        dvFromEmp = CType(Me.ViewState("dtFromEmp"), DataTable).DefaultView
    '        'Nilay (21-Apr-2015) -- End

    '        Dim StrSearch As String = String.Empty
    '        If txtFrmSearch.Text.Trim.Length > 0 Then
    '            StrSearch = "employeecode LIKE '%" & txtFrmSearch.Text & "%' OR employeename LIKE '%" & txtFrmSearch.Text & "%'"
    '            dvFromEmp.RowFilter = StrSearch 'Nilay (21-Apr-2015) -- dView.RowFilter = StrSearch
    '        End If

    '        'Nilay (21-Apr-2015) -- Start
    '        'Enhancement- ADD Allocation Link and Reset Link
    '        If mstrAdvanceFilter.Trim.Length > 0 Then
    '            dvFromEmp.RowFilter &= IIf(dvFromEmp.RowFilter.Trim.Length > 0, "AND ", "").ToString() & mstrAdvanceFilter
    '        End If
    '        'Nilay (21-Apr-2015) -- End
    '        dgvFrmEmp.AutoGenerateColumns = False
    '        dgvFrmEmp.DataSource = dvFromEmp 'Nilay (21-Apr-2015) -- dgvFrmEmp.DataSource = dView
    '        dgvFrmEmp.DataBind()
    '        txtFrmSearch.Focus()
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("Procedure txtFrmSearch_TextChanged : " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub txtToSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtToSearch.TextChanged
    '    Try
    '        Dim dView As DataView = Nothing
    '        Dim StrSearch As String = String.Empty
    '        If mltiview.ActiveViewIndex = 0 Then
    '            dView = CType(Me.ViewState("dtMigratedEmp"), DataTable).DefaultView
    '        ElseIf mltiview.ActiveViewIndex = 1 Then
    '            dView = CType(Me.ViewState("dtAssignedEmp"), DataTable).DefaultView
    '        End If

    '        If txtToSearch.Text.Trim.Length > 0 Then
    '            StrSearch = "employeecode LIKE '%" & txtToSearch.Text & "%' OR employeename LIKE '%" & txtToSearch.Text & "%'"
    '        End If

    '        dView.RowFilter = StrSearch

    '        If mltiview.ActiveViewIndex = 0 Then
    '            dgvToEmp.AutoGenerateColumns = False
    '            dgvToEmp.DataSource = dView
    '            dgvToEmp.DataBind()
    '        ElseIf mltiview.ActiveViewIndex = 1 Then
    '            dgvAssignedEmp.AutoGenerateColumns = False
    '            dgvAssignedEmp.DataSource = dView
    '            dgvAssignedEmp.DataBind()
    '        End If
    '        txtToSearch.Focus()
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("Procedure txtToSearch_TextChanged : " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Pinkal (14-May-2020) -- End

#End Region

#Region " CheckBox Events "

    'Pinkal (14-May-2020) -- Start
    'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.

    'Protected Sub chkHeder1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If dgvFrmEmp.Rows.Count <= 0 Then Exit Sub
    '        Dim dvEmployee As DataView = CType(Me.ViewState("dtFromEmp"), DataTable).DefaultView

    '        If Me.ViewState("EmpAdvanceSearch") IsNot Nothing Then
    '            dvEmployee.RowFilter = CType(Me.ViewState("EmpAdvanceSearch"), String)
    '        Else
    '            dvEmployee.RowFilter = ""
    '        End If

    '        'Nilay (23-Aug-2016) -- Start
    '        'ENHANCEMENT : Search option on payslip in MSS
    '        'dvEmployee.RowFilter = "employeecode LIKE '%" & txtFrmSearch.Text & "%' OR employeename LIKE '%" & txtFrmSearch.Text & "%'"



    '        'If txtFrmSearch.Text.Trim.Length > 0 Then dvEmployee.RowFilter = "employeecode LIKE '%" & txtFrmSearch.Text & "%' OR employeename LIKE '%" & txtFrmSearch.Text & "%'"

    '        'Nilay (23-Aug-2016) -- End

    '        For i As Integer = 0 To dvEmployee.ToTable.Rows.Count - 1
    '            Dim gvRow As GridViewRow = dgvFrmEmp.Rows(i)
    '            CType(gvRow.FindControl("chkbox1"), CheckBox).Checked = cb.Checked
    '            Dim dRow() As DataRow = CType(Me.ViewState("dtFromEmp"), DataTable).Select("employeecode = '" & gvRow.Cells(1).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("select") = cb.Checked
    '            End If
    '            dvEmployee.Table.AcceptChanges()
    '        Next
    '        Me.ViewState("dtFromEmp") = dvEmployee.Table
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkHeder1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkbox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
    '        If gvr.Cells.Count > 0 Then
    '            Dim dRow() As DataRow = CType(Me.ViewState("dtFromEmp"), DataTable).Select("employeecode = '" & gvr.Cells(1).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("select") = cb.Checked
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkbox1_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkHeder2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If dgvToEmp.Rows.Count <= 0 Then Exit Sub

    '        'SHANI [01 FEB 2015]-START
    '        'Enhancement - REDESIGN SELF SERVICE.
    '        'Nilay (23-Aug-2016) -- Start
    '        'ENHANCEMENT : Search option on payslip in MSS
    '        'Dim dvEmployee As DataView = CType(Me.ViewState("dtMigratedEmp"), DataTable).DefaultView
    '        'For i As Integer = 0 To dvEmployee.ToTable.Rows.Count - 1
    '        '    Dim gvRow As GridViewRow = dgvToEmp.Rows(i) 'SHANI [01 FEB 2015]-dgvToEmp.Rows(j)
    '        '    CType(gvRow.FindControl("chkbox2"), CheckBox).Checked = cb.Checked
    '        '    Dim dRow() As DataRow = CType(Me.ViewState("dtMigratedEmp"), DataTable).Select("employeecode = '" & gvRow.Cells(1).Text & "'")
    '        '    If dRow.Length > 0 Then
    '        '        dRow(0).Item("select") = cb.Checked
    '        '    End If
    '        '    dvEmployee.Table.AcceptChanges()
    '        'Next
    '        Dim dtMigratedEmp As DataTable = CType(Me.ViewState("dtMigratedEmp"), DataTable)
    '        For i As Integer = 0 To dgvToEmp.Rows.Count - 1
    '            Dim gvRow As GridViewRow = dgvToEmp.Rows(i) 'SHANI [01 FEB 2015]-dgvToEmp.Rows(j)
    '            CType(gvRow.FindControl("chkbox2"), CheckBox).Checked = cb.Checked
    '            Dim dRow() As DataRow = dtMigratedEmp.Select("employeecode = '" & gvRow.Cells(1).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("select") = cb.Checked
    '            End If
    '        Next
    '        dtMigratedEmp.AcceptChanges()
    '        Me.ViewState("dtMigratedEmp") = dtMigratedEmp
    '        'Nilay (23-Aug-2016)

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkHeder2_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub chkbox2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvr As GridViewRow = DirectCast(cb.NamingContainer, GridViewRow)
    '        If gvr.Cells.Count > 0 Then
    '            Dim dRow() As DataRow = CType(Me.ViewState("dtMigratedEmp"), DataTable).Select("employeecode = '" & gvr.Cells(1).Text & "'")
    '            If dRow.Length > 0 Then
    '                dRow(0).Item("select") = cb.Checked
    '            End If
    '        End If
    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkbox2_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub


    Protected Sub chkShowInactiveApprovers_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowInactiveApprovers.CheckedChanged
        Try
            chkShowInActiveEmployees.Enabled = True
            chkShowInActiveEmployees.Checked = False
            FillCombo()
            'Pinkal (28-May-2020) -- Start
            'BUG NMB:  problem solved on TnA OT Requisition whenever employee apply for OT on holiday. 
            cboFrmLevel_SelectedIndexChanged(cboFrmLevel, New EventArgs())
            cboToLevel_SelectedIndexChanged(cboToLevel, New EventArgs())
            'Pinkal (28-May-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkShowInActiveEmployees_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowInActiveEmployees.CheckedChanged
        Try
            cboFrmLevel_SelectedIndexChanged(cboFrmLevel, New EventArgs())
            cboToLevel_SelectedIndexChanged(cboToLevel, New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (14-May-2020) -- End


#End Region

    'Nilay (21-Apr-2015) -- Start
    'Enhancement- ADD Allocation Link and Reset Link
#Region "LinkButton's Events"

    Protected Sub lnkAllocation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAllocation.Click
        Try
            If CInt(cboFrmApprover.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("From Leave Approver is compulsory information.Please select From Approver.", Me)
                Exit Sub
            End If
            If CInt(cboFrmLevel.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Level is compulsory information.Please select Level.", Me)
                Exit Sub
            End If


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            If dgvFrmEmp.HeaderRow Is Nothing Then Exit Sub
            'Pinkal (06-Jan-2016) -- End

            CType(dgvFrmEmp.HeaderRow.FindControl("chkHeder1"), CheckBox).Checked = False

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim dtTable As DataTable = CType(Me.ViewState("dtFromEmp"), DataTable)
            Dim dtTable As DataTable = CType(Me.ViewState("dtFromEmp"), DataTable).Copy
            'Pinkal (11-Sep-2020) -- End


            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then

                For i As Integer = 0 To dtTable.Rows.Count - 1
                    dtTable.Rows(i)("select") = False
                Next
                dtTable.AcceptChanges()
                Me.ViewState("dtFromEmp") = dtTable
            End If

            popupAdvanceFilter.Show()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkAllocation_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            cboFrmApprover.Focus()
        End Try
    End Sub

    Protected Sub lnkReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReset.Click
        Try

            'Pinkal (14-May-2020) -- Start
            'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            ' txtFrmSearch.Text = ""
            'Pinkal (14-May-2020) -- End


            mstrAdvanceFilter = ""
            Me.ViewState("EmpAdvanceSearch") = Nothing
            If ViewState("dtFromEmp") IsNot Nothing Then
                Call FillOldApproverEmployeeList()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkReset_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region
    'Nilay (21-Apr-2015) -- End

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)

        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
        'SHANI [01 FEB 2015]--END

            Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "gbInfo", Me.lblCaption.Text)
            Me.lblFromApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "lblCaption1", Me.lblFromApprover.Text)
            Me.lblToApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "lblNewApprover", Me.lblToApprover.Text)
            Me.lblFrmLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblFrmLevel.ID, Me.lblFrmLevel.Text)
            Me.lblToLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "lblNewLevel", Me.lblToLevel.Text)

            Me.dgvFrmEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvFrmEmp.Columns(1).FooterText, Me.dgvFrmEmp.Columns(1).HeaderText)
            Me.dgvFrmEmp.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvFrmEmp.Columns(2).FooterText, Me.dgvFrmEmp.Columns(2).HeaderText)

            Me.lblCaption1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "tbpMigratedEmployee", Me.lblCaption1.Text)
            Me.dgvToEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvToEmp.Columns(1).FooterText, Me.dgvToEmp.Columns(1).HeaderText)
            Me.dgvToEmp.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvToEmp.Columns(2).FooterText, Me.dgvToEmp.Columns(2).HeaderText)

            Me.lblCaption2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "tbpAssignEmployee", Me.lblCaption2.Text)
            Me.dgvAssignedEmp.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvAssignedEmp.Columns(0).FooterText, Me.dgvAssignedEmp.Columns(0).HeaderText)
            Me.dgvAssignedEmp.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgvAssignedEmp.Columns(1).FooterText, Me.dgvAssignedEmp.Columns(1).HeaderText)

            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")


        'Pinkal (14-May-2020) -- Start
        'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
            Me.chkShowInactiveApprovers.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowInactiveApprovers.ID, Me.chkShowInactiveApprovers.Text)
            Me.chkShowInActiveEmployees.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowInActiveEmployees.ID, Me.chkShowInActiveEmployees.Text)
        'Pinkal (14-May-2020) -- End



        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

End Class