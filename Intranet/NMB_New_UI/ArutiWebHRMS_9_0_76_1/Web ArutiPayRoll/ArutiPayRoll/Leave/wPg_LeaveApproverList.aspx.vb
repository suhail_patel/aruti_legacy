﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.clsDataOperation
Imports System.Web.UI.WebControls.DataGridColumn
Imports System.Drawing
Imports Aruti.Data

#End Region

Partial Class Leave_wPg_LeaveApproverList
    Inherits Basepage

#Region " Private Variables "

    Private DisplayMessage As New CommonCodes

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objLeaveApprover As New clsleaveapprover_master
    'Pinkal (11-Sep-2020) -- End

    'Pinkal (06-May-2014) -- Start
    'Enhancement : TRA Changes
    Private ReadOnly mstrModuleName As String = "frmLeaveApproversList"
    Private ReadOnly mstrModuleName1 As String = "frmApprover_UserMapping"
    Private ReadOnly mstrModuleName2 As String = "frmApprover_leavetypeMapping"
    'Pinkal (06-May-2014) -- End

#End Region

#Region " Page Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT
            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (27 Apr 2013) -- End

            If IsPostBack = False Then

                'Pinkal (06-May-2014) -- Start
                'Enhancement : Language Changes 
                SetLanguage()
                'Pinkal (06-May-2014) -- End

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (11-Sep-2020) -- End

                btnNew.Visible = CBool(Session("AddLeaveApprover"))
                FillCombo()

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                If Session("LeaveApprover_Level") IsNot Nothing Then
                    drpLevel.SelectedValue = Session("LeaveApprover_Level").ToString()
                    Session.Remove("LeaveApprover_Level")
                    Call BtnSearch_Click(btnSearch, Nothing)
                End If
                'SHANI [09 Mar 2015]--END 

                'Shani(17-Aug-2015) -- Start
                'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            Else
                If Session("dtTable") IsNot Nothing Then
                    dgView.DataSource = CType(Session("dtTable"), DataTable)
                    dgView.DataBind()
                End If
                'Shani(17-Aug-2015) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Try
            Me.IsLoginRequired = True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            Dim objMaster As New clsMasterData
            Dim dsCombo As DataSet = objMaster.getComboListTranHeadActiveInActive("List", False)
            With cboStatus
                .DataTextField = "Name"
                .DataValueField = "Id"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
                .SelectedValue = "1"
            End With

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objMaster = Nothing
            'Pinkal (11-Sep-2020) -- End

            Call cboStatus_SelectedIndexChanged(cboStatus, Nothing)
            'Shani(17-Aug-2015) -- End

            Dim objLevel As New clsapproverlevel_master
            Dim dsList As DataSet = objLevel.getListForCombo("Level", True)
            drpLevel.DataTextField = "name"
            drpLevel.DataValueField = "levelunkid"
            drpLevel.DataSource = dsList.Tables(0)
            drpLevel.DataBind()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsCombo = Nothing
            objLevel = Nothing
            'Pinkal (11-Sep-2020) -- End

            drpLevel_SelectedIndexChanged(New Object(), New EventArgs())
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub FillList()
        Dim dsApproverList As New DataSet
        Dim dtApprover As DataTable = Nothing
        Dim strSearching As String = ""

        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeaveApprover As New clsleaveapprover_master
        'Pinkal (11-Sep-2020) -- End

        Try

            If CBool(Session("AllowToViewLeaveApproverList")) = True Then

                If CInt(drpLevel.SelectedValue) > 0 Then
                    strSearching &= "AND lvleaveapprover_master.levelunkid = " & CInt(drpLevel.SelectedValue) & " "
                End If

                If CInt(drpApprover.SelectedValue) > 0 Then
                    strSearching &= "AND lvleaveapprover_master.leaveapproverunkid = " & CInt(drpApprover.SelectedValue) & " "
                End If

                dsApproverList = objLeaveApprover.GetList("List", _
                                                       Session("Database_Name").ToString(), _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       Session("EmployeeAsOnDate").ToString(), _
                                                       Session("UserAccessModeSetting").ToString(), True, _
                                                      True, _
                                                       CBool(IIf(CInt(cboStatus.SelectedValue) = 1, True, False)), False, -1, Nothing, strSearching, "")

                dtApprover = New DataView(dsApproverList.Tables("List"), "", "levelname", DataViewRowState.CurrentRows).ToTable()

                Dim strLeaveName As String = ""
                Dim dtTable As DataTable = dtApprover.Clone
                dtTable.Columns.Add("IsGrp", Type.GetType("System.String"))
                Dim dtRow As DataRow = Nothing
                For Each drow As DataRow In dtApprover.Rows
                    If CStr(drow("levelname")).Trim <> strLeaveName.Trim Then
                        dtRow = dtTable.NewRow
                        dtRow("IsGrp") = True
                        dtRow("approverunkid") = drow("approverunkid")
                        dtRow("levelname") = drow("levelname")
                        strLeaveName = drow("levelname").ToString()
                        dtTable.Rows.Add(dtRow)
                    End If
                    dtRow = dtTable.NewRow
                    For Each dtcol As DataColumn In dtApprover.Columns
                        dtRow(dtcol.ColumnName) = drow(dtcol.ColumnName)
                    Next
                    dtRow("IsGrp") = False
                    dtTable.Rows.Add(dtRow)
                Next

                If CInt(cboStatus.SelectedValue) = 1 Then
                    dgView.Columns(0).Visible = CBool(Session("EditLeaveApprover"))
                    dgView.Columns(1).Visible = CBool(Session("DeleteLeaveApprover"))
                    dgView.Columns(2).Visible = False
                    dgView.Columns(3).Visible = CBool(Session("LeaveApproverForLeaveType"))
                    dgView.Columns(3).Visible = CBool(Session("AllowtoMapLeaveType"))
                Else
                    dgView.Columns(0).Visible = False
                    dgView.Columns(1).Visible = False
                    dgView.Columns(2).Visible = False
                    dgView.Columns(3).Visible = False
                    dgView.Columns(3).Visible = False
                End If

                dtTable.AcceptChanges()
                dgView.DataSource = dtTable
                dgView.DataKeyField = "approverunkid"
                dgView.DataBind()


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                Session("dtTable") = dtTable.Copy()

                If dtTable IsNot Nothing Then dtTable.Clear()
                dtTable = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                dgView.CurrentPageIndex = 0
                dgView.DataBind()
            Else
                Throw ex
                DisplayMessage.DisplayError(ex, Me)
            End If
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtApprover IsNot Nothing Then dtApprover.Clear()
            dtApprover = Nothing
            If dsApproverList IsNot Nothing Then dsApproverList.Clear()
            dsApproverList = Nothing
            objLeaveApprover = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try

    End Sub

#End Region

#Region "Dropdown Event"

    Private Sub drpLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles drpLevel.SelectedIndexChanged
        Try
            Dim objApprover As New clsleaveapprover_master
            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            'Dim dsList As DataSet = objApprover.GetList("List", True)
            Dim dsList As DataSet = Nothing

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If CInt(cboStatus.SelectedValue) = 1 Then
            '    dsList = objApprover.GetList("List", True)
            'ElseIf CInt(cboStatus.SelectedValue) = 2 Then
            '    dsList = objApprover.GetList("List", False)
            'End If


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            'If CInt(cboStatus.SelectedValue) = 1 Then
            '    dsList = objApprover.GetList("List", _
            '                                 Session("Database_Name"), _
            '                                 Session("UserId"), _
            '                                 Session("Fin_year"), _
            '                                 Session("CompanyUnkId"), _
            '                                 Session("EmployeeAsOnDate"), _
            '                                 Session("UserAccessModeSetting"), True, _
            '                                 Session("IsIncludeInactiveEmp"), True)
            'ElseIf CInt(cboStatus.SelectedValue) = 2 Then
            '    dsList = objApprover.GetList("List", _
            '                                 Session("Database_Name"), _
            '                                 Session("UserId"), _
            '                                 Session("Fin_year"), _
            '                                 Session("CompanyUnkId"), _
            '                                 Session("EmployeeAsOnDate"), _
            '                                 Session("UserAccessModeSetting"), True, _
            '                                 Session("IsIncludeInactiveEmp"), False)
            'End If

            If CInt(cboStatus.SelectedValue) = 1 Then

                dsList = objApprover.GetList("List", _
                                             Session("Database_Name").ToString(), _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             Session("EmployeeAsOnDate").ToString(), _
                                             Session("UserAccessModeSetting").ToString(), True, _
                                             True, True)

            ElseIf CInt(cboStatus.SelectedValue) = 2 Then

                dsList = objApprover.GetList("List", _
                                             Session("Database_Name").ToString(), _
                                             CInt(Session("UserId")), _
                                             CInt(Session("Fin_year")), _
                                             CInt(Session("CompanyUnkId")), _
                                             Session("EmployeeAsOnDate").ToString(), _
                                             Session("UserAccessModeSetting").ToString(), True, _
                                             True, False)
            End If

            'Pinkal (06-Jan-2016) -- End

            'Shani(20-Nov-2015) -- End

            If dsList Is Nothing Then Exit Sub
            'Shani(17-Aug-2015) -- End

            Dim dtTable As DataTable = Nothing
            If CInt(drpLevel.SelectedValue) > 0 Then
                dtTable = New DataView(dsList.Tables(0), "levelunkid = " & CInt(drpLevel.SelectedValue), "", DataViewRowState.CurrentRows).ToTable
            Else
                'S.SANDEEP [30 JAN 2016] -- START
                'dtTable = dsList.Tables(0).DefaultView.ToTable(True, "leaveapproverunkid", "name")
                dtTable = dsList.Tables(0).DefaultView.ToTable(True, "leaveapproverunkid", "name", "isexternalapprover")
                'S.SANDEEP [30 JAN 2016] -- END
            End If

            Dim dr As DataRow = dtTable.NewRow
            dr("leaveapproverunkid") = 0
            dr("name") = "Select"
            'S.SANDEEP [30 JAN 2016] -- START
            dr("isexternalapprover") = False
            'S.SANDEEP [30 JAN 2016] -- END
            dtTable.Rows.InsertAt(dr, 0)

            drpApprover.DataTextField = "name"
            drpApprover.DataValueField = "leaveapproverunkid"
            drpApprover.DataSource = dtTable
            drpApprover.DataBind()


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objApprover = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboLevel_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Shani(17-Aug-2015) -- Start
    'Leave Enhancement : Putting Activate Feature in Leave Approver Master
    Protected Sub cboStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboStatus.SelectedIndexChanged
        Try
            If CInt(cboStatus.SelectedValue) = 1 Then
                dgView.Columns(4).Visible = False
                dgView.Columns(5).Visible = CBool(Session("AllowSetleaveInactiveApprover"))
            ElseIf CInt(cboStatus.SelectedValue) = 2 Then
                dgView.Columns(4).Visible = CBool(Session("AllowSetleaveActiveApprover"))
                dgView.Columns(5).Visible = False
            Else
                dgView.Columns(4).Visible = False
                dgView.Columns(5).Visible = False
            End If
            If IsPostBack Then

                'Shani(17-Aug-2015) -- Start
                'Leave Enhancement : Putting Activate Feature in Leave Approver Master
                Call drpLevel_SelectedIndexChanged(drpLevel, Nothing)
                'Shani(17-Aug-2015) -- End/
                Call FillList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Shani(17-Aug-2015) -- End

#End Region

#Region " GridView's Event(s) "

    Protected Sub dgView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgView.PageIndexChanged
        Try
            dgView.CurrentPageIndex = e.NewPageIndex
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgView.ItemDataBound
        Try

            If e.Item.ItemType = ListItemType.Header Then
                e.Item.Cells(6).Text = e.Item.Cells(7).Text.Trim
                e.Item.Cells(6).ColumnSpan = 2
                e.Item.Cells(7).Visible = False
                'Pinkal (16-May-2020) -- Start
                'Enhancement NMB Leave Report Changes -   Working on Leave Reports required by NMB.
                'Language.setLanguage(mstrModuleName)
                'Pinkal (16-May-2020) -- End
            End If
            If (e.Item.ItemIndex >= 0) Then
                If CBool(e.Item.Cells(12).Text) = True Then
                    CType(e.Item.Cells(0).FindControl("ImgSelect"), LinkButton).Visible = False
                    CType(e.Item.Cells(1).FindControl("ImgDelete"), LinkButton).Visible = False
                    CType(e.Item.Cells(2).FindControl("lnkMapUser"), LinkButton).Visible = False
                    CType(e.Item.Cells(3).FindControl("lnkMapLeaveType"), LinkButton).Visible = False
                    CType(e.Item.Cells(4).FindControl("lnkActiveApprover"), LinkButton).Visible = False
                    CType(e.Item.Cells(5).FindControl("lnkInactiveApprover"), LinkButton).Visible = False
                    If CInt(cboStatus.SelectedValue) = 1 Then
                        If CBool(Session("EditLeaveApprover")) Then
                            e.Item.Cells(0).Text = e.Item.Cells(6).Text
                            e.Item.Cells(0).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(0).Style.Add("text-align", "left")

                            'Gajanan [17-Sep-2020] -- Start
                            'New UI Change
                            'e.Item.Cells(0).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(0).CssClass = "group-header"
                            'Gajanan [17-Sep-2020] -- End

                            e.Item.Cells(1).Visible = False
                            e.Item.Cells(2).Visible = False
                            e.Item.Cells(3).Visible = False
                            e.Item.Cells(4).Visible = False
                            e.Item.Cells(5).Visible = False
                            e.Item.Cells(6).Visible = False
                        ElseIf CBool(Session("DeleteLeaveApprover")) Then
                            e.Item.Cells(1).Text = e.Item.Cells(6).Text
                            e.Item.Cells(1).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(1).Style.Add("text-align", "left")

                            'Gajanan [17-Sep-2020] -- Start
                            'New UI Change
                            'e.Item.Cells(1).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(1).CssClass = "group-header"
                            'Gajanan [17-Sep-2020] -- End

                            e.Item.Cells(2).Visible = False
                            e.Item.Cells(3).Visible = False
                            e.Item.Cells(4).Visible = False
                            e.Item.Cells(5).Visible = False
                            e.Item.Cells(6).Visible = False
                        ElseIf CBool(Session("AllowMapApproverWithUser")) Then
                            e.Item.Cells(2).Text = e.Item.Cells(6).Text
                            e.Item.Cells(2).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(2).Style.Add("text-align", "left")

                            'Gajanan [17-Sep-2020] -- Start
                            'New UI Change
                            'e.Item.Cells(2).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(2).CssClass = "group-header"
                            'Gajanan [17-Sep-2020] -- End

                            e.Item.Cells(3).Visible = False
                            e.Item.Cells(4).Visible = False
                            e.Item.Cells(5).Visible = False
                            e.Item.Cells(6).Visible = False
                        ElseIf CBool(Session("LeaveApproverForLeaveType")) = True AndAlso CBool(Session("AllowtoMapLeaveType")) = True Then
                            e.Item.Cells(3).Text = e.Item.Cells(6).Text
                            e.Item.Cells(3).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(3).Style.Add("text-align", "left")

                            'Gajanan [17-Sep-2020] -- Start
                            'New UI Change
                            'e.Item.Cells(3).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(3).CssClass = "group-header"
                            'Gajanan [17-Sep-2020] -- End

                            e.Item.Cells(4).Visible = False
                            e.Item.Cells(5).Visible = False
                            e.Item.Cells(6).Visible = False
                        ElseIf CBool(Session("AllowSetleaveInactiveApprover")) Then
                            e.Item.Cells(5).Text = e.Item.Cells(6).Text
                            e.Item.Cells(5).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(5).Style.Add("text-align", "left")

                            'Gajanan [17-Sep-2020] -- Start
                            'New UI Change
                            'e.Item.Cells(5).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(5).CssClass = "group-header"
                            'Gajanan [17-Sep-2020] -- End

                            e.Item.Cells(6).Visible = False
                        Else
                            e.Item.Cells(6).Text = e.Item.Cells(6).Text
                            e.Item.Cells(6).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(6).Style.Add("text-align", "left")

                            'Gajanan [17-Sep-2020] -- Start
                            'New UI Change
                            'e.Item.Cells(6).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(6).CssClass = "group-header"
                            'Gajanan [17-Sep-2020] -- End

                        End If
                    Else
                        If CBool(Session("AllowSetleaveActiveApprover")) Then
                            e.Item.Cells(4).Text = e.Item.Cells(6).Text
                            e.Item.Cells(4).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(4).Style.Add("text-align", "left")

                            'Gajanan [17-Sep-2020] -- Start
                            'New UI Change
                            'e.Item.Cells(4).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(4).CssClass = "group-header"
                            'Gajanan [17-Sep-2020] -- End

                            e.Item.Cells(5).Visible = False
                            e.Item.Cells(6).Visible = False
                        Else
                            e.Item.Cells(6).Text = e.Item.Cells(6).Text
                            e.Item.Cells(6).ColumnSpan = e.Item.Cells.Count - 1
                            e.Item.Cells(6).Style.Add("text-align", "left")

                            'Gajanan [17-Sep-2020] -- Start
                            'New UI Change
                            'e.Item.Cells(6).CssClass = "GroupHeaderStyleBorderLeft"
                            e.Item.Cells(6).CssClass = "group-header"
                            'Gajanan [17-Sep-2020] -- End

                        End If
                    End If

                    For i = 7 To e.Item.Cells.Count - 1
                        e.Item.Cells(i).Visible = False
                    Next
                Else
                    If e.Item.HasControls Then
                        'Pinkal (28-Apr-2020) -- Start
                        'Optimization  - Working on Process Optimization and performance for require module.	
                        ' 'Language.setLanguage(mstrModuleName)
                        'Pinkal (28-Apr-2020) -- End
                        Dim mnuMapUser As LinkButton = DirectCast(e.Item.Cells(1).FindControl("lnkMapUser"), LinkButton)

                        'Gajanan [17-Sep-2020] -- Start
                        'New UI Change
                        'mnuMapUser.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"mnuMapUser", mnuMapUser.Text).Replace("&", "")
                        'mnuMapUser.ToolTip = mnuMapUser.Text
                        mnuMapUser.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "mnuMapUser", mnuMapUser.ToolTip).Replace("&", "")
                        'Gajanan [17-Sep-2020] -- End

                        Dim mnuLeaveType As LinkButton = DirectCast(e.Item.Cells(2).FindControl("lnkMapLeaveType"), LinkButton)

                        'Gajanan [17-Sep-2020] -- Start
                        'New UI Change
                        'mnuLeaveType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"mnuLeaveType", mnuLeaveType.Text).Replace("&", "")
                        'mnuLeaveType.ToolTip = mnuLeaveType.Text
                        mnuLeaveType.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "mnuLeaveType", mnuLeaveType.ToolTip).Replace("&", "")
                        'Gajanan [17-Sep-2020] -- End

                        e.Item.Cells(6).Attributes.Add("Level", e.Item.Cells(6).Text.Trim)
                        e.Item.Cells(6).Text = e.Item.Cells(7).Text.Trim

                        e.Item.Cells(6).ColumnSpan = 2
                        e.Item.Cells(7).Visible = False
                    End If
                End If
            End If

            'Shani(17-Aug-2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.ItemCommand
        Dim ds As DataSet = Nothing
        Try
            If e.CommandName = "Edit" Then

                'SHANI [09 Mar 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                Session("LeaveApprover_Level") = drpLevel.SelectedValue
                'SHANI [09 Mar 2015]--END 

                Session.Add("ApproverId", dgView.DataKeys(e.Item.ItemIndex))

                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.
                'Response.Redirect(Session("servername") & "~/Leave/wPg_AddEditLeaveApprover.aspx", False)
                Response.Redirect("~/Leave/wPg_AddEditLeaveApprover.aspx", False)
                'Pinkal (06-Jan-2016) -- End


            ElseIf e.CommandName = "MapUser" Then  'FOR USER MAPPING

                'Shani(17-Aug-2015) -- Start
                'Leave Enhancement : Putting Activate Feature in Leave Approver Master
                'LblApproverVal.Text = e.Item.Cells(5).Text.Trim
                'LblLevelVal.Text = e.Item.Cells(4).Text.Trim
                LblApproverVal.Text = e.Item.Cells(6).Text.Trim
                LblLevelVal.Text = e.Item.Cells(6).Attributes("Level").Trim
                'Shani(17-Aug-2015) -- End
                FillUser()
                Dim objUserMapping As New clsapprover_Usermapping
                Dim blnflag As Boolean = objUserMapping.isExist(enUserType.Approver, CInt(dgView.DataKeys(e.Item.ItemIndex)))
                Me.ViewState.Add("ApproverId", dgView.DataKeys(e.Item.ItemIndex))

                If blnflag Then

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'Dim dsFill As DataSet = objUserMapping.GetList("List )
                    Dim dsFill As DataSet = objUserMapping.GetList("List", CInt(enUserType.Approver))
                    'Pinkal (11-Sep-2020) -- End

                    Dim dtFill As DataTable = New DataView(dsFill.Tables("List"), "approverunkid=" & CInt(dgView.DataKeys(e.Item.ItemIndex)) & " AND usertypeid =  " & enUserType.Approver, "", DataViewRowState.CurrentRows).ToTable
                    If dtFill.Rows.Count > 0 Then
                        Me.ViewState.Add("mappingunkid", CInt(dtFill.Rows(0)("mappingunkid")))

                        'Pinkal (09-Jan-2013) -- Start
                        'Enhancement : TRA Changes

                        Dim dtUser As DataTable = CType(drpUser.DataSource, DataTable)
                        Dim drRow() As DataRow = dtUser.Select("userunkid = " & CInt(dtFill.Rows(0)("userunkid")))
                        If drRow.Length > 0 Then
                            drpUser.SelectedValue = dtFill.Rows(0)("userunkid").ToString()
                        End If
                        'Pinkal (09-Jan-2013) -- End

                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        If dtUser IsNot Nothing Then dtUser.Clear()
                        dtUser = Nothing
                        'Pinkal (11-Sep-2020) -- End

                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsFill IsNot Nothing Then dsFill.Clear()
                    dsFill = Nothing
                    'Pinkal (11-Sep-2020) -- End

                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objUserMapping = Nothing
                'Pinkal (11-Sep-2020) -- End


                popupUserMapping.Show()

            ElseIf e.CommandName = "MapLeaveType" Then  'FOR LEAVE TYPE MAPPING
                Me.ViewState.Add("FirstRecordNo", 0)
                Me.ViewState.Add("LastRecordNo", GvLeaveTypeMapping.PageSize)
                Me.ViewState.Add("ApproverId", dgView.DataKeys(e.Item.ItemIndex))

                'Shani(17-Aug-2015) -- Start
                'Leave Enhancement : Putting Activate Feature in Leave Approver Master
                'LblApproverLeaveMapVal.Text = e.Item.Cells(5).Text.Trim
                'LblLevelLeaveMapVal.Text = e.Item.Cells(4).Text.Trim
                'GvLeaveTypeMapping.PageIndex = 0
                LblApproverLeaveMapVal.Text = e.Item.Cells(6).Text.Trim
                LblLevelLeaveMapVal.Text = e.Item.Cells(6).Attributes("Level").Trim
                'Shani(17-Aug-2015) -- End
                FillLeaveType()
                popupLeaveMapping.Show()
                'Shani(17-Aug-2015) -- Start
                'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            ElseIf e.CommandName.ToUpper = "INACTIVE" Then
                Me.ViewState("LeaveApproverUnkId") = dgView.DataKeys(e.Item.ItemIndex)
                popupInactive.Title = "Aruti"
                popupInactive.Message = "Are you sure you want to set selected approver as Inactive ?"
                popupInactive.Show()
            ElseIf e.CommandName.ToUpper = "ACTIVE" Then
                Me.ViewState("LeaveApproverUnkId") = dgView.DataKeys(e.Item.ItemIndex)
                popupActive.Title = "Aruti"
                popupActive.Message = "Are you sure you want to set selected approver as Active ?"
                popupActive.Show()
                'Shani(17-Aug-2015) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.DeleteCommand
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeaveApprover As New clsleaveapprover_master
        'Pinkal (11-Sep-2020) -- End
        Try
            If objLeaveApprover.isUsed(CInt(dgView.DataKeys(e.Item.ItemIndex))) Then
                DisplayMessage.DisplayMessage("Sorry, You cannot delete this Approver. Reason: This Approver is in use.", Me) '?2
                Exit Sub
            Else
                Me.ViewState.Add("ApproverId", CInt(dgView.DataKeys(e.Item.ItemIndex)))
                popup1.Show()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveApprover = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub BtnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            'SHANI [09 Mar 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            If CInt(drpLevel.SelectedValue) > 0 Then
                Session("LeaveApprover_Level") = drpLevel.SelectedValue
            End If
            'SHANI [09 Mar 2015]--END 

            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.
            'Response.Redirect(Session("servername") & "~/Leave/wPg_AddEditLeaveApprover.aspx", False)
            Response.Redirect("~/Leave/wPg_AddEditLeaveApprover.aspx", False)
            'Pinkal (06-Jan-2016) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BtnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            drpApprover.SelectedIndex = 0
            drpLevel.SelectedIndex = 0
            FillList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDelete.Click
    Protected Sub BtnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup1.buttonDelReasonYes_Click
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeaveApprover As New clsleaveapprover_master
        'Pinkal (11-Sep-2020) -- End
        Try
            If (popup1.Reason.Trim = "") Then 'SHANI [01 FEB 2015]-If (txtreasondel.Text.Trim = "") Then
                DisplayMessage.DisplayMessage("Please enter delete reason.", Me)
                Exit Sub
            End If


            'Pinkal (06-Apr-2013) -- Start
            'Enhancement : TRA Changes
            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmLeaveApproversList"
            StrModuleName2 = "mnuLeaveInformation"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'Pinkal (06-Apr-2013) -- End



            objLeaveApprover._Userumkid = CInt(Session("UserId"))

            With objLeaveApprover
                ._Isvoid = True
                ._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                ._Voidreason = popup1.Reason.Trim 'SHANI [01 FEB 2015]--txtreasondel.Text.Trim
                ._Voiduserunkid = CInt(Session("UserId"))


                'Pinkal (12-Oct-2020) -- Start
                'Enhancement SportPesa Tz -   Don't Display Inactive Employee on Leave Screens.
                '.Delete(CInt(Me.ViewState("ApproverId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date)
                .Delete(CInt(Me.ViewState("ApproverId")), eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, CStr(Session("Database_Name")))
                'Pinkal (12-Oct-2020) -- End


                If (._Message.Length > 0) Then
                    DisplayMessage.DisplayMessage("You Cannot Delete this Entry." & ._Message, Me)
                Else
                    DisplayMessage.DisplayMessage("Entry Successfully deleted." & ._Message, Me)
                    Me.ViewState("ApproverId") = Nothing
                    drpLevel_SelectedIndexChanged(sender, e)
                End If
                popup1.Dispose()

                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'txtreasondel.Text = ""
                'SHANI [01 FEB 2015]--END

                FillList()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            objLeaveApprover = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    'Shani [ 24 DEC 2014 ] -- START
    'Implement Close Button Code on Each Page.

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Try
    '        Response.Redirect(Session("servername") & "~\UserHome.aspx")
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END

    'Shani [ 24 DEC 2014 ] -- END

    'Shani(17-Aug-2015) -- Start
    'Leave Enhancement : Putting Activate Feature in Leave Approver Master

    Protected Sub popupActive_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupActive.buttonYes_Click
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeaveApprover As New clsleaveapprover_master
        'Pinkal (11-Sep-2020) -- End
        Try
            If Me.ViewState("LeaveApproverUnkId") IsNot Nothing Then
                If objLeaveApprover.InActiveApprover(CInt(Me.ViewState("LeaveApproverUnkId")), CInt(Session("CompanyUnkId")), True) = False Then
                    DisplayMessage.DisplayMessage(objLeaveApprover._Message, Me)
                    Exit Sub
                Else
                    Call drpLevel_SelectedIndexChanged(drpLevel, Nothing)
                    Call FillList()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            objLeaveApprover = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub popupInactive_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupInactive.buttonYes_Click
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objLeaveApprover As New clsleaveapprover_master
        'Pinkal (11-Sep-2020) -- End
        Try
            If Me.ViewState("LeaveApproverUnkId") IsNot Nothing Then
                Dim objLeavForm As New clsleaveform
                If objLeavForm.GetApproverPendingLeaveFormCount(CInt(Me.ViewState("LeaveApproverUnkId")), "") > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "You cannot inactive this approver.Reason :This Approver has Pending Leave Application Form(s)."), Me)
                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objLeavForm = Nothing
                    'Pinkal (11-Sep-2020) -- End
                    Exit Sub
                End If
                objLeavForm = Nothing

                If objLeaveApprover.InActiveApprover(CInt(Me.ViewState("LeaveApproverUnkId")), CInt(Session("CompanyUnkId")), False) = False Then
                    DisplayMessage.DisplayMessage(objLeaveApprover._Message, Me)
                    Exit Sub
                Else
                    Call drpLevel_SelectedIndexChanged(drpLevel, Nothing)
                    Call FillList()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Finally
            objLeaveApprover = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub popupInactive_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupInactive.buttonNo_Click
        Try
            If Me.ViewState("LeaveApproverUnkId") IsNot Nothing Then
                Me.ViewState("LeaveApproverUnkId") = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Shani(17-Aug-2015) -- End

#End Region

#Region "User Mapping"

#Region "Private Methods"

    Private Sub FillUser()
        Dim mblnAdUser As Boolean = False
        Try

            'Pinkal (09-Jan-2013) -- Start
            'Enhancement : TRA Changes

            'Dim objUserMapping As New clsapprover_Usermapping
            'Dim dsFill As DataSet = objUserMapping.GetUserWithAppoverPrivilage("User", True)
            'drpUser.DataTextField = "username"
            'drpUser.DataValueField = "userunkid"
            'drpUser.DataSource = dsFill.Tables("User")
            'drpUser.DataBind()

            Dim objUser As New clsUserAddEdit
            Dim objOption As New clsPassowdOptions
            Dim drOption As DataRow() = objOption._DataTable.Select("passwdoptionid = " & enPasswordOption.USER_LOGIN_MODE)

            If drOption.Length > 0 Then
                If objOption._UserLogingModeId <> enAuthenticationMode.BASIC_AUTHENTICATION Then mblnAdUser = True
            End If

            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            'Dim dsFill As DataSet = objUser.getComboList("User", True, mblnAdUser, objOption._IsEmployeeAsUser, CInt(Session("CompanyUnkId")), 264, CInt(Session("Fin_year")))
            Dim dsFill As DataSet = objUser.getNewComboList("User", 0, True, CInt(Session("CompanyUnkId")), CStr(264), CInt(Session("Fin_year")))
            'Pinkal (01-Mar-2016) -- End


            drpUser.DataTextField = "name"
            drpUser.DataValueField = "userunkid"
            drpUser.DataSource = dsFill.Tables("User")
            drpUser.DataBind()

            objOption = Nothing
            objUser = Nothing
            'Pinkal (09-Jan-2013) -- End


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsFill IsNot Nothing Then dsFill.Clear()
            dsFill = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillUser :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Protected Sub btnMapSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMapSave.Click
        Try
            Dim blnFlag As Boolean = False
            If CInt(drpUser.SelectedValue) = 0 Then
                DisplayMessage.DisplayMessage("User is compulsory information.Please Select User.", Me)
                drpUser.Focus()
                Exit Sub
            End If
            Dim objUserMapping As New clsapprover_Usermapping


            'Pinkal (06-Apr-2013) -- Start
            'Enhancement : TRA Changes
            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmLeaveApproversList"
            StrModuleName2 = "mnuLeaveInformation"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'Pinkal (06-Apr-2013) -- End


            If CInt(Me.ViewState("mappingunkid")) > 0 Then
                objUserMapping._Mappingunkid = CInt(Me.ViewState("mappingunkid"))
                objUserMapping._Approverunkid = CInt(Me.ViewState("ApproverId"))
                objUserMapping._Userunkid = CInt(drpUser.SelectedValue)
                objUserMapping._UserTypeid = enUserType.Approver

                'Pinkal (06-Apr-2013) -- Start
                'Enhancement : TRA Changes
                objUserMapping._AuditUserunkid = CInt(Session("UserId"))
                'Pinkal (06-Apr-2013) -- End

                blnFlag = objUserMapping.Update()
            Else
                objUserMapping._Approverunkid = CInt(Me.ViewState("ApproverId"))
                objUserMapping._Userunkid = CInt(drpUser.SelectedValue)
                objUserMapping._UserTypeid = enUserType.Approver
                'Pinkal (06-Apr-2013) -- Start
                'Enhancement : TRA Changes
                objUserMapping._AuditUserunkid = CInt(Session("UserId"))
                'Pinkal (06-Apr-2013) -- End
                blnFlag = objUserMapping.Insert()
            End If

            If blnFlag = False And objUserMapping._Message <> "" Then
                DisplayMessage.DisplayMessage("btnMapSave_Click:- " & objUserMapping._Message, Me)
            Else
                Me.ViewState("mappingunkid") = Nothing
                Me.ViewState("ApproverId") = Nothing
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objUserMapping = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnMapDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMapDelete.Click
        Try
            If CInt(drpUser.SelectedValue) < 1 Then
                DisplayMessage.DisplayMessage("Please select User from the list to perform further operation on it.", Me)
                drpUser.Focus()
                Exit Sub
            End If

            'Pinkal (06-Apr-2013) -- Start
            'Enhancement : TRA Changes
            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmLeaveApproversList"
            StrModuleName2 = "mnuLeaveInformation"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
            'Pinkal (06-Apr-2013) -- End

            'If eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Are you sure you want to delete this User Mapping?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
            Dim objUserMapping As New clsapprover_Usermapping
            'Pinkal (06-Apr-2013) -- Start
            'Enhancement : TRA Changes
            objUserMapping._AuditUserunkid = CInt(Session("UserId"))
            'Pinkal (06-Apr-2013) -- End
            objUserMapping.Delete(CInt(Me.ViewState("mappingunkid")))
            drpUser.SelectedIndex = 0
            Me.ViewState("mappingunkid") = Nothing
            'End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objUserMapping = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "Leave Type Mapping"

#Region "Private Methods"

    Private Sub FillLeaveType()
        Dim dsFill As DataSet = Nothing
        Try
            Dim objLeaveTypeMapping As New clsapprover_leavetype_mapping
            Dim objLeaveType As New clsleavetype_master

            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            'dsFill = objLeaveType.GetList("List", True, True)
            dsFill = objLeaveType.GetList("List", True, True, "ISNULL(skipapproverflow,0) = 0")
            'Pinkal (01-Oct-2018) -- End

            For Each dr As DataRow In dsFill.Tables(0).Rows
                Dim drRow As DataRow = objLeaveTypeMapping._dtLeaveType.NewRow()
                drRow("leavetypemappingunkid") = objLeaveTypeMapping.GetLeaveTypeMappingUnkId(CInt(Me.ViewState("ApproverId")), CInt(dr("leavetypeunkid")))
                drRow("ischecked") = objLeaveTypeMapping.isExist(CInt(Me.ViewState("ApproverId")), CInt(dr("leavetypeunkid")))
                drRow("approverunkid") = CInt(Me.ViewState("ApproverId"))
                drRow("leavetypeunkid") = dr("leavetypeunkid").ToString()
                drRow("leavetypecode") = dr("leavetypecode").ToString()
                drRow("leavename") = dr("leavename").ToString()
                drRow("ispaid") = dr("ispaid").ToString()
                objLeaveTypeMapping._dtLeaveType.Rows.Add(drRow)
            Next

            Me.ViewState.Add("LeaveTypeMapping", objLeaveTypeMapping._dtLeaveType)

            GvLeaveTypeMapping.DataSource = objLeaveTypeMapping._dtLeaveType
            GvLeaveTypeMapping.DataBind()

            'Shani(17-Aug-2015) -- Start
            'Leave Enhancement : Putting Activate Feature in Leave Approver Master
            SetCheckBox()
            'Shani(17-Aug-2015) -- End


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveType = Nothing
            objLeaveTypeMapping = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillLeaveType:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsFill IsNot Nothing Then dsFill.Clear()
            dsFill = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Private Sub GetPageRecordNo()
        Try
            If GvLeaveTypeMapping.PageIndex > 0 Then
                Me.ViewState("FirstRecordNo") = (((GvLeaveTypeMapping.PageIndex + 1) * GvLeaveTypeMapping.Rows.Count) - GvLeaveTypeMapping.Rows.Count)
            Else
                Me.ViewState("FirstRecordNo") = ((1 * GvLeaveTypeMapping.PageSize) - GvLeaveTypeMapping.Rows.Count)
            End If
            Me.ViewState("LastRecordNo") = ((GvLeaveTypeMapping.PageIndex + 1) * GvLeaveTypeMapping.Rows.Count)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetPageRecordNo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Private Sub SetCheckBox()
        Try


            'Pinkal (16-Feb-2016) -- Start
            'Enhancement - Testing on External Approver in Leave Module for Self Service in 57.2.
            '  GetPageRecordNo()
            'If GvLeaveTypeMapping.Rows.Count <= 0 Then Exit Sub
            'Dim dtLeaveTypeMapping As DataTable = CType(Me.ViewState("LeaveTypeMapping"), DataTable)

            'If Me.ViewState("FirstRecordNo") IsNot Nothing AndAlso Me.ViewState("LastRecordNo") IsNot Nothing Then
            '    Dim mintFirstRecord As Integer = Me.ViewState("FirstRecordNo")
            '    Dim mintLastRecord As Integer = Me.ViewState("LastRecordNo")
            '    Dim mintrowindex As Integer = 0
            'For i As Integer = mintFirstRecord To mintLastRecord - 1
            '    If dtLeaveTypeMapping.Rows.Count < i Then Exit For
            '    Dim drRow As DataRow() = dtLeaveTypeMapping.Select("leavetypecode = '" & GvLeaveTypeMapping.Rows(mintrowindex).Cells(1).Text.Trim & "' AND ischecked = true")
            '    If drRow.Length > 0 Then
            '        Dim gvRow As GridViewRow = GvLeaveTypeMapping.Rows(mintrowindex)
            '        CType(gvRow.FindControl("ChkgvSelect"), CheckBox).Checked = True
            '    End If
            '    mintrowindex += 1
            'Next
            'End If

            If GvLeaveTypeMapping.Rows.Count <= 0 Then Exit Sub
            Dim dtLeaveTypeMapping As DataTable = CType(Me.ViewState("LeaveTypeMapping"), DataTable)
            Dim mintrowindex As Integer = 0
            For i As Integer = 0 To dtLeaveTypeMapping.Rows.Count - 1
                If dtLeaveTypeMapping.Rows.Count < i Then Exit For
                Dim drRow As DataRow() = dtLeaveTypeMapping.Select("leavetypecode = '" & GvLeaveTypeMapping.Rows(mintrowindex).Cells(1).Text.Trim & "' AND ischecked = true")
                If drRow.Length > 0 Then
                    Dim gvRow As GridViewRow = GvLeaveTypeMapping.Rows(mintrowindex)
                    CType(gvRow.FindControl("ChkgvSelect"), CheckBox).Checked = True
                End If
                mintrowindex += 1
            Next
            'Pinkal (16-Feb-2016) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetCheckBox :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    'Pinkal (28-Apr-2020) -- Start
    'Optimization  - Working on Process Optimization and performance for require module.	

    'Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        If GvLeaveTypeMapping.Rows.Count <= 0 Then Exit Sub
    '        Dim dtLeaveTypeMapping As DataTable = CType(Me.ViewState("LeaveTypeMapping"), DataTable)


    '        'SHANI [01 FEB 2015]-START
    '        'Enhancement - REDESIGN SELF SERVICE.

    '        'GetPageRecordNo()
    '        'If Me.ViewState("FirstRecordNo") IsNot Nothing AndAlso Me.ViewState("LastRecordNo") IsNot Nothing Then
    '        'Dim mintFirstRecord As Integer = Me.ViewState("FirstRecordNo")
    '        'Dim mintLastRecord As Integer = Me.ViewState("LastRecordNo")
    '        'Dim mintrowindex As Integer = 0

    '        If GvLeaveTypeMapping.Rows.Count > 0 Then

    '            'Pinkal (30-Nov-2013) -- Start
    '            'Enhancement : Oman Changes
    '            'If mintFirstRecord = 0 Then mintLastRecord = mintLastRecord - 1
    '            'Pinkal (30-Nov-2013) -- End


    '            'Pinkal (30-Apr-2013) -- Start
    '            'Enhancement : TRA Changes
    '            'For i As Integer = mintFirstRecord To mintLastRecord - 1
    '            'For i As Integer = mintFirstRecord To mintLastRecord - 1
    '            For i As Integer = 0 To GvLeaveTypeMapping.Rows.Count - 1
    '                'If dtLeaveTypeMapping.Rows.Count - 1 < i Then Exit For
    '                If dtLeaveTypeMapping.Rows.Count < i Then Exit For

    '                Dim drRow As DataRow() = dtLeaveTypeMapping.Select("leavetypecode = '" & GvLeaveTypeMapping.Rows(i).Cells(1).Text.Trim & "'") 'SHANI [01 FEB 2015]-START GvLeaveTypeMapping.Rows(mintrowindex).Cells(1).Text.Trim 
    '                If drRow.Length > 0 Then
    '                    drRow(0)("ischecked") = cb.Checked
    '                    Dim gvRow As GridViewRow = GvLeaveTypeMapping.Rows(i) 'SHANI [01 FEB 2015]-START GvLeaveTypeMapping.Rows(mintrowindex)
    '                    CType(gvRow.FindControl("ChkgvSelect"), CheckBox).Checked = cb.Checked
    '                End If
    '                dtLeaveTypeMapping.AcceptChanges()
    '                'mintrowindex += 1 'SHANI [01 FEB 2015]-START
    '            Next
    '            'SHANI [01 FEB 2015]--END

    '            'Pinkal (30-Apr-2013) -- End

    '            Me.ViewState("LeaveTypeMapping") = dtLeaveTypeMapping
    '            popupLeaveMapping.Show()
    '        End If

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("chkAll_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Protected Sub ChkgvSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim cb As CheckBox = CType(sender, CheckBox)
    '        Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
    '        If gvRow.Cells.Count > 0 Then
    '            Dim drRow() As DataRow = CType(Me.ViewState("LeaveTypeMapping"), DataTable).Select("leavetypecode = '" & gvRow.Cells(1).Text & "'")
    '            If drRow.Length > 0 Then
    '                drRow(0)("ischecked") = cb.Checked
    '                drRow(0).AcceptChanges()
    '            End If
    '            popupLeaveMapping.Show()
    '        End If

    '    Catch ex As Exception
    '        'Sohail (23 Mar 2019) -- Start
    '        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
    '        'DisplayMessage.DisplayMessage("ChkgvSelect_CheckedChanged :- " & ex.Message, Me)
    '        DisplayMessage.DisplayError(ex, Me)
    '        'Sohail (23 Mar 2019) -- End
    '    End Try
    'End Sub

    'Pinkal (28-Apr-2020) -- End

#End Region

#Region " GridView's Event(s) "

    Protected Sub GvLeaveTypeMapping_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GvLeaveTypeMapping.PageIndexChanging
        Try
            GvLeaveTypeMapping.PageIndex = e.NewPageIndex
            GvLeaveTypeMapping.DataSource = CType(Me.ViewState("LeaveTypeMapping"), DataTable)
            GvLeaveTypeMapping.DataBind()
            SetCheckBox()
            popupLeaveMapping.Show()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GvLeaveTypeMapping_PageIndexChanging :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Button's Event(s)"

    Protected Sub btnLeaveMapSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeaveMapSave.Click
        Try
            Dim dtLeaveTypeMapping As DataTable = CType(Me.ViewState("LeaveTypeMapping"), DataTable)


            'Pinkal (28-Apr-2020) -- Start
            'Optimization  - Working on Process Optimization and performance for require module.	


            'Dim drRow As DataRow() = dtLeaveTypeMapping.Select("ischecked = true")
            'If drRow.Length <= 0 Then
            '    DisplayMessage.DisplayMessage("Leave Type is compulsory information.Please Select one Leave Type.", Me)
            '    popupLeaveMapping.Show()
            '    Exit Sub
            'End If

            Dim gchekcedRow As IEnumerable(Of GridViewRow) = Nothing
            gchekcedRow = GvLeaveTypeMapping.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = True)

            If gchekcedRow Is Nothing OrElse gchekcedRow.Count <= 0 Then
                DisplayMessage.DisplayMessage("Leave Type is compulsory information.Please Select one Leave Type.", Me)
                popupLeaveMapping.Show()
                Exit Sub
            End If

            For i As Integer = 0 To gchekcedRow.Count - 1
                Dim drRow() As DataRow = dtLeaveTypeMapping.Select("leavetypeunkid = " & CInt(GvLeaveTypeMapping.DataKeys(gchekcedRow(i).DataItemIndex)("leavetypeunkid")))
                If drRow.Length > 0 Then
                    drRow(0)("ischecked") = True
                    drRow(0).AcceptChanges()
                End If
            Next

            Dim gunchekcedRow As IEnumerable(Of GridViewRow) = GvLeaveTypeMapping.Rows.Cast(Of GridViewRow).Where(Function(x) CType(x.FindControl("ChkgvSelect"), CheckBox).Checked = False)
            If gunchekcedRow IsNot Nothing AndAlso gunchekcedRow.Count > 0 Then
                For i As Integer = 0 To gunchekcedRow.Count - 1
                    Dim drRow() As DataRow = dtLeaveTypeMapping.Select("leavetypeunkid = " & CInt(GvLeaveTypeMapping.DataKeys(gunchekcedRow(i).DataItemIndex)("leavetypeunkid")))
                    If drRow.Length > 0 Then
                        drRow(0)("ischecked") = False
                        drRow(0).AcceptChanges()
                    End If
                Next
            End If

            'Pinkal (28-Apr-2020) -- End



            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmLeaveApproversList"
            StrModuleName2 = "mnuLeaveInformation"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()

            Dim objLeaveTypeMapping As New clsapprover_leavetype_mapping
            objLeaveTypeMapping._Approverunkid = CInt(Me.ViewState("ApproverId"))
            objLeaveTypeMapping._Userunkid = CInt(Session("UserId"))
            objLeaveTypeMapping._dtLeaveType = dtLeaveTypeMapping

            Dim blnFlag As Boolean = objLeaveTypeMapping.Insert()

            If blnFlag = False And objLeaveTypeMapping._Message <> "" Then
                'Pinkal (28-Apr-2020) -- Start
                'Optimization  - Working on Process Optimization and performance for require module.	
                'DisplayMessage.DisplayMessage("btnLeaveMapSave_Click:- " & objLeaveTypeMapping._Message, Me)
                DisplayMessage.DisplayMessage(objLeaveTypeMapping._Message, Me)
                'Pinkal (28-Apr-2020) -- End
            Else
                Me.ViewState("ApproverId") = Nothing
                Me.ViewState("FirstRecordNo") = Nothing
                Me.ViewState("LastRecordNo") = Nothing
                Me.ViewState("LeaveTypeMapping") = Nothing
            End If


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveTypeMapping = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnLeaveMapSave_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region


#End Region


    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            Me.lblCaption.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblCaption.Text)

            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblLevel.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblLevel.ID, Me.lblLevel.Text)

            Me.dgView.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(4).FooterText, Me.dgView.Columns(4).HeaderText)
            Me.dgView.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(5).FooterText, Me.dgView.Columns(5).HeaderText)
            Me.dgView.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(6).FooterText, Me.dgView.Columns(6).HeaderText)
            Me.dgView.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(7).FooterText, Me.dgView.Columns(7).HeaderText)
            Me.dgView.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(8).FooterText, Me.dgView.Columns(8).HeaderText)
            Me.dgView.Columns(9).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(9).FooterText, Me.dgView.Columns(9).HeaderText)
            Me.dgView.Columns(10).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.dgView.Columns(10).FooterText, Me.dgView.Columns(10).HeaderText)

            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnNew.ID, Me.btnNew.Text).Replace("&", "")

            'Language.setLanguage(mstrModuleName1)
            Me.LblUserMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, Me.LblUserMapping.Text)
            Me.LblApproverMap.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "lblApprover", Me.LblApproverMap.Text)
            Me.LblLevelMap.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "lblApproverLevel", Me.LblLevelMap.Text)
            Me.LblUser.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "lblUser", Me.LblUser.Text)

            Me.btnMapSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "btnSave", Me.btnMapSave.Text).Replace("&", "")
            Me.btnMapDelete.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "btnDelete", Me.btnMapDelete.Text).Replace("&", "")
            Me.btnMapClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "btnClose", Me.btnMapClose.Text).Replace("&", "")

            'Language.setLanguage(mstrModuleName2)
            Me.LblLeaveMapping.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), mstrModuleName2, Me.LblLeaveMapping.Text)
            Me.LblApproverLeaveMap.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), "lblApprover", Me.LblApproverLeaveMap.Text)
            Me.LblLevelLeaveMap.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), "lblApproverLevel", Me.LblLevelLeaveMap.Text)

            Me.GvLeaveTypeMapping.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveTypeMapping.Columns(1).FooterText, Me.GvLeaveTypeMapping.Columns(1).HeaderText)
            Me.GvLeaveTypeMapping.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveTypeMapping.Columns(2).FooterText, Me.GvLeaveTypeMapping.Columns(2).HeaderText)
            Me.GvLeaveTypeMapping.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), Me.GvLeaveTypeMapping.Columns(3).FooterText, Me.GvLeaveTypeMapping.Columns(3).HeaderText)

            Me.btnLeaveMapSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), "btnSave", Me.btnLeaveMapSave.Text).Replace("&", "")
            Me.btnLeaveMapClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName2, CInt(HttpContext.Current.Session("LangId")), "btnClose", Me.btnLeaveMapClose.Text).Replace("&", "")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

End Class
