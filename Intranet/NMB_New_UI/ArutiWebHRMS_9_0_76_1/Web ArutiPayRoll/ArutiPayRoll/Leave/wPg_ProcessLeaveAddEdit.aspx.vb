﻿Option Strict On

Imports System.Data
Imports System.Data.SqlClient
Imports eZeeCommonLib.eZeeDataType
Imports Aruti.Data.clsEmployee_Master
Imports eZeeCommonLib.clsDataOperation
Imports System.IO
Imports System.Configuration
Imports Aruti.Data
Imports System.Net.Dns
Imports System.Globalization
Imports System.Threading


Partial Class Leave_wPg_ProcessLeaveAddEdit
    Inherits Basepage

#Region "Private Variable(S)"
    Private DisplayMessage As New CommonCodes
    Private Shared ReadOnly mstrModuleNameClaim As String = "frmClaims_RequestAddEdit"
    Private Shared ReadOnly mstrModuleName As String = "frmProcessLeave"
    Private ReadOnly mstrModuleName1 As String = "frmDependentsList"
    Private ReadOnly mstrModuleName2 As String = "frmLeaveForm_AddEdit"

    Private objCONN As SqlConnection

    'Pinkal (11-Sep-2020) -- Start
    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
    'Private objPendingLeave As New clspendingleave_Tran
    'Pinkal (11-Sep-2020) -- End


    Private mintLeaveFormUnkId As Integer = -1
    Private mintEmpUnkId As Integer = -1
    Private mintApprverId As Integer = -1
    Private mintPenddingLvTranUnkId As Integer = -1
    Private mintPriority As Integer = 0
    Private mintApproverTranUnkId As Integer = 0
    Private mintFreactionApproverId As Integer = -1
    Private mdtFraction As DataTable = Nothing

    Private mintLeaveTypeId As Integer = -1
    Private mdtExpense As DataTable = Nothing
    Private mdtFinalClaimTransaction As DataTable = Nothing
    Private mintClaimMstUnkId As Integer = -1
    Private mblnClaimRequest As Boolean = False

    Private mintEditClaimTranId As Integer = -1
    Private mstrEditGUID As String = ""
    Private mstrFilePath As String = ""
    Private mstrArutiSelfServiceURL As String = ""
    Private mblnIsLeaveEncashment As Boolean = False


    'Pinkal (01-Mar-2016) -- Start
    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
    Private mblnIsExternalApprover As Boolean = False
    'Pinkal (01-Mar-2016) -- End


    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.
    Dim objClaimPendingMst As clsclaim_request_master
    Private mblnApprovalClaimRequest As Boolean = False
    Private mdtLeaveExpenseTran As DataTable = Nothing
    Private mdtClaimAttchment As DataTable = Nothing
    Private mdtFinalClaimAttchment As DataTable = Nothing
    Private blnShowAttchmentPopup As Boolean = False
    Private mintDeleteIndex As Integer = 0
    Private mdtChildClaimAttachment As DataTable = Nothing
    Private mstrFolderName As String = ""
    'Pinkal (10-Jan-2017) -- End

    'Pinkal (29-Sep-2017) -- Start
    'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
    Private intEligibleExpenseDays As Integer = 0
    Private NoofDays As Decimal = 0
    'Pinkal (29-Sep-2017) -- End

    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mblnIsLvTypePaid As Boolean = False
    Private mintLvformMinDays As Integer = 0
    Private mintLvformMaxDays As Integer = 0
    'Pinkal (08-Oct-2018) -- End



    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mblnIsApprovalExpenseEditClick As Boolean = False
    Private mblnIsExpenseEditClick As Boolean = False
    Private mblnIsExpenseApproveClick As Boolean = False
    'Pinkal (22-Oct-2018) -- End


    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Private mintEmpMaxCountDependentsForCR As Integer = 0
    'Pinkal (25-Oct-2018) -- End

    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
    Private mblnConsiderLeaveOnTnAPeriod As Boolean = False
    'Pinkal (01-Jan-2019) -- End

    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mblnIsHRExpenseClaimRequest As Boolean = False
    Dim mblnIsClaimFormBudgetMandatoryClaimRequest As Boolean = False
    Dim mblnIsHRExpenseApprovalClaimRequest As Boolean = False
    Dim mblnIsClaimFormBudgetMandatoryApprovalClaimRequest As Boolean = False
    Dim mintClaimBaseCountryId As Integer = 0
    Dim mintApprovalBaseCountryId As Integer = 0
    'Pinkal (04-Feb-2019) -- End


    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mblnIsBlockLeave As Boolean = False
    Dim mblnIsStopApplyAcrossFYELC As Boolean = False
    Dim mblnIsShortLeave As Boolean = False
    Dim mintDeductedLeaveTypeID As Integer = 0
    Dim mintLvFormLvTypeID As Integer = 0
    'Pinkal (26-Feb-2019) -- End

    'Pinkal (07-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Dim mintClaimRequestApprovalExpTranId As Integer = 0
    Dim mstrClaimRequestApprovalExpGUID As String = ""
    'Pinkal (07-Mar-2019) -- End


    'Pinkal (03-May-2019) -- Start
    'Enhancement - Working on Leave UAT Changes for NMB.
    Private mblnConsiderExpense As Boolean = True
    'Pinkal (03-May-2019) -- End


    'Pinkal (25-May-2019) -- Start
    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
    Private xStatusId As Integer = 2
    'Pinkal (25-May-2019) -- End


    'Pinkal (05-Jun-2020) -- Start
    'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
    Private mblnDoNotApplyForFutureDates As Boolean = False
    'Pinkal (05-Jun-2020) -- End

    'Pinkal (18-Mar-2021) -- Start
    'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
    Private mintExpenseIndex As Integer = -1
    'Pinkal (18-Mar-2021) -- End


    'Pinkal (08-Feb-2022) -- Start
    'Enhancement NMB  - Language Change in PM Module.	
    Dim objLeaveEmailList As New List(Of clsEmailCollection)
    'Pinkal (08-Feb-2022) -- End

#End Region

#Region "Page Event(S)"

    Protected Sub Page_Load1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try


            If IsPostBack = False Then

                If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) Then
                    Me.Title = ""
                    Me.lblPageHeader.Text = ""


                    'Pinkal (10-Jan-2017) -- Start
                    'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                    If Request.QueryString("uploadimage") Is Nothing Then
                        'Pinkal (01-Apr-2019) -- Start
                        'Enhancement - Working on Leave Changes for NMB.
                    
                    Call DirectApprovLinkPageLoad()
                        'If DirectApprovLinkPageLoad() = False Then Exit Sub
                        'Pinkal (01-Apr-2019) -- End

                    If Session("isUrlPost") Is Nothing Then
                        Session("isUrlPost") = True
                    End If
                    Else
                        mdtClaimAttchment = CType(Me.Session("mdtClaimAttchment"), DataTable)
                        mdtChildClaimAttachment = CType(Me.Session("mdtChildClaimAttachment"), DataTable)
                        mdtFinalClaimAttchment = CType(Me.Session("mdtFinalClaimAttchment"), DataTable)
                        blnShowAttchmentPopup = CBool(Me.ViewState("blnShowAttchmentPopup"))
                        mintDeleteIndex = CInt(Me.ViewState("mintDeleteIndex"))
                        If Request.QueryString("uploadimage") IsNot Nothing Then
                            If CBool(clsCrypto.Dicrypt(Request.QueryString("uploadimage"))) = True Then
                                Dim postedFile As HttpPostedFile = Context.Request.Files("myfile")
                                postedFile.SaveAs(Server.MapPath("~/images/" & postedFile.FileName))
                                Session.Add("Imagepath", Server.MapPath("~/images/" & postedFile.FileName))
                            End If
                        End If
                        Exit Sub
                    End If
                    'Pinkal (10-Jan-2017) -- End

                ElseIf Session("pendingleavetranunkid") IsNot Nothing AndAlso Session("formunkid") IsNot Nothing AndAlso _
                        Session("employeeunkid") IsNot Nothing AndAlso Session("Approvertranunkid") IsNot Nothing AndAlso _
                        Session("priority") IsNot Nothing AndAlso Session("approverEmpUnkid") IsNot Nothing AndAlso _
                        Session("IsExternalApprover") IsNot Nothing Then

                    mintPenddingLvTranUnkId = CInt(Session("pendingleavetranunkid"))
                    mintLeaveFormUnkId = CInt(Session("formunkid"))
                    mintEmpUnkId = CInt(Session("employeeunkid"))
                    mintApproverTranUnkId = CInt(Session("Approvertranunkid"))
                    mintPriority = CInt(Session("priority"))
                    mintApprverId = CInt(Session("approverEmpUnkid"))

                    'Pinkal (01-Mar-2016) -- Start
                    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                    mblnIsExternalApprover = CBool(Session("IsExternalApprover"))
                    'Pinkal (01-Mar-2016) -- End


                    If Session("isUrlPost") Is Nothing Then
                        Session("isUrlPost") = False
                    End If
                    Session("pendingleavetranunkid") = Nothing
                    Session("formunkid") = Nothing
                    Session("employeeunkid") = Nothing
                    Session("Approvertranunkid") = Nothing
                    Session("priority") = Nothing
                    Session("approverEmpUnkid") = Nothing


                    'Pinkal (01-Mar-2016) -- Start
                    'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                    Session("IsExternalApprover") = Nothing
                    'Pinkal (01-Mar-2016) -- End


                    cboAllowance.Enabled = CBool(Session("AddLeaveAllowance"))

                Else
                    Response.Redirect(Session("rootpath").ToString & "Leave/wPg_ProcessLeaveList.aspx", False)
                    Exit Sub
                End If


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                GC.Collect()
                'Pinkal (11-Sep-2020) -- End

                SetLanguage()

                Call FillCombo()
                Call GetData()
                Call GetEmployeeValue()

            Else

                mintLeaveFormUnkId = CInt(Me.ViewState("mintLeaveFormUnkId"))
                mintFreactionApproverId = CInt(Me.ViewState("mintFreactionApproverId"))
                mdtFraction = CType(Me.ViewState("mdtFraction"), DataTable)
                mintPenddingLvTranUnkId = CInt(Me.ViewState("mintPenddingLvTranUnkId"))
                mintClaimMstUnkId = CInt(Me.ViewState("mintClaimMstUnkId"))
                mblnClaimRequest = CBool(Me.ViewState("mblnClaimRequest"))
                mintEmpUnkId = CInt(Me.ViewState("mintEmpUnkId"))
                mdtExpense = CType(Me.ViewState("mdtExpense"), DataTable)
                mdtFinalClaimTransaction = CType(Me.ViewState("mdtFinalClaimTransaction"), DataTable)
                mintEditClaimTranId = CInt(Me.ViewState("mintEditClaimTranId"))
                mstrEditGUID = CStr(IIf(Me.ViewState("mstrEditGUID") Is Nothing, "", Me.ViewState("mstrEditGUID").ToString()))
                mintApproverTranUnkId = CInt(Me.ViewState("mintApproverTranUnkId"))
                mintApprverId = CInt(Me.ViewState("mintApprverId"))
                mstrArutiSelfServiceURL = Me.ViewState("ArutiSelfServiceURL").ToString
                mblnIsLeaveEncashment = CBool(Me.ViewState("mblnIsLeaveEncashment"))
                mintPriority = CInt(Me.ViewState("priority"))

                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                mblnIsExternalApprover = CBool(Me.ViewState("IsExternalApprover"))
                'Pinkal (01-Mar-2016) -- End


                'Pinkal (10-Jan-2017) -- Start
                'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                mblnApprovalClaimRequest = CBool(Me.ViewState("mblnApprovalClaimRequest"))
                mdtLeaveExpenseTran = CType(Me.ViewState("mdtLeaveExpenseTran"), DataTable)
                mdtClaimAttchment = CType(Me.Session("mdtClaimAttchment"), DataTable)
                mdtChildClaimAttachment = CType(Me.Session("mdtChildClaimAttachment"), DataTable)
                mdtFinalClaimAttchment = CType(Me.Session("mdtFinalClaimAttchment"), DataTable)
                blnShowAttchmentPopup = CBool(Me.ViewState("blnShowAttchmentPopup"))
                mintDeleteIndex = CInt(Me.ViewState("mintDeleteIndex"))
                objClaimPendingMst = CType(Me.Session("objClaimPendingMst"), clsclaim_request_master)
                'Pinkal (10-Jan-2017) -- End

                'Pinkal (29-Sep-2017) -- Start
                'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
                intEligibleExpenseDays = CInt(Me.ViewState("intEligibleExpenseDays"))
                NoofDays = CDec(Me.ViewState("NoofDays"))
                'Pinkal (29-Sep-2017) -- End

                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                mblnIsLvTypePaid = CBool(Me.ViewState("LvTypeIsPaid"))
                mintLvformMinDays = CInt(Me.ViewState("LVDaysMinLimit"))
                mintLvformMaxDays = CInt(Me.ViewState("LVDaysMaxLimit"))
                'Pinkal (08-Oct-2018) -- End


                'Pinkal (22-Oct-2018) -- Start
                'Enhancement - Implementing Claim & Request changes For NMB .
                mblnIsApprovalExpenseEditClick = CBool(Me.ViewState("mblnIsApprovalExpenseEditClick"))
                mblnIsExpenseEditClick = CBool(Me.ViewState("mblnIsExpenseEditClick"))
                mblnIsExpenseApproveClick = CBool(Me.ViewState("mblnIsExpenseApproveClick"))
                'Pinkal (22-Oct-2018) -- End

                'Pinkal (25-Oct-2018) -- Start
                'Enhancement - Implementing Claim & Request changes For NMB .
                mintEmpMaxCountDependentsForCR = CInt(Me.ViewState("EmpMaxCountDependentsForCR"))
                'Pinkal (25-Oct-2018) -- End

                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                mblnConsiderLeaveOnTnAPeriod = CBool(Me.ViewState("ConsiderLeaveOnTnAPeriod"))
                'Pinkal (01-Jan-2019) -- End

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                mblnIsClaimFormBudgetMandatoryClaimRequest = CBool(Me.ViewState("IsClaimFormBudgetMandatoryCR"))
                mblnIsHRExpenseClaimRequest = CBool(Me.ViewState("CRIsHRExpenseCR"))
                mblnIsClaimFormBudgetMandatoryApprovalClaimRequest = CBool(Me.ViewState("IsClaimFormBudgetMandatoryApprovalCR"))
                mblnIsHRExpenseApprovalClaimRequest = CBool(Me.ViewState("IsHRExpenseApprovalCR"))
                mintClaimBaseCountryId = CInt(Me.ViewState("ClaimBaseCountryId"))
                mintApprovalBaseCountryId = CInt(Me.ViewState("ApprovalBaseCountryId"))
                'Pinkal (04-Feb-2019) -- End


                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                mblnIsBlockLeave = CBool(Me.ViewState("IsBlockLeave"))
                mblnIsStopApplyAcrossFYELC = CBool(Me.ViewState("IsStopApplyAcrossFYELC"))
                mblnIsShortLeave = CBool(Me.ViewState("IsShortLeave "))
                mintDeductedLeaveTypeID = CInt(Me.ViewState("DeductedLeaveTypeID"))
                mintLvFormLvTypeID = CInt(Me.ViewState("LvFormLvTypeID"))
                'Pinkal (26-Feb-2019) -- End

                'Pinkal (07-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                mintClaimRequestApprovalExpTranId = CInt(Me.ViewState("ClaimRequestApprovalExpTranId"))
                mstrClaimRequestApprovalExpGUID = Me.ViewState("ClaimRequestApprovalExpGUID").ToString()
                'Pinkal (07-Mar-2019) -- End

                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                mblnConsiderExpense = CBool(Me.ViewState("ConsiderExpense"))
                'Pinkal (03-May-2019) -- End

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                xStatusId = CInt(Me.ViewState("StatusId"))
                'Pinkal (25-May-2019) -- End

                'Pinkal (05-Jun-2020) -- Start
                'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
                mblnDoNotApplyForFutureDates = CBool(Me.ViewState("DoNotApplyForFutureDates"))
                'Pinkal (05-Jun-2020) -- End


                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                mintExpenseIndex = CInt(Me.ViewState("mintExpenseIndex"))
                'Pinkal (18-Mar-2021) -- End

            End If

            If mblnClaimRequest Then
                popupClaimRequest.Show()
            End If

            'Pinkal (10-Jan-2017) -- Start
            'Enhancement - Working on TRA C&R Module Changes with Leave Module.
            If mblnApprovalClaimRequest Then
                popupExpense.Show()
            End If

            If blnShowAttchmentPopup Then
                popup_ScanAttchment.Show()
            End If

            'Pinkal (10-Jan-2017) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            If IsPostBack = False Then
                If Session("startdate") IsNot Nothing Then
                    dtpStartdate.SetDate = CDate(Session("startdate"))
                    Session("startdate") = Nothing
                End If
                If Session("enddate") IsNot Nothing Then
                    dtpEnddate.SetDate = CDate(Session("enddate"))
                    Session("enddate") = Nothing
                End If
                Call dtpStartdate_ValueChanged(dtpStartdate, Nothing)

                If Session("finalexpense") IsNot Nothing Then
                    mdtFinalClaimTransaction = CType(Session("finalexpense"), DataTable)
                    Session("finalexpense") = Nothing
                End If
                If Session("dayfreaction") IsNot Nothing Then
                    mdtFraction = CType(Session("dayfreaction"), DataTable)
                    Session("dayfreaction") = Nothing

                    If mdtFraction IsNot Nothing AndAlso mdtFraction.Rows.Count > 0 Then
                        objNoofDays.Text = "Total Days : " & CDec(mdtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'")).ToString("#0.00")
                        'Pinkal (29-Sep-2017) -- Start
                        'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
                        NoofDays = CDec(mdtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'"))
                        'Pinkal (29-Sep-2017) -- End
                    End If
                End If

            End If
            Me.ViewState("mintLeaveFormUnkId") = mintLeaveFormUnkId
            Me.ViewState("mintFreactionApproverId") = mintFreactionApproverId
            Me.ViewState("mdtFraction") = mdtFraction
            Me.ViewState("mintPenddingLvTranUnkId") = mintPenddingLvTranUnkId
            Me.ViewState("mblnClaimRequest") = mblnClaimRequest
            Me.ViewState("mintEmpUnkId") = mintEmpUnkId
            Me.ViewState("mdtExpense") = mdtExpense
            Me.ViewState("mdtFinalClaimTransaction") = mdtFinalClaimTransaction
            Me.ViewState("mintEditClaimTranId") = mintEditClaimTranId
            Me.ViewState("mstrEditGUID") = mstrEditGUID
            Me.ViewState("mintClaimMstUnkId") = mintClaimMstUnkId
            Me.ViewState("mintApproverTranUnkId") = mintApproverTranUnkId
            Me.ViewState("ArutiSelfServiceURL") = mstrArutiSelfServiceURL
            Me.ViewState("mblnIsLeaveEncashment") = mblnIsLeaveEncashment
            Me.ViewState("mintApprverId") = mintApprverId
            Me.ViewState("priority") = mintPriority

            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.
            Me.ViewState("IsExternalApprover") = mblnIsExternalApprover
            'Pinkal (01-Mar-2016) -- End


            'Pinkal (10-Jan-2017) -- Start
            'Enhancement - Working on TRA C&R Module Changes with Leave Module.
            Me.ViewState("mblnApprovalClaimRequest") = mblnApprovalClaimRequest
            Me.ViewState("mdtLeaveExpenseTran") = mdtLeaveExpenseTran
            Me.Session("mdtClaimAttchment") = mdtClaimAttchment
            Me.Session("mdtChildClaimAttachment") = mdtChildClaimAttachment
            Me.Session("mdtFinalClaimAttchment") = mdtFinalClaimAttchment
            Me.ViewState("blnShowAttchmentPopup") = blnShowAttchmentPopup
            Me.ViewState("mintDeleteIndex") = mintDeleteIndex
            Me.Session("objClaimPendingMst") = objClaimPendingMst
            'Pinkal (10-Jan-2017) -- End

            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
            Me.ViewState("intEligibleExpenseDays") = intEligibleExpenseDays
            Me.ViewState("NoofDays") = NoofDays
            'Pinkal (29-Sep-2017) -- End


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            Me.ViewState("LvTypeIsPaid") = mblnIsLvTypePaid
            Me.ViewState("LVDaysMinLimit") = mintLvformMinDays
            Me.ViewState("LVDaysMaxLimit") = mintLvformMaxDays
            'Pinkal (08-Oct-2018) -- End

            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            Me.ViewState("mblnIsApprovalExpenseEditClick") = mblnIsApprovalExpenseEditClick
            Me.ViewState("mblnIsExpenseEditClick") = mblnIsExpenseEditClick
            Me.ViewState("mblnIsExpenseApproveClick") = mblnIsExpenseApproveClick
            'Pinkal (22-Oct-2018) -- End

            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            Me.ViewState("EmpMaxCountDependentsForCR") = mintEmpMaxCountDependentsForCR
            'Pinkal (25-Oct-2018) -- End

            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            Me.ViewState("ConsiderLeaveOnTnAPeriod") = mblnConsiderLeaveOnTnAPeriod
            'Pinkal (01-Jan-2019) -- End

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Me.ViewState("IsClaimFormBudgetMandatoryCR") = mblnIsClaimFormBudgetMandatoryClaimRequest
            Me.ViewState("CRIsHRExpenseCR") = mblnIsHRExpenseClaimRequest
            Me.ViewState("IsClaimFormBudgetMandatoryApprovalCR") = mblnIsClaimFormBudgetMandatoryApprovalClaimRequest
            Me.ViewState("IsHRExpenseApprovalCR") = mblnIsHRExpenseApprovalClaimRequest
            Me.ViewState("ClaimBaseCountryId") = mintClaimBaseCountryId
            Me.ViewState("ApprovalBaseCountryId") = mintApprovalBaseCountryId
            'Pinkal (04-Feb-2019) -- End

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Me.ViewState("IsBlockLeave") = mblnIsBlockLeave
            Me.ViewState("IsStopApplyAcrossFYELC") = mblnIsStopApplyAcrossFYELC
            Me.ViewState("IsShortLeave ") = mblnIsShortLeave
            Me.ViewState("DeductedLeaveTypeID") = mintDeductedLeaveTypeID
            Me.ViewState("LvFormLvTypeID") = mintLvFormLvTypeID
            'Pinkal (26-Feb-2019) -- End

            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            Me.ViewState("ClaimRequestApprovalExpTranId") = mintClaimRequestApprovalExpTranId
            Me.ViewState("ClaimRequestApprovalExpGUID") = mstrClaimRequestApprovalExpGUID
            'Pinkal (07-Mar-2019) -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            Me.ViewState("ConsiderExpense") = mblnConsiderExpense
            'Pinkal (03-May-2019) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            Me.ViewState("StatusId") = xStatusId
            'Pinkal (25-May-2019) -- End


            'Pinkal (05-Jun-2020) -- Start
            'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
            Me.ViewState("DoNotApplyForFutureDates") = mblnDoNotApplyForFutureDates
            'Pinkal (05-Jun-2020) -- End


            'Pinkal (18-Mar-2021) -- Start
            'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
            Me.ViewState("mintExpenseIndex") = mintExpenseIndex
            'Pinkal (18-Mar-2021) -- End


            If mstrFilePath.Trim.Length > 0 Then
                If System.IO.File.Exists(mstrFilePath) Then
                    'System.IO.File.Delete(Me.ViewState("Path") & "\" & Me.ViewState("FileName"))
                    System.IO.File.Delete(mstrFilePath)
                    mstrFilePath = ""
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Private Funcation(S)"

    'Pinkal (01-Apr-2019) -- Start
    'Enhancement - Working on Leave Changes for NMB.

    ''' <summary>
    ''' Checking this viewstate  Me.ViewState.Add("ArutiSelfServiceURL" 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub DirectApprovLinkPageLoad()
        Try
            If (Session("clsuser") Is Nothing OrElse Request.QueryString.Count > 0) AndAlso IsPostBack = False Then
                Me.Title = ""
                Me.lblPageHeader.Text = ""
                If Request.QueryString.Count > 0 Then
                    'S.SANDEEP |17-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : PM ERROR
                    KillIdleSQLSessions()
                    'S.SANDEEP |17-MAR-2020| -- END
                    objCONN = Nothing
                    If objCONN Is Nothing OrElse objCONN.State = ConnectionState.Closed OrElse objCONN.State = ConnectionState.Broken Then
                        Dim constr As String = ConfigurationManager.ConnectionStrings("paydb").ConnectionString
                        Dim dbPwd As String = ConfigurationManager.AppSettings("dbpassword").ToString()
                        constr = constr.Replace("*****", clsSecurity.Decrypt(dbPwd, "ezee").ToString())
                        objCONN = New SqlConnection
                        objCONN.ConnectionString = constr
                        objCONN.Open()
                        HttpContext.Current.Session("gConn") = objCONN
                    End If


                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    GC.Collect()
                    'Pinkal (11-Sep-2020) -- End

                    Dim arr() As String = clsCrypto.Dicrypt(Server.UrlDecode(Request.QueryString.ToString)).Split(CChar("|"))
                    If arr.Length = 7 Then
                        HttpContext.Current.Session("CompanyUnkId") = CInt(arr(0))
                        HttpContext.Current.Session("UserId") = CInt(arr(1))
                        mintEmpUnkId = CInt(arr(2))
                        mintApprverId = CInt(arr(3))
                        mintLeaveFormUnkId = CInt(arr(4))
                        mintPenddingLvTranUnkId = CInt(arr(5))

                        'Pinkal (01-Mar-2016) -- Start
                        'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                        mblnIsExternalApprover = CBool(arr(6))
                        'Pinkal (01-Mar-2016) -- End

                        Dim strError As String = ""
                        If GetCompanyYearInfo(strError, CInt(Session("CompanyUnkId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString & "Index.aspx")
                            Exit Sub
                        End If

                        HttpContext.Current.Session("mdbname") = Session("Database_Name")

                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        gobjConfigOptions = New clsConfigOptions
                        'gobjConfigOptions._Companyunkid = CInt(Session("CompanyUnkId"))
                        ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        'Company._Object._Companyunkid = CInt(Session("CompanyUnkId"))
                        'Pinkal (11-Sep-2020) -- End

                        CType(HttpContext.Current.Session("gConn"), SqlConnection).ChangeDatabase(Session("mdbname").ToString) 'Sohail (28 Oct 2013)

                        ArtLic._Object = New ArutiLic(False)
                        If ConfigParameter._Object.GetKeyValue(0, "Emp") = "" Then
                            Dim objGroupMaster As New clsGroup_Master
                            objGroupMaster._Groupunkid = 1
                            ArtLic._Object.HotelName = objGroupMaster._Groupname
                        End If

                        If ConfigParameter._Object._IsArutiDemo = False AndAlso (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Manager_Self_Service) = False OrElse ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False) Then
                            DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                            Exit Sub
                        End If

                        If ConfigParameter._Object._IsArutiDemo Then
                            If ConfigParameter._Object._IsExpire Then
                                DisplayMessage.DisplayMessage("The evaluation period of Aruti is Over. To continue using this software you have to register this software.", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                Exit Try
                            Else
                                If Today.Date > eZeeDate.convertDate(acore32.core.HD) Then
                                    DisplayMessage.DisplayMessage("Your demo period is over. Please contact " & acore32.core.SupportTeam & ".", Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/Index.aspx")
                                    Exit Try
                                End If
                            End If
                        End If



                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        'Dim clsConfig As New clsConfigOptions
                        'clsConfig._Companyunkid = CInt(Session("CompanyUnkId"))
                        'Session("IsIncludeInactiveEmp") = clsConfig._IsIncludeInactiveEmp.ToString
                        'Session("EmployeeAsOnDate") = clsConfig._EmployeeAsOnDate
                        'Session("LeaveApproverForLeaveType") = clsConfig._IsLeaveApprover_ForLeaveType

                        'If clsConfig._LeaveBalanceSetting <= 0 Then
                        '    Session("LeaveBalanceSetting") = enLeaveBalanceSetting.Financial_Year
                        'Else
                        '    Session("LeaveBalanceSetting") = clsConfig._LeaveBalanceSetting
                        'End If

                        'If clsConfig._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                        '    mstrArutiSelfServiceURL = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath
                        'Else
                        '    mstrArutiSelfServiceURL = clsConfig._ArutiSelfServiceURL
                        'End If

                        'Session("UserAccessModeSetting") = clsConfig._UserAccessModeSetting.Trim
                        'Session("IsAutomaticIssueOnFinalApproval") = clsConfig._IsAutomaticIssueOnFinalApproval
                        'Session("Notify_IssuedLeave_Users") = clsConfig._Notify_IssuedLeave_Users.Trim


                        ''Pinkal (16-Apr-2016) -- Start
                        ''Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                        'Session("DateFormat") = clsConfig._CompanyDateFormat
                        'Session("DateSeparator") = clsConfig._CompanyDateSeparator
                        'SetDateFormat()
                        ''Pinkal (16-Apr-2016) -- End


                        Session("IsIncludeInactiveEmp") = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                        Session("EmployeeAsOnDate") = ConfigParameter._Object._EmployeeAsOnDate
                        Session("LeaveApproverForLeaveType") = ConfigParameter._Object._IsLeaveApprover_ForLeaveType

                        If ConfigParameter._Object._LeaveBalanceSetting <= 0 Then
                            Session("LeaveBalanceSetting") = enLeaveBalanceSetting.Financial_Year
                        Else
                            Session("LeaveBalanceSetting") = ConfigParameter._Object._LeaveBalanceSetting
                        End If

                        If ConfigParameter._Object._ArutiSelfServiceURL = "http://" & Request.ApplicationPath Then
                            mstrArutiSelfServiceURL = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath
                        Else
                            mstrArutiSelfServiceURL = ConfigParameter._Object._ArutiSelfServiceURL
                        End If

                        Session("UserAccessModeSetting") = ConfigParameter._Object._UserAccessModeSetting.Trim
                        Session("IsAutomaticIssueOnFinalApproval") = ConfigParameter._Object._IsAutomaticIssueOnFinalApproval
                        Session("Notify_IssuedLeave_Users") = ConfigParameter._Object._Notify_IssuedLeave_Users.Trim


                        'Pinkal (16-Apr-2016) -- Start
                        'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                        Session("DateFormat") = ConfigParameter._Object._CompanyDateFormat
                        Session("DateSeparator") = ConfigParameter._Object._CompanyDateSeparator
                        SetDateFormat()
                        'Pinkal (16-Apr-2016) -- End

                        'Pinkal (11-Sep-2020) -- End

                        Try
                            If Request.ServerVariables("HTTP_X_FORWARDED_FOR") Is Nothing Then
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            Else
                                HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("HTTP_X_FORWARDED_FOR").ToString()
                                HttpContext.Current.Session("HOST_NAME") = GetHostByAddress(Request.ServerVariables("REMOTE_HOST")).HostName
                            End If

                        Catch ex As Exception
                            HttpContext.Current.Session("IP_ADD") = Request.ServerVariables("REMOTE_ADDR").ToString
                            HttpContext.Current.Session("HOST_NAME") = Request.ServerVariables("REMOTE_ADDR").ToString
                        End Try

                        Dim objUser As New clsUserAddEdit
                        objUser._Userunkid = CInt(Session("UserId"))
                        Session("rootpath") = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath & "/"
                        Call GetDatabaseVersion()

                        Dim clsuser As New User(objUser._Username, objUser._Password, Session("mdbname").ToString)
                        HttpContext.Current.Session("clsuser") = clsuser
                        HttpContext.Current.Session("UserName") = clsuser.UserName
                        HttpContext.Current.Session("Firstname") = clsuser.Firstname
                        HttpContext.Current.Session("Surname") = clsuser.Surname
                        HttpContext.Current.Session("MemberName") = clsuser.MemberName
                        HttpContext.Current.Session("LoginBy") = Global.User.en_loginby.User
                        HttpContext.Current.Session("UserId") = clsuser.UserID
                        HttpContext.Current.Session("Employeeunkid") = clsuser.Employeeunkid
                        HttpContext.Current.Session("Password") = clsuser.password
                        HttpContext.Current.Session("RoleID") = clsuser.RoleUnkID
                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid


                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objUser = Nothing
                        'Pinkal (11-Sep-2020) -- End


                        strError = ""
                        If SetUserSessions(strError) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        End If

                        strError = ""
                        If SetCompanySessions(strError, CInt(Session("CompanyUnkId")), CInt(Session("LangId"))) = False Then
                            DisplayMessage.DisplayMessage(strError, Me.Page, Session("rootpath").ToString() & "Index.aspx")
                            Exit Sub
                        End If

                        HttpContext.Current.Session("LangId") = clsuser.LanguageUnkid
                        gobjLocalization = New clsLocalization(enArutiApplicatinType.Aruti_Payroll, Session("mdbname").ToString)
                        gobjLocalization._LangId = CInt(HttpContext.Current.Session("LangId"))

                        Dim objUserPrivilege As New clsUserPrivilege
                        objUserPrivilege.setUserPrivilege(CInt(Session("UserId")))
                        Session("AddLeaveAllowance") = objUserPrivilege._AllowtoAddLeaveAllowance
                        Session("AddLeaveExpense") = objUserPrivilege._AddLeaveExpense
                        Session("EditLeaveExpense") = objUserPrivilege._EditLeaveExpense
                        Session("DeleteLeaveExpense") = objUserPrivilege._DeleteLeaveExpense
                        Session("AllowtoApproveLeave") = objUserPrivilege._AllowtoApproveLeave
                        Session("AllowIssueLeave") = objUserPrivilege._AllowIssueLeave

                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objUserPrivilege = Nothing
                        Dim objPendingLeave As New clspendingleave_Tran
                        'Pinkal (11-Sep-2020) -- End

                        Dim dsList As DataSet = objPendingLeave.GetList("PendingLeave", _
                                                                        Session("Database_Name").ToString(), _
                                                                        CInt(Session("UserId")), _
                                                                        CInt(Session("Fin_year")), _
                                                                        CInt(Session("CompanyUnkId")), _
                                                                        eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                                                        CBool(Session("IsIncludeInactiveEmp")), True, "pendingleavetranunkid = " & mintPenddingLvTranUnkId & " ")

                        If dsList.Tables("PendingLeave").Rows.Count > 0 Then
                            If CInt(dsList.Tables("PendingLeave").Rows(0)("visibleid")) = 7 Then
                                Session.Abandon()
                                DisplayMessage.DisplayMessage("You cannot Edit this Leave detail. Reason: This Leave was already issued.", Me, "../Index.aspx")
                                'Pinkal (11-Sep-2020) -- Start
                                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                If dsList IsNot Nothing Then dsList.Clear()
                                dsList = Nothing
                                objPendingLeave = Nothing
                                'Pinkal (11-Sep-2020) -- End
                                Exit Sub
                            ElseIf CInt(dsList.Tables("PendingLeave").Rows(0)("visibleid")) = 1 Then
                                Session.Abandon()
                                DisplayMessage.DisplayMessage("You cannot Edit this Leave detail. Reason: This Leave approval was already approved.", Me, "../Index.aspx")
                                'Pinkal (11-Sep-2020) -- Start
                                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                If dsList IsNot Nothing Then dsList.Clear()
                                dsList = Nothing
                                objPendingLeave = Nothing
                                'Pinkal (11-Sep-2020) -- End
                                Exit Sub
                            End If
                            mintPriority = CInt(dsList.Tables("PendingLeave").Rows(0)("priority"))
                            mintApproverTranUnkId = CInt(dsList.Tables("PendingLeave").Rows(0)("approvertranunkid"))
                        End If

                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        If dsList IsNot Nothing Then dsList.Clear()
                        dsList = Nothing
                        objPendingLeave = Nothing
                        'Pinkal (11-Sep-2020) -- End

                    Else
                        Response.Redirect("../Index.aspx", False)
                        Exit Sub
                    End If
                Else
                    Response.Redirect("../Index.aspx", False)
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (01-Apr-2019) -- End

    Private Sub FillCombo()
        Dim objApprover As New clsleaveapprover_master
        Dim objMaster As New clsMasterData
        Dim dsFill As DataSet = Nothing
        Try

            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'cboApprover.DataSource = objApprover.GetEmployeeApprover(Session("Database_Name").ToString(), _
            '                                                         CInt(Session("Fin_year")), _
            '                                                         CInt(Session("CompanyUnkId")), _
            '                                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
            '                                                         CBool(Session("IsIncludeInactiveEmp")), _
            '                                                         CInt(Session("UserId")), -1, mintApprverId, -1)

            cboApprover.DataSource = objApprover.GetEmployeeApprover(Session("Database_Name").ToString(), _
                                                                     CInt(Session("Fin_year")), _
                                                                     CInt(Session("CompanyUnkId")), _
                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                     CBool(Session("IsIncludeInactiveEmp")), _
                                                                     CInt(Session("UserId")), -1, mintApprverId, -1, Session("LeaveApproverForLeaveType").ToString(), Nothing, mblnIsExternalApprover)


            'Pinkal (01-Mar-2016) -- End

            cboApprover.DataTextField = "employeename"
            cboApprover.DataValueField = "employeeunkid"
            cboApprover.DataBind()


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'dsFill = objMaster.getLeaveStatusList("Status", True)
            'Dim dtStatus As DataTable = Nothing
            'dtStatus = New DataView(dsFill.Tables(0), "statusunkid not in (6,7)", "", DataViewRowState.CurrentRows).ToTable
            'cboStatus.DataValueField = "statusunkid"
            'cboStatus.DataTextField = "name"
            'cboStatus.DataSource = dtStatus
            'cboStatus.DataBind()
            'Pinkal (25-May-2019) -- End

            Dim objTransactionhead As New clsTransactionHead
            cboAllowance.DataSource = objTransactionhead.getComboList(Session("Database_Name").ToString(), "Allowance", True, , , Aruti.Data.enTypeOf.Allowance)
            cboAllowance.DataTextField = "name"
            cboAllowance.DataValueField = "tranheadunkid"
            cboAllowance.DataBind()
            objTransactionhead = Nothing


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            LblReliever.Visible = CBool(Session("SetRelieverAsMandatoryForApproval"))
            cboReliever.Visible = CBool(Session("SetRelieverAsMandatoryForApproval"))
            If CBool(Session("SetRelieverAsMandatoryForApproval")) Then
                FillReliver()
            End If
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            chkIncludeAllStaff.Visible = CBool(Session("SetRelieverAsMandatoryForApproval"))
            'Pinkal (03-May-2019) -- End


            'Pinkal (03-Mar-2021)-- Start
            'KADCO Enhancement  -  Working on  KADCO - Option to Adjust Expense Accrue Balance.
            dsFill = Nothing
            dsFill = objMaster.getLeaveStatusList("Status", "", False, False, False)
            Dim dRow() As DataRow = dsFill.Tables(0).Select("statusunkid not in (" & Session("ApplicableLeaveStatus").ToString() & ")", "")
            If dRow IsNot Nothing AndAlso dRow.Length > 0 Then
                For i As Integer = 0 To dRow.Length - 1
                    If CInt(dRow(i)("statusunkid")) = 1 Then
                        btnApprove.Visible = False
                    ElseIf CInt(dRow(i)("statusunkid")) = 3 Then
                        btnReject.Visible = False
                    ElseIf CInt(dRow(i)("statusunkid")) = 4 Then
                        btnReschedule.Visible = False
                    End If
                Next
            End If
            'Pinkal (03-Mar-2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsFill IsNot Nothing Then dsFill.Clear()
            dsFill = Nothing
            objMaster = Nothing
            objApprover = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Public Sub GetData()
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objPendingLeave As New clspendingleave_Tran
            'Pinkal (11-Sep-2020) -- End

            Dim dtEmployeedata As DataTable = objPendingLeave.GetList("PendingLeave", _
                                                                    Session("Database_Name").ToString(), _
                                                                    CInt(Session("UserId")), _
                                                                    CInt(Session("Fin_year")), _
                                                                    CInt(Session("CompanyUnkId")), _
                                                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                                                    CBool(Session("IsIncludeInactiveEmp")), True, "pendingleavetranunkid = " & mintPenddingLvTranUnkId & " ").Tables(0)


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objPendingLeave = Nothing
            'Pinkal (11-Sep-2020) -- End


            txtEmployee.Text = dtEmployeedata.Rows(0)("EmployeeName").ToString
            txtFormNo.Text = dtEmployeedata.Rows(0)("formno").ToString()
            dtpApplyDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtpStartdate.SetDate = eZeeDate.convertDate(dtEmployeedata.Rows(0)("startdate").ToString).Date
            dtpEnddate.SetDate = eZeeDate.convertDate(dtEmployeedata.Rows(0)("enddate").ToString).Date
            txtRemark.Text = dtEmployeedata.Rows(0)("remark").ToString
            txtAllowance.Text = dtEmployeedata.Rows(0)("amount").ToString

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'cboStatus.SelectedValue = dtEmployeedata.Rows(0)("statusunkid").ToString()
            txtLeaveType.Text = dtEmployeedata.Rows(0)("leavename").ToString()
            'Pinkal (25-May-2019) -- End


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If CBool(Session("SetRelieverAsMandatoryForApproval")) Then
                cboReliever.SelectedValue = dtEmployeedata.Rows(0)("relieverempunkid").ToString()
            End If
            'Pinkal (01-Oct-2018) -- End

            dtpStartdate_ValueChanged(New Object(), New EventArgs())


            Dim objLeaveAccrue As New clsleavebalance_tran
            Dim Balancedata As DataSet = Nothing
            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

                'Balancedata = objLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, _
                '                                                 dtEmployeedata.Rows(0)("Leavetypeunkid").ToString(), _
                '                                                 dtEmployeedata.Rows(0)("Employeeunkid").ToString(), _
                '                                                 CDate(Session("fin_startdate")).Date, _
                '                                                 CDate(Session("fin_enddate")).Date, , , _
                '                                                 CInt(Session("LeaveBalanceSetting")), , _
                '                                                 CInt(Session("Fin_year")))

                Balancedata = objLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, _
                                                                 dtEmployeedata.Rows(0)("Leavetypeunkid").ToString(), _
                                                                 dtEmployeedata.Rows(0)("Employeeunkid").ToString(), _
                                                              CInt(Session("LeaveAccrueTenureSetting")), _
                                                              CInt(Session("LeaveAccrueDaysAfterEachMonth")), _
                                                                 CDate(Session("fin_startdate")).Date, _
                                                                 CDate(Session("fin_enddate")).Date, , , _
                                                                 CInt(Session("LeaveBalanceSetting")), , _
                                                                 CInt(Session("Fin_year")))

                'Pinkal (16-Dec-2016) -- End

            ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                objLeaveAccrue._DBStartdate = CDate(Session("fin_startdate")).Date
                objLeaveAccrue._DBEnddate = CDate(Session("fin_enddate")).Date

                'Pinkal (16-Dec-2016) -- Start
                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

                'Balancedata = objLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, _
                '                                                 dtEmployeedata.Rows(0)("Leavetypeunkid").ToString(), _
                '                                                 dtEmployeedata.Rows(0)("Employeeunkid").ToString(), _
                '                                                 Nothing, Nothing, True, True, _
                '                                                 CInt(Session("LeaveBalanceSetting")), , _
                '                                                 CInt(Session("Fin_year")))


                Balancedata = objLeaveAccrue.GetLeaveBalanceInfo(ConfigParameter._Object._CurrentDateAndTime.Date, _
                                                                 dtEmployeedata.Rows(0)("Leavetypeunkid").ToString(), _
                                                                 dtEmployeedata.Rows(0)("Employeeunkid").ToString(), _
                                                                 CInt(Session("LeaveAccrueTenureSetting")), _
                                                                 CInt(Session("LeaveAccrueDaysAfterEachMonth")), _
                                                                 Nothing, Nothing, True, True, _
                                                                 CInt(Session("LeaveBalanceSetting")), , _
                                                                 CInt(Session("Fin_year")))

                'Pinkal (16-Dec-2016) -- End


            End If

            If Not Balancedata.Tables(0).Rows.Count = 0 Then
                If Balancedata.Tables("Balanceinfo").Rows.Count > 0 Then
                    objlblLastYearAccruedValue.Text = CDec(Balancedata.Tables("Balanceinfo").Rows(0)("LstyearAccrue_amount")).ToString("#0.00")
                    objlblLastYearIssuedValue.Text = CDec(Balancedata.Tables("Balanceinfo").Rows(0)("LstyearIssue_amount")).ToString("#0.00")
                    objlblAccruedToDateValue.Text = CDec(Balancedata.Tables("Balanceinfo").Rows(0)("Accrue_amount")).ToString("#0.00")
                    objlblBalanceValue.Text = CDec(Balancedata.Tables("Balanceinfo").Rows(0)("Balance")).ToString("#0.00")

                    'Pinkal (24-Aug-2016) -- Start
                    'Enhancement - Removing 2 columns(Leave Accrue Upto Last Year AND Leave Issued Upto Last Year) From Whole Leave Module AS Per Rutta's Request For VFT
                    ' objlblIssuedToDateValue.Text = CDec(Balancedata.Tables("Balanceinfo").Rows(0)("Issue_amount")).ToString("#0.00")
                    objlblIssuedToDateValue.Text = CDec(CDec(Balancedata.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(Balancedata.Tables("Balanceinfo").Rows(0)("LeaveEncashment"))).ToString("#0.00")
                    objlblLeaveBFvalue.Text = CDec(Balancedata.Tables("Balanceinfo").Rows(0)("LeaveBF")).ToString("#0.00")
                    objlblTotalAdjustment.Text = CDec(Balancedata.Tables("Balanceinfo").Rows(0)("LeaveAdjustment")).ToString("#0.00")
                    'Pinkal (24-Aug-2016) -- End


                    'Pinkal (15-Apr-2019) -- Start
                    'Enhancement - Working on Leave & CR Changes for NMB.
                    objlblBalanceAsonDateValue.Text = CDec(CDec(CDec(Balancedata.Tables("Balanceinfo").Rows(0)("LeaveBF")) + CDec(Balancedata.Tables("Balanceinfo").Rows(0)("Accrue_amount"))) - CDec(CDec(Balancedata.Tables("Balanceinfo").Rows(0)("Issue_amount")) + CDec(Balancedata.Tables("Balanceinfo").Rows(0)("LeaveEncashment")) + CDec(Balancedata.Tables("Balanceinfo").Rows(0)("LeaveAdjustment")))).ToString("#0.00")
                    'Pinkal (15-Apr-2019) -- End


                End If
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveAccrue = Nothing
            'Pinkal (11-Sep-2020) -- End

            dtEmployeedata.Rows.Clear()
            dtEmployeedata = Nothing
            Balancedata.Tables(0).Rows.Clear()
            Balancedata = Nothing

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub GetEmployeeValue()
        Try
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = mintEmpUnkId

            Dim objdept As New clsDepartment
            objdept._Departmentunkid = objEmployee._Departmentunkid
            txtDepartment.Text = objdept._Name

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objdept = Nothing
            'Dim objJob As New clsJobs
            ' objJob._Jobunkid = objEmployee._Jobunkid
            'Pinkal (11-Sep-2020) -- End

            Dim objReportTo As New clsReportingToEmployee

            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            'objReportTo._EmployeeUnkid = mintEmpUnkId
            objReportTo._EmployeeUnkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = mintEmpUnkId
            'Pinkal (18-Aug-2018) -- End

            Dim dt As DataTable = objReportTo._RDataTable
            Dim DefaultReportList As List(Of DataRow) = (From p In dt.AsEnumerable() Where (CBool(p.Item("ishierarchy")) = True) Select (p)).ToList
            If DefaultReportList.Count > 0 Then
                txtReportTo.Text = DefaultReportList(0).Item("ename").ToString

                'Pinkal (17-Nov-2017) -- Start
                'Enhancement - issue # 0001563 Windsor Reporting To Issue on Leave Approval Process.
                'Else
                '    txtReportTo.Text = objJob._Job_Name
                'Pinkal (17-Nov-2017) -- End
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objReportTo = Nothing
            'Pinkal (11-Sep-2020) -- End


            Dim objLeaveform As New clsleaveform
            Dim objlvtype As New clsleavetype_master
            objLeaveform._Formunkid = mintLeaveFormUnkId
            objlvtype._Leavetypeunkid = objLeaveform._Leavetypeunkid
            lnkShowScanDocuementList.Visible = objlvtype._IsCheckDocOnLeaveForm

            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            mblnIsLvTypePaid = objlvtype._IsPaid
            mintLvformMinDays = objlvtype._LVDaysMinLimit
            mintLvformMaxDays = objlvtype._LVDaysMaxLimit
            'Pinkal (08-Oct-2018) -- End
            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            mblnConsiderLeaveOnTnAPeriod = objLeaveform._ConsiderLeaveOnTnAPeriod
            'Pinkal (01-Jan-2019) -- End

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            mblnIsBlockLeave = objlvtype._IsBlockLeave
            mblnIsStopApplyAcrossFYELC = objlvtype._IsStopApplyAcrossFYELC
            mblnIsShortLeave = objlvtype._IsShortLeave
            'Pinkal (26-Feb-2019) -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            mblnConsiderExpense = objlvtype._ConsiderExpense
            lnkShowLeaveExpense.Visible = mblnConsiderExpense
            'Pinkal (03-May-2019) -- End


            'Pinkal (05-Jun-2020) -- Start
            'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
            mblnDoNotApplyForFutureDates = objlvtype._DoNotApplyForFutureDates
            'Pinkal (05-Jun-2020) -- End


            objlvtype = Nothing
            objLeaveform = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function CalculateDays(ByVal dtFraction As DataTable) As DataTable
        Try
            Dim blnIssueonHoliday As Boolean = False
            Dim objLeaveForm As New clsleaveform
            Dim objLeaveType As New clsleavetype_master

            objLeaveForm._Formunkid = mintLeaveFormUnkId
            objLeaveType._Leavetypeunkid = objLeaveForm._Leavetypeunkid

            Call GetApproverDayFraction()

            Dim objLeaveFraction As New clsleaveday_fraction
            dtFraction = objLeaveFraction.GetList("List", mintLeaveFormUnkId, True, -1, mintFreactionApproverId).Tables(0)

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveFraction = Nothing
            objLeaveForm = Nothing
            'Pinkal (11-Sep-2020) -- End

            Dim mintTotalDays As Integer = CInt(DateDiff(DateInterval.Day, dtpStartdate.GetDate.Date, dtpEnddate.GetDate.AddDays(1).Date))
            Dim objEmp As New clsEmployee_Master
            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = mintEmpUnkId
            Dim objEmpHoliday As New clsemployee_holiday
            Dim objShift As New clsNewshift_master
            Dim objEmpShiftTran As New clsEmployee_Shift_Tran
            Dim objShiftTran As New clsshift_tran
            Dim blnIssueonweekend As Boolean = False
            Dim blnConsiderLVHlOnWk As Boolean = False
            blnIssueonweekend = objLeaveType._Isissueonweekend
            blnConsiderLVHlOnWk = objLeaveType._IsLeaveHL_onWK

            For i As Integer = 0 To mintTotalDays - 1
                objShift._Shiftunkid = objEmpShiftTran.GetEmployee_Current_ShiftId(dtpStartdate.GetDate.AddDays(i).Date, objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date))
                objShiftTran.GetShiftTran(objShift._Shiftunkid)

                Dim strWeekName As String = CultureInfo.CurrentUICulture.DateTimeFormat.GetDayName(dtpStartdate.GetDate.AddDays(i).DayOfWeek)

                If blnIssueonHoliday = False AndAlso blnIssueonweekend = True Then
                    If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date), dtpStartdate.GetDate.AddDays(i).Date).Rows.Count > 0 Then
                        Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & dtpStartdate.GetDate.AddDays(i).Date & "' AND AUD <> 'D'")
                        If dRow.Length > 0 Then
                            dRow(0)("AUD") = "D"
                            dtFraction.AcceptChanges()
                        End If

                        If blnIssueonweekend AndAlso blnConsiderLVHlOnWk = False Then

                            If objShiftTran._dtShiftday IsNot Nothing Then
                                Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
                                If drShift.Length > 0 Then
                                    GoTo AddRecord
                                End If

                            End If

                        End If
                        Continue For
                    End If

                    Dim objReccurent As New clsemployee_holiday
                    If objReccurent.GetEmployeeReCurrentHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date), dtpStartdate.GetDate.AddDays(i).Date) Then
                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objReccurent = Nothing
                        'Pinkal (11-Sep-2020) -- End
                        Continue For
                    End If
                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objReccurent = Nothing
                    'Pinkal (11-Sep-2020) -- End
                End If

                If blnIssueonweekend = False AndAlso blnIssueonHoliday = True Then
                    If objShiftTran._dtShiftday IsNot Nothing Then
                        Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
                        If drShift.Length > 0 Then
                            Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & dtpStartdate.GetDate.AddDays(i).Date & "' AND AUD <> 'D'")
                            If dRow.Length > 0 Then
                                dRow(0)("AUD") = "D"
                                dtFraction.AcceptChanges()
                            End If

                            If blnIssueonHoliday AndAlso blnConsiderLVHlOnWk = False Then
                                If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date), dtpStartdate.GetDate.AddDays(i).Date).Rows.Count > 0 Then
                                    GoTo AddRecord
                                End If
                            End If
                            Continue For
                        End If
                    End If
                End If

                If blnIssueonweekend = False AndAlso blnIssueonHoliday = False Then
                    If objShiftTran._dtShiftday IsNot Nothing Then
                        Dim drShift() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(strWeekName) & " AND isweekend = 1")
                        If drShift.Length > 0 Then
                            Dim dRow() As DataRow = dtFraction.Select("leavedate = '" & dtpStartdate.GetDate.AddDays(i).Date & "' AND AUD <> 'D'")
                            If dRow.Length > 0 Then
                                dRow(0)("AUD") = "D"
                                dtFraction.AcceptChanges()
                            End If
                            Continue For
                        Else
                            If objEmpHoliday.GetEmployeeHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date), dtpStartdate.GetDate.AddDays(i).Date).Rows.Count > 0 Then
                                Continue For
                            End If
                            Dim objReccurent As New clsemployee_holiday
                            If objReccurent.GetEmployeeReCurrentHoliday(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date), dtpStartdate.GetDate.AddDays(i).Date) Then
                                'Pinkal (11-Sep-2020) -- Start
                                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                objReccurent = Nothing
                                'Pinkal (11-Sep-2020) -- End
                                Continue For
                            End If
                            'Pinkal (11-Sep-2020) -- Start
                            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            objReccurent = Nothing
                            'Pinkal (11-Sep-2020) -- End
                        End If
                    End If

                End If

AddRecord:
                Dim dtRow As DataRow() = dtFraction.Select("leavedate = '" & dtpStartdate.GetDate.AddDays(i).Date & "' AND AUD = ''")
                If dtRow.Length = 0 Then
                    Dim dr As DataRow = dtFraction.NewRow
                    dr("dayfractionunkid") = -1
                    dr("formunkid") = mintLeaveFormUnkId
                    dr("leavedate") = dtpStartdate.GetDate.AddDays(i).ToShortDateString
                    dr("dayfraction") = 1
                    If dr("AUD").ToString() = "" Then
                        dr("AUD") = "A"
                    End If
                    dr("approverunkid") = mintFreactionApproverId
                    dr("GUID") = Guid.NewGuid.ToString()
                    dtFraction.Rows.Add(dr)
                End If
            Next
            dtFraction = New DataView(dtFraction, "", "leavedate asc", DataViewRowState.CurrentRows).ToTable


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objEmpHoliday = Nothing
            objEmpShiftTran = Nothing
            objShiftTran = Nothing
            objShift = Nothing
            objEmp = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("CalculateDays :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
        Return dtFraction
    End Function

    Private Sub GetApproverDayFraction()
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objPendingLeave As New clspendingleave_Tran
        'Pinkal (11-Sep-2020) -- End
        Try
            objPendingLeave._Pendingleavetranunkid = mintPenddingLvTranUnkId

            If objPendingLeave._Statusunkid = 1 Then
                mintFreactionApproverId = objPendingLeave._Approverunkid
            Else
                Dim objleaveapprover As New clsleaveapprover_master
                Dim objApproveLevel As New clsapproverlevel_master
                objleaveapprover._Approverunkid = objPendingLeave._Approvertranunkid
                objApproveLevel._Levelunkid = objleaveapprover._Levelunkid


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objleaveapprover = Nothing
                'Pinkal (11-Sep-2020) -- End


                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'Dim mstrSearch As String = "lvpendingleave_tran.approverunkid <> " & objPendingLeave._Approverunkid & " AND  lvpendingleave_tran.formunkid = " & objPendingLeave._Formunkid
                Dim mstrSearch As String = "lvpendingleave_tran.approvertranunkid <> " & objPendingLeave._Approvertranunkid & " AND  lvpendingleave_tran.formunkid = " & objPendingLeave._Formunkid
                'Pinkal (26-Feb-2019) -- End

                Dim dsPedingList As DataSet = objPendingLeave.GetList("PendingProcess", _
                                                                      Session("Database_Name").ToString, _
                                                                      CInt(Session("UserId")), _
                                                                      CInt(Session("Fin_year")), _
                                                                      CInt(Session("CompanyUnkId")), _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                                                      CBool(Session("IsIncludeInactiveEmp")), True, mstrSearch)

                Dim drRow() As DataRow = dsPedingList.Tables(0).Select("priority < " & objApproveLevel._Priority)
                If drRow.Length > 0 Then
                    Dim mintApprovPriority As Integer = CInt(dsPedingList.Tables(0).Compute("Max(priority)", "statusunkid=1"))
                    For i As Integer = 0 To drRow.Length - 1
                        If CInt(drRow(i)("priority")) = mintApprovPriority AndAlso CInt(drRow(i)("statusunkid")) = 1 Then
                            mintFreactionApproverId = CInt(drRow(i)("approverunkid"))
                            Exit For
                        End If
                    Next
                Else
                    mintFreactionApproverId = -1
                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsPedingList IsNot Nothing Then dsPedingList.Clear()
                dsPedingList = Nothing
                objApproveLevel = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objPendingLeave = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Private Sub GetFractionList()
        Dim dtTable As DataTable = Nothing
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'dtTable = New DataView(mdtFraction, "AUD<>'D'", "", DataViewRowState.CurrentRows).ToTable()
            dtTable = New DataView(mdtFraction, "AUD<>'D'", "", DataViewRowState.CurrentRows).ToTable().Copy()
            'Pinkal (11-Sep-2020) -- End
            dgFraction.DataSource = dtTable
            dgFraction.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Public Sub EntrySave()
        Try
            Dim clsPendingLeave As New clspendingleave_Tran

            If (cboApprover.SelectedValue Is Nothing Or cboApprover.SelectedValue = "") OrElse CInt(cboApprover.SelectedValue) = 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Approver is compulsory information.Please Select Approver."), Me)
                cboApprover.Focus()
                Exit Sub

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'ElseIf CInt(cboStatus.SelectedValue) = 0 Then
                '    'Language.setLanguage(mstrModuleName)
                '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Status is compulsory information.Please Select Status."), Me)
                '    cboStatus.Focus()
                '    Exit Sub
                'Pinkal (25-May-2019) -- End
            End If

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If (CInt(cboStatus.SelectedValue) = 3 Or CInt(cboStatus.SelectedValue) = 4) And txtRemark.Text = "" Then
            If (xStatusId = 3 OrElse xStatusId = 4) AndAlso txtRemark.Text.Trim() = "" Then
                'Pinkal (25-May-2019) -- End
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Remark cannot be blank. Remark is required information."), Me)
                txtRemark.Focus()
                Exit Sub
            End If

            If dtpStartdate.GetDate.Date > dtpEnddate.GetDate.Date Then
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 4, "Please Select Proper Start Date."), Me)
                'Sohail (23 Mar 2019) -- End
                dtpStartdate.Focus()
                Exit Sub
            End If

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If mblnIsStopApplyAcrossFYELC Then

                If mblnIsShortLeave = False Then

                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                        If LeaveDateValidation(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date) = False Then Exit Sub

                    ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                        Dim objAccrueBalance As New clsleavebalance_tran
                        Dim dsBalanceList As DataSet = objAccrueBalance.GetList("List", Session("Database_Name").ToString(), CInt(Session("UserId")), CInt(Session("Fin_year")) _
                                                                , CInt(Session("CompanyUnkId")), Session("EmployeeAsOnDate").ToString(), Session("UserAccessModeSetting").ToString() _
                                                                , True, CBool(Session("IsIncludeInactiveEmp")), True, False, False, mintEmpUnkId, True, True, False, "lvleavebalance_tran.leavetypeunkid = " & CInt(IIf(mintDeductedLeaveTypeID > 0, mintDeductedLeaveTypeID, mintLvFormLvTypeID)) & " AND lvleavebalance_tran.isvoid = 0", Nothing)
                        objAccrueBalance = Nothing

                        If dsBalanceList IsNot Nothing AndAlso dsBalanceList.Tables(0).Rows.Count > 0 Then
                            If LeaveDateValidation(CDate(dsBalanceList.Tables(0).Rows(0)("startdate")).Date, CDate(dsBalanceList.Tables(0).Rows(0)("enddate")).Date) = False Then Exit Sub
                        End If

                    End If

                ElseIf mblnIsShortLeave Then
                    If LeaveDateValidation(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date) = False Then Exit Sub
                End If  '  ElseIf mblnIsShortLeave Then

            End If '  If mblnIsStopApplyAcrossFYELC Then

            'Pinkal (26-Feb-2019) -- End


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.

            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            'If CBool(Session("SetRelieverAsMandatoryForApproval")) AndAlso CInt(cboReliever.SelectedValue) <= 0 Then

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If CBool(Session("SetRelieverAsMandatoryForApproval")) AndAlso CInt(cboReliever.SelectedValue) <= 0 AndAlso CInt(cboStatus.SelectedValue) = 1 Then    'Check Only When Status is only approved.
            If CBool(Session("SetRelieverAsMandatoryForApproval")) AndAlso CInt(cboReliever.SelectedValue) <= 0 AndAlso xStatusId = 1 Then    'Check Only When Status is only approved.
                'Pinkal (25-May-2019) -- End
                'Pinkal (03-May-2019) -- End
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 22, "Reliever is compulsory information.Please select reliever to approve/reject the leave application."), Me)
                'Sohail (23 Mar 2019) -- End
                Exit Sub
            End If
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            If mblnIsLvTypePaid Then
                Dim mdecTotalDays As Decimal = 0
                If mdtFraction IsNot Nothing Then
                    mdecTotalDays = mdtFraction.AsEnumerable().Where(Function(row) row.Field(Of String)("AUD") <> "D").Sum(Function(row) row.Field(Of Decimal)("dayfraction"))
                End If

                If mintLvformMinDays <> 0 And mintLvformMinDays > mdecTotalDays Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 23, "Sorry, you cannot approve/Issue for this leave type as it has been configured with setting for minimum") & " " & mintLvformMinDays & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 25, "day(s) to be applied for this leave type."), Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
                If mintLvformMaxDays <> 0 And mintLvformMaxDays < mdecTotalDays Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 24, "Sorry, you cannot approve/Issue for this leave type as it has been configured with setting for maximum") & " " & mintLvformMaxDays & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 25, "day(s) to be applied for this leave type."), Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
            End If
            'Pinkal (08-Oct-2018) -- End


            'Pinkal (08-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.	
            'If gobjEmailList.Count > 0 AndAlso CStr(Session("Notify_IssuedLeave_Users")).Trim.Length > 0 Then
            If objLeaveEmailList.Count > 0 AndAlso CStr(Session("Notify_IssuedLeave_Users")).Trim.Length > 0 Then
                'Pinkal (08-Feb-2022) -- End

                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Sending Email(s) process is in progress from other module. Please wait for some time."), Me)
                'Sohail (23 Mar 2019) -- End
                Exit Sub
            End If

            Dim objMaster As New clsMasterData
            If CBool(Session("ClosePayrollPeriodIfLeaveIssue")) Then

                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                'Dim mintPeriodId As Integer = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpStartdate.GetDate.Date, CInt(Session("Fin_year")), , )
                Dim mintPeriodId As Integer = 0
                If mblnConsiderLeaveOnTnAPeriod Then
                    mintPeriodId = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartdate.GetDate.Date)
                Else
                    mintPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpStartdate.GetDate.Date, CInt(Session("Fin_year")), 0, False, True, Nothing, True)
                End If
                'Pinkal (01-Jan-2019) -- End


                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'If mintPeriodId > 0 AndAlso CInt(cboStatus.SelectedValue) = 1 Then
                If mintPeriodId > 0 AndAlso xStatusId = 1 Then
                    'Pinkal (25-May-2019) -- End
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(Session("Database_Name").ToString()) = mintPeriodId

                    If objPeriod._Statusid = StatusType.Close Then
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "You can't approve this leave for this employee.Reason: Leave dates are falling in a period which is already closed."), Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    ElseIf objPeriod._Statusid = StatusType.Open Then
                        'If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo = True Then
                        Dim objTnALeaveTran As New clsTnALeaveTran
                        Dim mdtTnADate As DateTime = Nothing

                        'Pinkal (01-Jan-2019) -- Start
                        'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                        Dim mintModuleRefence As enModuleReference
                        If mblnConsiderLeaveOnTnAPeriod Then
                            If objPeriod._TnA_EndDate.Date < dtpStartdate.GetDate.Date Then
                                mdtTnADate = dtpStartdate.GetDate.Date
                            Else
                                mdtTnADate = objPeriod._TnA_EndDate.Date
                            End If
                            mintModuleRefence = enModuleReference.TnA
                        Else
                            If objPeriod._End_Date.Date < dtpStartdate.GetDate.Date Then
                                mdtTnADate = dtpStartdate.GetDate.Date
                            Else
                                mdtTnADate = objPeriod._End_Date.Date
                            End If
                            mintModuleRefence = enModuleReference.Payroll
                        End If

                        If objTnALeaveTran.IsPayrollProcessDone(objPeriod._Periodunkid(Session("Database_Name").ToString()), mintEmpUnkId.ToString, mdtTnADate.Date, mintModuleRefence, Nothing) Then
                            'Sohail (23 Mar 2019) -- Start
                            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                            'DisplayMessage.DisplayError(ex, Me)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "You can't approve this leave for this employee.Reason: Process Payroll is already done for last date of period in which leave dates are falling."), Me)
                            'Sohail (23 Mar 2019) -- End
                            Exit Sub
                        End If
                        'End If
                        'Pinkal (01-Jan-2019) -- End
                    End If
                End If
            End If

            clsPendingLeave._Pendingleavetranunkid = mintPenddingLvTranUnkId

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If clsPendingLeave._Statusunkid <> 2 AndAlso CInt(cboStatus.SelectedValue) = 2 Then
            If clsPendingLeave._Statusunkid <> 2 AndAlso xStatusId = 2 Then
                'Pinkal (25-May-2019) -- End

                'Pinkal (03-Jan-2020) -- Start
                'Enhancement - ATLAS COPCO TANZANIA LTD [0003673] - Remove or rename “Rescheduled” status, when approver is changing leave form status.
                'Dim dsStatusList As DataSet = objMaster.getLeaveStatusList("Status")
                Dim dsStatusList As DataSet = objMaster.getLeaveStatusList("Status", Session("ApplicableLeaveStatus").ToString())
                'Pinkal (03-Jan-2020) -- End


                Dim drRow() As DataRow = dsStatusList.Tables(0).Select("statusunkid = " & clsPendingLeave._Statusunkid)
                If drRow.Length > 0 Then
                    'Language.setLanguage(mstrModuleName)
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "You cannot change ") & drRow(0)("Name").ToString() & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, " status to Pending status."), Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
            End If

            Dim dtFraction As DataTable = mdtFraction
            If dtFraction IsNot Nothing Then
                Dim drRow() As DataRow = dtFraction.Select("AUD <> 'D'")
                If drRow.Length <= 0 Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "You cannot approve this leave form.Reason : As these days are falling in holiday(s) and weekend."), Me)
                    Exit Sub
                End If
            End If


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.

            'If CBool(Session("IsAutomaticIssueOnFinalApproval")) Then
            '    Dim blnConfirmation As Boolean = False
            '    If GetPromptForBalance(blnConfirmation) = False Then
            '        Exit Sub
            '    Else
            '        If CInt(cboStatus.SelectedValue) = 1 Then
            '            'If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
            '            Dim objPaymentTran As New clsPayment_tran
            '            Dim objPeriod As New clsMasterData
            '            Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartdate.GetDate.Date)

            '            Dim mdtTnAStartdate As Date = Nothing
            '            If intPeriodId > 0 Then
            '                Dim objTnAPeriod As New clscommom_period_Tran
            '                mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
            '            End If

            '            If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, clsPendingLeave._Employeeunkid, mdtTnAStartdate, intPeriodId, dtpStartdate.GetDate.Date) > 0 Then
            '                'Language.setLanguage(mstrModuleName)
            '                LeaveApprovalConfirmation.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 19, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to Approve/Issue this leave ?  ")
            '                LeaveApprovalConfirmation.Show()
            '                Exit Sub
            '            End If
            '            'End If
            '        End If
            '        FinalSave(clsPendingLeave)
            '    End If
            'Else

            '    If CInt(cboStatus.SelectedValue) = 1 Then
            '        'If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
            '        Dim objPaymentTran As New clsPayment_tran
            '        Dim objPeriod As New clsMasterData
            '        Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartdate.GetDate.Date)

            '        Dim mdtTnAStartdate As Date = Nothing
            '        If intPeriodId > 0 Then
            '            Dim objTnAPeriod As New clscommom_period_Tran
            '            mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
            '        End If

            '        If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, clsPendingLeave._Employeeunkid, mdtTnAStartdate, intPeriodId, dtpStartdate.GetDate.Date) > 0 Then
            '            'Language.setLanguage(mstrModuleName)
            '            LeaveApprovalConfirmation.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 19, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to Approve/Issue this leave ?  ")
            '            LeaveApprovalConfirmation.Show()
            '            Exit Sub
            '        End If
            '        'End If
            '    End If
            '    FinalSave(clsPendingLeave)
            'End If
            Dim blnConfirmation As Boolean = False
            If GetPromptForBalance(blnConfirmation) = False Then Exit Sub


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If CInt(cboStatus.SelectedValue) = 1 Then
            If xStatusId = 1 Then
                'Pinkal (25-May-2019) -- End
                'If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) OrElse ArtLic._Object.IsDemo Then
                Dim objPaymentTran As New clsPayment_tran
                Dim objPeriod As New clsMasterData
                Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartdate.GetDate.Date)

                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                'Dim intPeriodId As Integer = objPeriod.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartdate.GetDate.Date)
                intPeriodId = 0
                If mblnConsiderLeaveOnTnAPeriod Then
                    intPeriodId = objMaster.getCurrentTnAPeriodID(enModuleReference.Payroll, dtpStartdate.GetDate.Date)
                Else
                    intPeriodId = objMaster.getCurrentPeriodID(enModuleReference.Payroll, dtpStartdate.GetDate.Date, CInt(Session("Fin_year")), 0, False, True, Nothing, True)
                End If

                'Dim mdtTnAStartdate As Date = Nothing
                'If intPeriodId > 0 Then
                '    Dim objTnAPeriod As New clscommom_period_Tran
                '    mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                'End If

                'If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, clsPendingLeave._Employeeunkid, mdtTnAStartdate, intPeriodId, dtpStartdate.GetDate.Date) > 0 Then
                Dim mdtTnAStartdate As Date = Nothing
                If intPeriodId > 0 Then
                    Dim objTnAPeriod As New clscommom_period_Tran
                    If mblnConsiderLeaveOnTnAPeriod Then
                        mdtTnAStartdate = objTnAPeriod.GetTnA_StartDate(intPeriodId)
                    Else
                        objTnAPeriod._Periodunkid(Session("Database_Name").ToString()) = intPeriodId
                        mdtTnAStartdate = objTnAPeriod._Start_Date
                    End If
                    objTnAPeriod = Nothing
                End If

                If intPeriodId > 0 AndAlso objPaymentTran.GetTotalPaymentCountWithinTnAPeriod(clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, clsPendingLeave._Employeeunkid, mdtTnAStartdate, intPeriodId, dtpStartdate.GetDate.Date, "", Nothing, mblnConsiderLeaveOnTnAPeriod) > 0 Then
                    'Pinkal (01-Jan-2019) -- End
                    'Language.setLanguage(mstrModuleName)
                    LeaveApprovalConfirmation.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 19, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to Approve/Issue this leave ?  ")
                    LeaveApprovalConfirmation.Show()
                    Exit Sub
                End If
                'End If
            End If
            FinalSave(clsPendingLeave)

            'Pinkal (08-Oct-2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function GetPromptForBalance(ByRef blnConfirmation As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim objLeaveIssueTran As New clsleaveissue_Tran
        Dim objbalance As New clsleavebalance_tran
        Dim objlvForm As New clsleaveform
        Dim objLeaveType As New clsleavetype_master
        Dim mdblCurrentLeavedays As Decimal = 0
        Try


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'If CInt(cboStatus.SelectedValue) = 1 Then
            If xStatusId = 1 Then
                'Pinkal (25-May-2019) -- End
                If mdtFraction IsNot Nothing AndAlso mdtFraction.Rows.Count > 0 Then
                    mdblCurrentLeavedays = CDec(mdtFraction.Compute("SUM(dayfraction)", "AUD<> 'D'"))
                Else
                    mdblCurrentLeavedays = 0
                End If

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'ElseIf CInt(cboStatus.SelectedValue) = 3 Or CInt(cboStatus.SelectedValue) = 4 Then    'REJECT OR RESCHEDULE
            ElseIf xStatusId = 3 OrElse xStatusId = 4 Then    'REJECT OR RESCHEDULE
                'Pinkal (25-May-2019) -- End
                For Each dr As DataRow In mdtFraction.Rows
                    dr("AUD") = "D"
                Next
                mdtFraction.AcceptChanges()
                mdblCurrentLeavedays = 0
            End If

            objlvForm._Formunkid = mintLeaveFormUnkId
            objLeaveType._Leavetypeunkid = objlvForm._Leavetypeunkid


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
            '    dsList = objbalance.GetList("List", _
            '                                Session("Database_Name").ToString(), _
            '                                CInt(Session("UserId")), _
            '                                CInt(Session("Fin_year")), _
            '                                CInt(Session("CompanyUnkId")), _
            '                                Session("EmployeeAsOnDate").ToString, _
            '                                Session("UserAccessModeSetting").ToString, True, _
            '                                CBool(Session("IsIncludeInactiveEmp")), True, True, False, _
            '                                mintEmpUnkId)
            'ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
            '    dsList = objbalance.GetList("List", _
            '                                Session("Database_Name").ToString(), _
            '                                CInt(Session("UserId")), _
            '                                CInt(Session("Fin_year")), _
            '                                CInt(Session("CompanyUnkId")), _
            '                                Session("EmployeeAsOnDate").ToString, _
            '                                Session("UserAccessModeSetting").ToString, True, _
            '                                CBool(Session("IsIncludeInactiveEmp")), True, True, False, _
            '                                mintEmpUnkId, True, True)
            'End If

            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                dsList = objbalance.GetList("List", _
                                            Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            Session("EmployeeAsOnDate").ToString, _
                                            Session("UserAccessModeSetting").ToString, True, _
                                            CBool(Session("IsIncludeInactiveEmp")), True, False, False, _
                                            mintEmpUnkId)
            ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                dsList = objbalance.GetList("List", _
                                            Session("Database_Name").ToString(), _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            Session("EmployeeAsOnDate").ToString, _
                                            Session("UserAccessModeSetting").ToString, True, _
                                            CBool(Session("IsIncludeInactiveEmp")), True, False, False, _
                                            mintEmpUnkId, True, True)
            End If

            'Pinkal (11-Sep-2020) -- End

            Dim drRow As DataRow() = dsList.Tables("List").Select("leavetypeunkid=" & objLeaveType._Leavetypeunkid)

            Dim mdtStartDate As Date = Nothing
            Dim mdtEndDate As Date = Nothing

            If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC AndAlso drRow.Length > 0 Then
                mdtStartDate = CDate(drRow(0)("startdate")).Date
                mdtEndDate = CDate(drRow(0)("enddate")).Date
            End If

            Dim mdblIssue As Decimal = 0
            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(mintEmpUnkId, objLeaveType._Leavetypeunkid, CInt(Session("Fin_year")), Nothing, mdtStartDate, mdtEndDate)
            Else
                mdblIssue = objLeaveIssueTran.GetEmployeeIssueDaysCount(mintEmpUnkId, objLeaveType._Leavetypeunkid, CInt(Session("Fin_year")))
            End If

            If objLeaveType._DeductFromLeaveTypeunkid > 0 Then

                If objLeaveType._MaxAmount < (mdblIssue + mdblCurrentLeavedays) Then
                    blnConfirmation = True
                    'Language.setLanguage(mstrModuleName)
                    'Pinkal (02-Jan-2018) -- Start
                    'Enhancement - Ref 125 For leave types whose days are deducted from leave types of accrue nature, don't allow employee to apply more than the maximum set on the Add/Edit Leave type screen. 
                    'As at now, employee can apply more than the set maximum; approver is only given a warning if he still wishes to issue. If he still wants to approve even with the exceeding leave days he can approve.
                    'Control to be put on application/not just issuing. employee to be warned and stopped.
                    'Confirmation.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?")
                    'Confirmation.Show()
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), Me)
                    Return False
                    'Pinkal (02-Jan-2018) -- End
                    Return False
                End If
            ElseIf objLeaveType._DeductFromLeaveTypeunkid <= 0 AndAlso objLeaveType._IsPaid = False Then
                If objLeaveType._MaxAmount < (mdblIssue + mdblCurrentLeavedays) Then
                    'Language.setLanguage(mstrModuleName)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), Me)
                    Return False
                End If
            End If


            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..

            If drRow.Length > 0 Then


                If CBool(drRow(0)("isshortleave")) = False Then

                    If CInt(drRow(0)("accruesetting")) <> enAccrueSetting.Donot_Issue_On_Exceeding_Balance AndAlso CInt(drRow(0)("accruesetting")) <> enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then


                        'Pinkal (08-Oct-2018) -- Start
                        'Enhancement - Leave Enhancement for NMB.
                        If CBool(drRow(0)("ispaid")) Then
                            If CDec(drRow(0)("maxnegative_limit")) <> 0 AndAlso CDec(drRow(0)("maxnegative_limit")) > CDec(CDec(drRow(0)("accrue_amount")) - (CDec(drRow(0)("issue_amount")) + mdblCurrentLeavedays)) Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 26, "You cannot approve/issue this leave form.Reason : This Leave Limit has exceeded the Maximum Negative Limit for this leave type."), Me)
                                Return False
                            End If
                        End If
                        'Pinkal (08-Oct-2018) -- End

                        If CDec(drRow(0)("Accrue_Amount")) < (CDec(drRow(0)("Issue_amount")) + mdblCurrentLeavedays) Then
                            blnConfirmation = True
                            'Language.setLanguage(mstrModuleName)
                            Confirmation.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 9, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?")
                            Confirmation.Show()
                            Return False
                        End If

                    ElseIf CInt(drRow(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance Then
                        If CDec(drRow(0)("Accrue_Amount")) < (CDec(drRow(0)("Issue_amount")) + mdblCurrentLeavedays) Then
                            'Language.setLanguage(mstrModuleName)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form."), Me)
                            Return False
                        End If

                    ElseIf CInt(drRow(0)("accruesetting")) = enAccrueSetting.Donot_Issue_On_Exceeding_Balance_Ason_Date Then

                        If CDec(objAsonDateBalance.Text) < mdblCurrentLeavedays Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 20, "You cannot approve/issue this leave form.Reason : This Leave Limit has exceeded the Maximum Leave balance as on date Limit for this leave type."), Me)
                            Return False
                        End If

                    End If

                ElseIf CBool(drRow(0)("isshortleave")) Then
                    If CDec(drRow(0)("Accrue_Amount")) < (CDec(drRow(0)("Issue_amount")) + mdblCurrentLeavedays) Then
                        DisplayMessage.DisplayMessage("Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form.", Me)
                        Return False
                    End If

                End If

            End If

            'Pinkal (16-Dec-2016) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objLeaveIssueTran = Nothing
            objbalance = Nothing
            objlvForm = Nothing
            objLeaveType = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Function

    Private Sub FinalSave(ByVal clsPendingLeave As clspendingleave_Tran)
        Try

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmProcessLeave"
            StrModuleName2 = "mnuLeaveInformation"
            clsCommonATLog._WebClientIP = CStr(Session("IP_ADD"))
            clsCommonATLog._WebHostName = CStr(Session("HOST_NAME"))
            clsPendingLeave._WebFrmName = "frmProcessLeave"
            clsPendingLeave._WebClientIP = CStr(Session("IP_ADD"))
            clsPendingLeave._WebHostName = CStr(Session("HOST_NAME"))

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If

            If CInt(cboApprover.SelectedValue) <> 0 Then

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'If CInt(cboStatus.SelectedValue) <> 0 Then
                If xStatusId <> 0 Then
                    'Pinkal (25-May-2019) -- End
                    With clsPendingLeave
                        If mintPenddingLvTranUnkId > 0 Then
                            ._Pendingleavetranunkid = mintPenddingLvTranUnkId

                            If SetValue(clsPendingLeave) Then
                                'Pinkal (25-May-2019) -- Start
                                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                                'btnSave.Enabled = False
                                'Pinkal (25-May-2019) -- End

                                .Update(Session("Database_Name").ToString(), _
                                        CInt(Session("UserId")), _
                                        CInt(Session("Fin_year")), _
                                        CInt(Session("CompanyUnkId")), _
                                        Session("UserAccessModeSetting").ToString, True, _
                                        ConfigParameter._Object._CurrentDateAndTime, _
                                        CStr(Session("IsIncludeInactiveEmp")), _
                                        Session("EmployeeAsOnDate").ToString, _
                                        CDate(Session("fin_startdate")), _
                                        CDate(Session("fin_enddate")), _
                                        Session("LeaveApproverForLeaveType").ToString(), _
                                        CInt(Session("LeaveBalanceSetting")), _
                                        Session("IsAutomaticIssueOnFinalApproval").ToString(), CBool(Session("SkipEmployeeMovementApprovalFlow")), CBool(Session("CreateADUserFromEmpMst")))
                                'Sohail (21 Oct 2019) - [blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]

                                If ._Message.Length > 0 Then

                                    'Pinkal (25-May-2019) -- Start
                                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                                    'btnSave.Enabled = True
                                    'Pinkal (25-May-2019) -- End
                                    DisplayMessage.DisplayMessage("Entry not Updated. Reason : " & ._Message, Me)
                                Else
                                    DisplayMessage.DisplayMessage("Entry updated sucessfully." & ._Message, Me)
                                    Dim objLeaveForm As New clsleaveform
                                    objLeaveForm._Formunkid = mintLeaveFormUnkId

                                    Dim objLeaveType As New clsleavetype_master
                                    objLeaveType._Leavetypeunkid = objLeaveForm._Leavetypeunkid

                                    Dim objNet As New clsNetConnectivity
                                    If objNet._Conected Then

                                        'Pinkal (13-Mar-2019) -- Start
                                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                                        Language._Object._LangId = CInt(Session("LangId"))
                                        'Pinkal (13-Mar-2019) -- End


                                        'Pinkal (25-May-2019) -- Start
                                        'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                                        'clsPendingLeave.SendMailToApprover(Session("Database_Name").ToString(), _
                                        '                                       CInt(Session("UserId")), _
                                        '                                       CInt(Session("Fin_year")), _
                                        '                                       CInt(Session("CompanyUnkId")), _
                                        '                                       eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                        '                                       Session("UserAccessModeSetting").ToString(), True, _
                                        '                                       CBool(Session("IsIncludeInactiveEmp")), _
                                        '                                       mintEmpUnkId, _
                                        '                                       txtFormNo.Text.Trim, _
                                        '                                       mintPriority, _
                                        '                                       CInt(cboStatus.SelectedValue), _
                                        '                                       objLeaveType._Leavename, _
                                        '                                       mstrArutiSelfServiceURL, _
                                        '                                       enLogin_Mode.MGR_SELF_SERVICE)


                                        'Pinkal (11-Sep-2020) -- Start
                                        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                        If objLeaveForm._Statusunkid <> 7 Then

                                        clsPendingLeave.SendMailToApprover(Session("Database_Name").ToString(), _
                                                                               CInt(Session("UserId")), _
                                                                               CInt(Session("Fin_year")), _
                                                                               CInt(Session("CompanyUnkId")), _
                                                                               eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                               Session("UserAccessModeSetting").ToString(), True, _
                                                                               CBool(Session("IsIncludeInactiveEmp")), _
                                                                               mintEmpUnkId, _
                                                                               txtFormNo.Text.Trim, _
                                                                               mintPriority, _
                                                                              xStatusId, _
                                                                               objLeaveType._Leavename, _
                                                                               mstrArutiSelfServiceURL, _
                                                                               enLogin_Mode.MGR_SELF_SERVICE)

                                        End If
                                        'Pinkal (11-Sep-2020) -- End

                                        'Pinkal (25-May-2019) -- End


                                        If objLeaveForm._Statusunkid <> 2 Then

                                            Dim objEmp As New clsEmployee_Master
                                            objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = objLeaveForm._Employeeunkid

                                            objLeaveForm._EmployeeCode = objEmp._Employeecode
                                            objLeaveForm._EmployeeFirstName = objEmp._Firstname
                                            objLeaveForm._EmployeeMiddleName = objEmp._Othername
                                            objLeaveForm._EmployeeSurName = objEmp._Surname
                                            objLeaveForm._EmpMail = objEmp._Email

                                            objLeaveForm._WebFrmName = "frmProcessLeave"
                                            objLeaveForm._WebClientIP = Session("IP_ADD").ToString
                                            objLeaveForm._WebHostName = Session("HOST_NAME").ToString

                                            If objLeaveForm._Statusunkid = 1 Then


                                                'Pinkal (13-Mar-2019) -- Start
                                                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                                                Language._Object._LangId = CInt(Session("LangId"))
                                                'Pinkal (13-Mar-2019) -- End

                                                'Sohail (30 Nov 2017) -- Start
                                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                                'objLeaveForm.SendMailToUser(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date), txtFormNo.Text.Trim, objLeaveType._Leavename, clsPendingLeave._Startdate.Date, clsPendingLeave._Enddate.Date, Session("Firstname").ToString() & " " & Session("Surname").ToString(), "", "", enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                                objLeaveForm.SendMailToUser(objEmp._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date), txtFormNo.Text.Trim, objLeaveType._Leavename, clsPendingLeave._Startdate.Date, clsPendingLeave._Enddate.Date, CInt(Session("CompanyUnkId")), Session("Firstname").ToString() & " " & Session("Surname").ToString(), "", "", enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")))
                                                'Sohail (30 Nov 2017) -- End
                                            End If


                                            'Pinkal (03-May-2019) -- Start
                                            'Enhancement - Working on Leave UAT Changes for NMB.

                                            '*************** START TO SEND RELIEVER EMAIL NOTIFICATION************************
                                            If CBool(Session("SetRelieverAsMandatoryForApproval")) AndAlso objLeaveForm._Statusunkid = 7 AndAlso clsPendingLeave._RelieverEmpId > 0 Then

                                                'Pinkal (03-Jul-2019) -- Start
                                                'Bug - Solved Problem for Blank Email Notification to Leave Approver for Reliever.
                                                'objLeaveForm.SendMailToReliever(CInt(Session("CompanyUnkId")), clsPendingLeave._RelieverEmpId, objLeaveForm._Leavetypeunkid _
                                                '                                                , objLeaveForm._Formunkid, objLeaveForm._Formno, txtEmployee.Text.Trim(), clsPendingLeave._Startdate, clsPendingLeave._Enddate _
                                                '                                                , CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE)

                                                objLeaveForm.SendMailToReliever(CInt(Session("CompanyUnkId")), clsPendingLeave._RelieverEmpId, objLeaveForm._Leavetypeunkid _
                                                                                                , objLeaveForm._Formunkid, objLeaveForm._Formno, txtEmployee.Text.Trim(), clsPendingLeave._Startdate, clsPendingLeave._Enddate _
                                                                                                , CInt(Session("UserId")), objLeaveForm._Statusunkid, "", enLogin_Mode.MGR_SELF_SERVICE)

                                                'Pinkal (27-Jun-2019) -- End
                                            End If
                                            '*************** END TO SEND RELIEVER EMAIL NOTIFICATION************************

                                            'Pinkal (03-May-2019) -- End


                                            If objEmp._Email.Trim.Length <= 0 Then GoTo CloseForm
                                            If objLeaveForm._Statusunkid = 1 OrElse objLeaveForm._Statusunkid = 7 Then
                                                Dim objCommonCode As New CommonCodes
                                                Dim Path As String = My.Computer.FileSystem.SpecialDirectories.Temp
                                                Dim strPath As String = ""


                                                'Pinkal (16-Dec-2016) -- Start
                                                'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance..

                                                If mintClaimMstUnkId > 0 Then
                                                    If CBool(Session("IsAutomaticIssueOnFinalApproval")) AndAlso CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                                                        'strPath = objCommonCode.Export_ELeaveForm(Path, txtFormNo.Text.Trim, mintEmpUnkId, objLeaveForm._Formunkid, CInt(Session("LeaveBalanceSetting")), CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date, True)
                                                        strPath = objCommonCode.Export_ELeaveForm(Path, txtFormNo.Text.Trim, mintEmpUnkId, objLeaveForm._Formunkid, CInt(Session("LeaveBalanceSetting")) _
                                                                                                                              , CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date, True _
                                                                                                                              , CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")))
                                                    ElseIf CBool(Session("IsAutomaticIssueOnFinalApproval")) AndAlso CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then
                                                        'strPath = objCommonCode.Export_ELeaveForm(Path, txtFormNo.Text.Trim, mintEmpUnkId, objLeaveForm._Formunkid, CInt(Session("LeaveBalanceSetting")), CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date, False)
                                                        strPath = objCommonCode.Export_ELeaveForm(Path, txtFormNo.Text.Trim, mintEmpUnkId, objLeaveForm._Formunkid, CInt(Session("LeaveBalanceSetting")) _
                                                                                                                             , CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date, False _
                                                                                                                             , CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")))
                                                    Else
                                                        'strPath = objCommonCode.Export_ELeaveForm(Path, txtFormNo.Text.Trim, mintEmpUnkId, objLeaveForm._Formunkid, CInt(Session("LeaveBalanceSetting")), CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date, False)
                                                        strPath = objCommonCode.Export_ELeaveForm(Path, txtFormNo.Text.Trim, mintEmpUnkId, objLeaveForm._Formunkid, CInt(Session("LeaveBalanceSetting")) _
                                                                                                                            , CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date, False _
                                                                                                                            , CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")))
                                                    End If
                                                Else
                                                    'strPath = objCommonCode.Export_ELeaveForm(Path, txtFormNo.Text.Trim, mintEmpUnkId, objLeaveForm._Formunkid, CInt(Session("LeaveBalanceSetting")), CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date, True)
                                                    strPath = objCommonCode.Export_ELeaveForm(Path, txtFormNo.Text.Trim, mintEmpUnkId, objLeaveForm._Formunkid, CInt(Session("LeaveBalanceSetting")) _
                                                                                                                         , CInt(Session("Fin_year")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date, True _
                                                                                                                          , CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")))
                                                End If

                                                'Pinkal (16-Dec-2016) -- End

                                                'Pinkal (13-Mar-2019) -- Start
                                                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                                                Language._Object._LangId = CInt(Session("LangId"))
                                                'Pinkal (13-Mar-2019) -- End


                                                'Pinkal (01-Apr-2019) -- Start
                                                'Enhancement - Working on Leave Changes for NMB.
                                                'objLeaveForm.SendMailToEmployee(mintEmpUnkId, objLeaveType._Leavename, clsPendingLeave._Startdate.Date, clsPendingLeave._Enddate.Date, objLeaveForm._Statusunkid, CInt(Session("CompanyUnkId")), "", Path, strPath, enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), "", mintClaimMstUnkId)
                                                objLeaveForm.SendMailToEmployee(mintEmpUnkId, objLeaveType._Leavename, clsPendingLeave._Startdate.Date, clsPendingLeave._Enddate.Date _
                                                                                                   , objLeaveForm._Statusunkid, CInt(Session("CompanyUnkId")), "", Path, strPath, enLogin_Mode.MGR_SELF_SERVICE _
                                                                                                   , 0, CInt(Session("UserId")), "", mintClaimMstUnkId, objLeaveForm._Formno)
                                                'Pinkal (01-Apr-2019) -- End




                                                mstrFilePath = Path & "\" & strPath
                                            Else

                                                'Pinkal (13-Mar-2019) -- Start
                                                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                                                Language._Object._LangId = CInt(Session("LangId"))
                                                'Pinkal (13-Mar-2019) -- End


                                                'Pinkal (25-May-2019) -- Start
                                                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                                                'If CInt(cboStatus.SelectedValue) = 3 OrElse CInt(cboStatus.SelectedValue) = 4 Then
                                                If xStatusId = 3 OrElse xStatusId = 4 Then
                                                    'Pinkal (25-May-2019) -- End


                                                    'Pinkal (01-Apr-2019) -- Start
                                                    'Enhancement - Working on Leave Changes for NMB.
                                                    'objLeaveForm.SendMailToEmployee(mintEmpUnkId, objLeaveType._Leavename, clsPendingLeave._Startdate.Date, clsPendingLeave._Enddate.Date, objLeaveForm._Statusunkid, CInt(Session("CompanyUnkId")), "", "", "", enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), txtRemark.Text.Trim, mintClaimMstUnkId)
                                                    objLeaveForm.SendMailToEmployee(mintEmpUnkId, objLeaveType._Leavename, clsPendingLeave._Startdate.Date, clsPendingLeave._Enddate.Date _
                                                                                                       , objLeaveForm._Statusunkid, CInt(Session("CompanyUnkId")), "", "", "", enLogin_Mode.MGR_SELF_SERVICE _
                                                                                                       , 0, CInt(Session("UserId")), txtRemark.Text.Trim, mintClaimMstUnkId, objLeaveForm._Formno)
                                                    'Pinkal (01-Apr-2019) -- End



                                                Else

                                                    'Pinkal (01-Apr-2019) -- Start
                                                    'Enhancement - Working on Leave Changes for NMB.
                                                    'objLeaveForm.SendMailToEmployee(mintEmpUnkId, objLeaveType._Leavename, clsPendingLeave._Startdate.Date, clsPendingLeave._Enddate.Date, objLeaveForm._Statusunkid, CInt(Session("CompanyUnkId")), "", "", "", enLogin_Mode.MGR_SELF_SERVICE, 0, CInt(Session("UserId")), txtRemark.Text.Trim, mintClaimMstUnkId)
                                                    objLeaveForm.SendMailToEmployee(mintEmpUnkId, objLeaveType._Leavename, clsPendingLeave._Startdate.Date, clsPendingLeave._Enddate.Date _
                                                                                                        , objLeaveForm._Statusunkid, CInt(Session("CompanyUnkId")), "", "", "", enLogin_Mode.MGR_SELF_SERVICE _
                                                                                                        , 0, CInt(Session("UserId")), txtRemark.Text.Trim, mintClaimMstUnkId, objLeaveForm._Formno)
                                                    'Pinkal (01-Apr-2019) -- End



                                                End If
                                            End If

                                            If CBool(Session("IsAutomaticIssueOnFinalApproval")) AndAlso CStr(Session("Notify_IssuedLeave_Users")).Trim.Length > 0 AndAlso clsPendingLeave._Statusunkid = 7 Then
                                                'Dim objMaster As New clsMasterData

                                                'Pinkal (08-Feb-2022) -- Start
                                                'Enhancement NMB  - Language Change Issue for All Modules.
                                                ' gobjEmailList = New List(Of clsEmailCollection)
                                                'Pinkal (08-Feb-2022) -- End

                                                Dim objUsr As New clsUserAddEdit : Dim StrMessage As String = String.Empty
                                                Dim objEmployee As New clsEmployee_Master
                                                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = mintEmpUnkId
                                                For Each sId As String In CStr(Session("Notify_IssuedLeave_Users")).Trim.Trim.Split(CChar(","))
                                                    If sId.Trim = "" Then Continue For
                                                    Dim dtUserAccess As DataTable = objUsr.GetUserAccessFromUser(CInt(sId), CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), CStr(Session("UserAccessModeSetting")))
                                                    If dtUserAccess IsNot Nothing AndAlso dtUserAccess.Rows.Count > 0 Then
                                                        Dim mblnFlag As Boolean = False
                                                        For Each AID As String In CStr(Session("UserAccessModeSetting")).Trim.Split(CChar(","))
                                                            Dim drRow() As DataRow = Nothing
                                                            Select Case CInt(AID)
                                                                Case enAllocation.DEPARTMENT
                                                                    drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Departmentunkid & " AND referenceunkid = " & enAllocation.DEPARTMENT)
                                                                    If drRow.Length > 0 Then
                                                                        mblnFlag = True
                                                                    Else
                                                                        mblnFlag = False
                                                                        Exit For
                                                                    End If
                                                                Case enAllocation.JOBS
                                                                    drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Jobunkid & " AND referenceunkid = " & enAllocation.JOBS)
                                                                    If drRow.Length > 0 Then
                                                                        mblnFlag = True
                                                                    Else
                                                                        mblnFlag = False
                                                                        Exit For
                                                                    End If
                                                                Case enAllocation.CLASS_GROUP
                                                                    drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Classgroupunkid & " AND referenceunkid = " & enAllocation.CLASS_GROUP)
                                                                    If drRow.Length > 0 Then
                                                                        mblnFlag = True
                                                                    Else
                                                                        mblnFlag = False
                                                                        Exit For
                                                                    End If
                                                                Case enAllocation.CLASSES
                                                                    drRow = dtUserAccess.Select("allocationunkid = " & objEmployee._Classunkid & " AND referenceunkid = " & enAllocation.CLASSES)
                                                                    If drRow.Length > 0 Then
                                                                        mblnFlag = True
                                                                    Else
                                                                        mblnFlag = False
                                                                        Exit For
                                                                    End If
                                                                Case Else
                                                                    mblnFlag = False
                                                            End Select
                                                        Next

                                                        If mblnFlag Then
                                                            'Language.setLanguage(mstrModuleName)
                                                            Dim objLeaveIssueMaster As New clsleaveissue_master
                                                            objUsr._Userunkid = CInt(sId)

                                                            'Pinkal (08-Feb-2022) -- Start
                                                            'Enhancement NMB  - Language Change Issue for All Modules.
                                                            'Language._Object._LangId = CInt(Session("LangId"))
                                                            'Pinkal (08-Feb-2022) -- End

                                                            StrMessage = objLeaveIssueMaster.SetNotificationForIssuedUser(objUsr._Firstname & " " & objUsr._Lastname, txtEmployee.Text, objLeaveForm._Formno, objLeaveType._Leavename, clsPendingLeave._Startdate.Date, clsPendingLeave._Enddate.Date)

                                                            'Pinkal (08-Feb-2022) -- Start
                                                            'Enhancement NMB  - Language Change Issue for All Modules.
                                                            'gobjEmailList.Add(New clsEmailCollection(objUsr._Email, Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 13, "Issued Leave Application form notification"), StrMessage, mstrModuleName, 0, Session("IP_ADD").ToString, Session("HOST_NAME").ToString, CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE, clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT, objUsr._Email))
                                                            objLeaveEmailList.Add(New clsEmailCollection(objUsr._Email, Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "Issued Leave Application form notification"), StrMessage, mstrModuleName, 0, Session("IP_ADD").ToString, Session("HOST_NAME").ToString, CInt(Session("UserId")), enLogin_Mode.MGR_SELF_SERVICE, clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT, objUsr._Email))
                                                            'Pinkal (08-Feb-2022) -- End

                                                            objLeaveIssueMaster = Nothing
                                                        End If
                                                    End If
                                                Next
                                                'Pinkal (11-Sep-2020) -- Start
                                                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                                objUsr = Nothing
                                                objEmployee = Nothing
                                                'Pinkal (11-Sep-2020) -- End
                                            End If
                                            Send_Notification()

                                            'Pinkal (11-Sep-2020) -- Start
                                            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                            objEmp = Nothing
                                            'Pinkal (11-Sep-2020) -- End

                                        End If

                                    End If
CloseForm:
                                    If CBool(Session("isUrlPost")) = True Then
                                        SetUserTracingInfo(False, enUserMode.Loging, enLogin_Mode.MGR_SELF_SERVICE, CInt(Session("UserId")), True, Session("mdbname").ToString, CInt(Session("CompanyUnkId")), CInt(Session("Fin_year")), Session("IP_ADD").ToString(), Session("HOST_NAME").ToString())
                                        Session.Abandon()
                                        Response.Redirect("../Index.aspx", False)
                                    Else
                                        Response.Redirect(Session("rootpath").ToString & "Leave/wPg_ProcessLeaveList.aspx", False)
                                    End If

                                    'Pinkal (11-Sep-2020) -- Start
                                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                                    objNet = Nothing
                                    objLeaveType = Nothing
                                    objLeaveForm = Nothing
                                    'Pinkal (11-Sep-2020) -- End

                                End If

                            End If
                        End If
                    End With
                Else
                    DisplayMessage.DisplayMessage("Please Select Status.", Me)
                End If
            Else
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Please Select Approver.", Me)
                'Sohail (23 Mar 2019) -- End
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FinalSave:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Function SetValue(ByVal clsPendingLeave As clspendingleave_Tran) As Boolean
        Dim blnFlag As Boolean = False
        Try
            With clsPendingLeave

                ._Formunkid = mintLeaveFormUnkId
                ._Employeeunkid = mintEmpUnkId
                ._Approverunkid = CInt(cboApprover.SelectedValue)

                If dtpStartdate.IsNull = True Then
                    ._Startdate = Nothing
                Else
                    ._Startdate = dtpStartdate.GetDate
                End If

                If dtpEnddate.IsNull = True Then
                    ._Enddate = Nothing
                Else
                    ._Enddate = dtpEnddate.GetDate
                End If

                If dtpApplyDate.IsNull = True Then
                    ._Approvaldate = Nothing
                Else
                    ._Approvaldate = dtpApplyDate.GetDate
                End If

                ._Remark = txtRemark.Text

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                '._Statusunkid = CInt(cboStatus.SelectedValue)
                ._Statusunkid = xStatusId
                'Pinkal (25-May-2019) -- End

                ._Amount = CDec(txtAllowance.Text)
                ._Userunkid = CInt(Session("UserId"))

                Dim dtFraction As DataTable = mdtFraction


                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'If CInt(cboStatus.SelectedValue) = 1 Then
                If xStatusId = 1 Then
                    'Pinkal (25-May-2019) -- End
                    ._DtFraction = dtFraction
                    If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count > 0 Then
                        ._DayFraction = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD<> 'D'"))
                    Else
                        ._DayFraction = 0
                    End If

                    'Pinkal (25-May-2019) -- Start
                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                    'ElseIf CInt(cboStatus.SelectedValue) = 3 Or CInt(cboStatus.SelectedValue) = 4 Then    'REJECT OR RESCHEDULE
                ElseIf xStatusId = 3 Or xStatusId = 4 Then    'REJECT OR RESCHEDULE
                    'Pinkal (25-May-2019) -- End
                    ._DtFraction = dtFraction
                    For Each dr As DataRow In ._DtFraction.Rows
                        dr("AUD") = "D"
                    Next
                    ._DtFraction.AcceptChanges()
                    ._DayFraction = 0
                End If
                ._YearId = CInt(Session("Fin_year"))

                Dim objClaimRequestMst As New clsclaim_request_master
                mintClaimMstUnkId = objClaimRequestMst.GetClaimRequestMstIDFromLeaveForm(mintLeaveFormUnkId)
                objClaimRequestMst = Nothing

                ._ClaimRequestMasterId = mintClaimMstUnkId
                ._PaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))

                mdtExpense = mdtFinalClaimTransaction


                'Pinkal (10-Jan-2017) -- Start
                'Enhancement - Working on TRA C&R Module Changes with Leave Module.

                'If mdtFinalClaimTransaction Is Nothing Then
                '    Dim objExpApproverTran As New clsclaim_request_approval_tran
                '    mdtExpense = objExpApproverTran.GetApproverExpesneList("List", False, _
                '                                                          CBool(Session("PaymentApprovalwithLeaveApproval")), _
                '                                                          Session("Database_Name").ToString, _
                '                                                          CInt(Session("UserId")), _
                '                                                          Session("EmployeeAsOnDate").ToString, _
                '                                                          enExpenseType.EXP_LEAVE, _
                '                                                          False, True, CInt(IIf(CBool(Session("PaymentApprovalwithLeaveApproval")), ._Approvertranunkid, -1)), , _
                '                                                          ._ClaimRequestMasterId).Tables(0)

                '    objExpApproverTran = Nothing
                'End If

                'If CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                '    If mdtExpense IsNot Nothing Then
                '        Dim drRow() As DataRow = mdtExpense.Select("AUD = '' ")
                '        If drRow.Length > 0 Then
                '            For Each dr As DataRow In drRow
                '                dr("AUD") = "U"
                '            Next
                '        End If
                '    End If
                'End If


                If mdtFinalClaimTransaction Is Nothing AndAlso ._ClaimRequestMasterId > 0 Then

                    Dim objExpApproverTran As New clsclaim_request_approval_tran
                    mdtExpense = objExpApproverTran.GetApproverExpesneList("List", False, _
                                                                          CBool(Session("PaymentApprovalwithLeaveApproval")), _
                                                                          Session("Database_Name").ToString, _
                                                                          CInt(Session("UserId")), _
                                                                          Session("EmployeeAsOnDate").ToString, _
                                                                          enExpenseType.EXP_LEAVE, _
                                                                          False, True, CInt(IIf(CBool(Session("PaymentApprovalwithLeaveApproval")), ._Approvertranunkid, -1)), , _
                                                                          ._ClaimRequestMasterId).Tables(0)


                    'Pinkal (22-Mar-2018) -- Start
                    'Bug - 0002108: Assess and find if there are pending issues in Leave module and all of them should be sorted at once.

                    'If CBool(Session("PaymentApprovalwithLeaveApproval")) Then

                    '    If mdtExpense IsNot Nothing Then
                    '        Dim drRow() As DataRow = mdtExpense.Select("AUD = '' ")
                    '        If drRow.Length > 0 Then
                    '            For Each dr As DataRow In drRow
                    '                dr("AUD") = "U"
                    '            Next
                    '        End If

                    '    End If

                    'End If

                    'Pinkal (22-Mar-2018) -- End

                    objExpApproverTran = Nothing

                End If

                'Pinkal (10-Jan-2017) -- End

                'Pinkal (22-Mar-2018) -- Start
                'Bug - 0002108: Assess and find if there are pending issues in Leave module and all of them should be sorted at once.
                If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso ._ClaimRequestMasterId > 0 Then
                    If mdtExpense IsNot Nothing Then
                        Dim drRow() As DataRow = mdtExpense.Select("AUD = '' ")
                        If drRow.Length > 0 Then
                            For Each dr As DataRow In drRow
                                dr("AUD") = "U"
                            Next
                        End If
                    End If
                End If
                'Pinkal (22-Mar-2018) -- End

                If mdtExpense IsNot Nothing AndAlso mdtExpense.Columns.Contains("crapprovaltranunkid") Then
                    mdtExpense.Columns.Remove("crapprovaltranunkid")
                End If

                ._DtExpense = mdtExpense
                ._CompanyID = CInt(Session("CompanyUnkId"))
                ._LoginMode = enLogin_Mode.MGR_SELF_SERVICE
                ._ArutiSelfServiceURL = Session("ArutiSelfServiceURL").ToString()



                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                If CBool(Session("SetRelieverAsMandatoryForApproval")) Then
                    ._RelieverEmpId = CInt(cboReliever.SelectedValue)
                End If
                'Pinkal (01-Oct-2018) -- End


                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'If objClaimPendingMst IsNot Nothing AndAlso CInt(cboStatus.SelectedValue) = 1 Then
                If objClaimPendingMst IsNot Nothing AndAlso xStatusId = 1 Then
                    'Pinkal (25-May-2019) -- End
                    ._dtClaimAttchment = mdtFinalClaimAttchment
                    ._ObjClaimRequestMst = objClaimPendingMst
                    ._DtExpense = mdtLeaveExpenseTran

                    Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                    Dim strError As String = ""
                    Dim mstrFolderName As String = ""
                    Dim mdsDoc As DataSet

                    mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
                    Dim strFileName As String = ""

                    If mdtFinalClaimAttchment IsNot Nothing Then
                        strError = ""
                        mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.CLAIM_REQUEST).Tables(0).Rows(0)("Name").ToString

                        For Each dRow As DataRow In mdtFinalClaimAttchment.Rows
                            mstrFolderName = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(dRow("scanattachrefid"))) Select (p.Item("Name").ToString)).FirstOrDefault
                            If dRow("AUD").ToString = "A" AndAlso dRow("orgfilepath").ToString <> "" Then
                                strFileName = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(dRow("orgfilepath")))
                                If File.Exists(CStr(dRow("orgfilepath"))) Then
                                    Dim strPath As String = Session("ArutiSelfServiceURL").ToString
                                    If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                        strPath += "/"
                                    End If
                                    strPath += "uploadimage/" & mstrFolderName & "/" + strFileName

                                    If Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName)) = False Then
                                        Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/UploadImage/" & mstrFolderName))
                                    End If

                                    File.Move(CStr(dRow("orgfilepath")), Server.MapPath("~/uploadimage/" & mstrFolderName & "/" + strFileName))
                                    dRow("fileuniquename") = strFileName
                                    dRow("filepath") = strPath
                                    dRow("form_name") = mstrModuleName
                                    dRow.AcceptChanges()
                                Else
                                    'Sohail (23 Mar 2019) -- Start
                                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                    'DisplayMessage.DisplayError(ex, Me)
                                    DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                                    'Sohail (23 Mar 2019) -- End
                                    Exit Function
                                End If
                            ElseIf dRow("AUD").ToString = "D" AndAlso dRow("fileuniquename").ToString <> "" Then
                                Dim strFilepath As String = CStr(dRow("filepath"))
                                Dim strArutiSelfService As String = Session("ArutiSelfServiceURL").ToString
                                If strFilepath.Contains(strArutiSelfService) Then
                                    strFilepath = strFilepath.Replace(strArutiSelfService, "")
                                    If Strings.Left(strFilepath, 1) <> "/" Then
                                        strFilepath = "~/" & strFilepath
                                    Else
                                        strFilepath = "~" & strFilepath
                                    End If

                                    If File.Exists(Server.MapPath(strFilepath)) Then
                                        File.Delete(Server.MapPath(strFilepath))
                                    Else
                                        'Sohail (23 Mar 2019) -- Start
                                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                        'DisplayMessage.DisplayError(ex, Me)
                                        DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                                        'Sohail (23 Mar 2019) -- End
                                        Exit Function
                                    End If
                                Else
                                    'Sohail (23 Mar 2019) -- Start
                                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                    'DisplayMessage.DisplayError(ex, Me)
                                    DisplayMessage.DisplayMessage("File does not exist on localpath", Me)
                                    'Sohail (23 Mar 2019) -- End
                                    Exit Function
                                End If
                            End If
                        Next
                    End If


                End If
                'Pinkal (10-Jan-2017) -- End

                If mdtExpense IsNot Nothing AndAlso mdtExpense.Rows.Count > 0 Then

                    'Pinkal (25-May-2019) -- Start
                    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                    'If (CInt(cboStatus.SelectedValue)) = 1 AndAlso mdtExpense.Rows.Count > 0 Then
                    If xStatusId = 1 AndAlso mdtExpense.Rows.Count > 0 Then
                        'Pinkal (25-May-2019) -- End
                        If IsValidExpense_Eligility() = False Then
                            'Pinkal (25-May-2019) -- Start
                            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                            'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 21, "Sorry,You cannot view/edit expense for this leave type.Reason : Eligibility to view/edit for this leave type does not match with eligibility days set on selected leave type. Eligible days set for the selected leave type is") & " (" & intEligibleExpenseDays & ").", Me)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 21, "Sorry,You cannot view/edit expense for this leave type.Reason : Eligibility to view/edit for this leave type does not match with eligibility days set on selected leave type."), Me)
                            'Pinkal (25-May-2019) -- End
                            Exit Function
                        End If
                    End If
                End If
                'Pinkal (29-Sep-2017) -- End

            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return True
    End Function

    Private Sub Send_Notification()
        Try

            'Pinkal (08-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.
            'If gobjEmailList.Count > 0 Then
            If objLeaveEmailList.Count > 0 Then
                'Pinkal (08-Feb-2022) -- End

                Dim objSendMail As New clsSendMail

                'Pinkal (08-Feb-2022) -- Start
                'Enhancement NMB  - Language Change Issue for All Modules.
                '  For Each obj In gobjEmailList
                For Each obj In objLeaveEmailList
                    'Pinkal (08-Feb-2022) -- End
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = mstrModuleName
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.MGR_SELF_SERVICE
                    objSendMail._UserUnkid = CInt(Session("UserId"))
                    objSendMail._SenderAddress = Company._Object._Senderaddress
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LEAVE_MGT
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        objSendMail.SendMail(CInt(Session("CompanyUnkId")))
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception
                        Throw ex
                    End Try
                Next

                'Pinkal (08-Feb-2022) -- Start
                'Enhancement NMB  - Language Change Issue for All Modules.
                '  gobjEmailList.Clear()
                objLeaveEmailList.Clear()
                'Pinkal (08-Feb-2022) -- End

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objSendMail = Nothing
                'Pinkal (11-Sep-2020) -- End
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Pinkal (08-Feb-2022) -- Start
            'Enhancement NMB  - Language Change Issue for All Modules.
            'If gobjEmailList.Count > 0 Then
            '    gobjEmailList.Clear()
            'End If
            If objLeaveEmailList.Count > 0 Then
                objLeaveEmailList.Clear()
            End If
            'Pinkal (08-Feb-2022) -- End
        End Try
    End Sub


    'Pinkal (16-Dec-2016) -- Start
    'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.

    Private Sub GetAsOnDateAccrue()
        Try

            Dim objLeaveForm As New clsleaveform
            objLeaveForm._Formunkid = mintLeaveFormUnkId

            Dim objLvAccrue As New clsleavebalance_tran
            Dim mdecAsonDateAccrue As Decimal = 0
            Dim mdecAsonDateBalance As Decimal = 0


            'Pinkal (06-Apr-2018) -- Start
            'bug - 0002164  Employee unable to apply for leave. message indicating that leave has exceeded max limit [CCK].

            'objLvAccrue.GetAsonDateAccrue(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date _
            '                                             , CInt(Session("LeaveBalanceSetting")), mintEmpUnkId, objLeaveForm._Leavetypeunkid _
            '                                             , dtpEnddate.GetDate.Date, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
            '                                             , mdecAsonDateAccrue, mdecAsonDateBalance)

            objLvAccrue.GetAsonDateAccrue(CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date _
                                                         , CInt(Session("LeaveBalanceSetting")), mintEmpUnkId, objLeaveForm._Leavetypeunkid _
                                                         , dtpEnddate.GetDate.Date, CInt(Session("LeaveAccrueTenureSetting")), CInt(Session("LeaveAccrueDaysAfterEachMonth")) _
                                                         , mdecAsonDateAccrue, mdecAsonDateBalance, CInt(Session("Fin_year")))

            'Pinkal (06-Apr-2018) -- End


            objAsonDateAccrue.Text = mdecAsonDateAccrue.ToString()
            objAsonDateBalance.Text = mdecAsonDateBalance.ToString()

            objLeaveForm = Nothing
            objLvAccrue = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "GetAsOnDateAccrue", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Pinkal (16-Dec-2016) -- End

    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private Sub FillReliver()
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objPendingLeave As New clspendingleave_Tran
        'Pinkal (11-Sep-2020) -- End
        Try
            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = mintEmpUnkId


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            Dim mstrSearching As String = ""
            objPendingLeave._Pendingleavetranunkid = mintPenddingLvTranUnkId

            If objPendingLeave._RelieverEmpId > 0 Then
                mstrSearching = "AND (hremployee_master.employeeunkid <> " & mintEmpUnkId & " "
            Else
                mstrSearching = "AND hremployee_master.employeeunkid <> " & mintEmpUnkId & " "
            End If

            If chkIncludeAllStaff.Checked = False Then

                'Pinkal (13-Mar-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                If Session("RelieverAllocation") IsNot Nothing AndAlso Session("RelieverAllocation").ToString().Length > 0 Then

                    Dim arAllocations() As String = Session("RelieverAllocation").ToString().Split(CChar(","))

                    If arAllocations.Length > 0 Then

                        For i As Integer = 0 To arAllocations.Length - 1

                            Select Case CType(arAllocations(i), enAllocation)

                                Case enAllocation.BRANCH

                                    If objEmployee._Stationunkid > 0 Then
                                        mstrSearching &= "AND T.stationunkid = " & objEmployee._Stationunkid & " "
                                    End If

                                Case enAllocation.DEPARTMENT_GROUP

                                    If objEmployee._Deptgroupunkid > 0 Then
                                        mstrSearching &= "AND T.deptgroupunkid = " & objEmployee._Deptgroupunkid & " "
                                    End If

                                Case enAllocation.DEPARTMENT

                                    If objEmployee._Departmentunkid > 0 Then
                                        mstrSearching &= "AND T.departmentunkid = " & objEmployee._Departmentunkid & " "
                                    End If

                                Case enAllocation.SECTION_GROUP

                                    If objEmployee._Sectiongroupunkid > 0 Then
                                        mstrSearching &= "AND T.sectiongroupunkid = " & objEmployee._Sectiongroupunkid & " "
                                    End If

                                Case enAllocation.SECTION

                                    If objEmployee._Sectionunkid > 0 Then
                                        mstrSearching &= "AND T.sectionunkid = " & objEmployee._Sectionunkid & " "
                                    End If

                                Case enAllocation.UNIT_GROUP

                                    If objEmployee._Unitgroupunkid > 0 Then
                                        mstrSearching &= "AND T.unitgroupunkid = " & objEmployee._Unitgroupunkid & " "
                                    End If

                                Case enAllocation.UNIT

                                    If objEmployee._Unitunkid > 0 Then
                                        mstrSearching &= "AND T.unitunkid = " & objEmployee._Unitunkid & " "
                                    End If

                                Case enAllocation.TEAM

                                    If objEmployee._Teamunkid > 0 Then
                                        mstrSearching &= "AND T.teamunkid = " & objEmployee._Teamunkid & " "
                                    End If

                                Case enAllocation.JOB_GROUP

                                    If objEmployee._Jobgroupunkid > 0 Then
                                        mstrSearching &= "AND J.jobgroupunkid = " & objEmployee._Jobgroupunkid & " "
                                    End If

                                Case enAllocation.JOBS

                                    If objEmployee._Jobunkid > 0 Then
                                        mstrSearching &= "AND J.jobunkid = " & objEmployee._Jobunkid & " "
                                    End If

                                Case enAllocation.CLASS_GROUP

                                    If objEmployee._Classgroupunkid > 0 Then
                                        mstrSearching &= "AND T.classgroupunkid = " & objEmployee._Classgroupunkid & " "
                                    End If

                                Case enAllocation.CLASSES

                                    If objEmployee._Classunkid > 0 Then
                                        mstrSearching &= "AND T.classunkid = " & objEmployee._Classunkid & " "
                                    End If

                                Case enAllocation.COST_CENTER

                                    If objEmployee._Costcenterunkid > 0 Then
                                        mstrSearching &= "AND C.costcenterunkid = " & objEmployee._Costcenterunkid & " "
                                    End If

                            End Select

                        Next

                    End If

                End If

            End If

            If objPendingLeave._RelieverEmpId > 0 Then
                mstrSearching &= ") OR hremployee_master.employeeunkid in (" & objPendingLeave._RelieverEmpId & ") "
            End If

            'Pinkal (03-May-2019) -- End


            'If objEmployee._Gradegroupunkid > 0 Then
            '    mstrSearching &= "AND G.gradegroupunkid = " & objEmployee._Gradegroupunkid & " "
            'End If

            'If objEmployee._Gradeunkid > 0 Then
            '    mstrSearching &= "AND G.gradeunkid = " & objEmployee._Gradeunkid & " "
            'End If

            'If objEmployee._Gradelevelunkid > 0 Then
            '    mstrSearching &= "AND G.gradelevelunkid = " & objEmployee._Gradelevelunkid & " "
            'End If

            'Pinkal (13-Mar-2019) -- End


            If mstrSearching.Trim.Length > 0 Then
                mstrSearching = mstrSearching.Substring(3)
            End If


            Dim dsEmpList As DataSet = objEmployee.GetEmployeeList(CStr(Session("Database_Name")), _
                                    CInt(Session("UserId")), _
                                    CInt(Session("Fin_year")), _
                                    CInt(Session("CompanyUnkId")), _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                    CStr(Session("UserAccessModeSetting")), True, _
                                    False, "Reliver", True, 0, , , , , , , , , , , , , , mstrSearching, , False)

            With cboReliever
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsEmpList.Tables(0)
                .DataBind()
                .SelectedIndex = 0
            End With


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsEmpList IsNot Nothing Then dsEmpList.Clear()
            dsEmpList = Nothing
            'Pinkal (11-Sep-2020) -- End


            objEmployee = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objPendingLeave = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    'Pinkal (01-Oct-2018) -- End


    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    Private Function LeaveDateValidation(ByVal mdtStartDate As Date, ByVal mdtenddate As Date) As Boolean
        Try
            If dtpStartDate.GetDate.Date < mdtStartDate.Date OrElse dtpStartDate.GetDate.Date > mdtenddate.Date Then

                'Pinkal (05-Jun-2020) -- Start
                'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 27, "Please select proper") & " " & lblStartdate.Text & "." & lblStartdate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "should be between") & " " & mdtStartDate.Date & " and " & mdtenddate.Date & ".", Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Please select proper") & " " & lblStartdate.Text & "." & lblStartdate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "should be between") & " " & mdtStartDate.Date & " and " & mdtenddate.Date & ".", Me)
                'Pinkal (05-Jun-2020) -- End
                Return False
            ElseIf dtpEndDate.GetDate.Date < mdtStartDate.Date OrElse dtpEndDate.GetDate.Date > mdtenddate.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 28, "Please select proper") & " " & lblEnddate.Text & "." & lblEnddate.Text & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "should be between") & " " & mdtStartDate.Date & " and " & mdtenddate.Date & ".", Me)
                Return False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function
    'Pinkal (26-Feb-2019) -- End


#Region "Expense"

    Private Sub SetVisibility()
        Try
            If CInt(Session("ClaimRequestVocNoType")) = 1 Then
                txtClaimNo.Enabled = False
            End If
            cboReference.Enabled = False
            cboClaimEmployee.Enabled = False

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.[On claim form when applying for leave expense, they want the leave form field removed.]
            objlblValue.Visible = False
            cboReference.Visible = False
            objApprovallblValue.Visible = False
            cboApprovalReference.Visible = False
            'Pinkal (25-May-2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("SetVisibility" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub FillClaimCombo()
        Dim objPrd As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Dim objEMaster As New clsEmployee_Master
        Try


            'Pinkal (01-Mar-2016) -- Start
            'Enhancement - Implementing External Approver in Claim Request & Leave Module.

            'dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
            '                                     CInt(Session("UserId")), _
            '                                     CInt(Session("Fin_year")), _
            '                                     CInt(Session("CompanyUnkId")), _
            '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
            '                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
            '                                     Session("UserAccessModeSetting").ToString(), True, _
            '                                     CBool(Session("IsIncludeInactiveEmp")), "List", False, _
            '                                     mintEmpUnkId)

            dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "List", False, _
                                              mintEmpUnkId, , , , , , , , , , , , , , , , CBool(IIf(mblnIsExternalApprover, False, True)))

            'Pinkal (01-Mar-2016) -- End

            With cboClaimEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List")
            With cboExpCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            Call cboExpCategory_SelectedIndexChanged(cboExpCategory, New EventArgs())


            'Pinkal (20-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            If CBool(Session("SectorRouteAssignToEmp")) = True AndAlso CBool(Session("SectorRouteAssignToExpense")) = False Then
                Dim objAssignEmp As New clsassignemp_sector
                Dim dtTable As DataTable = objAssignEmp.GetSectorFromEmployee(mintEmpUnkId, True)
                With cboSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtTable
                    .DataBind()
                End With
                objAssignEmp = Nothing

            ElseIf CBool(Session("SectorRouteAssignToEmp")) = False AndAlso CBool(Session("SectorRouteAssignToExpense")) = False Then
                Dim objcommonMst As New clsCommon_Master
                dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
                With cboSectorRoute
                    .DataValueField = "masterunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                End With
                objcommonMst = Nothing
            End If

            'Pinkal (20-Feb-2019) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dsCombo = Nothing
            Dim objCostCenter As New clscostcenter_master
            Dim dtClaimTable As DataTable = Nothing
            dsCombo = objCostCenter.getComboList("List", True)
            With cboCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    dtClaimTable = dsCombo.Tables(0)
                Else
                    dtClaimTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                End If
                .DataSource = dtClaimTable
                .DataBind()
            End With

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtClaimTable IsNot Nothing Then dtClaimTable.Clear()
            dtClaimTable = Nothing
            objCostCenter = Nothing
            'Pinkal (11-Sep-2020) -- End


            dsCombo = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombo = objExchange.getComboList("List", True, False)
            With cboClaimCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    mintClaimBaseCountryId = CInt(drRow(0)("countryunkid"))
                End If
                drRow = Nothing
            End With
            'Pinkal (04-Feb-2019) -- End

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExchange = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillClaimCombo" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objPrd = Nothing
            objEMaster = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Private Sub GetValue()
        Try

            Dim objClaimMst As New clsclaim_request_master
            mintClaimMstUnkId = objClaimMst.GetClaimRequestMstIDFromLeaveForm(mintLeaveFormUnkId)
            objClaimMst._Crmasterunkid = mintClaimMstUnkId
            txtClaimNo.Text = objClaimMst._Claimrequestno
            txtClaimRemark.Text = objClaimMst._Claim_Remark

            'Pinkal (20-May-2019) -- Start
            'Enhancement [0003788] - Comment field to be Mandatory under expenses. The claim remark.
            txtClaimRemark.ReadOnly = True
            'Pinkal (20-May-2019) -- End

            If objClaimMst._Transactiondate <> Nothing Then
                dtpDate.SetDate = objClaimMst._Transactiondate.Date
            Else
                dtpDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            End If
            dtpDate.Enabled = False
            'Pinkal (30-Apr-2018) - End

            Dim objClaimTran As New clsclaim_request_tran
            objClaimTran._ClaimRequestMasterId = mintClaimMstUnkId

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'mdtExpense = objClaimTran._DataTable
            mdtExpense = objClaimTran._DataTable.Copy()
            'Pinkal (11-Sep-2020) -- End




            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimTran = Nothing
            'Pinkal (11-Sep-2020) -- End

            objClaimMst = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetValue:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub Fill_Expense()
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdtTran As DataTable = mdtExpense
            Dim mdtTran As DataTable = mdtExpense.Copy()
            'Pinkal (11-Sep-2020) -- End

            Dim mdView As New DataView
            mdView = mdtTran.DefaultView
            mdView.RowFilter = "AUD <> 'D'"
            dgvData.DataSource = mdView
            dgvData.DataBind()
            If mdtTran.Rows.Count > 0 Then
                Dim dTotal() As DataRow = Nothing
                dTotal = mdtTran.Select("AUD<> 'D'")
                If dTotal.Length > 0 Then
                    txtGrandTotal.Text = Format(CDec(mdtTran.Compute("SUM(amount)", "AUD<>'D'")), Session("fmtCurrency").ToString())
                Else
                    txtGrandTotal.Text = ""
                End If
            Else
                txtGrandTotal.Text = ""
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Fill_Expense:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub


    'Pinkal (04-Feb-2020) -- Start
    'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
    'Private Function Validation() As Boolean
    Private Function Validation(ByVal sender As Object) As Boolean
        'Pinkal (04-Feb-2020) -- End
        Try
            If CInt(cboExpense.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 5, "Expense is mandatory information. Please select Expense to continue."), Me)
                cboExpense.Focus()
                Return False
            End If

            'Pinkal (16-Mar-2018) -- Start
            'Bug - Support Id :2098 Error when posting claim in Pacra.
            If txtCalimQty.Text.Trim() = "" OrElse CDec(txtCalimQty.Text) <= 0 Then
                'Pinkal (16-Mar-2018) -- End
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 6, "Quantity is mandatory information. Please enter Quantity to continue."), Me)
                txtCalimQty.Focus()
                Return False
            End If


            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpense._IsSecRouteMandatory AndAlso CInt(cboSectorRoute.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 20, "Sector/Route is mandatory information. Please enter Sector/Route to continue."), Me)
                cboSectorRoute.Focus()
                Exit Function
            End If

            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            If objExpense._IsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < CDec(txtCalimQty.Text) Then
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 25, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit."), Me)
                txtCalimQty.Focus()
                Return False
            End If

            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < CDec(txtCalimQty.Text) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 49, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), Me)
                txtCalimQty.Focus()
                Return False
            End If
            'Pinkal (10-Jun-2020) -- End

            objExpense = Nothing
            'Pinkal (25-Oct-2018) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If CInt(cboClaimCurrency.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 46, "Currency is mandatory information. Please select Currency to continue."), Me)
                cboClaimCurrency.Focus()
                Return False
            End If
            'Pinkal (04-Feb-2019) -- End



            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            'If IsNumeric(txtUnitPrice.Text) Then
            '    If CInt(txtUnitPrice.Text) <= 0 Then
            '        'Language.setLanguage(mstrModuleNameClaim)
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 7, "Unit Price is mandatory information. Please enter Unit Price to continue."), Me)
            '        txtUnitPrice.Focus()
            '        Return False
            '    End If
            'Else
            '    'Language.setLanguage(mstrModuleNameClaim)
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 7, "Unit Price is mandatory information. Please enter Unit Price to continue."), Me)
            '    txtUnitPrice.Focus()
            '    Return False
            'End If
            'Pinkal (22-Oct-2018) -- End

            If CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                Dim objapprover As New clsleaveapprover_master

                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                'Dim dtList As DataTable = objapprover.GetEmployeeApprover(Session("Database_Name").ToString(), _
                '                                                          CInt(Session("Fin_year")), _
                '                                                          CInt(Session("CompanyUnkId")), _
                '                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                '                                                          CBool(Session("IsIncludeInactiveEmp")), _
                '                                                          CInt(Session("UserId")), _
                '                                                          CInt(cboClaimEmployee.SelectedValue), -1, -1)

                Dim dtList As DataTable = objapprover.GetEmployeeApprover(Session("Database_Name").ToString(), _
                                                                          CInt(Session("Fin_year")), _
                                                                          CInt(Session("CompanyUnkId")), _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, _
                                                                          CBool(Session("IsIncludeInactiveEmp")), _
                                                                         -1, CInt(cboClaimEmployee.SelectedValue), -1, -1)

                'Pinkal (01-Mar-2016) -- End

                If dtList.Rows.Count = 0 Then
                    'Language.setLanguage(mstrModuleNameClaim)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 14, "Please Assign Leave Approver to this employee and also map Leave Approver to system user."), Me)
                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtList IsNot Nothing Then dtList.Clear()
                    dtList = Nothing
                    'Pinkal (11-Sep-2020) -- End
                    Return False
                End If

                If dtList.Rows.Count > 0 Then
                    Dim objUsermapping As New clsapprover_Usermapping
                    Dim isUserExist As Boolean = False
                    For Each dr As DataRow In dtList.Rows
                        objUsermapping.GetData(enUserType.Approver, CInt(dr("approverunkid")), -1, -1)
                        If objUsermapping._Mappingunkid > 0 Then isUserExist = True
                    Next

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objUsermapping = Nothing
                    'Pinkal (11-Sep-2020) -- End

                    If isUserExist = False Then
                        'Language.setLanguage(mstrModuleNameClaim)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 15, "Please Map this employee's Leave Approver to system user."), Me)
                        Return False
                    End If
                End If

                If CBool(Session("LeaveApproverForLeaveType")) Then

                    'Pinkal (01-Mar-2016) -- Start
                    'Enhancement - Implementing External Approver in Claim Request & Leave Module.

                    'dtList = objapprover.GetEmployeeApprover(Session("Database_Name").ToString(), _
                    '                                         CInt(Session("Fin_year")), _
                    '                                         CInt(Session("CompanyUnkId")), _
                    '                                         eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                    '                                         CBool(Session("IsIncludeInactiveEmp")), _
                    '                                         CInt(Session("UserId")), _
                    '                                         CInt(cboClaimEmployee.SelectedValue), -1, _
                    '                                         CInt(cboLeaveType.SelectedValue))

                    dtList = objapprover.GetEmployeeApprover(Session("Database_Name").ToString(), _
                                                             CInt(Session("Fin_year")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                             CBool(Session("IsIncludeInactiveEmp")), _
                                                             -1, CInt(cboClaimEmployee.SelectedValue), -1, _
                                                             CInt(cboLeaveType.SelectedValue))

                    'Pinkal (01-Mar-2016) -- End

                    If dtList.Rows.Count = 0 Then
                        'Language.setLanguage(mstrModuleNameClaim)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 16, "Please Map this Leave type to this employee's Leave Approver(s)."), Me)
                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        If dtList IsNot Nothing Then dtList.Clear()
                        dtList = Nothing
                        'Pinkal (11-Sep-2020) -- End
                        Return False
                    End If

                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtList IsNot Nothing Then dtList.Clear()
                dtList = Nothing
                'Pinkal (11-Sep-2020) -- End


            ElseIf CBool(Session("PaymentApprovalwithLeaveApproval")) = False Then

                Dim objExapprover As New clsExpenseApprover_Master
                Dim dsList As DataSet = objExapprover.GetEmployeeApprovers(CInt(cboExpCategory.SelectedValue), CInt(cboClaimEmployee.SelectedValue), "List", Nothing)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                    'Language.setLanguage(mstrModuleNameClaim)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 17, "Please Assign Expense Approver to this employee and also map Expense Approver to system user."), Me)
                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList = Nothing
                    objExapprover = Nothing
                    'Pinkal (11-Sep-2020) -- End
                    Return False
                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsList IsNot Nothing Then dsList.Clear()
                dsList = Nothing
                objExapprover = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If

            'Pinkal (29-Feb-2016) -- Start
            'Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
            Dim objClaimMaster As New clsclaim_request_master
            Dim sMsg As String = String.Empty

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'sMsg = objClaimMaster.IsValid_Expense(CInt(cboClaimEmployee.SelectedValue), CInt(cboExpense.SelectedValue))


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'sMsg = objClaimMaster.IsValid_Expense(CInt(cboClaimEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
            '                                                                           , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
            '                                                                           , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting))


            'Pinkal (22-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.

            'sMsg = objClaimMaster.IsValid_Expense(CInt(cboClaimEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
            '                                                                            , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
            '                                                                            , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimMstUnkId)


            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.

            'sMsg = objClaimMaster.IsValid_Expense(CInt(cboClaimEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
            '                                                                           , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
            '                                                                            , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimMstUnkId, True)

            Dim mblnFlag As Boolean = True

            If CType(sender, Button).ID = btnClaimAdd.ID Then
                mblnFlag = True
            Else
                mblnFlag = False
            End If

            sMsg = objClaimMaster.IsValid_Expense(CInt(cboClaimEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_year")) _
                                                                                       , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                                                                                        , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimMstUnkId, mblnFlag)

            'Pinkal (04-Feb-2020) -- End


            'Pinkal (22-Jan-2020) -- End


            'Pinkal (07-Mar-2019) -- End

            'Pinkal (26-Feb-2019) -- End


            objClaimMaster = Nothing
            If sMsg <> "" Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Exit Function
            End If
            sMsg = ""

            'Pinkal (29-Feb-2016) -- End


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If CheckForOccurrence(mdtExpense, CInt(cboClaimEmployee.SelectedValue), CInt(cboExpense.SelectedValue), dtpDate.GetDate.Date) = False Then Return False
            'Pinkal (07-Mar-2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Validation:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
        Return True
    End Function

    Private Sub Enable_Disable_Ctrls(ByVal iFlag As Boolean)
        Try
            cboExpCategory.Enabled = iFlag
            cboClaimEmployee.Enabled = iFlag
            cboPeriod.Enabled = iFlag
            dtpDate.Enabled = iFlag
            cboReference.Enabled = iFlag

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If mdtExpense IsNot Nothing Then
                If mdtExpense.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                    cboClaimCurrency.Enabled = False
                    cboClaimCurrency.SelectedValue = CStr(mdtExpense.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                Else
                    cboClaimCurrency.SelectedValue = mintClaimBaseCountryId.ToString()
                    cboClaimCurrency.Enabled = True
                End If
            End If
            'Pinkal (04-Feb-2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Enable_Disable_Controls:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub Clear_Controls()
        Try
            cboExpense.SelectedValue = "0"
            Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())
            txtExpRemark.Text = ""

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            'txtCalimQty.Text = "0"
            txtCalimQty.Text = "1"
            'Pinkal (04-Feb-2020) -- End



            'Pinkal (07-Feb-2020) -- Start
            'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
            'txtUnitPrice.Text = "0"
            txtUnitPrice.Text = "1.00"
            'Pinkal (07-Feb-2020) -- End

            cboSectorRoute.SelectedValue = "0"
            mintEditClaimTranId = -1
            mstrEditGUID = ""

            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            mintEmpMaxCountDependentsForCR = 0
            'Pinkal (25-Oct-2018) -- End

            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            mintClaimRequestApprovalExpTranId = 0
            mstrClaimRequestApprovalExpGUID = ""
            'Pinkal (07-Mar-2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Clear_Controls:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Function Is_Valid() As Boolean
        Try
            If CInt(Session("ClaimRequestVocNoType")) = 0 Then
                If txtClaimNo.Text.Trim.Length <= 0 Then
                    'Language.setLanguage(mstrModuleNameClaim)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 1, "Claim No. is mandatory information. Please select Claim No. to continue."), Me)
                    txtClaimNo.Focus()
                    Return False
                End If
            End If

            If CInt(cboExpCategory.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), Me)
                cboExpCategory.Focus()
                Return False
            End If

            If CInt(cboClaimEmployee.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 4, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboClaimEmployee.Focus()
                Return False
            End If

            If dgvData.Items.Count <= 0 Then
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 8, "Please add atleast one expense in order to save."), Me)
                dgvData.Focus()
                Return False
            End If

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Is_Valid :" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function


    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    Private Sub AddExpense()

        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (11-Sep-2020) -- End
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtExpense IsNot Nothing Then
                mdtTran = mdtExpense.Copy()
            End If
            'Pinkal (11-Sep-2020) -- End


            Dim dtmp() As DataRow = Nothing
            If mdtTran.Rows.Count > 0 Then

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                '   dtmp = mdtTran.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND AUD <> 'D'")
                dtmp = mdtTran.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtCalimQty.Text.Trim.Length > 0, CDec(txtCalimQty.Text), 0)) & _
                                                            "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                If dtmp.Length > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 10, "Sorry, you cannot add same expense again in the below list."), Me)
                    Exit Sub
                End If
            End If
            'Pinkal (04-Feb-2019) -- End

            Dim objExpMaster As New clsExpense_Master
            objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)


            'Pinkal (16-Sep-2020) -- Start
            'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.
            If objExpMaster._DoNotAllowToApplyForBackDate AndAlso dtpDate.GetDate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 50, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense."), Me)
                dtpDate.Focus()
                Exit Sub
            End If
            'Pinkal (16-Sep-2020) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            mblnIsHRExpenseClaimRequest = objExpMaster._IsHRExpense

            mblnIsClaimFormBudgetMandatoryClaimRequest = objExpMaster._IsBudgetMandatory
            Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
            Dim mstrGLCode As String = ""
            Dim mstrGLDescription As String = objExpMaster._Description
            Dim mintCostCenterID As Integer = 0
            Dim mstrCostCenterCode As String = ""
            If mblnIsClaimFormBudgetMandatoryClaimRequest AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), enMsgBoxStyle.Information)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                    'Sohail (23 Mar 2019) -- End
                    cboCostCenter.Focus()
                    Exit Sub
                End If
                GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            End If

            'Pinkal (04-Feb-2019) -- End



            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                        If CDec(txtCalimQty.Text) > CDec(txtClaimBalance.Text) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                            txtCalimQty.Focus()
                            Exit Sub
                        End If
                    ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                        If CDec(txtCalimQty.Text) > CDec(txtClaimBalance.Text) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 24, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
                            txtCalimQty.Focus()
                            Exit Sub
                        End If
                    End If
                    'Pinkal (04-Feb-2019) -- End
                ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                    If CDec(CDec(txtCalimQty.Text) * CDec(txtUnitPrice.Text)) > CDec(txtClaimBalance.Text) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 19, "Sorry, you cannot set amount greater than balance set."), Me)
                        txtCalimQty.Focus()
                        Exit Sub
                    End If
                End If
            End If


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim mdecBudgetAmount As Decimal = 0
                If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrCostCenterCode, mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                If mdecBudgetAmount < (CDec(txtCalimQty.Text) * CDec(txtUnitPrice.Text)) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 50, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
                    Exit Sub
                End If
            End If
            'Pinkal (04-Feb-2019) -- End

            If dgvData.Items.Count >= 1 Then
                Dim objExpMasterOld As New clsExpense_Master
                Dim iEncashment As DataRow() = Nothing
                If CInt(dgvData.Items(0).Cells(10).Text) > 0 Then
                    iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
                ElseIf dgvData.Items(0).Cells(12).Text.ToString <> "" Then
                    iEncashment = mdtTran.Select("GUID = '" & dgvData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If

                objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
                If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    Exit Sub

                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    Exit Sub
                    'Pinkal (04-Feb-2019) -- End

                End If
                objExpMasterOld = Nothing
            End If
            objExpMaster = Nothing

            Dim dRow As DataRow = mdtTran.NewRow

            dRow.Item("crtranunkid") = -1
            dRow.Item("crmasterunkid") = mintClaimMstUnkId
            dRow.Item("expenseunkid") = CInt(cboExpense.SelectedValue)
            dRow.Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            dRow.Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing OrElse txtCostingTag.Value = "", 0, txtCostingTag.Value)
            dRow.Item("unitprice") = CDec(txtUnitPrice.Text)
            dRow.Item("quantity") = txtCalimQty.Text
            dRow.Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtCalimQty.Text)
            dRow.Item("expense_remark") = txtExpRemark.Text
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("expense") = cboExpense.SelectedItem.Text
            dRow.Item("uom") = txtUoMType.Text
            dRow.Item("employeeunkid") = CInt(cboClaimEmployee.SelectedValue)
            dRow.Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            dRow.Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)
            dRow.Item("crapprovaltranunkid") = -1

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dRow.Item("costcenterunkid") = mintCostCenterID
            dRow.Item("costcentercode") = mstrCostCenterCode
            dRow.Item("glcodeunkid") = mintGLCodeID
            dRow.Item("glcode") = mstrGLCode
            dRow.Item("gldesc") = mstrGLDescription
            dRow.Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatoryClaimRequest
            dRow.Item("ishrexpense") = mblnIsHRExpenseClaimRequest
            dRow.Item("countryunkid") = CInt(cboClaimCurrency.SelectedValue)
            dRow.Item("base_countryunkid") = mintClaimBaseCountryId

            Dim mintExchangeRateId As Integer = 0
            Dim mdecBaseAmount As Decimal = 0
            Dim mdecExchangeRate As Decimal = 0
            GetCurrencyRate(CInt(cboClaimCurrency.SelectedValue), CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, txtUnitPrice.Text, 0)), CDec(IIf(txtCalimQty.Text.Trim.Length > 0, txtCalimQty.Text, 0)), dtpDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
            dRow.Item("base_amount") = mdecBaseAmount
            dRow.Item("exchangerateunkid") = mintExchangeRateId
            dRow.Item("exchange_rate") = mdecExchangeRate
            'Pinkal (04-Feb-2019) -- End

            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If mdtTran.Columns.Contains("tdate") Then
                dRow.Item("tdate") = eZeeDate.convertDate(dtpDate.GetDate.Date).ToString()
            End If
            'Pinkal (07-Mar-2019) -- End

            mdtTran.Rows.Add(dRow)

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'mdtExpense = mdtTran
            mdtExpense = mdtTran.Copy()
            'Pinkal (11-Sep-2020) -- End


            Fill_Expense()
            Enable_Disable_Ctrls(False)
            Clear_Controls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Private Sub EditExpense()
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim iRow As DataRow() = Nothing
        Dim mdtTran As DataTable = Nothing
        'Pinkal (11-Sep-2020) -- End
        Try
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtExpense IsNot Nothing Then
                mdtTran = mdtExpense.Copy()
            End If
            'Pinkal (11-Sep-2020) -- End


            If mintEditClaimTranId > 0 Then
                iRow = mdtTran.Select("crapprovaltranunkid = '" & mintEditClaimTranId & "' AND AUD <> 'D'")
            Else
                iRow = mdtTran.Select("GUID = '" & mstrEditGUID & "' AND AUD <> 'D'")
            End If

            If iRow IsNot Nothing Then


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                Dim dtmp() As DataRow = Nothing
                If CInt(iRow(0).Item("crtranunkid")) > 0 Then
                    dtmp = mdtTran.Select("crtranunkid <> '" & iRow(0).Item("crtranunkid").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtCalimQty.Text.Length > 0, CDec(txtCalimQty.Text), 0)) & _
                                                         "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                Else
                    dtmp = mdtTran.Select("GUID <> '" & iRow(0).Item("GUID").ToString() & "' AND  expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtCalimQty.Text.Length > 0, CDec(txtCalimQty.Text), 0)) & _
                                                      "' AND unitprice = '" & CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, CDec(txtUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                End If

                If dtmp.Length > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 10, "Sorry, you cannot add same expense again in the below list."), Me)
                    Exit Sub
                End If
                'Pinkal (04-Feb-2019) -- End

                Dim objExpMaster As New clsExpense_Master
                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                mblnIsHRExpenseClaimRequest = objExpMaster._IsHRExpense
                mblnIsClaimFormBudgetMandatoryClaimRequest = objExpMaster._IsBudgetMandatory
                Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
                Dim mstrGLCode As String = ""
                Dim mstrGLDescription As String = objExpMaster._Description
                Dim mintCostCenterID As Integer = 0
                Dim mstrCostCenterCode As String = ""
                If mblnIsClaimFormBudgetMandatoryClaimRequest AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboCostCenter.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                        cboCostCenter.Focus()
                        Exit Sub
                    End If
                    GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
                End If
                'Pinkal (04-Feb-2019) -- End


                If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                    If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                        'Pinkal (04-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                            If CDec(txtCalimQty.Text) > CDec(txtClaimBalance.Text) Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                                txtCalimQty.Focus()
                                Exit Sub
                            End If
                        ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                            If CDec(txtCalimQty.Text) > CDec(txtClaimBalance.Text) Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 24, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
                                txtCalimQty.Focus()
                                Exit Sub
                            End If
                        End If
                        'Pinkal (04-Feb-2019) -- End
                    ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                        If CDec(txtClaimBalance.Text) < CDec(CDec(txtCalimQty.Text) * CDec(txtUnitPrice.Text)) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 19, "Sorry, you cannot set amount greater than balance set."), Me)
                            txtCalimQty.Focus()
                            Exit Sub
                        End If
                    End If
                End If

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                    Dim mdecBudgetAmount As Decimal = 0
                    If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrCostCenterCode, mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                    If mdecBudgetAmount < (CDec(txtCalimQty.Text) * CDec(txtUnitPrice.Text)) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
                        Exit Sub
                    End If
                End If
                'Pinkal (04-Feb-2019) -- End

                If dgvData.Items.Count >= 1 Then
                    Dim objExpMasterOld As New clsExpense_Master
                    Dim iEncashment As DataRow() = Nothing

                    If CInt(dgvData.Items(0).Cells(10).Text) > 0 Then
                        iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
                    ElseIf dgvData.Items(0).Cells(12).Text.ToString <> "" Then
                        iEncashment = mdtTran.Select("GUID = '" & dgvData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
                    End If

                    objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
                    If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                        Exit Sub

                        'Pinkal (20-Nov-2018) -- Start
                        'Enhancement - Working on P2P Integration for NMB.
                    ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                        Exit Sub
                        'Pinkal (20-Nov-2018) -- End

                    End If
                    objExpMasterOld = Nothing
                End If
                objExpMaster = Nothing

                iRow(0).Item("crtranunkid") = iRow(0).Item("crtranunkid")
                iRow(0).Item("crmasterunkid") = mintClaimMstUnkId
                iRow(0).Item("expenseunkid") = CInt(cboExpense.SelectedValue)
                iRow(0).Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
                iRow(0).Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing, 0, txtCostingTag.Value)
                iRow(0).Item("unitprice") = txtUnitPrice.Text
                iRow(0).Item("quantity") = txtCalimQty.Text
                iRow(0).Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtCalimQty.Text)
                iRow(0).Item("expense_remark") = txtExpRemark.Text
                iRow(0).Item("isvoid") = False
                iRow(0).Item("voiduserunkid") = -1
                iRow(0).Item("voiddatetime") = DBNull.Value
                iRow(0).Item("voidreason") = ""
                If iRow(0).Item("AUD").ToString.Trim = "" Then
                    iRow(0).Item("AUD") = "U"
                End If
                iRow(0).Item("GUID") = Guid.NewGuid.ToString
                iRow(0).Item("expense") = cboExpense.SelectedItem.Text
                iRow(0).Item("uom") = txtUoMType.Text
                iRow(0).Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                iRow(0).Item("costcenterunkid") = mintCostCenterID
                iRow(0).Item("costcentercode") = mstrCostCenterCode
                iRow(0).Item("glcodeunkid") = mintGLCodeID
                iRow(0).Item("glcode") = mstrGLCode
                iRow(0).Item("gldesc") = mstrGLDescription
                iRow(0).Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatoryClaimRequest
                iRow(0).Item("ishrexpense") = mblnIsHRExpenseClaimRequest
                iRow(0).Item("countryunkid") = CInt(cboClaimCurrency.SelectedValue)
                iRow(0).Item("base_countryunkid") = mintClaimBaseCountryId

                Dim mintExchangeRateId As Integer = 0
                Dim mdecBaseAmount As Decimal = 0
                Dim mdecExchangeRate As Decimal = 0
                GetCurrencyRate(CInt(cboClaimCurrency.SelectedValue), CDec(IIf(txtUnitPrice.Text.Trim.Length > 0, txtUnitPrice.Text, 0)), CDec(IIf(txtCalimQty.Text.Trim.Length > 0, txtCalimQty.Text, 0)), dtpDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
                iRow(0).Item("base_amount") = mdecBaseAmount
                iRow(0).Item("exchangerateunkid") = mintExchangeRateId
                iRow(0).Item("exchange_rate") = mdecExchangeRate
                'Pinkal (04-Feb-2019) -- End

                iRow(0).AcceptChanges()

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'mdtExpense = mdtTran
                mdtExpense = mdtTran.Copy()
                'Pinkal (11-Sep-2020) -- End

                Fill_Expense()
                Clear_Controls()
                btnClaimEdit.Visible = False : btnClaimAdd.Visible = True
                cboExpense.Enabled = True

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            iRow = Nothing
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Private Sub SaveExpense()
        Try
            Dim mdtTran As DataTable = mdtExpense
            If mdtTran IsNot Nothing Then
                For Each dRow As DataRow In mdtTran.Rows
                    Dim sMsg As String = String.Empty
                    Dim objClaimMaster As New clsclaim_request_master

                    'Pinkal (26-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'sMsg = objClaimMaster.IsValid_Expense(mintClaimMstUnkId, CInt(dRow("expenseunkid")))


                    'Pinkal (07-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    'sMsg = objClaimMaster.IsValid_Expense(CInt(cboClaimEmployee.SelectedValue), CInt(dRow("expenseunkid")), CInt(Session("Fin_year")) _
                    '                                                                   , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                    '                                                                   , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting))


                    'Pinkal (22-Jan-2020) -- Start
                    'Enhancements -  Working on OT Requisistion Reports for NMB.

                    'sMsg = objClaimMaster.IsValid_Expense(CInt(cboClaimEmployee.SelectedValue), CInt(dRow("expenseunkid")), CInt(Session("Fin_year")) _
                    '                                                                    , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                    '                                                                   , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimMstUnkId)


                    sMsg = objClaimMaster.IsValid_Expense(CInt(cboClaimEmployee.SelectedValue), CInt(dRow("expenseunkid")), CInt(Session("Fin_year")) _
                                                                                       , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpDate.GetDate.Date _
                                                                                      , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimMstUnkId, False)

                    'Pinkal (22-Jan-2020) -- End


                    'Pinkal (07-Mar-2019) -- End


                    'Pinkal (26-Feb-2019) -- End

                    objClaimMaster = Nothing
                    If sMsg <> "" Then
                        DisplayMessage.DisplayMessage(sMsg, Me)
                        Exit Sub
                    End If
                    sMsg = ""
                Next
            End If

            If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
                Dim drRow() As DataRow = mdtTran.Select("AUD = ''")
                If drRow.Length > 0 Then
                    For Each dr As DataRow In drRow
                        dr("AUD") = "U"
                        dr.AcceptChanges()
                    Next
                End If
            End If

            mdtFinalClaimTransaction = mdtTran.Copy
            mdtTran.Clear()
            mdtTran = Nothing
            mdtExpense = Nothing
            mblnClaimRequest = False
            popupClaimRequest.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (22-Oct-2018) -- End

    'Pinkal (25-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    Private Function GetEmployeeDepedentCountForCR(ByVal xEmployeeId As Integer) As Integer
        Dim mintEmpDepedentsCoutnForCR As Integer = 0
        Try
            Dim objDependents As New clsDependants_Beneficiary_tran
            Dim dtDependents As DataTable = objDependents.GetEmployeeMaxCountCRBenefitDependants(xEmployeeId)
            mintEmpDepedentsCoutnForCR = dtDependents.AsEnumerable().Sum(Function(row) row.Field(Of Integer)("MaxCount")) + 1  ' + 1 Means Employee itself included in this Qty.
            objDependents = Nothing
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtDependents IsNot Nothing Then dtDependents.Clear()
            dtDependents = Nothing
            'Pinkal (11-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mintEmpDepedentsCoutnForCR
    End Function

    'Pinkal (25-Oct-2018) -- End    

#End Region


    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.

#Region "     Add For Approval Expense"

    Private Sub FillApprovalClaimCombo()
        Dim objPrd As New clscommom_period_Tran
        Dim dsCombo As New DataSet
        Dim objEMaster As New clsEmployee_Master
        Try


            dsCombo = objEMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                                 CInt(Session("UserId")), _
                                                 CInt(Session("Fin_year")), _
                                                 CInt(Session("CompanyUnkId")), _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                 Session("UserAccessModeSetting").ToString(), True, _
                                                 CBool(Session("IsIncludeInactiveEmp")), "List", False, _
                                                  mintEmpUnkId, , , , , , , , , , , , , , , , CBool(IIf(mblnIsExternalApprover, False, True)))

            With cboApprovalExpEmployee
                .DataValueField = "employeeunkid"
                .DataTextField = "EmpCodeName"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            dsCombo = clsExpCommonMethods.Get_ExpenseTypes(False, False, False, "List")
            With cboApprovalExpCategory
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            Call cboApprovalExpCategory_SelectedIndexChanged(cboApprovalExpCategory, New EventArgs())


            'Pinkal (20-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            If CBool(Session("SectorRouteAssignToEmp")) = True AndAlso CBool(Session("SectorRouteAssignToExpense")) = False Then

                Dim objAssignEmp As New clsassignemp_sector
                Dim dtTable As DataTable = objAssignEmp.GetSectorFromEmployee(mintEmpUnkId, True)
                With cboApprovalSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtTable
                    .DataBind()
                End With
                objAssignEmp = Nothing

            ElseIf CBool(Session("SectorRouteAssignToEmp")) = False AndAlso CBool(Session("SectorRouteAssignToExpense")) = False Then

                Dim objcommonMst As New clsCommon_Master
                dsCombo = objcommonMst.getComboList(clsCommon_Master.enCommonMaster.SECTOR_ROUTE, True, "List")
                With cboApprovalSectorRoute
                    .DataValueField = "masterunkid"
                    .DataTextField = "name"
                    .DataSource = dsCombo.Tables(0)
                    .DataBind()
                End With
                objcommonMst = Nothing
            End If

            'Pinkal (20-Feb-2019) -- End


            dsCombo = (New clsCommon_Master).getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            With cboScanDcoumentType
                .DataValueField = "masterunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables("List")
                .DataBind()
            End With

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dsCombo = Nothing
            Dim objCostCenter As New clscostcenter_master
            Dim dtApprovalTable As DataTable = Nothing
            dsCombo = objCostCenter.getComboList("List", True)
            With cboApprovalCostCenter
                .DataValueField = "costcenterunkid"
                .DataTextField = "costcentername"
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    dtApprovalTable = dsCombo.Tables(0)
                Else
                    dtApprovalTable = New DataView(dsCombo.Tables(0), "costcenterunkid <= 0", "", DataViewRowState.CurrentRows).ToTable()
                End If
                .DataSource = dtApprovalTable
                .DataBind()
            End With

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtApprovalTable IsNot Nothing Then dtApprovalTable.Clear()
            dtApprovalTable = Nothing
            objCostCenter = Nothing
            'Pinkal (11-Sep-2020) -- End


            dsCombo = Nothing
            Dim objExchange As New clsExchangeRate
            dsCombo = objExchange.getComboList("List", True, False)
            With cboApprovalCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("isbasecurrency = 1")
                If drRow.Length > 0 Then
                    .SelectedValue = drRow(0)("countryunkid").ToString()
                    mintApprovalBaseCountryId = CInt(drRow(0)("countryunkid"))
                End If
                drRow = Nothing
            End With
            'Pinkal (04-Feb-2019) -- End

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExchange = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillApprovalClaimCombo" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objPrd = Nothing
            objEMaster = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Private Sub GetApprovalExpenseValue()
        Try
            Dim objClaimMst As New clsclaim_request_master
            mintClaimMstUnkId = objClaimMst.GetClaimRequestMstIDFromLeaveForm(CInt(IIf(mintLeaveFormUnkId > 0, mintLeaveFormUnkId, -1)))
            objClaimMst._Crmasterunkid = mintClaimMstUnkId
            Dim objClaimTran As New clsclaim_request_tran
            objClaimTran._ClaimRequestMasterId = mintClaimMstUnkId
            'If mdtLeaveExpenseTran Is Nothing Then
            mdtLeaveExpenseTran = objClaimTran._DataTable
            'End If
            objClaimTran = Nothing
            objClaimMst = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("GetApprovalExpenseValue:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Sub FillApprovalExpense()
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim mdtTran As DataTable = Nothing
        'Pinkal (11-Sep-2020) -- End
        Try
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdtTran As DataTable = mdtLeaveExpenseTran
            If mdtLeaveExpenseTran IsNot Nothing Then
                mdtTran = mdtLeaveExpenseTran.Copy()
            End If
            'Pinkal (11-Sep-2020) -- End

            Dim mdView As New DataView
            mdView = mdtTran.DefaultView
            mdView.RowFilter = "AUD <> 'D'"
            If mdView.ToTable.Rows.Count > dgvApprovalData.PageSize Then
                dgvApprovalData.AllowPaging = True
            Else
                dgvApprovalData.AllowPaging = False
            End If
            dgvApprovalData.DataSource = mdView
            dgvApprovalData.DataBind()
            If mdtTran.Rows.Count > 0 Then
                Dim dTotal() As DataRow = Nothing
                dTotal = mdtTran.Select("AUD<> 'D'")
                If dTotal.Length > 0 Then
                    txtApprovalGrandTotal.Text = Format(CDec(mdtTran.Compute("SUM(amount)", "AUD<>'D'")), CStr(Session("fmtCurrency")))
                Else
                    txtApprovalGrandTotal.Text = CDec(0).ToString(Session("fmtCurrency").ToString())
                End If
            Else
                txtApprovalGrandTotal.Text = CDec(0).ToString(Session("fmtCurrency").ToString())
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillApprovalExpense:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub


    'Pinkal (04-Feb-2020) -- Start
    'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
    'Private Function Approval_Validation() As Boolean
    Private Function Approval_Validation(ByVal sender As Object) As Boolean
        'Pinkal (04-Feb-2020) -- End
        Try
            If CInt(cboApprovalExpense.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 5, "Expense is mandatory information. Please select Expense to continue."), Me)
                cboApprovalExpense.Focus()
                Return False
            End If


            If txtApprovalQty.Text = "" OrElse CInt(txtApprovalQty.Text) <= 0 Then
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 6, "Quantity is mandatory information. Please enter Quantity to continue."), Me)
                txtApprovalQty.Focus()
                Return False
            End If

            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboApprovalExpense.SelectedValue)
            If objExpense._IsSecRouteMandatory AndAlso CInt(cboApprovalSectorRoute.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 20, "Sector/Route is mandatory information. Please enter Sector/Route to continue."), Me)
                cboApprovalSectorRoute.Focus()
                Exit Function
            End If

            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            If objExpense._IsConsiderDependants AndAlso mintEmpMaxCountDependentsForCR < CDec(txtApprovalQty.Text) Then
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 25, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit."), Me)
                txtApprovalQty.Focus()
                Return False
            End If
            'Pinkal (25-Oct-2018) -- End

            'Pinkal (10-Jun-2020) -- Start
            'Enhancement Claim NMB - Implementing Maximum Quantity setting expense wise on Claim module.
            If objExpense._Expense_MaxQuantity > 0 AndAlso objExpense._Expense_MaxQuantity < CDec(txtApprovalQty.Text) Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 49, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master."), Me)
                txtApprovalQty.Focus()
                Return False
            End If
            'Pinkal (10-Jun-2020) -- End

            objExpense = Nothing


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If CInt(cboApprovalCurrency.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 46, "Currency is mandatory information. Please select Currency to continue."), Me)
                cboApprovalCurrency.Focus()
                Return False
            End If
            'Pinkal (04-Feb-2019) -- End


            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            'If IsNumeric(txtApprovalUnitPrice.Text) Then
            '    If CInt(txtApprovalUnitPrice.Text) <= 0 Then
            '        'Language.setLanguage(mstrModuleNameClaim)
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 7, "Unit Price is mandatory information. Please enter Unit Price to continue."), Me)
            '        txtApprovalUnitPrice.Focus()
            '        Return False
            '    End If
            'Else
            '    'Language.setLanguage(mstrModuleNameClaim)
            '    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 7, "Unit Price is mandatory information. Please enter Unit Price to continue."), Me)
            '    txtApprovalUnitPrice.Focus()
            '    Return False
            'End If
            'Pinkal (22-Oct-2018) -- End


            If CBool(Session("PaymentApprovalwithLeaveApproval")) Then

                Dim objapprover As New clsleaveapprover_master

                Dim dtList As DataTable = objapprover.GetEmployeeApprover(CStr(Session("Database_Name")), _
                                                                          CInt(Session("Fin_year")), _
                                                                          CInt(Session("CompanyUnkId")), _
                                                                          eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                                          CBool(Session("IsIncludeInactiveEmp")), -1, _
                                                                          CInt(cboApprovalExpEmployee.SelectedValue))

                If dtList.Rows.Count = 0 Then
                    'Language.setLanguage(mstrModuleNameClaim)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 14, "Please Assign Leave Approver to this employee and also map Leave Approver to system user."), Me)
                    Return False
                End If

                If dtList.Rows.Count > 0 Then
                    Dim objUsermapping As New clsapprover_Usermapping
                    Dim isUserExist As Boolean = False
                    For Each dr As DataRow In dtList.Rows
                        objUsermapping.GetData(enUserType.Approver, CInt(dr("approverunkid")), -1, -1)
                        If objUsermapping._Mappingunkid > 0 Then isUserExist = True
                    Next

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objUsermapping = Nothing
                    'Pinkal (11-Sep-2020) -- End

                    If isUserExist = False Then
                        'Language.setLanguage(mstrModuleNameClaim)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 15, "Please Map this employee's Leave Approver to system user."), Me)
                        Return False
                    End If

                End If

                If CBool(Session("LeaveApproverForLeaveType")) Then

                    dtList = objapprover.GetEmployeeApprover(CStr(Session("Database_Name")), _
                                                             CInt(Session("Fin_year")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                             CBool(Session("IsIncludeInactiveEmp")), _
                                                             -1, _
                                                             CInt(cboApprovalExpEmployee.SelectedValue), -1, _
                                                             CInt(cboApprovalExpLeaveType.SelectedValue))

                    If dtList.Rows.Count = 0 Then
                        'Language.setLanguage(mstrModuleNameClaim)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 16, "Please Map this Leave type to this employee's Leave Approver(s)."), Me)
                        Return False
                    End If

                End If


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtList IsNot Nothing Then dtList.Clear()
                dtList = Nothing
                objapprover = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If

            Dim sMsg As String = String.Empty
            Dim objClaimMst As New clsclaim_request_master

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'sMsg = objClaimMst.IsValid_Expense(CInt(cboApprovalExpEmployee.SelectedValue), CInt(cboApprovalExpense.SelectedValue))


            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'sMsg = objClaimMst.IsValid_Expense(CInt(cboApprovalExpEmployee.SelectedValue), CInt(cboApprovalExpense.SelectedValue), CInt(Session("Fin_year")) _
            '                                                                           , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpApprovalClaimDate.GetDate.Date _
            '                                                                           , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting))

            'Pinkal (22-Jan-2020) -- Start
            'Enhancements -  Working on OT Requisistion Reports for NMB.

            'sMsg = objClaimMst.IsValid_Expense(CInt(cboApprovalExpEmployee.SelectedValue), CInt(cboApprovalExpense.SelectedValue), CInt(Session("Fin_year")) _
            '                                                                           , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpApprovalClaimDate.GetDate.Date _
            '                                                                           , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimMstUnkId)


            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.

            'sMsg = objClaimMst.IsValid_Expense(CInt(cboApprovalExpEmployee.SelectedValue), CInt(cboApprovalExpense.SelectedValue), CInt(Session("Fin_year")) _
            '                                                                           , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpApprovalClaimDate.GetDate.Date _
            '                                                                          , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimMstUnkId, True)
            Dim mblnFlag As Boolean = True

            If CType(sender, Button).ID = btnApprovalAdd.ID Then
                mblnFlag = True
            Else
                mblnFlag = False
            End If

            sMsg = objClaimMst.IsValid_Expense(CInt(cboApprovalExpEmployee.SelectedValue), CInt(cboApprovalExpense.SelectedValue), CInt(Session("Fin_year")) _
                                                                                       , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, dtpApprovalClaimDate.GetDate.Date _
                                                                                 , CType(CInt(Session("LeaveBalanceSetting")), enLeaveBalanceSetting), mintClaimMstUnkId, mblnFlag)

            'Pinkal (04-Feb-2020) -- End


            'Pinkal (22-Jan-2020) -- End


            'Pinkal (07-Mar-2019) -- End


            'Pinkal (26-Feb-2019) -- End
            If sMsg <> "" Then
                DisplayMessage.DisplayMessage(sMsg, Me)
                Exit Function
            End If
            sMsg = ""


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objClaimMst = Nothing
            'Pinkal (11-Sep-2020) -- End



            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If CheckForOccurrence(mdtLeaveExpenseTran, CInt(cboApprovalExpEmployee.SelectedValue), CInt(cboApprovalExpense.SelectedValue), dtpApprovalClaimDate.GetDate.Date) = False Then Return False
            'Pinkal (07-Mar-2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Approval_Validation:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            Return False
        End Try
        Return True
    End Function

    Private Sub EnableDisable_Controls(ByVal iFlag As Boolean)
        Dim objClaimMaster As New clsclaim_request_master
        Try

            cboApprovalExpCategory.Enabled = iFlag
            cboApprovalExpEmployee.Enabled = iFlag
            cboApprovalPeriod.Enabled = iFlag
            dtpApprovalClaimDate.Enabled = iFlag
            If mintClaimMstUnkId > 0 Then
                If objClaimMaster._Expensetypeid = enExpenseType.EXP_LEAVE Then
                    cboApprovalExpLeaveType.Enabled = iFlag
                End If
            Else
                If CInt(cboApprovalExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                    cboApprovalExpLeaveType.Enabled = iFlag
                End If
            End If
            cboApprovalReference.Enabled = iFlag

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If mdtLeaveExpenseTran IsNot Nothing Then
                If mdtLeaveExpenseTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                    cboApprovalCurrency.Enabled = False
                    cboApprovalCurrency.SelectedValue = CStr(mdtLeaveExpenseTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                Else
                    cboApprovalCurrency.SelectedValue = mintClaimBaseCountryId.ToString()
                    cboApprovalCurrency.Enabled = True
                End If
            End If
            'Pinkal (04-Feb-2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("EnableDisable_Controls:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objClaimMaster = Nothing
        End Try
    End Sub

    Private Sub ClearControls()
        Try
            cboApprovalExpense.SelectedValue = "0"
            cboApprovalSectorRoute.SelectedValue = "0"
            Call cboApprovalExpense_SelectedIndexChanged(cboApprovalExpense, New EventArgs())
            txtApprovalExpRemark.Text = ""

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            'txtApprovalQty.Text = "0"
            txtApprovalQty.Text = "1"
            'Pinkal (04-Feb-2020) -- End


            'Pinkal (07-Feb-2020) -- Start
            'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
            'txtApprovalUnitPrice.Text = "0"
            txtApprovalUnitPrice.Text = "1.00"
            'Pinkal (07-Feb-2020) -- End



            'Pinkal (20-May-2019) -- Start
            'Enhancement [0003788] - Comment field to be Mandatory under expenses. The claim remark.
            'txtApprovalClaimRemark.Text = ""
            'Pinkal (20-May-2019) -- End



            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            mintEmpMaxCountDependentsForCR = 0
            'Pinkal (25-Oct-2018) -- End

            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            mintClaimRequestApprovalExpTranId = 0
            mstrClaimRequestApprovalExpGUID = ""
            'Pinkal (07-Mar-2019) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ClearControls:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Private Function IsApproval_Valid() As Boolean
        Try
            If CInt(Session("ClaimRequestVocNoType")) = 0 Then
                If txtApprovalClaimNo.Text.Trim.Length <= 0 Then
                    'Language.setLanguage(mstrModuleNameClaim)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 1, "Claim No. is mandatory information. Please select Claim No. to continue."), Me)
                    txtApprovalClaimNo.Focus()
                    Return False
                End If
            End If

            If CInt(cboApprovalExpCategory.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 3, "Expense Category is mandatory information. Please select Expense Category to continue."), Me)
                cboApprovalExpCategory.Focus()
                Return False
            End If

            If CInt(cboApprovalExpEmployee.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 4, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboApprovalExpEmployee.Focus()
                Return False
            End If

            If dgvApprovalData.Items.Count <= 0 Then
                'Language.setLanguage(mstrModuleNameClaim)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 8, "Please add atleast one expense in order to save."), Me)
                dgvApprovalData.Focus()
                Return False
            End If

            'Pinkal (20-May-2019) -- Start
            'Enhancement [0003788] - Comment field to be Mandatory under expenses. The claim remark.
            If CBool(Session("ClaimRemarkMandatoryForClaim")) AndAlso txtApprovalClaimRemark.Text.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 47, "Claim Remark is mandatory information. Please enter claim remark to continue."), Me)
                'Gajanan [17-Sep-2020] -- Start
                'New UI Change
                'TabContainer1.ActiveTabIndex = 1
                'Gajanan [17-Sep-2020] -- End
                txtApprovalClaimRemark.Focus()
                Return False
            End If
            'Pinkal (20-May-2019) -- End


            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsApproval_Valid:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function

    Private Sub FillClaimAttachment()
        Dim dtView As DataView
        Try
            If mdtClaimAttchment Is Nothing Then Exit Sub
            dtView = New DataView(mdtClaimAttchment, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgv_Attchment.AutoGenerateColumns = False
            dgv_Attchment.DataSource = dtView
            dgv_Attchment.DataBind()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub AddDocumentAttachmentForClaim(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtClaimAttchment.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtClaimAttchment.NewRow
                dRow("scanattachtranunkid") = -1
                dRow("employeeunkid") = CInt(cboApprovalExpEmployee.SelectedValue)
                dRow("documentunkid") = CInt(cboScanDcoumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Claim_Request
                dRow("scanattachrefid") = enScanAttactRefId.CLAIM_REQUEST

                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'dRow("transactionunkid") = dgvApprovalData.Items(mintDeleteIndex).Cells(10).Text
                dRow("transactionunkid") = dgvApprovalData.Items(mintExpenseIndex).Cells(10).Text
                'Pinkal (18-Mar-2021) -- End

                dRow("filepath") = ""
                dRow("filename") = f.Name
                dRow("filesize") = f.Length / 1024
                dRow("attached_date") = Today.Date
                dRow("orgfilepath") = strfullpath

                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'dRow("GUID") = dgvApprovalData.Items(mintDeleteIndex).Cells(12).Text
                dRow("GUID") = dgvApprovalData.Items(mintExpenseIndex).Cells(12).Text
                'Pinkal (18-Mar-2021) -- End

                dRow("AUD") = "A"
                dRow("form_name") = mstrModuleNameClaim
                dRow("userunkid") = CInt(Session("userid"))
                'S.SANDEEP |25-JAN-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                Dim xDocumentData As Byte() = IO.File.ReadAllBytes(strfullpath)
                dRow("file_data") = xDocumentData
                'S.SANDEEP |25-JAN-2019| -- END
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 42, "Selected information is already present for particular employee."), Me)
                Exit Sub
            End If
            mdtClaimAttchment.Rows.Add(dRow)
            Call FillClaimAttachment()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function AddRowForClaimAttachment(ByVal dr As DataRow, ByVal strScreenName As String) As Boolean
        Try
            Dim xRow As DataRow() = Nothing


            'Pinkal (18-Mar-2021) -- Start
            'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
            'If CInt(dgvApprovalData.Items(mintDeleteIndex).Cells(10).Text) > 0 Then
            '    xRow = mdtChildClaimAttachment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND transactionunkid = '" & CInt(dgvApprovalData.Items(mintDeleteIndex).Cells(10).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            'ElseIf CStr(dgvApprovalData.Items(mintDeleteIndex).Cells(12).Text).Trim.Length > 0 Then
            '    xRow = mdtChildClaimAttachment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND GUID = '" & CStr(dgvApprovalData.Items(mintDeleteIndex).Cells(12).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            'End If
            If CInt(dgvApprovalData.Items(mintExpenseIndex).Cells(10).Text) > 0 Then
                xRow = mdtChildClaimAttachment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND transactionunkid = '" & CInt(dgvApprovalData.Items(mintExpenseIndex).Cells(10).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            ElseIf CStr(dgvApprovalData.Items(mintExpenseIndex).Cells(12).Text).Trim.Length > 0 Then
                xRow = mdtChildClaimAttachment.Select("scanattachrefid = '" & enScanAttactRefId.CLAIM_REQUEST & "' AND GUID = '" & CStr(dgvApprovalData.Items(mintExpenseIndex).Cells(12).Text) & "' AND filename = '" & dr.Item("filename").ToString & "' ")
            End If
            'Pinkal (18-Mar-2021) -- End


            If xRow.Length <= 0 Then
                mdtChildClaimAttachment.ImportRow(dr)
            Else
                mdtChildClaimAttachment.Rows.Remove(xRow(0))
                mdtChildClaimAttachment.ImportRow(dr)
            End If
            mdtChildClaimAttachment.AcceptChanges()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "AddRowForClaimAttachment", mstrModuleName1)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
        Return True
    End Function


    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .

    Private Sub AddForApprovalExpense()
        Try

            Dim dtmp() As DataRow = Nothing
            If mdtLeaveExpenseTran.Rows.Count > 0 Then
                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                'dtmp = mdtLeaveExpenseTran.Select("expenseunkid = '" & CInt(cboApprovalExpense.SelectedValue) & "' AND AUD <> 'D'")
                dtmp = mdtLeaveExpenseTran.Select("expenseunkid = '" & CInt(cboApprovalExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboApprovalSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtApprovalQty.Text.Trim.Length > 0, CDec(txtApprovalQty.Text), 0)) & _
                                                         "' AND unitprice = '" & CDec(IIf(txtApprovalUnitPrice.Text.Trim.Length > 0, CDec(txtApprovalUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtApprovalExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                If dtmp.Length > 0 Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'DisplayMessage.DisplayError(ex, Me)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 10, "Sorry, you cannot add same expense again in the below list."), Me)
                    'Sohail (23 Mar 2019) -- End
                    Exit Sub
                End If
                'Pinkal (04-Feb-2019) -- End
            End If


            Dim objExpMaster As New clsExpense_Master
            objExpMaster._Expenseunkid = CInt(cboApprovalExpense.SelectedValue)




            'Pinkal (16-Sep-2020) -- Start
            'Enhancement NMB - Adding Setting in Expense Master to allow to apply claim application for back date or not.
            If objExpMaster._DoNotAllowToApplyForBackDate AndAlso dtpDate.GetDate.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 50, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense."), Me)
                dtpDate.Focus()
                Exit Sub
            End If
            'Pinkal (16-Sep-2020) -- End

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            mblnIsHRExpenseApprovalClaimRequest = objExpMaster._IsHRExpense

            mblnIsClaimFormBudgetMandatoryApprovalClaimRequest = objExpMaster._IsBudgetMandatory
            Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
            Dim mstrGLCode As String = ""
            Dim mstrGLDescription As String = objExpMaster._Description
            Dim mintCostCenterID As Integer = 0
            Dim mstrCostCenterCode As String = ""
            If mblnIsClaimFormBudgetMandatoryApprovalClaimRequest AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboApprovalCostCenter.SelectedValue) <= 0 Then
                    'Sohail (23 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                    'eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), enMsgBoxStyle.Information)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                    'Sohail (23 Mar 2019) -- End
                    cboCostCenter.Focus()
                    Exit Sub
                End If
                GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
            End If

            'Pinkal (04-Feb-2019) -- End


            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then

                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

                    If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                        If CDec(txtApprovalQty.Text) > CDec(txtApprovalBalance.Text) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                            txtApprovalQty.Focus()
                            Exit Sub
                        End If
                    ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                        If CDec(txtApprovalQty.Text) > CDec(txtApprovalBalance.Text) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 24, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
                            txtApprovalQty.Focus()
                            Exit Sub
                        End If
                    End If


                ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                    If CDec(txtApprovalBalance.Text) < CDec(CDec(txtApprovalQty.Text) * CDec(txtApprovalUnitPrice.Text)) Then
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 19, "Sorry, you cannot set amount greater than balance set."), Me)
                        'Sohail (23 Mar 2019) -- End
                        txtApprovalQty.Focus()
                        Exit Sub
                    End If
                End If
            End If

            If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                Dim mdecBudgetAmount As Decimal = 0
                If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrCostCenterCode, mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                If mdecBudgetAmount < (CDec(txtApprovalQty.Text) * CDec(txtApprovalUnitPrice.Text)) Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
                    Exit Sub
                End If
            End If
            'Pinkal (04-Feb-2019) -- End

            If dgvApprovalData.Items.Count >= 1 Then
                Dim objExpMasterOld As New clsExpense_Master
                Dim iEncashment As DataRow() = Nothing
                If CInt(dgvApprovalData.Items(0).Cells(10).Text) > 0 Then
                    iEncashment = mdtLeaveExpenseTran.Select("crtranunkid = '" & CInt(dgvApprovalData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
                ElseIf dgvApprovalData.Items(0).Cells(12).Text.ToString <> "" Then
                    iEncashment = mdtLeaveExpenseTran.Select("GUID = '" & dgvApprovalData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If

                objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
                If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                    'Language.setLanguage(mstrModuleNameClaim)
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    Exit Sub

                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                    Exit Sub
                    'Pinkal (04-Feb-2019) -- End

                End If
                objExpMasterOld = Nothing
            End If

            objExpMaster = Nothing

            Dim dRow As DataRow = mdtLeaveExpenseTran.NewRow

            dRow.Item("crtranunkid") = -1
            dRow.Item("crmasterunkid") = mintClaimMstUnkId
            dRow.Item("expenseunkid") = CInt(cboApprovalExpense.SelectedValue)
            dRow.Item("secrouteunkid") = CInt(cboApprovalSectorRoute.SelectedValue)
            dRow.Item("costingunkid") = IIf(txtApprovalCostingTag.Value Is Nothing OrElse txtApprovalCostingTag.Value = "", 0, txtApprovalCostingTag.Value)
            dRow.Item("unitprice") = CDec(txtApprovalUnitPrice.Text)
            dRow.Item("quantity") = txtApprovalQty.Text
            dRow.Item("amount") = CDec(txtApprovalUnitPrice.Text) * CDec(txtApprovalQty.Text)
            dRow.Item("expense_remark") = txtApprovalExpRemark.Text
            dRow.Item("isvoid") = False
            dRow.Item("voiduserunkid") = -1
            dRow.Item("voiddatetime") = DBNull.Value
            dRow.Item("voidreason") = ""
            dRow.Item("loginemployeeunkid") = -1
            dRow.Item("AUD") = "A"
            dRow.Item("GUID") = Guid.NewGuid.ToString
            dRow.Item("expense") = cboApprovalExpense.SelectedItem.Text
            dRow.Item("uom") = txtApprovalUoMType.Text
            dRow.Item("voidloginemployeeunkid") = -1
            dRow.Item("sector") = IIf(CInt(cboApprovalSectorRoute.SelectedValue) <= 0, "", cboApprovalSectorRoute.SelectedItem.Text)

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            dRow.Item("costcenterunkid") = mintCostCenterID
            dRow.Item("costcentercode") = mstrCostCenterCode
            dRow.Item("glcodeunkid") = mintGLCodeID
            dRow.Item("glcode") = mstrGLCode
            dRow.Item("gldesc") = mstrGLDescription
            dRow.Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatoryApprovalClaimRequest
            dRow.Item("ishrexpense") = mblnIsHRExpenseApprovalClaimRequest
            dRow.Item("countryunkid") = CInt(cboApprovalCurrency.SelectedValue)
            dRow.Item("base_countryunkid") = mintApprovalBaseCountryId

            Dim mintExchangeRateId As Integer = 0
            Dim mdecBaseAmount As Decimal = 0
            Dim mdecExchangeRate As Decimal = 0
            GetCurrencyRate(CInt(cboApprovalCurrency.SelectedValue), CDec(IIf(txtApprovalUnitPrice.Text.Trim.Length > 0, txtApprovalUnitPrice.Text, 0)), CDec(IIf(txtApprovalQty.Text.Trim.Length > 0, txtApprovalQty.Text, 0)), dtpApprovalClaimDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
            dRow.Item("base_amount") = mdecBaseAmount
            dRow.Item("exchangerateunkid") = mintExchangeRateId
            dRow.Item("exchange_rate") = mdecExchangeRate
            'Pinkal (04-Feb-2019) -- End

            'Pinkal (07-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If mdtLeaveExpenseTran.Columns.Contains("tdate") Then
                dRow.Item("tdate") = eZeeDate.convertDate(dtpApprovalClaimDate.GetDate.Date).ToString()
            End If
            'Pinkal (07-Mar-2019) -- End

            mdtLeaveExpenseTran.Rows.Add(dRow) : Call FillApprovalExpense()
            Call EnableDisable_Controls(False) : Call ClearControls()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub EditForApprovalExpense()
        Try
            Dim iRow As DataRow() = Nothing
            If mintEditClaimTranId > 0 Then
                iRow = mdtLeaveExpenseTran.Select("crtranunkid = '" & mintEditClaimTranId & "' AND AUD <> 'D'")
            Else
                iRow = mdtLeaveExpenseTran.Select("GUID = '" & mstrEditGUID & "' AND AUD <> 'D'")
            End If

            If iRow IsNot Nothing Then

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                Dim dtmp() As DataRow = Nothing
                If CInt(iRow(0).Item("crtranunkid")) > 0 Then
                    dtmp = mdtLeaveExpenseTran.Select("crtranunkid <> '" & iRow(0).Item("crtranunkid").ToString() & "' AND  expenseunkid = '" & CInt(cboApprovalExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboApprovalSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtApprovalQty.Text.Length > 0, CDec(txtApprovalQty.Text), 0)) & _
                                                         "' AND unitprice = '" & CDec(IIf(txtApprovalUnitPrice.Text.Trim.Length > 0, CDec(txtApprovalUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtApprovalExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                Else
                    dtmp = mdtLeaveExpenseTran.Select("GUID <> '" & iRow(0).Item("GUID").ToString() & "' AND  expenseunkid = '" & CInt(cboApprovalExpense.SelectedValue) & "' AND secrouteunkid = '" & CInt(cboApprovalSectorRoute.SelectedValue) & "' AND quantity = '" & CDec(IIf(txtApprovalQty.Text.Length > 0, CDec(txtApprovalQty.Text), 0)) & _
                                                      "' AND unitprice = '" & CDec(IIf(txtApprovalUnitPrice.Text.Trim.Length > 0, CDec(txtApprovalUnitPrice.Text), 0)) & "' AND expense_remark = '" & txtApprovalExpRemark.Text.Trim() & "' AND AUD <> 'D'")
                End If

                If dtmp.Length > 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 10, "Sorry, you cannot add same expense again in the below list."), Me)
                    Exit Sub
                End If
                'Pinkal (04-Feb-2019) -- End


                Dim objExpMaster As New clsExpense_Master
                objExpMaster._Expenseunkid = CInt(cboApprovalExpense.SelectedValue)

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                mblnIsHRExpenseApprovalClaimRequest = objExpMaster._IsHRExpense
                mblnIsClaimFormBudgetMandatoryApprovalClaimRequest = objExpMaster._IsBudgetMandatory
                Dim mintGLCodeID As Integer = objExpMaster._GLCodeId
                Dim mstrGLCode As String = ""
                Dim mstrGLDescription As String = objExpMaster._Description
                Dim mintCostCenterID As Integer = 0
                Dim mstrCostCenterCode As String = ""
                If mblnIsClaimFormBudgetMandatoryApprovalClaimRequest AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex AndAlso CInt(cboApprovalCostCenter.SelectedValue) <= 0 Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 28, "Cost Center is mandatory information. Please select Cost Center to continue."), Me)
                        cboCostCenter.Focus()
                        Exit Sub
                    End If
                    GetP2PRequireData(objExpMaster._ExpenditureTypeId, mintGLCodeID, mstrGLCode, mintCostCenterID, mstrCostCenterCode)
                End If
                'Pinkal (04-Feb-2019) -- End


                If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
                    If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
                        'Pinkal (04-Feb-2019) -- Start
                        'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                        If objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_TotalBalance Then
                            If CDec(txtApprovalQty.Text) > CDec(txtApprovalBalance.Text) Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
                                txtApprovalQty.Focus()
                                Exit Sub
                            End If
                        ElseIf objExpMaster._AccrueSetting = clsExpense_Master.enExpAccrueSetting.Issue_Qty_Balance_Ason_Date Then
                            If CDec(txtApprovalQty.Text) > CDec(txtApprovalBalance.Text) Then
                                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 24, "Sorry, you cannot set quantity greater than balance as on date set."), Me)
                                txtApprovalQty.Focus()
                                Exit Sub
                            End If
                        End If

                    ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
                        If CDec(txtApprovalBalance.Text) < CDec(CDec(txtApprovalQty.Text) * CDec(txtApprovalUnitPrice.Text)) Then
                            'Language.setLanguage(mstrModuleNameClaim)
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 19, "Sorry, you cannot set amount greater than balance set."), Me)
                            txtApprovalQty.Focus()
                            Exit Sub
                        End If
                    End If
                End If

                If objExpMaster._IsBudgetMandatory AndAlso Session("BgtRequestValidationP2PServiceURL").ToString().Trim.Length > 0 Then
                    Dim mdecBudgetAmount As Decimal = 0
                    If CheckBudgetRequestValidationForP2P(Session("BgtRequestValidationP2PServiceURL").ToString().Trim(), mstrCostCenterCode, mstrGLCode, mdecBudgetAmount) = False Then Exit Sub
                    If mdecBudgetAmount < (CDec(txtApprovalQty.Text) * CDec(txtApprovalUnitPrice.Text)) Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount."), Me)
                        Exit Sub
                    End If
                End If
                'Pinkal (04-Feb-2019) -- End

                If dgvApprovalData.Items.Count >= 1 Then
                    Dim objExpMasterOld As New clsExpense_Master
                    Dim iEncashment As DataRow() = Nothing

                    If CInt(dgvApprovalData.Items(0).Cells(10).Text) > 0 Then
                        iEncashment = mdtLeaveExpenseTran.Select("crtranunkid = '" & CInt(dgvApprovalData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
                    ElseIf dgvApprovalData.Items(0).Cells(12).Text.ToString <> "" Then
                        iEncashment = mdtLeaveExpenseTran.Select("GUID = '" & dgvApprovalData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
                    End If
                    objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())

                    If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
                        'Language.setLanguage(mstrModuleNameClaim)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
                        Exit Sub

                        'Pinkal (20-Nov-2018) -- Start
                        'Enhancement - Working on P2P Integration for NMB.
                    ElseIf objExpMaster._IsHRExpense <> objExpMasterOld._IsHRExpense Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form."), Me)
                        Exit Sub
                        'Pinkal (20-Nov-2018) -- End

                    End If
                    objExpMasterOld = Nothing
                End If
                objExpMaster = Nothing

                iRow(0).Item("crtranunkid") = mintEditClaimTranId
                iRow(0).Item("crmasterunkid") = mintClaimMstUnkId
                iRow(0).Item("expenseunkid") = CInt(cboApprovalExpense.SelectedValue)
                iRow(0).Item("secrouteunkid") = CInt(cboApprovalSectorRoute.SelectedValue)
                iRow(0).Item("costingunkid") = IIf(txtApprovalCostingTag.Value Is Nothing, 0, txtApprovalCostingTag.Value)
                iRow(0).Item("unitprice") = txtApprovalUnitPrice.Text
                iRow(0).Item("quantity") = txtApprovalQty.Text
                iRow(0).Item("amount") = CDec(txtApprovalUnitPrice.Text) * CDec(txtApprovalQty.Text)
                iRow(0).Item("expense_remark") = txtApprovalExpRemark.Text
                iRow(0).Item("isvoid") = False
                iRow(0).Item("voiduserunkid") = -1
                iRow(0).Item("voiddatetime") = DBNull.Value
                iRow(0).Item("voidreason") = ""
                iRow(0).Item("loginemployeeunkid") = -1
                If iRow(0).Item("AUD").ToString.Trim = "" Then
                    iRow(0).Item("AUD") = "U"
                End If
                iRow(0).Item("GUID") = Guid.NewGuid.ToString
                iRow(0).Item("expense") = cboApprovalExpense.SelectedItem.Text
                iRow(0).Item("uom") = txtApprovalUoMType.Text
                iRow(0).Item("voidloginemployeeunkid") = -1
                iRow(0).Item("sector") = IIf(CInt(cboApprovalSectorRoute.SelectedValue) <= 0, "", cboApprovalSectorRoute.SelectedItem.Text)

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                iRow(0).Item("costcenterunkid") = mintCostCenterID
                iRow(0).Item("costcentercode") = mstrCostCenterCode
                iRow(0).Item("glcodeunkid") = mintGLCodeID
                iRow(0).Item("glcode") = mstrGLCode
                iRow(0).Item("gldesc") = mstrGLDescription
                iRow(0).Item("isbudgetmandatory") = mblnIsClaimFormBudgetMandatoryApprovalClaimRequest
                iRow(0).Item("ishrexpense") = mblnIsHRExpenseApprovalClaimRequest
                iRow(0).Item("countryunkid") = CInt(cboApprovalCurrency.SelectedValue)
                iRow(0).Item("base_countryunkid") = mintApprovalBaseCountryId

                Dim mintExchangeRateId As Integer = 0
                Dim mdecBaseAmount As Decimal = 0
                Dim mdecExchangeRate As Decimal = 0
                GetCurrencyRate(CInt(cboApprovalCurrency.SelectedValue), CDec(IIf(txtApprovalUnitPrice.Text.Trim.Length > 0, txtApprovalUnitPrice.Text, 0)), CDec(IIf(txtApprovalQty.Text.Trim.Length > 0, txtApprovalQty.Text, 0)), dtpApprovalClaimDate.GetDate.Date, mintExchangeRateId, mdecExchangeRate, mdecBaseAmount)
                iRow(0).Item("base_amount") = mdecBaseAmount
                iRow(0).Item("exchangerateunkid") = mintExchangeRateId
                iRow(0).Item("exchange_rate") = mdecExchangeRate
                'Pinkal (04-Feb-2019) -- End


                mdtLeaveExpenseTran.AcceptChanges()

                Call FillApprovalExpense() : Call ClearControls()

                btnApprovalEdit.Visible = False : btnApprovalAdd.Visible = True
                cboApprovalExpense.Enabled = True
                iRow = Nothing
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (22-Oct-2018) -- End


#End Region

    'Pinkal (10-Jan-2017) -- End

    'Pinkal (29-Sep-2017) -- Start
    'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
    Private Function IsValidExpense_Eligility() As Boolean
        Dim blnFlag As Boolean = False
        Try
            If mintLeaveFormUnkId > 0 Then
                Dim objlvform As New clsleaveform
                objlvform._Formunkid = mintLeaveFormUnkId
                Dim objlvtype As New clsleavetype_master
                objlvtype._Leavetypeunkid = objlvform._Leavetypeunkid
                If objlvtype._EligibilityOnAfter_Expense > 0 Then
                    intEligibleExpenseDays = objlvtype._EligibilityOnAfter_Expense
                    If CDec(NoofDays) >= objlvtype._EligibilityOnAfter_Expense Then
                        blnFlag = True
                    End If
                Else
                    blnFlag = True
                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objlvform = Nothing
                objlvtype = Nothing
                'Pinkal (11-Sep-2020) -- End
            End If
            Return blnFlag
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "IsValidExpense_Eligility", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Function
    'Pinkal (29-Sep-2017) -- End

    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    Private Sub GetP2PRequireData(ByVal xExpenditureTypeId As Integer, ByVal xGLCodeId As Integer, ByRef mstrGLCode As String, ByRef mintEmpCostCenterId As Integer, ByRef mstrCostCenterCode As String)
        Try

            If xExpenditureTypeId <> enP2PExpenditureType.None Then
                If xGLCodeId > 0 Then
                    Dim objAccout As New clsAccount_master
                    objAccout._Accountunkid = xGLCodeId
                    mstrGLCode = objAccout._Account_Code
                    objAccout = Nothing
                End If

                If xExpenditureTypeId = enP2PExpenditureType.Opex Then
                    If mblnClaimRequest Then
                        mintEmpCostCenterId = GetEmployeeCostCenter(dtpDate.GetDate.Date, CInt(cboClaimEmployee.SelectedValue))
                    ElseIf mblnApprovalClaimRequest Then
                        mintEmpCostCenterId = GetEmployeeCostCenter(dtpApprovalClaimDate.GetDate.Date, CInt(cboApprovalExpEmployee.SelectedValue))
                    End If
                    If mintEmpCostCenterId > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        objCostCenter._Costcenterunkid = mintEmpCostCenterId
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If

                ElseIf xExpenditureTypeId = enP2PExpenditureType.Capex Then
                    If CInt(cboCostCenter.SelectedValue) > 0 Then
                        Dim objCostCenter As New clscostcenter_master
                        If mblnClaimRequest Then
                            objCostCenter._Costcenterunkid = CInt(cboCostCenter.SelectedValue)
                        ElseIf mblnApprovalClaimRequest Then
                            objCostCenter._Costcenterunkid = CInt(cboApprovalCostCenter.SelectedValue)
                        End If
                        mintEmpCostCenterId = objCostCenter._Costcenterunkid
                        mstrCostCenterCode = objCostCenter._Costcentercode
                        objCostCenter = Nothing
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Function CheckBudgetRequestValidationForP2P(ByVal strServiceURL As String, ByVal xCostCenterCode As String, ByVal xGLCode As String, ByRef mdecBudgetAmount As Decimal) As Boolean
        Dim mstrError As String = ""
        Try
            If xGLCode.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 40, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense."), Me)
                Return False
            ElseIf xCostCenterCode.Trim.Length <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 41, "Cost Center Code is compulsory information for budget request validation.Please set Cost Center Code with this current employee/expense."), Me)
                Return False
            End If

            If xCostCenterCode.Trim.Length > 0 AndAlso xGLCode.Trim.Length > 0 Then
                Dim mstrBgtRequestValidationP2PServiceURL As String = Session("BgtRequestValidationP2PServiceURL").ToString().Trim() & "?costCenter=" & xCostCenterCode.Trim() & "&glcode=" & xGLCode.Trim()
                Dim mstrGetData As String = ""
                Dim objMstData As New clsMasterData
                If objMstData.GetSetP2PWebRequest(mstrBgtRequestValidationP2PServiceURL, True, True, "", mstrGetData, mstrError) = False Then
                    DisplayMessage.DisplayMessage(mstrError, Me)
                    Return False
                End If

                objMstData = Nothing
                'mstrGetData = "{""amount"":""10000""}"

                If mstrGetData.Trim.Length > 0 Then
                    Dim dtAmount As DataTable = JsonStringToDataTable(mstrGetData)
                    If dtAmount IsNot Nothing AndAlso dtAmount.Rows.Count > 0 Then
                        mdecBudgetAmount = CDec(dtAmount.Rows(0)("amount"))
                    End If
                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtAmount IsNot Nothing Then dtAmount.Clear()
                    dtAmount = Nothing
                    'Pinkal (11-Sep-2020) -- End
                End If

            End If
            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
    End Function

    Private Function GetEmployeeCostCenter(ByVal dtClaimDate As Date, ByVal xEmployeeId As Integer) As Integer
        Dim mintEmpCostCenterId As Integer = 0
        Try
            Dim objEmpCC As New clsemployee_cctranhead_tran
            Dim dsList As DataSet = objEmpCC.Get_Current_CostCenter_TranHeads(dtClaimDate.Date, True, xEmployeeId)
            objEmpCC = Nothing
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintEmpCostCenterId = CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid"))
            End If
            dsList.Clear()
            dsList = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return mintEmpCostCenterId
    End Function

    Private Sub GetCurrencyRate(ByVal xCurrencyId As Integer, ByVal mdecUnitPrice As Decimal, ByVal mdecQty As Decimal, ByVal xCurrencyDate As Date, ByRef mintExchangeRateId As Integer, ByRef mdecExchangeRate As Decimal, ByRef mdecBaseAmount As Decimal)
        Try
            Dim objExchange As New clsExchangeRate
            Dim dsList As DataSet = objExchange.GetList("List", True, False, 0, xCurrencyId, True, xCurrencyDate, False, Nothing)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintExchangeRateId = CInt(dsList.Tables(0).Rows(0).Item("exchangerateunkid"))
                mdecExchangeRate = CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
                mdecBaseAmount = (mdecUnitPrice * mdecQty) / CDec(dsList.Tables(0).Rows(0).Item("exchange_rate"))
            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            'Pinkal (11-Sep-2020) -- End

            objExchange = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (04-Feb-2019) -- End


    'Pinkal (07-Mar-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Private Function CheckForOccurrence(ByVal mdtTran As DataTable, ByVal iEmployeeId As Integer, ByVal iExpenseId As Integer, ByVal dtClaimDate As Date) As Boolean
        Try
            If mdtTran IsNot Nothing Then
                Dim objExpBalance As New clsEmployeeExpenseBalance
                Dim dsBalance As DataSet = objExpBalance.Get_Balance(iEmployeeId, iExpenseId, CInt(Session("Fin_year")), dtClaimDate.Date _
                                                                                               , CBool(IIf(CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC, True, False)), CBool(IIf(CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC, True, False)))

                If dsBalance IsNot Nothing AndAlso dsBalance.Tables(0).Rows.Count > 0 Then
                    If CInt(dsBalance.Tables(0).Rows(0)("occurrence")) > 0 AndAlso CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) > 0 Then

                        Dim xOccurrence As Integer = 0
                        If mintClaimRequestApprovalExpTranId > 0 Then
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("crtranunkid") <> mintClaimRequestApprovalExpTranId And x.Field(Of Integer)("expenseunkid") = iExpenseId And x.Field(Of String)("AUD") <> "D").Count()
                        ElseIf mstrClaimRequestApprovalExpGUID.Trim.Length > 0 Then
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of String)("GUID") <> mstrClaimRequestApprovalExpGUID.Trim() And x.Field(Of Integer)("expenseunkid") = iExpenseId And x.Field(Of String)("AUD") <> "D").Count()
                        Else
                            xOccurrence = mdtTran.AsEnumerable().Where(Function(x) x.Field(Of Integer)("expenseunkid") = iExpenseId And x.Field(Of String)("AUD") <> "D").Count()
                        End If

                        If xOccurrence >= CInt(dsBalance.Tables(0).Rows(0)("remaining_occurrence")) Then
                            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsclaim_request_master", 3, "Sorry, you cannot apply for this expense as you can only apply for [ ") & CInt(dsBalance.Tables(0).Rows(0)("occurrence")) & _
                               Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsclaim_request_master", 4, " ] time(s)."), Me)
                            'Pinkal (11-Sep-2020) -- Start
                            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                            If dsBalance IsNot Nothing Then dsBalance.Clear()
                            dsBalance = Nothing
                            objExpBalance = Nothing
                            'Pinkal (11-Sep-2020) -- End
                            Return False
                        End If
                    End If
                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsBalance IsNot Nothing Then dsBalance.Clear()
                dsBalance = Nothing
                objExpBalance = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            Return False
        End Try
        Return True
    End Function
    'Pinkal (07-Mar-2019) -- End


#End Region

#Region "Button Event(S)"

    Protected Sub btnsave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click, btnReject.Click, btnReschedule.Click
        Try
            If mintLeaveFormUnkId > 0 Then
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Dim objPendingLeave As New clspendingleave_Tran
                Dim objPendingLeave As New clspendingleave_Tran
                'Pinkal (11-Sep-2020) -- End
                Dim dsList As DataSet = objPendingLeave.GetList("PendingLeave", _
                                                                Session("Database_Name").ToString(), _
                                                                CInt(Session("UserId")), _
                                                                CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                                                CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                "pendingleavetranunkid = " & mintPenddingLvTranUnkId & " ")
                If dsList.Tables("PendingLeave").Rows.Count > 0 Then
                    If CInt(dsList.Tables("PendingLeave").Rows(0)("statusunkid")) = 7 Then
                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        If dsList IsNot Nothing Then dsList.Clear()
                        dsList = Nothing
                        objPendingLeave = Nothing
                        'Pinkal (11-Sep-2020) -- End
                        Exit Sub
                    ElseIf CInt(dsList.Tables("PendingLeave").Rows(0)("statusunkid")) = 1 Then
                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        If dsList IsNot Nothing Then dsList.Clear()
                        dsList = Nothing
                        objPendingLeave = Nothing
                        'Pinkal (11-Sep-2020) -- End
                        Exit Sub
                    End If
                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dsList IsNot Nothing Then dsList.Clear()
                dsList = Nothing
                objPendingLeave = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If



            'Pinkal (05-Jun-2020) -- Start
            'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
            If mblnDoNotApplyForFutureDates Then
                'Language.setLanguage(mstrModuleName2)
                If dtpStartdate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "Sorry, you cannot approve/reject this leave application.Reason:As Per setting configured on leave type master you can not approve/reject for future date(s)."), Me)
                    Exit Sub
                ElseIf dtpEnddate.GetDate.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 32, "Sorry, you cannot approve/reject this leave application.Reason:As Per setting configured on leave type master you can not approve/reject for future date(s)."), Me)
                    Exit Sub
                End If
            End If
            'Pinkal (05-Jun-2020) -- End



            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'Call EntrySave()
            If CType(sender, Button).ID.ToUpper() = btnApprove.ID.ToUpper() Then
                xStatusId = 1
            ElseIf CType(sender, Button).ID.ToUpper() = btnReject.ID.ToUpper() Then
                xStatusId = 3
            ElseIf CType(sender, Button).ID.ToUpper() = btnReschedule.ID.ToUpper() Then
                xStatusId = 4
            End If
            EntrySave()
            'Pinkal (25-May-2019) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Confirmation_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Confirmation.buttonYes_Click, LeaveApprovalConfirmation.buttonYes_Click
        Try
            Dim clsPendingLeave As New clspendingleave_Tran
            clsPendingLeave._Pendingleavetranunkid = mintPenddingLvTranUnkId
            FinalSave(clsPendingLeave)
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            clsPendingLeave = Nothing
            'Pinkal (11-Sep-2020) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnclose.Click
        Try

            'Pinkal (10-Jan-2017) -- Start
            'Enhancement - Working on TRA C&R Module Changes with Leave Module.
            Me.Session.Remove("mdtClaimAttchment")
            Me.Session.Remove("mdtFinalClaimAttchment")
            Me.Session.Remove("mdtChildClaimAttachment")
            'Pinkal (10-Jan-2017) -- End

            If Request.QueryString.ToString.Length <= 0 Then
                Response.Redirect(Session("rootpath").ToString & "Leave/wPg_ProcessLeaveList.aspx", False)
            Else
                Response.Redirect(Session("rootpath").ToString & "Index.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_ShowAttchment_btnPreviewClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ShowAttchment.btnPreviewClose_Click
        Try
            popup_ShowAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


#Region "Days Fraction"

    Protected Sub btnFractionOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFractionOK.Click
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objPendingLeave As New clspendingleave_Tran
        Dim dtFraction As DataTable = Nothing
        'Pinkal (11-Sep-2020) -- End
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim dtFraction As DataTable = mdtFraction
            If mdtFraction IsNot Nothing Then
                dtFraction = mdtFraction.Copy()
            End If
            'Pinkal (11-Sep-2020) -- End


            Dim dtTable As DataTable = New DataView(dtFraction, "AUD = 'D'", "", DataViewRowState.CurrentRows).ToTable

            objPendingLeave._Pendingleavetranunkid = mintPenddingLvTranUnkId

            For Each dr As DataRow In dtFraction.Rows
                If mintFreactionApproverId <> objPendingLeave._Approverunkid AndAlso dr("AUD").ToString().Trim <> "D" Then
                    dr("AUD") = "A"
                Else
                    If dr("AUD").ToString().Trim = "" AndAlso dr.RowState = DataRowState.Modified Then
                        dr("AUD") = "U"
                    End If

                End If
            Next

            ' START IMPORT DELETED ALL ROWS FROM DTTABLE TO DTFRACTION WHEN OLD ROWS IS ALREADY IN DATABASE TO VOID
            For Each dr As DataRow In dtTable.Rows
                Dim drDeleteRow() As DataRow = dtFraction.Select("leavedate ='" & CDate(dr("leavedate")) & "' AND AUD = 'D' AND dayfraction = " & CDec(dr("dayfraction")))
                If drDeleteRow.Length <= 0 Then
                    dtFraction.ImportRow(dr)
                End If
            Next
            ' END IMPORT DELETED ALL ROWS FROM DTTABLE TO DTFRACTION WHEN OLD ROWS IS ALREADY IN DATABASE TO VOID
            If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count > 0 Then
                objNoofDays.Text = "Total Days : " & CDec(dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'")).ToString("#0.00")
                'Pinkal (29-Sep-2017) -- Start
                'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
                NoofDays = CDec(dtFraction.Compute("SUM(dayfraction)", "AUD <> 'D'"))
                'Pinkal (29-Sep-2017) -- End
            End If


            dtFraction.AcceptChanges()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'mdtFraction = dtFraction
            mdtFraction = dtFraction.Copy()
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtFraction IsNot Nothing Then dtFraction.Clear()
            dtFraction = Nothing
            objPendingLeave = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub
#End Region

#Region "Expesnse"

    Protected Sub btnClaimAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClaimAdd.Click
        Try

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            'If Validation() = False Then
            If Validation(sender) = False Then
                'Pinkal (04-Feb-2020) -- End
                Exit Sub
            End If

            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .

            'Dim mdtTran As DataTable = mdtExpense
            'Dim dtmp() As DataRow = Nothing
            'If mdtTran.Rows.Count > 0 Then
            '    dtmp = mdtTran.Select("expenseunkid = '" & CInt(cboExpense.SelectedValue) & "' AND AUD <> 'D'")
            '    If dtmp.Length > 0 Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 10, "Sorry, you cannot add same expense again in the below list."), Me)
            '        Exit Sub
            '    End If
            'End If
            'Dim objExpMaster As New clsExpense_Master
            'objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)

            'If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
            '    If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
            '        If CDec(txtCalimQty.Text) > CDec(txtClaimBalance.Text) Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
            '            txtCalimQty.Focus()
            '            Exit Sub
            '        End If
            '    ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
            '        If CDec(CDec(txtCalimQty.Text) * CDec(txtUnitPrice.Text)) > CDec(txtClaimBalance.Text) Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 19, "Sorry, you cannot set amount greater than balance set."), Me)
            '            txtCalimQty.Focus()
            '            Exit Sub
            '        End If
            '    End If
            'End If

            'If dgvData.Items.Count >= 1 Then
            '    Dim objExpMasterOld As New clsExpense_Master
            '    Dim iEncashment As DataRow() = Nothing
            '    If CInt(dgvData.Items(0).Cells(10).Text) > 0 Then
            '        iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
            '    ElseIf dgvData.Items(0).Cells(12).Text.ToString <> "" Then
            '        iEncashment = mdtTran.Select("GUID = '" & dgvData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
            '    End If

            '    objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
            '    If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
            '        Exit Sub
            '    End If
            '    objExpMasterOld = Nothing
            'End If
            'objExpMaster = Nothing

            'Dim dRow As DataRow = mdtTran.NewRow

            'dRow.Item("crtranunkid") = -1
            'dRow.Item("crmasterunkid") = mintClaimMstUnkId
            'dRow.Item("expenseunkid") = CInt(cboExpense.SelectedValue)
            'dRow.Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            'dRow.Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing OrElse txtCostingTag.Value = "", 0, txtCostingTag.Value)
            'dRow.Item("unitprice") = CDec(txtUnitPrice.Text)
            'dRow.Item("quantity") = txtCalimQty.Text
            'dRow.Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtCalimQty.Text)
            'dRow.Item("expense_remark") = txtExpRemark.Text
            'dRow.Item("isvoid") = False
            'dRow.Item("voiduserunkid") = -1
            'dRow.Item("voiddatetime") = DBNull.Value
            'dRow.Item("voidreason") = ""
            ''dRow.Item("iscancel") = False
            ''dRow.Item("canceluserunkid") = -1
            ''dRow.Item("cancel_datetime") = DBNull.Value
            ''dRow.Item("cancel_remark") = ""
            'dRow.Item("AUD") = "A"
            'dRow.Item("GUID") = Guid.NewGuid.ToString
            'dRow.Item("expense") = cboExpense.SelectedItem.Text
            'dRow.Item("uom") = txtUoMType.Text

            ''Pinkal (10-Jan-2017) -- Start
            ''Enhancement - Working on TRA C&R Module Changes with Leave Module.
            'dRow.Item("employeeunkid") = CInt(cboClaimEmployee.SelectedValue)
            ''Pinkal (10-Jan-2017) -- End
            'dRow.Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            'dRow.Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)


            ''Pinkal (10-Jan-2017) -- Start
            ''Enhancement - Working on TRA C&R Module Changes with Leave Module.
            'dRow.Item("crapprovaltranunkid") = -1
            ''Pinkal (10-Jan-2017) -- End


            'mdtTran.Rows.Add(dRow)
            'mdtExpense = mdtTran
            'Fill_Expense()
            'Enable_Disable_Ctrls(False)
            'Clear_Controls()


            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count >= 1 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End

            mblnIsExpenseApproveClick = False
            mblnIsExpenseEditClick = False

            If IsNumeric(txtUnitPrice.Text) = False Then
                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0.00"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End
            End If

            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 26, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 45, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 26, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    AddExpense()
                End If
                'Pinkal (04-Feb-2019) -- End
            End If

            'Pinkal (22-Oct-2018) -- End


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClaimAdd_Click:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnclaimEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClaimEdit.Click
        Try

            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .

            'Dim iRow As DataRow() = Nothing
            'Dim mdtTran As DataTable = mdtExpense
            'If Validation() = False Then
            '    Exit Sub
            'End If


            ''Pinkal (10-Jan-2017) -- Start
            ''Enhancement - Working on TRA C&R Module Changes with Leave Module.
            ''If mintEditClaimTranId > 0 Then
            ''    iRow = mdtTran.Select("crtranunkid = '" & mintEditClaimTranId & "' AND AUD <> 'D'")
            ''Else
            ''    iRow = mdtTran.Select("GUID = '" & mstrEditGUID & "' AND AUD <> 'D'")
            ''End If
            'If mintEditClaimTranId > 0 Then
            '    iRow = mdtTran.Select("crapprovaltranunkid = '" & mintEditClaimTranId & "' AND AUD <> 'D'")
            'Else
            '    iRow = mdtTran.Select("GUID = '" & mstrEditGUID & "' AND AUD <> 'D'")
            'End If
            ''Pinkal (10-Jan-2017) -- End



            'If iRow IsNot Nothing Then
            '    Dim objExpMaster As New clsExpense_Master
            '    objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)

            '    If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
            '        If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
            '            If CDec(txtCalimQty.Text) > CDec(txtClaimBalance.Text) Then
            '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 10, "Sorry, you cannot set quantity greater than balance set."), Me)
            '                txtCalimQty.Focus()
            '                Exit Sub
            '            End If
            '        ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
            '            If CDec(txtClaimBalance.Text) < CDec(CDec(txtCalimQty.Text) * CDec(txtUnitPrice.Text)) Then
            '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 11, "Sorry, you cannot set amount greater than balance set."), Me)
            '                txtCalimQty.Focus()
            '                Exit Sub
            '            End If
            '        End If
            '    End If

            '    If dgvData.Items.Count >= 1 Then
            '        Dim objExpMasterOld As New clsExpense_Master
            '        Dim iEncashment As DataRow() = Nothing

            '        If CInt(dgvData.Items(0).Cells(10).Text) > 0 Then
            '            iEncashment = mdtTran.Select("crtranunkid = '" & CInt(dgvData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
            '        ElseIf dgvData.Items(0).Cells(12).Text.ToString <> "" Then
            '            iEncashment = mdtTran.Select("GUID = '" & dgvData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
            '        End If

            '        objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
            '        If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
            '            Exit Sub
            '        End If
            '        objExpMasterOld = Nothing
            '    End If
            '    objExpMaster = Nothing

            '    iRow(0).Item("crtranunkid") = iRow(0).Item("crtranunkid")
            '    iRow(0).Item("crmasterunkid") = mintClaimMstUnkId
            '    iRow(0).Item("expenseunkid") = CInt(cboExpense.SelectedValue)
            '    iRow(0).Item("secrouteunkid") = CInt(cboSectorRoute.SelectedValue)
            '    iRow(0).Item("costingunkid") = IIf(txtCostingTag.Value Is Nothing, 0, txtCostingTag.Value)
            '    iRow(0).Item("unitprice") = txtUnitPrice.Text
            '    iRow(0).Item("quantity") = txtCalimQty.Text
            '    iRow(0).Item("amount") = CDec(txtUnitPrice.Text) * CDec(txtCalimQty.Text)
            '    iRow(0).Item("expense_remark") = txtExpRemark.Text
            '    iRow(0).Item("isvoid") = False
            '    iRow(0).Item("voiduserunkid") = -1
            '    iRow(0).Item("voiddatetime") = DBNull.Value
            '    iRow(0).Item("voidreason") = ""
            '    If iRow(0).Item("AUD").ToString.Trim = "" Then
            '        iRow(0).Item("AUD") = "U"
            '    End If
            '    iRow(0).Item("GUID") = Guid.NewGuid.ToString
            '    iRow(0).Item("expense") = cboExpense.SelectedItem.Text
            '    iRow(0).Item("uom") = txtUoMType.Text
            '    iRow(0).Item("sector") = IIf(CInt(cboSectorRoute.SelectedValue) <= 0, "", cboSectorRoute.SelectedItem.Text)
            '    iRow(0).AcceptChanges()
            '    mdtExpense = mdtTran
            '    Fill_Expense()
            '    Clear_Controls()
            '    btnClaimEdit.Visible = False : btnClaimAdd.Visible = True
            '    cboExpense.Enabled = True
            '    iRow = Nothing
            'End If

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            'If Validation() = False Then
            If Validation(sender) = False Then
                'Pinkal (04-Feb-2020) -- End
                Exit Sub
            End If

            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count > 1 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End

            mblnIsExpenseApproveClick = False
            mblnIsExpenseEditClick = True

            If IsNumeric(txtUnitPrice.Text) = False Then
                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0.00"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End
            End If

            If CInt(txtUnitPrice.Text) <= 0 Then
                popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 26, "Do you wish to continue?")
                popup_UnitPriceYesNo.Show()
            Else
                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 45, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 26, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    EditExpense()
                End If
                'Pinkal (04-Feb-2019) -- End
            End If
            'Pinkal (22-Oct-2018) -- End


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnEdit_Click:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub btnDelReasonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelReasonYes.Click
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim iRow As DataRow() = Nothing
        Dim mdtTran As DataTable = Nothing
        'Pinkal (11-Sep-2020) -- End
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim mdtTran As DataTable = mdtExpense
            If mdtExpense IsNot Nothing Then
                mdtTran = mdtExpense.Copy()
            End If
            'Pinkal (11-Sep-2020) -- End

            If mintEditClaimTranId > 0 Then
                If txtClaimDelReason.Text.Trim.Length > 0 Then
                    iRow = mdtTran.Select("crtranunkid = '" & mintEditClaimTranId & "' AND AUD <> 'D'")
                    iRow(0).Item("isvoid") = True
                    iRow(0).Item("voiduserunkid") = Session("UserId")
                    iRow(0).Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
                    iRow(0).Item("voidreason") = txtClaimDelReason.Text.Trim
                    iRow(0).Item("AUD") = "D"
                    iRow(0).AcceptChanges()

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    'mdtExpense = mdtTran
                    mdtExpense = mdtTran.Copy()
                    'Pinkal (11-Sep-2020) -- End

                    Fill_Expense()
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 43, "Sorry, Delete reason is Mendtory information, Please enter delete reason"), Me)
                    popupClaimDelete.Show()
                End If
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 44, "Sorry, Expense Delete process fail."), Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            iRow = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub btnClaimSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClaimSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Is_Valid() = False Then Exit Sub

            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .

            'Dim mdtTran As DataTable = mdtExpense


            ''Pinkal (29-Feb-2016) -- Start
            ''Enhancement - Adding Occurrence/Remaining Occurrence in Claim Expense Balance Requested By Rutta.
            'If mdtTran IsNot Nothing Then
            '    For Each dRow As DataRow In mdtTran.Rows
            '        Dim sMsg As String = String.Empty
            '        Dim objClaimMaster As New clsclaim_request_master
            '        sMsg = objClaimMaster.IsValid_Expense(mintClaimMstUnkId, CInt(dRow("expenseunkid")))
            '        objClaimMaster = Nothing
            '        If sMsg <> "" Then
            '            DisplayMessage.DisplayMessage(sMsg, Me)
            '            Exit Sub
            '        End If
            '        sMsg = ""
            '    Next
            'End If
            ''Pinkal (29-Feb-2016) -- End


            'If mdtTran IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
            '    Dim drRow() As DataRow = mdtTran.Select("AUD = ''")
            '    If drRow.Length > 0 Then
            '        For Each dr As DataRow In drRow
            '            dr("AUD") = "U"
            '            dr.AcceptChanges()
            '        Next
            '    End If
            'End If

            'mdtFinalClaimTransaction = mdtTran.Copy
            'mdtTran.Clear()
            'mdtTran = Nothing
            'mdtExpense = Nothing

            ''Call SetValue()

            ''If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
            ''    mblnPaymentApprovalwithLeaveApproval = Session("PaymentApprovalwithLeaveApproval")
            ''End If
            'mblnClaimRequest = False
            'popupClaimRequest.Hide()


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

            'mblnIsExpenseApproveClick = True
            'mblnIsExpenseEditClick = False

            'If IsNumeric(txtUnitPrice.Text) = False Then txtUnitPrice.Text = "0.00"
            'If CInt(txtUnitPrice.Text) <= 0 Then
            '    popup_UnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 1001, "Some of the expense(s) had 0 unit price for selected employee.") & vbCrLf & _
            '                                                        Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 17, "Do you wish to continue?")
            '    popup_UnitPriceYesNo.Show()
            'Else
            '    EditExpense()
            'End If
            SaveExpense()

            'Pinkal (04-Feb-2019) -- Start

            'Pinkal (22-Oct-2018) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClaimSave_Click:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub btnClaimClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClaimClose.Click
        Try
            cboExpense.Enabled = True
            cboSectorRoute.Enabled = True
            txtUoMType.Text = ""
            txtClaimBalance.Text = ""
            txtCalimQty.Text = "0"

            'Pinkal (07-Feb-2020) -- Start
            'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
            'txtUnitPrice.Text = "0.00"
            txtUnitPrice.Text = "1.00"
            'Pinkal (07-Feb-2020) -- End

            txtClaimRemark.Text = ""
            txtExpRemark.Text = ""
            mblnClaimRequest = False
            popupClaimRequest.Hide()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClaimClose_Click :" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnViewScanAttchment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewScanAttchment.Click
        Dim mstrPreviewIds As String = ""
        Dim dtTable As New DataTable
        Try
            dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(mintEmpUnkId, _
                                                                             enScanAttactRefId.CLAIM_REQUEST, _
                                                                             enImg_Email_RefId.Claim_Request, _
                                                                             mintClaimMstUnkId)
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mstrPreviewIds = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
            End If

            popup_ShowAttchment.ScanTranIds = mstrPreviewIds
            'S.SANDEEP |25-JAN-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
            popup_ShowAttchment._ZipFileName = "Claim_Request_" + txtEmployee.Text.Replace(" ", "_") + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
            'S.SANDEEP |25-JAN-2019| -- END
            popup_ShowAttchment._Webpage = Me
            popup_ShowAttchment.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Protected Sub popup_UnitPriceYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_UnitPriceYesNo.buttonYes_Click
        Try
            If mblnIsExpenseEditClick = True AndAlso mblnIsExpenseApproveClick = False Then
                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 45, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 26, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    EditExpense()
                End If
                'Pinkal (04-Feb-2019) -- End
            ElseIf mblnIsExpenseEditClick = False AndAlso mblnIsExpenseApproveClick = False Then
                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If txtExpRemark.Text.Trim.Length <= 0 Then
                    popup_ExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 45, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 26, "Do you wish to continue?")
                    popup_ExpRemarkYesNo.Show()
                Else
                    AddExpense()
                End If
                'Pinkal (04-Feb-2019) -- End
            ElseIf mblnIsExpenseEditClick = False AndAlso mblnIsExpenseApproveClick = True Then
                SaveExpense()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (22-Oct-2018) -- End

    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Protected Sub popup_ExpRemarkYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ExpRemarkYesNo.buttonYes_Click
        Try
            If mblnIsExpenseEditClick = True AndAlso mblnIsExpenseApproveClick = False Then
                EditExpense()
            ElseIf mblnIsExpenseEditClick = False AndAlso mblnIsExpenseApproveClick = False Then
                AddExpense()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (04-Feb-2019) -- End

    'S.SANDEEP |16-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
    Protected Sub btnDownloadAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadAll.Click
        Dim strMsg As String = String.Empty
        Try
            If dgv_Attchment.Items.Count <= 0 Then
                DisplayMessage.DisplayMessage("No Files to download.", Me)
                Exit Sub
            End If
            strMsg = DownloadAllDocument("file" & cboApprovalExpEmployee.SelectedItem.Text.Replace(" ", "") + ".zip", mdtClaimAttchment, CStr(Session("ArutiSelfServiceURL")), IO.Packaging.CompressionOption.Normal)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'S.SANDEEP |16-MAY-2019| -- END
#End Region


    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.

#Region "Add For Approval Expense "

    Protected Sub btnApprovalAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovalAdd.Click
        Try

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            'If Approval_Validation() = False Then
            If Approval_Validation(sender) = False Then
                'Pinkal (04-Feb-2020) -- End
                Exit Sub
            End If

            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvApprovalData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count >= 1 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End


            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .

            mblnIsApprovalExpenseEditClick = False

            If IsNumeric(txtApprovalUnitPrice.Text) = False Then
                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtApprovalUnitPrice.Text = "0.00"
                txtApprovalUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End
            End If

            If CInt(txtApprovalUnitPrice.Text) <= 0 Then
                popup_ApprovalUnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 26, "Do you wish to continue?")
                popup_ApprovalUnitPriceYesNo.Show()
            Else

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If txtApprovalExpRemark.Text.Trim.Length <= 0 Then
                    popup_ApprovalExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 45, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 26, "Do you wish to continue?")
                    popup_ApprovalExpRemarkYesNo.Show()
                Else
                    AddForApprovalExpense()
                End If
                'Pinkal (04-Feb-2019) -- End

            End If



            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'Pinkal (11-Sep-2020) -- End


            'Dim dtmp() As DataRow = Nothing
            'If mdtLeaveExpenseTran.Rows.Count > 0 Then
            '    dtmp = mdtLeaveExpenseTran.Select("expenseunkid = '" & CInt(cboApprovalExpense.SelectedValue) & "' AND AUD <> 'D'")
            '    If dtmp.Length > 0 Then
            '        DisplayMessage.DisplayError(ex, Me)
            '        Exit Sub
            '    End If
            'End If


            'Dim objExpMaster As New clsExpense_Master
            'objExpMaster._Expenseunkid = CInt(cboApprovalExpense.SelectedValue)

            'If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
            '    If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
            '        If CDec(txtApprovalQty.Text) > CDec(txtApprovalBalance.Text) Then
            '            DisplayMessage.DisplayError(ex, Me)
            '            txtApprovalQty.Focus()
            '            Exit Sub
            '        End If
            '    ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
            '        If CDec(txtApprovalBalance.Text) < CDec(CDec(txtApprovalQty.Text) * CDec(txtUnitPrice.Text)) Then
            '            DisplayMessage.DisplayError(ex, Me)
            '            txtApprovalQty.Focus()
            '            Exit Sub
            '        End If
            '    End If
            'End If

            'If dgvApprovalData.Items.Count >= 1 Then
            '    Dim objExpMasterOld As New clsExpense_Master
            '    Dim iEncashment As DataRow() = Nothing
            '    If CInt(dgvApprovalData.Items(0).Cells(10).Text) > 0 Then
            '        iEncashment = mdtLeaveExpenseTran.Select("crtranunkid = '" & CInt(dgvApprovalData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
            '    ElseIf dgvApprovalData.Items(0).Cells(12).Text.ToString <> "" Then
            '        iEncashment = mdtLeaveExpenseTran.Select("GUID = '" & dgvApprovalData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
            '    End If

            '    objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())
            '    If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
            '        'Language.setLanguage(mstrModuleNameClaim)
            '        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
            '        Exit Sub
            '    End If
            '    objExpMasterOld = Nothing
            'End If

            'objExpMaster = Nothing

            'Dim dRow As DataRow = mdtLeaveExpenseTran.NewRow

            'dRow.Item("crtranunkid") = -1
            'dRow.Item("crmasterunkid") = mintClaimMstUnkId
            'dRow.Item("expenseunkid") = CInt(cboApprovalExpense.SelectedValue)
            'dRow.Item("secrouteunkid") = CInt(cboApprovalSectorRoute.SelectedValue)
            'dRow.Item("costingunkid") = IIf(txtApprovalCostingTag.Value Is Nothing OrElse txtApprovalCostingTag.Value = "", 0, txtApprovalCostingTag.Value)
            'dRow.Item("unitprice") = CDec(txtApprovalUnitPrice.Text)
            'dRow.Item("quantity") = txtApprovalQty.Text
            'dRow.Item("amount") = CDec(txtApprovalUnitPrice.Text) * CDec(txtApprovalQty.Text)
            'dRow.Item("expense_remark") = txtApprovalExpRemark.Text
            'dRow.Item("isvoid") = False
            'dRow.Item("voiduserunkid") = -1
            'dRow.Item("voiddatetime") = DBNull.Value
            'dRow.Item("voidreason") = ""
            'dRow.Item("loginemployeeunkid") = -1
            'dRow.Item("AUD") = "A"
            'dRow.Item("GUID") = Guid.NewGuid.ToString
            'dRow.Item("expense") = cboApprovalExpense.SelectedItem.Text
            'dRow.Item("uom") = txtApprovalUoMType.Text
            'dRow.Item("voidloginemployeeunkid") = -1
            'dRow.Item("sector") = IIf(CInt(cboApprovalSectorRoute.SelectedValue) <= 0, "", cboApprovalSectorRoute.SelectedItem.Text)

            'mdtLeaveExpenseTran.Rows.Add(dRow) : Call FillApprovalExpense()
            'Call EnableDisable_Controls(False) : Call ClearControls()

            'Pinkal (22-Oct-2018) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnApprovalEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovalEdit.Click
        Try

            'Pinkal (22-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            'If Approval_Validation() = False Then
            If Approval_Validation(sender) = False Then
                'Pinkal (04-Feb-2020) -- End
                Exit Sub
            End If

            'Pinkal (30-May-2020) -- Start
            'Enhancement Kadcco Employee Claim Report Changes -   Working on Employee Claim Report Changes.
            Dim gRow As IEnumerable(Of DataGridItem) = Nothing
            gRow = dgvApprovalData.Items.Cast(Of DataGridItem).Where(Function(x) x.Visible = True)

            If CBool(Session("AllowOnlyOneExpenseInClaimApplication")) AndAlso gRow.Count > 1 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application."), Me)
                Exit Sub
            End If
            'Pinkal (30-May-2020) -- End

            mblnIsApprovalExpenseEditClick = True


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'If IsNumeric(txtUnitPrice.Text) = False Then txtUnitPrice.Text = "0.00"
            'If CInt(txtUnitPrice.Text) <= 0 Then
            If IsNumeric(txtApprovalUnitPrice.Text) = False Then
                'txtApprovalUnitPrice.Text = "0.00"
                txtApprovalUnitPrice.Text = "1.00"
            End If

            If CInt(txtApprovalUnitPrice.Text) <= 0 Then
                'Pinkal (04-Feb-2019) -- End
                popup_ApprovalUnitPriceYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.") & vbCrLf & _
                                                                    Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 26, "Do you wish to continue?")
                popup_ApprovalUnitPriceYesNo.Show()
            Else
                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                If txtApprovalExpRemark.Text.Trim.Length <= 0 Then
                    popup_ApprovalExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 45, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 26, "Do you wish to continue?")
                    popup_ApprovalExpRemarkYesNo.Show()
                Else
                    EditForApprovalExpense()
                End If
                'Pinkal (04-Feb-2019) -- End

            End If

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If gRow IsNot Nothing Then gRow.ToList.Clear()
            gRow = Nothing
            'Pinkal (11-Sep-2020) -- End


            'Dim iRow As DataRow() = Nothing
            'If Approval_Validation() = False Then
            '    Exit Sub
            'End If
            'If mintEditClaimTranId > 0 Then
            '    iRow = mdtLeaveExpenseTran.Select("crtranunkid = '" & mintEditClaimTranId & "' AND AUD <> 'D'")
            'Else
            '    iRow = mdtLeaveExpenseTran.Select("GUID = '" & mstrEditGUID & "' AND AUD <> 'D'")
            'End If

            'If iRow IsNot Nothing Then
            '    Dim objExpMaster As New clsExpense_Master
            '    objExpMaster._Expenseunkid = CInt(cboApprovalExpense.SelectedValue)

            '    If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then
            '        If objExpMaster._Uomunkid = enExpUoM.UOM_QTY Then
            '            If CDec(txtApprovalQty.Text) > CDec(txtApprovalBalance.Text) Then
            '                'Language.setLanguage(mstrModuleNameClaim)
            '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 11, "Sorry, you cannot set quantity greater than balance set."), Me)
            '                txtApprovalQty.Focus()
            '                Exit Sub
            '            End If
            '        ElseIf objExpMaster._Uomunkid = enExpUoM.UOM_AMOUNT Then
            '            If CDec(txtApprovalBalance.Text) < CDec(CDec(txtApprovalQty.Text) * CDec(txtUnitPrice.Text)) Then
            '                'Language.setLanguage(mstrModuleNameClaim)
            '                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 19, "Sorry, you cannot set amount greater than balance set."), Me)
            '                txtApprovalQty.Focus()
            '                Exit Sub
            '            End If
            '        End If
            '    End If

            '    If dgvApprovalData.Items.Count >= 1 Then
            '        Dim objExpMasterOld As New clsExpense_Master
            '        Dim iEncashment As DataRow() = Nothing

            '        If CInt(dgvApprovalData.Items(0).Cells(10).Text) > 0 Then
            '            iEncashment = mdtLeaveExpenseTran.Select("crtranunkid = '" & CInt(dgvApprovalData.Items(0).Cells(10).Text) & "' AND AUD <> 'D'")
            '        ElseIf dgvApprovalData.Items(0).Cells(12).Text.ToString <> "" Then
            '            iEncashment = mdtLeaveExpenseTran.Select("GUID = '" & dgvApprovalData.Items(0).Cells(12).Text.ToString & "' AND AUD <> 'D'")
            '        End If
            '        objExpMasterOld._Expenseunkid = CInt(iEncashment(0).Item("expenseunkid").ToString())

            '        If objExpMaster._IsLeaveEncashment <> objExpMasterOld._IsLeaveEncashment Then
            '            'Language.setLanguage(mstrModuleNameClaim)
            '            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form."), Me)
            '            Exit Sub
            '        End If
            '        objExpMasterOld = Nothing
            '    End If
            '    objExpMaster = Nothing

            '    iRow(0).Item("crtranunkid") = mintEditClaimTranId
            '    iRow(0).Item("crmasterunkid") = mintClaimMstUnkId
            '    iRow(0).Item("expenseunkid") = CInt(cboApprovalExpense.SelectedValue)
            '    iRow(0).Item("secrouteunkid") = CInt(cboApprovalSectorRoute.SelectedValue)
            '    iRow(0).Item("costingunkid") = IIf(txtApprovalCostingTag.Value Is Nothing, 0, txtApprovalCostingTag.Value)
            '    iRow(0).Item("unitprice") = txtApprovalUnitPrice.Text
            '    iRow(0).Item("quantity") = txtApprovalQty.Text
            '    iRow(0).Item("amount") = CDec(txtApprovalUnitPrice.Text) * CDec(txtApprovalQty.Text)
            '    iRow(0).Item("expense_remark") = txtApprovalExpRemark.Text
            '    iRow(0).Item("isvoid") = False
            '    iRow(0).Item("voiduserunkid") = -1
            '    iRow(0).Item("voiddatetime") = DBNull.Value
            '    iRow(0).Item("voidreason") = ""
            '    iRow(0).Item("loginemployeeunkid") = -1
            '    If iRow(0).Item("AUD").ToString.Trim = "" Then
            '        iRow(0).Item("AUD") = "U"
            '    End If
            '    iRow(0).Item("GUID") = Guid.NewGuid.ToString
            '    iRow(0).Item("expense") = cboApprovalExpense.SelectedItem.Text
            '    iRow(0).Item("uom") = txtApprovalUoMType.Text
            '    iRow(0).Item("voidloginemployeeunkid") = -1
            '    iRow(0).Item("sector") = IIf(CInt(cboApprovalSectorRoute.SelectedValue) <= 0, "", cboApprovalSectorRoute.SelectedItem.Text)
            '    mdtLeaveExpenseTran.AcceptChanges()

            '    Call FillApprovalExpense() : Call ClearControls()

            '    btnApprovalEdit.Visible = False : btnApprovalAdd.Visible = True
            '    cboApprovalExpense.Enabled = True
            '    iRow = Nothing
            'End If

            'Pinkal (22-Oct-2018) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnApprovalSaveAddEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovalSaveAddEdit.Click
        Try
            If IsApproval_Valid() = False Then Exit Sub

            Dim objExpense As New clsExpense_Master
            Dim xRow() As DataRow = Nothing

            For Each dRow As DataRow In mdtLeaveExpenseTran.Select("AUD <> 'D'")
                objExpense._Expenseunkid = CInt(dRow.Item("expenseunkid"))
                If objExpense._IsAttachDocMandetory = True Then

                    If CInt(dRow.Item("crtranunkid")) > 0 Then
                        xRow = mdtChildClaimAttachment.Select("transactionunkid = '" & CInt(dRow.Item("crtranunkid")) & "' AND AUD <> 'D' ")
                    ElseIf CStr(dRow.Item("GUID")).Trim.Length > 0 Then
                        xRow = mdtChildClaimAttachment.Select("GUID = '" & CStr(dRow.Item("GUID")) & "' AND AUD <> 'D' ")
                    End If
                    If xRow.Count <= 0 Then
                        'Sohail (23 Mar 2019) -- Start
                        'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                        'DisplayMessage.DisplayError(ex, Me)
                        DisplayMessage.DisplayMessage(dRow.Item("expense").ToString & " " & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 22, "has mandatory document attachment. please attach document."), Me)
                        'Sohail (23 Mar 2019) -- End
                        Exit Sub
                    End If
                End If
            Next

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExpense = Nothing
            'Pinkal (11-Sep-2020) -- End


            objClaimPendingMst = New clsclaim_request_master

            objClaimPendingMst._Transactiondate = dtpApprovalClaimDate.GetDate.Date
            objClaimPendingMst._Employeeunkid = CInt(cboApprovalExpEmployee.SelectedValue)
            objClaimPendingMst._Referenceunkid = CInt(cboApprovalReference.SelectedValue)
            objClaimPendingMst._Expensetypeid = enExpenseType.EXP_LEAVE
            objClaimPendingMst._IsPaymentApprovalwithLeaveApproval = CBool(Session("PaymentApprovalwithLeaveApproval"))
            objClaimPendingMst._LeaveApproverForLeaveType = CBool(Session("LeaveApproverForLeaveType"))
            objClaimPendingMst._IsBalanceDeduct = False

            If CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                objClaimPendingMst._FromModuleId = enExpFromModuleID.FROM_LEAVE
            Else
                objClaimPendingMst._FromModuleId = enExpFromModuleID.FROM_EXPENSE
            End If

            objClaimPendingMst._Statusunkid = 2 'DEFAULT PENDING
            objClaimPendingMst._Modulerefunkid = enModuleReference.Leave
            objClaimPendingMst._Referenceunkid = CInt(IIf(cboApprovalReference.SelectedValue = "", 0, cboApprovalReference.SelectedValue))
            objClaimPendingMst._LeaveBalanceSetting = CInt(Session("LeaveBalanceSetting"))
            objClaimPendingMst._LeaveTypeId = CInt(cboApprovalExpLeaveType.SelectedValue)
            objClaimPendingMst._LeaveApproverForLeaveType = CBool(Session("LeaveApproverForLeaveType"))
            objClaimPendingMst._YearId = CInt(Session("Fin_Year"))
            objClaimPendingMst._Cancel_Datetime = Nothing
            objClaimPendingMst._Cancel_Remark = ""
            objClaimPendingMst._Canceluserunkid = -1
            objClaimPendingMst._Iscancel = False
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                objClaimPendingMst._Loginemployeeunkid = -1
                objClaimPendingMst._Userunkid = CInt(Session("UserId"))
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                objClaimPendingMst._Loginemployeeunkid = CInt(Session("Employeeunkid"))
                objClaimPendingMst._Userunkid = -1
            End If

            objClaimPendingMst._Voiddatetime = Nothing
            objClaimPendingMst._Voiduserunkid = -1
            'Pinkal (20-May-2019) -- Start
            'Enhancement [0003788] - Comment field to be Mandatory under expenses. The claim remark.
            'objClaimPendingMst._Claim_Remark = txtClaimRemark.Text
            objClaimPendingMst._Claim_Remark = txtApprovalClaimRemark.Text.Trim()
            'Pinkal (20-May-2019) -- End


            mdtFinalClaimTransaction = mdtLeaveExpenseTran
            mdtFinalClaimAttchment = mdtChildClaimAttachment.Copy

            'Dim objapprover As New clsleaveapprover_master
            'Dim dtApprover As DataTable = objapprover.GetEmployeeApprover(CStr(Session("Database_Name")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")) _
            '                                                                 , eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, CBool(Session("IsIncludeInactiveEmp")) _
            '                                                                 , -1, CInt(cboApprovalExpEmployee.SelectedValue), -1, CInt(IIf(CBool(Session("LeaveApproverForLeaveType")), CInt(cboApprovalExpLeaveType.SelectedValue), -1)) _
            '                                                                 , CBool(Session("LeaveApproverForLeaveType")).ToString(), Nothing)

            'objapprover = Nothing


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim objPendingLeave As New clspendingleave_Tran
            'Pinkal (11-Sep-2020) -- End
            Dim dtApprover As DataTable = objPendingLeave.GetEmployeeApproverListWithPriority(CInt(cboApprovalExpEmployee.SelectedValue), CInt(cboApprovalReference.SelectedValue), Nothing).Tables(0)
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'objClaimPendingMst._dtLeaveApprover = New DataView(dtApprover, "priority >= " & mintPriority, "priority", DataViewRowState.CurrentRows).ToTable()
            objClaimPendingMst._dtLeaveApprover = New DataView(dtApprover, "priority >= " & mintPriority, "priority", DataViewRowState.CurrentRows).ToTable().Copy()

            If dtApprover IsNot Nothing Then dtApprover.Clear()
            dtApprover = Nothing
            objPendingLeave = Nothing
            'Pinkal (11-Sep-2020) -- End


            ClearControls()
            mblnApprovalClaimRequest = False
            popupExpense.Hide()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnApprovalSaveAddEdit_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnApprovalCloseAddEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprovalCloseAddEdit.Click
        Try
            cboApprovalExpense.Enabled = True
            cboApprovalSectorRoute.Enabled = True
            txtApprovalUoMType.Text = ""
            txtApprovalBalance.Text = ""

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
            'txtApprovalQty.Text = "0"
            txtApprovalQty.Text = "1"
            'Pinkal (04-Feb-2020) -- End


            'Pinkal (07-Feb-2020) -- Start
            'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
            'txtApprovalUnitPrice.Text = "0.00"
            txtApprovalUnitPrice.Text = "1.00"
            'Pinkal (07-Feb-2020) -- End

            txtApprovalClaimRemark.Text = ""
            txtApprovalExpRemark.Text = ""
            mblnApprovalClaimRequest = False
            popupExpense.Hide()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnApprovalCloseAddEdit_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSaveAttachment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            If Session("Imagepath") Is Nothing Then Exit Sub
            If Session("Imagepath").ToString.Trim <> "" Then
                Dim f As New System.IO.FileInfo(Session("Imagepath").ToString)

                'Pinkal (27-Nov-2020) -- Start
                'Enhancement Outlook  -  Working on  Don't Allow Users to attach .Exe file types in email.
                Dim mstrExtension As String = Path.GetExtension(f.FullName)
                If mstrExtension = ".exe" Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"frmScanOrAttachmentInfo", 14, "You can not attach .exe(Executable File) for the security reason."), Me)
                    Exit Sub
                End If
                'Pinkal (27-Nov-2020) -- End

                mdtClaimAttchment = CType(Session("mdtClaimAttchment"), DataTable)
                AddDocumentAttachmentForClaim(f, f.FullName)
                Call FillClaimAttachment()
            End If
            Session.Remove("Imagepath")
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonYes_Click
        Try
            If mintDeleteIndex >= 0 Then
                mdtClaimAttchment.Rows(mintDeleteIndex)("AUD") = "D"
                mdtClaimAttchment.AcceptChanges()
                mintDeleteIndex = 0
                Call FillClaimAttachment()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popup_YesNo_buttonNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_YesNo.buttonNo_Click
        Try
            If mintDeleteIndex > 0 Then
                mintDeleteIndex = 0
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnScanClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanClose.Click
        Try
            blnShowAttchmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnScanSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnScanSave.Click
        Try
            mdtClaimAttchment.Select("").ToList().ForEach(Function(x) AddRowForClaimAttachment(x, mstrModuleName))
            blnShowAttchmentPopup = False
            popup_ScanAttchment.Hide()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (22-Oct-2018) -- Start
    'Enhancement - Implementing Claim & Request changes For NMB .
    Protected Sub popup_ApprovalUnitPriceYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ApprovalUnitPriceYesNo.buttonYes_Click
        Try
            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If txtApprovalExpRemark.Text.Trim.Length <= 0 Then
                popup_ApprovalExpRemarkYesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 45, "You have not set your expense remark.") & Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 26, "Do you wish to continue?")
                popup_ApprovalExpRemarkYesNo.Show()
            Else
                If mblnIsApprovalExpenseEditClick Then
                    EditForApprovalExpense()
                Else
                    AddForApprovalExpense()
                End If
            End If
            'Pinkal (04-Feb-2019) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (22-Oct-2018) -- End

    'Pinkal (04-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Protected Sub popup_ApprovalExpRemarkYesNo_buttonYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup_ApprovalExpRemarkYesNo.buttonYes_Click
        Try
            If mblnIsApprovalExpenseEditClick Then
                EditForApprovalExpense()
            Else
                AddForApprovalExpense()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Pinkal (04-Feb-2019) -- End

#End Region

    'Pinkal (10-Jan-2017) -- End


#End Region

#Region "Gridview Event(S)"

#Region "Days Fraction"

    Protected Sub dgFraction_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles dgFraction.PageIndexChanging
        Try
            dgFraction.PageIndex = e.NewPageIndex
            GetFractionList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupFraction.Show()
        End Try
    End Sub

    Protected Sub dgFraction_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles dgFraction.RowDataBound
        Try

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            If e.Row.RowIndex >= 0 Then


                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'If CStr(Session("DateFormat")) <> Nothing Then
                '    e.Row.Cells(1).Text = CDate(e.Row.Cells(1).Text).Date.ToString(Session("DateFormat").ToString)
                'Else
                '    e.Row.Cells(1).Text = CDate(e.Row.Cells(1).Text).Date.ToShortDateString
                'End If
                e.Row.Cells(1).Text = CDate(e.Row.Cells(1).Text).Date.ToShortDateString
                'Pinkal (16-Apr-2016) -- End


                If e.Row.Cells(2).Text.Trim <> "&nbsp;" AndAlso e.Row.Cells(2).Text.Trim <> "" Then
                    e.Row.Cells(2).Text = CDec(e.Row.Cells(2).Text).ToString("#0.00")
                End If

                If e.Row.Cells(2).Controls.Count > 0 Then
                    CType(e.Row.Cells(2).Controls(0), TextBox).CssClass = "RightTextAlign"
                    CType(e.Row.Cells(2).Controls(0), TextBox).Text = CDec(CType(e.Row.Cells(2).Controls(0), TextBox).Text).ToString("#0.00")
                    CType(e.Row.Cells(2).Controls(0), TextBox).Focus()
                End If
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgFraction_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles dgFraction.RowEditing
        Try
            dgFraction.EditIndex = e.NewEditIndex
            GetFractionList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupFraction.Show()
        End Try
    End Sub

    Protected Sub dgFraction_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles dgFraction.RowUpdating
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim dtTab As DataTable = Nothing
        'Pinkal (11-Sep-2020) -- End
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'Dim dtTab As DataTable = New DataView(mdtFraction, "AUD<>'D'", "", DataViewRowState.CurrentRows).ToTable
            dtTab = New DataView(mdtFraction, "AUD<>'D'", "", DataViewRowState.CurrentRows).ToTable().Copy()
            'Pinkal (11-Sep-2020) -- End


            For i = 0 To dgFraction.Columns.Count - 1

                If TypeOf (dgFraction.Columns(i)) Is CommandField Or TypeOf (dgFraction.Columns(i)) Is TemplateField Then Continue For

                If DirectCast(DirectCast(dgFraction.Columns(i), System.Web.UI.WebControls.DataControlField), System.Web.UI.WebControls.BoundField).ReadOnly = False Then

                    Dim cell As DataControlFieldCell = CType(dgFraction.Rows(e.RowIndex).Cells(i), DataControlFieldCell)
                    cell.HorizontalAlign = HorizontalAlign.Right
                    dgFraction.Columns(i).ExtractValuesFromCell(e.NewValues, cell, DataControlRowState.Edit, True)

                    For Each key As String In e.NewValues.Keys
                        Dim str As String = e.NewValues(key).ToString()

                        If dtTab.Columns(key).DataType Is System.Type.GetType("System.Decimal") Then
                            Dim decTemp As Decimal = 1
                            If Decimal.TryParse(str, decTemp) = True Then

                                If decTemp < 0.05 Or decTemp > 1 Then
                                    'Sohail (23 Mar 2019) -- Start
                                    'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                    'DisplayMessage.DisplayError(ex, Me)
                                    DisplayMessage.DisplayMessage("Please enter day fraction between 0.05 to 1.", Me)
                                    'Sohail (23 Mar 2019) -- End
                                    e.Cancel = True
                                    cell.Focus()
                                    Exit Sub
                                End If
                                dtTab.Rows(e.RowIndex).Item(key) = decTemp.ToString("#0.00")

                                'If Me.ViewState("ApproverForFraction") IsNot Nothing AndAlso CInt(Me.ViewState("ApproverForFraction")) = CInt(cboApprover.SelectedValue) AndAlso dtTab.Rows(e.RowIndex)("AUD").ToString() <> "D" Then
                                If mintFreactionApproverId = CInt(cboApprover.SelectedValue) AndAlso dtTab.Rows(e.RowIndex)("AUD").ToString() <> "D" Then
                                    dtTab.Rows(e.RowIndex)("AUD") = "U"
                                End If

                            Else
                                'Sohail (23 Mar 2019) -- Start
                                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                                'DisplayMessage.DisplayError(ex, Me)
                                DisplayMessage.DisplayMessage("Please enter proper day fraction.", Me)
                                'Sohail (23 Mar 2019) -- End
                                e.Cancel = True
                                cell.Focus()
                                Exit Sub
                            End If

                        Else
                            dtTab.Rows(e.RowIndex).Item(key) = str
                        End If

                    Next
                End If
            Next

            If e.NewValues.Count > 0 Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'mdtFraction = dtTab
                mdtFraction = dtTab.Copy()
                'Pinkal (11-Sep-2020) -- End

                dgFraction.EditIndex = -1
                GetFractionList()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTab IsNot Nothing Then dtTab.Clear()
            dtTab = Nothing
            'Pinkal (11-Sep-2020) -- End

            popupFraction.Show()
        End Try
    End Sub

    Protected Sub dgFraction_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs) Handles dgFraction.RowCancelingEdit
        Try
            dgFraction.EditIndex = -1
            GetFractionList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popupFraction.Show()
        End Try
    End Sub

#End Region

#Region "Expense"

    Protected Sub dgvData_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.DeleteCommand
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim iRow As DataRow() = Nothing
        Dim mdtTran As DataTable = Nothing
        'Pinkal (11-Sep-2020) -- End
        Try

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtExpense IsNot Nothing Then
                mdtTran = mdtExpense.Copy()
            End If
            'Pinkal (11-Sep-2020) -- End


            'Pinkal (10-Jan-2017) -- Start
            'Enhancement - Working on TRA C&R Module Changes with Leave Module.
            txtClaimDelReason.Text = ""
            'Pinkal (10-Jan-2017) -- End

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'If CInt(e.Item.Cells(10).Text) <> 0 Then
            If CInt(e.Item.Cells(10).Text) > 0 Then
                'Pinkal (04-Feb-2019) -- End
                iRow = mdtTran.Select("crtranunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    mintEditClaimTranId = CInt(iRow(0).Item("crtranunkid"))
                    popupClaimDelete.Show()
                End If
            ElseIf CStr(e.Item.Cells(12).Text) <> "" Then
                iRow = mdtTran.Select("GUID = '" & CStr(e.Item.Cells(12).Text) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    iRow(0).Item("AUD") = "D"
                End If
            End If
            iRow(0).AcceptChanges()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'mdtExpense = mdtTran
            mdtExpense = mdtTran.Copy()
            'Pinkal (11-Sep-2020) -- End

            Fill_Expense()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            iRow = Nothing
            If mdtTran IsNot Nothing Then mdtTran.Clear()
            mdtTran = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub dgvData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvData.ItemCommand
        Try
            If e.CommandName = "Edit" Then

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'Dim mdtTran As DataTable = mdtExpense
                Dim mdtTran As DataTable = mdtExpense.Copy()
                'Pinkal (11-Sep-2020) -- End

                Dim iRow As DataRow() = Nothing


                'Pinkal (10-Jan-2017) -- Start
                'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                'If CInt(e.Item.Cells(10).Text) <> 0 Then
                '    iRow = mdtTran.Select("crtranunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D'")
                'ElseIf e.Item.Cells(12).Text.ToString <> "" Then
                '    iRow = mdtTran.Select("GUID = '" & e.Item.Cells(12).Text.ToString & "' AND AUD <> 'D'")
                'End If

                If CInt(e.Item.Cells(13).Text) <> 0 Then
                    iRow = mdtTran.Select("crapprovaltranunkid = '" & CInt(e.Item.Cells(13).Text) & "' AND AUD <> 'D'")
                ElseIf e.Item.Cells(12).Text.ToString <> "" Then
                    iRow = mdtTran.Select("GUID = '" & e.Item.Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If
                'Pinkal (10-Jan-2017) -- End



                If iRow.Length > 0 Then
                    btnClaimAdd.Visible = False : btnClaimEdit.Visible = True
                    cboExpense.SelectedValue = iRow(0).Item("expenseunkid").ToString()
                    Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())

                    cboSectorRoute.SelectedValue = iRow(0).Item("secrouteunkid").ToString()
                    Call dtpDate_TextChanged(dtpDate, New EventArgs())

                    txtCalimQty.Text = CDec(iRow(0).Item("quantity")).ToString()
                    txtUnitPrice.Text = CDec(Format(CDec(iRow(0).Item("unitprice")), Session("fmtCurrency").ToString)).ToString()
                    txtExpRemark.Text = CStr(iRow(0).Item("expense_remark"))
                    cboExpense.Enabled = False


                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    cboCostCenter.SelectedValue = iRow(0).Item("costcenterunkid").ToString()
                    cboCostCenter.Enabled = False
                    'Pinkal (04-Feb-2019) -- End

                    'Pinkal (10-Jan-2017) -- Start
                    'Enhancement - Working on TRA C&R Module Changes with Leave Module.

                    'If CInt(e.Item.Cells(10).Text) > 0 Then
                    '    mintEditClaimTranId = CInt(iRow(0).Item("crtranunkid"))
                    'ElseIf e.Item.Cells(11).Text.ToString <> "" Then
                    '    mstrEditGUID = CStr(iRow(0).Item("GUID"))
                    'End If

                    If CInt(e.Item.Cells(13).Text) > 0 Then
                        mintEditClaimTranId = CInt(iRow(0).Item("crapprovaltranunkid"))
                    ElseIf e.Item.Cells(11).Text.ToString <> "" Then
                        mstrEditGUID = CStr(iRow(0).Item("GUID"))
                    End If

                    'Pinkal (10-Jan-2017) -- End

                    'Pinkal (07-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    mintClaimRequestApprovalExpTranId = CInt(e.Item.Cells(10).Text)
                    mstrClaimRequestApprovalExpGUID = e.Item.Cells(12).Text.ToString()
                    'Pinkal (07-Mar-2019) -- End

                End If


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If mdtTran IsNot Nothing Then mdtTran.Clear()
                mdtTran = Nothing
                'Pinkal (11-Sep-2020) -- End


                'Shani (20-Aug-2016) -- Start
                'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew 
            ElseIf e.CommandName.ToUpper = "PREVIEW" Then

                'Pinkal (10-Jan-2017) -- Start
                'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                popup_ShowAttchment.ClearPreviewDataSource()
                'Pinkal (10-Jan-2017) -- End

                Dim mstrPreviewIds As String = ""
                Dim dtTable As DataTable = Nothing

                'Pinkal (10-Jan-2017) -- Start
                'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                'dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(CInt(cboClaimEmployee.SelectedValue), _
                '                                                                     enScanAttactRefId.CLAIM_REQUEST, _
                '                                                                     enImg_Email_RefId.Claim_Request, _
                '                                                                     CInt(e.Item.Cells(10).Text))
                If CInt(CInt(e.Item.Cells(10).Text)) > 0 Then
                    dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(CInt(cboClaimEmployee.SelectedValue), _
                                                                                         enScanAttactRefId.CLAIM_REQUEST, _
                                                                                         enImg_Email_RefId.Claim_Request, _
                                                                                         CInt(e.Item.Cells(10).Text))
                End If
                'Pinkal (10-Jan-2017) -- End



                If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                    mstrPreviewIds = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
                End If

                popup_ShowAttchment.ScanTranIds = mstrPreviewIds
                'S.SANDEEP |25-JAN-2019| -- START
                'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
                popup_ShowAttchment._ZipFileName = "Claim_Request_" + cboClaimEmployee.SelectedItem.Text.Replace(" ", "_") + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
                'S.SANDEEP |25-JAN-2019| -- END
                popup_ShowAttchment._Webpage = Me
                popup_ShowAttchment.Show()
                'Shani (20-Aug-2016) -- End

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtTable IsNot Nothing Then dtTable.Clear()
                dtTable = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvData_ItemCommand:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvData.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                If e.Item.Cells(7).Text.Trim.Length > 0 Then  'Unit Price
                    e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), Session("fmtCurrency").ToString)
                End If

                If e.Item.Cells(8).Text.Trim.Length > 0 Then  'Amount
                    e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(8).Text), Session("fmtCurrency").ToString)
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvData_ItemDataBound:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.

#Region "Add For Approval Expense"

    Protected Sub dgvApprovalData_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgvApprovalData.ItemDataBound
        Try
            If e.Item.ItemIndex >= 0 Then
                If e.Item.Cells(7).Text.Trim.Length > 0 Then  'Unit Price
                    e.Item.Cells(7).Text = Format(CDec(e.Item.Cells(7).Text), CStr(Session("fmtCurrency")))
                End If
                If e.Item.Cells(8).Text.Trim.Length > 0 Then  'Amount
                    e.Item.Cells(8).Text = Format(CDec(e.Item.Cells(8).Text), CStr(Session("fmtCurrency")))
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvApprovalData_ItemDataBound:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvApprovalData_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvApprovalData.DeleteCommand
        Try
            Dim iRow As DataRow() = Nothing
            txtClaimDelReason.Text = ""
            If CInt(e.Item.Cells(10).Text) > 0 Then
                iRow = mdtLeaveExpenseTran.Select("crtranunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    mintEditClaimTranId = CInt(iRow(0).Item("crtranunkid"))
                    popupClaimDelete.Show()
                End If
            ElseIf CStr(e.Item.Cells(12).Text) <> "" Then
                iRow = mdtLeaveExpenseTran.Select("GUID = '" & CStr(e.Item.Cells(12).Text) & "' AND AUD <> 'D'")
                If iRow.Length > 0 Then
                    iRow(0).Item("AUD") = "D"
                End If
                iRow(0).AcceptChanges()

                Dim dRow() As DataRow = Nothing
                If CStr(e.Item.Cells(12).Text) <> "" Then
                    dRow = mdtChildClaimAttachment.Select("GUID = '" & CStr(e.Item.Cells(12).Text) & "' AND AUD <> 'D'")
                End If
                If dRow IsNot Nothing AndAlso dRow.Count > 0 Then
                    For Each dtRow As DataRow In dRow
                        dtRow.Item("AUD") = "D"
                    Next
                End If
                mdtChildClaimAttachment.AcceptChanges()

                FillApprovalExpense()
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvApprovalData_DeleteCommand:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgvApprovalData_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgvApprovalData.ItemCommand
        Try
            If e.CommandName = "Edit" Then
                Dim iRow As DataRow() = Nothing
                If CInt(e.Item.Cells(10).Text) > 0 Then
                    iRow = mdtLeaveExpenseTran.Select("crtranunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D'")
                ElseIf e.Item.Cells(12).Text.ToString <> "" Then
                    iRow = mdtLeaveExpenseTran.Select("GUID = '" & e.Item.Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If

                If iRow.Length > 0 Then
                    btnApprovalAdd.Visible = False : btnApprovalEdit.Visible = True
                    cboApprovalExpense.SelectedValue = CStr(iRow(0).Item("expenseunkid"))
                    Call cboApprovalExpense_SelectedIndexChanged(cboApprovalExpense, New EventArgs())
                    cboApprovalSectorRoute.SelectedValue = CStr(iRow(0).Item("secrouteunkid"))
                    Call dtpApprovalClaimDate_TextChanged(dtpApprovalClaimDate, New EventArgs())

                    txtApprovalQty.Text = CStr(CDec(iRow(0).Item("quantity")))
                    txtApprovalUnitPrice.Text = Format(CDec(iRow(0).Item("unitprice")), CStr(Session("fmtCurrency")))    'CStr(CDec(Format(CDec(iRow(0).Item("unitprice")), CStr(Session("fmtCurrency")))))
                    txtApprovalExpRemark.Text = CStr(iRow(0).Item("expense_remark"))
                    cboApprovalExpense.Enabled = False

                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    cboApprovalCostCenter.SelectedValue = iRow(0).Item("costcenterunkid").ToString()
                    cboApprovalCostCenter.Enabled = False
                    'Pinkal (04-Feb-2019) -- End


                    If CInt(e.Item.Cells(10).Text) > 0 Then
                        mintEditClaimTranId = CInt(iRow(0).Item("crtranunkid"))
                    ElseIf e.Item.Cells(12).Text.ToString <> "" Then
                        mstrEditGUID = CStr(iRow(0).Item("GUID"))
                    End If

                    'Pinkal (07-Mar-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    mintClaimRequestApprovalExpTranId = CInt(e.Item.Cells(10).Text)
                    mstrClaimRequestApprovalExpGUID = e.Item.Cells(12).Text.ToString()
                    'Pinkal (07-Mar-2019) -- End

                End If

            ElseIf e.CommandName.ToUpper = "ATTACHMENT" Then

                mdtClaimAttchment = mdtChildClaimAttachment.Clone
                Dim xRow() As DataRow = Nothing
                If CInt(e.Item.Cells(10).Text) > 0 Then
                    xRow = mdtChildClaimAttachment.Select("transactionunkid = '" & CInt(e.Item.Cells(10).Text) & "' AND AUD <> 'D'")
                ElseIf e.Item.Cells(12).Text.ToString <> "" Then
                    xRow = mdtChildClaimAttachment.Select("GUID = '" & e.Item.Cells(12).Text.ToString & "' AND AUD <> 'D'")
                End If

                If xRow.Count > 0 Then
                    mdtClaimAttchment = xRow.CopyToDataTable
                End If

                'Pinkal (18-Mar-2021) -- Start
                'Bug Claim Request/Retirement -   Working Claim Request/Retirement Bug.
                'mintDeleteIndex = e.Item.ItemIndex
                mintExpenseIndex = e.Item.ItemIndex
                'Pinkal (18-Mar-2021) -- End

                Call FillClaimAttachment()
                blnShowAttchmentPopup = True
                popup_ScanAttchment.Show()
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dgvApprovalData_ItemCommand:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub dgv_Attchment_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgv_Attchment.ItemCommand
        Try
            If e.Item.ItemIndex >= 0 Then
                Dim xrow() As DataRow
                'S.SANDEEP |16-MAY-2019| -- START
                'ISSUE/ENHANCEMENT : {Attachment Preview} Leave UAT
                'If CInt(e.Item.Cells(3).Text) > 0 Then
                '    xrow = mdtClaimAttchment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(3).Text) & "")
                'Else
                '    xrow = mdtClaimAttchment.Select("GUID = '" & e.Item.Cells(2).Text.Trim & "'")
                'End If
                'If e.CommandName = "Delete" Then
                '    If xrow.Length > 0 Then
                '        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 39, "Are you sure you want to delete this attachment?")
                '        mintDeleteIndex = mdtClaimAttchment.Rows.IndexOf(xrow(0))
                '        popup_YesNo.Show()
                '    End If
                'End If

                If CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) > 0 Then
                    xrow = mdtClaimAttchment.Select("scanattachtranunkid = " & CInt(e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhScanUnkId", False, True)).Text) & "")
                Else
                    xrow = mdtClaimAttchment.Select("GUID = '" & e.Item.Cells(GetColumnIndex.getColumnId_Datagrid(dgv_Attchment, "objcolhGUID", False, True)).Text.Trim & "'")
                End If
                If e.CommandName = "Delete" Then
                    If xrow.Length > 0 Then
                        popup_YesNo.Message = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 39, "Are you sure you want to delete this attachment?")
                        mintDeleteIndex = mdtClaimAttchment.Rows.IndexOf(xrow(0))
                        popup_YesNo.Show()
                    End If
                ElseIf e.CommandName = "Download" Then
                    If xrow IsNot Nothing AndAlso xrow.Length > 0 Then
                        Dim xPath As String = ""

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            xPath = xrow(0).Item("filepath").ToString
                            xPath = xPath.Replace(Session("ArutiSelfServiceURL").ToString, "")
                            If Strings.Left(xPath, 1) <> "/" Then
                                xPath = "~/" & xPath
                            Else
                                xPath = "~" & xPath
                            End If
                            xPath = Server.MapPath(xPath)
                        ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                            xPath = xrow(0).Item("orgfilepath").ToString
                        End If

                        If xPath.Trim <> "" Then
                            Dim fileInfo As New IO.FileInfo(xPath)
                            If fileInfo.Exists = False Then
                                DisplayMessage.DisplayMessage("File does not Exist...", Me)
                                Exit Sub
                            End If
                            fileInfo = Nothing
                            Dim strFile As String = xPath
                            Response.ContentType = "image/jpg/pdf"
                            Response.AddHeader("Content-Disposition", "attachment;filename=""" & xrow(0)("filename").ToString() & """")
                            Response.Clear()
                            Response.TransmitFile(strFile)
                            HttpContext.Current.ApplicationInstance.CompleteRequest()
                        End If
                    End If
                End If
                'S.SANDEEP |16-MAY-2019| -- END
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Pinkal (10-Jan-2017) -- End


#End Region

#Region "Link Event(S)'"

    Protected Sub lnkShowLeaveForm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkShowLeaveForm.Click
        Try
            Dim objLeaveForm As New clsleaveform
            objLeaveForm._Formunkid = mintLeaveFormUnkId
            If objLeaveForm._Formunkid > 0 Then
                txtLeaveFormNo.Text = objLeaveForm._Formno


                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                'If CStr(Session("DateFormat")) <> Nothing Then
                '    txtLeaveFormApplydate.Text = objLeaveForm._Applydate.Date.ToString(Session("DateFormat").ToString())
                '    txtLeaveFormStartdate.Text = objLeaveForm._Startdate.Date.ToString(Session("DateFormat").ToString())
                '    txtLeaveFormReturndate.Text = objLeaveForm._Returndate.Date.ToString(Session("DateFormat").ToString())
                'Else
                '    txtLeaveFormApplydate.Text = objLeaveForm._Applydate.ToShortDateString()
                '    txtLeaveFormStartdate.Text = objLeaveForm._Startdate.ToShortDateString()
                '    txtLeaveFormReturndate.Text = objLeaveForm._Returndate.ToShortDateString()
                'End If
                SetDateFormat()
                txtLeaveFormApplydate.Text = objLeaveForm._Applydate.ToShortDateString()
                txtLeaveFormStartdate.Text = objLeaveForm._Startdate.ToShortDateString()
                txtLeaveFormReturndate.Text = objLeaveForm._Returndate.ToShortDateString()
                'Pinkal (16-Apr-2016) -- End


                txtLeaveFormAddress.Text = objLeaveForm._Addressonleave
                Dim objFraction As New clsleaveday_fraction
                txtLeaveFormDayApply.Text = objFraction.GetList("Fraction", mintLeaveFormUnkId, True).Tables(0).Compute("SUM(dayfraction)", "").ToString()

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objFraction = Nothing
                'Pinkal (11-Sep-2020) -- End

                txtLeaveFormRemark.Text = objLeaveForm._Remark

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = objLeaveForm._Employeeunkid
                txtLeaveFormEmpCode.Text = objEmployee._Employeecode
                txtLeaveFormEmployee.Text = objEmployee._Firstname & " " & objEmployee._Surname

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objEmployee = Nothing
                'Pinkal (11-Sep-2020) -- End


                Dim objLeaveType As New clsleavetype_master
                objLeaveType._Leavetypeunkid = objLeaveForm._Leavetypeunkid
                txtLeaveFormLeaveType.Text = objLeaveType._Leavename

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objLeaveForm = Nothing
                objLeaveType = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If

            popupLeaveForm.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkLeaveDayCount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLeaveDayCount.Click
        Try
            If dtpStartdate.GetDate.Date > dtpEnddate.GetDate.Date Then
                DisplayMessage.DisplayMessage("Please Select Proper Start Date.", Me)
                dtpStartdate.Focus()
                Exit Sub
            End If
            GetApproverDayFraction()
            If mdtFraction Is Nothing Then
                Dim objLeaveFraction As New clsleaveday_fraction
                Dim dtFraction As DataTable = objLeaveFraction.GetList("List", mintLeaveFormUnkId, True, -1, mintFreactionApproverId).Tables(0)
                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                'mdtFraction = dtFraction
                mdtFraction = dtFraction.Copy()

                If dtFraction IsNot Nothing Then dtFraction.Clear()
                dtFraction = Nothing
                objLeaveFraction = Nothing
                'Pinkal (11-Sep-2020) -- End

            End If

            dgFraction.EditIndex = -1
            dgFraction.PageIndex = 0
            Call GetFractionList()
            popupFraction.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkShowLeaveExpense_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkShowLeaveExpense.Click
        Try
            txtUnitPrice.Attributes.Add("onfocusin", "select();")


            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
            If IsValidExpense_Eligility() = False Then

                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 21, "Sorry,You cannot view/edit expense for this leave type.Reason : Eligibility to view/edit for this leave type does not match with eligibility days set on selected leave type. Eligible days set for the selected leave type is") & " (" & intEligibleExpenseDays & ").", Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 21, "Sorry,You cannot view/edit expense for this leave type.Reason : Eligibility to view/edit for this leave type does not match with eligibility days set on selected leave type."), Me)
                'Pinkal (25-May-2019) -- End
                Exit Sub
            End If
            'Pinkal (29-Sep-2017) -- End

            Call SetVisibility()

            Dim objLvForm As New clsleaveform
            objLvForm._Formunkid = mintLeaveFormUnkId
            mintLeaveTypeId = objLvForm._Leavetypeunkid

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLvForm = Nothing
            'Pinkal (11-Sep-2020) -- End

            Call FillClaimCombo()
            Call GetValue()

            Dim objClaimRequestMst As New clsclaim_request_master
            objClaimRequestMst._Crmasterunkid = mintClaimMstUnkId


            'Pinkal (10-Jan-2017) -- Start
            'Enhancement - Working on TRA C&R Module Changes with Leave Module.
            'If mdtFinalClaimTransaction Is Nothing Then
            If mdtFinalClaimTransaction Is Nothing OrElse mintClaimMstUnkId <= 0 Then
                'Pinkal (10-Jan-2017) -- End

                If CBool(Session("PaymentApprovalwithLeaveApproval")) Then
                    Dim objClaimProcess As New clsclaim_process_Tran
                    Dim dsClaimProcess As DataSet = objClaimProcess.GetList("Process", True, 1, "cmclaim_process_tran.crmasterunkid = " & mintClaimMstUnkId)
                    If dsClaimProcess IsNot Nothing AndAlso dsClaimProcess.Tables(0).Rows.Count > 0 Then
                        DisplayMessage.DisplayMessage("You can't Edit/Delete Claim Expense.Reason:This Claim Expense is already posted to Payroll.", Me)
                        Exit Sub
                    End If
                    dsClaimProcess.Clear()
                    dsClaimProcess = Nothing
                    objClaimProcess = Nothing

                    If objClaimRequestMst._Statusunkid = 1 OrElse objClaimRequestMst._Statusunkid = 2 Then
                        Dim objleaveapprover As New clsleaveapprover_master
                        Dim objApproveLevel As New clsapproverlevel_master
                        Dim objExpenseApproval As New clsclaim_request_approval_tran
                        Dim dsApprovalList As DataSet = Nothing

                        objleaveapprover._Approverunkid = mintApproverTranUnkId
                        objApproveLevel._Levelunkid = objleaveapprover._Levelunkid


                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objleaveapprover = Nothing
                        'Pinkal (11-Sep-2020) -- End


                        'Pinkal (10-Jan-2017) -- Start
                        'Enhancement - Working on TRA C&R Module Changes with Leave Module.

                        'dsApprovalList = objExpenseApproval.GetApproverExpesneList("Approval", False, _
                        '                                                           CBool(Session("PaymentApprovalwithLeaveApproval")), _
                        '                                                           Session("Database_Name").ToString(), _
                        '                                                           CInt(Session("UserId")), _
                        '                                                           Session("EmployeeAsOnDate").ToString(), _
                        '                                                           enExpenseType.EXP_LEAVE, False, True, -1, "", _
                        '                                                           mintClaimMstUnkId)

                        dsApprovalList = objExpenseApproval.GetApproverExpesneList("Approval", False, _
                                                                                   CBool(Session("PaymentApprovalwithLeaveApproval")), _
                                                                                   Session("Database_Name").ToString(), _
                                                                                   CInt(Session("UserId")), _
                                                                                   Session("EmployeeAsOnDate").ToString(), _
                                                                                   enExpenseType.EXP_LEAVE, False, True, mintApproverTranUnkId, "", _
                                                                                   mintClaimMstUnkId)

                        'Pinkal (10-Jan-2017) -- End



                        Dim drRow() As DataRow = dsApprovalList.Tables(0).Select("statusunkid = 1 AND crpriority = " & objApproveLevel._Priority)
                        If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                            DisplayMessage.DisplayMessage("You can't Edit/Delete Claim Expense.Reason:This Claim Expense is already approved.", Me)
                            Exit Sub
                        End If

                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        'mdtExpense = New DataView(dsApprovalList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                        mdtExpense = New DataView(dsApprovalList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable().Copy()

                        If dsApprovalList IsNot Nothing Then dsApprovalList.Clear()
                        dsApprovalList = Nothing
                        objApproveLevel = Nothing
                        objExpenseApproval = Nothing
                        'Pinkal (11-Sep-2020) -- End

                    Else

                        'Pinkal (10-Jan-2017) -- Start
                        'Enhancement - Working on TRA C&R Module Changes with Leave Module.
                        'mdtExpense = Nothing
                        'DisplayMessage.DisplayMessage("There is no Leave expense data for this leave form.", Me)
                        'Exit Sub
                        If CInt(Session("ClaimRequestVocNoType")) = 1 Then
                            txtApprovalClaimNo.Enabled = False
                        End If
                        txtUnitPrice.Enabled = False
                        mblnApprovalClaimRequest = True
                        dtpApprovalClaimDate.SetDate = dtpApplyDate.GetDate.Date
                        dtpApprovalClaimDate.Enabled = False
                        cboApprovalExpCategory.Focus()
                        FillApprovalClaimCombo()
                        GetApprovalExpenseValue()

                        If mdtFinalClaimTransaction IsNot Nothing Then
                            mdtLeaveExpenseTran = mdtFinalClaimTransaction
                        End If

                        FillApprovalExpense()

                        If mdtFinalClaimAttchment Is Nothing Then
                            Dim objAttchement As New clsScan_Attach_Documents
                            'S.SANDEEP |04-SEP-2021| -- START
                            'ISSUE : TAKING CARE FROM SLOWNESS QUERY
                            'Call objAttchement.GetList(ConfigParameter._Object._Document_Path, "List", "", CInt(cboApprovalExpEmployee.SelectedValue))
                            Call objAttchement.GetList(Session("Document_Path").ToString(), "List", "", CInt(cboApprovalExpEmployee.SelectedValue), False, Nothing _
                                                                 , Nothing, "", CBool(IIf(CInt(cboApprovalExpEmployee.SelectedValue) <= 0, True, False)))
                            'S.SANDEEP |04-SEP-2021| -- END


                            Dim strTranIds As String = "0"
                            mdtFinalClaimAttchment = New DataView(objAttchement._Datatable, "scanattachrefid = " & enScanAttactRefId.CLAIM_REQUEST & " AND transactionunkid IN (" & strTranIds & ")", "", DataViewRowState.CurrentRows).ToTable
                            objAttchement = Nothing
                        End If
                        mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.LEAVEFORMS).Tables(0).Rows(0)("Name").ToString

                        mdtChildClaimAttachment = mdtFinalClaimAttchment.Copy

                        popupExpense.Show()
                        Exit Sub
                        'Pinkal (10-Jan-2017) -- End
                    End If
                Else

                    Dim objClaimApprover As New clsclaim_request_approval_tran
                    Dim mdtData As DataTable = objClaimApprover.GetEmployeeLastApprovedExpenseDetail(mintClaimMstUnkId) 'ClaimRequestMasterId
                    If mdtData Is Nothing OrElse mdtData.Rows.Count <= 0 Then
                        mdtData = Nothing
                        DisplayMessage.DisplayMessage("There is no Leave expense data for this leave form.", Me)
                        Exit Sub
                    End If

                    'Pinkal (04-Feb-2019) -- Start
                    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                    If mdtData IsNot Nothing AndAlso mdtData.Rows.Count > 0 Then
                        Dim objExpApproverTran As New clsclaim_request_approval_tran
                        mdtExpense = objExpApproverTran.GetApproverExpesneList("List", False, CBool(Session("PaymentApprovalwithLeaveApproval")), Session("Database_Name").ToString() _
                                                                      , CInt(Session("UserId")), Session("EmployeeAsOnDate").ToString(), CInt(cboExpCategory.SelectedValue), False _
                                                                      , True, CInt(mdtData.Rows(0)("crapproverunkid")), "", mintClaimMstUnkId).Tables(0)

                        'Pinkal (11-Sep-2020) -- Start
                        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                        objExpApproverTran = Nothing
                        'Pinkal (11-Sep-2020) -- End

                    End If
                    'Pinkal (04-Feb-2019) -- End


                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If mdtData IsNot Nothing Then mdtData.Clear()
                    mdtData = Nothing
                    objClaimApprover = Nothing
                    'Pinkal (11-Sep-2020) -- End

                End If
            End If

            If mdtFinalClaimTransaction IsNot Nothing Then
                mdtExpense = mdtFinalClaimTransaction
            Else
                mdtFinalClaimTransaction = mdtExpense
            End If


            txtUnitPrice.Enabled = False
            Call Fill_Expense()
            cboExpCategory.Focus()
            mblnClaimRequest = True

            btnClaimSave.Visible = CBool(Session("PaymentApprovalwithLeaveApproval"))
            dgvData.Columns(0).Visible = CBool(Session("PaymentApprovalwithLeaveApproval"))
            dgvData.Columns(1).Visible = CBool(Session("PaymentApprovalwithLeaveApproval"))
            btnClaimAdd.Visible = CBool(Session("PaymentApprovalwithLeaveApproval"))
            btnClaimEdit.Visible = False
            popupClaimRequest.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkShowScanDocuementList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkShowScanDocuementList.Click
        Dim mstrPreviewIds As String = ""
        Dim dtTable As New DataTable
        Try

            'Shani (20-Aug-2016) -- Start
            'Enhancement - Add Scan Attchment in Claim Request Module given by Matthew
            'Dim mstrPreviewIds As String = ""
            'Dim dtTable As New DataTable

            'Dim objScanAttach As New clsScan_Attach_Documents
            'dtTable = objScanAttach.GetAttachmentTranunkIds(mintEmpUnkId, _
            '                                                enScanAttactRefId.LEAVEFORMS, _
            '                                                enImg_Email_RefId.Leave_Module, _
            '                                                mintLeaveFormUnkId)

            'If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
            '    For Each dRow As DataRow In dtTable.Rows
            '        mstrPreviewIds &= CInt(dRow("scanattachtranunkid")) & ","
            '    Next
            'End If
            'If mstrPreviewIds.Trim.Length > 0 Then
            '    mstrPreviewIds = mstrPreviewIds.Trim.Substring(0, mstrPreviewIds.Length - 1)
            'End If
            'Session("pendingleavetranunkid") = mintPenddingLvTranUnkId
            'Session("formunkid") = mintLeaveFormUnkId
            'Session("employeeunkid") = mintEmpUnkId
            'Session("Approvertranunkid") = mintApproverTranUnkId
            'Session("priority") = mintPriority
            'Session("approverEmpUnkid") = mintApprverId
            'Session("finalexpense") = mdtFinalClaimTransaction
            'Session("dayfreaction") = mdtFraction
            'Session("startdate") = dtpStartdate.GetDate
            'Session("enddate") = dtpEnddate.GetDate

            'Response.Redirect(Session("rootpath").ToString() & "Others_Forms/wPg_PreviewDocuments.aspx?" _
            '                  & HttpUtility.UrlEncode(clsCrypto.Encrypt(mstrPreviewIds.ToString)), False)
            'Shani (20-Aug-2016) -- End


            dtTable = (New clsScan_Attach_Documents).GetAttachmentTranunkIds(mintEmpUnkId, _
                                                            enScanAttactRefId.LEAVEFORMS, _
                                                            enImg_Email_RefId.Leave_Module, _
                                                            mintLeaveFormUnkId)
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                mstrPreviewIds = String.Join(",", dtTable.AsEnumerable().Select(Function(x) x.Field(Of Integer)("scanattachtranunkid").ToString).ToArray)
            End If

            popup_ShowAttchment.ScanTranIds = mstrPreviewIds
            'S.SANDEEP |25-JAN-2019| -- START
            'ISSUE/ENHANCEMENT : {Ref#2540|ARUTI-}
            popup_ShowAttchment._ZipFileName = "LeaveForm_" + txtEmployee.Text.Replace(" ", "_") + DateTime.Now.ToString("MMyyyyddsshhmm").ToString() + ".zip"
            'S.SANDEEP |25-JAN-2019| -- END
            popup_ShowAttchment._Webpage = Me
            popup_ShowAttchment.Show()


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkShowScanDocuementList_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub lnkClaimDepedents_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkClaimDepedents.Click
        Try
            If CInt(cboClaimEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 4, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboClaimEmployee.Focus()
                Exit Sub
            End If

            Dim objDependant As New clsDependants_Beneficiary_tran
            Dim dsList As DataSet = Nothing
            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                dsList = objDependant.GetQualifiedDepedant(CInt(cboClaimEmployee.SelectedValue), False, True, dtpDate.GetDate.Date)
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL Then
                dsList = objDependant.GetQualifiedDepedant(CInt(cboClaimEmployee.SelectedValue), True, False, dtpDate.GetDate.Date)
            Else
                Exit Sub
            End If
            dgDepedent.DataSource = dsList.Tables(0)
            dgDepedent.DataBind()
            popupEmpDepedents.Show()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objDependant = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lnkClaimDepedents_Click :" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.

    Protected Sub lnkApprovalViewDependants_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkApprovalViewDependants.Click
        Try
            If CInt(cboApprovalExpEmployee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 4, "Employee is mandatory information. Please select Employee to continue."), Me)
                cboApprovalExpEmployee.Focus()
                Exit Sub
            End If

            Dim objDependant As New clsDependants_Beneficiary_tran
            Dim dsList As DataSet = Nothing
            If CInt(cboApprovalExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                dsList = objDependant.GetQualifiedDepedant(CInt(cboApprovalExpEmployee.SelectedValue), False, True, dtpApprovalClaimDate.GetDate.Date)
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL Then
                dsList = objDependant.GetQualifiedDepedant(CInt(cboApprovalExpEmployee.SelectedValue), True, False, dtpApprovalClaimDate.GetDate.Date)
            Else
                Exit Sub
            End If
            dgApprovalDepedent.DataSource = dsList.Tables(0)
            dgApprovalDepedent.DataBind()
            popupApprovalEmpDepedents.Show()

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsList IsNot Nothing Then dsList.Clear()
            dsList = Nothing
            objDependant = Nothing
            'Pinkal (11-Sep-2020) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Pinkal (10-Jan-2017) -- End


#End Region

#Region "Combo Event(S)"

#Region "Expense"

    Protected Sub cboExpCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpCategory.SelectedIndexChanged, cboClaimEmployee.SelectedIndexChanged
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim dsCombo As New DataSet
            Dim objExpMaster As New clsExpense_Master
            Dim objLeaveType As New clsleavetype_master
            Dim mdtLeaveType As DataTable = Nothing
        'Pinkal (11-Sep-2020) -- End
        Try

            dsCombo = objExpMaster.getComboList(CInt(IIf(CStr(cboExpCategory.SelectedValue) = "", 0, cboExpCategory.SelectedValue)), True, "List", CInt(IIf(CStr(cboClaimEmployee.SelectedValue) = "", 0, cboClaimEmployee.SelectedValue)), True)


            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
            Dim dtTable As DataTable = Nothing
            dtTable = New DataView(dsCombo.Tables(0), "Id <= 0 OR  isleaveencashment = 0", "", DataViewRowState.CurrentRows).ToTable
            With cboExpense
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dtTable
                .DataBind()
            End With
            'Pinkal (22-Mar-2016) -- End


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable = Nothing
            'Pinkal (11-Sep-2020) -- End


            Call cboExpense_SelectedIndexChanged(cboExpense, New EventArgs())

            If CInt(IIf(CStr(cboExpCategory.SelectedValue) = "", 0, cboExpCategory.SelectedValue)) > 0 Then
                Select Case CInt(cboExpCategory.SelectedValue)

                    Case enExpenseType.EXP_LEAVE
                        objlblValue.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 12, "Leave Form")
                        If mintClaimMstUnkId > 0 Then
                            cboLeaveType.Enabled = False
                        Else
                            cboLeaveType.Enabled = True
                            If cboReference.DataSource IsNot Nothing Then cboReference.SelectedIndex = 0
                            cboReference.Enabled = True
                        End If

                        'Pinkal (25-May-2019) -- Start
                        'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, CStr(Session("Database_Name")), "", True)
                        Else
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, CStr(Session("Database_Name")), "", False)
                        End If
                        'Pinkal (25-May-2019) -- End

                        If mintLeaveTypeId > 0 Then
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "leavetypeunkid = " & mintLeaveTypeId, "", DataViewRowState.CurrentRows).ToTable
                        Else
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                        End If

                        With cboLeaveType
                            .DataValueField = "leavetypeunkid"
                            .DataTextField = "name"
                            .DataSource = mdtLeaveType
                            .DataBind()
                            .SelectedValue = mintLeaveTypeId.ToString()
                        End With
                        Call cboLeaveType_SelectedIndexChanged(cboLeaveType, New EventArgs()) '1Time
                        objLeaveType = Nothing
                End Select

                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                'If CInt(cboClaimEmployee.SelectedValue) > 0 Then
                If cboClaimEmployee.SelectedValue IsNot Nothing AndAlso cboClaimEmployee.SelectedValue <> "" AndAlso CInt(cboClaimEmployee.SelectedValue) > 0 Then
                    'Pinkal (01-Mar-2016) -- End
                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboClaimEmployee.SelectedValue)

                    Dim objState As New clsstate_master
                    objState._Stateunkid = objEmployee._Domicile_Stateunkid

                    Dim mstrCountry As String = ""
                    Dim objCountry As New clsMasterData
                    Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
                    If dsCountry.Tables("List").Rows.Count > 0 Then
                        mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
                    End If
                    dsCountry.Clear()
                    dsCountry = Nothing
                    Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                               IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                               mstrCountry

                    txtDomicileAddress.Text = strAddress
                    objState = Nothing
                    objCountry = Nothing
                    objEmployee = Nothing
                Else
                    txtDomicileAddress.Text = ""
                End If
            Else
                objlblValue.Text = "" : cboReference.DataSource = Nothing : cboReference.Enabled = False : cboLeaveType.Enabled = False
                txtDomicileAddress.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            objExpMaster = Nothing
            objLeaveType = Nothing
            If mdtLeaveType IsNot Nothing Then mdtLeaveType.Clear()
            mdtLeaveType = Nothing
            'Pinkal (11-Sep-2020) -- End

        End Try
    End Sub

    Protected Sub cboExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboExpense.SelectedIndexChanged
        Try

            'Pinkal (20-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If CBool(Session("SectorRouteAssignToExpense")) = True Then
                Dim objAssignExpense As New clsassignexpense_sector
                Dim dtSector As DataTable = objAssignExpense.GetSectorFromExpense(CInt(cboExpense.SelectedValue), True)
                With cboSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtSector
                    .DataBind()
                End With

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtSector IsNot Nothing Then dtSector.Clear()
                dtSector = Nothing
                'Pinkal (11-Sep-2020) -- End

                objAssignExpense = Nothing
            End If
            'Pinkal (20-Feb-2019) -- End

            Dim objExpMaster As New clsExpense_Master
            If CInt(cboExpense.SelectedValue) > 0 Then
                objExpMaster._Expenseunkid = CInt(cboExpense.SelectedValue)
                mblnIsLeaveEncashment = objExpMaster._IsLeaveEncashment

                cboSectorRoute.Enabled = objExpMaster._IsSecRouteMandatory
                cboSectorRoute.SelectedValue = "0"


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                cboCostCenter.SelectedValue = "0"
                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.None Then
                        cboCostCenter.Enabled = False
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Opex Then
                        cboCostCenter.Enabled = False
                        cboCostCenter.SelectedValue = GetEmployeeCostCenter(dtpDate.GetDate.Date, CInt(cboClaimEmployee.SelectedValue)).ToString()
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex Then
                        cboCostCenter.Enabled = True
                    End If
                Else
                    cboCostCenter.Enabled = False
                End If
                'Pinkal (25-May-2019) -- End
                'Pinkal (04-Feb-2019) -- End


                If objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True
                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtUnitPrice.Text = "0.00"
                    txtUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End
                    txtClaimBalance.Text = "0.00"
                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet
                    'Pinkal (30-Apr-2018) - Start
                    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                    'dsBal = objEmpExpBal.Get_Balance(CInt(cboClaimEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_Year")))
                    dsBal = objEmpExpBal.Get_Balance(CInt(cboClaimEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_Year")), dtpDate.GetDate.Date)
                    'Pinkal (30-Apr-2018) - End

                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtClaimBalance.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")).ToString().ToString()
                        'Pinkal (30-Apr-2018) - Start
                        'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                        txtClaimBalanceAsOnDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        'Pinkal (30-Apr-2018) - End
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsBal IsNot Nothing Then dsBal.Clear()
                    dsBal = Nothing
                    'Pinkal (11-Sep-2020) -- End

                    objEmpExpBal = Nothing

                ElseIf objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = True Then

                    txtUnitPrice.Enabled = False : txtUnitPrice.Text = "1.00"
                    Dim objLeave As New clsleavebalance_tran
                    Dim dsList As DataSet = Nothing
                    Dim dtbalance As DataTable = Nothing

                    'Pinkal (09-Aug-2018) -- Start
                    'Bug - Solving Bug for PACRA.


                    'If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                    '    dsList = objLeave.GetList("List", _
                    '                          Session("Database_Name").ToString(), _
                    '                          CInt(Session("UserId")), _
                    '                          CInt(Session("Fin_year")), _
                    '                          CInt(Session("CompanyUnkId")), _
                    '                          Session("EmployeeAsOnDate").ToString(), _
                    '                          Session("UserAccessModeSetting").ToString(), True, _
                    '                          CBool(Session("IsIncludeInactiveEmp")), True, True, False, _
                    '                             CInt(cboClaimEmployee.SelectedValue), False, False, False, "", Nothing, mblnIsExternalApprover)

                    'ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                    '    dsList = objLeave.GetList("List", _
                    '                          Session("Database_Name").ToString(), _
                    '                          CInt(Session("UserId")), _
                    '                          CInt(Session("Fin_year")), _
                    '                          CInt(Session("CompanyUnkId")), _
                    '                          Session("EmployeeAsOnDate").ToString, _
                    '                          Session("UserAccessModeSetting").ToString(), True, _
                    '                          CBool(Session("IsIncludeInactiveEmp")), True, True, False, _
                    '                         CInt(cboClaimEmployee.SelectedValue), True, True, False, "", Nothing, mblnIsExternalApprover)


                    'End If
                    'dtbalance = New DataView(dsList.Tables(0), "leavetypeunkid = " & CInt(objExpMaster._Leavetypeunkid), "", DataViewRowState.CurrentRows).ToTable



                    Dim mstrLeaveTypeID As String = ""
                    Dim objleavetype As New clsleavetype_master
                    mstrLeaveTypeID = objleavetype.GetDeductdToLeaveTypeIDs(CInt(objExpMaster._Leavetypeunkid))

                    If mstrLeaveTypeID.Trim.Length > 0 Then
                        mstrLeaveTypeID &= "," & CInt(objExpMaster._Leavetypeunkid).ToString()
                    Else
                        mstrLeaveTypeID = CInt(objExpMaster._Leavetypeunkid).ToString()
                    End If

                    objleavetype = Nothing

                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                        dsList = objLeave.GetLeaveBalanceInfo(dtpDate.GetDate.Date, mstrLeaveTypeID, CInt(cboClaimEmployee.SelectedValue).ToString(), CInt(Session("LeaveAccrueTenureSetting")) _
                                                                     , CInt(Session("LeaveAccrueDaysAfterEachMonth")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date _
                                                                     , False, False, CInt(Session("LeaveBalanceSetting")), -1, CInt(Session("Fin_year")), Nothing)
                    Else
                        objLeave._DBStartdate = CDate(Session("fin_startdate")).Date
                        objLeave._DBEnddate = CDate(Session("fin_enddate")).Date

                        dsList = objLeave.GetLeaveBalanceInfo(dtpDate.GetDate.Date, mstrLeaveTypeID, CInt(cboClaimEmployee.SelectedValue).ToString(), CInt(Session("LeaveAccrueTenureSetting")) _
                                                                     , CInt(Session("LeaveAccrueDaysAfterEachMonth")), Nothing, Nothing _
                                                                     , True, True, CInt(Session("LeaveBalanceSetting")), -1, CInt(Session("Fin_year")), Nothing)
                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objLeave = Nothing
                    'Pinkal (11-Sep-2020) -- End


                    dtbalance = dsList.Tables(0)


                    If dtbalance IsNot Nothing AndAlso dtbalance.Rows.Count > 0 Then


                        'txtClaimBalance.Text = CStr(CDec(dtbalance.Rows(0)("accrue_amount")) - CDec(dtbalance.Rows(0)("issue_amount")))

                        ''Pinkal (30-Apr-2018) - Start
                        ''Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                        'If CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Yearly Then

                        '    If IsDBNull(dtbalance.Rows(0)("enddate")) Then dtbalance.Rows(0)("enddate") = FinancialYear._Object._Database_End_Date.Date
                        '    If CDate(dtbalance.Rows(0)("enddate")).Date <= dtpDate.GetDate.Date Then
                        '        txtClaimBalanceAsOnDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), CDate(dtbalance.Rows(0)("enddate")).AddDays(1)) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                        '    Else
                        '        txtClaimBalanceAsOnDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), dtpDate.GetDate.Date) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                        '    End If

                        'ElseIf CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Monthly Then


                        '    'Pinkal (18-Jul-2018) -- Start
                        '    'Bug - Solved bug for Showing Wrong Leave Balance When Accrue is Monthly Basis.

                        '    Dim mdtDate As Date = dtpDate.GetDate.Date
                        '    Dim mdtDays As Integer = 0
                        '    'If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                        '    '    intDiff = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                        '    '    mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                        '    'Else
                        '    '    intDiff = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, mdtDate.Date))
                        '    'End If

                        '    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                        '        If IsDBNull(dtbalance.Rows(0)("enddate")) Then
                        '            mdtDate = CDate(Session("fin_enddate")).Date
                        '            'Pinkal (24-Jul-2018) -- Start
                        '            'Bug - Taking Wrong Month Count.
                        '            'mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(Session("fin_enddate")).Date))
                        '            mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(Session("fin_enddate")).AddDays(1).Date))
                        '            'Pinkal (24-Jul-2018) -- End

                        '        Else
                        '            If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                        '    mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                        '                'Pinkal (24-Jul-2018) -- Start
                        '                'Bug - Taking Wrong Month Count.
                        '                'mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                        '                mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).AddDays(1).Date))
                        '                'Pinkal (24-Jul-2018) -- End
                        '            End If
                        '        End If

                        '    ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                        '        If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                        '            mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                        '        End If
                        '        'Pinkal (24-Jul-2018) -- Start
                        '        'Bug - Taking Wrong Month Count.
                        '        'mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                        '        mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).AddDays(1).Date))
                        '        'Pinkal (24-Jul-2018) -- End

                        '    End If

                        '    Dim intDiff As Integer = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, mdtDate.Date.AddMonths(1)))

                        '    If CInt(Session("LeaveAccrueDaysAfterEachMonth")) > mdtDate.Date.Day OrElse intDiff > mdtDays Then
                        '        intDiff = intDiff - 1
                        '    End If

                        '    'Pinkal (18-Jul-2018) -- End

                        '    txtClaimBalanceAsOnDate.Text = Math.Round(CDec(CInt(dtbalance.Rows(0)("monthly_accrue")) * intDiff) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                        'End If
                        ''Pinkal (30-Apr-2018) - End

                        txtClaimBalance.Text = CDec(CDec(dtbalance.Rows(0)("TotalAcccrueAmt")) + CDec(dtbalance.Rows(0)("LeaveAdjustment")) + CDec(dtbalance.Rows(0)("LeaveBF")) - (CDec(dtbalance.Rows(0)("Issue_amount")) + CDec(dtbalance.Rows(0)("LeaveEncashment")))).ToString("#0.00")
                        txtClaimBalanceAsOnDate.Text = CDec(CDec(dtbalance.Rows(0)("Accrue_amount")) + CDec(dtbalance.Rows(0)("LeaveAdjustment")) + CDec(dtbalance.Rows(0)("LeaveBF")) - (CDec(dtbalance.Rows(0)("Issue_amount")) + CDec(dtbalance.Rows(0)("LeaveEncashment")))).ToString("#0.00")

                        'Pinkal (09-Aug-2018) -- End

                        txtUoMType.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsExpCommonMethods", 6, "Quantity")
                    End If


                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList = Nothing
                    If dtbalance IsNot Nothing Then dtbalance.Clear()
                    dtbalance = Nothing
                    'Pinkal (11-Sep-2020) -- End


                ElseIf objExpMaster._Isaccrue = True AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtUnitPrice.Enabled = True

                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtUnitPrice.Text = "0.00"
                    txtUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End

                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet
                    'Pinkal (30-Apr-2018) - Start
                    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                    'dsBal = objEmpExpBal.Get_Balance(CInt(cboClaimEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_Year")))
                    dsBal = objEmpExpBal.Get_Balance(CInt(cboClaimEmployee.SelectedValue), CInt(cboExpense.SelectedValue), CInt(Session("Fin_Year")), dtpDate.GetDate.Date)
                    'Pinkal (30-Apr-2018) - End
                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtClaimBalance.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")).ToString()
                        'Pinkal (30-Apr-2018) - Start
                        'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                        txtClaimBalanceAsOnDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        'Pinkal (30-Apr-2018) - End
                        txtUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsBal IsNot Nothing Then dsBal.Clear()
                    dsBal = Nothing
                    'Pinkal (11-Sep-2020) -- End

                    objEmpExpBal = Nothing
                End If
            Else
                txtUoMType.Text = ""
                txtUnitPrice.Enabled = True

                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0.00"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End

                'Pinkal (30-Apr-2018) - Start
                'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                txtClaimBalance.Text = "0.00"
                txtClaimBalanceAsOnDate.Text = "0.00"
                'Pinkal (30-Apr-2018) - End

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                cboCostCenter.Enabled = False
                cboCostCenter.SelectedValue = "0"
                'Pinkal (04-Feb-2019) -- End

                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                txtCalimQty.Text = "1"
                'Pinkal (04-Feb-2020) -- End

            End If



            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager. AS Per Guidance with Matthew on 06-Feb-2020. 
            'If objExpMaster._IsConsiderDependants AndAlso objExpMaster._IsSecRouteMandatory = False Then
            If objExpMaster._IsConsiderDependants Then
                txtCalimQty.Text = GetEmployeeDepedentCountForCR(CInt(cboClaimEmployee.SelectedValue)).ToString()
                mintEmpMaxCountDependentsForCR = CInt(txtCalimQty.Text)
            Else
                txtCalimQty.Text = "1"
            End If
            'Pinkal (04-Feb-2020) -- End

            'Pinkal (13-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES. [shall i allow to edit quantity in Claim approval as per yesterday conversation ? Matthew 26-Jun-2019, 11:44  Yes, allow. Only on approval only]
            'If objExpMaster._IsConsiderDependants Then txtCalimQty.Enabled = False Else txtCalimQty.Enabled = True
            'Pinkal (13-Jun-2019) -- End

            txtUnitPrice.Enabled = objExpMaster._IsUnitPriceEditable
            'Pinkal (25-May-2019) -- End


            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then

                'Gajanan [17-Sep-2020] -- Start
                'New UI Change
                'lblBalanceAddEdit.Visible = True
                'lblClaimBalanceasondate.Visible = True
                'txtClaimBalance.Visible = True
                'txtClaimBalanceAsOnDate.Visible = True
                pnlClaimBalAsonDate.Visible = True
                'Gajanan [17-Sep-2020] -- End
             
                cboClaimCurrency.SelectedValue = mintClaimBaseCountryId.ToString()
                cboClaimCurrency.Enabled = False
            Else

                'Gajanan [17-Sep-2020] -- Start
                'New UI Change
                'lblBalanceAddEdit.Visible = False
                'lblClaimBalanceasondate.Visible = False
                'txtClaimBalance.Visible = False
                'txtClaimBalanceAsOnDate.Visible = False
                pnlClaimBalAsonDate.Visible = False
                'Gajanan [17-Sep-2020] -- End

                If mdtExpense IsNot Nothing Then
                    If mdtExpense.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                        cboClaimCurrency.Enabled = False
                        cboClaimCurrency.SelectedValue = CStr(mdtExpense.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                    Else
                        cboClaimCurrency.SelectedValue = mintClaimBaseCountryId.ToString()
                        cboClaimCurrency.Enabled = True
                    End If
                End If

            End If
            'Pinkal (04-Feb-2019) -- End

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExpMaster = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboExpense_SelectedIndexChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboLeaveType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLeaveType.SelectedIndexChanged
        Try
            Dim objleave As New clsleaveform
            Dim dsLeave As New DataSet
            Dim mdtLeaveForm As DataTable = Nothing

            If CInt(IIf(cboLeaveType.SelectedValue = "", 0, cboLeaveType.SelectedValue)) = 0 Then
                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                'dsLeave = objleave.GetLeaveFormForExpense(0, "2,7", CInt(cboClaimEmployee.SelectedValue), True, True, mintLeaveFormUnkId)
                Dim intEmpID As Integer = -1
                If cboClaimEmployee.SelectedValue Is Nothing OrElse cboClaimEmployee.SelectedValue = "" Then
                    intEmpID = -1
                ElseIf cboClaimEmployee.SelectedValue IsNot Nothing AndAlso CInt(cboClaimEmployee.SelectedValue) > 0 Then
                    intEmpID = CInt(cboClaimEmployee.SelectedValue)
                End If
                dsLeave = objleave.GetLeaveFormForExpense(0, "2,7", intEmpID, True, True, mintLeaveFormUnkId)
                'Pinkal (01-Mar-2016) -- End
            Else

                'Pinkal (01-Mar-2016) -- Start
                'Enhancement - Implementing External Approver in Claim Request & Leave Module.
                'dsLeave = objleave.GetLeaveFormForExpense(CInt(cboLeaveType.SelectedValue), "2,7", CInt(cboClaimEmployee.SelectedValue), True, True, mintLeaveFormUnkId)
                Dim intEmpID As Integer = -1
                If cboClaimEmployee.SelectedValue Is Nothing OrElse cboClaimEmployee.SelectedValue = "" Then
                    intEmpID = -1
                ElseIf cboClaimEmployee.SelectedValue IsNot Nothing AndAlso CInt(cboClaimEmployee.SelectedValue) > 0 Then
                    intEmpID = CInt(cboClaimEmployee.SelectedValue)
                End If
                dsLeave = objleave.GetLeaveFormForExpense(CInt(cboLeaveType.SelectedValue), "2,7", intEmpID, True, True, mintLeaveFormUnkId)
                'Pinkal (01-Mar-2016) -- End

            End If

            If mintLeaveFormUnkId > 0 Then
                mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & mintLeaveFormUnkId, "", DataViewRowState.CurrentRows).ToTable
            Else
                If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso mintLeaveFormUnkId <= 0 Then
                    mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & mintLeaveFormUnkId, "", DataViewRowState.CurrentRows).ToTable
                ElseIf CBool(Session("PaymentApprovalwithLeaveApproval")) = False AndAlso mintLeaveFormUnkId <= 0 Then
                    mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & mintLeaveFormUnkId, "", DataViewRowState.CurrentRows).ToTable
                Else
                    mdtLeaveForm = New DataView(dsLeave.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                End If
            End If

            cboReference.DataSource = Nothing
            With cboReference
                .DataValueField = "formunkid"
                .DataTextField = "name"
                .DataSource = mdtLeaveForm.Copy
                .DataBind()
                If mintLeaveFormUnkId < 0 Then
                    .SelectedValue = "0"
                Else
                    .SelectedValue = mintLeaveFormUnkId.ToString()
                End If
            End With

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If mdtLeaveForm IsNot Nothing Then mdtLeaveForm.Clear()
            mdtLeaveForm = Nothing
            If dsLeave IsNot Nothing Then dsLeave.Clear()
            dsLeave = Nothing
            'Pinkal (11-Sep-2020) -- End

            objleave = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboLeaveType_SelectedIndexChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Pinkal (10-Jan-2017) -- Start
    'Enhancement - Working on TRA C&R Module Changes with Leave Module.

#Region " Add For Approval Expense"

    Protected Sub cboApprovalExpCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApprovalExpCategory.SelectedIndexChanged, cboApprovalExpEmployee.SelectedIndexChanged
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            Dim dsCombo As New DataSet
            Dim objExpMaster As New clsExpense_Master
            Dim objLeaveType As New clsleavetype_master
            Dim mdtLeaveType As DataTable = Nothing
        'Pinkal (11-Sep-2020) -- End
        Try


            'Pinkal (26-Dec-2018) -- Start
            'Enhancement - Working on Leave/Claim Enhancement Phase 2 for NMB.
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                dsCombo = objExpMaster.getComboList(CInt(IIf(CStr(cboApprovalExpCategory.SelectedValue) = "", 0, cboApprovalExpCategory.SelectedValue)), True, "List", CInt(IIf(CStr(cboApprovalExpEmployee.SelectedValue) = "", 0, cboApprovalExpEmployee.SelectedValue)), True, 0, "ISNULL(cr_expinvisible,0) = 0 AND ISNULL(cmexpense_master.isshowoness,0) = 1")
            ElseIf (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                dsCombo = objExpMaster.getComboList(CInt(IIf(CStr(cboApprovalExpCategory.SelectedValue) = "", 0, cboApprovalExpCategory.SelectedValue)), True, "List", CInt(IIf(CStr(cboApprovalExpEmployee.SelectedValue) = "", 0, cboApprovalExpEmployee.SelectedValue)), True, 0, "ISNULL(isleaveencashment,0) = 0")
            End If

            'Dim dtTable As DataTable = Nothing
            'dtTable = New DataView(dsCombo.Tables(0), "Id <= 0 OR  isleaveencashment = 0", "", DataViewRowState.CurrentRows).ToTable
            With cboApprovalExpense
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            'Pinkal (26-Dec-2018) -- End


            Call cboApprovalExpense_SelectedIndexChanged(cboApprovalExpense, New EventArgs())

            If CInt(IIf(CStr(cboApprovalExpCategory.SelectedValue) = "", 0, cboApprovalExpCategory.SelectedValue)) > 0 Then
                Select Case CInt(cboApprovalExpCategory.SelectedValue)

                    Case enExpenseType.EXP_LEAVE
                        objlblValue.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, 12, "Leave Form")
                        If mintClaimMstUnkId > 0 Then
                            cboApprovalExpLeaveType.Enabled = False
                        Else
                            cboApprovalExpLeaveType.Enabled = True
                            If cboApprovalReference.DataSource IsNot Nothing Then cboApprovalReference.SelectedIndex = 0
                            cboApprovalReference.Enabled = True
                        End If

                        'Pinkal (25-May-2019) -- Start
                        'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                        If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, CStr(Session("Database_Name")), "", True)
                        Else
                            dsCombo = objLeaveType.getListForCombo("List", True, 1, CStr(Session("Database_Name")), "", False)
                        End If
                        'Pinkal (25-May-2019) -- End

                        If mintLeaveTypeId > 0 Then
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "leavetypeunkid = " & mintLeaveTypeId, "", DataViewRowState.CurrentRows).ToTable
                        Else
                            mdtLeaveType = New DataView(dsCombo.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                        End If

                        With cboApprovalExpLeaveType
                            .DataValueField = "leavetypeunkid"
                            .DataTextField = "name"
                            .DataSource = mdtLeaveType
                            .DataBind()
                            .SelectedValue = mintLeaveTypeId.ToString()
                        End With
                        Call cboApprovalExpLeaveType_SelectedIndexChanged(cboApprovalExpLeaveType, New EventArgs()) '1Time
                        objLeaveType = Nothing
                End Select

                If cboApprovalExpEmployee.SelectedValue IsNot Nothing AndAlso cboApprovalExpEmployee.SelectedValue <> "" AndAlso CInt(cboApprovalExpEmployee.SelectedValue) > 0 Then
                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date) = CInt(cboApprovalExpEmployee.SelectedValue)

                    Dim objState As New clsstate_master
                    objState._Stateunkid = objEmployee._Domicile_Stateunkid

                    Dim mstrCountry As String = ""
                    Dim objCountry As New clsMasterData
                    Dim dsCountry As DataSet = objCountry.getCountryList("List", False, objEmployee._Domicile_Countryunkid)
                    If dsCountry.Tables("List").Rows.Count > 0 Then
                        mstrCountry = dsCountry.Tables("List").Rows(0)("country_name").ToString
                    End If
                    dsCountry.Clear()
                    dsCountry = Nothing
                    Dim strAddress As String = IIf(objEmployee._Domicile_Address1 <> "", objEmployee._Domicile_Address1 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Address2 <> "", objEmployee._Domicile_Address2 & Environment.NewLine, "").ToString & _
                                               IIf(objEmployee._Domicile_Road <> "", objEmployee._Domicile_Road & Environment.NewLine, "").ToString & _
                                               IIf(objState._Name <> "", objState._Name & Environment.NewLine, "").ToString & _
                                               mstrCountry

                    txtApprovalDomicileAddress.Text = strAddress
                    objState = Nothing
                    objCountry = Nothing
                    objEmployee = Nothing
                Else
                    txtApprovalDomicileAddress.Text = ""
                End If
            Else
                objlblValue.Text = "" : cboApprovalReference.DataSource = Nothing : cboApprovalReference.Enabled = False : cboApprovalExpLeaveType.Enabled = False
                txtApprovalDomicileAddress.Text = ""
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsCombo IsNot Nothing Then dsCombo.Clear()
            dsCombo = Nothing
            If mdtLeaveType IsNot Nothing Then mdtLeaveType.Clear()
            mdtLeaveType = Nothing
            objExpMaster = Nothing
            objLeaveType = Nothing
            'Pinkal (11-Sep-2020) -- End
        End Try
    End Sub

    Protected Sub cboApprovalExpense_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApprovalExpense.SelectedIndexChanged
        Try

            'Pinkal (20-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If CBool(Session("SectorRouteAssignToExpense")) Then
                Dim objAssignExpense As New clsassignexpense_sector
                Dim dtSector As DataTable = objAssignExpense.GetSectorFromExpense(CInt(cboApprovalExpense.SelectedValue), True)
                With cboApprovalSectorRoute
                    .DataValueField = "secrouteunkid"
                    .DataTextField = "Sector"
                    .DataSource = dtSector
                    .DataBind()
                    .SelectedIndex = 0
                End With

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                If dtSector IsNot Nothing Then dtSector.Clear()
                dtSector = Nothing
                'Pinkal (11-Sep-2020) -- End

                objAssignExpense = Nothing
            End If
            'Pinkal (20-Feb-2019) -- End

            Dim objExpMaster As New clsExpense_Master
            If CInt(cboApprovalExpense.SelectedValue) > 0 Then
                objExpMaster._Expenseunkid = CInt(cboApprovalExpense.SelectedValue)
                mblnIsLeaveEncashment = objExpMaster._IsLeaveEncashment

                cboApprovalSectorRoute.Enabled = objExpMaster._IsSecRouteMandatory
                cboApprovalSectorRoute.SelectedValue = "0"


                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                cboApprovalCostCenter.SelectedValue = "0"
                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                If Session("NewRequisitionRequestP2PServiceURL").ToString().Trim.Length > 0 Then
                    If objExpMaster._ExpenditureTypeId = enP2PExpenditureType.None Then
                        cboApprovalCostCenter.Enabled = False
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Opex Then
                        cboApprovalCostCenter.Enabled = False
                        cboApprovalCostCenter.SelectedValue = GetEmployeeCostCenter(dtpApprovalClaimDate.GetDate.Date, CInt(cboApprovalExpEmployee.SelectedValue)).ToString()
                    ElseIf objExpMaster._ExpenditureTypeId = enP2PExpenditureType.Capex Then
                        cboApprovalCostCenter.Enabled = True
                    End If
                Else
                    cboApprovalCostCenter.Enabled = False
                End If
                'Pinkal (25-May-2019) -- End
                'Pinkal (04-Feb-2019) -- End

                If objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtApprovalUnitPrice.Enabled = True

                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtApprovalUnitPrice.Text = "0.00"
                    txtApprovalUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End

                    txtApprovalBalance.Text = "0.00"
                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet
                    'Pinkal (30-Apr-2018) - Start
                    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                    'dsBal = objEmpExpBal.Get_Balance(CInt(cboApprovalExpEmployee.SelectedValue), CInt(cboApprovalExpense.SelectedValue), CInt(Session("Fin_Year")))
                    dsBal = objEmpExpBal.Get_Balance(CInt(cboApprovalExpEmployee.SelectedValue), CInt(cboApprovalExpense.SelectedValue), CInt(Session("Fin_Year")), dtpApprovalClaimDate.GetDate.Date)
                    'Pinkal (30-Apr-2018) - End
                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtApprovalBalance.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")).ToString()
                        'Pinkal (30-Apr-2018) - Start
                        'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                        txtApprovalExpBalAsonDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        'Pinkal (30-Apr-2018) - End
                        txtApprovalUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsBal IsNot Nothing Then dsBal.Clear()
                    dsBal = Nothing
                    'Pinkal (11-Sep-2020) -- End

                    objEmpExpBal = Nothing

                ElseIf objExpMaster._Isaccrue = False AndAlso objExpMaster._IsLeaveEncashment = True Then

                    txtApprovalUnitPrice.Enabled = False : txtApprovalUnitPrice.Text = "1.00"
                    Dim objLeave As New clsleavebalance_tran
                    Dim dsList As DataSet = Nothing
                    Dim dtbalance As DataTable = Nothing

                    'Pinkal (09-Aug-2018) -- Start
                    'Bug - Solving Bug for PACRA.

                    'If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then

                    '    dsList = objLeave.GetList("List", _
                    '                              Session("Database_Name").ToString(), _
                    '                              CInt(Session("UserId")), _
                    '                              CInt(Session("Fin_year")), _
                    '                              CInt(Session("CompanyUnkId")), _
                    '                              Session("EmployeeAsOnDate").ToString(), _
                    '                              Session("UserAccessModeSetting").ToString(), True, _
                    '                              CBool(Session("IsIncludeInactiveEmp")), True, True, False, _
                    '                             CInt(cboApprovalExpEmployee.SelectedValue), False, False, False, "", Nothing, mblnIsExternalApprover)

                    'ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then

                    '    dsList = objLeave.GetList("List", _
                    '                              Session("Database_Name").ToString(), _
                    '                              CInt(Session("UserId")), _
                    '                              CInt(Session("Fin_year")), _
                    '                              CInt(Session("CompanyUnkId")), _
                    '                              Session("EmployeeAsOnDate").ToString, _
                    '                              Session("UserAccessModeSetting").ToString(), True, _
                    '                              CBool(Session("IsIncludeInactiveEmp")), True, True, False, _
                    '                              CInt(cboApprovalExpEmployee.SelectedValue), True, True, False, "", Nothing, mblnIsExternalApprover)

                    'End If

                    'dtbalance = New DataView(dsList.Tables(0), "leavetypeunkid = " & CInt(objExpMaster._Leavetypeunkid), "", DataViewRowState.CurrentRows).ToTable

                    Dim mstrLeaveTypeID As String = ""
                    Dim objleavetype As New clsleavetype_master
                    mstrLeaveTypeID = objleavetype.GetDeductdToLeaveTypeIDs(CInt(objExpMaster._Leavetypeunkid))

                    If mstrLeaveTypeID.Trim.Length > 0 Then
                        mstrLeaveTypeID &= "," & CInt(objExpMaster._Leavetypeunkid).ToString()
                    Else
                        mstrLeaveTypeID = CInt(objExpMaster._Leavetypeunkid).ToString()
                    End If

                    objleavetype = Nothing

                    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                        dsList = objLeave.GetLeaveBalanceInfo(dtpDate.GetDate.Date, mstrLeaveTypeID, CInt(cboApprovalExpEmployee.SelectedValue).ToString(), CInt(Session("LeaveAccrueTenureSetting")) _
                                                                     , CInt(Session("LeaveAccrueDaysAfterEachMonth")), CDate(Session("fin_startdate")).Date, CDate(Session("fin_enddate")).Date _
                                                                     , False, False, CInt(Session("LeaveBalanceSetting")), -1, CInt(Session("Fin_year")), Nothing)
                    Else
                        objLeave._DBStartdate = CDate(Session("fin_startdate")).Date
                        objLeave._DBEnddate = CDate(Session("fin_enddate")).Date

                        dsList = objLeave.GetLeaveBalanceInfo(dtpDate.GetDate.Date, mstrLeaveTypeID, CInt(cboApprovalExpEmployee.SelectedValue).ToString(), CInt(Session("LeaveAccrueTenureSetting")) _
                                                                     , CInt(Session("LeaveAccrueDaysAfterEachMonth")), Nothing, Nothing _
                                                                     , True, True, CInt(Session("LeaveBalanceSetting")), -1, CInt(Session("Fin_year")), Nothing)
                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    objLeave = Nothing
                    'Pinkal (11-Sep-2020) -- End


                    dtbalance = dsList.Tables(0)

                    If dtbalance IsNot Nothing AndAlso dtbalance.Rows.Count > 0 Then

                        'txtApprovalBalance.Text = CStr(CDec(dtbalance.Rows(0)("accrue_amount")) - CDec(dtbalance.Rows(0)("issue_amount")))

                        ''Pinkal (30-Apr-2018) - Start
                        ''Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.

                        ''Pinkal (18-Jul-2018) -- Start
                        ''Bug - Solved bug for Showing Wrong Leave Balance When Accrue is Monthly Basis.

                        'If CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Yearly Then

                        '    If IsDBNull(dtbalance.Rows(0)("enddate")) Then dtbalance.Rows(0)("enddate") = CDate(Session("fin_enddate")).Date
                        '    If CDate(dtbalance.Rows(0)("enddate")).Date <= dtpDate.GetDate.Date Then
                        '        txtApprovalExpBalAsonDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), CDate(dtbalance.Rows(0)("enddate")).AddDays(1)) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                        '    Else
                        '        txtApprovalExpBalAsonDate.Text = Math.Round(CDec(DateDiff(DateInterval.Day, CDate(dtbalance.Rows(0)("startdate")), dtpDate.GetDate.Date) * CDec(dtbalance.Rows(0)("daily_amount"))) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()
                        '    End If

                        'ElseIf CInt(Session("LeaveAccrueTenureSetting")) = enLeaveAccrueTenureSetting.Monthly Then

                        '    Dim mdtDate As Date = dtpDate.GetDate.Date
                        '    Dim mdtDays As Integer = 0

                        '    If CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.Financial_Year Then
                        '        If IsDBNull(dtbalance.Rows(0)("enddate")) Then
                        '            mdtDate = CDate(Session("fin_enddate")).Date
                        '            'Pinkal (24-Jul-2018) -- Start
                        '            'Bug - Taking Wrong Month Count.
                        '            'mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(Session("fin_enddate")).Date))
                        '            mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(Session("fin_enddate")).AddDays(1).Date))
                        '            'Pinkal (24-Jul-2018) -- End

                        '        Else
                        '            If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                        '                mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                        '                'Pinkal (24-Jul-2018) -- Start
                        '                'Bug - Taking Wrong Month Count.
                        '                'mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                        '                mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).AddDays(1).Date))
                        '                'Pinkal (24-Jul-2018) -- End
                        '            End If
                        '        End If

                        '    ElseIf CInt(Session("LeaveBalanceSetting")) = enLeaveBalanceSetting.ELC Then
                        '        If mdtDate.Date > CDate(dtbalance.Rows(0)("enddate")).Date Then
                        '            mdtDate = CDate(dtbalance.Rows(0)("enddate")).Date
                        '        End If
                        '        'Pinkal (24-Jul-2018) -- Start
                        '        'Bug - Taking Wrong Month Count.
                        '        'mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).Date))
                        '        mdtDays = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, CDate(dtbalance.Rows(0)("enddate")).AddDays(1).Date))
                        '        'Pinkal (24-Jul-2018) -- End
                        '    End If

                        '    Dim intDiff As Integer = CInt(DateDiff(DateInterval.Month, CDate(dtbalance.Rows(0)("startdate")).Date, mdtDate.Date.AddMonths(1)))

                        '    If CInt(Session("LeaveAccrueDaysAfterEachMonth")) > mdtDate.Date.Day OrElse intDiff > mdtDays Then
                        '        intDiff = intDiff - 1
                        '    End If

                        '    txtApprovalExpBalAsonDate.Text = Math.Round(CDec(CInt(dtbalance.Rows(0)("monthly_accrue")) * intDiff) + CDec(dtbalance.Rows(0)("adj_remaining_bal")) + CDec(dtbalance.Rows(0)("LeaveBF")) - CDec(dtbalance.Rows(0)("Issue_amount")), 2).ToString()

                        'End If

                        'Pinkal (18-Jul-2018) -- End

                        'Pinkal (30-Apr-2018) - End

                        txtApprovalBalance.Text = CDec(CDec(dtbalance.Rows(0)("TotalAcccrueAmt")) + CDec(dtbalance.Rows(0)("LeaveAdjustment")) + CDec(dtbalance.Rows(0)("LeaveBF")) - (CDec(dtbalance.Rows(0)("Issue_amount")) + CDec(dtbalance.Rows(0)("LeaveEncashment")))).ToString("#0.00")
                        txtApprovalExpBalAsonDate.Text = CDec(CDec(dtbalance.Rows(0)("Accrue_amount")) + CDec(dtbalance.Rows(0)("LeaveAdjustment")) + CDec(dtbalance.Rows(0)("LeaveBF")) - (CDec(dtbalance.Rows(0)("Issue_amount")) + CDec(dtbalance.Rows(0)("LeaveEncashment")))).ToString("#0.00")

                        'Pinkal (09-Aug-2018) -- End

                        txtApprovalUoMType.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),"clsExpCommonMethods", 6, "Quantity")
                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dtbalance IsNot Nothing Then dtbalance.Clear()
                    dtbalance = Nothing
                    If dsList IsNot Nothing Then dsList.Clear()
                    dsList = Nothing
                    'Pinkal (11-Sep-2020) -- End


                ElseIf objExpMaster._Isaccrue = True AndAlso objExpMaster._IsLeaveEncashment = False Then
                    txtApprovalUnitPrice.Enabled = True
                    'Pinkal (07-Feb-2020) -- Start
                    'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                    'txtApprovalUnitPrice.Text = "0.00"
                    txtApprovalUnitPrice.Text = "1.00"
                    'Pinkal (07-Feb-2020) -- End

                    Dim objEmpExpBal As New clsEmployeeExpenseBalance : Dim dsBal As New DataSet
                    'Pinkal (30-Apr-2018) - Start
                    'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                    'dsBal = objEmpExpBal.Get_Balance(CInt(cboApprovalExpEmployee.SelectedValue), CInt(cboApprovalExpense.SelectedValue), CInt(Session("Fin_Year")))
                    dsBal = objEmpExpBal.Get_Balance(CInt(cboApprovalExpEmployee.SelectedValue), CInt(cboApprovalExpense.SelectedValue), CInt(Session("Fin_Year")), dtpApprovalClaimDate.GetDate.Date)
                    'Pinkal (30-Apr-2018) - End
                    If dsBal.Tables(0).Rows.Count > 0 Then
                        txtApprovalBalance.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("bal")).ToString()
                        'Pinkal (30-Apr-2018) - Start
                        'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
                        txtApprovalExpBalAsonDate.Text = Convert.ToDecimal(dsBal.Tables(0).Rows(0).Item("balasondate")).ToString()
                        'Pinkal (30-Apr-2018) - End
                        txtApprovalUoMType.Text = CStr(dsBal.Tables(0).Rows(0).Item("uom"))
                    End If

                    'Pinkal (11-Sep-2020) -- Start
                    'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                    If dsBal IsNot Nothing Then dsBal.Clear()
                    dsBal = Nothing
                    'Pinkal (11-Sep-2020) -- End

                    objEmpExpBal = Nothing
                End If
            Else
                txtApprovalUoMType.Text = "" : txtApprovalBalance.Text = ""
                txtApprovalUnitPrice.Enabled = True

                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtApprovalUnitPrice.Text = "0.00"
                txtApprovalUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End

                'Pinkal (04-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                txtApprovalBalance.Text = "0.00"
                txtApprovalExpBalAsonDate.Text = "0.00"
                cboApprovalCostCenter.Enabled = False
                cboApprovalCostCenter.SelectedValue = "0"
                'Pinkal (04-Feb-2019) -- End


                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                txtApprovalQty.Text = "1"
                'Pinkal (04-Feb-2020) -- End

            End If

            'Pinkal (04-Feb-2020) -- Start
            'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager. AS Per Guidance with Matthew on 06-Feb-2020. 
            'If objExpMaster._IsConsiderDependants AndAlso objExpMaster._IsSecRouteMandatory = False Then
            If objExpMaster._IsConsiderDependants Then
                txtApprovalQty.Text = GetEmployeeDepedentCountForCR(CInt(cboApprovalExpEmployee.SelectedValue)).ToString()
                mintEmpMaxCountDependentsForCR = CInt(txtApprovalQty.Text)
            Else
                txtApprovalQty.Text = "1"
            End If
            'Pinkal (25-Oct-2018) -- End

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            'Pinkal (13-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES. [shall i allow to edit quantity in Claim approval as per yesterday conversation ? Matthew 26-Jun-2019, 11:44  Yes, allow. Only on approval only]
            'If objExpMaster._IsConsiderDependants Then txtApprovalQty.Enabled = False Else txtApprovalQty.Enabled = True
            'Pinkal (13-Jun-2019) -- End

            txtApprovalUnitPrice.Enabled = objExpMaster._IsUnitPriceEditable
            'Pinkal (25-May-2019) -- End

            'Pinkal (04-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            If objExpMaster._Isaccrue OrElse objExpMaster._IsLeaveEncashment Then


                'Gajanan [17-Sep-2020] -- Start
                'New UI Change
                'lblApprovalExpBalance.Visible = True
                'lblApprovalExpBalAsonDate.Visible = True
                'txtApprovalBalance.Visible = True
                'txtApprovalExpBalAsonDate.Visible = True
                pnlApprovalClaimBalAsonDate.Visible = True
                'Gajanan [17-Sep-2020] -- End
                
                cboApprovalCurrency.SelectedValue = mintApprovalBaseCountryId.ToString()
                cboApprovalCurrency.Enabled = False
            Else
                'Gajanan [17-Sep-2020] -- Start
                'New UI Change
                'lblApprovalExpBalance.Visible = False
                'lblApprovalExpBalAsonDate.Visible = False
                'txtApprovalBalance.Visible = False
                'txtApprovalExpBalAsonDate.Visible = False
                pnlApprovalClaimBalAsonDate.Visible = False
                'Gajanan [17-Sep-2020] -- End

                If mdtLeaveExpenseTran IsNot Nothing Then
                    If mdtLeaveExpenseTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).Count > 0 Then
                        cboApprovalCurrency.Enabled = False
                        cboApprovalCurrency.SelectedValue = CStr(mdtLeaveExpenseTran.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "D").Select(Function(x) x.Field(Of Integer)("Countryunkid")).First())
                    Else
                        cboApprovalCurrency.SelectedValue = mintApprovalBaseCountryId.ToString()
                        cboApprovalCurrency.Enabled = True
                    End If
                End If

            End If
            'Pinkal (04-Feb-2019) -- End

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objExpMaster = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboApprovalExpense_SelectedIndexChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub cboApprovalExpLeaveType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboApprovalExpLeaveType.SelectedIndexChanged
        Try
            Dim objleave As New clsleaveform
            Dim dsLeave As New DataSet
            Dim mdtLeaveForm As DataTable = Nothing

            If CInt(IIf(cboApprovalExpLeaveType.SelectedValue = "", 0, cboApprovalExpLeaveType.SelectedValue)) = 0 Then
                Dim intEmpID As Integer = -1
                If cboApprovalExpEmployee.SelectedValue Is Nothing OrElse cboApprovalExpEmployee.SelectedValue = "" Then
                    intEmpID = -1
                ElseIf cboApprovalExpEmployee.SelectedValue IsNot Nothing AndAlso CInt(cboApprovalExpEmployee.SelectedValue) > 0 Then
                    intEmpID = CInt(cboApprovalExpEmployee.SelectedValue)
                End If
                dsLeave = objleave.GetLeaveFormForExpense(0, "2,7", intEmpID, True, True, mintLeaveFormUnkId)
            Else
                Dim intEmpID As Integer = -1
                If cboApprovalExpEmployee.SelectedValue Is Nothing OrElse cboApprovalExpEmployee.SelectedValue = "" Then
                    intEmpID = -1
                ElseIf cboApprovalExpEmployee.SelectedValue IsNot Nothing AndAlso CInt(cboApprovalExpEmployee.SelectedValue) > 0 Then
                    intEmpID = CInt(cboApprovalExpEmployee.SelectedValue)
                End If
                dsLeave = objleave.GetLeaveFormForExpense(CInt(cboLeaveType.SelectedValue), "2,7", intEmpID, True, True, mintLeaveFormUnkId)
            End If

            If mintLeaveFormUnkId > 0 Then
                mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & mintLeaveFormUnkId, "", DataViewRowState.CurrentRows).ToTable
            Else
                If CBool(Session("PaymentApprovalwithLeaveApproval")) AndAlso mintLeaveFormUnkId <= 0 Then
                    mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & mintLeaveFormUnkId, "", DataViewRowState.CurrentRows).ToTable
                ElseIf CBool(Session("PaymentApprovalwithLeaveApproval")) = False AndAlso mintLeaveFormUnkId <= 0 Then
                    mdtLeaveForm = New DataView(dsLeave.Tables(0), "formunkid = " & mintLeaveFormUnkId, "", DataViewRowState.CurrentRows).ToTable
                Else
                    mdtLeaveForm = New DataView(dsLeave.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                End If
            End If

            cboApprovalReference.DataSource = Nothing
            With cboApprovalReference
                .DataValueField = "formunkid"
                .DataTextField = "name"
                .DataSource = mdtLeaveForm.Copy
                .DataBind()
                If mintLeaveFormUnkId < 0 Then
                    .SelectedValue = "0"
                Else
                    .SelectedValue = mintLeaveFormUnkId.ToString()
                End If
            End With

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dsLeave IsNot Nothing Then dsLeave.Clear()
            dsLeave = Nothing
            If mdtLeaveForm IsNot Nothing Then mdtLeaveForm.Clear()
            mdtLeaveForm = Nothing
            'Pinkal (11-Sep-2020) -- End

            objleave = Nothing
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboApprovalExpLeaveType_SelectedIndexChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Pinkal (10-Jan-2017) -- End


#End Region

#Region "Controls Event(S)"

    Private Sub dtpStartdate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpStartdate.TextChanged, dtpEnddate.TextChanged
        'Pinkal (11-Sep-2020) -- Start
        'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
        Dim objPendingLeave As New clspendingleave_Tran
        'Pinkal (11-Sep-2020) -- End
        Try
            objPendingLeave._Pendingleavetranunkid = mintPenddingLvTranUnkId


            'Pinkal (25-Mar-2019) -- Start
            'Enhancement - Working on Leave Changes for NMB.
            If CBool(Session("AllowLvApplicationApplyForBackDates")) = False AndAlso objPendingLeave._Startdate.Date > dtpStartdate.GetDate.Date Then

                'Pinkal (05-Jun-2020) -- Start
                'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 29, "Sorry, As per company setting you are not allow to set leave application start date to previous date."), Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 30, "Sorry, As per company setting you are not allow to set leave application start date to previous date."), Me)
                'Pinkal (05-Jun-2020) -- End
                Exit Sub
            ElseIf CBool(Session("AllowLvApplicationApplyForBackDates")) = False AndAlso objPendingLeave._Startdate.Date > dtpEnddate.GetDate.Date Then

                'Pinkal (05-Jun-2020) -- Start
                'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
                'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 30, "Sorry, As per company setting you are not allow to set leave application end date to previous date."), Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 31, "Sorry, As per company setting you are not allow to set leave application end date to previous date."), Me)
                'Pinkal (05-Jun-2020) -- End
                Exit Sub
            End If
            'Pinkal (25-Mar-2019) -- End


            Dim intApproverID As Integer = -1
            Dim objLeaveFraction As New clsleaveday_fraction
            Dim dtFraction As DataTable = objLeaveFraction.GetList("List", objPendingLeave._Formunkid, True, -1, CInt(cboApprover.SelectedValue)).Tables(0)

            If dtFraction IsNot Nothing AndAlso dtFraction.Rows.Count <= 0 Then
                Dim objleaveapprover As New clsleaveapprover_master
                Dim objApproveLevel As New clsapproverlevel_master
                objleaveapprover._Approverunkid = objPendingLeave._Approvertranunkid
                objApproveLevel._Levelunkid = objleaveapprover._Levelunkid


                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objleaveapprover = Nothing
                'Pinkal (11-Sep-2020) -- End

                Dim mstrSearch As String = "lvpendingleave_tran.approverunkid <> " & objPendingLeave._Approverunkid & " AND  lvpendingleave_tran.formunkid = " & objPendingLeave._Formunkid
                Dim dsPedingList As DataSet = objPendingLeave.GetList("PendingProcess", _
                                                                      Session("Database_Name").ToString(), _
                                                                      CInt(Session("UserId")), _
                                                                      CInt(Session("Fin_year")), _
                                                                      CInt(Session("CompanyUnkId")), _
                                                                      eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString).Date, True, _
                                                                      CBool(Session("IsIncludeInactiveEmp")), True, mstrSearch)
                Dim dRow() As DataRow = dsPedingList.Tables(0).Select("priority < " & objApproveLevel._Priority)
                If dRow.Length > 0 Then
                    Dim mintPriority As Integer = CInt(dsPedingList.Tables(0).Compute("Max(priority)", "statusunkid=1"))
                    For i As Integer = 0 To dRow.Length - 1
                        If CInt(dRow(i)("priority")) = mintPriority AndAlso CInt(dRow(i)("statusunkid")) = 1 Then
                            intApproverID = CInt(dRow(i)("approverunkid"))
                            Exit For
                        End If
                    Next
                End If

                'Pinkal (11-Sep-2020) -- Start
                'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
                objApproveLevel = Nothing
                'Pinkal (11-Sep-2020) -- End

            Else
                intApproverID = CInt(cboApprover.SelectedValue)
            End If

            dtFraction = objLeaveFraction.GetList("List", objPendingLeave._Formunkid, True, -1, intApproverID).Tables(0)


            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            objLeaveFraction = Nothing
            'Pinkal (11-Sep-2020) -- End


            If objPendingLeave._Startdate.Date <> dtpStartdate.GetDate.Date Or objPendingLeave._Enddate.Date <> dtpEnddate.GetDate.Date Then
                dtFraction = CalculateDays(dtFraction)
            End If

            Dim drRow As DataRow() = dtFraction.Select("leavedate < '" & dtpStartdate.GetDate.Date & "' AND AUD <> 'D'")

            If drRow.Length > 0 Then
                For i As Integer = 0 To drRow.Length - 1
                    drRow(i)("AUD") = "D"
                Next
                dtFraction.AcceptChanges()
            End If

            drRow = Nothing
            drRow = dtFraction.Select("leavedate > '" & dtpEnddate.GetDate.Date & "' AND AUD <> 'D'")
            If drRow.Length > 0 Then
                For i As Integer = 0 To drRow.Length - 1
                    drRow(i)("AUD") = "D"
                Next
                dtFraction.AcceptChanges()
            End If

            For Each dr As DataRow In dtFraction.Rows
                If intApproverID <> objPendingLeave._Approverunkid AndAlso dr("AUD").ToString().Trim <> "D" Then
                    dr("AUD") = "A"
                Else
                    If dr("AUD").ToString().Trim = "" AndAlso dr.RowState = DataRowState.Modified Then
                        dr("AUD") = "U"
                    End If

                End If
            Next

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            'mdtFraction = dtFraction
            mdtFraction = dtFraction.Copy()
            'Pinkal (11-Sep-2020) -- End


            Dim mdblDays As Double = 0
            If dtFraction IsNot Nothing And dtFraction.Rows.Count > 0 Then
                If IsDBNull(dtFraction.Compute("SUM(dayfraction)", "formunkid = " & objPendingLeave._Formunkid & " AND AUD <> 'D' AND approverunkid = " & intApproverID)) = False Then
                    mdblDays = CDec(dtFraction.Compute("SUM(dayfraction)", "formunkid = " & objPendingLeave._Formunkid & " AND AUD <> 'D' AND approverunkid = " & intApproverID))
                End If
            End If
            objNoofDays.Text = "Total Days : " & mdblDays.ToString("#0.00")
            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA (REF-ID # 86).
            NoofDays = CDec(mdblDays)
            'Pinkal (29-Sep-2017) -- End

            'Pinkal (16-Dec-2016) -- Start
            'Enhancement - Working on IBA Leave Requirement as per Mr.Rutta's Guidance.
            GetAsOnDateAccrue()
            'Pinkal (16-Dec-2016) -- End

            'Pinkal (11-Sep-2020) -- Start
            'Optimzation Leave NMB:  Working on Optimzing Various modules for Garbage Colletion Issue.
            If dtFraction IsNot Nothing Then dtFraction.Clear()
            dtFraction = Nothing
            'Pinkal (11-Sep-2020) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#Region "Expense"

    Protected Sub dtpDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDate.TextChanged, cboSectorRoute.SelectedIndexChanged
        Try
            Dim iCostingId As Integer = -1 : Dim iAmount As Decimal = 0
            Dim objCosting As New clsExpenseCosting
            objCosting.GetDefaultCosting(CInt(cboSectorRoute.SelectedValue), iCostingId, iAmount, dtpDate.GetDate)
            txtCosting.Text = Format(CDec(iAmount), Session("fmtCurrency").ToString())
            txtCostingTag.Value = iCostingId.ToString
            txtUnitPrice.Text = Format(CDec(iAmount), Session("fmtCurrency").ToString())


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            Dim objExpnese As New clsExpense_Master
            objExpnese._Expenseunkid = CInt(cboExpense.SelectedValue)
            'Pinkal (25-Oct-2018) -- End

            If CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then
                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                'If objExpnese._Uomunkid = enExpUoM.UOM_QTY Then
                'txtCalimQty.Text = "0"
                'Else
                    txtCalimQty.Text = "1"
                'End If
                'Pinkal (04-Feb-2020) -- End
            ElseIf CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL OrElse CInt(cboExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                txtCalimQty.Text = "1"
            End If
            objCosting = Nothing

            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            If objExpnese._IsConsiderDependants AndAlso objExpnese._IsSecRouteMandatory Then
                txtCalimQty.Text = GetEmployeeDepedentCountForCR(CInt(cboClaimEmployee.SelectedValue)).ToString()
                mintEmpMaxCountDependentsForCR = CInt(txtCalimQty.Text)
            End If

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            'Pinkal (13-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES. [shall i allow to edit quantity in Claim approval as per yesterday conversation ? Matthew 26-Jun-2019, 11:44  Yes, allow. Only on approval only]
            'If objExpnese._IsConsiderDependants Then txtCalimQty.Enabled = False Else txtCalimQty.Enabled = True
            'Pinkal (13-Jun-2019) -- End

            txtUnitPrice.Enabled = objExpnese._IsUnitPriceEditable
            'Pinkal (25-May-2019) -- End

            objExpnese = Nothing
            'Pinkal (25-Oct-2018) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("dtpDate_TextChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

#End Region

#Region "Add For Approval Expense"

    Private Sub dtpApprovalClaimDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpApprovalClaimDate.TextChanged, cboApprovalSectorRoute.SelectedIndexChanged
        Try
            Dim iCostingId As Integer = -1 : Dim iAmount As Decimal = 0
            Dim objCosting As New clsExpenseCosting
            objCosting.GetDefaultCosting(CInt(cboApprovalSectorRoute.SelectedValue), iCostingId, iAmount, dtpDate.GetDate)
            txtApprovalCosting.Text = Format(CDec(iAmount), Session("fmtCurrency").ToString())
            txtApprovalCostingTag.Value = iCostingId.ToString
            txtApprovalUnitPrice.Text = Format(CDec(iAmount), Session("fmtCurrency").ToString())


            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            Dim objExpnese As New clsExpense_Master
            objExpnese._Expenseunkid = CInt(cboApprovalExpense.SelectedValue)
            'Pinkal (25-Oct-2018) -- End

            If CInt(cboApprovalExpCategory.SelectedValue) = enExpenseType.EXP_LEAVE Then

                'Pinkal (04-Feb-2020) -- Start
                'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                'If objExpnese._Uomunkid = enExpUoM.UOM_QTY Then
                'txtApprovalQty.Text = "0"
                'Else
                    txtApprovalQty.Text = "1"
                'End If
                'Pinkal (04-Feb-2020) -- End

            ElseIf CInt(cboApprovalExpCategory.SelectedValue) = enExpenseType.EXP_MEDICAL OrElse CInt(cboApprovalExpCategory.SelectedValue) = enExpenseType.EXP_MISCELLANEOUS Then
                txtApprovalQty.Text = "1"
            End If
            objCosting = Nothing

            'Pinkal (25-Oct-2018) -- Start
            'Enhancement - Implementing Claim & Request changes For NMB .
            If objExpnese._IsConsiderDependants AndAlso objExpnese._IsSecRouteMandatory Then
                txtApprovalQty.Text = GetEmployeeDepedentCountForCR(CInt(cboApprovalExpEmployee.SelectedValue)).ToString()
                mintEmpMaxCountDependentsForCR = CInt(txtApprovalQty.Text)
            End If

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.

            'Pinkal (13-Jun-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES. [shall i allow to edit quantity in Claim approval as per yesterday conversation ? Matthew 26-Jun-2019, 11:44  Yes, allow. Only on approval only]
            'If objExpnese._IsConsiderDependants Then txtApprovalQty.Enabled = False Else txtApprovalQty.Enabled = True
            'Pinkal (13-Jun-2019) -- End

            txtApprovalUnitPrice.Enabled = objExpnese._IsUnitPriceEditable
            'Pinkal (25-May-2019) -- End

            objExpnese = Nothing
            'Pinkal (25-Oct-2018) -- End


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#End Region

#Region "TextBox Event"

#Region "Expense"

    Protected Sub txtCalimQty_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCalimQty.TextChanged
        Try

            If txtCalimQty.Text.Trim.Length > 0 Then
                If IsNumeric(txtCalimQty.Text) = False Then

                    'Pinkal (04-Feb-2020) -- Start
                    'Enhancement Claim Request NMB  - Send Notification to Employee whenever the claim application is applied by manager.
                    'txtCalimQty.Text = "0"
                    txtCalimQty.Text = "1"
                    'Pinkal (04-Feb-2020) -- End
                    Exit Sub
                End If
                If CInt(txtCalimQty.Text) > 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                    If mblnIsLeaveEncashment = False Then
                        txtUnitPrice.Enabled = True
                        If txtUnitPrice.Text.Trim.Length <= 0 OrElse CDec(txtUnitPrice.Text) <= 0 Then
                            txtUnitPrice.Text = CDec(Format(CDec(txtCosting.Text), Session("fmtCurrency").ToString())).ToString
                            txtUnitPrice.Focus()
                        End If
                    Else
                        txtUnitPrice.Enabled = False
                    End If
                ElseIf CInt(txtCalimQty.Text) <= 0 AndAlso CInt(cboSectorRoute.SelectedValue) > 0 Then
                    If txtUnitPrice.Text.Trim.Length <= 0 OrElse CDec(txtUnitPrice.Text) <= 0 Then
                        txtUnitPrice.Text = CDec(Format(CDec(txtCosting.Text), Session("fmtCurrency").ToString)).ToString
                    End If
                End If
            Else
                txtUnitPrice.Enabled = False
            End If

            'Pinkal (18-Mar-2021) -- Start 
            'NMB Enhancmenet : AD Enhancement for NMB.
            Dim objExpense As New clsExpense_Master
            objExpense._Expenseunkid = CInt(cboExpense.SelectedValue)
            If objExpense._IsUnitPriceEditable = False Then txtUnitPrice.Enabled = objExpense._IsUnitPriceEditable
            objExpense = Nothing
            'Pinkal (18-Mar-2021) -- End

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtCalimQty_TextChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
        End Try
    End Sub

    Protected Sub txtUnitPrice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnitPrice.TextChanged
        Try
            If IsNumeric(txtUnitPrice.Text) = False Then
                'Pinkal (07-Feb-2020) -- Start
                'Enhancement Claim Request  - Claim Request Unit price changes given by Matthew for NMB .
                'txtUnitPrice.Text = "0"
                txtUnitPrice.Text = "1.00"
                'Pinkal (07-Feb-2020) -- End
                Exit Sub
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("txtUnitPrice_TextChanged:-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try

    End Sub

#End Region

#End Region


    'Pinkal (03-May-2019) -- Start
    'Enhancement - Working on Leave UAT Changes for NMB.

#Region "Checkbox Events"

    Protected Sub chkIncludeAllStaff_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIncludeAllStaff.CheckedChanged
        Try
            FillReliver()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

    'Pinkal (03-May-2019) -- End


    Private Sub SetLanguage()
        Try

            'Pinkal (13-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'Language._Object._LangId = CInt(Session("LangId"))
            'Pinkal (13-Mar-2019) -- End

            'Language.setLanguage(mstrModuleName)

            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.lblPageHeader.Text)
            Me.lblDetialHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),"gbProcessLeaveInfo", Me.lblDetialHeader.Text)

            Me.lblAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblAmount.ID, Me.lblAmount.Text)
            Me.lblDescription.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblDescription.ID, Me.lblDescription.Text)
            Me.lblLastYearAccrued.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblLastYearAccrued.ID, Me.lblLastYearAccrued.Text)
            Me.lblToDateAccrued.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblToDateAccrued.ID, Me.lblToDateAccrued.Text)
            Me.lblLastYearIssued.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblLastYearIssued.ID, Me.lblLastYearIssued.Text)
            Me.lblTotalIssuedToDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblTotalIssuedToDate.ID, Me.lblTotalIssuedToDate.Text)
            Me.lblBalance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblBalance.ID, Me.lblBalance.Text)
            Me.lblReportTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblReportTo.ID, Me.lblReportTo.Text)
            Me.lblDepartment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblDepartment.ID, Me.lblDepartment.Text)

            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'Me.lblStatus.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblStatus.ID, Me.lblStatus.Text)
            'Pinkal (25-May-2019) -- End
            Me.lblApprovalDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblApprovalDate.ID, Me.lblApprovalDate.Text)
            Me.lblApprover.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblApprover.ID, Me.lblApprover.Text)
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblRemark.ID, Me.lblRemark.Text)
            Me.lblAllowance.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblAllowance.ID, Me.lblAllowance.Text)
            Me.lblValue.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblValue.ID, Me.lblValue.Text)
            Me.lblFormNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblFormNo.ID, Me.lblFormNo.Text)
            Me.lblEnddate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblEnddate.ID, Me.lblEnddate.Text)
            Me.lblStartdate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblStartdate.ID, Me.lblStartdate.Text)
            Me.lnkShowLeaveForm.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkShowLeaveForm.ID, Me.lnkShowLeaveForm.Text)
            Me.lnkShowLeaveExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkShowLeaveExpense.ID, Me.lnkShowLeaveExpense.Text)
            Me.lnkLeaveDayCount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkLeaveDayCount.ID, Me.lnkLeaveDayCount.Text)


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            'Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.lblLeaveType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lblLeaveType.ID, Me.lblLeaveType.Text)
            Me.btnApprove.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnApprove.ID, Me.btnApprove.Text).Replace("&", "")
            Me.btnReject.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnReject.ID, Me.btnReject.Text).Replace("&", "")
            Me.btnReschedule.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.btnReschedule.ID, Me.btnReschedule.Text).Replace("&", "")
            'Pinkal (25-May-2019) -- End


            Me.Btnclose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.Btnclose.ID, Me.Btnclose.Text).Replace("&", "")
            Me.LblLeaveBF.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblLeaveBF.ID, Me.LblLeaveBF.Text)
            Me.LblTotalAdjustment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblTotalAdjustment.ID, Me.LblTotalAdjustment.Text)

            'Pinkal (15-Apr-2019) -- Start
            'Enhancement - Working on Leave & CR Changes for NMB.
            Me.LblBalanceAsonDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.LblBalanceAsonDate.ID, Me.LblBalanceAsonDate.Text)
            'Pinkal (15-Apr-2019) -- End



            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            Me.lnkShowScanDocuementList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),Me.lnkShowScanDocuementList.ID, Me.lnkShowScanDocuementList.Text)
            'Pinkal (25-May-2019) -- End



            'Pinkal (13-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'Language._Object._LangId = CInt(Session("LangId"))
            'Pinkal (13-Mar-2019) -- End

            'Language.setLanguage(mstrModuleNameClaim)


            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.lblExpenseHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, Me.lblExpenseHeader.Text)
            'Gajanan [17-Sep-2020] -- End

            Me.lblpopupHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),mstrModuleNameClaim, "Claims & Request")

            Me.lblDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.lblDate.ID, Me.lblDate.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.lblEmpAddEdit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),"lblEmployee", Me.lblEmpAddEdit.Text)
            Me.lblLeaveTypeAddEdit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),"lblLeaveType", Me.lblLeaveTypeAddEdit.Text)
            Me.lblClaimNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.lblClaimNo.ID, Me.lblClaimNo.Text)
            Me.lblExpCategory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.lblExpCategory.ID, Me.lblExpCategory.Text)
            Me.lblExpense.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.lblExpense.ID, Me.lblExpense.Text)
            Me.lblUoM.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.lblUoM.ID, Me.lblUoM.Text)
            Me.lblCosting.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.lblCosting.ID, Me.lblCosting.Text)
            Me.lblBalanceAddEdit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),"lblBalance", Me.lblBalanceAddEdit.Text)
            Me.lblQty.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.lblQty.ID, Me.lblQty.Text)
            Me.lblUnitPrice.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.lblUnitPrice.ID, Me.lblUnitPrice.Text)
            Me.lblGrandTotal.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.lblGrandTotal.ID, Me.lblGrandTotal.Text)
            Me.lblSector.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.lblSector.ID, Me.lblSector.Text)



            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.tbExpenseRemark.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.tbExpenseRemark.ID, Me.tbExpenseRemark.HeaderText)
            'Me.tbClaimRemark.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.tbClaimRemark.ID, Me.tbClaimRemark.HeaderText)
            'Me.lnkClaimDepedents.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),"lnkViewDependants", Me.lnkClaimDepedents.Text)
            Me.tbExpenseRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.tbExpenseRemark.ID, Me.tbExpenseRemark.Text)
            Me.tbClaimRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.tbClaimRemark.ID, Me.tbClaimRemark.Text)
            Me.lnkClaimDepedents.ToolTip = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),"lnkViewDependants", Me.lnkClaimDepedents.ToolTip)
            'Gajanan [17-Sep-2020] -- End
            
            Me.LblDomicileAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),Me.LblDomicileAdd.ID, Me.LblDomicileAdd.Text)



            'Pinkal (30-Apr-2018) - Start
            'Enhancement  [Ref # 224] calculate/display the total balance & Balance As on date.
            Me.lblClaimBalanceasondate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),"lblBalanceasondate", Me.lblClaimBalanceasondate.Text)
            Me.lblApprovalExpBalAsonDate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),"lblBalanceasondate", Me.lblApprovalExpBalAsonDate.Text)
            'Pinkal (30-Apr-2018) - End

            Me.btnClaimAdd.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),"btnAdd", Me.btnClaimAdd.Text).Replace("&", "")
            Me.btnClaimEdit.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),"btnEdit", Me.btnClaimEdit.Text).Replace("&", "")
            Me.btnClaimSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),"btnSave", Me.btnClaimSave.Text).Replace("&", "")
            Me.btnClaimClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),"btnClose", Me.btnClaimClose.Text).Replace("&", "")


            dgvData.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),dgvData.Columns(2).FooterText, dgvData.Columns(2).HeaderText)
            dgvData.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),dgvData.Columns(3).FooterText, dgvData.Columns(3).HeaderText)
            dgvData.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),dgvData.Columns(4).FooterText, dgvData.Columns(4).HeaderText)
            dgvData.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),dgvData.Columns(5).FooterText, dgvData.Columns(5).HeaderText)
            dgvData.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),dgvData.Columns(6).FooterText, dgvData.Columns(6).HeaderText)
            dgvData.Columns(7).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),dgvData.Columns(7).FooterText, dgvData.Columns(7).HeaderText)
            dgvData.Columns(8).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim,CInt(HttpContext.Current.Session("LangId")),dgvData.Columns(8).FooterText, dgvData.Columns(8).HeaderText)



            'Pinkal (13-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'Language._Object._LangId = CInt(Session("LangId"))
            'Pinkal (13-Mar-2019) -- End

            'Language.setLanguage(mstrModuleName1)

            Me.LblEmpDependentsList.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, Me.LblEmpDependentsList.Text)
            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.Label6.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, Me.Label6.Text)
            'Gajanan [17-Sep-2020] -- End
            Me.btnEmppnlEmpDepedentsClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),"btnClose", Me.btnEmppnlEmpDepedentsClose.Text).Replace("&", "")

            dgDepedent.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),dgDepedent.Columns(0).FooterText, dgDepedent.Columns(0).HeaderText)
            dgDepedent.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),dgDepedent.Columns(1).FooterText, dgDepedent.Columns(1).HeaderText)
            dgDepedent.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),dgDepedent.Columns(2).FooterText, dgDepedent.Columns(2).HeaderText)
            dgDepedent.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),dgDepedent.Columns(3).FooterText, dgDepedent.Columns(3).HeaderText)
            dgDepedent.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),dgDepedent.Columns(4).FooterText, dgDepedent.Columns(4).HeaderText)


            'Pinkal (13-Mar-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            'Language._Object._LangId = CInt(Session("LangId"))
            'Pinkal (13-Mar-2019) -- End

            'Language.setLanguage(mstrModuleName2)
            Me.LblLeaveFormText.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),mstrModuleName2, Me.LblLeaveFormText.Text)

            'Gajanan [17-Sep-2020] -- Start
            'New UI Change
            'Me.LblLeaveFormText1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),mstrModuleName2, Me.LblLeaveFormText1.Text)
            'Gajanan [17-Sep-2020] -- End

            Me.LblLeaveFormNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),"lblFormNo", Me.LblLeaveFormNo.Text)
            Me.lblLeaveFormEmpCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),Me.lblLeaveFormEmpCode.ID, Me.lblLeaveFormEmpCode.Text)
            Me.lblLeaveFormEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),"lblEmployee", Me.lblLeaveFormEmployee.Text)
            Me.lblLeaveFormLeaveType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),"lblLeaveType", Me.lblLeaveFormLeaveType.Text)
            Me.lblLeaveFormApplydate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),"lblApplyDate", Me.lblLeaveFormApplydate.Text)
            Me.lblLeaveFormStartdate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),"lblStartDate", Me.lblLeaveFormStartdate.Text)
            Me.lblLeaveFormReturndate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),"lblReturnDate", Me.lblLeaveFormReturndate.Text)
            Me.lblLeaveFormDayApply.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),"lblDays", Me.lblLeaveFormDayApply.Text)
            Me.lblLeaveAddress.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),Me.lblLeaveAddress.ID, Me.lblLeaveAddress.Text)
            Me.lblLeaveFormRemark.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),"lblRemarks", Me.lblLeaveFormRemark.Text)
            Me.btnLeaveFormOk.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),"btnClose", Me.btnLeaveFormOk.Text).Replace("&", "")
            'Hemant (30 Nov 2018) -- Start
            'Enhancement : Including Language Settings For Scan/Attachment Button
            btnSaveAttachment.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),Me.btnSaveAttachment.ID, Me.btnSaveAttachment.Text).Replace("&", "")
            btnAddFile.Value = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName2,CInt(HttpContext.Current.Session("LangId")),Me.btnSaveAttachment.ID, Me.btnSaveAttachment.Text).Replace("&", "")
            'Hemant (30 Nov 2018) -- End
        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 1, "Approver is compulsory information.Please Select Approver.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 3, "Remark cannot be blank. Remark is required information.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 4, "Please Select Proper Start Date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 5, "You can't approve this leave for this employee.Reason: Leave dates are falling in a period which is already closed.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 6, "You can't approve this leave for this employee.Reason: Process Payroll is already done for last date of period in which leave dates are falling.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 7, "You cannot change")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 8, " status to Pending status.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 9, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type. Are you sure you want to Issue this leave?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 10, "Issue Leave Limit has exceeded the Maximum Leave Limit for this leave type.You can't Issue this leave form.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 11, "You cannot approve this leave form.Reason : As these days are falling in holiday(s) and weekend.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 13, "Issued Leave Application form notification")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 14, "Sending Email(s) process is in progress from other module. Please wait for some time.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 19, "Payroll payment for this employee is already done for this tenure.If you press YES then payroll payment will differ from the amount already paid to this employee. Are you sure you want to Approve/Issue this leave ?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 20, "You cannot approve/issue this leave form.Reason : This Leave Limit has exceeded the Maximum Leave balance as on date Limit for this leave type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 21, "Sorry,You cannot view/edit expense for this leave type.Reason : Eligibility to view/edit for this leave type does not match with eligibility days set on selected leave type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 22, "Reliever is compulsory information.Please select reliever to approve/reject the leave application.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 23, "Sorry, you cannot approve/Issue for this leave type as it has been configured with setting for minimum")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 24, "Sorry, you cannot approve/Issue for this leave type as it has been configured with setting for maximum")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 25, "day(s) to be applied for this leave type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 26, "You cannot approve/issue this leave form.Reason : This Leave Limit has exceeded the Maximum Negative Limit for this leave type.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 28, "Please select proper")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 29, "should be between")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 30, "Sorry, As per company setting you are not allow to set leave application start date to previous date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 31, "Sorry, As per company setting you are not allow to set leave application end date to previous date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName, 32, "Sorry, you cannot approve/reject this leave application.Reason:As Per setting configured on leave type master you can not approve/reject for future date(s).")


            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 1, "Claim No. is mandatory information. Please select Claim No. to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 3, "Expense Category is mandatory information. Please select Expense Category to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 4, "Employee is mandatory information. Please select Employee to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 5, "Expense is mandatory information. Please select Expense to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 6, "Quantity is mandatory information. Please enter Quantity to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 8, "Please add atleast one expense in order to save.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 10, "Sorry, you cannot add same expense again in the below list.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 11, "Sorry, you cannot set quantity greater than balance set.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 12, "Leave Form")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 14, "Please Assign Leave Approver to this employee and also map Leave Approver to system user.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 15, "Please Map this employee's Leave Approver to system user.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 16, "Please Map this Leave type to this employee's Leave Approver(s).")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 17, "Please Assign Expense Approver to this employee and also map Expense Approver to system user.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 18, "Sorry, you cannot add this transaction. Reason : You cannot add  Leave Encashment or Other Expenses with each other they have to be added in separate Claim form.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 19, "Sorry, you cannot set amount greater than balance set.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 20, "Sector/Route is mandatory information. Please enter Sector/Route to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 22, "has mandatory document attachment. please attach document.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 24, "Sorry, you cannot set quantity greater than balance as on date set.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 25, "Sorry, you cannot set quantity greater than maximum total quantity which is defined in dependent age limit.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 26, "Do you wish to continue?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 27, "You have not set Unit price. 0 will be set to unit price for selected employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 28, "Cost Center is mandatory information. Please select Cost Center to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 29, "Sorry,you cannot apply for this expense.Reason : This expense claim amout is exceeded the budget amount.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 30, "Sorry, you cannot add this transaction. Reason : You cannot add HR Expense or Other Expenses with each other they have to be added in separate Claim form.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 39, "Are you sure you want to delete this attachment?")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 40, "GL Code is compulsory information for budget request validation.Please map GL Code with this current expense.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 41, "Cost Center Code is compulsory information for budget request validation.Please set Cost Center Code with this current employee/expense.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 42, "Selected information is already present for particular employee.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 43, "Sorry, Delete reason is Mendtory information, Please enter delete reason")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 44, "Sorry, Expense Delete process fail.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 45, "You have not set your expense remark.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 46, "Currency is mandatory information. Please select Currency to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 47, "Claim Remark is mandatory information. Please enter claim remark to continue.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 48, "Sorry,you cannot add expense in this claim application.Reason : As per the configuration setting you cannot add more than one expense in claim application.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 49, "Sorry, you cannot set quantity greater than maximum quantity which is defined in expense master.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleNameClaim, 50, "You cannot apply for this expense.Reason : You cannot apply for back date which is configured on expese master for this expense.")

        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>

   
End Class
