﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_AssignIssueUserList.aspx.vb"
    Inherits="Leave_wPg_AssignIssueUserList" Title="Assign Issue User To Employee List" %>

<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/GetCombolist.ascx" TagName="ComboList" TagPrefix="uc4" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/DeleteReason.ascx" TagName="DeleteReason" TagPrefix="ucDel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Assign Issue User To Employee List"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblCaption" runat="server" Text="Assign Issue User To Employee List"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblIssueUser" runat="server" Text="Issue User" CssClass="form-label" />
                                        <div class="form-group">
                                            <uc4:ComboList ID="drpIssueUser" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="LblEmployee" runat="server" Text="Employee" CssClass="form-label" />
                                        <div class="form-group">
                                            <uc4:ComboList ID="drpEmployee" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnNew" runat="server" Text="New" CssClass="btn btn-primary" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="height: 400px">
                                            <asp:DataGrid ID="GvIssueUserList" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                                CssClass="table table-hover table-bordered">
                                                <Columns>
                                                    <asp:TemplateColumn HeaderStyle-Width="30px" ItemStyle-Width="30px" HeaderStyle-HorizontalAlign="Center"
                                                        FooterText="btnDelete">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <span class="gridiconbc">
                                                                <asp:LinkButton ID="ImgDelete" runat="server" CommandName="Delete" ToolTip="Delete">
                                                                      <i class="fas fa-trash text-danger"></i>
                                                                </asp:LinkButton>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderStyle-HorizontalAlign="Center">
                                                        <ItemStyle HorizontalAlign="Center" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkMigration" runat="server" ToolTip="Migration" 
                                                                CommandName="Migration">
                                                             <i class="fas fa-sign-out-alt"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn DataField="issueuser" HeaderText="Issue User" ReadOnly="true" FooterText="colhIssueUser" />
                                                    <asp:BoundColumn DataField="employee" HeaderText="Employee" ReadOnly="true" FooterText="colhEmployeeName" />
                                                    <asp:BoundColumn DataField="department" HeaderText="Department" ReadOnly="true" FooterText="colhDepartment" />
                                                    <asp:BoundColumn DataField="section" HeaderText="Section" ReadOnly="true" FooterText="colhSection" />
                                                    <asp:BoundColumn DataField="job" HeaderText="Job" ReadOnly="true" FooterText="colhJob" />
                                                    <asp:BoundColumn DataField="employeeunkid" HeaderText="Employeeunkid" ReadOnly="true"
                                                        Visible="false" />
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="panel-primary">
                        <div class="panel-heading">
                            
                        </div>
                        <div class="panel-body">
                            <div id="FilterCriteria" class="panel-default">
                                <div id="FilterCriteriaTitle" class="panel-heading-default">
                                    <div style="float: left;">
                                        
                                    </div>
                                </div>
                                <div id="FilterCriteriaBody" class="panel-body-default" style="text-align: left">
                                    <table style="width: 80%;">
                                        <tr style="width: 100%">
                                            <td style="width: 15%">
                                              
                                            </td>
                                            <td style="width: 35%">
                                                
                                            </td>
                                            <td style="width: 15%">
                                                
                                            </td>
                                            <td style="width: 35%">
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <div class="btn-default">
                                        
                                    </div>
                                </div>
                            </div>
                            <asp:Panel ID="pnl_gvView" runat="server" Height="400px" ScrollBars="Auto">
                               
                            </asp:Panel>
                        </div>
                    </div>--%>
                <ucDel:DeleteReason ID="popup1" runat="server" Title="Are you Sure You Want To delete?:" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
