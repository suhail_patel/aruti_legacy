﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Data.SqlClient
Imports System.Collections
Imports eZeeCommonLib.clsDataOperation
Imports System.HttpStyleUriParser
Imports Aruti.Data

#End Region

Partial Class LeavePlanner
    Inherits Basepage

#Region "Variable Declaration"

    'Dim msql As String
    'Dim clsDataOpr As New eZeeCommonLib.clsDataOperation
    'Dim objGlobalAccess As New GlobalAccess
    'Dim i As Integer
    ' Dim dsLeaveForm As New DataSet
    'Dim clsemp As New clsEmployee_Master
    ' Dim dtEmp As DataTable

    Dim ds As DataSet
    Dim Flag As String
    Dim clsuser As New User
    Dim DisplayMessage As New CommonCodes
    Dim strSearching As String = ""
    Dim clsLeavePlanner As New clsleaveplanner
    Private _prfilmode As enAnalysisRef
    Private ReadOnly mstrModuleName As String = "frmleaveplannerList"
    Private ReadOnly mstrModuleName1 As String = "frmleaveplanner_AddEdit"

#End Region

#Region "Property"

    Public Property filtermode() As enAnalysisRef
        Get
            Return _prfilmode
        End Get
        Set(ByVal value As enAnalysisRef)
            _prfilmode = value
        End Set
    End Property

#End Region

#Region "Page Events"

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ViewState.Add("Flag", Flag)
        Me.ViewState.Add("Filtermode", Me.filtermode)

        'SHANI [01 FEB 2015]-START
        'Enhancement - REDESIGN SELF SERVICE.
        'ToolbarEntry1.VisibleImageSaprator1(False)
        'ToolbarEntry1.VisibleImageSaprator2(False)
        'ToolbarEntry1.VisibleImageSaprator3(False)
        'ToolbarEntry1.VisibleImageSaprator4(False)
        'ToolbarEntry1.VisibleNewButton(False)
        'ToolbarEntry1.VisibleSaveButton(False)
        'ToolbarEntry1.VisibleCancelButton(False)
        'SHANI [01 FEB 2015]--END

        'Me.ViewState.Add("EmployeeList", dtEmp)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT

            'Anjan [20 February 2016] -- Start
            'ENHANCEMENT : Changed parameter of demo license with session parameter. 
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) = False Then
                'Anjan [20 February 2016] -- End

                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If


            If (Page.IsPostBack = False) Then
            SetLanguage()
                clsuser = CType(Session("clsuser"), User)
                FillCombo()
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    rbtfiltertype.Items(0).Selected = False
                    rbtfiltertype.Items(1).Enabled = False
                    rbtfiltertype.Items(2).Enabled = False
                    rbtfiltertype.Items(3).Enabled = False
                    rbtfiltertype.AutoPostBack = False
                    chkgrp.Enabled = False

                    'BindChkListByEmpid(CInt(Session("Employeeunkid")))
                    BindchkbxListEmp(CInt(Session("Employeeunkid")))
                    Dim objEmployee As New clsEmployee_Master
                    objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = CInt(Session("Employeeunkid"))
                    If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) > 0 Then
                        drpGender.Enabled = False
                    End If
                    drpGender.SelectedValue = objEmployee._Gender.ToString()

                    chkbxlistemlployee.Items(0).Selected = True
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.Employeeunkid
                    Me.ViewState("Empunkid") = CInt(Session("Employeeunkid"))
                    'Sohail (30 Mar 2015) -- End
                Else
                    'Sohail (30 Mar 2015) -- Start
                    'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
                    'Me.ViewState("Empunkid") = clsuser.UserID
                    Me.ViewState("Empunkid") = Session("UserId")
                    'Sohail (30 Mar 2015) -- End
                End If

                txtleaveplannerunkid.Visible = False

                dtStartDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtEnddate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                If (Request.QueryString("Planner_Id") <> "") Then

                    'SHANI [01 FEB 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Form)
                    'ToolbarEntry1.VisibleSaveButton(False)
                    'SHANI [01 FEB 2015]--END
                    pnldispGrid.Visible = False
                    btnSave.Visible = True
                    pnlLeaveEntry.Visible = True
                Else
                    'SHANI [01 FEB 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
                    'SHANI [01 FEB 2015]--END
                    pnlLeaveEntry.Visible = False
                    pnldispGrid.Visible = True
                    btnSave.Visible = False
                End If

            Else

                Flag = CType(Me.ViewState("Flag"), String)
                Me.filtermode = CType(Me.ViewState("Filtermode"), enAnalysisRef)
                'ds = CType(Me.ViewState("EmployeeList"), DataSet)
            End If

            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                dgView.Columns(2).Visible = False
                dgView.Columns(3).Visible = False
            Else
                dgView.Columns(2).Visible = True
                dgView.Columns(3).Visible = True
                btnNew.Visible = CBool(Session("AddPlanLeave"))
                'Pinkal (12-Feb-2015) -- Start
                'Enhancement - CHANGING ONE LOGIN FOR MSS AND ESS.
                dgView.Columns(0).Visible = CBool(Session("EditPlanLeave"))
                dgView.Columns(1).Visible = CBool(Session("DeletePlanLeave"))
                'Pinkal (12-Feb-2015) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    Protected Sub Page_PreRenderComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRenderComplete
        Try
            If Not IsPostBack Then
                RedirectedFromViewer()
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Public Function RedirectedFromViewer() As Boolean
        Try
            If (Request.QueryString("Planner_Id") <> "") Then
                Dim mintLeavePlannerunkid As Integer = -1
                mintLeavePlannerunkid = CInt(b64decode(Request.QueryString("Planner_Id")))
                'txtleaveplannerunkid.Text = mintLeavePlannerunkid
                Me.ViewState.Add("LeavePlannerunkid", mintLeavePlannerunkid)
                clsLeavePlanner._Leaveplannerunkid = mintLeavePlannerunkid
                GetLoginEmployeeData()
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Public Function b64decode(ByVal StrDecode As String) As String
        Dim decodedString As String = ""
        Try
            decodedString = System.Text.ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(StrDecode))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return decodedString
    End Function

    Public Sub BindchkbxListEmp(Optional ByVal EmployeeID As Integer = -1)
        Try
            Dim dtEmp As DataTable = Nothing
            chkbxlistemlployee.Items.Clear()
            If Session("EmployeeList") Is Nothing Then
                Dim clsemp As New clsEmployee_Master

                Dim blnApplyFilter As Boolean = True
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    EmployeeID = CInt(Session("Employeeunkid"))
                    blnApplyFilter = False
                End If


                'Pinkal (02-May-2017) -- Start
                'Enhancement - Working on Leave Enhancement for HJF.
                'ds = clsemp.GetList(Session("Database_Name").ToString(), _
                '                    CInt(Session("UserId")), _
                '                    CInt(Session("Fin_year")), _
                '                    CInt(Session("CompanyUnkId")), _
                '                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                '                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                '                    Session("UserAccessModeSetting").ToString(), True, _
                '                    True, "EmployeeList", CBool(Session("ShowFirstAppointmentDate")), EmployeeID, , , False, blnApplyFilter)

                ds = clsemp.GetList(Session("Database_Name").ToString(), _
                                    CInt(Session("UserId")), _
                                    CInt(Session("Fin_year")), _
                                    CInt(Session("CompanyUnkId")), _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                    CStr(IIf(EmployeeID <= 0, Session("UserAccessModeSetting").ToString(), "0")), True, _
                                    True, "EmployeeList", CBool(Session("ShowFirstAppointmentDate")), EmployeeID, False, "", False, blnApplyFilter)

                'Pinkal (02-May-2017) -- End



                Session.Add("EmployeeList", ds)
            Else
                ds = CType(Session("Employeelist"), DataSet)
            End If

            Dim strSearching As String = ""
            If CInt(drpGender.SelectedValue) > 0 Then
                strSearching = " gender = " & CInt(drpGender.SelectedValue)
            End If

            If EmployeeID > 0 Then
                dtEmp = New DataView(ds.Tables(0), "employeeunkid=" & EmployeeID, "", DataViewRowState.CurrentRows).ToTable
                chkbxlistemlployee.DataSource = dtEmp
            Else
                dtEmp = New DataView(ds.Tables(0), strSearching, "", DataViewRowState.CurrentRows).ToTable
                chkbxlistemlployee.DataSource = dtEmp
            End If

            chkbxlistemlployee.DataTextField = "name"
            chkbxlistemlployee.DataValueField = "employeeunkid"
            chkbxlistemlployee.DataBind()

            If EmployeeID > 0 Then
                chkbxlistemlployee.Items(0).Selected = True
                chkbxlistemlployee_TextChanged(New Object, New EventArgs())
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub EnableFilterType()
        Try
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                rbtfiltertype.Items(0).Enabled = True
                rbtfiltertype.Items(1).Enabled = True
                rbtfiltertype.Items(2).Enabled = True
                rbtfiltertype.Items(3).Enabled = True
                rbtfiltertype.Items(0).Selected = False
                rbtfiltertype.Items(1).Selected = False
                rbtfiltertype.Items(2).Selected = False
                rbtfiltertype.Items(3).Selected = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Function Entryvalid() As Boolean
        Dim mValid As Boolean
        mValid = True
        Try
            If chkbxlistemlployee.SelectedItem Is Nothing Then
                'Language.setLanguage(mstrModuleName1)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Employee is compulsory information.Please Check atleast One Employee from the List."), Me)
                mValid = False
                Exit Function
            End If
            If dtStartDate.GetDate < CDate(Session("fin_startdate")) Or dtStartDate.GetDate > CDate(Session("fin_enddate")) Then
                DisplayMessage.DisplayMessage("Start Date should be greater or less than  Fin.year date " & CType(Session("fin_startdate"), Date) & " " & "-" & " " & CType(Session("fin_enddate"), Date) & " !!!", Me)
                dtStartDate.Focus()
                mValid = False
                Exit Function
            End If
            If dtEnddate.GetDate < CDate(Session("fin_startdate")) Or dtEnddate.GetDate > CDate(Session("fin_enddate")) Then
                DisplayMessage.DisplayMessage("End Date should be greater or less than  Fin.year date " & CType(Session("fin_startdate"), Date) & " " & "-" & " " & CType(Session("fin_enddate"), Date) & "  !!!", Me)
                dtEnddate.Focus()
                mValid = False
                Exit Function

            End If

            If dtEnddate.GetDate < dtStartDate.GetDate Then
                DisplayMessage.DisplayMessage("End Date should be greater than Start Date.", Me)
                dtEnddate.Focus()
                mValid = False
                Exit Function

            End If
            Return mValid
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Function

    Public Function b64encode(ByVal StrEncode As String) As String
        Dim encodedString As String = ""
        Try
            encodedString = (Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes(StrEncode)))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return encodedString
    End Function

    Public Sub Binddata()
        Try
            Dim dtLeaveForm As New DataTable

            If CInt(Session("LoginBy")) = Global.User.en_loginby.User Then
                If CBool(Session("ViewLeavePlannerList")) = False Then Exit Sub
            End If

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsLeaveForm As DataSet = clsLeavePlanner.GetList("Leaveplanner", True, False, Session("EmployeeAsOnDate"), Session("AccessLevelFilterString"))


            'Pinkal (06-Jan-2016) -- Start
            'Enhancement - Working on Changes in SS for Leave Module.

            'Dim dsLeaveForm As DataSet = clsLeavePlanner.GetList("Leaveplanner", _
            '                                                     Session("Database_Name"), _
            '                                                     Session("UserId"), _
            '                                                     Session("Fin_year"), _
            '                                                     Session("CompanyUnkId"), _
            '                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                                     Session("UserAccessModeSetting"), True, _
            '                                                     Session("IsIncludeInactiveEmp"), True, False)


            'Pinkal (22-Mar-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.

            Dim blnApplyFilter As Boolean = True
            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                blnApplyFilter = False
            End If

            Dim dsLeaveForm As DataSet = clsLeavePlanner.GetList("Leaveplanner", _
                                                                 Session("Database_Name").ToString(), _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                  Session("UserAccessModeSetting").ToString(), True, True, blnApplyFilter, True)

            'Pinkal (22-Mar-2016) -- End

            'Pinkal (06-Jan-2016) -- End

            If CInt(ddlfilemloyee.SelectedValue) > 0 Then
                strSearching += "and employeeunkid =" & CInt(ddlfilemloyee.SelectedValue) & " "
            End If

            If CInt(ddlfilleavetype.SelectedValue) > 0 Then
                strSearching += "AND leavetypeunkid =" & CInt(ddlfilleavetype.SelectedValue) & " "
            End If

            If dtfilstdate.IsNull = False Then
                strSearching += "AND startdate >='" & eZeeDate.convertDate(dtfilstdate.GetDate) & "' "
            End If

            If dtfilenddate.IsNull = False Then
                strSearching += "AND stopdate <='" & eZeeDate.convertDate(dtfilenddate.GetDate) & "' "
            End If

            If strSearching.Length > 0 Then
                strSearching = strSearching.Substring(3)
                dtLeaveForm = New DataView(dsLeaveForm.Tables("LeavePlanner"), strSearching, "", DataViewRowState.CurrentRows).ToTable
            Else
                dtLeaveForm = dsLeaveForm.Tables("LeavePlanner")
            End If

            If (Not dtLeaveForm Is Nothing) Then

                dgView.DataSource = dtLeaveForm
                dgView.DataKeyField = "leaveplannerunkid"
                dgView.DataBind()

            Else
                DisplayMessage.DisplayMessage("Error In Binddata method  ", Me)
                Exit Sub
            End If

        Catch ex As Exception
            If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." Then
                dgView.CurrentPageIndex = 0
                dgView.DataBind()
            Else
                Throw ex
                DisplayMessage.DisplayError(ex, Me)
            End If
        End Try
    End Sub

    Public Sub bindChklistGroup()
        Try
            Dim ds As New DataSet
            chkbxlistemlployee.Items.Clear()

            If Me.filtermode = enAnalysisRef.DeptInDeptGroup Then
                Dim clsdept As New clsDepartment
                chklistgrp.DataSource = clsdept.GetList("Department", True)
                chklistgrp.DataTextField = "name"
                chklistgrp.DataValueField = "departmentunkid"
                chklistgrp.DataBind()

            ElseIf Me.filtermode = enAnalysisRef.Section Then
                Dim clssect As New clsSections
                chklistgrp.DataSource = clssect.GetList("Sections", True)
                chklistgrp.DataTextField = "name"
                chklistgrp.DataValueField = "sectionunkid"
                chklistgrp.DataBind()

            ElseIf Me.filtermode = enAnalysisRef.JobInJobGroup Then
                Dim clsjob As New clsJobs
                chklistgrp.DataSource = clsjob.GetList("Jobs", True)
                chklistgrp.DataTextField = "jobName"
                chklistgrp.DataValueField = "jobunkid"
                chklistgrp.DataBind()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Public Function FilterEmployee(ByVal listb As ListItemCollection, ByVal primaryfield As String) As DataTable
        Dim dtEmp As DataTable = Nothing
        Try
            Dim strfil As String = ""
            For i As Integer = 0 To CInt(listb.Count) - 1
                strfil += listb(i).Value + ","
            Next

            If (strfil.Length > 0) Then
                strfil = strfil.Substring(0, strfil.Length - 1)
            End If
            If Session("EmployeeList") Is Nothing Then
                Dim clsemp As New clsEmployee_Master

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If CBool(Session("IsIncludeInactiveEmp")) = False Then
                '    ds = clsemp.GetList("EmployeeList", False, , eZeeDate.convertDate(Session("EmployeeAsOnDate")), eZeeDate.convertDate(Session("EmployeeAsOnDate")), -1, Session("AccessLevelFilterString"))
                'Else
                '    ds = clsemp.GetList("EmployeeList", False, , , , -1, Session("AccessLevelFilterString"))
                'End If


                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.

                'ds = clsemp.GetList(Session("Database_Name"), Session("UserId"), _
                '                            Session("Fin_year"), _
                '                            Session("CompanyUnkId"), _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                            eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                            Session("UserAccessModeSetting"), True, _
                '                            Session("IsIncludeInactiveEmp"), _
                '                            "EmployeeList", _
                '                            Session("ShowFirstAppointmentDate"), , , , , )


                ds = clsemp.GetList(Session("Database_Name").ToString, CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                            Session("UserAccessModeSetting").ToString(), True, True, "EmployeeList", _
                                            CBool(Session("ShowFirstAppointmentDate")), , , , False)

                'Pinkal (06-Jan-2016) -- End

                'Shani(24-Aug-2015) -- End
                Session.Add("EmployeeList", ds)
            Else
                ds = CType(Session("EmployeeList"), DataSet)
            End If

            Dim strSearching As String = ""
            If CInt(drpGender.SelectedValue) > 0 Then
                strSearching = " AND gender = " & CInt(drpGender.SelectedValue)
            End If

            dtEmp = New DataView(ds.Tables(0), primaryfield + " in (" + strfil + " ) " & strSearching, "", DataViewRowState.CurrentRows).ToTable

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        Return dtEmp
    End Function

    Public Sub fillchkboxlistfilter(ByVal list As ListItemCollection)
        Try
            chkbxlistemlployee.Items.Clear()

            If Not list.Count = 0 Then
                If (Me.filtermode = enAnalysisRef.DeptInDeptGroup) Then
                    chkbxlistemlployee.DataSource = FilterEmployee(list, "departmentunkid")

                ElseIf (Me.filtermode = enAnalysisRef.Section) Then
                    chkbxlistemlployee.DataSource = FilterEmployee(list, "sectionunkid")

                ElseIf (Me.filtermode = enAnalysisRef.JobInJobGroup) Then
                    chkbxlistemlployee.DataSource = FilterEmployee(list, "jobunkid")
                End If

                chkbxlistemlployee.DataTextField = "name"
                chkbxlistemlployee.DataValueField = "employeeunkid"
                chkbxlistemlployee.DataBind()
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub GetSelectedList()
        Try
            Dim listitems As New ListItemCollection
            For i As Integer = 0 To chklistgrp.Items.Count - 1
                If (chklistgrp.Items(i).Selected) Then
                    listitems.Add(chklistgrp.Items(i).Value)
                End If
            Next

            fillchkboxlistfilter(listitems)

            If chklistgrp.Items.Count <= 0 Then
                chkgrp.Enabled = False
            Else
                chkgrp.Enabled = True
            End If

            If chkbxlistemlployee.Items.Count <= 0 Then
                chkEmp.Enabled = False
            Else
                chkEmp.Enabled = True
            End If
            chkEmp.Checked = False

            If listitems.Count = chklistgrp.Items.Count Then
                chkgrp.Checked = True
            Else
                chkgrp.Checked = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub ClearCheckboxlist()
        Try
        chkbxlistemlployee.Items.Clear()
        chklistgrp.Items.Clear()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub BlankObjects()
        Try
            txtRemarks.Text = ""

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'txtreason.Text = ""
            'SHANI [01 FEB 2015]--END

            txtleaveplannerunkid.Text = ""

            leavetype.SelectedIndex = 0
            ddlfilemloyee.SelectedIndex = 0
            ddlfilleavetype.SelectedIndex = 0

            dtfilstdate.SetDate = Nothing
            dtfilenddate.SetDate = Nothing
            dtStartDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
            dtEnddate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date

            chkEmp.Checked = False
            chkEmp.Enabled = False
            chkgrp.Checked = False
            chkgrp.Enabled = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub SetValue()
        Try
            With clsLeavePlanner
                ._Employeeunkid = CInt(Val(chkbxlistemlployee.SelectedValue))

                If dtStartDate.IsNull = True Then
                    ._Startdate = Nothing
                Else
                    ._Startdate = dtStartDate.GetDate
                End If

                If dtEnddate.IsNull = True Then
                    ._Stopdate = Nothing
                Else
                    ._Stopdate = dtEnddate.GetDate
                End If

                ._Remarks = txtRemarks.Text
                ._Leavetypeunkid = CInt(leavetype.SelectedValue)
                ._Analysisrefid = Me.filtermode
            End With
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayError(ex, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Public Sub EntrySave()
        Try
            Dim dsplanner As New DataSet
            Dim clsEmpMaster As New clsEmployee_Master

            If (Not Entryvalid()) Then
                Exit Sub
            End If

            With clsLeavePlanner

                If (CInt(Session("loginBy")) = Global.User.en_loginby.Employee) Then
                    ._LoginEmployeeunkid = CInt(Session("Employeeunkid"))   'Me.ViewState("Empunkid")
                Else
                    ._Userunkid = CInt(Session("UserId"))     'Me.ViewState("Empunkid")
                End If

                '    If ToolbarEntry1.EntryMode = Controls_ToolBarEntry.enEntryMode.Add Then
                If Me.ViewState("LeavePlannerunkid") Is Nothing Or CInt(Me.ViewState("LeavePlannerunkid")) = 0 Then

                    SetValue()
                    For i As Integer = 0 To chkbxlistemlployee.Items.Count - 1
                        If (chkbxlistemlployee.Items(i).Selected) Then

                            'Pinkal (06-Jan-2016) -- Start
                            'Enhancement - Working on Changes in SS for Leave Module.

                            'dsplanner.Merge(clsEmpMaster.GetEmployeeList(Session("Database_Name"), _
                            '                                    Session("UserId"), _
                            '                                    Session("Fin_year"), _
                            '                                    Session("CompanyUnkId"), _
                            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                            '                                    Session("UserAccessModeSetting"), True, _
                            '                                    Session("IsIncludeInactiveEmp"), "Employee", False, _
                            '                                    CInt(chkbxlistemlployee.Items(i).Value)))

                            'Pinkal (22-Mar-2016) -- Start
                            'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
                            Dim blnApplyFilter As Boolean = True
                            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                                blnApplyFilter = False
                            End If
                            'Pinkal (22-Mar-2016) -- End


                            dsplanner.Merge(clsEmpMaster.GetEmployeeList(Session("Database_Name").ToString(), _
                                                            CInt(Session("UserId")), _
                                                            CInt(Session("Fin_year")), _
                                                            CInt(Session("CompanyUnkId")), _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                            Session("UserAccessModeSetting").ToString(), True, _
                                                            True, "Employee", False, CInt(chkbxlistemlployee.Items(i).Value), , , , , , , , , , , , , , , , blnApplyFilter))

                            'Pinkal (06-Jan-2016) -- End

                        End If
                    Next


                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    '.Insert(dsplanner)
                    .Insert(dsplanner, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date)
                    'Shani(20-Nov-2015) -- End


                    If ._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage("Entry Not Saved Due To " & ._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage("Entry Saved Successfully !!!!!", Me)
                        BlankObjects()
                        If (CInt(Session("loginBy")) = Global.User.en_loginby.User) Then
                            ClearCheckboxlist()
                            EnableFilterType()
                        End If
                    End If

                Else
                    ._Leaveplannerunkid = CInt(Me.ViewState("LeavePlannerunkid"))
                    SetValue()

                    'Shani(20-Nov-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    '.Update()
                    .Update(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date)
                    'Shani(20-Nov-2015) -- End


                    If ._Message.Length > 0 Then
                        DisplayMessage.DisplayMessage("Entry Not Updated Due To " & ._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage("Entry Updated Successfully !!!!!", Me)
                        If (Request.QueryString("Planner_Id") <> "") Then
                            If CType(Me.ViewState("LeavePlannerunkid"), Integer) > 0 Then
                                Session("RedirectFromPlanner") = True
                                Response.Redirect("~/Leave/LeavePLannerViewer.aspx?Planer_Id=" & b64encode(CType(Me.ViewState("LeavePlannerunkid"), Integer).ToString), False)
                            End If
                        End If
                        BlankObjects()
                        EnableFilterType()
                        If (CInt(Session("loginBy")) = Global.User.en_loginby.User) Then
                            ClearCheckboxlist()
                        End If
                    End If
                End If
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Public Sub GetLoginEmployeeData()
        Try
            chkgrp.Enabled = False
            chkEmp.Enabled = False
            chkEmp.Checked = True
            chkbxlistemlployee.Enabled = False

            Dim objEmployee As New clsEmployee_Master
            objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = clsLeavePlanner._Employeeunkid
            If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) > 0 Then
                drpGender.Enabled = False
            End If
            drpGender.SelectedValue = objEmployee._Gender.ToString()

            'BindChkListByEmpid(clsLeavePlanner._Employeeunkid)
            BindchkbxListEmp(clsLeavePlanner._Employeeunkid)

            dtStartDate.SetDate = clsLeavePlanner._Startdate
            dtEnddate.SetDate = clsLeavePlanner._Stopdate
            leavetype.SelectedValue = clsLeavePlanner._Leavetypeunkid.ToString()
            Me.ViewState("Filtermode") = enAnalysisRef.Employee
            txtRemarks.Text = clsLeavePlanner._Remarks

            rbtfiltertype.Items(0).Selected = True
            rbtfiltertype.Items(0).Enabled = True
            rbtfiltertype.Items(1).Enabled = False
            rbtfiltertype.Items(2).Enabled = False
            rbtfiltertype.Items(3).Enabled = False
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.EntryMode = Controls_ToolBarEntry.EN_ButtonMode.Update
            'ToolbarEntry1.SetButtons()
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Form)

            'ToolbarEntry1.VisibleNewButton(False)
            'ToolbarEntry1.VisibleModeGridButton(False)
            'ToolbarEntry1.VisibleCancelButton(False)
            'ToolbarEntry1.VisibleImageSaprator2(False)
            'ToolbarEntry1.VisibleImageSaprator3(False)
            'ToolbarEntry1.VisibleImageSaprator4(False)
            'SHANI [01 FEB 2015]--END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FetchData(ByVal DS As DataTable)
        Try
            ClearCheckboxlist()
            With DS
                'txtleaveplannerunkid.Text = .Rows(0)("leaveplannerunkid")
                Dim employee As String = .Rows(0)("employeeunkid").ToString()

                Dim objEmployee As New clsEmployee_Master
                objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) = CInt(employee)
                If objEmployee._Employeeunkid(eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date) > 0 Then
                    drpGender.Enabled = False
                End If
                drpGender.SelectedValue = objEmployee._Gender.ToString

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    'BindChkListByEmpid(CType(employee, Integer))
                    BindchkbxListEmp(CType(employee, Integer))
                Else
                    'BindChkListByEmpid(CInt(Session("Employeeunkid")))
                    BindchkbxListEmp(CInt(Session("Employeeunkid")))
                End If

                leavetype.SelectedValue = .Rows(0)("leavetypeunkid").ToString()
                dtStartDate.SetDate = eZeeDate.convertDate(.Rows(0)("startdate").ToString).Date
                dtEnddate.SetDate = eZeeDate.convertDate(.Rows(0)("stopdate").ToString).Date
                txtRemarks.Text = .Rows(0)("remarks").ToString
                chkEmp.Checked = True
                chkgrp.Enabled = False
                Me.filtermode = enAnalysisRef.Employee
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Sub FillCombo()
        Try
            Dim objMaster As New clsMasterData
            Dim dsFill As DataSet = objMaster.getGenderList("Gender", True)
            drpGender.DataTextField = "Name"
            drpGender.DataValueField = "id"
            drpGender.DataSource = dsFill.Tables(0)
            drpGender.DataBind()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

#Region "Button Events"

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Me.ViewState("LeavePlannerunkid") = Nothing

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Form)
            'SHANI [01 FEB 2015]--END
            pnlLeaveEntry.Visible = True
            pnldispGrid.Visible = False
            btnSave.Visible = True

            Blank_ModuleName()
            clsCommonATLog._WebFormName = "frmleaveplanner_AddEdit"
            StrModuleName2 = "mnuLeaveInformation"
            clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
            clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()


            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
            Else
                clsCommonATLog._LoginEmployeeUnkid = -1
            End If


            If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                lblGender.Visible = False
                drpGender.Visible = False
                chkEmp.Visible = False
                chkgrp.Visible = False
                pnlFirstTable.Visible = False
                leavetype.SelectedIndex = 0
                dtStartDate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtEnddate.SetDate = ConfigParameter._Object._CurrentDateAndTime.Date
                txtRemarks.Text = ""
            End If
            SetLanguage()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnNew_Click :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            EntrySave()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If CInt(ddlfilemloyee.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please Select Employee to Continue", Me)
                Exit Sub
            End If

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.EntryMode = Controls_ToolBarEntry.enEntryMode.grid
            'SHANI [01 FEB 2015]--END
            Binddata()
            pnldispGrid.Visible = True
            pnlLeaveEntry.Visible = False
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ButDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popup1.buttonDelReasonYes_Click
        'SHANI [01 FEB 2015]--END
        Try

            Dim clsLeavePlanner As New clsleaveplanner

            If (popup1.Reason.Trim = "") Then 'SHANI [01 FEB 2015]--If (txtreason.Text = "") Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Please Enter void reason.", Me)
                'Sohail (23 Mar 2019) -- End
                Exit Sub
            End If

            If Not chkbxlistemlployee.SelectedValue Is Nothing Then

                Blank_ModuleName()
                clsCommonATLog._WebFormName = "frmleaveplannerList"
                StrModuleName2 = "mnuLeaveInformation"
                clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
                clsCommonATLog._WebHostName = Session("HOST_NAME").ToString()
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                Else
                    clsCommonATLog._LoginEmployeeUnkid = -1
                End If


                With clsLeavePlanner
                    ._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                    ._Voidreason = popup1.Reason.Trim 'SHANI [01 FEB 2015]--txtreason.Text

                    If (CInt(Session("loginBy")) = Global.User.en_loginby.Employee) Then
                        ._Voidloginemployeeunkid = CInt(Session("Employeeunkid"))
                    Else
                        ._Voiduserunkid = CInt(Session("UserId"))
                    End If

                    '.Delete(CType(txtleaveplannerunkid.Text, Integer))
                    .Delete(CInt(Me.ViewState("LeavePlannerunkid")))

                    If (._Message.Length > 0) Then
                        DisplayMessage.DisplayMessage("Can not Delete This Entry " & ._Message, Me)
                    Else
                        DisplayMessage.DisplayMessage("Entry  Successfully deleted " & ._Message, Me)
                        Me.ViewState("LeavePlannerunkid") = Nothing
                        If (CInt(Session("loginBy")) = Global.User.en_loginby.User) Then
                            ClearCheckboxlist()
                        End If
                        Flag = "ADD"
                        Binddata()
                    End If
                    BlankObjects()
                    popup1.Dispose()

                    'SHANI [01 FEB 2015]-START
                    'Enhancement - REDESIGN SELF SERVICE.
                    'txtreason.Text = ""
                    'SHANI [01 FEB 2015]--END

                End With
            Else
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Please Select Employee from list. ", Me)
                'Sohail (23 Mar 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub Btnclose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnclose.Click
        Try
            If CType(Me.ViewState("LeavePlannerunkid"), Integer) > 0 Then

                'Pinkal (22-Mar-2016) -- Start
                'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.
                'Response.Redirect("~/Leave/LeavePLannerViewer.aspx")
                If CBool(Session("RidirectFromViewer")) = False Then
                    Response.Redirect("~/Leave/LeavePLanner.aspx", False)
                Else
                    Response.Redirect("~/Leave/LeavePLannerViewer.aspx", False)
                End If
                'Pinkal (22-Mar-2016) -- End

            End If
            If pnlLeaveEntry.Visible Then
                pnldispGrid.Visible = True
                pnlLeaveEntry.Visible = False
                btnSave.Visible = False
            Else
                Response.Redirect("~\UserHome.aspx", False)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "CheckBox Events"

    Protected Sub chkgrp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkgrp.CheckedChanged
        Try
            If (chkgrp.Checked) Then
                For i As Integer = 0 To chklistgrp.Items.Count - 1
                    chklistgrp.Items(i).Selected = True
                Next
            Else
                For i As Integer = 0 To chklistgrp.Items.Count - 1
                    chklistgrp.Items(i).Selected = False
                Next
            End If
            GetSelectedList()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkEmp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEmp.CheckedChanged
        Try
            If (chkEmp.Checked) Then
                For i As Integer = 0 To chkbxlistemlployee.Items.Count - 1
                    chkbxlistemlployee.Items(i).Selected = True
                Next

            ElseIf (chkEmp.Checked = False) Then
                For i As Integer = 0 To chkbxlistemlployee.Items.Count - 1
                    chkbxlistemlployee.Items(i).Selected = False
                Next
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkbxlistemlployee_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkbxlistemlployee.TextChanged
        Try
            Dim listitems As New ListItemCollection
            For i As Integer = 0 To chkbxlistemlployee.Items.Count - 1
                If (chkbxlistemlployee.Items(i).Selected) Then
                    listitems.Add(chkbxlistemlployee.Items(i).Value)
                End If
            Next
            If listitems.Count = chkbxlistemlployee.Items.Count Then
                chkEmp.Checked = True
            Else
                chkEmp.Checked = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chklistgrp_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chklistgrp.TextChanged
        Try
            GetSelectedList()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

#End Region

#Region "ToolBar Entry Events"

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.

    'Protected Sub ToolbarEntry1_Cancel_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean) Handles ToolbarEntry1.Cancel_Click
    '    Try
    '        If (Session("loginBy") = Global.User.en_loginby.User) Then
    '            ClearCheckboxlist()
    '            EnableFilterType()
    '        End If
    '        BlankObjects()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub ToolbarEntry1_FormMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.FormMode_Click
    '    Try

    '        'Pinkal (22-Nov-2012) -- Start
    '        'Enhancement : TRA Changes
    '        If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '            If CBool(Session("AddPlanLeave")) = False Then
    '                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Grid)
    '                Exit Sub
    '            End If
    '        End If
    '        'Pinkal (22-Nov-2012) -- End

    '        pnlLeaveEntry.Visible = True
    '        pnldispGrid.Visible = False
    '        If Flag = "ADD" Then
    '            ToolbarEntry1.EntryMode = Controls_ToolBarEntry.EN_ButtonMode.Add
    '        Else
    '            ToolbarEntry1.EntryMode = Controls_ToolBarEntry.EN_ButtonMode.Update
    '        End If
    '        ToolbarEntry1.SetButtons()
    '        btnSave.Visible = True

    '        'Pinkal (06-May-2014) -- Start
    '        'Enhancement : Language Changes 
    '        Me.Title = ""
    '        Me.Closebotton1.PageHeading = ""
    '        SetLanguage()
    '        'Pinkal (06-May-2014) -- End

    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub ToolbarEntry1_GridMode_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ToolbarEntry1.GridMode_Click
    '    Try
    '        pnlLeaveEntry.Visible = False
    '        pnldispGrid.Visible = True
    '        btnSave.Visible = False
    '        Binddata()
    '        ddlfilemloyee.BindEmployee()
    '        ddlfilleavetype.Mode = Controls_LeaveType.enentrymode.Filter
    '        ddlfilleavetype.BindLeaveType()


    '        'Pinkal (06-May-2014) -- Start
    '        'Enhancement : Language Changes 
    '        Me.Title = ""
    '        Me.Closebotton1.PageHeading = ""
    '        SetLanguage()
    '        'Pinkal (06-May-2014) -- End


    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub ToolbarEntry1_New_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean) Handles ToolbarEntry1.New_Click
    '    Try
    '        BlankObjects()
    '        EnableFilterType()
    '        btnSave.Visible = True
    '        ToolbarEntry1.EntryMode = Controls_ToolBarEntry.enEntryMode.Add
    '        ToolbarEntry1.SetButtons()
    '        If (Session("LoginBy") = Global.User.en_loginby.User) Then
    '            ClearCheckboxlist()
    '        End If
    '        Flag = "ADD"
    '        txtleaveplannerunkid.Text = 0
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub

    'Protected Sub ToolbarEntry1_Save_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean) Handles ToolbarEntry1.Save_Click
    '    Try
    '        EntrySave()
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try
    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region

#Region "RadioButton Events"

    Protected Sub RadioButtonList1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtfiltertype.SelectedIndexChanged
        Try

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'ToolbarEntry1.EntryMode = Controls_ToolBarEntry.enEntryMode.Add
            'ToolbarEntry1.SetButtons()
            'SHANI [01 FEB 2015]--END
            BlankObjects()
            ClearCheckboxlist()

            If rbtfiltertype.SelectedValue Is Nothing Or rbtfiltertype.SelectedValue = "" Then Exit Sub

            If (CInt(rbtfiltertype.SelectedValue) = 1) Then
                Me.filtermode = enAnalysisRef.Employee
                BindchkbxListEmp()
            End If

            If (CInt(rbtfiltertype.SelectedValue) = 2) Then
                If chkgrp.Checked Then chkgrp.Checked = False
                If chkEmp.Checked Then chkEmp.Checked = False
                Me.filtermode = enAnalysisRef.Section
                bindChklistGroup()
            End If

            If (CInt(rbtfiltertype.SelectedValue) = 3) Then
                If chkgrp.Checked Then chkgrp.Checked = False
                If chkEmp.Checked Then chkEmp.Checked = False
                Me.filtermode = enAnalysisRef.DeptInDeptGroup
                bindChklistGroup()
            End If

            If (CInt(rbtfiltertype.SelectedValue) = 4) Then
                If chkgrp.Checked Then chkgrp.Checked = False
                If chkEmp.Checked Then chkEmp.Checked = False
                Me.filtermode = enAnalysisRef.JobInJobGroup
                bindChklistGroup()
            End If

            If chklistgrp.Items.Count > 0 Then chkgrp.Enabled = True
            If chkbxlistemlployee.Items.Count > 0 Then chkEmp.Enabled = True

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

#End Region

#Region "DataGrid Events"

    Protected Sub dgView_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgView.PageIndexChanged
        Try
            dgView.CurrentPageIndex = e.NewPageIndex
            Binddata()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.DeleteCommand
        Try
            If e.CommandName = "Delete" Then
                BlankObjects()
                Flag = "EDIT"
                Me.ViewState.Add("LeavePlannerunkid", CInt(dgView.DataKeys(e.Item.ItemIndex)))
                popup1.Show()
                Binddata()

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
        'txtleaveplannerunkid.Text = e.Item.Cells(2).Text

        ' If (Session("loginBy") = Global.User.en_loginby.User) Then
        '  Dim clsLeavePlanner As New clsleaveplanner
        '  clsLeavePlanner._Leaveplannerunkid = CInt(dgView.DataKeys(e.Item.ItemIndex))
        'Else
        '   DisplayMessage.DisplayError(ex, Me)
        '  End If
    End Sub

    Protected Sub dgView_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgView.ItemCommand
        Try
            If e.CommandName = "Select" Then

                BlankObjects()
                Flag = "EDIT"


                'Pinkal (22-Mar-2016) -- Start
                'Enhancement - WORKING ON ENHANCEMENT ON CLAIM SUMMARY REPORT FOR KBC.

                'Dim dsLeaveForm As DataSet = clsLeavePlanner.GetList("Leaveplanner", _
                '                                                     Session("Database_Name").ToString(), _
                '                                                     CInt(Session("UserId")), _
                '                                                     CInt(Session("Fin_year")), _
                '                                                     CInt(Session("CompanyUnkId")), _
                '                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                '                                                     Session("UserAccessModeSetting").ToString(), True, _
                '                                                     True, True, False)

                Dim blnApplyFilter As Boolean = True
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    blnApplyFilter = False
                End If

                Dim dsLeaveForm As DataSet = clsLeavePlanner.GetList("Leaveplanner", _
                                                                     Session("Database_Name").ToString(), _
                                                                     CInt(Session("UserId")), _
                                                                     CInt(Session("Fin_year")), _
                                                                     CInt(Session("CompanyUnkId")), _
                                                                     eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString()).Date, _
                                                                     Session("UserAccessModeSetting").ToString(), True, _
                                                                   True, blnApplyFilter, False)

                'Pinkal (22-Mar-2016) -- End

                Blank_ModuleName()
                clsCommonATLog._WebFormName = "frmleaveplanner_AddEdit"
                StrModuleName2 = "mnuLeaveInformation"
                clsCommonATLog._WebClientIP = Session("IP_ADD").ToString()
                clsCommonATLog._WebHostName = Session("HOST_NAME").ToString
                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    clsCommonATLog._LoginEmployeeUnkid = CInt(Session("Employeeunkid"))
                Else
                    clsCommonATLog._LoginEmployeeUnkid = -1
                End If

                Dim dtLeaveForm As DataTable


                strSearching = "and leaveplannerunkid=" & CInt(dgView.DataKeys(e.Item.ItemIndex)) & ""

                Me.ViewState.Add("LeavePlannerunkid", CInt(dgView.DataKeys(e.Item.ItemIndex)))

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtLeaveForm = New DataView(dsLeaveForm.Tables("LeavePlanner"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtLeaveForm = dsLeaveForm.Tables("LeavePlanner")
                End If

                FetchData(dtLeaveForm)

                rbtfiltertype.Items(0).Enabled = True
                rbtfiltertype.Items(0).Selected = True
                rbtfiltertype.Items(1).Enabled = False
                rbtfiltertype.Items(2).Enabled = False
                rbtfiltertype.Items(3).Enabled = False
                btnSave.Visible = True
                Me.filtermode = enAnalysisRef.Employee


                'SHANI [01 FEB 2015]-START
                'Enhancement - REDESIGN SELF SERVICE.
                'ToolbarEntry1.EntryMode = Controls_ToolBarEntry.EN_ButtonMode.Update
                'ToolbarEntry1.SetButtons()
                'ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.E_DisplayMode.Form)
                'SHANI [01 FEB 2015]--END

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.Employee) Then
                    lblGender.Visible = False
                    drpGender.Visible = False

                    chkEmp.Visible = False
                    chkgrp.Visible = False
                    pnlFirstTable.Visible = False
                End If

                pnlLeaveEntry.Visible = True
                pnldispGrid.Visible = False
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub dgView_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgView.ItemDataBound
        Try
            If (e.Item.ItemIndex >= 0) Then

                If Session("DateFormat").ToString() <> Nothing Then
                    e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).Date.ToString(Session("DateFormat").ToString())
                    e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text).Date.ToString(Session("DateFormat").ToString())
                Else
                    e.Item.Cells(5).Text = eZeeDate.convertDate(e.Item.Cells(5).Text).ToString()
                    e.Item.Cells(6).Text = eZeeDate.convertDate(e.Item.Cells(6).Text).ToString()
                End If

                If (CInt(Session("LoginBy")) = Global.User.en_loginby.User) Then
                    dgView.Columns(0).Visible = CBool(Session("EditPlanLeave"))
                    dgView.Columns(1).Visible = CBool(Session("DeletePlanLeave"))
                End If

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

        'e.Item.Cells(2).Visible = False
        'e.Item.Cells(3).Visible = False
        'e.Item.Cells(4).Visible = False
        'e.Item.Cells(6).Visible = False
        'e.Item.Cells(8).Visible = False
        'e.Item.Cells(11).Visible = False
        'e.Item.Cells(12).Visible = False
        'e.Item.Cells(14).Visible = False
        'e.Item.Cells(15).Visible = False
        'e.Item.Cells(16).Visible = False
        'e.Item.Cells(17).Visible = False
        'e.Item.Cells(18).Visible = False
        'e.Item.Cells(19).Visible = False
        'e.Item.Cells(20).Visible = False
        'e.Item.Cells(21).Visible = False

        'If (e.Item.ItemIndex = -1) Then


        '    e.Item.Cells(5).Text = e.Item.Cells(5).Text
        '    e.Item.Cells(7).Text = e.Item.Cells(7).Text
        '    e.Item.Cells(9).Text = e.Item.Cells(9).Text
        '    e.Item.Cells(10).Text = e.Item.Cells(10).Text
        '    e.Item.Cells(13).Text = e.Item.Cells(13).Text
        'End If
    End Sub

#End Region

#Region "ImageButton Events"

    'SHANI [01 FEB 2015]-START
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebotton1_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebotton1.CloseButton_click
    '    Response.Redirect("~\UserHome.aspx", False)
    'End Sub
    'SHANI [01 FEB 2015]--END
#End Region

#Region "Dropdown Event"

    Protected Sub drpGender_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpGender.SelectedIndexChanged
        Try
            RadioButtonList1_SelectedIndexChanged(sender, e)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("drpGender_SelectedIndexChanged :- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region


    Private Sub SetLanguage()

        If pnldispGrid.Visible Then
            'Language.setLanguage(mstrModuleName)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, "Leave Planner List")

            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, "Leave Planner List")
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, "Leave Planner List")
            'SHANI [01 FEB 2015]--END
            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblfilleavetype.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "lblLeaveType", Me.lblfilleavetype.Text)
            Me.lblstartdate0.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "lblStartDate", Me.lblstartdate0.Text)
            Me.lblStopdate0.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "lblEndDate", Me.lblStopdate0.Text)
            Me.btnNew.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), btnNew.ID, Me.btnNew.Text).Replace("&", "")

            dgView.Columns(0).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "btnEdit", dgView.Columns(0).HeaderText).Replace("&", "")
            dgView.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), "btnDelete", dgView.Columns(1).HeaderText).Replace("&", "")
            dgView.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(3).FooterText, dgView.Columns(3).HeaderText)
            dgView.Columns(4).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(4).FooterText, dgView.Columns(4).HeaderText)
            dgView.Columns(5).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(5).FooterText, dgView.Columns(5).HeaderText)
            dgView.Columns(6).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), dgView.Columns(6).FooterText, dgView.Columns(6).HeaderText)


        ElseIf pnlLeaveEntry.Visible Then

            'Language.setLanguage(mstrModuleName1)

            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, "Add / Edit Leave Planner")
            'SHANI [01 FEB 2015]-START
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebotton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName1,CInt(HttpContext.Current.Session("LangId")),mstrModuleName1, "Add / Edit Leave Planner")
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, "Add / Edit Leave Planner")
            'SHANI [01 FEB 2015]-END

            Me.lblGender.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblGender.ID, Me.lblGender.Text)
            Me.lblfiltercriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "gbFilter", Me.lblfiltercriteria.Text)
            Me.chkgrp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "lblGroup", Me.chkgrp.Text)
            Me.tabpan.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "lblGroup", Me.tabpan.HeaderText)
            Me.chkEmp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "lblEmployee", Me.chkEmp.Text)
            Me.TabPanel1.HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "lblEmployee", Me.TabPanel1.HeaderText)
            Me.rbtfiltertype.Items(0).Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "rabEmployee", Me.rbtfiltertype.Items(0).Text)
            Me.rbtfiltertype.Items(1).Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "rabSection", Me.rbtfiltertype.Items(1).Text)
            Me.rbtfiltertype.Items(2).Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "rabDepartment", Me.rbtfiltertype.Items(2).Text)
            Me.rbtfiltertype.Items(3).Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "rabJob", Me.rbtfiltertype.Items(3).Text)

            Me.lblleaveinf.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), "gbLeavePlannerInfo", Me.lblleaveinf.Text)
            Me.lblleavetype.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblleavetype.ID, Me.lblleavetype.Text)
            Me.lblstartdate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblstartdate.ID, Me.lblstartdate.Text)
            Me.lblStopdate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblStopdate.ID, Me.lblStopdate.Text)
            Me.lblremarks.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblremarks.ID, Me.lblremarks.Text)

            Me.btnSave.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnSave.ID, Me.btnSave.Text).Replace("&", "")
            Me.Btnclose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.Btnclose.ID, Me.Btnclose.Text).Replace("&", "")

        End If

    End Sub


End Class


'Public Sub BindChkListByEmpid(ByVal empid As Integer)
'    Try
'        Dim dt As DataTable = Nothing

'        chkbxlistemlployee.Items.Clear()
'        If dtEmp Is Nothing Then
'            dtEmp = CType(Me.ViewState("Employeelist"), DataTable)
'        End If
'        dt = New DataView(dtEmp, "employeeunkid=" & empid, "", DataViewRowState.CurrentRows).ToTable

'        chkbxlistemlployee.DataSource = dt
'        chkbxlistemlployee.DataTextField = "name"
'        chkbxlistemlployee.DataValueField = "employeeunkid"
'        chkbxlistemlployee.DataBind()
'        chkbxlistemlployee.Items(0).Selected = True
'    Catch ex As Exception
'        DisplayMessage.DisplayError(ex, Me)
'    End Try

'    ' clsemp.GetEmployeeList("EmployeeList", False, True)
'    'msql = "select Userunkid,username from hrmsConfiguration..cfuser_master"
'    ' msql = " select employeeunkid,(firstname+' '+ employeecode  ) as name from hremployee_master where employeeunkid = " & empid & " "
'    'ds = clsDataOpr.WExecQuery(msql, "hremployee_master")
'    'If ds Is Nothing Then
'    '    ds = CType(Me.ViewState("Employeelist"), DataSet)
'    'End If
'    ' chkbxlistemlployee.DataSource =clsemp.GetEmployeeList("EmployeeList", False, True, empid)
'End Sub


'Public Property Enabled() As Boolean
'    Get
'    End Get
'    Set(ByVal value As Boolean)
'        Me.Enabled = value
'    End Set
'End Property

'Public Enum enAnalysisRef
'    Employee = 1
'    Grade = 2
'    Access = 3
'    Section = 4
'    CostCenter = 5
'    PayPoint = 6
'    DeptInDeptGroup = 7
'    SectionInDepartment = 8
'    JobInJobGroup = 9
'    ClassInClassGroup = 10
'    Station = 11

'End Enum
'Protected Sub ToolbarEntry1_Delete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean) Handles ToolbarEntry1.Delete_Click
'    Try
'        ' If (Session("loginBy") = Global.User.en_loginby.User) Then
'        Dim clsLeavePlanner As New Aruti.Data.clsleaveplanner
'        popup1.Show()
'        '  Else
'        ' DisplayMessage.DisplayError(ex, Me)
'        '  BlankObjects()
'        'End If
'    Catch ex As Exception
'        DisplayMessage.DisplayError(ex, Me)
'    End Try

'End Sub

'Protected Sub ToolbarEntry1_Find_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean) Handles ToolbarEntry1.Find_Click
'    'ModalPopupExtender1.Show()
'End Sub
'Protected Sub ToolbarEntry1_First_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean) Handles ToolbarEntry1.First_Click

'    Try
'        System.Threading.Thread.Sleep(250)
'        If Session("LoginBy") = Global.User.en_loginby.Employee Then
'            '  msql = "select top 1 * from  " & Session("mdbname") & "..lvleaveplanner where employeeunkid =" + Me.ViewState("Empunkid").ToString + " and isvoid <> 'True'   order by startdate,employeeunkid"
'            msql = "select top 1 * from  " & Session("mdbname") & "..lvleaveplanner where employeeunkid =" & IIf(chkbxlistemlployee.SelectedValue = Nothing, Me.ViewState("Empunkid"), chkbxlistemlployee.SelectedValue) & " and isvoid <> 'True'   order by startdate"
'        Else
'            msql = "select top 1 * from  " & Session("mdbname") & "..lvleaveplanner where  isvoid <> 'True'   and Userunkid =" & Session("UserId") & "  order by leaveplannerunkid"
'        End If
'        SetNavigation(msql)
'        Flag = "EDIT"
'        SetButtons = True
'    Catch ex As Exception
'        DisplayMessage.DisplayError(ex, Me)
'    End Try
'End Sub
'Protected Sub ToolbarEntry1_Prev_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean) Handles ToolbarEntry1.Prev_Click

'    Try
'        System.Threading.Thread.Sleep(250)
'        If Session("LoginBy") = Global.User.en_loginby.Employee Then
'            msql = "select top 1 * from  " & Session("mdbname") & " ..lvleaveplanner where employeeunkid = " & IIf(chkbxlistemlployee.SelectedValue = Nothing, Me.ViewState("Empunkid"), chkbxlistemlployee.SelectedValue) & "   and startdate < '" & Format(CDate(dtStartDate.GetDate), "yyyyMMdd") & "' and  isvoid <> 'True' order by startdate desc"
'        Else
'            msql = "select top 1 * from  " & Session("mdbname") & " ..lvleaveplanner where    leaveplannerunkid < " & IIf(txtleaveplannerunkid.Text = "", 0, Val(txtleaveplannerunkid.Text)) & " and  isvoid <> 'true' and Userunkid =" & Session("UserId") & "  order by leaveplannerunkid  desc , startdate desc" 'startdate <= '" & Format(CDate(dtStartDate.GetDate), "yyyyMMdd") & "' and
'        End If
'        SetNavigation(msql)
'        SetButtons = True
'        Flag = "EDIT"
'    Catch ex As Exception
'        DisplayMessage.DisplayError(ex, Me)
'    End Try
'End Sub
'Protected Sub ToolbarEntry1_Next_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean) Handles ToolbarEntry1.Next_Click
'    Try

'        System.Threading.Thread.Sleep(250)
'        ' ToolbarEntry1.EnableMoveNextButton(False)
'        If Session("LoginBy") = Global.User.en_loginby.Employee Then
'            ' msql = "select top 1 * from  " & Session("mdbname") & " ..lvleaveplanner where employeeunkid = " & Me.ViewState("Empunkid") & " and  startdate > '" & Format(CDate(dtStartDate.GetDate), "yyyyMMdd") & "' and  isvoid <> 'True'  order by startdate,employeeunkid"
'            msql = "select top 1 * from  " & Session("mdbname") & " ..lvleaveplanner where employeeunkid = " & IIf(chkbxlistemlployee.SelectedValue = Nothing, Me.ViewState("Empunkid"), chkbxlistemlployee.SelectedValue) & " and  startdate > '" & Format(CDate(dtStartDate.GetDate), "yyyyMMdd") & "' and  isvoid <> 'True'  order by startdate"
'        Else
'            msql = "select top 1 * from  " & Session("mdbname") & " ..lvleaveplanner where   leaveplannerunkid > " & IIf(txtleaveplannerunkid.Text = "", 0, Val(txtleaveplannerunkid.Text)) & " and  isvoid <> 'true' and Userunkid =" & Session("UserId") & "  order by leaveplannerunkid , startdate" ' startdate >= '" & Format(CDate(dtStartDate.GetDate), "yyyyMMdd") & "'  and 
'        End If
'        SetNavigation(msql)
'        SetButtons = True
'        Flag = "EDIT"
'        ' ToolbarEntry1.EnableMoveNextButton(True)
'    Catch ex As Exception
'        DisplayMessage.DisplayError(ex, Me)
'    End Try
'End Sub
'Protected Sub ToolbarEntry1_Last_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs, ByRef SetButtons As Boolean) Handles ToolbarEntry1.Last_Click
'    Try

'        System.Threading.Thread.Sleep(250)
'        If Session("LoginBy") = Global.User.en_loginby.Employee Then
'            msql = "select top 1 * from  " & Session("mdbname") & " ..lvleaveplanner where employeeunkid = " & IIf(chkbxlistemlployee.SelectedValue = Nothing, Me.ViewState("Empunkid"), chkbxlistemlployee.SelectedValue) & " and isvoid <> 'True' order by startdate desc"
'        Else
'            msql = "select top 1 * from  " & Session("mdbname") & " ..lvleaveplanner  where  isvoid <> 'true' and Userunkid =" & Session("UserId") & " order by leaveplannerunkid desc"
'        End If
'        SetNavigation(msql)
'        SetButtons = True
'        Flag = "EDIT"
'    Catch ex As Exception
'        DisplayMessage.DisplayError(ex, Me)
'    End Try
'End Sub

'Public Sub BindDataGrid()
'    Try
'        msql = "   select p.leaveplannerunkid as unkid , p.analysisrefid,e.firstname as name ,l.leavename as leave,convert(nvarchar(15),p.startdate,106) as StartDate ,convert(nvarchar(15),p.stopdate,106) as StopDate,p.remarks,p.canceldate,p.cancelremarks from " & Session("mdbname") & "..lvleaveplanner p"
'        msql += "  left outer join hremployee_master e on e.employeeunkid = p.employeeunkid"
'        msql += "  left outer join lvleavetype_master l on l.leavetypeunkid = p.leavetypeunkid"
'        msql += "  where  isvoid <> 'True'  " & vbCrLf
'        Dim ds As DataSet = clsDataOpr.WExecQuery(msql, "lvleaveplanner")
'        dgView.DataSource = ds
'        dgView.DataBind()
'    Catch ex As Exception
'        DisplayMessage.DisplayError(ex, Me)
'    End Try
'End Sub

'Public Sub BindData()
'    Try

'        msql = "   select  p.leaveplannerunkid as unkid, p.employeeunkid , p.analysisrefid,e.displayname as name ,l.leavename as leave,convert(nvarchar(15),p.startdate,103) as StartDate ,convert(nvarchar(15),p.stopdate,103) as StopDate,p.remarks,p.canceldate,p.cancelremarks from  lvleaveplanner p"
'        msql += "  left outer join hremployee_master e on e.employeeunkid = p.employeeunkid"
'        msql += "  left outer join lvleavetype_master l on l.leavetypeunkid = p.leavetypeunkid"
'        msql += "  where  isvoid = 'False'  " & vbCrLf
'        If (Session("LoginBy") = Global.User.en_loginby.User) Then
'            msql += "  and  Userunkid =" & Session("UserId") & " " & vbCrLf
'        End If
'        If (ddlfilemloyee.SelectedValue <> -1) Then msql += "  and p.employeeunkid =" & ddlfilemloyee.SelectedValue & "" & vbCrLf
'        If (ddlfilleavetype.SelectedValue <> -1) Then msql += "  and p.leavetypeunkid =" & ddlfilleavetype.SelectedValue & "" & vbCrLf
'        ' If dtfilstdate.ChkInclude_Checked Then msql += "  and StartDate >='" & Format(CDate(dtfilstdate.GetDate), "yyyyMMdd") & "'" & vbCrLf
'        '  If dtfilenddate.ChkInclude_Checked Then msql += "  and StopDate  <= '" & Format(CDate(dtfilenddate.GetDate), "yyyyMMdd") & "'" & vbCrLf

'        Dim ds As DataSet = clsDataOpr.WExecQuery(msql, "lvleaveplanner")
'        ' dgView.CurrentPageIndex = 0

'        dgView.DataSource = ds
'        dgView.DataBind()


'    Catch ex As Exception

'        If ex.Message = "Invalid CurrentPageIndex value. It must be >= 0 and < the PageCount." And dgView.CurrentPageIndex > 0 Then
'            dgView.CurrentPageIndex = dgView.CurrentPageIndex - 1
'            dgView.DataBind()
'        Else
'            Throw ex
'            DisplayMessage.DisplayError(ex, Me)
'        End If

'    End Try
'End Sub

'Public Function SetNavigation(ByVal mQry As String) As Boolean
'    Try
'        Dim Ds As DataSet, mretval As Boolean = False
'        Ds = clsDataOpr.WExecQuery(msql, "lvleaveplanner")
'        If Not Ds Is Nothing Then
'            If Ds.Tables(0).Rows.Count > 0 Then
'                FetchData(Ds)
'                mretval = True
'            Else
'                DisplayMessage.DisplayMessage("Record Not Found", Me)
'                ToolbarEntry1.EntryMode = Controls_ToolBarEntry.enEntryMode.Add
'                ToolbarEntry1.SetEntryMode(Controls_ToolBarEntry.enEntryMode.Add)
'                'ToolbarEntry1.EntryMode = Controls_ToolBarEntry.enEntryMode.Add
'                'ToolbarEntry1.SetButtons()
'                'Flag = "ADD"
'                'BlankObjects()
'            End If
'        End If
'        Return mretval
'    Catch ex As Exception
'        DisplayMessage.DisplayError(ex, Me)
'    End Try


'End Function

