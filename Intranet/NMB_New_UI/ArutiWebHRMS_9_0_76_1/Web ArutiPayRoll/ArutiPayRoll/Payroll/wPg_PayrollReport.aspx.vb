﻿Option Strict On

#Region " Imports "

Imports System.Data
Imports System.Drawing
Imports eZeeCommonLib.clsDataOperation
Imports eZeeCommonLib
Imports System.Globalization
Imports System.Threading
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports Aruti.Data
Imports ArutiReports

#End Region

Partial Class Payroll_wPg_PayrollReport
    Inherits Basepage

#Region " Private Variables "
    Dim DisplayMessage As New CommonCodes

    'Sohail (31 Jan 2014) -- Start
    Private mdecEx_Rate As Decimal = 1
    Private mstrCurr_Sign As String = String.Empty

    Private mstrBaseCurrSign As String = String.Empty
    Private mintBaseCurrId As Integer = 0
    Private mstrCurrency_Rate As String = String.Empty

    Private mstrFromDatabaseName As String = ""
    Private mstrToDatabaseName As String = ""
    Private mintFirstOpenPeriod As Integer = 0
    'Sohail (31 Jan 2014) -- End

    Private mdtHeads As DataTable 'Sohail (29 Apr 2014)

    'Sohail (20 May 2014) -- Start
    'Enhancement - Analysis By on Payroll Report
    Private mstrStringIds As String = ""
    Private mstrStringName As String = ""
    Private mintViewIdx As Integer = 0
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    'Sohail (20 May 2014) -- End

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmPayrollReport"
    'Anjan [04 June 2014] -- End

    'Shani [ 22 NOV 2014 ] -- START
    ''FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
    Private marrCustomPayrollReportHeadsIds As New ArrayList
    'Shani [ 22 NOV 2014 ] -- END
#End Region

#Region " Private Functions & Methods "
    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        'Sohail (30 Jan 2014) -- Start
        Dim objEmp As New clsEmployee_Master
        Dim objBranch As New clsStation
        Dim objExRate As New clsExchangeRate
        'Sohail (30 Jan 2014) -- End
        'Sohail (10 Jul 2019) -- Start
        'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
        Dim objMembership As New clsmembership_master
        'Sohail (10 Jul 2019) -- End
        Dim dsCombo As DataSet
        Try

            With ddlReportType
                .Items.Clear()
                'Language.setLanguage(mstrModuleName)
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Payroll Report"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Personal Salary Calculation Report"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 102, "Employee Payroll Summary Report")) 'Sohail (01 Sep 2014)
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 103, "Custom Payroll Report")) 'Shani (22 OCT 2014)
                'SHANI (20 Apr 2015) -- Start
                'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Custom Payroll Report Template 2"))
                'SHANI (20 Apr 2015) -- END
                .SelectedIndex = 0
            End With
            Call ddlReportType_SelectedIndexChanged(ddlReportType, Nothing) 'Shani [ 22 OCT 2014 ] -- START
            Dim strDatabaseName As String = ""
            Dim objCompany As New clsCompany_Master
            dsCombo = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "List", CInt(Session("Fin_year")))
            If dsCombo.Tables("List").Rows.Count > 0 Then
                strDatabaseName = dsCombo.Tables("List").Rows(0).Item("database_name").ToString
            End If

            'Sohail (31 Jan 2014) -- Start

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmp.GetEmployeeList("Emp", True, False, , , , , , , , , , , , , , , , Session("AccessLevelFilterString"))
            'Sohail (09 Feb 2016) -- Start
            'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
            'dsCombo = objEmp.GetEmployeeList(Session("Database_Name"), _
            '                                    Session("UserId"), _
            '                                    Session("Fin_year"), _
            '                                    Session("CompanyUnkId"), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
            '                                    Session("UserAccessModeSetting"), True, _
            '                                    Session("IsIncludeInactiveEmp"), "Emp", True)
            'Nilay (10-Feb-2016) -- Start
            'dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
            '                                   CInt(Session("UserId")), _
            '                                   CInt(Session("Fin_year")), _
            '                                   CInt(Session("CompanyUnkId")), _
            '                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                   eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                   Session("UserAccessModeSetting").ToString, True, _
            '                                   chkInactiveemp.Checked, "Emp", True)
            'Sohail (13 Feb 2016) -- Start
            'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
            'dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
            '                                    CInt(Session("UserId")), _
            '                                    CInt(Session("Fin_year")), _
            '                                    CInt(Session("CompanyUnkId")), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                    eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                    Session("UserAccessModeSetting").ToString, True, _
            '                                   True, "Emp", True)
            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If

            dsCombo = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                                CInt(Session("UserId")), _
                                                CInt(Session("Fin_year")), _
                                                CInt(Session("CompanyUnkId")), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                                Session("UserAccessModeSetting").ToString, True, _
                                               True, "Emp", blnSelect, intEmpUnkId, , , , , , , , , , , , , , , , blnApplyAccess)
            'Sohail ((13 Feb 2016) -- End
            'Nilay (10-Feb-2016) -- End

            'Sohail (09 Feb 2016) -- End

            'Shani(24-Aug-2015) -- End
            With cboEmployee
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            dsCombo = objBranch.getComboList("List", True)
            With cboBranch
                .DataValueField = "stationunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
            End With

            dsCombo = objExRate.getComboList("ExRate", True)
            If dsCombo.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsCombo.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                End If
            End If
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_sign"
                .DataSource = dsCombo.Tables("ExRate")
                .DataBind()
            End With
            'Sohail (31 Jan 2014) -- End

            'Sohail (30 Jan 2014) -- Start
            'Enhancement - Kenya Statutory Report
            'Dim intFirstOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, 1, CInt(Session("Fin_year")))
            mintFirstOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)
            'Sohail (30 Jan 2014) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, "Period", True, 2, , , strDatabaseName)
            'Nilay (10-Feb-2016) -- Start
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString, CDate(Session("fin_startdate")), "Period", True, 2)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, 0, Session("Database_Name").ToString, CDate(Session("fin_startdate")), "Period", True)
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            If mintFirstOpenPeriod > 0 Then
                Dim drRow() As DataRow = dsCombo.Tables(0).Select("periodunkid = " & mintFirstOpenPeriod & " ")
                If drRow.Length <= 0 Then
                    Dim dRow As DataRow = dsCombo.Tables(0).NewRow

                    objPeriod._Periodunkid(Session("Database_Name").ToString) = mintFirstOpenPeriod

                    dRow.Item("periodunkid") = mintFirstOpenPeriod
                    dRow.Item("name") = objPeriod._Period_Name
                    dRow.Item("start_date") = eZeeDate.convertDate(objPeriod._Start_Date)
                    dRow.Item("end_date") = eZeeDate.convertDate(objPeriod._End_Date)
                    dRow.Item("yearunkid") = objPeriod._Yearunkid 'Sohail (20 Aug 2014)

                    dsCombo.Tables(0).Rows.Add(dRow)
                End If
            End If
            With drpPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = mintFirstOpenPeriod.ToString
            End With

            With drpPeriodTo
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0).Copy
                .DataBind()
                .SelectedValue = mintFirstOpenPeriod.ToString
            End With

            If ViewState("dsPeriod") Is Nothing Then
                ViewState("dsPeriod") = dsCombo.Tables(0).Copy
            End If

            'Shani [ 22 NOV 2014 ] -- START
            'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
            dsCombo = objMaster.getComboListForHeadType("HeadType")
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            Dim dr As DataRow = dsCombo.Tables("HeadType").NewRow
            dr.Item("Id") = -1
            dr.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Loan")
            dsCombo.Tables("HeadType").Rows.Add(dr)
            dr = dsCombo.Tables("HeadType").NewRow
            dr.Item("Id") = -2
            dr.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Advance")
            dsCombo.Tables("HeadType").Rows.Add(dr)
            dr = dsCombo.Tables("HeadType").NewRow
            dr.Item("Id") = -3
            dr.Item("Name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Savings")
            dsCombo.Tables("HeadType").Rows.Add(dr)
            'Sohail (03 Feb 2016) -- End
            With cboTrnHeadType
                .DataValueField = "Id"
                .DataTextField = "Name"
                .DataSource = dsCombo.Tables("HeadType")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Shani [ 22 NOV 2014 ] -- END

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
            dsCombo = objMembership.getListForCombo("List", True)
            With cboMembership
                .DataValueField = "membershipunkid"
                .DataTextField = "name"
                .DataSource = dsCombo.Tables(0)
                .DataBind()
                .SelectedValue = "0"
            End With

            'Sohail (10 Jul 2019) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("FillCombo:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
        Finally
            objMembership = Nothing
            'Sohail (10 Jul 2019) -- End
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Dim objPeriod As New clscommom_period_Tran
        Dim objPayment As New clsPayment_tran
        Try
            If drpPeriod.SelectedValue = "" OrElse CInt(drpPeriod.SelectedValue) <= 0 Then
                DisplayMessage.DisplayMessage("Please select Period.", Me)
                Return False
            End If

            'Sohail (01 Sep 2014) -- Start
            'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
            'If ddlReportType.SelectedIndex = 1 Then
            If ddlReportType.SelectedIndex = 1 OrElse ddlReportType.SelectedIndex = 2 Then
                'Sohail (01 Sep 2014) -- End
                'Sohail (20 Aug 2014) -- Start
                'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
                'Sohail (01 Sep 2014) -- Start
                'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                'If cboEmployee.SelectedValue = "" OrElse CInt(cboEmployee.SelectedValue) <= 0 Then
                If ddlReportType.SelectedIndex = 1 AndAlso (cboEmployee.SelectedValue = "" OrElse CInt(cboEmployee.SelectedValue) <= 0) Then
                    'Sohail (01 Sep 2014) -- End
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Employee is compulsory infomation. Please select Employee to continue."), Me)
                    drpPeriodTo.Focus()
                    Return False
                    'Sohail (20 Aug 2014) -- End
                ElseIf drpPeriodTo.SelectedValue = "" OrElse CInt(drpPeriodTo.SelectedValue) <= 0 Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Period is compulsory infomation. Please select period to continue."), Me)
                    drpPeriodTo.Focus()
                    Return False
                ElseIf drpPeriodTo.SelectedIndex < drpPeriod.SelectedIndex Then
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, " To Period cannot be less than Form Period"), Me)
                    drpPeriodTo.Focus()
                    Return False
                End If
            End If

            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriod.SelectedItem.Value)
            If objPeriod._Statusid <> 2 Then

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim ds As DataSet = objPayment.GetListByPeriod("Payment", clsPayment_tran.enPaymentRefId.PAYSLIP, CInt(drpPeriod.SelectedItem.Value), clsPayment_tran.enPayTypeId.PAYMENT, CInt(Session("Employeeunkid")))
                'Sohail (09 Feb 2016) -- Start
                'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
                'Dim ds As DataSet = objPayment.GetListByPeriod(Session("Database_Name"), _
                '                                               Session("UserId"), _
                '                                               Session("Fin_year"), _
                '                                               Session("CompanyUnkId"), _
                '                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                               eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                               Session("UserAccessModeSetting"), True, _
                '                                               Session("IsIncludeInactiveEmp"), _
                '                                               "Payment", _
                '                                               clsPayment_tran.enPaymentRefId.PAYSLIP, _
                '                                               CInt(drpPeriod.SelectedItem.Value), _
                '                                               clsPayment_tran.enPayTypeId.PAYMENT, _
                '                                               CInt(Session("Employeeunkid")))
                Dim ds As DataSet = objPayment.GetListByPeriod(Session("Database_Name").ToString, _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               objPeriod._Start_Date, _
                                                               objPeriod._End_Date, _
                                                               Session("UserAccessModeSetting").ToString, True, _
                                                               chkInactiveemp.Checked, _
                                                               "Payment", _
                                                               clsPayment_tran.enPaymentRefId.PAYSLIP, _
                                                               CInt(drpPeriod.SelectedItem.Value), _
                                                               clsPayment_tran.enPayTypeId.PAYMENT, _
                                                               CInt(Session("Employeeunkid")).ToString, False)
                'Sohail (09 Feb 2016) -- End
                'Shani(20-Nov-2015) -- End

                If ds.Tables("Payment").Rows.Count <= 0 Then
                    DisplayMessage.DisplayMessage("Sorry! Payment is not done for selected month.", Me)
                    Return False
                End If
            End If

            'Sohail (31 Jan 2014) -- Start
            If mdecEx_Rate <= 0 Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Sorry, No exchange rate is defined for this currency for the period selected."), Me)
                cboCurrency.Focus()
                Return False
            End If
            'Sohail (31 Jan 2014) -- End

            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("IsValidate:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    'Sohail (29 Apr 2014) -- Start
    'Enhancement - Export Excel Report with Company Logo 
    Private Sub CreateHeadListTable()
        Try
            mdtHeads = New DataTable
            With mdtHeads
                .Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
                .Columns.Add("tranheadunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                .Columns.Add("code", System.Type.GetType("System.String")).DefaultValue = ""
                .Columns.Add("name", System.Type.GetType("System.String")).DefaultValue = ""
            End With

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillList()
        Dim objTranHead As New clsTransactionHead
        Try
            mdtHeads.Rows.Clear()

            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'Dim dsList As DataSet = objTranHead.getComboList("Heads", False, , , , , , "(typeof_id IN (" & enTypeOf.Allowance & "," & enTypeOf.Other_Earnings & ") OR trnheadtype_id  IN (" & enTranHeadType.DeductionForEmployee & "))")
            Dim dsList As DataSet = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", False, , , , , , "(typeof_id IN (" & enTypeOf.Allowance & "," & enTypeOf.Other_Earnings & ") OR trnheadtype_id  IN (" & enTranHeadType.DeductionForEmployee & "))")
            'Sohail (03 Feb 2016) -- End

            Dim dtRow As DataRow

            For Each dsRow As DataRow In dsList.Tables("Heads").Rows
                dtRow = mdtHeads.NewRow

                dtRow.Item("tranheadunkid") = CInt(dsRow.Item("tranheadunkid"))
                dtRow.Item("code") = dsRow.Item("code").ToString
                dtRow.Item("name") = dsRow.Item("name").ToString

                mdtHeads.Rows.Add(dtRow)
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objTranHead = Nothing

            If mdtHeads.Rows.Count <= 0 Then
                Dim r As DataRow = mdtHeads.NewRow

                r.Item(2) = "None" ' To Hide the row and display only Row Header
                r.Item(1) = "0"

                mdtHeads.Rows.Add(r)
            End If

            lvTranHead.DataSource = mdtHeads
            lvTranHead.DataBind()
        End Try
    End Sub

    Private Sub CheckAllHeads(ByVal blnCheckAll As Boolean)
        Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In lvTranHead.Rows
                If CInt(mdtHeads.Rows(gvr.RowIndex).Item("tranheadunkid")) > 0 AndAlso mdtHeads.Rows(gvr.RowIndex).Item("name").ToString <> "" Then
                    cb = CType(lvTranHead.Rows(gvr.RowIndex).FindControl("chkSelect"), CheckBox)
                    cb.Checked = blnCheckAll

                    mdtHeads.Rows(gvr.RowIndex).Item("IsChecked") = blnCheckAll
                End If
            Next

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim chkSelectAll As CheckBox = TryCast(sender, CheckBox)
            If chkSelectAll Is Nothing Then Exit Try

            Call CheckAllHeads(chkSelectAll.Checked)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim chkSelect As CheckBox = TryCast(sender, CheckBox)
            If chkSelect Is Nothing Then Exit Try

            Dim gvRow As GridViewRow = CType(chkSelect.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                mdtHeads.Rows(gvRow.RowIndex).Item("IsChecked") = chkSelect.Checked
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (29 Apr 2014) -- End

    'Shani [ 22 OCT 2014 ] -- START
    'Implementing "Custom Payroll Report" under Web As One of the Report Type
    Private Sub Fill_Allocation()
        Try

            Dim dsList As New DataSet
            Dim dtTable As New DataTable
            Dim objPayroll As New clsPayrollReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            'SHANI (20 APR 2015)-START
            'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
            'Dim xAlloc() As String = CStr(Session("CustomPayrollReportAllocIds")).Split(",")
            Dim marrAlloc As New ArrayList
            If ddlReportType.SelectedIndex = 4 Then
                If CStr(Session("CustomPayrollReportTemplate2AllocIds")).Trim <> "" Then
                    marrAlloc.AddRange(CStr(Session("CustomPayrollReportTemplate2AllocIds")).Split(CChar(",")))
                End If
            Else 'Custom Payroll Report
                If CStr(Session("CustomPayrollReportAllocIds")).Trim <> "" Then
                    marrAlloc.AddRange(CStr(Session("CustomPayrollReportAllocIds")).Split(CChar(",")))
                End If
            End If

            If marrAlloc.Count > 0 Then
                dsList = objPayroll.GetAllocationList("Id NOT IN(" & String.Join(",", CType(marrAlloc.ToArray(Type.GetType("System.String")), String())) & ")")
                dsList.Tables(0).Columns.Add("IsChecked", Type.GetType("System.Boolean")).SetOrdinal(0)
                For Each dsRow As DataRow In dsList.Tables(0).Rows
                    dsRow.Item("IsChecked") = False
                Next
                dsList.Tables(0).AcceptChanges()

                dtTable.Merge(dsList.Tables(0), True)

                dsList = objPayroll.GetAllocationList("Id IN (" & String.Join(",", CType(marrAlloc.ToArray(Type.GetType("System.String")), String())) & ")")
                dsList.Tables(0).Columns.Add("IsChecked", Type.GetType("System.Boolean")).SetOrdinal(0)
                For Each dsRow As DataRow In dsList.Tables(0).Rows
                    dsRow.Item("IsChecked") = False
                Next

                Dim intPos As Integer = 0
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    Dim dr As DataRow = dtTable.NewRow
                    dr.ItemArray = dRow.ItemArray
                    dtTable.Rows.InsertAt(dr, intPos)
                    intPos += 1
                Next
            Else
                'dtTable.Columns("IsChecked").DefaultValue = False
                dsList = objPayroll.GetAllocationList()
                dsList.Tables(0).Columns.Add("IsChecked", Type.GetType("System.Boolean")).SetOrdinal(0)
                For Each dsRow As DataRow In dsList.Tables(0).Rows
                    dsRow.Item("IsChecked") = False
                Next
                dtTable.Merge(dsList.Tables(0), True)
            End If

            For Each xrow As DataRow In dtTable.Rows
                If marrAlloc.Contains(xrow.Item("Id").ToString) = True Then
                    xrow("IsChecked") = True
                    xrow.AcceptChanges()
                End If
            Next
            'SHANI (20 APR 2015)--END \

            With lvAllocation_hierarchy
                .DataSource = dtTable
                .DataBind()
            End With
            Me.ViewState.Add("dtAllocation", dtTable)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub FillCustomHeads()
        Dim objTranHead As New clsTransactionHead
        Dim dsList As DataSet = Nothing
        Try
            Dim dtTable As New DataTable


            'Shani (20 Apr 2015) -- Start
            'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
            ConfigParameter._Object._Companyunkid = CInt(Session("CompanyUnkId"))
            ConfigParameter._Object.Refresh()
            marrCustomPayrollReportHeadsIds.Clear()
            If ddlReportType.SelectedIndex = 4 Then 'Custom Payroll Report Template 2 
                If CStr(Session("CustomPayrollReportTemplate2HeadsIds")).Trim <> "" Then
                    marrCustomPayrollReportHeadsIds.AddRange(CStr(Session("CustomPayrollReportTemplate2HeadsIds")).Split(CChar(",")))
                End If
            Else 'Custom Payroll Report
                If CStr(Session("CustomPayrollReportHeadsIds")).Trim <> "" Then
                    marrCustomPayrollReportHeadsIds.AddRange(CStr(Session("CustomPayrollReportHeadsIds")).Split(CChar(",")))
                End If
            End If
            Dim ds As DataSet
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            Dim objLoan As New clsLoan_Scheme
            Dim objSaving As New clsSavingScheme
            Dim dsLoanIN As DataSet = Nothing
            Dim dsLoanNotIN As DataSet = Nothing
            Dim dsSavingIN As DataSet = Nothing
            Dim dsSavingNotIN As DataSet = Nothing
            Dim arrHead() As String = (From p In marrCustomPayrollReportHeadsIds Where (IsNumeric(p)) Select (p.ToString)).ToArray
            Dim arrLoan() As String = (From p In marrCustomPayrollReportHeadsIds Where (p.ToString.StartsWith("Loan")) Select (p.ToString.Substring(4).ToString)).ToArray
            Dim arrAdvance() As String = (From p In marrCustomPayrollReportHeadsIds Where (p.ToString.StartsWith("Advance")) Select (p.ToString.Substring(7).ToString)).ToArray
            Dim arrSaving() As String = (From p In marrCustomPayrollReportHeadsIds Where (p.ToString.StartsWith("Saving")) Select (p.ToString.Substring(6).ToString)).ToArray
            Dim aLoan As New Dictionary(Of String, DataRow)
            Dim aAdvance As New Dictionary(Of String, DataRow)
            Dim aSaving As New Dictionary(Of String, DataRow)
            'Sohail (03 Feb 2016) -- End
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'If marrCustomPayrollReportHeadsIds.Count > 0 Then
            '    dsList = objTranHead.getComboList("Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , "prtranhead_master.tranheadunkid NOT IN(" & String.Join(",", marrCustomPayrollReportHeadsIds.ToArray(Type.GetType("System.String"))) & ")", , True, True)
            '    ds = objTranHead.getComboList("Heads", False, , , , , , "prtranhead_master.tranheadunkid IN(" & String.Join(",", marrCustomPayrollReportHeadsIds.ToArray(Type.GetType("System.String"))) & ")", , True, True)
            'Else
            '    dsList = objTranHead.getComboList("Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , , , True, True)
            '    ds = objTranHead.getComboList("Heads", False, , , , , , , , True, True)
            'End If
            Dim strNonHeadFilter As String = ""
            If CInt(cboTrnHeadType.SelectedValue) < 0 Then
                strNonHeadFilter = " AND 1 = 2 "
            End If
            'Transaction Heads
            If arrHead.Length > 0 Then
                dsList = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , "prtranhead_master.tranheadunkid NOT IN(" & String.Join(",", arrHead) & ")" & strNonHeadFilter, True, True)
                ds = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", False, , , , , , "prtranhead_master.tranheadunkid IN(" & String.Join(",", arrHead) & ")", True, True)
            Else
                dsList = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", False, CInt(cboTrnHeadType.SelectedValue), , , , , If(strNonHeadFilter.Trim <> "", strNonHeadFilter.Substring(4), ""), True, True)
                ds = objTranHead.getComboList(Session("Database_Name").ToString, "Heads", False, , , , , , , True, True)
            End If
            Dim a As Dictionary(Of String, DataRow) = (From p In ds.Tables(0).AsEnumerable Select New With {.ID = p.Item("tranheadunkid").ToString, .DATAROW = p}).ToDictionary(Function(x) x.ID, Function(y) y.DATAROW)

            'Loan
            If arrLoan.Length > 0 Then
                dsLoanNotIN = objLoan.getComboList(False, "Loan", , "lnloan_scheme_master.loanschemeunkid NOT IN (" & String.Join(",", arrLoan) & ") ")
                dsLoanIN = objLoan.getComboList(False, "Loan", , "lnloan_scheme_master.loanschemeunkid IN (" & String.Join(",", arrLoan) & ") ")
                dsLoanNotIN.Tables(0).Columns("loanschemeunkid").ColumnName = "tranheadunkid"
                dsLoanNotIN.Tables(0).Columns("Code").ColumnName = "code"

                dsLoanIN.Tables(0).Columns("loanschemeunkid").ColumnName = "tranheadunkid"
                dsLoanIN.Tables(0).Columns("Code").ColumnName = "code"

                aLoan = (From p In dsLoanIN.Tables(0).AsEnumerable Select New With {.ID = p.Item("tranheadunkid").ToString, .DATAROW = p}).ToDictionary(Function(x) "Loan" + x.ID, Function(y) y.DATAROW)
                a = a.Union(aLoan).ToDictionary(Function(x) x.Key, Function(y) y.Value)
            ElseIf CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -1 Then
                dsLoanNotIN = objLoan.getComboList(False, "Loan")
                dsLoanNotIN.Tables(0).Columns("loanschemeunkid").ColumnName = "tranheadunkid"
                dsLoanNotIN.Tables(0).Columns("Code").ColumnName = "code"
            End If

            'Saving
            If arrSaving.Length > 0 Then
                dsSavingNotIN = objSaving.getComboList(False, "Saving", , "svsavingscheme_master.savingschemeunkid NOT IN (" & String.Join(",", arrSaving) & ") ")
                dsSavingIN = objSaving.getComboList(False, "Saving", , "svsavingscheme_master.savingschemeunkid  IN (" & String.Join(",", arrSaving) & ") ")
                dsSavingNotIN.Tables(0).Columns("savingschemeunkid").ColumnName = "tranheadunkid"

                dsSavingIN.Tables(0).Columns("savingschemeunkid").ColumnName = "tranheadunkid"

                aSaving = (From p In dsSavingIN.Tables(0).AsEnumerable Select New With {.ID = p.Item("tranheadunkid").ToString, .DATAROW = p}).ToDictionary(Function(x) "Saving" + x.ID, Function(y) y.DATAROW)
                a = a.Union(aSaving).ToDictionary(Function(x) x.Key, Function(y) y.Value)
            ElseIf CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -3 Then
                dsSavingNotIN = objSaving.getComboList(False, "Saving")
                dsSavingNotIN.Tables(0).Columns("savingschemeunkid").ColumnName = "tranheadunkid"
            End If

            If dsLoanNotIN IsNot Nothing Then
                Dim dc As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc.DefaultValue = -1
                dsLoanNotIN.Tables(0).Columns.Add(dc)
                dsLoanNotIN.Tables(0).Columns("code").SetOrdinal(1)

                dc = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dc.DefaultValue = False
                dsLoanNotIN.Tables(0).Columns.Add(dc)
                dsLoanNotIN.Tables(0).Columns("IsChecked").SetOrdinal(0)
            End If

            If dsLoanIN IsNot Nothing Then
                Dim dc1 As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc1.DefaultValue = -1
                dsLoanIN.Tables(0).Columns.Add(dc1)
                dsLoanIN.Tables(0).Columns("code").SetOrdinal(1)

                dc1 = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dc1.DefaultValue = True
                dsLoanIN.Tables(0).Columns.Add(dc1)
                dsLoanIN.Tables(0).Columns("IsChecked").SetOrdinal(0)
            End If

            If dsSavingNotIN IsNot Nothing Then
                Dim dc3 As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc3.DefaultValue = -3
                dsSavingNotIN.Tables(0).Columns.Add(dc3)
                dsSavingNotIN.Tables(0).Columns("code").SetOrdinal(1)

                dc3 = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dc3.DefaultValue = False
                dsSavingNotIN.Tables(0).Columns.Add(dc3)
                dsSavingNotIN.Tables(0).Columns("IsChecked").SetOrdinal(0)
            End If

            If dsSavingIN IsNot Nothing Then
                Dim dc4 As New DataColumn("trnheadtype_id", System.Type.GetType("System.Int32"))
                dc4.DefaultValue = -3
                dsSavingIN.Tables(0).Columns.Add(dc4)
                dsSavingIN.Tables(0).Columns("code").SetOrdinal(1)

                dc4 = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dc4.DefaultValue = True
                dsSavingIN.Tables(0).Columns.Add(dc4)
                dsSavingIN.Tables(0).Columns("IsChecked").SetOrdinal(0)
            End If
            'Sohail (03 Feb 2016) -- End

            dsList.Tables(0).Columns.Add("IsChecked", Type.GetType("System.Boolean")).SetOrdinal(0)
            For Each dsRow As DataRow In dsList.Tables(0).Rows
                dsRow.Item("IsChecked") = False
            Next
            dsList.Tables(0).AcceptChanges()

            ds.Tables(0).Columns.Add("IsChecked", Type.GetType("System.Boolean")).SetOrdinal(0)
            For Each dsRow As DataRow In ds.Tables(0).Rows
                dsRow.Item("IsChecked") = True
            Next
            ds.Tables(0).AcceptChanges()

            'Dim a As Dictionary(Of Integer, DataRow) = (From p In ds.Tables(0).AsEnumerable Select New With {.ID = CInt(p.Item("tranheadunkid")), .DATAROW = p}).ToDictionary(Function(x) CInt(x.ID), Function(y) y.DATAROW) 'Sohail (03 Feb 2016)

            dtTable.Merge(dsList.Tables(0), True)
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            If dsLoanNotIN IsNot Nothing Then
                If CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -1 Then
                    dtTable.Merge(dsLoanNotIN.Tables(0), True)
                End If
            End If

            If dsSavingNotIN IsNot Nothing Then
                If CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -3 Then
                    dtTable.Merge(dsSavingNotIN.Tables(0), True)
                End If
            End If

            'Advance
            Dim dr_Advance As DataRow = dtTable.NewRow
            dr_Advance.Item("tranheadunkid") = 1
            dr_Advance.Item("code") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Advance")
            dr_Advance.Item("name") = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Advance")
            dr_Advance.Item("trnheadtype_id") = -2

            If arrAdvance.Length > 0 Then
                dr_Advance.Item("IsChecked") = True
                a.Add("Advance1", dr_Advance)
            ElseIf CInt(cboTrnHeadType.SelectedValue) = 0 OrElse CInt(cboTrnHeadType.SelectedValue) = -2 Then
                dr_Advance.Item("IsChecked") = False
                dtTable.Rows.Add(dr_Advance)
            End If

            dtTable = New DataView(dtTable, "", "name", DataViewRowState.CurrentRows).ToTable
            'Sohail (03 Feb 2016) -- End
            Dim intPos As Integer = 0
            For Each itm As String In marrCustomPayrollReportHeadsIds
                Dim dr As DataRow = dtTable.NewRow
                'Sohail (03 Feb 2016) -- Start
                'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                'dr.ItemArray = a.Item(CInt(itm)).ItemArray
                dr.ItemArray = a.Item(itm).ItemArray
                'Sohail (03 Feb 2016) -- End
                dtTable.Rows.InsertAt(dr, intPos)
                intPos += 1
            Next

            Me.ViewState.Add("dtTranHead", dtTable)
            With lvCustomTranHead
                .DataSource = dtTable
                .DataBind()
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Shani [ 22 OCT 2014 ] -- END
#End Region

#Region " Page's Event "
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then

                Exit Sub
            End If

            'Sohail (27 Apr 2013) -- Start
            'TRA - ENHANCEMENT

            'Nilay (10-Feb-2016) -- Start
            'If ConfigParameter._Object._IsArutiDemo = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
            '    DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
            '    Exit Sub
            'End If

            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Nilay (10-Feb-2016) -- End

            'Sohail (27 Apr 2013) -- End


            If Not IsPostBack Then
                SetLanguage()
                Call FillCombo()
                'Sohail (29 Apr 2014) -- Start
                'Enhancement - Export Excel Report with Company Logo 
                Call CreateHeadListTable()
                Call FillList()
                Call CheckAllHeads(True)
                'Sohail (29 Apr 2014) -- End
                'Sohail (31 Jan 2014) -- Start
                ' Call 'Language.setLanguage("frmPayrollReport")
                lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), "frmPayrollReport", CInt(HttpContext.Current.Session("LangId")), lblEmployee.ID, Me.lblEmployee.Text)
                'Shani [ 22 NOV 2014 ] -- START
                'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
                marrCustomPayrollReportHeadsIds.Clear()
                If CStr(Session("CustomPayrollReportHeadsIds")).Trim.Length > 0 Then
                    marrCustomPayrollReportHeadsIds.AddRange(CStr(Session("CustomPayrollReportHeadsIds")).Split(CChar(",")))
                End If
                'Shani [ 22 NOV 2014 ] -- END
            Else
                mdecEx_Rate = CDec(ViewState("mdecEx_Rate"))
                mstrCurr_Sign = ViewState("mstrCurr_Sign").ToString

                mstrBaseCurrSign = ViewState("mstrBaseCurrSign").ToString
                mintBaseCurrId = CInt(ViewState("mintBaseCurrId"))
                mstrCurrency_Rate = ViewState("mstrCurrency_Rate").ToString

                mstrFromDatabaseName = ViewState("mstrFromDatabaseName").ToString
                mstrToDatabaseName = ViewState("mstrToDatabaseName").ToString
                mintFirstOpenPeriod = CInt(ViewState("mintFirstOpenPeriod"))
                'Sohail (31 Jan 2014) -- End

                mdtHeads = CType(ViewState("mdtHeads"), DataTable) 'Sohail (29 Apr 2014)

                'Sohail (20 May 2014) -- Start
                'Enhancement - Analysis By on Payroll Report
                mstrStringIds = ViewState("mstrStringIds").ToString
                mstrStringName = ViewState("mstrStringName").ToString
                mintViewIdx = CInt(ViewState("mintViewIdx"))
                mstrAnalysis_Fields = ViewState("mstrAnalysis_Fields").ToString
                mstrAnalysis_Join = ViewState("mstrAnalysis_Join").ToString
                mstrAnalysis_OrderBy = ViewState("mstrAnalysis_OrderBy").ToString
                mstrReport_GroupName = ViewState("mstrReport_GroupName").ToString
                'Sohail (20 May 2014) -- End

                'Shani [ 22 NOV 2014 ] -- START
                ''FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
                If Me.ViewState("CustomPayrollReportHeadsIds") IsNot Nothing Then
                    marrCustomPayrollReportHeadsIds = CType(Me.ViewState("CustomPayrollReportHeadsIds"), ArrayList)
                End If
                'Shani [ 22 NOV 2014 ] -- END
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Sohail (31 Jan 2014) -- Start
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.ViewState.Add("mdecEx_Rate", mdecEx_Rate)
        Me.ViewState.Add("mstrCurr_Sign", mstrCurr_Sign)

        Me.ViewState.Add("mstrBaseCurrSign", mstrBaseCurrSign)
        Me.ViewState.Add("mintBaseCurrId", mintBaseCurrId)
        Me.ViewState.Add("mstrCurrency_Rate", mstrCurrency_Rate)

        Me.ViewState.Add("mstrFromDatabaseName", mstrFromDatabaseName)
        Me.ViewState.Add("mstrToDatabaseName", mstrToDatabaseName)
        Me.ViewState.Add("mintFirstOpenPeriod", mintFirstOpenPeriod)

        Me.ViewState.Add("mdtHeads", mdtHeads) 'Sohail (29 Apr 2014)

        'Sohail (20 May 2014) -- Start
        'Enhancement - Analysis By on Payroll Report
        Me.ViewState.Add("mstrStringIds", mstrStringIds)
        Me.ViewState.Add("mstrStringName", mstrStringName)
        Me.ViewState.Add("mintViewIdx", mintViewIdx)
        Me.ViewState.Add("mstrAnalysis_Fields", mstrAnalysis_Fields)
        Me.ViewState.Add("mstrAnalysis_Join", mstrAnalysis_Join)
        Me.ViewState.Add("mstrAnalysis_OrderBy", mstrAnalysis_OrderBy)
        Me.ViewState.Add("mstrReport_GroupName", mstrReport_GroupName)
        'Sohail (20 May 2014) -- End

        'Shani [ 22 NOV 2014 ] -- START
        ''FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
        If Me.ViewState("CustomPayrollReportHeadsIds") Is Nothing Then
            Me.ViewState.Add("CustomPayrollReportHeadsIds", marrCustomPayrollReportHeadsIds)
        Else
            Me.ViewState("CustomPayrollReportHeadsIds") = marrCustomPayrollReportHeadsIds
        End If
        'Shani [ 22 NOV 2014 ] -- END
    End Sub
    'Sohail (31 Jan 2014) -- End

#End Region

#Region " Combobox's Events "
    Protected Sub ddlReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlReportType.SelectedIndexChanged
        Try
            'Sohail (01 Sep 2014) -- Start
            'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
            chkShowPaymentDetails.Visible = True
            'Sohail (01 Sep 2014) -- End

            'Sohail (26 Dec 2015) -- Start
            'Enhancement - Show Company Logo option on Payroll Report as SUm function was not working at FDRC for HTML as their french language in MS EXCEL.
            chkShowCompanyLogoOnReport.Visible = True
            'Sohail (26 Dec 2015) -- End

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
            lblMembership.Visible = False
            cboMembership.Visible = False
            chkShowEmpNameinSeperateColumn.Visible = False
            'Sohail (10 Jul 2019) -- End

            'Hemant (17 Sep 2020) -- Start
            'New UI Change
            pnlMembership.Visible = False
            'Hemant (17 Sep 2020) -- End

            Select Case ddlReportType.SelectedIndex
                Case 0 'Payroll Report
                    lblPeriod.Text = "Period"
                    lblToPeriod.Visible = False
                    drpPeriodTo.Visible = False

                    'Shani [ 22 OCT 2014 ] -- START
                    'Implementing "Custom Payroll Report" under Web As One of the Report Type
                    lvTranHead.Enabled = True
                    uppnlCustomReport.Visible = False
                    'Shani [ 22 OCT 2014 ] -- END

                    'Nilay (12-Oct-2016) -- Start
                    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
                    lblCostCenterCode.Visible = False
                    txtCostCenterCode.Text = ""
                    txtCostCenterCode.Visible = False
                    'Nilay (12-Oct-2016) -- End

                    'Hemant (17 Sep 2020) -- Start
                    'New UI Change
                    pnlCostCenterCode.Visible = False
                    'Hemant (17 Sep 2020) -- End

                    'Sohail (01 Sep 2014) -- Start
                    'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                    'Case 1 'Personal Salary Calculation
                Case 1, 2 'Personal Salary Calculation OR Employee Payroll Summary
                    'Sohail (01 Sep 2014) -- End
                    lblPeriod.Text = "From Period"
                    lblToPeriod.Visible = True
                    drpPeriodTo.Visible = True

                    'Sohail (01 Sep 2014) -- Start
                    'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
                    If ddlReportType.SelectedIndex = 2 Then
                        chkShowPaymentDetails.Checked = False
                        chkShowPaymentDetails.Visible = False
                    End If
                    'Sohail (01 Sep 2014) -- End

                    'Nilay (12-Oct-2016) -- Start
                    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
                    lblCostCenterCode.Visible = False
                    txtCostCenterCode.Text = ""
                    txtCostCenterCode.Visible = False
                    'Nilay (12-Oct-2016) -- End

                    'Hemant (17 Sep 2020) -- Start
                    'New UI Change
                    pnlCostCenterCode.Visible = False
                    'Hemant (17 Sep 2020) -- End

                    'Shani [ 22 OCT 2014 ] -- START
                    'Implementing "Custom Payroll Report" under Web As One of the Report Type
                    uppnlCustomReport.Visible = False
                    lvTranHead.Enabled = True
                Case 3 'Custom Payroll Report
                    lblPeriod.Text = "Period"
                    lblToPeriod.Visible = False
                    drpPeriodTo.Visible = False

                    lvTranHead.Enabled = False
                    uppnlCustomReport.Visible = True

                    'Shani [ 22 NOV 2014 ] -- START
                    'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
                    cboTrnHeadType.SelectedValue = "0"
                    marrCustomPayrollReportHeadsIds.Clear()
                    If CStr(Session("CustomPayrollReportHeadsIds")).Trim.Length > 0 Then
                        marrCustomPayrollReportHeadsIds.AddRange(CStr(Session("CustomPayrollReportHeadsIds")).Split(CChar(",")))
                    End If
                    'Shani [ 22 NOV 2014 ] -- END

                    lblCurrency.Visible = True
                    cboCurrency.Visible = True
                    cboCurrency.SelectedValue = "0"
                    lblExRate.Text = ""
                    lblExRate.Visible = True
                    Call Fill_Allocation()
                    Call FillCustomHeads()
                    'Shani [ 22 OCT 2014 ] -- END

                    'Sohail (26 Dec 2015) -- Start
                    'Enhancement - Show Company Logo option on Payroll Report as SUm function was not working at FDRC for HTML as their french language in MS EXCEL.
                    chkShowCompanyLogoOnReport.Checked = False
                    chkShowCompanyLogoOnReport.Visible = False
                    'Sohail (26 Dec 2015) -- End

                    'Nilay (12-Oct-2016) -- Start
                    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
                    lblCostCenterCode.Visible = False
                    txtCostCenterCode.Text = ""
                    txtCostCenterCode.Visible = False
                    'Nilay (12-Oct-2016) -- End

                    'Sohail (10 Jul 2019) -- Start
                    'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
                    lblMembership.Visible = True
                    cboMembership.Visible = True
                    chkShowEmpNameinSeperateColumn.Visible = True
                    'Sohail (10 Jul 2019) -- End

                    'Hemant (17 Sep 2020) -- Start
                    'New UI Change
                    pnlCostCenterCode.Visible = False
                    pnlMembership.Visible = True
                    'Hemant (17 Sep 2020) -- End

                    'Shani (20 Apr 2015) -- Start
                    'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
                Case 4
                    lblPeriod.Text = "Period"
                    lblToPeriod.Visible = False
                    drpPeriodTo.Visible = False
                    cboTrnHeadType.SelectedValue = "0"
                    'pnlTranHead.Enabled = False : txtSearchHead.Enabled = False
                    uppnlCustomReport.Visible = True
                    lblCurrency.Visible = True
                    cboCurrency.Visible = True
                    cboCurrency.SelectedValue = "0"
                    lblExRate.Text = ""
                    lblExRate.Visible = True
                    Call Fill_Allocation()
                    Call FillCustomHeads()
                    'Shani (20 Apr 2015) -- End

                    'Sohail (26 Dec 2015) -- Start
                    'Enhancement - Show Company Logo option on Payroll Report as SUm function was not working at FDRC for HTML as their french language in MS EXCEL.
                    chkShowCompanyLogoOnReport.Checked = False
                    chkShowCompanyLogoOnReport.Visible = False
                    'Sohail (26 Dec 2015) -- End

                    'Nilay (12-Oct-2016) -- Start
                    'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
                    lblCostCenterCode.Visible = True
                    txtCostCenterCode.Text = ""
                    txtCostCenterCode.Visible = True
                    'Nilay (12-Oct-2016) -- End

                    'Sohail (10 Jul 2019) -- Start
                    'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
                    lblMembership.Visible = True
                    cboMembership.Visible = True
                    chkShowEmpNameinSeperateColumn.Visible = True
                    'Sohail (10 Jul 2019) -- End

                    'Hemant (17 Sep 2020) -- Start
                    'New UI Change
                    pnlCostCenterCode.Visible = True
                    pnlMembership.Visible = True
                    'Hemant (17 Sep 2020) -- End

            End Select
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("ddlReportType_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    'Sohail (31 Jan 2014) -- Start
    Protected Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged, _
                                                                                                                drpPeriod.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            If cboCurrency.Visible = True Then
                If CInt(cboCurrency.SelectedValue) > 0 Then
                    Dim objExRate As New clsExchangeRate
                    objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriod.SelectedValue)
                    dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, objPeriod._End_Date.Date, True)
                    If dsList.Tables("ExRate").Rows.Count > 0 Then
                        mdecEx_Rate = CDec(dsList.Tables("ExRate").Rows(0)("exchange_rate"))
                        mstrCurr_Sign = dsList.Tables("ExRate").Rows(0)("currency_sign").ToString
                        lblExRate.Text = "Exchange Rate :" & " " & CDbl(mdecEx_Rate) & " "
                        mstrCurrency_Rate = Format(CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate1")), GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & CDec(dsList.Tables("ExRate").Rows(0).Item("exchange_rate2")).ToString & " " & mstrCurr_Sign & " " 'Sohail (16 Mar 2013)
                    Else
                        lblExRate.Text = "" : mdecEx_Rate = 0 : mstrCurr_Sign = ""
                    End If
                Else
                    mdecEx_Rate = 1 : lblExRate.Text = "" : mstrCurr_Sign = String.Empty
                    mstrCurrency_Rate = ""
                End If
            End If

            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriod.SelectedValue)
            dsList = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrFromDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrFromDatabaseName = ""
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboCurrency_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try
    End Sub

    Protected Sub drpPeriodTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles drpPeriodTo.SelectedIndexChanged
        Dim objPeriod As New clscommom_period_Tran
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Try
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriodTo.SelectedValue)
            dsList = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "Database", objPeriod._Yearunkid)
            If dsList.Tables("Database").Rows.Count > 0 Then
                mstrToDatabaseName = dsList.Tables("Database").Rows(0).Item("database_name").ToString
            Else
                mstrToDatabaseName = ""
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("drpPeriodTo_SelectedIndexChanged:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        Finally
            objPeriod = Nothing
            objCompany = Nothing
        End Try
    End Sub
    'Sohail (31 Jan 2014) -- End


    'Shani [ 22 NOV 2014 ] -- START
    'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
    Protected Sub cboTrnHeadType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTrnHeadType.SelectedIndexChanged
        Try
            Call FillCustomHeads()
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("cboTrnHeadType_SelectedIndexChanged :-" & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Shani [ 22 NOV 2014 ] -- END

#End Region

#Region " Button's Event(s) "

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click, Closebutton1.CloseButton_click
    '    Try
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
    '    End Try
    'End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnClose_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Nilay (01-Feb-2015) -- End



    Protected Sub btnPreview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        'Sohail (20 Aug 2014) -- Start
        'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
        Dim objCompany As New clsCompany_Master
        Dim dsList As DataSet
        Dim arrDatabaseName As New ArrayList
        Dim intPrevYearID As Integer = 0
        'Sohail (20 Aug 2014) -- End

        Dim mdicYearDBName As New Dictionary(Of Integer, String) 'Sohail (21 Aug 2015)

        Try
            If IsValidate() = False Then
                Exit Sub
            End If

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            Dim objPayroll As New clsPayrollReport(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            objPayroll.SetDefaultValue()

            objPayroll._ReportId = ddlReportType.SelectedIndex
            objPayroll._ReportTypeName = ddlReportType.SelectedItem.Text
            objPayroll._CurrentDatabaseName = Session("Database_Name").ToString

            'Sohail (01 Sep 2014) -- Start
            'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
            'If ddlReportType.SelectedIndex = 1 Then 'Personal Salary Calculation Report
            If ddlReportType.SelectedIndex = 1 OrElse ddlReportType.SelectedIndex = 2 Then 'Personal Salary Calculation Report
                'Sohail (01 Sep 2014) -- End
                objPayroll._ToPeriodId = CInt(drpPeriodTo.SelectedValue)
                objPayroll._ToPeriodName = drpPeriodTo.SelectedItem.Text
            Else
                objPayroll._ToPeriodId = 0
                objPayroll._ToPeriodName = ""
            End If

            Dim i As Integer = 0
            Dim strList As String = drpPeriod.SelectedValue.ToString
            'Sohail (20 Aug 2014) -- Start
            'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
            dsList = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "Database", CInt(CType(ViewState("dsPeriod"), DataTable).Select("periodunkid = " & CInt(drpPeriod.SelectedValue) & " ")(0).Item("yearunkid")))
            If dsList.Tables("Database").Rows.Count > 0 Then
                arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                intPrevYearID = CInt(CType(ViewState("dsPeriod"), DataTable).Select("periodunkid = " & CInt(drpPeriod.SelectedValue) & " ")(0).Item("yearunkid"))
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                If mdicYearDBName.ContainsKey(intPrevYearID) = False Then
                    mdicYearDBName.Add(intPrevYearID, dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                End If
                'Sohail (21 Aug 2015) -- End
            End If
            'Sohail (20 Aug 2014) -- End
            'Sohail (01 Sep 2014) -- Start
            'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
            'If ddlReportType.SelectedIndex = 1 Then 'Personal Salary Calculation Report
            If ddlReportType.SelectedIndex = 1 OrElse ddlReportType.SelectedIndex = 2 Then 'Personal Salary Calculation Report OR Employee Payroll Summary
                'Sohail (01 Sep 2014) -- End
                For Each dsRow As DataRow In CType(ViewState("dsPeriod"), DataTable).Rows
                    If i > drpPeriod.SelectedIndex AndAlso i <= drpPeriodTo.SelectedIndex Then
                        strList &= ", " & dsRow.Item("periodunkid").ToString
                        'Sohail (20 Aug 2014) -- Start
                        'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
                        If intPrevYearID <> CInt(dsRow.Item("yearunkid")) Then
                            dsList = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "Database", CInt(dsRow.Item("yearunkid")))
                            If dsList.Tables("Database").Rows.Count > 0 Then
                                arrDatabaseName.Add(dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                If mdicYearDBName.ContainsKey(CInt(dsRow.Item("yearunkid"))) = False Then
                                    mdicYearDBName.Add(CInt(dsRow.Item("yearunkid")), dsList.Tables("Database").Rows(0).Item("database_name").ToString)
                                End If
                                'Sohail (21 Aug 2015) -- End
                            End If
                        End If
                        intPrevYearID = CInt(dsRow.Item("yearunkid"))
                        'Sohail (20 Aug 2014) -- End
                    End If
                    i += 1
                Next
            End If

            objPayroll._PeriodIdList = strList
            'Sohail (20 Aug 2014) -- Start
            'Enhancement - Show Payment Details on Payroll Report and ED Spreadsheet Report.
            objPayroll._Arr_DatabaseName = arrDatabaseName
            objPayroll._ShowPaymentDetails = chkShowPaymentDetails.Checked
            'Sohail (20 Aug 2014) -- End
            objPayroll._IncludeEmployerContribution = chkIncludeEmployerContribution.Checked 'Sohail (12 Sep 2014)

            'Sohail (29 Apr 2014) -- Start
            'Enhancement - Export Excel Report with Company Logo 
            'objPayroll._EmployeeId = CInt(Session("Employeeunkid"))
            'objPayroll._EmployeeName = Session("UserName")
            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                objPayroll._EmployeeId = CInt(Session("Employeeunkid"))
                objPayroll._EmployeeName = Session("UserName").ToString
            Else
                objPayroll._EmployeeId = CInt(cboEmployee.SelectedValue)
                objPayroll._EmployeeName = cboEmployee.SelectedItem.Text
            End If

            'Sohail (29 Apr 2014) -- End

            objPayroll._PeriodId = CInt(drpPeriod.SelectedValue)
            objPayroll._PeriodName = drpPeriod.SelectedItem.Text

            'Sohail (29 Apr 2014) -- Start
            'Enhancement - Export Excel Report with Company Logo 
            Dim lstUnSelected As List(Of String) = (From p In mdtHeads Where CBool(p.Item("IsChecked")) = False Select (p.Item("tranheadunkid").ToString)).ToList
            Dim strIDs As String = ""
            If lstUnSelected.Count > 0 Then
                strIDs = String.Join(",", lstUnSelected.ToArray)
            End If
            objPayroll._UnSelectedHeadIDs = strIDs
            'Sohail (29 Apr 2014) -- End

            objPayroll._IsActive = chkInactiveemp.Checked


            'Sohail (31 Jan 2014) -- Start
            'objPayroll._BranchId = 0
            'objPayroll._Branch_Name = ""
            objPayroll._BranchId = CInt(cboBranch.SelectedValue)
            objPayroll._Branch_Name = cboBranch.Text
            'Sohail (31 Jan 2014) -- End

            'Sohail (20 May 2014) -- Start
            'Enhancement - Analysis By on Payroll Report
            'objPayroll._ViewByIds = ""
            'objPayroll._ViewIndex = 0
            'objPayroll._ViewByName = ""
            'objPayroll._Analysis_Fields = ""
            'objPayroll._Analysis_Join = ""
            'objPayroll._Analysis_OrderBy = ""
            'objPayroll._Report_GroupName = ""
            objPayroll._ViewByIds = mstrStringIds
            objPayroll._ViewIndex = mintViewIdx
            objPayroll._ViewByName = mstrStringName
            objPayroll._Analysis_Fields = mstrAnalysis_Fields
            objPayroll._Analysis_Join = mstrAnalysis_Join
            objPayroll._Analysis_OrderBy = mstrAnalysis_OrderBy
            objPayroll._Report_GroupName = mstrReport_GroupName
            'Sohail (20 May 2014) -- End

            objPayroll._IgnoreZeroHeads = chkIgnorezeroHead.Checked

            Dim intFromYearUnkId As Integer = 0
            Dim intToYearUnkId As Integer = 0
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriod.SelectedValue)
            objPayroll._PeriodStartDate = objPeriod._Start_Date
            intFromYearUnkId = objPeriod._Yearunkid

            'Sohail (01 Sep 2014) -- Start
            'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
            'If ddlReportType.SelectedIndex = 1 Then 'Personal Salary Calculation Report
            If ddlReportType.SelectedIndex = 1 OrElse ddlReportType.SelectedIndex = 2 Then 'Personal Salary Calculation Report
                'Sohail (01 Sep 2014) -- End
                objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriodTo.SelectedValue)
                intToYearUnkId = objPeriod._Yearunkid
            End If
            objPayroll._PeriodEndDate = objPeriod._End_Date

            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            objPayroll._ShowLoansInSeparateColumns = chkShowLoansInSeparateColumns.Checked
            objPayroll._ShowSavingsInSeparateColumns = chkShowSavingsInSeparateColumns.Checked
            'Sohail (03 Feb 2016) -- End

            'Sohail (31 Jan 2014) -- Start
            'objPayroll._Ex_Rate = 1
            'objPayroll._Currency_Sign = ""
            'objPayroll._Currency_Rate = 0
            objPayroll._Ex_Rate = mdecEx_Rate
            objPayroll._Currency_Sign = mstrCurr_Sign
            objPayroll._Currency_Rate = mstrCurrency_Rate
            'Sohail (31 Jan 2014) -- End

            'Sohail (31 Jan 2014) -- Start
            'Dim objCompany As New clsCompany_Master
            'Dim dsCombo As DataSet
            'Dim mstrFromDatabaseName As String = ""
            'Dim mstrToDatabaseName As String = ""
            'dsCombo = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "List", intFromYearUnkId)
            'If dsCombo.Tables("List").Rows.Count > 0 Then
            '    mstrFromDatabaseName = dsCombo.Tables("List").Rows(0).Item("database_name").ToString
            'End If

            'objPayroll._FromDatabaseName = mstrFromDatabaseName

            'If ddlReportType.SelectedIndex = 1 Then 'Personal Salary Calculation Report
            '    dsCombo = objCompany.GetFinancialYearList(CInt(Session("CompanyUnkId")), , "List", intToYearUnkId)
            '    If dsCombo.Tables("List").Rows.Count > 0 Then
            '        mstrToDatabaseName = dsCombo.Tables("List").Rows(0).Item("database_name").ToString
            '    End If
            '    objPayroll._ToDatabaseName = mstrToDatabaseName
            'Else
            '    objPayroll._ToDatabaseName = ""
            'End If
            objPayroll._FromDatabaseName = mstrFromDatabaseName
            'Sohail (01 Sep 2014) -- Start
            'Enhancement - New Report Type Employee Payroll Summary in Payroll Report.
            'If ddlReportType.SelectedIndex = 1 Then 'Personal Salary Calculation Report
            If ddlReportType.SelectedIndex = 1 OrElse ddlReportType.SelectedIndex = 2 Then 'Personal Salary Calculation Report
                'Sohail (01 Sep 2014) -- End
                objPayroll._ToDatabaseName = mstrToDatabaseName
            Else
                objPayroll._ToDatabaseName = ""
            End If
            'Sohail (31 Jan 2014) -- End


            objPayroll._Advance_Filter = ""

            'Sohail (10 Jul 2019) -- Start
            'Good Neighbours Enhancement - Support Issue Id # 3876 - 76.1 - Put membership number in custom payroll report -& separate employee name as First Name, Middle Name and Last Name.
            objPayroll._ShowEmpNameInSeprateColumns = chkShowEmpNameinSeperateColumn.Checked
            objPayroll._MembershipUnkId = CInt(cboMembership.SelectedValue)
            objPayroll._MembershipName = cboMembership.SelectedItem.Text
            'Sohail (10 Jul 2019) -- End

            'Sohail (13 Feb 2016) -- Start
            'Enhancement - Report was not coming from ESS in 58.1 due to UserUnkId = -1 passed in NewAccessLevelFilter method.
            Dim blnSelect As Boolean = True
            Dim blnApplyAccess As Boolean = True
            Dim intEmpUnkId As Integer = 0

            If CInt(Session("LoginBy")) = Global.User.en_loginby.Employee Then
                blnSelect = False
                blnApplyAccess = False
                intEmpUnkId = CInt(Session("Employeeunkid"))
            End If
            objPayroll._ApplyUserAccessFilter = blnApplyAccess
            'Sohail ((13 Feb 2016) -- End

            GUI.fmtCurrency = Session("fmtCurrency").ToString

            objPayroll._Base_CurrencyId = CInt(Session("Base_CurrencyId"))
            objPayroll._SetPayslipPaymentApproval = CBool(Session("SetPayslipPaymentApproval"))
            objPayroll._fmtCurrency = Session("fmtCurrency").ToString
            'objPayroll._ExportReportPath = Server.MapPath("../Documents")
            objPayroll._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            objPayroll._UserAccessFilter = Session("AccessLevelFilterString").ToString 'Sohail (31 Jan 2014)

            objPayroll._OpenAfterExport = False

            'Nilay (12-Oct-2016) -- Start
            'CCK Enhancements -  Salary Journal - To include cost center codes in custom payroll report
            If ddlReportType.SelectedIndex = 4 Then
                objPayroll._CostCenterCode = txtCostCenterCode.Text.Trim
            End If
            'Nilay (12-Oct-2016) -- End

            'Sohail (03 Aug 2019) -- Start
            'PAYTECH KENYA issue # 0004019 - 76.1 - Inaccurate data on payroll reports i.e payroll summary report When an employee is moved between departments/branches, payroll report captures only the latest transfer records regardless of the period when payroll was done. System should pick the allocation based on period of transfer without overriding the former transfer..
            objPayroll._Analysis_Join = mstrAnalysis_Join.Replace(Session("EmployeeAsOnDate").ToString, eZeeDate.convertDate(objPeriod._End_Date))
            'Sohail (03 Aug 2019) -- End

            'Sohail (29 Apr 2014) -- Start
            'Enhancement - Export Excel Report with Company Logo 
            'objPayroll.Generate_PayrollReport()

            'Shani [ 22 OCT 2014 ] -- START
            'Implementing "Custom Payroll Report" under Web As One of the Report Type
            'objPayroll.Generate_PayrollReport(True)

            'SHANI (20 APR 2015)-START
            'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
            'If ddlReportType.SelectedIndex = 3 Then
            If ddlReportType.SelectedIndex = 3 OrElse ddlReportType.SelectedIndex = 4 Then
                'SHANI (20 APR 2015)--END 
                Dim result As Dictionary(Of Integer, String) = (From p In CType(Me.ViewState("dtAllocation"), DataTable).AsEnumerable() Where p.Field(Of Boolean)("IsChecked") = True Select p).ToDictionary(Function(row) CInt(row("Id")), Function(row) row("Name").ToString())
                objPayroll._AllocationDetails = result

                Dim xStr As String = ""
                'Sohail (03 Feb 2016) -- Start
                'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
                'Dim selectedTags As List(Of String) = (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where p.Field(Of Boolean)("IsChecked") = True Select (p.Item("tranheadunkid").ToString)).ToList
                'xStr = String.Join(",", selectedTags.ToArray())
                xStr = String.Join(",", (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where (p.Field(Of Boolean)("IsChecked") = True AndAlso CInt(p.Item("trnheadtype_id")) > 0) Select (p.Item("tranheadunkid").ToString)).ToArray _
                                            .Union _
                                            (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where (p.Field(Of Boolean)("IsChecked") = True AndAlso CInt(p.Item("trnheadtype_id")) = -1) Select ("Loan" + p.Item("tranheadunkid").ToString)).ToArray _
                                            .Union _
                                            (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where (p.Field(Of Boolean)("IsChecked") = True AndAlso CInt(p.Item("trnheadtype_id")) = -2) Select ("Advance" + p.Item("tranheadunkid").ToString)).ToArray _
                                            .Union _
                                            (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where (p.Field(Of Boolean)("IsChecked") = True AndAlso CInt(p.Item("trnheadtype_id")) = -3) Select ("Saving" + p.Item("tranheadunkid").ToString)).ToArray)
                'Sohail (03 Feb 2016) -- End
                If xStr.Trim.Length > 0 Then
                    objPayroll._CustomReportHeadsIds = xStr
                Else
                    DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Please tick atleast one transaction head in order to save."), Me)
                    Exit Sub
                End If

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objPayroll.Generate_CustomPayrollHeadsReport()
                'Sohail (09 Feb 2016) -- Start
                'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
                'objPayroll.Generate_CustomPayrollHeadsReport(mdicYearDBName, _
                '                                             Session("UserId"), _
                '                                             Session("CompanyUnkId"), _
                '                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                             eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                             Session("UserAccessModeSetting"), True, _
                '                                             chkInactiveemp.Checked, True, _
                '                                             Session("Base_CurrencyId"), _
                '                                             Session("SetPayslipPaymentApproval"), _
                '                                             Session("fmtCurrency"), _
                '                                             Session("ExportReportPath"), _
                '                                             Session("OpenAfterExport"))
                objPayroll.Generate_CustomPayrollHeadsReport(mdicYearDBName, _
                                                             CInt(Session("UserId")), _
                                                             CInt(Session("CompanyUnkId")), _
                                                             objPeriod._Start_Date, _
                                                             objPeriod._End_Date, _
                                                             Session("UserAccessModeSetting").ToString, True, _
                                                             chkInactiveemp.Checked, True, _
                                                             CInt(Session("Base_CurrencyId")), _
                                                             CBool(Session("SetPayslipPaymentApproval")), _
                                                             Session("fmtCurrency").ToString, _
                                                             IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                             False)
                'Sohail (27 Oct 2021) - [Session("OpenAfterExport") = False]
                'Sohail (09 Feb 2016) -- End
                'Shani(20-Nov-2015) -- End

            Else

                'Shani(20-Nov-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objPayroll.Generate_PayrollReport(True)
                'Sohail (26 Dec 2015) -- Start
                'Enhancement - Show Company Logo option on Payroll Report as SUm function was not working at FDRC for HTML as their french language in MS EXCEL.
                'objPayroll.Generate_PayrollReport(mdicYearDBName, _
                '                                  Session("UserId"), _
                '                                  Session("CompanyUnkId"), _
                '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                  Session("UserAccessModeSetting"), True, _
                '                                  chkInactiveemp.Checked, True, _
                '                                  Session("Base_CurrencyId"), _
                '                                  Session("SetPayslipPaymentApproval"), _
                '                                  Session("fmtCurrency"), _
                '                                  Session("ExportReportPath"), _
                '                                  Session("OpenAfterExport"), True)
                'Sohail (09 Feb 2016) -- Start
                'Enhancement - Provide 3 Logo options (Company Details with Logo, Company Details and Logo) on E & D Detail Report in 58.1 for KBC.
                'objPayroll.Generate_PayrollReport(mdicYearDBName, _
                '                                  Session("UserId"), _
                '                                  Session("CompanyUnkId"), _
                '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                  eZeeDate.convertDate(Session("EmployeeAsOnDate")), _
                '                                  Session("UserAccessModeSetting"), True, _
                '                                  chkInactiveemp.Checked, True, _
                '                                  Session("Base_CurrencyId"), _
                '                                  Session("SetPayslipPaymentApproval"), _
                '                                  Session("fmtCurrency"), _
                '                                  Session("ExportReportPath"), _
                '                                  Session("OpenAfterExport"), chkShowCompanyLogoOnReport.Checked)
                'Sohail (01 Mar 2016) -- Start
                'Enhancement - Rounding issue for Net Pay on Payroll Report, not matching with Payslip Report.
                'objPayroll.Generate_PayrollReport(mdicYearDBName, _
                '                                  CInt(Session("UserId")), _
                '                                  CInt(Session("CompanyUnkId")), _
                '                                  objPeriod._Start_Date, _
                '                                  objPeriod._End_Date, _
                '                                  Session("UserAccessModeSetting").ToString, True, _
                '                                  chkInactiveemp.Checked, True, _
                '                                  CInt(Session("Base_CurrencyId")), _
                '                                  CBool(Session("SetPayslipPaymentApproval")), _
                '                                  Session("fmtCurrency").ToString, _
                '                                  IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                '                                  CBool(Session("OpenAfterExport")), chkShowCompanyLogoOnReport.Checked)
                GC.Collect()
                objPayroll.Generate_PayrollReport(mdicYearDBName, _
                                                  CInt(Session("UserId")), _
                                                  CInt(Session("CompanyUnkId")), _
                                                  objPeriod._Start_Date, _
                                                  objPeriod._End_Date, _
                                                  Session("UserAccessModeSetting").ToString, True, _
                                                  chkInactiveemp.Checked, True, _
                                                  CInt(Session("Base_CurrencyId")), _
                                                  CBool(Session("SetPayslipPaymentApproval")), _
                                                  Session("fmtCurrency").ToString, _
                                                  IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp), _
                                                  False, chkShowCompanyLogoOnReport.Checked, _
                                                  CDbl(Session("RoundOff_Type")) _
                                                  )
                'Sohail (27 Oct 2021) - [Session("OpenAfterExport") = False]
                'Sohail (01 Mar 2016) -- End
                'Sohail (09 Feb 2016) -- End
                'Shani(20-Nov-2015) -- End
                'Sohail (26 Dec 2015) -- End 
            End If

            'Shani [ 22 OCT 2014 ] -- END


            'Sohail (29 Apr 2014) -- End


            If objPayroll._FileNameAfterExported.Trim <> "" Then
                Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objPayroll._FileNameAfterExported


                'Shani [ 18 NOV 2014 ] -- START
                'Issue : Chrome is not supporting ShowModalDialog option now."
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('../Reports/default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End
                'Shani [ 18 NOV 2014 ] -- END
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("btnPreview_Click:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

#End Region

    'Sohail (29 Apr 2014) -- Start
    'Enhancement - Export Excel Report with Company Logo 
#Region " GridView Events "
    Protected Sub lvTranHead_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles lvTranHead.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                If e.Row.Cells(2).Text = "None" AndAlso e.Row.Cells(1).Text = "" Then
                    e.Row.Visible = False
                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("lvTranHead_RowDataBound:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
#End Region
    'Sohail (29 Apr 2014) -- End

    'Sohail (20 May 2014) -- Start
    'Enhancement - Analysis By on Payroll Report
#Region " Other Controls Events "

    Protected Sub lnkAnalysisBy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalysisBy.Click
        Try
            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(drpPeriod.SelectedValue)
            popupAnalysisBy._EffectiveDate = objPeriod._End_Date
            'Sohail (23 May 2017) -- End
            popupAnalysisBy.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub popupAnalysisBy_buttonApply_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonApply_Click
        Try
            mstrStringIds = popupAnalysisBy._ReportBy_Ids
            mstrStringName = popupAnalysisBy._ReportBy_Name
            mintViewIdx = popupAnalysisBy._ViewIndex
            mstrAnalysis_Fields = popupAnalysisBy._Analysis_Fields
            mstrAnalysis_Join = popupAnalysisBy._Analysis_Join
            mstrAnalysis_OrderBy = popupAnalysisBy._Analysis_OrderBy
            mstrReport_GroupName = popupAnalysisBy._Report_GroupName
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Protected Sub popupAnalysisBy_buttonClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles popupAnalysisBy.buttonClose_Click
        Try
            mstrStringIds = ""
            mstrStringName = ""
            mintViewIdx = 0
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Shani [ 22 OCT 2014 ] -- START
    'Implementing "Custom Payroll Report" under Web As One of the Report Type
    Protected Sub chkAllocationSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkAllocationSelectAll As CheckBox = TryCast(sender, CheckBox)
            Dim dtAllocation As DataTable = CType(Me.ViewState("dtAllocation"), DataTable)
            If chkAllocationSelectAll Is Nothing Then Exit Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In lvAllocation_hierarchy.Rows
                cb = CType(lvAllocation_hierarchy.Rows(gvr.RowIndex).FindControl("chkAllcationSelect"), CheckBox)
                cb.Checked = chkAllocationSelectAll.Checked
            Next
            For Each gvr As DataRow In dtAllocation.Rows
                gvr("IsChecked") = chkAllocationSelectAll.Checked
            Next
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkHeadSelectAll_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chkHeadSelectAll As CheckBox = TryCast(sender, CheckBox)
            Dim dtTranHead As DataTable = CType(Me.ViewState("dtTranHead"), DataTable)
            If chkHeadSelectAll Is Nothing Then Exit Try
            Dim cb As CheckBox
            For Each gvr As GridViewRow In lvCustomTranHead.Rows
                cb = CType(lvCustomTranHead.Rows(gvr.RowIndex).FindControl("chkHeadSelect"), CheckBox)
                cb.Checked = chkHeadSelectAll.Checked
            Next
            For Each gvr As DataRow In dtTranHead.Rows
                gvr("IsChecked") = chkHeadSelectAll.Checked
            Next
            dtTranHead.AcceptChanges()
            'Shani [ 22 NOV 2014 ] -- START
            'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
            marrCustomPayrollReportHeadsIds.Clear()
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'marrCustomPayrollReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True) Select (p("tranheadunkid").ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True AndAlso CInt(p.Item("trnheadtype_id")) > 0) Select (p.Item("tranheadunkid").ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True AndAlso CInt(p.Item("trnheadtype_id")) = -1) Select ("Loan" + p.Item("tranheadunkid").ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True AndAlso CInt(p.Item("trnheadtype_id")) = -2) Select ("Advance" + p.Item("tranheadunkid").ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True AndAlso CInt(p.Item("trnheadtype_id")) = -3) Select ("Saving" + p.Item("tranheadunkid").ToString)).ToArray)
            'Sohail (03 Feb 2016) -- End
            'Shani [ 22 NOV 2014 ] -- END

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkHeadSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = TryCast(sender, CheckBox)
            Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                Dim drRow() As DataRow = CType(ViewState("dtTranHead"), DataTable).Select("tranheadunkid = '" & lvCustomTranHead.DataKeys(gvRow.RowIndex).Value.ToString & "'")
                If drRow.Length > 0 Then
                    drRow(0)("IsChecked") = cb.Checked
                    drRow(0).AcceptChanges()
                End If
            End If
            'Shani [ 22 NOV 2014 ] -- START
            'FDRC Enhancemwnt - Provide Transaction Head Type filter on Custom Payroll Report.
            marrCustomPayrollReportHeadsIds.Clear()
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'marrCustomPayrollReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True) Select (p("tranheadunkid").ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True AndAlso CInt(p.Item("trnheadtype_id")) > 0) Select (p.Item("tranheadunkid").ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True AndAlso CInt(p.Item("trnheadtype_id")) = -1) Select ("Loan" + p.Item("tranheadunkid").ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True AndAlso CInt(p.Item("trnheadtype_id")) = -2) Select ("Advance" + p.Item("tranheadunkid").ToString)).ToArray)
            marrCustomPayrollReportHeadsIds.AddRange((From p In CType(ViewState("dtTranHead"), DataTable) Where (CBool(p.Item("Ischecked")) = True AndAlso CInt(p.Item("trnheadtype_id")) = -3) Select ("Saving" + p.Item("tranheadunkid").ToString)).ToArray)
            'Sohail (03 Feb 2016) -- End
            'Shani [ 22 NOV 2014 ] -- END
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub chkAllocationSelect_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim cb As CheckBox = TryCast(sender, CheckBox)
            Dim gvRow As GridViewRow = CType(cb.NamingContainer, GridViewRow)
            If gvRow.Cells.Count > 0 Then
                Dim drRow() As DataRow = CType(ViewState("dtAllocation"), DataTable).Select("Id = '" & lvAllocation_hierarchy.DataKeys(gvRow.RowIndex).Value.ToString & "'")
                If drRow.Length > 0 Then
                    drRow(0)("IsChecked") = cb.Checked
                    drRow(0).AcceptChanges()
                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChangeAllocationLocation(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim commandArgument As String = TryCast(sender, LinkButton).CommandArgument
            Dim rowIndex As Integer = TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex
            Dim dtAllocation As DataTable = CType(Me.ViewState("dtAllocation"), DataTable)
            Dim xRow As DataRow = dtAllocation.Rows(rowIndex)
            Dim xNewRow As DataRow = dtAllocation.NewRow
            xNewRow.ItemArray = xRow.ItemArray
            If commandArgument = "down" Then
                If rowIndex >= lvAllocation_hierarchy.Rows.Count - 1 Then Exit Sub
                dtAllocation.Rows.RemoveAt(rowIndex)
                dtAllocation.Rows.InsertAt(xNewRow, rowIndex + 1)
                dtAllocation.Rows.IndexOf(xNewRow)
            ElseIf commandArgument = "up" Then
                If rowIndex <= 0 Then Exit Sub
                dtAllocation.Rows.Remove(xRow)
                dtAllocation.Rows.InsertAt(xNewRow, rowIndex - 1)
                dtAllocation.Rows.IndexOf(xNewRow)
            End If
            With lvAllocation_hierarchy
                .DataSource = dtAllocation
                .DataBind()
            End With
            dtAllocation.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub ChangeTranHeadLocation(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim commandArgument As String = TryCast(sender, LinkButton).CommandArgument
            Dim rowIndex As Integer = TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex
            Dim dtTtanHead As DataTable = CType(Me.ViewState("dtTranHead"), DataTable)
            Dim xRow As DataRow = dtTtanHead.Rows(rowIndex)
            Dim xNewRow As DataRow = dtTtanHead.NewRow
            xNewRow.ItemArray = xRow.ItemArray
            If commandArgument = "down" Then
                If rowIndex >= lvCustomTranHead.Rows.Count - 1 Then Exit Sub
                dtTtanHead.Rows.RemoveAt(rowIndex)
                dtTtanHead.Rows.InsertAt(xNewRow, rowIndex + 1)
                dtTtanHead.Rows.IndexOf(xNewRow)
            ElseIf commandArgument = "up" Then
                If rowIndex <= 0 Then Exit Sub
                dtTtanHead.Rows.Remove(xRow)
                dtTtanHead.Rows.InsertAt(xNewRow, rowIndex - 1)
                dtTtanHead.Rows.IndexOf(xNewRow)
            End If
            With lvCustomTranHead
                .DataSource = dtTtanHead
                .DataBind()
            End With
            dtTtanHead.AcceptChanges()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub lnkCustomSettings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCustomSettings.Click
        Try
            Dim xStr As String = ""
            Dim objConfig As New clsConfigOptions
            objConfig._Companyunkid = CInt(Session("Companyunkid"))

            Dim selectedTags As List(Of String) = (From p In CType(Me.ViewState("dtAllocation"), DataTable).AsEnumerable() Where p.Field(Of Boolean)("IsChecked") = True Select (p.Item("Id").ToString)).ToList
            xStr = String.Join(",", selectedTags.ToArray())
            'SHANI (20 Apr 2015) -- Start
            'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
            'objConfig._CustomPayrollReportAllocIds = xStr
            If xStr.Trim.Length > 0 Then
                If ddlReportType.SelectedIndex = 4 Then
                    objConfig._CustomPayrollReportTemplate2AllocIds = xStr
                Else 'Custom Payroll Report
                    objConfig._CustomPayrollReportAllocIds = xStr
                End If
            End If

            'SHANI (20 Apr 2015) -- END

            xStr = "" : selectedTags = Nothing
            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'selectedTags = (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where p.Field(Of Boolean)("IsChecked") = True Select (p.Item("tranheadunkid").ToString)).ToList
            'xStr = String.Join(",", selectedTags.ToArray())
            xStr = String.Join(",", (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where (CBool(p.Item("IsChecked")) = True AndAlso CInt(p.Item("trnheadtype_id")) > 0) Select (p.Item("tranheadunkid").ToString)).ToArray _
                                    .Union _
                                    (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where (CBool(p.Item("IsChecked")) = True AndAlso CInt(p.Item("trnheadtype_id")) = -1) Select ("Loan" + p.Item("tranheadunkid").ToString)).ToArray _
                                    .Union _
                                    (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where (CBool(p.Item("IsChecked")) = True AndAlso CInt(p.Item("trnheadtype_id")) = -2) Select ("Advance" + p.Item("tranheadunkid").ToString)).ToArray _
                                    .Union _
                                    (From p In CType(Me.ViewState("dtTranHead"), DataTable).AsEnumerable() Where (CBool(p.Item("IsChecked")) = True AndAlso CInt(p.Item("trnheadtype_id")) = -3) Select ("Saving" + p.Item("tranheadunkid").ToString)).ToArray)
            'Sohail (03 Feb 2016) -- End
            If xStr.Trim.Length > 0 Then
                'SHANI (20 APR 2015)-START
                'KVTC Enhancement - New Report Custom Payroll Report Template 2 in Payroll Report. (Email From: Rutta Anatory, Subject: KVTC, Sent: Monday, March 02, 2015 1:13 PM)
                'objConfig._CustomPayrollReportHeadsIds = xStr
                If ddlReportType.SelectedIndex = 4 Then
                    objConfig._CustomPayrollReportTemplate2HeadsIds = xStr
                Else
                    objConfig._CustomPayrollReportHeadsIds = xStr
                End If
                'SHANI (20 APR 2015)--END 
            Else
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 11, "Sorry, Please tick atleast one transaction head in order to save."), Me)
                Exit Sub
            End If


            Session("CustomPayrollReportAllocIds") = objConfig._CustomPayrollReportAllocIds
            Session("CustomPayrollReportHeadsIds") = objConfig._CustomPayrollReportHeadsIds
            Session("CustomPayrollReportTemplate2AllocIds") = objConfig._CustomPayrollReportTemplate2AllocIds
            Session("CustomPayrollReportTemplate2HeadsIds") = objConfig._CustomPayrollReportTemplate2HeadsIds
            objConfig.updateParam()
            objConfig = Nothing
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub txtSearchTranHeads_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchTranHeads.TextChanged
        Try
            Dim dtTranHead As DataTable = CType(Me.ViewState("dtTranHead"), DataTable)
            If dtTranHead.Rows.Count <= 0 Then Exit Sub
            Dim dtView As DataView = dtTranHead.DefaultView
            dtView.RowFilter = "Name LIKE '%" & txtSearchTranHeads.Text & "%' OR Code LIKE '%" & txtSearchTranHeads.Text & "%' "
            dtTranHead = dtView.ToTable
            With lvCustomTranHead
                .DataSource = dtTranHead
                .DataBind()
            End With
            If txtSearchTranHeads.Text.Trim.Length > 0 Then
                lvCustomTranHead.Columns(4).Visible = False
            Else
                lvCustomTranHead.Columns(4).Visible = True
            End If
            'If txtSearchTranHeads.Text.trim.length > 0 Then
            '    Call FillCustomHeads(dtTranHead)
            'Else
            '    Call FillCustomHeads(CType(Me.ViewState("OriginalData"), DataTable))
            'End If
            'Dim iRowIdx As Integer = 0
            'If dtTranHead.Rows.Count > 0 Then
            '    For i As Integer = 0 To dtTranHead.Rows.Count
            '    Next
            '    Dim dt() As DataRow = CType(Me.ViewState("dtTranHead"), DataTable).Select("Code ='" & dtTranHead.Rows(0).Item("Code") & "'")
            '    If dt.Length > 0 Then
            '        iRowIdx = CType(Me.ViewState("dtTranHead"), DataTable).Rows.IndexOf(dt(0))
            '        lvCustomTranHead.Rows(iRowIdx).BackColor = Color.Cyan
            '    End If
            'End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Shani [ 22 OCT 2014 ] -- END


#End Region
    'Sohail (20 May 2014) -- End


    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            ''Me.Closebutton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebutton1.PageHeading)

            Me.lblEmployee.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployee.ID, Me.lblEmployee.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.chkInactiveemp.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkInactiveemp.ID, Me.chkInactiveemp.Text)
            Me.lblBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBranch.ID, Me.lblBranch.Text)
            Me.lnkAnalysisBy.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkAnalysisBy.ID, Me.lnkAnalysisBy.Text)
            Me.chkIgnorezeroHead.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkIgnorezeroHead.ID, Me.chkIgnorezeroHead.Text)
            Me.lblReportType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblReportType.ID, Me.lblReportType.Text)
            Me.lblToPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblToPeriod.ID, Me.lblToPeriod.Text)
            Me.lblCurrency.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.lblExRate.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExRate.ID, Me.lblExRate.Text)

            Me.lvTranHead.Columns(2).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvTranHead.Columns(2).FooterText, Me.lvTranHead.Columns(2).HeaderText)
            Me.lvTranHead.Columns(3).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lvTranHead.Columns(3).FooterText, Me.lvTranHead.Columns(3).HeaderText)

            Me.lblIncludeHeads.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIncludeHeads.ID, Me.lblIncludeHeads.Text)
            Me.gbFilterCriteria.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.gbFilterCriteria.ID, Me.gbFilterCriteria.Text)
            Me.chkShowPaymentDetails.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowPaymentDetails.ID, Me.chkShowPaymentDetails.Text)
            Me.chkShowCompanyLogoOnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowCompanyLogoOnReport.ID, Me.chkShowCompanyLogoOnReport.Text)
            Me.chkIncludeEmployerContribution.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkIncludeEmployerContribution.ID, Me.chkIncludeEmployerContribution.Text)
            Me.chkShowLoansInSeparateColumns.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowLoansInSeparateColumns.ID, Me.chkShowLoansInSeparateColumns.Text)
            Me.chkShowSavingsInSeparateColumns.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowSavingsInSeparateColumns.ID, Me.chkShowSavingsInSeparateColumns.Text)
            Me.lblMembership.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMembership.ID, Me.lblMembership.Text)
            Me.chkShowEmpNameinSeperateColumn.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowEmpNameinSeperateColumn.ID, Me.chkShowEmpNameinSeperateColumn.Text)
        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Anjan [04 June 2014] -- End



End Class
