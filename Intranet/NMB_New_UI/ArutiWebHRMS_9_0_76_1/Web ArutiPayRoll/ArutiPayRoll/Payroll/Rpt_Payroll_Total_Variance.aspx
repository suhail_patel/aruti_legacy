﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="Rpt_Payroll_Total_Variance.aspx.vb"
    Inherits="Reports_Rpt_Payroll_Total_Variance" Title="Payroll Total Variance" %>

<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--  <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">

function pageLoad(sender, args) {
    $("select").searchable();
}
    </script>--%>
    <asp:Panel ID="Panel1" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Payroll Total Variance" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPeriodFrom" runat="server" Text="Period From" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPeriodFrom" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPeriodTo" runat="server" Text="To" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPeriodTo" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTrnHeadType" runat="server" Text="Tran.Head Type" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboTrnHeadType" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkIgnoreZeroVariance" runat="server" Text="Ignore Zero Variance"
                                                            Checked="True" Visible="false" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkShowVariancePercentageColumns" runat="server" Text="Show Variance% Columns" />
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:CheckBox ID="chkInactiveemp" runat="server" Text="Include Inactive Employee" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="btnReset" runat="server" CssClass="btn btn-default" Text="Reset" />
                                                <asp:Button ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Report" />
                                                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="lblInfoHeadHeader" runat="server" Text="Informational Heads" CssClass="form-label"></asp:Label>
                                                    <div style="text-align: right">
                                                        <asp:LinkButton ID="lnkSave" runat="server" Text="Save Settings" CssClass="lnkhover"
                                                            Visible="false"></asp:LinkButton>
                                                    </div>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <asp:Panel ID="pnlInfoheads" runat="server" class="panel-default">
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <div class="form-line">
                                                                    <asp:TextBox ID="txtSearchTranHeads" runat="server" AutoPostBack="true" placeholder="Search Heads"
                                                                        CssClass="form-control"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row clearfix">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                            <div class="table-responsive" style="max-height: 315px;">
                                                                <asp:GridView ID="dgHeads" runat="server" AutoGenerateColumns="False" ShowFooter="False"
                                                                    Width="99%" AllowPaging="false" HeaderStyle-Font-Bold="false" DataKeyNames="tranheadunkid"
                                                                    CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chkHeadSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkHeadSelectAll_OnCheckedChanged"
                                                                                    Text=" " />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkHeadSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                    OnCheckedChanged="chkHeadSelect_OnCheckedChanged" Text=" " />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="Code" HeaderText="Code" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                            FooterText="dgColhHeadCode" />
                                                                        <asp:BoundField DataField="Name" HeaderText="Name" ReadOnly="true" HeaderStyle-HorizontalAlign="Left"
                                                                            FooterText="dgColhHeadName" />
                                                                        <asp:BoundField DataField="tranheadunkid" ReadOnly="true" Visible="false" />
                                                                        <%--<asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkUp" CssClass="button" CommandArgument="up" runat="server"
                                                                                    Text="&#x25B2;" OnClick="ChangeTranHeadLocation" />
                                                                                <asp:LinkButton ID="lnkDown" CssClass="button" CommandArgument="down" runat="server"
                                                                                    Text="&#x25BC;" OnClick="ChangeTranHeadLocation" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <asp:HiddenField ID="hfScrollPosition2" runat="server" Value="0" />
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
