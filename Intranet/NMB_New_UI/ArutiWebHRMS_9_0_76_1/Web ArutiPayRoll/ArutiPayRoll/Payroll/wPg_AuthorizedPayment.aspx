﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_AuthorizedPayment.aspx.vb"
    Inherits="Payroll_wPg_AuthorizedPayment" Title="Authorized Payment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/AdvanceFilter.ascx" TagName="AdvanceFilter" TagPrefix="uc6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>--%>
    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
            var prm;
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });

            function beginRequestHandler(sender, args) {
                $("#endreq").val("0");
                $("#bodyy").val($(window).scrollTop());
            }

            function endRequestHandler(sender, args) {
                $("#endreq").val("1");
                if (args.get_error() == undefined) {
                    $("#scrollable-container").scrollTop($(scroll.Y).val());
                }
            }
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <uc6:AdvanceFilter ID="popupAdvanceFilter" runat="server" />
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Authorized Payment" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="table-responsive" style="max-height: 570px;">
                                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:GridView ID="GvAuthorizedPayment" runat="server" AutoGenerateColumns="False"
                                                                        AllowPaging="False" ShowFooter="False" Width="99%" HeaderStyle-Font-Bold="false"
                                                                        CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                                        <Columns>
                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                                                <HeaderTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_OnCheckedChanged"
                                                                                            Text=" " />
                                                                                    </span>
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <span class="gridiconbc">
                                                                                        <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("IsChecked")) %>'
                                                                                            OnCheckedChanged="chkSelect_OnCheckedChanged" Text=" " />
                                                                                    </span>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="employeecode" HeaderText="Code" ReadOnly="True" FooterText="colhCode" />
                                                                            <asp:BoundField DataField="EmpName" HeaderText="Name" ReadOnly="True" FooterText="colhName" />
                                                                            <asp:BoundField DataField="expaidamt" HeaderText="Paid Amount" ReadOnly="True" ItemStyle-HorizontalAlign="Right"
                                                                                HeaderStyle-HorizontalAlign="Right" FooterText="colhAmount" />
                                                                            <asp:BoundField DataField="paidcurrency" HeaderText="Currency" ReadOnly="True" FooterText="colhCurrency" />
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="cboPayPeriod" EventName="SelectedIndexChanged" />
                                                                    <asp:AsyncPostBackTrigger ControlID="cboCurrency" EventName="SelectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="gbAmountInfo" runat="server" Text="Total Amount" CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="divider divider-with-text-left">
                                                    <span class="ant-divider-inner-text"></span>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTotalAmount" runat="server" Text="Total Amount" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtTotalAmount" runat="server" ReadOnly="true" Style="text-align: right;"
                                                                    Text="0" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbPaymentInfo" runat="server" Text="Payment Information" CssClass="form-label"></asp:Label>
                                                </h2>
                                                <ul class="header-dropdown m-r--5">
                                                    <li class="dropdown">
                                                        <asp:LinkButton ID="lnkAdvanceFilter" runat="server" Text="Advance Filter" ToolTip="Advance Filter">
                                                           <i class="fas fa-sliders-h"></i>
                                                        </asp:LinkButton>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPayPeriod" runat="server" Text="Pay Period" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPayPeriod" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCurrency" runat="server" Text="Currency" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboCurrency" runat="server" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblPmtVoucher" runat="server" Text="Voucher #" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <asp:DropDownList ID="cboPmtVoucher" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <asp:Button ID="objbtnSearch" runat="server" Text="Search" CssClass="btn btn-primary" />
                                                <asp:Button ID="objbtnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                                            </div>
                                        </div>
                                        <div class="card inner-card">
                                            <div class="header">
                                                <h2>
                                                    <asp:Label ID="gbPaymentModeInfo" runat="server" Text="Payment Mode Summary" CssClass="form-label"></asp:Label>
                                                </h2>
                                            </div>
                                            <div class="body">
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblbankPaid" runat="server" Text="Bank Payment" CssClass="form-label"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                        <asp:Label ID="objBankpaidVal" runat="server" Text="0" CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblCashPaid" runat="server" Text="Cash Payment" CssClass="form-label"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                        <asp:Label ID="objCashpaidVal" runat="server" Text="0" CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="divider divider-with-text-left">
                                                    <span class="ant-divider-inner-text"></span>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblTotalPaid" runat="server" Text="Total Payment" CssClass="form-label"></asp:Label>
                                                    </div>
                                                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                        <asp:Label ID="objTotalpaidVal" runat="server" Text="0" CssClass="form-label"></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="divider divider-with-text-left">
                                                    <span class="ant-divider-inner-text"></span>
                                                </div>
                                                <div class="row clearfix">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <asp:Label ID="lblRemarks" runat="server" Text="Remark" CssClass="form-label"></asp:Label>
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="footer">
                                                <div style="float: left;">
                                                    <h4>
                                                        <ul class="header-dropdown p-l-0" style="list-style: none;">
                                                            <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                                                role="button" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v">
                                                                </i></a>
                                                                <ul class="dropdown-menu pull-right">
                                                                    <li>
                                                                        <asp:LinkButton ID="lnkPayrollVarianceReport" runat="server" Text="Show Payroll Variance Report"></asp:LinkButton>
                                                                        <li>
                                                                            <asp:LinkButton ID="lnkPayrollTotalVarianceReport" runat="server" Text="Show Payroll Total Variance Report"></asp:LinkButton>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </h4>
                                                </div>
                                                <asp:Button ID="btnProcess" runat="server" Text="Authorize Payment" CssClass="btn btn-primary" />
                                                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
