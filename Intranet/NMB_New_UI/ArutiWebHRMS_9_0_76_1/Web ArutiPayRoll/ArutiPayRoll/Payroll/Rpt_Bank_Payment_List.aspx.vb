﻿Option Strict On 'Nilay (10-Feb-2016)

#Region " Imports "
Imports Aruti.Data
Imports eZeeCommonLib
Imports ArutiReports
Imports System.Data

#End Region


Partial Class Payroll_Rpt_Bank_Payment_List
    Inherits Basepage

#Region " Private Variable "

    Dim DisplayMessage As New CommonCodes
    Private objBankPaymentList As clsBankPaymentList
    Private objBank As clspayrollgroup_master

    Private mintFirstOpenPeriod As Integer = 0

    'Anjan [04 June 2014] -- Start
    'ENHANCEMENT : Implementing Language,requested by Andrew
    Private ReadOnly mstrModuleName As String = "frmBankPaymentList"
    'Anjan [04 June 2014 ] -- End

    'Hemant (27 June 2019) -- Start
    'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
    Private Shared ReadOnly mstrModuleName1 As String = "frmEFTCustomColumnsExport"
    'Hemant (27 June 2019) -- End

    'Sohail (12 Feb 2015) -- Start
    'Enhancement - EFT CBA with New CSV Format.
    Private mstrOrderByDisplay As String = ""
    Private mstrOrderByQuery As String = ""
    'Sohail (12 Feb 2015) -- End

    'SHANI [13 Mar 2015]-START
    'Enhancement - New EFT Report Custom CSV Report.
    Private objUserDefRMode As New clsUserDef_ReportMode
    Private mintModeId As Integer = -1
    Private mstrEFTCustomColumnsIds As String = String.Empty
    Private mblnShowColumnHeader As Boolean = False
    Private marrEFTCustomColumnIds As New ArrayList
    Private dtEFTTable As New DataTable
    'SHANI [13 Mar 2015]--END 
    'Sohail (13 Apr 2016) -- Start
    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
    Private mintMembershipUnkId As Integer = 0
    'Sohail (13 Apr 2016) -- End
    'Hemant (27 June 2019) -- Start
    'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
    Private mblnSaveAsTXT As Boolean = False
    Private mblnTabDelimiter As Boolean = False
    'Hemant (27 June 2019) -- End
    'Hemant (02 Jul 2020) -- Start
    'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
    Private mstrDateFormat As String = String.Empty
    'Hemant (02 Jul 2020) -- End
#End Region

    'Sohail (01 Apr 2014) -- Start
    'ENHANCEMENT - 
#Region " Private Enum "
    Private Enum enHeadTypeId
        Company_Bank = 1
        Company_Branch = 2
        Company_Account_No = 3
        Report_Mode = 4
        Employee_Bank = 5
        Employee_Branch = 6
        Country = 7
        Paid_Currency = 8
        Show_Signatory_1 = 9
        Show_Signatory_2 = 10
        Show_Signatory_3 = 11
        Show_Defined_Signatory_1 = 12
        Show_Defined_Signatory_2 = 13
        Show_Defined_Signatory_3 = 14
        Show_Bank_Branch_Group = 15
        Show_Employee_Sign = 16
        Include_Inactive_Employee = 17
        Show_Period = 18
        Show_Sort_Code = 19
        Show_Separate_FName_Surname = 20
        Show_Bank_Code = 21
        Show_Branch_Code = 22
        Show_Account_Type = 23
        Show_employee_code = 24
        'Sohail (28 Feb 2017) -- Start
        'Enhancement - Add New Bank Payment List Integration for TAMA
        Letterhead = 26
        Address_to_Employee_Bank = 27
        'Sohail (28 Feb 2017) -- End

        'Sohail (12 Feb 2018) -- Start
        'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
        Present_Days = 28
        Basic_Salary = 29
        Social_Security = 30
        'Sohail (12 Feb 2018) -- End
        'Sohail (16 Apr 2018) -- Start
        'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
        ExtraIncome = 31
        AbsentDays = 32
        IdentityType = 33
        PaymentType = 34
        'Sohail (16 Apr 2018) -- End
        'Sohail (19 Apr 2018) -- Start
        'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
        CustomCurrencyFormat = 35
        'Sohail (19 Apr 2018) -- End
        'Hemant (19 June 2019) -- Start
        'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
        Show_Selected_Bank_Info = 36
        'Hemant (19 June 2019) -- End
        'Hemant 20 Jul 2019) -- Start
        'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
        Show_Company_Group_Info = 37
        'Hemant 20 Jul 2019) -- End
        'Hemant (16 Jul 2020) -- Start
        'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
        Show_Company_Logo = 38
        Save_As_TXT = 39
        'Hemant (16 Jul 2020) -- End        
        'Sohail (08 Jul 2021) -- Start
        'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
        Membership = 40
        'Sohail (08 Jul 2021) -- End
    End Enum
#End Region
    'Sohail (01 Apr 2014) -- End

    'Sohail (13 Apr 2016) -- Start
    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
#Region " Private Enum "
    Private Enum enReportMode
        CUSTOM_COLUMNS = 0
        SHOW_REPORT_HEADER = 1
        MEMBERSHIP = 2
        'Hemant (27 June 2019) -- Start
        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
        SAVE_AS_TXT = 3
        TAB_DELIMITER = 4
        'Hemant (27 June 2019) -- End
        'Hemant (02 Jul 2020) -- Start
        'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
        DATE_FORMAT = 5
        'Hemant (02 Jul 2020) -- End
    End Enum
#End Region
    'Sohail (13 Apr 2016) -- End

#Region " Page's Event "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Session("clsuser") Is Nothing Then
                Exit Sub
            End If

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 3 Logo options on Payroll Summary Report for KBC.
            If CBool(Session("IsArutiDemo")) = False AndAlso ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Payroll_Management) = False Then
                DisplayMessage.DisplayMessage(m_NoLicenceMsg, Me.Page, Request.Url.GetLeftPart(UriPartial.Authority) & Request.ApplicationPath & "/UserHome.aspx")
                Exit Sub
            End If
            'Sohail (21 Jan 2016) -- End

            objBankPaymentList = New clsBankPaymentList(CInt(HttpContext.Current.Session("LangId")), CInt(HttpContext.Current.Session("CompanyUnkId")))

            Dim mintCurrPeriodID As Integer = 0
            If Not IsPostBack Then

                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                SetLanguage()
                'Anjan [04 June 2014 ] -- End

                FillCombo()
                ResetValue()
                'Sohail (12 Feb 2015) -- Start
                'Enhancement - EFT CBA with New CSV Format.
            Else
                mstrOrderByDisplay = Me.ViewState("mstrOrderByDisplay").ToString
                mstrOrderByQuery = Me.ViewState("mstrOrderByQuery").ToString
                'Sohail (12 Feb 2015) -- End
                mintModeId = CInt(Me.ViewState("mintModeId"))
                mstrEFTCustomColumnsIds = Me.ViewState("mstrEFTCustomColumnsIds").ToString
                mblnShowColumnHeader = CBool(Me.ViewState("mblnShowColumnHeader"))
                marrEFTCustomColumnIds = CType(Me.ViewState("marrEFTCustomColumnIds"), ArrayList)
                dtEFTTable = CType(Me.ViewState("dtEFTTable"), DataTable)
                mintMembershipUnkId = CInt(Me.ViewState("mintMembershipUnkId"))   'Sohail (13 Apr 2016)
                'Hemant (27 June 2019) -- Start
                'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                mblnSaveAsTXT = CBool(Me.ViewState("mblnSaveAsTXT"))
                mblnTabDelimiter = CBool(Me.ViewState("mblnTabDelimiter"))
                'Hemant (27 June 2019) -- End
                'Hemant (02 Jul 2020) -- Start
                'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                mstrDateFormat = CStr(Me.ViewState("mstrDateFormat"))
                'Hemant (02 Jul 2020) -- End
            End If

        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_Load:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Me.IsLoginRequired = True
    End Sub

    'Sohail (12 Feb 2015) -- Start
    'Enhancement - EFT CBA with New CSV Format.
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Try
            Me.ViewState("mstrOrderByDisplay") = mstrOrderByDisplay
            Me.ViewState("mstrOrderByQuery") = mstrOrderByQuery
            Me.ViewState("mintModeId") = mintModeId
            Me.ViewState("mstrEFTCustomColumnsIds") = mstrEFTCustomColumnsIds
            Me.ViewState("mblnShowColumnHeader") = mblnShowColumnHeader
            Me.ViewState("marrEFTCustomColumnIds") = marrEFTCustomColumnIds
            Me.ViewState("dtEFTTable") = dtEFTTable
            Me.ViewState("mintMembershipUnkId") = mintMembershipUnkId 'Sohail (13 Apr 2016)
            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            Me.ViewState("mblnSaveAsTXT") = mblnSaveAsTXT
            Me.ViewState("mblnTabDelimiter") = mblnTabDelimiter
            'Hemant (27 June 2019) -- End
            'Hemant (02 Jul 2020) -- Start
            'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
            Me.ViewState("mstrDateFormat") = mstrDateFormat
            'Hemant (02 Jul 2020) -- End
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayMessage("Page_PreRender:- " & ex.Message, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (12 Feb 2015) -- End

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim dsList As New DataSet
        Dim objExRate As New clsExchangeRate
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim objCountry As clsMasterData
        Dim objEmp As clsEmployee_Master
        Dim objPeriod As clscommom_period_Tran
        Dim objCurrentPeriodId As clsMasterData
        Dim itm As ListItem
        Dim objMembership As New clsmembership_master 'Sohail (13 Apr 2016)
        Dim objMaster As New clsMasterData 'Nilay (10-Nov-2016)
        'Sohail (12 Feb 2018) -- Start
        'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
        Dim objTransactionHead As New clsTransactionHead
        'Sohail (12 Feb 2018) -- End
        Try
            objEmp = New clsEmployee_Master
            objBank = New clspayrollgroup_master
            objCountry = New clsMasterData
            objPeriod = New clscommom_period_Tran
            objCurrentPeriodId = New clsMasterData

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objEmp.GetEmployeeList("Emp", True, True)

            'Nilay (10-Feb-2016) -- Start
            'dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
            '                                CInt(Session("UserId")), _
            '                                CInt(Session("Fin_year")), _
            '                                CInt(Session("CompanyUnkId")), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
            '                                Session("UserAccessModeSetting").ToString, True, _
            '                                CBool(Session("IsIncludeInactiveEmp")), "Emp", True)

            dsList = objEmp.GetEmployeeList(Session("Database_Name").ToString, _
                                            CInt(Session("UserId")), _
                                            CInt(Session("Fin_year")), _
                                            CInt(Session("CompanyUnkId")), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString), _
                                            Session("UserAccessModeSetting").ToString, True, _
                                            True, "Emp", True)
            'Nilay (10-Feb-2016) -- End

            'Shani(20-Nov-2015) -- End

            With cboEmployeeName
                .DataValueField = "employeeunkid"
                'Nilay (09-Aug-2016) -- Start
                'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
                '.DataTextField = "employeename"
                .DataTextField = "EmpCodeName"
                'Nilay (09-Aug-2016) -- End
                .DataSource = dsList.Tables("Emp")
                .DataBind()
                .SelectedValue = "0"
            End With


            dsList = objBank.getListForCombo(enPayrollGroupType.Bank, "List", True)
            With cboBankName
                .DataValueField = "groupmasterunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            cboBankName_SelectedIndexChanged(New Object(), New EventArgs())


            dsList = objCountry.getCountryList("List", True)
            With cboCountry
                .DataValueField = "countryunkid"
                .DataTextField = "country_name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With


            mintFirstOpenPeriod = objCountry.getFirstPeriodID(enModuleReference.Payroll, CInt(Session("Fin_year")), 1)

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, Session("Fin_year"), "List", True, , , , Session("database_name").ToString)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, CInt(Session("Fin_year")), Session("Database_Name").ToString, CDate(Session("fin_startdate")), "List", True)
            'Shani(20-Nov-2015) -- End

            With cboPeriod
                .DataValueField = "periodunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = mintFirstOpenPeriod.ToString
                Me.ViewState.Add("FirstOpenPeriod", mintFirstOpenPeriod)
                Call cboPeriod_SelectedIndexChanged(cboPeriod, New System.EventArgs) 'Sohail (07 Dec 2013)
            End With

            dsList = objExRate.getComboList("Currency", True)
            With cboCurrency
                .DataValueField = "countryunkid"
                .DataTextField = "currency_name"
                .DataSource = dsList.Tables("Currency")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            dsList = objMaster.GetCondition(False, False, True, True, False)
            With cboCondition
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables(0)
                .DataBind()
                .SelectedIndex = 0
            End With
            'Nilay (10-Nov-2016) -- End

            cboReportType.Items.Clear()
            'Language.setLanguage(mstrModuleName)
            itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 2, "Bank Payment List"), CInt(enBankPaymentReport.Bank_payment_List).ToString)
            cboReportType.Items.Add(itm)

            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            'Language.setLanguage(mstrModuleName)
            'Anjan [04 June 2014 ] -- End
            itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 3, "Electronic Funds Transfer"), CInt(enBankPaymentReport.Electronic_Fund_Transfer).ToString)
            cboReportType.Items.Add(itm)

            If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_StandardBank Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 10, "EFT Standard Bank"), CInt(enBankPaymentReport.EFT_Standard_Bank).ToString)
                cboReportType.Items.Add(itm)
            End If

            If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_StandardCharteredBank Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 12, "EFT Standard Chartered Bank"), CInt(enBankPaymentReport.EFT_StandardCharteredBank).ToString)
                cboReportType.Items.Add(itm)
            End If

            If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_SFIBank Then
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 13, "EFT SFI Bank"), CInt(enBankPaymentReport.EFT_SFIBANK).ToString)
                cboReportType.Items.Add(itm)
            End If

            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            itm = New ListItem("Bank Payment Voucher Report", CInt(enBankPaymentReport.Bank_Payment_Voucher).ToString)
            cboReportType.Items.Add(itm)
            'Pinkal (12-Jun-2014) -- End

            'Sohail (25 Mar 2015) -- Start
            'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
            itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 21, "EFT Advance Salary CSV"), CInt(enBankPaymentReport.EFT_ADVANCE_SALARY_CSV).ToString)
            cboReportType.Items.Add(itm)

            itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 22, "EFT Advance Salary XSV"), CInt(enBankPaymentReport.EFT_ADVANCE_SALARY_XLS).ToString)
            cboReportType.Items.Add(itm)
            'Sohail (25 Mar 2015) -- End

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            If CInt(Session("Compcountryid")) = 162 Then 'OMAN Salary Format WPS
                itm = New ListItem(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 27, "Salary Format WPS"), CInt(enBankPaymentReport.Salary_Format_WPS).ToString())
                cboReportType.Items.Add(itm)
            End If
            'Sohail (12 Feb 2018) -- End

            cboReportType.SelectedIndex = 0
            With cboReportMode
                .Items.Clear()
                'Anjan [04 June 2014] -- Start
                'ENHANCEMENT : Implementing Language,requested by Andrew
                'Language.setLanguage(mstrModuleName)
                'Anjan [04 June 2014 ] -- End
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 8, "DRAFT"))
                .Items.Add(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 9, "AUTHORIZED"))
                .SelectedIndex = 0
            End With

            cboReportType_SelectedIndexChanged(New Object(), New EventArgs())

            dsList = objCompanyBank.GetComboList(CInt(Session("CompanyUnkId")), 1, "Bank")
            With cboCompanyBankName
                .DataValueField = "id"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Bank")
                .DataBind()
                .SelectedValue = "0"
            End With

            cboCompanyBankName_SelectedIndexChanged(New Object(), New EventArgs())


            'Sohail (13 Apr 2016) -- Start
            'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
            dsList = objMembership.getListForCombo("Membership", True)
            With cboMembership
                .DataValueField = "membershipunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Membership")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Sohail (13 Apr 2016) -- End

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            dsList = objTransactionHead.getComboList(Session("Database_Name").ToString, "List", True, CInt(enTranHeadType.Informational))
            With cboPresentDays
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Sohail (16 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            'dsList = objTransactionHead.getComboList(Session("Database_Name").ToString, "List", True, CInt(enTranHeadType.EarningForEmployees))
            dsList = objTransactionHead.getComboList(Session("Database_Name").ToString, "List", True)
            'Sohail (16 Apr 2018) -- End
            With cboBasicSalary
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With

            'Sohail (16 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            With cboExtraIncome
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
                .SelectedValue = "0"
            End With

            With cboAbsentDays
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List").Copy
                .DataBind()
                .SelectedValue = "0"
            End With
            'Sohail (16 Apr 2018) -- End

            dsList = objTransactionHead.getComboList(Session("Database_Name").ToString, "List", True, , , , , , "trnheadtype_id IN (" & CInt(enTranHeadType.DeductionForEmployee) & ", " & CInt(enTranHeadType.EmployeesStatutoryDeductions) & ")")
            With cboSocialSecurity
                .DataValueField = "tranheadunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Sohail (12 Feb 2018) -- End

            'Sohail (08 Jul 2021) -- Start
            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
            dsList = objMembership.getListForCombo("Membership", True)
            With cboMembershipRepo
                .DataValueField = "membershipunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("Membership")
                .DataBind()
                .SelectedValue = "0"
            End With
            'Sohail (08 Jul 2021) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally

            dsList.Dispose()
            dsList = Nothing
            objEmp = Nothing
            objBank = Nothing
            objCountry = Nothing
            objPeriod = Nothing
            objCurrentPeriodId = Nothing
            objExRate = Nothing
            objMembership = Nothing 'Sohail (13 Apr 2016)
            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            objTransactionHead = Nothing
            'Sohail (12 Feb 2018) -- End
        End Try

    End Sub

    Private Sub ResetValue()

        Try
            cboBankName.SelectedValue = "0"
            cboBranchName.SelectedValue = "0"
            cboCountry.SelectedValue = "0"
            cboEmployeeName.SelectedValue = "0"
            cboPeriod.SelectedValue = mintFirstOpenPeriod.ToString
            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            'txtEmpCode.Text = ""
            cboChequeNo.SelectedIndex = 0
            'Sohail (07 Dec 2013) -- End
            txtAmount.Text = "0"
            txtAmountTo.Text = "0"

            chkEmployeeSign.Checked = False
            chkSignatory1.Checked = False
            chkSignatory2.Checked = False
            chkSignatory3.Checked = False
            chkDefinedSignatory.Checked = False
            'Sohail (24 Dec 2013) -- Start
            'Enhancement - Oman
            If chkShowPayrollPeriod.Enabled = True Then
                chkShowPayrollPeriod.Checked = True
            Else
                chkShowPayrollPeriod.Checked = False
            End If
            If chkShowSortCode.Enabled = True Then
                chkShowSortCode.Checked = True
            Else
                chkShowSortCode.Checked = False
            End If
            'Sohail (24 Dec 2013) -- End
            chkShowFNameSeparately.Checked = False
            objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
            cboCurrency.SelectedValue = "0"
            chkShowGroupByBankBranch.Checked = False
            cboReportMode.SelectedIndex = 0
            cboCompanyBankName.SelectedValue = "0"
            cboCompanyBranchName.SelectedValue = "0"
            cboCompanyAccountNo.SelectedValue = "0"


            'Anjan [03 Feb 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            chkShowBankCode.Checked = True
            chkShowBranchCode.Checked = True
            chkDefinedSignatory2.Checked = False
            chkDefinedSignatory3.Checked = False
            'Anjan [03 Feb 2014 ] -- End

            txtCutOffAmount.Text = "" 'Sohail (15 Mar 2014)

            'Sohail (01 Apr 2014) -- Start
            'ENHANCEMENT - 
            chkShowAccountType.Checked = False
            chkShowReportHeader.Checked = True
            chkShowEmployeeCode.Checked = True
            'Sohail (01 Apr 2014) -- End
            chkShowSelectedBankInfo.Checked = False 'Hemant (19 June 2019)
            chkShowCompanyGrpInfo.Checked = False 'Hemant (20 Jul 2019)

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            cboCondition.SelectedIndex = 0
            'Nilay (10-Nov-2016) -- End

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            cboPresentDays.SelectedValue = "0"
            cboBasicSalary.SelectedValue = "0"
            cboSocialSecurity.SelectedValue = "0"
            'Sohail (12 Feb 2018) -- End
            'Sohail (16 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            cboExtraIncome.SelectedValue = "0"
            cboAbsentDays.SelectedValue = "0"
            txtIdentityType.Text = ""
            txtPaymentType.Text = ""
            'Sohail (16 Apr 2018) -- End
            'Sohail (19 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            txtCustomCurrFormat.Text = ""
            'Sohail (19 Apr 2018) -- End
            'Hemant (16 Jul 2020) -- Start
            'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
            chkSaveAsTXT_WPS.Checked = False
            'Hemant (16 Jul 2020) -- End
            cboMembershipRepo.SelectedValue = "0" 'Sohail (08 Jul 2021)"

            Call GetValue() 'Sohail (01 Apr 2014)

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Sub

    Private Function SetFilter() As Boolean
        Try

            If CInt(cboReportType.SelectedIndex) > 0 AndAlso CInt(cboCurrency.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 4, "Currency is mandatory information. Please select Currency."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCurrency.Focus()
                Return False

            ElseIf CInt(cboCompanyBankName.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 5, "Please select Company Bank."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCompanyBankName.Focus()
                Return False

            ElseIf CInt(cboCompanyBranchName.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 6, "Please select Company Bank Branch."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCompanyBranchName.Focus()
                Return False

            ElseIf CInt(cboCompanyAccountNo.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 7, "Please select Company Bank Account."), Me)
                'Sohail (23 Mar 2019) -- End
                cboCompanyAccountNo.Focus()
                Return False

                Return False

            End If

            objBankPaymentList.SetDefaultValue()

            If CInt(cboPeriod.SelectedValue) > 0 Then
                objBankPaymentList._PeriodId = CInt(cboPeriod.SelectedValue).ToString
            End If

            If CInt(cboBankName.SelectedValue) > 0 Then
                objBankPaymentList._BankId = CInt(cboBankName.SelectedValue)
                objBankPaymentList._BankName = cboBankName.SelectedItem.Text
            End If

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            If CInt(cboCondition.SelectedValue) > 0 Then
                objBankPaymentList._ConditionId = CInt(cboCondition.SelectedValue)
                objBankPaymentList._ConditionText = cboCondition.SelectedItem.Text
            End If
            'Nilay (10-Nov-2016) -- End

            If CInt(cboBranchName.SelectedValue) > 0 Then
                objBankPaymentList._BranchId = CInt(cboBranchName.SelectedValue)
                objBankPaymentList._BankBranchName = cboBranchName.SelectedItem.Text
            End If

            If CInt(cboCountry.SelectedValue) > 0 Then
                objBankPaymentList._CountryId = CInt(cboCountry.SelectedValue)
                objBankPaymentList._CountryName = cboCountry.SelectedItem.Text
            End If

            If CInt(cboEmployeeName.SelectedValue) > 0 Then
                objBankPaymentList._EmployeeUnkId = CInt(cboEmployeeName.SelectedValue)
                objBankPaymentList._EmpName = cboEmployeeName.SelectedItem.Text
            End If

            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            'If Trim(txtEmpCode.Text) <> "" Then
            '    objBankPaymentList._EmpCode = txtEmpCode.Text
            'End If
            If cboChequeNo.SelectedIndex > 0 Then
                objBankPaymentList._ChequeNo = cboChequeNo.Text
            Else
                objBankPaymentList._ChequeNo = ""
            End If
            'Sohail (07 Dec 2013) -- End

            If Trim(txtAmount.Text) <> "" And Trim(txtAmountTo.Text) <> "" Then
                objBankPaymentList._Amount = CDec(txtAmount.Text)
                objBankPaymentList._AmountTo = CDec(txtAmountTo.Text)
            End If

            If chkEmployeeSign.Checked = True Then
                objBankPaymentList._IsEmployeeSign = True
            End If
            If chkSignatory1.Checked = True Then
                objBankPaymentList._IsSignatory1 = True
            End If
            If chkSignatory2.Checked = True Then
                objBankPaymentList._IsSignatory2 = True
            End If
            If chkSignatory3.Checked = True Then
                objBankPaymentList._IsSignatory3 = True
            End If

            'Anjan [ 22 Nov 2013 ] -- Start
            'ENHANCEMENT : Requested by Rutta
            If chkDefinedSignatory.Checked = True Then
                objBankPaymentList._IsDefinedSignatory = True
            End If
            'Anjan [22 Nov 2013 ] -- End

            'Sohail (24 Dec 2013) -- Start
            'Enhancement - Oman
            objBankPaymentList._ShowPayrollPeriod = chkShowPayrollPeriod.Checked
            objBankPaymentList._ShowSortCode = chkShowSortCode.Checked
            'Sohail (24 Dec 2013) -- End
            objBankPaymentList._ShowFNameSurNameInSeparateColumn = chkShowFNameSeparately.Checked
            objBankPaymentList._CurrencyId = CInt(cboCurrency.SelectedValue)
            objBankPaymentList._CurrencyName = cboCurrency.SelectedItem.Text
            objBankPaymentList._ShowGroupByBankBranch = chkShowGroupByBankBranch.Checked
            objBankPaymentList._PeriodName = cboPeriod.SelectedItem.Text

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            objBankPaymentList._PeriodCode = objPeriod._Period_Code 'Sohail (24 Dec 2014) 
            objBankPaymentList._PeriodStartDate = objPeriod._Start_Date
            objBankPaymentList._PeriodEndDate = objPeriod._End_Date
            'Sohail (24 Mar 2018) -- Start
            'Voltamp Enhancement - 70.2 - Changes in WPS report as per discussion with Prabhakar. [Salary month should be character for example Jan to be displayed as 01, Feb as 02, two digit number]
            objBankPaymentList._PeriodCustomCode = objPeriod._Sunjv_PeriodCode
            'Sohail (24 Mar 2018) -- End
            objPeriod = Nothing

            objBankPaymentList._ReportTypeId = CInt(CType(cboReportType.SelectedItem, ListItem).Value)
            objBankPaymentList._ReportTypeName = cboReportType.SelectedItem.Text
            objBankPaymentList._ReportModeId = CInt(cboReportMode.SelectedIndex)
            objBankPaymentList._ReportModeName = cboReportMode.SelectedItem.Text

            objBankPaymentList._CompanyBranchId = CInt(cboCompanyBranchName.SelectedValue)
            objBankPaymentList._CompanyBankBranchName = cboCompanyBranchName.SelectedItem.Text
            objBankPaymentList._CompanyBankAccountNo = cboCompanyAccountNo.SelectedItem.Text

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            objBankPaymentList._PresentDaysId = CInt(cboPresentDays.SelectedValue)
            objBankPaymentList._PresentDaysName = cboPresentDays.SelectedItem.Text

            objBankPaymentList._BasicSalaryId = CInt(cboBasicSalary.SelectedValue)
            objBankPaymentList._BasicSalaryName = cboBasicSalary.SelectedItem.Text

            objBankPaymentList._SocialSecurityId = CInt(cboSocialSecurity.SelectedValue)
            objBankPaymentList._SocialSecurityName = cboSocialSecurity.SelectedItem.Text
            'Sohail (12 Feb 2018) -- End

            'Sohail (16 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            objBankPaymentList._ExtraIncomeId = CInt(cboExtraIncome.SelectedValue)
            objBankPaymentList._ExtraIncomeName = cboExtraIncome.SelectedItem.Text

            objBankPaymentList._AbsentDaysId = CInt(cboAbsentDays.SelectedValue)
            objBankPaymentList._AbsentDaysName = cboAbsentDays.SelectedItem.Text

            objBankPaymentList._IdentityType = txtIdentityType.Text.Trim
            objBankPaymentList._PaymentType = txtPaymentType.Text.Trim
            'Sohail (16 Apr 2018) -- End
            'Sohail (19 Apr 2018) -- Start
            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
            objBankPaymentList._CustomCurrencyFormat = txtCustomCurrFormat.Text.Trim
            'Sohail (19 Apr 2018) -- End
            'Hemant (16 Jul 2020) -- Start
            'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
            objBankPaymentList._SaveAsTXT = chkSaveAsTXT_WPS.Checked
            'Hemant (16 Jul 2020) -- End

            objBankPaymentList._UserUnkId = CInt(Session("UserId"))
            objBankPaymentList._CompanyUnkId = CInt(Session("CompanyUnkId"))
            objBankPaymentList._UserAccessFilter = Session("AccessLevelFilterString").ToString
            'objBankPaymentList._CompanyBankGroupId = Session("CompanyBankGroupId")
            'objBankPaymentList._CompanyBranchId = Session("CompanyBankBranchId")
            GUI.fmtCurrency = Session("fmtCurrency").ToString


            'Anjan [03 Feb 2014] -- Start
            'ENHANCEMENT : Requested by Rutta

            If chkDefinedSignatory2.Checked = True Then
                objBankPaymentList._IsDefinedSignatory2 = True
            End If
            If chkDefinedSignatory3.Checked = True Then
                objBankPaymentList._IsDefinedSignatory3 = True
            End If
            If chkShowBankCode.Checked = True Then
                objBankPaymentList._ShowBankCode = True
            End If
            If chkShowBranchCode.Checked = True Then
                objBankPaymentList._ShowBranchCode = True
            End If
            'Anjan [03 Feb 2014 ] -- End

            'Sohail (15 Mar 2014) -- Start
            'Enhancement - Grouping on cut off amount.
            Dim decAmt As Decimal
            Decimal.TryParse(txtCutOffAmount.Text, decAmt)
            objBankPaymentList._Cut_Off_Amount = decAmt
            'Sohail (15 Mar 2014) -- End

            'Sohail (01 Apr 2014) -- Start
            'ENHANCEMENT - 
            objBankPaymentList._ShowReportHeader = chkShowReportHeader.Checked
            objBankPaymentList._ShowAccountType = chkShowAccountType.Checked
            objBankPaymentList._ShowEmployeeCode = chkShowEmployeeCode.Checked
            'Sohail (01 Apr 2014) -- End

            'Sohail (12 Feb 2015) -- Start
            'Enhancement - EFT CBA with New CSV Format.
            objBankPaymentList.OrderByDisplay = mstrOrderByDisplay
            objBankPaymentList.OrderByQuery = mstrOrderByQuery
            'Sohail (12 Feb 2015) -- End

            'Sohail (01 Jun 2016) -- Start
            'Enhancement -  New EFT Flex Cube Retail - GEFU for Finca Nigeria in 61.1 SP.
            objBankPaymentList._Posting_Date = dtpPostingDate.GetDate
            'Sohail (01 Jun 2016) -- End

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            objBankPaymentList._EFTCitiDirectCountryCode = Session("EFTCitiDirectCountryCode").ToString
            objBankPaymentList._EFTCitiDirectSkipPriorityFlag = CBool(Session("EFTCitiDirectSkipPriorityFlag"))
            objBankPaymentList._EFTCitiDirectChargesIndicator = Session("EFTCitiDirectChargesIndicator").ToString
            objBankPaymentList._EFTCitiDirectAddPaymentDetail = CBool(Session("EFTCitiDirectAddPaymentDetail"))
            'Sohail (09 Jan 2016) -- End

            'Sohail (28 Feb 2017) -- Start
            'Enhancement - Add New Bank Payment List Integration for TAMA
            objBankPaymentList._RoundOff_Type = CDbl(Session("RoundOff_Type"))
            objBankPaymentList._IsLetterhead = chkLetterhead.Checked
            objBankPaymentList._IsAddress_To_Emp_Bank = chkAddresstoEmployeeBank.Checked
            'Sohail (28 Feb 2017) -- End

            'Hemant (25 Jul 2019) -- Start
            'ISSUE/ENHANCEMENT(GOOD NEIGHBORS) :  Note the commas in the Bank branch address, they should be in all fields of the branch address or all be removed
            objBankPaymentList._CompanyBankGroupId = CInt(cboCompanyBankName.SelectedValue)
            'Hemant (25 Jul 2019) -- End
            'Sohail (04 Nov 2019) -- Start
            'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
            Dim objBankGroup As New clspayrollgroup_master
            objBankGroup._Groupmasterunkid = CInt(cboCompanyBankName.SelectedValue)
            objBankPaymentList._CompanyBankGroupCode = objBankGroup._Groupcode
            'Sohail (04 Nov 2019) -- End
            'Hemant (19 June 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            objBankPaymentList._ShowSelectedBankInfo = chkShowSelectedBankInfo.Checked
            'Hemant (19 June 2019) -- End
            'Hemant 20 Jul 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            objBankPaymentList._ShowCompanyGroupInfo = chkShowCompanyGrpInfo.Checked
            'Hemant 20 Jul 2019) -- End

            'Sohail (08 Jul 2021) -- Start
            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
            objBankPaymentList._MembershipUnkId = CInt(cboMembershipRepo.SelectedValue)
            objBankPaymentList._MembershipName = cboMembershipRepo.SelectedItem.Text
            'Sohail (08 Jul 2021) -- End

            Return True
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try

    End Function


    'Sohail (01 Apr 2014) -- Start
    'ENHANCEMENT - 
    Private Sub GetValue()
        Dim objUserDefRMode As New clsUserDef_ReportMode
        Dim dsList As DataSet
        Try
            'SHANI (13 Mar 2015) -- Start
            'Enhancement - New EFT Report Custom CSV Report.
            'dsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList)
            dsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList, 0)
            'SHANI (13 Mar 2015) -- End

            If dsList.Tables("List").Rows.Count > 0 Then

                For Each dsRow As DataRow In dsList.Tables("List").Rows
                    Select Case CInt(dsRow.Item("headtypeid"))

                        Case enHeadTypeId.Company_Bank
                            cboCompanyBankName.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString
                            Call cboCompanyBankName_SelectedIndexChanged(cboCompanyBankName, New EventArgs)

                        Case enHeadTypeId.Company_Branch
                            cboCompanyBranchName.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Company_Account_No
                            cboCompanyAccountNo.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Report_Mode
                            cboReportMode.SelectedIndex = CInt(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Employee_Bank
                            cboBankName.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString
                            Call cboBankName_SelectedIndexChanged(cboBankName, New EventArgs)

                        Case enHeadTypeId.Employee_Branch
                            cboBranchName.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Country
                            cboCountry.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Paid_Currency
                            cboCurrency.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Show_Signatory_1
                            chkSignatory1.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Signatory_2
                            chkSignatory2.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Signatory_3
                            chkSignatory3.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Defined_Signatory_1
                            chkDefinedSignatory.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Defined_Signatory_2
                            chkDefinedSignatory2.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Defined_Signatory_3
                            chkDefinedSignatory3.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Bank_Branch_Group
                            chkShowGroupByBankBranch.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Employee_Sign
                            chkEmployeeSign.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Include_Inactive_Employee
                            'chkIncludeInactiveEmp.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Period
                            chkShowPayrollPeriod.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Sort_Code
                            chkShowSortCode.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Separate_FName_Surname
                            chkShowFNameSeparately.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Bank_Code
                            chkShowBankCode.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Branch_Code
                            chkShowBranchCode.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_Account_Type
                            chkShowAccountType.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Show_employee_code
                            chkShowEmployeeCode.Checked = CBool(dsRow.Item("transactionheadid"))

                            'Sohail (28 Feb 2017) -- Start
                            'Enhancement - Add New Bank Payment List Integration for TAMA
                        Case enHeadTypeId.Letterhead
                            chkLetterhead.Checked = CBool(dsRow.Item("transactionheadid"))

                        Case enHeadTypeId.Address_to_Employee_Bank
                            chkAddresstoEmployeeBank.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Sohail (28 Feb 2017) -- End

                            'Sohail (12 Feb 2018) -- Start
                            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                        Case enHeadTypeId.Present_Days
                            cboPresentDays.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Basic_Salary
                            cboBasicSalary.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case enHeadTypeId.Social_Security
                            cboSocialSecurity.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString
                            'Sohail (12 Feb 2018) -- End

                            'Sohail (16 Apr 2018) -- Start
                            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
                        Case CInt(enHeadTypeId.ExtraIncome)
                            cboExtraIncome.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case CInt(enHeadTypeId.AbsentDays)
                            cboAbsentDays.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString

                        Case CInt(enHeadTypeId.IdentityType)
                            txtIdentityType.Text = dsRow.Item("transactionheadid").ToString

                        Case CInt(enHeadTypeId.PaymentType)
                            txtPaymentType.Text = dsRow.Item("transactionheadid").ToString
                            'Sohail (16 Apr 2018) -- End

                            'Sohail (19 Apr 2018) -- Start
                            'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
                        Case CInt(enHeadTypeId.CustomCurrencyFormat)
                            txtCustomCurrFormat.Text = dsRow.Item("transactionheadid").ToString
                            'Sohail (19 Apr 2018) -- End

                            'Hemant (16 Jul 2020) -- Start
                            'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
                        Case CInt(enHeadTypeId.Save_As_TXT)
                            chkSaveAsTXT_WPS.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Hemant (16 Jul 2020) -- End

                            'Hemant (19 June 2019) -- Start
                            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                        Case enHeadTypeId.Show_Selected_Bank_Info
                            chkShowSelectedBankInfo.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Hemant (19 June 2019) -- End

                            'Hemant (20 Jul 2019) -- Start
                            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                        Case enHeadTypeId.Show_Company_Group_Info
                            chkShowCompanyGrpInfo.Checked = CBool(dsRow.Item("transactionheadid"))
                            'Hemant (20 Jul 2019) -- End

                            'Sohail (08 Jul 2021) -- Start
                            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
                        Case CInt(enHeadTypeId.Membership)
                            cboMembershipRepo.SelectedValue = CInt(dsRow.Item("transactionheadid")).ToString
                            'Sohail (08 Jul 2021) -- End

                    End Select
                Next

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            objUserDefRMode = Nothing
        End Try
    End Sub
    'Sohail (01 Apr 2014) -- End


    'SHANI [13 Mar 2015]-START
    'Enhancement - New EFT Report Custom CSV Report.
    Private Sub GetEFTValue()
        Dim dsList As DataSet
        Try
            'Sohail (25 Mar 2015) -- Start
            'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
            'dsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList, mintModeId)
            dsList = objUserDefRMode.GetList("List", enArutiReport.BankPaymentList, mintModeId, CInt(CType(cboReportType.SelectedItem, ListItem).Value))
            'Sohail (25 Mar 2015) -- End
            If dsList.Tables("List").Rows.Count > 0 Then
                'Hemant (27 June 2019) -- Start
                'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                'For i = 0 To 2
                For i As Integer = 0 To GetEnumHighestValue(Of enReportMode)()
                    'Hemant (27 June 2019) -- End
                    If i = enReportMode.CUSTOM_COLUMNS Then 'Column order
                        marrEFTCustomColumnIds.AddRange(dsList.Tables("List").Rows(i).Item("transactionheadid").ToString.Split(CChar(",")))

                    ElseIf i = enReportMode.SHOW_REPORT_HEADER Then 'Show Column Header
                        chkShowColumnHeader.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))

                        'Sohail (13 Apr 2016) -- Start
                        'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                    ElseIf i = enReportMode.MEMBERSHIP Then
                        cboMembership.SelectedValue = CInt(dsList.Tables("List").Rows(i).Item("transactionheadid")).ToString

                        If CInt(cboMembership.SelectedValue) > 0 Then
                            cboMembership.Enabled = True
                        Else
                            cboMembership.SelectedValue = "0"
                            cboMembership.Enabled = False
                        End If
                        'Sohail (13 Apr 2016) -- End
                        'Hemant (27 June 2019) -- Start
                        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                    ElseIf i = enReportMode.SAVE_AS_TXT Then 'SAVE AS TXT
                        If dsList.Tables("List").Rows.Count > 3 Then
                            chkSaveAsTXT.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        End If


                    ElseIf i = enReportMode.TAB_DELIMITER Then 'TAB DELIMITER
                        If dsList.Tables("List").Rows.Count > 4 Then
                            chkTABDelimiter.Checked = CBool(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        End If
                        'Hemant (27 June 2019) -- End

                        'Hemant (02 Jul 2020) -- Start
                        'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                    ElseIf i = enReportMode.DATE_FORMAT Then 'DATE FORMAT
                        If dsList.Tables("List").Rows.Count > 5 Then
                            txtDateFormat.Text = CStr(dsList.Tables("List").Rows(i).Item("transactionheadid"))
                        End If
                        'Hemant (02 Jul 2020) -- End

                    End If
                Next

            End If
        Catch ex As Exception
            popup_EFTCustom.Show()
            'Throw New Exception(ex.Message & "; Procedure : GetValue ; Module Name : " & mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub SetEFTValue()
        Try
            objUserDefRMode._Reportmodeid = mintModeId
            objUserDefRMode._Reporttypeid = 0
            objUserDefRMode._Reportunkid = enArutiReport.BankPaymentList
        Catch ex As Exception
            popup_EFTCustom.Show()
            'Throw New Exception(ex.Message & "; Procedure : SetValue ; Module Name : " & mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Private Sub FillEFTList()
        Dim objMaster As New clsMasterData
        Dim dsTicked As DataSet = Nothing
        Dim dsUnticked As DataSet = Nothing
        Dim lvItem As ListViewItem
        Try
            Dim xColCheck As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
            Dim xColName As New DataColumn("ID", System.Type.GetType("System.Int64"))
            Dim xColID As New DataColumn("Name", System.Type.GetType("System.String"))

            dtEFTTable.Columns.Add(xColCheck)
            dtEFTTable.Columns.Add(xColID)
            dtEFTTable.Columns.Add(xColName)

            Dim dicTicked As Dictionary(Of Integer, DataRow) = Nothing
            If marrEFTCustomColumnIds.Count > 0 Then
                dsTicked = objMaster.GetComboListForEFTCustomColumns("List", False, "ID IN (" & String.Join(",", CType(marrEFTCustomColumnIds.ToArray(Type.GetType("System.String")), String())) & ") ")
                dicTicked = (From p In dsTicked.Tables(0).AsEnumerable Select New With {.ID = CInt(p.Item("Id")), .DATAROW = p}).ToDictionary(Function(x) CInt(x.ID), Function(y) y.DATAROW)
                dsUnticked = objMaster.GetComboListForEFTCustomColumns("List", False, "ID NOT IN (" & String.Join(",", CType(marrEFTCustomColumnIds.ToArray(Type.GetType("System.String")), String())) & ") ")
            Else
                dsUnticked = objMaster.GetComboListForEFTCustomColumns("List", False)
            End If

            If dsTicked IsNot Nothing Then

                Dim dtRow As DataRow = Nothing
                For Each id As Integer In CType(marrEFTCustomColumnIds.Clone, ArrayList)
                    dtRow = dicTicked.Item(id)
                    Dim xRow As DataRow = dtEFTTable.NewRow
                    xRow("IsChecked") = True
                    xRow("ID") = dtRow.Item("Id")
                    xRow("Name") = dtRow.Item("NAME")
                    dtEFTTable.Rows.Add(xRow)
                Next

            End If

            If dsUnticked IsNot Nothing Then
                For Each dtRow As DataRow In dsUnticked.Tables(0).Rows
                    Dim xRow As DataRow = dtEFTTable.NewRow
                    xRow("IsChecked") = False
                    xRow("ID") = dtRow.Item("Id")
                    xRow("Name") = dtRow.Item("NAME")
                    dtEFTTable.Rows.Add(xRow)
                    lvItem = Nothing
                Next
            End If
            With lvEFTCustomColumns
                .DataSource = dtEFTTable
                .DataBind()
            End With

        Catch ex As Exception
            popup_EFTCustom.Show()
            'Throw New Exception(ex.Message & "; Procedure : FillList ; Module Name : " & mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
        Finally
            lvItem = Nothing
        End Try
    End Sub

    Private Function IsEFTValidate() As Boolean
        Try
            Select Case CInt(mintModeId)
                Case enEFT_Export_Mode.CSV, enEFT_Export_Mode.XLS
                    Dim xRow() As DataRow = dtEFTTable.Select("IsChecked=True")

                    If xRow.Length <= 0 Then
                        'Hemant (27 June 2019) -- Start
                        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                        'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 1, "Please Select atleast one column to export EFT report."), Me)
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 1, "Please Select atleast one column to export EFT report."), Me)
                        'Hemant (27 June 2019) -- End
                        lvEFTCustomColumns.Focus()
                        popup_EFTCustom.Show()
                        Return False
                        'Sohail (13 Apr 2016) -- Start
                        'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                    ElseIf CBool(dtEFTTable.Select("Id = " & enEFT_EFT_Custom_Columns.EMP_MEMBERSHIP_NO & " ")(0).Item("isChecked")) = True AndAlso CInt(cboMembership.SelectedValue) <= 0 Then
                        'Hemant (27 June 2019) -- Start
                        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                        'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 3, "Please Select Membership."), Me) 
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 3, "Please Select Membership."), Me)
                        'Hemant (27 June 2019) -- End
                        cboMembership.Focus()
                        popup_EFTCustom.Show()
                        Return False
                        'Sohail (13 Apr 2016) -- End
                        'Hemant (02 Jul 2020) -- Start
                        'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                    ElseIf CBool(dtEFTTable.Select("Id = " & enEFT_EFT_Custom_Columns.PAYMENT_DATE & " ")(0).Item("isChecked")) = True AndAlso txtDateFormat.Text = "" Then
                        DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 4, "Please Enter Date Format."), Me)
                        cboMembership.Focus()
                        popup_EFTCustom.Show()
                        Return False
                        'Hemant (02 Jul 2020) -- End
                    End If
            End Select

            mstrEFTCustomColumnsIds = String.Join(",", (From p In dtEFTTable Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("ID").ToString)).ToArray)
            mblnShowColumnHeader = chkShowColumnHeader.Checked
            'Sohail (13 Apr 2016) -- Start
            'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
            mintMembershipUnkId = CInt(cboMembership.SelectedValue)
            'Sohail (13 Apr 2016) -- End
            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            mblnSaveAsTXT = chkSaveAsTXT.Checked
            mblnTabDelimiter = chkTABDelimiter.Checked
            'Hemant (27 June 2019) -- End
            'Hemant (02 Jul 2020) -- Start
            'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
            mstrDateFormat = txtDateFormat.Text
            'Hemant (02 Jul 2020) -- End
            Call SetEFTValue()
            Return True
        Catch ex As Exception
            popup_EFTCustom.Show()
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "IsEFTValidate", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function

    Private Sub Export_CSV_XLS()
        Try
            If SetFilter() = False Then Exit Sub
            If IsEFTValidate() = False Then Exit Sub
            GUI.fmtCurrency = Session("fmtCurrency").ToString

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            'Sohail (09 Jan 2016) -- End

            If mintModeId = enEFT_Export_Mode.CSV Then
                objBankPaymentList._OpenAfterExport = False
                'Sohail (25 Mar 2015) -- Start
                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                'Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\EFT_CUSTOM_CSV.csv"
                Dim mstrPath As String = ""
                Dim strDilimiter As String = "" 'Hemant (27 June 2019) 
                'If objBankPaymentList.EFT_Custom_Columns_CSV_Export_GenerateReport(mstrPath, mstrEFTCustomColumnsIds, mblnShowColumnHeader) = True Then
                '    Session("ExFileName") = mstrPath
                '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                'End If
                Select Case CInt(CType(cboReportType.SelectedItem, ListItem).Value)
                    Case enBankPaymentReport.Electronic_Fund_Transfer
                        'Hemant (27 June 2019) -- Start
                        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                        If chkSaveAsTXT.Checked = True Then
                            mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\EFT_CUSTOM_TXT.txt"
                        Else
                            'Hemant (27 June 2019) -- End
                            mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\EFT_CUSTOM_CSV.csv"
                        End If

                        'Hemant (27 June 2019) -- Start
                        'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                        If chkTABDelimiter.Checked = True Then
                            strDilimiter = vbTab
                        Else
                            strDilimiter = ","
                        End If
                        'Hemant (27 June 2019) -- End

                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objBankPaymentList.EFT_Custom_Columns_CSV_Export_GenerateReport(mstrPath, mstrEFTCustomColumnsIds, mblnShowColumnHeader) = True Then
                        'Sohail (13 Apr 2016) -- Start
                        'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                        'If objBankPaymentList.EFT_Custom_Columns_CSV_Export_GenerateReport(Session("Database_Name").ToString, _
                        '                                                                   CInt(Session("UserId")), _
                        '                                                                   CInt(Session("Fin_year")), _
                        '                                                                   CInt(Session("CompanyUnkId")), _
                        '                                                                   objPeriod._Start_Date, _
                        '                                                                   objPeriod._End_Date, _
                        '                                                                   Session("UserAccessModeSetting").ToString, True, _
                        '                                                                    CBool(Session("IsIncludeInactiveEmp")), True, _
                        '                                                                   mstrPath, _
                        '                                                                   mstrEFTCustomColumnsIds, _
                        '                                                                   mblnShowColumnHeader, _
                        '                                                                   Session("fmtCurrency").ToString) = True Then
                        If objBankPaymentList.EFT_Custom_Columns_CSV_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                                           CInt(Session("UserId")), _
                                                                                           CInt(Session("Fin_year")), _
                                                                                           CInt(Session("CompanyUnkId")), _
                                                                                           objPeriod._Start_Date, _
                                                                                           objPeriod._End_Date, _
                                                                                           Session("UserAccessModeSetting").ToString, True, _
                                                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                                           mstrPath, _
                                                                                           mstrEFTCustomColumnsIds, _
                                                                                           mblnShowColumnHeader, _
                                                                                           Session("fmtCurrency").ToString, mintMembershipUnkId, _
                                                                                           strDilimiter, _
                                                                                           mstrDateFormat) = True Then
                            'Hemant (02 Jul 2020) -- [mstrDateFormat]
                            'Hemant (27 June 2019) -- [strDilimiter]
                            'Sohail (13 Apr 2016) -- End
                            'Shani(20-Nov-2015) -- End

                            Session("ExFileName") = mstrPath
                            'Gajanan (3 Jan 2019) -- Start
                            'Enhancement :Add Report Export Control 
                            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                            Export.Show()
                            'Gajanan (3 Jan 2019) -- End

                        End If

                    Case enBankPaymentReport.EFT_ADVANCE_SALARY_CSV
                        mstrPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\ADVANCE_SALARY_CUSTOM_CSV.csv"


                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objBankPaymentList.EFT_Advance_Salary_CSV_Export_GenerateReport(mstrPath, mstrEFTCustomColumnsIds, mblnShowColumnHeader) = True Then
                        If objBankPaymentList.EFT_Advance_Salary_CSV_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                                           CInt(Session("UserId")), _
                                                                                           CInt(Session("Fin_year")), _
                                                                                           CInt(Session("CompanyUnkId")), _
                                                                                           objPeriod._Start_Date, _
                                                                                           objPeriod._End_Date, _
                                                                                           Session("UserAccessModeSetting").ToString, True, _
                                                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                                           mstrPath, mstrEFTCustomColumnsIds, _
                                                                                           mblnShowColumnHeader, Session("fmtCurrency").ToString) = True Then
                            'Shani(20-Nov-2015) -- End

                            Session("ExFileName") = mstrPath
                            'Gajanan (3 Jan 2019) -- Start
                            'Enhancement :Add Report Export Control 
                            'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                            Export.Show()
                            'Gajanan (3 Jan 2019) -- End

                        End If

                End Select
                'Sohail (25 Mar 2015) -- End
            ElseIf mintModeId = enEFT_Export_Mode.XLS Then
                objBankPaymentList._OpenAfterExport = False
                Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
                'Sohail (25 Mar 2015) -- Start
                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                'If objBankPaymentList.EFT_Custom_Columns_XLS_Export_GenerateReport(mstrPath, mstrEFTCustomColumnsIds, mblnShowColumnHeader) = True Then
                '    If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                '        Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                '        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                '    End If
                'End If
                Select Case CInt(CType(cboReportType.SelectedItem, ListItem).Value)
                    Case enBankPaymentReport.Electronic_Fund_Transfer

                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objBankPaymentList.EFT_Custom_Columns_XLS_Export_GenerateReport(mstrPath, mstrEFTCustomColumnsIds, mblnShowColumnHeader) = True Then
                        'Sohail (13 Apr 2016) -- Start
                        'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                        'If objBankPaymentList.EFT_Custom_Columns_XLS_Export_GenerateReport(Session("Database_Name").ToString, _
                        '                                                                   CInt(Session("UserId")), _
                        '                                                                   CInt(Session("Fin_year")), _
                        '                                                                   CInt(Session("CompanyUnkId")), _
                        '                                                                   objPeriod._Start_Date, _
                        '                                                                   objPeriod._End_Date, _
                        '                                                                   Session("UserAccessModeSetting").ToString, True, _
                        '                                                                   CBool(Session("IsIncludeInactiveEmp")), True, _
                        '                                                                   mstrPath, mstrEFTCustomColumnsIds, _
                        '                                                                   mblnShowColumnHeader, _
                        '                                                                   CBool(Session("OpenAfterExport")), _
                        '                                                                   Session("fmtCurrency").ToString) = True Then
                        If objBankPaymentList.EFT_Custom_Columns_XLS_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                                           CInt(Session("UserId")), _
                                                                                           CInt(Session("Fin_year")), _
                                                                                           CInt(Session("CompanyUnkId")), _
                                                                                           objPeriod._Start_Date, _
                                                                                           objPeriod._End_Date, _
                                                                                           Session("UserAccessModeSetting").ToString, True, _
                                                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                                           mstrPath, mstrEFTCustomColumnsIds, _
                                                                                           mblnShowColumnHeader, _
                                                                                           CBool(Session("OpenAfterExport")), _
                                                                                           Session("fmtCurrency").ToString, mintMembershipUnkId, _
                                                                                           mstrDateFormat) = True Then
                            'Hemant (02 Jul 2020) -- [mstrDateFormat]
                            'Sohail (13 Apr 2016) -- End
                            'Shani(20-Nov-2015) -- End

                            If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                                Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                                'Gajanan (3 Jan 2019) -- Start
                                'Enhancement :Add Report Export Control 
                                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                                Export.Show()
                                'Gajanan (3 Jan 2019) -- End

                            End If
                        End If

                    Case enBankPaymentReport.EFT_ADVANCE_SALARY_XLS

                        'Shani(20-Nov-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'If objBankPaymentList.EFT_Advance_Salary_XLS_Export_GenerateReport(mstrPath, mstrEFTCustomColumnsIds, mblnShowColumnHeader) = True Then
                        If objBankPaymentList.EFT_Advance_Salary_XLS_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                                           CInt(Session("UserId")), _
                                                                                           CInt(Session("Fin_year")), _
                                                                                           CInt(Session("CompanyUnkId")), _
                                                                                           objPeriod._Start_Date, _
                                                                                           objPeriod._End_Date, _
                                                                                           Session("UserAccessModeSetting").ToString, True, _
                                                                                           CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                                           mstrPath, mstrEFTCustomColumnsIds, _
                                                                                           mblnShowColumnHeader, _
                                                                                           CBool(Session("OpenAfterExport")), _
                                                                                           Session("fmtCurrency").ToString) = True Then
                            'Shani(20-Nov-2015) -- End

                            If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                                Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                                'Gajanan (3 Jan 2019) -- Start
                                'Enhancement :Add Report Export Control 
                                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                                Export.Show()
                                'Gajanan (3 Jan 2019) -- End

                            End If
                        End If

                End Select
                'Sohail (25 Mar 2015) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'SHANI [13 Mar 2015]--END 

#End Region

#Region "Combobox Event"

    Protected Sub cboReportType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboReportType.SelectedIndexChanged
        Try
            chkEmployeeSign.Checked = False
            chkShowGroupByBankBranch.Checked = False
            chkSignatory1.Checked = False
            chkSignatory2.Checked = False
            chkSignatory3.Checked = False
            cboCountry.SelectedIndex = 0
            'cboCurrency.SelectedIndex = 0 'Sohail (01 Apr 2014)
            cboReportMode.Enabled = True
            chkShowFNameSeparately.Checked = False
            'Sohail (24 Dec 2013) -- Start
            'Enhancement - Oman
            chkShowPayrollPeriod.Checked = True
            chkShowSortCode.Checked = True
            'Sohail (24 Dec 2013) -- End
            cboCountry.Enabled = False
            cboCurrency.Enabled = True
            chkEmployeeSign.Enabled = False
            chkShowGroupByBankBranch.Enabled = False
            'Anjan [04 June 2014] -- Start
            'ENHANCEMENT : Implementing Language,requested by Andrew
            'Language.setLanguage(mstrModuleName)
            'Anjan [04 June 2014 ] -- End
            chkShowGroupByBankBranch.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 15, "Show Group By Bank / Branch")
            chkSignatory1.Enabled = False
            chkSignatory2.Enabled = False
            chkSignatory3.Enabled = False
            'Anjan [03 Feb 2014] -- Start
            'ENHANCEMENT : Requested by Rutta
            chkDefinedSignatory.Enabled = False
            chkDefinedSignatory2.Enabled = False
            chkDefinedSignatory3.Enabled = False

            chkShowBankCode.Checked = False
            chkShowBranchCode.Checked = False


            chkShowBankCode.Enabled = False
            chkShowBranchCode.Enabled = False

            'Sohail (01 Apr 2014) -- Start
            'ENHANCEMENT - 
            chkShowReportHeader.Visible = False
            chkShowReportHeader.Checked = False
            chkShowAccountType.Visible = False
            chkShowAccountType.Checked = False
            'Sohail (01 Apr 2014) -- End
            'Hemant (19 June 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            chkShowSelectedBankInfo.Visible = False
            'Hemant (19 June 2019) -- End
            'Hemant 20 Jul 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            chkShowCompanyGrpInfo.Visible = False
            'Hemant 20 Jul 2019) -- End

            'Sohail (18 Nov 2016) -- Start
            lblPostingDate.Enabled = False
            dtpPostingDate.Enabled = False
            'Sohail (18 Nov 2016) -- End

            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            pnlOtherSetting.Visible = False
            'Sohail (12 Feb 2018) -- End
            'Hemant (16 Jul 2020) -- Start
            'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
            chkSaveAsTXT_WPS.Visible = False
            'Hemant (16 Jul 2020) -- End

            'Sohail (08 Jul 2021) -- Start
            'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
            lblMembershipRepo.Visible = False
            cboMembershipRepo.Visible = False
            cboMembershipRepo.SelectedValue = "0"
            'Sohail (08 Jul 2021) -- End

            'Nilay (10-Nov-2016) -- Start
            'Enhancement : New EFT report : EFT ECO bank for OFFGRID
            If CInt(cboReportType.SelectedIndex) >= 0 Then
                cboCondition.SelectedIndex = 0
            End If
            'Nilay (10-Nov-2016) -- End

            Select Case CInt(CType(cboReportType.SelectedItem, ListItem).Value)

                Case enBankPaymentReport.Bank_payment_List
                    cboCountry.Enabled = True
                    chkEmployeeSign.Enabled = True
                    chkShowGroupByBankBranch.Enabled = True
                    chkSignatory1.Enabled = True
                    chkSignatory2.Enabled = True
                    chkSignatory3.Enabled = True
                    'Anjan [03 Feb 2014] -- Start
                    'ENHANCEMENT : Requested by Rutta
                    chkDefinedSignatory.Enabled = True
                    chkDefinedSignatory2.Enabled = True
                    chkDefinedSignatory3.Enabled = True

                    chkShowBankCode.Checked = True
                    chkShowBranchCode.Checked = True


                    chkShowBankCode.Enabled = True
                    chkShowBranchCode.Enabled = True
                    'Anjan [03 Feb 2014 ] -- End

                    'Sohail (01 Apr 2014) -- Start
                    'ENHANCEMENT - 
                    chkShowReportHeader.Visible = True
                    chkShowReportHeader.Checked = True
                    chkShowAccountType.Visible = True
                    chkShowAccountType.Checked = False
                    chkShowSortCode.Enabled = True
                    'Sohail (01 Apr 2014) -- End

                Case enBankPaymentReport.Electronic_Fund_Transfer
                    chkShowGroupByBankBranch.Enabled = True
                    'Anjan [04 June 2014] -- Start
                    'ENHANCEMENT : Implementing Language,requested by Andrew
                    'Language.setLanguage(mstrModuleName)
                    'Anjan [04 June 2014 ] -- End
                    chkShowGroupByBankBranch.Text = Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 16, "Show Group By Bank")
                    'Sohail (01 Apr 2014) -- Start
                    'ENHANCEMENT - 
                    chkShowReportHeader.Visible = True
                    chkShowReportHeader.Checked = True
                    'Sohail (01 Apr 2014) -- End
                    'Hemant (19 June 2019) -- Start
                    'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                    chkShowSelectedBankInfo.Visible = True
                    'Hemant (19 June 2019) -- End
                    'Hemant 20 Jul 2019) -- Start
                    'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
                    chkShowCompanyGrpInfo.Visible = True
                    'Hemant 20 Jul 2019) -- End
                    'Sohail (12 Feb 2018) -- Start
                    'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                Case enBankPaymentReport.Salary_Format_WPS
                    pnlOtherSetting.Visible = True
                    'Hemant (16 Jul 2020) -- Start
                    'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
                    chkSaveAsTXT_WPS.Visible = True
                    'Hemant (16 Jul 2020) -- End
                    'Sohail (12 Feb 2018) -- End

            End Select

            If CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.Electronic_Fund_Transfer Then

                chkShowFNameSeparately.Enabled = True
                'Sohail (24 Dec 2013) -- Start
                'Enhancement - Oman
                chkShowPayrollPeriod.Enabled = True
                chkShowSortCode.Enabled = True
                'Sohail (24 Dec 2013) -- End

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                chkShowEmployeeCode.Visible = True
                'Pinkal (12-Jun-2014) -- End

                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_CityDirect Then
                    lnkEFTCityBankExport.Visible = True
                Else
                    lnkEFTCityBankExport.Visible = False
                End If

                'Sohail (08 Dec 2014) -- Start
                'Enhancement - New Mobile Money EFT integration with MPESA.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_CBA Then
                    'lnkEFT_CBA_Export.Location = lnkEFTCityBankExport.Location
                    'lnkEFT_CBA_Export.Size = lnkEFTCityBankExport.Size
                    lnkEFT_CBA_Export.Visible = True
                Else
                    lnkEFT_CBA_Export.Visible = False
                End If

                'Sohail (24 Dec 2014) -- Start
                'AMI Enhancement - New EFT Report EFT EXIM.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_EXIM Then
                    'lnkEFT_EXIM_Export.Location = lnkEFTCityBankExport.Location
                    'lnkEFT_EXIM_Export.Size = lnkEFTCityBankExport.Size
                    lnkEFT_EXIM_Export.Visible = True
                Else
                    lnkEFT_EXIM_Export.Visible = False
                End If
                'Sohail (24 Dec 2014) -- End

                If CInt(Session("MobileMoneyEFTIntegration")) = enMobileMoneyEFTIntegration.MPESA Then
                    lnkMobileMoneyEFTMPesaExport.Visible = True
                Else
                    lnkMobileMoneyEFTMPesaExport.Visible = False
                End If
                'Sohail (08 Dec 2014) -- End

                'SHANI (13 Mar 2015) -- Start
                'Enhancement - New EFT Report Custom CSV Report.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_CUSTOM_CSV Then
                    lnkEFT_Custom_CSV_Export.Visible = True
                Else
                    lnkEFT_Custom_CSV_Export.Visible = False
                End If

                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_CUSTOM_XLS Then
                    lnkEFT_Custom_XLS_Export.Visible = True
                Else
                    lnkEFT_Custom_XLS_Export.Visible = False
                End If
                'SHANI (13 Mar 2015) -- End

                'Sohail (01 Jun 2016) -- Start
                'Enhancement -  New EFT Flex Cube Retail - GEFU for Finca Nigeria in 61.1 SP.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_FLEX_CUBE_RETAIL Then
                    lnkEFT_FlexCubeRetailGEFU_Export.Visible = True
                    'Nilay (10-Nov-2016) -- Start
                    'Enhancement : New EFT report : EFT ECO bank for OFFGRID
                    'lblPostingDate.Visible = True
                    'dtpPostingDate.Visible = True
                    lblPostingDate.Enabled = True
                    dtpPostingDate.Enabled = True
                    'Nilay (10-Nov-2016) -- End
                Else
                    lnkEFT_FlexCubeRetailGEFU_Export.Visible = False
                    'Nilay (10-Nov-2016) -- Start
                    'Enhancement : New EFT report : EFT ECO bank for OFFGRID
                    'lblPostingDate.Visible = False
                    'dtpPostingDate.Visible = False
                    'Sohail (18 Nov 2016) -- Start
                    'lblPostingDate.Enabled = False
                    'dtpPostingDate.Enabled = False
                    'Sohail (18 Nov 2016) -- End
                    'Nilay (10-Nov-2016) -- End
                End If
                'Sohail (01 Jun 2016) -- End

                'Nilay (10-Nov-2016) -- Start
                'Enhancement : New EFT report : EFT ECO bank for OFFGRID
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_ECO_BANK Then
                    lnkEFT_ECO_Bank.Visible = True
                    lblPostingDate.Enabled = True
                    dtpPostingDate.Enabled = True
                Else
                    lnkEFT_ECO_Bank.Visible = False
                    'Sohail (18 Nov 2016) -- Start
                    'lblPostingDate.Enabled = False
                    'dtpPostingDate.Enabled = False
                    'Sohail (18 Nov 2016) -- End
                End If
                'Nilay (10-Nov-2016) -- End

                'Sohail (28 Feb 2017) -- Start
                'Enhancement - Add New Bank Payment List Integration for TAMA
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_BANK_PAYMENT_LETTER Then
                    lnkBankPaymentLetter.Visible = True
                    chkLetterhead.Visible = True
                    chkAddresstoEmployeeBank.Visible = True
                Else
                    lnkBankPaymentLetter.Visible = False
                    chkLetterhead.Visible = False
                    chkAddresstoEmployeeBank.Visible = False
                End If
                'Sohail (28 Feb 2017) -- End


                'Varsha (20 Sep 2017) -- Start
                'Paytech Enhancement - 69.1 - EFT file for barclays bank should be in csv format. And contain the codes representing a specific item. A sample EFT file was shared with Rutta.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_BARCLAYS_BANK Then
                    lnkEFTBarclaysBankExport.Visible = True
                    lblPostingDate.Enabled = True
                    dtpPostingDate.Enabled = True
                Else
                    lnkEFTBarclaysBankExport.Visible = False
                End If
                'Varsha (20 Sep 2017)  -- End

                'S.SANDEEP [04-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_NATIONAL_BANK_OF_MALAWI Then
                    lnkEFTNationalBankMalawi.Visible = True
                Else
                    lnkEFTNationalBankMalawi.Visible = False
                End If
                'S.SANDEEP [04-May-2018] -- END

                'Sohail (04 Nov 2019) -- Start
                'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_FNB_BANK Then
                    lnkEFT_FNB_Bank_Export.Visible = True
                Else
                    lnkEFT_FNB_Bank_Export.Visible = False
                End If
                'Sohail (04 Nov 2019) -- End

                'Hemant (11 Dec 2019) -- Start
                'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_STANDARD_CHARTERED_BANK_S2B Then
                    lnkEFTStandardCharteredBank_S2B.Visible = True
                    'Sohail (15 Apr 2020) -- Start
                    'SANLAM LIFE INSURANCE Enhancement # 0004324 : Roll back to XLS format and additional column Posting date in MM/DD/YYYY.
                    lblPostingDate.Enabled = True
                    dtpPostingDate.Enabled = True
                    'Sohail (15 Apr 2020) -- End

                Else
                    lnkEFTStandardCharteredBank_S2B.Visible = False
                End If
                'Hemant (11 Dec 2019) -- End

                'Hemant (29 Jun 2020) -- Start
                'ENHANCEMENT(Benchmark) # 0004751: ABSA BANK EFT
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_ABSA_BANK Then
                    lnkEFT_ABSA_Bank.Visible = True
                Else
                    lnkEFT_ABSA_Bank.Visible = False
                End If
                'Hemant (29 Jun 2020) -- End

                'Hemant (24 Dec 2020) -- Start
                'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_NATIONAL_BANK_OF_MALAWI_XLSX Then
                    lnkEFTNationalBankMalawiXLSX.Visible = True

                    lblPostingDate.Enabled = True
                    dtpPostingDate.Enabled = True
                Else
                    lnkEFTNationalBankMalawiXLSX.Visible = False
                End If
                'Hemant (24 Dec 2020) -- End

                'Sohail (08 Jul 2021) -- Start
                'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_EQUITY_BANK_OF_KENYA Then
                    lnkEFTEquityBankKenya.Visible = True

                    lblMembershipRepo.Visible = True
                    cboMembershipRepo.Visible = True
                Else
                    lnkEFTEquityBankKenya.Visible = False
                End If

                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_NATIONAL_BANK_OF_KENYA Then
                    lnkEFTNationalBankKenya.Visible = True
                Else
                    lnkEFTNationalBankKenya.Visible = False
                End If
                'Sohail (08 Jul 2021) -- End

                'Sohail (27 Jul 2021) -- Start
                'Scania Kenya Enhancement : OLD - OLD-426 : New Bank EFT Development (EFT Citi Bank Kenya).
                If CInt(Session("EFTIntegration")) = enEFTIntegration.EFT_CITI_BANK_KENYA Then
                    lnkEFTCitiBankKenya.Visible = True
                Else
                    lnkEFTCitiBankKenya.Visible = False
                End If
                'Sohail (27 Jul 2021) -- End

            Else
                chkShowFNameSeparately.Enabled = False
                lnkEFTCityBankExport.Visible = False
                'Sohail (08 Dec 2014) -- Start
                'Enhancement - New EFT integration EFT CBA || New Mobile Money EFT integration with MPESA.
                lnkEFT_CBA_Export.Visible = False
                lnkMobileMoneyEFTMPesaExport.Visible = False
                'Sohail (08 Dec 2014) -- End
                lnkEFT_EXIM_Export.Visible = False 'Sohail (24 Dec 2014)
                'Sohail (24 Dec 2013) -- Start
                'Enhancement - Oman
                chkShowPayrollPeriod.Enabled = False
                chkShowSortCode.Enabled = False
                chkShowPayrollPeriod.Checked = False
                chkShowSortCode.Checked = False
                'Sohail (24 Dec 2013) -- End

                'SHANI (13 Mar 2015) -- Start
                'Enhancement - New EFT Report Custom CSV Report.
                lnkEFT_Custom_CSV_Export.Visible = False
                lnkEFT_Custom_XLS_Export.Visible = False
                'SHANI (13 Mar 2015) -- End
                'Sohail (01 Jun 2016) -- Start
                'Enhancement -  New EFT Flex Cube Retail - GEFU for Finca Nigeria in 61.1 SP.

                'Nilay (10-Nov-2016) -- Start
                'Enhancement : New EFT report : EFT ECO bank for OFFGRID
                'lblPostingDate.Visible = False
                'dtpPostingDate.Visible = False
                lblPostingDate.Enabled = False
                dtpPostingDate.Enabled = False
                'Nilay (10-Nov-2016) -- End

                lnkEFT_FlexCubeRetailGEFU_Export.Visible = False
                'Sohail (01 Jun 2016) -- End

                'Nilay (10-Nov-2016) -- Start
                'Enhancement : New EFT report : EFT ECO bank for OFFGRID
                lnkEFT_ECO_Bank.Visible = False
                'Nilay (10-Nov-2016) -- End
                'Sohail (28 Feb 2017) -- Start
                'Enhancement - Add New Bank Payment List Integration for TAMA
                lnkBankPaymentLetter.Visible = False
                chkLetterhead.Visible = False
                chkAddresstoEmployeeBank.Visible = False
                'Sohail (28 Feb 2017) -- End

                'Varsha (20 Sep 2017) -- Start
                'Paytech Enhancement - 69.1 - EFT file for barclays bank should be in csv format. And contain the codes representing a specific item. A sample EFT file was shared with Rutta.
                lnkEFTBarclaysBankExport.Visible = False
                'Varsha (20 Sep 2017) -- End

                'S.SANDEEP [04-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
                lnkEFTNationalBankMalawi.Visible = False
                'S.SANDEEP [04-May-2018] -- END
                'Sohail (04 Nov 2019) -- Start
                'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
                lnkEFT_FNB_Bank_Export.Visible = False
                'Sohail (04 Nov 2019) -- End
                'Hemant (11 Dec 2019) -- Start
                'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
                lnkEFTStandardCharteredBank_S2B.Visible = False
                'Hemant (11 Dec 2019) -- End
                'Hemant (29 Jun 2020) -- Start
                'ENHANCEMENT(Benchmark) # 0004751: ABSA BANK EFT
                lnkEFT_ABSA_Bank.Visible = False
                'Hemant (29 Jun 2020) -- End
                'Hemant (24 Dec 2020) -- Start
                'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
                lnkEFTNationalBankMalawiXLSX.Visible = False
                'Hemant (24 Dec 2020) -- End

                'Sohail (08 Jul 2021) -- Start
                'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
                lnkEFTEquityBankKenya.Visible = False
                lnkEFTNationalBankKenya.Visible = False
                'Sohail (08 Jul 2021) -- End

                'Sohail (27 Jul 2021) -- Start
                'Scania Kenya Enhancement : OLD - OLD-426 : New Bank EFT Development (EFT Citi Bank Kenya).
                lnkEFTCitiBankKenya.Visible = False
                'Sohail (27 Jul 2021) -- End
            End If

            If CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.EFT_SFIBANK Then

                cboEmployeeName.SelectedValue = "0"
                cboBankName.Enabled = False
                cboBranchName.SelectedValue = "0"
                cboBranchName.Enabled = False
                'txtEmpCode.Enabled = False

            ElseIf CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.EFT_StandardCharteredBank Then
                cboEmployeeName.SelectedValue = "0"
                cboBankName.Enabled = False
                cboBranchName.SelectedValue = "0"
                cboBranchName.Enabled = False

                'Sohail (25 Mar 2015) -- Start
                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance Payment.
            ElseIf CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.EFT_ADVANCE_SALARY_CSV Then
                lnkEFT_Custom_CSV_Export.Visible = True
            ElseIf CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.EFT_ADVANCE_SALARY_XLS Then
                lnkEFT_Custom_XLS_Export.Visible = True
                'Sohail (25 Mar 2015) -- End
            End If

            'Sohail (15 Mar 2014) -- Start
            'Enhancement - Grouping on cut off amount.
            'Call objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            'If (CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.Electronic_Fund_Transfer _
            '  AndAlso CInt(Session("Compcountryid")) = 162) OrElse CInt(cboReportType.SelectedValue) = enBankPaymentReport.Bank_Payment_Voucher Then 'OMAN EFT 
            'Pinkal (12-Jun-2014) -- Start  'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]  OrElse CInt(CType(cboReportType.SelectedItem, ComboBoxValue).Value) = enBankPaymentReport.Bank_Payment_Voucher
            If (CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Electronic_Fund_Transfer) _
              AndAlso CInt(Session("Compcountryid")) = 162) OrElse CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Bank_Payment_Voucher) Then 'OMAN EFT 
                'Sohail (12 Feb 2018) -- End

                chkEmployeeSign.Checked = False
                chkShowGroupByBankBranch.Checked = False
                chkSignatory1.Checked = False
                chkSignatory2.Checked = False
                chkSignatory3.Checked = False
                cboCountry.SelectedIndex = 0
                'cboCurrency.SelectedIndex = 0 'Sohail (01 Apr 2014)
                'mstrStringIds = ""
                'mstrStringName = ""
                'mintViewIdx = 0
                cboReportMode.Enabled = True
                chkShowFNameSeparately.Checked = False
                chkShowPayrollPeriod.Checked = True
                chkShowSortCode.Checked = True
                cboCountry.Enabled = False
                cboCurrency.Enabled = True
                chkEmployeeSign.Enabled = False
                chkShowGroupByBankBranch.Enabled = False
                chkSignatory1.Enabled = False
                chkSignatory2.Enabled = False
                chkSignatory3.Enabled = False
                chkShowPayrollPeriod.Checked = False
                chkShowSortCode.Checked = False
                chkShowPayrollPeriod.Enabled = False
                chkShowSortCode.Enabled = False
                chkShowFNameSeparately.Enabled = False
                chkDefinedSignatory.Enabled = False

                chkShowBankCode.Checked = False
                chkShowBranchCode.Checked = False
                chkDefinedSignatory2.Checked = False
                chkDefinedSignatory3.Checked = False

                chkShowBankCode.Enabled = False
                chkShowBranchCode.Enabled = False
                chkDefinedSignatory2.Enabled = False
                chkDefinedSignatory3.Enabled = False


                lblCutOffAmount.Visible = True
                txtCutOffAmount.Visible = True


                'Sohail (01 Apr 2014) -- Start
                'ENHANCEMENT - 
                'Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.Bank_payment_List)


                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                'Sohail (12 Feb 2018) -- Start
                'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                'If CInt(cboReportType.SelectedValue) = enBankPaymentReport.Bank_Payment_Voucher Then
                If CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Bank_Payment_Voucher) Then
                    'Sohail (12 Feb 2018) -- End
                    Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.Bank_Payment_Voucher)
                    chkShowReportHeader.Visible = True
                    chkShowReportHeader.Checked = True
                    chkShowEmployeeCode.Visible = False
                Else
                    Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.EFT_VOLTAMP)
                End If
                'Pinkal (12-Jun-2014) -- End
                'Sohail (01 Apr 2014) -- End
            Else
                lblCutOffAmount.Visible = False
                txtCutOffAmount.Visible = False
                txtCutOffAmount.Text = ""

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                chkShowEmployeeCode.Visible = True
                'Pinkal (12-Jun-2014) -- End

                Call objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
            End If
            'Sohail (15 Mar 2014) -- End

            'Sohail (12 Feb 2015) -- Start
            'Enhancement - EFT CBA with New CSV Format.
            mstrOrderByDisplay = objBankPaymentList.OrderByDisplay
            mstrOrderByQuery = objBankPaymentList.OrderByQuery
            'Sohail (12 Feb 2015) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboCompanyBankName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCompanyBankName.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsList As New DataSet
        Try
            dsList = objCompanyBank.GetComboList(CInt(Session("CompanyUnkId")), 2, "List", " cfbankbranch_master.bankgroupunkid = " & CInt(cboCompanyBankName.SelectedValue) & " ")
            With cboCompanyBranchName
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
            cboCompanyBranchName_SelectedIndexChanged(New Object(), New EventArgs())
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboCompanyBranchName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCompanyBranchName.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsCombos As New DataSet
        Try
            dsCombos = objCompanyBank.GetComboListBankAccount("Account", CInt(Session("CompanyUnkId")), True, CInt(cboCompanyBankName.SelectedValue), CInt(cboCompanyBranchName.SelectedValue))
            With cboCompanyAccountNo
                .DataValueField = "id"
                .DataTextField = "Name"
                .DataSource = dsCombos.Tables("Account")
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboBankName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankName.SelectedIndexChanged
        Dim objBankBranch As New clsbankbranch_master
        Dim dsList As New DataSet
        Try
            dsList = objBankBranch.getListForCombo("List", True, CInt(cboBankName.SelectedValue))
            With cboBranchName
                .DataValueField = "branchunkid"
                .DataTextField = "name"
                .DataSource = dsList.Tables("List")
                .DataBind()
                .SelectedValue = "0"
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim objPayment As New clsPayment_tran
        Dim dsCombos As DataSet
        Try
            dsCombos = objPayment.Get_DIST_ChequeNo("Cheque", True, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, Session("database_name").ToString, CInt(cboPeriod.SelectedValue))
            With cboChequeNo
                .DataValueField = "chequeno"
                .DataTextField = "chequeno"
                .DataSource = dsCombos.Tables("Cheque")
                .DataBind()
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
#End Region

#Region "LinkButton Event"

    Protected Sub lnkEFTCityBankExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTCityBankExport.Click
        Try
            'Sohail (08 Dec 2014) -- Start
            'Enhancement - New Mobile Money EFT integration with MPESA.
            'If CInt(cboReportType.SelectedIndex) > 0 AndAlso CInt(cboCurrency.SelectedValue) <= 0 Then
            '    'Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayError(ex, Me)
            '    cboCurrency.Focus()
            '    Exit Try
            'ElseIf CInt(cboCompanyBankName.SelectedValue) <= 0 Then
            '    'Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayError(ex, Me)
            '    cboCompanyBankName.Focus()
            '    Exit Try
            'ElseIf CInt(cboCompanyBranchName.SelectedValue) <= 0 Then
            '    'Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayError(ex, Me)
            '    cboCompanyBranchName.Focus()
            '    Exit Try
            'ElseIf CInt(cboCompanyAccountNo.SelectedValue) <= 0 Then
            '    'Language.setLanguage(mstrModuleName)
            '    DisplayMessage.DisplayError(ex, Me)
            '    cboCompanyAccountNo.Focus()
            '    Exit Try
            'ElseIf CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Sohail (08 Dec 2014) -- End
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            ElseIf Session("DatafileExportPath").ToString = "" Then
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Please Set Data File Export Path From Aruti Configuration -> Option -> Integration -> EFT Integration."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            ElseIf Session("DatafileName").ToString = "" Then
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Please Set Data File Name From Aruti Configuration -> Option -> Integration -> EFT Integration."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try

            End If

            'Sohail (08 Dec 2014) -- Start
            'Enhancement - New Mobile Money EFT integration with MPESA.
            'objBankPaymentList.SetDefaultValue()
            'If CInt(cboPeriod.SelectedValue) > 0 Then
            '    objBankPaymentList._PeriodId = CInt(cboPeriod.SelectedValue)
            'End If

            'If CInt(cboBankName.SelectedValue) > 0 Then
            '    objBankPaymentList._BankId = CInt(cboBankName.SelectedValue)
            '    objBankPaymentList._BankName = cboBankName.SelectedItem.Text
            'End If

            'If CInt(cboBranchName.SelectedValue) > 0 Then
            '    objBankPaymentList._BranchId = CInt(cboBranchName.SelectedValue)
            '    objBankPaymentList._BankBranchName = cboBranchName.SelectedItem.Text
            'End If

            'If CInt(cboCountry.SelectedValue) > 0 Then
            '    objBankPaymentList._CountryId = CInt(cboCountry.SelectedValue)
            '    objBankPaymentList._CountryName = cboCountry.SelectedItem.Text
            'End If

            'If CInt(cboEmployeeName.SelectedValue) > 0 Then
            '    objBankPaymentList._EmployeeUnkId = CInt(cboEmployeeName.SelectedValue)
            '    objBankPaymentList._EmpName = cboEmployeeName.SelectedItem.Text
            'End If

            ''Sohail (07 Dec 2013) -- Start
            ''Enhancement - OMAN
            ''If Trim(txtEmpCode.Text) <> "" Then
            ''    objBankPaymentList._EmpCode = txtEmpCode.Text
            ''End If
            'If cboChequeNo.SelectedIndex > 0 Then
            '    objBankPaymentList._ChequeNo = cboChequeNo.Text
            'Else
            '    objBankPaymentList._ChequeNo = ""
            'End If
            ''Sohail (07 Dec 2013) -- End


            'If Trim(txtAmount.Text) <> "" And Trim(txtAmountTo.Text) <> "" Then
            '    objBankPaymentList._Amount = txtAmount.Text
            '    objBankPaymentList._AmountTo = txtAmountTo.Text
            'End If

            'If chkEmployeeSign.Checked = True Then
            '    objBankPaymentList._IsEmployeeSign = True
            'End If
            'If chkSignatory1.Checked = True Then
            '    objBankPaymentList._IsSignatory1 = True
            'End If
            'If chkSignatory2.Checked = True Then
            '    objBankPaymentList._IsSignatory2 = True
            'End If
            'If chkSignatory3.Checked = True Then
            '    objBankPaymentList._IsSignatory3 = True
            'End If

            'objBankPaymentList._ShowFNameSurNameInSeparateColumn = chkShowFNameSeparately.Checked
            'Sohail (24 Dec 2013) -- Start
            'Enhancement - Oman
            'objBankPaymentList._ShowPayrollPeriod = chkShowPayrollPeriod.Checked
            'objBankPaymentList._ShowSortCode = chkShowSortCode.Checked
            ''Sohail (24 Dec 2013) -- End
            'objBankPaymentList._CurrencyId = CInt(cboCurrency.SelectedValue)
            'objBankPaymentList._CurrencyName = cboCurrency.SelectedItem.Text
            'objBankPaymentList._ShowGroupByBankBranch = chkShowGroupByBankBranch.Checked

            'objBankPaymentList._PeriodName = cboPeriod.Text

            'Dim objPeriod As New clscommom_period_Tran
            'objPeriod._Periodunkid = CInt(cboPeriod.SelectedValue)
            'objBankPaymentList._PeriodStartDate = objPeriod._Start_Date
            'objBankPaymentList._PeriodEndDate = objPeriod._End_Date
            'objPeriod = Nothing

            'objBankPaymentList._ReportTypeId = CType(cboReportType.SelectedItem, ListItem).Value
            'objBankPaymentList._ReportTypeName = cboReportType.SelectedItem.Text

            'objBankPaymentList._ReportModeId = CInt(cboReportMode.SelectedIndex)
            'objBankPaymentList._ReportModeName = cboReportMode.SelectedItem.Text

            'objBankPaymentList._CompanyBranchId = CInt(cboCompanyBranchName.SelectedValue)
            'objBankPaymentList._CompanyBankBranchName = cboCompanyBranchName.SelectedItem.Text
            'objBankPaymentList._CompanyBankAccountNo = cboCompanyAccountNo.SelectedItem.Text

            'objBankPaymentList._UserUnkId = Session("UserId")
            'objBankPaymentList._CompanyUnkId = Session("CompanyUnkId")
            'objBankPaymentList._UserAccessFilter = Session("AccessLevelFilterString")
            'Sohail (08 Dec 2014) -- End

            'objBankPaymentList._CompanyBankGroupId = Session("CompanyBankGroupId")
            'objBankPaymentList._CompanyBranchId = Session("CompanyBankBranchId")
            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False
            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            'objBankPaymentList._EFTCompanyName = Session("gstrCompanyName")
            objBankPaymentList._EFTCompanyName = Session("CompName").ToString
            'Sohail (09 Jan 2016) -- End
            objBankPaymentList._CityBankDataExportPath = "InValid Path"

            Dim strConfigExpPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & Session("DatafileName").ToString & ".txt"

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            'Sohail (09 Jan 2016) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objBankPaymentList.EFT_CityDirect_Export_GenerateReport(strConfigExpPath) = True Then
            If objBankPaymentList.EFT_CityDirect_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                       CInt(Session("UserId")), _
                                                                       CInt(Session("Fin_year")), _
                                                                       CInt(Session("CompanyUnkId")), _
                                                                       objPeriod._Start_Date, _
                                                                       objPeriod._End_Date, _
                                                                       Session("UserAccessModeSetting").ToString, True, _
                                                                       CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                       strConfigExpPath, _
                                                                       Session("CompName").ToString, _
                                                                       ConfigParameter._Object._CurrentDateAndTime, _
                                                                       Session("DatafileExportPath").ToString, _
                                                                       Session("SMimeRunPath").ToString, _
                                                                       Session("fmtCurrency").ToString) = True Then
                'Shani(20-Nov-2015) -- End

                Session("ExFileName") = strConfigExpPath
                'Shani [ 10 DEC 2014 ] -- START
                'Issue : Chrome is not supporting ShowModalDialog option now."
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('../Reports/default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

                'Shani [ 10 DEC 2014 ] -- END

                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage("Data Exported Successfuly.", Me)
                'Sohail (23 Mar 2019) -- End
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (08 Dec 2014) -- Start
    'Enhancement - New EFT Integration EFT CBA || New Mobile Money EFT integration with MPESA.
    Protected Sub lnkEFT_CBA_Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_CBA_Export.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            'Sohail (12 Feb 2015) -- Start
            'Enhancement - EFT CBA with New CSV Format.
            'Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            'If objBankPaymentList.EFT_CBA_Export_GenerateReport(mstrPath) = True Then
            '    If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
            '        Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
            '        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
            '    End If
            'End If
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\CBA.csv"

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            'Sohail (09 Jan 2016) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objBankPaymentList.EFT_CBA_Export_GenerateReport(mstrPath) = True Then
            If objBankPaymentList.EFT_CBA_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                CInt(Session("UserId")), _
                                                                CInt(Session("Fin_year")), _
                                                                CInt(Session("CompanyUnkId")), _
                                                                objPeriod._Start_Date, _
                                                                objPeriod._End_Date, _
                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                                Session("fmtCurrency").ToString) = True Then
                'Shani(20-Nov-2015) -- End

                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If
            'Sohail (12 Feb 2015) -- End
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (24 Dec 2014) -- Start
    'AMI Enhancement - New EFT Report EFT EXIM.
    Protected Sub lnkEFT_EXIM_Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_EXIM_Export.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If


            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            'Sohail (09 Jan 2016) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objBankPaymentList.EFT_EXIM_Export_GenerateReport(mstrPath) = True Then
            If objBankPaymentList.EFT_EXIM_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 objPeriod._Start_Date, _
                                                                 objPeriod._End_Date, _
                                                                 Session("UserAccessModeSetting").ToString, True, _
                                                                 CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                 mstrPath, CDate(Session("fin_startdate")), _
                                                                 CDate(Session("fin_enddate")), _
                                                                 CBool(Session("OpenAfterExport")), _
                                                                 Session("fmtCurrency").ToString) = True Then
                'Shani(20-Nov-2015) -- End

                If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End

                End If
            End If
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "lnkEFT_EXIM_Export_Click", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub
    'Sohail (24 Dec 2014) -- End

    Protected Sub lnkMobileMoneyEFTMPesaExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMobileMoneyEFTMPesaExport.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\MPesa.csv"

            'Sohail (09 Jan 2016) -- Start
            'Enhancement - Country Code, Skip Priority Flag and Charges Indicator option for EFT Citi Direct on Aruti Configuration for KBC.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            'Sohail (09 Jan 2016) -- End

            'Shani(20-Nov-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If objBankPaymentList.EFT_MPesa_Export_GenerateReport(mstrPath) = True Then
            If objBankPaymentList.EFT_MPesa_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                  CInt(Session("UserId")), _
                                                                  CInt(Session("Fin_year")), _
                                                                  CInt(Session("CompanyUnkId")), _
                                                                  objPeriod._Start_Date, _
                                                                  objPeriod._End_Date, _
                                                                  Session("UserAccessModeSetting").ToString, True, _
                                                                  CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                  mstrPath, Session("fmtCurrency").ToString) = True Then
                'Shani(20-Nov-2015) -- End

                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (08 Dec 2014) -- End

    'SHANI [13 Mar 2015]-START
    'Enhancement - New EFT Report Custom CSV Report.

    Private Sub lnkEFT_Custom_CSV_Export_LinkClicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_Custom_CSV_Export.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If
            mintModeId = -1
            mstrEFTCustomColumnsIds = String.Empty
            mblnShowColumnHeader = False
            marrEFTCustomColumnIds = New ArrayList
            dtEFTTable = New DataTable
            mintModeId = enEFT_Export_Mode.CSV
            Call GetEFTValue()
            Call FillEFTList()
            Call GetEFTValue() 'Sohail (13 Apr 2016)
            popup_EFTCustom.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Private Sub lnkEFT_Custom_XLS_Export_LinkClicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_Custom_XLS_Export.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If
            mintModeId = -1
            mstrEFTCustomColumnsIds = String.Empty
            mblnShowColumnHeader = False
            marrEFTCustomColumnIds = New ArrayList
            dtEFTTable = New DataTable
            mintModeId = enEFT_Export_Mode.XLS
            Call GetEFTValue()
            Call FillEFTList()
            Call GetEFTValue() 'Sohail (13 Apr 2016)
            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            chkSaveAsTXT.Visible = False
            chkSaveAsTXT.Checked = False
            chkTABDelimiter.Visible = False
            chkTABDelimiter.Checked = False
            'Hemant (27 June 2019) -- End
            popup_EFTCustom.Show()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (01 Jun 2016) -- Start
    'Enhancement -  New EFT Flex Cube Retail - GEFU for Finca Nigeria in 61.1 SP.
    Protected Sub lnkEFT_FlexCubeRetailGEFU_Export_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_FlexCubeRetailGEFU_Export.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then 'Nilay (10-Nov-2016) -- [dtpPostingDate.GetDate = Nothing]
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Please set Posting Date."), Me)
                'Sohail (23 Mar 2019) -- End
                dtpPostingDate.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\FlexCubeRetailGEFU.txt"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_Flex_Cube_Retail_GEFU_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                                CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                                objPeriod._Start_Date, _
                                                                                objPeriod._End_Date, _
                                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                                CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                                                Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (01 Jun 2016) -- End

    'Nilay (10-Nov-2016) -- Start
    'Enhancement : New EFT report : EFT ECO bank for OFFGRID
    Protected Sub lnkEFT_ECO_Bank_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFT_ECO_Bank.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Please set Posting Date."), Me)
                'Sohail (23 Mar 2019) -- End
                dtpPostingDate.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\ECOBank.csv"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_ECO_Bank(Session("Database_Name").ToString, _
                                               CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                               CInt(Session("CompanyUnkId")), _
                                               objPeriod._Start_Date, _
                                               objPeriod._End_Date, _
                                               Session("UserAccessModeSetting").ToString, True, _
                                               CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                               Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Nilay (10-Nov-2016) -- End

    'Sohail (28 Feb 2017) -- Start
    'Enhancement - Add New Bank Payment List Integration for TAMA
    Protected Sub lnkBankPaymentLetter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkBankPaymentLetter.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False


            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_Bank_Payment_Letter_Report(Session("Database_Name").ToString, _
                                                                 CInt(Session("UserId")), _
                                                                 CInt(Session("Fin_year")), _
                                                                 CInt(Session("CompanyUnkId")), _
                                                                 objPeriod._Start_Date, _
                                                                 objPeriod._End_Date, _
                                                                 Session("UserAccessModeSetting").ToString, True, _
                                                                 CBool(Session("IsIncludeInactiveEmp")), True, "", _
                                                                 Session("fmtCurrency").ToString, eZeeDate.convertDate(Session("EmployeeAsOnDate").ToString)) = True Then

                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Session("objRpt") = objBankPaymentList._Rpt
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (28 Feb 2017) -- End

    Protected Sub ChangeLocation(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim commandArgument As String = TryCast(sender, LinkButton).CommandArgument
            Dim rowIndex As Integer = TryCast(TryCast(sender, LinkButton).NamingContainer, GridViewRow).RowIndex
            Dim xRow As DataRow = dtEFTTable.Rows(rowIndex)
            Dim xNewRow As DataRow = dtEFTTable.NewRow
            xNewRow.ItemArray = xRow.ItemArray
            If commandArgument = "down" Then
                If rowIndex >= lvEFTCustomColumns.Rows.Count - 1 Then Exit Sub
                dtEFTTable.Rows.RemoveAt(rowIndex)
                dtEFTTable.Rows.InsertAt(xNewRow, rowIndex + 1)
                dtEFTTable.Rows.IndexOf(xNewRow)
            ElseIf commandArgument = "up" Then
                If rowIndex <= 0 Then Exit Sub
                dtEFTTable.Rows.Remove(xRow)
                dtEFTTable.Rows.InsertAt(xNewRow, rowIndex - 1)
                dtEFTTable.Rows.IndexOf(xNewRow)
            End If
            With lvEFTCustomColumns
                .DataSource = dtEFTTable
                .DataBind()
            End With
            dtEFTTable.AcceptChanges()

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_EFTCustom.Show()
        End Try
    End Sub
    'SHANI [13 Mar 2015]--END 


    'Varsha (20 Sep 2017) -- Start
    'Paytech Enhancement - 69.1 - EFT file for barclays bank should be in csv format. And contain the codes representing a specific item. A sample EFT file was shared with Rutta.
    Private Sub lnkEFTBarclaysBankExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTBarclaysBankExport.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Please set Posting Date."), Me)
                'Sohail (23 Mar 2019) -- End
                dtpPostingDate.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\BarclaysBank.csv"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_Barclays_Bank(Session("Database_Name").ToString, _
                                               CInt(Session("UserId")), _
                                               CInt(Session("Fin_year")), _
                                               CInt(Session("CompanyUnkId")), _
                                               objPeriod._Start_Date, _
                                               objPeriod._End_Date, _
                                               Session("UserAccessModeSetting").ToString, True, _
                                               CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                               Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = mstrPath
                'Gajanan (3 Jan 2019) -- Start
                'Enhancement :Add Report Export Control 
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                Export.Show()
                'Gajanan (3 Jan 2019) -- End

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Varsha (20 Sep 2017) -- End

    'S.SANDEEP [04-May-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
    Protected Sub lnkEFTNationalBankMalawi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTNationalBankMalawi.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                'Sohail (23 Mar 2019) -- End
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_National_Bank_Malawi_GenerateReport(Session("Database_Name").ToString, _
                                                                                CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                                objPeriod._Start_Date, _
                                                                                objPeriod._End_Date, _
                                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                                CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                                                Session("fmtCurrency").ToString) = True Then

                If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End

                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'S.SANDEEP [04-May-2018] -- END
    'Sohail (04 Nov 2019) -- Start
    'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
    Private Sub lnkEFT_FNB_Bank_Export_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkEFT_FNB_Bank_Export.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\FNB.csv"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_FNB_Bank_Export_GenerateReport(Session("Database_Name").ToString, _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               objPeriod._Start_Date, _
                                                               objPeriod._End_Date, _
                                                               Session("UserAccessModeSetting").ToString, True, _
                                                               CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                               Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = mstrPath
                Export.Show()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (04 Nov 2019) -- End

    'Hemant (11 Dec 2019) -- Start
    'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
    Private Sub lnkEFTStandardCharteredBank_S2B_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkEFTStandardCharteredBank_S2B.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
                'Sohail (15 Apr 2020) -- Start
                'SANLAM LIFE INSURANCE Enhancement # 0004324 : Roll back to XLS format and additional column Posting date in MM/DD/YYYY.
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Please set Posting Date."), Me)
                dtpPostingDate.Focus()
                Exit Try
                'Sohail (15 Apr 2020) -- End
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            'Sohail (31 Mar 2020) -- Start
            'SANLAM LIFE INSURANCE Enhancement # 0004324 : Assist to change the File Extension from XLS to CSV.
            'Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            'Sohail (15 Apr 2020) -- Start
            'SANLAM LIFE INSURANCE Enhancement # 0004324 : Roll back to XLS format and additional column Posting date in MM/DD/YYYY.
            'Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\S2B.csv"
            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
            'Sohail (15 Apr 2020) -- End
            'Sohail (31 Mar 2020) -- End

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_Standard_Chartered_Bank_S2B_Report(Session("Database_Name").ToString, _
                                                                                CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                                objPeriod._Start_Date, _
                                                                                objPeriod._End_Date, _
                                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                                CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                                                Session("fmtCurrency").ToString) = True Then

                'Sohail (31 Mar 2020) -- Start
                'SANLAM LIFE INSURANCE Enhancement # 0004324 : Assist to change the File Extension from XLS to CSV.
                'If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                '    Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                '    Export.Show()

                'End If
                'Sohail (15 Apr 2020) -- Start
                'SANLAM LIFE INSURANCE Enhancement # 0004324 : Roll back to XLS format and additional column Posting date in MM/DD/YYYY.
                'Session("ExFileName") = mstrPath
                'Export.Show()
                If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                    Export.Show()
                End If
                'Sohail (15 Apr 2020) -- End
                'Sohail (31 Mar 2020) -- End
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (11 Dec 2019) -- End

    'Hemant (29 Jun 2020) -- Start
    'ENHANCEMENT(Benchmark) # 0004751: ABSA BANK EFT
    Private Sub lnkEFT_ABSA_Bank_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkEFT_ABSA_Bank.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                eZeeMsgBox.Show(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), enMsgBoxStyle.Information)
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\ABSA.csv"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            If objBankPaymentList.EFT_ABSA_Bank_Report(Session("Database_Name").ToString, _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               objPeriod._Start_Date, _
                                                               objPeriod._End_Date, _
                                                               Session("UserAccessModeSetting").ToString, True, _
                                                               CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                               Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = mstrPath
                Export.Show()

            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Hemant (29 Jun 2020) -- End
    'Hemant (24 Dec 2020) -- Start
    'Enhancement # OLD-221 : Blantyre Water Board - EFT National Bank of Malawi XLSX
    Protected Sub lnkEFTNationalBankMalawiXLSX_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTNationalBankMalawiXLSX.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
            ElseIf dtpPostingDate.GetDate = Nothing OrElse eZeeDate.convertDate(dtpPostingDate.GetDate) = "19000101" Then
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 23, "Please set Posting Date."), Me)
                dtpPostingDate.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            Dim strConfigExpPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\EFT_National_Bank.xlsx"

            If objBankPaymentList.EFT_National_Bank_Malawi_XLSX_GenerateReport(Session("Database_Name").ToString, _
                                                                                CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                                objPeriod._Start_Date, _
                                                                                objPeriod._End_Date, _
                                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                                CBool(Session("IsIncludeInactiveEmp")), True, strConfigExpPath, _
                                                                                Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = strConfigExpPath
                Export.Show()
                DisplayMessage.DisplayMessage("Data Exported Successfuly.", Me)
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Hemant (24 Dec 2020) -- End

    'Sohail (08 Jul 2021) -- Start
    'Bio Corn Oil Enhancement : OLD - 420 & OLD - 421 : New EFT Integrations EFT Equity Bank of Kenya and EFT National Bank of Kenya.
    Protected Sub lnkEFTEquityBankKenya_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTEquityBankKenya.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
            ElseIf CInt(cboMembershipRepo.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 31, "Please select membership to generate report."), Me)
                cboMembershipRepo.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_Equity_Bank_Kenya_GenerateReport(Session("Database_Name").ToString, _
                                                                                CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                                objPeriod._Start_Date, _
                                                                                objPeriod._End_Date, _
                                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                                CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                                                Session("fmtCurrency").ToString) = True Then

                If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                    Export.Show()

                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub

    Protected Sub lnkEFTNationalBankKenya_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTNationalBankKenya.Click
        Try
            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
            End If

            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False

            Dim mstrPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_National_Bank_Kenya_GenerateReport(Session("Database_Name").ToString, _
                                                                                CInt(Session("UserId")), _
                                                                                CInt(Session("Fin_year")), _
                                                                                CInt(Session("CompanyUnkId")), _
                                                                                objPeriod._Start_Date, _
                                                                                objPeriod._End_Date, _
                                                                                Session("UserAccessModeSetting").ToString, True, _
                                                                                CBool(Session("IsIncludeInactiveEmp")), True, mstrPath, _
                                                                                Session("fmtCurrency").ToString) = True Then

                If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = mstrPath & "\" & objBankPaymentList._FileNameAfterExported
                    Export.Show()

                End If
            End If

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
        End Try
    End Sub
    'Sohail (08 Jul 2021) -- End

    'Sohail (27 Jul 2021) -- Start
    'Scania Kenya Enhancement : OLD - OLD-426 : New Bank EFT Development (EFT Citi Bank Kenya).
    Protected Sub lnkEFTCitiBankKenya_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEFTCitiBankKenya.Click
        Try

            If SetFilter() = False Then Exit Try

            If CInt(cboReportMode.SelectedIndex) = 0 Then 'DRAFT mode
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 14, "Please select Authorize mode to generate report."), Me)
                cboReportMode.Focus()
                Exit Try
            ElseIf Session("DatafileExportPath").ToString = "" Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 17, "Please Set Data File Export Path From Aruti Configuration -> Option -> Integration -> EFT Integration."), Me)
                cboReportMode.Focus()
                Exit Try
            ElseIf Session("DatafileName").ToString = "" Then
                'Language.setLanguage(mstrModuleName)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 18, "Please Set Data File Name From Aruti Configuration -> Option -> Integration -> EFT Integration."), Me)
                cboReportMode.Focus()
                Exit Try

            End If


            GUI.fmtCurrency = Session("fmtCurrency").ToString
            objBankPaymentList._OpenAfterExport = False
            objBankPaymentList._EFTCompanyName = Session("CompName").ToString
            objBankPaymentList._CityBankDataExportPath = "InValid Path"

            Dim strConfigExpPath As String = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & Session("DatafileName").ToString & ".txt"

            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)

            If objBankPaymentList.EFT_CityBankKenya_Export_GenerateReport(Session("Database_Name").ToString, _
                                                                       CInt(Session("UserId")), _
                                                                       CInt(Session("Fin_year")), _
                                                                       CInt(Session("CompanyUnkId")), _
                                                                       objPeriod._Start_Date, _
                                                                       objPeriod._End_Date, _
                                                                       Session("UserAccessModeSetting").ToString, True, _
                                                                       CBool(Session("IsIncludeInactiveEmp")), True, _
                                                                       strConfigExpPath, _
                                                                       Session("CompName").ToString, _
                                                                       ConfigParameter._Object._CurrentDateAndTime, _
                                                                       Session("DatafileExportPath").ToString, _
                                                                       Session("SMimeRunPath").ToString, _
                                                                       Session("fmtCurrency").ToString) = True Then

                Session("ExFileName") = strConfigExpPath
                Export.Show()

                DisplayMessage.DisplayMessage("Data Exported Successfuly.", Me)
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (27 Jul 2021) -- End

#End Region

#Region "CheckBox Event"

    Protected Sub ChkShowGroupByBankBranch_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowGroupByBankBranch.CheckedChanged
        Try
            objBankPaymentList._ShowGroupByBankBranch = chkShowGroupByBankBranch.Checked
            Call objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    'Sohail (01 Apr 2014) -- Start
    'ENHANCEMENT - 
    Protected Sub chkShowEmployeeCode_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowEmployeeCode.CheckedChanged
        Try
            objBankPaymentList._ShowEmployeeCode = chkShowEmployeeCode.Checked
            If chkShowGroupByBankBranch.Checked = True OrElse (CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.Electronic_Fund_Transfer AndAlso CInt(Session("Compcountryid")) = 162) Then
                objBankPaymentList.Create_OnDetailReportWithoutBankBranch()
            Else
                objBankPaymentList.Create_OnDetailReport()
            End If
            If CInt(CType(cboReportType.SelectedItem, ListItem).Value) = enBankPaymentReport.Electronic_Fund_Transfer _
               AndAlso CInt(Session("Compcountryid")) = 162 Then 'OMAN EFT 
                Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.EFT_VOLTAMP)
            Else
                Call objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
            End If
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub
    'Sohail (01 Apr 2014) -- End


    'SHANI [13 Mar 2015]-START
    'Enhancement - New EFT Report Custom CSV Report.
    Public Sub lvEFTCustomColumns_ItemChecked(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        Try
            If chk Is Nothing Then Exit Try
            If lvEFTCustomColumns.Rows.Count <= 0 Then Exit Sub
            Select Case chk.ToolTip.ToUpper
                Case "ALL"
                    For Each dgvRow As GridViewRow In lvEFTCustomColumns.Rows
                        Dim cb As CheckBox = CType(lvEFTCustomColumns.Rows(dgvRow.RowIndex).FindControl("chkSelect"), CheckBox)
                        cb.Checked = chk.Checked
                    Next
                    dtEFTTable.AsEnumerable().Cast(Of DataRow).ToList.ForEach(Function(x) CheckUncheckAll(x, chk.Checked))
                    dtEFTTable.AcceptChanges()

                    'Sohail (13 Apr 2016) -- Start
                    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                    If chk.Checked = True Then
                        cboMembership.Enabled = True
                    Else
                        cboMembership.SelectedValue = "0"
                        cboMembership.Enabled = False
                    End If
                    'Sohail (13 Apr 2016) -- End
                    'Hemant (02 Jul 2020) -- Start
                    'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                    If chk.Checked = True Then
                        txtDateFormat.Enabled = True
                    Else
                        txtDateFormat.Text = "ddMMyyyy"
                        txtDateFormat.Enabled = False
                    End If
                    'Hemant (02 Jul 2020) -- End
                Case "CHECKED"
                    Dim gvRow As GridViewRow = CType(chk.NamingContainer, GridViewRow)
                    Dim xRow() As DataRow = dtEFTTable.Select("ID=" & dtEFTTable.Rows(gvRow.RowIndex)("Id").ToString)
                    If xRow.Length > 0 Then
                        xRow(0).Item("IsChecked") = chk.Checked
                        xRow(0).AcceptChanges()
                    End If

                    'Sohail (13 Apr 2016) -- Start
                    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                    If CInt(dtEFTTable.Rows(gvRow.RowIndex).Item("Id").ToString) = enEFT_EFT_Custom_Columns.EMP_MEMBERSHIP_NO Then
                        If chk.Checked = True Then
                            cboMembership.Enabled = True
                        Else
                            cboMembership.SelectedValue = "0"
                            cboMembership.Enabled = False
                        End If
                    End If
                    'Sohail (13 Apr 2016) -- End

                    'Hemant (02 Jul 2020) -- Start
                    'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                    If CInt(dtEFTTable.Rows(gvRow.RowIndex).Item("Id").ToString) = enEFT_EFT_Custom_Columns.PAYMENT_DATE Then
                        If chk.Checked = True Then
                            txtDateFormat.Enabled = True
                        Else
                            txtDateFormat.Text = "ddMMyyyy"
                            txtDateFormat.Enabled = False
                        End If
                    End If
                    'Hemant (02 Jul 2020) -- End

            End Select
            marrEFTCustomColumnIds.AddRange((From p In dtEFTTable Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("ID").ToString)).ToArray)


        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        Finally
            popup_EFTCustom.Show()
        End Try
    End Sub

    Private Function CheckUncheckAll(ByVal x As DataRow, ByVal blnCheckAll As Boolean) As Boolean
        Try
            x.Item("ischecked") = blnCheckAll
            Return True
        Catch ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", ex.Message, "CheckUncheckAll", mstrModuleName)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Function
    'SHANI [13 Mar 2015]--END 

#End Region

#Region "Button Event"

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReport.Click
        Try
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                'Language.setLanguage(mstrModuleName)
                'Sohail (23 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
                'DisplayMessage.DisplayError(ex, Me)
                DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName, 1, "Period is mandatory information.Please select Period."), Me)
                'Sohail (23 Mar 2019) -- End
                cboPeriod.Focus()
                Exit Sub
            End If

            If Not SetFilter() Then Exit Sub

            'Pinkal (16-Apr-2016) -- Start
            'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
            Call SetDateFormat()
            'Pinkal (16-Apr-2016) -- End

            'Sohail (21 Jan 2016) -- Start
            'Enhancement - 58.1 changes in Web.
            Dim objPeriod As New clscommom_period_Tran
            objPeriod._Periodunkid(Session("Database_Name").ToString) = CInt(cboPeriod.SelectedValue)
            'Sohail (21 Jan 2016) -- End

            'Sohail (15 Mar 2014) -- Start
            'Enhancement - Grouping on cut off amount.
            'objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
            'objBankPaymentList.generateReport(CInt(CType(cboReportType.SelectedItem, ListItem).Value), enPrintAction.None, enExportAction.None)
            If cboReportType.SelectedIndex = 1 AndAlso CInt(Session("Compcountryid")) = 162 Then 'OMAN EFT
                Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.Bank_payment_List)
                'Sohail (21 Jan 2016) -- Start
                'Enhancement - 58.1 changes in Web.
                'objBankPaymentList.generateReport(enBankPaymentReport.EFT_VOLTAMP, enPrintAction.None, enExportAction.None)
                objBankPaymentList.generateReportNew(Session("Database_Name").ToString, _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       objPeriod._Start_Date, _
                                                       objPeriod._End_Date, _
                                                       Session("UserAccessModeSetting").ToString, True, _
                                                       Session("ExportReportPath").ToString, _
                                                       CBool(Session("OpenAfterExport")), _
                                                       enBankPaymentReport.EFT_VOLTAMP, _
                                                       enPrintAction.None, enExportAction.None, _
                                                       CInt(Session("Base_CurrencyId")))
                'Sohail (21 Jan 2016) -- End

                'Pinkal (12-Jun-2014) -- Start
                'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
                'Sohail (12 Feb 2018) -- Start
                'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
                'ElseIf CInt(cboReportType.SelectedValue) = enBankPaymentReport.Bank_Payment_Voucher Then 'BANK PAYMENT VOUCHER REPORT
            ElseIf CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Bank_Payment_Voucher) Then 'BANK PAYMENT VOUCHER REPORT
                'Sohail (12 Feb 2018) -- End
                objBankPaymentList._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

                'Pinkal (16-Apr-2016) -- Start
                'Enhancement - Implementing Date Format (DD-MMM-YYYY) as Per Mr.Andrew's Comment.
                Session("ExportReportPath") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
                'Pinkal (16-Apr-2016) -- End

                objBankPaymentList._OpenAfterExport = False

                Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.Bank_Payment_Voucher)
                'Sohail (21 Jan 2016) -- Start
                'Enhancement - 58.1 changes in Web.
                'objBankPaymentList.generateReport(enBankPaymentReport.Bank_Payment_Voucher, enPrintAction.None, enExportAction.ExcelExtra)
                objBankPaymentList.generateReportNew(Session("Database_Name").ToString, _
                                                               CInt(Session("UserId")), _
                                                               CInt(Session("Fin_year")), _
                                                               CInt(Session("CompanyUnkId")), _
                                                               objPeriod._Start_Date, _
                                                               objPeriod._End_Date, _
                                                               Session("UserAccessModeSetting").ToString, True, _
                                                               Session("ExportReportPath").ToString, _
                                                               CBool(Session("OpenAfterExport")), _
                                                               enBankPaymentReport.Bank_Payment_Voucher, enPrintAction.None, enExportAction.ExcelExtra, _
                                                               CInt(Session("Base_CurrencyId")))
                'Sohail (21 Jan 2016) -- End

                If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objBankPaymentList._FileNameAfterExported
                    'Shani [ 10 DEC 2014 ] -- START
                    'Issue : Chrome is not supporting ShowModalDialog option now."
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Openwindow", "window.showModalDialog('../Reports/default.aspx','mywindow','menubar=0,resizable=0,width=250,height=250,modal=yes');", True)
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End

                    'Shani [ 10 DEC 2014 ] -- END
                End If
                'Pinkal (12-Jun-2014) -- End

                'Sohail (12 Feb 2018) -- Start
                'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            ElseIf CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Salary_Format_WPS) Then 'Salary Format WPS

                objBankPaymentList._ExportReportPath = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)

                'Sohail (24 Mar 2018) -- Start
                'Voltamp Enhancement - 70.2 - Changes in WPS report as per discussion with Prabhakar.
                'Session("ExportReportPath") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
                'Hemant (16 Jul 2020) -- Start
                'ENHANCEMENT(VOLTAMP) #0004786: New WPS Salary format
                If chkSaveAsTXT_WPS.Checked = True Then
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\SalaryFormatWPS.txt"
                Else
                    'Hemant (16 Jul 2020) -- End
                    'Hemant (25 Sep 2020) -- Start
                    'Enhancement (Allan Comments On JIRA) : This client wants this EFT in XLs and not CSV as provided.  Please assist. He miscommunicated this bit.
                    'Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\SalaryFormatWPS.csv"
                    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp)
                    'Hemant (25 Sep 2020) -- End
                End If 'Hemant (16 Jul 2020)
                'Sohail (24 Mar 2018) -- End
                objBankPaymentList._OpenAfterExport = False

                Call objBankPaymentList.setDefaultOrderBy(enBankPaymentReport.Salary_Format_WPS)

                'Sohail (16 Apr 2018) -- Start
                'Voltamp Enhancement - Ref # 227 : Changes in "Salary Format WPS" report in 72.1.
                'If objBankPaymentList.Generate_Salary_Format_WPS_Report(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, Session("UserAccessModeSetting").ToString, True, False, CInt(enBankPaymentReport.Salary_Format_WPS), True, Session("fmtCurrency").ToString, Session("ExportReportPath").ToString, CBool(Session("OpenAfterExport"))) = True Then
                If objBankPaymentList.Generate_Salary_Format_WPS_Report(Session("Database_Name").ToString, CInt(Session("UserId")), CInt(Session("Fin_year")), CInt(Session("CompanyUnkId")), objPeriod._Start_Date, objPeriod._End_Date, Session("UserAccessModeSetting").ToString, True, False, CInt(enBankPaymentReport.Salary_Format_WPS), True, Session("fmtCurrency").ToString, Session("ExFileName").ToString, CBool(Session("OpenAfterExport")), chkSaveAsTXT_WPS.Checked) = True Then
                    'Hemant (25 Sep 2020) -- [chkSaveAsTXT.Checked]
                    'Sohail (16 Apr 2018) -- End

                    'Sohail (24 Mar 2018) -- Start
                    'Voltamp Enhancement - 70.2 - Changes in WPS report as per discussion with Prabhakar.
                    'If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                    '    Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objBankPaymentList._FileNameAfterExported
                    '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    'End If
                    'Gajanan (3 Jan 2019) -- Start
                    'Enhancement :Add Report Export Control 
                    'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    Export.Show()
                    'Gajanan (3 Jan 2019) -- End


                    'Sohail (24 Mar 2018) -- End

                End If
                'Hemant (25 Sep 2020) -- Start
                'Enhancement (Allan Comments On JIRA) : This client wants this EFT in XLs and not CSV as provided.  Please assist. He miscommunicated this bit.
                If chkSaveAsTXT.Checked = False Then
                    If objBankPaymentList._FileNameAfterExported.Trim <> "" Then
                        Session("ExFileName") = IO.Path.GetFullPath(My.Computer.FileSystem.SpecialDirectories.Temp) & "\" & objBankPaymentList._FileNameAfterExported
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Open_window", "ShowFileSaveDialog();", True)
                    End If
                End If
                'Hemant (25 Sep 2020) -- End

                'Sohail (12 Feb 2018) -- End

            Else
                objBankPaymentList.setDefaultOrderBy(CInt(CType(cboReportType.SelectedItem, ListItem).Value))
                'Sohail (21 Jan 2016) -- Start
                'Enhancement - 58.1 changes in Web.
                'objBankPaymentList.generateReport(CInt(CType(cboReportType.SelectedItem, ListItem).Value), enPrintAction.None, enExportAction.None)
                objBankPaymentList.generateReportNew(Session("Database_Name").ToString, _
                                                       CInt(Session("UserId")), _
                                                       CInt(Session("Fin_year")), _
                                                       CInt(Session("CompanyUnkId")), _
                                                       objPeriod._Start_Date, _
                                                       objPeriod._End_Date, _
                                                       Session("UserAccessModeSetting").ToString, True, _
                                                       Session("ExportReportPath").ToString, _
                                                       CBool(Session("OpenAfterExport")), _
                                                       CInt(CType(cboReportType.SelectedItem, ListItem).Value), enPrintAction.None, enExportAction.None, _
                                                       CInt(Session("Base_CurrencyId")))
                'Sohail (21 Jan 2016) -- End
            End If
            'Sohail (15 Mar 2014) -- End

            'Pinkal (12-Jun-2014) -- Start
            'Enhancement : TANAPA Changes [Loan & Bank Payment Report Changes]
            'Sohail (12 Feb 2018) -- Start
            'Voltamp Enhancement Ref. # 175: New Bank Payment List Report "Salary Format WPS" in 70.1.
            'If CInt(cboReportType.SelectedValue) <> enBankPaymentReport.Bank_Payment_Voucher Then 'BANK PAYMENT VOUCHER REPORT
            'Sohail (24 Mar 2018) -- Start
            'Voltamp Enhancement - 70.2 - Changes in WPS report as per discussion with Prabhakar.
            'If CInt(CType(cboReportType.SelectedItem, ListItem).Value) <> CInt(enBankPaymentReport.Bank_Payment_Voucher) Then 'BANK PAYMENT VOUCHER REPORT
            If Not (CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Bank_Payment_Voucher) OrElse CInt(CType(cboReportType.SelectedItem, ListItem).Value) = CInt(enBankPaymentReport.Salary_Format_WPS)) Then 'BANK PAYMENT VOUCHER REPORT
                'Sohail (24 Mar 2018) -- End
                'Sohail (12 Feb 2018) -- End
                Session("objRpt") = objBankPaymentList._Rpt
                Response.Redirect("../Aruti Report Structure/Report.aspx", False)
            End If
            'Pinkal (12-Jun-2014) -- End

        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            ResetValue()
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.ViewState("FirstOpenPeriod") = Nothing
            Response.Redirect("~\UserHome.aspx", False)
        Catch ex As Exception
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub


    'SHANI [13 Mar 2015]-START
    'Enhancement - New EFT Report Custom CSV Report.
    Protected Sub btnEFTSaveSelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEFTSaveSelection.Click
        Try
            If IsEFTValidate() = False Then Exit Sub
            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            'For i As Integer = 0 To 2
            For i As Integer = 0 To GetEnumHighestValue(Of enReportMode)()
                'Hemant (27 June 2019) -- End
                objUserDefRMode = New clsUserDef_ReportMode
                Dim intUnkid As Integer = -1

                objUserDefRMode._Reportunkid = enArutiReport.BankPaymentList
                'Sohail (25 Mar 2015) -- Start
                'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                'objUserDefRMode._Reporttypeid = 0
                objUserDefRMode._Reporttypeid = CInt(CType(cboReportType.SelectedItem, ListItem).Value)
                'Sohail (25 Mar 2015) -- End
                objUserDefRMode._Reportmodeid = mintModeId

                If i = enReportMode.CUSTOM_COLUMNS Then 'Custom Columns


                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mstrEFTCustomColumnsIds
                    'Sohail (25 Mar 2015) -- Start
                    'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                    'intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, mintModeId, i)
                    intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, CInt(CType(cboReportType.SelectedItem, ListItem).Value), mintModeId, i)
                    'Sohail (25 Mar 2015) -- End


                ElseIf i = enReportMode.SHOW_REPORT_HEADER Then 'Show Column Header
                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mblnShowColumnHeader.ToString
                    'Sohail (25 Mar 2015) -- Start
                    'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                    'intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, 0, mintModeId, i)
                    intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, CInt(CType(cboReportType.SelectedItem, ListItem).Value), mintModeId, i)
                    'Sohail (25 Mar 2015) -- End

                    'Sohail (13 Apr 2016) -- Start
                    'Enhancement - 58.1 - Allow to show Employee Membership No. on EFT Custom Column XLS and CSV Report in Bank Payment List Report.
                ElseIf i = enReportMode.MEMBERSHIP Then

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mintMembershipUnkId.ToString

                    intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, CInt(CType(cboReportType.SelectedItem, ListItem).Value), mintModeId, i)
                    'Sohail (13 Apr 2016) -- End

                    'Hemant (27 June 2019) -- Start
                    'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
                ElseIf i = enReportMode.SAVE_AS_TXT Then 'SAVE AS TXT

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mblnSaveAsTXT.ToString

                    intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, CInt(CType(cboReportType.SelectedItem, ListItem).Value), mintModeId, i)

                ElseIf i = enReportMode.TAB_DELIMITER Then 'TAB DELIMITER

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mblnTabDelimiter.ToString

                    intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, CInt(CType(cboReportType.SelectedItem, ListItem).Value), mintModeId, i)
                    'Hemant (27 June 2019) -- End

                    'Hemant (02 Jul 2020) -- Start
                    'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
                ElseIf i = enReportMode.DATE_FORMAT Then 'DATE FORMAT

                    objUserDefRMode._Headtypeid = i
                    objUserDefRMode._EarningTranHeadIds = mstrDateFormat.ToString

                    intUnkid = objUserDefRMode.isExist(enArutiReport.BankPaymentList, CInt(CType(cboReportType.SelectedItem, ListItem).Value), mintModeId, i)
                    'Hemant (02 Jul 2020) -- End

                End If
                objUserDefRMode._Reportmodeunkid = intUnkid

                If intUnkid <= 0 Then
                    objUserDefRMode.Insert()
                Else
                    objUserDefRMode.Update()
                End If
            Next
            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            'DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(),CInt(HttpContext.Current.Session("LangId")),mstrModuleName, 2, "Information Successfully Saved."), Me)
            DisplayMessage.DisplayMessage(Basepage.GetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), CInt(HttpContext.Current.Session("LangId")), mstrModuleName1, 2, "Information Successfully Saved."), Me)
            'Hemant (27 June 2019) -- End

            Call Export_CSV_XLS()
        Catch ex As Exception
            popup_EFTCustom.Show()
            DisplayMessage.DisplayError(ex, Me)
        End Try
    End Sub

    Protected Sub btnEFTOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEFTOK.Click
        Try
            Call Export_CSV_XLS()
        Catch ex As Exception
            popup_EFTCustom.Show()
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayMessage.DisplayError(ex, Me)
            DisplayMessage.DisplayError(ex, Me)
            'Sohail (23 Mar 2019) -- End

        End Try
    End Sub
    'SHANI [13 Mar 2015]--END 

#End Region

#Region "ImageButton Events"

    'Nilay (01-Feb-2015) -- Start
    'Enhancement - REDESIGN SELF SERVICE.
    'Protected Sub Closebutton_CloseButton_click(ByVal sen As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Closebutton1.CloseButton_click
    '    Try
    '        Me.ViewState("FirstOpenPeriod") = Nothing
    '        Response.Redirect("~\UserHome.aspx", False)
    '    Catch ex As Exception
    '        DisplayMessage.DisplayError(ex, Me)
    '    End Try

    'End Sub
    'Nilay (01-Feb-2015) -- End
#End Region

    Private Sub SetLanguage()
        Try
            'Language.setLanguage(mstrModuleName)
            Me.Title = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.Title)
            'Nilay (01-Feb-2015) -- Start
            'Enhancement - REDESIGN SELF SERVICE.
            'Me.Closebutton1.PageHeading = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(),mstrModuleName,CInt(HttpContext.Current.Session("LangId")),mstrModuleName, Me.Closebutton1.PageHeading)
            Me.lblPageHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), mstrModuleName, Me.lblPageHeader.Text)
            'Nilay (01-Feb-2015) -- End
            Me.lblEmployeeName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblEmployeeName.ID, Me.lblEmployeeName.Text)
            Me.lblCountryName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCountryName.ID, Me.lblCountryName.Text)
            Me.lblBranchName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBranchName.ID, Me.lblBranchName.Text)
            Me.lblBankName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBankName.ID, Me.lblBankName.Text)
            Me.lblAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAmount.ID, Me.lblAmount.Text)
            Me.lblAmountTo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAmountTo.ID, Me.lblAmountTo.Text)
            Me.lblPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPeriod.ID, Me.lblPeriod.Text)
            Me.chkEmployeeSign.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkEmployeeSign.ID, Me.chkEmployeeSign.Text)
            Me.chkSignatory3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkSignatory3.ID, Me.chkSignatory3.Text)
            Me.chkSignatory2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkSignatory2.ID, Me.chkSignatory2.Text)
            Me.chkSignatory1.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkSignatory1.ID, Me.chkSignatory1.Text)
            Me.chkShowGroupByBankBranch.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowGroupByBankBranch.ID, Me.chkShowGroupByBankBranch.Text)
            Me.lblCurrency.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCurrency.ID, Me.lblCurrency.Text)
            Me.lblReportType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblReportType.ID, Me.lblReportType.Text)
            Me.lblReportMode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblReportMode.ID, Me.lblReportMode.Text)
            Me.lblCompanyBankName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCompanyBankName.ID, Me.lblCompanyBankName.Text)
            Me.lblCompanyBranchName.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCompanyBranchName.ID, Me.lblCompanyBranchName.Text)
            Me.lblCompanyAccountNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCompanyAccountNo.ID, Me.lblCompanyAccountNo.Text)
            Me.chkShowFNameSeparately.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowFNameSeparately.ID, Me.chkShowFNameSeparately.Text)
            Me.lnkEFTCityBankExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFTCityBankExport.ID, Me.lnkEFTCityBankExport.Text)
            Me.chkDefinedSignatory.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkDefinedSignatory.ID, Me.chkDefinedSignatory.Text)
            Me.lblChequeNo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblChequeNo.ID, Me.lblChequeNo.Text)
            Me.chkShowSortCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowSortCode.ID, Me.chkShowSortCode.Text)
            Me.chkShowPayrollPeriod.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowPayrollPeriod.ID, Me.chkShowPayrollPeriod.Text)
            Me.chkDefinedSignatory3.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkDefinedSignatory3.ID, Me.chkDefinedSignatory3.Text)
            Me.chkDefinedSignatory2.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkDefinedSignatory2.ID, Me.chkDefinedSignatory2.Text)
            Me.chkShowBranchCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowBranchCode.ID, Me.chkShowBranchCode.Text)
            Me.chkShowBankCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowBankCode.ID, Me.chkShowBankCode.Text)
            Me.chkShowEmployeeCode.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowEmployeeCode.ID, Me.chkShowEmployeeCode.Text)
            Me.chkShowAccountType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowAccountType.ID, Me.chkShowAccountType.Text)
            Me.chkShowReportHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowReportHeader.ID, Me.chkShowReportHeader.Text)
            Me.lnkEFT_CBA_Export.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFT_CBA_Export.ID, Me.lnkEFT_CBA_Export.Text).Replace("&", "")
            Me.lnkMobileMoneyEFTMPesaExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkMobileMoneyEFTMPesaExport.ID, Me.lnkMobileMoneyEFTMPesaExport.Text).Replace("&", "")
            Me.lnkEFT_EXIM_Export.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFT_EXIM_Export.ID, Me.lnkEFT_EXIM_Export.Text).Replace("&", "")
            Me.lnkEFT_Custom_CSV_Export.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFT_Custom_CSV_Export.ID, Me.lnkEFT_Custom_CSV_Export.Text).Replace("&", "")
            Me.lnkEFT_Custom_XLS_Export.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFT_Custom_XLS_Export.ID, Me.lnkEFT_Custom_XLS_Export.Text).Replace("&", "")
            Me.lblCutOffAmount.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCutOffAmount.ID, Me.lblCutOffAmount.Text)
            Me.lnkEFT_FlexCubeRetailGEFU_Export.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFT_FlexCubeRetailGEFU_Export.ID, Me.lnkEFT_FlexCubeRetailGEFU_Export.Text).Replace("&", "")
            Me.lnkEFT_ECO_Bank.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFT_ECO_Bank.ID, Me.lnkEFT_ECO_Bank.Text).Replace("&", "")
            Me.lnkEFTBarclaysBankExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFTBarclaysBankExport.ID, Me.lnkEFTBarclaysBankExport.Text).Replace("&", "")
            Me.btnClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnClose.ID, Me.btnClose.Text).Replace("&", "")
            Me.lblPresentDays.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPresentDays.ID, Me.lblPresentDays.Text)
            Me.lblBasicSalary.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblBasicSalary.ID, Me.lblBasicSalary.Text)
            Me.lblSocialSecurity.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblSocialSecurity.ID, Me.lblSocialSecurity.Text)
            Me.lblExtraIncome.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblExtraIncome.ID, Me.lblExtraIncome.Text)
            Me.lblIdentityType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblIdentityType.ID, Me.lblIdentityType.Text)
            Me.lblPaymentType.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblPaymentType.ID, Me.lblPaymentType.Text)
            Me.lblAbsentDays.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblAbsentDays.ID, Me.lblAbsentDays.Text)
            Me.lblCustomCurrFormat.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblCustomCurrFormat.ID, Me.lblCustomCurrFormat.Text)
            Me.lnkEFTBarclaysBankExport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFTBarclaysBankExport.ID, Me.lnkEFTBarclaysBankExport.Text).Replace("&", "")
            'S.SANDEEP [04-May-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#215|#ARUTI-146}
            Me.lnkEFTNationalBankMalawi.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFTNationalBankMalawi.ID, Me.lnkEFTNationalBankMalawi.Text).Replace("&", "")
            'S.SANDEEP [04-May-2018] -- END
            'Sohail (04 Nov 2019) -- Start
            'Mainspring Resourcing Ghana Enhancement # 0004103 : New EFT integration "FNB Bank EFT" in csv format.
            Me.btnReport.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.btnReport.ID, Me.btnReport.Text).Replace("&", "")
            Me.lnkEFT_FNB_Bank_Export.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFT_FNB_Bank_Export.ID, Me.lnkEFT_FNB_Bank_Export.Text).Replace("&", "")
            'Sohail (04 Nov 2019) -- End
            'Hemant (11 Dec 2019) -- Start
            'Ref # 4324 (SANLAM LIFE INSURANCE) : ARUTI-STANDARD CHARTERED STRAIGHT TO BANK (S2B) INTEGRATION FILE (NEW REQUIREMENT)
            Me.lnkEFTStandardCharteredBank_S2B.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFTStandardCharteredBank_S2B.ID, Me.lnkEFTStandardCharteredBank_S2B.Text).Replace("&", "")
            'Hemant (11 Dec 2019) -- End
            'Hemant (29 Jun 2020) -- Start
            'ENHANCEMENT(Benchmark) # 0004751: ABSA BANK EFT
            Me.lnkEFT_ABSA_Bank.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFT_ABSA_Bank.ID, Me.lnkEFT_ABSA_Bank.Text).Replace("&", "")
            'Hemant (29 Jun 2020) -- End
            Me.lnkEFTNationalBankMalawiXLSX.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFTNationalBankMalawiXLSX.ID, Me.lnkEFTNationalBankMalawiXLSX.Text).Replace("&", "")
            Me.lblMembershipRepo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lblMembershipRepo.ID, Me.lblMembershipRepo.Text).Replace("&", "")
            Me.lnkEFTEquityBankKenya.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFTEquityBankKenya.ID, Me.lnkEFTEquityBankKenya.Text).Replace("&", "")
            Me.lnkEFTNationalBankKenya.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFTNationalBankKenya.ID, Me.lnkEFTNationalBankKenya.Text).Replace("&", "")
            Me.lnkEFTCitiBankKenya.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.lnkEFTCitiBankKenya.ID, Me.lnkEFTCitiBankKenya.Text).Replace("&", "")

            'Hemant (20 Jul 2019) -- Start
            'ENHANCEMENT#3913(GOOD NEIGHBORS) : The address for Company Group, Company and Bank Branch. Also company logo should also be in the report.
            Me.chkShowSelectedBankInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowSelectedBankInfo.ID, Me.chkShowSelectedBankInfo.Text)
            Me.chkShowCompanyGrpInfo.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, CInt(HttpContext.Current.Session("LangId")), Me.chkShowCompanyGrpInfo.ID, Me.chkShowCompanyGrpInfo.Text)
            'Hemant (20 Jul 2019) -- End

            'EFT CUSTOM COLUMNS
            'Language.setLanguage(mstrModuleName1) 'Hemant (27 June 2019)
            lvEFTCustomColumns.Columns(1).HeaderText = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), lvEFTCustomColumns.Columns(2).FooterText, lvEFTCustomColumns.Columns(2).HeaderText)
            Me.btnEFTSaveSelection.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnEFTSaveSelection.ID, Me.btnEFTSaveSelection.Text)
            Me.btnEFTOK.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnEFTOK.ID, Me.btnEFTOK.Text)
            Me.btnEFTClose.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.btnEFTClose.ID, Me.btnEFTClose.Text)
            Me.chkShowColumnHeader.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.chkShowColumnHeader.ID, Me.chkShowColumnHeader.Text)
            Me.lblMembership.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblMembership.ID, Me.lblMembership.Text)
            'Hemant (27 June 2019) -- Start
            'ENHANCEMENT :  REF # 0003777: Bank upload file format. same as EFT custom Xls,CSV now add custom txt with same  option with TAB delimiter
            Me.chkSaveAsTXT.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.chkSaveAsTXT.ID, Me.chkSaveAsTXT.Text)
            Me.chkTABDelimiter.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.chkTABDelimiter.ID, Me.chkTABDelimiter.Text)
            'Hemant (27 June 2019) -- End
            'Hemant (02 Jul 2020) -- Start
            'ENHANCEMENT(Freight Forwarders Kenya) #0004769: Add Payment Currency and Payment Date fields to EFT Custom Columns
            Me.lblDateFormat.Text = Basepage.SetWebLanguage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, CInt(HttpContext.Current.Session("LangId")), Me.lblDateFormat.ID, Me.lblDateFormat.Text)
            'Hemant (02 Jul 2020) -- End
        Catch Ex As Exception
            'Sohail (23 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - OWASP Changes, Dont show actual error message to client.
            'DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
            DisplayMessage.DisplayError(Ex, Me)
            'Sohail (23 Mar 2019) -- End
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Sub SetMessages()
        Try
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 1, "Period is mandatory information.Please select Period.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 2, "Bank Payment List")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 3, "Electronic Funds Transfer")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 4, "Currency is mandatory information. Please select Currency.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 5, "Please select Company Bank.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 6, "Please select Company Bank Branch.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 7, "Please select Company Bank Account.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 8, "DRAFT")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 9, "AUTHORIZED")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 10, "EFT Standard Bank")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 12, "EFT Standard Chartered Bank")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 13, "EFT SFI Bank")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 14, "Please select Authorize mode to generate report.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 15, "Show Group By Bank / Branch")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 16, "Show Group By Bank")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 17, "Please Set Data File Export Path From Aruti Configuration -> Option -> Integration -> EFT Integration.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 18, "Please Set Data File Name From Aruti Configuration -> Option -> Integration -> EFT Integration.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 21, "EFT Advance Salary CSV")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 22, "EFT Advance Salary XSV")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 23, "Please set Posting Date.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 27, "Salary Format WPS")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName, 31, "Please select membership to generate report.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 1, "Please Select atleast one column to export EFT report.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 2, "Information Successfully Saved.")
            Basepage.SetWebMessage(HttpContext.Current.Session("Database_Name").ToString(), mstrModuleName1, 3, "Please Select Membership.")

        Catch Ex As Exception
            DisplayMessage.DisplayError(Ex, Me)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
