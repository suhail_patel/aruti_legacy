﻿<%@ Page Language="VB" MasterPageFile="~/Home1.master" AutoEventWireup="false" CodeFile="wPg_PaymentApproval.aspx.vb"
    Inherits="Payroll_wPg_PaymentApproval" Title="Payment Approval List" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Controls/Closebutton.ascx" TagName="Closebutton" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/DateCtrl.ascx" TagName="DateCtrl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/GetComboList.ascx" TagName="DropDownList" TagPrefix="uc7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery-1.7.1.min.js"></script>

    <script type="text/javascript" src="../Help/aruti_help_managers/js/jquery.searchabledropdown-1.0.8.src.js"></script>

    <script type="text/javascript">
    function pageLoad(sender, args) {
        $("select").searchable();
    }
    </script>--%>

    <script type="text/javascript">
    function SetGeidScrolls()
    {
        var arrPnl=$('.gridscroll');
        for(j = 0; j < arrPnl.length; j++)
        {
            var trtag=$(arrPnl[j]).find('.gridview').children('tbody').children();
            if (trtag.length>52)
            {
                var trheight=0;
                for (i = 0; i < 52; i++) { 
                    trheight = trheight + $(trtag[i]).height();
                }
                $(arrPnl[j]).css("overflow", "auto");
                $(arrPnl[j]).css("height", trheight+"px"); 
            }
            else{
                $(arrPnl[j]).css("overflow", "auto"); 
                $(arrPnl[j]).css("height", "100%"); 
            }
        }
    }
    </script>

    <input type="hidden" id="endreq" value="0" />
    <input type="hidden" id="bodyy" value="0" />
    <asp:HiddenField ID="hfScrollPosition" runat="server" Value="0" />

    <script type="text/javascript">
        var prm;
            prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_beginRequest(beginRequestHandler);
            prm.add_endRequest(endRequestHandler);

            $(window).scroll(function () {
                var cend = $("#endreq").val();
                if (cend == "1") {
                    $("#endreq").val("0");
                    var nbodyY = $("#bodyy").val();
                    $(window).scrollTop(nbodyY);
                }
            });
            
            var scroll = {
                Y: '#<%= hfScrollPosition.ClientID %>'
            };
            
function beginRequestHandler(sender, args) {
    $("#endreq").val("0");
    $("#bodyy").val($(window).scrollTop());
}

function endRequestHandler(sender, args) {
    $("#endreq").val("1");
    
     if (args.get_error() == undefined) {
            $("#scrollable-container").scrollTop($(scroll.Y).val());
           
    }
}
    </script>

    <asp:Panel ID="MainPan" runat="server" Style="width: 100%">
        <asp:UpdatePanel ID="uppnl_mian" runat="server">
            <ContentTemplate>
                <div class="block-header">
                    <h2>
                        <asp:Label ID="lblPageHeader" runat="server" Text="Payment Approval List" CssClass="form-label"></asp:Label>
                    </h2>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="header">
                                <h2>
                                    <asp:Label ID="lblDetialHeader" runat="server" Text="Filter Criteria" CssClass="form-label"></asp:Label>
                                </h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblPeriod" runat="server" Text="Pay Period" CssClass="form-label">
                                        </asp:Label>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboPeriod" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <asp:Label ID="lblEmployee" runat="server" Text="Employee" CssClass="form-label">
                                        </asp:Label>
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="cboEmployee" runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                <asp:Button ID="btnApprove" runat="server" Text="Approve" CssClass="btn btn-primary" />
                                <asp:Button ID="btnVoidApproved" runat="server" Text="Void/Reject Approval" CssClass="btn btn-default" />
                                <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-default" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="card">
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="table-responsive" style="max-height: 400px;">
                                            <asp:Panel ID="pnl_gvApproval" ScrollBars="Auto" runat="server">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:DataGrid ID="gvApproval" runat="server" Style="margin: auto" AutoGenerateColumns="false"
                                                            AllowPaging="false" Width="99%" CssClass="table table-hover table-bordered" RowStyle-Wrap="false">
                                                            <Columns>
                                                                <asp:BoundColumn DataField="employeename" HeaderText="Employee" FooterText="colhEmployee" />
                                                                <asp:BoundColumn DataField="period_name" HeaderText="Period Name" FooterText="colhPeriodName" />
                                                                <asp:BoundColumn DataField="voucherno" HeaderText="Vou. No." FooterText="colhVNo" />
                                                                <asp:BoundColumn DataField="username" HeaderText="Approver" FooterText="colhApprover" />
                                                                <asp:BoundColumn DataField="levelname" HeaderText="Level" FooterText="colhLevel" />
                                                                <asp:BoundColumn DataField="priority" HeaderText="Priority" FooterText="colhPriority" />
                                                                <asp:BoundColumn DataField="statusname" HeaderText="Status" FooterText="colhStatus" />
                                                                <asp:BoundColumn DataField="approval_date" HeaderText="Approval Date" FooterText="colhApprovalDate" />
                                                                <asp:BoundColumn DataField="approvedbyusername" HeaderText="By User" FooterText="colhByUser" />
                                                                <asp:BoundColumn DataField="remarks" HeaderText="Remarks" FooterText="colhRemarks" />
                                                                <asp:BoundColumn DataField="IsGrp" Visible="false" />
                                                            </Columns>
                                                        </asp:DataGrid>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                                                        <asp:AsyncPostBackTrigger ControlID="btnReset" EventName="Click" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
